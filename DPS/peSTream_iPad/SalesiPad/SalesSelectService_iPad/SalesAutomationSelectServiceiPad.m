//
//  SalesAutomationSelectService.m
//  DPS
//
//  Created by Rakesh Jain on 19/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
#import "AllImportsViewController.h"
#import "SalesAutomationSelectServiceiPad.h"
#import "SalesAutomationServiceTableViewCell.h"
#import "SalesAutomationTableViewCellSecond.h"
#import "SalesAutomationServiceSummary.h"
#import "AddStandardService.h"
#import "AddNonStandardServices.h"
#import "Global.h"
#import "AddPlusService.h"
#import "GlobalSyncViewController.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>
#import "SelectServiceCouponDiscountTableViewCell.h"
#import "RenewalServiceTableViewCell.h"
#import "DPS-Swift.h"
@interface SalesAutomationSelectServiceiPad ()<CLLocationManagerDelegate>

{
    UIButton *btnTemp;
    UILabel *lblTemp,*lblBorder,*lblBorder2;
    NSArray *arrOfLeads;
    NSMutableArray *arrData,*arrData1,*arrCategory,*arrFinalIndexPath,*arrFinalIndexPathNonStan;
    UITableView *tblData;
    SalesAutomationServiceTableViewCell *cell1;
    SalesAutomationTableViewCellSecond *cell2;
    BOOL chkScroll,chkScroll2;
    NSMutableArray *arrInitialPrice,*arrDiscount,*arrMaintenancePrice,*arrFrequencyName,*arrServiceName,*arrStanIsSold,*arrSysName,*arrSysNameConditionalStandard;
    NSMutableArray *arrDiscountPer;
    
    NSMutableArray *arrNonStanInitialPrice,*arrNonStanDiscount,*arrNonStanServiceName,*arrNonStanServiceDesc,*arrNonStanIsSold;
    NSMutableArray *arrTem1Initial,*arrTem2Discount,*arrTem3Maintenance,*arrTemFreqName,*arrTemServiceName;
    NSMutableArray *arrNonStanTem1Initial,*arrNonStanTem2Discount;
    BOOL chkClickStan;
    NSString *strLeadId;
    BOOL chkBtnCheckBoxStan,chkBtnCheckBoxNonStan;
    Global *global;
    NSMutableArray *arrDepartmentName,*arrDepartmentSysName;
    //Nilind
    NSMutableArray *arrMasterId;
    NSDictionary *dictServiceName;
    //.............
    BOOL chkkkk,chkStatus;
    NSString *strBranchSysName;
    
    
    BOOL chkPlusService;
    BOOL chkBox,isEditedInSalesAuto;
    UIButton *btnAddRecord;
    NSDictionary *dictForDepartment;
    NSDictionary*dictFreqNameFromSysName;
    NSMutableArray *arrUnit,*arrFinalInitialPrice,*arrFinalMaintPrice;
    NSDictionary *dictQuantityStatus,*dictPackageName;
    NSMutableArray *arrPackageNameId;
    NSString *strCouponStatus,*strCouponStatusNonStan;
    ;
    BOOL isChkBoxEnable,isCheckBoxClickFirstTime;
    BOOL isCheckBoxClickFirstTimeNonStan,isChkBoxEnableNonStan;
    NSMutableArray *arrBillingFreqSysName;
    
    NSMutableArray *arrDiscountPerNonStan;
    
    
    //Nilind 14 Sept Image Change
    
    BOOL chkForDuplicateImageSave;
    
    NSMutableArray *arrNoImage;
    
    
    //Nilind 07 Jun ImageCaption
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    NSMutableArray *arrOfImageCaption,*arrOfImageDescription,*arrOfImageCaptionGraph,*arrOfImageDescriptionGraph;
    NSMutableArray *arrImagePath,*arrGraphImage;
    NSString *strLeadStatusGlobal,*strUserName,*strCompanyKey,*strServiceUrlMain;
    NSDictionary *dictBundleNameFromId;
    NSMutableArray *arrBundleId;
    
    //Bundle Code
    NSArray *arrMatchesBundle;
    NSArray *arrBundleRow;
    NSMutableArray *arrInitialPriceBundle,*arrDiscountPerBundle,*arrDiscountBundle,*arrMaintenancePriceBundle,*arrFrequencyNameBundle,*arrServiceNameBundle,*arrStanIsSoldBundle,*arrSysNameBundle,*arrUnitBundle,*arrFinalInitialPriceBundle,*arrFinalMaintPriceBundle,*arrPackageNameIdBundle,*arrBillingFreqSysNameBundle;
    NSMutableArray *arrTempBundelDetailId,*arrTempBundleServiceSysName;
    double totalInitialBundle,totalMaintBundle;
    NSString *strSoldStatusBundle;
    NSString *strBundleIdForTextField;
    NSMutableArray *arrSoldServiceStandardId;
    
    // Nilind 23 Nov
    
    NSMutableArray *arrImageLattitude,*arrImageLongitude,*arrImageGraphLattitude,*arrImageGraphLongitude;
    
    NSString *strStageSysName;
    NSMutableArray *arrRandomId;
    
    NSDictionary *dictServiceParmaterBasedStatus,*dictYearlyOccurFromFreqSysName;
    NSMutableArray *arrAdditionalParamterDcs;
    NSMutableArray *arrFreqSysName,*arrBillingFreqSysNameNonStan;
    NSMutableArray *arrTempCoupon;
    
    NSMutableArray *arrDiscountCoupon;
    NSDictionary *dictDiscountNameFromSysName;
    NSString *strAppliedDiscountServiceId;
    BOOL chkDirectServiceDelete;
    
    float appliedDiscountInitial;
    float appliedDiscountMaint;
    BOOL chkForSaveCoupon;
    NSMutableArray *arrSoldServiceStandardIdBundle;
    NSMutableArray *arrSoldServiceIdOfAllService;
    
    NSString *strAccountNoGlobal;
    NSDictionary *dictDiscountUsageFromSysName,*dictDiscountUsageFromCode;
    NSArray *arrAppliedCouponDetail;
    NSMutableArray *arrAllUnsoldServiceId;
    
    //Saavan Chnages on 9th july 2018
    float globalAmountInitialPrice,globalAmountMaintenancePrice;
    NSMutableArray *arrSoldServiceStandardIdForCoupon;
    
    
    NSArray *arrStanProposal,*arrNonStanProposal;
    
    NSArray *arrSoldAvailableStan,*arrSoldAvailableNonStan;
    BOOL chkFreqConfiguration;
    BOOL chkKeyboardCompeUp;
    NSMutableArray *arrMaintPriceNonStan,*arrServiceFreqSysNameNonStan;
    
    
    // Nilind 11 May 2020
    NSArray *arrAllObjRenewal;
    NSDictionary *dictRenewalMaster, *dictServiceNameForId;;
    NSMutableArray  *arrFrequencySysNameBundle, *arrServiceIdBundle, *arrServiceId, *arrSoldServiceIdBundleNew;

    NSMutableArray *arrTaxStatusNonStan,*arrSoldServiceNonStandId;

}
@end

@implementation SalesAutomationSelectServiceiPad

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *defsStanClick = [NSUserDefaults standardUserDefaults];
    [defsStanClick setBool:YES forKey:@"clickStan"];
    [defsStanClick synchronize];
    
    
    //chkDirectServiceDelete=NO;
    chkFreqConfiguration =NO;
    chkKeyboardCompeUp = NO;
    chkForSaveCoupon=NO;
    strBundleIdForTextField=@"0";
    appliedDiscountInitial=0;
    appliedDiscountMaint=0;
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strLeadStatusGlobal=[NSString stringWithFormat:@"%@",[defsLogindDetail valueForKey:@"leadStatusSales"]];
    
    if ([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strLeadStatusGlobal caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
    {
        
        strLeadStatusGlobal=@"Complete";
        
    }
    
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    chkFreqConfiguration =[[dictLoginData valueForKeyPath:@"Company.CompanyConfig.MaintPriceFreq_1"]boolValue];
    
    isEditedInSalesAuto=NO;
    
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    isEditedInSalesAuto=NO;
    chkPlusService=NO;
    isChkBoxEnable=NO;
    isCheckBoxClickFirstTime=NO;
    chkBox=NO;
    isCheckBoxClickFirstTimeNonStan=NO;
    isChkBoxEnableNonStan=NO;
    //Nilind  2 Nov
    [self salesFetch];
    
    
    //strCouponStatus=@"false";
    //strCouponStatus=@"true";
    
    //NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
    //chkStatus=[dfsStatus boolForKey:@"status"];
    if ([strCouponStatus isEqualToString:@"true"])
    {
        if (chkStatus==YES)
        {
            _const_Discount_H.constant=0;//0
            _const_TableStan_H.constant=_const_TableStan_H.constant+ _const_Discount_H.constant;
        }
        else
        {
            _const_Discount_H.constant=50-50;
            _const_TableStan_H.constant=_const_TableStan_H.constant- _const_Discount_H.constant;
        }
    }
    else
    {
        _const_Discount_H.constant=0;//0
        _const_TableStan_H.constant=_const_TableStan_H.constant+ _const_Discount_H.constant;
    }
    //_const_Discount_H.constant=50;
    
    [self getFrequencySysNameFromName];
    
    
    //..............
    
    chkkkk=NO;
    //Nilind
    
    arrMasterId=[[NSMutableArray alloc]init];
    [self serviceName];
    //..................
    
    _lblNameAcount.text=[defs valueForKey:@"lblName"];
    _lblNameTitle.text=[defs valueForKey:@"nameTitle"];
    _lblNameAcount.textAlignment=NSTextAlignmentCenter;
    _lblNameAcount.font=[UIFont boldSystemFontOfSize:26];
    _lblNameTitle.font=[UIFont boldSystemFontOfSize:26];
    
    global = [[Global alloc] init];
    [self getClockStatus];
    
    chkClickStan=YES;
    _btnCategory1.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _btnCategory1.layer.borderWidth=1.0;
    _btnCategory2.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _btnCategory2.layer.borderWidth=1.0;
    _btnCategory3.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _btnCategory3.layer.borderWidth=1.0;
    //Non StandardView Array creation of InitialPrice Discount and Maintenance
    arrNonStanTem1Initial=[[NSMutableArray alloc]init];
    arrNonStanTem2Discount=[[NSMutableArray alloc]init];
    // arrTem3Maintenance=[[NSMutableArray alloc]init];
    
    
    arrNonStanInitialPrice=[[NSMutableArray alloc]init];
    arrNonStanDiscount=[[NSMutableArray alloc]init];
    arrNonStanServiceName=[[NSMutableArray alloc]init];
    arrNonStanServiceDesc=[[NSMutableArray alloc]init];
    arrDepartmentName=[[NSMutableArray alloc]init];
    arrDepartmentSysName=[[NSMutableArray alloc]init];
    //
    
    //StandardView Array creation of InitialPrice Discount and Maintenance
    
    arrTem1Initial=[[NSMutableArray alloc]init];
    arrTem2Discount=[[NSMutableArray alloc]init];
    arrTem3Maintenance=[[NSMutableArray alloc]init];
    arrTemFreqName=[[NSMutableArray alloc]init];
    arrTemServiceName=[[NSMutableArray alloc]init];
    
    
    arrInitialPrice=[[NSMutableArray alloc]init];
    arrDiscount=[[NSMutableArray alloc]init];
    arrMaintenancePrice=[[NSMutableArray alloc]init];
    
    arrFrequencyName=[[NSMutableArray alloc]init];
    arrServiceName=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    //
    
    
    arrData=[[NSMutableArray alloc]init];
    arrData1=[[NSMutableArray alloc]init];
    arrCategory=[[NSMutableArray alloc]init];
    arrFinalIndexPath=[[NSMutableArray alloc]init];
    arrFinalIndexPathNonStan=[[NSMutableArray alloc]init];
    [arrCategory addObject:@"Residence"];
    [arrCategory addObject:@"Test"];
    [arrCategory addObject:@"Commercial"];
    //[_tblRecord setHidden:YES];
    
    _viewForNonStandardService.hidden=YES;
    _viewForStandardService.hidden=NO;
    btnTemp=[[UIButton alloc]init];
    lblTemp=[[UILabel alloc]init];
    btnTemp.frame=CGRectMake(0, 0, 150+50, 50);
    lblTemp.frame=CGRectMake(0, 0, 150+50, 50);
    arrOfLeads=[[NSMutableArray alloc]init];
    arrSysNameConditionalStandard=[[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
    arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Service",@"Service Summary",@"Agreement", nil];
    _scrollViewLeads.delegate=self;
    CGFloat scrollWidth=arrOfLeads.count*(160+40+20);
    
    [_scrollViewLeads setFrame:CGRectMake(0,_viewTop.frame.origin.y+_viewTop.frame.size.height,[UIScreen mainScreen].bounds.size.width,_scrollViewLeads.frame.size.height)];
    [_scrollViewLeads setContentSize:CGSizeMake(scrollWidth,100)];
    
    
    _scrollViewLeads.showsHorizontalScrollIndicator = NO;
    
    int var = 30;
    if([UIScreen mainScreen].bounds.size.width > 1300)
    {
        var = 50;
    }
    lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-5, (150+50+var)*3, 3)];

    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        lblBorder.frame=CGRectMake(0,_scrollViewLeads.contentSize.height-3, 100*3+15, 2);
    }
    lblBorder.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-5, _scrollViewLeads.frame.size.width+btnTemp.frame.size.width-35, 3)];
    lblBorder2.backgroundColor=[UIColor lightGrayColor];
    
    
    [_scrollViewLeads addSubview:lblBorder2];
    [_scrollViewLeads addSubview:lblBorder];
    _scrollViewLeads.backgroundColor=[UIColor whiteColor];
    [_scrollViewLeads addSubview:btnTemp];
    [_scrollViewLeads addSubview:lblTemp];
    [self addButtons];
    //Dynamic Table
    
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.dataSource=self;
    tblData.delegate=self;
    
    
    btnAddRecord=[[UIButton alloc]init];
    
    
    btnAddRecord.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-100, _viewForStandardService.frame.origin.y+[UIScreen mainScreen].bounds.size.height-300-20, 80, 80);
    
    //btnAddRecord.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-100, CGRectGetMaxY(_btnMarkAsLost.frame) - 40, 80, 80);

    
    btnAddRecord.layer.cornerRadius=40;
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        btnAddRecord.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-80, _viewForStandardService.frame.origin.y+[UIScreen mainScreen].bounds.size.height-120, 40, 40);
        btnAddRecord.layer.cornerRadius=20;
    }
    if ([UIScreen mainScreen].bounds.size.height==568)
    {
        btnAddRecord.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-80, _viewForStandardService.frame.origin.y+[UIScreen mainScreen].bounds.size.height-135, 40, 40);
        btnAddRecord.layer.cornerRadius=20;
    }
    
    btnAddRecord.layer.borderWidth=1;
    btnAddRecord.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    // [btnAddRecord setTitle:@"ADD" forState:UIControlStateNormal];
    [btnAddRecord setImage:[UIImage imageNamed:@"add_white.png"] forState:UIControlStateNormal];
    btnAddRecord.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
  //  [_viewForStandardService addSubview:btnAddRecord];
    [btnAddRecord addTarget:self action:@selector(addAction) forControlEvents:UIControlEventTouchUpInside];
   // [self.view addSubview:btnAddRecord];
    
    [_btnNonStandardService setBackgroundColor:[UIColor lightGrayColor]];
    
    //Nilind 2 Nov
    if (chkStatus==YES)
    {
        [btnAddRecord setEnabled:NO];
        _btnGlobalSync.enabled=NO;
        _btnMarkAsLost.hidden=YES;
        _btnApplyDiscount.enabled=NO;
        _btnAddService.enabled=NO;

        
    }
    else
    {
        _btnMarkAsLost.hidden=NO;
        
    }
    if ([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
    {
        _btnMarkAsLost.hidden=YES;
    }
    
    //........................
    //Standar Data Fetch
    [self fetchDepartmentName];
    [self fetchFromCoreDataStandard];
    [self fetchFromCoreDataStandardNewSaavan];
    /* UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
     [_tblRecord addGestureRecognizer:gestureRecognizer];
     [_tblNonStandardService addGestureRecognizer:gestureRecognizer];*/
    
    //Nilind 14 sEPT
    chkForDuplicateImageSave=NO;
    arrNoImage=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImagePath=[[NSMutableArray alloc]init];
    
    arrImageLattitude=[[NSMutableArray alloc]init];
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    
    [self fetchImageDetailFromDataBase];
    //_btnMarkAsLost.layer.borderWidth=1.0;
    
    // _btnMarkAsLost.layer.cornerRadius=5.0;
    //End
    
    _tblCouponDiscount.rowHeight=UITableViewAutomaticDimension;
    _tblCouponDiscount.estimatedRowHeight=50;
    _const_TableDiscount_H.constant=0;
    _const_ViewDiscountCoupon_H.constant=_const_ViewDiscountCoupon_H.constant-108;
    [self fetchForAppliedDiscountFromCoreData];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardCameUp:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWentAway:) name:UIKeyboardWillHideNotification object:nil];
    
       [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeOrientation:) name:UIDeviceOrientationDidChangeNotification object:nil];
}
- (void)changeOrientation:(NSNotification *)notification
{
    btnAddRecord.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-100, _viewForStandardService.frame.origin.y+[UIScreen mainScreen].bounds.size.height-300-20, 80, 80);

}
- (void)keyboardCameUp:(NSNotification *)notification {
    
    chkKeyboardCompeUp = YES;
    NSLog(@"Keyboard came up!");
}

- (void)keyboardWentAway:(NSNotification *)notification {
    
    chkKeyboardCompeUp = NO;
    NSLog(@"Keyboard went away!");
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [self salesFetch];
    //Nilind 2 Nov
    if (chkStatus==YES)
    {
        [btnAddRecord setEnabled:NO];
        _btnGlobalSync.enabled=NO;
        _btnMarkAsLost.hidden=YES;
        _btnApplyDiscount.enabled=NO;
         _btnAddService.enabled=NO;
    }
    else
    {
        _btnMarkAsLost.hidden=NO;
        
    }
    if ([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
    {
        _btnMarkAsLost.hidden=YES;
    }
    
    
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    [self fetchImageDetailFromDataBaseForGraph];
    
    isCheckBoxClickFirstTime=NO;
    isCheckBoxClickFirstTimeNonStan=NO;
    [self fetchFromCoreDataStandard];
    //temp 19 April
    [self fetchFromCoreDataStandardNewSaavan];
    //End
    
    [self serviceName];
    
    BOOL chk,chkNonStan;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    chk=[defs boolForKey:@"backStan"];
    
    if(chk==YES)
    {
        //Standar Data Fetch
        [self fetchFromCoreDataStandard];
        [_tblRecord reloadData];
        [_tblRecord scrollRectToVisible:CGRectMake(_tblRecord.contentSize.width - 1,_tblRecord.contentSize.height - 1, 1, 1) animated:YES];
        
    }
    
    chkNonStan=[defs boolForKey:@"backNonStandard"];
    if (chkNonStan==YES)
    {
        //Non Standar Data Fetch
        [self fetchFromCoreDataNonStandard];
        [_tblNonStandardService reloadData];
        [_tblNonStandardService scrollRectToVisible:CGRectMake(_tblNonStandardService.contentSize.width - 1,_tblNonStandardService.contentSize.height - 1, 1, 1) animated:YES];
        
    }
    
    
    if([defs boolForKey:@"isFromBackServiceSummary"]==YES)
    {
        [self fetchForAppliedDiscountFromCoreData];
        // [self fetchForAppliedDiscountFromCoreDataFromBack];
    }
    
  
    //Nilind 15 Sept IMAGE FETCH COLLECTION VIEW  FOOTER
    
    
    [self getImageCollectionView];
    [self heightManage];
    
    if (chkNonStan==YES)
    {
        int sumNonStan=0;
        for (int i=0; i<arrNonStanServiceName.count; i++)
        {
            sumNonStan=sumNonStan+_tblNonStandardService.rowHeight;
        }
        _const_TableNonStan_H.constant=sumNonStan+50+50;
        [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableNonStan_H.constant+100)];
        [defs setBool:NO forKey:@"backNonStandard"];
        [defs synchronize];
    }
    else
    {
        if ([defs boolForKey:@"clickStan"] == NO)
        {
            int sumNonStan=0;
            for (int i=0; i<arrNonStanServiceName.count; i++)
            {
                sumNonStan=sumNonStan+_tblNonStandardService.rowHeight;
            }
            _const_TableNonStan_H.constant=sumNonStan+50+50;
            [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableNonStan_H.constant+100)];
        }
    }
    //Nilind
    
    if([defs boolForKey:@"backStan"])
    {
        [self fetchForAppliedDiscountFromCoreData];

    }
    if([defs boolForKey:@"backNonStandard"])
    {

    }
    
}
//============================================================================
#pragma mark- DYNAMIC LABEL AND BUTTON METHOD
//============================================================================

-(void)addButtons
{
    int var = 30;
    if([UIScreen mainScreen].bounds.size.width > 1300)
    {
        var = 50;
    }
    btnTemp=[[UIButton alloc]init];
    lblTemp=[[UILabel alloc]init];
    btnTemp.frame=CGRectMake(0, 0, 150+50+var, 50);
    lblTemp.frame=CGRectMake(0, 0, 150+50+var, 50);
   
    for(int i =0;i<arrOfLeads.count;i++)
    {
        
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*(100+var)+20, 0, 50, 50)];
        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
        lbl.textColor = [UIColor whiteColor];
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 25;
        lbl.layer.borderWidth=2.0;
        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:14]];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        if (i==0)
        {
            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 150+var+50, 50);
            lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 50, 50);
            lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
            lbl.textColor = [UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        else
        {
            
            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 150+var+50, 50);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 50, 50);
            lbl.textColor = [UIColor darkGrayColor];
            lbl.backgroundColor=[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            
        }
        for(int j=0;j<=2;j++)
        {
            if (i==j)
            {
                
                
                lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
                lbl.textColor = [UIColor whiteColor];
                lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        }
        
        
        btnTemp.frame=btn.frame;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
        
        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
        btn.titleLabel.numberOfLines=2;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i;
        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
        [_scrollViewLeads addSubview:btn];
        
        [_scrollViewLeads addSubview:lbl];
    }
    
    
    CGFloat scrollWidth=arrOfLeads.count*(160+40+20+var);
    
    [_scrollViewLeads setFrame:CGRectMake(0,_viewTop.frame.origin.y+_viewTop.frame.size.height,[UIScreen mainScreen].bounds.size.width,_scrollViewLeads.frame.size.height)];
    [_scrollViewLeads setContentSize:CGSizeMake(scrollWidth,100)];
    _scrollViewLeads.showsHorizontalScrollIndicator = NO;
    
    
    //============================================================================
    //============================================================================
    //lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-5, 150+50+var, 3)];
    
   // lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-5, _scrollViewLeads.frame.size.width+btnTemp.frame.size.width-35, 3)];
    lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-5, (150+50+var)*3, 3)];
    lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-5, _scrollViewLeads.frame.size.width+btnTemp.frame.size.width-35, 3)];
    
    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        lblBorder.frame=CGRectMake(0,_scrollViewLeads.contentSize.height-2, 100, 2);
    }
    lblBorder.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    
    lblBorder2.backgroundColor=[UIColor lightGrayColor];
}

-(void)addButtonsOrignal
{
    for(int i =0;i<arrOfLeads.count;i++)
    {
        
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*100+20, 0, 50, 50)];
        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
        lbl.textColor = [UIColor whiteColor];
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 25;
        lbl.layer.borderWidth=1.0;
        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:14]];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        if (i==0)
        {
            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
            lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 50, 50);
            lbl.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
            lbl.textColor =  [UIColor lightGrayColor];//[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        else
        {
            
            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 50, 50);
            lbl.textColor = [UIColor darkGrayColor];
            lbl.backgroundColor=[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            
        }
        for(int j=0;j<=2;j++)
        {
            if (i==j)
            {
                
                
                lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
                lbl.textColor = [UIColor whiteColor];
                lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        }
        
        btnTemp.frame=btn.frame;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
        
        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
        btn.titleLabel.numberOfLines=2;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i;
        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
        // [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollViewLeads addSubview:btn];
        
        [_scrollViewLeads addSubview:lbl];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionOnBack:(id)sender
{
     [self endEditing];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isBackFromSelectService"];
    [defs synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}
//============================================================================
#pragma mark- TABLEVIEW DELEGATE METHOD
//tblRecord Tag=0
//tblNonStandardService Tag=1
//============================================================================
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==0)
    {
        NSArray *arrOther;
        if (arrAdditionalParamterDcs.count>0)
        {
            arrOther=[arrAdditionalParamterDcs objectAtIndex:indexPath.row];
            
        }
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]];
        if ([str isEqualToString:@"0"])
        {
            if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]isEqualToString:@"1"])
            {
                if (arrOther.count==0)
                {
                    return 242-65+18+((arrOther.count)*(175))+50+10+12;
                }
                return 242-65+18+((arrOther.count)*(175))+50+12;
            }
            else
            {
                return 242-65+18+12;
                
            }
            //return 260;
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                //return 227-65;
                if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]isEqualToString:@"1"])
                {
                    
                    return 242-65+18+((arrOther.count)*175)+50+12;
                }
                else
                {
                    return 242-65+18+12;
                    
                }
                
            }
            else
            {
                if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]isEqualToString:@"1"])
                {
                    
                    return 242+18+((arrOther.count)*175)+50+12; //242
                }
                else
                {
                    return 242+18+12; //242
                    
                }
            }
            //return 260;
        }
    }
    else if (tableView.tag==2)
    {
        
        NSString *strBundellll=[arrBundleRow objectAtIndex:indexPath.section];
        if (strBundellll.length==0 || [strBundellll isEqualToString:@""] || [strBundellll isEqual:nil])
        {
            
        }
        else
        {
            [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:indexPath.section]];
        }
        
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:indexPath.row]]];
        if ([str isEqualToString:@"0"])
        {
            //return 227-65;
            return tableView.rowHeight-65+10;
            
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                //return 227-65;
                return tableView.rowHeight-65+10;
                
            }
            else
            {
                //return 227;
                return tableView.rowHeight+10;
            }
        }
        
    }
    else if (tableView.tag==101)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==102)
    {
        return 230;
    }
    else
    {
        //return 190;
        return 300;//tableView.rowHeight;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView.tag==0)
    {
        return arrInitialPrice.count;
    }
    else if(tableView.tag==1)
    {
        return [arrNonStanInitialPrice count];//[arrData1 count];//[arrData count];
    }
    else if (tableView.tag==2)
    {
        NSInteger k=0;
        NSString *strBundleId=[arrBundleRow objectAtIndex:section];
        k = [self getBundleCount:strBundleId];
       /* int k=0;
        NSString *strBundleId=[arrBundleRow objectAtIndex:section];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        NSArray *arrTempBundle=[dictMasters valueForKey:@"ServiceBundles"];
        NSMutableArray *arrAllBundles;
        arrAllBundles=[[NSMutableArray alloc]init];
        for (int i=0; i<arrTempBundle.count;i++)
        {
            NSDictionary *dict=[arrTempBundle objectAtIndex:i];
            [arrAllBundles addObject:dict];
            
        }
        for (int i=0; i<arrAllBundles.count; i++)
        {
            NSDictionary *dict=[arrAllBundles objectAtIndex:i];
            if ([strBundleId isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceBundleId"]]])
            {
                NSArray *arrServiceBundle=[dict valueForKey:@"ServiceBundleDetails"];
                k=(int)arrServiceBundle.count;
                break;
            }
        }*/
       
        return k;
    }
    else if (tableView.tag==101)
    {
        return arrDiscountCoupon.count;
    }
    else if (tableView.tag==102)
    {
        return arrAllObjRenewal.count;
    }
    else
    {
        return [arrCategory count];
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==0)
    {
        
        static NSString *identifier=@"cellId";
        // SalesAutomationServiceTableViewCell *
        cell1=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell1==nil)
        {
            cell1=[[SalesAutomationServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell1.lblFrequency.text=@"dsfdsfdsf";
            
        }
        
        
        if (chkStatus==YES)
        {
            cell1.btnImgStandardService.enabled=NO;
            cell1.btnPlusService.enabled=NO;
            cell1.txtUnit.enabled=NO;
            
        }
        cell1.btnPlusService.hidden=YES;
        chkkkk=NO;
        
        //cell1.backgroundColor=[UIColor lightGrayColor];
        cell1.btnImgStandardService.tag=indexPath.row;
        
        NSLog(@"cell1.btnImgStandardService.tag %ld",(long)cell1.btnImgStandardService.tag);
        
        NSLog(@"cell1.imgView.tag %ld",(long)cell1.imgView.tag);
        NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        if ([arrFinalIndexPath containsObject:str])
        {
            
            [cell1.imgView setImage:[UIImage imageNamed:@"check_box_2.png"]];
        }
        else
        {
            
            [cell1.imgView setImage:[UIImage imageNamed:@"check_box_1.png"]];
            
        }
        
        
        [cell1.btnImgStandardService addTarget:self
                                        action:@selector(buttonClickedStandardService:) forControlEvents:UIControlEventTouchDown];
        
        
        NSString *str12=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]];
        
        if ([str12 isEqualToString:@"0"]||[str isEqualToString:@"(null)"]||str12.length==0)
        {
            cell1.const_viewUnit_H.constant=0;
            
            
        }
        else
        {
            cell1.const_viewUnit_H.constant=65;
            
            
        }
        
        if ([[arrInitialPrice objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblInitialPriceValue.text=@"TBD";
        }
        else
        {
            cell1.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrInitialPrice objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrMaintenancePrice objectAtIndex:indexPath.row]isEqualToString:@"TBD"])
        {
            cell1.lblMaintenanceValue.text=@"TBD";
        }
        else
        {
            cell1.lblMaintenanceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrMaintenancePrice objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrFinalInitialPrice objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblFinalInitialPrice.text=@"TBD";
        }
        else
        {
            cell1.lblFinalInitialPrice.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalInitialPrice objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrFinalMaintPrice objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblFinalMaintPrice.text=@"TBD";
        }
        else
        {
            cell1.lblFinalMaintPrice.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalMaintPrice objectAtIndex:indexPath.row] doubleValue]];
        }
        
        
        
        //Nilind 25 May
        //End
        cell1.lblUnit.hidden=YES;
        //cell1.lblUnit.text=[arrUnit objectAtIndex:indexPath.row];
        cell1.txtUnit.text=[arrUnit objectAtIndex:indexPath.row];
        
        //End
        
        
        cell1.lblDiscountValue.text=[NSString stringWithFormat:@"$%.2f",[[arrDiscount objectAtIndex:indexPath.row]doubleValue]];
        
        cell1.lblDiscountPercent.text=[NSString stringWithFormat:@"%.2f",[[arrDiscountPer objectAtIndex:indexPath.row]doubleValue]];
        
        
        
        //cell1.lblFrequency.text=[dictFreqNameFromSysName valueForKey:[arrFrequencyName objectAtIndex:indexPath.row]];
        cell1.lblFrequency.text = [arrFrequencyName objectAtIndex:indexPath.row];

        
        NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysName objectAtIndex:indexPath.row]]];
        if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
        {
            strBillingFreq=@"N/A";
        }
        
        //Billing Frequency Formula Calculation
        
        NSArray *arrAdditionalPara=[arrAdditionalParamterDcs objectAtIndex:indexPath.row];
        
        float totalParaInitial=0.0,totalParaMaint=0.0;
        if(arrAdditionalPara.count>0)
        {
            for (int i=0; i<arrAdditionalPara.count; i++)
            {
                NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
                totalParaInitial=totalParaInitial+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]floatValue];
                totalParaMaint=totalParaMaint+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]floatValue];
                
            }
        }
        totalParaInitial=totalParaInitial+([[arrInitialPrice objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
        
        if (indexPath.row==0) {
            
            globalAmountInitialPrice=totalParaInitial;
            
        }
        
        totalParaInitial=totalParaInitial-[[arrDiscount objectAtIndex:indexPath.row]floatValue];
        
        totalParaMaint=totalParaMaint+([[arrMaintenancePrice objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
        
        if (indexPath.row==0) {
            
            globalAmountMaintenancePrice=totalParaMaint;
            
        }
        
        NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
        strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFreqSysName objectAtIndex:indexPath.row]]];
        strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysName objectAtIndex:indexPath.row]]];
        float totalBillingFreqCharge=0.0;
        
        if ([[arrFreqSysName objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFreqSysName objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
        {
            
            totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            //strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
            
        }
        else
        {
            
            if (chkFreqConfiguration == YES)
            {
                if ([[arrFreqSysName objectAtIndex:indexPath.row]isEqualToString:@"Yearly"])
                {
                    totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                    
                }
                else
                {
                    totalBillingFreqCharge=(totalParaMaint * ([strServiceFreqYearOccurence floatValue]-1))/([strBillingFreqYearOccurence floatValue]);
                    
                }
                
                
            }
            else
            {
                totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
            }
            
            //strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
        }
        //End
        cell1.lblBillingFrequency.text=strBillingFreq;
        //Nilind 02 June
        
        //cell1.lblServiceType.text=[arrServiceName objectAtIndex:indexPath.row];
        
        NSString *strServiceWithPackage,*strPackage;
        strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameId objectAtIndex:indexPath.row]]];
        if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
        {
            strPackage=@"";
        }
        strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",[arrServiceName objectAtIndex:indexPath.row],strPackage];
        
        //For Bundle
        
        /* NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
         if ([strBundle isEqualToString:@"0"])
         {
         }
         else
         {
         strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
         if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
         {
         strPackage=@"NO Package";
         }
         strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,[arrServiceName objectAtIndex:indexPath.row]];
         
         }*/
        
        //End
        
        
        cell1.lblServiceType.text=strServiceWithPackage;
        [cell1.lblServiceType sizeToFit];
        //End
        
        cell1.lblServiceSysName.text=[arrSysName objectAtIndex:indexPath.row];
        NSString *strSysNameService=[arrSysName objectAtIndex:indexPath.row];
        
        [self chkForPlusServiceButton:strSysNameService];
        if (chkkkk==YES)
        {
            cell1.btnPlusService.hidden=NO;
            
        }
        else
        {
            cell1.btnPlusService.hidden=YES;
        }
        
#pragma mark- change sysname array
        //Nilind 6 Oct
        
        cell1.btnPlusService.tag=indexPath.row;
        [cell1.btnPlusService addTarget:self action:@selector(plusService:) forControlEvents:UIControlEventTouchUpInside];
        cell1.txtStanDiscount.text=[arrDiscount objectAtIndex:indexPath.row];
        cell1.txtDiscountPercent.text=[arrDiscountPer objectAtIndex:indexPath.row];
        
        if (isCheckBoxClickFirstTime==YES)
        {
            if ([strCouponStatus isEqualToString:@"true"])
            {
                if (isChkBoxEnable==YES)
                {
                    cell1.txtStanDiscount.hidden=NO;
                    cell1.lblDiscountValue.hidden=YES;
                    cell1.txtDiscountPercent.hidden=NO;
                    cell1.lblDiscountPercent.hidden=YES;
                    cell1.txtStanDiscount.text=[arrDiscount objectAtIndex:indexPath.row];
                }
                else
                {
                    cell1.txtStanDiscount.hidden=YES;
                    cell1.lblDiscountValue.hidden=NO;
                    
                    cell1.txtDiscountPercent.hidden=YES;
                    cell1.lblDiscountPercent.hidden=NO;
                    if([cell1.lblInitialPriceValue.text isEqualToString:@"TBD"])
                    {
                        cell1.lblDiscountValue.text=@"TBD";
                        cell1.txtStanDiscount.text=@"TBD";
                        cell1.lblDiscountPercent.text=@"TBD";
                        cell1.txtDiscountPercent.text=@"TBD";
                        
                    }
                    else
                    {
                        cell1.lblDiscountValue.text=@"0";
                        cell1.txtStanDiscount.text=@"0";
                        cell1.lblDiscountPercent.text=@"0";
                        cell1.txtDiscountPercent.text=@"0";
                        
                    }
                }
            }
            else
            {
                cell1.txtStanDiscount.hidden=YES;
                cell1.lblDiscountValue.hidden=NO;
                cell1.txtDiscountPercent.hidden=YES;
                cell1.lblDiscountPercent.hidden=NO;
                
                
            }
            cell1.txtStanDiscount.tag=indexPath.row;
            cell1.txtDiscountPercent.tag=indexPath.row;
            
        }
        else
        {
            cell1.txtStanDiscount.hidden=YES;
            cell1.lblDiscountValue.hidden=NO;
            
            cell1.txtDiscountPercent.hidden=YES;
            cell1.lblDiscountPercent.hidden=NO;
        }
        cell1.txtUnit.tag=indexPath.row;
        
        //Temp Code
        /*cell1.txtStanDiscount.hidden=NO;
         cell1.lblDiscountValue.hidden=YES;
         
         cell1.txtDiscountPercent.hidden=NO;
         cell1.lblDiscountPercent.hidden=YES;*/
        
        
        if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPath.row]]isEqualToString:@"1"])
        {
            //NSArray *arrOther=[self calculationForParamterBasedService:[arrSysName objectAtIndex:indexPath.row]];
            
            NSArray *arrOther=[arrAdditionalParamterDcs objectAtIndex:indexPath.row];
            if([arrOther isKindOfClass:[NSArray class]])
            {
                int heigthDynamic;
                heigthDynamic=0;
                float sumFinalInitialPrice = 0.0;
                float sumFinalMaintPrice = 0.0;
                int maxY=0;
                // UILabel* lbl;//=[[UILabel alloc]init];
                // UITextField* txt;//=[[UITextField alloc]init];
                
                //[cell1.viewForParameterPrice removeFromSuperview];
                
                
                
                //                dispatch_async (dispatch_get_main_queue(), ^{
                
                for(UIView *view in cell1.viewForParameterPrice.subviews)
                {
                    [view removeFromSuperview];
                }
                
                for (int i=0; i<arrOther.count; i++)
                {
                    UILabel* lbl;
                    UITextField* txt;
                    
                    NSDictionary *dictOther=[arrOther objectAtIndex:i];
                    NSArray *arrKey=[dictOther allKeys];
                    
                    if ([arrKey containsObject:@"AdditionalParameterName"])
                    {
                        
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25, maxY,cell1.viewForParameterPrice.frame.size.width, 20)];;
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(25, maxY,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        
                        lbl.text=[NSString stringWithFormat:@"%@ : %@",@"Field Name",[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"AdditionalParameterName"]]];
                        
                        lbl.font=[UIFont boldSystemFontOfSize:16];
                        
                        /*txt.frame=CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20);
                         
                         txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"Name"]];
                         txt.font=[UIFont systemFontOfSize:16];
                         txt.layer.borderColor=[[UIColor blackColor] CGColor];
                         txt.delegate=self;
                         txt.layer.borderWidth=1.0;
                         txt.textAlignment=NSTextAlignmentCenter;
                         
                         txt.tag=(10*i)+10+indexPath.row;*/
                        
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        // [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                    }
                    if ([arrKey containsObject:@"UnitName"] && [NSString stringWithFormat:@"%@",[dictOther valueForKey:@"UnitName"]].length>0)
                    {
                        
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        
                        lbl.text=[NSString stringWithFormat:@"%@",@"Unit Name"];
                        
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"UnitName"]];
                        txt.font=[UIFont systemFontOfSize:16];
                        //txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        txt.enabled=NO;
                        //txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=1000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        
                    }
                    if ([arrKey containsObject:@"UnitName"] && [NSString stringWithFormat:@"%@",[dictOther valueForKey:@"UnitName"]].length>0)
                    {
                        
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        lbl.text=[NSString stringWithFormat:@"%@",@"Enter Unit"];
                        
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"Unit"]];
                        txt.font=[UIFont systemFontOfSize:16];
                        txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=2000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        if (chkStatus==YES)
                        {
                            txt.enabled=NO;
                        }
                        else
                        {
                            txt.enabled=YES;
                            
                        }
                        txt.keyboardType=UIKeyboardTypeDecimalPad;
                        
                    }
                    if ([arrKey containsObject:@"InitialUnitPrice"])
                    {
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        
                        lbl.text=[NSString stringWithFormat:@"%@",@"Initial Unit Price"];
                        
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        
                        //txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"InitialUnitPrice"]];
                        if([[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"InitialUnitPrice"]] floatValue]<[[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]]floatValue])
                        {
                            txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]];
                        }
                        else
                        {
                            txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"InitialUnitPrice"]];
                        }
                        
                        txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"InitialUnitPrice"]];
                        
                        txt.text = [self convertDecimalToString:txt.text];
                        txt.font=[UIFont systemFontOfSize:16];
                        // txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        //txt.layer.borderWidth=1.0;
                        txt.enabled=NO;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=3000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        
                    }
                    if ([arrKey containsObject:@"MaintUnitPrice"])
                    {
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        lbl.text=[NSString stringWithFormat:@"%@",@"Maint. Unit Price"];
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MaintUnitPrice"]];
                        txt.text = [self convertDecimalToString:txt.text];

                        txt.font=[UIFont systemFontOfSize:16];
                        txt.delegate=self;
                        txt.enabled=NO;
                        // txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=4000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        
                    }
                    if ([arrKey containsObject:@"FinalInitialUnitPrice"])
                    {
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        lbl.text=[NSString stringWithFormat:@"%@",@"Total Initial Price"];
                        //                        lbl.backgroundColor = [UIColor yellowColor];
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        // txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"FinalInitialUnitPrice"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"FinalInitialUnitPrice"]] floatValue]<[[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]]floatValue])
                        {
                            txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]];
                        }
                        else
                        {
                            txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"FinalInitialUnitPrice"]];
                        }
                        
                        if ([[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"Unit"]]floatValue] == 0)
                        {
                             txt.text=[NSString stringWithFormat:@"%@",@"0.00"];
                        }
                        
                        txt.text = [self convertDecimalToString:txt.text];

                        txt.font=[UIFont systemFontOfSize:16];
                        txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        //txt.enabled=NO;
                        txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=5000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        
                        sumFinalInitialPrice=sumFinalInitialPrice + [txt.text floatValue];
                        
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        if (chkStatus==YES)
                        {
                            txt.enabled=NO;
                        }
                        else
                        {
                            txt.enabled=YES;
                            
                        }
                        txt.keyboardType=UIKeyboardTypeDecimalPad;
                        
                    }
                    if ([arrKey containsObject:@"FinalMaintUnitPrice"])
                    {
                        lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20)];
                        txt=[[UITextField alloc]initWithFrame:CGRectMake(cell1.txtUnit.frame.origin.x, lbl.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20)];
                        lbl.text=[NSString stringWithFormat:@"%@",@"Total Maint. Price"];
                        lbl.font=[UIFont systemFontOfSize:16];;
                        
                        txt.text=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"FinalMaintUnitPrice"]];
                        txt.text = [self convertDecimalToString:txt.text];

                        txt.font=[UIFont systemFontOfSize:16];
                        txt.layer.borderColor=[[UIColor blackColor] CGColor];
                        txt.delegate=self;
                        //txt.enabled=NO;
                        txt.layer.borderWidth=1.0;
                        txt.textAlignment=NSTextAlignmentCenter;
                        txt.tag=6000+i;
                        heigthDynamic=heigthDynamic+lbl.frame.size.height;
                        sumFinalMaintPrice=sumFinalMaintPrice + [txt.text floatValue];
                        [cell1.viewForParameterPrice addSubview:lbl];
                        [cell1.viewForParameterPrice addSubview:txt];
                        maxY=CGRectGetMaxY(lbl.frame);
                        if (chkStatus==YES)
                        {
                            txt.enabled=NO;
                        }
                        else
                        {
                            txt.enabled=YES;
                            
                        }
                        txt.keyboardType=UIKeyboardTypeDecimalPad;
                        
                    }
                    
                }
                
                
                
                UILabel* lblFinalInitialPrice=[[UILabel alloc]init];
                UILabel* lblValueFinalInitialPrice=[[UILabel alloc]init];
                
                lblFinalInitialPrice=[[UILabel alloc]init];
                lblValueFinalInitialPrice=[[UILabel alloc]init];
                lblFinalInitialPrice.frame=CGRectMake(10,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20);
                lblFinalInitialPrice.text=[NSString stringWithFormat:@"%@",@"Final Initial Price"];
                
                lblFinalInitialPrice.font=[UIFont boldSystemFontOfSize:16];
                
                lblValueFinalInitialPrice.frame=CGRectMake(cell1.txtUnit.frame.origin.x, lblFinalInitialPrice.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20);
                
                
                sumFinalInitialPrice=sumFinalInitialPrice+[[arrInitialPrice objectAtIndex:indexPath.row]floatValue]-[[arrDiscount objectAtIndex:indexPath.row]floatValue];
                lblValueFinalInitialPrice.text=[NSString stringWithFormat:@"$%.2f",sumFinalInitialPrice];
                
                lblValueFinalInitialPrice.font=[UIFont boldSystemFontOfSize:16];
                lblValueFinalInitialPrice.textAlignment=NSTextAlignmentCenter;
                [cell1.viewForParameterPrice addSubview:lblFinalInitialPrice];
                [cell1.viewForParameterPrice addSubview:lblValueFinalInitialPrice];
                maxY=CGRectGetMaxY(lblFinalInitialPrice.frame);
                cell1.const_ViewParameter_H.constant=maxY+10;
                
                
                UILabel* lblFinalMaintPrice=[[UILabel alloc]init];
                UILabel* lblValueFinalMaintPrice=[[UILabel alloc]init];
                
                lblFinalMaintPrice=[[UILabel alloc]init];
                lblValueFinalMaintPrice=[[UILabel alloc]init];
                
                lblFinalMaintPrice.frame=CGRectMake(10,maxY+5,cell1.viewForParameterPrice.frame.size.width/2, 20);
                lblFinalMaintPrice.text=@"Final Maint. Price";
                
                lblFinalMaintPrice.font=[UIFont boldSystemFontOfSize:16];
                
                lblValueFinalMaintPrice.frame=CGRectMake(cell1.txtUnit.frame.origin.x, lblFinalMaintPrice.frame.origin.y,CGRectGetMaxX(cell1.txtUnit.frame)-cell1.txtUnit.frame.origin.x, 20);
                
                
                
                sumFinalMaintPrice=sumFinalMaintPrice+[[arrMaintenancePrice objectAtIndex:indexPath.row]floatValue];
                
                lblValueFinalMaintPrice.text=[NSString stringWithFormat:@"$%.2f",sumFinalMaintPrice];
                
                lblValueFinalMaintPrice.font=[UIFont boldSystemFontOfSize:16];
                lblValueFinalMaintPrice.textAlignment=NSTextAlignmentCenter;
                [cell1.viewForParameterPrice addSubview:lblFinalMaintPrice];
                [cell1.viewForParameterPrice addSubview:lblValueFinalMaintPrice];
                maxY=CGRectGetMaxY(lblFinalMaintPrice.frame);
                
                cell1.const_ViewParameter_H.constant=maxY+10;
                //});
                
            }
            
            
        }
        else
        {
            cell1.const_ViewParameter_H.constant=0;
            
        }
        
        return cell1;
    }
    else if(tableView.tag==1)
    {
        static NSString *identifier=@"cellId2";
        // SalesAutomationTableViewCellSecond *
        cell2=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell2==nil)
        {
            cell2=[[SalesAutomationTableViewCellSecond alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (chkStatus==YES)
        {
            cell2.btnImageNonStandardService.enabled=NO;
        }
        cell2.btnImageNonStandardService.tag=indexPath.row;
        
        NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        if ([arrFinalIndexPathNonStan containsObject:str])
        {
            //[cell2.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            [cell2.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"]];
        }
        else
        {
            //[cell2.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            [cell2.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"]];
        }
        
        // cell2.backgroundColor=[UIColor lightGrayColor];
        [cell2.btnImageNonStandardService addTarget:self
                                             action:@selector(buttonClickedNonStandardService:) forControlEvents:UIControlEventTouchDown];
        
        cell2.lblServiceType.text=[arrNonStanServiceName objectAtIndex:indexPath.row];
        cell2.lblServiceDescription.text=[arrNonStanServiceDesc objectAtIndex:indexPath.row];
        
        cell2.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrNonStanInitialPrice objectAtIndex:indexPath.row] doubleValue]];
        
        cell2.lblDiscountValue.text=[NSString stringWithFormat:@"$%.2f",[[arrNonStanDiscount objectAtIndex:indexPath.row]doubleValue]];
        
        cell2.lblDiscountPercent.text=[NSString stringWithFormat:@"%.2f",[[arrDiscountPerNonStan objectAtIndex:indexPath.row]doubleValue]];
        
        
        if(arrDepartmentName.count==0)
        {
            cell2.lblDepartmentValue.text=@"N/A";
        }
        else
        {
            if ([NSString stringWithFormat:@"%@",[arrDepartmentName objectAtIndex:indexPath.row]].length==0)
            {
                cell2.lblDepartmentValue.text=@"N/A";
            }
            else
            {
                cell2.lblDepartmentValue.text=[NSString stringWithFormat:@"%@",[arrDepartmentName objectAtIndex:indexPath.row]];
                //cell2.lblDepartmentValue.text=[NSString stringWithFormat:@"%@",[dictForDepartment valueForKey:[arrDepartmentName objectAtIndex:indexPath.row]]];
                //[dictForDepartment valueForKey:[matches valueForKey:@"departmentSysname"]]
            }
        }
        
        //Nilind 08 June
        
        cell2.txtDiscountNonStan.text=[arrNonStanDiscount objectAtIndex:indexPath.row];
        cell2.txtDiscountPercent.text=[arrDiscountPerNonStan objectAtIndex:indexPath.row];
        
        if (isCheckBoxClickFirstTimeNonStan==YES)
        {
            if ([strCouponStatusNonStan isEqualToString:@"true"])
            {
                if (isChkBoxEnableNonStan==YES)
                {
                    cell2.txtDiscountNonStan.hidden=NO;
                    cell2.lblDiscountValue.hidden=YES;
                    cell2.txtDiscountNonStan.text=[arrNonStanDiscount objectAtIndex:indexPath.row];
                    
                    cell2.txtDiscountPercent.hidden=NO;
                    cell2.lblDiscountPercent.hidden=YES;
                    cell2.txtDiscountPercent.text=[arrDiscountPerNonStan objectAtIndex:indexPath.row];
                }
                else
                {
                    cell2.txtDiscountNonStan.hidden=YES;
                    cell2.lblDiscountValue.hidden=NO;
                    
                    cell2.txtDiscountPercent.hidden=YES;
                    cell2.lblDiscountPercent.hidden=NO;
                    
                    if([cell2.lblInitialPriceValue.text isEqualToString:@"TBD"])
                    {
                        cell2.lblDiscountValue.text=@"TBD";
                        cell2.txtDiscountNonStan.text=@"TBD";
                        
                        cell2.lblDiscountPercent.text=@"TBD";
                        cell2.txtDiscountPercent.text=@"TBD";
                    }
                    else
                    {
                        cell2.lblDiscountValue.text=@"0";
                        cell2.txtDiscountNonStan.text=@"0";
                        
                        cell2.lblDiscountPercent.text=@"0";
                        cell2.txtDiscountPercent.text=@"0";
                        
                    }
                }
            }
            else
            {
                cell2.txtDiscountNonStan.hidden=YES;
                cell2.lblDiscountValue.hidden=NO;
                
                cell2.txtDiscountPercent.hidden=YES;
                cell2.lblDiscountPercent.hidden=NO;
                
            }
            cell2.txtDiscountNonStan.tag=indexPath.row;
            cell2.txtDiscountPercent.tag=indexPath.row;
            
        }
        
        else
        {
            cell2.txtDiscountNonStan.hidden=YES;
            cell2.lblDiscountValue.hidden=NO;
            
            cell2.txtDiscountPercent.hidden=YES;
            cell2.lblDiscountPercent.hidden=NO;
            
        }
        cell2.txtDiscountNonStan.tag=indexPath.row;
        cell2.txtDiscountPercent.tag=indexPath.row;
        //End
        
        
        
        
        //.....................................................................
        
        double diff;
        diff=[[arrNonStanInitialPrice objectAtIndex:indexPath.row]doubleValue]-[[arrNonStanDiscount objectAtIndex:indexPath.row]doubleValue];
        cell2.lblMaintenanceValue.text=[NSString stringWithFormat:@"$%.2f",diff];
        
        if ([[arrNonStanIsSold objectAtIndex:indexPath.row] isEqualToString:@"true"])
        {
            //[cell2.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            [cell2.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"]];
        }
        else if ([[arrNonStanIsSold objectAtIndex:indexPath.row] isEqualToString:@"false"])
        {
            //[cell2.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            [cell2.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"]];
        }
        
        
        //Billing Freq Calculation
        NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
        if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
        {
            strBillingFreq=@"N/A";
        }
        NSString *strBillingFreqYearOccurence,*strServiceFreqYearOccurence;
        strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrServiceFreqSysNameNonStan objectAtIndex:indexPath.row]]];
        
        strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
        if([strBillingFreqYearOccurence isEqualToString:@"(null)"])
        {
            strBillingFreqYearOccurence=@"0";
        }
        float totalBillingFreqCharge=0.0;
        float totalInitialNonStan=0.0,totalMaintNonStan=0.0;
        totalInitialNonStan = [[arrNonStanInitialPrice objectAtIndex:indexPath.row]floatValue]-[[arrNonStanDiscount objectAtIndex:indexPath.row]floatValue];
        totalMaintNonStan =[[arrMaintPriceNonStan objectAtIndex:indexPath.row]floatValue];
        
        if ([[arrServiceFreqSysNameNonStan objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrServiceFreqSysNameNonStan objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
        {
            @try
            {
                totalBillingFreqCharge=(totalInitialNonStan*[strServiceFreqYearOccurence floatValue]) /[strBillingFreqYearOccurence floatValue];
                
            }
            @catch (NSException *exception)
            {
                totalBillingFreqCharge=0.0;
            }
            @finally
            {
                
            }
        }
        else
        {
            @try
            {
                if (chkFreqConfiguration==YES)
                {
                    
                    if ([[arrServiceFreqSysNameNonStan objectAtIndex:indexPath.row]isEqualToString:@"Yearly"])
                    {
                        totalBillingFreqCharge=(totalMaintNonStan*[strServiceFreqYearOccurence floatValue]) /[strBillingFreqYearOccurence floatValue];
                    }
                    else
                    {
                        totalBillingFreqCharge=(totalMaintNonStan*([strServiceFreqYearOccurence floatValue]-1)) /[strBillingFreqYearOccurence floatValue];
                    }
                }
                else
                {
                    totalBillingFreqCharge=(totalMaintNonStan*[strServiceFreqYearOccurence floatValue]) /[strBillingFreqYearOccurence floatValue];
                }
                
            }
            @catch (NSException *exception)
            {
                totalBillingFreqCharge=0.0;
            }
            @finally
            {
                
            }
        }
        
        /* @try
         {
         
         totalBillingFreqCharge=([[arrNonStanInitialPrice objectAtIndex:indexPath.row]floatValue]-[[arrNonStanDiscount objectAtIndex:indexPath.row]floatValue]) /[strBillingFreqYearOccurence floatValue];
         
         } @catch (NSException *exception)
         {
         totalBillingFreqCharge=0.0;
         }
         @finally
         {
         
         }*/
        strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
        
        cell2.lblBillingFreqValue.text=strBillingFreq;
        //End
        
        //Nilind 17 July
        NSString *strServiceFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrServiceFreqSysNameNonStan objectAtIndex:indexPath.row]]];
        if ([strServiceFreq isEqualToString:@""]||[strServiceFreq isEqual:nil]||[strServiceFreq isEqualToString:@"(null)"]||strServiceFreq.length==0  )
        {
            strServiceFreq=@"N/A";
        }
        cell2.lblMaintPriceValue.text = [NSString stringWithFormat:@"$%.2f",[[arrMaintPriceNonStan objectAtIndex:indexPath.row] doubleValue]];
        cell2.lblServiceFreqValue.text = strServiceFreq;
        //ENd
        //Non Standard Taxable
        if ([[NSString stringWithFormat:@"%@",[arrTaxStatusNonStan objectAtIndex:indexPath.row]] isEqualToString:@"true"])
        {
            cell2.imgViewTaxableNonStan.image = [UIImage imageNamed:@"check_box_2.png"];
        }
        else
        {
            cell2.imgViewTaxableNonStan.image = [UIImage imageNamed:@"check_box_1.png"];

        }
        cell2.btnTaxableNonStan.tag = indexPath.row;
        [cell2.btnTaxableNonStan addTarget:self
                                             action:@selector(buttonClickedTaxableNonStandard:) forControlEvents:UIControlEventTouchDown];
        
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            [cell2.btnTaxableNonStan setEnabled:NO];
        }
        
        
        return cell2;
    }
    else if (tableView.tag==2) //bUndle
    {
        static NSString *identifier=@"cellId";
        // SalesAutomationServiceTableViewCell *
        cell1=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell1==nil)
        {
            cell1=[[SalesAutomationServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell1.lblFrequency.text=@"dsfdsfdsf";
            
        }
        NSString *strBundellll=[arrBundleRow objectAtIndex:indexPath.section];
        if (strBundellll.length==0 || [strBundellll isEqualToString:@""] || [strBundellll isEqual:nil])
        {
            
        }
        else
        {
            [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:indexPath.section]];
        }
        
        if (chkStatus==YES)
        {
            cell1.btnImgStandardService.enabled=NO;
            cell1.btnPlusService.enabled=NO;
            cell1.txtUnit.enabled=NO;
            
        }
        cell1.btnPlusService.hidden=YES;
        chkkkk=NO;
        
        //cell1.backgroundColor=[UIColor lightGrayColor];
        cell1.btnImgStandardService.tag=indexPath.row;
        
        NSLog(@"cell1.btnImgStandardService.tag %ld",(long)cell1.btnImgStandardService.tag);
        
        NSLog(@"cell1.imgView.tag %ld",(long)cell1.imgView.tag);
        NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        /* if ([arrFinalIndexPath containsObject:str])
         {
         
         [cell1.imgView setImage:[UIImage imageNamed:@"check_box_2.png"]];
         }
         else
         {
         
         [cell1.imgView setImage:[UIImage imageNamed:@"check_box_1.png"]];
         
         }
         */
        
        //[cell1.btnImgStandardService addTarget:self
        //action:@selector(buttonClickedStandardService:) forControlEvents:UIControlEventTouchDown];
        
        
        NSString *str12=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:indexPath.row]]];
        if ([str12 isEqualToString:@"0"]||[str isEqualToString:@"(null)"]||str12.length==0)
        {
            cell1.const_viewUnit_H.constant=0;
            
            
        }
        else
        {
            cell1.const_viewUnit_H.constant=65;
            
            
        }
        
        if ([[arrInitialPriceBundle objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblInitialPriceValue.text=@"TBD";
        }
        else
        {
            cell1.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrInitialPriceBundle objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrMaintenancePriceBundle objectAtIndex:indexPath.row]isEqualToString:@"TBD"])
        {
            cell1.lblMaintenanceValue.text=@"TBD";
        }
        else
        {
            cell1.lblMaintenanceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrMaintenancePriceBundle objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrFinalInitialPriceBundle objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblFinalInitialPrice.text=@"TBD";
        }
        else
        {
            cell1.lblFinalInitialPrice.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalInitialPriceBundle objectAtIndex:indexPath.row] doubleValue]];
        }
        if ([[arrFinalMaintPriceBundle objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblFinalMaintPrice.text=@"TBD";
        }
        else
        {
            cell1.lblFinalMaintPrice.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalMaintPriceBundle objectAtIndex:indexPath.row] doubleValue]];
        }
        
        
        
        //Nilind 25 May
        //End
        cell1.lblUnit.hidden=YES;
        //cell1.lblUnit.text=[arrUnit objectAtIndex:indexPath.row];
        cell1.txtUnit.text=[arrUnitBundle objectAtIndex:indexPath.row];
        
        //End
        
        
        cell1.lblDiscountValue.text=[NSString stringWithFormat:@"$%.2f",[[arrDiscountBundle objectAtIndex:indexPath.row]doubleValue]];
        
        cell1.lblDiscountPercent.text=[NSString stringWithFormat:@"%.2f",[[arrDiscountPerBundle objectAtIndex:indexPath.row]doubleValue]];
        
        
        
        //cell1.lblFrequency.text=[dictFreqNameFromSysName valueForKey:[arrFrequencyNameBundle objectAtIndex:indexPath.row]];
        cell1.lblFrequency.text = [arrFrequencyNameBundle objectAtIndex:indexPath.row];

        
        NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
        if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
        {
            strBillingFreq=@"N/A";
        }
        //Billing Freq Calculation
        
        float totalParaInitial=0.0,totalParaMaint=0.0;
        
        totalParaInitial=totalParaInitial+([[arrInitialPriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
        
        if (indexPath.row==0) {
            
            globalAmountInitialPrice=totalParaInitial;
            
        }
        
        totalParaInitial=totalParaInitial-[[arrDiscountBundle objectAtIndex:indexPath.row]floatValue];
        
        totalParaMaint=totalParaMaint+([[arrMaintenancePriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
        
        
        if (indexPath.row==0) {
            
            globalAmountMaintenancePrice=totalParaMaint;
            
        }
        
        NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
        strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFrequencySysNameBundle objectAtIndex:indexPath.row]]];
        strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
        float totalBillingFreqCharge=0.0;
        
        if ([[arrFrequencySysNameBundle objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFrequencySysNameBundle objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
        {
            
            totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            //  strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
        }
        else
        {
            
            if (chkFreqConfiguration == YES)
            {
                if ([[arrFrequencySysNameBundle objectAtIndex:indexPath.row]isEqualToString:@"Yearly"])
                {
                    totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                    
                }
                else
                {
                    totalBillingFreqCharge=(totalParaMaint * ([strServiceFreqYearOccurence floatValue]-1))/([strBillingFreqYearOccurence floatValue]);
                    
                }
                
                
            }
            else
            {
                totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                
            }
            // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
        }
        cell1.lblBillingFrequency.text=strBillingFreq;
        
        //End
        
        //Nilind 02 June
        
        //cell1.lblServiceType.text=[arrServiceName objectAtIndex:indexPath.row];
        
        NSString *strServiceWithPackage,*strPackage;
        strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameIdBundle objectAtIndex:indexPath.row]]];
        if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
        {
            strPackage=@"";
        }
        strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",[arrServiceNameBundle objectAtIndex:indexPath.row],strPackage];
        
        //For Bundle
        
        NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
        if ([strBundle isEqualToString:@"0"])
        {
        }
        else
        {
            strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
            if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
            {
                strPackage=@"";
            }
            strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,[arrServiceNameBundle objectAtIndex:indexPath.row]];
            
        }
        
        //End
        
        
        cell1.lblServiceType.text=strServiceWithPackage;
        [cell1.lblServiceType sizeToFit];
        //End
        
        cell1.lblServiceSysName.text=[arrSysNameBundle objectAtIndex:indexPath.row];
        NSString *strSysNameService=[arrSysNameBundle objectAtIndex:indexPath.row];
        
        [self chkForPlusServiceButton:strSysNameService];
        if (chkkkk==YES)
        {
            //cell1.btnPlusService.hidden=NO;
            cell1.btnPlusService.hidden=YES;
            
        }
        else
        {
            cell1.btnPlusService.hidden=YES;
        }
        
#pragma mark- change sysname array
        //Nilind 6 Oct
        
        cell1.btnPlusService.tag=indexPath.row;
        [cell1.btnPlusService addTarget:self action:@selector(plusService:) forControlEvents:UIControlEventTouchUpInside];
        cell1.txtStanDiscount.text=[arrDiscountBundle objectAtIndex:indexPath.row];
        cell1.txtDiscountPercent.text=[arrDiscountPerBundle objectAtIndex:indexPath.row];
        
        if (isCheckBoxClickFirstTime==YES)
        {
            if ([strCouponStatus isEqualToString:@"true"])
            {
                if (isChkBoxEnable==YES)
                {
                    cell1.txtStanDiscount.hidden=NO;
                    cell1.lblDiscountValue.hidden=YES;
                    cell1.txtDiscountPercent.hidden=NO;
                    cell1.lblDiscountPercent.hidden=YES;
                    cell1.txtStanDiscount.text=[arrDiscountBundle objectAtIndex:indexPath.row];
                }
                else
                {
                    cell1.txtStanDiscount.hidden=YES;
                    cell1.lblDiscountValue.hidden=NO;
                    
                    cell1.txtDiscountPercent.hidden=YES;
                    cell1.lblDiscountPercent.hidden=NO;
                    if([cell1.lblInitialPriceValue.text isEqualToString:@"TBD"])
                    {
                        cell1.lblDiscountValue.text=@"TBD";
                        cell1.txtStanDiscount.text=@"TBD";
                        cell1.lblDiscountPercent.text=@"TBD";
                        cell1.txtDiscountPercent.text=@"TBD";
                        
                    }
                    else
                    {
                        cell1.lblDiscountValue.text=@"0";
                        cell1.txtStanDiscount.text=@"0";
                        cell1.lblDiscountPercent.text=@"0";
                        cell1.txtDiscountPercent.text=@"0";
                        
                    }
                }
            }
            else
            {
                cell1.txtStanDiscount.hidden=YES;
                cell1.lblDiscountValue.hidden=NO;
                cell1.txtDiscountPercent.hidden=YES;
                cell1.lblDiscountPercent.hidden=NO;
                
                
            }
            cell1.txtStanDiscount.tag=indexPath.row;
            cell1.txtDiscountPercent.tag=indexPath.row;
            
        }
        else
        {
            cell1.txtStanDiscount.hidden=YES;
            cell1.lblDiscountValue.hidden=NO;
            
            cell1.txtDiscountPercent.hidden=YES;
            cell1.lblDiscountPercent.hidden=NO;
        }
        cell1.txtUnit.tag=indexPath.row;
        return cell1;
    }
    else if (tableView.tag==101)
    {
        static NSString *identifier=@"SelectServiceCouponDiscountTableViewCell";
        SelectServiceCouponDiscountTableViewCell *cellCoupon=[_tblCouponDiscount dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[SelectServiceCouponDiscountTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSManagedObject *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
        
        cellCoupon.lblCouponName.text=[NSString stringWithFormat:@"%@",[dictDiscountNameFromSysName valueForKey:[dict valueForKey:@"discountSysName"]]];
        
        //  cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountAmount"]];
        
        
        cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"$%.2f",[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]]floatValue]];
        
        cellCoupon.lbCouponDescription.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]];
        return cellCoupon;
        
    }
    else if (tableView.tag==102)
    {
        static NSString *identifier=@"RenewalServiceTableViewCell";
        RenewalServiceTableViewCell *cellRenewalService=[_tblRenewalService dequeueReusableCellWithIdentifier:identifier];
        if (cellRenewalService==nil)
        {
            cellRenewalService=[[RenewalServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
       
        if (chkStatus == YES)
        {
            [cellRenewalService.txtRenewalPrice setEnabled:NO];
            [cellRenewalService.txtViewRenewalServiceDesc setEditable:NO];
        }
        
        NSManagedObject *matchesRenewal = [arrAllObjRenewal objectAtIndex:indexPath.row];
        
        
        cellRenewalService.txtRenewalPrice.tag = indexPath.row;
        
        cellRenewalService.txtViewRenewalServiceDesc.tag = indexPath.row;
        
        cellRenewalService.lblRenewalService.text = [NSString stringWithFormat:@"%@",[dictServiceNameForId valueForKey:[matchesRenewal valueForKey:@"serviceId"]]];
        
        cellRenewalService.lblRenewalFrequency.text = [NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[matchesRenewal valueForKey:@"renewalFrequencySysName"]]];
        
        cellRenewalService.txtRenewalPrice.text = [NSString stringWithFormat:@"%@",[matchesRenewal valueForKey:@"renewalAmount"]];
        
        
        cellRenewalService.txtViewRenewalServiceDesc.text =[NSString stringWithFormat:@"%@",[matchesRenewal valueForKey:@"renewalDescription"] ];
        
        
        cellRenewalService.txtViewRenewalServiceDesc.backgroundColor = [UIColor whiteColor];
        
        
        cellRenewalService.txtViewRenewalServiceDesc.layer.borderWidth=1.0;
        cellRenewalService.txtViewRenewalServiceDesc.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        
        cellRenewalService.txtViewRenewalServiceDesc.layer.cornerRadius=5.0;
        
        cellRenewalService.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cellRenewalService;
        
    }
    else
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.textLabel.text=[arrCategory objectAtIndex:indexPath.row];
        if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
        {
            cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        return cell;
    }
}
-(void)buttonClickedStandardService:(UIButton*)sender
{
    isEditedInSalesAuto=YES;
    [self fetchFromCoreDataStandard];
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    
    // NSIndexPath *indexPath1 = [_tblRecord indexPathForCell:(UITableViewCell *)sender.superview.superview];
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    SalesAutomationServiceTableViewCell *tappedCell = (SalesAutomationServiceTableViewCell *)[_tblRecord cellForRowAtIndexPath:indexpath];
    
    
    if ([tappedCell.imgView.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        [tappedCell.imgView setImage:[UIImage imageNamed:@"check_box_1.png"]];
        chkBtnCheckBoxStan=NO;
        
        //Old
        /* if (arrSysNameConditionalStandard.count>=indexpath.row) {
         
         [arrSysNameConditionalStandard removeObject:arrSysName[indexpath.row]];
         
         NSString *strPath=[NSString stringWithFormat:@"%ld",(long)indexpath.row];
         
         [arrFinalIndexPath removeObject:strPath];
         
         }*/
        if ([arrFinalIndexPath containsObject:[NSString stringWithFormat:@"%ld",(long)indexpath.row]]) {
            
            [arrSysNameConditionalStandard removeObject:arrSysName[indexpath.row]];
            
            NSString *strPath=[NSString stringWithFormat:@"%ld",(long)indexpath.row];
            
            [arrFinalIndexPath removeObject:strPath];
            
        }
        
        /*if (arrSysNameConditionalStandard.count>0) {
         
         [arrSysNameConditionalStandard removeObject:arrSysName[indexpath.row]];
         
         NSString *strPath=[NSString stringWithFormat:@"%ld",(long)indexpath.row];
         
         [arrFinalIndexPath removeObject:strPath];
         
         }*/
        
    }
    else
    {
        BOOL isMatched;
        int indexxMatched=9999999;
        isMatched=NO;
        //NSString *strPath=[NSString stringWithFormat:@"%ld",(long)indexpath.row];
        
        //[arrFinalIndexPath removeObject:strPath];
        for (int k1=0; k1<arrSysNameConditionalStandard.count; k1++) {
            
            NSString *strConditional=arrSysNameConditionalStandard[k1];
            NSString *strssysname=arrSysName[indexpath.row];
            
            if ([strConditional isEqualToString:strssysname]) {
                
                indexxMatched=k1;
                isMatched=YES;
                
            }
        }
        
        if (isMatched)
        {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:@"Service already sold do yo want to continue"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      
                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                     NSString *strIndex=[NSString stringWithFormat:@"%@",arrFinalIndexPath[indexxMatched]];
                                     
                                     NSManagedObject *record = [arrAllObj objectAtIndex:[strIndex integerValue]];
                                     
                                     [self updateStadardNew:record];
                                     
                                     [arrFinalIndexPath removeObjectAtIndex:indexxMatched];
                                     [arrSysNameConditionalStandard removeObjectAtIndex:indexxMatched];
                                     
                                     chkBtnCheckBoxStan=YES;
                                     [arrFinalIndexPath addObject:[NSString stringWithFormat:@"%ld",(long)indexpath.row]];
                                     NSLog(@"Final Index array %@",arrFinalIndexPath);
                                     NSLog(@"Final IndexArray count %lu",(unsigned long)arrFinalIndexPath.count);
                                     //[tappedCell.btnImgStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
                                     [tappedCell.imgView setImage:[UIImage imageNamed:@"check_box_2.png"]];
                                     
                                     NSString *strSysNameConditional=arrSysName[indexpath.row];
                                     
                                     [arrSysNameConditionalStandard addObject:strSysNameConditional];
                                     //Nilind 28 Dec
                                     
                                     /* chkBox=YES;
                                      NSManagedObject *recordCheckBox = [arrAllObj objectAtIndex:indexpath.row];
                                      [self updateStadardForFinalSave:recordCheckBox];*/
                                     //............
                                     NSLog(@"arrFinalIndexPath>>%@",arrFinalIndexPath);
                                     
                                     [_tblRecord reloadData];
                                     
                                     
                                 }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            
            chkBtnCheckBoxStan=YES;
            [arrFinalIndexPath addObject:[NSString stringWithFormat:@"%ld",(long)indexpath.row]];
            NSLog(@"Final Index array %@",arrFinalIndexPath);
            NSLog(@"Final IndexArray count %lu",(unsigned long)arrFinalIndexPath.count);
            //[tappedCell.btnImgStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            [tappedCell.imgView setImage:[UIImage imageNamed:@"check_box_2.png"]];
            
            NSString *strSysNameConditional=arrSysName[indexpath.row];
            
            [arrSysNameConditionalStandard addObject:strSysNameConditional];
            
        }
    }
    
    NSManagedObject *record = [arrAllObj objectAtIndex:sender.tag];//[self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath1];
    [self updateStadard:record];
    if(isEditedInSalesAuto==YES)
    {
        NSLog(@"Global modify date called in buttonClickedStandardService");
        [global updateSalesModifydate:strLeadId];
    }
}
-(void)updateStadardNew:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    
    NSError *error1 = nil;
    matches=indexValue;
    //    if (chkBtnCheckBoxStan==YES)
    //    {
    //        [matches setValue:@"true" forKey:@"isSold"];
    //    }
    //    else
    //    {
    [matches setValue:@"false" forKey:@"isSold"];
    //    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataStandard];
}
-(void)buttonClickedNonStandardService:(UIButton*)sender
{
    isEditedInSalesAuto=YES;
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexPath1 = [_tblNonStandardService indexPathForCell:(UITableViewCell *)sender.superview.superview];
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    SalesAutomationTableViewCellSecond *tappedCell = (SalesAutomationTableViewCellSecond *)[_tblNonStandardService cellForRowAtIndexPath:indexpath];
    //if ([tappedCell.btnImageNonStandardService.imageView.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    if ([tappedCell.imgViewNonStandardService.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        
        // [tappedCell.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [tappedCell.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_1.png"]];
        chkBtnCheckBoxNonStan=NO;
        for(int i=0;i<arrFinalIndexPathNonStan.count;i++)
        {
            if([[arrFinalIndexPathNonStan objectAtIndex:i]isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath1.row]])
            {
                [arrFinalIndexPathNonStan removeObjectAtIndex:i];
                
            }
            NSLog(@"After Removing New Final IndexArray%@",arrFinalIndexPathNonStan);
            NSLog(@"After Removing New Final IndexArray count %lu",(unsigned long)arrFinalIndexPathNonStan.count);
        }
        
    }
    else
    {
        chkBtnCheckBoxNonStan=YES;
        [arrFinalIndexPathNonStan addObject:[NSString stringWithFormat:@"%ld",(long)indexPath1.row]];
        NSLog(@"Final Index array %@",arrFinalIndexPathNonStan);
        NSLog(@"Final IndexArray count %lu",(unsigned long)arrFinalIndexPathNonStan.count);
        
        //[tappedCell.btnImageNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [tappedCell.imgViewNonStandardService setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    [self fetchFromCoreDataNonStandard];
    NSManagedObject *record = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath1];
    [self updateNonStadard:record];
    if(isEditedInSalesAuto==YES)
    {
        NSLog(@"Global modify date called in buttonClickedNonStandardService");
        [global updateSalesModifydate:strLeadId];
    }
}
-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==1) //Non Standard
    {
        
        UITableViewRowAction *rowActionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                              
                                               {
            
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setBool:NO forKey:@"backStan"];
            [defs synchronize];
            UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
            AddNonStandardServicesiPad *objAddNonStandardServices=[storyBoard instantiateViewControllerWithIdentifier:@"AddNonStandardServicesiPad"];
            
            objAddNonStandardServices.strForEdit = @"EditNonStandard";
            
            objAddNonStandardServices.matchesServiceToBeEdited = [self fetchFromCoreDataNonStandardForUpdate:[arrSoldServiceNonStandId objectAtIndex:indexPath.row]];
            
            [self.navigationController presentViewController:objAddNonStandardServices animated:YES completion:nil];
            
            /* UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"WDOiPad" bundle:nil];
             
             HTMLEditorVC *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"HTMLEditorVC"];
             
             objSalesAutomationAgreementProposal.strHtml=[arrNonStanServiceDesc objectAtIndex:indexPath.row];
             
             objSalesAutomationAgreementProposal.strLeadId = strLeadId;
             objSalesAutomationAgreementProposal.strServiceId = [arrSoldServiceNonStandId objectAtIndex:indexPath.row];
             
             objSalesAutomationAgreementProposal.strfrom=@"SalesResidentialNonStan";
             
             objSalesAutomationAgreementProposal.modalPresentationStyle = UIModalPresentationFullScreen;
             [self presentViewController:objSalesAutomationAgreementProposal animated:NO completion:nil];*/
            
            
        }];
        
            UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                                     {
                
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Alert!"
                                              message:@"Are you sure want to delete"
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                    isEditedInSalesAuto=YES;
                    NSLog(@"arrFinalIndexPath before delete==%@",arrFinalIndexPathNonStan);
                    NSLog(@"arrInitialPrice before delete ==%@",arrNonStanInitialPrice);
                    NSLog(@"arrDiscount before delete ==%@",arrNonStanDiscount);
                    NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
                    [self fetchFromCoreDataNonStandard];
                    
                    NSManagedObject *managedObject = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath];
                    [context deleteObject:managedObject];
                    NSError *saveError = nil;
                    [context save:&saveError];
                    
                    [self fetchFromCoreDataNonStandard];
                    
                    [_tblNonStandardService reloadData];
                    
                }];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Cancel"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                    
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                
                [alert addAction:ok];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
            }];
        
        rowActionDelete.backgroundColor=[UIColor redColor];
        rowActionEdit.backgroundColor=[UIColor grayColor];
        return @[rowActionDelete,rowActionEdit];
        return @[rowActionEdit];
    }
    else if (tableView.tag==0) //Standard
    {
        
        UITableViewRowAction *rowActionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                              
                                               {
            if ([self chkForPlusServiceExistence:[arrSysName objectAtIndex:indexPath.row]]) //PLus Service
            {
                 UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
                 AddPlusServiceiPad *objAddPlusService=[storyBoard instantiateViewControllerWithIdentifier:@"AddPlusServiceiPad"];
                objAddPlusService.strForEdit = @"EditStandard";
                
                objAddPlusService.arrPlusService= [self getPLusServiceArray:[arrSysName objectAtIndex:indexPath.row]];

                objAddPlusService.matchesServiceToBeEdited = [self fetchFromCoreDataStandardForUpdate:[arrSoldServiceStandardId objectAtIndex:indexPath.row]];

                 [self.navigationController presentViewController:objAddPlusService animated:YES completion:nil];
            }
            else
            {
                UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
                AddStandardServiceiPad *objAddStandardService=[storyBoard instantiateViewControllerWithIdentifier:@"AddStandardServiceiPad"];
                objAddStandardService.serviceType = @"Standard";
                objAddStandardService.strForEdit = @"EditStandard";
                
                objAddStandardService.matchesServiceToBeEdited = [self fetchFromCoreDataStandardForUpdate:[arrSoldServiceStandardId objectAtIndex:indexPath.row]];
                
                [self.navigationController presentViewController:objAddStandardService animated:YES completion:nil];
            }

        }];
        
            UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                                     {
                
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Alert!"
                                              message:@"Are you sure want to delete"
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                    isEditedInSalesAuto=YES;
                    NSLog(@"arrFinalIndexPath before delete ==%@",arrFinalIndexPath);
                    NSLog(@"arrInitialPrice before delete ==%@",arrInitialPrice);
                    NSLog(@"arrDiscount before delete ==%@",arrDiscount);
                    NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
                    chkDirectServiceDelete=YES;
                    [self fetchFromCoreDataStandard];
                    NSString *strId=[arrSoldServiceStandardId objectAtIndex:indexPath.row];
                    [self deleteFromCoreDataSalesInfo:[arrSysName objectAtIndex:indexPath.row]:[arrSoldServiceStandardId objectAtIndex:indexPath.row]];
                    
                    [self fetchForAppliedDiscountServiceFromCoreData:strId];
                    [self fetchFromCoreDataStandard];
                    
                    if ([arrFinalIndexPath containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                        
                        NSInteger index=[arrFinalIndexPath indexOfObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
                        
                        [arrSysNameConditionalStandard removeObjectAtIndex:index];
                        
                        [arrFinalIndexPath removeObjectAtIndex:index];
                        
                    }
                    
                    [self fetchFromCoreDataStandardNewSaavan];
                    
                    [_tblRecord reloadData];
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    chkDirectServiceDelete=NO;
                }];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Cancel"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                    
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                
                [alert addAction:ok];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
                /*   NSLog(@"arrFinalIndexPath before delete ==%@",arrFinalIndexPath);
                 NSLog(@"arrInitialPrice before delete ==%@",arrInitialPrice);
                 NSLog(@"arrDiscount before delete ==%@",arrDiscount);
                 NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
                 
                 
                 NSManagedObject *managedObject = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath];
                 [context deleteObject:managedObject];
                 NSError *saveError = nil;
                 [context save:&saveError];
                 
                 [self fetchFromCoreDataStandard];
                 
                 [_tblRecord reloadData];*/
            }];
        
        rowActionDelete.backgroundColor=[UIColor redColor];
        rowActionEdit.backgroundColor=[UIColor grayColor];
        return @[rowActionDelete,rowActionEdit];
        return @[rowActionEdit];
    }
    
    else
    {
        return UITableViewRowActionStyleDefault;
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==0 || tableView.tag==1 || tableView.tag==101 || tableView.tag==102)
    {
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            return NO;
        }
        else
        {
            
            if (chkKeyboardCompeUp == YES)
            {
                return NO;
            }
            else
            {
                return YES;
            }
            //return YES;
            
        }
    }
    
    else
    {
        return NO;
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==0)
    {
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Are you sure want to delete"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     isEditedInSalesAuto=YES;
                                     NSLog(@"arrFinalIndexPath before delete ==%@",arrFinalIndexPath);
                                     NSLog(@"arrInitialPrice before delete ==%@",arrInitialPrice);
                                     NSLog(@"arrDiscount before delete ==%@",arrDiscount);
                                     NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
                                     chkDirectServiceDelete=YES;
                                     [self fetchFromCoreDataStandard];
                                     NSString *strId=[arrSoldServiceStandardId objectAtIndex:indexPath.row];
                                     [self deleteFromCoreDataSalesInfo:[arrSysName objectAtIndex:indexPath.row]:[arrSoldServiceStandardId objectAtIndex:indexPath.row]];
                                     
                                     [self fetchForAppliedDiscountServiceFromCoreData:strId];
                                     [self fetchFromCoreDataStandard];
                                     
                                     if ([arrFinalIndexPath containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                                         
                                         NSInteger index=[arrFinalIndexPath indexOfObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
                                         
                                         [arrSysNameConditionalStandard removeObjectAtIndex:index];
                                         
                                         [arrFinalIndexPath removeObjectAtIndex:index];
                                         
                                     }
                                     
                                     [self fetchFromCoreDataStandardNewSaavan];
                                     
                                     [_tblRecord reloadData];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     chkDirectServiceDelete=NO;
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
            /*   NSLog(@"arrFinalIndexPath before delete ==%@",arrFinalIndexPath);
             NSLog(@"arrInitialPrice before delete ==%@",arrInitialPrice);
             NSLog(@"arrDiscount before delete ==%@",arrDiscount);
             NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
             
             
             NSManagedObject *managedObject = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath];
             [context deleteObject:managedObject];
             NSError *saveError = nil;
             [context save:&saveError];
             
             [self fetchFromCoreDataStandard];
             
             [_tblRecord reloadData];*/
        }
    }
    else if (tableView.tag==1)
    {
        
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Are you sure want to delete"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     isEditedInSalesAuto=YES;
                                     NSLog(@"arrFinalIndexPath before delete==%@",arrFinalIndexPathNonStan);
                                     NSLog(@"arrInitialPrice before delete ==%@",arrNonStanInitialPrice);
                                     NSLog(@"arrDiscount before delete ==%@",arrNonStanDiscount);
                                     NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
                                     [self fetchFromCoreDataNonStandard];
                                     
                                     NSManagedObject *managedObject = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath];
                                     [context deleteObject:managedObject];
                                     NSError *saveError = nil;
                                     [context save:&saveError];
                                     
                                     [self fetchFromCoreDataNonStandard];
                                     
                                     [_tblNonStandardService reloadData];
                                     
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            /* NSLog(@"arrFinalIndexPath before delete==%@",arrFinalIndexPathNonStan);
             NSLog(@"arrInitialPrice before delete ==%@",arrNonStanInitialPrice);
             NSLog(@"arrDiscount before delete ==%@",arrNonStanDiscount);
             NSLog(@"arrMaintenancePrice before delete ==%@",arrMaintenancePrice);
             
             
             NSManagedObject *managedObject = [self.fetchedResultsControllerSalesInfo objectAtIndexPath:indexPath];
             [context deleteObject:managedObject];
             NSError *saveError = nil;
             [context save:&saveError];
             
             [self fetchFromCoreDataNonStandard];
             
             [_tblNonStandardService reloadData];*/
        }
    }
    else if (tableView.tag==101)
    {
        {
            
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Are you sure want to delete"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [self fetchForAppliedDiscountFromCoreData];
                                     NSManagedObject *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
                                     
                                     [self deleteAppliedCouponFromCoreDataSalesInfo:dict:@"strDirect"];
                                     [self heightForDiscountCoupon];
                                     
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
    }
    else if (tableView.tag==102) //Renewal Service
    {
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert!"
                                      message:@"Are you sure want to delete"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
            
             [self fetchRenewalServiceFromCoreData];
            
             NSManagedObject *dict=[arrAllObjRenewal objectAtIndex:indexPath.row];
            
            [self deleteRenewalFromCoreDataSalesInfo:[NSString stringWithFormat:@"%@",[dict valueForKey:@"soldServiceStandardId"]]];
            
            //[self deleteAppliedCouponFromCoreDataSalesInfo:dict:@"strDirect"];
            // [self heightManage];
            //[self heightForDiscountCoupon];
            
        }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}
-(void)addAction
{
     [self endEditing];
    if (chkClickStan==YES)
    {
        /* UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
         AddStandardServiceiPad *objAddStandardService=[storyBoard instantiateViewControllerWithIdentifier:@"AddStandardServiceiPad"];
         [self.navigationController presentViewController:objAddStandardService animated:YES completion:nil];*/
        [self bundleCheck];
    }
    
    else
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"backStan"];
        [defs synchronize];
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
        AddNonStandardServicesiPad *objAddNonStandardServices=[storyBoard instantiateViewControllerWithIdentifier:@"AddNonStandardServicesiPad"];
        [self.navigationController presentViewController:objAddNonStandardServices animated:YES completion:nil];
        
        //[self bundleCheck];
    }
    
}
- (IBAction)actionOnStandardService:(id)sender
{
     [self endEditing];
    NSUserDefaults *defsStanClick = [NSUserDefaults standardUserDefaults];
    [defsStanClick setBool:YES forKey:@"clickStan"];
    [defsStanClick synchronize];
    
    
    [_btnNonStandardService setBackgroundColor:[UIColor lightGrayColor]];
    [_btnStandardService setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    _viewForNonStandardService.hidden=YES;
    _viewForStandardService.hidden=NO;
    chkClickStan=YES;
    [self fetchFromCoreDataStandard];
    [self heightManage];
    
}

- (IBAction)actionOnNonStandardService:(id)sender
{
    [self endEditing];
    NSUserDefaults *defsStanClick = [NSUserDefaults standardUserDefaults];
    [defsStanClick setBool:NO forKey:@"clickStan"];
    [defsStanClick synchronize];
    
    [self discountStatusNonStan];
    [_btnStandardService setBackgroundColor:[UIColor lightGrayColor]];
    [_btnNonStandardService setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    
    [self fetchFromCoreDataNonStandard];
    
    //    [_btnStandardService setBackgroundColor:[UIColor lightGrayColor]];
    //    [_btnNonStandardService setBackgroundColor:[UIColor colorWithRed:236.0f/255 green:27.0f/255 blue:33.0f/255 alpha:1]];
    
    chkClickStan=NO;
    _viewForNonStandardService.hidden=NO;
    _viewForStandardService.hidden=YES;
    
    
    
    //Non Standard
    int sumNonStan=0;
    for (int i=0; i<arrNonStanServiceName.count; i++)
    {
        sumNonStan=sumNonStan+_tblNonStandardService.rowHeight;
    }
    _const_TableNonStan_H.constant=sumNonStan+50+50;
    [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableNonStan_H.constant+100)];
    
    // [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableStan_H.constant+_constTblBundle_H.constant+_const_TableNonStan_H.constant)];
}

- (IBAction)actionOnAddNonStandardService:(id)sender
{
    
}

- (IBAction)actionOnCategory1:(id)sender
{
    tblData.tag=10;
    [self tableLoad:tblData.tag];
}
- (IBAction)actionCategory2:(id)sender
{
    tblData.tag=20;
    [self tableLoad:tblData.tag];
}
- (IBAction)actionOnCategory3:(id)sender
{
    tblData.tag=30;
    [self tableLoad:tblData.tag];
}
-(void)setTableFrame:(NSInteger)btntag
{
    
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            tblData.frame=CGRectMake(_btnCategory1.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory1.frame.origin.y+35+150, [UIScreen mainScreen].bounds.size.width/3, 300);
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(_btnCategory1.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory1.frame.origin.y+35+150-15, [UIScreen mainScreen].bounds.size.width/3, 250);
            }
            
            break;
        }
        case 20:
        {
            tblData.frame=CGRectMake(_btnCategory2.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory2.frame.origin.y+35+150, [UIScreen mainScreen].bounds.size.width/3, 300);
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(_btnCategory2.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory2.frame.origin.y+35+150-10, [UIScreen mainScreen].bounds.size.width/3, 250);
            }
            
            break;
        }
        case 30:
        {
            tblData.frame=CGRectMake(_btnCategory3.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory3.frame.origin.y+35+150, [UIScreen mainScreen].bounds.size.width/3, 300);
            if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
            {
                tblData.frame=CGRectMake(_btnCategory3.frame.origin.x,_viewForStandardService.frame.origin.y+ _btnCategory3.frame.origin.y+35+150-10, [UIScreen mainScreen].bounds.size.width/3, 250);
            }
            break;
        }
            
        default:
            break;
    }
    
    
    
    [self.view addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
        case 20:
        {
            [self setTableFrame:i];
            break;
        }
        case 30:
        {
            [self setTableFrame:i];
            break;
        }
            
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger i;
    i=tblData.tag;
    switch (i)
    {
        case 10:
        {
            [_btnCategory1 setTitle:[arrCategory objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            break;
        }
        case 20:
        {
            [_btnCategory2 setTitle:[arrCategory objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            break;
        }
        case 30:
        {
            [_btnCategory3 setTitle:[arrCategory objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            break;
        }
        default:
            break;
    }
    
    [tblData removeFromSuperview];
}

- (IBAction)actionOnSaveContinue:(id)sender
{
     [self endEditing];
    //New  NIlind
    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
    {
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
        SalesAutomationServiceSummaryiPad *objSaleAutoServiceSummary=[storyBoard instantiateViewControllerWithIdentifier:@"SalesAutomationServiceSummaryiPad"];
        [self.navigationController pushViewController:objSaleAutoServiceSummary animated:YES];
        
        NSUserDefaults *defs= [NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"fromSelectServiceProposal"];
        [defs synchronize];
    }
    else
    {
        [self fetchFromCoreDataStandardForSold];
        [self fetchFromCoreDataNonStandardForSold];
        
        if (arrSoldAvailableStan.count==0 && arrSoldAvailableNonStan.count==0)
        {
            [global displayAlertController:@"Alert" :@"To proceed, please add atleast one service to the agreement" :self];
            
        }
        else
        {
            if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
            {
                
            }
            else
            {
                [self saveImageToCoreData];
                
            }
            [self chekcBox];
            [self fetchFromCoreDataStandard];
            [global updateSalesZSYNC:strLeadId :@"yes"];
            if(isEditedInSalesAuto==YES)
            {
                NSLog(@"Global modify date called in Select Service");
                [global updateSalesModifydate:strLeadId];
            }
            if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
            {
            }
            else
            {
                [self finalUpdatedOnSaveForTotalInitialAndMaintPrice];
                [self finalUpdatedNonStanOnSaveForBillilngFreqPrice];
                [self updateLeadAppliedDiscountForAppliedCoupon];
                
                
                //temp
                [self updateLeadAppliedDiscountForCredit];
                
                /*[self fetchFromCoreDataStandard];
                 if (arrAllObj.count==0)
                 {
                 [self deleteLeadAppliedDiscountFromCoreData];
                 }*/
                
            }
            UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
            SalesAutomationServiceSummaryiPad *objSaleAutoServiceSummary=[storyBoard instantiateViewControllerWithIdentifier:@"SalesAutomationServiceSummaryiPad"];
            [self.navigationController pushViewController:objSaleAutoServiceSummary animated:YES];
            
            NSUserDefaults *defs= [NSUserDefaults standardUserDefaults];
            [defs setBool:NO forKey:@"fromSelectServiceProposal"];
            [defs synchronize];
        }
        
    }
    
    //End
}

- (IBAction)actionOnCancel:(id)sender
{
     [self endEditing];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isBackFromSelectService"];
    [defs synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewStandard
{
    UIView *viewStandard=[[UIView alloc]init];
    viewStandard.frame=CGRectMake(20, 50, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height-100);
    viewStandard.backgroundColor=[UIColor purpleColor];
    
    UILabel *lblTitle=[[UILabel alloc]init];
    lblTitle.frame=CGRectMake(0, 0,  viewStandard.frame.size.width, 35);
    lblTitle.textColor=[UIColor blueColor];
    lblTitle.text=@"VIEW STANDARD";
    lblTitle.backgroundColor=[UIColor lightGrayColor];
    [viewStandard addSubview:lblTitle];
    
    
    UIButton *btnCategory1=[[UIButton alloc]init];
    btnCategory1.frame=CGRectMake(10, lblTitle.frame.origin.y+lblTitle.frame.size.height+5, viewStandard.frame.size.width-10, 30);
    btnCategory1.backgroundColor=[UIColor blueColor];
    
    
    UIButton *btnCategory2=[[UIButton alloc]init];
    btnCategory2.frame=CGRectMake(10, btnCategory1.frame.origin.y+btnCategory1.frame.size.height+5, viewStandard.frame.size.width-10, 30);
    btnCategory2.backgroundColor=[UIColor blueColor];
    
    
    UIButton *btnCategory3=[[UIButton alloc]init];
    btnCategory3.frame=CGRectMake(10, btnCategory2.frame.origin.y+btnCategory2.frame.size.height+5, viewStandard.frame.size.width-10, 30);
    btnCategory3.backgroundColor=[UIColor blueColor];
    
    
    [viewStandard addSubview:btnCategory1];
    [viewStandard addSubview:btnCategory2];
    [viewStandard addSubview:btnCategory3];
    
    
    UITextField *txt1=[[UITextField alloc]init];
    txt1.frame=CGRectMake(5, btnCategory3.frame.origin.y+btnCategory3.frame.size.height+5, 50, 30);
    txt1.backgroundColor=[UIColor blueColor];
    
    UITextField *txt2=[[UITextField alloc]init];
    txt2.frame=CGRectMake(txt1.frame.origin.x+txt1.frame.size.width+ 5, btnCategory3.frame.origin.y+btnCategory3.frame.size.height+5, 50, 30);
    txt2.backgroundColor=[UIColor blueColor];
    
    UITextField *txt3=[[UITextField alloc]init];
    txt3.frame=CGRectMake(txt2.frame.origin.x+txt2.frame.size.width+ 5 ,btnCategory3.frame.origin.y+btnCategory3.frame.size.height+5, 50, 30);
    txt3.backgroundColor=[UIColor blueColor];
    
    [viewStandard addSubview:txt1];
    [viewStandard addSubview:txt2];
    [viewStandard addSubview:txt3];
    
    [self.view addSubview:viewStandard];
}
#pragma mark- 30 Aug Nilind
#pragma mark- FETCH CORE DATA
-(void)fetchFromCoreDataStandard
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrMatchesBundle= [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrInitialPrice=[[NSMutableArray alloc]init];
    arrDiscountPer=[[NSMutableArray alloc]init];
    
    arrDiscount=[[NSMutableArray alloc]init];
    arrMaintenancePrice=[[NSMutableArray alloc]init];
    arrFrequencyName=[[NSMutableArray alloc]init];
    arrServiceName=[[NSMutableArray alloc]init];
    arrStanIsSold=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    
    arrUnit=[[NSMutableArray alloc]init];
    arrFinalInitialPrice=[[NSMutableArray alloc]init];
    arrFinalMaintPrice=[[NSMutableArray alloc]init];
    
    arrPackageNameId=[[NSMutableArray alloc]init];
    arrBillingFreqSysName=[[NSMutableArray alloc]init];
    //arrBundleId=[[NSMutableArray alloc]init];
    NSMutableArray *arrBundleNew;
    arrBundleNew=[[NSMutableArray alloc]init];
    
    arrTempBundelDetailId=[[NSMutableArray alloc]init];
    arrTempBundleServiceSysName=[[NSMutableArray alloc]init];
    arrBundleRow=[[NSMutableArray alloc]init];
    arrSoldServiceStandardId=[[NSMutableArray alloc]init];
    arrRandomId=[[NSMutableArray alloc]init];
    arrAdditionalParamterDcs=[[NSMutableArray alloc]init];
    arrFreqSysName=[[NSMutableArray alloc]init];
    arrSoldServiceStandardIdForCoupon=[[NSMutableArray alloc]init];
    arrServiceId=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                [arrFrequencyName addObject:[matches valueForKey:@"serviceFrequency"]];
                [arrFreqSysName addObject:[matches valueForKey:@"frequencySysName"]];
                [arrServiceId addObject:[matches valueForKey:@"serviceId"]];
                
                if ([[matches valueForKey:@"serviceSysName"]isEqualToString:@""] || [[matches valueForKey:@"serviceSysName"]isEqualToString:@"(null)"])
                {
                    [arrServiceName addObject:@""];
                }
                else
                {
                    /*NSString *str = [dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]];
                    
                    if([str isEqual:nil] || str.length == 0)
                    {
                        [arrServiceName addObject:@""];
                    }
                    else
                    {
                        [arrServiceName addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];

                    }*/
                    
                    [arrServiceName addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                }
                
                [arrStanIsSold addObject: [matches valueForKey:@"isSold"]];
                
                [arrSysName addObject:[matches valueForKey:@"serviceSysName"]];
                
                //Nilind 6 oct
                
                if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isTBD"]]isEqualToString:@"true"])
                {
                    /* [arrInitialPrice addObject:@"TBD"];
                     [arrDiscountPer addObject:@"TBD"];
                     [arrDiscount addObject:@"TBD"];
                     [arrMaintenancePrice addObject:@"TBD"];
                     [arrFinalInitialPrice addObject:@"TBD"];
                     [arrFinalMaintPrice addObject:@"TBD"];*/
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]isEqualToString:@"TBD"])
                    {
                        [arrInitialPrice addObject:@"TBD"];
                    }
                    else
                    {
                        [arrInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
                        
                    }
                    
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"discount"]]isEqualToString:@"TBD"])
                    {
                        [arrDiscount addObject:@"TBD"];
                    }
                    else
                    {
                        [arrDiscount addObject:[matches valueForKey:@"discount"]];
                        
                    }
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]isEqualToString:@"TBD"])
                    {
                        [arrMaintenancePrice addObject:@"TBD"];
                    }
                    else
                    {
                        [arrMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
                        
                    }
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedInitialPrice"]]isEqualToString:@"TBD"])
                    {
                        [arrFinalInitialPrice addObject:@"TBD"];
                    }
                    else
                    {
                        [arrFinalInitialPrice addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
                        
                    }
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedMaintPrice"]]isEqualToString:@"TBD"])
                    {
                        [arrFinalMaintPrice addObject:@"TBD"];
                    }
                    else
                    {
                        [arrFinalMaintPrice addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
                        
                    }
                    
                    
                    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"discountPercentage"]]isEqualToString:@"TBD"])
                    {
                        [arrDiscountPer addObject:@"TBD"];
                    }
                    else
                    {
                        [arrDiscountPer addObject:[matches valueForKey:@"discountPercentage"]];
                        
                    }
                    
                    
                }
                else
                {
                    [arrInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
                    [arrDiscount addObject:[matches valueForKey:@"discount"]];
                    [arrMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
                    [arrFinalInitialPrice addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
                    [arrFinalMaintPrice addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
                    [arrDiscountPer addObject:[matches valueForKey:@"discountPercentage"]];
                    
                }
                [arrUnit addObject:[matches valueForKey:@"unit"]];
                
                [arrPackageNameId addObject:[matches valueForKey:@"packageId"]];
                [arrBillingFreqSysName addObject:[matches valueForKey:@"billingFrequencySysName"]];
                [arrSoldServiceStandardId addObject:[matches valueForKey:@"soldServiceStandardId"]];
                
                //[arrBundleId addObject:[matches valueForKey:@"bundleId"]];
                // if([[matches valueForKey:@"additionalParameterPriceDcs"] isKindOfClass:[NSArray class]])
                // {
                [arrAdditionalParamterDcs addObject:[matches valueForKey:@"additionalParameterPriceDcs"]];
                // }
                
            }
            else
            {
                //[arrBundleId addObject:[matches valueForKey:@"bundleId"]];
                //[arrBundleRow addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]];
                [arrBundleNew addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]];
                
                if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleDetailId"]] isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleDetailId"]] isEqual:nil])
                {
                    
                }
                else
                {
                    //[arrTempBundleId addObject:[matches valueForKey:@"bundleId"]];
                    [arrTempBundelDetailId addObject:[matches valueForKey:@"bundleDetailId"]];
                    [arrTempBundleServiceSysName addObject:[matches valueForKey:@"serviceSysName"]];
                    
                    
                }
                
                
            }
            [arrSoldServiceStandardIdForCoupon addObject:[matches valueForKey:@"soldServiceStandardId"]];
            
        }
        arrBundleRow = [[NSSet setWithArray:arrBundleNew] allObjects];
        
        // [self fetchFromCoreDataStandard];
        [_tblBundle reloadData];
        [_tblRecord reloadData];
        //[self heightManage];
        
    }
    if(arrAllObj.count==0)
    {
        _lblMaintenanceServiceStandard.hidden=YES;
    }
    else
    {
        _lblMaintenanceServiceStandard.hidden=NO;
    }
    if(arrBundleRow.count==0)
    {
        _lblBundleServiceStandard.hidden=YES;
    }
    else
    {
        _lblBundleServiceStandard.hidden=NO;
        
    }
    [self fetchRenewalServiceFromCoreData];
}
-(void)fetchFromCoreDataStandardNewSaavan
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,@"0"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrAllObjSaavan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrInitialPrice=[[NSMutableArray alloc]init];
    arrDiscount=[[NSMutableArray alloc]init];
    arrMaintenancePrice=[[NSMutableArray alloc]init];
    arrFrequencyName=[[NSMutableArray alloc]init];
    arrServiceName=[[NSMutableArray alloc]init];
    arrStanIsSold=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    //temp
    arrSysNameConditionalStandard=[[NSMutableArray alloc]init];
    arrFinalIndexPath=[[NSMutableArray alloc]init];
    //End
    if (arrAllObjSaavan.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSaavan.count; k++)
        {
            matches=arrAllObjSaavan[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            [arrFrequencyName addObject:[matches valueForKey:@"serviceFrequency"]];
            
            if ([[matches valueForKey:@"serviceSysName"]isEqualToString:@""] || [[matches valueForKey:@"serviceSysName"]isEqualToString:@"(null)"])
            {
                [arrServiceName addObject:@""];
            }
            else
            {
                [arrServiceName addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
            }
            
            [arrStanIsSold addObject: [matches valueForKey:@"isSold"]];
            
            if ([[matches valueForKey:@"isSold"] isEqualToString:@"true"]) {
                
                NSString *strPath=[NSString stringWithFormat:@"%d",k];
                
                [arrFinalIndexPath addObject:strPath];
                
                NSString *strSysNameConditional=[matches valueForKey:@"serviceSysName"];
                
                [arrSysNameConditionalStandard addObject:strSysNameConditional];
                
            }else{
                
                // NSString *strPath=[NSString stringWithFormat:@"%d",k];
                
                
                
                // NSString *strSysNameConditional=[matches valueForKey:@"serviceSysName"];
                
                if (arrSysNameConditionalStandard.count>k) {
                    
                    [arrSysNameConditionalStandard removeObjectAtIndex:k];
                    [arrFinalIndexPath removeObjectAtIndex:k];
                }
            }
            [arrSysName addObject:[matches valueForKey:@"serviceSysName"]];
            
            //Nilind 6 oct
            
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isTBD"]]isEqualToString:@"true"])
            {
                [arrInitialPrice addObject:@"TBD"];
                [arrDiscount addObject:@"TBD"];
                [arrMaintenancePrice addObject:@"TBD"];
                [arrDiscountPer addObject:@"TBD"];
            }
            else
            {
                [arrInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
                [arrDiscount addObject:[matches valueForKey:@"discount"]];
                [arrDiscountPer addObject:[matches valueForKey:@"discountPercentage"]];
                [arrMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
            }
            
            //....................
            
        }
        // [self fetchFromCoreDataStandard];
        [_tblRecord reloadData];
    }
}

-(void)fetchFromCoreDataNonStandard
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrNonStanInitialPrice=[[NSMutableArray alloc]init];
    arrNonStanDiscount=[[NSMutableArray alloc]init];
    arrNonStanServiceName=[[NSMutableArray alloc]init];
    arrNonStanServiceDesc=[[NSMutableArray alloc]init];
    arrNonStanIsSold=[[NSMutableArray alloc]init];
    arrDepartmentSysName=[[NSMutableArray alloc]init];
    arrDiscountPerNonStan=[[NSMutableArray alloc]init];
    arrBillingFreqSysNameNonStan=[[NSMutableArray alloc]init];
    arrServiceFreqSysNameNonStan=[[NSMutableArray alloc]init];
    arrMaintPriceNonStan=[[NSMutableArray alloc]init];
    
    arrTaxStatusNonStan = [[NSMutableArray alloc]init];
    arrSoldServiceNonStandId = [[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            [arrNonStanInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
            [arrNonStanDiscount addObject:[matches valueForKey:@"discount"]];
            [arrNonStanServiceName addObject:[matches valueForKey:@"serviceName"]];
            [arrNonStanServiceDesc addObject:[matches valueForKey:@"serviceDescription"]];
            [arrNonStanIsSold addObject:[matches valueForKey:@"isSold"]];
            
            //Nilind
            [arrDepartmentSysName addObject:[matches valueForKey:@"departmentSysname"]];
            [arrDiscountPerNonStan addObject:[matches valueForKey:@"discountPercentage"]];
            [arrBillingFreqSysNameNonStan addObject:[matches valueForKey:@"billingFrequencySysName"]];
            
            //......................................................................
            [arrMaintPriceNonStan addObject:[matches valueForKey:@"maintenancePrice"]];
            [arrServiceFreqSysNameNonStan addObject:[matches valueForKey:@"frequencySysName"]];
            
            [arrTaxStatusNonStan addObject:[matches valueForKey:@"isTaxable"]];
            [arrSoldServiceNonStandId addObject:[matches valueForKey:@"soldServiceNonStandardId"]];
            
        }
        
        NSDictionary *dictSalesLeadMaster;
        /*NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
         dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
         
         NSArray *arrDept=[dictSalesLeadMaster valueForKey:@"DepartmentMasters"];*/
        //        for (int i=0; i<arrDept.count; i++)
        //        {
        //            NSDictionary *dict=[arrDept objectAtIndex:i];
        //
        //            if ([[arrDepartmentSysName objectAtIndex:i] isEqualToString:[dict valueForKey:@"SysName"]])
        //            {
        //                [arrDepartmentName addObject:[dict valueForKey:@"Name"]];
        //            }
        //        }
        //Nilind
        // NSMutableArray *arrDept=[[NSMutableArray alloc]init];
        NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
        dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
        NSMutableArray *arrDept;
        NSMutableArray *arrName;//*arrSysName;
        arrName=[[NSMutableArray alloc]init];
        ///arrSysName=[[NSMutableArray alloc]init];//////
        arrDept=[[NSMutableArray alloc]init];
        NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
        for (int i=0;i<arrDeptName.count; i++)
        {
            NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
            NSArray *arry=[dictBranch valueForKey:@"Departments"];
            for (int j=0; j<arry.count; j++)
            {
                NSDictionary *dict=[arry objectAtIndex:j];
                if ([strBranchSysName isEqualToString:[dictBranch valueForKey:@"SysName"]])
                {
                    //  [arrDept addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                    [arrDept addObject:dict];
                    //                [arrName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                    //                [arrSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                }
                
            }
            
        }
        
        
        
        //............
        
        arrDepartmentName=arrDepartmentSysName;
        for (int i=0; i<arrDepartmentSysName.count; i++)
        {
            for (int k=0; k<arrDept.count; k++)
            {
                NSDictionary *dict=[arrDept objectAtIndex:k];
                if([[arrDepartmentSysName objectAtIndex:i] isEqualToString:[dict valueForKey:@"SysName"]])
                {
                    // [arrDepartmentName addObject:[dict valueForKey:@"Name"]];
                    [arrDepartmentName replaceObjectAtIndex:i withObject:[dict valueForKey:@"Name"]];
                }
            }
        }
        
        [_tblNonStandardService reloadData];
    }
}
//============================================================================
//============================================================================
#pragma mark- Sales Info CoreData Delete
//============================================================================
//============================================================================



-(void)deleteFromCoreDataSalesInfo:(NSString *)sysName : (NSString *)strSoldServiceStandardId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        NSString *strIdBundle,*strSysNameService,*strId;
        strIdBundle=[NSString stringWithFormat:@"%@",[data valueForKey:@"bundleId"]];
        strSysNameService=[NSString stringWithFormat:@"%@",[data valueForKey:@"serviceSysName"]];
        strId=[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceStandardId"]];
        if ([strIdBundle isEqualToString:@"0"]&&[strSysNameService isEqualToString:sysName]&&[strId isEqualToString:strSoldServiceStandardId])
        {
            [context deleteObject:data];
            break;
            
        }
    }
    [self deleteRenewalFromCoreDataSalesInfo:strSoldServiceStandardId];
    
    NSError *saveError = nil;
    [context save:&saveError];
    [self heightManage];
    
    
}
/*-(void)deleteFromCoreDataSalesInfo
 {
 
 appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
 context = [appDelegate managedObjectContext];
 
 //  Delete Lead Detail Data
 
 NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
 NSFetchRequest *allData = [[NSFetchRequest alloc] init];
 [allData setPredicate:predicate];
 [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
 [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
 
 NSError * error = nil;
 NSArray * Data = [context executeFetchRequest:allData error:&error];
 //error handling goes here
 for (NSManagedObject * data in Data) {
 [context deleteObject:data];
 }
 NSError *saveError = nil;
 [context save:&saveError];
 
 }*/
//===============31August============================

#pragma mark- Update Sales Info CoreData Standard

//============================================================================
-(void)updateStadard:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    
    NSError *error1 = nil;
    matches=indexValue;
    if (chkBtnCheckBoxStan==YES)
    {
        [matches setValue:@"true" forKey:@"isSold"];
    }
    else
    {
        [matches setValue:@"false" forKey:@"isSold"];
    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataStandard];
}

-(void)updateNonStadard:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    
    NSError *error1 = nil;
    matches=indexValue;
    if (chkBtnCheckBoxNonStan==YES)
    {
        [matches setValue:@"true" forKey:@"isSold"];
    }
    else
    {
        [matches setValue:@"false" forKey:@"isSold"];
    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    [context save:&error1];
    
    [self fetchFromCoreDataNonStandard];
    
}
//===============6 Oct============================
-(void)plusService:(UIButton *)btn
{
    
    NSDictionary *dictChk;
    NSInteger tagPLusService;
    tagPLusService=btn.tag;
    NSLog(@"tagPLusService>>>%ld",(long)tagPLusService);
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tagPLusService inSection:0];
    
    SalesAutomationServiceTableViewCell *tappedCell = (SalesAutomationServiceTableViewCell *)[_tblRecord cellForRowAtIndexPath:indexpath];
    NSLog(@"%@",tappedCell.lblServiceSysName.text);
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //   NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    NSString *strSysNameService;
    BOOL chk;
    chk=NO;
    strSysNameService=tappedCell.lblServiceSysName.text;
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSMutableArray *arrPlusServiceDict;
    arrPlusServiceDict=[[NSMutableArray alloc]init];
    
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict1=[arrServices objectAtIndex:j];
                if ([strSysNameService isEqualToString:[dict1 valueForKey:@"ParentSysName"]])
                {
                    dictChk=dict;
                    NSLog(@"SERTVICE FOUND");
                    NSLog(@"TRUE");
                    NSLog(@"ParentSysName>>>>%@--%@",[dict1 valueForKey:@"ParentSysName"],[dict1 valueForKey:@"ServiceMasterId"]);
                    NSLog(@"Name>>>>%@",[dict1 valueForKey:@"Name"]);
                    chk=YES;
                    [arrPlusServiceDict addObject:dict1];
                }
                else
                {
                    
                }
            }
            
        }
        
    }
    if (arrPlusServiceDict.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Sorry,Plus Service Not Avaible" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
        AddPlusServiceiPad *objAddPlusService=[storyBoard instantiateViewControllerWithIdentifier:@"AddPlusServiceiPad"];
        objAddPlusService.arrPlusService=arrPlusServiceDict;
        
        objAddPlusService.strFreqName=tappedCell.lblFrequency.text;
        
        NSString *separatorString = @"/";
        NSString *myString = tappedCell.lblBillingFrequency.text;
        NSString *myNewString = [myString componentsSeparatedByString:separatorString].lastObject;
        objAddPlusService.strBillingFreqName=myNewString;
        
        NSLog(@"%@",arrPlusServiceDict);
        [self.navigationController presentViewController:objAddPlusService animated:YES completion:nil];
    }
    
    
}
-(void)serviceName
{
    NSMutableArray *name,*sysName,*qtyVal,*arrPackageId,*arrPackageName, *arrParamService,*arrServiceMasterId;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    qtyVal=[[NSMutableArray alloc]init];
    arrPackageId=[[NSMutableArray alloc]init];
    arrPackageName=[[NSMutableArray alloc]init];
    arrServiceMasterId=[[NSMutableArray alloc]init];
    
    
    arrParamService=[[NSMutableArray alloc]init];
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    // NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [qtyVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsUnitBasedService"]]];
                [arrParamService addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsParameterizedPriced"]]];
                [arrServiceMasterId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceMasterId"]]];
                
                NSArray *arrPackage=[dict valueForKey:@"ServicePackageDcs"];
                for (int k=0; k<arrPackage.count; k++)
                {
                    NSDictionary *dictPackage=[arrPackage objectAtIndex:k];
                    [arrPackageId addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"ServicePackageId"]]];
                    [arrPackageName addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"PackageName"]]];
                }
                
            }
        }
    }
    
    dictServiceName = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
    
    dictQuantityStatus = [NSDictionary dictionaryWithObjects:qtyVal forKeys:sysName];
    dictServiceNameForId =[NSDictionary dictionaryWithObjects:name forKeys:arrServiceMasterId];
    dictPackageName =[NSDictionary dictionaryWithObjects:arrPackageName forKeys:arrPackageId];
    dictServiceParmaterBasedStatus = [NSDictionary dictionaryWithObjects:arrParamService forKeys:sysName];
    
    
    NSLog(@"PaymentInfo%@",dictServiceName);
    NSLog(@"Package Detail %@",dictPackageName);
    
    //For Bundle
    NSArray *arrBundleData=[dictMasters valueForKey:@"ServiceBundles"];
    NSMutableArray *nameBundle,*keyBundleId;
    nameBundle=[[NSMutableArray alloc]init];
    keyBundleId=[[NSMutableArray alloc]init];
    for (int i=0; i<arrBundleData.count; i++)
    {
        NSDictionary *dict=[arrBundleData objectAtIndex:i];
        {
            [nameBundle addObject:[dict valueForKey:@"BundleName"]];
            [keyBundleId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceBundleId"]]];
        }
    }
    dictBundleNameFromId =[NSDictionary dictionaryWithObjects:nameBundle forKeys:keyBundleId];
    
    
    
    
    //For Discount
    
    NSArray *arrForDiscount;
    NSMutableArray *valNameDiscount,*keySysNameDiscount,*arrDiscountUsage,*arrDiscountCode;
    valNameDiscount=[[NSMutableArray alloc]init];
    keySysNameDiscount=[[NSMutableArray alloc]init];
    arrDiscountUsage=[[NSMutableArray alloc]init];
    arrDiscountCode=[[NSMutableArray  alloc]init];
    arrForDiscount=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
    if ([arrForDiscount isKindOfClass:[NSArray class]])
    {
        for (int i=0; i<arrForDiscount.count; i++)
        {
            NSDictionary *dict=[arrForDiscount objectAtIndex:i];
            {
                [valNameDiscount addObject:[dict valueForKey:@"Name"]];
                [keySysNameDiscount addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [arrDiscountUsage addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Usage"]]];
                [arrDiscountCode addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountCode"]]];
                
            }
        }
        dictDiscountNameFromSysName =[NSDictionary dictionaryWithObjects:valNameDiscount forKeys:keySysNameDiscount];
        dictDiscountUsageFromSysName =[NSDictionary dictionaryWithObjects:arrDiscountUsage forKeys:keySysNameDiscount];
        dictDiscountUsageFromCode =[NSDictionary dictionaryWithObjects:arrDiscountUsage forKeys:arrDiscountCode];
        
    }
    
    
}

-(void)chkForPlusServiceButton:(NSString *)strSysNameService
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict1=[arrServices objectAtIndex:j];
                if ([strSysNameService isEqualToString:[dict1 valueForKey:@"ParentSysName"]])
                {
                    chkkkk=YES;
                }
            }
            
        }
        
    }
}
-(BOOL)chkForPlusServiceExistence:(NSString *)strSysNameService
{
    BOOL show;
    
    show = NO;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict1=[arrServices objectAtIndex:j];
                if ([strSysNameService isEqualToString:[dict1 valueForKey:@"SysName"]] && [[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"IsPlusService"] ]isEqualToString:@"1"])//IsPlusService
                {
                    show=YES;
                    break;
                }
            }
            
        }
        
    }
    return show;
}
//Nilind 14 Nov

#pragma mark - SALES FETCH
-(void)salesFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjSales.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matches=arrAllObjSales[k];
            strBranchSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"branchSysName"]];
            strCouponStatus=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCouponApplied"]];
            strCouponStatusNonStan=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCouponApplied"]];
            
            strAccountNoGlobal=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountNo"]];
            
            NSString *strLeadStatus=[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
            strLeadStatusGlobal=[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
            strStageSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"stageSysName"]];
            
            if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
            {
                
                chkStatus=YES;
            }
            else
            {
                chkStatus=NO;
                
            }
            strStageSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"stageSysName"]];//BillingFirstName
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        }
    }
}


//...............
//Nilind 29 Dec

-(void)chekcBox
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request = [[NSFetchRequest alloc] init];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,@"0"];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSArray *arrAllObjStan;
    NSManagedObject *matchesStan;
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObjStan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSArray *sortedArrayOfString = [arrFinalIndexPath sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [(NSString *)obj1 compare:(NSString *)obj2 options:NSNumericSearch];
    }];
    NSLog(@"sortedArrayOfString1%@",sortedArrayOfString);
    
    
    if (arrAllObjStan.count==0)
    {
    }
    else
    {
        
        for (int j=0; j<arrAllObjStan.count; j++)
        {
            matchesStan=[arrAllObjStan objectAtIndex:j];
            
            
            /*for (int i=0; i>arrFinalIndexPath.count; i++)
             {
             if([[sortedArrayOfString objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%d",j]])
             {
             [matches setValue:@"true" forKey:@"isSold"];
             }
             else
             {
             [matches setValue:@"false" forKey:@"isSold"];
             }
             }*/
            for (int i=0; i<arrFinalIndexPath.count; i++)
            {
                if([[NSString stringWithFormat:@"%d",j] isEqualToString:[sortedArrayOfString objectAtIndex:i]])
                {
                    [matchesStan setValue:@"true" forKey:@"isSold"];
                    break;
                }
                else
                {
                    [matchesStan setValue:@"false" forKey:@"isSold"];
                }
            }
            
            
        }
        
        
    }
    
    [matchesStan setValue:[global modifyDate] forKey:@"modifiedDate"];
    
    [context save:&error1];
    
    [self fetchFromCoreDataNonStandard];
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Detemine if it's in editing mode
    
    if (chkStatus==YES)
    {
        if (_tblRecord.editing)
        {
            return UITableViewCellEditingStyleDelete;
        }
        
        return UITableViewCellEditingStyleNone;
    }
    else
    {
        return UITableViewCellEditingStyleDelete;
    }
}
//........
-(NSString *)fetchLead
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSString *str;
    if (arrAllObj12.count==0)
    {
    }
    else
    {
        
        NSManagedObject *matches12=arrAllObj12[0];
        str=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]];
        
    }
    return str;
    
}

- (IBAction)actionOnGlobalSync:(id)sender
{
    
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"Alert!":@"No Internet connection available"];
    }
    else
    {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:@"startLoader" object:nil];
        [self performSelector:@selector(methodSync) withObject:nil afterDelay:0.5];
        
    }}
-(void)methodSync
{
    GlobalSyncViewController *objGlobalSyncViewController=[[GlobalSyncViewController alloc]init];
    NSString *str=@"general";
    
    [objGlobalSyncViewController syncCall:str];
}
-(void)stopLoader
{
    [DejalBezelActivityView removeView];
}
//Nilind 20 Jan

-(void)fetchDepartmentName
{
    
    NSDictionary *dictSalesLeadMaster;
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    NSMutableArray *arrDept;
    NSMutableArray *arrName12,*arrSysName12;
    arrName12=[[NSMutableArray alloc]init];
    arrSysName12=[[NSMutableArray alloc]init];
    arrDept=[[NSMutableArray alloc]init];
    NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    for (int i=0;i<arrDeptName.count; i++)
    {
        NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
        NSArray *arry=[dictBranch valueForKey:@"Departments"];
        for (int j=0; j<arry.count; j++)
        {
            NSDictionary *dict=[arry objectAtIndex:j];
            
            [arrName12 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
            [arrSysName12 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
            
            
        }
        
    }
    dictForDepartment = [NSDictionary dictionaryWithObjects:arrName12 forKeys:arrSysName12];
}

//.......
//nilind 03 May
-(void)getFrequencySysNameFromName
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    NSMutableArray *arrName1,*arrSysName1,*arrYearlyOccurence;
    arrName1=[[NSMutableArray alloc]init];
    arrSysName1=[[NSMutableArray alloc]init];
    arrYearlyOccurence=[[NSMutableArray alloc]init];
    
    NSArray *arrFreq=[dictMasters valueForKey:@"Frequencies"];
    for (int i=0; i<arrFreq.count; i++)
    {
        NSDictionary *dict=[arrFreq objectAtIndex:i];
        [arrName1 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]];
        [arrSysName1 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
        [arrYearlyOccurence addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"YearlyOccurrence"]]];
        
        
    }
    
    dictFreqNameFromSysName=[NSDictionary dictionaryWithObjects:arrName1 forKeys:arrSysName1];
    dictYearlyOccurFromFreqSysName=[NSDictionary dictionaryWithObjects:arrYearlyOccurence forKeys:arrSysName1];
    
    
    NSLog(@"dictFreqNameFromSysName>>%@",dictFreqNameFromSysName);
    
}
#pragma mark- ********* TEXT FIELD DELEGATE **********
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==_txtApplyDiscount)
    {
    }
    else
    {
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
        numberToolbar.barStyle = UIBarStyleDefault;//UIBarStyleBlackTranslucent;//
        numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"DONE" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)]];
        [numberToolbar sizeToFit];
        textField.inputAccessoryView = numberToolbar;
    }
    /* for (int i=0; i<3; i++)
     {
     if (textField.tag==(10*i)+10)
     {
     NSLog(@"Text field %d",i);
     break;
     }
     }
     */
    
    return YES;
}
- (void)hideKeyboard
{
    [self.view endEditing:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //Temp Parameter Code
    if (textField==_txtApplyDiscount)
    {
        
    }
    else
    {
        CGPoint point = [textField.superview convertPoint:textField.frame.origin toView:_tblRecord];
        NSIndexPath * indexPathNew = [_tblRecord indexPathForRowAtPoint:point]; NSLog(@"Indexpath = %@", indexPathNew);
        int modTxtField = (textField.tag%2);
        bool chkForPara;
        chkForPara = NO;
        if((textField.tag >= 1000) && (textField.tag <2000))
         {
             modTxtField = (int)(textField.tag - 1000);
         }
         else if((textField.tag >= 2000) && (textField.tag <3000))
         {
             modTxtField = (int)(textField.tag - 2000);
         }
         else if((textField.tag >= 3000) && (textField.tag <4000))
         {
             modTxtField = (int)(textField.tag - 3000);
         }
         else if((textField.tag >= 4000) && (textField.tag <5000))
         {
             modTxtField = (int)(textField.tag - 4000);
         }
         else if((textField.tag >= 5000) && (textField.tag <6000))
         {
             modTxtField = (int)(textField.tag - 5000);
         }
         else if((textField.tag >= 6000) && (textField.tag <7000))
         {
             modTxtField = (int)(textField.tag - 6000);
         }
        
        int txtFieldNumber = (int)(textField.tag-modTxtField)/1000;
        
        BOOL ChkTextTag=NO;
        
        if (textField.tag >= 1000 && textField.tag < 7000)
        {
            if(arrSysName.count>0)
            {
                if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPathNew.row]]isEqualToString:@"1"])
                {
                    NSArray *arrOther=[arrAdditionalParamterDcs objectAtIndex:indexPathNew.row];//[self calculationForParamterBasedService:[arrSysName objectAtIndex:indexPathNew.row]];
                    
                    if (arrOther.count > 0)
                    {
                    
                        NSMutableDictionary *dictOther=[[arrOther objectAtIndex:modTxtField] mutableCopy];
                        
                        if(txtFieldNumber==2)
                        {
                            NSString *unitValue=textField.text;
                            if([unitValue isEqualToString:@"0"]||[unitValue isEqualToString:@""] || [unitValue floatValue] == 0)
                            {
                                //unitValue=@"1";
                                //[dictOther setValue:@"1" forKey:@"Unit"];
                                unitValue=@"0";
                                [dictOther setValue:@"0" forKey:@"Unit"];
                            }
                            else
                            {
                                [dictOther setValue:textField.text forKey:@"Unit"];
                            }
                            NSString *strInitialPricePara,*strMaintPricePara;
                            strInitialPricePara=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"InitialUnitPrice"]];
                            strMaintPricePara=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MaintUnitPrice"]];
                            
                            
                            
                            float calulateIntial,calulateMaint;
                            calulateIntial=[strInitialPricePara floatValue]*[unitValue floatValue];
                            calulateMaint=[strMaintPricePara floatValue]*[unitValue floatValue];
                            
                            
                            if(calulateIntial<[[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]]floatValue])
                            {
                                calulateIntial=[[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]]floatValue];
                            }
                            
                            if([unitValue isEqualToString:@"0"]||[unitValue isEqualToString:@""] || [unitValue floatValue] == 0)
                            {
                                calulateIntial = 0.0;
                            }
                            
                            
                            [dictOther setValue:[NSString stringWithFormat:@"%.5f",calulateIntial] forKey:@"FinalInitialUnitPrice"];
                            [dictOther setValue:[NSString stringWithFormat:@"%.5f",calulateMaint] forKey:@"FinalMaintUnitPrice"];
                            
                            ChkTextTag=YES;
                            
                        }
                        
                        else if (txtFieldNumber==5)
                        {
                            
                            /* NSString *strInitialPricePara;
                             strInitialPricePara=[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"InitialUnitPrice"]];
                             float calulateIntial;
                             calulateIntial=[strInitialPricePara floatValue];
                             
                             
                             if(calulateIntial<[[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]]floatValue])
                             {
                             calulateIntial=[[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"MinUnitPrice"]]floatValue];
                             textField.text=[NSString stringWithFormat:@"%.2f",calulateIntial];
                             }
                             */
                           // [dictOther setValue:textField.text forKey:@"FinalInitialUnitPrice"];
                            
                            if ([[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"Unit"]]floatValue] == 0)
                            {
                                [dictOther setValue:@"0.00" forKey:@"FinalInitialUnitPrice"];
                            }
                            else
                            {
                                
                                [dictOther setValue:textField.text forKey:@"FinalInitialUnitPrice"];
                            }
                            
                            ChkTextTag=YES;
                            
                            
                        }
                        
                        else if (txtFieldNumber==6)
                        {
                           // [dictOther setValue:textField.text forKey:@"FinalMaintUnitPrice"];
                            
                            if ([[NSString stringWithFormat:@"%@",[dictOther valueForKey:@"Unit"]]floatValue] == 0)
                            {
                                [dictOther setValue:@"0.00" forKey:@"FinalMaintUnitPrice"];
                            }
                            else
                            {
                                
                                [dictOther setValue:textField.text forKey:@"FinalMaintUnitPrice"];
                            }
                            ChkTextTag=YES;
                            
                        }
                     /*   NSMutableArray *arrUpadte=[[NSMutableArray alloc]init];
                        for (int i=0; i<arrOther.count; i++)
                        {
                            [arrUpadte addObject:[arrOther objectAtIndex:i]];
                        }
                        [arrUpadte replaceObjectAtIndex:modTxtField withObject:dictOther];
                        [self finalUpdatedParameterData:arrUpadte :indexPathNew.row ];
                        NSLog(@"%@",arrUpadte);
                        
                        //Parameter service renewalCalculation
                        float totalIntialPriceRenewal = 0.0;
                        for (int i=0;i<arrUpadte.count;i++)
                        {
                            NSDictionary *dictRenewal = [arrUpadte objectAtIndex:i];
                            totalIntialPriceRenewal = totalIntialPriceRenewal + [[NSString stringWithFormat:@"%@",[dictRenewal valueForKey:@"FinalInitialUnitPrice"]]floatValue];
                        }
                        totalIntialPriceRenewal = totalIntialPriceRenewal + [[NSString stringWithFormat:@"%@",[arrInitialPrice objectAtIndex:indexPathNew.row]]floatValue];
                        NSLog(@"%.05f",totalIntialPriceRenewal);
                        
                        [self updateRenewalServiceWithId:[arrSoldServiceStandardId objectAtIndex:indexPathNew.row] ServiceMasterId:[arrServiceId objectAtIndex:indexPathNew.row] RenewalAmount:[NSString stringWithFormat:@"%.5f",totalIntialPriceRenewal]];*/
                        
                        
                        //End
                        
                        //07 Aug 2020
                        
                        //[self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
                        //[self finalUpdatedOnSaveForTotalInitialAndMaintPrice];
                        //[self updateLeadAppliedDiscountOnUnitUpdate:[arrSoldServiceStandardId objectAtIndex:indexPathNew.row]];
                        if ([self chkAppliendCouponCountOnService:[arrSoldServiceStandardId objectAtIndex:indexPathNew.row]] == YES)
                            
                        {
                             UIAlertController *alert= [UIAlertController
                                                       alertControllerWithTitle:@"Alert"
                                                       message:@"Change in price may impact coupon discount so please remove and add the coupon again"
                                                       preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                        handler:^(UIAlertAction * action)
                                                  
                            {
                                
                                //[_tblBundle reloadData];
                            }];

                            [alert addAction:yes];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        else
                        {
                            //Paramter Price Code
                            
                            
                            NSMutableArray *arrUpadte=[[NSMutableArray alloc]init];
                            for (int i=0; i<arrOther.count; i++)
                            {
                                [arrUpadte addObject:[arrOther objectAtIndex:i]];
                            }
                            [arrUpadte replaceObjectAtIndex:modTxtField withObject:dictOther];
                            [self finalUpdatedParameterData:arrUpadte :indexPathNew.row ];
                            NSLog(@"%@",arrUpadte);
                            
                            //End
                            
                            //Parameter service renewalCalculation
                            
                            float totalIntialPriceRenewal = 0.0;
                            
                            for (int i=0;i<arrUpadte.count;i++)
                            {
                                NSDictionary *dictRenewal = [arrUpadte objectAtIndex:i];
                                totalIntialPriceRenewal = totalIntialPriceRenewal + [[NSString stringWithFormat:@"%@",[dictRenewal valueForKey:@"FinalInitialUnitPrice"]]floatValue];
                            }
                            
                            totalIntialPriceRenewal = totalIntialPriceRenewal + [[NSString stringWithFormat:@"%@",[arrInitialPrice objectAtIndex:indexPathNew.row]]floatValue];
                            
                            
                            NSLog(@"%.05f",totalIntialPriceRenewal);
                            
                            
                            [self updateRenewalServiceWithId:[arrSoldServiceStandardId objectAtIndex:indexPathNew.row] ServiceMasterId:[arrServiceId objectAtIndex:indexPathNew.row] RenewalAmount:[NSString stringWithFormat:@"%.5f",totalIntialPriceRenewal]];
                            
                            
                            
                            //End
                            
                            
                            
                            [self finalUpdatedOnSaveForTotalInitialAndMaintPrice];
                            [self updateLeadAppliedDiscountOnUnitUpdate:[arrSoldServiceStandardId objectAtIndex:indexPathNew.row]];
                            
                            [self callAtDidEnd];
                            
                        }
                        
                    }
                }
                
            }
        }

#pragma mark- Dynamic Text Field

        
        NSInteger row = textField.tag;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        SalesAutomationServiceTableViewCell *tappedCell = (SalesAutomationServiceTableViewCell *)[_tblRecord cellForRowAtIndexPath:indexPath];
        strBundleIdForTextField=@"0";
        
        if(textField==tappedCell.txtDiscountPercent)
        {
            NSString *strDiscoutnPer;
            NSString *strInitialPrice = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            strDiscoutnPer = [tappedCell.txtDiscountPercent.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            strDiscoutnPer = [strDiscoutnPer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            double initial,discountValue;
            initial=[strInitialPrice doubleValue];
            discountValue=(initial*[strDiscoutnPer doubleValue])/100;
            
            [arrDiscount replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",discountValue]];
            
            [arrDiscountPer replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%@",tappedCell.txtDiscountPercent.text]];
            
            // [_tblRecord reloadData];
            
            [self callAtDidEnd];
            
            
        }
        else if(textField==tappedCell.txtStanDiscount)
        {
            NSString *strDiscoutn;
            NSString *strInitialPrice = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            strDiscoutn = [tappedCell.txtStanDiscount.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            strDiscoutn = [strDiscoutn stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            double initial,discountPer;
            initial=[strInitialPrice doubleValue];
            discountPer=([strDiscoutn doubleValue]*100)/initial;
            
            [arrDiscountPer replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",discountPer]];
            [arrDiscount replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%@",tappedCell.txtStanDiscount.text]];
            
            //[_tblRecord reloadData];
            
            [self callAtDidEnd];
            
        }
        else if (textField==tappedCell.txtUnit)
        {
            //[_txtInitialPrice.text doubleValue]*[_txtQuantity.text doubleValue];
            NSString *strUnit=tappedCell.txtUnit.text;
           /* float unit = [[NSString stringWithFormat:@"%@",strUnit]floatValue];
            
            if([strUnit isEqualToString:@"0"]||[strUnit isEqualToString:@""] || unit == 0)
            {
                strUnit=@"0";
            }
            
            
            
            double finalInitialPrice,finalMaintPrice;
            finalInitialPrice=[strUnit floatValue]*[[arrInitialPrice objectAtIndex:row]doubleValue];
            finalMaintPrice=[strUnit floatValue]*[[arrMaintenancePrice objectAtIndex:row]doubleValue];
            
            
            [arrUnit replaceObjectAtIndex:row withObject:strUnit];
            [arrFinalInitialPrice replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",finalInitialPrice]];
            [arrFinalMaintPrice replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",finalMaintPrice]];
            
            [self updateRenewalServiceWithId:[arrSoldServiceStandardId objectAtIndex:row] ServiceMasterId:[arrServiceId objectAtIndex:row] RenewalAmount:[NSString stringWithFormat:@"%.5f",finalInitialPrice]];*/

            //[_tblRecord reloadData];
            
            // 05 Aug 2020
            
            //[self updateLeadAppliedDiscountOnUnitUpdate:[arrSoldServiceStandardId objectAtIndex:row]];
            
            //[self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
            
            //[self updateLeadAppliedDiscountOnUnitUpdate:[arrSoldServiceStandardId objectAtIndex:row]];
            
            if ([self chkAppliendCouponCountOnService:[arrSoldServiceStandardId objectAtIndex:indexPathNew.row]] == YES)
                
            {
                 UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Change in price may impact coupon discount so please remove and add the coupon again"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      
                {
                    
                    
                }];

                [alert addAction:yes];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {

                //Stan Service Unit Code
               float unit = [[NSString stringWithFormat:@"%@",strUnit]floatValue];
                
                if([strUnit isEqualToString:@"0"]||[strUnit isEqualToString:@""] || unit == 0)
                {
                    strUnit=@"0";
                }
                
                
                double finalInitialPrice,finalMaintPrice;
                finalInitialPrice=[strUnit floatValue]*[[arrInitialPrice objectAtIndex:row]doubleValue];
                finalMaintPrice=[strUnit floatValue]*[[arrMaintenancePrice objectAtIndex:row]doubleValue];
                
                
                [arrUnit replaceObjectAtIndex:row withObject:strUnit];
                [arrFinalInitialPrice replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",finalInitialPrice]];
                [arrFinalMaintPrice replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",finalMaintPrice]];
                
                [self updateRenewalServiceWithId:[arrSoldServiceStandardId objectAtIndex:row] ServiceMasterId:[arrServiceId objectAtIndex:row] RenewalAmount:[NSString stringWithFormat:@"%.5f",finalInitialPrice]];
                //
                [self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
                
                [self updateLeadAppliedDiscountOnUnitUpdate:[arrSoldServiceStandardId objectAtIndex:row]];
                
                [self callAtDidEnd];
            }
            
            
        }
        else
        {
            NSInteger row = textField.tag;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
            RenewalServiceTableViewCell *tappedCell = (RenewalServiceTableViewCell *)[_tblRenewalService cellForRowAtIndexPath:indexPath];
            
            if (textField == tappedCell.txtRenewalPrice)
            {
                NSLog(@"text field editing %@",textField.text);
                
                NSString *strRenewalServiceId;
                NSManagedObject *matchesTemp = [arrAllObjRenewal objectAtIndex:indexPath.row];
                
                strRenewalServiceId = [NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"soldServiceStandardId"]];
                
                [self updateRenewaData:strRenewalServiceId RenewalAmount:tappedCell.txtRenewalPrice.text RenewalDescription:@"" UpdateType:@"amount"];
            }
            else
            {
                if (ChkTextTag==YES)
                {
                    
                }
                else
                {
                    NSInteger rowBundle; //Bundle Table
                    CGPoint buttonPosition = [textField convertPoint:CGPointZero toView:self.tblBundle];
                    NSIndexPath *indexPathBundle = [self.tblBundle indexPathForRowAtPoint:buttonPosition];
                    NSLog(@"Section=%ld row=%ld",(long)indexPathBundle.section,(long)indexPathBundle.row);
                    strBundleIdForTextField=[arrBundleRow objectAtIndex:indexPathBundle.section];
                    rowBundle=indexPathBundle.row;
                    [self fetchFromCoreDataStandardForBundle:strBundleIdForTextField];
                    SalesAutomationServiceTableViewCell *tappedCellBundle = (SalesAutomationServiceTableViewCell *)[_tblBundle cellForRowAtIndexPath:indexPathBundle];
                    if (textField==tappedCellBundle.txtStanDiscount)
                    {
                        NSString *strDiscoutn;
                        NSString *strInitialPrice = [tappedCellBundle.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                        strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        strDiscoutn = [tappedCellBundle.txtStanDiscount.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                        strDiscoutn = [strDiscoutn stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        double initial,discountPer;
                        initial=[strInitialPrice doubleValue];
                        discountPer=([strDiscoutn doubleValue]*100)/initial;
                        
                        [arrDiscountPerBundle replaceObjectAtIndex:rowBundle withObject:[NSString stringWithFormat:@"%.5f",discountPer]];
                        [arrDiscountBundle replaceObjectAtIndex:rowBundle withObject:[NSString stringWithFormat:@"%@",tappedCellBundle.txtStanDiscount.text]];
                        //[self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
                        
                         [self callAtDidEnd];
                        
                    }
                    else if (textField==tappedCellBundle.txtDiscountPercent)
                    {
                        NSString *strDiscoutnPer;
                        NSString *strInitialPrice = [tappedCellBundle.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                        strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        strDiscoutnPer = [tappedCellBundle.txtDiscountPercent.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                        strDiscoutnPer = [strDiscoutnPer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        double initial,discountValue;
                        initial=[strInitialPrice doubleValue];
                        discountValue=(initial*[strDiscoutnPer doubleValue])/100;
                        
                        [arrDiscountBundle replaceObjectAtIndex:rowBundle withObject:[NSString stringWithFormat:@"%.5f",discountValue]];
                        
                        [arrDiscountPerBundle replaceObjectAtIndex:rowBundle withObject:[NSString stringWithFormat:@"%@",tappedCellBundle.txtDiscountPercent.text]];
                        //[self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
                         [self callAtDidEnd];
                        
                    }
                    else if (textField==tappedCellBundle.txtUnit)
                    {
                        NSString *strUnit=tappedCellBundle.txtUnit.text;
                       /* float unit = [[NSString stringWithFormat:@"%@",strUnit]floatValue];
                        
                        if([strUnit isEqualToString:@"0"]||[strUnit isEqualToString:@""] || unit == 0)
                        {
                            strUnit=@"0";
                        }
                        
                        
                        double finalInitialPrice,finalMaintPrice;
                        finalInitialPrice=[strUnit floatValue]*[[arrInitialPriceBundle objectAtIndex:rowBundle]doubleValue];
                        finalMaintPrice=[strUnit floatValue]*[[arrMaintenancePriceBundle objectAtIndex:rowBundle]doubleValue];
                        
                        [arrUnitBundle replaceObjectAtIndex:rowBundle withObject:strUnit];
                        [arrFinalInitialPriceBundle replaceObjectAtIndex:rowBundle withObject:[NSString stringWithFormat:@"%.5f",finalInitialPrice]];
                        [arrFinalMaintPriceBundle replaceObjectAtIndex:rowBundle withObject:[NSString stringWithFormat:@"%.5f",finalMaintPrice]];
                        
                        // [self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
                        [self updateRenewalServiceWithId:[arrSoldServiceIdBundleNew objectAtIndex:row] ServiceMasterId:[arrServiceIdBundle objectAtIndex:row] RenewalAmount:[NSString stringWithFormat:@"%.5f",finalInitialPrice]];*/
                        
                        //07 Aug 2020
                        //[self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
                        
                        //[self updateLeadAppliedDiscountOnUnitUpdate:[arrSoldServiceIdBundleNew objectAtIndex:row]];
                        
                        if ([self chkAppliendCouponCountOnService:[arrSoldServiceIdBundleNew objectAtIndex:indexPathNew.row]] == YES)
                            
                        {
                            UIAlertController *alert= [UIAlertController
                                                       alertControllerWithTitle:@"Alert"
                                                       message:@"Change in price may impact coupon discount so please remove and add the coupon again"
                                                       preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                        handler:^(UIAlertAction * action)
                                                  
                                                 
                                                  
                            {
                                //[_tblBundle reloadData];
                                
                            }];
                            
                            [alert addAction:yes];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        else
                        {
                            
                            //Bundle Unit Code
                            
                            float unit = [[NSString stringWithFormat:@"%@",strUnit]floatValue];
                             
                             if([strUnit isEqualToString:@"0"]||[strUnit isEqualToString:@""] || unit == 0)
                             {
                                 strUnit=@"0";
                             }
                             
                             double finalInitialPrice,finalMaintPrice;
                             finalInitialPrice=[strUnit floatValue]*[[arrInitialPriceBundle objectAtIndex:rowBundle]doubleValue];
                             finalMaintPrice=[strUnit floatValue]*[[arrMaintenancePriceBundle objectAtIndex:rowBundle]doubleValue];
                             
                            [arrUnitBundle replaceObjectAtIndex:rowBundle withObject:strUnit];
                             [arrFinalInitialPriceBundle replaceObjectAtIndex:rowBundle withObject:[NSString stringWithFormat:@"%.5f",finalInitialPrice]];
                             [arrFinalMaintPriceBundle replaceObjectAtIndex:rowBundle withObject:[NSString stringWithFormat:@"%.5f",finalMaintPrice]];
                             
                             
                             [self updateRenewalServiceWithId:[arrSoldServiceIdBundleNew objectAtIndex:row] ServiceMasterId:[arrServiceIdBundle objectAtIndex:row] RenewalAmount:[NSString stringWithFormat:@"%.5f",finalInitialPrice]];
                            
                            //
                            
                            [self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
                            
                            [self updateLeadAppliedDiscountOnUnitUpdate:[arrSoldServiceIdBundleNew objectAtIndex:row]];
                            
                            
                            [self callAtDidEnd];
                        }
                        
                    }
                    else //Non Standard
                    {
                        NSInteger row = textField.tag;
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
                        SalesAutomationTableViewCellSecond *tappedCell = (SalesAutomationTableViewCellSecond *)[_tblNonStandardService cellForRowAtIndexPath:indexPath];
                        
                        if (textField==tappedCell.txtDiscountPercent)
                        {
                            NSString *strDiscoutnPer;
                            NSString *strInitialPrice = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                            strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            strDiscoutnPer = [tappedCell.txtDiscountPercent.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                            strDiscoutnPer = [strDiscoutnPer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            double initial,discountValue;
                            initial=[strInitialPrice doubleValue];
                            discountValue=(initial*[strDiscoutnPer doubleValue])/100;
                            
                            [arrNonStanDiscount replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",discountValue]];
                            
                            [arrDiscountPerNonStan replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%@",tappedCell.txtDiscountPercent.text]];
                        }
                        else if (textField==tappedCell.txtDiscountNonStan)
                        {
                            NSString *strDiscoutn;
                            NSString *strInitialPrice = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                            strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            strDiscoutn = [tappedCell.txtDiscountNonStan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                            strDiscoutn = [strDiscoutn stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            double initial,discountPer;
                            initial=[strInitialPrice doubleValue];
                            discountPer=([strDiscoutn doubleValue]*100)/initial;
                            
                            [arrDiscountPerNonStan replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",discountPer]];
                            [arrNonStanDiscount replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%@",tappedCell.txtDiscountNonStan.text]];
                            
                             [self callAtDidEnd];
                        }
                    }
                    
                }
            }
            
        }
        [textField resignFirstResponder];
        
        [self finalUpdatedTableDataWithDiscountNonStan];
        //[self finalUpdatedTableDataWithDiscount];
        //Temp Comment
        
       // [self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
       // [self finalUpdatedOnSaveForTotalInitialAndMaintPrice];
        
        //End
        [_tblRecord reloadData];
        [_tblBundle reloadData];
        [_tblNonStandardService reloadData];
    }
    
}
-(void)callAtDidEnd
{
    [self finalUpdatedTableDataWithBundleDiscount:strBundleIdForTextField];
     [self finalUpdatedOnSaveForTotalInitialAndMaintPrice];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField==_txtApplyDiscount)
    {
        return YES;
    }
    else
    {
        
        NSInteger row = textField.tag;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        SalesAutomationServiceTableViewCell *tappedCell = (SalesAutomationServiceTableViewCell *)[_tblRecord cellForRowAtIndexPath:indexPath];
        
        if (textField==tappedCell.txtStanDiscount)
        {
            
            if([tappedCell.txtStanDiscount.text isEqualToString:@"TBD"])
            {
                return NO;
            }
            else
            {
                double initialPrice,discount;
                if(textField==tappedCell.txtStanDiscount)
                {
                    NSString *strDiscount = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                    strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    initialPrice=[strDiscount doubleValue];
                    discount=[tappedCell.txtStanDiscount.text doubleValue];
                    
                    NSString *str1= tappedCell.txtStanDiscount.text;
                    string=[NSString stringWithFormat:@"%@%@",str1,string];
                    discount=[string doubleValue];
                    
                }
                if (discount>initialPrice)
                {
                    
                    if([string isEqualToString:@""])
                    {
                        return YES;
                    }
                    else
                    {
                        [global AlertMethod:@"Alert!" :@"Discount Can't be greater than Initial Price"];
                        return NO;
                    }
                }
                else
                {
                    [arrDiscount replaceObjectAtIndex:row withObject:string];
                    return YES;
                }
            }
        }
        else if (textField==tappedCell.txtDiscountPercent)
        {
            if([tappedCell.txtDiscountPercent.text isEqualToString:@"TBD"])
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        else if (textField==tappedCell.txtUnit)
        {
            /*BOOL isNuberOnly=[global isNumberOnly:string :range :2 :(int)textField.text.length :@"0123456789"];
             
             return isNuberOnly;*/
            
            BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :tappedCell.txtUnit.text];
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
           /* if([newString isEqualToString:@"0"])
            {
                return NO;
            }
            else
            {
                
                NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                
                if (isNuberOnly)
                {
                    if ([arrayOfString count] ==2 )
                    {
                        NSString *str=[arrayOfString objectAtIndex:1];
                        if (str.length>2)
                        {
                            return NO;
                            
                        }
                    }
                    return YES;
                }
                else
                {
                    return NO;
                }
            }*/
            NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
            
            if (isNuberOnly)
            {
                if ([arrayOfString count] ==2 )
                {
                    NSString *str=[arrayOfString objectAtIndex:1];
                    if (str.length>2)
                    {
                        return NO;
                        
                    }
                }
                return YES;
            }
            else
            {
                return NO;
            }
            
            
        }
        //Nilind 08 June
        else
        {
#pragma mark- Dynamic Text Field
            //Temp Parameter Code
            CGPoint point = [textField.superview convertPoint:textField.frame.origin toView:_tblRecord];
            NSIndexPath * indexPathNew = [_tblRecord indexPathForRowAtPoint:point]; NSLog(@"Indexpath = %@", indexPathNew);
            int modTxtField = (textField.tag%2);
            
            if((textField.tag >= 1000) && (textField.tag <2000))
             {
                 modTxtField = (int)(textField.tag - 1000);
             }
             else if((textField.tag >= 2000) && (textField.tag <3000))
             {
                 modTxtField = (int)(textField.tag - 2000);
             }
             else if((textField.tag >= 3000) && (textField.tag <4000))
             {
                 modTxtField = (int)(textField.tag - 3000);
             }
             else if((textField.tag >= 4000) && (textField.tag <5000))
             {
                 modTxtField = (int)(textField.tag - 4000);
             }
             else if((textField.tag >= 5000) && (textField.tag <6000))
             {
                 modTxtField = (int)(textField.tag - 5000);
             }
             else if((textField.tag >= 6000) && (textField.tag <7000))
             {
                 modTxtField = (int)(textField.tag - 6000);
             }
            
            
            int txtFieldNumber = (int)(textField.tag-modTxtField)/1000;
            
            BOOL ChkTextTag=NO;
            if (textField.tag >= 1000 && textField.tag < 7000)
            {
                if (arrSysName.count>0)
                {
                    if ([[dictServiceParmaterBasedStatus valueForKey:[arrSysName objectAtIndex:indexPathNew.row]]isEqualToString:@"1"])
                    {
                        
                        if(txtFieldNumber==2) //For Unit
                        {
                            ChkTextTag=YES;
                            NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@".0123456789"];
                            NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
                            BOOL isNumbersOnly = ([numbersOnly isSupersetOfSet:characterSetFromTextField]);
                            if (!isNumbersOnly)
                            {
                                return NO;
                            }
                            if ([string rangeOfString:@"."].location !=NSNotFound && [textField.text rangeOfString:string].location !=NSNotFound)
                            {
                                
                                return NO;
                            }
                            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                            
                           /* if([newString isEqualToString:@"0"])
                            {
                                return NO;
                            }
                            
                            else
                            {
                                
                                NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                                
                                if (isNumbersOnly)
                                {
                                    if ([arrayOfString count] ==2 )
                                    {
                                        NSString *str=[arrayOfString objectAtIndex:1];
                                        if (str.length>2)
                                        {
                                            return NO;
                                            
                                        }
                                    }
                                    
                                    return YES;
                                    
                                }
                                
                            }*/
                            NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                            
                            if (isNumbersOnly)
                            {
                                if ([arrayOfString count] ==2 )
                                {
                                    NSString *str=[arrayOfString objectAtIndex:1];
                                    if (str.length>2)
                                    {
                                        return NO;
                                        
                                    }
                                }
                                
                                return YES;
                                
                            }
                            
                            return YES;
                            
                            
                        }
                        
                        else if (txtFieldNumber==5)
                        {
                            ChkTextTag=YES;
                            NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@".0123456789"];
                            NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
                            BOOL isNumbersOnly = ([numbersOnly isSupersetOfSet:characterSetFromTextField]);
                            if (!isNumbersOnly)
                            {
                                return NO;
                            }
                            if ([string rangeOfString:@"."].location !=NSNotFound && [textField.text rangeOfString:string].location !=NSNotFound)
                            {
                                
                                return NO;
                            }
                            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                            NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                            
                            if (isNumbersOnly)
                            {
                                if ([arrayOfString count] ==2 )
                                {
                                    NSString *str=[arrayOfString objectAtIndex:1];
                                    if (str.length>2)
                                    {
                                        return NO;
                                        
                                    }
                                }
                                
                                return YES;
                                
                            }
                            
                            return YES;
                            /*BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :textField.text];
                             NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                             NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                             
                             if (isNuberOnly)
                             {
                             if ([arrayOfString count] ==2 )
                             {
                             NSString *str=[arrayOfString objectAtIndex:1];
                             if (str.length>2)
                             {
                             return NO;
                             
                             }
                             }
                             
                             return YES;
                             
                             }
                             else
                             {
                             return NO;
                             }*/
                        }
                        
                        else if (txtFieldNumber==6)
                        {
                            ChkTextTag=YES;
                            NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@".0123456789"];
                            NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
                            BOOL isNumbersOnly = ([numbersOnly isSupersetOfSet:characterSetFromTextField]);
                            if (!isNumbersOnly)
                            {
                                return NO;
                            }
                            if ([string rangeOfString:@"."].location !=NSNotFound && [textField.text rangeOfString:string].location !=NSNotFound)
                            {
                                
                                return NO;
                            }
                            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                            NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                            
                            if (isNumbersOnly)
                            {
                                if ([arrayOfString count] ==2 )
                                {
                                    NSString *str=[arrayOfString objectAtIndex:1];
                                    if (str.length>2)
                                    {
                                        return NO;
                                        
                                    }
                                }
                                
                                return YES;
                                
                            }
                            
                            return YES;
                            /*BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :textField.text];
                             NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                             NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                             
                             if (isNuberOnly)
                             {
                             if ([arrayOfString count] ==2 )
                             {
                             NSString *str=[arrayOfString objectAtIndex:1];
                             if (str.length>2)
                             {
                             return NO;
                             
                             }
                             }
                             
                             return YES;
                             
                             }
                             else
                             {
                             return NO;
                             }*/
                        }
                        
                        
                        //End
                    }
                }
            }

            //End
            if (ChkTextTag==YES)
            {
                return YES;
            }
            else
            {
                NSInteger row = textField.tag;
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
                RenewalServiceTableViewCell *tappedCell = (RenewalServiceTableViewCell *)[_tblRenewalService cellForRowAtIndexPath:indexPath];
                
                if (textField == tappedCell.txtRenewalPrice) // Renewal
                {
                    
                    BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :tappedCell.txtRenewalPrice.text];
                    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                    if([newString isEqualToString:@"0"])
                    {
                        return NO;
                    }
                    else
                    {
                        NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                        
                        if (isNuberOnly)
                        {
                            if ([arrayOfString count] ==2 )
                            {
                                NSString *str=[arrayOfString objectAtIndex:1];
                                if (str.length>2)
                                {
                                    return NO;
                                    
                                }
                            }
                            return YES;
                        }
                        else
                        {
                            return NO;
                        }
                    }
                }
                else
                {
                    NSInteger rowBundle; //Bundle Table
                    CGPoint buttonPosition = [textField convertPoint:CGPointZero toView:self.tblBundle];
                    NSIndexPath *indexPathBundle = [self.tblBundle indexPathForRowAtPoint:buttonPosition];
                    NSLog(@"Section=%ld row=%ld",(long)indexPathBundle.section,(long)indexPathBundle.row);
                    strBundleIdForTextField=[arrBundleRow objectAtIndex:indexPathBundle.section];
                    rowBundle=indexPathBundle.row;
                    [self fetchFromCoreDataStandardForBundle:strBundleIdForTextField];
                    SalesAutomationServiceTableViewCell *tappedCellBundle = (SalesAutomationServiceTableViewCell *)[_tblBundle cellForRowAtIndexPath:indexPathBundle];
                    if (textField==tappedCellBundle.txtStanDiscount)
                    {
                        if([tappedCellBundle.txtStanDiscount.text isEqualToString:@"TBD"])
                        {
                            return NO;
                        }
                        else
                        {
                            double initialPrice,discount;
                            if(textField==tappedCellBundle.txtStanDiscount)
                            {
                                NSString *strDiscount = [tappedCellBundle.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                initialPrice=[strDiscount doubleValue];
                                discount=[tappedCellBundle.txtStanDiscount.text doubleValue];
                                
                                NSString *str1= tappedCellBundle.txtStanDiscount.text;
                                string=[NSString stringWithFormat:@"%@%@",str1,string];
                                discount=[string doubleValue];
                                
                            }
                            if (discount>initialPrice)
                            {
                                
                                if([string isEqualToString:@""])
                                {
                                    return YES;
                                }
                                else
                                {
                                    [global AlertMethod:@"Alert!" :@"Discount Can't be greater than Initial Price"];
                                    return NO;
                                }
                            }
                            else
                            {
                                [arrDiscountBundle replaceObjectAtIndex:row withObject:string];
                                return YES;
                            }
                        }
                        
                    }
                    else if (textField==tappedCellBundle.txtDiscountPercent)
                    {
                        if([tappedCellBundle.txtDiscountPercent.text isEqualToString:@"TBD"])
                        {
                            return NO;
                        }
                        else
                        {
                            return YES;
                        }
                    }
                    else if (textField==tappedCellBundle.txtUnit)
                    {
                        // return YES;
                        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :tappedCellBundle.txtUnit.text];
                        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                       /* if([newString isEqualToString:@"0"])
                        {
                            return NO;
                            
                        }
                        else
                        {
                            NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                            
                            if (isNuberOnly)
                            {
                                if ([arrayOfString count] ==2 )
                                {
                                    NSString *str=[arrayOfString objectAtIndex:1];
                                    if (str.length>2)
                                    {
                                        return NO;
                                        
                                    }
                                }
                                return YES;
                            }
                            else
                            {
                                return NO;
                            }
                        }*/
                        NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
                        
                        if (isNuberOnly)
                        {
                            if ([arrayOfString count] ==2 )
                            {
                                NSString *str=[arrayOfString objectAtIndex:1];
                                if (str.length>2)
                                {
                                    return NO;
                                    
                                }
                            }
                            return YES;
                        }
                        else
                        {
                            return NO;
                        }
                        
                    }
                    else //Non Standard
                    {
                        NSInteger row = textField.tag;
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
                        SalesAutomationTableViewCellSecond *tappedCell = (SalesAutomationTableViewCellSecond *)[_tblNonStandardService cellForRowAtIndexPath:indexPath];
                        
                        
                        if (textField==tappedCell.txtDiscountNonStan)
                        {
                            if([tappedCell.txtDiscountNonStan.text isEqualToString:@"TBD"])
                            {
                                return NO;
                            }
                            else
                            {
                                double initialPrice,discount;
                                if(textField==tappedCell.txtDiscountNonStan)
                                {
                                    NSString *strDiscount = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                                    strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                    initialPrice=[strDiscount doubleValue];
                                    discount=[tappedCell.txtDiscountNonStan.text doubleValue];
                                    
                                    NSString *str1= tappedCell.txtDiscountNonStan.text;
                                    string=[NSString stringWithFormat:@"%@%@",str1,string];
                                    discount=[string doubleValue];
                                    
                                }
                                if (discount>initialPrice)
                                {
                                    
                                    if([string isEqualToString:@""])
                                    {
                                        return YES;
                                    }
                                    else
                                    {
                                        [global AlertMethod:@"Alert!" :@"Discount Can't be greater than Initial Price"];
                                        return NO;
                                    }
                                }
                                else
                                {
                                    [arrNonStanDiscount replaceObjectAtIndex:row withObject:string];
                                    return YES;
                                }
                            }
                            
                        }
                        else
                        {
                            return YES;
                        }
                        
                    }
                }
            }
        }
        
        //End
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_txtApplyDiscount)
    {
        [_txtApplyDiscount resignFirstResponder];
        return YES;
    }
    else
    {
        
        NSInteger row = textField.tag;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        SalesAutomationServiceTableViewCell *tappedCell = (SalesAutomationServiceTableViewCell *)[_tblRecord cellForRowAtIndexPath:indexPath];
        if(textField==tappedCell.txtDiscountPercent)
        {
            NSString *strDiscoutnPer;
            NSString *strInitialPrice = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            strDiscoutnPer = [tappedCell.txtDiscountPercent.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            strDiscoutnPer = [strDiscoutnPer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            double initial,discountValue;
            initial=[strInitialPrice doubleValue];
            discountValue=(initial*[strDiscoutnPer doubleValue])/100;
            
            [arrDiscount replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",discountValue]];
            
            [arrDiscountPer replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%@",tappedCell.txtDiscountPercent.text]];
            
            // [_tblRecord reloadData];
            
            
        }
        else if(textField==tappedCell.txtStanDiscount)
        {
            NSString *strDiscoutn;
            NSString *strInitialPrice = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            strDiscoutn = [tappedCell.txtStanDiscount.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            strDiscoutn = [strDiscoutn stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            double initial,discountPer;
            initial=[strInitialPrice doubleValue];
            discountPer=([strDiscoutn doubleValue]*100)/initial;
            
            [arrDiscountPer replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",discountPer]];
            [arrDiscount replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%@",tappedCell.txtStanDiscount.text]];
            
            //[_tblRecord reloadData];
            
        }
        else if (textField==tappedCell.txtUnit)
        {
            //[_txtInitialPrice.text doubleValue]*[_txtQuantity.text doubleValue];
            NSString *strUnit=tappedCell.txtUnit.text;
            double finalInitialPrice;
            finalInitialPrice=[strUnit floatValue]*[[arrInitialPrice objectAtIndex:row]doubleValue];
            
            [arrUnit replaceObjectAtIndex:row withObject:strUnit];
            [arrFinalInitialPrice replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",finalInitialPrice]];
            
            //[_tblRecord reloadData];
        }
        else //Non Standard
        {
            NSInteger row = textField.tag;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
            SalesAutomationTableViewCellSecond *tappedCell = (SalesAutomationTableViewCellSecond *)[_tblNonStandardService cellForRowAtIndexPath:indexPath];
            
            if (textField==tappedCell.txtDiscountPercent)
            {
                NSString *strDiscoutnPer;
                NSString *strInitialPrice = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                strDiscoutnPer = [tappedCell.txtDiscountPercent.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscoutnPer = [strDiscoutnPer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                double initial,discountValue;
                initial=[strInitialPrice doubleValue];
                discountValue=(initial*[strDiscoutnPer doubleValue])/100;
                
                [arrNonStanDiscount replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",discountValue]];
                
                [arrDiscountPerNonStan replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%@",tappedCell.txtDiscountPercent.text]];
            }
            else if (textField==tappedCell.txtDiscountNonStan)
            {
                NSString *strDiscoutn;
                NSString *strInitialPrice = [tappedCell.lblInitialPriceValue.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strInitialPrice = [strInitialPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                strDiscoutn = [tappedCell.txtDiscountNonStan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscoutn = [strDiscoutn stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                double initial,discountPer;
                initial=[strInitialPrice doubleValue];
                discountPer=([strDiscoutn doubleValue]*100)/initial;
                
                [arrDiscountPerNonStan replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%.5f",discountPer]];
                [arrNonStanDiscount replaceObjectAtIndex:row withObject:[NSString stringWithFormat:@"%@",tappedCell.txtDiscountNonStan.text]];
            }
        }
        
        [textField resignFirstResponder];
        
        
        
        [self finalUpdatedTableDataWithDiscountNonStan];
        [self finalUpdatedTableDataWithDiscount];
        [_tblRecord reloadData];
        [_tblNonStandardService reloadData];
        
        return YES;
    }
}
//End
//Nilind 06 June

- (IBAction)actionOnChkBoxDiscount:(id)sender
{
    isCheckBoxClickFirstTime=YES;
    isEditedInSalesAuto=YES;
    if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        isChkBoxEnable=NO;
        [_imgChkBoxDiscount setImage:[UIImage imageNamed:@"check_box_1.png"]];
    }
    else
    {
        isChkBoxEnable=YES;
        [_imgChkBoxDiscount setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    //[_tblRecord reloadData];
    
    //Nilind
    
    isCheckBoxClickFirstTimeNonStan=YES;
    isEditedInSalesAuto=YES;
    if ([_imgChkBoxDiscountNonStan.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        isChkBoxEnableNonStan=NO;
        [_imgChkBoxDiscountNonStan setImage:[UIImage imageNamed:@"check_box_1.png"]];
    }
    else
    {
        isChkBoxEnableNonStan=YES;
        [_imgChkBoxDiscountNonStan setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    //[_tblNonStandardService reloadData];
    [self finalUpdatedTableDataWithDiscount];
    [self finalUpdatedTableDataWithDiscountNonStan];
    [self fetchFromCoreDataStandard];
    [_tblRecord reloadData];
    [_tblBundle reloadData];
    [_tblNonStandardService reloadData];
    
    
}
- (IBAction)actionOnDiscountCheckBox:(id)sender
{
}
-(void)finalUpdatedTableDataWithDiscount
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matchesDiscoutUpdate=arrAllObj[i];
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                
                /*if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
                 {
                 
                 //[matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
                 //[matchesDiscoutUpdate setValue:[arrDiscountPer objectAtIndex:i] forKey:@"discountPercentage"];
                 }
                 else
                 {
                 [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
                 [matchesDiscoutUpdate setValue:@"0" forKey:@"discountPercentage"];
                 }
                 // [matchesDiscoutUpdate setValue:[arrFinalInitialPrice objectAtIndex:i] forKey:@"finalUnitBasedInitialPrice"];
                 //[matchesDiscoutUpdate setValue:[arrUnit objectAtIndex:i] forKey:@"unit"];*/
                [matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
                [matchesDiscoutUpdate setValue:[arrDiscountPer objectAtIndex:i] forKey:@"discountPercentage"];
            }
            else  //For Bundle
            {
                
                /*if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
                 {
                 
                 // [matchesDiscoutUpdate setValue:[arrDiscountBundle objectAtIndex:i] forKey:@"discount"];
                 //[matchesDiscoutUpdate setValue:[arrDiscountPerBundle objectAtIndex:i] forKey:@"discountPercentage"];
                 }
                 else
                 {
                 [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
                 [matchesDiscoutUpdate setValue:@"0" forKey:@"discountPercentage"];
                 }*/
                [matchesDiscoutUpdate setValue:[arrDiscountBundle objectAtIndex:i] forKey:@"discount"];
                [matchesDiscoutUpdate setValue:[arrDiscountPerBundle objectAtIndex:i] forKey:@"discountPercentage"];
            }
        }
        [context save:&error1];
    }
    
}
-(void)finalUpdatedTableDataWithBundleDiscount:(NSString *)strBundleIdForTextField1
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,strBundleIdForTextField1];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matchesDiscoutUpdate=arrAllObj[i];
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                
                if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
                {
                    
                    [matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
                    [matchesDiscoutUpdate setValue:[arrDiscountPer objectAtIndex:i] forKey:@"discountPercentage"];
                }
                else
                {
                    // [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
                    // [matchesDiscoutUpdate setValue:@"0" forKey:@"discountPercentage"];
                    
                    
                    [matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
                    [matchesDiscoutUpdate setValue:[arrDiscountPer objectAtIndex:i] forKey:@"discountPercentage"];
                }
                [matchesDiscoutUpdate setValue:[arrFinalInitialPrice objectAtIndex:i] forKey:@"finalUnitBasedInitialPrice"];
                [matchesDiscoutUpdate setValue:[arrFinalMaintPrice objectAtIndex:i] forKey:@"finalUnitBasedMaintPrice"];
                
                [matchesDiscoutUpdate setValue:[arrFinalInitialPrice objectAtIndex:i] forKey:@"totalInitialPrice"];
                [matchesDiscoutUpdate setValue:[arrFinalMaintPrice objectAtIndex:i] forKey:@"totalMaintPrice"];
                
                [matchesDiscoutUpdate setValue:[arrUnit objectAtIndex:i] forKey:@"unit"];
            }
            else  //For Bundle
            {
                
                NSString *strIdBundle,*strSysNameService;
                strIdBundle=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]];
                
                if ([strIdBundle isEqualToString:strBundleIdForTextField])
                {
                    if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
                    {
                        
                        [matchesDiscoutUpdate setValue:[arrDiscountBundle objectAtIndex:i] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[arrDiscountPerBundle objectAtIndex:i] forKey:@"discountPercentage"];
                    }
                    else
                    {
                        // [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
                        //[matchesDiscoutUpdate setValue:@"0" forKey:@"discountPercentage"];
                        [matchesDiscoutUpdate setValue:[arrDiscountBundle objectAtIndex:i] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[arrDiscountPerBundle objectAtIndex:i] forKey:@"discountPercentage"];
                    }
                    [matchesDiscoutUpdate setValue:[arrFinalInitialPriceBundle objectAtIndex:i] forKey:@"finalUnitBasedInitialPrice"];
                    [matchesDiscoutUpdate setValue:[arrFinalMaintPriceBundle objectAtIndex:i] forKey:@"finalUnitBasedMaintPrice"];
                    
                    [matchesDiscoutUpdate setValue:[arrFinalInitialPriceBundle objectAtIndex:i] forKey:@"totalInitialPrice"];
                    [matchesDiscoutUpdate setValue:[arrFinalMaintPriceBundle objectAtIndex:i] forKey:@"totalMaintPrice"];
                    
                    [matchesDiscoutUpdate setValue:[arrUnitBundle objectAtIndex:i] forKey:@"unit"];
                    
                }
            }
        }
        [context save:&error1];
    }
    /*appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
     requestNew = [[NSFetchRequest alloc] init];
     [requestNew setEntity:entitySoldServiceStandardDetail];
     NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
     
     [requestNew setPredicate:predicate];
     
     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
     sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
     
     [requestNew setSortDescriptors:sortDescriptors];
     
     self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
     [self.fetchedResultsControllerSalesInfo setDelegate:self];
     
     
     // Perform Fetch
     NSError *error1 = nil;
     [self.fetchedResultsControllerSalesInfo performFetch:&error1];
     arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
     NSManagedObject *matchesDiscoutUpdate;
     if (arrAllObj.count==0)
     {
     
     }else
     {
     for (int i=0; i<arrAllObj.count; i++)
     {
     matchesDiscoutUpdate=arrAllObj[i];
     
     if (isCheckBoxClickFirstTime==YES)
     {
     if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
     {
     
     [matchesDiscoutUpdate setValue:[arrDiscount objectAtIndex:i] forKey:@"discount"];
     }
     else
     {
     [matchesDiscoutUpdate setValue:@"0" forKey:@"discount"];
     }
     }
     }
     [context save:&error1];
     }
     */
    [_tblBundle reloadData];
    
}
- (IBAction)actionOnCheckBoxNonStan:(id)sender
{
    isCheckBoxClickFirstTimeNonStan=YES;
    isEditedInSalesAuto=YES;
    if ([_imgChkBoxDiscountNonStan.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        isChkBoxEnableNonStan=NO;
        [_imgChkBoxDiscountNonStan setImage:[UIImage imageNamed:@"check_box_1.png"]];
    }
    else
    {
        isChkBoxEnableNonStan=YES;
        [_imgChkBoxDiscountNonStan setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    
    
    
    //Nilind
    
    isCheckBoxClickFirstTime=YES;
    isEditedInSalesAuto=YES;
    if ([_imgChkBoxDiscount.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        isChkBoxEnable=NO;
        [_imgChkBoxDiscount setImage:[UIImage imageNamed:@"check_box_1.png"]];
    }
    else
    {
        isChkBoxEnable=YES;
        [_imgChkBoxDiscount setImage:[UIImage imageNamed:@"check_box_2.png"]];
    }
    [self finalUpdatedTableDataWithDiscount];
    [self finalUpdatedTableDataWithDiscountNonStan];
    [_tblRecord reloadData];
    [_tblNonStandardService reloadData];
    
}
//Nilind 08 June
-(void)discountStatusNonStan
{
    //strCouponStatusNonStan=@"false";
    //strCouponStatusNonStan=@"true";
    
    if ([strCouponStatusNonStan isEqualToString:@"true"])
    {
        if (chkStatus==YES)
        {
            _constViewDiscountNonStan_H.constant=0;//0
            _const_TableNonStan_H.constant=_const_TableNonStan_H.constant+ _constViewDiscountNonStan_H.constant;
        }
        else
        {
            _constViewDiscountNonStan_H.constant=50-50;
            _const_TableNonStan_H.constant=_const_TableNonStan_H.constant- _constViewDiscountNonStan_H.constant;
        }
    }
    else
    {
        _constViewDiscountNonStan_H.constant=0;//0
        _const_TableNonStan_H.constant=_const_TableNonStan_H.constant+ _constViewDiscountNonStan_H.constant;
    }
    
    //_constViewDiscountNonStan_H.constant=50;
    
}

-(void)finalUpdatedTableDataWithDiscountNonStan
{
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray*arrAllObjNonStan;
    
    arrAllObjNonStan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdateNonStan;
    if (arrAllObjNonStan.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObjNonStan.count; i++)
        {
            matchesDiscoutUpdateNonStan=arrAllObjNonStan[i];
            
            /*if (isCheckBoxClickFirstTimeNonStan==YES)
             {
             if ([_imgChkBoxDiscountNonStan.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
             {
             
             [matchesDiscoutUpdateNonStan setValue:[arrNonStanDiscount objectAtIndex:i] forKey:@"discount"];
             }
             else
             {
             [matchesDiscoutUpdateNonStan setValue:@"0" forKey:@"discount"];
             }
             }*/
            if ([_imgChkBoxDiscountNonStan.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
            {
                
                [matchesDiscoutUpdateNonStan setValue:[arrNonStanDiscount objectAtIndex:i] forKey:@"discount"];
                [matchesDiscoutUpdateNonStan setValue:[arrDiscountPerNonStan objectAtIndex:i] forKey:@"discountPercentage"];
            }
            else
            {
                [matchesDiscoutUpdateNonStan setValue:@"0" forKey:@"discount"];
                [matchesDiscoutUpdateNonStan setValue:@"0" forKey:@"discountPercentage"];
            }
            
        }
        [context save:&error1];
    }
}
//Nilind 14 Sept
#pragma mark - ---------Nilind 14 Sept FOOTER CHANGE -----------

- (IBAction)actionAddAfterImages:(id)sender
{
     [self endEditing];
    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        
        /* UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
         delegate:self
         cancelButtonTitle:@"Cancel"
         destructiveButtonTitle:nil
         otherButtonTitles:@"Capture New", @"Gallery", nil];
         
         [actionSheet showInView:self.view];*/
        [self newAlertMethodAddImage];
        
        
    }
}

- (IBAction)actionOnCancelAfterImage:(id)sender
{
     [self endEditing];
    [_viewForAfterImage removeFromSuperview];
    
}

- (IBAction)actionOnAfterImgView:(id)sender
{
    // [self endEditing];
   // [self goToGlobalmage:@"Before"];
    
    UIButton *btn=(UIButton*)sender;
    [self attachImage:btn];
   
}
-(void)getImageCollectionView
{
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage)
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        EditImageViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    }
    else
    {
        
        //Nilind 07 June Image Caption
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isFromBackServiceSummary"];
        
        if (isFromBack)
        {
            
            //arrOfBeforeImageAll=nil;
            //arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            arrNoImage=[[NSMutableArray alloc]init];
            
            arrImageLattitude=nil;
            arrImageLattitude=[[NSMutableArray alloc]init];
            arrImageLongitude=nil;
            arrImageLongitude=[[NSMutableArray alloc]init];
            
            arrImageGraphLattitude=nil;
            arrImageGraphLattitude=[[NSMutableArray alloc]init];
            arrImageGraphLongitude=nil;
            arrImageGraphLongitude=[[NSMutableArray alloc]init];
            
            
            [defsBack setBool:NO forKey:@"isFromBackServiceSummary"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseForGraph];
            
            [self fetchImageDetailFromDataBase];
            
        }
        
        
        
        //End
        
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreviewSales"];
        if (yesFromDeleteImage)
        {
            isEditedInSalesAuto=YES;
            NSLog(@"Database edited");
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreviewSales"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrNoImage.count; k++)
            {
                
                NSDictionary *dictdat=arrNoImage[k];
                NSString *strImageName;
                if([dictdat isKindOfClass:[NSDictionary class]])
                {
                    strImageName=[dictdat valueForKey:@"leadImagePath"];
                }
                else
                {
                    strImageName=arrNoImage[k];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImagesSales"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrNoImage[k]];
                        
                    }
                }
                
            }
            if (!(arrTempBeforeImage.count==0))
            {
                
                [arrNoImage removeObjectsInArray:arrTempBeforeImage];
            }
            else if((arrTempBeforeImage.count==0))
            {
                arrNoImage=nil;
                arrNoImage=[[NSMutableArray alloc]init];
            }
            
            //nIlind 09 April
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImagesSales"];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            
            
            [defsnew synchronize];
            
            [self saveImageToCoreData];
            
            //End
            
        }
        else
        {
            
        }
        //Nilind 07 June Image Caption
        
        
        //Change in image Captions
        BOOL yesEditedImageCaptionGraph=[defsSS boolForKey:@"yesEditedImageCaptionGraph"];
        if (yesEditedImageCaptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescriptionGraph=[defsSS boolForKey:@"yesEditedImageDescriptionGraph"];
        if (yesEditedImageDescriptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //End
        
        
        [self downloadingImagesThumbNailCheck];
        //Nilind 10 Jan
        
    }
    //...........
    [_collectionViewAfterImage reloadData];
    [_collectionViewGraphImage reloadData];
    
}

#pragma mark- ---------- FETCH IMAGE ----------
-(void)fetchImageDetailFromDataBase
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //arrGraphImage=[[NSMutableArray alloc]init];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" : leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                /*
                 [arrOfImagenameCollewctionView addObject:leadImagePath];
                 
                 //Nilind 07 June
                 
                 NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                 
                 if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                 
                 [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                 
                 }
                 else
                 {
                 
                 
                 [arrOfImageCaptionGraph addObject:strImageCaption];
                 
                 }
                 
                 
                 NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                 
                 if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                 
                 [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                 
                 } else {
                 
                 [arrOfImageDescriptionGraph addObject:strImageDescription];
                 
                 }
                 
                 
                 */
                //End
                
            }
            else
            {
                
                [arrNoImage addObject:dict_ToSendLeadInfo];
                
                //Nilind 07 June
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageLongitude addObject:strLong];
                    
                }
                //End
                
            }
            
            
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    }
    else
    {
    }
}


-(void)openGalleryy{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera
{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==7)
    {
        
        
        NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        
        NSArray *arrOfImagesDetail=arrNoImage;
        
        arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dictData=arrOfImagesDetail[k];
                
                [arrOfBeforeImages addObject:dictData];
                
            }else{
                
                [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
                
            }
            
        }
        
        arrOfImagesDetail=arrOfBeforeImages;
        
        if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
            [global AlertMethod:Info :NoBeforeImg];
        }else if (arrOfImagesDetail.count==0){
            [global AlertMethod:Info :NoBeforeImg];
        }
        else {
            NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
            for (int k=0; k<arrOfImagesDetail.count; k++) {
                if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dict=arrOfImagesDetail[k];
                    [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
                }else{
                    
                    [arrOfImagess addObject:arrOfImagesDetail[k]];
                    
                }
            }
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMainiPad"
                                                                     bundle: nil];
            ImagePreviewSalesAutoiPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAutoiPad"];
            objByProductVC.arrOfImages=arrOfImagess;
            
            [self.navigationController pushViewController:objByProductVC animated:YES];
            
        }
    }
    else if (buttonIndex == 0)
    {
        NSLog(@"The CApture Image.");
        
        
        if (arrNoImage.count<10)
        {
            NSLog(@"The CApture Image.");
            
            BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Camera Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                         }
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //............
        
    }
    else if (buttonIndex == 1)
    {
        NSLog(@"The Gallery.");
        if (arrNoImage.count<10)
        {
            BOOL isCameraPermissionAvailable=[global isGalleryPermission];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Gallery Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         }
                                         else
                                         {
                                             
                                             
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    
}

//============================================================================

#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==_collectionViewAfterImage)
    {
        return arrNoImage.count;
    }
    else
    {
        return arrGraphImage.count;
    }
    
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==_collectionViewAfterImage)
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    else
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *str = [arrNoImage objectAtIndex:indexPath.row];
    if (collectionView==_collectionViewAfterImage)
    {
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreview : strIndex];
            
        }
    }
    else
    {
        // NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *str;// = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrGraphImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewGraph : strIndex];
            
        }
        
    }
    
    
}
//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        
        
        
    }else if (arrOfImagesDetail.count==0){
        
        
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        if (arrOfImagess.count==1)
        {
        }
        else if (arrOfImagess.count==2)
        {
            
        }
        else
        {
        }
        //_const_ImgView_H.constant=100;  temp comment
        //_const_SaveContinue_B.constant=62;
        
        // 21 April 2020
        //[self downloadImages:arrOfImagess];
        
    }
}


-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            [self ShowFirstImage:str:k];
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    //NIlind 14 Sept
    
    [_collectionViewAfterImage reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    [self ShowFirstImage : name : indexx];
    
}

- (UIImage*)loadImage :(NSString*)name :(int)indexxx
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (indexxx==0)
    {
        
        
    } else if(indexxx==1)
    {
        
        
        
    }else{
        
        
    }
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    return image;
}
-(void)goingToPreview :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMainiPad"
                                                                 bundle: nil];
        ImagePreviewSalesAutoiPad
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAutoiPad"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
            
        }
        else
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=@"Open";
            
        }    objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaption;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescription;
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backfromDynamicView"];
    [defs synchronize];
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    // NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
    
    NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@%@.jpg",strDate,strTime,strLeadId];
    
    
    
    [arrNoImage addObject:strImageNamess];
    //UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    // [self saveImage:chosenImage :strImageNamess];
    
    [self resizeImage:chosenImage :strImageNamess];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    //Lat long code
     
     CLLocationCoordinate2D coordinate = [global getLocation] ;
     NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
     NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
     [arrImageLattitude addObject:latitude];
     [arrImageLongitude addObject:longitude];
    
    //imageCaption
    
    NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
    
    BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
    
    if (yesImageCaption) {
        
        [self alertViewCustom];
        
    } else {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        [self saveImageToCoreData];
        
    }
    
 
    
    
}

//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender
{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
        //[_scrollViewService setContentOffset:bottomOffset animated:YES];
        [_scrollViewService setContentOffset:CGPointZero animated:YES];
        [self saveImageToCoreData];
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}
-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    //    CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
    //    [_scrollViewService setContentOffset:bottomOffset animated:YES];
    [self saveImageToCoreData];
    
}

-(void)alertToEnterImageCaption{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Caption Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Image Description Here...";
        textField.tag=8;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        UITextField * txtImageDescriptions = textfields[1];
        if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
            
            if (txtHistoricalDays.text.length>0) {
                
                [arrOfImageCaption addObject:txtHistoricalDays.text];
                
            } else {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtImageDescriptions.text.length>0) {
                
                [arrOfImageDescription addObject:txtImageDescriptions.text];
                
            } else {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            }
            
            CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
            [_scrollViewService setContentOffset:bottomOffset animated:YES];
            
            
        } else {
            
            [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
        [_scrollViewService setContentOffset:bottomOffset animated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
//.................................................................................
-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = actualHeight/1.5;
    float maxWidth = actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    //CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    CGRect rect = CGRectMake(0.0, 0.0, image.size.width/2, image.size.height/2);
    
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}
#pragma mark- Save & Remove Image  Methods
//============================================================================
//============================================================================

- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
#pragma mark- Note
    /*
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL yesfromback=[defs boolForKey:@"backfromDynamicView"];
    if (yesfromback) {
        
        [defs setBool:NO forKey:@"backfromDynamicView"];
        [defs synchronize];
        
    }
    else
    {
        
        [self deleteImageFromCoreDataSalesInfo];
        
        for (int k=0; k<arrNoImage.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            if ([arrNoImage[k] isKindOfClass:[NSString class]])
            {
                
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[arrNoImage objectAtIndex:k]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                NSError *error1;
                [context save:&error1];
            }
            else
            {
                NSDictionary *dictData=[arrNoImage objectAtIndex:k];
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                NSError *error1;
                [context save:&error1];
                
            }
            
        }
        for (int k=0; k<arrGraphImage.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            NSDictionary *dict=[arrGraphImage objectAtIndex:k];
            if ([arrGraphImage[k] isKindOfClass:[NSDictionary class]])
            {
                
                objImageDetail.createdBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdBy"]];
                objImageDetail.createdDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdDate"]];
                objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
                objImageDetail.leadImageId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageId"]];
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageType"]];
                objImageDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"modifiedBy"]];
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                objImageDetail.leadImageCaption=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageCaption"]];
                objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
                
                
                //Latlong code
                
                //NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
                objImageDetail.latitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"latitude"]];
                // NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
                objImageDetail.longitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"longitude"]];
                
                
                NSError *error1;
                [context save:&error1];
            }
        }
        
        
        
    }
    
    */
    //........................................................................
    // [self fetchForUpdate];
    
    //[self fetchImageFromCoreDataStandard];
    
}
-(void)deleteImageFromCoreDataSalesInfo
{
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
    [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    [allDataImage setPredicate:predicateImage];
    
    [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error2 = nil;
    NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
    //error handling goes here
    for (NSManagedObject * data in DataImage) {
        [context deleteObject:data];
    }
    NSError *saveError2 = nil;
    [context save:&saveError2];
    
}
#pragma mark -------------- ADDING GRAPH IMAGE ----------------

- (IBAction)actionOnAddGraphImage:(id)sender
{
    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"firstGraphImage"];
        [defs setBool:NO forKey:@"servicegraph"];
        
        [defs synchronize];
        
        //        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
        //        EditGraphViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewControlleriPad"];
        //        objSignViewController.strLeadId=strLeadId;
        //        objSignViewController.strCompanyKey=strCompanyKey;
        //        objSignViewController.strUserName=strUserName;
        //        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
        objSignViewController.strLeadId=strLeadId;
        objSignViewController.strCompanyKey=strCompanyKey;
        objSignViewController.strUserName=strUserName;
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    }
}

- (IBAction)actionOnCancelBeforeImage:(id)sender
{
    [_viewForGraphImage removeFromSuperview];
}

- (IBAction)actionOnGraphImageFooter:(id)sender
{
    [self endEditing];
    [self goToGlobalmage:@"Graph"];
}
-(void)goingToPreviewGraph :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrGraphImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++)
    {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
        {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }
        else
        {
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++)
        {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        GraphImagePreviewViewControlleriPad
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImagePreviewViewControlleriPad"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.strLeadId=strLeadId;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaptionGraph;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescriptionGraph;
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
            
        }
        else
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=@"Open";
            
        }
        
        
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
-(void)fetchImageDetailFromDataBaseForGraph
{
    
    arrGraphImage=nil;
    
    arrGraphImage=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" :leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                
                //[arrOfImagenameCollewctionView addObject:leadImagePath];
                [arrGraphImage addObject:dict_ToSendLeadInfo];
                
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionGraph addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionGraph addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLongitude addObject:strLong];
                    
                }
                
                
            }
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    } else {
    }
    
    [self downloadImagesGraphs:arrGraphImage];
    
}
-(void)downloadImagesGraphs :(NSArray*)arrOfImagesDownload
{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        // NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
        NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrOfImagesDownload objectAtIndex:k];
        }
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    [_collectionViewGraphImage reloadData];
    
}

#pragma mark- NILIND 30 Oct bundle change

-(void)heightManage
{
    NSMutableArray *arrHeight;
    arrHeight=[[NSMutableArray alloc]init];
    [self fetchFromCoreDataStandard];
    [self fetchFromCoreDataNonStandard];
    for (int i=0; i<arrSysName.count; i++)
    {
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysName objectAtIndex:i]]];
        if ([str isEqualToString:@"0"])
        {
            [arrHeight addObject:[NSString stringWithFormat:@"%.2f",_tblRecord.rowHeight-55]];
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                [arrHeight addObject:[NSString stringWithFormat:@"%.2f",_tblRecord.rowHeight-55]];
            }
            else
            {
                [arrHeight addObject:[NSString stringWithFormat:@"%.2f",_tblRecord.rowHeight+20]];
            }
        }
    }
    double sum=0;
    for (int i=0; i<arrHeight.count; i++)
    {
        sum=sum+[[arrHeight objectAtIndex:i]doubleValue];
    }
    
    for(int i=0;i<arrAdditionalParamterDcs.count;i++)
    {
        NSArray *arr=[arrAdditionalParamterDcs objectAtIndex:i];
        if (arr.count>0)
        {
            NSDictionary *dict=[arr objectAtIndex:0];
            
            NSArray *arrCount=[dict allKeys];
            
            //sum=sum+([arrCount count]-4)*40;
            sum=sum+([arr count])*7*25 + 65;
            //sum=sum+450;
        }
        if (arr.count==0)
        {
            sum=sum+65;
            
        }
    }
    //_tblRecord.backgroundColor=[UIColor purpleColor];
    _const_TableStan_H.constant=sum+10+20;//+(400*(arrAdditionalParamterDcs.count));
    //25*16=400
    NSMutableArray *arrBundleHeight;
    arrBundleHeight=[[NSMutableArray alloc]init];
    for (int i=0; i<arrTempBundleServiceSysName.count; i++)
    {
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrTempBundleServiceSysName objectAtIndex:i]]];
        if ([str isEqualToString:@"0"])
        {
            [arrBundleHeight addObject:[NSString stringWithFormat:@"%.2f",_tblBundle.rowHeight-65+10+10+15]];
            
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                //return 227-65;
                [arrBundleHeight addObject:[NSString stringWithFormat:@"%.2f",_tblBundle.rowHeight-65+10+10+15]];
                //return _tblBundle.rowHeight-65+10;
                
            }
            else
            {
                //return 227;
                [arrBundleHeight addObject:[NSString stringWithFormat:@"%.2f",_tblBundle.rowHeight+10+10+15]];
                
                //return _tblBundle.rowHeight+10;
            }
        }
        
    }
    int sumBundle=0;
    for (int i=0; i<arrBundleHeight.count; i++)
    {
        sumBundle=sumBundle+[[arrBundleHeight objectAtIndex:i]doubleValue];
    }
    
    _constTblBundle_H.constant=sumBundle+25;//(260)*arrBundleHeight.count;
    _const_tblRenewalService_H.constant = 230 * arrAllObjRenewal.count;
    
    if (arrAllObjRenewal.count == 0)
    {
        [_viewRenewalService setHidden:YES];
    }
    else
    {
        [_viewRenewalService setHidden:NO];
    }
    if (arrInitialPrice.count == 0)
    {
         [_lblMaintenanceServiceStandard setHidden:YES];
    }
    else
    {
        [_lblMaintenanceServiceStandard setHidden:NO];
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    if([defs boolForKey:@"fromServiceSummaryScroll"]==YES)
    {
        
        [defs setBool:NO forKey:@"fromServiceSummaryScroll"];
        [defs synchronize];
        [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableStan_H.constant+_constTblBundle_H.constant+160+40+_const_ViewDiscountCoupon_H.constant + _const_tblRenewalService_H.constant + 100)];
    }
    else
    {
        
        [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableStan_H.constant+_constTblBundle_H.constant + _const_tblRenewalService_H.constant + 100 + 200 )];
        
    }
    if([defs boolForKey:@"backStan"]==NO)
    {
        [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableStan_H.constant+_constTblBundle_H.constant+250+_const_ViewDiscountCoupon_H.constant + _const_tblRenewalService_H.constant + 100)];
    }
    
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        NSString *sectionName;
        sectionName=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        return sectionName;
    }
    else
    {
        return @"";
    }
    
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        NSString *sectionName;
        sectionName=[NSString stringWithFormat:@"%@ Footer",[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        return sectionName;
    }
    else
    {
        return @"";
    }
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==2)
    {
        return arrBundleRow.count;
    }
    else
    {
        return 1;
        //return arrSoldServiceStandardId.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        return 35.0f;
    }
    else
    {
        return 0.0f;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        return 30.0f;
    }
    else
    {
        /*if (tableView==_tblRecord)
         {
         return 50;
         }*/
        return 0.0f;
    }
}
-(void)fetchFromCoreDataStandardForBundle:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    
    
    
    arrInitialPriceBundle=[[NSMutableArray alloc]init];
    arrDiscountPerBundle=[[NSMutableArray alloc]init];
    
    arrDiscountBundle=[[NSMutableArray alloc]init];
    arrMaintenancePriceBundle=[[NSMutableArray alloc]init];
    arrFrequencyNameBundle=[[NSMutableArray alloc]init];
    arrServiceNameBundle=[[NSMutableArray alloc]init];
    arrStanIsSoldBundle=[[NSMutableArray alloc]init];
    arrSysNameBundle=[[NSMutableArray alloc]init];
    
    arrUnitBundle=[[NSMutableArray alloc]init];
    arrFinalInitialPriceBundle=[[NSMutableArray alloc]init];
    arrFinalMaintPriceBundle=[[NSMutableArray alloc]init];
    
    arrPackageNameIdBundle=[[NSMutableArray alloc]init];
    arrBillingFreqSysNameBundle=[[NSMutableArray alloc]init];
    arrBundleId=[[NSMutableArray alloc]init];
    arrFrequencySysNameBundle = [[NSMutableArray alloc]init];
    arrServiceIdBundle = [[NSMutableArray alloc]init];
    arrSoldServiceIdBundleNew = [[NSMutableArray alloc]init];
    // arrBundleRow=[[NSMutableArray alloc]init];
    NSMutableArray *arrBundleNew;
    arrBundleNew=[[NSMutableArray alloc]init];
    
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                [arrFrequencyNameBundle addObject:[matches valueForKey:@"serviceFrequency"]];
                [arrFrequencySysNameBundle addObject:[matches valueForKey:@"frequencySysName"]];
                [arrServiceIdBundle addObject:[matches valueForKey:@"serviceId"]];
                if ([[matches valueForKey:@"serviceSysName"]isEqualToString:@""] || [[matches valueForKey:@"serviceSysName"]isEqualToString:@"(null)"])
                {
                    [arrServiceNameBundle addObject:@""];
                }
                else
                {
                    [arrServiceNameBundle addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                }
                
                [arrStanIsSoldBundle addObject: [matches valueForKey:@"isSold"]];
                
                [arrSysNameBundle addObject:[matches valueForKey:@"serviceSysName"]];
                
                //Nilind 6 oct
                
                if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isTBD"]]isEqualToString:@"true"])
                {
                    [arrInitialPriceBundle addObject:@"TBD"];
                    [arrDiscountPerBundle addObject:@"TBD"];
                    [arrDiscountBundle addObject:@"TBD"];
                    [arrMaintenancePriceBundle addObject:@"TBD"];
                    [arrFinalInitialPriceBundle addObject:@"TBD"];
                    [arrFinalMaintPriceBundle addObject:@"TBD"];
                }
                else
                {
                    [arrInitialPriceBundle addObject:[matches valueForKey:@"initialPrice"]];
                    [arrDiscountBundle addObject:[matches valueForKey:@"discount"]];
                    [arrMaintenancePriceBundle addObject:[matches valueForKey:@"maintenancePrice"]];
                    [arrFinalInitialPriceBundle addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
                    [arrFinalMaintPriceBundle addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
                    [arrDiscountPerBundle addObject:[matches valueForKey:@"discountPercentage"]];
                    
                }
                [arrUnitBundle addObject:[matches valueForKey:@"unit"]];
                
                [arrPackageNameIdBundle addObject:[matches valueForKey:@"packageId"]];
                [arrBillingFreqSysNameBundle addObject:[matches valueForKey:@"billingFrequencySysName"]];
                [arrBundleId addObject:[matches valueForKey:@"bundleId"]];
                 [arrSoldServiceIdBundleNew addObject:[matches valueForKey:@"soldServiceStandardId"]];
                
            }
        }
    }
    // [self heightManage];
}
/*-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
 {
 
 }*/
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(tableView==_tblBundle)
    {
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 35)];
        //headerView.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        headerView.backgroundColor = [UIColor colorWithRed:246.0/255 green:246.0/255 blue:246.0/255 alpha:1];
        
        //headerView.backgroundColor = [UIColor colorWithRed:211.0/255 green:175.0/255 blue:72.0/255 alpha:1];
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = section + 1000;
        button.frame = CGRectMake(_tblBundle.frame.origin.x+5, 2, 30, 30);
        
        [self fetchFromCoreDataStandardForSectionCheckBoxBundle:[arrBundleRow objectAtIndex:section]];
        if ([strSoldStatusBundle isEqualToString:@"true"])
        {
            [button setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            [button setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            
        }
        
        [button addTarget:self action:@selector(checkBundleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:button];
        [button setTitle:[arrBundleRow objectAtIndex:section] forState:UIControlStateNormal];
        // button.titleLabel.textColor=[UIColor clearColor];
        
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x+button.frame.size.width+10, 0, 250, 35)];
        
        
        NSString *sectionName;
        sectionName=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        headerLabel.text =sectionName;
        headerLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        headerLabel.textColor = [UIColor blackColor];
        //headerLabel.backgroundColor = [UIColor lightGrayColor];
        [headerView addSubview:headerLabel];
        
        UIButton *buttonDelete = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonDelete.tag = section + 1000;
        buttonDelete.frame = CGRectMake(CGRectGetMaxX(headerView.frame)-70, 0, 35, 35);
        //[buttonDelete setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        //buttonDelete.backgroundColor=[UIColor redColor];
        [buttonDelete addTarget:self action:@selector(deleteBundleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:buttonDelete];
        [buttonDelete setTitle:[arrBundleRow objectAtIndex:section]  forState:UIControlStateNormal];
        buttonDelete.titleLabel.textColor=[UIColor clearColor];
        [buttonDelete setImage:[UIImage imageNamed:@"ic_action_delete.png"] forState:UIControlStateNormal];
        [buttonDelete setBackgroundImage:[UIImage imageNamed:@"ic_action_delete.png"] forState:UIControlStateNormal];
        if (chkStatus==YES)
        {
            button.enabled=NO;
            buttonDelete.enabled=NO;
        }
        else
        {
            button.enabled=YES;
            buttonDelete.enabled=YES;
            
        }
        return headerView;
    }
    else
    {
        UIView *headerView = [[UIView alloc] init];
        return headerView;
        
    }
}
- (IBAction)deleteBundleButtonPressed:(UIButton *)sender
{
    // NSInteger section = sender.tag - 1000;
    //UIButton *selectedButton = (UIButton *) sender;
    
    //[self deleteBundleButtonPressed:sender.titleLabel.text];
    NSLog(@"Section Bundle Id %@",sender.titleLabel.text);
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert!"
                               message:@"Are you sure want to delete Bundle"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              [self fetchFromCoreDataStandard];
                              [self fetchFromCoreDataStandardForBundleForDiscountCupon:[NSString stringWithFormat:@"%@",sender.titleLabel.text]];
                              [self deleteBundleFromCoreDataSalesInfo:[NSString stringWithFormat:@"%@",sender.titleLabel.text]];
                              [self fetchFromCoreDataStandard];
                              
                              
                              
                              for (int i=0;i< arrSoldServiceStandardIdBundle.count; i++)
                              {
                                  NSString *strId=[arrSoldServiceStandardIdBundle objectAtIndex:i];
                                  [self fetchForAppliedDiscountServiceFromCoreData:strId];
                              }
                              
                              [_tblBundle reloadData];
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             
                             
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)checkBundleButtonPressed:(UIButton *)sender
{
    NSInteger section = sender.tag - 1000;
    
    UIButton *selectedButton = (UIButton *) sender;
    NSLog(@"Section Bundle Id %@",selectedButton.titleLabel.text);
    if([selectedButton.currentImage isEqual:[UIImage imageNamed:@"check_box_2.png"]])
        //if(selectedButton.selected==YES)
    {
        [selectedButton setSelected:NO];
        [selectedButton setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [self updateBundle:selectedButton.titleLabel.text :@"false"];
        
        
    }
    else
    {
        [selectedButton setSelected:YES];
        [selectedButton setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [self updateBundle:selectedButton.titleLabel.text :@"true"];
    }
    
}
-(void)updateBundle: (NSString*)strButtonBundleId : (NSString*)status
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,strButtonBundleId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            //if([[matches valueForKey:@"bundleId"]isEqualToString:strButtonBundleId])
            //{
            [matches setValue:status forKey:@"isSold"];
            [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
            // [context save:&error1];
            //}
            
        }
        [context save:&error1];
    }
    
    [self fetchFromCoreDataStandard];
    
}
-(void)deleteBundleFromCoreDataSalesInfo:(NSString*)strBundleIdd
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    for (int i=0; i<Data.count; i++)
    {
        NSManagedObject *dataMatches=[Data objectAtIndex:i];
        if ([[dataMatches valueForKey:@"bundleId"]isEqualToString:strBundleIdd])
        {
            [context deleteObject:dataMatches];
             [self deleteRenewalFromCoreDataSalesInfo:[NSString stringWithFormat:@"%@", [dataMatches valueForKey:@"soldServiceStandardId"]]];
            NSError *saveError = nil;
            [context save:&saveError];
            
        }
    }
    
    /*for (NSManagedObject * data in Data)
     {
     [context deleteObject:data];
     }*/
    
    //[self fetchFromCoreDataStandard];
    [self heightManage];
    //[self fetchFromCoreDataStandard];
    
    // [_tblBundle reloadData];
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    //if (tableView==_tblBundle)
    //{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 30)];
    headerView.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(_tblBundle.frame.origin.x+5, 0, _tblBundle.frame.size.width/2, 30)];
    [self fetchFromCoreDataStandardForSectionCheckBoxBundle:[arrBundleRow objectAtIndex:section]];
    headerLabel.text =[NSString stringWithFormat:@"Total Initial Cost = $%.2f",totalInitialBundle];
    headerLabel.font=[UIFont boldSystemFontOfSize:12];
    headerLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabel];
    
    UILabel *headerLabelMaint = [[UILabel alloc] initWithFrame:CGRectMake(headerLabel.frame.origin.x+headerLabel.frame.size.width+120, 0,_tblBundle.frame.size.width/2, 30)];
    headerLabelMaint.text =[NSString stringWithFormat:@"Total Maint Cost = $%.2f",totalMaintBundle];
    CGSize expectedLabelSize = [ headerLabelMaint.text sizeWithFont:headerLabelMaint.font constrainedToSize:CGSizeMake(headerLabelMaint.frame.size.width, FLT_MAX) lineBreakMode:headerLabelMaint.lineBreakMode];
    headerLabelMaint.frame=CGRectMake(_tblBundle.frame.size.width -expectedLabelSize.width-10, 0, expectedLabelSize.width, 30);
    
    headerLabelMaint.font=[UIFont boldSystemFontOfSize:14];
    
    headerLabelMaint.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabelMaint.textColor = [UIColor blackColor];
    headerLabelMaint.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabelMaint];
    
    return headerView;
    //}
    /*  else if (tableView==_tblRecord)
     {
     
     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblRecord.frame.size.width, 40)];
     headerView.backgroundColor = [UIColor yellowColor];
     
     /*UILabel *lbl;
     UITextField *txt;
     
     _viewFooter.frame=CGRectMake(_tblRecord.frame.origin.x, 0, _tblRecord.frame.size.width, 20);
     for (int i=0; i<3; i++)
     {
     _viewFooter=[[UIView alloc]init];
     lbl=[[UILabel alloc]init];
     txt=[[UITextField alloc]init];
     //_viewFooter.backgroundColor=[UIColor redColor];
     if(i==0)
     {
     _viewFooter.frame=CGRectMake(_tblRecord.frame.origin.x, 0, _tblRecord.frame.size.width, 20);
     lbl.frame=CGRectMake(10, 0,_viewFooter.frame.size.width/2, 20);
     lbl.text=[NSString stringWithFormat:@"Label %d",i];
     lbl.font=[UIFont boldSystemFontOfSize:14];
     
     txt.frame=CGRectMake(CGRectGetMaxX(lbl.frame), lbl.frame.origin.y,_viewFooter.frame.size.width/2-20, 20);
     
     txt.text=[NSString stringWithFormat:@"Text %d",i];
     txt.font=[UIFont boldSystemFontOfSize:14];
     txt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     txt.delegate=self;
     txt.layer.borderWidth=1.0;
     
     txt.layer.cornerRadius=5.0;
     
     txt.tag=(10*i)+10;
     
     [_viewFooter addSubview:lbl];
     [_viewFooter addSubview:txt];
     
     }
     else
     {
     _viewFooter.frame=CGRectMake(_tblRecord.frame.origin.x, (20+5)*(i), _tblRecord.frame.size.width, 20);
     
     lbl.frame=CGRectMake(10, 0,_viewFooter.frame.size.width/2, 20);
     lbl.text=[NSString stringWithFormat:@"Label %d",i];
     txt.frame=CGRectMake(CGRectGetMaxX(lbl.frame), lbl.frame.origin.y,_viewFooter.frame.size.width/2-20, 20);
     txt.text=[NSString stringWithFormat:@"Text %d",i];
     
     lbl.font=[UIFont boldSystemFontOfSize:14];
     txt.font=[UIFont boldSystemFontOfSize:14];
     
     txt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     txt.delegate=self;
     
     txt.layer.borderWidth=1.0;
     txt.tag=(10*i)+10;
     
     txt.layer.cornerRadius=5.0;
     [_viewFooter addSubview:lbl];
     [_viewFooter addSubview:txt];
     }
     
     [headerView addSubview:_viewFooter];
     
     }
     headerView.frame=CGRectMake(headerView.frame.origin.x, headerView.frame.origin.y, headerView.frame.size.width, 20* 3.5);
     
     return headerView;
     
     }
     else
     {
     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 30)];
     return headerView;
     
     }*/
}
-(void)fetchFromCoreDataStandardForSectionCheckBoxBundle:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    strSoldStatusBundle=@"false";
    NSMutableArray *arrTotalInitialCostBundle,*arrTotalMaintCostBundle;
    arrTotalInitialCostBundle=[[NSMutableArray alloc]init];
    arrTotalMaintCostBundle=[[NSMutableArray alloc]init];
    totalInitialBundle=0;totalMaintBundle=0;
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                strSoldStatusBundle=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isSold"]];
                
                
                
                NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                if ([str isEqualToString:@"0"])
                {
                    totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                    totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                    
                }
                else
                {
                    if (str.length==0 || [str isEqualToString:@"(null)"])
                    {
                        totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                        totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                        
                    }
                    else
                    {
                        totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedInitialPrice"]]doubleValue];
                        
                        totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedMaintPrice"]]doubleValue];
                    }
                }
                
                //totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                //totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                //break;
            }
        }
    }
}
- (IBAction)actionOnNotesHistory:(id)sender
{
    [self endEditing];
    [self goToHistory];
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
     [self endEditing];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ClockInOutViewControlleriPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewControlleriPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
-(void)updateLeadIdDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        matches=arrAllObj[0];
        [matches setValue:@"Lost" forKey:@"stageSysName"];
        [matches setValue:@"Complete" forKey:@"statusSysName"];
        
        
    }
    [context save:&error1];
}

- (IBAction)actionOnBtnMarkAsLost:(id)sender
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert!"
                                  message:@"Are you sure to mark Opportunity as lost?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self updateLeadIdDetail];
                             Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
                             NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
                             if (netStatusWify1== NotReachable)
                             {
                                 //[global AlertMethod:@"Alert!":@"No Internet connection available"];
                             }
                             else
                             {
                                 /*[DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
                                  
                                  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:@"startLoader" object:nil];
                                  [self performSelector:@selector(methodSync) withObject:nil afterDelay:0.5];*/
                                 
                             }
        NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
        
        NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
        
        if ([strAppointmentFlow isEqualToString:@"New"])
        {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
            AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }
        else
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
            AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
        }
                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"NO"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(NSArray*)calculationForParamterBasedService:(NSString *)strServiceSysNamePara
{
    NSMutableArray *arrOtherNonDefault;
    arrOtherNonDefault=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        
        NSArray *arrServicePara=[dict valueForKey:@"Services"];
        
        for(int j=0;j<arrServicePara.count;j++)
        {
            NSDictionary *dictForJ=[arrServicePara objectAtIndex:j];
            
            if ([strServiceSysNamePara isEqualToString:[NSString stringWithFormat:@"%@",[dictForJ valueForKey:@"SysName"]]])
            {
                NSArray *arrServiceParametersDcs=[dictForJ valueForKey:@"ServiceParametersDcs"];
                
                for (int k=0; k<arrServiceParametersDcs.count; k++)
                {
                    NSDictionary *dictForK=[arrServiceParametersDcs objectAtIndex:k];
                    
                    if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"0"])
                    {
                        [arrOtherNonDefault addObject:dictForK];
                    }
                }
                
                break;
                
            }
        }
        
        
    }
    return arrOtherNonDefault;
    
}


#pragma mark- final update parameter data
-(void)finalUpdatedParameterData:(NSMutableArray*) arrTempPara :(long)row
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrUpdate;
    arrUpdate = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesUpdate;
    if (arrUpdate.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrUpdate.count; i++)
        {
            matchesUpdate=arrUpdate[i];
            
            if (i==row)
            {
                [matchesUpdate setValue:arrTempPara forKey:@"additionalParameterPriceDcs"];
                [arrAdditionalParamterDcs replaceObjectAtIndex:i withObject:arrTempPara];
                
            }
        }
        [context save:&error1];
    }
    //[self fetchFromCoreDataStandard];
    // [_tblRecord reloadData];
    
}
#pragma mark- final update parameter data
-(void)finalUpdatedOnSaveForTotalInitialAndMaintPrice
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrUpdate;
    arrUpdate = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrSoldServiceIdOfAllService=[[NSMutableArray alloc]init];
    arrAllUnsoldServiceId=[[NSMutableArray alloc]init];
    NSManagedObject *matchesUpdate;
    if (arrUpdate.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrUpdate.count; k++)
        {
            matchesUpdate=arrUpdate[k];
            
            if([[matchesUpdate valueForKey:@"isSold"]isEqualToString:@"true"]||[[matchesUpdate valueForKey:@"isSold"]isEqualToString:@"True"])
            {
                [arrSoldServiceIdOfAllService addObject:[matchesUpdate valueForKey:@"soldServiceStandardId"]];
            }
            else
            {
                [arrAllUnsoldServiceId addObject:[matchesUpdate valueForKey:@"soldServiceStandardId"]];
            }
            
            if ([[NSString stringWithFormat:@"%@",[matchesUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                NSArray *arrAdditionalPara=[matchesUpdate valueForKey:@"additionalParameterPriceDcs"];
                
                float totalParaInitial=0.0,totalParaMaint=0.0;
                
                if(arrAdditionalPara.count>0)
                {
                    for (int i=0; i<arrAdditionalPara.count; i++)
                    {
                        NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
                        totalParaInitial=totalParaInitial+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]floatValue];
                        totalParaMaint=totalParaMaint+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]floatValue];
                        
                    }
                    //totalParaInitial=totalParaInitial+[[matchesUpdate valueForKey:@"initialPrice"]floatValue];
                    //totalParaMaint=totalParaMaint+[[matchesUpdate valueForKey:@"maintenancePrice"]floatValue];
                }
                totalParaInitial=totalParaInitial+([[matchesUpdate valueForKey:@"initialPrice"]floatValue])*[[matchesUpdate valueForKey:@"unit"]floatValue];
                
                //commented by saavan
                //totalParaInitial=totalParaInitial-[[matchesUpdate valueForKey:@"discount"]floatValue];
                
                totalParaMaint=totalParaMaint+[[matchesUpdate valueForKey:@"maintenancePrice"]floatValue]*[[matchesUpdate valueForKey:@"unit"]floatValue];
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalParaInitial] forKey:@"totalInitialPrice"];
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalParaMaint] forKey:@"totalMaintPrice"];
                
                
                //For Billing Price Calculation
                
                NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
                strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]]];
                
                strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"billingFrequencySysName"]]]];
                
                
                float totalBillingFreqCharge=0.0;
                
                if ([[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"OneTime"]||[[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"One Time"])
                {
                    
                    @try {
                        totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                        
                    } @catch (NSException *exception) {
                        totalBillingFreqCharge=0;
                    } @finally {
                        
                    }
                    
                    
                }
                else
                {
                    @try
                    {
                        if (chkFreqConfiguration == YES)
                        {
                            if ([[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"Yearly"])
                            {
                                totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                                
                            }
                            else
                            {
                                totalBillingFreqCharge=(totalParaMaint * ([strServiceFreqYearOccurence floatValue]-1))/([strBillingFreqYearOccurence floatValue]);
                                
                            }
                            
                            
                        }
                        else
                        {
                            totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                            
                        }
                    }
                    @catch (NSException *exception)
                    {
                        totalBillingFreqCharge=0;
                    }
                    @finally
                    {
                        
                    }
                    
                    
                }
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalBillingFreqCharge] forKey:@"billingFrequencyPrice"];
                //End
            }
            else
            {  //Bundle
                float totalParaInitial=0.0,totalParaMaint=0.0;
                totalParaInitial=totalParaInitial+([[matchesUpdate valueForKey:@"initialPrice"]floatValue])*[[matchesUpdate valueForKey:@"unit"]floatValue];
                totalParaMaint=totalParaMaint+[[matchesUpdate valueForKey:@"maintenancePrice"]floatValue]*[[matchesUpdate valueForKey:@"unit"]floatValue];
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalParaInitial] forKey:@"totalInitialPrice"];
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalParaMaint] forKey:@"totalMaintPrice"];
                
                
                //For Billing Price Calculation
                
                NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
                strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]]];
                
                strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"billingFrequencySysName"]]]];
                
                
                float totalBillingFreqCharge=0.0;
                
                if ([[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"OneTime"]||[[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"One Time"])
                {
                    @try {
                        totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                        
                    } @catch (NSException *exception) {
                        totalBillingFreqCharge=0;
                    } @finally {
                        
                    }
                    
                }
                else
                {
                    @try
                    {
                        if (chkFreqConfiguration == YES)
                        {
                            
                            if ([[matchesUpdate valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"Yearly"])
                            {
                                totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                                
                            }
                            else
                            {
                                totalBillingFreqCharge=(totalParaMaint * ([strServiceFreqYearOccurence floatValue]-1))/([strBillingFreqYearOccurence floatValue]);
                                
                            }
                            
                        }
                        else
                        {
                            totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                            
                        }
                        
                    } @catch (NSException *exception) {
                        totalBillingFreqCharge=0;
                    } @finally {
                        
                    }
                    
                    
                }
                [matchesUpdate setValue:[NSString stringWithFormat:@"%.5f",totalBillingFreqCharge] forKey:@"billingFrequencyPrice"];
                //End
                
                
            }
        }
        [context save:&error1];
    }
    
}
-(void)finalUpdatedNonStanOnSaveForBillilngFreqPrice
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray*arrAllObjNonStan;
    
    arrAllObjNonStan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdateNonStan;
    if (arrAllObjNonStan.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObjNonStan.count; i++)
        {
            matchesDiscoutUpdateNonStan=arrAllObjNonStan[i];
            
            //Billing Freq Calculation
            
            NSString *strBillingFreqYearOccurence,*strServiceFreqYearOccurence;
            strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesDiscoutUpdateNonStan valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]]];
            
            strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[matchesDiscoutUpdateNonStan valueForKey:[NSString stringWithFormat:@"%@",@"billingFrequencySysName"]]]];
            
            if([strBillingFreqYearOccurence isEqualToString:@"(null)"])
            {
                strBillingFreqYearOccurence=@"0";
            }
            float totalBillingFreqCharge=0.0;
            
            float totalInitialPriceNonStan=0.0,totalMaintPriceNonStan=0.0;
            totalInitialPriceNonStan = [[matchesDiscoutUpdateNonStan valueForKey:@"initialPrice"]floatValue]-[[matchesDiscoutUpdateNonStan valueForKey:@"discount"]floatValue];
            totalMaintPriceNonStan = [[matchesDiscoutUpdateNonStan valueForKey:@"maintenancePrice"]floatValue];
            
            
            
            if ([[matchesDiscoutUpdateNonStan valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"OneTime"]||[[matchesDiscoutUpdateNonStan valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"One Time"])
            {
                @try
                {
                    
                    
                    totalBillingFreqCharge=(totalInitialPriceNonStan * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                    
                }
                @catch (NSException *exception)
                {
                    totalBillingFreqCharge=0.0;
                }
                @finally
                {
                    
                }
            }
            else
            {
                @try
                {
                    
                    
                    if (chkFreqConfiguration==YES)
                    {
                        
                        if([[matchesDiscoutUpdateNonStan valueForKey:[NSString stringWithFormat:@"%@",@"frequencySysName"]]isEqualToString:@"Yearly"])
                        {
                            totalBillingFreqCharge=(totalMaintPriceNonStan * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                            
                        }
                        else
                        {
                            totalBillingFreqCharge=(totalMaintPriceNonStan * ([strServiceFreqYearOccurence floatValue]-1))/([strBillingFreqYearOccurence floatValue]);
                        }
                        
                    }
                    else
                    {
                        totalBillingFreqCharge=(totalMaintPriceNonStan * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
                    }
                    
                }
                @catch (NSException *exception)
                {
                    totalBillingFreqCharge=0.0;
                }
                @finally
                {
                    
                }
            }
            
            [matchesDiscoutUpdateNonStan setValue:[NSString stringWithFormat:@"%.5f",totalBillingFreqCharge] forKey:@"billingFrequencyPrice"];
        }
        [context save:&error1];
    }
}

- (IBAction)actionOnApplyDiscount:(id)sender
{
    [self fetchFromCoreDataStandard];
    if(_txtApplyDiscount.text.length==0)
    {
        [global AlertMethod:@"Alert!" :@"Please enter coupon code"];
        _txtApplyDiscount.text=@"";
    }
    else if(arrAllObj.count==0)
    {
        
        [global AlertMethod:@"Alert!" :@"Please add service first to apply coupon"];
        _txtApplyDiscount.text=@"";
        
    }
    else if (globalAmountInitialPrice == 0)
    {
        [global AlertMethod:Alert :@"Coupon not applicable"];
    }
    /*else if(arrInitialPrice.count==0 && arrTempBundleServiceSysName.count>0)
     {
     
     [global AlertMethod:@"Alert!" :@"Please add normal service first to apply coupon"];
     _txtApplyDiscount.text=@"";
     
     }*/
    else
    {
        arrDiscountCoupon=[[NSMutableArray alloc]init];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        arrDiscountCoupon=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
        if([arrDiscountCoupon isKindOfClass:[NSArray class]])
        {
            arrTempCoupon=[[NSMutableArray alloc]init];
            
            NSString *strCouponDiscount=_txtApplyDiscount.text;
            
            //14 June Temp
            NSString *strDiscountUsage=[dictDiscountUsageFromCode valueForKey:strCouponDiscount];
            
            BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount:strDiscountUsage];
            //End
            //BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount];
            if (chkDiscountApplied==YES)
            {
                [global AlertMethod:@"Alert!" :@"Coupon already applied"];
            }
            else
            {
                BOOL chkValidDiscount;
                chkValidDiscount=NO;
                for(int i=0;i<arrDiscountCoupon.count;i++)
                {
                    
                    NSDictionary *dict=[arrDiscountCoupon objectAtIndex:i];
                    
                    if ([strCouponDiscount isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountCode"]]])
                    {
                        NSLog(@"Matched Discount");
                        
                        //Discount Validity Check
                        chkValidDiscount=[self discountValidityCheck:dict];
                        if (chkValidDiscount==YES)
                        {
                            NSLog(@"VALID DISCOUNT");
                            
                            
                            appliedDiscountMaint=0.0;
                            appliedDiscountInitial=0.0;
                            
                            
                            NSArray *arrTempCreditAlreadyApplied=[self fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot:@"Coupon"];
                            
                            float totalDiscountAmoutAlreadyPresent=0.0;
                            
                            for (int k1=0; k1<arrTempCreditAlreadyApplied.count; k1++) {
                                
                                NSManagedObject *objTemp=arrTempCreditAlreadyApplied[k1];
                                
                                NSString *strDiscountAmountPresent=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"appliedInitialDiscount"]];
                                
                                float discountAmountPresentt=[strDiscountAmountPresent floatValue];
                                
                                if([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"true"])
                                {
                                    
                                    
                                }else{
                                    
                                    totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
                                    
                                }
                                
                            }
                            
                            BOOL ifToSaveOrNot;
                            
                            ifToSaveOrNot=NO;
                            
                            NSString *strDiscountAmountTemp=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]];
                            
                            float discount=[strDiscountAmountTemp floatValue];
                            
                            NSString *strIsDiscountTypeNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
                            
                            if ([strIsDiscountTypeNew isEqualToString:@"1"] || [strIsDiscountTypeNew isEqualToString:@"true"] || [strIsDiscountTypeNew isEqualToString:@"True"]) {
                                
                                NSString *strDiscountPercenteNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]];
                                
                                float discountPercentValue=[strDiscountPercenteNew floatValue];
                                
                                if (discountPercentValue==100 || discountPercentValue>100)
                                {
                                    discountPercentValue=100.00;
                                    
                                }
                                
                                discount=(globalAmountInitialPrice*discountPercentValue)/100;
                                
                            }
                            
                            
                            if (!(globalAmountInitialPrice>=(totalDiscountAmoutAlreadyPresent+discount))) {
                                
                                
                                if (globalAmountInitialPrice<(totalDiscountAmoutAlreadyPresent+discount)) {
                                    
                                    float amountDiscountToApplyCredit=globalAmountInitialPrice-totalDiscountAmoutAlreadyPresent;
                                    
                                    NSMutableDictionary *dictTempToSave=[[NSMutableDictionary alloc]init];
                                    
                                    [dictTempToSave addEntriesFromDictionary:dict];
                                    
                                    [dictTempToSave setValue:[NSString stringWithFormat:@"%f",amountDiscountToApplyCredit] forKey:@"DiscountAmount"];
                                    
                                    dict=dictTempToSave;
                                    
                                    ifToSaveOrNot=YES;
                                    
                                    if (amountDiscountToApplyCredit>0) {
                                        
                                        ifToSaveOrNot=YES;
                                        appliedDiscountInitial=amountDiscountToApplyCredit;
                                        
                                        
                                    }else{
                                        
                                        ifToSaveOrNot=NO;
                                        appliedDiscountInitial=0.0;
                                        
                                        
                                    }
                                    
                                }
                            }else{
                                
                                ifToSaveOrNot=YES;
                                
                            }
                            
                            
                            if (ifToSaveOrNot) {
                                
                                NSString *strDiscountPerCheck=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
                                if ([strDiscountPerCheck isEqualToString:@"1"]||[strDiscountPerCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                                {
                                    [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :@"0" CheckForDiscountPer:@"1" DiscountPerValue:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]]];
                                }
                                else
                                {
                                    [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]]CheckForDiscountPer:@"0" DiscountPerValue:@"0"];
                                }
                                if (chkForSaveCoupon==YES)
                                {
                                    [self saveAppliedDiscountCoreData:dict];
                                    [self fetchForAppliedDiscountFromCoreData];
                                    
                                }
                                
                            }else{
                                
                                [global AlertMethod:Alert :@"Coupon can not be more than billing amount."];
                                
                            }
                            NSLog(@"VALID DISCOUNT");
                            
                            
                        }
                        else
                        {
                            NSLog(@"INVALID DISCOUNT");
                            //[global AlertMethod:@"Alert" :@"No valid coupon"];
                            
                        }
                        //[self heightManage];
                        break;
                        
                    }
                    else
                    {
                        chkValidDiscount=NO;
                    }
                    
                }
                if(chkValidDiscount==NO)// && chkForSaveCoupon==NO)
                {
                    [global AlertMethod:@"Alert" :@"Invalid coupon"];
                    
                }
                else
                {
                    if(chkForSaveCoupon==NO)
                    {
                        [global AlertMethod:@"Alert" :@"Invalid coupon"];
                        
                    }
                }
                
                /* arrDiscountCoupon=[[NSMutableArray alloc]init];
                 for (int i=0; i<arrTempCoupon.count; i++)
                 {
                 [arrDiscountCoupon addObject:[arrTempCoupon objectAtIndex:i]];
                 }
                 [self heightForDiscountCoupon];*/
                
            }
        }
        
        
    }
}


-(void)heightForDiscountCoupon
{
    _const_TableDiscount_H.constant=108;
    _const_ViewDiscountCoupon_H.constant=165;
    UILabel *lbl;
    lbl=[[UILabel alloc]init];
    lbl.frame=CGRectMake(0, 0, _tblCouponDiscount.frame.size.width-50, 21);
    arrTempCoupon=[[NSMutableArray alloc]init];
    [arrTempCoupon addObjectsFromArray:arrDiscountCoupon];
    
    float totalHeightStan=0;
    for (int i=0; i<arrTempCoupon.count; i++)
    {
        
        NSManagedObject *dict=[arrTempCoupon objectAtIndex:i];
        
        lbl.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]];
        
        CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lbl.lineBreakMode];
        
        totalHeightStan=totalHeightStan+expectedLabelSize.height;
        
    }
    [_tblCouponDiscount reloadData];
    _const_TableDiscount_H.constant=totalHeightStan+[arrDiscountCoupon count]*80;
    _const_ViewDiscountCoupon_H.constant=_const_TableDiscount_H.constant+50;
}
-(void)finalUpdatedTableDataWithBundleDiscountNew:(NSString *)strServiceNameDiscount :(NSString *)strDiscountAmount CheckForDiscountPer:(NSString*)strDisountCheck DiscountPerValue:(NSString *)strDiscountPercent
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND bundleId=%@",strLeadId,@"0"];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjDiscount = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObjDiscount.count==0)
    {
        
    }
    else
    {
        bool chkDiscountApplied,chkForServiceBasedCoupon,chkForBundle=NO;
        chkDiscountApplied=NO;chkForServiceBasedCoupon=NO;chkForSaveCoupon=NO;
        for (int i=0; i<arrAllObjDiscount.count; i++)
        {
            matchesDiscoutUpdate=arrAllObjDiscount[i];
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                chkForBundle=NO;
                
            }
            else
            {
                chkForBundle=YES;
            }
            
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                //strServiceNameDiscount=@"SignaturePestControl";
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    chkForServiceBasedCoupon=YES;
                    if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        
                        discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        if (discountPerCoupon==100|| discountPerCoupon>100)
                        {
                            discountPerCoupon=100.00;
                            
                        }
                        
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*discountPerCoupon)/100; //initialPrice
                        
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                            //13 June Temp
                            // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        //Calulcate Applied Discount Amount For Initial And Maint
 
                        appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*[strDiscountPercent floatValue])/100;//initialPrice
                        appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue]*[strDiscountPercent floatValue])/100;//maintenancePrice
                        
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                            //13 June Temp
                            // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        

                        //14 June Temp
                        float appDiscIntial,appDiscMaint;
                        appDiscIntial=0;appDiscMaint=0;
                        appDiscIntial=discount;
                        appDiscMaint=discount;
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                            appDiscIntial=discount;
                            
                            //13 June Temp
                            //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue])//maintenancePrice
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue];//maintenancePrice
                            appDiscMaint=discount;
                            
                        }
                        
                        appliedDiscountInitial=appDiscIntial;
                        appliedDiscountMaint=appDiscMaint;
                        
                        //Nilind 29 June
                        appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                        {
                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                        }
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue])//maintenancePrice
                        {
                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue];//maintenancePrice
                        }
                        //End
                        
                        
                    }
                    
                    chkForSaveCoupon=YES;
                }
                
            }
            else  //For Bundle
            {
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    chkForServiceBasedCoupon=YES;
                    if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        
                        discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        if (discountPerCoupon==100|| discountPerCoupon>100)
                        {
                            discountPerCoupon=100.00;
                            
                        }
                        
                        //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*discountPerCoupon)/100; //initialPrice
                        
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]) //initialPrice
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                            //13 June Temp
                            // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        
                        //Calulcate Applied Discount Amount For Initial And Maint
                        
                        
                        
                        /*appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"initialPrice"]]floatValue]*discountPerCoupon)/100;
                         appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"maintenancePrice"]]floatValue]*discountPerCoupon)/100;*/
                        
                        appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*[strDiscountPercent floatValue])/100; //initialPrice
                        
                        appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue]*[strDiscountPercent floatValue])/100; //maintenancePrice
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]) //initialPrice
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                            //13 June Temp
                            //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]; //initialPrice
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                        strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                        

                        //14 June Temp
                        float appDiscIntial,appDiscMaint;
                        appDiscIntial=0;appDiscMaint=0;
                        appDiscIntial=discount;
                        appDiscMaint=discount;
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]) //initialPrice
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                            appDiscIntial=discount;
                            
                            //13 June Temp
                            //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                        }
                        
                        if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue])//maintenancePrice
                        {
                            discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue];//maintenancePrice
                            appDiscMaint=discount;
                            
                        }
                        
                        appliedDiscountInitial=appDiscIntial;
                        appliedDiscountMaint=appDiscMaint;
                        
                        //Nilind 29 June
                        appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                        
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                        {
                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                        }
                        if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue])//maintenancePrice
                        {
                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue];//maintenancePrice
                        }
                        //End
                        
                        
                    }
                    
                    chkForSaveCoupon=YES;
                    chkForBundle=YES;
                }
            }
        }
        if(chkDiscountApplied==NO)//&& chkForServiceBasedCoupon==NO
        {
            if(strServiceNameDiscount.length==0)//Non ServiceBased
            {
                if (arrAllObjDiscount.count>0)
                {
                    matchesDiscoutUpdate=arrAllObjDiscount[0];
                    
                    if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                    {
                        chkForBundle=NO;
                    }
                    else
                    {
                        chkForBundle=YES;
                    }
                    
                    
                    if (chkForBundle==NO)
                    {
                        matchesDiscoutUpdate=arrAllObjDiscount[0];
                        
                        if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                        {
                            float discountPerCoupon=0, discount=0;
                            
                            discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                            if (discountPerCoupon==100|| discountPerCoupon>100)
                            {
                                discountPerCoupon=100.00;
                                
                            }
                            
                            //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                            
                            discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*discountPerCoupon)/100; //initialPrice
                            
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                                //13 June Temp
                                // strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                            }
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                            
                            chkDiscountApplied=YES;
                            strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                            
                        
                            appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*[strDiscountPercent floatValue])/100; //initialPrice
                            appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue]*[strDiscountPercent floatValue])/100; //maintenancePrice
                            
                            
                        }
                        else
                        {
                            float discountPerCoupon=0, discount=0;
                            //strDiscountAmount=@"50";
                            discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                                //13 June Temp
                                //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                            }
                            
                            
                            discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                            
                            discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                            
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                            [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                            
                            strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                            

                            
                            //14 June Temp
                            float appDiscIntial,appDiscMaint;
                            appDiscIntial=0;appDiscMaint=0;
                            appDiscIntial=discount;
                            appDiscMaint=discount;
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                                appDiscIntial=discount;
                                
                                
                                //13 June Temp
                                //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                            }
                            
                            if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue])//maintenancePrice
                            {
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue];//maintenancePrice
                                appDiscMaint=discount;
                                
                            }
                            
                            /*appliedDiscountInitial=appDiscIntial;
                             appliedDiscountMaint=appDiscMaint;*/
                            
                            
                            //Nilind 29 June
                            appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                            
                            if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                            {
                                appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                            }
                            if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue])//maintenancePrice
                            {
                                appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue];
                            }
                            //End
                            
                        }
                    }
                    else
                    {
                        for (int i=0; i<arrAllObjDiscount.count; i++)
                        {
                            matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                            
                            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                            {
                            }
                            else
                            {
                                matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                                if ([strDisountCheck isEqualToString:@"1"]||[strDisountCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                                {
                                    float discountPerCoupon=0, discount=0;
                                    
                                    discountPerCoupon=[strDiscountPercent floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                    if (discountPerCoupon==100|| discountPerCoupon>100)
                                    {
                                        discountPerCoupon=100.00;
                                        
                                    }
                                    
                                    //discount=discount+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                                    
                                    discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*discountPerCoupon)/100;//initialPrice
                                    
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                                    }
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                                    
                                    chkDiscountApplied=YES;
                                    strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                                    
                      
                                    appliedDiscountInitial=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*[strDiscountPercent floatValue])/100; //initialPrice
                                    appliedDiscountMaint=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue]*[strDiscountPercent floatValue])/100;//maintenancePrice
                                    
                                    
                                }
                                else
                                {
                                    float discountPerCoupon=0, discount=0;
                                    //strDiscountAmount=@"50";
                                    discount=[strDiscountAmount floatValue]+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue];
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                                    }
                                    
                                    
                                    discountPerCoupon=discountPerCoupon+[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue];
                                    
                                    discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                                    
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                                    [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                                    strAppliedDiscountServiceId=[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"soldServiceStandardId"]];
                                    

                                    //14 June Temp
                                    float appDiscIntial,appDiscMaint;
                                    appDiscIntial=0;appDiscMaint=0;
                                    appDiscIntial=discount;
                                    appDiscMaint=discount;
                                    
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                                        appDiscIntial=discount;
                                        
                                        //13 June Temp
                                        //strDiscountAmount=[NSString stringWithFormat:@"%.2f",discount];
                                    }
                                    
                                    if(discount>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue])//maintenancePrice
                                    {
                                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue];//maintenancePrice
                                        appDiscMaint=discount;
                                        
                                    }
                                    
                                    /*appliedDiscountInitial=appDiscIntial;
                                     appliedDiscountMaint=appDiscMaint;*/
                                    
                                    
                                    
                                    //Nilind 29 June
                                    appliedDiscountInitial=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    appliedDiscountMaint=[[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue];
                                    
                                    if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue])//initialPrice
                                    {
                                        appliedDiscountInitial=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue];//initialPrice
                                    }
                                    if([[NSString stringWithFormat:@"%@",strDiscountAmount]floatValue]>[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue])//maintenancePrice
                                    {
                                        appliedDiscountMaint=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalMaintPrice"]]floatValue];//maintenancePrice
                                    }
                                    //End
                                    
                                    
                                    
                                }
                                
                                break;
                            }
                            
                        }
                    }
                    
                    chkForSaveCoupon=YES;
                    
                }
            }
        }
        [context save:&error1];
    }
    [self fetchFromCoreDataStandard];
    [_tblRecord reloadData];
    _txtApplyDiscount.text=@"";
}
-(BOOL)checkForAppliedDiscountFromCoreData:(NSString *)strDiscoutCode : (NSString *)strDiscountUsageType
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    
    NSPredicate *predicate;
    
    if ([strDiscountUsageType caseInsensitiveCompare:@"OneTime"]==NSOrderedSame)
    {
        predicate =[NSPredicate predicateWithFormat:@"accountNo=%@ && discountCode=%@",strAccountNoGlobal,strDiscoutCode];
    }
    else if([strDiscountUsageType caseInsensitiveCompare:@"Multiple"]==NSOrderedSame)
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && discountCode=%@",strLeadId,strDiscoutCode];
    }
    else
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && discountCode=%@",strLeadId,strDiscoutCode];
    }
    
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    // NSManagedObject *matchesDiscount;
    
    //One Time
    if ([strDiscountUsageType caseInsensitiveCompare:@"OneTime"]==NSOrderedSame)
    {
        if (arrAllObjSales.count==0)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    //Multiple
    if ([strDiscountUsageType caseInsensitiveCompare:@"Multiple"]==NSOrderedSame)
    {
        if (arrAllObjSales.count==0)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
}
-(BOOL)checkForAppliedDiscountFromCoreDataTemp:(NSString *)strDiscoutCode
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    if (arrAllObjSales.count==0)
    {
        return NO;
    }
    else
    {
        BOOL chkDiscount=NO;
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if ([strDiscoutCode isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscount valueForKey:@"discountCode"]]])
            {
                chkDiscount=YES;
                break;
            }
            
            NSLog(@"Lead IDDDD====%@",[matchesDiscount valueForKey:@"leadId"]);
        }
        if (chkDiscount==YES)
        {
            /* if ([[matchesDiscount valueForKey:@"accountNo"] isEqualToString:strAccountNoGlobal])
             {
             if([strLeadId isEqualToString:[matchesDiscount valueForKey:@"leadId"]])
             {
             return YES;
             }
             else
             {
             if([[dictDiscountUsageFromSysName valueForKey:[matchesDiscount valueForKey:@"discountSysName"] ]caseInsensitiveCompare:@"OneTime"] == NSOrderedSame )
             {
             return YES;
             }
             else
             {
             return NO;
             }
             }
             
             }
             else
             {
             return NO;
             
             }*/
            return YES;
        }
        else
        {
            return NO;
        }
    }
}

-(void)fetchForAppliedDiscountFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    arrDiscountCoupon=[[NSMutableArray alloc]init];
    
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        //NSArray *uniqueArray = [[NSSet setWithArray:arrSoldServiceStandardIdForCoupon] allObjects];
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
            {
                [arrDiscountCoupon addObject:matchesDiscount];
            }
            
        }
    }
    [self heightForDiscountCoupon];
}
-(void)fetchForAppliedDiscountServiceFromCoreData:(NSString *)strServiceIdAppliedDiscount
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    arrDiscountCoupon=[[NSMutableArray alloc]init];
    
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"] && [[matchesDiscount valueForKey:@"soldServiceId"] isEqualToString:strServiceIdAppliedDiscount])
            {
                [arrDiscountCoupon addObject:matchesDiscount];
                break;
            }
            
        }
    }
    if(arrDiscountCoupon.count==0)
    {
        [self fetchForAppliedDiscountFromCoreData];
    }
    else
    {
        [self deleteAppliedCouponFromCoreDataSalesInfo:matchesDiscount:@""];
        [self heightForDiscountCoupon];
    }
}

-(void)saveAppliedDiscountCoreData:(NSDictionary *)dictForCoupon
{
    
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    
    LeadAppliedDiscounts *objLeadAppliedDiscounts = [[LeadAppliedDiscounts alloc]initWithEntity:entityLeadAppliedDiscounts insertIntoManagedObjectContext:context];
    objLeadAppliedDiscounts.leadAppliedDiscountId=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountSetupId"]];
    objLeadAppliedDiscounts.serviceSysName=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"ServiceSysName"]];
    objLeadAppliedDiscounts.discountSysName=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"SysName"]];
    objLeadAppliedDiscounts.discountType=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Type"]];
    objLeadAppliedDiscounts.discountCode=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountCode"]];
    objLeadAppliedDiscounts.discountAmount=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountAmount"]];
    objLeadAppliedDiscounts.discountDescription=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Description"]];
    
    if ([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"IsActive"]] isEqualToString:@"1"])
    {
        objLeadAppliedDiscounts.isActive=@"true";
        
    }
    else
    {
        objLeadAppliedDiscounts.isActive=@"false";
        
    }
    if ([[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"IsDiscountPercent"]] isEqualToString:@"1"])
    {
        objLeadAppliedDiscounts.isDiscountPercent=@"true";
        objLeadAppliedDiscounts.discountPercent=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountPercent"]];
        objLeadAppliedDiscounts.discountAmount=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"DiscountPercent"]];
    }
    else
    {
        objLeadAppliedDiscounts.isDiscountPercent=@"false";
        objLeadAppliedDiscounts.discountPercent=@"0";
        
        
    }
    objLeadAppliedDiscounts.soldServiceId=strAppliedDiscountServiceId;
    objLeadAppliedDiscounts.serviceType=@"Standard";
    objLeadAppliedDiscounts.name=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"Name"]];
    
    objLeadAppliedDiscounts.leadId=strLeadId;
    objLeadAppliedDiscounts.userName=strUserName;
    objLeadAppliedDiscounts.companyKey=strCompanyKey;
    
    objLeadAppliedDiscounts.appliedMaintDiscount=[NSString stringWithFormat:@"%.5f",appliedDiscountMaint];
    objLeadAppliedDiscounts.appliedMaintDiscount=@"0";
    
    objLeadAppliedDiscounts.appliedInitialDiscount=[NSString stringWithFormat:@"%.5f",appliedDiscountInitial];
    
    objLeadAppliedDiscounts.accountNo=strAccountNoGlobal;
    NSError *error1;
    [context save:&error1];
}
-(void)deleteAppliedCouponFromCoreDataSalesInfo:(NSManagedObject *)dictForCoupon :(NSString *)strDirect
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    NSString *strService=[dictForCoupon valueForKey:@"serviceSysName"];
    NSString *strServiceId=[dictForCoupon valueForKey:@"soldServiceId"];
    
    NSString *strDiscountCode=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"discountCode"]];
    NSString *strDiscountAmount=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"discountAmount"]];
    NSString *strDiscountPer=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"discountPercent"]];
    NSString *strDiscountSysName=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"discountSysName"]];
    NSString *strChkDiscountPer=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"isDiscountPercent"]];
    //13 June Temp
    strDiscountAmount=[NSString stringWithFormat:@"%@",[dictForCoupon valueForKey:@"appliedInitialDiscount"]];
    
    NSString *strServiceIdFinal;
    if([strDirect isEqualToString:@"strDirect"])
    {
        for (NSManagedObject * data in Data)
        {
            NSString *strSysNameServiceDataBase,*strCouponCodeDataBase;
            strSysNameServiceDataBase=[NSString stringWithFormat:@"%@",[data valueForKey:@"serviceSysName"]];
            strCouponCodeDataBase=[NSString stringWithFormat:@"%@",[data valueForKey:@"discountCode"]];
            /*if([strSysNameServiceDataBase isEqualToString:strService]&&[strCouponCodeDataBase isEqualToString:strDiscountCode])
             {
             [context deleteObject:data];
             break;
             }*/
            if([[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceId"]] isEqualToString:strServiceId] && [strDiscountSysName isEqualToString:[NSString stringWithFormat:@"%@",[data valueForKey:@"discountSysName"]]])
            {
                strServiceIdFinal=[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceId"]];
                [context deleteObject:data];
                break;
                
            }
        }
        
    }
    else
    {
        for (NSManagedObject * data in Data)
        {
            NSString *strSysNameServiceDataBase,*strCouponCodeDataBase;
            strSysNameServiceDataBase=[NSString stringWithFormat:@"%@",[data valueForKey:@"serviceSysName"]];
            strCouponCodeDataBase=[NSString stringWithFormat:@"%@",[data valueForKey:@"discountCode"]];
            
            if([[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceId"]] isEqualToString:strServiceId])
            {
                strServiceIdFinal=[NSString stringWithFormat:@"%@",[data valueForKey:@"soldServiceId"]];
                
                [context deleteObject:data];
                
            }
        }
        
        
    }
    NSError *saveError = nil;
    [context save:&saveError];
    [self fetchForAppliedDiscountFromCoreData];
    [_tblCouponDiscount reloadData];
    
    if (chkDirectServiceDelete==YES)
    {
        chkDirectServiceDelete=NO;
    }
    else
    {
        
        [self updateDiscountAfterDelete:strService DiscountAmount:strDiscountAmount DiscountPercent:strDiscountPer CheckForDiscountPer:strChkDiscountPer:strServiceIdFinal];
    }
    //[self updateDiscountAfterDelete:strService DiscountAmount:strDiscountAmount DiscountPercent:strDiscountPer];
}
-(void)updateDiscountAfterDelete:(NSString *)strServiceNameDiscount DiscountAmount: (NSString *)strDiscountAmount DiscountPercent:(NSString *)StrDiscPer CheckForDiscountPer:(NSString*)strChkForDiscountPer : (NSString *)strServiceIdFinal
{
     [self finalUpdatedOnSaveForTotalInitialAndMaintPrice];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND bundleId=%@",strLeadId,@"0"];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND soldServiceStandardId =%@",strLeadId,strServiceIdFinal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjDiscount = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscoutUpdate;
    if (arrAllObjDiscount.count==0)
    {
        
    }else
    {
        bool chkDiscountApplied;
        chkDiscountApplied=NO;
        BOOL chkForBundle=NO;
        for (int i=0; i<arrAllObjDiscount.count; i++)
        {
            matchesDiscoutUpdate=arrAllObjDiscount[i];
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                chkForBundle=NO;
            }
            else
            {
                chkForBundle=YES;
            }
            if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    
                    if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        // discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*discountPerCoupon)/100; //initialPrice
                        
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]; //initialPrice
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    
                }
                
            }
            else  //For Bundle
            {
                if ([strServiceNameDiscount isEqualToString:[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"serviceSysName"]]])
                {
                    
                    if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        // discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*discountPerCoupon)/100; //initialPrice
                        
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]; //initialPrice
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                        chkDiscountApplied=YES;
                        
                    }
                    
                }
                
                chkForBundle=YES;
                break;
                
            }
        }
        if(chkDiscountApplied==NO)
        {
            if (arrAllObjDiscount.count>0)
            {
                matchesDiscoutUpdate=arrAllObjDiscount[0];
                
                if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                {
                    chkForBundle=NO;
                }
                else
                {
                    chkForBundle=YES;
                }
                
                if (chkForBundle==NO)
                {
                    matchesDiscoutUpdate=arrAllObjDiscount[0];
                    
                    if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                    {
                        float discountPerCoupon=0, discount=0;
                        
                        discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                        
                        discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*discountPerCoupon)/100; //initialPrice
                        
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]; //initialPrice
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                    }
                    else
                    {
                        float discountPerCoupon=0, discount=0;
                        // strDiscountAmount=@"50";
                        discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                       
                        discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]; //initialPrice
                        
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                        [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                        
                    }
                    
                }
                // BUNDLE
                else
                {
                    for (int i=0; i<arrAllObjDiscount.count; i++)
                    {
                        matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                        
                        if ([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"bundleId"]]isEqualToString:@""])
                        {
                        }
                        else
                        {
                            matchesDiscoutUpdate=[arrAllObjDiscount objectAtIndex:i];
                            if ([strChkForDiscountPer isEqualToString:@"1"]||[strChkForDiscountPer caseInsensitiveCompare:@"true"] == NSOrderedSame)
                            {
                                float discountPerCoupon=0, discount=0;
                                
                                discountPerCoupon=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discountPercentage"]]floatValue]-[StrDiscPer floatValue];
                                
                                discount=([[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]*discountPerCoupon)/100; //initialPrice
 
                                discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]; //initialPrice
                                
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                            }
                            else
                            {
                                float discountPerCoupon=0, discount=0;
                                // strDiscountAmount=@"50";
                                discount=[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"discount"]]floatValue]-[strDiscountAmount floatValue];
                                if(discount<0)
                                {
                                    discount=0;
                                }
                              
                                discountPerCoupon=(discount*100)/[[NSString stringWithFormat:@"%@",[matchesDiscoutUpdate valueForKey:@"totalInitialPrice"]]floatValue]; //initialPrice
                                if(discountPerCoupon<0)
                                {
                                    discountPerCoupon=0;
                                }
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discount] forKey:@"discount"];
                                [matchesDiscoutUpdate setValue:[NSString stringWithFormat:@"%.5f",discountPerCoupon] forKey:@"discountPercentage"];
                                
                            }
                        }
                    }
                }
                
                
                
                
            }
        }
        
        [context save:&error1];
    }
    [self fetchFromCoreDataStandard];
    [_tblRecord reloadData];
    _txtApplyDiscount.text=@"";
}
-(void)fetchFromCoreDataStandardForBundleForDiscountCupon:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrSoldServiceStandardIdBundle=[[NSMutableArray alloc]init];
    
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                
                [arrSoldServiceStandardIdBundle addObject:[matches valueForKey:@"soldServiceStandardId"]];
                
            }
        }
    }
    // [self heightManage];
}
-(BOOL)discountValidityCheck:(NSDictionary*)dict
{
    //Discount Validity Check
    BOOL chkValidity;
    chkValidity=NO;
    NSString *strCurrentDate,*strValidFrom,*strValidTo;
    NSDate *dateCurrent,*dateValidFrom,*dateValidTo;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    
    strCurrentDate = [formatter stringFromDate:[NSDate date]];
    
    strValidFrom=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ValidFrom"]];
    dateCurrent=[formatter dateFromString:strCurrentDate];
    
    
    
    strValidFrom=[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:strValidFrom];
    dateValidFrom=[formatter dateFromString:strValidFrom];
    
    strValidTo=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ValidTo"]];
    
    strValidTo=[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:strValidTo];
    dateValidTo=[formatter dateFromString:strValidTo];
    
    
    
    NSComparisonResult result = [dateValidFrom compare:dateCurrent];
    
    if(result==NSOrderedAscending || result== NSOrderedSame )
    {
        NSComparisonResult result2 = [dateValidTo compare:dateCurrent];
        if (result2==NSOrderedDescending|| result2==NSOrderedSame)
        {
            NSLog(@"valid B");
            chkValidity=YES;
        }
    }
    //End
    return chkValidity;
    
}
-(void)updateLeadAppliedDiscountForAppliedCoupon
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    
    if (arrAllObjSales.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            for (int j=0; j<arrSoldServiceIdOfAllService.count; j++)
            {
                if ([[matchesDiscount valueForKey:@"soldServiceId"] isEqualToString:[arrSoldServiceIdOfAllService objectAtIndex:j]])
                {
                    [matchesDiscount setValue:@"true" forKey:@"isApplied"];
                }
                
            }
        }
    }
    [context save:&error1];
}
-(void)updateLeadAppliedDiscountForCredit
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    
    if (arrAllObjSales.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if([[matchesDiscount valueForKey:@"discountType"]isEqualToString:@"Credit"]||[[matchesDiscount valueForKey:@"discountType"]isEqualToString:@"credit"])
            {
                
                if(arrSoldServiceIdOfAllService.count>0)
                {
                    [matchesDiscount setValue:[arrSoldServiceIdOfAllService objectAtIndex:0] forKey:@"soldServiceId"];
                }
                
                /*for (int j=0; j<arrAllUnsoldServiceId.count; j++)
                 {
                 if(arrSoldServiceIdOfAllService.count>0)
                 {
                 if ([[matchesDiscount valueForKey:@"soldServiceId"] isEqualToString:[arrAllUnsoldServiceId objectAtIndex:j]])
                 {
                 [matchesDiscount setValue:[arrSoldServiceIdOfAllService objectAtIndex:0] forKey:@"soldServiceId"];
                 }
                 }
                 
                 }*/
            }
        }
    }
    [context save:&error1];
}

-(void)deleteLeadAppliedDiscountFromCoreData
{
    //Delete LeadAppliedDiscounts Data
    
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    
    NSFetchRequest *allDataLeadAppliedDiscounts = [[NSFetchRequest alloc] init];
    [allDataLeadAppliedDiscounts setEntity:[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context]];
    
    NSPredicate *predicateLeadAppliedDiscounts =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    
    [allDataLeadAppliedDiscounts setPredicate:predicateLeadAppliedDiscounts];
    [allDataLeadAppliedDiscounts setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorLeadAppliedDiscounts= nil;
    NSArray * DataLeadAppliedDiscounts = [context executeFetchRequest:allDataLeadAppliedDiscounts error:&errorLeadAppliedDiscounts];
    //error handling goes here
    for (NSManagedObject * data in DataLeadAppliedDiscounts) {
        [context deleteObject:data];
    }
    NSError *saveErrorLeadAppliedDiscounts = nil;
    [context save:&saveErrorLeadAppliedDiscounts];
}
//......................................................

// Saavan Changes 9 july 2018

-(NSArray*)fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot :(NSString*)strType
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    NSMutableArray *arrDiscountCouponNew=[[NSMutableArray alloc]init];
    NSMutableArray *arrDiscountCreditNew=[[NSMutableArray alloc]init];
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            for (int j=0; j<arrSoldServiceStandardId.count; j++)
            {
                NSString *strId=[arrSoldServiceStandardId objectAtIndex:j];
                if ([strId isEqualToString:[matchesDiscount valueForKey:@"soldServiceId"]])
                {
                    if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
                    {
                        [arrDiscountCouponNew addObject:matchesDiscount];
                    }
                    else
                    {
                        [arrDiscountCreditNew addObject:matchesDiscount];
                        
                    }
                    
                    
                }
                // [arrDiscountCoupon addObject:matchesDiscount];
            }
        }
    }
    
    NSMutableArray *arrTempToReturn=[[NSMutableArray alloc]init];
    
    //if ([strType isEqualToString:@"Credit"]) {
    
    if (arrDiscountCreditNew.count>0) {
        
        [arrTempToReturn addObjectsFromArray:arrDiscountCreditNew];
        
    }
    
    
    //} else {
    
    if (arrDiscountCouponNew.count>0) {
        
        [arrTempToReturn addObjectsFromArray:arrDiscountCouponNew];
        
    }
    
    // }
    
    NSArray *arrTemopNew=[[NSArray alloc]init];
    
    arrTemopNew=arrTempToReturn;
    
    return arrTemopNew;
    
}

- (IBAction)actionOnChemicalSensitivityList:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                 bundle: nil];
        ChemicalSensitivityList
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChemicalSensitivityList"];
        objByProductVC.strFrom=@"sales";
        objByProductVC.strId=strLeadId;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
    //End
    
}
-(void)fetchForAppliedDiscountFromCoreDataFromBack
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    arrDiscountCoupon=[[NSMutableArray alloc]init];
    
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        
        
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
            {
                for (int l=0; l<arrSoldServiceStandardId.count; l++)
                {
                    if ([[arrSoldServiceStandardId objectAtIndex:l]isEqualToString:[matchesDiscount valueForKey:@"soldServiceId"]])
                    {
                        [arrDiscountCoupon addObject:matchesDiscount];
                    }
                }
                //[arrDiscountCoupon addObject:matchesDiscount];
            }
            
        }
        
    }
    [self heightForDiscountCoupon];
    
    
}
-(void)newAlertMethodAddImage
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              NSLog(@"The CApture Image.");
                              
                              
                              if (arrNoImage.count<10)
                              {
                                  NSLog(@"The CApture Image.");
                                  
                                  BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                  
                                  if (isCameraPermissionAvailable) {
                                      
                                      if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                                      {
                                          
                                          [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                                          
                                      }else{
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }
                                  }else{
                                      
                                      UIAlertController *alert= [UIAlertController
                                                                 alertControllerWithTitle:@"Alert"
                                                                 message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                      
                                      UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action)
                                                            {
                                                                
                                                                
                                                                
                                                            }];
                                      [alert addAction:yes];
                                      UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                   NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                   [[UIApplication sharedApplication] openURL:url];
                                                               } else {
                                                                   
                                                               }
                                                           }];
                                      [alert addAction:no];
                                      [self presentViewController:alert animated:YES completion:nil];
                                  }
                                  
                              }
                              else
                              {
                                  chkForDuplicateImageSave=YES;
                                  UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                  [alert show];
                              }
                              //............
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             NSLog(@"The Gallery.");
                             if (arrNoImage.count<10)
                             {
                                 BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                 
                                 if (isCameraPermissionAvailable) {
                                     
                                     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                                     {
                                         
                                         [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                                         
                                     }else{
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }
                                 }else{
                                     
                                     UIAlertController *alert= [UIAlertController
                                                                alertControllerWithTitle:@"Alert"
                                                                message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                preferredStyle:UIAlertControllerStyleAlert];
                                     
                                     UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               
                                                               
                                                           }];
                                     [alert addAction:yes];
                                     UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                handler:^(UIAlertAction * action)
                                                          {
                                                              
                                                              if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                  NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                  [[UIApplication sharedApplication] openURL:url];
                                                              }
                                                              else
                                                              {
                                                                  
                                                                  
                                                                  
                                                              }
                                                              
                                                          }];
                                     [alert addAction:no];
                                     [self presentViewController:alert animated:YES completion:nil];
                                 }
                             }
                             else
                             {
                                 chkForDuplicateImageSave=YES;
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                 [alert show];
                             }
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)actionOnSendProposal:(id)sender
{
     [self endEditing];
    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))    {
        
    }
    else
    {
        
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            
        }
        else
        {
            [self saveImageToCoreData];
            
            [self chekcBox];
            [self fetchFromCoreDataStandard];
            [self fetchFromCoreDataStandardForProposal];
            [self fetchFromCoreDataNonStandardForProposal];
        }
        
        
        if (arrStanProposal.count>0 || arrNonStanProposal.count>0)
        {
            [global updateSalesZSYNC:strLeadId :@"yes"];
            
            if(isEditedInSalesAuto==YES)
            {
                NSLog(@"Global modify date called in Select Service");
                [global updateSalesModifydate:strLeadId];
            }
            if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
            {
                
            }
            else
            {
                [self finalUpdatedOnSaveForTotalInitialAndMaintPrice];
                [self finalUpdatedNonStanOnSaveForBillilngFreqPrice];
                [self updateLeadAppliedDiscountForAppliedCoupon];
                
                
                //temp
                [self updateLeadAppliedDiscountForCredit];
                [self updateLeadIdDetailForProposal];
                
            }
            
            UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
            SalesAutomationAgreementProposaliPad *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"SalesAutomationAgreementProposaliPad"];
            objSalesAutomationAgreementProposal.strSummarySendPropsal=@"strSummarySendPropsal";
            
            [self.navigationController pushViewController:objSalesAutomationAgreementProposal animated:YES];
            
            NSUserDefaults *defs= [NSUserDefaults standardUserDefaults];
            [defs setBool:YES forKey:@"fromSelectServiceProposal"];
            [defs synchronize];
        }
        else
        {
            [global displayAlertController:@"Alert" :@"To proceed, please propose atleast one service" :self];
        }
    }
}


#pragma mark- ---------- For Proposal Check-------------
-(void)fetchFromCoreDataStandardForProposal
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"false"];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrStanProposal=[[NSArray alloc]init];
    arrStanProposal = [self.fetchedResultsControllerSalesInfo fetchedObjects];
}
-(void)fetchFromCoreDataNonStandardForProposal
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate ;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"false"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrNonStanProposal=[[NSArray alloc]init];
    arrNonStanProposal = [self.fetchedResultsControllerSalesInfo fetchedObjects];
}

#pragma mark- ---------- For Sold Check-------------

-(void)fetchFromCoreDataStandardForSold
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrSoldAvailableStan=[[NSArray alloc]init];
    arrSoldAvailableStan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
}
-(void)fetchFromCoreDataNonStandardForSold
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate ;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrSoldAvailableNonStan=[[NSArray alloc]init];
    arrSoldAvailableNonStan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
}
-(void)bundleCheck
{
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Please Choose" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bundled Services" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self gotToAddStandardService:@"Bundle"];
        // [self dismissViewControllerAnimated:YES completion:^{
        //}];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Standard Services" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self gotToAddStandardService:@"Standard Services"];
        // [self dismissViewControllerAnimated:YES completion:^{
        // }];
    }]];
    
    /*  [actionSheet addAction:[UIAlertAction actionWithTitle:@"Non Standard Service" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
     
     [self goToNonStandardView];
     
     }]];*/
    
    
    
    // Present action sheet.
    //[self presentViewController:actionSheet animated:YES completion:nil];
    
    
    UIPopoverPresentationController *popPresenter = [actionSheet
                                                     popoverPresentationController];
    popPresenter.sourceView = btnAddRecord;
    popPresenter.sourceRect = btnAddRecord.bounds;
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(void)gotToAddStandardService : (NSString *)serviceType
{
    //Stan Action Code
    
    NSUserDefaults *defsStanClick = [NSUserDefaults standardUserDefaults];
    [defsStanClick setBool:YES forKey:@"clickStan"];
    [defsStanClick synchronize];
    
    
    [_btnNonStandardService setBackgroundColor:[UIColor lightGrayColor]];
    [_btnStandardService setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    _viewForNonStandardService.hidden=YES;
    _viewForStandardService.hidden=NO;
    chkClickStan=YES;
    [self fetchFromCoreDataStandard];
    [self heightManage];
    
    //
    
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
    AddStandardServiceiPad *objAddStandardService=[storyBoard instantiateViewControllerWithIdentifier:@"AddStandardServiceiPad"];
    objAddStandardService.serviceType = serviceType;
    [self.navigationController presentViewController:objAddStandardService animated:YES completion:nil];
}
-(void)goToNonStandardView
{
    // Non Stan Code
    NSUserDefaults *defsStanClick = [NSUserDefaults standardUserDefaults];
    [defsStanClick setBool:NO forKey:@"clickStan"];
    [defsStanClick synchronize];
    
    [self discountStatusNonStan];
    [_btnStandardService setBackgroundColor:[UIColor lightGrayColor]];
    [_btnNonStandardService setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    
    [self fetchFromCoreDataNonStandard];
    
    //    [_btnStandardService setBackgroundColor:[UIColor lightGrayColor]];
    //    [_btnNonStandardService setBackgroundColor:[UIColor colorWithRed:236.0f/255 green:27.0f/255 blue:33.0f/255 alpha:1]];
    
    chkClickStan=NO;
    _viewForNonStandardService.hidden=NO;
    _viewForStandardService.hidden=YES;
    
    
    
    //Non Standard
    int sumNonStan=0;
    for (int i=0; i<arrNonStanServiceName.count; i++)
    {
        sumNonStan=sumNonStan+_tblNonStandardService.rowHeight;
    }
    _const_TableNonStan_H.constant=sumNonStan+50+50;
    [_scrollViewService setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,_const_TableNonStan_H.constant+100)];
    
    //
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backStan"];
    [defs synchronize];
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
    AddNonStandardServicesiPad *objAddNonStandardServices=[storyBoard instantiateViewControllerWithIdentifier:@"AddNonStandardServicesiPad"];
    [self.navigationController presentViewController:objAddNonStandardServices animated:YES completion:nil];
}
-(void)endEditing
{
    [self.view endEditing:YES];
}
-(void)goToGlobalmage:(NSString *)strType
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"PestiPhone" bundle:nil];
    GlobalImageVC *objGlobalImageVC=[storyBoard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objGlobalImageVC.strHeaderTitle = strType;
    objGlobalImageVC.strWoId = strLeadId;
    objGlobalImageVC.strWoLeadId = strLeadId;
    objGlobalImageVC.strModuleType = @"SalesFlow";
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Won"] == NSOrderedSame  )
    {
        
        objGlobalImageVC.strWoStatus = @"Complete";
        
    }
    else
    {
        objGlobalImageVC.strWoStatus = @"InComplete";
        
    }
    [self.navigationController presentViewController:objGlobalImageVC animated:NO completion:nil];
    
}

#pragma mark- ----------Textview Delegate Method-----------

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    
    if([text isEqualToString:@"\n"])
    {
        NSInteger row = textView.tag;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        RenewalServiceTableViewCell *tappedCell = (RenewalServiceTableViewCell *)[_tblRenewalService cellForRowAtIndexPath:indexPath];
        [tappedCell.txtViewRenewalServiceDesc resignFirstResponder];
        [txtFieldCaption resignFirstResponder];
        [txtViewImageDescription resignFirstResponder];
        
        NSString *strRenewalServiceId;
        NSManagedObject *matchesTemp = [arrAllObjRenewal objectAtIndex:indexPath.row];
        strRenewalServiceId = [NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"soldServiceStandardId"]];
        [self updateRenewaData:strRenewalServiceId RenewalAmount:@"" RenewalDescription:tappedCell.txtViewRenewalServiceDesc.text UpdateType:@"description"];
        
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    // [self.view setFrame:CGRectMake(0,-150,self.view.frame.size.width,self.view.frame.size.height)];
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
   
    NSInteger row = textView.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    RenewalServiceTableViewCell *tappedCell = (RenewalServiceTableViewCell *)[_tblRenewalService cellForRowAtIndexPath:indexPath];
    
    if (textView == tappedCell.txtViewRenewalServiceDesc)
    {
        [tappedCell.txtViewRenewalServiceDesc resignFirstResponder];
        
        NSString *strRenewalServiceId;
        NSManagedObject *matchesTemp = [arrAllObjRenewal objectAtIndex:indexPath.row];
        strRenewalServiceId = [NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"soldServiceStandardId"]];
        [self updateRenewaData:strRenewalServiceId RenewalAmount:@"" RenewalDescription:tappedCell.txtViewRenewalServiceDesc.text UpdateType:@"description"];
    }
    
}
-(NSString *)convertDecimalToString:(NSString *)strValue
{
    float tempPrice = [strValue floatValue];
    
    return [NSString stringWithFormat:@"%.02f",tempPrice];

}
-(NSInteger)getBundleCount:(NSString *)strBundleId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND bundleId=%@",strLeadId,strBundleId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arrBundleCount = [[NSArray alloc]init];
    arrBundleCount = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    return arrBundleCount.count;
}

#pragma mark: ------- Renewal Service Core Data Method------------

-(void)fetchRenewalServiceFromCoreData
{
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityRenewalSerivceDetail=[NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
    
    requestNew = [[NSFetchRequest alloc] init];
    
    [requestNew setEntity:entityRenewalSerivceDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrAllObjRenewal = [self.fetchedResultsControllerSalesInfo fetchedObjects];

    if (arrAllObjRenewal.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjRenewal.count; k++)
        {
            
        }
    }
    [_tblRenewalService reloadData];
}

-(void)updateRenewaData: (NSString *)strRenewalServiceId RenewalAmount : (NSString *)strRenewalAmount RenewalDescription : (NSString *)strRenewalDesc UpdateType : (NSString *)strUpdateType
{
    if ([strRenewalAmount isEqualToString:@""])
    {
        //strRenewalAmount = @"0";
    }

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityRenewalSerivceDetail= [NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
    
    [request setEntity:entityRenewalSerivceDetail];

    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceStandardId=%@ && userName=%@",strLeadId,strRenewalServiceId,strUserName];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjRenewalTemp = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSManagedObject *matchesRenewalTemp;
    if (arrAllObjRenewalTemp.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObjRenewalTemp.count; i++)
        {
            matchesRenewalTemp = [arrAllObjRenewalTemp objectAtIndex:i];
            
            if ([strUpdateType isEqualToString:@"amount"])
            {
                [matchesRenewalTemp setValue:strRenewalAmount forKey:@"renewalAmount"];
            }
            if ([strUpdateType isEqualToString:@"description"])
            {
                [matchesRenewalTemp setValue:strRenewalDesc forKey:@"renewalDescription"];
            }
            
            /*if (strRenewalAmount.length > 0)
            {
                [matchesRenewalTemp setValue:strRenewalAmount forKey:@"renewalAmount"];
            }
            if (strRenewalDesc.length > 0)
            {
                [matchesRenewalTemp setValue:strRenewalDesc forKey:@"renewalDescription"];
            }*/
            
        }
    }
    
    [context save:&error1];
    
    [self fetchFromCoreDataStandard];

}
/*-(void)deleteRenewalFromCoreDataSalesInfo:(NSManagedObject *)dataRenewal
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    [context deleteObject:dataRenewal];
    
    NSError *saveError = nil;
    [context save:&saveError];
    [self fetchFromCoreDataStandard];
    

}*/

-(void)deleteRenewalFromCoreDataSalesInfo: (NSString *)strSoldServiceStandardId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceStandardId=%@",strLeadId,strSoldServiceStandardId];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context]];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    [self heightManage];
}


-(void)updateRenewalServiceWithId:(NSString *)strSoldServiceId ServiceMasterId:(NSString *)strServiceMasterId RenewalAmount:(NSString *)strRenewalAmount
{
    if ([strRenewalAmount isEqualToString:@""])
    {
       // strRenewalAmount = @"0";
    }

    if ([self checkForRenewalServicePricePercentageBased:strServiceMasterId] == YES)
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        entityRenewalSerivceDetail=[NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
        
        requestNew = [[NSFetchRequest alloc] init];
        [requestNew setEntity:entityRenewalSerivceDetail];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceStandardId=%@ && serviceId=%@",strLeadId,strSoldServiceId,strServiceMasterId];
        
        [requestNew setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [requestNew setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        NSArray *arrRenewal = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        NSManagedObject *matchesRenewal;
        if (arrRenewal.count==0)
        {
            
        }else
        {
            for (int i=0; i<arrRenewal.count; i++)
            {
                matchesRenewal=arrRenewal[i];
                
                float price = 0.0;
                float per = [[NSString stringWithFormat:@"%@",[matchesRenewal valueForKey:@"renewalPercentage"]]floatValue];
                
                price = ([strRenewalAmount floatValue]*per/100);
                
                if (price < [[NSString stringWithFormat:@"%@",[dictRenewalMaster valueForKey:@"MinimumPrice"]]floatValue])
                {
                    price = [[NSString stringWithFormat:@"%@",[dictRenewalMaster valueForKey:@"MinimumPrice"]]floatValue];
                }
                [matchesRenewal setValue:[NSString stringWithFormat:@"%.2f",price] forKey:@"renewalAmount"];
                
            }
            [context save:&error1];
        }
        [self fetchRenewalServiceFromCoreData];
    }
}
-(BOOL)checkForRenewalServicePricePercentageBased : (NSString *)strServiceId
{
    BOOL chk = NO;
    BOOL find = NO;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        
        NSArray *arrServices=[dict valueForKey:@"Services"];
        
        for (int j=0; j<arrServices.count; j++)
        {
            NSDictionary *dictService = [arrServices objectAtIndex:j];
            
            NSArray *arrRenewalData = [dictService valueForKey:@"ServiceMasterRenewalPrices"];
            
            for (int k = 0; k<arrRenewalData.count; k++)
            {
                NSDictionary *dictRenewal = [arrRenewalData objectAtIndex:k];
                if([[NSString stringWithFormat:@"%@",[dictRenewal valueForKey:@"ServiceMasterId"]] isEqualToString:strServiceId])
                {
                    find = YES;
                    dictRenewalMaster = dictRenewal;
                    if ([[NSString stringWithFormat:@"%@",[dictRenewal valueForKey:@"PriceBasedOn"]] isEqualToString:@"Percentage"])
                    {
                        chk = YES;
                    }
                    break;
                    
                }
            }
            
            if (find)
            {
                break;
            }
        }
        if (find)
        {
            break;
        }
    }
    
    return chk;
}

-(void)attachImage:(UIButton *)btn
{
    [self endEditing];
   // [self goToGlobalmage:@"Before"];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* addImage = [UIAlertAction actionWithTitle:@"Add Images" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
        
        [self goToGlobalmage:@"Before"];
        
    }];
    
    UIAlertAction* addDocument = [UIAlertAction actionWithTitle:@"Add Documents" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        
        [self goToUploadDocument];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addImage];
    [alert addAction:addDocument];
    [alert addAction:dismiss];
    alert.popoverPresentationController.sourceView = btn;
    [self presentViewController:alert animated:YES completion:nil];
  
}
-(void)goToUploadDocument
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
    UploadDocumentSales *objUploadDocumentSales=[storyBoard instantiateViewControllerWithIdentifier:@"UploadDocumentSalesiPad"];
    objUploadDocumentSales.strWoId = strLeadId;

    [self.navigationController presentViewController:objUploadDocumentSales animated:NO completion:nil];
    
}
//06 Aug 2020

-(void)updateLeadAppliedDiscountOnUnitUpdate: (NSString *)strServiceId
{
    [self getCouponObjectOnUnitChange:strServiceId];
}
-(void)getCouponObjectOnUnitChange : (NSString *)strServiceId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND soldServiceId=%@",strLeadId,strServiceId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    
    if (arrAllObjSales.count == 0 )
    {
        
    }
    else
    {
        for (int i=0; i<arrAllObjSales.count;i++)
        {
            matchesDiscount = [arrAllObjSales objectAtIndex:i];
            NSString *strDiscountName = [matchesDiscount valueForKey:@"discountCode"];
            [self deleteAppliedCouponFromCoreDataSalesInfo:matchesDiscount:@"strDirect"];
            [self applyDiscount:strDiscountName];
        }
    }
    
   
}
-(void)applyDiscount : (NSString *)strDiscountName
{
   //  [self endEditing];
    [self fetchFromCoreDataStandard];
    if(strDiscountName.length==0)
    {
        [global AlertMethod:@"Alert!" :@"Please enter coupon code"];
        _txtApplyDiscount.text=@"";
    }
    else if(arrAllObj.count==0)
    {
        
        [global AlertMethod:@"Alert!" :@"Please add service first to apply coupon"];
        _txtApplyDiscount.text=@"";
        
    }

    else
    {
        arrDiscountCoupon=[[NSMutableArray alloc]init];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        arrDiscountCoupon=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
        if([arrDiscountCoupon isKindOfClass:[NSArray class]])
        {
            arrTempCoupon=[[NSMutableArray alloc]init];
            
            NSString *strCouponDiscount=  strDiscountName;//_txtApplyDiscount.text;
            
            //14 June Temp
            NSString *strDiscountUsage=[dictDiscountUsageFromCode valueForKey:strCouponDiscount];
            
            BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount:strDiscountUsage];
            
            // BOOL chkDiscountApplied=[self checkForAppliedDiscountFromCoreData:strCouponDiscount];
            if (chkDiscountApplied==YES)
            {
                [global AlertMethod:@"Alert!" :@"Coupon already applied"];
            }
            else
            {
                BOOL chkValidDiscount;
                chkValidDiscount=NO;
                for(int i=0;i<arrDiscountCoupon.count;i++)
                {
                    NSDictionary *dict=[arrDiscountCoupon objectAtIndex:i];
                    
                    if ([strCouponDiscount isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountCode"]]])
                    {
                        NSLog(@"Matched Discount");
                        
                        //Discount Validity Check
                        
                        chkValidDiscount=[self discountValidityCheck:dict];
                        if (chkValidDiscount==YES)
                        {
                            NSLog(@"VALID DISCOUNT");
                            
                            
                            appliedDiscountMaint=0.0;
                            appliedDiscountInitial=0.0;
                            
                            
                            NSArray *arrTempCreditAlreadyApplied=[self fetchAppliedDiscountFromCoreDataToCheckIfToApplyOrNot:@"Coupon"];
                            
                            float totalDiscountAmoutAlreadyPresent=0.0;
                            
                            for (int k1=0; k1<arrTempCreditAlreadyApplied.count; k1++) {
                                
                                NSManagedObject *objTemp=arrTempCreditAlreadyApplied[k1];
                                
                                NSString *strDiscountAmountPresent=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"appliedInitialDiscount"]];
                                
                                float discountAmountPresentt=[strDiscountAmountPresent floatValue];
                                
                                if([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"applicableForMaintenance"] ]isEqualToString:@"true"])
                                {
                                    
                                    
                                }else{
                                    
                                    totalDiscountAmoutAlreadyPresent=totalDiscountAmoutAlreadyPresent+discountAmountPresentt;
                                    
                                }
                                
                            }
                            
                            BOOL ifToSaveOrNot;
                            
                            ifToSaveOrNot=NO;
                            
                            NSString *strDiscountAmountTemp=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]];
                            
                            float discount=[strDiscountAmountTemp floatValue];
                            
                            NSString *strIsDiscountTypeNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
                            
                            if ([strIsDiscountTypeNew isEqualToString:@"1"] || [strIsDiscountTypeNew isEqualToString:@"true"] || [strIsDiscountTypeNew isEqualToString:@"True"]) {
                                
                                NSString *strDiscountPercenteNew=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]];
                                
                                float discountPercentValue=[strDiscountPercenteNew floatValue];
                                
                                if (discountPercentValue==100 || discountPercentValue>100)
                                {
                                    discountPercentValue=100.00;
                                    
                                }
                                
                                discount=(globalAmountInitialPrice*discountPercentValue)/100;
                                
                            }
                            
                            
                            if (!(globalAmountInitialPrice>=(totalDiscountAmoutAlreadyPresent+discount))) {
                                
                                
                                if (globalAmountInitialPrice<(totalDiscountAmoutAlreadyPresent+discount)) {
                                    
                                    float amountDiscountToApplyCredit=globalAmountInitialPrice-totalDiscountAmoutAlreadyPresent;
                                    
                                    NSMutableDictionary *dictTempToSave=[[NSMutableDictionary alloc]init];
                                    
                                    [dictTempToSave addEntriesFromDictionary:dict];
                                    
                                    [dictTempToSave setValue:[NSString stringWithFormat:@"%f",amountDiscountToApplyCredit] forKey:@"DiscountAmount"];
                                    
                                    dict=dictTempToSave;
                                    
                                    ifToSaveOrNot=YES;
                                    
                                    if (amountDiscountToApplyCredit>0) {
                                        
                                        ifToSaveOrNot=YES;
                                        appliedDiscountInitial=amountDiscountToApplyCredit;
                                        
                                        
                                    }else{
                                        
                                        ifToSaveOrNot=NO;
                                        appliedDiscountInitial=0.0;
                                        
                                        
                                    }
                                    
                                }
                            }else{
                                
                                ifToSaveOrNot=YES;
                                
                            }
                            
                            
                            if (ifToSaveOrNot) {
                                
                                NSString *strDiscountPerCheck=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDiscountPercent"]];
                                if ([strDiscountPerCheck isEqualToString:@"1"]||[strDiscountPerCheck caseInsensitiveCompare:@"true"] == NSOrderedSame)
                                {
                                    [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :@"0" CheckForDiscountPer:@"1" DiscountPerValue:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountPercent"]]];
                                }
                                else
                                {
                                    [self finalUpdatedTableDataWithBundleDiscountNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]] :[NSString stringWithFormat:@"%@",[dict valueForKey:@"DiscountAmount"]]CheckForDiscountPer:@"0" DiscountPerValue:@"0"];
                                }
                                if (chkForSaveCoupon==YES)
                                {
                                    [self saveAppliedDiscountCoreData:dict];
                                    [self fetchForAppliedDiscountFromCoreData];
                                    
                                }
                                
                            }else{
                                
                                [global AlertMethod:Alert :@"Coupon can not be more than billing amount."];
                                
                            }
                            NSLog(@"VALID DISCOUNT");
                            
                        }
                        else
                        {
                            NSLog(@"INVALID DISCOUNT");
                            //[global AlertMethod:@"Alert" :@"No valid coupon"];
                            
                        }
                        //[self heightManage];
                        break;
                        
                    }
                    else
                    {
                        chkValidDiscount=NO;
                    }
                    
                }
                if(chkValidDiscount==NO)// && chkForSaveCoupon==NO)
                {
                    [global AlertMethod:@"Alert" :@"Invalid coupon"];
                    
                }
                else
                {
                    if(chkForSaveCoupon==NO)
                    {
                        [global AlertMethod:@"Alert" :@"Invalid coupon"];
                        
                    }
                }
              
                
            }
        }
        
        
    }
}
-(BOOL)chkAppliendCouponCountOnService : (NSString *)strServiceId
{
    BOOL chk;
    
    chk =  NO;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND soldServiceId=%@",strLeadId,strServiceId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjSales.count == 0 )
    {
        chk = NO;
    }
    else
    {
        chk = YES;
    }
    
    return chk;
   
}
#pragma mark- ---------- NonStandard Taxable Code -----------

-(void)buttonClickedTaxableNonStandard:(UIButton*)button
{
    NSString *strStatus = [arrTaxStatusNonStan objectAtIndex:button.tag];
    NSString *strId = [arrSoldServiceNonStandId objectAtIndex:button.tag];
    if ([strStatus isEqualToString:@"true"])
    {
        [self updateNonStandardTaxableStatus:strId :@"false"];
    }
    else
    {
        [self updateNonStandardTaxableStatus:strId :@"true"];

    }
    [self fetchFromCoreDataNonStandard];

}

-(void)updateNonStandardTaxableStatus:(NSString *)strNonStanId : (NSString *)strStatus
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceNonStandardId=%@",strLeadId,strNonStanId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjTaxNonStan = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesTaxNonStan;
    if (arrAllObjTaxNonStan.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObjTaxNonStan.count; i++)
        {
            matchesTaxNonStan=arrAllObjTaxNonStan[i];
            [matchesTaxNonStan setValue:strStatus forKey:@"isTaxable"];
            
        }
        [context save:&error1];
    }
    
}
#pragma mark: ------------- Email & Service History -------------

-(void)goToEmailHistory
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSString *strLeadNo = [defs valueForKey:@"leadNumber"];
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"CRM_New_iPad" bundle:nil];
    EmailListNew_iPhoneVC *objEmailListNew_iPhoneVC=[storyBoard instantiateViewControllerWithIdentifier:@"EmailListNew_iPhoneVC"];
    objEmailListNew_iPhoneVC.refNo = strLeadNo;
    [self.navigationController pushViewController:objEmailListNew_iPhoneVC animated:NO];
    //[self.navigationController presentViewController:objEmailListNew_iPhoneVC animated:YES completion:nil];
}
-(void)goToServiceHistory
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceNotesHistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
    objByProductVC.strTypeOfService=@"sales";
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
}
-(void)goToHistory
{
    [self endEditing];
   // [self goToGlobalmage:@"Before"];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* addServiceHistory = [UIAlertAction actionWithTitle:@"Service History" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
        
        [self goToServiceHistory];
        
    }];
    
    UIAlertAction* addEmailHistory = [UIAlertAction actionWithTitle:@"Email History" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        
        [self goToEmailHistory];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addServiceHistory];
    [alert addAction:addEmailHistory];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
  
}
- (IBAction)actionOnAddService:(id)sender
{
    [self addAction];
}
-(void)updateLeadIdDetailForProposal
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObjNew.count==0)
    {
        
    }else
    {
        NSManagedObject *matchesNew;
        matchesNew=arrAllObjNew[0];
        [matchesNew setValue:@"false" forKey:@"isCustomerNotPresent"];
            
    }
    [context save:&error1];
}

#pragma  mark: ---------- Service Edit ----------

-(NSManagedObject *)fetchFromCoreDataStandardForUpdate:(NSString *)strSoldServiceId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND soldServiceStandardId=%@",strLeadId,strSoldServiceId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arr = [[NSArray alloc]init];
    
    arr = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSManagedObject *matchUpdate;
    
    matchUpdate = [arr objectAtIndex:0];
    
    return matchUpdate;
    
}
-(NSManagedObject *)fetchFromCoreDataNonStandardForUpdate:(NSString *)strSoldServiceId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND soldServiceNonStandardId=%@",strLeadId,strSoldServiceId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arr = [[NSArray alloc]init];
    
    arr = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSManagedObject *matchUpdate;
    
    matchUpdate = [arr objectAtIndex:0];
    
    return matchUpdate;
    
}
-(NSMutableArray*)getPLusServiceArray: (NSString *)strSysNameService
{
    NSString *strParentServiceName;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    NSMutableArray *arrPlusServiceDict = [[NSMutableArray alloc]init];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict1=[arrServices objectAtIndex:j];
                if([strSysNameService isEqualToString:[dict1 valueForKey:@"SysName"]])
                {
                    strParentServiceName = [dict1 valueForKey:@"ParentSysName"];
                    break;
                    
                }
            }
            
        }
        
    }
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict1=[arrServices objectAtIndex:j];
                
                if ([strParentServiceName isEqualToString:[dict1 valueForKey:@"ParentSysName"]])
                {
                    [arrPlusServiceDict addObject:dict1];
                }

            }
            
        }
        
    }

    return arrPlusServiceDict;
}
@end


