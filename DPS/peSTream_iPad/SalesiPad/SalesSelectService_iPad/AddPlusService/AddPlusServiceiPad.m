//
//  AddPlusService.m
//  DPS
//
//  Created by Rakesh Jain on 07/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "AddPlusServiceiPad.h"
#import "SalesAutomationSelectService.h"
#import "SoldServiceStandardDetail.h"
#import "Global.h"
#import "AllImportsViewController.h"

@interface AddPlusServiceiPad ()
{
    
    NSDictionary *dictMasters;
    NSMutableArray *arrFrequencies,*arrCategory,*arrServiceName,*arrFreqId,*arrServicePackages,*arrServicePackageId,*arrFreqNew;
    UITableView *tblData;
    NSDictionary *dictFreq,*dictFreqNew;
    BOOL checkPackage;
    NSMutableArray *arrInitialPrice,*arrDiscount,*arrMaintenancePrice,*arrServiceMasterId,*arrServiceSysName;
    NSInteger indexPathCategory,indexPathService,indexPathPackage,indexPathFreq;
    BOOL checkCat1;
    NSString *strLeadId;
    NSString *strServicePackageId;
    NSString *strPackageId,*strServiceMasterId,*strServiceSysName;
    NSString *strFrequencyId;
    Global *global;
    NSArray *tempDictArray,*arrTempSelectedService;
    
    UIView *viewBackGround;
    BOOL chkTbd;
    NSDictionary *dictServiceName;
    NSString *strCompanyKey,*strEmployeeNo,*strUserName;
    BOOL isEditedInSalesAuto;
    UILabel *lbl;
    //New Change
    NSMutableArray *arrBillingFreq;
    NSString *strBillingFreqSysName;
    NSDictionary *dictFreqSysName;
    NSDictionary *dictQuantityStatus;
    
    //Parameter Change
    //04 April
    NSDictionary *dictServiceParmaterBasedStatus;
    BOOL isParameterBasedService;
    NSMutableArray *arrCategorySysName;
    NSString *strCategorySysName;
    NSMutableArray *arrZillowBasedSysName;
    NSString *strArea,*strBedroom,*strBathroom,*strLinearSqFt,*strLotsize,*strNoStory,*strTurfArea,*strShrubArea;
    
    
    //09 April
    
    BOOL isParameterExist;
    NSMutableArray *arrAdditionalParamterDcs;
    NSString *strServiceSysNameForParamterService;
    NSDictionary *dictFreqNameFromSysName,*dictFreqIdFromSysName;
    NSDictionary *dictPackageName;
    
    
    NSString *strInitialPriceGlobal,*strMaintPriceGlobal;
    
    NSDictionary *dictDeptNameFromSysName;
    NSMutableArray *arrDeptNameCategory;
    
    //Nilind
    
    NSDictionary *dictServiceSysNameForId;
    NSDictionary *dictFreqSysNameFromId;
    NSDictionary *dictFreqNameFromId,*dictTermsForService,*dictCategoryNameFromServiceSysName;
    BOOL chkParaTBD;
    
    NSArray *arrServiceMasterRenewalPrices;
    NSString *strSoldServiceStandardId,*strTotalInitialPrice;
    NSDictionary *dictDefaultFreqTypeService,*dictDefaultFreqSysNameService,*dictCategoryNameFromSysName,*dictServiceNameFromSysName, *dictPackageObjFromId;
    BOOL isCouponApplied;
    NSMutableArray *arrAppliedInitialPrice, *arrAppliedMaintenancePrice;
   
    NSString *strMinPackageCost,*strMinPackageMaintCost;


}
@end

@implementation AddPlusServiceiPad
@synthesize dictPlusService,arrPlusService;

- (void)viewDidLoad
{
    isEditedInSalesAuto=NO;
    isParameterExist=NO;
    isParameterBasedService=NO;
    chkParaTBD=NO;
    arrAdditionalParamterDcs=[[NSMutableArray alloc]init];
    isCouponApplied = NO;
    strMinPackageCost = @"";
    strMinPackageMaintCost = @"";
    //Nilind 16 Nov
    /*lbl=[[UILabel alloc]init];
     lbl.frame=CGRectMake(_txtInitialPrice.frame.origin.x, _txtInitialPrice.frame.origin.y-_txtInitialPrice.frame.size.height+15, 250 , 30);
     lbl.font=[UIFont systemFontOfSize:18];
     lbl.text=@"Initial Price";
     [_viewStandardServices addSubview:lbl];*/
    
    
    NSUserDefaults *defsLogin=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLogin=[defsLogin valueForKey:@"LoginDetails"];
    
    strCompanyKey =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.CompanyKey"]];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLogin valueForKey:@"EmployeeNumber"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.Username"]];
    
    //.....................
    
    
    
    [super viewDidLoad];
    [self serviceName];
    [self removeUnitView];
    // Do any additional setup after loading the view.
    NSLog(@"%@",arrPlusService);
    
    chkTbd=NO;
    //Nilind
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [viewBackGround addGestureRecognizer:singleTapGestureRecognizer];
    
    //...............
    
    global = [[Global alloc] init];
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    [self salesFetch];
    
    indexPathCategory=0;
    //border color
    
    _viewStandardServices.layer.borderColor=[[UIColor colorWithRed:149.0f/255 green:158.0f/255 blue:174.0f/255 alpha:1] CGColor];
    _viewStandardServices.layer.borderWidth=1.0;
    _viewStandardServices.layer.cornerRadius=15.0;
    
    _lblStandardService.backgroundColor=[UIColor clearColor];
    //_lblStandardService.hidden=YES;
    //    _lblStandardService.layer.borderWidth=1.0;
    //    _lblStandardService.layer.cornerRadius=15.0;
    
    
    _txtInitialPrice.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtInitialPrice.layer.borderWidth=1.0;
    
    
    _txtDiscount.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtDiscount.layer.borderWidth=1.0;
    _txtMaintenance.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtMaintenance.layer.borderWidth=1.0;
    
    _txtDiscountPercent.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtDiscountPercent.layer.borderWidth=1.0;
    
    
    //end
    
    arrInitialPrice=[[NSMutableArray alloc]init];
    arrDiscount=[[NSMutableArray alloc]init];
    arrMaintenancePrice=[[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
    arrCategory=[[NSMutableArray alloc]init];
    arrServiceName=[[NSMutableArray alloc]init];
    arrFrequencies=[[NSMutableArray alloc]init];
    arrFreqId=[[NSMutableArray alloc]init];
    arrServicePackages=[[NSMutableArray alloc]init];
    arrFreqNew=[[NSMutableArray alloc]init];
    arrServicePackageId=[[NSMutableArray alloc]init];
    arrServiceMasterId=[[NSMutableArray alloc]init];
    arrServiceSysName=[[NSMutableArray alloc]init];
    //dictFreq=[[NSMutableDictionary alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    // NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    [self getFrequencySysNameFromName];
    [self serviceUnitStatus];
    [self serviceNameFromId];
    //Nilind
    
    //    NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
    //    for (int i=0; i<arrCtgry.count; i++)
    //    {
    //        NSDictionary *dict=[arrCtgry objectAtIndex:i];
    //        [arrCategory addObject:[dict valueForKey:@"Name"]];
    //        NSArray *arrServices=[dict valueForKey:@"Services"];
    //        for (int j=0; j<arrServices.count; j++)
    //        {
    //            NSDictionary *dict=[arrServices objectAtIndex:j];
    //            [arrServiceName addObject:[dict valueForKey:@"Name"]];
    //
    //            NSArray *arrPackages=[dict valueForKey:@"ServicePackageDcs"];
    //            for (int k=0; k<arrPackages.count; k++)
    //            {
    //                NSDictionary *dict=[arrPackages objectAtIndex:k];
    //                [arrServicePackages addObject:[dict valueForKey:@"PackageName"]];
    //
    //                NSArray *arrackageDetails=[dict valueForKey:@"ServicePackageDetails"];
    //                for (int l=0; l<arrackageDetails.count; l++)
    //                {
    //                    NSDictionary *dict=[arrackageDetails objectAtIndex:l];
    //                    [arrFreqNew addObject:[dict valueForKey:@"FrequencyId"]];
    //                }
    //            }
    //        }
    //    }
    
    //..................
    
    //Frequency Fetching
    NSArray *arrFreq=[dictMasters valueForKey:@"Frequencies"];
    for (int i=0; i<arrFreq.count; i++)
    {
        NSDictionary *dict=[arrFreq objectAtIndex:i];
        [arrFrequencies addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]];
        [arrFreqId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyId"]]];
    }
    dictFreq=[NSDictionary dictionaryWithObjects:arrFreqId forKeys:arrFrequencies];
    dictFreqNew=[NSDictionary dictionaryWithObjects:arrFrequencies forKeys:arrFreqId];
    
    //Dynamic Table
    
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.dataSource=self;
    tblData.delegate=self;
    // [self fetchFromCoreData];
    
    [_btnService setTitle:@"--Select Service--" forState:UIControlStateNormal];
    [_btnPackage setTitle:@"--Select Package--" forState:UIControlStateNormal];
    [_btnFrequency setTitle:@"--Select Frequency--" forState:UIControlStateNormal];
    // [_btnBillingFrequency setTitle:@"--Select Billing Frequency--" forState:UIControlStateNormal];
#pragma mark- Depratment Master
    //Nilind 4 Oct
    
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        _const_View_T.constant=0;
    }
    
    //Nilind 10 Jan
    
    _txtInitialPrice.layer.cornerRadius=5.0;
    _txtDiscount.layer.cornerRadius=5.0;
    _txtMaintenance.layer.cornerRadius=5.0;
    _txtDiscountPercent.layer.cornerRadius=5.0;
    _txtQuantity.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtQuantity.layer.borderWidth=1.0;
    //End
    [self setDefaultBillingFreq];
    [self setTextCorner];
    
    if ([_strForEdit isEqualToString:@"EditStandard"])
    {
        [self editServiceDetail];
        [_btnAdd setTitle:@"Update" forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionOnBack:(id)sender
{
    [self endEditing];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionOnCategoryService:(id)sender
{
    [self endEditing];
    arrServiceName=[[NSMutableArray alloc]init];
    arrServiceSysName=[[NSMutableArray alloc]init];
    arrServiceMasterId=[[NSMutableArray alloc]init];
    
    if (arrPlusService.count>0)
    {
        for (int i=0; i<arrPlusService.count; i++)
        {
            NSDictionary *dict=[arrPlusService objectAtIndex:i];
            
            NSString *strIsDoorToDoor=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDoortodoor"]];
            
            //if ([strIsDoorToDoor isEqualToString:@"false"] || [strIsDoorToDoor isEqualToString:@"False"] || [strIsDoorToDoor isEqualToString:@"0"])
            //{
                [arrServiceName addObject:[dict valueForKey:@"Name"]];//[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]]];
                [arrServiceSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [arrServiceMasterId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceMasterId"]]];
           //}
        }
        if (arrServiceName.count==0)
        {
            [global AlertMethod:@"Alert!" :@"No Service Available"];
        }
        else
        {
            tblData.tag=10;
            [self tableLoad:tblData.tag];
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Service Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    
    [_btnPackage setTitle:@"--Select Package--" forState:UIControlStateNormal];
    [_btnFrequency setTitle:@"--Select Frequency--" forState:UIControlStateNormal];
    //[_btnBillingFrequency setTitle:@"--Select Billing Frequency--" forState:UIControlStateNormal];
    _txtInitialPrice.text=@"";
    _txtMaintenance.text=@"";
    _txtDiscount.text=@"";
    
    
    
    
}

- (IBAction)actionOnCategoryPackage:(id)sender
{
    [self endEditing];
    if ([_btnService.titleLabel.text isEqualToString:@"--Select Service--"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select Service" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        
        //Nilind
        // checkCat1=NO;
        arrServicePackages=[[NSMutableArray alloc]init];
        arrFreqNew=[[NSMutableArray alloc]init];
        arrServicePackageId=[[NSMutableArray alloc]init];
        
        
        NSDictionary *dict1=[arrPlusService objectAtIndex:indexPathService];
        
        //Nilind 10 Feb
        for (int i=0;i<arrPlusService.count;i++)
        {
            NSDictionary *dictTemp = [arrPlusService objectAtIndex:i];
            
            if ([[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"SysName"]] isEqualToString:strServiceSysName])
            {
                dict1 = dictTemp;
                break;
            }
        }
        //ENd
        
        
        NSArray *arrPackages=[dict1 valueForKey:@"ServicePackageDcs"];
        arrTempSelectedService=arrPackages;
        if (arrPackages.count>0)
        {
            for (int k=0; k<arrPackages.count; k++)
            {
                NSDictionary *dict=[arrPackages objectAtIndex:k];
                [arrServicePackages addObject:[dict valueForKey:@"PackageName"]];
                [arrServicePackageId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServicePackageId"]]];
            }
            tblData.tag=20;
            [self tableLoad:tblData.tag];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Package Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    [_btnFrequency setTitle:@"--Select Frequency--" forState:UIControlStateNormal];
    // [_btnBillingFrequency setTitle:@"--Select Billing Frequency--" forState:UIControlStateNormal];
    _txtInitialPrice.text=@"";
    _txtMaintenance.text=@"";
    _txtDiscount.text=@"";
    //..................
    
    
}

- (IBAction)actionOnCategoryFrequency:(id)sender
{
    [self endEditing];
    if (isParameterBasedService==YES)
    {
        
        if ([_btnService.titleLabel.text isEqualToString:@"--Select Service--"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select Service" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            arrFreqNew=[[NSMutableArray alloc]init];
            NSArray *arrTempFreq=[dictMasters valueForKey:@"Frequencies"];
            
            if (arrTempFreq.count==0)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Frequency Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
            }
            else
            {
                [self getParameterBasedFreq:strServiceSysNameForParamterService];
                /*for (int i=0; i<arrTempFreq.count;i++)
                 {
                 NSDictionary *dict=[arrTempFreq objectAtIndex:i];
                 [arrFreqNew addObject:[dict valueForKey:@"FrequencyId"]];
                 
                 }*/
                //02 Sept 2020
                NSMutableArray *arrTempFreq = [self getServiceFrequencyAsPerType:arrFreqNew Type:@"Service"];

                arrFreqNew = [[NSMutableArray alloc]init];
                arrFreqNew = arrTempFreq;
                
                //End
                
                if(arrFreqNew.count==0)
                {
                    [global AlertMethod:@"Alert!" :@"No frequency available"];
                }
                else
                {
                    tblData.tag=30;
                    [self tableLoad:tblData.tag];
                }
                
                
            }
        }
        
    }
    else
    {
        if ([_btnService.titleLabel.text isEqualToString:@"--Select Service--"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select Service" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else if ([_btnPackage.titleLabel.text isEqualToString:@"--Select Package--"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select Package" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            
            checkCat1=NO;
            
            arrServicePackages=[[NSMutableArray alloc]init];
            arrFreqNew=[[NSMutableArray alloc]init];
            
            
            NSDictionary *dict1;
            dict1=[arrPlusService objectAtIndex:indexPathService];
      
            //Nilind 10 Feb
            for (int i=0;i<arrPlusService.count;i++)
            {
                NSDictionary *dictTemp = [arrPlusService objectAtIndex:i];
                
                if ([[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"SysName"]] isEqualToString:strServiceSysName])
                {
                    dict1 = dictTemp;
                    break;
                }
            }
            //ENd
            
            NSArray *arrPackages=[dict1 valueForKey:@"ServicePackageDcs"];
            NSDictionary *dict2;
            if (arrPackages.count==0)
            {
            }
            else
            {
                dict2=[arrPackages objectAtIndex:indexPathPackage];
                
                //Nilind 11 Feb
                for (int i=0;i<arrPackages.count;i++)
                {
                    NSDictionary *dictTemp = [arrPackages objectAtIndex:i];//ServicePackageId
                    if ([[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"ServicePackageId"]] isEqualToString:strPackageId])
                    {
                        dict2 = dictTemp;
                        break;
                    }
                    
                }
                //End
            }
            
            NSArray *arrackageDetails=[dict2 valueForKey:@"ServicePackageDetails"];
            tempDictArray=arrackageDetails;
            if(arrackageDetails.count>0)
            {
                for (int l=0; l<arrackageDetails.count; l++)
                {
                    NSDictionary *dict=[arrackageDetails objectAtIndex:l];
                    [arrFreqNew addObject:[dict valueForKey:@"FrequencyId"]];
                }
                
                //02 Sept 2020
                NSMutableArray *arrTempFreq = [self getServiceFrequencyAsPerType:arrFreqNew Type:@"Service"];

                arrFreqNew = [[NSMutableArray alloc]init];
                arrFreqNew = arrTempFreq;
                
                //End
                
                
                if(arrFreqNew.count==0)
                {
                    [global AlertMethod:@"Alert!" :@"No Frequency Available"];
                }
                else
                {
                    tblData.tag=30;
                    [self tableLoad:tblData.tag];
                }
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Frequency Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    //..................
    
}
- (IBAction)actionOnBillingFrequency:(id)sender
{
    [self endEditing];
    arrBillingFreq=[[NSMutableArray alloc]init];
    NSArray *arrTempFreq=[dictMasters valueForKey:@"Frequencies"];
    if (arrTempFreq.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Billing Frequency Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    }
    else
    {
        for (int i=0; i<arrTempFreq.count;i++)
        {
            NSDictionary *dict=[arrTempFreq objectAtIndex:i];
            [arrBillingFreq addObject:dict];
            
        }
        
        if ([_btnFrequency.currentTitle caseInsensitiveCompare:@"OneTime"]==NSOrderedSame||[_btnFrequency.currentTitle caseInsensitiveCompare:@"One Time"]==NSOrderedSame)
        {
            arrBillingFreq=[[NSMutableArray alloc]init];
            for (int i=0; i<arrTempFreq.count;i++)
            {
                NSDictionary *dict=[arrTempFreq objectAtIndex:i];
                
                // if ([[dict valueForKey:@"SysName"]caseInsensitiveCompare:@"OneTime"]||[[dict valueForKey:@"SysName"]caseInsensitiveCompare:@"One Time"])
                if ([[dict valueForKey:@"SysName"]isEqualToString:@"OneTime"]||[[dict valueForKey:@"SysName"]isEqualToString:@"oneTime"])
                {
                    [arrBillingFreq addObject:dict];
                }
            }
        }
        
        //02 Sept 2020
        NSMutableArray *arrTempFreq = [self getBillingFrequencyAsPerType:arrBillingFreq Type:@"Billing"];
        
        arrBillingFreq = [[NSMutableArray alloc]init];
        arrBillingFreq = arrTempFreq;
        
        //End
        
        if (arrBillingFreq.count==0)
        {
            [global AlertMethod:@"Alert!" :@"No Billing Frequency Available"];
        }
        else
        {
            tblData.tag=50;
            [self tableLoad:tblData.tag];
        }
    }
    
}

- (IBAction)actionOnAdd:(id)sender
{
    [self endEditing];
    if ([_btnService.titleLabel.text isEqualToString:@"--Select Service--"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select service" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if ([_btnPackage.titleLabel.text isEqualToString:@"--Select Package--"]&& isParameterBasedService==NO)
        // else if ([_btnPackage.titleLabel.text isEqualToString:@"--Select Package--"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select package" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if ([_btnFrequency.titleLabel.text isEqualToString:@"--Select Frequency--"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select frequency" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if ([_btnBillingFrequency.titleLabel.text isEqualToString:@"--Select Billing Frequency--"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please select Billing frequency" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if (_txtInitialPrice.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Initial price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    //Nilind 24 Jan
    /*else if (_txtDiscount.text.length==0)
     {
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Discount" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
     [alert show];
     }
    else if (_txtMaintenance.text.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter Maintenance price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    */
   //End
    else if ([strMinPackageCost floatValue] > [_txtInitialPrice.text floatValue])
    {
        [global AlertMethod:@"Alert!" :[NSString stringWithFormat:@"Initial price of this service can't be less than from it's minimum price, of $ %.2f",[strMinPackageCost floatValue]]];
    }
    else if ([strMinPackageMaintCost floatValue] > [_txtMaintenance.text floatValue])
    {
        [global AlertMethod:@"Alert!" :[NSString stringWithFormat:@"Maintenance price of this service can't be less than from it's minimum price, of $ %.2f",[strMinPackageCost floatValue]]];
    }
    else if([_txtDiscount.text longLongValue]>[_txtInitialPrice.text longLongValue] )
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Discount can't be greater than Inital price" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        
        if ([_strFreqName isEqualToString:_btnFrequency.titleLabel.text]&&[_strBillingFreqName isEqualToString:_btnBillingFrequency.titleLabel.text])
        {
            if([self checkForServiceAvailabiltiy]==YES)
            {
                [global displayAlertController:@"Alert!" :@"Service with same frequency is already taken, please choose another service" :self];
            }
            else
            {
                [self finalCall];
            }
        }
        else
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Either plus service frequency or billing frequency is different from parent service frequency or billing frequency, Do you want to proceed?"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"YES"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     if([self checkForServiceAvailabiltiy]==YES)
                                     {
                                         [global displayAlertController:@"Alert!" :@"Service with same frequency is already taken, please choose another service" :self];
                                     }
                                     else
                                     {
                                         [self finalCall];
                                         
                                     }
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"NO"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    
}
-(void)finalCall
{
    if ([_strForEdit isEqualToString:@"EditStandard"])
    {
        [self fetchForAppliedDiscountFromCoreData];
        
        if (isCouponApplied)
        {
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert!"
                                       message:@"Edit in price will delete all the coupon, credit and discount associated with service, Do you want to proceed ?"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                        [self updateServiceDetails];
                [self updateServiceDiscount:@"Coupon"];
                [self deleteAppliedCoupon:strSoldServiceStandardId ServiceSysName:strServiceSysName];
                                //[self finalSaveCode:strServiceName:strFreqName];

                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];

        }
        else
        {
            [self updateServiceDetails];
            //[self finalSaveCode:strServiceName:strFreqName];


        }
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"fromEdit"];
        [defs synchronize];
        
    }
    else //Same as previous
    {
        [self updateSoldServiceStandardDetail];
        [self addRenewalService];
    }
    
    if (isEditedInSalesAuto==YES)
    {
        NSLog(@"Global modify date called in Addplus service");
        [global updateSalesModifydate:strLeadId];
    }
    
    
    [arrInitialPrice addObject:_txtInitialPrice.text];
    [arrDiscount addObject:_txtDiscount.text];
    [arrMaintenancePrice addObject:_txtMaintenance.text];
    _txtInitialPrice.text=@"";
    _txtDiscount.text=@"";
    _txtMaintenance.text=@"";
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)actionOnCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)setTableFrame:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    BOOL isValuePresent;
    
    isValuePresent=NO;
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            
            /* if (arrInitialPrice.count>0) {
             
             isValuePresent = YES;
             
             }*/
            
            [self setTableFrame:i];
            break;
        }
        case 20:
        {
            
            /* if (arrInitialPrice.count>0) {
             
             isValuePresent = YES;
             
             }*/
            [self setTableFrame:i];
            break;
        }
        case 30:
        {
            
            /* if (arrInitialPrice.count>0) {
             
             isValuePresent = YES;
             
             }*/
            [self setTableFrame:i];
            break;
        }
        case 50:
        {
            /*if (arrInitialPrice.count>0) {
             
             isValuePresent = YES;
             
             }*/
            [self setTableFrame:i];
            break;
        }
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
    NSIndexPath *indexpat= [NSIndexPath indexPathForRow:0 inSection:0];
    
    /*if (isValuePresent) {
     
     [tblData scrollToRowAtIndexPath:indexpat
     atScrollPosition:UITableViewScrollPositionTop
     animated:YES
     ];
     
     }*/
    [tblData scrollToRowAtIndexPath:indexpat
                   atScrollPosition:UITableViewScrollPositionTop
                           animated:YES
     ];
    
}
//============================================================================
#pragma mark- TABLEVIEW DELEGATE METHOD
//tblRecord Tag=0
//tblNonStandardService Tag=1
//============================================================================
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==10)
    {
        return [arrServiceName count];//[arrData1 count];//[arrData count];
    }
    else if(tableView.tag==20)
    {
        return [arrServicePackages count];
    }
    else if(tableView.tag==50)
    {
        return [arrBillingFreq count];
    }
    else
    {
        return [arrFreqNew count];// return [arrFrequencies count];
    }
}
//============================================================================
#pragma mark- TABLEVIEW DELEGATE METHOD
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (tblData.tag==10)
    {
        cell.textLabel.text=[arrServiceName objectAtIndex:indexPath.row];
    }
    else if (tblData.tag==20)
    {
        cell.textLabel.text=[arrServicePackages objectAtIndex:indexPath.row];
    }
    else if (tblData.tag==30)
    {
        
        cell.textLabel.text=[dictFreqNew valueForKey:[NSString stringWithFormat:@"%@",[arrFreqNew objectAtIndex:indexPath.row]]];//[NSString stringWithFormat:@"%@",[arrFreqNew objectAtIndex:indexPath.row]];
    }
    else if (tblData.tag==50) //Billing Freq
    {
        NSDictionary *dict=[arrBillingFreq objectAtIndex:indexPath.row];
        
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]];
    }
    if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
    {
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        cell.textLabel.font=[UIFont systemFontOfSize:22];
    }
    
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger i;
    i=tblData.tag;
    switch (i)
    {
            
        case 10:
        {
            [_btnService setTitle:[arrServiceName objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            indexPathService=indexPath.row;
            strServiceMasterId=[arrServiceMasterId objectAtIndex:indexPath.row];
            strServiceSysName=[arrServiceSysName objectAtIndex:indexPath.row];
            strCategorySysName=[dictCategoryNameFromServiceSysName valueForKey:strServiceSysName];
            
            if ([[dictQuantityStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                [self addUnitView];
            }
            else
            {
                [self removeUnitView];
            }
            
            //04 April
            if ([[dictServiceParmaterBasedStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                isParameterBasedService=YES;
                strPackageId=@"";
                _const_BtnPackage_H.constant=0;
                _const_BtnPackage_T.constant=0; strServiceSysNameForParamterService=strServiceSysName;
                
                [self calculationForParamterBasedService:strCategorySysName ServiceSysName:strServiceSysName];
                NSLog(@"Parameterbased service");
            }
            else
            {
                isParameterBasedService=NO;
                _const_BtnPackage_H.constant=50;
                _const_BtnPackage_T.constant=15;
                NSLog(@"Non-Parameterbased service");
                
            }
            //End
            
            
            break;
        }
        case 20:
        {
            [_btnPackage setTitle:[arrServicePackages objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            indexPathPackage=indexPath.row;
            strPackageId=[arrServicePackageId objectAtIndex:indexPath.row];
            // strServiceSysName=[arrServiceSysName objectAtIndex:indexPath.row];
            NSLog(@"PackageId ID %@",strPackageId);
            //NIlind
            
            NSDictionary *dict=[arrTempSelectedService objectAtIndex:indexPath.row];
            NSString *strTbd;
            strTbd=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsTBD"]];
            if ([strTbd isEqualToString:@"0"])
            {
                strTbd=@"false";
                chkTbd=NO;
            }
            else
            {
                strTbd=@"true";
                chkTbd=YES;
            }
            
            NSLog(@"%@",strTbd);
            //............
            
            
            break;
        }
        case 30:
        {
            
            if (isParameterBasedService==YES)
            {
                [_btnFrequency setTitle:[dictFreqNew valueForKey:[NSString stringWithFormat:@"%@",[arrFreqNew objectAtIndex:indexPath.row]]]forState:UIControlStateNormal];
                strFrequencyId=[NSString stringWithFormat:@"%@",[arrFreqNew objectAtIndex:indexPath.row]];
                if (isParameterExist==YES)
                {
                    NSString *strFrqSysNameNew;
                    strFrqSysNameNew=[NSString stringWithFormat:@"%@",[dictFreqSysNameFromId valueForKey:strFrequencyId]];
                    [self servicePricingSetupCalculation:strServiceSysName FrequencySysName:strFrqSysNameNew ParameterSysName:arrZillowBasedSysName];
                }
                else
                {
                    _txtInitialPrice.text=@"TBD";
                    _txtMaintenance.text=@"TBD";
                    _txtDiscount.text=@"0";
                    _txtDiscountPercent.text=@"0";
                    
                }
                
            }
            else
            {
                
                [_btnFrequency setTitle:[dictFreqNew valueForKey:[NSString stringWithFormat:@"%@",[arrFreqNew objectAtIndex:indexPath.row]]]forState:UIControlStateNormal];
                strFrequencyId=[NSString stringWithFormat:@"%@",[arrFreqNew objectAtIndex:indexPath.row]];
                NSLog(@"FREQUENCY ID %@",strFrequencyId);
                for (int i=0; i<tempDictArray.count; i++)
                {
                    NSDictionary *dict=[tempDictArray objectAtIndex:i];
                    if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyId"]] isEqualToString:strFrequencyId])
                    {
                        NSString *strPackageCost,*strPackageMaintCost,*strDiscout;
                        
                        strMinPackageCost=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MinPackageCost"]];
                        strMinPackageMaintCost=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MinPackageMaintCost"]];
                        
                        strPackageCost=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PackageCost"]];
                        strPackageMaintCost=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PackageMaintCost"]];
                        strDiscout=([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServicePackageDc"]] isKindOfClass:[NSNull class]])?@"":[dict valueForKey:@"ServicePackageDc"];
                        NSLog(@"%@ %@ %@",strPackageCost,strPackageMaintCost,strDiscout);
                        if (chkTbd==YES)
                        {
                            strPackageCost=@"TBD";
                            strPackageMaintCost=@"TBD";
                            strDiscout=@"TBD";
                            _txtInitialPrice.text=[NSString stringWithFormat:@"%@",strPackageCost ];
                            _txtMaintenance.text=[NSString stringWithFormat:@"%@",strPackageMaintCost ];
                        }
                        else
                        {
                            _txtInitialPrice.text=strPackageCost;
                            _txtMaintenance.text=strPackageMaintCost;
                        }
                        if ([strDiscout isEqualToString:@""])
                        {
                            strDiscout=@"0";
                        }
                        // _txtDiscount.text=strDiscout;
                        _txtDiscount.text=[NSString stringWithFormat:@"%.2f",[strDiscout doubleValue]];
                        
                        double initial,discountPer;
                        initial=[_txtInitialPrice.text doubleValue];
                        discountPer=([_txtDiscount.text doubleValue])*100/initial;
                        _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f",discountPer];
                        if ([[NSString stringWithFormat:@"%.2f",discountPer] isEqualToString:@"nan"]||[[NSString stringWithFormat:@"%.2f",discountPer] isEqualToString:@"Nan"])
                        {
                            _txtDiscountPercent.text=@"0";
                        }
                        
                        // NSString *strPer=@"%";
                        //_txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f %@",discountPer,strPer];
                    }
                }
            }
            [self forOneTimeFreq];
            
            //Billing Frequcy Setup
            NSString *strDefaultBillingFreqType, *strDefaultBillingFreqSysName;
            strDefaultBillingFreqType = [NSString stringWithFormat:@"%@",[dictDefaultFreqTypeService valueForKey:strServiceSysName]];
            strDefaultBillingFreqSysName = [NSString stringWithFormat:@"%@",[dictDefaultFreqSysNameService valueForKey:strServiceSysName]];
            [self billingFrequencySetup:strDefaultBillingFreqType :strDefaultBillingFreqSysName];
            
            break;
        }
        case 50:
        {
            
            NSDictionary *dict=[arrBillingFreq objectAtIndex:indexPath.row];
            [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]forState:UIControlStateNormal];
            strBillingFreqSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
            
            
            break;
        }
        default:
            break;
    }
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}


//.......................................................
#pragma mark- Textfield Delegate
//.......................................................

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    /*
     
     if(textField==_txtInitialPrice)
     {
     
     lbl.frame=CGRectMake(_txtInitialPrice.frame.origin.x, _txtInitialPrice.frame.origin.y-_txtInitialPrice.frame.size.height+5, 200 , 30);
     lbl.font=[UIFont systemFontOfSize:12];
     lbl.text=@"Initial Price";
     }
     else if(textField==_txtDiscount)
     {
     
     //[lbl removeFromSuperview];
     lbl.frame=CGRectMake(_txtDiscount.frame.origin.x, _txtDiscount.frame.origin.y-_txtDiscount.frame.size.height+5, 150 , 30);
     lbl.font=[UIFont systemFontOfSize:12];
     lbl.text=@"Discount";
     //[_viewTextFiled addSubview:lbl];
     
     
     }
     else if(textField==_txtDiscountPercent)
     {
     lbl.frame=CGRectMake(_txtDiscountPercent.frame.origin.x, _txtDiscountPercent.frame.origin.y-_txtDiscountPercent.frame.size.height+5, 150 , 30);
     lbl.font=[UIFont systemFontOfSize:12];
     lbl.text=@"Discount %";
     // [_viewTextFiled addSubview:lbl];
     
     }
     else if(textField==_txtMaintenance)
     {
     
     lbl.frame=CGRectMake(_txtMaintenance.frame.origin.x, _txtMaintenance.frame.origin.y-_txtMaintenance.frame.size.height+5, 150 , 30);
     lbl.font=[UIFont systemFontOfSize:12];
     lbl.text=@"Maintenance Price";
     //[_viewTextFiled addSubview:lbl];
     
     }
     
     */
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_txtDiscount)
    {
        NSLog(@"TXT DISCOUNT RETURN");
        double initial,discountPer;
        initial=[_txtInitialPrice.text doubleValue];
        discountPer=([_txtDiscount.text doubleValue])*100/initial;
        _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f",discountPer];
        
        //NSString *strPer=@"%";
        //_txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f %@",discountPer,strPer];
    }
    if (textField==_txtDiscountPercent)
    {
        NSLog(@"TXT DISCOUNT RETURN");
        double initial,discountPer;
        initial=[_txtInitialPrice.text doubleValue];
        discountPer=(initial*[[self getNumberStringNew:_txtDiscountPercent.text] doubleValue])/100;
        
        _txtDiscount.text=[NSString stringWithFormat:@"%.2f",discountPer];
    }
    if (textField==_txtDiscountPercent)
    {
        NSLog(@"TXT DISCOUNT PERCENT RETURN");
        
    }
    [_txtInitialPrice resignFirstResponder];
    [_txtDiscount resignFirstResponder];
    [_txtMaintenance resignFirstResponder];
    [_txtDiscountPercent resignFirstResponder];
    [_txtQuantity resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_txtDiscount)
    {
        
        NSLog(@"TXT DISCOUNT RETURN");
        double initial,discountPer;
        initial=[_txtInitialPrice.text doubleValue];
        discountPer=([_txtDiscount.text doubleValue])*100/initial;
        if (initial==0 || initial<0)
        {
            discountPer=0;
        }
        
        _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f",discountPer];
        
        
        // NSString *strPer=@"%";
        //_txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f %@",discountPer,strPer];
        
        
    }
    if (textField==_txtDiscountPercent)
    {
        
        
        NSLog(@"TXT DISCOUNT RETURN");
        double initial,discountPer;
        initial=[_txtInitialPrice.text doubleValue];
        discountPer=(initial*[[self getNumberStringNew:_txtDiscountPercent.text] doubleValue])/100;
        
        _txtDiscount.text=[NSString stringWithFormat:@"%.2f",discountPer];
        
        /* NSString *strPerNew=@"%";
         double discPer;
         discPer=[[self getNumberStringNew:_txtDiscountPercent.text]doubleValue];
         _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f %@",discPer,strPerNew];*/
        
    }
    if (textField==_txtDiscount)
    {
        NSLog(@"TEXT QUANTITY ");
    }
    else
    {
        double initial,discountPer;
        initial=[_txtInitialPrice.text doubleValue];
        discountPer=([_txtDiscount.text doubleValue])*100/initial;
        if (initial==0 || initial<0)
        {
            discountPer=0;
        }
        
        _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f",discountPer];
        // NSString *strPer=@"%";
        //_txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f %@",discountPer,strPer];
        
        /* NSString *strPerNew=@"%";
         double discPer;
         discPer=[[self getNumberStringNew:_txtDiscountPercent.text]doubleValue];
         _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f %@",discPer,strPerNew];*/
    }
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField==_txtDiscount)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtDiscount.text];
        
        if (isNuberOnly) {
            
            if ([string isEqualToString:@""])
            {
                double initial,discountPer;
                NSString *strDiscount;
                strDiscount=[_txtDiscount.text stringByReplacingCharactersInRange:range withString:string];
                initial=[_txtInitialPrice.text doubleValue];
                discountPer=([strDiscount doubleValue])*100/initial;
                if (initial==0 || initial<0)
                {
                    discountPer=0;
                }
                
                _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f",discountPer];
            }
            else
            {
                double initial,discountPer;
                NSString *strDiscount;
                strDiscount=[NSString stringWithFormat:@"%@%@",_txtDiscount.text,string];
                initial=[_txtInitialPrice.text doubleValue];
                discountPer=([strDiscount doubleValue])*100/initial;
                if (initial==0 || initial<0)
                {
                    discountPer=0;
                }
                
                _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f",discountPer];
            }
            return YES;
        } else {
            return NO;
        }
        
    }
    else if (textField==_txtDiscountPercent)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtDiscountPercent.text];
        
        if (isNuberOnly) {
            
            if ([string isEqualToString:@""])
            {
                double initial,discount;
                initial=[_txtInitialPrice.text doubleValue];
                
                NSString *strDiscountPer;
                NSString *strText=_txtDiscountPercent.text;
                strDiscountPer=[strText stringByReplacingCharactersInRange:range withString:string];
                
                discount=(initial*[strDiscountPer doubleValue])/100;
                _txtDiscount.text=[NSString stringWithFormat:@"%.2f",discount];
                
            }
            else
            {
                double initial,discount;
                initial=[_txtInitialPrice.text doubleValue];
                
                NSString *strDiscountPer;
                strDiscountPer=[NSString stringWithFormat:@"%@%@",_txtDiscountPercent.text,string];
                
                
                discount=(initial*[strDiscountPer doubleValue])/100;
                _txtDiscount.text=[NSString stringWithFormat:@"%.2f",discount];
            }
            return YES;
        } else {
            return NO;
        }
        
        
    }
    else if (textField==_txtInitialPrice)
    {
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtInitialPrice.text];
        
        if (isNuberOnly) {
            
            if ([string isEqualToString:@""])
            {
                double initial,discountPer;
                NSString *strInitial;
                strInitial=[_txtInitialPrice.text stringByReplacingCharactersInRange:range withString:string];
                initial=[strInitial doubleValue];
                discountPer=([_txtDiscount.text doubleValue])*100/initial;
                if (initial==0 || initial<0)
                {
                    discountPer=0;
                }
                
                _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f",discountPer];
            }
            else
            {
                double initial,discountPer;
                NSString *strInitial;
                strInitial=[NSString stringWithFormat:@"%@%@",_txtInitialPrice.text,string];
                initial=[strInitial doubleValue];
                discountPer=([_txtDiscount.text doubleValue])*100/initial;
                if (initial==0 || initial<0)
                {
                    discountPer=0;
                }
                
                _txtDiscountPercent.text=[NSString stringWithFormat:@"%.2f",discountPer];
                
            }
            return YES;
        } else {
            return NO;
        }
        
    }
    else if (textField==_txtMaintenance)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@".0123456789" :_txtMaintenance.text];
        
        if (isNuberOnly) {
            
            return YES;
        } else {
            return NO;
        }
        
        
    }
    else if(textField==_txtQuantity)
    {
       
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtQuantity.text];
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
        
        if([newString isEqualToString:@"0"])
        {
            return NO;
        }
        else
        {
            if (isNuberOnly)
            {
                if ([arrayOfString count] ==2 )
                {
                    NSString *str=[arrayOfString objectAtIndex:1];
                    if (str.length>2)
                    {
                        return NO;
                        
                    }
                }
                return YES;
            }
            else
            {
                return NO;
            }
        }
    }
    return YES;
    
   
}

//End


-(NSString*)getNumberStringNew:(NSString*)str
{
    NSString *value;
    NSString *strDiscount = [str stringByReplacingOccurrencesOfString:@"%" withString:@""];
    strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    value=[NSString stringWithFormat:@"%@",strDiscount];
    return value;
}
//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [_txtInitialPrice resignFirstResponder];
    [_txtDiscount resignFirstResponder];
    [_txtMaintenance resignFirstResponder];
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
}

-(void)serviceName
{
    NSMutableArray *name,*sysName;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMastersss=[defs valueForKey:@"MasterSalesAutomation"];
    //  NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMastersss);
    
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                
            }
        }
    }
    
    dictServiceName = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
    NSLog(@"PaymentInfo%@",dictServiceName);
    
}
#pragma mark- 30 Aug
-(void)updateSoldServiceStandardDetail
{
#pragma mark- Note
    //New
    isEditedInSalesAuto=YES;
    // visibility and Inspector alag se save krna he
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    
    SoldServiceStandardDetail *objentitySoldServiceStandardDetail = [[SoldServiceStandardDetail alloc]initWithEntity:entitySoldServiceStandardDetail insertIntoManagedObjectContext:context];
    objentitySoldServiceStandardDetail.leadId=strLeadId;
    objentitySoldServiceStandardDetail.createdBy=@"";        objentitySoldServiceStandardDetail.createdDate=@"";
    objentitySoldServiceStandardDetail.discount=_txtDiscount.text;
    objentitySoldServiceStandardDetail.initialPrice=_txtInitialPrice.text;
    objentitySoldServiceStandardDetail.isSold=@"false";
    objentitySoldServiceStandardDetail.maintenancePrice=_txtMaintenance.text;
    objentitySoldServiceStandardDetail.modifiedBy=@"";
    objentitySoldServiceStandardDetail.modifiedDate=[global modifyDate];
    objentitySoldServiceStandardDetail.modifiedInitialPrice=@"";
    objentitySoldServiceStandardDetail.modifiedMaintenancePrice=@"";
    objentitySoldServiceStandardDetail.packageId=strPackageId;//strServicePackageId;
    //objentitySoldServiceStandardDetail.serviceFrequency=_btnFrequency.titleLabel.text;
    //Nilind 03 May
    //objentitySoldServiceStandardDetail.serviceFrequency=[dictFreqSysName valueForKey: _btnFrequency.titleLabel.text];
    objentitySoldServiceStandardDetail.serviceFrequency=_btnFrequency.titleLabel.text;
    
    objentitySoldServiceStandardDetail.frequencySysName=[dictFreqSysName valueForKey: _btnFrequency.titleLabel.text];
    //End
    objentitySoldServiceStandardDetail.serviceId=strServiceMasterId;
    objentitySoldServiceStandardDetail.serviceSysName=strServiceSysName;//_btnService.titleLabel.text;//dictServiceSysNameForId
    //objentitySoldServiceStandardDetail.soldServiceStandardId=@"";
    //Temp
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString  *strRandomId = [NSString stringWithFormat:@"%@%@",strDate,strTime];
    objentitySoldServiceStandardDetail.soldServiceStandardId=strRandomId;
    strSoldServiceStandardId = strRandomId;

    //End
    //strServicePackageId;//@"";
    //Nilind 6 Oct
    if ([_txtInitialPrice.text isEqualToString:@"TBD"])
    {
        objentitySoldServiceStandardDetail.isTBD=@"true";
    }
    else
    {
        objentitySoldServiceStandardDetail.isTBD=@"false";
    }
    objentitySoldServiceStandardDetail.serviceTermsConditions=[dictTermsForService valueForKey:strServiceSysName];
    //..........................
    objentitySoldServiceStandardDetail.userName=strUserName;
    objentitySoldServiceStandardDetail.companyKey=strCompanyKey;
    
    //Nilind 24 may
    //Nilind 24 may
    if([_txtQuantity.text isEqualToString:@"0"]||[_txtQuantity.text isEqualToString:@""])
    {
        _txtQuantity.text=@"1";
    }
    objentitySoldServiceStandardDetail.unit=_txtQuantity.text;
    
    double finalUnitBaseInitialPrice,finalUnitBasePriceMaintPrice;
    if ([[dictQuantityStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
    {
        finalUnitBaseInitialPrice=[_txtInitialPrice.text doubleValue]*[_txtQuantity.text doubleValue];
        finalUnitBasePriceMaintPrice=[_txtMaintenance.text doubleValue]*[_txtQuantity.text doubleValue];
    }
    else
    {
        finalUnitBaseInitialPrice=0;
        finalUnitBasePriceMaintPrice=0;
    }
    
    
    
    objentitySoldServiceStandardDetail.finalUnitBasedInitialPrice=[NSString stringWithFormat:@"%.2f",finalUnitBaseInitialPrice];
    objentitySoldServiceStandardDetail.finalUnitBasedMaintPrice=[NSString stringWithFormat:@"%.2f",finalUnitBasePriceMaintPrice];
    
    
    objentitySoldServiceStandardDetail.billingFrequencySysName=strBillingFreqSysName;
    
    
    objentitySoldServiceStandardDetail.discountPercentage=[self getNumberStringNew:_txtDiscountPercent.text];
    objentitySoldServiceStandardDetail.bundleDetailId=@"0";
    objentitySoldServiceStandardDetail.bundleId=@"0";
    //End
    
    // NSMutableArray *arrAdditionalData;
    // arrAdditionalData=[[NSMutableArray alloc]init];
    //objentitySoldServiceStandardDetail.additionalParameterPriceDcs=arrAdditionalData;
    
    //Create PARAMETER DATA Dictionary
    NSMutableArray *arrAdditionalData;
    arrAdditionalData=[[NSMutableArray alloc]init];
    for (int i=0; i<arrAdditionalParamterDcs.count; i++)
    {
        NSArray *arrKeyParameter,*arrValueParameter;
        NSDictionary *dictParameter=[arrAdditionalParamterDcs objectAtIndex:i];
        
        NSString *strFinalInitialUnitPrice;
        strFinalInitialUnitPrice=[NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"InitialUnitPrice"]];
        if ([[NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"InitialUnitPrice"]]floatValue]<[[NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"MinimumPrice"]]floatValue])
        {
           // strFinalInitialUnitPrice=[NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"MinimumPrice"]];
            
        }
        
        arrKeyParameter=[NSArray arrayWithObjects:
                         @"SoldServiceStandardId",
                         @"AdditionalParameterName",
                         @"UnitName",
                         @"AddiParamSysname",
                         @"Unit",
                         @"MinUnitPrice",
                         @"InitialUnitPrice",
                         @"MaintUnitPrice",
                         @"FinalInitialUnitPrice",
                         @"FinalMaintUnitPrice",
                         @"IsInitalPricechange",
                         @"IsMaintPricechange",
                         nil];
        arrValueParameter=[NSArray arrayWithObjects:
                           strRandomId,
                           [NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"Name"]],
                           [NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"UnitName"]],
                           [NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"SysName"]],
                           @"1",
                           [NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"MinimumPrice"]],
                           [NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"InitialUnitPrice"]],
                           [NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"MaintUnitPrice"]],
                           strFinalInitialUnitPrice,
                           [NSString stringWithFormat:@"%@",[dictParameter valueForKey:@"MaintUnitPrice"]],
                           @"0",
                           @"0",
                           nil];
        dictParameter=[NSDictionary dictionaryWithObjects:arrValueParameter forKeys:arrKeyParameter];
        [arrAdditionalData addObject:dictParameter];
        
    }
    objentitySoldServiceStandardDetail.additionalParameterPriceDcs=arrAdditionalData;
    objentitySoldServiceStandardDetail.billingFrequencyPrice=@"";
    
    
     objentitySoldServiceStandardDetail.internalNotes=@"";
    
    
    
    //For Total Final Initial Price
    float finalTotalInitialPrice=0.0;
    float finalTotalMaintPrice=0.0;
    
    
    for (int i=0; i<arrAdditionalData.count; i++)
    {
        NSDictionary *dict=[arrAdditionalData objectAtIndex:i];
        
        NSString *strFinalInitialPrice,*strFinalMaintPrice;
        strFinalInitialPrice=[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]];
        strFinalMaintPrice=[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]];
        
        finalTotalInitialPrice=finalTotalInitialPrice+[strFinalInitialPrice floatValue];
        finalTotalMaintPrice=finalTotalMaintPrice+[strFinalMaintPrice floatValue];
        
    }
    finalTotalInitialPrice=finalTotalInitialPrice+[_txtInitialPrice.text floatValue]*[_txtQuantity.text floatValue];
    
    finalTotalMaintPrice=finalTotalMaintPrice+[_txtMaintenance.text doubleValue]*[_txtQuantity.text doubleValue];
    
    objentitySoldServiceStandardDetail.totalInitialPrice=[NSString stringWithFormat:@"%.2f",finalTotalInitialPrice];
    objentitySoldServiceStandardDetail.totalMaintPrice=[NSString stringWithFormat:@"%.2f",finalTotalMaintPrice];
    
    strTotalInitialPrice = [NSString stringWithFormat:@"%.2f",finalTotalInitialPrice];
    
    objentitySoldServiceStandardDetail.categorySysName = strCategorySysName;
    objentitySoldServiceStandardDetail.servicePackageName=[dictPackageName valueForKey:strPackageId];


    
    NSError *error1;
    [context save:&error1];
    [self fetchFromCoreData];

}
#pragma mark- FETCH CORE DATA
-(void)fetchFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        }
    }
}
-(void)getFrequencySysNameFromName
{
    NSMutableArray *arrName,*arrSysName,*arrId;
    arrName=[[NSMutableArray alloc]init];
    arrSysName=[[NSMutableArray alloc]init];
    arrId=[[NSMutableArray alloc]init];
    
    
    
    NSArray *arrFreq=[dictMasters valueForKey:@"Frequencies"];
    for (int i=0; i<arrFreq.count; i++)
    {
        NSDictionary *dict=[arrFreq objectAtIndex:i];
        [arrName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]];
        [arrSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
        [arrId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyId"]]];
        
    }
    
    dictFreqSysName=[NSDictionary dictionaryWithObjects:arrSysName forKeys:arrName];
    dictFreqIdFromSysName=[NSDictionary dictionaryWithObjects:arrId forKeys:arrSysName];
    dictFreqNameFromSysName=[NSDictionary dictionaryWithObjects:arrName forKeys:arrSysName];
    
    
}
-(void)setDefaultBillingFreq
{
    arrBillingFreq=[[NSMutableArray alloc]init];
    NSArray *arrTempFreq=[dictMasters valueForKey:@"Frequencies"];
    [arrBillingFreq addObjectsFromArray:arrTempFreq];
    for (int i=0; i<arrBillingFreq.count; i++)
    {
        NSDictionary *dict=[arrBillingFreq objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]] isEqualToString:@"Monthly"]||[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]] isEqualToString:@"monthly"])
        {
            [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]forState:UIControlStateNormal];
            strBillingFreqSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
            break;
        }
    }
}
-(void)setPlaceHolderZero
{
    if ([_txtInitialPrice.text floatValue]==0 && ![_txtInitialPrice.text isEqualToString:@"TBD"])
    {
        _txtInitialPrice.text=@"";
        _txtInitialPrice.placeholder=@"0.00";
    }
    if ([_txtDiscount.text floatValue]==0 )
    {
        _txtDiscount.text=@"";
        _txtDiscount.placeholder=@"0.00";
    }
    if ([_txtDiscountPercent.text floatValue]==0)
    {
        _txtDiscountPercent.text=@"";
        _txtDiscountPercent.placeholder=@"0";
    }
    if ([_txtMaintenance.text floatValue]==0 && ![_txtMaintenance.text isEqualToString:@"TBD"])
    {
        _txtMaintenance.text=@"";
        _txtMaintenance.placeholder=@"0.00";
    }
}

//.......................................................

//Nilind 25 Sep
-(void)serviceUnitStatus
{
    NSMutableArray *qtyVal,*sysName,*arrParamService;
    arrParamService=[[NSMutableArray alloc]init];
    qtyVal=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasterss=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasterss valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [qtyVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsUnitBasedService"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [arrParamService addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsParameterizedPriced"]]];
                
                
            }
        }
    }
    dictQuantityStatus = [NSDictionary dictionaryWithObjects:qtyVal forKeys:sysName];
    dictServiceParmaterBasedStatus = [NSDictionary dictionaryWithObjects:arrParamService forKeys:sysName];
    
}
-(void)calculationForParamterBasedService:(NSString *)strCategorySysNamePara  ServiceSysName:(NSString *)strServiceSysNamePara
{
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        
        if ([strCategorySysNamePara isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]])
        {
            NSArray *arrServicePara=[dict valueForKey:@"Services"];
            
            for(int j=0;j<arrServicePara.count;j++)
            {
                NSDictionary *dictForJ=[arrServicePara objectAtIndex:j];
                
                if ([strServiceSysNamePara isEqualToString:[NSString stringWithFormat:@"%@",[dictForJ valueForKey:@"SysName"]]])
                {
                    NSArray *arrServiceParametersDcs=[dictForJ valueForKey:@"ServiceParametersDcs"];
                    arrZillowBasedSysName=[[NSMutableArray alloc]init];
                    
                    //Other Dict Add Defaule False
                    arrAdditionalParamterDcs=[[NSMutableArray alloc]init];
                    for (int l=0; l<arrServiceParametersDcs.count; l++)
                    {
                        NSDictionary *dictForL=[arrServiceParametersDcs objectAtIndex:l];
                        
                        if([[NSString stringWithFormat:@"%@",[dictForL valueForKey:@"IsDefault"]] isEqualToString:@"0"])
                        {
                            [arrAdditionalParamterDcs addObject:dictForL];
                        }
                    }
                    //End
                    
                    
                    for (int k=0; k<arrServiceParametersDcs.count; k++)
                    {
                        NSDictionary *dictForK=[arrServiceParametersDcs objectAtIndex:k];
                        NSString *strSysNameZillow=[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"SysName"]];
                        
                        
                        if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"1"])
                        {
                            if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"1"]&& [strSysNameZillow caseInsensitiveCompare:@"area"]==NSOrderedSame && strArea.length>0 && ![strSysNameZillow isEqualToString:@"0"])
                            {
                                
                                isParameterExist=YES;
                                
                                [arrZillowBasedSysName addObject:[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"SysName"]]];
                                
                            }
                            else if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"1"]&& [strSysNameZillow caseInsensitiveCompare:@"noOfBedroom"]==NSOrderedSame && strBedroom.length>0 && ![strSysNameZillow isEqualToString:@"0"])
                            {
                                
                                isParameterExist=YES;
                                
                                [arrZillowBasedSysName addObject:[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"SysName"]]];
                                
                            }
                            else if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"1"]&& [strSysNameZillow caseInsensitiveCompare:@"noOfBathroom"]==NSOrderedSame && strBathroom.length>0 && ![strSysNameZillow isEqualToString:@"0"])
                            {
                                
                                isParameterExist=YES;
                                
                                [arrZillowBasedSysName addObject:[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"SysName"]]];
                                
                            }
                            else if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"1"]&& [strSysNameZillow caseInsensitiveCompare:@"LotSize"]==NSOrderedSame && strLotsize.length>0 && ![strSysNameZillow isEqualToString:@"0"])
                            {
                                
                                isParameterExist=YES;
                                
                                [arrZillowBasedSysName addObject:[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"SysName"]]];
                                
                            }
                            else if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"1"]&& [strSysNameZillow caseInsensitiveCompare:@"linearSqFt"]==NSOrderedSame && strLinearSqFt.length>0 && ![strSysNameZillow isEqualToString:@"0"])
                            {
                                
                                isParameterExist=YES;
                                
                                [arrZillowBasedSysName addObject:[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"SysName"]]];
                                
                            }
                            //New
                            else if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"1"]&& [strSysNameZillow caseInsensitiveCompare:@"noOfStory"]==NSOrderedSame && strNoStory.length>0 && ![strSysNameZillow isEqualToString:@"0"])
                            {
                                
                                isParameterExist=YES;
                                
                                [arrZillowBasedSysName addObject:[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"SysName"]]];
                                
                            }
                            else if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"1"]&& [strSysNameZillow caseInsensitiveCompare:@"turfArea"]==NSOrderedSame && strTurfArea.length>0 && ![strSysNameZillow isEqualToString:@"0"])
                            {
                                
                                isParameterExist=YES;
                                
                                [arrZillowBasedSysName addObject:[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"SysName"]]];
                                
                            }
                            else if([[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"IsDefault"]] isEqualToString:@"1"]&& [strSysNameZillow caseInsensitiveCompare:@"shrubArea"]==NSOrderedSame && strShrubArea.length>0 && ![strSysNameZillow isEqualToString:@"0"])
                            {
                                
                                isParameterExist=YES;
                                
                                [arrZillowBasedSysName addObject:[NSString stringWithFormat:@"%@",[dictForK valueForKey:@"SysName"]]];
                                
                            }
                            else
                            {
                                isParameterExist=NO;
                                break;
                                
                            }
                            
                            
                            
                        }
                        
                        
                        
                    }
                    
                    //break;
                    
                }
            }
            
        }
    }
    // [self servicePricingSetupCalculation:@"" FrequencySysName:@"" ParameterSysName:arrZillowBasedSysName];
    
}
-(void)servicePricingSetupCalculation:(NSString *)strServiceSysNamePara FrequencySysName:(NSString *)strFreqSysNamePara ParameterSysName:(NSArray*)arrParaMeterSysName
{
    NSArray *arrServicePricingDetailDcs;
    NSArray *arrServicePricingSetup=[dictMasters valueForKey:@"ServicePricingSetup"];
    arrServicePricingSetup = [self getActiveArray:arrServicePricingSetup];
    for (int i=0; i<arrServicePricingSetup.count; i++)
    {
        NSDictionary *dict=[arrServicePricingSetup objectAtIndex:i];
        
        NSString *str1,*str2;
        str1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]];
        str2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencySysName"]];
        
        if ([str1 isEqualToString:strServiceSysNamePara]&&[str2 isEqualToString:strFreqSysNamePara])
        {
            arrServicePricingDetailDcs=[dict valueForKey:@"ServicePricingDetailDcs"];
            break;
            /*for (int j=0; j<arrServicePricingDetailDcs.count; j++)
             {
             NSDictionary *dictServicePricingDetailDcs=[arrServicePricingDetailDcs objectAtIndex:j];
             NSArray *arrServicePricingParameterDcs=[dictServicePricingDetailDcs valueForKey:@"ServicePricingParameterDcs" ];
             
             
             
             }*/
            
        }
        
    }
    BOOL tempZillow;
    NSString *initialPrice,*maintPrice;
    tempZillow=NO;
    NSLog(@">>%@",arrServicePricingDetailDcs);
    
    arrServicePricingDetailDcs = [self getActiveArray:arrServicePricingDetailDcs];
    
    for (int j=0; j<arrServicePricingDetailDcs.count; j++)
    {
        NSDictionary *dictServicePricingDetailDcs=[arrServicePricingDetailDcs objectAtIndex:j];
        NSArray *arrServicePricingParameterDcs=[dictServicePricingDetailDcs valueForKey:@"ServicePricingParameterDcs" ];
        
        tempZillow=NO;
        
        for(int l=0;l<arrParaMeterSysName.count;l++)
        {
            
            for (int k=0; k<arrServicePricingParameterDcs.count; k++)
            {
                NSDictionary *dictPriceParamaetesDCS=[arrServicePricingParameterDcs objectAtIndex:k];
                NSString *strSysNameZillow=[NSString stringWithFormat:@"%@",[dictPriceParamaetesDCS valueForKey:@"ParameterSysName"]];
                
                NSString *strRangeFrom,*strRangeTo;
                strRangeFrom=[NSString stringWithFormat:@"%@",[dictPriceParamaetesDCS valueForKey:@"RangeFrom"]];
                strRangeTo=[NSString stringWithFormat:@"%@",[dictPriceParamaetesDCS valueForKey:@"RangeTo"]];
                
                if ([[arrParaMeterSysName objectAtIndex:l] caseInsensitiveCompare:strSysNameZillow]==NSOrderedSame)
                {
                    if ([strSysNameZillow caseInsensitiveCompare:@"area"]==NSOrderedSame)
                    {
                        if (([strArea doubleValue]>[strRangeFrom doubleValue]||[strArea doubleValue]==[strRangeFrom doubleValue])&&([strArea doubleValue]<[strRangeTo doubleValue]||[strArea doubleValue]==[strRangeTo doubleValue]))
                        {
                            NSLog(@"matched range area");
                            tempZillow=YES;
                        }
                        else
                        {
                            tempZillow=NO;
                            
                            
                        }
                        break;
                    }
                    else if ([strSysNameZillow caseInsensitiveCompare:@"noOfBedroom"]==NSOrderedSame)
                    {
                        if ([strBedroom doubleValue]==[strRangeTo doubleValue]||[strBedroom doubleValue]<[strRangeTo doubleValue])
                        {
                            NSLog(@"matched range area");
                            tempZillow=YES;
                        }
                        else
                        {
                            tempZillow=NO;
                            
                            
                            
                            
                        }
                        break;
                        
                    }
                    else if ([strSysNameZillow caseInsensitiveCompare:@"noOfBathroom"]==NSOrderedSame)
                    {
                        
                        if ([strBathroom doubleValue]==[strRangeTo doubleValue]||[strBathroom doubleValue]<[strRangeTo doubleValue])
                        {
                            NSLog(@"matched range area");
                            tempZillow=YES;
                        }
                        else
                        {
                            tempZillow=NO;
                            
                            
                            
                            
                        }
                        break;
                    }
                    else if ([strSysNameZillow caseInsensitiveCompare:@"LinearSqFt"]==NSOrderedSame)
                    {
                        
                        if (([strLinearSqFt doubleValue]>[strRangeFrom doubleValue]||[strLinearSqFt doubleValue]==[strRangeFrom doubleValue])&&([strLinearSqFt doubleValue]<[strRangeTo doubleValue]||[strLinearSqFt doubleValue]==[strRangeTo doubleValue]))
                        {
                            NSLog(@"matched range area");
                            tempZillow=YES;
                        }
                        else
                        {
                            tempZillow=NO;
                            
                            
                        }
                        break;
                    }
                    else if ([strSysNameZillow caseInsensitiveCompare:@"LotSize"]==NSOrderedSame)
                    {
                        if (([strLotsize doubleValue]>[strRangeFrom doubleValue]||[strLotsize doubleValue]==[strRangeFrom doubleValue])&&([strLotsize doubleValue]<[strRangeTo doubleValue]||[strLotsize doubleValue]==[strRangeTo doubleValue]))
                        {
                            NSLog(@"matched range area");
                            tempZillow=YES;
                        }
                        else
                        {
                            tempZillow=NO;
                            
                            
                            
                        }
                        break;
                    }
                    //New 24 July
                    else if ([strSysNameZillow caseInsensitiveCompare:@"NoOfStory"]==NSOrderedSame)
                    {
                        if ([strNoStory doubleValue]==[strRangeTo doubleValue]||[strNoStory doubleValue]<[strRangeTo doubleValue])
                        {
                            NSLog(@"matched range area");
                            tempZillow=YES;
                        }
                        else
                        {
                            tempZillow=NO;
                            
                        }
                        break;
                    }
                    else if ([strSysNameZillow caseInsensitiveCompare:@"turfArea"]==NSOrderedSame)
                    {
                        if (([strTurfArea doubleValue]>[strRangeFrom doubleValue]||[strTurfArea doubleValue]==[strRangeFrom doubleValue])&&([strTurfArea doubleValue]<[strRangeTo doubleValue]||[strTurfArea doubleValue]==[strRangeTo doubleValue]))
                        {
                            NSLog(@"matched range area");
                            tempZillow=YES;
                        }
                        else
                        {
                            tempZillow=NO;
                            
                            
                            
                        }
                        break;
                    }
                    else if ([strSysNameZillow caseInsensitiveCompare:@"shrubArea"]==NSOrderedSame)
                    {
                        if (([strShrubArea doubleValue]>[strRangeFrom doubleValue]||[strShrubArea doubleValue]==[strRangeFrom doubleValue])&&([strShrubArea doubleValue]<[strRangeTo doubleValue]||[strShrubArea doubleValue]==[strRangeTo doubleValue]))
                        {
                            NSLog(@"matched shrub area");
                            tempZillow=YES;
                        }
                        else
                        {
                            tempZillow=NO;
                            
                            
                            
                        }
                        break;
                    }
                }
                
            }
            if (tempZillow==NO)
            {
                break;
            }
            
        }//End of For
        
        /* if (tempZillow==YES)
         {
         break;
         }*/
        
        
        
        //}
        
        
        initialPrice=[NSString stringWithFormat:@"%@",[dictServicePricingDetailDcs valueForKey:@"InitialPrice"]];
        maintPrice=[NSString stringWithFormat:@"%@",[dictServicePricingDetailDcs valueForKey:@"MaintenancePrice"]];
        if(tempZillow==YES)
        {
            if([[NSString stringWithFormat:@"%@",[dictServicePricingDetailDcs valueForKey:@"IsTBD"]] isEqualToString:@"1"])
            {
                chkParaTBD=YES;
            }
            break;
        }
        //        if (tempZillow==NO)
        //        {
        //            break;
        //        }
        
        /*  if (tempZillow==YES)
         {
         _txtInitialPrice.text=initialPrice;
         _txtMaintenance.text=maintPrice;
         _txtDiscount.text=@"0";
         _txtDiscountPercent.text=@"0";
         break;
         }
         else
         {
         _txtInitialPrice.text=@"TBD";
         _txtMaintenance.text=@"TBD";
         _txtDiscount.text=@"0";
         _txtDiscountPercent.text=@"0";
         break;
         }*/
        
        
    }
    if (tempZillow==YES)
    {
        _txtInitialPrice.text=initialPrice;
        _txtMaintenance.text=maintPrice;
        _txtDiscount.text=@"0";
        _txtDiscountPercent.text=@"0";
        if (chkParaTBD==YES)
        {
            _txtInitialPrice.text=@"TBD";
            _txtMaintenance.text=@"TBD";
            _txtDiscount.text=@"0";
            _txtDiscountPercent.text=@"0";
        }
    }
    else
    {
        _txtInitialPrice.text=@"TBD";
        _txtMaintenance.text=@"TBD";
        _txtDiscount.text=@"0";
        _txtDiscountPercent.text=@"0";
    }
}
-(void)getParameterBasedFreq :(NSString *)sysNameForParamterService
{
    arrFreqNew=[[NSMutableArray alloc]init];
    NSArray *arrServicePricingSetup=[dictMasters valueForKey:@"ServicePricingSetup"];
    arrServicePricingSetup = [self getActiveArray:arrServicePricingSetup];
    for (int i=0; i<arrServicePricingSetup.count; i++)
    {
        NSDictionary *dict=[arrServicePricingSetup objectAtIndex:i];
        
        NSString *strService,*str2;
        strService=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]];
        str2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencySysName"]];
        if ([strService isEqualToString:sysNameForParamterService])
        {
            //[arrFreqNew addObject:[dictFreqIdFromSysName valueForKey:str2]];
            if ([dictFreqIdFromSysName valueForKey:str2] == nil)
            {
                
            }
            else
            {
                [arrFreqNew addObject:[dictFreqIdFromSysName valueForKey:str2]];
            }
        }
        
    }
    
}
-(void)salesFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject*  matchesPara;
    if (arrAllObjSales.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesPara=arrAllObjSales[k];
            
            strArea=[matchesPara valueForKey:@"area"];
            strBedroom=[matchesPara valueForKey:@"noOfBedroom"];
            strBathroom=[matchesPara valueForKey:@"noOfBathroom"];
            strLinearSqFt=[matchesPara valueForKey:@"linearSqFt"];
            strLotsize=[matchesPara valueForKey:@"lotSizeSqFt"];
            strNoStory=[matchesPara valueForKey:@"noOfStory"];
            strTurfArea=[matchesPara valueForKey:@"turfArea"];
            strShrubArea=[matchesPara valueForKey:@"shrubArea"];
            strArea = [self handleBlankString:strArea];
            strBedroom = [self handleBlankString:strBedroom];
            strBathroom = [self handleBlankString:strBathroom];
            strLinearSqFt = [self handleBlankString:strLinearSqFt];
            strLotsize = [self handleBlankString:strLotsize];
            strNoStory = [self handleBlankString:strNoStory];
            strTurfArea = [self handleBlankString:strTurfArea];
            strShrubArea = [self handleBlankString:strShrubArea];
            
            NSLog(@"Lead IDDDD====%@",[matchesPara valueForKey:@"leadId"]);
        }
    }
}
-(NSString *)handleBlankString:(NSString*)str
{
    if ([str isEqualToString:@""] || str.length == 0)
    {
        str = @"0";
    }
    return str;
}
-(void)serviceNameFromId
{
    NSMutableArray *sysName,*arrServiceId,*arrTermsService,*arrPackageName,*arrPackageId,*arrDefaultFreqType,*arrDefaultFreqSysName,*arrServiceNameNew,*arrCategoryNameNew, *arrCategorySysNameNew,*arrPackageObjectFromId;
    sysName=[[NSMutableArray alloc]init];
    arrServiceId=[[NSMutableArray alloc]init];
    arrTermsService=[[NSMutableArray alloc]init];
    
    arrPackageName=[[NSMutableArray alloc]init];
    arrPackageId=[[NSMutableArray alloc]init];
    arrDefaultFreqType=[[NSMutableArray alloc]init];
    arrDefaultFreqSysName=[[NSMutableArray alloc]init];
    arrServiceNameNew = [[NSMutableArray alloc]init];
    arrCategoryNameNew = [[NSMutableArray alloc]init];
    arrCategorySysNameNew = [[NSMutableArray alloc]init];
    arrPackageObjectFromId  = [[NSMutableArray alloc]init];
    
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [arrServiceNameNew addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];

                [arrServiceId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceMasterId"]]];
                [arrTermsService addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TermsConditions"]]];
                
                [arrDefaultFreqType addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DefaultBillingFrequency"]]];
                [arrDefaultFreqSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DefaultBillingFrequencySysName"]]];
                //Temp
                NSArray *arrPackage=[dict valueForKey:@"ServicePackageDcs"];
                for (int k=0; k<arrPackage.count; k++)
                {
                    NSDictionary *dictPackage=[arrPackage objectAtIndex:k];
                    [arrPackageId addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"ServicePackageId"]]];
                    [arrPackageName addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"PackageName"]]];
                    [arrPackageObjectFromId addObject:dictPackage];
                }
                //End
                
            }
            [arrCategoryNameNew addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
            [arrCategorySysNameNew addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
        }
    }
    
    dictServiceNameFromSysName =[NSDictionary dictionaryWithObjects:arrServiceNameNew forKeys:sysName];
    dictCategoryNameFromSysName =[NSDictionary dictionaryWithObjects:arrCategoryNameNew forKeys:arrCategorySysNameNew];
    
    dictPackageName =[NSDictionary dictionaryWithObjects:arrPackageName forKeys:arrPackageId];
    
    dictServiceSysNameForId =[NSDictionary dictionaryWithObjects:sysName forKeys:arrServiceId];
    dictTermsForService=[NSDictionary dictionaryWithObjects:arrTermsService forKeys:sysName];
    
    dictDefaultFreqTypeService=[NSDictionary dictionaryWithObjects:arrDefaultFreqType forKeys:sysName];
    dictDefaultFreqSysNameService=[NSDictionary dictionaryWithObjects:arrDefaultFreqSysName forKeys:sysName];
    
    NSArray *arrFrequencyForBundle=[dictMasters valueForKey:@"Frequencies"];
    NSMutableArray *freqId,*freqSysName,*freqName;
    freqId=[[NSMutableArray alloc]init];
    freqSysName=[[NSMutableArray alloc]init];
    freqName=[[NSMutableArray alloc]init];
    for (int i=0; i<arrFrequencyForBundle.count; i++)
    {
        NSDictionary *dict=[arrFrequencyForBundle objectAtIndex:i];
        [freqId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyId"]]];
        [freqSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
        [freqName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]];
        
    }
    dictFreqSysNameFromId =[NSDictionary dictionaryWithObjects:freqSysName forKeys:freqId];
    dictFreqNameFromId =[NSDictionary dictionaryWithObjects:freqName forKeys:freqId];//freqSysName
    [self getCategoryForService];
}
-(void)getCategoryForService
{
    NSMutableArray *sysName,*sysNameCategory;
    sysName=[[NSMutableArray alloc]init];
    sysNameCategory=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasterss=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCatergory=[dictMasterss valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        
        NSArray *arrServices=[dict valueForKey:@"Services"];
        for (int j=0; j<arrServices.count; j++)
        {
            NSDictionary *dictService=[arrServices objectAtIndex:j];
            [sysNameCategory addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
            [sysName addObject:[NSString stringWithFormat:@"%@",[dictService valueForKey:@"SysName"]]];
        }
    }
    dictCategoryNameFromServiceSysName = [NSDictionary dictionaryWithObjects:sysNameCategory forKeys:sysName];
    
}
-(void)addUnitView
{
    _const_Quantity_H.constant=60;
}
-(void)removeUnitView
{
    _const_Quantity_H.constant=0;
}
-(void)forOneTimeFreq
{
    if ([_btnFrequency.currentTitle caseInsensitiveCompare:@"OneTime"]==NSOrderedSame||[_btnFrequency.currentTitle caseInsensitiveCompare:@"One Time"]==NSOrderedSame)
    {
        
        arrBillingFreq=[[NSMutableArray alloc]init];
        NSArray *arrTempFreq=[dictMasters valueForKey:@"Frequencies"];
        if (arrTempFreq.count==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No Billing Frequency Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
        }
        else
        {
            for (int i=0; i<arrTempFreq.count;i++)
            {
                NSDictionary *dict=[arrTempFreq objectAtIndex:i];
                [arrBillingFreq addObject:dict];
                
            }
            
            if ([_btnFrequency.currentTitle caseInsensitiveCompare:@"OneTime"]==NSOrderedSame||[_btnFrequency.currentTitle caseInsensitiveCompare:@"One Time"]==NSOrderedSame)
            {
                arrBillingFreq=[[NSMutableArray alloc]init];
                for (int i=0; i<arrTempFreq.count;i++)
                {
                    NSDictionary *dict=[arrTempFreq objectAtIndex:i];
                    
                    
                    if ([[dict valueForKey:@"SysName"]isEqualToString:@"OneTime"]||[[dict valueForKey:@"SysName"]isEqualToString:@"oneTime"])
                    {
                        [arrBillingFreq addObject:dict];
                        [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]forState:UIControlStateNormal];
                        strBillingFreqSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
                        break;
                    }
                }
            }
            
        }
    }
    
}
-(BOOL)checkForServiceAvailabiltiy
{
    BOOL chkAvailable = NO;
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@""])
            {
                if ([strServiceSysName isEqualToString:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]] && [[dictFreqSysNameFromId valueForKey:strFrequencyId] isEqualToString:[NSString stringWithFormat:@"%@",[matches valueForKey:@"frequencySysName"]]])
                {
                    chkAvailable =YES;
                    
                    break;
                }
            }
            else
            {
                chkAvailable = NO;
            }
            
            
            
        }
        
        
    }
    
    if ([_strForEdit isEqualToString:@"EditStandard"])
    {
        chkAvailable = NO;
    }
    
    
    return chkAvailable;
    
}
-(void)endEditing
{
    [self.view endEditing:YES];
}

#pragma mark: ---------------- Renewal Service Methods ----------------

-(BOOL)chkRenewalService:(NSString *)strCategorySysName : (NSString *)strServiceSysName
{
    BOOL isRenewal;
    isRenewal = NO;
    arrServiceMasterRenewalPrices = [[NSArray alloc]init];
    
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            if ([strCategorySysName isEqualToString:[dict valueForKey:@"SysName"]])
            {
                NSArray *arrServices=[dict valueForKey:@"Services"];
                for (int j=0; j<arrServices.count; j++)
                {
                    NSDictionary *dict=[arrServices objectAtIndex:j];
                    
                    if ([[NSString stringWithFormat: @"%@",[dict valueForKey:@"SysName"]] isEqualToString:strServiceSysName])
                    {
                        
                        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsRenewal"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsRenewal"]] isEqualToString:@"1"])
                        {
                            
                            arrServiceMasterRenewalPrices = [dict valueForKey:@"ServiceMasterRenewalPrices"];
                            
                            isRenewal = YES;
                            
                            break;
                        }
                    }
                    
                }
            }
        }
        if (isRenewal)
        {
            break;
        }
    }
    
    return  isRenewal;
}
-(void)saveRenewalServiceInToCoreData:(NSDictionary *)dictRenewalData MaintPrice:(NSString*)strInitialPrice ServiceId:(NSString *)strServiceId ServiceMasterId:(NSString *)strServiceMasterIdRenewal
{
    isEditedInSalesAuto=YES;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityRenewalServiceDetail=[NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
    
    RenewalServiceExtDcs *objentityRenewalServiceDetail = [[RenewalServiceExtDcs alloc]initWithEntity:entityRenewalServiceDetail insertIntoManagedObjectContext:context];
    
    objentityRenewalServiceDetail.leadId=strLeadId;
    objentityRenewalServiceDetail.userName=strUserName;
    objentityRenewalServiceDetail.companyKey=strCompanyKey;
    
    objentityRenewalServiceDetail.renewalServiceId = strServiceMasterIdRenewal;//[NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"RenewalServiceId"]];
    objentityRenewalServiceDetail.serviceId = strServiceMasterIdRenewal;
    
    
    if ([[NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"PriceBasedOn"]] isEqualToString:@"Percentage"])
    {
        objentityRenewalServiceDetail.renewalPercentage = [NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"Price_Percentage"]];
        
        float price = 0.0;
        float per = [[NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"Price_Percentage"]]floatValue];
        
        //[strInitialPrice floatValue]
         price = ([strInitialPrice floatValue]*per/100);
        
        if (price < [[NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"MinimumPrice"]]floatValue])
        {
            price = [[NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"MinimumPrice"]]floatValue];
        }
        
        objentityRenewalServiceDetail.renewalAmount = [NSString stringWithFormat:@"%.2f",price];
    }
    else
    {
        objentityRenewalServiceDetail.renewalPercentage = @"0";
    }
    
    
    //
    if ([[NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"PriceBasedOn"]] isEqualToString:@"FlatPrice"])
    {
        objentityRenewalServiceDetail.renewalAmount = [NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"Price_Percentage"]];
    }
    
    
    objentityRenewalServiceDetail.renewalFrequencySysName = [NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"FrequencySysName"]];
    
    objentityRenewalServiceDetail.renewalDescription = [NSString stringWithFormat:@"%@",[dictRenewalData valueForKey:@"Description"]];
    
    //objentityRenewalServiceDetail.soldServiceStandardId = strSoldServiceStandardId;
    objentityRenewalServiceDetail.soldServiceStandardId = strServiceId;

    
    NSError *error1;
    [context save:&error1];
}
-(void)addRenewalService
{
    if (![self checkRenewalServiceExistence:strServiceMasterId])
    {
        BOOL chkRenewalStatus;
        chkRenewalStatus = [self chkRenewalService:strCategorySysName:strServiceSysName];
        NSLog(@"%@",arrServiceMasterRenewalPrices);
        
        if ((arrServiceMasterRenewalPrices.count > 0 ) && chkRenewalStatus == YES )
        {
            NSDictionary *dict = [arrServiceMasterRenewalPrices objectAtIndex:0];
            [self saveRenewalServiceInToCoreData:dict MaintPrice:strTotalInitialPrice ServiceId:strSoldServiceStandardId ServiceMasterId:strServiceMasterId];
            
        }
    }
}
-(BOOL)checkRenewalServiceExistence:(NSString *)strRenewalServiceId
{
    BOOL chkRenewalServiceExist;
    chkRenewalServiceExist = NO;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityRenewalServiceDetail=[NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
    
    requestNew = [[NSFetchRequest alloc] init];
    
    [requestNew setEntity:entityRenewalServiceDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && serviceId=%@",strLeadId,strRenewalServiceId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arrAllObjRenewal = [self.fetchedResultsControllerSalesInfo fetchedObjects];

    if (arrAllObjRenewal.count > 0)
    {
        chkRenewalServiceExist = YES;
    }
    return chkRenewalServiceExist;
}

-(NSArray *)getActiveArray :(NSArray *)arrAllObjects
{
    //Nilind
      
      NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
      for (int i=0; i<arrAllObjects.count; i++) {
          
          NSDictionary *dict = [arrAllObjects objectAtIndex:i];
          if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]] isEqualToString:@"True"])
          {
              [arrTemp addObject:dict];
          }
      }
      //ENd
    NSArray *arrAcvite = [[NSArray alloc]init];
    
    arrAcvite = arrTemp;
    
    return  arrAcvite;
}

#pragma mark: ----------------- Billing Frequency Setup ---------------

-(void)billingFrequencySetup:(NSString *)strDefaultBillingFreqType :(NSString*)strDefaultBillingFreqSysname
{
    NSString *strFrqSysNameNew, *strServiceFreqSysNameNew;;
    strFrqSysNameNew = @"";
    strServiceFreqSysNameNew = @"";
    strServiceFreqSysNameNew = [NSString stringWithFormat:@"%@",[dictFreqSysNameFromId valueForKey:strFrequencyId]];
    
    
    if ([strDefaultBillingFreqType isEqualToString:@"SameAsService"])
    {
        strFrqSysNameNew=[NSString stringWithFormat:@"%@",[dictFreqSysNameFromId valueForKey:strFrequencyId]];
        
        if ([strServiceFreqSysNameNew caseInsensitiveCompare:@"OneTime"] == NSOrderedSame)
        {
            
        }
        else
        {
            [self setBillingFrequency:strFrqSysNameNew];
        }
    }
    else if ([strDefaultBillingFreqType isEqualToString:@"OtherFrequency"])
    {
        strFrqSysNameNew=[NSString stringWithFormat:@"%@",strDefaultBillingFreqSysname];
        
        if ([strServiceFreqSysNameNew caseInsensitiveCompare:@"OneTime"] == NSOrderedSame)
        {
            
        }
        else
        {
            [self setBillingFrequency:strFrqSysNameNew];
        }
    }
    else if ([strDefaultBillingFreqType isEqualToString:@""])
    {
        [self forOneTimeFreq];
    }
}
-(void)setBillingFrequency : (NSString *)strFrqSysNameNew
{
    arrBillingFreq=[[NSMutableArray alloc]init];
    NSArray *arrTempFreq=[dictMasters valueForKey:@"Frequencies"];
    [arrBillingFreq addObjectsFromArray:arrTempFreq];
    bool chkMatched;
    chkMatched = NO;
    for (int i=0; i<arrBillingFreq.count; i++)
    {
        NSDictionary *dict=[arrBillingFreq objectAtIndex:i];
        
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]] isEqualToString:strFrqSysNameNew])
        {
            [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]forState:UIControlStateNormal];
            strBillingFreqSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
            chkMatched = YES;
         //   [_btnBillingFreqBundle setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]forState:UIControlStateNormal];
            break;
        }
    }
    if (chkMatched == NO)
    {
        [self forOneTimeFreq];
    }
}
#pragma mark: ----------------- Frequency Type Setup ---------------


-(NSMutableArray *)getServiceFrequencyAsPerType:(NSMutableArray *)arrFreq Type:(NSString *)strTypeFreq
{
    NSMutableArray *arrTempFreq = [[NSMutableArray alloc]init];
    
    NSMutableArray *arrFreqFromId = [[NSMutableArray alloc]init];
    NSArray *arrAllFreq=[dictMasters valueForKey:@"Frequencies"];
    
    for (int i =0; i<arrFreq.count;i++)
    {
        NSString *strFreqId = [NSString stringWithFormat:@"%@",[arrFreq objectAtIndex:i]];
        for(int j=0;j<arrAllFreq.count;j++)
        {
            NSDictionary *dict = [arrAllFreq objectAtIndex:j];
            if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyId"]] isEqualToString:strFreqId])
            {
                [arrFreqFromId addObject:dict];
            }
        }
    }
    
    //Temp
    NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
    arrTemp = [self sortArrayByType:arrFreqFromId Type:@"FrequencyName"];
    arrFreqFromId = [[NSMutableArray alloc]init];
    [arrFreqFromId addObjectsFromArray:arrTemp];
    //

    for(int i=0;i<arrFreqFromId.count;i++)
    {
        NSDictionary *dict = [arrFreqFromId objectAtIndex:i];
        NSString *strType = [NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyType"]];
        if ([strType isEqualToString:strTypeFreq] || [strType isEqualToString:@"Both"] || [strType isEqualToString:@""])
        {
            //[arrTempFreq addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyId"]]];
        }
         [arrTempFreq addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyId"]]];
    }
    return arrTempFreq;
}

-(NSMutableArray *)getBillingFrequencyAsPerType:(NSMutableArray *)arrFreq Type:(NSString *)strTypeFreq
{
    NSMutableArray *arrTempFreq = [[NSMutableArray alloc]init];
    
    for(int i=0;i<arrFreq.count;i++)
    {
        NSDictionary *dict = [arrFreq objectAtIndex:i];
        NSString *strType = [NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyType"]];
        if ([strType isEqualToString:strTypeFreq] || [strType isEqualToString:@"Both"] || [strType isEqualToString:@""] )
        {
            [arrTempFreq addObject:dict];
        }
    }
    
    //Temp
    NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
    arrTemp = [self sortArrayByType:arrTempFreq Type:@"FrequencyName"];
    arrTempFreq = [[NSMutableArray alloc]init];
    [arrTempFreq addObjectsFromArray:arrTemp];
    //
    
    return arrTempFreq;
}

-(NSMutableArray *)sortArrayByType:(NSMutableArray *)arr Type:(NSString *)strType
{
     NSMutableArray *arrTemp = (NSMutableArray*)arr;
    
     NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"FrequencyName"
         ascending:YES];
     NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
     NSArray *sortedArray = [arrTemp sortedArrayUsingDescriptors:sortDescriptors];
     NSLog(@"%@",sortedArray);
    
    
    NSMutableArray *arrFinal = [[NSMutableArray alloc]init];
    [arrFinal addObjectsFromArray:sortedArray];
    
    return  arrFinal;
}
-(void)setTextCorner
{
    _txtDiscount.layer.masksToBounds = YES;
    _txtQuantity.layer.masksToBounds = YES;
    _txtInitialPrice.layer.masksToBounds = YES;
    _txtMaintenance.layer.masksToBounds = YES;
    _txtDiscountPercent.layer.masksToBounds = YES;
}
#pragma mark: ----------------- Edit Price ---------------

-(void)editServiceDetail
{
    if ([_strForEdit isEqualToString:@"EditStandard"])
    {
        
        //[_btnCategory setEnabled:NO];
        [_btnService setEnabled:NO];
        //[_btnPackage setEnabled:NO];

        if ([_matchesServiceToBeEdited isKindOfClass:[NSManagedObject class]])
        {
            
            strSoldServiceStandardId = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"soldServiceStandardId"]];

            //Category
            
            strCategorySysName = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"categorySysName"]];
            //[_btnCategory setTitle:[dictCategoryNameFromSysName valueForKey:strCategorySysName] forState:UIControlStateNormal];
            
            //Service
            
            strServiceMasterId = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"serviceId"]];
            strServiceSysName = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"serviceSysName"]];
            [_btnService setTitle:[dictServiceNameFromSysName valueForKey:strServiceSysName] forState:UIControlStateNormal];
            
            if ([[dictQuantityStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                [self addUnitView];
                
                _txtQuantity.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"unit"]];

            }
            else
            {
                [self removeUnitView];
            }
            
            //Package
            
            strPackageId = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"packageId"]];
            
            [_btnPackage setTitle:[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"servicePackageName"]] forState:UIControlStateNormal];
            
            if ([[dictServiceParmaterBasedStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                isParameterBasedService=YES;
                strPackageId=@"";
                _const_BtnPackage_H.constant=0;
                strServiceSysNameForParamterService=strServiceSysName;
            }
            else
            {
                isParameterBasedService=NO;
                _const_BtnPackage_H.constant=50;
            }
            
            
            //ServiceFrequency
            
            strFrequencyId = [NSString stringWithFormat:@"%@",[dictFreqIdFromSysName valueForKey:[_matchesServiceToBeEdited valueForKey:@"frequencySysName"]]];
            
            [_btnFrequency setTitle:[_matchesServiceToBeEdited valueForKey:@"serviceFrequency"] forState:UIControlStateNormal];
            
            //Billing Frequency
            
            strBillingFreqSysName=[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"billingFrequencySysName"]];
            
            [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:strBillingFreqSysName]]forState:UIControlStateNormal];
            
            //Price
            
            _txtInitialPrice.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"initialPrice"]];
            _txtMaintenance.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"maintenancePrice"]];
            
            _txtDiscount.text = [NSString stringWithFormat:@"%.2f",[[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"discount"]]floatValue]];
            
            
            _txtDiscountPercent.text = [NSString stringWithFormat:@"%.2f",[[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"discountPercentage"]]floatValue]];
            
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = [dictPackageObjFromId valueForKey:strPackageId];
            
            if (![dict isEqual:nil])
            {
                NSArray *arr = [dict valueForKey:@"ServicePackageDetails"];
                
                for (int i=0; i<arr.count; i++)
                {
                    NSDictionary *dictPackageDcs = [arr objectAtIndex:i];
                    
                    //if ([strPackageId isEqualToString:[NSString stringWithFormat:@"%@",[dictPackageDcs valueForKey:@"ServicePackageId"]]] && [strFrequencyId isEqualToString:[NSString stringWithFormat:@"%@",[dictPackageDcs valueForKey:@"FrequencyId"]]])
                    if ([strFrequencyId isEqualToString:[NSString stringWithFormat:@"%@",[dictPackageDcs valueForKey:@"FrequencyId"]]])
                    {
                        if (![dictPackageDcs isEqual:nil])
                        {
                            strMinPackageCost = [NSString stringWithFormat:@"%@",[dictPackageDcs valueForKey:@"MinPackageCost"]];
                            strMinPackageMaintCost = [NSString stringWithFormat:@"%@",[dictPackageDcs valueForKey:@"MinPackageMaintCost"]];
                            break;
                        }
                    }
                }
                
                
            }
            
        }
    }

}
-(void)updateServiceDetails
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceStandardId=%@",strLeadId,strSoldServiceStandardId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesServiceUpdate;
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matchesServiceUpdate=arrAllObj[i];
            
            [matchesServiceUpdate setValue:_txtInitialPrice.text forKey:@"initialPrice"];
            
            if ([_txtInitialPrice.text isEqualToString:@"TBD"])
            {
                [matchesServiceUpdate setValue:@"true" forKey:@"isTBD"];

            }
            else
            {
                [matchesServiceUpdate setValue:@"false" forKey:@"isTBD"];
            }

            [matchesServiceUpdate setValue:_txtMaintenance.text forKey:@"maintenancePrice"];
            
            [matchesServiceUpdate setValue:_txtDiscount.text forKey:@"discount"];
            
            //[matchesServiceUpdate setValue:strPackageId forKey:@"packageId"];

            [matchesServiceUpdate setValue:[self getNumberStringNew:_txtDiscountPercent.text] forKey:@"discountPercentage"];
            
            if (isCouponApplied)
            {
                [matchesServiceUpdate setValue:@"0.0" forKey:@"discount"];
                
                [matchesServiceUpdate setValue:@"0.0" forKey:@"discountPercentage"];
            }
            
            if ([[dictQuantityStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                [matchesServiceUpdate setValue:_txtQuantity.text forKey:@"unit"];
                
                double finalUnitBaseInitialPrice,finalUnitBasePriceMaintPrice;
                if ([[dictQuantityStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
                {
                    finalUnitBaseInitialPrice=[_txtInitialPrice.text doubleValue]*[_txtQuantity.text doubleValue];
                    
                    finalUnitBasePriceMaintPrice=[_txtMaintenance.text doubleValue]*[_txtQuantity.text doubleValue];
                }
                else
                {
                    finalUnitBaseInitialPrice=0;
                    finalUnitBasePriceMaintPrice=0;
                }

                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",finalUnitBaseInitialPrice] forKey:@"finalUnitBasedInitialPrice"];

                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",finalUnitBasePriceMaintPrice] forKey:@"finalUnitBasedMaintPrice"];
            }
            
            if ([[dictServiceParmaterBasedStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                float paraInitialPrice = 0;
                float paraMaintPrice = 0;

                NSArray *arrAdditionalParameter = [_matchesServiceToBeEdited valueForKey:@"additionalParameterPriceDcs"];
                
                if ([arrAdditionalParameter isKindOfClass:[NSArray class]] && arrAdditionalParameter.count > 0)
                {
                    for (int i=0; i<arrAdditionalParameter.count > 0; i++)
                    {
                        NSDictionary *dict = [arrAdditionalParameter objectAtIndex:i];
                        
                        NSString *strParaInitialPrice = [NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]];
                        NSString *strParaMaintPrice = [NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]];
                        paraInitialPrice = paraInitialPrice + [strParaInitialPrice floatValue];
                        paraMaintPrice = paraMaintPrice + [strParaMaintPrice floatValue];

                    }
                }
                
                float finalTotalInitialPrice=0;
                finalTotalInitialPrice=paraInitialPrice+[_txtInitialPrice.text floatValue]*[_txtQuantity.text floatValue];
                
                float finalTotalMaintPrice=0;
                finalTotalMaintPrice=paraMaintPrice+[_txtMaintenance.text doubleValue]*[_txtQuantity.text doubleValue];
                
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",finalTotalInitialPrice] forKey:@"totalInitialPrice"];
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",finalTotalMaintPrice] forKey:@"totalMaintPrice"];
            }
            else
            {
                float initialPrice = 0;
                float maintPrice = 0;
                
                initialPrice= [_txtInitialPrice.text floatValue]*[_txtQuantity.text floatValue];
                maintPrice= [_txtMaintenance.text floatValue]*[_txtQuantity.text floatValue];
                
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",initialPrice] forKey:@"totalInitialPrice"];
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",maintPrice] forKey:@"totalMaintPrice"];

            }
            
            [matchesServiceUpdate setValue:[dictFreqNameFromId valueForKey:strFrequencyId] forKey:@"serviceFrequency"];
            [matchesServiceUpdate setValue:[dictFreqSysNameFromId valueForKey:strFrequencyId] forKey:@"frequencySysName"];
            [matchesServiceUpdate setValue:strBillingFreqSysName forKey:@"billingFrequencySysName"];

        }
        [context save:&error1];
    }
}
-(void)deleteAppliedCoupon:(NSString *)strServiceId ServiceSysName:  (NSString*)serviceSysName
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
        
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceId = %@",strLeadId,strServiceId];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];

    for (NSManagedObject * data in Data)
    {
        [context deleteObject:data];
        
    }
    [context save:&error];
}
-(void)fetchForAppliedDiscountFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceId=%@",strLeadId,strSoldServiceStandardId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    arrAppliedInitialPrice = [[NSMutableArray alloc]init];
    arrAppliedMaintenancePrice = [[NSMutableArray alloc]init];

    if (arrAllObjSales.count==0)
    {
        isCouponApplied = NO;
    }
    else
    {
        isCouponApplied = YES;

        for (int i=0; i<arrAllObjSales.count; i++)
        {
            matchesDiscount = [arrAllObjSales objectAtIndex:i];
            
            if ([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
            {
                NSString *strAppliedIntialPrice, *strAppliedMaintPrice;
                strAppliedIntialPrice = [matchesDiscount valueForKey:@"appliedInitialDiscount"];
                strAppliedMaintPrice = [matchesDiscount valueForKey:@"appliedMaintDiscount"];
                
                [arrAppliedInitialPrice addObject:strAppliedIntialPrice];
                [arrAppliedMaintenancePrice addObject:strAppliedMaintPrice];
            }

        }
    }
}
-(void)updateServiceDiscount:(NSString *)strCouponType
{
    strCouponType = @"Coupon";
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceStandardId=%@",strLeadId,strSoldServiceStandardId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesServiceUpdate;
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matchesServiceUpdate=arrAllObj[i];
            
            NSString *strDiscount,*strDiscountPer, *strTotalInitialPrice, *strTotalMaintPrice;
            strDiscount = [matchesServiceUpdate valueForKey:@"discount"];
            strDiscountPer = [matchesServiceUpdate valueForKey:@"discountPercentage"];
            strTotalInitialPrice = [matchesServiceUpdate valueForKey:@"totalInitialPrice"];
            strTotalMaintPrice = [matchesServiceUpdate valueForKey:@"totalMaintPrice"];
            
            float discount,discountPer;
            discount = 0;
            discountPer = 0;
            
            if ([strCouponType isEqualToString:@"Coupon"] )
            {
                for(int j=0;j<arrAppliedInitialPrice.count;j++)
                {
                    discount = [strDiscount floatValue] - [[arrAppliedInitialPrice objectAtIndex:j]floatValue];
                }
                if (discount < 0)
                {
                    discount = 0;
                }
                discountPer = (discount * 100)/ [strTotalInitialPrice floatValue];
                
                if (discount < 0)
                {
                    discount = 0;
                }
                if (discountPer < 0)
                {
                    discountPer = 0;
                }
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",discount] forKey:@"discount"];
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",discountPer] forKey:@"discountPercentage"];
                
                [matchesServiceUpdate setValue:@"0.0" forKey:@"discount"];
                
                [matchesServiceUpdate setValue:@"0.0" forKey:@"discountPercentage"];
                
                
            }
            if ([strCouponType isEqualToString:@"Credit"] )
            {
                if ([strCouponType isEqualToString:@"ApplicableForInitial"] )
                {
                    
                }
                else if ([strCouponType isEqualToString:@"ApplicableForInitial"] && [strCouponType isEqualToString:@"ApplicableForMaintenance"] )
                {
                    
                }

            }


        }
        [context save:&error1];
    }
}




-(void)editServiceDetailOld
{
    if ([_strForEdit isEqualToString:@"EditStandard"])
    {
        //[_btnCategory setEnabled:NO];
        [_btnService setEnabled:NO];
        //[_btnPackage setEnabled:NO];

        if ([_matchesServiceToBeEdited isKindOfClass:[NSManagedObject class]])
        {
            
            strSoldServiceStandardId = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"soldServiceStandardId"]];
            
            //Category
            
            strCategorySysName = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"categorySysName"]];
            //[_btnCategory setTitle:[dictCategoryNameFromSysName valueForKey:strCategorySysName] forState:UIControlStateNormal];
            
            //Service
            
            strServiceMasterId = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"serviceId"]];
            strServiceSysName = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"serviceSysName"]];
            [_btnService setTitle:[dictServiceNameFromSysName valueForKey:strServiceSysName] forState:UIControlStateNormal];
            
            if ([[dictQuantityStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                [self addUnitView];
                
                _txtQuantity.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"unit"]];

            }
            else
            {
                [self removeUnitView];
            }
            
            //Package
            
            strPackageId = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"packageId"]];
            
            [_btnPackage setTitle:[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"servicePackageName"]] forState:UIControlStateNormal];
            
            if ([[dictServiceParmaterBasedStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                isParameterBasedService=YES;
                strPackageId=@"";
                _const_BtnPackage_H.constant=0;
                strServiceSysNameForParamterService=strServiceSysName;
            }
            else
            {
                isParameterBasedService=NO;
                _const_BtnPackage_H.constant=50;
            }
            
            
            
            //ServiceFrequency
            
            strFrequencyId = [NSString stringWithFormat:@"%@",[dictFreqIdFromSysName valueForKey:[_matchesServiceToBeEdited valueForKey:@"frequencySysName"]]];
            
            [_btnFrequency setTitle:[_matchesServiceToBeEdited valueForKey:@"serviceFrequency"] forState:UIControlStateNormal];
            
            //Billing Frequency
            
            strBillingFreqSysName=[NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"billingFrequencySysName"]];
            
            [_btnBillingFrequency setTitle:[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:strBillingFreqSysName]]forState:UIControlStateNormal];
            
            
            
            _strFreqName = [_matchesServiceToBeEdited valueForKey:@"serviceFrequency"];
            _strBillingFreqName = [NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:strBillingFreqSysName]];
            //Price
            
            _txtInitialPrice.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"initialPrice"]];
            _txtMaintenance.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"maintenancePrice"]];
            _txtDiscount.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"discount"]];
            _txtDiscountPercent.text = [NSString stringWithFormat:@"%@",[_matchesServiceToBeEdited valueForKey:@"discountPercentage"]];
            
        }
    }

}
-(void)updateServiceDetailsOld
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && soldServiceStandardId=%@",strLeadId,strSoldServiceStandardId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesServiceUpdate;
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matchesServiceUpdate=arrAllObj[i];
            
            [matchesServiceUpdate setValue:_txtInitialPrice.text forKey:@"initialPrice"];
            
            if ([_txtInitialPrice.text isEqualToString:@"TBD"])
            {
                [matchesServiceUpdate setValue:@"true" forKey:@"isTBD"];

            }
            else
            {
                [matchesServiceUpdate setValue:@"false" forKey:@"isTBD"];
            }

            [matchesServiceUpdate setValue:_txtMaintenance.text forKey:@"maintenancePrice"];
            
            [matchesServiceUpdate setValue:_txtDiscount.text forKey:@"discount"];
            
            [matchesServiceUpdate setValue:[self getNumberStringNew:_txtDiscountPercent.text] forKey:@"discountPercentage"];
            
            [matchesServiceUpdate setValue:@"0.0" forKey:@"discount"];
            
            [matchesServiceUpdate setValue:@"0.0" forKey:@"discountPercentage"];
            
            if ([[dictQuantityStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                [matchesServiceUpdate setValue:_txtQuantity.text forKey:@"unit"];
                
                double finalUnitBaseInitialPrice,finalUnitBasePriceMaintPrice;
                if ([[dictQuantityStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
                {
                    finalUnitBaseInitialPrice=[_txtInitialPrice.text doubleValue]*[_txtQuantity.text doubleValue];
                    
                    finalUnitBasePriceMaintPrice=[_txtMaintenance.text doubleValue]*[_txtQuantity.text doubleValue];
                }
                else
                {
                    finalUnitBaseInitialPrice=0;
                    finalUnitBasePriceMaintPrice=0;
                }

                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",finalUnitBaseInitialPrice] forKey:@"finalUnitBasedInitialPrice"];

                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",finalUnitBasePriceMaintPrice] forKey:@"finalUnitBasedMaintPrice"];
            }
            
            if ([[dictServiceParmaterBasedStatus valueForKey:strServiceSysName]isEqualToString:@"1"])
            {
                float paraInitialPrice = 0;
                float paraMaintPrice = 0;

                NSArray *arrAdditionalParameter = [_matchesServiceToBeEdited valueForKey:@"additionalParameterPriceDcs"];
                
                if ([arrAdditionalParameter isKindOfClass:[NSArray class]] && arrAdditionalParameter.count > 0)
                {
                    for (int i=0; i<arrAdditionalParameter.count > 0; i++)
                    {
                        NSDictionary *dict = [arrAdditionalParameter objectAtIndex:i];
                        
                        NSString *strParaInitialPrice = [NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]];
                        NSString *strParaMaintPrice = [NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]];
                        paraInitialPrice = paraInitialPrice + [strParaInitialPrice floatValue];
                        paraMaintPrice = paraMaintPrice + [strParaMaintPrice floatValue];

                    }
                }
                
                float finalTotalInitialPrice=0;
                finalTotalInitialPrice=paraInitialPrice+[_txtInitialPrice.text floatValue]*[_txtQuantity.text floatValue];
                
                float finalTotalMaintPrice=0;
                finalTotalMaintPrice=paraMaintPrice+[_txtMaintenance.text doubleValue]*[_txtQuantity.text doubleValue];
                
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",finalTotalInitialPrice] forKey:@"totalInitialPrice"];
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",finalTotalMaintPrice] forKey:@"totalMaintPrice"];
            }
            else
            {
                float initialPrice = 0;
                float maintPrice = 0;
                
                initialPrice= [_txtInitialPrice.text floatValue]*[_txtQuantity.text floatValue];
                maintPrice= [_txtMaintenance.text floatValue]*[_txtQuantity.text floatValue];
                
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",initialPrice] forKey:@"totalInitialPrice"];
                [matchesServiceUpdate setValue:[NSString stringWithFormat:@"%.2f",maintPrice] forKey:@"totalMaintPrice"];

            }
            
            [matchesServiceUpdate setValue:[dictFreqNameFromId valueForKey:strFrequencyId] forKey:@"serviceFrequency"];
            [matchesServiceUpdate setValue:[dictFreqSysNameFromId valueForKey:strFrequencyId] forKey:@"frequencySysName"];
            [matchesServiceUpdate setValue:strBillingFreqSysName forKey:@"billingFrequencySysName"];

        }
        [context save:&error1];
    }
}
@end

