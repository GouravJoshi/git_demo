
//  Created by Rakesh Jain on 19/07/16. changes28 Feb New Nilidnd
//Nilind Sharma changes changes changes Changes Sales Nilind
//  Copyright © 2016 Saavan. All rights reserved. changes changes changes changes changes Nilind
// changes Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind Nilind
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "CurrentService.h"
#import <MessageUI/MessageUI.h>


@interface SaleAutomationGeneralInfoiPad : UIViewController<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIDocumentInteractionControllerDelegate,MFMailComposeViewControllerDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityTask;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entityCurrentService,
    *entitySoldServiceStandardDetail,
    *entityContactNumberDetail;
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewLeads;
@property (strong, nonatomic) IBOutlet UIView *viewTop;
@property(strong,nonatomic)NSManagedObject *matchesGeneralInfo;

- (IBAction)actoinBack:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtLead;

@property (strong, nonatomic) IBOutlet UITextField *txtCustomerName;
@property (strong, nonatomic) IBOutlet UITextField *txtScheduleStartDate;
@property (strong, nonatomic) IBOutlet UITextField *txtAccount;

@property (strong, nonatomic) IBOutlet UITextField *txtPrimaryEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtSecondayEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPrimaryPhone;

@property (strong, nonatomic) IBOutlet UITextField *txtSecondaryPhone;

@property (strong, nonatomic) IBOutlet UIButton *btnStatus;
- (IBAction)actionOnStatus:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnStage;
- (IBAction)actionOnStage:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnPriority;
- (IBAction)actionOnPriority:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnVisibility;

- (IBAction)actionOnVisibility:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSource;

- (IBAction)actionOnSource:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnInspector;
- (IBAction)actionOnInspector:(id)sender;



@property (weak, nonatomic) IBOutlet UITextField *txtCityBillingAddress;

@property (weak, nonatomic) IBOutlet UITextField *txtZipCodeBillingAddress;






@property (weak, nonatomic) IBOutlet UITextField *txtCityServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtZipcodeServiceAddress;
#pragma mark- Outlet ServiceRequired
@property (weak, nonatomic) IBOutlet UITextView *txtViewNotes;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtTagServiceRequired;
- (IBAction)actionOnAttachImage:(id)sender;

- (IBAction)actionOnSaveContinue:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
@property(strong,nonatomic)NSDictionary *dictSaleAuto;
- (IBAction)actionOnSave:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *txtViewAdd1ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAdd2ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAdd1BillingAddress;

@property (weak, nonatomic) IBOutlet UITextView *txtViewAdd2BillingAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnCountryServiceAddress;

@property (weak, nonatomic) IBOutlet UIButton *btnStateServiceAddress;

@property (weak, nonatomic) IBOutlet UIButton *btnCountryBillingAddress;

@property (weak, nonatomic) IBOutlet UIButton *btnStateBillingAddress;


- (IBAction)actionOnCountryServiceAddress:(id)sender;

- (IBAction)actionOnStateServiceAddress:(id)sender;
- (IBAction)actionOnCountryBillingAddress:(id)sender;
- (IBAction)actionOnStateBillingAddress:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
- (IBAction)actionOnCheckBox:(id)sender;
- (IBAction)actionOnUploadDocument:(id)sender;

//Nilind 30 Sept
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyName;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtMiddleName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextView *txtViewCurrentServices;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLblCurrenService_H;

//................................
//Nilind 4 Oct


@property (weak, nonatomic) IBOutlet UITextField *txtBranchName;

//...............................
@property (weak, nonatomic) IBOutlet UILabel *lblNameAcount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ScrollLead_H;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewDetail;
@property(strong,nonatomic)NSString *strFromAppoint;
@property (weak, nonatomic) IBOutlet UIButton *btnAddImages;
@property (weak, nonatomic) IBOutlet UIButton *btnAddGraphImageFooter;
@property (weak, nonatomic) IBOutlet UIButton *btnAddImageFooter;


@property (weak, nonatomic) IBOutlet UIButton *btnUploadDocument;

//Nilind 06 Jan

@property (weak, nonatomic) IBOutlet UILabel *lblBranchName;

@property (weak, nonatomic) IBOutlet UIButton *buttonImg1;
@property (weak, nonatomic) IBOutlet UIButton *buttonImg2;
@property (weak, nonatomic) IBOutlet UIButton *buttonImg3;
- (IBAction)actionOn_buttonImg1:(id)sender;
- (IBAction)actionOn_buttonImg2:(id)sender;
- (IBAction)actionOn_buttonImg3:(id)sender;
//.............

@property (strong, nonatomic) IBOutlet UIButton *btnGlobalSync;
- (IBAction)actionOnGlobalSync:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;
//Nilind 08 Feb
@property (weak, nonatomic) IBOutlet UITextField *txtAreaSqFt;
@property (weak, nonatomic) IBOutlet UITextField *txtBedroom;
@property (weak, nonatomic) IBOutlet UITextField *txtBathroom;
@property (weak, nonatomic) IBOutlet UITextField *txtLotSizeSqFt;

@property (weak, nonatomic) IBOutlet UITextField *txtYearBuil;
- (IBAction)actionOnGetPropertyInfo:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnGetPropertyInfo;
@property (weak, nonatomic) IBOutlet UITextField *txtNoOfStory;
@property (weak, nonatomic) IBOutlet UITextField *txtShrubArea;
@property (weak, nonatomic) IBOutlet UITextField *txtTurfArea;

//End

//Saavan Changes For Email Id
@property (strong, nonatomic) IBOutlet UIButton *btnPrimaryEmail;
- (IBAction)action_PrimaryEmail:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSecondaryEmail;
- (IBAction)action_SecondaryEmail:(id)sender;
//End Of Changes by Saavan
- (IBAction)action_AddGraph:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAddGraph;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewGraph;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_BtnSavePriority;

//Nilind 03 August
@property (weak, nonatomic) IBOutlet UITextField *txtMapCodeServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtGateCodeServiceNotes;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAddressNotesServiceAddress;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDirectionServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtMapCodeBillingAddress;

//Nilind 13 Sept // //
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewAddImage;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAccountDesc;
@property (weak, nonatomic) IBOutlet UITextField *txtLinearSqFt;
@property (weak, nonatomic) IBOutlet UITextField *txtCellNo;
//Service POC //
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCMiddleName;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCPrimaryEmail;

@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCSecondaryEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCCellNo;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCPrimaryPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePOCSecondaryPhone;

//Billing POC
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCMiddleName;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCPrimaryEmail;

@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCSecondaryEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCCellNo;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCPrimaryPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingPOCSecondaryPhone;
- (IBAction)actionOnNotesHistory:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;

//Service POC
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constServicePOCDetail_H;
@property (weak, nonatomic) IBOutlet UIView *viewServicePOCDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnShowServicePOCDetail;
- (IBAction)actionOnShowServicePOCDetail:(id)sender;
- (IBAction)actionOnEditServicePOCDetail:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEditServicePOCDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnSendMailServicePOC;
- (IBAction)actionOnSendMailServicePOC:(id)sender;


//Billing POC
@property (weak, nonatomic) IBOutlet UIView *viewBillingPOCDetail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constantBillingPOCDetail_H;
@property (weak, nonatomic) IBOutlet UIButton *btnEditBillingPOCDetail;
- (IBAction)actionOnEditBillingPOCDetail:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnShowBillingPOCDetail;
- (IBAction)actionOnShowBillingPOCDetail:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIButton *btnSendMailBillingPOC;
- (IBAction)actionOnSendMailBillingPOC:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *viewCustomerInfo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewCustomerInfo;
@property (weak, nonatomic) IBOutlet UIView *viewNameCustomerInfo;
@property (weak, nonatomic) IBOutlet UILabel *labelNameCustomerInfo;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFirstNameCustomerInfo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMiddleNameCustomerInfo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLastNameCustomerInfo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpcBtwnViewNameAndPrimaryEmail_CustomerInfoView;
@property (weak, nonatomic) IBOutlet UIButton *buttonCellNoCustomerInfoView;
@property (weak, nonatomic) IBOutlet UIButton *buttonPrimaryPhoneCustomerInfoView;
@property (weak, nonatomic) IBOutlet UIButton *buttonSecondaryPhoneCustomerInfoView;

@property (weak, nonatomic) IBOutlet UIButton *buttonShowHide_CustomerInfo;
@property (weak, nonatomic) IBOutlet UIButton *buttonEdit_CustomerInfo;




// service
@property (weak, nonatomic) IBOutlet UIView *viewServiceAddress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewServiceAddress;

@property (weak, nonatomic) IBOutlet UIButton *buttonShowHide_ServiceAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonEdit_ServiceAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLblAddressLine1;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTxtViewAddressLine1;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLblAddressLine2;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTxtViewAddressLine2;

@property (weak, nonatomic) IBOutlet UILabel *labelAddress_ServiceAddress;

@property (weak, nonatomic) IBOutlet UITextView *txtViewAddress_ServiceAddress;

// billing address

@property (weak, nonatomic) IBOutlet UIButton *buttonShowHide_BillingAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonEditBillingAddress;
@property (weak, nonatomic) IBOutlet UIView *viewBillingAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewBillingAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLblAddress1_BillingAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTxtViewAddress1_BillingAddress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLblAddress2_BillingAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTxtViewAddress2_BillingAddress;
@property (weak, nonatomic) IBOutlet UIView *viewAddress_BillingAddress;

@property (weak, nonatomic) IBOutlet UITextView *txtViewAddress_BillingAddress;
//////

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMainContainerView;


@property (weak, nonatomic) IBOutlet UILabel *lblState_ServiceAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightState_ServiceAddress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightBtnState_ServiceAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblCity_ServiceAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblZipCode_ServiceAddress;


@property (weak, nonatomic) IBOutlet UIView *viewAddressContainer_ServiceAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewAddressContainer_ServiceAddress;

@property (weak, nonatomic) IBOutlet UIView *viewServiceContactName;

@property (weak, nonatomic) IBOutlet UILabel *labelServiceContactNameHeader;

@property (weak, nonatomic) IBOutlet UILabel *labelServiceContactName;

@property (weak, nonatomic) IBOutlet UILabel *lblServiceContactMiddleName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightServiceContactMiddleName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTxtServiceContactMiddleName;

@property (weak, nonatomic) IBOutlet UILabel *labelServiceContactLastName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLblServiceContactLastName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTxtServiceContactLastName;

@property (weak, nonatomic) IBOutlet UILabel *labelServiceContactFirstName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightServiceContactFirstName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTxtServiceContactFirstName;

@property (weak, nonatomic) IBOutlet UIView *viewAddressContainerView_BillingAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewAddressConainer_BillingAddress;

@property (weak, nonatomic) IBOutlet UILabel *labelName_BillingAddress;

@property (weak, nonatomic) IBOutlet UILabel *labelBillingContactMiddleName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightBillingContactMiddleName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTxtBillingContactMiddleName;

@property (weak, nonatomic) IBOutlet UILabel *labelBillingContactLastName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightBillingContactLastName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTxtBillingContactLastName;

@property (weak, nonatomic) IBOutlet UILabel *labelBillingContactNameHeader;

@property (weak, nonatomic) IBOutlet UILabel *labelAddressHeader_BillingAddress;
@property (weak, nonatomic) IBOutlet UIView *viewBillingContactName_BillingAddress;


//new outlet
@property (weak, nonatomic) IBOutlet UIButton *btnCellNo;
@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondaryPhone;
- (IBAction)actionOnCellNo:(id)sender;

- (IBAction)actionCallPrimaryPhone:(id)sender;
- (IBAction)actionCallSecondaryPhone:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactPrimaryEmail;
- (IBAction)actionServiceContactPrimaryEmail:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactSecondaryEmail;
- (IBAction)actionOnServiceContactSecondaryEmail:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactCell;
- (IBAction)actionOnServiceContactCell:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactPrimaryPhone;
- (IBAction)actionOnServiceContactPrimaryPhone:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnServiceContactSecondaryPhone;
- (IBAction)actionOnServiceContactSecondaryPhone:(id)sender;

- (IBAction)actionOnBillingAddress:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactPrimaryEmail;
- (IBAction)actionBillingContactPrimaryEmail:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactSecondaryEmail;
- (IBAction)actionOnBillingContactSecondaryEmail:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactCell;
- (IBAction)actionOnBillingContactCell:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactPrimaryPhone;
- (IBAction)actionOnBillingContactPrimaryPhone:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBillingContactSecondaryPhone;
- (IBAction)actionOnBillingContactSecondaryPhone:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *labelStaticAddressLine1_ServiceAddress;

@property (weak, nonatomic) IBOutlet UIView *viewCommonAddress_ServiceAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelAddresLine1Header_BillingAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelStaticBillingContactFirstName;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewServicePOCDetails;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewContainerMiddleLastName_ServiceAddress;
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *heightViewContainerMiddleLastName_BillingAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryEmailNew;
- (IBAction)actionOnPrimaryEmailNew:(id)sender;

- (IBAction)actionOnNewServiceAddress:(id)sender;
- (IBAction)actionOnNewBillingAddress:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *txtViewProblemescription;
@property (weak, nonatomic) IBOutlet UIButton *btnLeadTypeInside;
- (IBAction)actiononLeadTypeInside:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnLeadTypeField;
- (IBAction)actiononLeadTypeField:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceCounty;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceSchoolDistrict;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingCounty;
@property (weak, nonatomic) IBOutlet UITextField *txtBillingSchoolDistrict;
- (IBAction)actionOnChemicalSensitivityList:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTaxCode;
- (IBAction)actionOnTaxCode:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtDriveTime;
@property (weak, nonatomic) IBOutlet UITextField *txtEarliestStartTime;
@property (weak, nonatomic) IBOutlet UITextField *txtLatestStartTime;
@property (weak, nonatomic) IBOutlet UIButton *btnPropertyType;
- (IBAction)actionOnProprtyType:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewPropertyType_H;

@property (weak, nonatomic) IBOutlet UIButton *btnOppTypeResidential;
@property (weak, nonatomic) IBOutlet UIButton *btnOppTypeCommercial;
- (IBAction)actionOnResidential:(id)sender;
- (IBAction)actionOnCommercial:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnReopen;
- (IBAction)actionOnReopen:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnServiceAddressSubType;
- (IBAction)actionOnServiceAddressSubType:(id)sender;

@end

