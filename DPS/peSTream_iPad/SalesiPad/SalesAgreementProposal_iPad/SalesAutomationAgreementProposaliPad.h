//
//  SalesAutomationAgreementProposal.h
//  DPS
//
//  Created by Rakesh Jain on 19/07/16.
//  Copyright © 2016 Saavan. All rights reserved. changes
//change
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface SalesAutomationAgreementProposaliPad : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate,UITextViewDelegate,UITextFieldDelegate,AVAudioPlayerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate>
{
    AVAudioPlayer *LandingSong;
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,*entityLeadAppliedDiscounts,*entityElectronicAuthorizedForm,*entityRenewalSerivceDetail;
    
    
    AppDelegate *appDelegateAgreementList;
    NSManagedObjectContext *contextAgreementList;
    NSFetchRequest *requestNewAgreementList;
    NSSortDescriptor *sortDescriptorAgreementList;
    NSArray *sortDescriptorsAgreementList;
    NSManagedObject *matchesAgreementList;
    NSArray *arrAllObjAgreementList;
    NSEntityDescription
    *entityLeadAgreementChecklistSetups;
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo,*fetchedResultsControllerSalesInfoAgreementList;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewLeads;
@property (strong, nonatomic) IBOutlet UIView *viewTop;
@property(strong,nonatomic)NSString *strFromSummary;

///***** PERSONAL INFO VIEW ******//
@property (weak, nonatomic) IBOutlet UIView *viewPersonalInfo;
- (IBAction)actionOnBack:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblAccountName;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerName;
@property (weak, nonatomic) IBOutlet UILabel *lblDateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceAddressValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingAddressValue;
@property (weak, nonatomic) IBOutlet UILabel *lblPrimaryPhoneValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondarPhoneValue;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailIdValue;
@property (weak, nonatomic) IBOutlet UILabel *lblInspectorValue;
///***** MAINTAINENANCE SERVICE VIEW ******//
@property (weak, nonatomic) IBOutlet UIView *viewMaintainanceService;
@property (weak, nonatomic) IBOutlet UITableView *tblMaintenanceService;
@property (weak, nonatomic) IBOutlet UILabel *lblInitialPricevValueMaintenanceService;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenancePriceMaintenanceService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntTblMaintenance_T;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntTblMaintenance_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntTblMaintenance_B;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSubtotalMainten_B;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrViewSubTotalMaintain_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnsrtViewMaintainService_H;

///***** NONSTANDARD SERVICE VIEW ******//

@property (weak, nonatomic) IBOutlet UIView *viewNonStandardService;
@property (weak, nonatomic) IBOutlet UITableView *tblNonStandardService;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalAmountValueNonStandardService;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntNonStandardTbl_H;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntNonStandardTbl_T;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntViewNonStandard_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrtntViewNonSubtotal;


///***** ADDITIONAL NOTES VIEW ******//
@property (weak, nonatomic) IBOutlet UIView *viewAdditionalNotes;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAdditionalNotes;

///***** PRICE INFORMATION VIEW ******//
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalAmountValuePriceInformation;
@property (weak, nonatomic) IBOutlet UILabel *lblCouponDiscountValuePriceInformation;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPriceValuePriceInformation;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxAmountValuePriceInformation;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingAmountPriceInformation;
@property (weak, nonatomic) IBOutlet UITextField *txtPaidAmountPriceInforamtion;

- (IBAction)actionOnCrash:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnCash;
@property (weak, nonatomic) IBOutlet UILabel *Cash;

- (IBAction)actionOnCheck:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UILabel *Check;

- (IBAction)actionOnCreditCard:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnCredit;

- (IBAction)actionOnAutoChangeCustomer:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnAutoChangeCustomer;

- (IBAction)actionOnCollectAtTimeOfScheduling:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtChequeValue;
@property (weak, nonatomic) IBOutlet UITextField *txtLicenseValue;
@property (weak, nonatomic) IBOutlet UITextField *txtExpirationDate;

@property (weak, nonatomic) IBOutlet UIButton *btnCollectTimeOfScheduling;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntViewPriceInfo_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntViewCreditCard_H;

@property (weak, nonatomic) IBOutlet UIView *viewCreditCard;

@property (weak, nonatomic) IBOutlet UITableView *tblMaintenance;

#pragma mark- VIEW TERMS & CONSITIONS 1 Sept
@property (weak, nonatomic) IBOutlet UIView *viewTermsConditions;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntViewTermsCondition_H;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckBoxCustomerNotPresent;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewInspectorSign;
- (IBAction)actionOnCustomerNotPresent:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *actionOnCustomerNotPresent;
@property (weak, nonatomic) IBOutlet UIButton *actionOnCustomerSignature;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCustomerSignature;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntCustomerSignatureView_H;
- (IBAction)actionOnSaveContinue:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewCustomerSignature;
@property (weak, nonatomic) IBOutlet UIView *viewInspectorSignature;
// New Change

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constPersonalInfoView_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constAdditionNots_H;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewAgreement;
- (IBAction)actionOnServiceAgrrment:(id)sender;
- (IBAction)actionOnServiceProposal:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewPriceInformation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constScrollview_H;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCash;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCheck;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCreditCard;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAutoChangeCustomer;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCollectAtTimeOfScheduling;

// SIGNATURE CODE

- (IBAction)actionOnInspectorSign:(id)sender;
- (IBAction)actionOnCustomerSignature:(id)sender;
- (IBAction)actionOnExpirayDate:(id)sender;
- (IBAction)actionOnTermsConditions:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewTerms;

@property (strong, nonatomic) IBOutlet UIView *viewTerms;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollTerms;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)scrollBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTerms;
- (IBAction)btnTerms:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnTerms;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constScrollTop_H;
//InspectionDetail
@property (strong, nonatomic) IBOutlet UIView *viewInspection;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constViewInspection_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ScrollAgreement_H;
@property (weak, nonatomic) IBOutlet UIButton *btnServiceAgreement;
@property (weak, nonatomic) IBOutlet UIButton *btnServiceProposal;
@property (weak, nonatomic) IBOutlet UILabel *lblNameAcount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_AddNotes_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_PriceTerms_H;
@property (weak,nonatomic)NSString *strSummarySendPropsal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ServiceAgreement_W;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Proposal_Leading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ServiceProposal_W;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceProposal;
@property (weak, nonatomic) IBOutlet UILabel *lblInspection;
@property (weak, nonatomic) IBOutlet UILabel *lblInspectionLine;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveContinue;
- (IBAction)actionOnDynamicFormButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnInspector;
@property (weak, nonatomic) IBOutlet UIButton *btnCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblTxtPaidAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnInitialSetup;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)actionOnInitialSetup:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRecordAudio;
- (IBAction)action_RecordAudio:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnPlayAudio;
- (IBAction)action_PlayAudio:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnIAgree;

@property (strong, nonatomic) IBOutlet UIButton *btnGlobalSync;
- (IBAction)actionOnIAgree:(id)sender;
- (IBAction)actionOnGlobalSync:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;
- (IBAction)actionOnCheckFrontImage:(id)sender;

- (IBAction)actionOnCheckBackImage:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnExpirationDate;


//View For Final Save

@property (weak, nonatomic) IBOutlet UIView *viewForFinalSave;
- (IBAction)actionOnAudioBottom:(id)sender;

//View For After Image
@property (strong, nonatomic) IBOutlet UIView *viewForAfterImage;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewAfterImage;
- (IBAction)actionAddAfterImages:(id)sender;
- (IBAction)actionOnCancelAfterImage:(id)sender;
- (IBAction)actionOnAfterImgView:(id)sender;

//View For Graph Image
@property (strong, nonatomic) IBOutlet UIView *viewForGraphImage;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewGraphImage;
- (IBAction)actionOnAddGraphImage:(id)sender;

- (IBAction)actionOnCancelBeforeImage:(id)sender;
- (IBAction)actionOnGraphImageFooter:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPriceNonStandard;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPriceStandard;
#pragma mark- New Bundle Change
@property (weak, nonatomic) IBOutlet UITableView *tblBundle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTblBundle_H;
- (IBAction)actionOnNotesHistory:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;

//Agreement CheckBox
@property (weak, nonatomic) IBOutlet UITableView *tblAgreement;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableAgreement_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewAgreement_H;
@property (weak, nonatomic) IBOutlet UIView *viewAgreementCheck;

@property (weak, nonatomic) IBOutlet UIButton *btnMarkAsLost;
- (IBAction)actionOnBtnMarkAsLost:(id)sender;

//View Preferred Months
@property (weak, nonatomic) IBOutlet UIView *viewPreferredMonth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewPreferredMonth_H;


@property (weak, nonatomic) IBOutlet UIButton *btnJan;
@property (weak, nonatomic) IBOutlet UIButton *btnFeb;
@property (weak, nonatomic) IBOutlet UIButton *btnMar;
@property (weak, nonatomic) IBOutlet UIButton *btnApr;
@property (weak, nonatomic) IBOutlet UIButton *btnMay;
@property (weak, nonatomic) IBOutlet UIButton *btnJun;
@property (weak, nonatomic) IBOutlet UIButton *btnJul;
@property (weak, nonatomic) IBOutlet UIButton *btnAug;
@property (weak, nonatomic) IBOutlet UIButton *btnSept;
@property (weak, nonatomic) IBOutlet UIButton *btnOct;
@property (weak, nonatomic) IBOutlet UIButton *btnNov;
@property (weak, nonatomic) IBOutlet UIButton *btnDec;

- (IBAction)actionOnJanuary:(UIButton *)sender;
- (IBAction)actionOnFebruary:(id)sender;
- (IBAction)actionOnMarch:(id)sender;
- (IBAction)actionOnApril:(id)sender;
- (IBAction)actionOnMay:(id)sender;
- (IBAction)actionOnJune:(id)sender;
- (IBAction)actionOnJuly:(id)sender;
- (IBAction)actionOnAugust:(id)sender;
- (IBAction)actionOnSeptember:(id)sender;
- (IBAction)actionOnOctober:(id)sender;
- (IBAction)actionOnNovember:(id)sender;
- (IBAction)actionOnDecember:(id)sender;
- (IBAction)actionBackToSchedule:(id)sender;

//View Top New Outlets and Actions
@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryPhone;
- (IBAction)actionOnPrimaryPhone:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondaryPhone;
- (IBAction)actionOnSecondaryPhone:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEmailId;
- (IBAction)actionOnEmailId:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPrintAgreement;
- (IBAction)actionOnPrintAgreement:(id)sender;
- (IBAction)actionOnServiceAddress:(id)sender;
- (IBAction)actionOnBillingAddress:(id)sender;

//VIEW CREDIT COUPON
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewCouponCredit_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblCoupon_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblCredit_H;

@property (weak, nonatomic) IBOutlet UIView *viewForCouponCredit;
@property (weak, nonatomic) IBOutlet UITextField *txtEnterCouponNo;
@property (weak, nonatomic) IBOutlet UIButton *btnApplyCoupon;
- (IBAction)actionOnApplyCoupon:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblCoupon;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalCoupon;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCredit;
- (IBAction)actionOnSelectCredit:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCredit;
- (IBAction)actionOnAddCredit:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblCredit;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalCredit;



//View New Initial and Maintenace Price

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewNewInitialMaintPrice_H;
@property (weak, nonatomic) IBOutlet UIView *viewNewPriceInfo;

@property (weak, nonatomic) IBOutlet UIView *viewNewInitialMaintPrice;
@property (weak, nonatomic) IBOutlet UITableView *tblNewInitialPriceCouponDiscount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblNewInitialPriceCouponDiscount_H;

@property (weak, nonatomic) IBOutlet UITableView *tblNewInitialPriceCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblNewInitialPriceCredit_H;


@property (weak, nonatomic) IBOutlet UITableView *tblNewMaintPriceCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblNewMaintPriceCredit_H;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceSubtotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceCouponDiscount;

@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceCredit;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceSubtotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceCredit;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceOtherDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceTaxAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceBillingAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceTaxAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceTotalDueAmount;

@property (weak, nonatomic) IBOutlet UIButton *buttonElectronicAuthorizedForm;
- (IBAction)actionOnElectroinicAuthorizedForm:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForAppliedCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewAppliedCredit_H;
@property (weak, nonatomic) IBOutlet UITableView *tblAppliedCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblAppliedCredit_H;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_lblCouponDisc_InitialPrice_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_lblCredit_InitialPrice_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_lblCredit_MaintPrice_H;
- (IBAction)actionOnChemicalSensitivityList:(id)sender;
//Standard
@property (weak, nonatomic) IBOutlet UITableView *tblBillingAmount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Tbl_BillingAmount_H;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingFreqAmount;
//NonStandard
@property (weak, nonatomic) IBOutlet UILabel *lblBillingFrqAmountNonStan;
@property (weak, nonatomic) IBOutlet UITableView *tblBillingAmountNonStan;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Tbl_BillingAmountNonStan_H;

@property (weak, nonatomic) IBOutlet UITableView *tblMaintBillingPrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblMaintBilling_H;


//View Proposal Notes
@property (weak, nonatomic) IBOutlet UIView *viewProposalNotes;
@property (weak, nonatomic) IBOutlet UITextView *txtViewProposalNotes;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cosnt_VewiProposalNotes_H;


@property (weak, nonatomic) IBOutlet UIImageView *imgViewInvoice;
@property (weak, nonatomic) IBOutlet UIButton *btnInvoice;
- (IBAction)actionOnInvoice:(id)sender;


//Nilind 12 March
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLblBundle_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLblBundleLine_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLblMaintService_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constLblLineMaintService_H;

@property (weak, nonatomic) IBOutlet UIButton *btnCellNo;
- (IBAction)actionOnCellNo:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblCellNo;
@property (weak, nonatomic) IBOutlet UIView *viewElectronicAuthForm;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailable;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalMaintPriceNonStan;

@property (weak, nonatomic) IBOutlet UIView *viewInternalNotes;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewInternalNotes_H;
@property (weak, nonatomic) IBOutlet UITextView *txtViewInternalNotes;
- (IBAction)actionOnSendText:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSendText;
@property (weak, nonatomic) IBOutlet UIButton *btnAddImageFooter;

@property (weak, nonatomic) IBOutlet UIButton *btnAllowCustomerToMakeSelection;

- (IBAction)actionOnAllowCustomerToMakeSelection:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_BtnAllowCustomer_H;
@end

