//
//  InitialSetUp.h
//  DPS
//
//  Created by Rakesh Jain on 24/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"

@interface InitialSetUpiPad : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail;
    
}
- (IBAction)action_ForwardRouteNames:(id)sender;
- (IBAction)action_BackRouteNames:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo;

- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxInitialSetup;

- (IBAction)actionOnInitialSetUp:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxSpecificTime;

- (IBAction)actionOnSpecificTime:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxRangeTime;

- (IBAction)actionOnRangeTime:(id)sender;


//View InitalSerivce
@property (weak, nonatomic) IBOutlet UIView *viewInitialService;

@property (weak, nonatomic) IBOutlet UIView *viewOneInitailService;

@property (weak, nonatomic) IBOutlet UIView *viewTwoPreferableTime;

@property (weak, nonatomic) IBOutlet UIView *viewThreeLockRout;

- (IBAction)actionOnInitialService:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtServicePrice;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceDuration;
- (IBAction)actionOnServiceDate:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceDate;
- (IBAction)actionOnServiceRoute:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnServiceRoute;
@property (weak, nonatomic) IBOutlet UIButton *btnPreferableTime;
- (IBAction)actionOnPreferableTime:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnNewPreferableTime;
- (IBAction)actionOnNewPreferableTime:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxLockRout;
- (IBAction)actionOnLockRout:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxLockTime;
- (IBAction)actionOnLockTime:(id)sender;
- (IBAction)actionOnViewAppointment:(id)sender;
- (IBAction)actionOnSaveContinue:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *actionOnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnInitialService;
#pragma mark- Appoint View
@property (weak, nonatomic) IBOutlet UITableView *tblAppointment;
@property (weak, nonatomic) IBOutlet UILabel *lblOne;
- (IBAction)actionOnBackAppointment:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewAppointment;
- (IBAction)actionOnSkip:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceTime;
- (IBAction)actionOnServiceTime:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnServiceTime;

@property (weak, nonatomic) IBOutlet UITextField *txtServiceProdValue;
@property (weak, nonatomic) IBOutlet UITextView *txtViewServiceInstruction;
@property (weak, nonatomic) IBOutlet UITextView *txtViewSetupInstruction;

- (IBAction)action_SaveGenrateWorkOrder:(id)sender;
//@property(weak,nonatomic)NSString *strFromAppointment,*strForFollowUp;
@property(weak,nonatomic)NSString *strForFollowUp;
@property(strong,nonatomic)NSString *strFromAppointment;

@property (weak, nonatomic) IBOutlet UIButton *btnSaveContinue;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveGenerateWorkOrder;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_PreferableView_H;
@property (weak, nonatomic) IBOutlet UILabel *lblPreferable;
@property (weak, nonatomic) IBOutlet UILabel *lblNonPreferable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewMain_H;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;
@end
