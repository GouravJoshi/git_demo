//
//  SendMailViewController.m
//  DPS
//
//  Created by Rakesh Jain on 02/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
// change

#import "SendMailViewControlleriPad.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "LeadDetail.h"
#import "PaymentInfo.h"
#import "LeadPreference.h"
#import "SoldServiceNonStandardDetail.h"
#import "SoldServiceStandardDetail.h"
#import "ImageDetail.h"
#import "EmailDetail.h"
#import "DocumentsDetail.h"
#import "DocumentsDetail.h"
#import "BeginAuditView.h"
#import "SalesAutomationAgreementProposal.h"
#import "InitialSetUp.h"
#import "AppointmentView.h"
#import "CurrentService.h"
#import "ServiceFollowUpTableViewCell.h"
#import "ProposalFollowUpTableViewCell.h"
#import "AllImportsViewController.h"
#import "DPS-Swift.h"

@interface SendMailViewControlleriPad ()
{
    NSString *strLeadId,*strLeadName,*strSalesAutoMainUrl,*strAudioNameGlobal;
    NSMutableArray *arrEmail,*arrFinalJson,*arrUpdateLeadId,*arrOfAllImagesToSendToServer,*arrOfAllSignatureImagesToSendToServer;
    UITableViewCell *cell;
    NSMutableArray *arr,*arr2;
    BOOL chk,temp,flag,isBackkk;
    int val;
    NSMutableArray *arrFinalSend;
    
    NSMutableArray  *arrFinalLeadDetail,
    *arrFinalPaymenyInfo,
    *arrFinalImageDetail,
    *arrFinalEmailDetail,
    *arrFinalDocumentsDetail,
    *arrFinalSoldStandard,
    *arrFinalLeadPreference,
    *arrFinalSoldNonStandard
    ,*arrPrimarySecondaryMail;
    
    
    NSMutableDictionary *dictFinal;
    //......
    Global *global;
    
    NSMutableArray *arrImageName,*arrImageSend,*arrResponseInspection;
    NSString *strModifyDateToSendToServerAll;
    int indexToSendImage,indexToSendImageSign;
    NSMutableArray *arrSaveEmail,*arrDuplicateMail,*arrSoldCount;
    
    NSMutableArray *arrMailChecked;
    
    NSMutableArray *arrCheckEmail,*arrUncheckEmail;
    BOOL chkClickStatus,lock;
    NSString *strCompanyKey,*strEmployeeNo,*strUserName;
    BOOL chkStatus;
    NSMutableArray *arrSendEmailCount;
    BOOL isEditedInSalesAuto;
    //Nilind13 Feb
    NSMutableArray *arrSoldService,*arrUnSoldService,*arrSolDeptName,*arrUnSolDeptName;
    NSDictionary *dictSoldService,*dictUnSoldService;
    
    
    //End
    NSMutableDictionary *dictMutableServiceFollow;
    NSDictionary *dictDeptNameByKey;
    NSArray *arrServiceFollowDept,*arrServiceFollowName,*arrProposalFollowDept,*arrProposalSerivceName;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    NSString *strDate;
    long tagValue;
    NSString *strTable;
    
    //Nilind 16 Feb
    NSMutableArray *arrForServiceFollowName,*arrServiceDepartmentName,*arrServiceFollowDate;
    
    NSMutableArray *arrForProposalService,*arrForProposalNotes,*arrForProposalFollowUpDate;
    //End
    
    BOOL chkForServiceFollow,chkForProposalFollow;
    NSDictionary *dictServiceName;
    NSMutableDictionary *dictForFollowUp;
    NSMutableArray *arrServiceDeptNameFinalSend,*arrServiceNotesFinalSend,*arrServiceDateFinalSend;
    NSMutableArray *arrProposalServiceNameFinalSend,*arrProposalNotesFinalSend,*arrProposalDateFinalSend;
    NSDictionary *dictCompanyDetail;
    BOOL isProposalFoolowUp,isServiceFollowUp;
    int serviceFollowDays,proposalFollowDays;
    
    //Nilind  08 Septs
    NSMutableArray *arrServiceIdForDocuments;
    NSDictionary *dictCategoryNameForDocumets,*dictServiceNameFromIdForDocumets,*dictDepartmentFromServiceId;
    NSMutableArray *arrFinalDocumets;
    NSMutableArray *arrLocalDocument;
    NSMutableArray *arrCheckImage;
    NSMutableArray *arrDocId;
    NSString *strAccountNo,*strScheduleInspectionDate;
    
    //Resend email change
    
    NSString *strCompanyId,*strLeadNumber,*strProposal,*isResendEmailCheck,*strGlobalLeadStatus,*strServiceUrlMainSales,*strElectronicSign,*strDefaultEmployeeEmail;
    
    
    NSMutableArray *arrTempDocumentForResend;
    NSMutableArray *arrTempResend;
    NSMutableArray *arrDeptNameNonStanForDocument;

    NSMutableArray *arrDocumentSelected,*arrAllTargetImages;
    NSMutableArray *arrUploadOtherDocument, *arrUploadOtherDocumentId;
    NSMutableArray *arrUploadGraphXML;
    
    NSString *strEmpName,*strEmpEmail,*strEmpPhone , *strCustomerName, *strCustomerCompanyName , *strCompanyName, *strFirstName, *strMiddleName, *strLastName ,*strProposedServices ,*strSoldServices, *strServices, *strScheduleDate;

}
@end

@implementation SendMailViewControlleriPad

- (void)viewDidLoad
{
    strEmpName = @"",strEmpEmail = @"", strEmpPhone = @"", strCustomerName = @"", strCustomerCompanyName = @"", strCompanyName = @"", strFirstName = @"", strMiddleName = @"", strLastName = @"", strProposedServices = @"", strSoldServices = @"", strServices = @"", strScheduleDate = @"";
    
    isBackkk = false;
    
    arrTempDocumentForResend=[[NSMutableArray alloc]init];
    arrDocumentSelected=[[NSMutableArray alloc]init];

    [pickerDate setMinimumDate: [NSDate date]];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"followUpSaved"];
    [defs synchronize];
    //Nilind 15 Feb
    isServiceFollowUp=NO;
    serviceFollowDays=NO;
    chkForServiceFollow=NO;chkForProposalFollow=NO;
    chkForServiceFollow=YES;chkForProposalFollow=YES;
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [viewBackGround addGestureRecognizer:singleTapGestureRecognizer];
    
    //End
    
    isEditedInSalesAuto=NO;
    //Nilind 16 Nov
    NSUserDefaults *defsLogin=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLogin=[defsLogin valueForKey:@"LoginDetails"];
    
    strCompanyKey =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.CompanyKey"]];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLogin valueForKey:@"EmployeeNumber"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.Username"]];
    strCompanyId=[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.SalesAutoCompanyId"]];
    strServiceUrlMainSales=[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    strDefaultEmployeeEmail=[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"EmployeeEmail"]];
    
    strCompanyName=[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.Name"]];
    strEmpName=[NSString stringWithFormat:@"%@",[dictLogin valueForKey:@"EmployeeName"]];
    strEmpEmail=[NSString stringWithFormat:@"%@",[dictLogin valueForKey:@"EmployeeEmail"]];
    strEmpPhone=[NSString stringWithFormat:@"%@",[dictLogin valueForKey:@"EmployeePrimaryPhone"]];

    //Nilind 15 Feb
    dictMutableServiceFollow=[[NSMutableDictionary alloc]init];
    dictForFollowUp=[[NSMutableDictionary alloc]init];
    
    //End
    _viewSendMail.frame=CGRectMake(4  ,60,[UIScreen mainScreen].bounds.size.width-8,_viewSendMail.frame.size.height);
    
    [__scrollViewSendMail addSubview:_viewSendMail];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    strLeadName=[defsLead valueForKey:@"defsLeadName"];
    NSString *strResend=[self fetchLeadDetailForResend];
    
    if ([_strFromWdo isEqualToString:@"yes"])
    {
        
    }
    else
    {
        if ([strResend isEqualToString:@"true"])
        {
            [_btnSendMail setTitle:@"Resend Email" forState:UIControlStateNormal];
        }
    }
    
    
    //NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
    //chkStatus=[dfsStatus boolForKey:@"status"];
    if (chkStatus==YES)
    {
        //_btnSurveyy.enabled=NO;
        _btnInitialSetup.enabled=NO;
        _btnInitialSetupDummy.enabled=NO;
        
    }
    //.....................
    lock=YES;
    
    
    _tblEmailData.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblEmailData.layer.borderWidth=1.0;
    
    _tblDocuments.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblDocuments.layer.borderWidth=1.0;
    
    _tblServiceFollowUp.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblServiceFollowUp.layer.borderWidth=1.0;
    
    _tblProposalFollowUp.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblProposalFollowUp.layer.borderWidth=1.0;
    
    
    NSUserDefaults *defsIsService=[NSUserDefaults standardUserDefaults];
    [defsIsService setBool:NO forKey:@"isServiceSurvey"];
    [defsIsService setBool:NO forKey:@"isMechanicalSurvey"];
    [defsIsService setBool:NO forKey:@"isNewSalesSurvey"];
    
    [defsIsService synchronize];
    
    indexToSendImage=0;
    indexToSendImageSign=0;
    
    [super viewDidLoad];
    
    _viewSendMail.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewSendMail.layer.borderWidth=1.0;
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    arrImageName=[[NSMutableArray alloc]init];
    //...........
    global = [[Global alloc] init];
    //...........
    
    chk=NO;temp=YES;flag=YES;
    val=0;
    
    // Mutable Array  Declaration
    
    arrEmail=[[NSMutableArray alloc]init];
    arr = [NSMutableArray array];
    arrFinalSend=[[NSMutableArray alloc]init];
    arrFinalJson=[[NSMutableArray alloc]init];
    
    dictFinal=[[NSMutableDictionary alloc]init];
    arrUpdateLeadId=[[NSMutableArray alloc]init];
    arrImageSend=[[NSMutableArray alloc]init];
    arrSaveEmail=[[NSMutableArray alloc]init];
    arrDuplicateMail=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    arrMailChecked=[[NSMutableArray alloc]init];
    arrPrimarySecondaryMail=[[NSMutableArray alloc]init];
    arrSoldCount=[[NSMutableArray alloc]init];
    arrSendEmailCount=[[NSMutableArray alloc]init];
    arrCheckImage=[[NSMutableArray alloc]init];
    //..........................................................
    
    
    //_lblFromValue.text=@"explore@quacito.com";//strLeadName;
    
    NSString *strSalesReportType=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoReportType"]];
    
    if ([strSalesReportType isEqualToString:@"CompanyEmail"]) {
        
        _lblFromValue.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoReportEmail"]];
        
    }
    else
    {
        
        _lblFromValue.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeEmail"]];
        
    }
    if(_lblFromValue.text.length==0 || [_lblFromValue.text isEqualToString:@""])
    {
        _lblFromValue.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Email"]];
        
    }
    
    
    
    
    
    // Email Fetch
#pragma mark- DO NOT DELETE
    /*
     [self fetchPrimarySeconddaryMail];
     NSArray * newArray = [[NSOrderedSet orderedSetWithArray:arrPrimarySecondaryMail] array];
     
     for(int i=0;i<newArray.count;i++)
     {
     if ([NSString stringWithFormat:@"%@",[newArray objectAtIndex:i]].length>3)
     {
     NSString *strEmailCheck=[NSString stringWithFormat:@"%@",[newArray objectAtIndex:i]];
     if([self emailExist:strEmailCheck]==YES)
     {
     
     }
     else
     {
     [self savePrimaryEmailToCoreData:[newArray objectAtIndex:i]];
     }
     }
     }
     */
    
    // Changes For Default Emails
    
    [self deleteDefaultEmailFromDB];
    [self fetchDefaultEmailFromDb];
    
    //Nilind 24 Oct
    [self salesEmailFetch];
    
    [_tblEmailData reloadData];
    
    [self finalMails];
    
    NSString *strIsGGQIntegration=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsGGQIntegration"]];
    NSString *strSurveyId=[defsLead valueForKey:@"SurveyID"];
    
    
    _btnInitialSetupDummy.hidden=YES;
    if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveyId.length>0))
    {
        [_btnSurveyy setHidden:NO];
        _btnInitialSetupDummy.hidden=YES;
        _btnInitialSetup.hidden=NO;
        _btnInitialSetup.hidden=YES;
        
        if ([_strForSendProposal isEqualToString:@"forSendProposal"])
        {
            [_btnSurveyy setHidden:YES];
        }
        
    }
    else
    {
        [_btnSurveyy setHidden:YES];
        
        _btnInitialSetup.hidden=YES;
        
        _btnInitialSetupDummy.hidden=NO;
        _btnInitialSetupDummy.hidden=YES;
    }
    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        _const_View_T.constant=0;
    }
    //Nilind 17 Oct
    
    arrCheckEmail=[[NSMutableArray alloc]init];
    arrUncheckEmail=[[NSMutableArray alloc]init];
    
    //Nilind 10 Feb
#pragma mark- TEMP COMMENT
    
    //End
    /*if([_strForSendProposal isEqualToString:@"forSendProposal"])
     {
     arrFinalDocumets=[[NSMutableArray alloc]init];
     arrLocalDocument=[[NSMutableArray alloc]init];
     arrDocId=[[NSMutableArray alloc]init];
     arrFinalDocumets=[[NSMutableArray alloc]init];
     arrLocalDocument=[[NSMutableArray alloc]init];
     arrTempResend=[[NSMutableArray alloc]init];
     
     }*/
    //else
    //{
    //[self fetchDocumentsToBeSend];
    //}
    
    if ([_strForSendProposal isEqualToString:@"forSendProposal"])
    {
        //_lblSendDocument.hidden=YES;
        _lblSendDocument.hidden=NO;
        [self fetchDocumentsToBeSend];
    }
    else
    {
        _lblSendDocument.hidden=NO;
        [self fetchDocumentsToBeSend];
    }
    
    
    [self adjusttableViewHeight];
    [self addServiceProposalFollow];
    [_btnForFollowUp setHidden:YES];
    //End
    
    
    BOOL isSurveyCompleted=[global isSurveyCompletedSales:strLeadId];
    
    if (isSurveyCompleted) {
        
        [_btnSurveyy setHidden:YES];
        
    }
    
    if ([_isCustomerPresent isEqualToString:@"no"]) {
        
        [_btnSurveyy setHidden:YES];
        
    } else {
        
        ////  [_btnSurveyy setHidden:NO];
        
    }
    
    if (_strHeaderValue.length>0){
        
        _lblMainHeader.text = _strHeaderValue;
        
    }else{
        
        _lblMainHeader.text = @"";
        
    }
    [self fetch_NewServiceData];

    [self fetchLeadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionOnCancel:(id)sender
{
    if ([_strFromWdo isEqualToString:@"yes"])
    {
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        [defsBack setBool:true forKey:@"isFromBackSendMailWdo"];
        [defsBack synchronize];
        
        // Before going back sync data to server
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"followUpSaved"];
        [defs synchronize];
        [self finalUpdatedTableData];
        [self deleteFromCoreDataFollowUp];
        [self saveToCoreDataServiceFollowUp];
        [self saveToCoreDataProposalFollowUp];
        [self fetchServiceFollowUpForSyncCall];
        //[self fetchProposalFollowupForSend];
        [self fetchProposalFollowUpForSyncCall];
        //End
        //eND
        [self salesEmailCountToSend];
        [self updateLeadIdDetail:@"false"];
        
        if (isEditedInSalesAuto==YES)
        {
            
            NSLog(@"Global mopdify date called in Agreement");
            [global updateSalesModifydate:strLeadId];
            
        }
        
        [self syncCall];
        
    }else if ([_strFromWdo isEqualToString:@"NPMA"])
    {
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        [defsBack setBool:true forKey:@"isFromBackSendMailNPMA"];
        [defsBack synchronize];
        
        // Before going back sync data to server
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"followUpSaved"];
        [defs synchronize];
        [self finalUpdatedTableData];
        [self deleteFromCoreDataFollowUp];
        [self saveToCoreDataServiceFollowUp];
        [self saveToCoreDataProposalFollowUp];
        [self fetchServiceFollowUpForSyncCall];
        //[self fetchProposalFollowupForSend];
        [self fetchProposalFollowUpForSyncCall];
        //End
        //eND
        [self salesEmailCountToSend];
        [self updateLeadIdDetail:@"false"];
        
        if (isEditedInSalesAuto==YES)
        {
            
            NSLog(@"Global mopdify date called in Agreement");
            [global updateSalesModifydate:strLeadId];
            
        }
        
        [self syncCall];
        
    }else  if ([_strFromWhere isEqualToString:@"Appointment"])
    {
        _strFromWhere=@"abc";
        [self.navigationController popViewControllerAnimated:NO];
        
    }
    else
    {
        BOOL chkForSave;
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        chkForSave=[defs boolForKey:@"followUpSaved"];
        if(chkForSave==YES)
        {
            
        }
        else
        {
            [self finalUpdatedTableData];
            [self deleteFromCoreDataFollowUp];
            [self saveToCoreDataServiceFollowUp];
            [self saveToCoreDataProposalFollowUp];
            [self fetchServiceFollowUpForSyncCall];
            //[self fetchProposalFollowupForSend];
            [self fetchProposalFollowUpForSyncCall];
            [defs setBool:YES forKey:@"followUpSaved"];
            [defs synchronize];
        }
    
        
        NSUserDefaults *defsFlowType=[NSUserDefaults standardUserDefaults];
        if( [[defsFlowType valueForKey:@"flowType"] isEqualToString:@"Commercial"]||[[defsFlowType valueForKey:@"flowType"] isEqualToString:@"commercial"])
        {
           
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[ClarkPestSalesAgreementProposaliPad class]]) {
                    index=k1;
                    //break;
                }
            }
            /*UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPad" bundle:nil];
            ClarkPestSalesAgreementProposaliPad *objSalesAutoService=[storyboard instantiateViewControllerWithIdentifier:@"ClarkPestSalesAgreementProposaliPad"];
            [self.navigationController pushViewController:objSalesAutoService animated:NO];*/
            
            
            ClarkPestSalesAgreementProposaliPad *myController = (ClarkPestSalesAgreementProposaliPad *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
            
            
        }
        else
        {
           
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[SalesAutomationAgreementProposaliPad class]]) {
                    index=k1;
                    //break;
                }
            }
            
            
            SalesAutomationAgreementProposaliPad *myController = (SalesAutomationAgreementProposaliPad *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
        }
        
        
    }
    
    
    //    for (UIViewController *controller in self.navigationController.viewControllers)
    //    {
    //        if ([controller isKindOfClass:[SalesAutomationAgreementProposal class]])
    //        {
    //            [self.navigationController popToViewController:controller animated:NO];
    //            break;
    //        }
    //    }
    // [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)actionOnSendEmail:(id)sender
{
    
#pragma mark- TEMP COMMENTS
    
    [self updateEmailSubject];
    
    if(_txtFldSubjectEmail.text.length == 0 || [_txtFldSubjectEmail.text isEqualToString:@""])
       
    {
           
        [global AlertMethod:Alert :@"Please enter email subject"];
       
    } else{
       
        if ([[_btnSendMail currentTitle]isEqualToString:@"Resend Email"] || [_btnSendMail.titleLabel.text isEqualToString:@"Resend Email"])
        {
            Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
            NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
            if (netStatusWify1== NotReachable)
            {
                [global AlertMethod:Alert :ErrorInternetMsg];
                NSIndexPath *path = [_tblDocuments indexPathForSelectedRow];
                NSLog(@"Path %@",path);
            }
            else
            {
    #pragma mark- TEMP COMMENTS
                
                appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                context = [appDelegate managedObjectContext];
                
                entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
                requestModifyDate = [[NSFetchRequest alloc] init];
                [requestModifyDate setEntity:entityModifyDate];
                
                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                
                NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadIdd = %@ AND userName = %@",strLeadId,strUsername];
                
                [requestModifyDate setPredicate:predicate];
                
                sortDescriptorModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
                sortDescriptorsModifyDate = [NSArray arrayWithObject:sortDescriptorModifyDate];
                
                [requestModifyDate setSortDescriptors:sortDescriptorsModifyDate];
                
                self.fetchedResultsControllerModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
                [self.fetchedResultsControllerModifyDate setDelegate:self];
                
                // Perform Fetch
                NSError *error = nil;
                [self.fetchedResultsControllerModifyDate performFetch:&error];
                arrAllObjModifyDate = [self.fetchedResultsControllerModifyDate fetchedObjects];
                if ([arrAllObjModifyDate count] == 0)
                {
    #pragma mark- NILIND 18 Oct
                    //Nilind
                    //strModifyDateToSendToServerAll=[global modifyDate];
                    //..........
                }
                else
                {
                    matchesModifyDate=arrAllObjModifyDate[0];
                    strModifyDateToSendToServerAll=[matchesModifyDate valueForKey:@"modifyDate"];
                }
                if (error)
                {
                    NSLog(@"Unable to execute fetch request.");
                    NSLog(@"%@, %@", error, error.localizedDescription);
                }
                else
                {
                }
                
                //Nilind 20Feb
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setBool:YES forKey:@"followUpSaved"];
                [defs synchronize];
                [self fetchLeadDetail];
                //[self saveDocumentToBeSend];
                [self saveDocumentToBeSendForResend];
                [self fetchDocumentsDetail];
                [self fetchEmailDetail];
                
                [self salesEmailCountToSend];
                if (arrSendEmailCount.count==0)
                {
                    [global AlertMethod:@"Alert!" :@"No email to send"];
                }
                else
                {
                    
                    if (isEditedInSalesAuto==YES)
                    {
                        NSLog(@"Global mopdify date called in Agreement");
                        [global updateSalesModifydate:strLeadId];
                    }
                    
                    //call method for resend email
                    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
                    [self performSelector:@selector(resendEmail) withObject:nil afterDelay:1.0];
                }
                
                
            }
            
        }
        
    #pragma mark- TEMP COMMENTS
        //Nilind 20Feb
        else
        {
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setBool:YES forKey:@"followUpSaved"];
            [defs synchronize];
            [self finalUpdatedTableData];
            [self deleteFromCoreDataFollowUp];
            [self saveToCoreDataServiceFollowUp];
            [self saveToCoreDataProposalFollowUp];
            [self fetchServiceFollowUpForSyncCall];
            //[self fetchProposalFollowupForSend];
            [self fetchProposalFollowUpForSyncCall];
            //End
            //eND
            [self salesEmailCountToSend];
            if (arrSendEmailCount.count==0)
            {
                [global AlertMethod:@"Alert!" :@"No email to send"];
            }
            else
            {
                
                [self updateLeadIdDetail:@"true"];
                
                if (isEditedInSalesAuto==YES)
                {
                    NSLog(@"Global mopdify date called in Agreement");
                    [global updateSalesModifydate:strLeadId];
                }
                [self syncCall];
            }
            /* Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
             NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
             if (netStatusWify1== NotReachable)
             {
             [DejalBezelActivityView removeView];
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection.Data saved offline" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
             [alert show];
             }
             else
             {
             
             [self FetchFromCoreDataToSendSalesDynamic];
             
             }*/
        }
        
    }
    
}
- (IBAction)actionOnContinueToSetup:(id)sender
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        
        [self goToBeginAuditView];
        
    }
    
}

-(void)goToBeginAuditView{
    
    NSString *strStatus;
    strStatus=[self fetchLead];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        if([UIScreen mainScreen].bounds.size.height == 480.0)
        {
            //move to your iphone5 xib
            BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView" bundle:nil];
            bav.statusOfInitialSetup=strStatus;
            [self.navigationController pushViewController:bav animated:YES];
        }
        else{
            //move to your iphone4s xib
            BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView_iphone5" bundle:nil];
            bav.statusOfInitialSetup=strStatus;
            [self.navigationController pushViewController:bav animated:YES];
        }
    }
    else{
        //BeginAuditView_iPad
        BeginAuditViewiPad *bav = [[BeginAuditViewiPad alloc] initWithNibName:@"BeginAuditView_iPad" bundle:nil];
        bav.statusOfInitialSetup=strStatus;
        [self.navigationController pushViewController:bav animated:NO];
    }
    
}


// 2nd Sept
#pragma mark -SalesAuto Fetch Core Data

-(void)salesEmailFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    arrEmail=[[NSMutableArray alloc]init];
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrEmail addObject:[matches valueForKey:@"emailId"]];
            [arr addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"emailId"]);
        
    }
    chk=YES;
    //[_tblEmailData reloadData];
    
}

#pragma mark- Table Delegate Method
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==0)
    {
        return tableView.rowHeight;
    }
    else if (tableView.tag==1)
    {
        return tableView.rowHeight;
    }
    else if (tableView.tag==2)
    {
        return tableView.rowHeight;
    }
    else
    {
        return tableView.rowHeight;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==0)
    {
        return arrEmail.count;
    }
    else if (tableView.tag==1)
    {
        if (chkForServiceFollow==YES)
        {
            return arrServiceFollowDept.count;
        }
        else
        {
            return arrServiceDepartmentName.count;
        }
        //10;
    }
    else if (tableView.tag==3) //For documents
    {
        return arrFinalDocumets.count;
        
    }
    
    else //if (tableView.tag==2)
    {
        if (chkForProposalFollow==YES)
        {
            return arrProposalFollowDept.count;//3;
        }
        else
        {
            return arrForProposalService.count;
        }
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==0)
    {
        static NSString *identifier=@"cell";
        
        cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        matches=[arrAllObj objectAtIndex:indexPath.row];
        
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"emailId"]];
        
        if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isMailSent"]] isEqualToString:@"true"])
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        cell.textLabel.font=[UIFont systemFontOfSize:20];
        return cell;
    }
    else if (tableView.tag==1)
    {
        
        static NSString *identifier=@"cellService";
        
        ServiceFollowUpTableViewCell *cellService=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellService==nil)
        {
            cellService=[[ServiceFollowUpTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (chkForServiceFollow==YES)
        {
            cellService.lblDeptName.text=[dictDeptNameByKey valueForKey:[arrServiceFollowDept objectAtIndex:indexPath.row]];
            NSArray *strArr= [dictMutableServiceFollow valueForKey:[arrServiceFollowDept objectAtIndex:indexPath.row]];
            
            NSMutableArray*arrAllService;
            arrAllService=[[NSMutableArray alloc]init];
            for (int l=0; l<strArr.count; l++)
            {
                NSString *strVal=[NSString stringWithFormat:@"%@",[dictServiceName valueForKey:[strArr objectAtIndex:l]]];
                
                if (strVal.length==0 || [strVal isEqualToString:@""]||[strVal isEqual:NULL] ||[strVal isEqual:nil]||[strVal isEqualToString:@"(null)"]  )
                    
                {
                    [arrAllService addObject:[strArr objectAtIndex:l]];
                }
                else
                {
                    [arrAllService addObject:strVal];
                }
            }
            
            NSString *joinedComponents = [arrAllService componentsJoinedByString:@","];
            cellService.txtViewService.text=[NSString stringWithFormat:@"Follow up for: %@",joinedComponents];
        }
        else
        {
            cellService.lblDeptName.text=[arrServiceDepartmentName objectAtIndex:indexPath.row];
            
            cellService.txtViewService.text=[arrForServiceFollowName objectAtIndex:indexPath.row];
            
            cellService.btnFollowUp.titleLabel.text=[arrServiceFollowDate objectAtIndex:indexPath.row];
        }
        // cellService.btnFollowUp.titleLabel.text=[self increaseDateFoService];
        NSString *strDateForService;
        strDateForService=[self increaseDateFoService];
        [cellService.btnFollowUp setTitle:strDateForService forState:UIControlStateNormal];
        cellService.btnFollowUp.tag=indexPath.row;
        
        [cellService.btnFollowUp addTarget:self
                                    action:@selector(buttonClickedForService:) forControlEvents:UIControlEventTouchDown];
        cellService.btnFollowUp.titleLabel.textColor=[UIColor blackColor];
        return  cellService;
    }
    else if (tableView.tag==3) //For documents
    {
        static NSString *identifier=@"cell";
        
        cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSString *isDefault;
        
        if (chkStatus==YES)
        {
            NSDictionary *dict=[arrFinalDocumets objectAtIndex:indexPath.row];
            
            cell.textLabel.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
            NSString *strDocSysName;
            strDocSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"OtherDocSysName"]];
            BOOL chkDoc;
            chkDoc=NO;
            for (int i=0; i<arrDocumentSelected.count; i++) //arrDocId
            {
                NSDictionary *dictNew = [arrDocumentSelected objectAtIndex:i];
                NSString *strDocSelected = [NSString stringWithFormat:@"%@",[dictNew valueForKey:@"OtherDocSysName"]];
              //  if ([[arrDocId objectAtIndex:i] isEqualToString:strDocSysName])
                if ([strDocSelected isEqualToString:strDocSysName])

                {
                    chkDoc=YES;
                    break;
                }
            }
            if (chkDoc==YES)
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                
            }
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                
            }
            
        }
        else
        {
            NSDictionary *dict=[arrFinalDocumets objectAtIndex:indexPath.row];
            isDefault=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDefault"]];
            cell.textLabel.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
            
            /*if ([isDefault isEqualToString:@"true"]||[isDefault isEqualToString:@"1"]||[isDefault isEqualToString:@"True"])
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                
            }
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                
                
            }*/
            NSString *strDocSysName;
            strDocSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"OtherDocSysName"]];
            BOOL chkDoc;
            chkDoc=NO;
            for (int i=0; i<arrDocumentSelected.count; i++) //arrDocId
            {
                NSDictionary *dictNew = [arrDocumentSelected objectAtIndex:i];
                NSString *strDocSelected = [NSString stringWithFormat:@"%@",[dictNew valueForKey:@"OtherDocSysName"]];
              //  if ([[arrDocId objectAtIndex:i] isEqualToString:strDocSysName])
                if ([strDocSelected isEqualToString:strDocSysName])

                {
                    chkDoc=YES;
                    break;
                }
            }
            if (chkDoc==YES)
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                
            }
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                
            }
        }
        cell.textLabel.font=[UIFont systemFontOfSize:20];
        
        return  cell;
    }
    else //if (tableView.tag==2)
    {
        static NSString *identifier=@"cellProposal";
        
        ProposalFollowUpTableViewCell *cellProposal=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellProposal==nil)
        {
            cellProposal=[[ProposalFollowUpTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (chkForProposalFollow==YES)
        {
            NSString *str;
            str=[arrProposalSerivceName objectAtIndex:indexPath.row];
            
            NSString *strVal=[NSString stringWithFormat:@"%@",[dictServiceName valueForKey:[arrProposalSerivceName objectAtIndex:indexPath.row]]];
            
            if (strVal.length==0 || [strVal isEqualToString:@""]||[strVal isEqual:NULL] ||[strVal isEqual:nil]||[strVal isEqualToString:@"(null)"]  )
            {
                cellProposal.lblServiceProposal.text=[arrProposalSerivceName objectAtIndex:indexPath.row];
                cellProposal.txtViewProposal.text=[NSString stringWithFormat:@"Follow up for: %@",str];
            }
            else
            {
                cellProposal.lblServiceProposal.text=strVal;
                cellProposal.txtViewProposal.text=[NSString stringWithFormat:@"Follow up for: %@",strVal];
            }
        }
        else
        {
            cellProposal.lblServiceProposal.text=[arrForProposalService objectAtIndex:indexPath.row];
            
            cellProposal.txtViewProposal.text=[arrForProposalNotes objectAtIndex:indexPath.row];
            cellProposal.btnProposalFollowUp.titleLabel.text=[arrForProposalFollowUpDate objectAtIndex:indexPath.row];
        }
        NSString *strDateForProposal;
        strDateForProposal=[self increaseDateForProposal];
        [cellProposal.btnProposalFollowUp setTitle:strDateForProposal forState:UIControlStateNormal];
        // cellProposal.btnProposalFollowUp.titleLabel.text=[self increaseDateForProposal];
        cellProposal.btnProposalFollowUp.tag=indexPath.row;
        [cellProposal.btnProposalFollowUp addTarget:self
                                             action:@selector(buttonClickedForProposal:) forControlEvents:UIControlEventTouchDown];
        cellProposal.btnProposalFollowUp.titleLabel.textColor=[UIColor blackColor];
        
        return  cellProposal;
    }
    // return cell;
    
}
-(void)buttonClickedForService:(UIButton*)sender
{
    strTable=@"service";
    NSLog(@"%ld",(long)sender.tag);
    tagValue=sender.tag;
    [self addPickerViewDateTo];
    //UIButton *button=(UIButton *) sender;
    //    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    //
    //    ServiceFollowUpTableViewCell *tappedCell = (ServiceFollowUpTableViewCell *)[_tblServiceFollowUp cellForRowAtIndexPath:indexpath];
    //    [tappedCell.btnFollowUp setTitle:strDate forState:UIControlStateNormal];
    
}

-(void)buttonClickedForProposal:(UIButton*)sender
{
    strTable=@"proposal";
    tagValue=sender.tag;
    [self addPickerViewDateTo];
    NSLog(@"%ld",(long)sender.tag);
    //UIButton *button=(UIButton *) sender;
    //    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    //
    //    ProposalFollowUpTableViewCell *tappedCell = (ProposalFollowUpTableViewCell *)[_tblProposalFollowUp cellForRowAtIndexPath:indexpath];
    //    [tappedCell.btnProposalFollowUp setTitle:strDate forState:UIControlStateNormal];
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==0)
    {
        
        [self salesEmailFetch];
        
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            chkClickStatus=YES;
        }
        else
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
            chkClickStatus=NO;
        }
        NSManagedObject *record = [arrAllObj objectAtIndex:indexPath.row];
        [self updateMailStatus:record];
        if (isEditedInSalesAuto==YES)
        {
            NSLog(@"Global modify date called in sendmail");
            [global updateSalesModifydate:strLeadId];
        }
        
    }
    else if (tableView.tag==3)
    {
        
        //Temp
        if ([_btnSendMail.titleLabel.text isEqualToString:@"Resend Email"] ||[_btnSendMail.titleLabel.text isEqualToString:@"ResendEmail"])
        {
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            NSDictionary *dict=[arrFinalDocumets objectAtIndex:index];
            
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                [arrTempDocumentForResend addObject:dict];
                
            }
            else
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                [arrTempDocumentForResend removeObject:dict];
                
            }
            
        }
        
        //End
        
        else
        {
            
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
            NSDictionary *dict=[arrFinalDocumets objectAtIndex:index];
            
            if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
                [arrLocalDocument addObject:dict];
                [arrDocumentSelected addObject:dict];
            }
            else
            {
                [cellNew setAccessoryType:UITableViewCellAccessoryNone];
                [arrLocalDocument removeObject:dict];
                [arrDocumentSelected removeObject:dict];

                
            }
        }
        
        
    }
    
}
-(void)finalMails
{
    /* arrFinalSend=[[NSMutableArray alloc]init];
     for (int i=0; i<arr.count; i++)
     {
     int value;
     value=[[arr objectAtIndex:i]intValue];
     [arrFinalSend addObject:[arrEmail objectAtIndex:value]];
     
     }
     NSLog(@"ArrFinalSend >>%@",arrFinalSend);*/
}
#pragma mark- 13 Sept Nilind
#pragma mark- FETCHING ALL RECORDS CORE DATA
-(void)fetchLeadDetail
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadDetail"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            strAudioNameGlobal=[matches valueForKey:@"audioFilePath"];
            
            NSArray *arrLeadDetailKey;
            //arrLeadDetailKey=[[NSMutableArray alloc]init];
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrLeadDetailKey);
            
            NSLog(@"all keys %@",arrLeadDetailValue);
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                if(![str isKindOfClass:[NSString class]])
                {
                    str=@"";
                }
                //[arrLeadDetailValue addObject:str];
                if([[arrLeadDetailKey objectAtIndex:i]isEqualToString:@"billingCountry"]||[[arrLeadDetailKey objectAtIndex:i]isEqualToString:@"serviceCountry"])
                {
                    [arrLeadDetailValue addObject:[str stringByReplacingOccurrencesOfString:@" " withString:@""]];
                }
                else
                {
                    [arrLeadDetailValue addObject:str];
                }
                
            }
            NSDictionary *dictLeadDetail;
            dictLeadDetail = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            NSLog(@"LeadDetail%@",dictLeadDetail);
            //[dictFinal setObject:dictLeadDetail forKey:@"LeadDetail"];
            
            
            
            int indexToRemove=-1;
            int indexToRemoveDate=-1;
            int indexToReplaceModifyDate=-1;
            int indexToReplaceScheduleDated=-1;
            
            for (int k=0; k<arrLeadDetailKey.count; k++) {
                
                NSString *strKeyLeadId=arrLeadDetailKey[k];
                //dateModified
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"dateModified"]) {
                    
                    indexToRemoveDate=k;
                    
                }
                
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"zdateScheduledStart"]) {
                    
                    indexToReplaceScheduleDated=k;
                    
                }
                
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrLeadDetailKey];
            [arrKeyTemp removeObjectAtIndex:indexToRemoveDate];
            // [arrKeyTemp removeObjectAtIndex:indexToRemove-1];
            arrLeadDetailKey=arrKeyTemp;
            [arrLeadDetailValue removeObjectAtIndex:indexToRemoveDate];
            //  [arrLeadDetailValue removeObjectAtIndex:indexToRemove-1];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceScheduleDated-1 withObject:@""];
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
            [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
            NSLog(@"%@",dictTemp);
            [dictFinal setObject:dictTemp forKey:@"LeadDetail"];
            
            //New Change
            strLeadNumber=[matches valueForKey:@"leadNumber"];
            strScheduleInspectionDate=[matches valueForKey:@"scheduleStartDate"];
            strAccountNo=[matches valueForKey:@"accountNo"];
            strProposal=[matches valueForKey:@"isProposalFromMobile"];
            if ([strProposal isEqualToString:@"true"])
            {
                strProposal=@"YES";
            }
            else
            {
                strProposal=@"NO";
                
            }
            isResendEmailCheck=[matches valueForKey:@"isResendAgreementProposalMail"];
        }
    }
}
-(void)fetchPaymentInfo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrCheckImage=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"PaymentInfo"];
        
    }
    else
    {
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
                
                NSString *strCustomerImage=[matches valueForKey:@"customerSignature"];
                NSString *strSalesPersonImage=[matches valueForKey:@"salesSignature"];
                NSString *strCheckFrontImage=[matches valueForKey:@"checkFrontImagePath"];
                NSString *strCheckBackImage=[matches valueForKey:@"checkBackImagePath"];
                
                if (!(strCustomerImage.length==0)) {
                    
                    [arrOfAllSignatureImagesToSendToServer addObject:strCustomerImage];//salesSignature
                    
                }
                if (!(strSalesPersonImage.length==0))
                {
                    
                    [arrOfAllSignatureImagesToSendToServer addObject:strSalesPersonImage];//salesSignature
                    
                }
                if (!(strCheckFrontImage.length==0))
                {
                    
                    [arrCheckImage addObject:strCheckFrontImage];//salesSignature
                    
                }
                if (!(strCheckBackImage.length==0))
                {
                    
                    [arrCheckImage addObject:strCheckBackImage];//salesSignature
                    
                }
                
                
                NSArray *arrPaymentInfoKey;
                NSMutableArray *arrPaymentInfoValue;
                
                arrPaymentInfoValue=[[NSMutableArray alloc]init];
                arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrPaymentInfoKey);
                
                NSLog(@"all keys %@",arrPaymentInfoValue);
                for (int i=0; i<arrPaymentInfoKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                    
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                    
                    [arrPaymentInfoValue addObject:str];
                }
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                int indexToRemove=-1;
                int indexToReplaceModifyDate=-1;
                
                for (int k=0; k<arrPaymentInfoKey.count; k++) {
                    
                    NSString *strKeyLeadId=arrPaymentInfoKey[k];
                    
                    if ([strKeyLeadId isEqualToString:@"leadId"]) {
                        
                        indexToRemove=k;
                        
                    }
                    if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                        
                        indexToReplaceModifyDate=k;
                        
                    }
                }
                
                NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
                
                [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
                
                [arrKeyTemp removeObjectAtIndex:indexToRemove];
                arrPaymentInfoKey=arrKeyTemp;
                [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
                
                [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                NSDictionary *dictPaymentInfo;
                dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
                NSLog(@"PaymentInfo%@",dictPaymentInfo);
                [dictFinal setObject:dictPaymentInfo forKey:@"PaymentInfo"];
            }
            
        
    }
}
-(void)fetchImageDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    arrUploadGraphXML = [[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matches valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                //Nilind 11 Nov
                /*NSString *result;
                 NSRange equalRange = [strImageName rangeOfString:@"/" options:NSBackwardsSearch];
                 if (equalRange.location != NSNotFound) {
                 result = [strImageName substringFromIndex:equalRange.location + equalRange.length];
                 }
                 if (result.length==0) {
                 NSRange equalRange = [strImageName rangeOfString:@"\\" options:NSBackwardsSearch];
                 if (equalRange.location != NSNotFound) {
                 result = [strImageName substringFromIndex:equalRange.location + equalRange.length];
                 }
                 }
                 [arrOfAllImagesToSendToServer addObject:result];*/
                //...................
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isNewGraph"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[matches valueForKey:@"isNewGraph"]] isEqualToString:@"True"] || [[NSString stringWithFormat:@"%@",[matches valueForKey:@"isNewGraph"]] isEqualToString:@"1"])
            {
                NSString *strXMLName = [matches valueForKey:@"leadXmlPath"];
                if (!(strXMLName.length==0))
                {
                    [arrUploadGraphXML addObject:strXMLName];
                }

            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
        
    }
}
-(void)fetchEmailDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetail= [NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    [request setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}
-(void)fetchDocumentsDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrDocumentsDetail forKey:@"DocumentsDetail"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrDocumentsDetail addObject:dictPaymentInfo];
            NSLog(@"DocumentsDetail%@",arrDocumentsDetail);
        }
        [dictFinal setObject:arrDocumentsDetail forKey:@"DocumentsDetail"];
    }
}
-(void)fetchLeadPreference
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadPreference= [NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context];
    [request setEntity:entityLeadPreference];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadPreference"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictLeadPreference;
            dictLeadPreference = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadPreference%@",dictLeadPreference);
            [dictFinal setObject:dictLeadPreference forKey:@"LeadPreference"];
        }
        
        
    }
}
-(void)fetchSoldServiceStandardDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrSoldServiceStandardDetail;
    arrSoldServiceStandardDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrSoldServiceStandardDetail forKey:@"SoldServiceStandardDetail"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        arrSoldCount=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //Nilind 09 Jan
            if([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isSold"]]isEqualToString:@"true"])
            {
                [arrSoldCount addObject:[NSString stringWithFormat:@"%d",k]];
            }
            //End
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                //NSString *str;
                id str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil])// || str.length==0 )
                {
                    str=@"";
                }
                if([str isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                }
                
                [arrPaymentInfoValue addObject:str];
                
                /* NSString *str;
                 str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                 
                 BOOL isTrue=[global isStringNullNilBlank:str];
                 if (isTrue) {
                 str=@"";
                 }
                 
                 [arrPaymentInfoValue addObject:str];*/
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictSoldServiceStandardDetail;
            dictSoldServiceStandardDetail = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictSoldServiceStandardDetail);
            [arrSoldServiceStandardDetail addObject:dictSoldServiceStandardDetail];
            NSLog(@"SoldServiceStandardDetail%@",arrSoldServiceStandardDetail);
        }
        [dictFinal setObject:arrSoldServiceStandardDetail forKey:@"SoldServiceStandardDetail"];
        
    }
}
-(void)fetchSoldServiceNonStandardDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceNonStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrSoldServiceNonStandardDetail;
    arrSoldServiceNonStandardDetail=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrSoldServiceNonStandardDetail forKey:@"SoldServiceNonStandardDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictSoldServiceNonStandardDetail;
            dictSoldServiceNonStandardDetail = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictSoldServiceNonStandardDetail);
            [arrSoldServiceNonStandardDetail addObject:dictSoldServiceNonStandardDetail];
            NSLog(@"SoldServiceNonStandardDetail%@",arrSoldServiceNonStandardDetail);
        }
        [dictFinal setObject:arrSoldServiceNonStandardDetail forKey:@"SoldServiceNonStandardDetail"];
        
    }
}

-(void)sendingleadToServer
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
    requestModifyDate = [[NSFetchRequest alloc] init];
    [requestModifyDate setEntity:entityModifyDate];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadIdd = %@ AND userName = %@",strLeadId,strUsername];
    
    [requestModifyDate setPredicate:predicate];
    
    sortDescriptorModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsModifyDate = [NSArray arrayWithObject:sortDescriptorModifyDate];
    
    [requestModifyDate setSortDescriptors:sortDescriptorsModifyDate];
    
    self.fetchedResultsControllerModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerModifyDate performFetch:&error];
    arrAllObjModifyDate = [self.fetchedResultsControllerModifyDate fetchedObjects];
    if ([arrAllObjModifyDate count] == 0)
    {
#pragma mark- NILIND 18 Oct
        //Nilind
        //strModifyDateToSendToServerAll=[global modifyDate];
        //..........
    }
    else
    {
        matchesModifyDate=arrAllObjModifyDate[0];
        strModifyDateToSendToServerAll=[matchesModifyDate valueForKey:@"modifyDate"];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    arrOfAllImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    
    indexToSendImage=0;
    indexToSendImageSign=0;
    
    [self saveDocumentToBeSend];
    [self fetchLeadDetail];
    [self fetchPaymentInfo];
    [self fetchImageDetail];
    [self fetchEmailDetail];
    [self fetchDocumentsDetail];
    [self fetchLeadPreference];
    [self fetchSoldServiceStandardDetail];
    [self fetchSoldServiceNonStandardDetail];
    [self fetchCurrentServices];
    [self fetchAgreementCheckListSales];
    [self fetchElectronicAuthorizedForm];
    [self fetchForAppliedDiscountFromCoreData];
    [self fetchRenewalServiceData];
    [self fetchTagDetail];
    //For  ClarkPest
    
    [self fetchScopeFromCoreData];
    [self fetchTargetFromCoreData];
    [self fetchLeadCommercialInitialInfoFromCoreData];
    [self fetchLeadCommercialMaintInfoFromCoreData];
    [self fetchLeadCommercialDetailExtDcFromCoreData];
    [self fetchMultiTermsFromCoreDataClarkPest];
    [self fetchForAppliedDiscountFromCoreDataClarkPest];
    [self fetchSalesMarketingContentFromCoreDataClarkPest];
    
    
    
    
    
    //Nilind 20Feb
    NSUserDefaults *defsFollow=[NSUserDefaults standardUserDefaults];
    NSString *str=[defsFollow valueForKey:@"FollowUp"];
    //[defs setValue:@"forFollowUp" forKey:@"FollowUp"];
    if ([str isEqualToString:@"forFollowUp"])
    {
        
        [self fetchServiceFollowUpForSyncCall];
        [self fetchProposalFollowUpForSyncCall];
        [defsFollow setObject:@"abcdefg" forKey:@"FollowUp"];
        [defsFollow synchronize];
    }
    //End
    else
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        if ([[defs valueForKey:@"fromGenerateWorkorder"] isEqualToString:@"fromGenerateWorkorder"])
        {
            [defs setValue:@"acb" forKey:@"fromGenerateWorkorder"];
            [defs synchronize];
        }
        
        else
        {
            if (isServiceFollowUp==YES)
            {
                [self finalUpdatedTableData];
                [self fetchServiceFollowupForSend];
            }
            if (isProposalFoolowUp==YES)
            {
                [self finalUpdatedTableData];
                [self fetchProposalFollowupForSend];
            }
        }
        
    }
    NSLog(@"Final Dictionary >>>>>>%@",dictFinal);
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        if (arrOfAllImagesToSendToServer.count==0) {
            
            if (arrOfAllSignatureImagesToSendToServer.count==0) {
                
                if (strAudioNameGlobal.length==0 || [strAudioNameGlobal isEqualToString:@"(null)"]) {
                    
                    [self fialJson];
                    
                }
                else
                {
                    
                    [self uploadAudio:strAudioNameGlobal];
                    
                }
                
                
            } else {
                
                [self uploadingAllImages:0];
                
            }
            
            
        }
        else
        {
            
            [self uploadingAllImages:0];
            
        }
    }
}
#pragma mark- FINAL JSON SENDING

-(void)fialJson
{
    for (int i=0; i<arrCheckImage.count; i++)
    {
        [self uploadImageCheckImage:[arrCheckImage objectAtIndex:i]];
    }
    for (int i=0; i<arrAllTargetImages.count; i++)
    {
        [self uploadTargetImage:[arrAllTargetImages objectAtIndex:i]];
    }
    if ([strElectronicSign isKindOfClass:[NSString class]])
    {
        [self uploadSignatureImageOnServer:strElectronicSign];
    }
    //For OtherDocument Upload
    [self fetchOtherDocument];
    for (int i=0; i<arrUploadOtherDocument.count; i++)
    {
        [self uploadOtherDocuments:[arrUploadOtherDocument objectAtIndex:i] DocumentId:[arrUploadOtherDocumentId objectAtIndex:i]];
        //[self uploadOtherDocumentImage:[arrUploadOtherDocument objectAtIndex:i]];
    }
    
    //For Graph XML Upload
    for (int i=0; i<arrUploadGraphXML.count; i++)
    {
        [self uploadGraphXml:[arrUploadGraphXML objectAtIndex:i]];
    }
    
    
    [self finalJsonAfterImageSync];
    //-(void)uploadImageCheckImage :(NSString*)strImageName
    
}
-(void)finalJsonAfterImageSync
{
    if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
    {
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    }
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dictFinal])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictFinal options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Update Sale  JSON: %@", jsonString);
            }
            
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesFinalUpdate];
        
        
        //============================================================================
        //============================================================================
        NSDictionary *dictTemp;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     //Sending Dynamic Json
                     //NSLog(@"fianl response %@",response);
                     
                     NSString *strJson = [global convertIntoJson:response :arr :@"dict" :@"Sales Response on update"];

                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         [global updateSalesZSYNC:strLeadId :@"no"];
                         [self updateLeadDetailOnResponse];
                         //Nilind 05 Dec
                         // Save target in DB
                         
                         NSArray *arrTargets=[response valueForKey:@"leadCommercialTargetExtDcs"];
                         
                         if ([arrTargets isKindOfClass:[NSArray class]]){
                             
                             NSString *strTempLeadId=[NSString stringWithFormat:@"%@",[response valueForKey:@"LeadId"]];
                             
                             WebService *objWebService = [[WebService alloc] init];
                             
                             [objWebService savingTargetsWithArrTarget:arrTargets strLeadId:strTempLeadId];
                             
                         }
                         //End
                         
                        
                         if ([_strFromWdo isEqualToString:@"yes"]) {
                             
                             UIAlertController *alert= [UIAlertController
                                                        alertControllerWithTitle:Info
                                                        message:InfoWdoServiceProposalDataSynced
                                                        preferredStyle:UIAlertControllerStyleAlert];
                             
                             UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * action)
                                                   {
                                                       
                                                       NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
                                                       [defs setBool:true forKey:@"YesWdoAgreementSynced"];
                                                       [defs setBool:false forKey:@"isFromBackSendMailWdo"];
                                                       [defs synchronize];
                                                       
                                                       if (isBackkk) {
                                                           
                                                           NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
                                                           [defs setBool:true forKey:@"YesWdoAgreementSynced"];
                                                           [defs setBool:true forKey:@"isFromBackSendMailWdo"];
                                                           [defs synchronize];
                                                           
                                                       }
                                                       
                                                       int index = 0;
                                                       NSArray *arrstack=self.navigationController.viewControllers;
                                                       for (int k1=0; k1<arrstack.count; k1++) {
                                                           if ([[arrstack objectAtIndex:k1] isKindOfClass:[Agreement_WDOVC class]]) {
                                                               index=k1;
                                                           }
                                                       }
                                                       
                                                       Agreement_WDOVC *myController = (Agreement_WDOVC *)[self.navigationController.viewControllers objectAtIndex:index];
                                                       [self.navigationController popToViewController:myController animated:NO];
                                                       
                                                   }];
                             [alert addAction:yes];
                             [self presentViewController:alert animated:YES completion:nil];
                             
                         }else if ([_strFromWdo isEqualToString:@"NPMA"]) {
                             
                             UIAlertController *alert= [UIAlertController
                                                        alertControllerWithTitle:Info
                                                        message:InfoNPMAServiceProposalDataSynced
                                                        preferredStyle:UIAlertControllerStyleAlert];
                             
                             UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * action)
                                                   {
                                                       
                                                       NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
                                                       [defs setBool:true forKey:@"YesNPMAAgreementSynced"];
                                                       [defs setBool:false forKey:@"isFromBackSendMailNPMA"];
                                                       [defs synchronize];
                                                       
                                                       if (isBackkk) {
                                                           
                                                           NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
                                                           [defs setBool:true forKey:@"YesNPMAAgreementSynced"];
                                                           [defs setBool:true forKey:@"isFromBackSendMailNPMA"];
                                                           [defs synchronize];
                                                           
                                                       }
                                                       
                                                       int index = 0;
                                                       NSArray *arrstack=self.navigationController.viewControllers;
                                                       for (int k1=0; k1<arrstack.count; k1++) {
                                                           if ([[arrstack objectAtIndex:k1] isKindOfClass:[NPMA_Agreement_iPadVC class]]) {
                                                               index=k1;
                                                           }
                                                       }
                                                       
                                 NPMA_Agreement_iPadVC *myController = (NPMA_Agreement_iPadVC *)[self.navigationController.viewControllers objectAtIndex:index];
                                                       [self.navigationController popToViewController:myController animated:NO];
                                                       
                                                   }];
                             [alert addAction:yes];
                             [self presentViewController:alert animated:YES completion:nil];

                             
                         }else if ([_strForSendProposal isEqualToString:@"forSendProposal"])
                         {
                             _strForSendProposal=@"abc";
                             NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
                             
                             NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
                             
                             if ([strAppointmentFlow isEqualToString:@"New"])
                             {
                                 
                                 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
                                 AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
                                 [self.navigationController pushViewController:objByProductVC animated:NO];
                                 
                             }
                             else
                             {
                                 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
                                 AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
                                 [self.navigationController pushViewController:objByProductVC animated:NO];
                             }
                         }
                         else
                         {
                             if([[NSUserDefaults standardUserDefaults] boolForKey:@"FromInitialSetup"] == YES)
                             {
                                 NSUserDefaults *defsFromInitialSetup=[NSUserDefaults standardUserDefaults];
                                 [defsFromInitialSetup setBool:NO forKey:@"FromInitialSetup"];
                                 [defsFromInitialSetup synchronize];
                                 
                                 UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:AlertAppointmentSynced delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                 [alert show];
                             }
                             else
                             {
                                 UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:AlertAppointmentCompleted delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                 [alert show];
                             }
                             
                             NSString *strStatus;
                             strStatus=[self fetchLead];
                             if ([strStatus isEqualToString:@"false"])
                             {
                                 //Nilind 09 Jan
                                 if (arrSoldCount.count ==0)
                                 {
                                     NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
                                     
                                     NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
                                     
                                     if ([strAppointmentFlow isEqualToString:@"New"])
                                     {
                                         
                                         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
                                         AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
                                         [self.navigationController pushViewController:objByProductVC animated:NO];
                                         
                                     }
                                     else
                                     {
                                         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
                                         AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
                                         [self.navigationController pushViewController:objByProductVC animated:NO];
                                     }
                                 }
                                 //End
                                 else
                                 {
                                     
                                     
                                     
                                     NSUserDefaults *defsFlowType=[NSUserDefaults standardUserDefaults];
                                     
                                     if( [[defsFlowType valueForKey:@"flowType"] isEqualToString:@"Commercial"]||[[defsFlowType valueForKey:@"flowType"] isEqualToString:@"commercial"])
                                     {
                                         int index = 0;
                                         NSArray *arrstack=self.navigationController.viewControllers;
                                         for (int k1=0; k1<arrstack.count; k1++) {
                                             if ([[arrstack objectAtIndex:k1] isKindOfClass:[ClarkPestSalesAgreementProposaliPad class]]) {
                                                 index=k1;
                                                 //break;
                                             }
                                         }
                                        
                                        /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPad" bundle:nil];
                                         ClarkPestSalesAgreementProposaliPad *objSalesAutoService=[storyboard instantiateViewControllerWithIdentifier:@"ClarkPestSalesAgreementProposaliPad"];
                                         [self.navigationController pushViewController:objSalesAutoService animated:NO];*/
                                         
                                         
                                         
                                         ClarkPestSalesAgreementProposaliPad *myController = (ClarkPestSalesAgreementProposaliPad *)[self.navigationController.viewControllers objectAtIndex:index];
                                         
                                         [self.navigationController popToViewController:myController animated:NO];
                                         
                                         
                                         
                                     }
                                     else
                                     {
                                         
                                         int index = 0;
                                         NSArray *arrstack=self.navigationController.viewControllers;
                                         for (int k1=0; k1<arrstack.count; k1++) {
                                             if ([[arrstack objectAtIndex:k1] isKindOfClass:[SalesAutomationAgreementProposaliPad class]]) {
                                                 index=k1;
                                                 //break;
                                             }
                                         }
                                         
                                         
                                         SalesAutomationAgreementProposaliPad *myController = (SalesAutomationAgreementProposaliPad *)[self.navigationController.viewControllers objectAtIndex:index];
                                         
                                         [self.navigationController popToViewController:myController animated:NO];
                                     }
                                     
                                     
                                 }
                             }
                             else
                             {
                                 NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
                                 
                                 NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
                                 
                                 if ([strAppointmentFlow isEqualToString:@"New"])
                                 {
                                     
                                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
                                     AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
                                     [self.navigationController pushViewController:objByProductVC animated:NO];
                                     
                                 }
                                 else
                                 {
                                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
                                     AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
                                     [self.navigationController pushViewController:objByProductVC animated:NO];
                                 }
                             }
                         }
                         
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
    }
    
}
#pragma mark- FINAL IMAGE SEND

-(void)uploadingAllImages :(int)indexToSendimages{
    
    if (arrOfAllImagesToSendToServer.count==indexToSendimages) {
        
        [self uploadingAllImagesSignature:0];
        
    } else {
        
        [self uploadImage:arrOfAllImagesToSendToServer[indexToSendimages]];
        
    }
}

-(void)uploadingAllImagesSignature :(int)indexToSendimages
{
    
    if (arrOfAllSignatureImagesToSendToServer.count==indexToSendimages)
    {
        
        if (strAudioNameGlobal.length==0 || [strAudioNameGlobal isEqualToString:@"(null)"]) {
            
            [self fialJson];
            
        }
        else
        {
            
            [self uploadAudio:strAudioNameGlobal];
            
        }
        
        
    }
    else
    {
        
        [self uploadImageSignature:arrOfAllSignatureImagesToSendToServer[indexToSendimages]];
        
    }
    
}
// Nilind 21 Sept
//============================================================================
//============================================================================
#pragma mark- Upload Image METHOD
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        if ([_strFromWdo isEqualToString:@"yes"]) {

            strImageName = [global splitString:strImageName :@"\\"];
            
        }
        if ([_strFromWdo isEqualToString:@"NPMA"]) {

            strImageName = [global splitString:strImageName :@"\\"];
            
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
            
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesImageUpload];
            //http://192.168.0.218:3333//api/File/UploadAsync
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Image Sent");
            }
            NSLog(@"Image Sent");
            
        }

    }
    indexToSendImage++;
    [self uploadingAllImages:indexToSendImage];
    
}

-(void)uploadImageSignature :(NSString*)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
            
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesSignatureImageUpload];
            //http://192.168.0.218:3333//api/File/UploadAsync
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Image Sent");
            }
            NSLog(@"Image Sent");
            
        }
        
    }
    indexToSendImageSign++;
    [self uploadingAllImagesSignature:indexToSendImageSign];
    
}
//============================================================================
//============================================================================
#pragma mark- UPLOAD CHECK IMAGE
//============================================================================
//============================================================================
-(void)uploadImageCheckImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
        
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
            NSString *strCheckUrl;
            strCheckUrl=@"/api/File/UploadCheckImagesAsync";
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,strCheckUrl];//UrlSalesImageUpload];//
            
            //http://192.168.0.218:3333//api/File/UploadAsync
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Image Sent");
            }
            //[self HudView:strrr];
            
        }
    
    }
    
    
}
//...........................................................
// Nilind 24 Sept
//============================================================================
//============================================================================
#pragma mark- TEXT FIELD DELEGATE
//============================================================================
//============================================================================
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtEmailId resignFirstResponder];
    return YES;
}


//============================================================================
//============================================================================
#pragma mark- Dynamic Form Send Method
//============================================================================
//============================================================================


-(void)FetchFromCoreDataToSendSalesDynamic
{
    //Nilind 27 Dec
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    
    //......
    
    //Nilind 2 Jan
    
    /*if(strLeadId.length==0 || [strLeadId isEqual:[NSNull null]])
     {
     strLeadId=[defsLead valueForKey:@"ForInitialSetUpLeadId"];
     }
     
     //............
     
     //Nilind 3'rd Jan
     NSUserDefaults *defs12=[NSUserDefaults standardUserDefaults];
     NSString *str;
     str=[defs12 valueForKey:@"fromInitialSetupClick"];
     
     if ([str isEqualToString:@"fromInitialSetupClick"])
     {
     strLeadId=[defs12 valueForKey:@"ForInitialSetUpLeadId"];
     }*/
    //.............
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    requestNewSalesDynamic = [[NSFetchRequest alloc] init];
    [requestNewSalesDynamic setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND userName = %@",strLeadId,strUsername];
    
    [requestNewSalesDynamic setPredicate:predicate];
    
    sortDescriptorSalesDynamic = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsSalesDynamic = [NSArray arrayWithObject:sortDescriptorSalesDynamic];
    
    [requestNewSalesDynamic setSortDescriptors:sortDescriptorsSalesDynamic];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewSalesDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObjSalesDynamic = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObjSalesDynamic count] == 0)
    {
        
        if ([_strFromWdo isEqualToString:@"yes"]) {
            
            //call method for resend email
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
            
            [self performSelector:@selector(sendingleadToServer) withObject:nil afterDelay:1.0];
            
        }else if ([_strFromWdo isEqualToString:@"NPMA"]) {
            
            //call method for resend email
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
            
            [self performSelector:@selector(sendingleadToServer) withObject:nil afterDelay:1.0];
            
        } else {
            
            [self sendingleadToServer];
            
        }
        
    }
    else
    {
        matchesSalesDynamic=nil;
        
        matchesSalesDynamic=arrAllObjSalesDynamic[0];
        NSArray *arrTemp = [matchesSalesDynamic valueForKey:@"arrFinalInspection"];
        arrResponseInspection =[[NSMutableArray alloc]init];
        [arrResponseInspection addObjectsFromArray:arrTemp];
        
        [self sendingFinalDynamicJsonToServer];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)sendingFinalDynamicJsonToServer
{
    
    if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
    {
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    }
    
    
    
    NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                       arrResponseInspection,nil];
    
    NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                     @"InspectionFormsData",nil];
    
    NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesInspectionDynamicFormSubmission];
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 //[DejalBezelActivityView removeView];
                 [self sendingleadToServer];
                 
                 if (success)
                 {
                     //                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     //                     if ([str containsString:@"Success"]) {
                     //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"Data Synced Successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     //                         [alert show];
                     //                     }else{
                     //
                     //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong,Some data didn't synced properly.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     //                         [alert show];
                     //
                     //                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
    
}

- (IBAction)actionOnInitialSetup:(id)sender
{
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    InitialSetUp *objInitialSetUp=[storyBoard instantiateViewControllerWithIdentifier:@"InitialSetUp"];
    // [self presentViewController:objInitialSetUp animated:YES completion:nil];
    [self.navigationController pushViewController:objInitialSetUp animated:NO];
}
- (IBAction)actionOnBack:(id)sender
{
    
    if ([_strFromWdo isEqualToString:@"yes"])
    {
     
        isBackkk = true;
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        [defsBack setBool:true forKey:@"isFromBackSendMailWdo"];//isFromBackSendMailWdo
        [defsBack synchronize];
        
        // Before going back sync data to server
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"followUpSaved"];
        [defs synchronize];
        [self finalUpdatedTableData];
        [self deleteFromCoreDataFollowUp];
        [self saveToCoreDataServiceFollowUp];
        [self saveToCoreDataProposalFollowUp];
        [self fetchServiceFollowUpForSyncCall];
        //[self fetchProposalFollowupForSend];
        [self fetchProposalFollowUpForSyncCall];
        //End
        //eND
        [self salesEmailCountToSend];
        [self updateLeadIdDetail:@"false"];
        
        if (isEditedInSalesAuto==YES)
        {
            
            NSLog(@"Global mopdify date called in Agreement");
            [global updateSalesModifydate:strLeadId];
            
        }
        
        [self syncCall];
        
        /*
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        [defsBack setBool:true forKey:@"isFromBackSendMailWdo"];
        [defsBack synchronize];
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[Agreement_WDOVC class]]) {
                index=k1;
            }
        }
        
        Agreement_WDOVC *myController = (Agreement_WDOVC *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        */
        
        NSUserDefaults *defsBack1=[NSUserDefaults standardUserDefaults];
        [defsBack1 setBool:true forKey:@"isFromBackSendMailWdo"];//isFromBackSendMailWdo
        [defsBack1 synchronize];
        
    }else if ([_strFromWdo isEqualToString:@"NPMA"])
    {
     
        isBackkk = true;
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        [defsBack setBool:true forKey:@"isFromBackSendMailNPMA"];//isFromBackSendMailWdo
        [defsBack synchronize];
        
        // Before going back sync data to server
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"followUpSaved"];
        [defs synchronize];
        [self finalUpdatedTableData];
        [self deleteFromCoreDataFollowUp];
        [self saveToCoreDataServiceFollowUp];
        [self saveToCoreDataProposalFollowUp];
        [self fetchServiceFollowUpForSyncCall];
        //[self fetchProposalFollowupForSend];
        [self fetchProposalFollowUpForSyncCall];
        //End
        //eND
        [self salesEmailCountToSend];
        [self updateLeadIdDetail:@"false"];
        
        if (isEditedInSalesAuto==YES)
        {
            
            NSLog(@"Global mopdify date called in Agreement");
            [global updateSalesModifydate:strLeadId];
            
        }
        
        [self syncCall];
        
        /*
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        [defsBack setBool:true forKey:@"isFromBackSendMailWdo"];
        [defsBack synchronize];
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[Agreement_WDOVC class]]) {
                index=k1;
            }
        }
        
        Agreement_WDOVC *myController = (Agreement_WDOVC *)[self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:myController animated:NO];
        */
        
        NSUserDefaults *defsBack1=[NSUserDefaults standardUserDefaults];
        [defsBack1 setBool:true forKey:@"isFromBackSendMailNPMA"];//isFromBackSendMailWdo
        [defsBack1 synchronize];
        
    }
    else if ([_strFromWhere isEqualToString:@"Appointment"])
    {
        _strFromWhere=@"abc";
        [self.navigationController popViewControllerAnimated:NO];
        
    }
    else
    {
        //[self dismissViewControllerAnimated:YES completion:nil];
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        [defsBack setBool:NO forKey:@"isFromBackSendMail"];
        [defsBack synchronize];
        
        
        BOOL chkForSave;
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        chkForSave=[defs boolForKey:@"followUpSaved"];
        if(chkForSave==YES)
        {
        }
        else
        {
            [self finalUpdatedTableData];
            [self deleteFromCoreDataFollowUp];
            [self saveToCoreDataServiceFollowUp];
            [self saveToCoreDataProposalFollowUp];
            [self fetchServiceFollowUpForSyncCall];
            //[self fetchProposalFollowupForSend];
            [self fetchProposalFollowUpForSyncCall];
            [defs setBool:YES forKey:@"followUpSaved"];
            [defs synchronize];
        }
        
        
        NSUserDefaults *defsFlowType=[NSUserDefaults standardUserDefaults];
        if( [[defsFlowType valueForKey:@"flowType"] isEqualToString:@"Commercial"]||[[defsFlowType valueForKey:@"flowType"] isEqualToString:@"commercial"])
        {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[ClarkPestSalesAgreementProposaliPad class]]) {
                    index=k1;
                    //break;
                }
            }
            
            
           /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPad" bundle:nil];
            ClarkPestSalesAgreementProposaliPad *objSalesAutoService=[storyboard instantiateViewControllerWithIdentifier:@"ClarkPestSalesAgreementProposaliPad"];
            [self.navigationController pushViewController:objSalesAutoService animated:NO];*/
            
            
            ClarkPestSalesAgreementProposaliPad *myController = (ClarkPestSalesAgreementProposaliPad *)[self.navigationController.viewControllers objectAtIndex:index];
            [self.navigationController popToViewController:myController animated:NO];
            
            
        }
        else
        {
           
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[SalesAutomationAgreementProposaliPad class]]) {
                    index=k1;
                    //break;
                }
            }
            
            SalesAutomationAgreementProposaliPad *myController = (SalesAutomationAgreementProposaliPad *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
        }
    }
    
    
}
- (IBAction)actionOnAdd:(id)sender

{
    
    _txtEmailId.text= [_txtEmailId.text stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (_txtEmailId.text.length==0)
        
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid Email Id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    }
    else
    {
        BOOL chkEmail;
        chkEmail=NO;
        for (int i=0; i<arrEmail.count; i++)
        {
            if ([_txtEmailId.text isEqualToString:[arrEmail objectAtIndex:i]])
            {
                chkEmail=YES;
            }
        }
        if (chkEmail==YES)
        {
            chkEmail=NO;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Email Id already exist" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            BOOL emailChk;
            emailChk=[self NSStringIsValidEmail:_txtEmailId.text];
            if (emailChk==NO)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid Email Id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            else
            {
                
                chk=YES;
                [arrEmail addObject:_txtEmailId.text];
                [arr addObject:[NSString stringWithFormat:@"%lu",(unsigned long)arrEmail.count-1]];
                [_txtEmailId resignFirstResponder];
                [self salesEmailFetchForSave];
                //[_tblEmailData reloadData];
                // [_tblEmailData scrollRectToVisible:CGRectMake(_tblEmailData.contentSize.width - 1,_tblEmailData.contentSize.height - 1, 1, 1) animated:YES];
                _txtEmailId.text=@"";
                [self adjusttableViewHeight];
                
            }
        }
    }
    [self finalMails];
}

#pragma mark- SAVE EMAIL TO CORE DATA 12Oct Nilind


-(void)salesEmailFetchForSave
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [requestNew setSortDescriptors:sortDescriptors];
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrDuplicateMail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrDuplicateMail addObject:[matches valueForKey:@"emailId"]];
        }
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"emailId"]);
    }
    NSMutableArray *arrFinalMail;
    arrFinalMail=[[NSMutableArray alloc]init];
    arrFinalMail=arrEmail;
    for (int i=0; i<arrFinalMail.count; i++)
    {
        for (int j=0; j<arrDuplicateMail.count; j++)
        {
            if ([[arrFinalMail objectAtIndex:i] isEqualToString:[arrDuplicateMail objectAtIndex:j]])
            {
                [arrFinalMail removeObjectAtIndex:i];
            }
        }
    }
    [self saveEmailToCoreData:arrFinalMail];
    
}

-(void)saveEmailToCoreData:(NSMutableArray *)arrEmailDetail
{
    for (int j=0; j<arrEmailDetail.count; j++)
    {
        //Email Detail Entity
        isEditedInSalesAuto=YES;
        
        entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
        EmailDetail *objEmailDetail = [[EmailDetail alloc]initWithEntity:entityEmailDetail insertIntoManagedObjectContext:context];
        objEmailDetail.createdBy=@"0";;
        
        objEmailDetail.createdDate=@"0";
        
        objEmailDetail.emailId=[arrEmailDetail objectAtIndex:j];
        
        objEmailDetail.isCustomerEmail=@"true";
        
        objEmailDetail.isMailSent=@"true";
        
        objEmailDetail.leadMailId=@"";
        objEmailDetail.isDefaultEmail=@"false";

        objEmailDetail.modifiedBy=@"";
        
        objEmailDetail.modifiedDate=[global modifyDate];
        
        objEmailDetail.subject=@"";
        
        objEmailDetail.leadId=strLeadId;
        objEmailDetail.userName=strUserName;
        objEmailDetail.companyKey=strCompanyKey;
        
        NSError *error1;
        
        [context save:&error1];
        
    }
    [self salesEmailFetch];
    [_tblEmailData reloadData];
    
}

//Nilind 17 Oct
-(void)updateMailStatus:(NSManagedObject *)indexValue
{
    isEditedInSalesAuto=YES;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetail= [NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    [request setEntity:entityEmailDetail];
    
    NSError *error1 = nil;
    matches=indexValue;
    if (chkClickStatus==YES)
    {
        [matches setValue:@"true" forKey:@"isMailSent"];
    }
    else
    {
        [matches setValue:@"false" forKey:@"isMailSent"];
    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    [context save:&error1];
    [self salesEmailFetch];
}

//....................................
//....................................
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
//------------------------------------------

- (IBAction)actionOnInitialSetupDummy:(id)sender
{
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    InitialSetUp *objInitialSetUp=[storyBoard instantiateViewControllerWithIdentifier:@"InitialSetUp"];
    [self presentViewController:objInitialSetUp animated:YES completion:nil];
}
//Nilind 21 Oct

-(void)updateLeadIdDetail : (NSString*)strIsSendMail
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    isEditedInSalesAuto=YES;
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        matches=arrAllObj[0];
        
        [matches setValue:@"Complete" forKey:@"statusSysName"];
        [matches setValue:strIsSendMail forKey:@"isMailSend"];

        if ([_strForSendProposal isEqualToString:@"forSendProposal"])
        {
            [matches setValue:@"Open" forKey:@"statusSysName"];
            [matches setValue:@"Proposed" forKey:@"stageSysName"];
        }
        if([[NSString stringWithFormat:@"%@",[matches valueForKey:@"stageSysName"]] isEqualToString:@"Proposed"])
        {
            [matches setValue:@"Open" forKey:@"statusSysName"];
            [matches setValue:@"Proposed" forKey:@"stageSysName"];
        }
        // [matches setValue:@"InComplete" forKey:@"statusSysName"];
        [context save:&error1];
        
    }
}
//Nilind 24 Oct
-(void)fetchPrimarySeconddaryMail
{
    
    
    arrPrimarySecondaryMail=[[NSMutableArray alloc]init];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            [arrPrimarySecondaryMail addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingPrimaryEmail"]]];
            [arrPrimarySecondaryMail addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingSecondaryEmail"]]];
            [arrPrimarySecondaryMail addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"servicePrimaryEmail"]]];
            [arrPrimarySecondaryMail addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSecondaryEmail"]]];
            
        }
    }
}

//Nilind 24 Oct

-(void)savePrimaryEmailToCoreData:(NSString *)str
{
    
    //Email Detail Entity
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    EmailDetail *objEmailDetail = [[EmailDetail alloc]initWithEntity:entityEmailDetail insertIntoManagedObjectContext:context];
    objEmailDetail.createdBy=@"0";;
    
    objEmailDetail.createdDate=@"0";
    
    objEmailDetail.emailId=str;
    
    objEmailDetail.isCustomerEmail=@"true";
    
    objEmailDetail.isMailSent=@"true";
    
    objEmailDetail.leadMailId=@"";
    objEmailDetail.isDefaultEmail=@"false";

    objEmailDetail.modifiedBy=@"";
    
    objEmailDetail.modifiedDate=[global modifyDate];
    
    objEmailDetail.subject=@"";
    
    objEmailDetail.leadId=strLeadId;
    
    objEmailDetail.userName=strUserName;
    objEmailDetail.companyKey=strCompanyKey;
    
    NSError *error1;
    
    [context save:&error1];
    // [self salesEmailFetch];
}
//..................
#pragma mark-  Fetch Current Services
-(void)fetchCurrentServices
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityCurrentService=[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityCurrentService];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES                                                                                                                                                                                                                                                                                                                              ];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //    NSManagedObject *matchesNew;
    NSMutableArray *arrCurrentServiceValue;
    
    arrCurrentServiceValue=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrCurrentServiceValue forKey:@"CurrentService"];
    }else
    {
        NSMutableArray *arrCurrentService=[[NSMutableArray alloc]init];
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrCurrentServiceKey;
            arrCurrentServiceKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrCurrentServiceKey);
            
            NSLog(@"all keys %@",arrCurrentServiceValue);
            for (int i=0; i<arrCurrentServiceKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]];
                
                [arrCurrentServiceValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrCurrentServiceKey.count; k++) {
                
                NSString *strKeyLeadId=arrCurrentServiceKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrCurrentServiceKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrCurrentServiceKey=arrKeyTemp;
            [arrCurrentServiceValue removeObjectAtIndex:indexToRemove];
            
            [arrCurrentServiceValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictCurrentService;
            dictCurrentService = [NSDictionary dictionaryWithObjects:arrCurrentServiceValue forKeys:arrCurrentServiceKey];
            
            NSLog(@"%@",dictCurrentService);
            [arrCurrentService addObject:dictCurrentService];
            NSLog(@"EmailDetail%@",arrCurrentService);
        }
        [dictFinal setObject:arrCurrentService forKey:@"CurrentService"];
    }
}
#pragma mark-  ----------- Fetch Renewal Services ---------------

-(void)fetchRenewalServiceData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityRenewalServiceDetail= [NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
    [request setEntity:entityRenewalServiceDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrRenewalServiceDetail;
    arrRenewalServiceDetail=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrRenewalServiceDetail forKey:@"RenewalServiceExtDcs"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);

            
            NSArray *arrRenewalServiceKey;
            NSMutableArray *arrRenewalServiceValue;
            
            arrRenewalServiceValue=[[NSMutableArray alloc]init];
            arrRenewalServiceKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrRenewalServiceKey);
            
            NSLog(@"all keys %@",arrRenewalServiceValue);
            for (int i=0; i<arrRenewalServiceKey.count; i++)
            {
                //NSString *str;
                id str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrRenewalServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrRenewalServiceKey objectAtIndex:i]]];
                if([str isEqual:nil])// || str.length==0 )
                {
                    str=@"";
                }
                if([str isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                }
                
                [arrRenewalServiceValue addObject:str];
                
                /* NSString *str;
                 str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                 
                 BOOL isTrue=[global isStringNullNilBlank:str];
                 if (isTrue) {
                 str=@"";
                 }
                 
                 [arrPaymentInfoValue addObject:str];*/
            }
            
            
            NSDictionary *dictSoldServiceStandardDetail;
            dictSoldServiceStandardDetail = [NSDictionary dictionaryWithObjects:arrRenewalServiceValue forKeys:arrRenewalServiceKey];
            
            NSLog(@"%@",dictSoldServiceStandardDetail);
            [arrRenewalServiceDetail addObject:dictSoldServiceStandardDetail];
            NSLog(@"RenewalServiceExtDcs%@",arrRenewalServiceDetail);
        }
        [dictFinal setObject:arrRenewalServiceDetail forKey:@"RenewalServiceExtDcs"];
    }
}
#pragma mark-  Fetch SERVICE FOLLOWUP FOR SEND
-(void)fetchServiceFollowupForSend
{
    
    if (isServiceFollowUp==YES)
    {
        
        NSMutableArray *arrDict;
        arrDict=[[NSMutableArray alloc]init];
        for (int i=0; i<arrServiceFollowDept.count; i++)
        {
            NSArray *strArr= [dictMutableServiceFollow valueForKey:[arrServiceFollowDept objectAtIndex:i]];
            NSMutableArray *arrAllService;
            arrAllService=[[NSMutableArray alloc]init];
            for (int l=0; l<strArr.count; l++)
            {
                NSString *strVal=[NSString stringWithFormat:@"%@",[dictServiceName valueForKey:[strArr objectAtIndex:l]]];
                
                if (strVal.length==0 || [strVal isEqualToString:@""]||[strVal isEqual:NULL] ||[strVal isEqual:nil]||[strVal isEqualToString:@"(null)"]  )
                {
                    [arrAllService addObject:[strArr objectAtIndex:l]];
                }
                else
                {
                    [arrAllService addObject:strVal];
                }
            }
            
            
            NSArray *arrKeyService=[NSArray arrayWithObjects:
                                    @"leadId",
                                    @"departmentName",
                                    @"followupDate",
                                    @"id",
                                    @"notes",
                                    @"serviceId",
                                    @"serviceSysName",
                                    @"departmentSysName",
                                    @"departmentId",
                                    @"serviceName",nil];
            
            NSArray *arrValService=[NSArray arrayWithObjects:
                                    strLeadId,
                                    [arrServiceDeptNameFinalSend objectAtIndex:i],
                                    [arrServiceDateFinalSend objectAtIndex:i],
                                    @"",
                                    [arrServiceNotesFinalSend objectAtIndex:i],
                                    @"",
                                    @"",
                                    [arrServiceFollowDept objectAtIndex:i],
                                    @"",
                                    @"",nil];
            
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:arrValService forKeys:arrKeyService];
            [arrDict addObject:dict_ToSendLeadInfo];
        }
        [dictForFollowUp setObject:arrDict forKey:@"serviceFollowUpDcs"];
        [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
    }
    else
    {
        NSMutableArray *arrDict;
        arrDict=[[NSMutableArray alloc]init];
        [dictForFollowUp setObject:arrDict forKey:@"serviceFollowUpDcs"];
        [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
        
    }
    
    //DO NOT DELETE COMMENTED
    /* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     
     NSFetchRequest *request = [[NSFetchRequest alloc] init];
     
     entityServiceFollowUp=[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context];
     
     [request setEntity:entityServiceFollowUp];
     
     NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
     
     [request setPredicate:predicate];
     
     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
     sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
     
     [request setSortDescriptors:sortDescriptors];
     
     self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
     [self.fetchedResultsControllerSalesInfo setDelegate:self];
     
     
     // Perform Fetch
     NSError *error1 = nil;
     [self.fetchedResultsControllerSalesInfo performFetch:&error1];
     arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
     //    NSManagedObject *matchesNew;
     NSMutableArray *arrCurrentServiceValue;
     
     arrCurrentServiceValue=[[NSMutableArray alloc]init];
     
     if (arrAllObj.count==0)
     {
     //[dictFinal setObject:arrCurrentServiceValue forKey:@"serviceFollowUpDcs"];
     
     [dictForFollowUp setObject:arrCurrentServiceValue forKey:@"serviceFollowUpDcs"];
     // NSMutableArray *arr12;
     // arr12=[[NSMutableArray alloc]init];
     // [arr12 addObject:dictForFollowUp];
     [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
     
     }else
     {
     NSMutableArray *arrCurrentService=[[NSMutableArray alloc]init];
     for (int k=0; k<arrAllObj.count; k++)
     {
     matches=arrAllObj[k];
     NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
     
     NSArray *arrCurrentServiceKey;
     arrCurrentServiceKey = [[[matches entity] attributesByName] allKeys];
     NSLog(@"all keys %@",arrCurrentServiceKey);
     arrCurrentServiceValue=[[NSMutableArray alloc]init];
     NSLog(@"all keys %@",arrCurrentServiceValue);
     for (int i=0; i<arrCurrentServiceKey.count; i++)
     {
     NSString *str;
     str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]];
     
     [arrCurrentServiceValue addObject:str];
     }
     
     NSDictionary *dictCurrentService;
     dictCurrentService = [NSDictionary dictionaryWithObjects:arrCurrentServiceValue forKeys:arrCurrentServiceKey];
     
     NSLog(@"%@",dictCurrentService);
     [arrCurrentService addObject:dictCurrentService];
     NSLog(@"ServiceFollowup%@",arrCurrentService);
     }
     
     //[dictFinal setObject:arrCurrentService forKey:@"serviceFollowUpDcs"];
     
     [dictForFollowUp setObject:arrCurrentService forKey:@"serviceFollowUpDcs"];
     //NSMutableArray *arr12;
     // arr12=[[NSMutableArray alloc]init];
     //[arr12 addObject:dictForFollowUp];
     [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
     }*/
}
-(void)fetchProposalFollowupForSend
{
    if (isProposalFoolowUp==YES)
    {
        
        NSMutableArray *arrDict;
        arrDict=[[NSMutableArray alloc]init];
        for (int i=0; i<arrProposalSerivceName.count; i++)
        {
            
            NSString *str,*strlog;
            str=[arrProposalSerivceName objectAtIndex:i];
            strlog=[NSString stringWithFormat:@"Follow up for: %@",str];
            
            NSArray *arrKeyService=[NSArray arrayWithObjects:
                                    @"proposalServiceName",
                                    @"proposalServiceSysName",
                                    @"proposalId",
                                    @"proposalServiceId",
                                    @"proposalLeadId",
                                    @"proposalFollowupDate",
                                    @"proposalNotes",
                                    nil];
            NSArray *arrValService=[NSArray arrayWithObjects:
                                    [arrProposalServiceNameFinalSend objectAtIndex:i],
                                    [arrProposalServiceNameFinalSend objectAtIndex:i],
                                    @"",
                                    @"",
                                    strLeadId,
                                    [arrProposalDateFinalSend objectAtIndex:i],
                                    [arrProposalNotesFinalSend objectAtIndex:i],
                                    nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:arrValService forKeys:arrKeyService];
            [arrDict addObject:dict_ToSendLeadInfo];
        }
        [dictForFollowUp setObject:arrDict forKey:@"proposalFollowUpDcs"];
        [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
        
    }
    
    else
    {
        NSMutableArray *arrDict;
        arrDict=[[NSMutableArray alloc]init];
        [dictForFollowUp setObject:arrDict forKey:@"proposalFollowUpDcs"];
        [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
    }
    //DO NOT DELETE COMMENTED
    /* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     
     NSFetchRequest *request = [[NSFetchRequest alloc] init];
     
     entityProposalFollowUp=[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context];
     [request setEntity:entityProposalFollowUp];
     
     NSPredicate *predicate =[NSPredicate predicateWithFormat:@"proposalLeadId=%@",strLeadId];
     
     [request setPredicate:predicate];
     
     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"proposalLeadId" ascending:YES];
     sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
     
     [request setSortDescriptors:sortDescriptors];
     
     self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
     [self.fetchedResultsControllerSalesInfo setDelegate:self];
     
     
     // Perform Fetch
     NSError *error1 = nil;
     [self.fetchedResultsControllerSalesInfo performFetch:&error1];
     arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
     //    NSManagedObject *matchesNew;
     NSMutableArray *arrCurrentServiceValue;
     
     arrCurrentServiceValue=[[NSMutableArray alloc]init];
     
     if (arrAllObj.count==0)
     {
     //[dictFinal setObject:arrCurrentServiceValue forKey:@"serviceFollowUpDcs"];
     
     [dictForFollowUp setObject:arrCurrentServiceValue forKey:@"proposalFollowUpDcs"];
     //NSMutableArray *arr12;
     //arr12=[[NSMutableArray alloc]init];
     // [arr12 addObject:dictForFollowUp];
     [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
     
     }else
     {
     NSMutableArray *arrCurrentService=[[NSMutableArray alloc]init];
     for (int k=0; k<arrAllObj.count; k++)
     {
     matches=arrAllObj[k];
     NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"proposalLeadId"]);
     
     NSArray *arrCurrentServiceKey;
     arrCurrentServiceKey = [[[matches entity] attributesByName] allKeys];
     NSLog(@"all keys %@",arrCurrentServiceKey);
     arrCurrentServiceValue=[[NSMutableArray alloc]init];
     NSLog(@"all keys %@",arrCurrentServiceValue);
     for (int i=0; i<arrCurrentServiceKey.count; i++)
     {
     NSString *str;
     str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]];
     
     [arrCurrentServiceValue addObject:str];
     }
     
     NSDictionary *dictCurrentService;
     dictCurrentService = [NSDictionary dictionaryWithObjects:arrCurrentServiceValue forKeys:arrCurrentServiceKey];
     
     NSLog(@"%@",dictCurrentService);
     [arrCurrentService addObject:dictCurrentService];
     NSLog(@"ProposalFollowup%@",arrCurrentService);
     }
     
     //[dictFinal setObject:arrCurrentService forKey:@"serviceFollowUpDcs"];
     
     [dictForFollowUp setObject:arrCurrentService forKey:@"proposalFollowUpDcs"];
     //NSMutableArray *arr12;
     //arr12=[[NSMutableArray alloc]init];
     //[arr12 addObject:dictForFollowUp];
     [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
     }*/
}
//Nilind 27 Dec
-(void)syncCall
{
    // [self addServiceProposalFollow];
    /* NSUserDefaults *defsLogin=[NSUserDefaults standardUserDefaults];
     NSDictionary *dictLogin=[defsLogin valueForKey:@"LoginDetails"];
     
     strCompanyKey =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.CompanyKey"]];
     strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLogin valueForKey:@"EmployeeNumber"]];
     strUserName       =[NSString stringWithFormat:@"%@",[dictLogin valueForKeyPath:@"Company.Username"]];
     
     NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
     NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
     strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];*/
    
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [DejalBezelActivityView removeView];
        
        [self saveDocumentToBeSend];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Data saved offline" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        if ([_strFromWdo isEqualToString:@"yes"]) {
            
            NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
            [defsBack setBool:false forKey:@"isFromBackSendMailWdo"];
            [defsBack setBool:true forKey:@"offlineWdoSaved"];
            [defsBack synchronize];
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[Agreement_WDOVC class]]) {
                    index=k1;
                }
            }
            
            Agreement_WDOVC *myController = (Agreement_WDOVC *)[self.navigationController.viewControllers objectAtIndex:index];
            [self.navigationController popToViewController:myController animated:NO];
            
        }else if ([_strFromWdo isEqualToString:@"NPMA"]) {
            
            NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
            [defsBack setBool:false forKey:@"isFromBackSendMailNPMA"];
            [defsBack setBool:true forKey:@"offlineNPMASaved"];
            [defsBack synchronize];
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[NPMA_Agreement_iPadVC class]]) {
                    index=k1;
                }
            }
            
            NPMA_Agreement_iPadVC *myController = (NPMA_Agreement_iPadVC *)[self.navigationController.viewControllers objectAtIndex:index];
            [self.navigationController popToViewController:myController animated:NO];
            
        } else {
            
            NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
            
            NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
            
            if ([strAppointmentFlow isEqualToString:@"New"])
            {
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
                AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
                
            }
            else
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
                AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
            
        }
        
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
//        AppointmentViewiPad*objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
//        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    else
    {
        
        [self FetchFromCoreDataToSendSalesDynamic];
        
    }
    
}

//...............

//Nilind 04 Jan

-(NSString *)fetchLead
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSString *str;
    if (arrAllObj12.count==0)
    {
        
    }
    else
    {
        
        NSManagedObject *matches12=arrAllObj12[0];
        
        str=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"isInitialSetupCreated"]];//isInitialSetupCreated
        
    }
    return str;
    
}

//============================================================================
//============================================================================
#pragma mark- Upload Audio METHOD
//============================================================================
//============================================================================


-(void)uploadAudio :(NSString*)strAudioName
{
    
    if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
    {
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    }
    
    
    NSRange equalRange = [strAudioName rangeOfString:@"\\" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        strAudioName = [strAudioName substringFromIndex:equalRange.location + equalRange.length];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    NSData *audioData;
    
    audioData = [NSData dataWithContentsOfFile:path];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlAudioUploadAsync];
    
    // setting up the request object now
    NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
    [request1 setURL:[NSURL URLWithString:urlString]];
    [request1 setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strAudioName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:audioData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request1 setHTTPBody:body];
    
    // now lets make the connection to the web
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Audio Sent");
    }
    
    [self fialJson];
    
    NSLog(@"Audio Sent Return Message is = = = %@",returnString);
}

-(void)salesEmailCountToSend
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrNew;
    arrNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesNew;
    
    arrSendEmailCount=[[NSMutableArray alloc]init];
    if (arrNew.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrNew.count; i++)
        {
            matchesNew=arrNew[i];
            if([[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"isMailSent"]]isEqualToString:@"true"] )
            {
                // [arrSendEmailCount addObject:@"%@",[NSString stringWithFormat:@"%d",i]];
                [arrSendEmailCount addObject:[NSString  stringWithFormat:@"%d",i] ];
            }
            
        }
        
    }
    
}
-(void)addServiceProposalFollow
{
    
    //Nilind 20 Feb
    
    [self serviceName];
    //Nilind 18 Feb
    NSUserDefaults *defsCompanu=[NSUserDefaults standardUserDefaults];
    dictCompanyDetail=[defsCompanu valueForKey:@"companyDetail"];
    isProposalFoolowUp=[[dictCompanyDetail valueForKey:@"IsProposalFollowUp"]boolValue];
    isServiceFollowUp=[[dictCompanyDetail valueForKey:@"IsServiceFollowUp"]boolValue];
    serviceFollowDays=[[dictCompanyDetail valueForKey:@"ServiceFollowUpDays"]intValue];
    proposalFollowDays=[[dictCompanyDetail valueForKey:@"ProposalFollowUpDays"]intValue];
    

    //End
    
    
    [self masterFetch];
    NSLog(@"SCROLL HEIGHT %f",_const_ScrollView_H.constant);
    //isServiceFollowUp=YES;isProposalFoolowUp=YES;
    if (isServiceFollowUp ==YES)
    {
        _tblServiceFollowUp.hidden=NO;
        _lblServiceFollowUp.hidden=NO;
        _const_tableService_H.constant=_const_tableService_H.constant*[arrServiceFollowDept count];
        
    }
    else
    {
        _tblServiceFollowUp.hidden=YES;
        _lblServiceFollowUp.hidden=YES;
        _const_tableService_H.constant=_const_tableService_H.constant*0;
        
        
    }
    if (isProposalFoolowUp==YES)
    {
        _tblProposalFollowUp.hidden=NO;
        _lblProposalFollowUp.hidden=NO;
        _const_tableProposal_H.constant=_const_tableProposal_H.constant*[arrProposalFollowDept count];
        
    }
    else
    {
        _tblProposalFollowUp.hidden=YES;
        _lblProposalFollowUp.hidden=YES;
        _const_tableProposal_H.constant=_const_tableProposal_H.constant*0;
        
    }
    //temp
    
    //end
    
    _const_tableProposal_H.constant=130*[arrProposalFollowDept count];//_const_tableProposal_H.constant
    _const_tableService_H.constant=130*[arrServiceFollowDept count];//_const_tableService_H.constant
    
    _viewForFollowUp.frame=CGRectMake(_viewSendMail.frame.origin.x  ,CGRectGetMaxY(_viewSendMail.frame)+30,[UIScreen mainScreen].bounds.size.width-_viewSendMail.frame.origin.x*2,_const_tableService_H.constant+_const_tableProposal_H.constant+200);
    
    [__scrollViewSendMail addSubview:_viewForFollowUp];
    
    [_tblProposalFollowUp reloadData];
    [_tblServiceFollowUp reloadData];
    
    [__scrollViewSendMail setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewForFollowUp.frame.size.height+_viewForFollowUp.frame.origin.y)];
}
//Nilind 13 Feb


-(void)fetchFromCoreDataStandardNew
{
    //Master Service
    
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    NSArray *arrServiceMaster;
    NSMutableArray *arrSysServiceVal,*arrSysMasterKey;
    arrSysServiceVal=[[NSMutableArray alloc]init];
    arrSysMasterKey=[[NSMutableArray alloc]init];
    
    if ([[dictSalesLeadMaster valueForKey:@"ServiceMasters"] isKindOfClass:[NSArray class]])
    {
        arrServiceMaster=[dictSalesLeadMaster valueForKey:@"ServiceMasters"];
    }
    
    NSDictionary *dictMasterSerivce;
    dictMasterSerivce=[[NSDictionary alloc]init];
        
    for (int i=0; i<arrServiceMaster.count; i++)
    {
        NSDictionary *dict=[arrServiceMaster objectAtIndex:i];
        [arrSysServiceVal addObject:[dict valueForKey:@"DepartmentSysName"]];
        [arrSysMasterKey addObject:[dict valueForKey:@"ServiceSysName"]];
    }
    // [dictMasterSerivce setObject:arrSysServiceVal forKey:arrSysMasterKey];
    dictMasterSerivce=[NSDictionary dictionaryWithObjects:arrSysServiceVal forKeys:arrSysMasterKey];
    NSLog(@"IN SERVICE MASTER %@",dictMasterSerivce);
    
    //End
    
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrSoldService=[[NSMutableArray alloc]init];
    arrSolDeptName=[[NSMutableArray alloc]init];
    arrUnSoldService=[[NSMutableArray alloc]init];
    arrUnSolDeptName=[[NSMutableArray alloc]init];
    
    NSArray *arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    
    NSManagedObject *matchesNew;
    
    if (arrAllObjNew.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjNew.count; k++)
        {
            matchesNew=arrAllObjNew[k];
            NSLog(@"Lead IDDDD====%@",[matchesNew valueForKey:@"leadId"]);
            
            
            
            if([[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"isSold"]]isEqualToString:@"true"])
            {
                
                [arrSoldService addObject:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceSysName"]]];
                NSString *str=[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceSysName"]];
                [arrSolDeptName addObject:[NSString stringWithFormat:@"%@",[ dictMasterSerivce valueForKey:str]]];
            }
            else
            {
                [arrUnSoldService addObject:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceSysName"]]];
                NSString *str=[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceSysName"]];
                [arrUnSolDeptName addObject:[NSString stringWithFormat:@"%@",[dictMasterSerivce valueForKey:str]]];
            }
            
        }
        [self fetchFromCoreDataNonStandardNew];
    }
}

-(void)fetchFromCoreDataNonStandardNew
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjNew1 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSManagedObject *matchesNew1;
    
    if (arrAllObjNew1.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjNew1.count; k++)
        {
            matchesNew1=arrAllObjNew1[k];
            NSLog(@"Lead IDDDD====%@",[matchesNew1 valueForKey:@"leadId"]);
            
            if([[NSString stringWithFormat:@"%@",[matchesNew1 valueForKey:@"isSold"]]isEqualToString:@"true"])
            {
                [arrSoldService addObject:[NSString stringWithFormat:@"%@",[matchesNew1 valueForKey:@"serviceName"]]];
                [arrSolDeptName addObject:[NSString stringWithFormat:@"%@",[matchesNew1 valueForKey:@"departmentSysname"]]];
            }
            else
            {
                [arrUnSoldService addObject:[NSString stringWithFormat:@"%@",[matchesNew1 valueForKey:@"serviceName"]]];
                [arrUnSolDeptName addObject:[NSString stringWithFormat:@"%@",[matchesNew1 valueForKey:@"departmentSysname"]]];
                
            }
            
        }
    }
    //dictSoldService=[NSDictionary dictionaryWithObjects:arrSoldService forKeys:arrSolDeptName];
    // dictUnSoldService=[NSDictionary dictionaryWithObjects:arrUnSoldService forKeys:arrUnSolDeptName];
    dictSoldService=[NSDictionary dictionaryWithObjects:arrSolDeptName forKeys:arrSoldService];
    dictUnSoldService=[NSDictionary dictionaryWithObjects:arrUnSolDeptName forKeys:arrUnSoldService];
    NSLog(@"dictSoldService>>%@\ndictUnSoldService>>%@",dictSoldService,dictUnSoldService);
}

//End


-(void)masterFetch
{
    
    [self fetchFromCoreDataStandardNew];
    
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    
    
    //Branch Master Fetch
    
    NSArray *arrBranchMaster;
    arrBranchMaster=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    
    NSMutableArray *arrDeptVal,*arrDeptKey;
    arrDeptVal=[[NSMutableArray alloc]init];
    arrDeptKey=[[NSMutableArray alloc]init];
    
    NSDictionary *dictMasterKeyValue;
    for (int i=0; i<arrBranchMaster.count; i++)
    {
        NSDictionary *dict=[arrBranchMaster objectAtIndex:i];
        NSArray *arrDepartments=[dict valueForKey:@"Departments"];
        for (int k=0; k<arrDepartments.count; k++)
        {
            NSDictionary *dict1=[arrDepartments objectAtIndex:k];
            [arrDeptVal addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"Name"]]];
            [arrDeptKey addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"SysName"]]];
        }
        
    }
    dictMasterKeyValue=[NSDictionary dictionaryWithObjects:arrDeptVal forKeys:arrDeptKey];
    dictDeptNameByKey=dictMasterKeyValue;
    NSLog(@"dictMasterKeyValue>>%@",dictMasterKeyValue);
    
    //End
    
    //Service Master Fetch
    
    NSArray *arrServiceMaster;
    NSMutableArray *arrSysServiceVal,*arrSysMasterKey;
    arrSysServiceVal=[[NSMutableArray alloc]init];
    arrSysMasterKey=[[NSMutableArray alloc]init];
    
    //arrServiceMaster=[dictSalesLeadMaster valueForKey:@"ServiceMasters"];
    
    if ([[dictSalesLeadMaster valueForKey:@"ServiceMasters"] isKindOfClass:[NSArray class]])
    {
        arrServiceMaster=[dictSalesLeadMaster valueForKey:@"ServiceMasters"];
    }
    
    
    NSMutableDictionary *dictMasterSerivce;
    dictMasterSerivce=[[NSMutableDictionary alloc]init];
    
    for (int j=0; j<arrDeptKey.count; j++)
    {
        
        arrSysServiceVal=[[NSMutableArray alloc]init];
        arrSysMasterKey=[[NSMutableArray alloc]init];
        for (int i=0; i<arrServiceMaster.count; i++)
        {
            NSDictionary *dict=[arrServiceMaster objectAtIndex:i];
            if ([[dict valueForKey:@"DepartmentSysName"] isEqualToString:[arrDeptKey objectAtIndex:j]])
            {
                // [arrSysMasterKey addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DepartmentSysName"]]];
                [arrSysServiceVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceSysName"]]];
            }
        }
        [dictMasterSerivce setObject:arrSysServiceVal forKey:[arrDeptKey objectAtIndex:j]];
    }
    
    NSLog(@"dictMasterSerivce>>%@",dictMasterSerivce);
    
    //End
    
    
    //For Table Access Array
    NSLog(@"arrSoldService>>%@ \n arrUnSoldService>>%@",arrSoldService,arrUnSoldService
          );
    NSMutableArray *arrDeptName;
    arrDeptName=[[NSMutableArray alloc]init];
    for (int i=0; i<arrSoldService.count; i++)
    {
        /* for (int j=0; condition; <#increment#>) {
         statements
         }*/
        NSString *str=[NSString stringWithFormat:@"%@",[arrSoldService objectAtIndex:i]];
        [arrDeptName addObject:[NSString stringWithFormat:@"%@",[dictSoldService valueForKey:str]]];
    }
    NSArray *uniqueArray = [[NSSet setWithArray:arrDeptName] allObjects];
    NSLog(@"uniqueArray>>%@",uniqueArray);
    
    NSMutableArray *arrServiceName;
    NSMutableDictionary *dictMutable;
    dictMutable=[[NSMutableDictionary alloc]init];
    for (int i=0; i<uniqueArray.count; i++)
    {
        arrServiceName=[[NSMutableArray alloc]init];
        for (int j=0; j<arrSoldService.count; j++)
        {
            if ([[dictSoldService valueForKey:[arrSoldService objectAtIndex:j]] isEqualToString:[uniqueArray objectAtIndex:i]])
            {
                [arrServiceName addObject:[arrSoldService objectAtIndex:j]];
            }
            [dictMutable setObject:arrServiceName forKey:[uniqueArray objectAtIndex:i]];
        }
        NSLog(@"dictmutable%@",dictMutable);
        
        
    }
    //For Service Follow
    dictMutableServiceFollow=dictMutable;
    arrServiceFollowDept=[dictMutableServiceFollow allKeys];
    //End
    
    //For Proposal Follow
    arrProposalFollowDept= arrUnSolDeptName;
    arrProposalSerivceName=arrUnSoldService;
    //End
}
-(void)serviceMaster
{
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    NSArray *arrServiceMaster;
    NSMutableArray *arrSysServiceVal,*arrSysMasterKey;
    arrSysServiceVal=[[NSMutableArray alloc]init];
    arrSysMasterKey=[[NSMutableArray alloc]init];
    
   // arrServiceMaster=[dictSalesLeadMaster valueForKey:@"ServiceMasters"];
    
    if ([[dictSalesLeadMaster valueForKey:@"ServiceMasters"] isKindOfClass:[NSArray class]])
    {
        arrServiceMaster=[dictSalesLeadMaster valueForKey:@"ServiceMasters"];
    }
    
    
    NSDictionary *dictMasterSerivce;
    dictMasterSerivce=[[NSDictionary alloc]init];
    for (int i=0; i<arrServiceMaster.count; i++)
    {
        NSDictionary *dict=[arrServiceMaster objectAtIndex:i];
        [arrSysServiceVal addObject:[dict valueForKey:@"DepartmentSysName"]];
        [arrSysMasterKey addObject:[dict valueForKey:@"ServiceSysName"]];
    }
    // [dictMasterSerivce setObject:arrSysServiceVal forKey:arrSysMasterKey];
    dictMasterSerivce=[NSDictionary dictionaryWithObjects:arrSysServiceVal forKeys:arrSysMasterKey];
    NSLog(@"IN SERVICE MASTER %@",dictMasterSerivce);
}
//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate: [NSDate date]];
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
-(void)tapDetectedOnBackGroundView
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDate = [dateFormat stringFromDate:pickerDate.date];
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tagValue inSection:0];
    
    if ([strTable isEqualToString:@"service"])
    {
        ServiceFollowUpTableViewCell *tappedCell = (ServiceFollowUpTableViewCell *)[_tblServiceFollowUp cellForRowAtIndexPath:indexpath];
        [tappedCell.btnFollowUp setTitle:strDate forState:UIControlStateNormal];
        
    }
    else
    {
        // NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tagValue inSection:0];
        
        ProposalFollowUpTableViewCell *tappedCell = (ProposalFollowUpTableViewCell *)[_tblProposalFollowUp cellForRowAtIndexPath:indexpath];
        [tappedCell.btnProposalFollowUp setTitle:strDate forState:UIControlStateNormal];
    }
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
    
}

-(void)fetchDataTable
{
    
}

- (IBAction)actionOnFollowUp:(id)sender
{
    [self finalUpdatedTableData];
    
}
-(void)finalUpdatedTableData
{
    
    if (isServiceFollowUp==YES)
    {
        arrServiceDeptNameFinalSend=[[NSMutableArray alloc]init];
        arrServiceNotesFinalSend=[[NSMutableArray alloc]init];
        arrServiceDateFinalSend=[[NSMutableArray alloc]init];
        for (int i=0; i<arrServiceFollowDept.count; i++)
        {
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
            ServiceFollowUpTableViewCell *tappedCell = (ServiceFollowUpTableViewCell *)[_tblServiceFollowUp cellForRowAtIndexPath:indexpath];
            NSLog(@"For row %d %@",i, tappedCell.btnFollowUp.titleLabel.text);
            NSLog(@"For row %d %@",i,tappedCell.txtViewService.text);
            
            NSString *str1=tappedCell.btnFollowUp.titleLabel.text;
            NSString *str2=tappedCell.txtViewService.text;
            NSString *str3=tappedCell.lblDeptName.text;
            
            
            if([str1 isEqualToString:@"(null)"]||[str1 isEqual:nil]||str1.length==0)
            {
                str1=@"";
            }
            if([str2 isEqualToString:@"(null)"]||[str2 isEqual:nil]||str2.length==0)
            {
                str2=@"";
            }
            if([str3 isEqualToString:@"(null)"]||[str3 isEqual:nil]||str3.length==0)
            {
                str3=@"";
            }
            
            [arrServiceDeptNameFinalSend addObject:str3];
            [arrServiceNotesFinalSend addObject:str2];
            [arrServiceDateFinalSend addObject:str1 ];
        }
        /*{
         NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
         ServiceFollowUpTableViewCell *tappedCell = (ServiceFollowUpTableViewCell *)[_tblServiceFollowUp cellForRowAtIndexPath:indexpath];
         NSLog(@"For row %d %@",i, tappedCell.btnFollowUp.titleLabel.text);
         NSLog(@"For row %d %@",i,tappedCell.txtViewService.text);
         
         [arrServiceDeptNameFinalSend addObject:tappedCell.lblDeptName.text];
         [arrServiceNotesFinalSend addObject:tappedCell.txtViewService.text];
         [arrServiceDateFinalSend addObject:tappedCell.btnFollowUp.titleLabel.text ];
         
         }*/
    }
    if (isProposalFoolowUp==YES)
    {
        arrProposalServiceNameFinalSend=[[NSMutableArray alloc]init];
        arrProposalNotesFinalSend=[[NSMutableArray alloc]init];
        arrProposalDateFinalSend=[[NSMutableArray alloc]init];
        
        for (int i=0; i<arrProposalFollowDept.count; i++)
        {
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
            ProposalFollowUpTableViewCell *tappedCell = (ProposalFollowUpTableViewCell *)[_tblProposalFollowUp cellForRowAtIndexPath:indexpath];
            NSLog(@"For row %d %@",i, tappedCell.btnProposalFollowUp.titleLabel.text);
            NSLog(@"For row %d %@",i,tappedCell.txtViewProposal.text);
            
            
            NSString *str1,*str2,*str3;
            str1=tappedCell.btnProposalFollowUp.titleLabel.text;
            str2=tappedCell.txtViewProposal.text;
            str3=tappedCell.lblServiceProposal.text;
            
            if([str1 isEqualToString:@"(null)"]||[str1 isEqual:nil]||str1.length==0)
            {
                str1=@"";
            }
            if([str2 isEqualToString:@"(null)"]||[str2 isEqual:nil]||str2.length==0)
            {
                str2=@"";
            }
            if([str3 isEqualToString:@"(null)"]||[str3 isEqual:nil]||str3.length==0)
            {
                str3=@"";
            }
            [arrProposalServiceNameFinalSend addObject:str3];
            [arrProposalNotesFinalSend addObject:str2];
            [arrProposalDateFinalSend addObject:str1 ];
        }
        /*{
         NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
         ProposalFollowUpTableViewCell *tappedCell = (ProposalFollowUpTableViewCell *)[_tblProposalFollowUp cellForRowAtIndexPath:indexpath];
         NSLog(@"For row %d %@",i, tappedCell.btnProposalFollowUp.titleLabel.text);
         NSLog(@"For row %d %@",i,tappedCell.txtViewProposal.text);
         [arrProposalServiceNameFinalSend addObject:tappedCell.lblServiceProposal.text];
         [arrProposalNotesFinalSend addObject:tappedCell.txtViewProposal.text];
         [arrProposalDateFinalSend addObject:tappedCell.btnProposalFollowUp.titleLabel.text ];
         
         }*/
        
    }
    
    /*
     
     arrServiceDeptNameFinalSend=[[NSMutableArray alloc]init];
     arrServiceNotesFinalSend=[[NSMutableArray alloc]init];
     arrServiceDateFinalSend=[[NSMutableArray alloc]init];
     
     arrProposalServiceNameFinalSend=[[NSMutableArray alloc]init];
     arrProposalNotesFinalSend=[[NSMutableArray alloc]init];
     arrProposalDateFinalSend=[[NSMutableArray alloc]init];
     for (int i=0; i<arrServiceFollowDept.count; i++)
     {
     NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
     ServiceFollowUpTableViewCell *tappedCell = (ServiceFollowUpTableViewCell *)[_tblServiceFollowUp cellForRowAtIndexPath:indexpath];
     NSLog(@"For row %d %@",i, tappedCell.btnFollowUp.titleLabel.text);
     NSLog(@"For row %d %@",i,tappedCell.txtViewService.text);
     
     [arrServiceDeptNameFinalSend addObject:tappedCell.lblDeptName.text];
     [arrServiceNotesFinalSend addObject:tappedCell.txtViewService.text];
     [arrServiceDateFinalSend addObject:tappedCell.btnFollowUp.titleLabel.text ];
     
     }
     for (int i=0; i<arrProposalFollowDept.count; i++)
     {
     NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
     ProposalFollowUpTableViewCell *tappedCell = (ProposalFollowUpTableViewCell *)[_tblProposalFollowUp cellForRowAtIndexPath:indexpath];
     NSLog(@"For row %d %@",i, tappedCell.btnProposalFollowUp.titleLabel.text);
     NSLog(@"For row %d %@",i,tappedCell.txtViewProposal.text);
     [arrProposalServiceNameFinalSend addObject:tappedCell.lblServiceProposal.text];
     [arrProposalNotesFinalSend addObject:tappedCell.txtViewProposal.text];
     [arrProposalDateFinalSend addObject:tappedCell.btnProposalFollowUp.titleLabel.text ];
     
     }
     */
}
#pragma mark- SAVE TO CORE DATA FOLLOW UP
-(void)fetchFromServiceFollowUp
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    entityServiceFollowUp=[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context];
    
    [request setEntity:entityServiceFollowUp];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjServiceFollowUp = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesServiceFollow;
    
    
    arrForServiceFollowName=[[NSMutableArray alloc]init];
    arrServiceDepartmentName=[[NSMutableArray alloc]init];
    arrServiceFollowDate=[[NSMutableArray alloc]init];
    
    
    if (arrAllObjServiceFollowUp.count==0)
    {
        chkForServiceFollow=YES;
    }
    else
    {
        chkForServiceFollow=NO;
        for (int k=0; k<arrAllObjServiceFollowUp.count; k++)
        {
            matchesServiceFollow=arrAllObjServiceFollowUp[k];
            NSLog(@"Lead IDDDD====%@",[matchesServiceFollow valueForKey:@"leadId"]);
            [arrForServiceFollowName addObject:[matchesServiceFollow valueForKey:@"notes"]];
            [arrServiceDepartmentName addObject:[matchesServiceFollow valueForKey:@"departmentName"]];
            [arrServiceFollowDate addObject:[NSString stringWithFormat:@"%@",[matchesServiceFollow valueForKey:@"followupDate"]]];
            
        }
    }
    
}
-(void)fetchFromProposalFollowUp
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    entityProposalFollowUp=[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context];
    [request setEntity:entityProposalFollowUp];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"proposalLeadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"proposalLeadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjProposalFollowUp = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSManagedObject *matchesProposalFollow;
    
    arrForProposalService=[[NSMutableArray alloc]init];
    arrForProposalNotes=[[NSMutableArray alloc]init];
    arrForProposalFollowUpDate=[[NSMutableArray alloc]init];
    
    
    if (arrAllObjProposalFollowUp.count==0)
    {
        chkForProposalFollow=YES;
    }
    else
    {
        matchesProposalFollow=[arrAllObjProposalFollowUp objectAtIndex:0];
        chkForProposalFollow=NO;
        
        for (int k=0; k<arrAllObjProposalFollowUp.count; k++)
        {
            matchesProposalFollow=[arrAllObjProposalFollowUp objectAtIndex:k];
            [arrForProposalService addObject:[matchesProposalFollow valueForKey:@"proposalServiceName"]];
            [arrForProposalNotes addObject:[matchesProposalFollow valueForKey:@"proposalNotes"]];
            [arrForProposalFollowUpDate addObject:[NSString stringWithFormat:@"%@",[matchesProposalFollow valueForKey:@"proposalFollowupDate"]]];
        }
    }
}
-(void)saveToCoreDataServiceFollowUp
{
    
    if (isServiceFollowUp==YES)
    {
        
        for (int i=0; i<arrServiceFollowDept.count; i++)
        {
            
            entityServiceFollowUp=[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context];
            ServiceFollowUpDcs *objentityServiceFollowUp = [[ServiceFollowUpDcs alloc]initWithEntity:entityServiceFollowUp insertIntoManagedObjectContext:context];
            objentityServiceFollowUp.leadId=strLeadId;
            
            objentityServiceFollowUp.departmentId=@"";
            
            objentityServiceFollowUp.departmentName=[dictDeptNameByKey valueForKey:[arrServiceFollowDept objectAtIndex:i]];
            objentityServiceFollowUp.departmentSysName=[arrServiceFollowDept objectAtIndex:i];
            
            
            objentityServiceFollowUp.followupDate=[arrServiceDateFinalSend objectAtIndex:i];
            
            objentityServiceFollowUp.id=@"";
            
            
            NSArray *strArr= [dictMutableServiceFollow valueForKey:[arrServiceFollowDept objectAtIndex:i]];
            NSMutableArray *arrAllService;
            arrAllService=[[NSMutableArray alloc]init];
            for (int l=0; l<strArr.count; l++)
            {
                NSString *strVal=[NSString stringWithFormat:@"%@",[dictServiceName valueForKey:[strArr objectAtIndex:l]]];
                
                if (strVal.length==0 || [strVal isEqualToString:@""]||[strVal isEqual:NULL] ||[strVal isEqual:nil]||[strVal isEqualToString:@"(null)"]  )
                {
                    [arrAllService addObject:[strArr objectAtIndex:l]];
                }
                else
                {
                    [arrAllService addObject:strVal];
                }
            }
            NSString *joinedComponents = [arrAllService componentsJoinedByString:@","];
            NSString *strLog=[NSString stringWithFormat:@"Follow up for: %@",joinedComponents];
            objentityServiceFollowUp.notes=strLog;
            
            objentityServiceFollowUp.serviceId=@"0";
            
            
            objentityServiceFollowUp.serviceName=@"";
            
            objentityServiceFollowUp.serviceSysName=@"";
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    // NSError *error2;
    // [context save:&error2];
}
-(void)saveToCoreDataProposalFollowUp
{
    
    //    //For Proposal Follow
    //    arrProposalFollowDept= arrUnSolDeptName;
    //    arrProposalSerivceName=arrUnSoldService;
    //    //End
    
    if (isProposalFoolowUp==YES)
    {
        
        for (int i=0; i<arrProposalSerivceName.count; i++)
        {
            entityProposalFollowUp=[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context];
            ProposalFollowUpDcs *objentityProposalFollowUp = [[ProposalFollowUpDcs alloc]initWithEntity:entityProposalFollowUp insertIntoManagedObjectContext:context];
            objentityProposalFollowUp.proposalLeadId=strLeadId;
            
            objentityProposalFollowUp.proposalFollowupDate=[arrProposalDateFinalSend objectAtIndex:i];
            
            
            
            objentityProposalFollowUp.proposalId=@"";
            
            NSString *str;
            
            str=[arrProposalSerivceName objectAtIndex:i];
            //Nilind 20 Feb
            
            NSString *strVal=[NSString stringWithFormat:@"%@",[dictServiceName valueForKey:[arrProposalSerivceName objectAtIndex:i]]];
            
            if (strVal.length==0 || [strVal isEqualToString:@""]||[strVal isEqual:NULL] ||[strVal isEqual:nil]||[strVal isEqualToString:@"(null)"]  )
            {
                objentityProposalFollowUp.proposalNotes=[NSString stringWithFormat:@"Follow up for: %@",str];
                objentityProposalFollowUp.proposalServiceName=[arrProposalSerivceName objectAtIndex:i];
                objentityProposalFollowUp.proposalServiceSysName=[arrProposalSerivceName objectAtIndex:i];
            }
            else
            {
                objentityProposalFollowUp.proposalNotes=[NSString stringWithFormat:@"Follow up for: %@",strVal];
                objentityProposalFollowUp.proposalServiceName=strVal;
                objentityProposalFollowUp.proposalServiceSysName=[arrProposalSerivceName objectAtIndex:i];
            }
            //End
            
            //  objentityProposalFollowUp.proposalNotes=[NSString stringWithFormat:@"Follow up for: %@",str];
            
            objentityProposalFollowUp.proposalServiceId=@"";
            
            //objentityProposalFollowUp.proposalServiceName=[arrProposalSerivceName objectAtIndex:i];
            //objentityProposalFollowUp.proposalServiceSysName=[arrProposalSerivceName objectAtIndex:i];
            
            NSError *error1;
            
            [context save:&error1];
        }
    }
    //NSError *error2;
    // [context save:&error2];
}
-(void)serviceName
{
    NSMutableArray *name,*sysName;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                
            }
        }
    }
    
    dictServiceName = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
    NSLog(@"PaymentInfo%@",dictServiceName);
    
    
}

-(NSString*)increaseDateFoService
{
    NSDate *today=[NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateComponents *components=[[NSDateComponents alloc] init];
    components.day=serviceFollowDays;
    NSDate *targetDate =[calendar dateByAddingComponents:components toDate:today options: 0];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:targetDate];
    NSLog(@"%@",finalTime);
    return finalTime;
}
-(NSString *)increaseDateForProposal
{
    NSDate *today=[NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateComponents *components=[[NSDateComponents alloc] init];
    components.day=proposalFollowDays;
    NSDate *targetDate =[calendar dateByAddingComponents:components toDate:today options: 0];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:targetDate];
    NSLog(@"%@",finalTime);
    return finalTime;
}
-(void)deleteFromCoreDataFollowUp
{
    //Nilind 16 Feb
    
    //Delete ServiceFollowUp Data
    
    if (isServiceFollowUp==YES)
    {
        
        
        entityServiceFollowUp=[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context];
        
        NSFetchRequest *allDataServiceFollowUpDcs = [[NSFetchRequest alloc] init];
        [allDataServiceFollowUpDcs setEntity:[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context]];
        
        NSPredicate *predicateServiceFollowUpDcs =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
        
        [allDataServiceFollowUpDcs setPredicate:predicateServiceFollowUpDcs];
        [allDataServiceFollowUpDcs setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorServiceFollowUpDcs= nil;
        NSArray * DataServiceFollowUpDcs = [context executeFetchRequest:allDataServiceFollowUpDcs error:&errorServiceFollowUpDcs];
        //error handling goes here
        for (NSManagedObject * data in DataServiceFollowUpDcs) {
            [context deleteObject:data];
        }
        NSError *saveErrorServiceFollowUpDcs = nil;
        [context save:&saveErrorServiceFollowUpDcs];
    }
    //......................................................
    //Nilind 16 Feb
    
    //Delete ProposalFollowUp Data
    if (isProposalFoolowUp==YES)
    {
        entityProposalFollowUp=[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context];
        
        NSFetchRequest *allDataProposalFollowUpDcs = [[NSFetchRequest alloc] init];
        [allDataProposalFollowUpDcs setEntity:[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context]];
        
        NSPredicate *predicateProposalFollowUpDcs =[NSPredicate predicateWithFormat:@"proposalLeadId = %@",strLeadId];
        [allDataProposalFollowUpDcs setPredicate:predicateProposalFollowUpDcs];
        [allDataProposalFollowUpDcs setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorProposalFollowUpDcs= nil;
        NSArray * DataProposalFollowUpDcs = [context executeFetchRequest:allDataProposalFollowUpDcs error:&errorProposalFollowUpDcs];
        //error handling goes here
        for (NSManagedObject * data in DataProposalFollowUpDcs) {
            [context deleteObject:data];
        }
        NSError *saveErrorProposalFollowUpDcs = nil;
        [context save:&saveErrorProposalFollowUpDcs];
    }
    //......................................................
    
    //End
}
-(void)fetchServiceFollowUpForSyncCall
{
    if (isServiceFollowUp==YES)
    {
        
        //DO NOT DELETE COMMENTED
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        
        entityServiceFollowUp=[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context];
        
        [request setEntity:entityServiceFollowUp];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
        
        [request setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [request setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        //    NSManagedObject *matchesNew;
        NSMutableArray *arrCurrentServiceValue;
        
        arrCurrentServiceValue=[[NSMutableArray alloc]init];
        
        if (arrAllObj.count==0)
        {
            //[dictFinal setObject:arrCurrentServiceValue forKey:@"serviceFollowUpDcs"];
            
            [dictForFollowUp setObject:arrCurrentServiceValue forKey:@"serviceFollowUpDcs"];
            // NSMutableArray *arr12;
            // arr12=[[NSMutableArray alloc]init];
            // [arr12 addObject:dictForFollowUp];
            [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
            
        }else
        {
            NSMutableArray *arrCurrentService=[[NSMutableArray alloc]init];
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
                
                NSArray *arrCurrentServiceKey;
                arrCurrentServiceKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrCurrentServiceKey);
                arrCurrentServiceValue=[[NSMutableArray alloc]init];
                NSLog(@"all keys %@",arrCurrentServiceValue);
                for (int i=0; i<arrCurrentServiceKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]];
                    if([str isEqual:nil])
                    {
                        str=@"";
                    }
                    [arrCurrentServiceValue addObject:str];
                }
                
                NSDictionary *dictCurrentService;
                dictCurrentService = [NSDictionary dictionaryWithObjects:arrCurrentServiceValue forKeys:arrCurrentServiceKey];
                
                NSLog(@"%@",dictCurrentService);
                [arrCurrentService addObject:dictCurrentService];
                NSLog(@"ServiceFollowup%@",arrCurrentService);
            }
            [dictForFollowUp setObject:arrCurrentService forKey:@"serviceFollowUpDcs"];
            [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
        }
    }
    else
    {
        NSMutableArray *arrCurrentServiceValue;
        
        arrCurrentServiceValue=[[NSMutableArray alloc]init];
        [dictForFollowUp setObject:arrCurrentServiceValue forKey:@"serviceFollowUpDcs"];
        // NSMutableArray *arr12;
        // arr12=[[NSMutableArray alloc]init];
        // [arr12 addObject:dictForFollowUp];
        [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
    }
}
-(void)fetchProposalFollowUpForSyncCall
{
    
    if (isProposalFoolowUp==YES)
    {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        
        entityProposalFollowUp=[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context];
        [request setEntity:entityProposalFollowUp];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"proposalLeadId=%@",strLeadId];
        
        [request setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"proposalLeadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [request setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        //    NSManagedObject *matchesNew;
        NSMutableArray *arrCurrentServiceValue;
        
        arrCurrentServiceValue=[[NSMutableArray alloc]init];
        
        if (arrAllObj.count==0)
        {
            [dictForFollowUp setObject:arrCurrentServiceValue forKey:@"proposalFollowUpDcs"];
            [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
            
        }
        else
        {
            NSMutableArray *arrCurrentService=[[NSMutableArray alloc]init];
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"proposalLeadId"]);
                
                NSArray *arrCurrentServiceKey;
                arrCurrentServiceKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrCurrentServiceKey);
                arrCurrentServiceValue=[[NSMutableArray alloc]init];
                NSLog(@"all keys %@",arrCurrentServiceValue);
                for (int i=0; i<arrCurrentServiceKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrCurrentServiceKey objectAtIndex:i]]];
                    
                    [arrCurrentServiceValue addObject:str];
                }
                
                NSDictionary *dictCurrentService;
                dictCurrentService = [NSDictionary dictionaryWithObjects:arrCurrentServiceValue forKeys:arrCurrentServiceKey];
                
                NSLog(@"%@",dictCurrentService);
                [arrCurrentService addObject:dictCurrentService];
                NSLog(@"ProposalFollowup%@",arrCurrentService);
            }
            [dictForFollowUp setObject:arrCurrentService forKey:@"proposalFollowUpDcs"];
            [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
        }
    }
    else
    {
        NSMutableArray *arrCurrentServiceValue;
        
        arrCurrentServiceValue=[[NSMutableArray alloc]init];
        [dictForFollowUp setObject:arrCurrentServiceValue forKey:@"proposalFollowUpDcs"];
        [dictFinal setObject:dictForFollowUp forKey:@"LeadFollowup"];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==_txtEmailId) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        else{
            return YES;
        }
    }
    else{
        return YES;
    }
    
}

//-(void)textViewDidEndEditing:(UITextView *)textView
//{
//    [textView resignFirstResponder];
//}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    /*if([text isEqualToString:@"\n"]) {
     [textView resignFirstResponder];
     return NO;
     }*/
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        
        return NO;
    }
    return YES;
    //return YES;
}
#pragma  mark- -------------------- DOCUMENTS CODE ---------------------
//Nilind 07 Sept

-(void)adjusttableViewHeight
{
    [self salesEmailFetch];
    
    _constTableEmail_H.constant=75*arrEmail.count;
    _const_TableDocument_H.constant=75*arrFinalDocumets.count;
    
    NSInteger height;
    height=_constTableEmail_H.constant+_const_TableDocument_H.constant;
    
    [_viewSendMail removeFromSuperview];
    
    _viewSendMail.frame=CGRectMake(4  ,60,[UIScreen mainScreen].bounds.size.width-8,750+height-50);//750+60
    
    [__scrollViewSendMail addSubview:_viewSendMail];
    
    
    _viewForFollowUp.frame=CGRectMake(_viewSendMail.frame.origin.x  ,CGRectGetMaxY(_viewSendMail.frame)+30,_viewForFollowUp.frame.size.width,_viewForFollowUp.frame.size.height);
    
    [__scrollViewSendMail setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewForFollowUp.frame.size.height+_viewForFollowUp.frame.origin.y)];
    
}
-(void)fetchSoldServiceToGetDocument
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    if ([_strForSendProposal isEqualToString:@"forSendProposal"])
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"false"];
    }
    else
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
        
    }
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjNew;
    arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesNew;
    NSMutableArray *arrSoldServiceStandardDetail;
    arrSoldServiceStandardDetail=[[NSMutableArray alloc]init];
    arrServiceIdForDocuments=[[NSMutableArray alloc]init];
    if (arrAllObjNew.count==0)
    {
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matchesNew valueForKey:@"leadId"]);
        
        
        for (int k=0; k<arrAllObjNew.count; k++)
        {
            matchesNew=arrAllObjNew[k];
            [arrServiceIdForDocuments addObject:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceId"]]];
            NSLog(@"Lead IDDDD====%@",[matchesNew valueForKey:@"leadId"]);
            
        }
        
    }
}
-(void)fetchSoldServiceToGetDocumentForNonStanDard
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceNonStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    if ([_strForSendProposal isEqualToString:@"forSendProposal"])
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"false"];
    }
    else
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
        
    }
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjNew;
    arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesNew;
    
    arrDeptNameNonStanForDocument=[[NSMutableArray alloc]init];
    if (arrAllObjNew.count==0)
    {
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matchesNew valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObjNew.count; k++)
        {
            matchesNew=arrAllObjNew[k];
            [arrDeptNameNonStanForDocument addObject:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"departmentSysname"]]];
        }
        
    }
}
-(void)fetchCategorySysnameFromServiceSysName
{
    NSMutableArray *name,*sysName,*serviceId,*deptSysName;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    serviceId=[[NSMutableArray alloc]init];
    deptSysName=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        
        NSString *strCategorySysName,*strDepartmentSysName;
        strCategorySysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        strDepartmentSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"DepartmentSysName"]];
        
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [deptSysName addObject:strDepartmentSysName];
                [name addObject:strCategorySysName];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [serviceId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceMasterId"]]];
            }
        }
    }
    dictCategoryNameForDocumets = [NSDictionary dictionaryWithObjects:name forKeys:serviceId];
    dictServiceNameFromIdForDocumets = [NSDictionary dictionaryWithObjects:sysName forKeys:serviceId];
    dictDepartmentFromServiceId=[NSDictionary dictionaryWithObjects:deptSysName forKeys:serviceId];
    
    NSLog(@"Dictonary Category%@",dictCategoryNameForDocumets);
    
}
-(void)fetchDocumentsToBeSend
{
    if (chkStatus==YES)
    {
        [self fetchDocumentsDetailDocument];
    }
    else
    {
        [self fetchDocumentsDetailDocument];
      /*  [self fetchCategorySysnameFromServiceSysName];
        [self fetchSoldServiceToGetDocument];
        
        NSDictionary  *dictSalesLeadMaster;
        NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
        dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
        
        NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSString *strBranchSysName;
        strBranchSysName=[defs valueForKey:@"branchSysName"];
        NSMutableArray *arrDepartment;
        arrDepartment=[[NSMutableArray alloc]init];
        
        for (int i=0;i<arrDeptName.count; i++)
        {
            NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
            NSArray *arry=[dictBranch valueForKey:@"Departments"];
            for (int j=0; j<arry.count; j++)
            {
                NSDictionary *dict=[arry objectAtIndex:j];
                if ([strBranchSysName isEqualToString:[dictBranch valueForKey:@"SysName"]])
                {
                    NSString *strIsActive=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]];
                    if ([strIsActive isEqualToString:@"false"]||[strIsActive isEqualToString:@"0"])
                    {
                        
                    }
                    else
                    {
                        [arrDepartment addObject:dict];
                    }
                }
                
            }
            
        }
        //Other Documents Fetch
        NSDictionary *dictMasters;
        dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        NSArray *arrOtherDocuments=[dictMasters valueForKey:@"OtherDocumentsMasterExts"];
        
        //FetchDocumets Departments Wise
        
        NSMutableArray *allDocuments;
        allDocuments=[[NSMutableArray alloc]init];
        
        //FetchDocumets Department Sold Serivce Wise
        
        for (int i=0; i<arrServiceIdForDocuments.count; i++)
        {
            NSString *strServiceId=[arrServiceIdForDocuments objectAtIndex:i];
            NSString *strDeptSysName;
            strDeptSysName=[dictDepartmentFromServiceId valueForKey:strServiceId];
            for (int j=0; j<arrOtherDocuments.count; j++)
            {
                NSDictionary *dictNew=[arrOtherDocuments objectAtIndex:j];
                
                if ([strDeptSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"DepartmentSysName"]]])
                {
                    [allDocuments addObject:dictNew];
                }
            }
        }
        
        
        //FetchDocumets Category Wise
        
        for (int i=0; i<arrServiceIdForDocuments.count; i++)
        {
            NSString *strServiceId=[arrServiceIdForDocuments objectAtIndex:i];
            NSString *strCategorySysName;
            strCategorySysName=[dictCategoryNameForDocumets valueForKey:strServiceId];
            for (int j=0; j<arrOtherDocuments.count; j++)
            {
                NSDictionary *dictNew=[arrOtherDocuments objectAtIndex:j];
                
                if ([strCategorySysName isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"CategorySysName"]]])
                {
                    [allDocuments addObject:dictNew];
                }
            }
        }
        
        //FetchDocumets Service Wise
        
        for (int i=0; i<arrServiceIdForDocuments.count; i++)
        {
            NSString *strServiceId=[arrServiceIdForDocuments objectAtIndex:i];
            
            for (int j=0; j<arrOtherDocuments.count; j++)
            {
                NSDictionary *dictNew=[arrOtherDocuments objectAtIndex:j];
                
                if ([strServiceId isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceId"]]])
                {
                    [allDocuments addObject:dictNew];
                }
            }
        }
        NSLog(@"All Documets %@",allDocuments);
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:allDocuments];
        NSArray *arrayWithoutDuplicates = [orderedSet array];
        NSLog(@"All Unique Documets %@",arrayWithoutDuplicates);
        
        arrFinalDocumets=[[NSMutableArray alloc]init];
        arrLocalDocument=[[NSMutableArray alloc]init];
        for (int i=0; i<arrayWithoutDuplicates.count; i++)
        {
            [arrFinalDocumets addObject:[arrayWithoutDuplicates objectAtIndex:i]];
            NSDictionary *dict=[arrayWithoutDuplicates objectAtIndex:i];
            NSString *isDefault=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDefault"]];
            if ([isDefault isEqualToString:@"true"]||[isDefault isEqualToString:@"1"]||[isDefault isEqualToString:@"True"])
            {
                [arrLocalDocument addObject:[arrayWithoutDuplicates objectAtIndex:i]];
            }
            
        }*/
    }
    
    [_tblDocuments reloadData];
}
-(void)fetchDocumentsDetailDocument
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSManagedObject *matchesDoc;
    NSArray  *arrAllObjDoc = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrFinalDocumets=[[NSMutableArray alloc]init];
    arrLocalDocument=[[NSMutableArray alloc]init];
    arrDocId=[[NSMutableArray alloc]init];
    arrFinalDocumets=[[NSMutableArray alloc]init];
    arrLocalDocument=[[NSMutableArray alloc]init];
    arrTempResend=[[NSMutableArray alloc]init];
    
    if (arrAllObjDoc.count==0)
    {
        
    }
    else
    {
        for(int i=0; i<arrAllObjDoc.count; i++)
        {
            matchesDoc=[arrAllObjDoc objectAtIndex:i];
            [arrDocId addObject:[NSString stringWithFormat:@"%@",[matchesDoc valueForKey:@"otherDocSysName"]]];//title
            //[arrFinalDocumets addObject:matchesDoc]
            
        }
    }
    [self fetchCategorySysnameFromServiceSysName];
    [self fetchSoldServiceToGetDocument];
    [self fetchSoldServiceToGetDocumentForNonStanDard];
    NSDictionary  *dictSalesLeadMaster;
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    
    NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    
    if ([[dictSalesLeadMaster valueForKey:@"BranchMasters"] isKindOfClass:[NSArray class]]) {
        
        // is array nothing to do.
        
    }else{
        
        arrDeptName = [[NSArray alloc]init];
        
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strBranchSysName;
    strBranchSysName=[defs valueForKey:@"branchSysName"];
    NSMutableArray *arrDepartment;
    arrDepartment=[[NSMutableArray alloc]init];
    
    for (int i=0;i<arrDeptName.count; i++)
    {
        NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
        NSArray *arry=[dictBranch valueForKey:@"Departments"];
        for (int j=0; j<arry.count; j++)
        {
            NSDictionary *dict=[arry objectAtIndex:j];
            if ([strBranchSysName isEqualToString:[dictBranch valueForKey:@"SysName"]])
            {
                NSString *strIsActive=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]];
                if ([strIsActive isEqualToString:@"false"]||[strIsActive isEqualToString:@"0"])
                {
                    
                }
                else
                {
                    [arrDepartment addObject:dict];
                }
            }
            
        }
        
    }
    //Other Documents Fetch
    NSDictionary *dictMasters;
    dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSArray *arrOtherDocuments=[dictMasters valueForKey:@"OtherDocumentsMasterExts"];
    
    //FetchDocumets Departments Wise
    
    NSMutableArray *allDocuments;
    allDocuments=[[NSMutableArray alloc]init];
    
    //FetchDocumets Department Sold Serivce Wise
    
    for (int i=0; i<arrServiceIdForDocuments.count; i++)
    {
        NSString *strServiceId=[arrServiceIdForDocuments objectAtIndex:i];
        NSString *strDeptSysName;
        strDeptSysName=[dictDepartmentFromServiceId valueForKey:strServiceId];
        for (int j=0; j<arrOtherDocuments.count; j++)
        {
            NSDictionary *dictNew=[arrOtherDocuments objectAtIndex:j];
            
            if ([strDeptSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"DepartmentSysName"]]])
            {
                [allDocuments addObject:dictNew];
            }
        }
    }
    
    
    //FetchDocumets Category Wise
    
    for (int i=0; i<arrServiceIdForDocuments.count; i++)
    {
        NSString *strServiceId=[arrServiceIdForDocuments objectAtIndex:i];
        NSString *strCategorySysName;
        strCategorySysName=[dictCategoryNameForDocumets valueForKey:strServiceId];
        for (int j=0; j<arrOtherDocuments.count; j++)
        {
            NSDictionary *dictNew=[arrOtherDocuments objectAtIndex:j];
            
            if ([strCategorySysName isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"CategorySysName"]]])
            {
                [allDocuments addObject:dictNew];
            }
        }
    }
    
    //FetchDocumets Service Wise
    
    for (int i=0; i<arrServiceIdForDocuments.count; i++)
    {
        NSString *strServiceId=[arrServiceIdForDocuments objectAtIndex:i];
        
        for (int j=0; j<arrOtherDocuments.count; j++)
        {
            NSDictionary *dictNew=[arrOtherDocuments objectAtIndex:j];
            
            if ([strServiceId isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceId"]]])
            {
                [allDocuments addObject:dictNew];
            }
        }
    }
    NSLog(@"All Documets %@",allDocuments);
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:allDocuments];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    NSLog(@"All Unique Documets %@",arrayWithoutDuplicates);
    
    for (int i=0; i<arrayWithoutDuplicates.count; i++)
    {
        [arrFinalDocumets addObject:[arrayWithoutDuplicates objectAtIndex:i]];
        
    }
    
    //New Logic 03 Sept
    NSMutableArray *arr1=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrServiceIdForDocuments.count; i++)
    {
        NSString *strServiceId=[arrServiceIdForDocuments objectAtIndex:i];
        NSString *strCategorySysName;
        strCategorySysName=[dictCategoryNameForDocumets valueForKey:strServiceId];
        
        NSString *strServiceNameFromId=[NSString stringWithFormat:@"%@",[dictServiceNameFromIdForDocumets valueForKey:strServiceId]];
        //dictServiceNameFromIdForDocumets
        for (int j=0; j<arrFinalDocumets.count; j++)
        {
            NSDictionary *dictNew=[arrFinalDocumets objectAtIndex:j];
            
            if ([strServiceNameFromId isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceSysName"]]] && [[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"CategorySysName"]] isEqualToString:@""])
            {
                //  [allDocuments addObject:dictNew];
                [arr1 addObject:dictNew];
            }
        }
        
        for (int k=0; k<arrFinalDocumets.count; k++)
        {
            NSDictionary *dictNew=[arrFinalDocumets objectAtIndex:k];
            
            if (([[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceSysName"]] isEqualToString:@""]||[[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceSysName"]] isEqualToString:@"0"]) && [strCategorySysName isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"CategorySysName"]]])
            {
                //  [allDocuments addObject:dictNew];
                [arr1 addObject:dictNew];
            }
        }
        for (int l=0; l<arrFinalDocumets.count; l++)
        {
            NSDictionary *dictNew=[arrFinalDocumets objectAtIndex:l];
            
            if ([strServiceNameFromId isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceSysName"]]] && [strCategorySysName isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"CategorySysName"]]])
            {
                //  [allDocuments addObject:dictNew];
                [arr1 addObject:dictNew];
            }
        }
        for (int m=0; m<arrFinalDocumets.count; m++)
        {
            NSDictionary *dictNew=[arrFinalDocumets objectAtIndex:m];
            
            if ([[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceSysName"]]isEqualToString:@""] && [[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"CategorySysName"]]isEqualToString:@""])
            {
                //  [allDocuments addObject:dictNew];
                [arr1 addObject:dictNew];
            }
        }
        
        //One mORE cONDITION
        
        for (int m=0; m<arrFinalDocumets.count; m++)
        {
            NSDictionary *dictNew=[arrFinalDocumets objectAtIndex:m];
            
            if ([[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceSysName"]]isEqualToString:@"0"] && [[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"CategorySysName"]]isEqualToString:@""])
            {
                //  [allDocuments addObject:dictNew];
                [arr1 addObject:dictNew];
            }
        }
        
    }
    //For NonStandard
    
    for (int i=0; i<arrDeptNameNonStanForDocument.count; i++)
    {
        for (int j=0; j<arrOtherDocuments.count; j++)
        {
            NSDictionary *dictNew=[arrOtherDocuments objectAtIndex:j];
            
            if ([[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"CategorySysName"]] isEqualToString:@""]&&([[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceSysName"]] isEqualToString:@""]||[[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"ServiceSysName"]] isEqualToString:@"0"])&&[[arrDeptNameNonStanForDocument objectAtIndex:i]isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"DepartmentSysName"]]])
            {
                [arr1 addObject:dictNew];
            }
        }
    }
    //End
    
    arrFinalDocumets=[[NSMutableArray alloc]init];
    NSArray *uniqueArray = [[NSSet setWithArray:arr1] allObjects];
    [arrFinalDocumets addObjectsFromArray:uniqueArray];
    
    
    //Temp 25 Oct
    arrTempDocumentForResend=[[NSMutableArray alloc]init];
    for (int i=0; i<arrFinalDocumets.count; i++)
    {
        NSDictionary *dict=[arrFinalDocumets objectAtIndex:i];
        
        for (int j=0; j<arrDocId.count; j++)
        {
            if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"OtherDocSysName"]]isEqualToString:[arrDocId objectAtIndex:j]])
            {
                [arrTempResend addObject:[arrFinalDocumets objectAtIndex:i]];
                [arrTempDocumentForResend addObject:[arrFinalDocumets objectAtIndex:i]];
                break;
            }
        }
    }
    NSLog(@"Already selected id %@ >>> %@",arrTempResend,arrTempDocumentForResend);
    
    //Additional Logic 26 May 2020
    
    for (int i=0; i<arrFinalDocumets.count;i++)
    {
        NSString *isDefault;
        
        NSDictionary *dict = [arrFinalDocumets objectAtIndex:i];
        isDefault=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDefault"]];
        
        if ([isDefault isEqualToString:@"true"]||[isDefault isEqualToString:@"1"]||[isDefault isEqualToString:@"True"])
        {
            
            [arrDocumentSelected addObject:dict];
            
        }
    }
     
    [arrDocumentSelected addObjectsFromArray:arrTempResend];
    NSArray *uniqueArrayNew = [[NSSet setWithArray:arrDocumentSelected] allObjects];
    arrDocumentSelected=[[NSMutableArray alloc]init];
    [arrDocumentSelected addObjectsFromArray:uniqueArrayNew];
    
    //}
}
-(void)deleteDocFromCoreDataSalesInfo
{
    entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataDoc = [[NSFetchRequest alloc] init];
    [allDataDoc setEntity:[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateDoc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    [allDataDoc setPredicate:predicateDoc];
    
    [allDataDoc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error2 = nil;
    NSArray * DataImage = [context executeFetchRequest:allDataDoc error:&error2];
    //error handling goes here
    for (NSManagedObject * data in DataImage)
    {
        if ([[data valueForKey:@"docType"] isEqualToString:@"DefaultDocuments"])
        {
            [context deleteObject:data];
        }
        else
        {
            
        }
      //  [context deleteObject:data];
    }
    NSError *saveError2 = nil;
    [context save:&saveError2];
    
}
-(void)saveDocumentToBeSend
{
    [self deleteDocFromCoreDataSalesInfo];
    
    NSLog(@"%@",arrFinalDocumets);

    for (int i=0; i<arrFinalDocumets.count;i++)
    {
        NSString *isDefault;
        
        NSDictionary *dict = [arrFinalDocumets objectAtIndex:i];
        isDefault=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDefault"]];
        
        if ([isDefault isEqualToString:@"true"]||[isDefault isEqualToString:@"1"]||[isDefault isEqualToString:@"True"])
        {
            
           // [arrLocalDocument addObject:dict];
            
        }
        
        
        for (int j=0; j<arrDocumentSelected.count;j++)
        {
            NSDictionary *dictNew = [arrDocumentSelected objectAtIndex:j];
            
            if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"OtherDocSysName"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"OtherDocSysName"]]])
            {
                [arrLocalDocument addObject:dict];
            }
        }
        
    }
    
    NSArray *uniqueArray = [[NSSet setWithArray:arrLocalDocument] allObjects];
    arrLocalDocument=[[NSMutableArray alloc]init];
    [arrLocalDocument addObjectsFromArray:uniqueArray];
    
    for (int z=0; z<arrLocalDocument.count; z++)
    {
        entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
        
        DocumentsDetail *objentityDocumentsDetail = [[DocumentsDetail alloc]initWithEntity:entityDocumentsDetail insertIntoManagedObjectContext:context];
        NSMutableDictionary *dictDocumentsDetail=[[NSMutableDictionary alloc]init];
        dictDocumentsDetail=[arrLocalDocument objectAtIndex:z];
        
        objentityDocumentsDetail.leadId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"LeadId"]];
        
        objentityDocumentsDetail.accountNo=strAccountNo;
        
        objentityDocumentsDetail.createdBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedBy"]];
        
        objentityDocumentsDetail.createdDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedDate"]];
        
        objentityDocumentsDetail.descriptionDocument=@"";
        objentityDocumentsDetail. docType=@"DefaultDocuments";
        
        objentityDocumentsDetail.fileName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocName"]];
        
        objentityDocumentsDetail.leadDocumentId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocumentId"]];
        
        objentityDocumentsDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedBy"]];
        
        objentityDocumentsDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedDate"]];
        
        objentityDocumentsDetail.scheduleStartDate=strScheduleInspectionDate;
        
        objentityDocumentsDetail.title=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"Title"]];
        
        objentityDocumentsDetail.otherDocSysName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocSysName"]];
        
        objentityDocumentsDetail.leadId=strLeadId;
        
        //Nilind 16 Nov
        
        objentityDocumentsDetail.userName=strUserName;
        objentityDocumentsDetail.companyKey=strCompanyKey;
        
        //........................
        NSError *error10;
        [context save:&error10];
        
    }
    
}
-(void)saveDocumentToBeSendOld
{
    [self deleteDocFromCoreDataSalesInfo];
    
    NSLog(@"%@",arrFinalDocumets);
    NSMutableArray *arrFinalDocumentToSend = [[NSMutableArray alloc]init];

    
    for (int i=0; i<arrFinalDocumets.count;i++)
    {
        NSString *isDefault;
        
        NSDictionary *dict = [arrFinalDocumets objectAtIndex:i];
        isDefault=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDefault"]];
        
        if ([isDefault isEqualToString:@"true"]||[isDefault isEqualToString:@"1"]||[isDefault isEqualToString:@"True"])
        {
            [arrLocalDocument addObject:dict];
        }
        
    }
    
    
    NSArray *uniqueArray = [[NSSet setWithArray:arrLocalDocument] allObjects];
    arrLocalDocument=[[NSMutableArray alloc]init];
    [arrLocalDocument addObjectsFromArray:uniqueArray];
    
    //Temp Logic
    [arrDocumentSelected addObjectsFromArray:arrLocalDocument];
    NSArray *uniqueArray2 = [[NSSet setWithArray:arrDocumentSelected] allObjects];
    arrDocumentSelected=[[NSMutableArray alloc]init];
    [arrDocumentSelected addObjectsFromArray:uniqueArray2];
    //End
    
    for (int i=0; i<arrLocalDocument.count ; i++)
    {
        NSDictionary *dict = [arrLocalDocument objectAtIndex:i];
        
        for(int j=0; j<arrDocumentSelected.count;j++)
        {
            NSDictionary *dictNew = [arrDocumentSelected objectAtIndex:j];
            
            if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"OtherDocSysName"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"OtherDocSysName"]]])
            {
                [arrFinalDocumentToSend addObject:dict];
            }
            
        }
    }
    
    arrLocalDocument=[[NSMutableArray alloc]init];
    [arrLocalDocument addObjectsFromArray:arrFinalDocumentToSend];
    
    for (int z=0; z<arrLocalDocument.count; z++)
    {
        entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
        
        DocumentsDetail *objentityDocumentsDetail = [[DocumentsDetail alloc]initWithEntity:entityDocumentsDetail insertIntoManagedObjectContext:context];
        NSMutableDictionary *dictDocumentsDetail=[[NSMutableDictionary alloc]init];
        dictDocumentsDetail=[arrLocalDocument objectAtIndex:z];
        
        objentityDocumentsDetail.leadId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"LeadId"]];
        
        objentityDocumentsDetail.accountNo=strAccountNo;
        
        objentityDocumentsDetail.createdBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedBy"]];
        
        objentityDocumentsDetail.createdDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedDate"]];
        
        objentityDocumentsDetail.descriptionDocument=@"";
        objentityDocumentsDetail. docType=@"DefaultDocuments";
        
        objentityDocumentsDetail.fileName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocName"]];
        
        objentityDocumentsDetail.leadDocumentId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocumentId"]];
        
        objentityDocumentsDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedBy"]];
        
        objentityDocumentsDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedDate"]];
        
        objentityDocumentsDetail.scheduleStartDate=strScheduleInspectionDate;
        
        objentityDocumentsDetail.title=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"Title"]];
        
        objentityDocumentsDetail.otherDocSysName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocSysName"]];
        
        objentityDocumentsDetail.leadId=strLeadId;
        
        //Nilind 16 Nov
        
        objentityDocumentsDetail.userName=strUserName;
        objentityDocumentsDetail.companyKey=strCompanyKey;
        
        //........................
        NSError *error10;
        [context save:&error10];
        
    }
    
}
#pragma mark- -------- API FOR RESEND EMAIL ---------------
-(void)resendEmail
{
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    NSDictionary *dict_ToSend;
    NSArray *key,*value;
    key=[NSArray arrayWithObjects:
         @"LeadId",
         @"LeadNumber",
         @"CompanyId",
         @"IsProposalFromMobile",
         @"DocumentsDetail",
         @"EmailDetail",
         nil];
    
    value=[NSArray arrayWithObjects:
           strLeadId,
           strLeadNumber,
           strCompanyId,
           strProposal,
           [dictFinal valueForKey:@"DocumentsDetail"],
           [dictFinal valueForKey:@"EmailDetail"],
           nil];
    dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,@"/api/MobileToSaleAuto/MobileResendAgreementProposalMail"];
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    if ([NSJSONSerialization isValidJSONObject:dictFinal])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Resend Email JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //New Header Change
    [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    //End
    //Nilind 1 Feb
    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    NSError *error = nil;
    NSURLResponse * response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSData* jsonData = [NSData dataWithData:data];
    if (jsonData==nil)
    {
        
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
    }
    else
    {
        [DejalBezelActivityView removeView];
        [DejalActivityView removeView];
        
        // NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        NSString *finalResponsestring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"RESPONSE RESEND MAIL >> %@",finalResponsestring);
        
        if ([finalResponsestring isEqualToString:@"true"]||[finalResponsestring isEqualToString:@"True"])
        {
            [global AlertMethod:@"Alert!" :@"Email sent successfully"];
            NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
            
            NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
            
            if ([strAppointmentFlow isEqualToString:@"New"])
            {
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
                AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
                
            }
            else
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
                AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
            
        }
        else
        {
            [global AlertMethod:@"Alert!" :@"Some thing went wrong, try again later"];
        }
        
        
    }
}
-(void)saveDocumentToBeSendForResend
{
    [self deleteDocFromCoreDataSalesInfo];
    
    
    
    for (int i=0; i<arrFinalDocumets.count;i++)
    {
        NSString *isDefault;
        
        NSDictionary *dict = [arrFinalDocumets objectAtIndex:i];
        isDefault=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsDefault"]];
        
        if ([isDefault isEqualToString:@"true"]||[isDefault isEqualToString:@"1"]||[isDefault isEqualToString:@"True"])
        {
            
           // [arrLocalDocument addObject:dict];
            
        }
        
        
        for (int j=0; j<arrDocumentSelected.count;j++)
        {
            NSDictionary *dictNew = [arrDocumentSelected objectAtIndex:j];
            
            if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"OtherDocSysName"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"OtherDocSysName"]]])
            {
                [arrTempDocumentForResend addObject:dict];
            }
        }
        
    }
    
    NSArray *uniqueArray = [[NSSet setWithArray:arrTempDocumentForResend] allObjects];
    arrTempDocumentForResend=[[NSMutableArray alloc]init];
    [arrTempDocumentForResend addObjectsFromArray:uniqueArray];
    

    for (int z=0; z<arrTempDocumentForResend.count; z++)
    {
        entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
        
        DocumentsDetail *objentityDocumentsDetail = [[DocumentsDetail alloc]initWithEntity:entityDocumentsDetail insertIntoManagedObjectContext:context];
        NSMutableDictionary *dictDocumentsDetail=[[NSMutableDictionary alloc]init];
        dictDocumentsDetail=[arrTempDocumentForResend objectAtIndex:z];
        
        objentityDocumentsDetail.accountNo=strAccountNo;
        
        objentityDocumentsDetail.createdBy=strUserName;
        
        objentityDocumentsDetail.createdDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedDate"]];
        
        objentityDocumentsDetail.descriptionDocument=@"";
        objentityDocumentsDetail.docType=@"DefaultDocuments";
        
        objentityDocumentsDetail.fileName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocName"]];
        
        objentityDocumentsDetail.leadDocumentId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocumentId"]];
        
        objentityDocumentsDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedBy"]];
        
        objentityDocumentsDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedDate"]];
        
        objentityDocumentsDetail.scheduleStartDate=strScheduleInspectionDate;
        
        objentityDocumentsDetail.title=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"Title"]];
        
        objentityDocumentsDetail.otherDocSysName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocSysName"]];
        
        objentityDocumentsDetail.leadId=strLeadId;
        
        //Nilind 16 Nov
        
        objentityDocumentsDetail.userName=strUserName;
        objentityDocumentsDetail.companyKey=strCompanyKey;
        
        //........................
        NSError *error10;
        [context save:&error10];
        
    }
}
-(void)saveDocumentToBeSendForResendOld
{
    [self deleteDocFromCoreDataSalesInfo];
    for (int z=0; z<arrTempDocumentForResend.count; z++)
    {
        entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
        
        DocumentsDetail *objentityDocumentsDetail = [[DocumentsDetail alloc]initWithEntity:entityDocumentsDetail insertIntoManagedObjectContext:context];
        NSMutableDictionary *dictDocumentsDetail=[[NSMutableDictionary alloc]init];
        dictDocumentsDetail=[arrTempDocumentForResend objectAtIndex:z];
        
        objentityDocumentsDetail.accountNo=strAccountNo;
        
        objentityDocumentsDetail.createdBy=strUserName;
        
        objentityDocumentsDetail.createdDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedDate"]];
        
        objentityDocumentsDetail.descriptionDocument=@"";
        objentityDocumentsDetail.docType=@"DefaultDocuments";
        
        objentityDocumentsDetail.fileName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocName"]];
        
        objentityDocumentsDetail.leadDocumentId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocumentId"]];
        
        objentityDocumentsDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedBy"]];
        
        objentityDocumentsDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedDate"]];
        
        objentityDocumentsDetail.scheduleStartDate=strScheduleInspectionDate;
        
        objentityDocumentsDetail.title=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"Title"]];
        
        objentityDocumentsDetail.otherDocSysName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocSysName"]];
        
        objentityDocumentsDetail.leadId=strLeadId;
        
        //Nilind 16 Nov
        
        objentityDocumentsDetail.userName=strUserName;
        objentityDocumentsDetail.companyKey=strCompanyKey;
        
        //........................
        NSError *error10;
        [context save:&error10];
        
    }
}
#pragma mark- 26 Oct

-(void)updateLeadDetailOnResponse
{
    // visibility and Inspector alag se save krna he
    isEditedInSalesAuto=YES;
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjResend = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject  *matchesResend;
    if (arrAllObjResend.count==0)
    {
        
    }else
    {
        
        matchesResend=arrAllObjResend[0];
        
        if ([[NSString stringWithFormat:@"%@",[matchesResend valueForKey:@"isCustomerNotPresent"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[matchesResend valueForKey:@"isCustomerNotPresent"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[matchesResend valueForKey:@"isCustomerNotPresent"]] isEqualToString:@"True"])
        {
            
        }
        else
        {
            if ([_strForSendProposal isEqualToString:@"forSendProposal"])
            {
            }
            else
            {
                [matchesResend setValue:@"true" forKey:@"isResendAgreementProposalMail"];
                
                // For Wdo Only Update in Case of Complete Pending
                
                if ([_strFromWdo isEqualToString:@"yes"]) {
                    
                    NSString *strStageLocal = [NSString stringWithFormat:@"%@",[matchesResend valueForKey:@"stageSysName"]];
                    NSString *strStatusLocal = [NSString stringWithFormat:@"%@",[matchesResend valueForKey:@"statusSysName"]];
                    
                    if([strStatusLocal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageLocal caseInsensitiveCompare:@"Won"] == NSOrderedSame)
                    {
                        
                        [matchesResend setValue:@"true" forKey:@"isResendAgreementProposalMail"];

                    }else{
                        
                        [matchesResend setValue:@"false" forKey:@"isResendAgreementProposalMail"];
                        
                    }
                }
            }
        }
        [matchesResend setValue:@"false" forKey:@"isResendAgreementProposalMail"];
        [matchesResend setValue:@"false" forKey:@"isMailSend"];

        
        [context save:&error1];
        
    }
}
-(NSString*)fetchLeadDetailForResend
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSString *str;
    if (arrAllObj12.count==0)
    {
        
    }
    else
    {
        
        NSManagedObject *matches12=arrAllObj12[0];
        
        str = @"";//[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"isResendAgreementProposalMail"]];//isInitialSetupCreated
        strGlobalLeadStatus=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]];
        if ([strGlobalLeadStatus isEqualToString:@"Complete"]||[strGlobalLeadStatus isEqualToString:@"complete"])
        {
            chkStatus=YES;
        }
        else
        {
            chkStatus=NO;
            
        }
        
    }
    return str;
}

-(NSManagedObject*)fetchLeadDetailForWDO
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matches12;
    if (arrAllObj12.count==0)
    {
        
    }
    else
    {
        
        matches12=arrAllObj12[0];

    }
    return matches12;
    
}

-(BOOL)emailExist:(NSString *)strEmail
{
    BOOL checkForExist;
    checkForExist=NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjForEmail = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesForEmail;
    if (arrAllObjForEmail.count==0)
    {
        
    }
    else
    {
        for (int i=0; i<arrAllObjForEmail.count; i++)
        {
            matchesForEmail=arrAllObjForEmail[i];
            if ([[NSString stringWithFormat:@"%@",[matchesForEmail valueForKey:@"emailId"]]isEqualToString:strEmail])
            {
                checkForExist=YES;
                break;
            }
        }
    }
    return checkForExist;
}
-(void)fetchAgreementCheckListSales
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:context];
    [request setEntity:entityLeadAgreementChecklistSetups];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isActive = %@",strLeadId,@"true"];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"LeadAgreementChecklistSetups"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            /*
             int indexToRemove=-1;
             int indexToReplaceModifyDate=-1;
             
             for (int k=0; k<arrPaymentInfoKey.count; k++) {
             
             NSString *strKeyLeadId=arrPaymentInfoKey[k];
             
             if ([strKeyLeadId isEqualToString:@"leadId"]) {
             
             indexToRemove=k;
             
             
             if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
             
             indexToReplaceModifyDate=k;
             
             }
             }
             
             NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
             
             [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
             
             [arrKeyTemp removeObjectAtIndex:indexToRemove];
             arrPaymentInfoKey=arrKeyTemp;
             [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
             
             [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
             */
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictAgreement;
            dictAgreement = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictAgreement);
            [arrEmailDetail addObject:dictAgreement];
            NSLog(@"AgreementChecklistSetups>>%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"LeadAgreementChecklistSetups"];
        
    }
}
-(void)fetchElectronicAuthorizedForm
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityElectronicAuthorizedForm = [NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
    [request setEntity:entityElectronicAuthorizedForm];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //arrCheckImage=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"ElectronicAuthorizationForm"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
                
                NSString  *strCustomerImage=[matches valueForKey:@"signaturePath"];
                if (!(strCustomerImage.length==0)) {
                    
                    strElectronicSign=strCustomerImage;
                    
                }
                NSArray *arrElectronicAuthorizedFormKey;
                NSMutableArray *arrElectronicAuthorizedFormValue;
                
                arrElectronicAuthorizedFormValue=[[NSMutableArray alloc]init];
                arrElectronicAuthorizedFormKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrElectronicAuthorizedFormKey);
                
                NSLog(@"all keys %@",arrElectronicAuthorizedFormValue);
                for (int i=0; i<arrElectronicAuthorizedFormKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrElectronicAuthorizedFormKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrElectronicAuthorizedFormKey objectAtIndex:i]]];
                    
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                    
                    [arrElectronicAuthorizedFormValue addObject:str];
                }
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                NSDictionary *dictPaymentInfo;
                dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrElectronicAuthorizedFormValue forKeys:arrElectronicAuthorizedFormKey];
                NSLog(@"ElectronicAuthorizationForm%@",dictPaymentInfo);
                [dictFinal setObject:dictPaymentInfo forKey:@"ElectronicAuthorizationForm"];
            }
            
        }
    }
}
//Upload Electronic Signature Image
-(void)uploadSignatureImageOnServer:(NSString *)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
        
    }
    else
    {
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainSalesProcess=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"]];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
        }
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        NSString *strCheckUrl;
        strCheckUrl=@"api/File/UploadSignatureAsync";
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainSalesProcess,strCheckUrl];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
    }
    
    
}
-(void)fetchForAppliedDiscountFromCoreData
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadAppliedDiscounts= [NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    [request setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadAppliedDiscount;
    arrLeadAppliedDiscount=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadAppliedDiscount forKey:@"LeadAppliedDiscounts"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                if(![str isKindOfClass:[NSString class]])
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadAppliedDiscounts%@",dictPaymentInfo);
            [arrLeadAppliedDiscount addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadAppliedDiscount forKey:@"LeadAppliedDiscounts"];
    }
}

-(void)fetchTagDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTagDetail=[NSEntityDescription entityForName:@"LeadTagExtDcs" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityTagDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadTagExtDcs"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadTagExtDcs%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadTagExtDcs"];
    }
}

#pragma mark- ----------- Fetch Record For Clark Pest -------------

#pragma mark- ------ Fetch Scope --------

-(void)fetchScopeFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialScopeExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialScopeExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialScopeExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialScopeExtDc"];
    }
    
}
#pragma mark- ------- Fetch Target --------

-(void)fetchTargetFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTargetExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTargetExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                
                if([[arrInfoKey objectAtIndex:i] isEqualToString:@"targetImageDetail"])
                {
                    NSString *strLeadCommercialTargetId, *strMobileTargetId;
                    strLeadCommercialTargetId = [matches valueForKey:@"leadCommercialTargetId"];
                    strMobileTargetId = [matches valueForKey:@"mobileTargetId"];
                    NSMutableArray *arrImage;
                    arrImage = [[NSMutableArray alloc]init];
                    arrImage = [self fetchTargetImageDetail:strLeadCommercialTargetId MobileTargetId:strMobileTargetId];
                    [arrInfoValue addObject:arrImage];
                    
                }
                else
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                    
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                    
                    [arrInfoValue addObject:str];
                }
            }
            
            //Nilind  28 Nov 2019
            NSMutableArray *arrImage;
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *strLeadCommercialTargetId, *strMobileTargetId;
                strLeadCommercialTargetId = [matches valueForKey:@"leadCommercialTargetId"];
                strMobileTargetId = [matches valueForKey:@"mobileTargetId"];
                
                arrImage = [[NSMutableArray alloc]init];
                arrImage = [self fetchTargetImageDetail:strLeadCommercialTargetId MobileTargetId:strMobileTargetId];
            }
            NSMutableDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSMutableDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            [dictPaymentInfo setObject:arrImage forKey:@"targetImageDetailExtDc"];
            
            
//            NSDictionary *dictPaymentInfo;
//            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialTargetExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTargetExtDc"];
    }
    
    [self fetchAllTargetImages];
    
}

#pragma mark- ------- Fetch LeadCommercialInitialInfoFromCoreData --------

-(void)fetchLeadCommercialInitialInfoFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialInitialInfoExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    // arrAllObj=[[NSArray alloc]init];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialInitialInfoExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialInitialInfoExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialInitialInfoExtDc"];
    }
    
    
    
}
#pragma mark- ------- Fetch LeadCommercialMaintInfoFromCoreData --------

-(void)fetchLeadCommercialMaintInfoFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialMaintInfoExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialMaintInfoExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialMaintInfoExtDc"];
    }
    
    
}
#pragma mark- ------- Fetch MultiTermsFromCoreDataClarkPest --------

-(void)fetchMultiTermsFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTermsExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTermsExtDc"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialTermsExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTermsExtDc"];

    }
}

#pragma mark- ------- Fetch LeadCommercialDiscountExtDc --------

-(void)fetchForAppliedDiscountFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialDiscountExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                if(![str isKindOfClass:[NSString class]])
                {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialDiscountExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialDiscountExtDc"];
    }
    
}
#pragma mark- ------- Fetch LeadCommercialDetailExtDc --------


-(void)fetchLeadCommercialDetailExtDcFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadCommercialDetailExtDc"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictLeadPreference;
            dictLeadPreference = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadCommercialDetailExtDc%@",dictLeadPreference);
            [dictFinal setObject:dictLeadPreference forKey:@"LeadCommercialDetailExtDc"];
        }
        
        
    }
    
    
}
#pragma mark- ------- Fetch Sales Marketing Content FromCoreDataClarkPest --------

-(void)fetchSalesMarketingContentFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadMarketingContentExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadMarketingContentExtDc"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadMarketingContentExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadMarketingContentExtDc"];
    }
}

-(NSMutableArray *)fetchTargetImageDetail:(NSString *)strLeadCommercialTargetId MobileTargetId:(NSString *)strMobileTargetId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"TargetImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate;
    
    if (strLeadCommercialTargetId.length > 0)
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && leadCommercialTargetId=%@" ,strLeadId, strLeadCommercialTargetId];
    }
    else
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && mobileTargetId=%@",strLeadId,strMobileTargetId];
    }
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjtargetImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    NSManagedObject *matchesTargetImage;
    if (arrAllObjtargetImage.count==0)
    {
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObjtargetImage.count; k++)
        {
            matchesTargetImage=arrAllObjtargetImage[k];
            NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matchesTargetImage valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                
               // [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matchesTargetImage entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictPaymentInfo];
            NSLog(@"TargetImageDetailImageDetail%@",arrImageDetail);
        }
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    return arrImageDetail;
}

-(void)fetchAllTargetImages
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"TargetImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate;
    predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjtargetImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    arrAllTargetImages = [[NSMutableArray alloc]init];
    NSManagedObject *matchesTargetImage;
    if (arrAllObjtargetImage.count==0)
    {
        
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObjtargetImage.count; k++)
        {
            matchesTargetImage=arrAllObjtargetImage[k];
            NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matchesTargetImage valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                
                [arrAllTargetImages addObject:strImageName];
                
            }
        }
        
        
    }
    
}
-(void)uploadTargetImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
        }
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        //NSString *strCheckUrl;
        //strCheckUrl=@"/api/File/UploadCheckImagesAsync";
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlUploadTargetImage];//UrlSalesImageUpload];//
        //NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesImageUpload];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
    }
    
    
}
-(void)uploadGraphXml :(NSString*)strXmlName
{
    
    if ((strXmlName.length==0) && [strXmlName isEqualToString:@"(null)"]) {
        
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        /*strXmlName = [global strDocNameFromPath:strXmlName];

        NSRange equalRange = [strXmlName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strXmlName = [strXmlName substringFromIndex:equalRange.location + equalRange.length];
        }*/
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strXmlName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlUploadXMLSales];

        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strXmlName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response On Graph XML Upload = = = =  = = %@",returnString);
        
        if ([returnString isEqualToString:@"OK"])
        {
            
            NSLog(@"Graph XML Sent");
            
        }
       
    }
    
}

-(void)endEditing
{
    [self.view endEditing:YES];
}

#pragma mark: ------------- Default Emails  ------------------

///New Change For Email Customs

-(void)deleteDefaultEmailFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isDefaultEmail=%@",strLeadId,@"true"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self deleteDefaultEmailFromDB1];
    
}

-(void)deleteDefaultEmailFromDB1{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isDefaultEmail=%@",strLeadId,@"1"];

    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)saveEmailToCoreDataDefaults:(NSMutableArray *)arrEmailDetail :(NSMutableArray *)arrOfDefaultsEmailsInvoiceId
{
    for (int j=0; j<arrEmailDetail.count; j++)
    {
        
        if ([arrEmail containsObject:[arrEmailDetail objectAtIndex:j]]) {
            
            
            
        } else {

        //Email Detail Entity
        entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
        EmailDetail *objEmailDetail = [[EmailDetail alloc]initWithEntity:entityEmailDetail insertIntoManagedObjectContext:context];
        
        objEmailDetail.createdBy=@"0";;
        objEmailDetail.createdDate=@"0";
        objEmailDetail.emailId=[arrEmailDetail objectAtIndex:j];
        objEmailDetail.isCustomerEmail=@"true";
        objEmailDetail.isMailSent=@"true";
        objEmailDetail.leadMailId=@"";
        objEmailDetail.isDefaultEmail=@"false";
        objEmailDetail.modifiedBy=@"";
        objEmailDetail.modifiedDate=[global modifyDate];
        objEmailDetail.subject=@"";
        objEmailDetail.leadId=strLeadId;
        objEmailDetail.userName=strUserName;
        objEmailDetail.companyKey=strCompanyKey;
        objEmailDetail.isDefaultEmail=@"true";
        objEmailDetail.isMailSent=@"false";

        //objEmailDetail.woInvoiceMailId=[arrOfDefaultsEmailsInvoiceId objectAtIndex:j];
        
        NSError *error1;
        
        [context save:&error1];
            
        }
        
    }
}
-(void)saveEmployeeEmailDefaultsTrue{
    
    //Email Detail Entity
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    EmailDetail *objEmailDetail = [[EmailDetail alloc]initWithEntity:entityEmailDetail insertIntoManagedObjectContext:context];
    
    objEmailDetail.createdBy=@"0";;
    objEmailDetail.createdDate=@"0";
    objEmailDetail.emailId=strDefaultEmployeeEmail;
    objEmailDetail.isCustomerEmail=@"true";
    objEmailDetail.isMailSent=@"true";
    objEmailDetail.leadMailId=@"";
    objEmailDetail.isDefaultEmail=@"false";
    objEmailDetail.modifiedBy=@"";
    objEmailDetail.modifiedDate=[global modifyDate];
    objEmailDetail.subject=@"";
    objEmailDetail.leadId=strLeadId;
    objEmailDetail.userName=strUserName;
    objEmailDetail.companyKey=strCompanyKey;
    objEmailDetail.isDefaultEmail=@"true";
    objEmailDetail.isMailSent=@"false";

    NSError *error1;
    
    [context save:&error1];
    
}

-(void)fetchDefaultEmailFromDb{
    
    NSUserDefaults *defsdictMasters=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictMasters=[defsdictMasters valueForKey:@"MasterSalesAutomation"];
    
    NSManagedObject *objTemp = [global fetchLeadDetailFromDb:strLeadId];

    NSString *strBranchSysName=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"branchSysName"]];

    NSArray *arrOfDefaultEmailsDB=[dictMasters valueForKey:@"DefaultEmails"];
    
    if ([arrOfDefaultEmailsDB isKindOfClass:[NSArray class]]) {
        
        NSMutableArray *arrOfDefaultsEmails=[[NSMutableArray alloc]init];
        NSMutableArray *arrOfDefaultsEmailsInvoiceId=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfDefaultEmailsDB.count; k++) {
            
            NSDictionary *dictData=arrOfDefaultEmailsDB[k];
            
            NSString *strBranchSysNameLocal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BranchSysName"]];

            if ([strBranchSysName isEqualToString:strBranchSysNameLocal]) {
                
                NSString *strEmailIds=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EmailId"]];
                if ([strEmailIds isEqualToString:@"##EmployeeEmail##"]) {
                    
                    if ([arrEmail containsObject:strDefaultEmployeeEmail]) {
                        
                        
                        
                    } else {
                        
                        [self saveEmployeeEmailDefaultsTrue];
                        
                    }
                    
                } else {
                    
                    [arrOfDefaultsEmails addObject:strEmailIds];
                    [arrOfDefaultsEmailsInvoiceId addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WoInvoiceMailId"]]];
                    
                }
                
            }
            
        }

        [self saveEmailToCoreDataDefaults:arrOfDefaultsEmails :arrOfDefaultsEmailsInvoiceId];
        
    } else {
        
    }
}
//============================================================================
#pragma mark- --------------- Upload Other Document METHOD -------------------
//============================================================================
//============================================================================
-(void)uploadOtherDocumentImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
            
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,@"/api/File/UploadOtherDocsAsync"];
            //http://192.168.0.218:3333//api/File/UploadAsync
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Document Sent");
            }
            NSLog(@"Document Sent");
            
        }
    
    }
}

-(void)uploadOtherDocuments :(NSString*)strDocName DocumentId:(NSString*)strDocId
{
    NSString* ServerUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,@"/api/File/UploadOtherDocsAsync"];
    
    if ((strDocName.length==0) && [strDocName isEqualToString:@"(null)"]) {
        
    } else {
        
        strDocName = [global strDocNameFromPath:strDocName];
        
        NSRange equalRange = [strDocName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strDocName = [strDocName substringFromIndex:equalRange.location + equalRange.length];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strDocName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@",ServerUrl];
        
        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strDocName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response On Document Upload = = = =  = = %@",returnString);
        
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Document Sent");
        }
        if (returnString.length > 0)
        {
            [self updateOtherDocumentSyncStatus:strDocId];
        }
//[{"Name":"pdfOther337825_2020618163749.pdf","Extension":".pdf","RelativePath":"UploadImages\\","Length":500337}]
        
        NSLog(@"Document Sent");
        
    }
}
-(void)fetchOtherDocument
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && docType=%@",strLeadId,@"Other"];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];

    NSArray *arrAllObjOtherDoucment = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    arrUploadOtherDocument = [[NSMutableArray alloc]init];
    arrUploadOtherDocumentId = [[NSMutableArray alloc]init];

    if (arrAllObjOtherDoucment.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjOtherDoucment.count; k++)
        {
            NSManagedObject *matchesTemp = [arrAllObjOtherDoucment objectAtIndex:k];
            if ([[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isToUpload"]] isEqualToString:@"true"])
            {
                [arrUploadOtherDocument addObject:[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"fileName"]]];
                [arrUploadOtherDocumentId addObject:[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"leadDocumentId"]]];

            }
        }
    }
}

-(void)updateOtherDocumentSyncStatus : (NSString *)strDocId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && docType=%@ && leadDocumentId=%@",strLeadId,@"Other",strDocId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjTemp = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObjTemp.count==0)
    {
        
    }else
    {
        NSManagedObject *matchesTemp=arrAllObjTemp[0];
        [matchesTemp setValue:@"false" forKey:@"isToUpload"];
        [context save:&error1];
    }
}

-(BOOL)checkOtherDocumentSyncStatus:(NSString *)strDocId
{
    BOOL chkStatus;
    chkStatus = NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && docType=%@ && leadDocumentId = %@",strLeadId,@"Other",strDocId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];

    NSArray *arrAllObjOtherDoucment = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    if (arrAllObjOtherDoucment.count==0)
    {
        
    }else
    {
        NSManagedObject *matchesTemp = [arrAllObjOtherDoucment objectAtIndex:0];
        if([[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isSync"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isSync"]] isEqualToString:@"True"] || [[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isSync"]] isEqualToString:@"1"])
        {
            chkStatus = YES;
        }
        else
        {
            chkStatus = NO;
        }
    }
    return chkStatus;
}

-(void)goToAppointment
{
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
        AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
        AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
#pragma mark: ------------ Email Subject Line Change -----------

-(NSString *)setDefaultEmailSubject : (NSString *)strAgreementType
{
    NSString *strFinalSubject;
    strFinalSubject = @"";
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    
    NSArray *arrEmailTemplate = [[NSArray alloc]init];
    
    arrEmailTemplate = [dictMasters valueForKey:@"EmailTemplates"];
    
    if ([arrEmailTemplate isKindOfClass:[NSArray class]])
        
    {
        if (arrEmailTemplate.count > 0)
        {
            for (int i=0; i <arrEmailTemplate.count ; i++)
            {
                NSDictionary *dict = [arrEmailTemplate objectAtIndex:i];
                
                if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]] isEqualToString:strAgreementType])
                {
                    strFinalSubject = [self handleTag:[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmailSubject"]]];
                    break;
                }
                
            }
        }
    }

    return  strFinalSubject;
}
-(NSString *)handleTag:(NSString *)strFinalMessage
{
    NSString *strFinalString = @"";
    
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##AccountNo##" withString:strAccountNo];
    
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##CustomerCompanyName##" withString:strCustomerCompanyName];
    
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##CustomerName##" withString:strCustomerName];

    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##EmployeeName##" withString:strEmpName];

    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##EmployeEmail##" withString:strEmpEmail];

    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##EmployeePhone##" withString:strEmpPhone];
    
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##CompanyName##" withString:strCompanyName];
    
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##OpportunityContactName##" withString:strUserName];

    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##OpportunityFirstName##" withString:strFirstName];

    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##OpportunityMiddleName##" withString:strMiddleName];
    
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##OpportunityLastName##" withString:strLastName];

    //-Navin
    
    
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##ProposedServices##" withString:strProposedServices];
   
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##SoldServices##" withString:strSoldServices];
   
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##Services##" withString:strServices];
  
    NSString *strCurrentDate = [global strCurrentDateFormatted:@"MM/dd/yyyy" :@""];
    
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##CurrentDate##" withString:strCurrentDate];
    
    strFinalMessage = [strFinalMessage stringByReplacingOccurrencesOfString:@"##ScheduleDate##" withString:strScheduleDate];

    //--END
    

    
    strFinalString = strFinalMessage;
    
    return strFinalString;
}
-(void)updateEmailSubject
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetail= [NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    [request setEntity:entityEmailDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrObjEmail = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject  *matchesEmail;
    if (arrObjEmail.count==0)
    {
        
    }
    else
    {
        for (int i=0;i<arrObjEmail.count;i++)
        {
            matchesEmail = [arrObjEmail objectAtIndex:i];
            [matchesEmail setValue:_txtFldSubjectEmail.text forKey:@"subject"];
            [context save:&error1];
        }
    }
}

-(void)fetchLeadData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjDetail = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjDetail.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjDetail.count; k++)
        {
            NSManagedObject *matchesObject=arrAllObjDetail[k];
            NSLog(@"Lead IDDDD====%@",[matchesObject valueForKey:@"leadId"]);
            
            //New Change
            strLeadNumber=[matchesObject valueForKey:@"leadNumber"];
            strAccountNo=[matchesObject valueForKey:@"accountNo"];
            strProposal=[matchesObject valueForKey:@"isProposalFromMobile"];
           
            //-Navin
            strScheduleDate=[NSString stringWithFormat:@"%@",[global ChangeDateToLocalDateOther:[NSString stringWithFormat:@"%@",[matchesObject valueForKey:@"scheduleStartDate"]]]];
            //--End--
          
            if ([strProposal isEqualToString:@"true"])
            {
                strProposal=@"YES";
            }
            else
            {
                strProposal=@"NO";
                
            }
            isResendEmailCheck=[matchesObject valueForKey:@"isResendAgreementProposalMail"];
            
            strCustomerName = [matchesObject valueForKey:@"customerName"];
            strCustomerCompanyName = [matchesObject valueForKey:@"companyName"];
            strFirstName = [matchesObject valueForKey:@"firstName"];
            strMiddleName = [matchesObject valueForKey:@"middleName"];
            strLastName = [matchesObject valueForKey:@"lastName"];
            

            if ([[NSString stringWithFormat:@"%@",[matchesObject valueForKey:@"statusSysName"]] isEqualToString:@"Complete"] && [[NSString stringWithFormat:@"%@",[matchesObject valueForKey:@"stageSysName"]] isEqualToString:@"Won"])
            {
                _txtFldSubjectEmail.text = [self setDefaultEmailSubject:@"SalesAgreement"];
            }
            else if ([[NSString stringWithFormat:@"%@",[matchesObject valueForKey:@"statusSysName"]] isEqualToString:@"Complete"] && [[NSString stringWithFormat:@"%@",[matchesObject valueForKey:@"stageSysName"]] isEqualToString:@"CompletePending"])
            {
                _txtFldSubjectEmail.text = [self setDefaultEmailSubject:@"CustomerSignatureLink"];

            }
            else if ([[NSString stringWithFormat:@"%@",[matchesObject valueForKey:@"statusSysName"]] isEqualToString:@"Open"] && [[NSString stringWithFormat:@"%@",[matchesObject valueForKey:@"stageSysName"]] isEqualToString:@"Proposed"])
            {
                _txtFldSubjectEmail.text = [self setDefaultEmailSubject:@"SalesProposal"];

            }
            else
            {
                _txtFldSubjectEmail.text = @"";

            }

        }
    }
}
-(void)fetch_NewServiceData{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];

    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjNew;
    arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesNew;
   
    NSMutableArray *arrSoldService_New, *arrUnSoldService_New, *arrAllService_New;
    arrSoldService_New = [NSMutableArray new];
    arrUnSoldService_New = [NSMutableArray new];
    arrAllService_New = [NSMutableArray new];

   
    for (int k=0; k<arrAllObjNew.count; k++)
    {
        matchesNew=arrAllObjNew[k];
        
        if([[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"isSold"]]isEqualToString:@"true"])
        {
            
            if ([[matchesNew valueForKey:@"serviceSysName"]isEqualToString:@""] || [[matchesNew valueForKey:@"serviceSysName"]isEqualToString:@"(null)"])
            {
                [arrSoldService_New addObject:@""];
            }
            else
            {
                [arrSoldService_New addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceSysName"]]]];
            }
            
        }
        else if([[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"isSold"]]isEqualToString:@"false"]){
            
            if ([[matchesNew valueForKey:@"serviceSysName"]isEqualToString:@""] || [[matchesNew valueForKey:@"serviceSysName"]isEqualToString:@"(null)"])
            {
                [arrUnSoldService_New addObject:@""];
            }
            else
            {
                [arrUnSoldService_New addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceSysName"]]]];
            }
            
        }
        
        if ([[matchesNew valueForKey:@"serviceSysName"]isEqualToString:@""] || [[matchesNew valueForKey:@"serviceSysName"]isEqualToString:@"(null)"])
        {
            [arrAllService_New addObject:@""];
        }
        else
        {
            [arrAllService_New addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceSysName"]]]];
        }
    }
    
    //FetchNonstandard Service
    
    NSArray *arrNonStanobj;
    
    arrNonStanobj = [self fetchNonStandardServiceData];
    
    for (int k=0; k<arrNonStanobj.count; k++)
    {
        matchesNew=arrNonStanobj[k];
        

        if([[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"isSold"]]isEqualToString:@"true"])
        {
            [arrSoldService_New addObject:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceName"]]];
        }
        else if([[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"isSold"]]isEqualToString:@"false"])
        {
            [arrUnSoldService_New addObject:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceName"]]];
        }
        
        [arrAllService_New addObject:[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"serviceName"]]];
    }
    
    //End
    
    arrSoldService_New = [[arrSoldService_New sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
    arrUnSoldService_New = [[arrUnSoldService_New sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
    arrAllService_New = [[arrAllService_New sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
    
    
    strSoldServices = [arrSoldService_New componentsJoinedByString:@","];
    strProposedServices = [arrUnSoldService_New componentsJoinedByString:@","];
    strServices = [arrAllService_New componentsJoinedByString:@","];

}
-(NSArray *)fetchNonStandardServiceData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceNonStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjNew;
    arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    return  arrAllObjNew;
}
@end

