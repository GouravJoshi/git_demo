//
//  SalesAutomationInspection.m
//  DPS
//
//  Created by Rakesh Jain on 19/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
#import "AllImportsViewController.h"
#import "SalesAutomationInspectioniPad.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "AppDelegate.h"
#import "SalesAutomationSelectService.h"
#import "SalesDynamicInspection+CoreDataProperties.h"
#import "SalesDynamicInspection.h"
#import <SafariServices/SafariServices.h>
#import "GlobalSyncViewController.h"
#import "CreditCardIntegration.h"
#import <CoreLocation/CoreLocation.h>
#import "DPS-Swift.h"

@interface SalesAutomationInspectioniPad ()<UITableViewDelegate,UITextViewDelegate,UITextFieldDelegate,UITableViewDataSource,CLLocationManagerDelegate>
{
    Global *global;
    NSDictionary *ResponseDict,*dictForAppoint;
    
    UIButton *btnTemp;
    UILabel *lblTemp,*lblBorder,*lblBorder2;
    NSArray *arrOfLeads;
    NSMutableArray *arrResponseInspection;
    int heightViewForm,yViewForm;
    int heightSection,ySection;
    UIView *MainViewForm,*ViewFormSections,*viewForm,*viewSection;
    UIButton *BtnMainView;
    
    //New change on 24 august 2016 for dynamic form
    NSMutableArray *arrayViews;
    NSMutableArray *arrayOfButtonsMain;
    NSMutableArray *arrOfHeightsViews;
    NSMutableArray *arrOfYAxisViews;
    NSMutableArray *arrOfButtonImages;
    
    //For DropDown i.e TAble View
    UITableView *tblData;
    NSMutableArray *arrDataTblView;
    UIView *viewBackGround;
    NSMutableArray *arrOfIndexes;
    NSMutableArray *arrOfControlssMain;
    BOOL isMultipleSelect;
    UIView *viewForDate;
    UIDatePicker *pickerDate;
    NSMutableArray *arraySelecteValues,*arraySelecteValuesToShow;
    int indexMultipleSelectTosetValue;
    NSString *strLeadId,*strGlobalDateToShow,*strGlobalDatenTime,*strLeadStatusGlobal;
    NSString *strEmpID,*strCompanyKey,*strCompanyId,*strServiceUrlMain,*strUserName,*strSalesUrlMain,*strGlobalDropDownSelectedValue;
    NSDictionary *dictDeptNameByKey;
    
    
    //Nilind 14 Sept Image Change
    
    BOOL chkForDuplicateImageSave,isEditedInSalesAuto;
    
    NSMutableArray *arrNoImage;
    
    
    //Nilind 07 Jun ImageCaption
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    NSMutableArray *arrOfImageCaption,*arrOfImageDescription,*arrOfImageCaptionGraph,*arrOfImageDescriptionGraph;
    NSMutableArray *arrImagePath,*arrGraphImage;
    
    // Nilind 23 Nov
    
    NSMutableArray *arrImageLattitude,*arrImageLongitude,*arrImageGraphLattitude,*arrImageGraphLongitude;
    NSString *strStageSysName;
    
}
@end

@implementation SalesAutomationInspectioniPad

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    strStageSysName = @"";
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strLeadStatusGlobal=[NSString stringWithFormat:@"%@",[defsLogindDetail valueForKey:@"leadStatusSales"]];
    strStageSysName=[NSString stringWithFormat:@"%@",[defsLogindDetail valueForKey:@"stageSysNameSales"]];
    
    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
    {
        
        strLeadStatusGlobal=@"Complete";
        
    }
    isEditedInSalesAuto=NO;
    chkForDuplicateImageSave=NO;
    
    
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    // strSalesUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    if (strLeadId.length==0) {
        // strLeadId=@"141";
    }
    _lbl_NameNaccountNo.text=[defsLead valueForKey:@"lblName"];
    _lblNameTitle.text=[defsLead valueForKey:@"nameTitle"];
    _lbl_NameNaccountNo.font=[UIFont boldSystemFontOfSize:26];
    _lblNameTitle.font=[UIFont boldSystemFontOfSize:26];
    
    _lbl_NameNaccountNo.textAlignment=NSTextAlignmentCenter;
    
    isMultipleSelect=NO;
    indexMultipleSelectTosetValue=-100000;
    //============================================================================
    //============================================================================
    
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate:[NSDate date]];
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
    arrayViews = [[NSMutableArray alloc] init];
    arrayOfButtonsMain = [[NSMutableArray alloc] init];
    arrOfHeightsViews = [[NSMutableArray alloc] init];
    arrOfYAxisViews = [[NSMutableArray alloc] init];
    arrOfButtonImages = [[NSMutableArray alloc] init];
    
    heightViewForm=0;yViewForm=0;
    heightSection=0;ySection=0;
    viewForm=[[UIView alloc]init];
    viewSection=[[UIView alloc]init];
    global = [[Global alloc] init];
    [self getClockStatus];
    
    //Comment hata na hai yad se
    [self fetchDepartmentNameBySysName];
    [self salesAutomationInspection];
    
    // [self dynamicFormCreation];
    
    btnTemp=[[UIButton alloc]init];
    lblTemp=[[UILabel alloc]init];
    btnTemp.frame=CGRectMake(0, 0, 150+50, 50);
    lblTemp.frame=CGRectMake(0, 0, 150+50, 50);
    arrOfLeads=[[NSMutableArray alloc]init];
    arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Serivce",@"Service Summary",@"Agreement", nil];
    _scrollViewLeads.delegate=self;
    //CGFloat scrollWidth=arrOfLeads.count*110;
    CGFloat scrollWidth=arrOfLeads.count*(160+40+20);
    
    [_scrollViewLeads setFrame:CGRectMake(0,_viewTop.frame.origin.y+_viewTop.frame.size.height,[UIScreen mainScreen].bounds.size.width,_scrollViewLeads.frame.size.height)];
    [_scrollViewLeads setContentSize:CGSizeMake(scrollWidth,100)];
    
    
    _scrollViewLeads.showsHorizontalScrollIndicator = NO;
    int var = 30;
    if([UIScreen mainScreen].bounds.size.width > 1300)
    {
        var = 50;
    }
    lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-5, (150+50+var)*2, 3)];

    
    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        lblBorder.frame=CGRectMake(0,_scrollViewLeads.contentSize.height-3, 100*2+5, 2);
    }
    lblBorder.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-5, _scrollViewLeads.frame.size.width+btnTemp.frame.size.width-35, 3)];
    lblBorder2.backgroundColor=[UIColor lightGrayColor];
    
    [_scrollViewLeads addSubview:lblBorder2];
    [_scrollViewLeads addSubview:lblBorder];
    _scrollViewLeads.backgroundColor=[UIColor whiteColor];
    [_scrollViewLeads addSubview:btnTemp];
    [_scrollViewLeads addSubview:lblTemp];
    [self addButtons];
    //_cnstrnScrollDynamic_H.constant=1500;
    
    NSString *strUrl;
    strUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",strServiceUrlMain,UrlGetSalesInspectionDynamicForm,strCompanyKey,UrlGetSalesInspectionDynamicFormleadID,strLeadId,UrlGetSalesInspectionDynamicFormInspectionId,@"0"];
    
    NSLog(@"Dynamic Form Url is====%@",strUrl);
    
    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
    {
        _btnGlobalSync.enabled=NO;
    }
    else
    {
        _btnGlobalSync.enabled=YES;
    }
    
    //Nilind 14 sEPT
    chkForDuplicateImageSave=NO;
    arrNoImage=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImageLattitude=[[NSMutableArray alloc]init];
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    arrImagePath=[[NSMutableArray alloc]init];
    [self fetchImageDetailFromDataBase];
    
    //End
    
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeOrientation:) name:UIDeviceOrientationDidChangeNotification object:nil];

    
}


 - (void)changeOrientation:(NSNotification *)notification
{
    arrayViews = [[NSMutableArray alloc] init];
    arrayOfButtonsMain = [[NSMutableArray alloc] init];
    arrOfHeightsViews = [[NSMutableArray alloc] init];
    arrOfYAxisViews = [[NSMutableArray alloc] init];
    arrOfButtonImages = [[NSMutableArray alloc] init];
    
    heightViewForm=0;yViewForm=0;
    heightSection=0;ySection=0;
    viewForm=[[UIView alloc]init];
    viewSection=[[UIView alloc]init];
    for (UIView *view in _scrollForDynamicView.subviews)
    {
        [view removeFromSuperview];
    }
    for (UILabel *view in _scrollForDynamicView.subviews)
    {
        [view removeFromSuperview];
    }
    for (UIButton *view in _scrollForDynamicView.subviews)
    {
        [view removeFromSuperview];
    }
    for (UIImageView *view in _scrollForDynamicView.subviews)
    {
        [view removeFromSuperview];
    }
    for (UITextField *view in _scrollForDynamicView.subviews)
    {
        [view removeFromSuperview];
    }
    for (UITextView *view in _scrollForDynamicView.subviews)
    {
        [view removeFromSuperview];
    }
    for (UIImageView *view in _scrollForDynamicView.subviews)
    {
        [view removeFromSuperview];
    }
    
    [[_scrollForDynamicView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for(UIView *subview in [_scrollForDynamicView subviews]) {
        [subview removeFromSuperview];
    }
    
    [self dynamicFormCreation];
  
    //[self dynamicFormCreation];
}

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    [_btnSavenContinue setEnabled:NO];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    [_btnSavenContinue setEnabled:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    strLeadStatusGlobal=[NSString stringWithFormat:@"%@",[defsClock valueForKey:@"leadStatusSales"]];
    strStageSysName=[NSString stringWithFormat:@"%@",[defsClock valueForKey:@"stageSysNameSales"]];
    
    
    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
    {
        _btnGlobalSync.enabled=NO;
        //[self dynamicFormCreation];
    }
    else
    {
        _btnGlobalSync.enabled=YES;
    }
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    [self fetchImageDetailFromDataBaseForGraph];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage)
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        //        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        //        EditImageViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewController"];
        //        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        DrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    }
    else
    {
        
        //Nilind 07 June Image Caption
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isBackFromSelectService"];
        
        if (isFromBack)
        {
            
            //arrOfBeforeImageAll=nil;
            //arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            arrNoImage=[[NSMutableArray alloc]init];
            
            arrImageLattitude=nil;
            arrImageLattitude=[[NSMutableArray alloc]init];
            arrImageLongitude=nil;
            arrImageLongitude=[[NSMutableArray alloc]init];
            
            arrImageGraphLattitude=nil;
            arrImageGraphLattitude=[[NSMutableArray alloc]init];
            arrImageGraphLongitude=nil;
            arrImageGraphLongitude=[[NSMutableArray alloc]init];
            
            
            
            [defsBack setBool:NO forKey:@"isBackFromSelectService"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseForGraph];
            
            [self fetchImageDetailFromDataBase];
            
        }
        
        
        
        //End
        
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreviewSales"];
        if (yesFromDeleteImage)
        {
            isEditedInSalesAuto=YES;
            NSLog(@"Database edited");
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreviewSales"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrNoImage.count; k++)
            {
                
                NSDictionary *dictdat=arrNoImage[k];
                NSString *strImageName;
                if([dictdat isKindOfClass:[NSDictionary class]])
                {
                    strImageName=[dictdat valueForKey:@"leadImagePath"];
                }
                else
                {
                    strImageName=arrNoImage[k];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImagesSales"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrNoImage[k]];
                        
                    }
                }
                
            }
            if (!(arrTempBeforeImage.count==0))
            {
                
                [arrNoImage removeObjectsInArray:arrTempBeforeImage];
            }
            else if((arrTempBeforeImage.count==0))
            {
                arrNoImage=nil;
                arrNoImage=[[NSMutableArray alloc]init];
            }
            
            //nIlind 09 April
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImagesSales"];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            
            
            [defsnew synchronize];
            
            [self saveImageToCoreData];
            
            //End
            
        }
        else
        {
            
        }
        //Nilind 07 June Image Caption
        
        
        //Change in image Captions
        BOOL yesEditedImageCaptionGraph=[defsSS boolForKey:@"yesEditedImageCaptionGraph"];
        if (yesEditedImageCaptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescriptionGraph=[defsSS boolForKey:@"yesEditedImageDescriptionGraph"];
        if (yesEditedImageDescriptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            [self saveImageToCoreData];
            
        }
        
        //End
        
        
        [self downloadingImagesThumbNailCheck];
        //Nilind 10 Jan
        
        //Nilind 03 Oct
        
        //End
        
    }
    //...........
    [_collectionViewAfterImage reloadData];
    [_collectionViewGraphImage reloadData];
    
}

//eND

//============================================================================
#pragma mark- DYNAMIC LABEL AND BUTTON METHOD
//============================================================================

-(void)addButtons
{
    int var = 30;
    if([UIScreen mainScreen].bounds.size.width > 1300)
    {
        var = 50;
    }
    btnTemp=[[UIButton alloc]init];
    lblTemp=[[UILabel alloc]init];
    btnTemp.frame=CGRectMake(0, 0, 150+50+var, 50);
    lblTemp.frame=CGRectMake(0, 0, 150+50+var, 50);
   
    for(int i =0;i<arrOfLeads.count;i++)
    {
        
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*(100+var)+20, 0, 50, 50)];
        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
        lbl.textColor = [UIColor whiteColor];
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 25;
        lbl.layer.borderWidth=2.0;
        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:14]];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        if (i==0)
        {
            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 150+var+50, 50);
            lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 50, 50);
            lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
            lbl.textColor = [UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        else
        {
            
            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 150+var+50, 50);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 50, 50);
            lbl.textColor = [UIColor darkGrayColor];
            lbl.backgroundColor=[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            
        }
        for(int j=0;j<=1;j++)
        {
            if (i==j)
            {
                
                
                lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
                lbl.textColor = [UIColor whiteColor];
                lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        }
        
        
        btnTemp.frame=btn.frame;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
        
        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
        btn.titleLabel.numberOfLines=2;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i;
        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
        [_scrollViewLeads addSubview:btn];
        
        [_scrollViewLeads addSubview:lbl];
    }
    
    
    CGFloat scrollWidth=arrOfLeads.count*(160+40+20+var);
    
    [_scrollViewLeads setFrame:CGRectMake(0,_viewTop.frame.origin.y+_viewTop.frame.size.height,[UIScreen mainScreen].bounds.size.width,_scrollViewLeads.frame.size.height)];
    [_scrollViewLeads setContentSize:CGSizeMake(scrollWidth,100)];
    _scrollViewLeads.showsHorizontalScrollIndicator = NO;
    
    
    //============================================================================
    //============================================================================
  //  lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-5, 150+50+var, 3)];
    
  //  lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-5, _scrollViewLeads.frame.size.width+btnTemp.frame.size.width-35, 3)];
    
    lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-5, (150+50+var)*2, 3)];
    lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-5, _scrollViewLeads.frame.size.width+btnTemp.frame.size.width-35, 3)];
    
    
    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        lblBorder.frame=CGRectMake(0,_scrollViewLeads.contentSize.height-2, 100, 2);
    }
    lblBorder.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    
    lblBorder2.backgroundColor=[UIColor lightGrayColor];
}
-(void)addButtonsOrignal
{
    for(int i =0;i<arrOfLeads.count;i++)
    {
        
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*100+20, 0, 50, 50)];
        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
        lbl.textColor = [UIColor whiteColor];
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 25;
        lbl.layer.borderWidth=1.0;
        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:14]];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        if (i==0)
        {
            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
            lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 50, 50);
            lbl.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
            lbl.textColor =  [UIColor lightGrayColor];//[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        else
        {
            
            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 50, 50);
            lbl.textColor = [UIColor darkGrayColor];
            lbl.backgroundColor=[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            
        }
        for(int j=0;j<=1;j++)
        {
            if (i==j)
            {
                
                
                lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
                lbl.textColor = [UIColor whiteColor];
                lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        }
        
        btnTemp.frame=btn.frame;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
        
        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
        btn.titleLabel.numberOfLines=20000;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i;
        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
        // [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollViewLeads addSubview:btn];
        
        [_scrollViewLeads addSubview:lbl];
    }/*
      {
      for(int i =0;i<arrOfLeads.count;i++)
      {
      
      UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*100+20, 0, 50, 50)];
      lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
      lbl.textColor = [UIColor whiteColor];
      lbl.layer.masksToBounds = YES;
      lbl.layer.cornerRadius = 25;
      lbl.layer.borderWidth=2.0;
      [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:14]];
      lbl.textAlignment = NSTextAlignmentCenter;
      
      UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
      if (i==0)
      {
      btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
      lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 50, 50);
      lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
      lbl.textColor = [UIColor whiteColor];
      lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
      }
      else
      {
      
      btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
      lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 50, 50);
      lbl.textColor = [UIColor darkGrayColor];
      lbl.backgroundColor=[UIColor whiteColor];
      lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
      
      
      }
      btnTemp.frame=btn.frame;
      [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
      
      //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
      
      
      btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
      btn.titleLabel.numberOfLines=2;
      btn.titleLabel.textAlignment=NSTextAlignmentCenter;
      [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
      btn.tag = i;
      NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
      [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
      // [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
      [_scrollViewLeads addSubview:btn];
      
      [_scrollViewLeads addSubview:lbl];
      }
      }
      */
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//============================================================================
//============================================================================
#pragma mark- SalesAutomation API
//============================================================================
//============================================================================

-(void)salesAutomationInspection
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [self FetchFromCoreDataToShowSalesDynamic];
    }
    else
    {
        
        [self FetchFromCoreDataToShowSalesDynamicInCaseOnline];
        
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)actionOnBack:(id)sender
{
    [self endEditing];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"backfromDynamicView"];
    [defs setBool:YES forKey:@"backFromInspectionNew"];
    
    [defs synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionOnSave:(id)sender
{
    [self endEditing];
    
    
    //    // Listen for keyboard appearances and disappearances
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardDidShow:)
    //                                                 name:UIKeyboardDidShowNotification
    //                                               object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardDidHide:)
    //                                                 name:UIKeyboardDidHideNotification
    //                                               object:nil];
    
    //START new change for email valid overall
    BOOL isEmailValid;
    int indexOfControl,indexToBlinkCursor;
    isEmailValid=YES;
    indexOfControl=-1;
    indexToBlinkCursor=-1;
    
    NSString *errorMessageTemp;
    for (int k=0; k<arrResponseInspection.count; k++) {
        
        NSDictionary *dictData=arrResponseInspection[k];
        NSArray *arrSectionsTemp=[dictData valueForKey:@"Sections"];
        
        for (int j=0; j<arrSectionsTemp.count; j++) {
            
            NSDictionary *dictDataControls=arrSectionsTemp[j];
            NSArray *arrOfControls=[dictDataControls valueForKey:@"Controls"];
            
            for (int l=0; l<arrOfControls.count; l++) {
                
                NSDictionary *dictOfOptions=arrOfControls[l];
                if ([[dictOfOptions valueForKey:@"Element"] isEqualToString:@"el-email"]) {
                    
                    indexOfControl++;
                    
                    NSString *strValueee=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
                    
                    if (strValueee.length==0) {
                        // isEmailValid=YES;
                        
                    }else{
                        
                        strValueee =  [strValueee stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        errorMessageTemp=[self validationCheck :strValueee :@"el-email"];
                        
                        if (errorMessageTemp) {
                            
                            isEmailValid=NO;
                            indexToBlinkCursor=indexOfControl;
                            
                        }else{
                            
                            //  isEmailValid=YES;
                            
                        }
                    }
                }else{
                    
                    indexOfControl++;
                    
                }
            }
        }
    }
    
    //    if (indexToBlinkCursor==-1) {
    //
    //    }else{
    //
    //        if ([arrOfControlssMain[indexToBlinkCursor] isKindOfClass:[UITextField class]]) {
    //            UITextField *firld=arrOfControlssMain[indexToBlinkCursor];
    //            [firld resignFirstResponder];
    //        }
    //    }
    
    //END new change for email valid overall
    
    if (isEmailValid) {
        
        NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
        [defss setObject:arrResponseInspection forKey:@"salesDynamicForm"];
        [defss synchronize];
        
        [self saveToCoreDataSalesDynamicFinal:arrResponseInspection];
        
        [self goToNextScreen];
        
    }else{
        
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :@"Please enter valid email id only"];
        
    }
    //[self sendingFinalDynamicJsonToServer];
}

- (IBAction)action_Cancel:(id)sender {
    
    [self endEditing];
    /* UIStoryboard *storyBoar=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
     SalesAutomationInspection *objSaleAutoInspection=[storyBoar instantiateViewControllerWithIdentifier:@"SalesAutomationInspection"];
     [self.navigationController pushViewController:objSaleAutoInspection animated:YES];*/
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"backfromDynamicView"];
    [defs setBool:YES forKey:@"backFromInspectionNew"];
    [defs synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)dynamicFormCreation{
    
    
    
    //    //Temporary HAi Comment KArna Yad SE
    //    arrResponseInspection=[[NSMutableArray alloc]init];
    //    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    //    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"DynamicFormJson.json"];
    //    NSError * error;
    //    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    //    NSArray *arrOfCountry = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    //    [arrResponseInspection addObjectsFromArray:arrOfCountry];
    
    
    CGFloat scrollViewHeight=0.0;
    
    arrOfIndexes =[[NSMutableArray alloc]init];
    arrOfControlssMain =[[NSMutableArray alloc]init];
    int tagToSetForIndexes=0;
    
    for (int i=0; i<arrResponseInspection.count; i++)
    {
        NSDictionary *dictMain=[arrResponseInspection objectAtIndex:i];
        if (!MainViewForm.frame.size.height) {
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, 52)];
        }
        else{
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 52)];
        }
        BtnMainView.tag=i;
        [BtnMainView addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        //Nilind 28 Feb
        
         //        [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentName"] forState:UIControlStateNormal];

        
        
         NSString *strTitle=[dictMain valueForKey:@"FormName"];
         
         if ([strTitle isEqual:nil]||[strTitle isEqualToString:@""])
         {
             strTitle = [dictDeptNameByKey valueForKey:[dictMain valueForKey:@"DepartmentSysName"]];
         }
         
         if ([strTitle isEqual:nil]||[strTitle isEqualToString:@""])
         {
             strTitle = @"";
         }
         
        [BtnMainView setTitle:strTitle forState:UIControlStateNormal];

        

        //End
        // [BtnMainView setTitle:@"Title for Button Main View To See Whole Text On Mobile iOS." forState:UIControlStateNormal];
        
        BtnMainView.titleLabel.numberOfLines=20000;
        
        BtnMainView.titleLabel.font=[UIFont systemFontOfSize:22];
        
        [BtnMainView setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)];
        
        UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 50, 50)];
        imgView.image=[UIImage imageNamed:@"minus_icon.png"];
        
        //scrollViewHeight=scrollViewHeight+BtnMainView.frame.size.height;
        [BtnMainView addSubview:imgView];
        
        [_scrollForDynamicView addSubview:BtnMainView];
        
        [arrayOfButtonsMain addObject:BtnMainView];
        [arrOfButtonImages addObject:imgView];
        
        // Form Creation
        MainViewForm=[[UIView alloc]init];
        
        MainViewForm.backgroundColor=[UIColor clearColor];
        
        MainViewForm.frame=CGRectMake(0, BtnMainView.frame.origin.y+BtnMainView.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 1000);//1000
        
        MainViewForm.tag=i+3000;
        
        [_scrollForDynamicView addSubview:MainViewForm];
        
        NSArray *arrSections=[dictMain valueForKey:@"Sections"];
        
        
        int yAxisViewFormSections=0.0;
        int heightVIewFormSections=0.0;
        int heightMainViewFormSections=0.0;
        
        
        for (int j=0; j<arrSections.count; j++)
        {
            NSDictionary *dictSection=[arrSections objectAtIndex:j];
            NSArray *arrControls=[dictSection valueForKey:@"Controls"];
            
            // Section Creation Start
            heightVIewFormSections=0.0;
            ViewFormSections=[[UIView alloc]init];
            ViewFormSections.frame=CGRectMake(0, yAxisViewFormSections, [UIScreen mainScreen].bounds.size.width, 400);
            
            ViewFormSections.backgroundColor=[UIColor whiteColor];
            UILabel *lblTitleSection=[[UILabel alloc]init];
            lblTitleSection.backgroundColor=[UIColor lightGrayColor];
            lblTitleSection.layer.borderWidth = 1.5f;
            lblTitleSection.layer.cornerRadius = 4.0f;
            lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
            
            lblTitleSection.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50);
            lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictSection valueForKey:@"Name"]] ;
            lblTitleSection.textAlignment=NSTextAlignmentCenter;
            lblTitleSection.font=[UIFont systemFontOfSize:22];
            [ViewFormSections addSubview:lblTitleSection];
            
            ViewFormSections.layer.borderWidth = 1.5f;
            ViewFormSections.layer.cornerRadius = 4.0f;
            ViewFormSections.layer.borderColor = [[UIColor grayColor] CGColor];
            
            [MainViewForm addSubview:ViewFormSections];
            
            //CGFloat xPositionOfControls= 0.0;
            CGFloat yPositionOfControls = 0.0;
            CGFloat heightOfControls= 0.0;
            CGFloat widthOfControls= 0.0;
            
            for (int k=0; k<arrControls.count; k++)
            {
                NSDictionary *dictControls=[arrControls objectAtIndex:k];
                UILabel *lblTitleControl=[[UILabel alloc]init];
                lblTitleControl.backgroundColor=[UIColor lightGrayColor];
                lblTitleControl.frame=CGRectMake(0, 50*k+55, [UIScreen mainScreen].bounds.size.width, 50);
                lblTitleControl.text=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Label"]] ;
                lblTitleControl.font=[UIFont systemFontOfSize:20];
                lblTitleControl.textAlignment=NSTextAlignmentCenter;
                // [ViewFormSections addSubview:lblTitleControl];
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - CheckBox-Combo
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox-combo"]) {
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    NSArray *arrOfIndexChecked;
                    if (!(strIndexOfValue.length==0)) {
                        arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    }
                    
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    if (s.height<50) {
                        s.height=50;
                    }
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,35,35);
                    
                    NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                    NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                    NSMutableArray *arrOfValuesToCheck=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOptions.count; k++) {
                        
                        NSDictionary *dictData=arrOptions[k];
                        
                        [arrOfValues addObject:[dictData valueForKey:@"Label"]];
                        [arrOfValuesToCheck addObject:[dictData valueForKey:@"Value"]];
                        
                    }
                    int btnCount=(int)[arrOfValues count];
                    
                    int viewHeight = 0;
                    
                    for (int l=0; l<btnCount; l++){
                        
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.frame = btnFrame;
                        NSString *title = [arrOfValues objectAtIndex:l];
                        
                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                        
                        [arrOfIndexes addObject:strTaggg];
                        
                        btn.tag=tagToSetForIndexes;
                        
                        tagToSetForIndexes++;
                        
                        btn.titleLabel.textColor = [UIColor clearColor];
                        btn.titleLabel.font=[UIFont systemFontOfSize:20];
                        [btn setTitle:title forState:UIControlStateNormal];
                        
                        [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                        
                        for (int s=0; s<arrOfIndexChecked.count; s++) {
                            
                            NSString *strValueTocompare=[NSString stringWithFormat:@"%@",arrOfIndexChecked[s]];
                            if ([[arrOfValuesToCheck objectAtIndex:l] isEqualToString:strValueTocompare]) {
                                [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                            }
                            
                        }
                        
                        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                        {
                            [btn setEnabled:NO];
                        }else{
                            [btn setEnabled:YES];
                        }
                        
                        [btn addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        
                        // btnFrame.origin.y+=btnFrame.size.height+15;
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+50)];
                        [view addSubview:btn];
                        
                        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnHidden.frame =CGRectMake(0, btnFrame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 55);
                        btnHidden.tag=tagToSetForIndexes-1;
                        [btnHidden setBackgroundColor:[UIColor clearColor]];
                        [btnHidden addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        [view addSubview:btnHidden];
                        
                        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                        {
                            [btnHidden setEnabled:NO];
                        }
                        else
                        {
                            [btnHidden setEnabled:YES];
                        }
                        
                        [arrOfControlssMain addObject:btn];
                        
                        UILabel *lblItem = [[UILabel alloc] init];
                        
                        CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice]-80, MAXFLOAT)];
                        
                        if (lblItemHeight.height<50) {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+55, btn.frame.origin.y-8, [global globalWidthDevice]-80, 50);
                            
                        } else {
                            lblItem.frame = CGRectMake(btn.frame.origin.x+55, btn.frame.origin.y-8, [global globalWidthDevice]-80, lblItemHeight.height);
                            
                        }
                        [lblItem setFont:[UIFont systemFontOfSize:18]];
                        // lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =title;
                        lblItem.font=[UIFont systemFontOfSize:20];
                        [view addSubview:lblItem];
                        
                        btnFrame.origin.y+=lblItem.frame.size.height+5;
                        
                        int tempheight=lblItem.frame.size.height;
                        
                        viewHeight =viewHeight+tempheight;
                        
                    }
                    
                    viewHeight=viewHeight+lbl.frame.size.height+lbl.frame.origin.y+5*btnCount;
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight)];
                    
                    [ViewFormSections addSubview:view];
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - CheckBox
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox"]) {
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    NSArray *arrOfIndexChecked;
                    if (!(strIndexOfValue.length==0)) {
                        arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    }
                    
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    if (s.height<50) {
                        s.height=50;
                    }
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,35,35);
                    
                    NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                    NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                    NSMutableArray *arrOfValuesToCheck=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOptions.count; k++) {
                        
                        NSDictionary *dictData=arrOptions[k];
                        
                        [arrOfValues addObject:[dictData valueForKey:@"Label"]];
                        [arrOfValuesToCheck addObject:[dictData valueForKey:@"Value"]];
                        
                    }
                    int btnCount=(int)[arrOfValues count];
                    
                    int viewHeight = 0;
                    
                    for (int l=0; l<btnCount; l++){
                        
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.frame = btnFrame;
                        NSString *title = [arrOfValues objectAtIndex:l];
                        
                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                        
                        [arrOfIndexes addObject:strTaggg];
                        
                        btn.tag=tagToSetForIndexes;
                        
                        tagToSetForIndexes++;
                        
                        btn.titleLabel.textColor = [UIColor clearColor];
                        btn.titleLabel.font=[UIFont systemFontOfSize:20];
                        [btn setTitle:title forState:UIControlStateNormal];
                        
                        [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                        
                        for (int s=0; s<arrOfIndexChecked.count; s++) {
                            
                            NSString *strValueTocompare=[NSString stringWithFormat:@"%@",arrOfIndexChecked[s]];
                            if ([[arrOfValuesToCheck objectAtIndex:l] isEqualToString:strValueTocompare]) {
                                [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                            }
                            
                        }
                        
                        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                        {
                            [btn setEnabled:NO];
                        }
                        else
                        {
                            [btn setEnabled:YES];
                        }
                        
                        [btn addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        
                        // btnFrame.origin.y+=btnFrame.size.height+15;
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+50)];
                        [view addSubview:btn];
                        
                        
                        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnHidden.frame =CGRectMake(0, btnFrame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 55);
                        btnHidden.tag=tagToSetForIndexes-1;
                        [btnHidden setBackgroundColor:[UIColor clearColor]];
                        [btnHidden addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        [view addSubview:btnHidden];
                        
                        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                        {
                            [btnHidden setEnabled:NO];
                        }
                        else
                        {
                            [btnHidden setEnabled:YES];
                        }
                        
                        [arrOfControlssMain addObject:btn];
                        
                        UILabel *lblItem = [[UILabel alloc] init];
                        
                        CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice]-80, MAXFLOAT)];
                        
                        if (lblItemHeight.height<50) {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+55, btn.frame.origin.y+5-13, [global globalWidthDevice]-80, 50);
                            
                        } else {
                            lblItem.frame = CGRectMake(btn.frame.origin.x+55, btn.frame.origin.y+5-13, [global globalWidthDevice]-80, lblItemHeight.height);
                            
                        }
                        [lblItem setFont:[UIFont systemFontOfSize:26]];
                        //lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =title;
                        lblItem.font=[UIFont systemFontOfSize:20];
                        [view addSubview:lblItem];
                        
                        btnFrame.origin.y+=lblItem.frame.size.height+5;
                        
                        int tempheight=lblItem.frame.size.height;
                        
                        viewHeight =viewHeight+tempheight;
                        
                    }
                    
                    viewHeight=viewHeight+lbl.frame.size.height+lbl.frame.origin.y+5*btnCount;
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight)];
                    
                    [ViewFormSections addSubview:view];
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Radio---Combo
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                //el-radio-combo
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio-combo"] ||[[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"]){
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    if (s.height<50) {
                        s.height=50;
                    }
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,35,35);
                    
                    NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                    NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                    
                    int indexToCheck=-1;
                    
                    for (int k=0; k<arrOptions.count; k++) {
                        
                        NSDictionary *dictData=arrOptions[k];
                        
                        [arrOfValues addObject:[dictData valueForKey:@"Label"]];
                        
                        if ([[dictData valueForKey:@"Value"] isEqualToString:strIndexOfValue]) {
                            
                            indexToCheck=k;
                            
                        }
                        
                    }
                    int btnCount=(int)[arrOfValues count];
                    
                    int viewHeight = 0;
                    
                    NSMutableArray *arrOfTempRadioBtns=[[NSMutableArray alloc]init];
                    
                    for (int l=0; l<btnCount; l++){
                        
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.frame = btnFrame;
                        NSString *title = [arrOfValues objectAtIndex:l];
                        
                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                        
                        [arrOfIndexes addObject:strTaggg];
                        
                        [btn setTitle:@"RadioBtnnComboooo" forState:UIControlStateNormal];
                        btn.titleLabel.textColor = [UIColor clearColor];
                        btn.titleLabel.font=[UIFont systemFontOfSize:20];
                        
                        //                            if (l==0) {
                        //
                        //                                [btn setTitle:@"RadioBtnnCombooooFirst" forState:UIControlStateNormal];
                        //
                        //                            }
                        //                            if (l==btnCount-1) {
                        //
                        //                                [btn setTitle:@"RadioBtnnCombooooLast" forState:UIControlStateNormal];
                        //
                        //                            }
                        
                        btn.tag=tagToSetForIndexes;
                        
                        tagToSetForIndexes++;
                        
                        
                        
                        //                            if(!(strIndexOfValue.length==0)){
                        //
                        //                               indexToCheck =[strIndexOfValue intValue];
                        //
                        //                            }
                        
                        if (indexToCheck==l) {
                            
                            [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                            
                        }else{
                            
                            [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                            
                        }
                        
                        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                        {
                            [btn setEnabled:NO];
                        }
                        else
                        {
                            [btn setEnabled:YES];
                        }
                        
                        [btn addTarget:self action:@selector(checkRadioButtonCombo:) forControlEvents:UIControlEventTouchDown];
                        
                        // btnFrame.origin.y+=btnFrame.size.height+15;
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 50)];
                        [view addSubview:btn];
                        
                        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnHidden.frame =CGRectMake(0, btnFrame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 55);
                        btnHidden.tag=tagToSetForIndexes-1;
                        [btnHidden setBackgroundColor:[UIColor clearColor]];
                        [btnHidden addTarget:self action:@selector(checkRadioButtonCombo:) forControlEvents:UIControlEventTouchDown];
                        [view addSubview:btnHidden];
                        
                        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                        {
                            [btnHidden setEnabled:NO];
                        }
                        else
                        {
                            [btnHidden setEnabled:YES];
                        }
                        
                        [arrOfTempRadioBtns addObject:btn];
                        
                        // [arrOfControlssMain addObject:arrOfTempRadioBtns];
                        
                        //[arrOfControlssMain addObject:btn];
                        
                        UILabel *lblItem = [[UILabel alloc] init];
                        CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice]-80, MAXFLOAT)];
                        
                        if (lblItemHeight.height<50) {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+55, btn.frame.origin.y+5-13, [global globalWidthDevice]-80, 50);
                            
                        } else {
                            lblItem.frame = CGRectMake(btn.frame.origin.x+55, btn.frame.origin.y+5-13, [global globalWidthDevice]-80, lblItemHeight.height);
                            
                        }
                        [lblItem setFont:[UIFont systemFontOfSize:18]];
                        //lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =title;
                        lblItem.font=[UIFont systemFontOfSize:20];
                        [view addSubview:lblItem];
                        // lblItem.backgroundColor=[UIColor grayColor];
                        btnFrame.origin.y+=lblItem.frame.size.height+5;
                        
                        int tempheight=lblItem.frame.size.height;
                        
                        viewHeight =viewHeight+tempheight;
                        
                        
                        //lblItem.backgroundColor=[UIColor yellowColor];
                    }
                    
                    for (int l=0; l<btnCount; l++){
                        
                        [arrOfControlssMain addObject:arrOfTempRadioBtns];
                        
                    }
                    
                    viewHeight=viewHeight+lbl.frame.size.height+lbl.frame.origin.y+5*btnCount;
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight)];
                    
                    [ViewFormSections addSubview:view];
                    
                    
                }
                /*
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 #pragma mark - Radio Button
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 
                 
                 
                 else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"]){
                 
                 //Add View At which radio buttons and Tiltle Label will genarate
                 if (!yPositionOfControls) {
                 yPositionOfControls=40;
                 }
                 
                 NSString *strValueOfRadio=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                 
                 CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);;
                 UIView *view = [[UIView alloc] init];
                 [view setFrame:buttonFrame];
                 CALayer *bottomBorder = [CALayer layer];
                 [view.layer addSublayer:bottomBorder];
                 view.backgroundColor=[UIColor whiteColor];
                 CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(308, MAXFLOAT)];
                 UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 255, s.height)];
                 lbl.text = [dictControls valueForKey:@"Label"];
                 [lbl setAdjustsFontSizeToFitWidth:YES];
                 [lbl setFont:[UIFont systemFontOfSize:15]];
                 [lbl setNumberOfLines:9];
                 [view addSubview:lbl];
                 
                 CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,30,30);
                 
                 NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                 int indexToSetValue=-1;
                 NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                 for (int k=0; k<arrOptions.count; k++) {
                 
                 NSDictionary *dictData=arrOptions[k];
                 
                 [arrOfValues addObject:[dictData valueForKey:@"Value"]];
                 if ([strValueOfRadio isEqualToString:[dictData valueForKey:@"Value"]]) {
                 indexToSetValue=k;
                 }
                 
                 }
                 
                 int btnCount=[arrOfValues count];
                 
                 float viewHeight = btnCount*42+lbl.frame.size.height+20;
                 for (int l=0; l<btnCount; l++)
                 {
                 UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(15, 10, 255, 40)];
                 btn.frame = btnFrame;
                 NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                 [arrOfIndexes addObject:strTaggg];
                 btn.tag=tagToSetForIndexes;
                 tagToSetForIndexes++;
                 
                 if (l==0) {
                 
                 [btn setTitle:@"firstRadioBtnn" forState:UIControlStateNormal];
                 btn.titleLabel.textColor = [UIColor clearColor];
                 
                 }
                 
                 [btn addTarget:self action:@selector(checkRadioButtonCombo:) forControlEvents:UIControlEventTouchDown];
                 [view addSubview:btn];
                 
                 UILabel *lblYes = [[UILabel alloc] init];
                 lblYes.frame = CGRectMake(btn.frame.origin.x+50, btn.frame.origin.y+5, 200, 100);
                 lblYes.text = [arrOfValues objectAtIndex:l];
                 NSString *str=lblYes.text;
                 [lblYes setNumberOfLines:2];
                 lblYes.center=CGPointMake(lblYes.center.x, btn.center.y);
                 
                 [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                 if (indexToSetValue==-1) {
                 
                 }else if (indexToSetValue==k){
                 
                 [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                 }
                 
                 [arrOfControlssMain addObject:btn];
                 
                 [lblYes setAdjustsFontSizeToFitWidth:YES];
                 [lblYes setTag:btn.tag+200];
                 [view addSubview:lblYes];
                 [lblYes setFont:[UIFont systemFontOfSize:15]];
                 btnFrame.origin.y+=btnFrame.size.height+12; //it changes Y exix of radio button
                 }
                 
                 [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight+20)];
                 
                 heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                 yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                 
                 [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                 
                 [ViewFormSections addSubview:view];
                 
                 }
                 */
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - TextArea i.e TextViewww
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textarea"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],100);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextView *txtView= [[UITextView alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    txtView.font=[UIFont systemFontOfSize:20];
                    [view addSubview:txtView];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [txtView setEditable:NO];
                    }
                    else
                    {
                        [txtView setEditable:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Paragraph i.e paragraph
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-paragraph"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],100);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextView *txtView= [[UITextView alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.font=[UIFont systemFontOfSize:20];
                    txtView.editable=NO;
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //el-textbox
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - TextBox i.e TextField
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textbox"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],55);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    txtView.font=[UIFont systemFontOfSize:20];
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [txtView setEnabled:NO];
                    }
                    else
                    {
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - EmailId Field
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-email"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],55);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.font=[UIFont systemFontOfSize:20];
                    txtView.keyboardType=UIKeyboardTypeEmailAddress;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [txtView setEnabled:NO];
                    }
                    else
                    {
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Phone Number Field
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-phone"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=50;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],55);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypePhonePad;
                    txtView.font=[UIFont systemFontOfSize:20];
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [txtView setEnabled:NO];
                    }
                    else
                    {
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //el-currency
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Currency
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-currency"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=50;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],55);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypePhonePad;
                    txtView.font=[UIFont systemFontOfSize:20];
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [txtView setEnabled:NO];
                    }
                    else
                    {
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Decimal
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-decimal"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=50;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],55);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeDecimalPad;
                    txtView.font=[UIFont systemFontOfSize:20];
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [txtView setEnabled:NO];
                    }
                    else
                    {
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Number
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-number"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=50;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],55);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeNumberPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    txtView.font=[UIFont systemFontOfSize:20];
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [txtView setEnabled:NO];
                    }
                    else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //el-auto-num  el-precent el-url el-upload
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-auto-num
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-auto-num"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=50;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],55);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeNumberPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    txtView.font=[UIFont systemFontOfSize:20];
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-precent
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-precent"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=50;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],55);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypePhonePad;
                    txtView.font=[UIFont systemFontOfSize:20];
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-url
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-url"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=50;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    //                        CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-30,35);
                    //                        float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    //                        UITextField *txtView= [[UITextField alloc] init];
                    //                        txtView.text = [dictControls valueForKey:@"Value"];
                    //                        txtView.frame =txtViewFrame;
                    //                        txtView.keyboardType=UIKeyboardTypeWebSearch;
                    //
                    //                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    //
                    //                        [arrOfIndexes addObject:strTaggg];
                    //
                    //                        txtView.tag=tagToSetForIndexes;
                    //                        tagToSetForIndexes++;
                    //
                    //                        //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    //                        // txtView.layer.borderWidth = 0.5f;
                    //                        txtView.layer.borderWidth = 1.5f;
                    //                        txtView.layer.cornerRadius = 4.0f;
                    //                        txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    //                        [txtView setBackgroundColor:[UIColor clearColor]];
                    //                        txtView.delegate=self;
                    //
                    //                        UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    //                        leftView.backgroundColor = [UIColor clearColor];
                    //
                    //
                    //                        txtView.leftView = leftView;
                    //
                    //                        txtView.leftViewMode = UITextFieldViewModeAlways;
                    //                        txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    //
                    //                        [view addSubview:txtView];
                    //
                    //                        [arrOfControlssMain addObject:txtView];
                    
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-30, 50)];
                    
                    float viewHeight = lbl.frame.size.height+btnDropDown.frame.size.height+50;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(oPenURLonWeb:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    
                    btnDropDown.backgroundColor=[UIColor clearColor];
                    [btnDropDown setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                    
                    [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    //[dictControls valueForKey:@"Value"]
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:20];
                    [view addSubview:btnDropDown];
                    
                    UILabel *lblUnderLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 33, btnDropDown.frame.size.width, 1)];
                    
                    lblUnderLine.backgroundColor=[UIColor blueColor];
                    
                    //  [btnDropDown addSubview:lblUnderLine];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-upload
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-upload"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=50;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],55);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeNumberPad;
                    txtView.font=[UIFont systemFontOfSize:20];
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Date Picker
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-date"]){
                    
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-30, 50)];
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [btnDropDown setEnabled:NO];
                    }else{
                        [btnDropDown setEnabled:YES];
                    }
                    
                    [btnDropDown addTarget:self action:@selector(oPenDatePicker:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    
                    btnDropDown.backgroundColor=[UIColor clearColor];
                    [btnDropDown setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:20];
                    [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Date Time Picker
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-date-time"]){
                    
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 50)];
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(oPenDateTimePicker:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:20];
                    btnDropDown.backgroundColor=[UIColor clearColor];
                    [btnDropDown setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [btnDropDown setEnabled:NO];
                    }else{
                        [btnDropDown setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - DropDown
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]){
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:20];
                    [view addSubview:lbl];
                    
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 50)];
                    
                    UIButton *ImgDropDown = [[UIButton alloc] initWithFrame:CGRectMake(btnDropDown.frame.size.width-50,0, 40, 50)];
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    ImgDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [btnDropDown setEnabled:NO];
                        [ImgDropDown setEnabled:NO];
                    }else{
                        [btnDropDown setEnabled:YES];
                        [ImgDropDown setEnabled:YES];
                    }
                    
                    [btnDropDown addTarget:self action:@selector(oPenDropDown:) forControlEvents:UIControlEventTouchDown];
                    [ImgDropDown addTarget:self action:@selector(oPenDropDown:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor clearColor] CGColor];
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:20];
                    btnDropDown.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
                    //drop_down1.png
                    [btnDropDown setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                    //15 March
                    
                    NSArray *arrTemp=[dictControls valueForKey:@"Options"];
                    NSString *strTempValueToCheck=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    NSString *strNameToSet;
                    
                    for (int q=0; q<arrTemp.count; q++) {
                        
                        NSDictionary *dictDataTemp=arrTemp[q];
                        
                        NSString *strTemp=[NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"Value"]];
                        
                        if ([strTemp isEqualToString:strTempValueToCheck]) {
                            
                            strNameToSet=[NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"Name"]];;
                            
                        }
                        
                    }
                    [btnDropDown setTitle:strNameToSet forState:UIControlStateNormal];
                    
                    //End 15 March
                    
                    // [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [ImgDropDown setImage:[UIImage imageNamed:@"drop_down1.png"] forState:UIControlStateNormal];
                    [btnDropDown addSubview:ImgDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //el-multi-select
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Multiple Select
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"]){
                    
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    //Added Label For Heading
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    
                    lbl.text = [dictControls valueForKey:@"Label"];
                    
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    
                    [lbl setNumberOfLines:20000];
                    
                    lbl.font=[UIFont systemFontOfSize:20];
                    
                    [view addSubview:lbl];
                    
                    //End
                    
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(lbl.frame)+5, [global globalWidthDevice], 50)];
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(multipleSelect:) forControlEvents:UIControlEventTouchDown];
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:20];
                    btnDropDown.backgroundColor=[UIColor lightGrayColor];
                    
                    // [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    NSArray *arrOfOptions=[dictControls valueForKey:@"Options"];
                    NSArray *arrOfValues=[[dictControls valueForKey:@"Value"] componentsSeparatedByString:@","];
                    NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOfValues.count; k++) {
                        
                        for (int l=0; l<arrOfOptions.count; l++) {
                            
                            NSDictionary *dictData=arrOfOptions[l];
                            NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                            
                            if ([strValue isEqualToString:arrOfValues[k]]) {
                                
                                [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                                
                            }
                            
                        }
                        
                    }
                    NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@","];
                    
                    [btnDropDown setTitle:strTextView forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
                    {
                        [btnDropDown setEnabled:NO];
                    }else{
                        [btnDropDown setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 50+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
            }
            
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
#pragma mark - Final Size scroll and view and all
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
            
            [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, heightVIewFormSections+arrControls.count*10+50)];
            
            heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrControls.count*10+50;
            
            MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width, heightMainViewFormSections+60);
            
            yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+10;
            
        }
        
        scrollViewHeight=scrollViewHeight+MainViewForm.frame.size.height;
        [arrayViews addObject:MainViewForm];
        
        CGFloat heightTemp=MainViewForm.frame.size.height;
        
        NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
        
        CGFloat YAxisTemp=MainViewForm.frame.origin.y;
        
        NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
        
        
        [arrOfHeightsViews addObject:strTempHeight];
        [arrOfYAxisViews addObject:strTempYAxis];
        
    }
    
    //For Save And Continue
    
    CGRect frameForSavenContinueView=CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+20, self.view.frame.size.width, _viewSavenCancel.frame.size.height);
    frameForSavenContinueView.origin.x=0;
    frameForSavenContinueView.origin.y=MainViewForm.frame.origin.y+MainViewForm.frame.size.height;
    [_viewSavenCancel setFrame:frameForSavenContinueView];
    [_scrollForDynamicView addSubview:_viewSavenCancel];
    
    
    _scrollForDynamicView.backgroundColor=[UIColor clearColor];
    
    _scrollForDynamicView.frame=CGRectMake(_scrollForDynamicView.frame.origin.x, _scrollForDynamicView.frame.origin.y, _scrollForDynamicView.frame.size.width, scrollViewHeight+arrResponseInspection.count*100+_viewSavenCancel.frame.size.height);
    
    
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,scrollViewHeight+arrResponseInspection.count*100+_viewSavenCancel.frame.size.height)];
    //MainViewForm.backgroundColor=[UIColor redColor];
    //_scrollForDynamicView.backgroundColor=[UIColor orangeColor];
    
    [self defaultCloseAllViews];
    
}

//============================================================================
//============================================================================
#pragma mark- Single Tap Methods
//============================================================================
//============================================================================
-(void)tapDetectedOnBackGroundView{
    [viewBackGround removeFromSuperview];
}

//============================================================================
//============================================================================
#pragma mark- ------------Radio Button Methods--------------
//============================================================================
//============================================================================
-(void)checkRadioButton:(id)sender{
    
    [self endEditing];
    UIButton *btn=(UIButton*)sender;
    
    int taggg=(int)btn.tag;
    
    UIButton *btnRadiButton=arrOfControlssMain[taggg];
    
    [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    if ([arrOfControlssMain[taggg-1] isKindOfClass:[UIButton class]]) {
        
        UIButton *btnlast=arrOfControlssMain[taggg-1];
        
        if ([btnlast.currentTitle isEqualToString:@"firstRadioBtnn"]) {
            
            [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            
        }else{
            
            UIButton *btnlast=arrOfControlssMain[taggg+1];
            
            [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            
        }
        
    }else{
        
        UIButton *btnlast=arrOfControlssMain[taggg+1];
        
        [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
    //firstRadioBtnn
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexLLL =[arrOfTag[3] intValue];
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
    
    NSString *strValue=[dictOfValues valueForKey:@"Value"];
    
    [self commonMethodToSaveValue:strValue :taggg];
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Radio Button Combooo Methods--------------
//============================================================================
//============================================================================
-(void)checkRadioButtonCombo:(id)sender{
    
    [self endEditing];
    
    BOOL isValueToSet;
    isValueToSet=NO;
    
    //RadioBtnnComboooo
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=(int)btn.tag;
    
    
    int taggToCheckTemp=0;
    
    NSArray *arrOfRadioBtnsTemp = arrOfControlssMain[taggg];
    
    for (int k=0; k<arrOfRadioBtnsTemp.count; k++) {
        
        UIButton *btnRadiButton=arrOfRadioBtnsTemp[k];
        if (btnRadiButton.tag==taggg) {
            taggToCheckTemp=k;
            
            if ([btnRadiButton.currentImage isEqual:[UIImage imageNamed:@"RadioButton-Selected.png"]]) {
                
                [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                isValueToSet=NO;
            } else {
                
                [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                isValueToSet=YES;
            }
            
            // [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            
        }
    }
    
    int taggToCheck=taggToCheckTemp-1;
    
    for (int s=taggToCheck;s>=0; s--) {
        
        if ([arrOfRadioBtnsTemp[s] isKindOfClass:[UIButton class]]) {
            
            UIButton *btnlast=arrOfRadioBtnsTemp[s];
            
            if ([btnlast.currentTitle isEqualToString:@"RadioBtnnComboooo"]) {
                
                [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                
            }
        }
        else
            break;
    }
    
    taggToCheck=taggToCheckTemp+1;
    
    for (int s=taggToCheck;s<arrOfRadioBtnsTemp.count; s++) {
        
        if ([arrOfRadioBtnsTemp[s] isKindOfClass:[UIButton class]]) {
            
            UIButton *btnlast=arrOfRadioBtnsTemp[s];
            
            if ([btnlast.currentTitle isEqualToString:@"RadioBtnnComboooo"]) {
                
                [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                
            }
        }
        else
            break;
    }
    
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexLLL =[arrOfTag[3] intValue];
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
    
    NSString *strValue=[dictOfValues valueForKey:@"Value"];
    
    if (!isValueToSet) {
        strValue=@"";
    }
    
    [self commonMethodToSaveValue:strValue :taggg];
    
    
    //firstRadioBtnn
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Check Box Button Methods--------------
//============================================================================
//============================================================================
-(void)checkCheckBoxes:(id)sender{
    
    [self endEditing];
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=(int)btn.tag;
    
    UIButton *btnCheckBox=arrOfControlssMain[taggg];
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"uncheck.png"];
    
    UIImage *img = [btnCheckBox imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        [btnCheckBox setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
    }
    else{
        
        [btnCheckBox setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexLLL =[arrOfTag[3] intValue];
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    NSString *strAlreadySavedValue=[dictOfOptions valueForKey:@"Value"];
    
    NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
    
    NSString *strValue=[dictOfValues valueForKey:@"Value"];
    
    NSString *strFinalValue;
    
    if (strAlreadySavedValue.length==0) {
        
        strFinalValue=strValue;
        
    }else{
        
        NSArray *arrOfValue=[strAlreadySavedValue componentsSeparatedByString:@","];
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        [arrTemp addObjectsFromArray:arrOfValue];
        
        
        if ([arrTemp containsObject:strValue]) {
            
            [arrTemp removeObject:strValue];
            
        }
        else{
            
            [arrTemp addObject:strValue];
            
        }
        
        arrOfValue = arrTemp;
        
        strFinalValue=[arrOfValue componentsJoinedByString:@","];
        
    }
    
    [self commonMethodToSaveValue:strFinalValue :taggg];
    
}
//============================================================================
//============================================================================
#pragma mark- ------------URL OPEN ON WEB Methods--------------
//============================================================================
//============================================================================
-(void)oPenURLonWeb:(id)sender{
    
    [self endEditing];
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggggs];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    // [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    // [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSString *strURL=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
    
    if (strURL.length>0) {
        
        [self openLinkNew:strURL];
        
    }else{
        [global AlertMethod:@"Info" :@"No Link Available"];
        // [self openLinkNew:@"http://www.citizencop.org/"];
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Open In Safari Methods--------------
//============================================================================
//============================================================================

- (void)openLinkNew:(NSString *)url {
    
    
    NSURL *URL = [NSURL URLWithString:url];
    
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = (id)self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:URL]) {
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Date Picker Methods--------------
//============================================================================
//============================================================================

-(void)oPenDatePicker:(id)sender{
    
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggggs];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    // [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    // [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    strGlobalDateToShow=[dictOfOptions valueForKey:@"Value"];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    [self addPickerViewDateTo : taggggs];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addPickerViewDateTo:(int)intTagg
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //  [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strGlobalDateToShow.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strGlobalDateToShow];
        
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    btnDone.tag=intTagg;
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    if (yesSameDay) {
        
        UIButton *btnDropdown=arrOfControlssMain[taggggs];
        
        [btnDropdown setTitle:strDate forState:UIControlStateNormal];
        strGlobalDateToShow=strDate;
        [self commonMethodToSaveValue:strDate :taggggs];
        
        [viewForDate removeFromSuperview];
        [viewBackGround removeFromSuperview];
        
    }
    else{
        
        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
        
        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
            
            [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
            
        }else{
            
            UIButton *btnDropdown=arrOfControlssMain[taggggs];
            
            [btnDropdown setTitle:strDate forState:UIControlStateNormal];
            strGlobalDateToShow=strDate;
            [self commonMethodToSaveValue:strDate :taggggs];
            
            [viewForDate removeFromSuperview];
            [viewBackGround removeFromSuperview];
            
        }
        
    }
    
}
//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

//============================================================================
//============================================================================
#pragma mark- ------------Time Picker Methods--------------
//============================================================================
//============================================================================

-(void)oPenDateTimePicker:(id)sender{
    
    [self endEditing];
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggggs];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    // [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    // [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    strGlobalDatenTime=[dictOfOptions valueForKey:@"Value"];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    [self addDAteTimePickerView : taggggs];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Time PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addDAteTimePickerView:(int)intTagg
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //  [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strGlobalDatenTime.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:MM:SS"];
        NSDate *dateToSett = [dateFormat dateFromString:strGlobalDatenTime];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDateAndTime;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    btnDone.tag=intTagg;
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateTimeOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)setDateTimeOnDone:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:MM:SS"];
    NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    if (yesSameDay) {
        
        UIButton *btnDropdown=arrOfControlssMain[taggggs];
        
        [btnDropdown setTitle:strDate forState:UIControlStateNormal];
        strGlobalDatenTime=strDate;
        [self commonMethodToSaveValue:strDate :taggggs];
        
        [viewForDate removeFromSuperview];
        [viewBackGround removeFromSuperview];
        
    }
    else{
        
        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
        
        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
            
            [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
            
        }else{
            
            UIButton *btnDropdown=arrOfControlssMain[taggggs];
            
            [btnDropdown setTitle:strDate forState:UIControlStateNormal];
            strGlobalDatenTime=strDate;
            [self commonMethodToSaveValue:strDate :taggggs];
            
            [viewForDate removeFromSuperview];
            [viewBackGround removeFromSuperview];
            
        }
        
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Drop Down Methods--------------
//============================================================================
//============================================================================

-(void)oPenDropDown:(id)sender
{
    [self endEditing];
    
    isMultipleSelect=NO;
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSArray *arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSArray *arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    [arrDataTblView addObjectsFromArray:arrDataOptionss];
    
    strGlobalDropDownSelectedValue=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        [self tableLoad:taggg];
        tblData.tag=taggg;
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Drop Down Methods--------------
//============================================================================
//============================================================================

-(void)multipleSelect:(id)sender{
    
    [self endEditing];
    arraySelecteValues=nil;
    arraySelecteValuesToShow=nil;
    arraySelecteValues= [[NSMutableArray alloc] init];
    arraySelecteValuesToShow= [[NSMutableArray alloc] init];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSArray *arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSArray *arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    [arrDataTblView addObjectsFromArray:arrDataOptionss];
    
    
    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
    
    if (!(strIndexOfValue.length==0)) {
        
        NSArray *arrayvalus=[strIndexOfValue componentsSeparatedByString:@","];
        
        for (int i=0; i<[arrayvalus count]; i++)
        {
            [arraySelecteValues addObject:arrayvalus[i]];
            // [arraySelecteValuesToShow addObject:arrayvalus[i]];
        }
        
    }
    
    NSArray *arrOfOptions=[dictOfOptions valueForKey:@"Options"];
    NSArray *arrOfValues=[[dictOfOptions valueForKey:@"Value"] componentsSeparatedByString:@","];
    NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfValues.count; k++) {
        
        for (int l=0; l<arrOfOptions.count; l++) {
            
            NSDictionary *dictData=arrOfOptions[l];
            NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
            
            if ([strValue isEqualToString:arrOfValues[k]]) {
                
                [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                
            }
            
        }
        
    }
    // NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@","];
    
    [arraySelecteValuesToShow addObjectsFromArray:arrOfValuesToSet];
    
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        [self tableLoad:taggg];
        tblData.tag=taggg;
        isMultipleSelect=YES;
    }
    
    
}

-(void)tableLoad:(NSInteger)btntag
{
    
    //    NSInteger i;
    //    i=btntag;
    //    switch (i)
    //    {
    //        case 101:
    //        {
    //            [self setTableFrame];
    //            break;
    //        }
    //        case 102:
    //        {
    //            [self setTableFrame];
    //            break;
    //        }
    //
    //        default:
    //            break;
    //    }
    
    [self setTableFrame];
    
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}
-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    //    singleTap1.numberOfTapsRequired = 1;
    //    [viewBackGround setUserInteractionEnabled:YES];
    //    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-10, 42)];
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(btnDoneAction:) forControlEvents:UIControlEventTouchDown];
    
    
    UIButton *btnCancel=[[UIButton alloc]initWithFrame:CGRectMake(btnDone.frame.origin.x+btnDone.frame.size.width+15, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-10, 42)];
    
    [btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCancel.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnCancel addTarget:self action:@selector(btnCancelAction:) forControlEvents:UIControlEventTouchDown];
    
    
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    [viewBackGround addSubview:btnDone];
    [viewBackGround addSubview:btnCancel];
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Button Done Action Methods--------------
//============================================================================
//============================================================================

-(void)btnDoneAction:(id)sender{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    if (isMultipleSelect) {
        
        if (!(indexMultipleSelectTosetValue==-100000)) {
            
            UIButton *btnDropdown=arrOfControlssMain[indexMultipleSelectTosetValue];
            
            NSString *joinedComponents = [arraySelecteValues componentsJoinedByString:@","];
            NSString *joinedComponentsToShow = [arraySelecteValuesToShow componentsJoinedByString:@","];
            
            if (!(joinedComponents.length==0)) {
                
                [btnDropdown setTitle:joinedComponentsToShow forState:UIControlStateNormal];
                
                [self commonMethodToSaveValue:joinedComponents :indexMultipleSelectTosetValue];
                
            } else {
                
                [btnDropdown setTitle:@"" forState:UIControlStateNormal];
                [self commonMethodToSaveValue:joinedComponents :indexMultipleSelectTosetValue];
                
            }
            
        }
    }
}
//============================================================================
//============================================================================
#pragma mark- ------------Button Cancel Action Methods--------------
//============================================================================
//============================================================================

-(void)btnCancelAction:(id)sender{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrDataTblView count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (!(arrDataTblView.count==0)) {
        
        if (isMultipleSelect) {
            
            NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
            
            NSString *str=[dictData valueForKey:@"Value"];
            
            cell.textLabel.text=[dictData valueForKey:@"Name"];
            
            long index=100000;
            
            for (int k=0; k<arraySelecteValues.count; k++) {
                if ([arraySelecteValues containsObject:str]) {
                    index=indexPath.row;
                    break;
                }
            }
            
            if (indexPath.row==index) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            } else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
        }
        else{
            
            NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
            cell.textLabel.text=[dictData valueForKey:@"Name"];
            
            NSString *str=[dictData valueForKey:@"Value"];
            long index=100000;
            if ([strGlobalDropDownSelectedValue isEqualToString:str]) {
                index=indexPath.row;
            }
            if (indexPath.row==index) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            } else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
        }
    }
    if ([UIScreen mainScreen].bounds.size.height==667) {
        cell.textLabel.font=[UIFont systemFontOfSize:22];
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        cell.textLabel.font=[UIFont systemFontOfSize:22];
    }
    else
        cell.textLabel.font=[UIFont systemFontOfSize:20];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!(arrDataTblView.count==0)) {
        
        if (isMultipleSelect) {
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
            if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                [arraySelecteValues addObject:[dictData valueForKey:@"Value"]];
                [arraySelecteValuesToShow addObject:[dictData valueForKey:@"Name"]];
            }
            else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                [arraySelecteValues removeObject:[dictData valueForKey:@"Value"]];
                [arraySelecteValuesToShow removeObject:[dictData valueForKey:@"Name"]];
            }
            
            
            
            // NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
            
            NSInteger btnIndex;
            btnIndex=tblData.tag;
            indexMultipleSelectTosetValue=(int)btnIndex;
            
        }
        else{
            NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
            
            NSInteger btnIndex;
            btnIndex=tblData.tag;
            
            UIButton *btnDropdown=arrOfControlssMain[btnIndex];
            
            [btnDropdown setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
            
            //[self commonMethodToSaveValue:[dictData valueForKey:@"Name"] :btnIndex];
            [self commonMethodToSaveValue:[dictData valueForKey:@"Value"] :btnIndex];
            
        }
        
    }
    
    if (isMultipleSelect) {
        
        
    } else {
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }
}


//============================================================================
//============================================================================
#pragma mark- ---------------------Open Close MainView METHODS-----------------
//============================================================================
//============================================================================
-(void)actionOpenCloseMAinView:(id)sender
{
    [self endEditing];
    
    UIButton *btn = (UIButton *)sender;
    int tagggg=(int)btn.tag;
    
    UIButton *btnTempp=[arrayOfButtonsMain objectAtIndex:tagggg];
    
    [arrayOfButtonsMain replaceObjectAtIndex:tagggg withObject:btnTempp];
    
    UIView *viewTempp=[arrayViews objectAtIndex:tagggg];
    
    if (viewTempp.frame.size.height==0) {
        
        UIImageView *imgView=arrOfButtonImages[tagggg];
        [imgView setImage:[UIImage imageNamed:@"minus_icon.png"]];
        
        NSString *strTempHeightLast=arrOfHeightsViews[tagggg];
        CGFloat hiTempLast=[strTempHeightLast floatValue];
        
        [viewTempp setFrame:CGRectMake(btnTempp.frame.origin.x, btnTempp.frame.origin.y+btnTempp.frame.size.height+5, btnTempp.frame.size.width, hiTempLast)];
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTempp.frame.size.height)];
        
        [viewTempp setHidden:NO];
    } else {
        
        UIImageView *imgView=arrOfButtonImages[tagggg];
        [imgView setImage:[UIImage imageNamed:@"add_white.png"]];
        
        [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTempp.frame.size.height)];
        
        [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
        [viewTempp setHidden:YES];
    }
    [arrayViews replaceObjectAtIndex:tagggg withObject:viewTempp];
    
    for (int k=tagggg+1; k<arrayViews.count; k++) {
        
        
        UIButton *btnTemppNew=[arrayOfButtonsMain objectAtIndex:k];
        UIView *viewTemppNew=[arrayViews objectAtIndex:k];
        UIView *viewTemppNewLasttt;
        CGFloat hiTempLast;
        if (k==0) {
            viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
        } else {
            viewTemppNewLasttt=[arrayViews objectAtIndex:k-1];
            NSString *strTempHeightLast=arrOfHeightsViews[k-1];
            hiTempLast =[strTempHeightLast floatValue];
            
        }
        
        [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
        [btnTemppNew addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        [arrayOfButtonsMain replaceObjectAtIndex:k withObject:btnTemppNew];
        
        [viewTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
        if (viewTemppNew.frame.size.height==0) {
            
            UIImageView *imgView=arrOfButtonImages[k];
            [imgView setImage:[UIImage imageNamed:@"add_white.png"]];
            
            [viewTemppNew setHidden:YES];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTemppNew.frame.size.height)];
            
        }else{
            UIImageView *imgView=arrOfButtonImages[k];//checked.png
            [imgView setImage:[UIImage imageNamed:@"minus_icon.png"]];
            
            [viewTemppNew setHidden:NO];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTemppNew.frame.size.height)];
            
        }
        [arrayViews replaceObjectAtIndex:k withObject:viewTemppNew];
        
    }
    
    
    //For Save And Continue
    
    CGRect frameForSavenContinueView=CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+20, self.view.frame.size.width, _viewSavenCancel.frame.size.height);
    frameForSavenContinueView.origin.x=0;
    frameForSavenContinueView.origin.y=MainViewForm.frame.origin.y+MainViewForm.frame.size.height;
    [_viewSavenCancel setFrame:frameForSavenContinueView];
    [_scrollForDynamicView addSubview:_viewSavenCancel];
    
}

-(void)defaultCloseAllViews
{
    [self endEditing];

    
    for (int k=0; k<arrayViews.count; k++) {
        
        
        UIButton *btnTemppNew=[arrayOfButtonsMain objectAtIndex:k];
        UIView *viewTemppNew=[arrayViews objectAtIndex:k];
        UIView *viewTemppNewLasttt;
        CGFloat hiTempLast;
        if (k==0) {
            viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
        } else {
            viewTemppNewLasttt=[arrayViews objectAtIndex:k-1];
            NSString *strTempHeightLast=arrOfHeightsViews[k-1];
            hiTempLast =[strTempHeightLast floatValue];
            
        }
        
        [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
        [btnTemppNew addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        [arrayOfButtonsMain replaceObjectAtIndex:k withObject:btnTemppNew];
        
        [viewTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, 0)];
        if (viewTemppNew.frame.size.height==0) {
            
            UIImageView *imgView=arrOfButtonImages[k];
            [imgView setImage:[UIImage imageNamed:@"add_white.png"]];
            
            [viewTemppNew setHidden:YES];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height-viewTemppNew.frame.size.height)];
            
        }else{
            UIImageView *imgView=arrOfButtonImages[k];//checked.png
            [imgView setImage:[UIImage imageNamed:@"minus_icon.png"]];
            
            [viewTemppNew setHidden:NO];
            
            //   [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,_scrollForDynamicView.contentSize.height+viewTemppNew.frame.size.height)];
            
        }
        [arrayViews replaceObjectAtIndex:k withObject:viewTemppNew];
        
    }
    
    
    //For Save And Continue
    
    CGRect frameForSavenContinueView=CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+20, self.view.frame.size.width, _viewSavenCancel.frame.size.height);
    frameForSavenContinueView.origin.x=0;
    frameForSavenContinueView.origin.y=MainViewForm.frame.origin.y+MainViewForm.frame.size.height;
    [_viewSavenCancel setFrame:frameForSavenContinueView];
    [_scrollForDynamicView addSubview:_viewSavenCancel];
    [_scrollForDynamicView setContentSize:CGSizeMake( _scrollForDynamicView.frame.size.width,MainViewForm.frame.origin.y+MainViewForm.frame.size.height+20+100)];

}

//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(void)textViewDidBeginEditing:(UITextView *)textField{
    
}
-(void)textViewDidEndEditing:(UITextView *)textField
{
    int taggg=(int)textField.tag;
    
    [self commonMethodToSaveValue:textField.text :taggg];
    
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT FIELD DELEGATE METHODS-----------------
//============================================================================
//============================================================================
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    
    
    textField.leftView = leftView;
    
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    int taggg=(int)textField.tag;
    
    [self commonMethodToSaveValue:textField.text :taggg];
    
}

//============================================================================
//============================================================================
#pragma mark ---------------- Validation Check Method--------------------------
//============================================================================
//============================================================================

-(NSString *)validationCheck :(NSString*)stringTovalidate :(NSString*)typeOfelement
{
    NSString *errorMessage;
    if (stringTovalidate.length>0) {
        
        if ([typeOfelement isEqualToString:@"el-email"]) {
            
            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            if (![emailPredicate evaluateWithObject:stringTovalidate])
            {
                errorMessage = @"Please enter valid email id only";
            }
        }
        else if ([typeOfelement isEqualToString:@""]){
            
        }
        
    }
    return errorMessage;
}

//============================================================================
//============================================================================
#pragma mark ---------------- Common Method To Save value---------------------
//============================================================================
//============================================================================
-(void)saveLeadIdInMasterForms :(int)indexReplace{
    
    NSDictionary *dictData=arrResponseInspection[indexReplace];
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    
    NSArray *arrTemp = [dictData valueForKey:@"Sections"];
    
    for (int k=0; k<arrTemp.count; k++) {
        
        NSDictionary *dictTemp = arrTemp[k];
        
        NSString *strNameTemp = [NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"Name"]];
        
        if ((strNameTemp.length == 0) || [strNameTemp isEqualToString:@"<null>"] || [strNameTemp isEqualToString:@"<Null>"])  {
            
        } else {
            
            [arrSections addObject:dictTemp];
            
        }
        
    }
    
    //[arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSArray *objValuearrResponseInspection=[NSArray arrayWithObjects:
                                            [dictData valueForKey:@"InspectionId"],
                                            strLeadId,
                                            [dictData valueForKey:@"FormId"],
                                            [dictData valueForKey:@"FormName"],
                                            [dictData valueForKey:@"CshtmlFileName"],
                                            [dictData valueForKey:@"CshtmlFilePath"],
                                            [dictData valueForKey:@"HtmlConvertedFileName"],
                                            [dictData valueForKey:@"HtmlConvertedFilePath"],
                                            [dictData valueForKey:@"HtmlFileName"],
                                            [dictData valueForKey:@"HtmlFilePath"],
                                            [dictData valueForKey:@"CompanyId"],
                                            [dictData valueForKey:@"DepartmentName"],
                                            [dictData valueForKey:@"DepartmentSysName"],
                                            [dictData valueForKey:@"BranchName"],
                                            [dictData valueForKey:@"BranchSysName"],
                                            [dictData valueForKey:@"FlowType"],
                                            arrSections,
                                            [dictData valueForKey:@"SequenceNo"],nil];
    
    NSArray *objKeyarrResponseInspection=[NSArray arrayWithObjects:
                                          @"InspectionId",
                                          @"LeadId",
                                          @"FormId",
                                          @"FormName",
                                          @"CshtmlFileName",
                                          @"CshtmlFilePath",
                                          @"HtmlConvertedFileName",
                                          @"HtmlConvertedFilePath",
                                          @"HtmlFileName",
                                          @"HtmlFilePath",
                                          @"CompanyId",
                                          @"DepartmentName",
                                          @"DepartmentSysName",
                                          @"BranchName",
                                          @"BranchSysName",
                                          @"FlowType",
                                          @"Sections",
                                          @"SequenceNo",nil];
    
    //NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
    
    //[arrResponseInspection replaceObjectAtIndex:indexReplace withObject:dictOfarrResponseInspection];
    
    @try {
           NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
           
           [arrResponseInspection replaceObjectAtIndex:indexReplace withObject:dictOfarrResponseInspection];

        }
        @catch (NSException * e) {
           NSLog(@"Exception: %@", e);
        }
        @finally {
           NSLog(@"finally");
        }
    
}
-(void)commonMethodToSaveValue :(NSString*)strValueToSet :(int)tagggg{
    
    int taggg=tagggg;
    
    if (taggg==2147483647) {
        
        
        
    } else {
        
        NSString *strTagg=arrOfIndexes[taggg];
        
        NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
        
        int indexKKK =[arrOfTag[2] intValue];
        int indexJJJ =[arrOfTag[1] intValue];
        int indexIII =[arrOfTag[0] intValue];
        
        
        NSDictionary *dictData=arrResponseInspection[indexIII];
        
        NSMutableArray *arrSections =[[NSMutableArray alloc]init];
        //arrSections=[dictData valueForKey:@"Sections"];
        
        [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
        
        NSDictionary *dictDataControls=arrSections[indexJJJ];
        
        NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
        // arrOfControls=[dictDataControls valueForKey:@"Controls"];
        
        [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
        
        NSDictionary *dictOfOptions=arrOfControls[indexKKK];
        
        
        
        //For Validations new On 18 sep sunday at home
        
        NSString *errorMessage;
        
        if ([[dictOfOptions valueForKey:@"Element"] isEqualToString:@"el-email"]) {
            
            strValueToSet =  [strValueToSet stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            errorMessage=[self validationCheck :strValueToSet :@"el-email"];
            
            
        }
        
        //    if (errorMessage) {
        //        NSString *strTitle = Alert;
        //        [global AlertMethod:strTitle :errorMessage];
        //        [DejalBezelActivityView removeView];
        //    } else {
        
        if (errorMessage) {
            NSString *strTitle = Alert;
            [global AlertMethod:strTitle :errorMessage];
            [DejalBezelActivityView removeView];
        }
        
        NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
        
        
#pragma mark- ---------------------Dictionary of Controls To Be Updated-----------------
        
        //Dictionary of Controls To Be Updated
        
        NSArray *arrOfOptionsToSend=arrDataOptionss;
        
        NSArray *objValueControls=[NSArray arrayWithObjects:
                                   [dictOfOptions valueForKey:@"Position"],
                                   [dictOfOptions valueForKey:@"Element"],
                                   [dictOfOptions valueForKey:@"Label"],
                                   strValueToSet,
                                   [dictOfOptions valueForKey:@"Id"],
                                   [dictOfOptions valueForKey:@"Name"],
                                   [dictOfOptions valueForKey:@"PlaceHolder"],
                                   [dictOfOptions valueForKey:@"HelpText"],
                                   [dictOfOptions valueForKey:@"Type"],
                                   [dictOfOptions valueForKey:@"MaxLength"],
                                   [dictOfOptions valueForKey:@"MinLength"],
                                   [dictOfOptions valueForKey:@"Required"],
                                   [dictOfOptions valueForKey:@"ReadOnly"],
                                   [dictOfOptions valueForKey:@"Default"],
                                   [dictOfOptions valueForKey:@"Format"],
                                   [dictOfOptions valueForKey:@"Rows"],
                                   [dictOfOptions valueForKey:@"ClassName"],
                                   [dictOfOptions valueForKey:@"IsPrimaryControl"],
                                   arrOfOptionsToSend,nil];
        
        NSArray *objKeyControls=[NSArray arrayWithObjects:
                                 @"Position",
                                 @"Element",
                                 @"Label",
                                 @"Value",
                                 @"Id",
                                 @"Name",
                                 @"PlaceHolder",
                                 @"HelpText",
                                 @"Type",
                                 @"MaxLength",
                                 @"MinLength",
                                 @"Required",
                                 @"ReadOnly",
                                 @"Default",
                                 @"Format",
                                 @"Rows",
                                 @"ClassName",
                                 @"IsPrimaryControl",
                                 @"Options",nil];
        
        NSDictionary *dictOfControlsToUpdate=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
        
        [arrOfControls replaceObjectAtIndex:indexKKK withObject:dictOfControlsToUpdate];
        
        
#pragma mark- ---------------------Dictinary Of Sections To Be Updated-----------------
        
        //Dictinary Of Sections To Be Updated
        
        NSArray *objValueSections=[NSArray arrayWithObjects:
                                   [dictDataControls valueForKey:@"Name"],
                                   [dictDataControls valueForKey:@"ColumnType"],
                                   [dictDataControls valueForKey:@"IsPrimaryControl"],
                                   arrOfControls,nil];
        NSArray *objKeySections=[NSArray arrayWithObjects:
                                 @"Name",
                                 @"ColumnType",
                                 @"IsPrimaryControl",
                                 @"Controls",nil];
        
        NSDictionary *dictOfSectionToUpdate=[[NSDictionary alloc] initWithObjects:objValueSections forKeys:objKeySections];
        
        [arrSections replaceObjectAtIndex:indexJJJ withObject:dictOfSectionToUpdate];
        
        
#pragma mark- ---------------------Dictionary of arrResponseInspection-----------------
        
        //Dictionary of arrResponseInspection  strLeadId
        
        
        NSArray *objValuearrResponseInspection=[NSArray arrayWithObjects:
                                                [dictData valueForKey:@"InspectionId"],
                                                strLeadId,
                                                [dictData valueForKey:@"FormId"],
                                                [dictData valueForKey:@"FormName"],
                                                [dictData valueForKey:@"CshtmlFileName"],
                                                [dictData valueForKey:@"CshtmlFilePath"],
                                                [dictData valueForKey:@"HtmlConvertedFileName"],
                                                [dictData valueForKey:@"HtmlConvertedFilePath"],
                                                [dictData valueForKey:@"HtmlFileName"],
                                                [dictData valueForKey:@"HtmlFilePath"],
                                                [dictData valueForKey:@"CompanyId"],
                                                [dictData valueForKey:@"DepartmentName"],
                                                [dictData valueForKey:@"DepartmentSysName"],
                                                [dictData valueForKey:@"BranchName"],
                                                [dictData valueForKey:@"BranchSysName"],
                                                [dictData valueForKey:@"FlowType"],
                                                arrSections,
                                                [dictData valueForKey:@"SequenceNo"],nil];
        
        NSArray *objKeyarrResponseInspection=[NSArray arrayWithObjects:
                                              @"InspectionId",
                                              @"LeadId",
                                              @"FormId",
                                              @"FormName",
                                              @"CshtmlFileName",
                                              @"CshtmlFilePath",
                                              @"HtmlConvertedFileName",
                                              @"HtmlConvertedFilePath",
                                              @"HtmlFileName",
                                              @"HtmlFilePath",
                                              @"CompanyId",
                                              @"DepartmentName",
                                              @"DepartmentSysName",
                                              @"BranchName",
                                              @"BranchSysName",
                                              @"FlowType",
                                              @"Sections",
                                              @"SequenceNo",nil];
        
        NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
        
        [arrResponseInspection replaceObjectAtIndex:indexIII withObject:dictOfarrResponseInspection];
        
        
#pragma mark- ---------------------Converting Finally To Json-----------------
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:arrResponseInspection])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrResponseInspection options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"FINAL FORM JSON: %@", jsonString);
            }
        }
        //  }
        
    }
}

-(void)sendingFinalDynamicJsonToServer{
    
    NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                       arrResponseInspection,nil];
    
    NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                     @"InspectionFormsData",nil];
    
    NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlSalesInspectionDynamicFormSubmission];
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     if ([str containsString:@"Success"]) {
                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [alert show];
                     }else{
                         
                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [alert show];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
                 
                 [self goToNextScreen];
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Core Data Methods-----------------
//============================================================================
//============================================================================


-(void)saveToCoreDataSalesDynamic :(NSArray*)arrOfSalesDynamicToSave{
    
    //==================================================================================
    //==================================================================================
    [self deleteFromCoreDataSalesDynamic];
    //==================================================================================
    //==================================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    
    SalesDynamicInspection *objSalesDynamic = [[SalesDynamicInspection alloc]initWithEntity:entitySalesDynamic insertIntoManagedObjectContext:context];
    objSalesDynamic.arrFinalInspection=arrOfSalesDynamicToSave;
    objSalesDynamic.leadId=strLeadId;
    objSalesDynamic.companyKey=strCompanyKey;
    objSalesDynamic.userName=strUserName;
    objSalesDynamic.empId=strEmpID;
    objSalesDynamic.isSentToServer=@"Yes";
    
    NSError *error1;
    [context save:&error1];
    
    //==================================================================================
    //==================================================================================
    [self FetchFromCoreDataToShowSalesDynamic];
    //==================================================================================
    //==================================================================================
    
}

-(void)saveToCoreDataSalesDynamicFinal :(NSArray*)arrOfSalesDynamicToSave{
    
    //==================================================================================
    //==================================================================================
    [self deleteFromCoreDataSalesDynamic];
    //==================================================================================
    //==================================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    
    SalesDynamicInspection *objSalesDynamic = [[SalesDynamicInspection alloc]initWithEntity:entitySalesDynamic insertIntoManagedObjectContext:context];
    objSalesDynamic.arrFinalInspection=arrOfSalesDynamicToSave;
    objSalesDynamic.leadId=strLeadId;
    objSalesDynamic.companyKey=strCompanyKey;
    objSalesDynamic.userName=strUserName;
    objSalesDynamic.empId=strEmpID;
    objSalesDynamic.isSentToServer=@"No";
    NSError *error1;
    [context save:&error1];
    
}
-(void)FetchFromCoreDataToShowSalesDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND userName = %@",strLeadId,strUsername];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            
            [self dynamicFormCreation];
            
        }else{
            
            [self getDynamicFormSales];
            
        }
    }
    else
    {
        matches=arrAllObj[0];
        NSArray *arrTemp = [matches valueForKey:@"arrFinalInspection"];
        
        if ([arrTemp isKindOfClass:[NSDictionary class]]) {
            
            arrResponseInspection =[[NSMutableArray alloc]init];
            NSDictionary *dictdata=(NSDictionary*)arrTemp;
            [arrResponseInspection addObject:dictdata];
            
        } else {
            
            arrResponseInspection =[[NSMutableArray alloc]init];
            [arrResponseInspection addObjectsFromArray:arrTemp];
            
        }
        
        // Change For To check if commercial or residentail flow saved and which flow type is of lead if it is changed then need to delete saved dynamic form and re enter new dynamic form.
        
        NSMutableArray *arrTempToDelete = [[NSMutableArray alloc]init];
        
        for (int k11=0; k11<arrResponseInspection.count; k11++) {
            
            NSDictionary *dictDataTemp =arrResponseInspection[k11];
            
            NSString *strFlowTypeDynamicForm;
            strFlowTypeDynamicForm=[NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"FlowType"]];
            
            NSUserDefaults *defsFlowType=[NSUserDefaults standardUserDefaults];
            
            if( [[defsFlowType valueForKey:@"flowType"] isEqualToString:@"Commercial"]||[[defsFlowType valueForKey:@"flowType"] isEqualToString:@"commercial"])
            {
                
                if ([strFlowTypeDynamicForm isEqualToString:@"Residential"] || [strFlowTypeDynamicForm isEqualToString:@"residential"]) {
                    
                    [arrTempToDelete addObject:dictDataTemp];
                    
                }
                
            }else{
                
                if ([strFlowTypeDynamicForm isEqualToString:@"Commercial"] || [strFlowTypeDynamicForm isEqualToString:@"commercial"]) {
                    
                    [arrTempToDelete addObject:dictDataTemp];
                    
                }
                
            }
            
        }
        
        if (arrTempToDelete.count>0) {
            
            [arrResponseInspection removeObjectsInArray:arrTempToDelete];
            
        }
        
        // ENd change
        
        //Change For getting Department from Masters which are not present in Database for lead
        
        NSMutableArray *arrOfDepartmentPresentInDB =[[NSMutableArray alloc] init];
        
        for (int k=0; k<arrResponseInspection.count; k++) {
            
            NSDictionary *dictData=arrResponseInspection[k];
            
            NSString *strDepartmentSysNamePresentInDB=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentSysName"]];
            
            [arrOfDepartmentPresentInDB addObject:strDepartmentSysNamePresentInDB];
            
        }
        
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            
            [self dynamicFormCreation];
            
        }else{
            
            [self getDynamicFormSalesEvenIfPresentInDB:arrOfDepartmentPresentInDB];
            
        }
        
        
        // [self dynamicFormCreation];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}
-(void)FetchFromCoreDataToShowSalesDynamicInCaseOnline{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND userName = %@",strLeadId,strUsername];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            
            [self dynamicFormCreation];
            
        }else{
            
            [self getDynamicFormSales];
            
        }
        
    }
    else
    {
        matches=arrAllObj[0];
        
        NSString *strIsSentToServer=[matches valueForKey:@"isSentToServer"];
        strIsSentToServer=@"no";
        if ([strIsSentToServer isEqualToString:@"Yes"]) {
            
            [self getDynamicFormSales];
            
        } else {
            
            NSArray *arrTemp = [matches valueForKey:@"arrFinalInspection"];
            arrResponseInspection =[[NSMutableArray alloc]init];
            [arrResponseInspection addObjectsFromArray:arrTemp];
            
            
            // Change For To check if commercial or residentail flow saved and which flow type is of lead if it is changed then need to delete saved dynamic form and re enter new dynamic form.
            
            NSMutableArray *arrTempToDelete = [[NSMutableArray alloc]init];
            
            for (int k11=0; k11<arrResponseInspection.count; k11++) {
                
                NSDictionary *dictDataTemp =arrResponseInspection[k11];
                
                NSString *strFlowTypeDynamicForm;
                strFlowTypeDynamicForm=[NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"FlowType"]];
                
                NSUserDefaults *defsFlowType=[NSUserDefaults standardUserDefaults];
                
                if( [[defsFlowType valueForKey:@"flowType"] isEqualToString:@"Commercial"]||[[defsFlowType valueForKey:@"flowType"] isEqualToString:@"commercial"])
                {
                    
                    if ([strFlowTypeDynamicForm isEqualToString:@"Residential"] || [strFlowTypeDynamicForm isEqualToString:@"residential"]) {
                        
                        [arrTempToDelete addObject:dictDataTemp];
                        
                    }
                    
                }else{
                    
                    if ([strFlowTypeDynamicForm isEqualToString:@"Commercial"] || [strFlowTypeDynamicForm isEqualToString:@"commercial"]) {
                        
                        [arrTempToDelete addObject:dictDataTemp];
                        
                    }
                    
                }
                
            }
            
            if (arrTempToDelete.count>0) {
                
                [arrResponseInspection removeObjectsInArray:arrTempToDelete];
                
            }
            
            // ENd change
            
            
            //Change For getting Department from Masters which are not present in Database for lead
            
            NSMutableArray *arrOfDepartmentPresentInDB =[[NSMutableArray alloc] init];
            
            for (int k=0; k<arrResponseInspection.count; k++) {
                
                NSDictionary *dictData=arrResponseInspection[k];
                
                NSString *strDepartmentSysNamePresentInDB=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentSysName"]];
                
                [arrOfDepartmentPresentInDB addObject:strDepartmentSysNamePresentInDB];
            }
            
            
            if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
            {
                
                [self dynamicFormCreation];
                
            }
            else{
                
                [self getDynamicFormSalesEvenIfPresentInDB:arrOfDepartmentPresentInDB];
                
            }
            
            // [self dynamicFormCreation];
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)getDynamicFormSales{
    
    NSUserDefaults *defsDynamic=[NSUserDefaults standardUserDefaults];
    
    NSArray *arrOfMasterDynamicData=[defsDynamic objectForKey:@"MasterSalesAutomationDynamicForm"];
    
    NSMutableArray *arrOfDynamicDataInspections=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfMasterDynamicData.count; k++) {
        
        NSDictionary *dictData=arrOfMasterDynamicData[k];
        
        NSString *strBranchSysNameD=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BranchSysName"]];
        NSString *strFlowTypeDynamicForm;
        strFlowTypeDynamicForm=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"FlowType"]];
        
        NSUserDefaults *defsFlowType=[NSUserDefaults standardUserDefaults];
        
        if( [[defsFlowType valueForKey:@"flowType"] isEqualToString:@"Commercial"]||[[defsFlowType valueForKey:@"flowType"] isEqualToString:@"commercial"])
        {
            if ([strBranchSysNameD isEqualToString:_strBrnachSysName] && ([strFlowTypeDynamicForm caseInsensitiveCompare:@"Commercial"] == NSOrderedSame || [strFlowTypeDynamicForm caseInsensitiveCompare:@"All"] == NSOrderedSame ))
            {
                
                [arrOfDynamicDataInspections addObject:dictData];
                
                
            }
        }
        else
        {
            if ([strBranchSysNameD isEqualToString:_strBrnachSysName] && ([strFlowTypeDynamicForm caseInsensitiveCompare:@"Residential"] == NSOrderedSame || [strFlowTypeDynamicForm caseInsensitiveCompare:@"All"] == NSOrderedSame ))
            {
                [arrOfDynamicDataInspections addObject:dictData];
                
            }
        }
        
        
        /*if ([strBranchSysNameD isEqualToString:_strBrnachSysName]) {
         
         [arrOfDynamicDataInspections addObject:dictData];
         
         }*/
    }
    
    if (arrOfDynamicDataInspections.count>0) {
        
        arrResponseInspection =[[NSMutableArray alloc]init];
        arrResponseInspection =[[NSMutableArray alloc]init];
        [arrResponseInspection addObjectsFromArray:arrOfDynamicDataInspections];
        
        for (int k=0; k<arrOfDynamicDataInspections.count; k++) {
            
            [self saveLeadIdInMasterForms:k];
            
        }
        
        [self dynamicFormCreation];
        
        //[self saveToCoreDataSalesDynamic:arrResponseInspection];
        
    }else{
        
        [self dynamicFormCreation];
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Info" message:@"No data available." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

-(void)getDynamicFormSalesEvenIfPresentInDB :(NSArray*)arrOfDepartmentAlreadyPresentInDB{
    
    NSUserDefaults *defsDynamic=[NSUserDefaults standardUserDefaults];
    
    NSArray *arrOfMasterDynamicData=[defsDynamic objectForKey:@"MasterSalesAutomationDynamicForm"];
    
    NSMutableArray *arrOfDynamicDataInspections=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfMasterDynamicData.count; k++) {
        
        NSDictionary *dictData=arrOfMasterDynamicData[k];
        
        NSString *strBranchSysNameD=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BranchSysName"]];
        
        if ([strBranchSysNameD isEqualToString:_strBrnachSysName]) {
            
            NSString *strDepartmentSysNameD=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentSysName"]];
            
            if ([arrOfDepartmentAlreadyPresentInDB containsObject:strDepartmentSysNameD]) {
                
                NSLog(@"Already Present in Database DepartmentSysNAme %@",strDepartmentSysNameD);
                
            } else {
                
                //[arrOfDynamicDataInspections addObject:dictData];
                
                NSString *strFlowTypeDynamicForm;
                strFlowTypeDynamicForm=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"FlowType"]];
                
                NSUserDefaults *defsFlowType=[NSUserDefaults standardUserDefaults];
                
                if( [[defsFlowType valueForKey:@"flowType"] isEqualToString:@"Commercial"]||[[defsFlowType valueForKey:@"flowType"] isEqualToString:@"commercial"])
                {
                    if (([strFlowTypeDynamicForm caseInsensitiveCompare:@"Commercial"] == NSOrderedSame || [strFlowTypeDynamicForm caseInsensitiveCompare:@"All"] == NSOrderedSame ))
                    {
                        
                        [arrOfDynamicDataInspections addObject:dictData];
                        
                        
                    }
                }
                else
                {
                    if (([strFlowTypeDynamicForm caseInsensitiveCompare:@"Residential"] == NSOrderedSame || [strFlowTypeDynamicForm caseInsensitiveCompare:@"All"] == NSOrderedSame ))
                    {
                        [arrOfDynamicDataInspections addObject:dictData];
                        
                    }
                }
                
                /*
                 
                 if ([strDepartmentSysNameD isEqualToString:_strDepartmentSysName]) {
                 
                 [arrOfDynamicDataInspections addObject:dictData];
                 
                 }else if([strDepartmentSysNameD caseInsensitiveCompare:@"General"] == NSOrderedSame){
                 
                 [arrOfDynamicDataInspections addObject:dictData];
                 
                 }
                 */
                
            }
        }
    }
    if (arrOfDynamicDataInspections.count>0) {
        
        // arrResponseInspection =[[NSMutableArray alloc]init];
        //  arrResponseInspection =[[NSMutableArray alloc]init];
        [arrResponseInspection addObjectsFromArray:arrOfDynamicDataInspections];
        
        for (int k=0; k<arrOfDynamicDataInspections.count; k++) {
            
            [self saveLeadIdInMasterForms:k];
            
        }
        [self dynamicFormCreation];
        
    }else{
        
        [self dynamicFormCreation];
        
    }
}

-(void)getDynamicFormSalesOld{
    
    NSString *strUrl;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strLeadId=[defs valueForKey:@"LeadId"];
    
    strUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",strServiceUrlMain,UrlGetSalesInspectionDynamicForm,strCompanyKey,UrlGetSalesInspectionDynamicFormleadID,strLeadId,UrlGetSalesInspectionDynamicFormInspectionId,@"0"];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Inspection Data..."];
    
    //============================================================================
    //============================================================================
    
    NSString *strType=@"salesInspection";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     
                     if (response.count==0) {
                         
                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Information..!!" message:@"No Data Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [alert show];
                         
                     }
                     else
                     {
                         dictForAppoint=response;
                         
                         NSString *strException;
                         @try {
                             if([dictForAppoint objectForKey:@"ExceptionMessage"] != nil) {
                                 // The key existed...
                                 strException=[dictForAppoint valueForKey:@"ExceptionMessage"];
                                 
                             }else{
                                 
                             }
                         } @catch (NSException *exception) {
                             
                         } @finally {
                             
                         }
                         if (strException.length==0) {
                             
                             [self saveToCoreDataSalesDynamic:(NSArray*)response];
                             
                         } else {
                             
                             UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to fetch data from server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             [alert show];
                             
                         }
                         
                     }
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
}

-(void)deleteFromCoreDataSalesDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND userName = %@",strLeadId,strUsername];
    [allData setPredicate:predicate];
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)updateSalesDynamicJsonInDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND userName = %@",strLeadId,strUsername];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        [global AlertMethod:@"Error" :@"Unable to save.Please try again later."];
    }
    else
    {
        matches=arrAllObj[0];
        
        [matches setValue:arrResponseInspection forKey:@"arrFinalInspection"];
        
        NSError *error1;
        [context save:&error1];
        
        [self goToNextScreen];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)goToNextScreen
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    
    if ([dictMasters isKindOfClass:[NSDictionary class]])
    {
        [global updateSalesZSYNC:strLeadId :@"yes"];
        
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
        }
        else
        {
            [self saveImageToCoreData];
            
        }
        
        NSUserDefaults *defsFlowType=[NSUserDefaults standardUserDefaults];
        if( [[defsFlowType valueForKey:@"flowType"] isEqualToString:@"Commercial"]||[[defsFlowType valueForKey:@"flowType"] isEqualToString:@"commercial"])
        {
            UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPad" bundle:nil];
            ClarkPestSalesSelectServiceiPad*objSalesAutoService=[storyboard instantiateViewControllerWithIdentifier:@"ClarkPestSalesSelectServiceiPad"];
            [self.navigationController pushViewController:objSalesAutoService animated:YES];
        }
        else
        {
            
            UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
            SalesAutomationSelectServiceiPad *objSalesAutoService=[storyboard instantiateViewControllerWithIdentifier:@"SalesAutomationSelectServiceiPad"];
            [self.navigationController pushViewController:objSalesAutoService animated:YES];
        }
        
    }
    else
    {
        [global AlertMethod:Alert :NoDataAvailable];
    }
    
}
- (IBAction)actionGlobalSync:(id)sender {
   
    [self endEditing];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"Alert!":@"No Internet connection available"];
    }
    else
    {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:@"startLoader" object:nil];
        [self performSelector:@selector(methodSync) withObject:nil afterDelay:0.5];
        
    }
    
}
-(void)methodSync
{
    GlobalSyncViewController *objGlobalSyncViewController=[[GlobalSyncViewController alloc]init];
    NSString *str=@"general";
    
    [objGlobalSyncViewController syncCall:str];
}
-(void)stopLoader
{
    [DejalBezelActivityView removeView];
}
//Nilind 28 Feb
-(void)fetchDepartmentNameBySysName
{
    
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    
    
    //Branch Master Fetch
    
    NSArray *arrBranchMaster;
    arrBranchMaster=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    
    NSMutableArray *arrDeptVal,*arrDeptKey;
    arrDeptVal=[[NSMutableArray alloc]init];
    arrDeptKey=[[NSMutableArray alloc]init];
    
    NSDictionary *dictMasterKeyValue;
    for (int i=0; i<arrBranchMaster.count; i++)
    {
        NSDictionary *dict=[arrBranchMaster objectAtIndex:i];
        NSArray *arrDepartments=[dict valueForKey:@"Departments"];
        for (int k=0; k<arrDepartments.count; k++)
        {
            NSDictionary *dict1=[arrDepartments objectAtIndex:k];
            [arrDeptVal addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"Name"]]];
            [arrDeptKey addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"SysName"]]];
        }
        
    }
    dictMasterKeyValue=[NSDictionary dictionaryWithObjects:arrDeptVal forKeys:arrDeptKey];
    dictDeptNameByKey=dictMasterKeyValue;
    NSLog(@"dictMasterKeyValue>>%@",dictMasterKeyValue);
}

//End
#pragma mark - Nilind 14 Sept change
//Nilind 14 Sept

- (IBAction)actionAddAfterImages:(id)sender
{
    [self endEditing];
    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        
        /* UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
         delegate:self
         cancelButtonTitle:@"Cancel"
         destructiveButtonTitle:nil
         otherButtonTitles:@"Capture New", @"Gallery", nil];
         
         [actionSheet showInView:self.view];*/
        
        [self newAlertMethodAddImage];
        
        
        
    }
}

- (IBAction)actionOnCancelAfterImage:(id)sender
{
    [_viewForAfterImage removeFromSuperview];
    
}

- (IBAction)actionOnAfterImgView:(id)sender
{
    //[self endEditing];
    //[self goToGlobalmage:@"Before"];
    
    UIButton *btn=(UIButton*)sender;
    [self attachImage:btn];
  
}



#pragma mark- ---------- FETCH IMAGE ----------
-(void)fetchImageDetailFromDataBase
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" : leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                /*
                 [arrOfImagenameCollewctionView addObject:leadImagePath];
                 
                 //Nilind 07 June
                 
                 NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                 
                 if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                 
                 [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                 
                 }
                 else
                 {
                 
                 
                 [arrOfImageCaptionGraph addObject:strImageCaption];
                 
                 }
                 
                 
                 NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                 
                 if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                 
                 [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                 
                 } else {
                 
                 [arrOfImageDescriptionGraph addObject:strImageDescription];
                 
                 }
                 
                 
                 */
                //End
                
            }
            else
            {
                
                [arrNoImage addObject:dict_ToSendLeadInfo];
                
                //Nilind 07 June
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageLongitude addObject:strLong];
                    
                }
                //End
                
            }
            
            
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    }
    else
    {
    }
}


-(void)openGalleryy{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera
{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==7)
    {
        
        
        NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        
        NSArray *arrOfImagesDetail=arrNoImage;
        
        arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dictData=arrOfImagesDetail[k];
                
                [arrOfBeforeImages addObject:dictData];
                
            }else{
                
                [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
                
            }
            
        }
        
        arrOfImagesDetail=arrOfBeforeImages;
        
        if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
            [global AlertMethod:Info :NoBeforeImg];
        }else if (arrOfImagesDetail.count==0){
            [global AlertMethod:Info :NoBeforeImg];
        }
        else {
            NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
            for (int k=0; k<arrOfImagesDetail.count; k++) {
                if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dict=arrOfImagesDetail[k];
                    [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
                }else{
                    
                    [arrOfImagess addObject:arrOfImagesDetail[k]];
                    
                }
            }
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMainiPad"
                                                                     bundle: nil];
            ImagePreviewSalesAutoiPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAutoiPad"];
            objByProductVC.arrOfImages=arrOfImagess;
            
            [self.navigationController pushViewController:objByProductVC animated:YES];
            
        }
    }
    else if (buttonIndex == 0)
    {
        NSLog(@"The CApture Image.");
        
        
        if (arrNoImage.count<10)
        {
            NSLog(@"The CApture Image.");
            
            BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Camera Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                         }
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //............
        
    }
    else if (buttonIndex == 1)
    {
        NSLog(@"The Gallery.");
        if (arrNoImage.count<10)
        {
            BOOL isCameraPermissionAvailable=[global isGalleryPermission];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Gallery Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         }
                                         else
                                         {
                                             
                                             
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    
}

//============================================================================

#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==_collectionViewAfterImage)
    {
        return arrNoImage.count;
    }
    else
    {
        return arrGraphImage.count;
    }
    
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==_collectionViewAfterImage)
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    else
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *str = [arrNoImage objectAtIndex:indexPath.row];
    if (collectionView==_collectionViewAfterImage)
    {
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreview : strIndex];
            
        }
    }
    else
    {
        // NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *str;// = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrGraphImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewGraph : strIndex];
            
        }
        
    }
    
    
}
//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        
        
        
    }else if (arrOfImagesDetail.count==0){
        
        
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        if (arrOfImagess.count==1)
        {
        }
        else if (arrOfImagess.count==2)
        {
            
        }
        else
        {
        }
        //_const_ImgView_H.constant=100;  temp comment
        //_const_SaveContinue_B.constant=62;
        
        // 21 April 2020
       // [self downloadImages:arrOfImagess];
        
    }
}


-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            [self ShowFirstImage:str:k];
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    //NIlind 14 Sept
    
    [_collectionViewAfterImage reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    [self ShowFirstImage : name : indexx];
    
}

- (UIImage*)loadImage :(NSString*)name :(int)indexxx
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (indexxx==0)
    {
        
        
    } else if(indexxx==1)
    {
        
        
        
    }else{
        
        
    }
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    return image;
}
-(void)goingToPreview :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMainiPad"
                                                                 bundle: nil];
        ImagePreviewSalesAutoiPad
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAutoiPad"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
            
        }
        else
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=@"Open";
            
        }
        
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaption;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescription;
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backfromDynamicView"];
    [defs synchronize];
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    
    //NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
    NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@%@.jpg",strDate,strTime,strLeadId];
    
    
    [arrNoImage addObject:strImageNamess];
    
    //UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    // [self saveImage:chosenImage :strImageNamess];
    
    [self resizeImage:chosenImage :strImageNamess];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    //Lat long code
       
       CLLocationCoordinate2D coordinate = [global getLocation] ;
       NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
       NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
       [arrImageLattitude addObject:latitude];
       [arrImageLongitude addObject:longitude];
    
    //imageCaption
    
    NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
    
    BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
    
    if (yesImageCaption) {
        
        [self alertViewCustom];
        
    } else {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        [self saveImageToCoreData];
        
    }
   
    
}

//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=2147483647;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=2147483647;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender
{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        CGPoint bottomOffset = CGPointMake(0, _scrollForDynamicView.contentSize.height - _scrollForDynamicView.bounds.size.height);
        [_scrollForDynamicView setContentOffset:bottomOffset animated:YES];
        [self saveImageToCoreData];
        
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}
-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollForDynamicView.contentSize.height - _scrollForDynamicView.bounds.size.height);
    [_scrollForDynamicView setContentOffset:bottomOffset animated:YES];
    
    [self saveImageToCoreData];
}

-(void)alertToEnterImageCaption{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Caption Here...";
        textField.tag=2147483647;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Image Description Here...";
        textField.tag=2147483647;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        UITextField * txtImageDescriptions = textfields[1];
        if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
            
            if (txtHistoricalDays.text.length>0) {
                
                [arrOfImageCaption addObject:txtHistoricalDays.text];
                
            } else {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtImageDescriptions.text.length>0) {
                
                [arrOfImageDescription addObject:txtImageDescriptions.text];
                
            } else {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            }
            
            CGPoint bottomOffset = CGPointMake(0, _scrollForDynamicView.contentSize.height - _scrollForDynamicView.bounds.size.height);
            [_scrollForDynamicView setContentOffset:bottomOffset animated:YES];
            
            
        } else {
            
            [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollForDynamicView.contentSize.height - _scrollForDynamicView.bounds.size.height);
        [_scrollForDynamicView setContentOffset:bottomOffset animated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
//.................................................................................
-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    //CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    CGRect rect = CGRectMake(0.0, 0.0, image.size.width/2, image.size.height/2);
    
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}
#pragma mark- Save & Remove Image  Methods
//============================================================================
//============================================================================

- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
#pragma mark- Note
    /*
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL yesfromback=[defs boolForKey:@"backfromDynamicView"];
    if (yesfromback) {
        
        [defs setBool:NO forKey:@"backfromDynamicView"];
        [defs synchronize];
        
    }
    else
    {
        
        [self deleteImageFromCoreDataSalesInfo];
        
        for (int k=0; k<arrNoImage.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            if ([arrNoImage[k] isKindOfClass:[NSString class]])
            {
                
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[arrNoImage objectAtIndex:k]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                
                
                NSError *error1;
                [context save:&error1];
            }
            else
            {
                NSDictionary *dictData=[arrNoImage objectAtIndex:k];
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                
                
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                
                NSError *error1;
                [context save:&error1];
                
            }
            
        }
        for (int k=0; k<arrGraphImage.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            NSDictionary *dict=[arrGraphImage objectAtIndex:k];
            if ([arrGraphImage[k] isKindOfClass:[NSDictionary class]])
            {
                
                objImageDetail.createdBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdBy"]];
                objImageDetail.createdDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdDate"]];
                objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
                objImageDetail.leadImageId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageId"]];
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageType"]];
                objImageDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"modifiedBy"]];
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                objImageDetail.leadImageCaption=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageCaption"]];
                objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
                //Latlong code
                
                //NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
                objImageDetail.latitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"latitude"]];
                // NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
                objImageDetail.longitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"longitude"]];
                
               
                NSError *error1;
                [context save:&error1];
            }
        }
        
        
        
    }
    */
    //........................................................................
    // [self fetchForUpdate];
    
    //[self fetchImageFromCoreDataStandard];
    
}
-(void)deleteImageFromCoreDataSalesInfo
{
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
    [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    [allDataImage setPredicate:predicateImage];
    
    [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error2 = nil;
    NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
    //error handling goes here
    for (NSManagedObject * data in DataImage) {
        [context deleteObject:data];
    }
    NSError *saveError2 = nil;
    [context save:&saveError2];
    
}

#pragma mark -------------- ADDING GRAPH IMAGE ----------------

- (IBAction)actionOnAddGraphImage:(id)sender
{
    [self endEditing];
    if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"firstGraphImage"];
        [defs setBool:NO forKey:@"servicegraph"];
        
        [defs synchronize];
        
        //        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
        //        EditGraphViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewControlleriPad"];
        //        objSignViewController.strLeadId=strLeadId;
        //        objSignViewController.strCompanyKey=strCompanyKey;
        //        objSignViewController.strUserName=strUserName;
        //        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
        objSignViewController.strLeadId=strLeadId;
        objSignViewController.strCompanyKey=strCompanyKey;
        objSignViewController.strUserName=strUserName;
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    }
}

- (IBAction)actionOnCancelBeforeImage:(id)sender
{
    [self endEditing];
    [_viewForGraphImage removeFromSuperview];
}

- (IBAction)actionOnGraphImageFooter:(id)sender
{
    [self endEditing];
    [self goToGlobalmage:@"Graph"];
    /* CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewForFinalSave.frame.origin.y-_viewForGraphImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewForGraphImage.frame.size.height);
     [_viewForGraphImage setFrame:frameFor_view_BeforeImageInfo];
     [_collectionViewGraphImage reloadData];
     [self.view addSubview:_viewForGraphImage];*/
}
-(void)goingToPreviewGraph :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrGraphImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++)
    {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
        {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }
        else
        {
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++)
        {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        GraphImagePreviewViewControlleriPad
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImagePreviewViewControlleriPad"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.strLeadId=strLeadId;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaptionGraph;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescriptionGraph;
        
        if (([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame) && ([strStageSysName caseInsensitiveCompare:@"Won"]== NSOrderedSame))
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
            
        }
        else
        {
            objImagePreviewSalesAuto.statusOfWorkOrder=@"Open";
            
        }
        
        
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
-(void)fetchImageDetailFromDataBaseForGraph
{
    
    arrGraphImage=nil;
    
    arrGraphImage=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" :leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                
                //[arrOfImagenameCollewctionView addObject:leadImagePath];
                [arrGraphImage addObject:dict_ToSendLeadInfo];
                
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionGraph addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionGraph addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLongitude addObject:strLong];
                    
                }
                
                
            }
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    } else {
    }
    
    [self downloadImagesGraphs:arrGraphImage];
    
}
-(void)downloadImagesGraphs :(NSArray*)arrOfImagesDownload
{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        // NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
        NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrOfImagesDownload objectAtIndex:k];
        }
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    [_collectionViewGraphImage reloadData];
    
}
- (IBAction)actionOnNotesHistory:(id)sender
{
    [self endEditing];
    [self goToHistory];
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    [self endEditing];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ClockInOutViewControlleriPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewControlleriPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
- (IBAction)actionOnChemicalSensitivityList:(id)sender
{
    [self endEditing];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                 bundle: nil];
        ChemicalSensitivityList
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChemicalSensitivityList"];
        objByProductVC.strFrom=@"sales";
        objByProductVC.strId=strLeadId;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
    //End
    
}
-(void)newAlertMethodAddImage
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              NSLog(@"The CApture Image.");
                              
                              
                              if (arrNoImage.count<10)
                              {
                                  NSLog(@"The CApture Image.");
                                  
                                  BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                  
                                  if (isCameraPermissionAvailable) {
                                      
                                      if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                                      {
                                          
                                          [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                                          
                                      }else{
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }
                                  }else{
                                      
                                      UIAlertController *alert= [UIAlertController
                                                                 alertControllerWithTitle:@"Alert"
                                                                 message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                      
                                      UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action)
                                                            {
                                                                
                                                                
                                                                
                                                            }];
                                      [alert addAction:yes];
                                      UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                   NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                   [[UIApplication sharedApplication] openURL:url];
                                                               } else {
                                                                   
                                                               }
                                                           }];
                                      [alert addAction:no];
                                      [self presentViewController:alert animated:YES completion:nil];
                                  }
                                  
                              }
                              else
                              {
                                  chkForDuplicateImageSave=YES;
                                  UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                  [alert show];
                              }
                              //............
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             NSLog(@"The Gallery.");
                             if (arrNoImage.count<10)
                             {
                                 BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                 
                                 if (isCameraPermissionAvailable) {
                                     
                                     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                                     {
                                         
                                         [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                                         
                                     }else{
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }
                                 }else{
                                     
                                     UIAlertController *alert= [UIAlertController
                                                                alertControllerWithTitle:@"Alert"
                                                                message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                preferredStyle:UIAlertControllerStyleAlert];
                                     
                                     UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               
                                                               
                                                           }];
                                     [alert addAction:yes];
                                     UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                handler:^(UIAlertAction * action)
                                                          {
                                                              
                                                              if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                  NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                  [[UIApplication sharedApplication] openURL:url];
                                                              }
                                                              else
                                                              {
                                                                  
                                                                  
                                                                  
                                                              }
                                                              
                                                          }];
                                     [alert addAction:no];
                                     [self presentViewController:alert animated:YES completion:nil];
                                 }
                             }
                             else
                             {
                                 chkForDuplicateImageSave=YES;
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                 [alert show];
                             }
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)endEditing
{
    [self.view endEditing:YES];
}
-(void)goToGlobalmage:(NSString *)strType
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"PestiPhone" bundle:nil];
    GlobalImageVC *objGlobalImageVC=[storyBoard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objGlobalImageVC.strHeaderTitle = strType;
    objGlobalImageVC.strWoId = strLeadId;
    objGlobalImageVC.strWoLeadId = strLeadId;
    objGlobalImageVC.strModuleType = @"SalesFlow";
    
 
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame  && [strStageSysName caseInsensitiveCompare:@"Lost"] == NSOrderedSame  )
    {
        
        objGlobalImageVC.strWoStatus = @"Complete";
        
    }
    else
    {
        objGlobalImageVC.strWoStatus = @"InComplete";
        
    }
    [self.navigationController presentViewController:objGlobalImageVC animated:NO completion:nil];
    
}

-(void)attachImage:(UIButton *)btn
{
    [self endEditing];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* addImage = [UIAlertAction actionWithTitle:@"Add Images" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
        
        [self goToGlobalmage:@"Before"];
        
    }];
    
    UIAlertAction* addDocument = [UIAlertAction actionWithTitle:@"Add Documents" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        
        [self goToUploadDocument];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addImage];
    [alert addAction:addDocument];
    [alert addAction:dismiss];
    alert.popoverPresentationController.sourceView = btn;
    [self presentViewController:alert animated:YES completion:nil];
  
}
-(void)goToUploadDocument
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
    UploadDocumentSales *objUploadDocumentSales=[storyBoard instantiateViewControllerWithIdentifier:@"UploadDocumentSalesiPad"];
    objUploadDocumentSales.strWoId = strLeadId;

    [self.navigationController presentViewController:objUploadDocumentSales animated:NO completion:nil];
    
}
#pragma mark: ------------- Email & Service History -------------

-(void)goToEmailHistory
{
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSString *strLeadNo = [defs valueForKey:@"leadNumber"];
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"CRM_New_iPad" bundle:nil];
    EmailListNew_iPhoneVC *objEmailListNew_iPhoneVC=[storyBoard instantiateViewControllerWithIdentifier:@"EmailListNew_iPhoneVC"];
    objEmailListNew_iPhoneVC.refNo = strLeadNo;
    [self.navigationController pushViewController:objEmailListNew_iPhoneVC animated:NO];
    //[self.navigationController presentViewController:objEmailListNew_iPhoneVC animated:YES completion:nil];
}
-(void)goToServiceHistory
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceNotesHistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
    objByProductVC.strTypeOfService=@"sales";
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
}
-(void)goToHistory
{
    [self endEditing];
   // [self goToGlobalmage:@"Before"];
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@""
                               message:@"Make Your Selection"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* addServiceHistory = [UIAlertAction actionWithTitle:@"Service History" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
        
        [self goToServiceHistory];
        
    }];
    
    UIAlertAction* addEmailHistory = [UIAlertAction actionWithTitle:@"Email History" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        
        [self goToEmailHistory];
        
        
    }];
    
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDestructive
                                                    handler:^(UIAlertAction * action)
                              {
        
    }];
    
    [alert addAction:addServiceHistory];
    [alert addAction:addEmailHistory];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
  
}
@end
