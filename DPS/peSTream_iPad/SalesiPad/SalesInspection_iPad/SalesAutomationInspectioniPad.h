//
//  SalesAutomationInspection.h
//  DPS
//
//  Created by Rakesh Jain on 19/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface SalesAutomationInspectioniPad : UIViewController<UIScrollViewDelegate,NSFetchedResultsControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entitySalesDynamic,*entityImageDetail;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
}
@property (strong, nonatomic) IBOutlet UIButton *btnGlobalSync;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewLeads;
@property (strong, nonatomic) IBOutlet UIView *viewTop;
- (IBAction)actionOnBack:(id)sender;
- (IBAction)actionOnSave:(id)sender;
- (IBAction)action_Cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollForDynamicView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrnScrollDynamic_H;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic,*fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_NameNaccountNo;
@property (strong, nonatomic)  NSString *strBrnachSysName;
@property (strong, nonatomic) IBOutlet UIView *viewSavenCancel;
- (IBAction)actionGlobalSync:(id)sender;
@property (strong, nonatomic)  NSString *strDepartmentSysName;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;



//View For Final Save

@property (weak, nonatomic) IBOutlet UIView *viewForFinalSave;

//View For After Image
@property (strong, nonatomic) IBOutlet UIView *viewForAfterImage;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewAfterImage;
- (IBAction)actionAddAfterImages:(id)sender;
- (IBAction)actionOnCancelAfterImage:(id)sender;
- (IBAction)actionOnAfterImgView:(id)sender;

//View For Graph Image
@property (strong, nonatomic) IBOutlet UIView *viewForGraphImage;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewGraphImage;
- (IBAction)actionOnAddGraphImage:(id)sender;

- (IBAction)actionOnCancelBeforeImage:(id)sender;
- (IBAction)actionOnGraphImageFooter:(id)sender;
- (IBAction)actionOnNotesHistory:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;

@property (strong, nonatomic) IBOutlet UIButton *btnSavenContinue;

- (IBAction)actionOnChemicalSensitivityList:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAddImageFooter;

@end
