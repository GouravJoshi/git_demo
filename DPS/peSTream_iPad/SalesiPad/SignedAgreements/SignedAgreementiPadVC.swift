//
//  SignedAgreementiPadVC.swift
//  DPS
//
//  Created by Saavan Patidar on 07/06/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class SignedAgreementiPadVC: UIViewController {

    // MARK: - -----------------------------------Variables------------------------------------------
    var refresher = UIRefreshControl()
    var arrSignedAgreements = NSArray()

    
    // MARK: - -----------------------------------IBoutlets-------------------------------------------

    @IBOutlet weak var tblView: UITableView!
    
    // MARK: - -----------------------------------View Life Cycle----------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()

        // This will remove extra separators from tableview
        tblView.tableFooterView = UIView();
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tblView!.addSubview(refresher)
        
        setDefaultFilterValuesSignedAgreement()
        
        // Saving date for last visit for showing badge count on dashboard
        // @"LastDateSignedAgreements"
        
        nsud.set(Global().strCurrentDateFormatted("MM/dd/yyyy hh:mm a", "EST"), forKey: "LastDateSignedAgreements")
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.getSignedAgreements()
        
    }
    
    // MARK: - -----------------------------------Button Actions-----------------------------------

    @IBAction func action_Back(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func action_Filter(_ sender: Any) {
        
        let mainStoryboardLocal = UIStoryboard(name: "CRMiPad", bundle: nil)
        let vc = mainStoryboardLocal.instantiateViewController(withIdentifier: "FilterSignedAgreementiPadVC")as! FilterSignedAgreementiPadVC
        self.present(vc, animated: false, completion: {})
        
    }
    
    
    // MARK: - -----------------------------------Functions------------------------------------------

    func getSignedAgreements() {
        
        let dictParameters = nsud.value(forKey: "FilterDictSignedAgreement")
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
//        var strCompanyKey = String()
//
//        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
//
//            strCompanyKey = "\(value)"
//
//        }
        
        var strUrl =  ""
        
        let strUrlMain = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") as? String
        
        strUrl = strUrlMain! + UrlSalesSignedAgreements
        
        FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
        
        WebService.postRequestWithHeaders(dictJson: dictParameters as! NSDictionary, url: strUrl , responseStringComing: "signedAgreements") { (Response, Status) in
            
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
                self.refresher.endRefreshing()

            }
            if(Status){
                
                let dictAddress = Response["data"] as! NSDictionary

                let arrData = dictAddress["signedAgreements"] as! NSArray
                
                //let arrData = Response["data"] as! NSArray
                
                if arrData.isKind(of: NSArray.self) {
                    
                    self.arrSignedAgreements = arrData
                    
                    if self.arrSignedAgreements.count == 0 {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        
                    }
                    
                    self.tblView.reloadData()
                    
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }

            }
            else
            {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)

            }
            
        }
        
    }
    
    @objc func RefreshloadData() {
        
        self.getSignedAgreements()
        
    }
    
    @objc func actionCall(_ sender: UIButton){
        
        var dictData = arrSignedAgreements[sender.tag] as! NSDictionary
        
        dictData = removeNullFromDict(dict: dictData.mutableCopy() as! NSMutableDictionary)

        //let phoneNo = dictData.value(forKey: "PhoneNo") as? String
        let primaryPhoneNo = dictData.value(forKey: "PrimaryPhone") as? String
        let secondaryPhoneNo = dictData.value(forKey: "SecondaryPhone") as? String
        let cellNo = dictData.value(forKey: "CellNo") as? String

        let alertController = UIAlertController(title: "Select to Call", message: "", preferredStyle:UIAlertController.Style.alert)
        
        if (primaryPhoneNo?.count)! > 0 {
            
            alertController.addAction(UIAlertAction(title: "Primary Phone : \(primaryPhoneNo ?? "")", style: UIAlertAction.Style.default)
            { action -> Void in
                
                Global().calling(primaryPhoneNo!)
                
            })
            
        }
        if (secondaryPhoneNo?.count)! > 0 {
            
            alertController.addAction(UIAlertAction(title: "Secondary Phone : \(secondaryPhoneNo ?? "")", style: UIAlertAction.Style.default)
            { action -> Void in
                
                Global().calling(secondaryPhoneNo!)
                
            })
            
        }
        if (cellNo?.count)! > 0 {
            
            alertController.addAction(UIAlertAction(title: "Cell No : \(cellNo ?? "")", style: UIAlertAction.Style.default)
            { action -> Void in
                
                Global().calling(cellNo!)
                
            })
            
        }
        if ((primaryPhoneNo?.count)! > 0) || (secondaryPhoneNo?.count)! > 0 || (cellNo?.count)! > 0 {
            
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default)
            { action -> Void in
                
                
                
            })
            
            self.present(alertController, animated: true, completion: nil)

        }
        
        //self.present(alertController, animated: true, completion: nil)
        
        //Global().calling(phoneNo!)
        
    }
    
    @objc func actionAddress(_ sender: UIButton){
        
        var dictData = arrSignedAgreements[sender.tag] as! NSDictionary
        
        dictData = removeNullFromDict(dict: dictData.mutableCopy() as! NSMutableDictionary)

        let address = dictData.value(forKey: "ServiceAddress") as? String
        
        Global().redirect(onAppleMap: self, address)
        
    }
    
    @objc func actionEmail(_ sender: UIButton){
        
        var dictData = arrSignedAgreements[sender.tag] as! NSDictionary
        
        dictData = removeNullFromDict(dict: dictData.mutableCopy() as! NSMutableDictionary)

        //let email = dictData.value(forKey: "Email") as? String
        let primarEmail = dictData.value(forKey: "PrimaryEmail") as? String
        let secondaryEmail = dictData.value(forKey: "SecondaryEmail") as? String

        let arrEmail = NSMutableArray()
        
        if (primarEmail?.count)! > 0 {
            
            arrEmail.add(primarEmail!)
            
        }
        if (secondaryEmail?.count)! > 0 {
            
            arrEmail.add(secondaryEmail!)

        }
        
        if arrEmail.count > 0 {

            openEmailComposer(arrEmail.componentsJoined(by: ","), strSubject: "", strBody: "", controller: self)
            
        }

    }
    
    @objc func actionPrint(_ sender: UIButton){
        
        var dictData = arrSignedAgreements[sender.tag] as! NSDictionary
        
        dictData = removeNullFromDict(dict: dictData.mutableCopy() as! NSMutableDictionary)

        let opportunityNo = dictData.value(forKey: "OpportunityNo") as? String
        
        let mainStoryboardLocal = UIStoryboard(name: "MainiPad", bundle: nil)
        let vc = mainStoryboardLocal.instantiateViewController(withIdentifier: "PrintiPhoneViewController")as! PrintiPhoneViewController
        vc.strLeadId = opportunityNo
        vc.strAgreementName=""
        vc.strProposalName=""
        vc.strInspectionName=""
        self.present(vc, animated: false, completion: {})
        
    }
    
    func openEmailComposer(_ stEmailIDs: String?, strSubject: String?, strBody: String?, controller: UIViewController?) {
        var strSubject = strSubject
        var strBody = strBody
        
        if (strSubject?.count ?? 0) == 0 {
            
            strSubject = ""
        }
        if (strBody?.count ?? 0) == 0 {
            
            strBody = ""
        }
        
        let arrOfemails = stEmailIDs?.components(separatedBy: ",")
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject(strSubject ?? "")
            mail.setMessageBody(strBody ?? "", isHTML: false)
            mail.setToRecipients(arrOfemails)
            controller?.present(mail, animated: true)
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertMailComposer, viewcontrol: controller!)
            
        }
        
    }
    
}


//==============================================================================================
// MARK: - MFMailComposeViewControllerDelegate
//==============================================================================================


extension SignedAgreementiPadVC : MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
        
    }
}


//==============================================================================================
// MARK: - ----------------------------TableVIew Delagtes----------------------------
//==============================================================================================

extension SignedAgreementiPadVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSignedAgreements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "SignedAgreement", for: indexPath as IndexPath) as! tblCell
        
        cell.btn_SignedAgreementsCall.tag = indexPath.row
        cell.btn_SignedAgreementsAddress.tag = indexPath.row
        cell.btn_SignedAgreementsPrint.tag = indexPath.row
        cell.btn_SignedAgreementsEmail.tag = indexPath.row
        cell.btn_SignedAgreementsEmaillbl.tag = indexPath.row
        cell.btn_SignedAgreementsAddresslbl.tag = indexPath.row

        cell.btn_SignedAgreementsCall.addTarget(self, action: #selector(actionCall(_:)), for: .touchUpInside)
        cell.btn_SignedAgreementsAddress.addTarget(self, action: #selector(actionAddress(_:)), for: .touchUpInside)
        cell.btn_SignedAgreementsPrint.addTarget(self, action: #selector(actionPrint(_:)), for: .touchUpInside)
        cell.btn_SignedAgreementsEmail.addTarget(self, action: #selector(actionEmail(_:)), for: .touchUpInside)
        cell.btn_SignedAgreementsAddresslbl.addTarget(self, action: #selector(actionAddress(_:)), for: .touchUpInside)
        cell.btn_SignedAgreementsEmaillbl.addTarget(self, action: #selector(actionEmail(_:)), for: .touchUpInside)
        
        var dictData = arrSignedAgreements[indexPath.row] as! NSDictionary
        
        dictData = removeNullFromDict(dict: dictData.mutableCopy() as! NSMutableDictionary)
        
        let accNo = dictData.value(forKey: "AccountNo") as? String
        let oppNo = dictData.value(forKey: "OpportunityNo") as? String
        //let estValue = dictData.value(forKey: "EstimatedValue") as? String
        let fN = dictData.value(forKey: "FirstName") as? String
        let mN = dictData.value(forKey: "MiddleName") as? String
        let lN = dictData.value(forKey: "LastName") as? String
        
        let arrName = NSMutableArray()

        if (fN?.count)! > 0 {
            
            arrName.add(fN!)
            
        }
        if (mN?.count)! > 0 {
            
            arrName.add(mN!)
            
        }
        if (lN?.count)! > 0 {
            
            arrName.add(lN!)
            
        }
        
        let customerName = arrName.componentsJoined(by: " ")
        
        let companyName = dictData.value(forKey: "CompanyName") as? String
        //let salesRep = dictData.value(forKey: "SalesPersonName") as? String
        //let opportunityName = dictData.value(forKey: "OpportunityName") as? String
        let signedDate = dictData.value(forKey: "ClientSignedDate") as? String
        let scheduleDate = dictData.value(forKey: "ScheduleFor") as? String
        let flowType = dictData.value(forKey: "FlowType") as? String
        let address = dictData.value(forKey: "ServiceAddress") as? String
        //let email = dictData.value(forKey: "Email") as? String
        
        let primarEmail = dictData.value(forKey: "PrimaryEmail") as? String
        let secondaryEmail = dictData.value(forKey: "SecondaryEmail") as? String
        
        let arrEmail = NSMutableArray()
        
        if (primarEmail?.count)! > 0 {
            
            arrEmail.add(primarEmail!)
            
        }
        if (secondaryEmail?.count)! > 0 {
            
            arrEmail.add(secondaryEmail!)
            
        }
        
//        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
//
//                var strEmployeeName = String()
//
//                if let value = dictLoginData.value(forKeyPath: "EmployeeName") {
//
//                    strEmployeeName = "\(value)"
//
//                }
        
        
        cell.lbl_SignedAgreementsAccountNo.text = "Opportunity #: \(oppNo ?? "N/A")" + " Acc # : \(accNo ?? "N/A")"
        //cell.lbl_SignedAgreementsEstValue.text = "EST : \(estValue ?? "N/A")"
        cell.lbl_SignedAgreementsTechName.text = "\(customerName)"
        cell.lbl_SignedAgreementsCompanyName.text = "Company: \(companyName ?? "N/A")"
        //cell.lbl_SignedAgreementsSalesRep.text = "Sales Representative : \(strEmployeeName)"
        //cell.lbl_SignedAgreementsOpportunityName.text = "Opportunity Name : \(opportunityName ?? "N/A")"
        cell.lbl_SignedAgreementsSignedDate.text = "Signed Date: \(signedDate ?? "N/A")"
        cell.lbl_SignedAgreementsScheduleDate.text = "Schedule For: \(scheduleDate ?? "N/A")"
        cell.lbl_SignedAgreementsStatus.text = "Opportunity Type: \(flowType ?? "")"// + " Sales"
        
        cell.btn_SignedAgreementsAddresslbl.setTitle(address, for: .normal)
        
        if arrEmail.count > 0 {
            
            cell.btn_SignedAgreementsEmaillbl.setTitle(arrEmail.componentsJoined(by: ","), for: .normal)
            cell.btn_SignedAgreementsEmaillbl.isHidden = false
            
        }else{
            
            cell.btn_SignedAgreementsEmaillbl.setTitle("", for: .normal)
            cell.btn_SignedAgreementsEmaillbl.isHidden = true
            
        }

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    
}
