//
//  FilterSignedAgreementiPadVC.swift
//  DPS
//
//  Created by Saavan Patidar on 07/06/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

// MARK: - -----------------------------------Date Picker Protocol-----------------------------------

extension FilterSignedAgreementiPadVC: DatePickerProtocol
{
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 10 {
            
            btnFromdate.setTitle(strDate, for: .normal)
            
        } else if tag == 11 {
            
            btnTodate.setTitle(strDate, for: .normal)
            
        }
        
    }
    
}
/*extension FilterSignedAgreementiPadVC: datePickerView {
    
    func setDateOnSelction(strDate: String, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 10 {
            
            btnFromdate.setTitle(strDate, for: .normal)
            
        } else if tag == 11 {
            
            btnTodate.setTitle(strDate, for: .normal)
            
        }
        
    }
    
}*/


class FilterSignedAgreementiPadVC: UIViewController {

    // MARK: - -----------------------------------Variables------------------------------------------
    
    
    // MARK: - -----------------------------------IBoutlets-------------------------------------------
    
    @IBOutlet weak var txtAccountNo: ACFloatingTextField!
    
    @IBOutlet weak var txtOpportunityNo: ACFloatingTextField!
    
    @IBOutlet weak var txtCompanyName: ACFloatingTextField!
    
    @IBOutlet weak var txtFN: ACFloatingTextField!
    
    @IBOutlet weak var txtMN: ACFloatingTextField!
    
    @IBOutlet weak var txtLN: ACFloatingTextField!
    
    @IBOutlet weak var btnFromdate: UIButton!
    
    @IBOutlet weak var btnTodate: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var viewBackGround: UIView!
    
    
    // MARK: - -----------------------------------View Life Cycle----------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Setting Buttons
        
        buttonRoundedThemeColor(sender: btnSave)
        buttonRoundedWithoutBackroundColor(sender: btnFromdate)
        buttonRoundedWithoutBackroundColor(sender: btnTodate)
        
        viewBackGround.layer.cornerRadius = 5.0
        viewBackGround.layer.borderColor = UIColor.themeColorBlack()?.cgColor
        viewBackGround.layer.borderWidth = 0.8
        viewBackGround.layer.shadowColor = UIColor.themeColorBlack()?.cgColor
        viewBackGround.layer.shadowOpacity = 0.3
        viewBackGround.layer.shadowRadius = 3.0
        viewBackGround.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        // Setting Default Dates
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        btnTodate.setTitle(result, for: .normal)

        let earlyDate = Calendar.current.date(
            byAdding: .day,
            value: -1,
            to: Date())
        
        let resultFrom = formatter.string(from: earlyDate!)

        btnFromdate.setTitle(resultFrom, for: .normal)
        
        
    }
    
    
    // MARK: - -----------------------------------Button Actions-----------------------------------
    @IBAction func action_Back(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func action_FromDate(_ sender: UIButton) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:(btnFromdate.titleLabel?.text)!)!
        //let toDate = dateFormatter.date(from:(btnTodate.titleLabel?.text)!)!
        
        self.gotoDatePickerView(sender: sender, strType: "Date",dateToSet: fromDate)
        
    }
    
    @IBAction func action_ToDate(_ sender: UIButton) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        //let fromDate = dateFormatter.date(from:(btnFromdate.titleLabel?.text)!)!
        let toDate = dateFormatter.date(from:(btnTodate.titleLabel?.text)!)!
        
        self.gotoDatePickerView(sender: sender, strType: "Date",dateToSet: toDate)
        
    }
    
    @IBAction func keyBoardDown(_ sender: Any) {
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:(btnFromdate.titleLabel?.text)!)!
        let toDate = dateFormatter.date(from:(btnTodate.titleLabel?.text)!)!
        
        if (toDate>fromDate) || (toDate == fromDate){
            
            self.view.endEditing(true)

            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            var strCompanyKey = String()
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
                
                strCompanyKey = "\(value)"
                
            }
            
            let dictParameters = NSMutableDictionary()
            dictParameters.setValue(btnFromdate.titleLabel?.text, forKey: "FromSigneddate")
            dictParameters.setValue(btnTodate.titleLabel?.text, forKey: "ToSignedDate")
            dictParameters.setValue(txtAccountNo.text, forKey: "AccountNo")
            dictParameters.setValue(txtOpportunityNo.text, forKey: "LeadNo")
            dictParameters.setValue(txtCompanyName.text, forKey: "CompanyName")
            dictParameters.setValue(txtFN.text, forKey: "FirstName")
            dictParameters.setValue(txtMN.text, forKey: "MiddleName")
            dictParameters.setValue(txtLN.text, forKey: "LastName")
            dictParameters.setValue(strCompanyKey, forKey: "CompanyKey")
            
            nsud.set(dictParameters, forKey: "FilterDictSignedAgreement")
            nsud.synchronize()

            self.dismiss(animated: false, completion: nil)
            
        } else {
            
            Global().displayAlertController(Alert, "From date should be less then To date", self)
            
        }
        
    }
    
    
    // MARK: - -----------------------------------Functions------------------------------------------
    
    func gotoDatePickerView(sender: UIButton, strType:String, dateToSet:Date )  {
        
        let vc: DateTimePicker = self.storyboard!.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self

        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
        
    }
    
}
