//
//  SalesAutomationServiceSummary.h
//  DPS
//
//  Created by Rakesh Jain on 19/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SalesAutomationServiceSummaryiPad : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate,UIActionSheetDelegate,UITextViewDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,
    *entityLeadAppliedDiscounts,*entityRenewalSerivceDetail;
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewLeads;
@property (strong, nonatomic) IBOutlet UIView *viewTop;
- (IBAction)actionOnBack:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tblRecord;



@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalAmountValue;
//Nilind 11 Jan Standard Service Subtotal View
@property (weak, nonatomic) IBOutlet UILabel *lblInitialPriceValue;

@property (weak, nonatomic) IBOutlet UILabel *lblDiscountValue;

@property (weak, nonatomic) IBOutlet UILabel *lblFinalInitialPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxAmountValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingAmountValue;

//End



- (IBAction)actionOnSave:(id)sender;
- (IBAction)actionOnCancel:(id)sender;



@property (weak, nonatomic) IBOutlet UILabel *lblNameAcount;
- (IBAction)actinoOnSendProposal:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSendProposal;
- (IBAction)actionOnNewCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNewCancel;

@property (weak, nonatomic) IBOutlet UIButton *btnNewSendProposal;
- (IBAction)actionOnNewSendProposal:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel3;
- (IBAction)actionOnCancel3:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveContinue;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

//11 Jan Nilind
@property (weak, nonatomic) IBOutlet UITableView *tblNonStandard;
@property (weak, nonatomic) IBOutlet UILabel *lblInitialPriceNonStan;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountNonStan;
@property (weak, nonatomic) IBOutlet UILabel *llblFinalPriceNonStan;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxAmountNonStan;

@property (weak, nonatomic) IBOutlet UILabel *lblBillingAmountNonStan;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Stan_table_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_NonStan_table_H;
- (IBAction)actionOnGlobalSync:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnGlobalSync;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewService;

//End

//View For Final Save

@property (weak, nonatomic) IBOutlet UIView *viewForFinalSave;

//View For After Image
@property (strong, nonatomic) IBOutlet UIView *viewForAfterImage;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewAfterImage;
- (IBAction)actionOnAfterImgView:(id)sender;
- (IBAction)actionAddAfterImages:(id)sender;
- (IBAction)actionOnCancelAfterImage:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnSaveFooter;


//View For Graph Image
@property (strong, nonatomic) IBOutlet UIView *viewForGraphImage;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewGraphImage;
- (IBAction)actionOnAddGraphImage:(id)sender;

- (IBAction)actionOnCancelBeforeImage:(id)sender;
- (IBAction)actionOnGraphImageFooter:(id)sender;
#pragma mark- New Bundle Change
@property (weak, nonatomic) IBOutlet UITableView *tblBundle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTblBundle_H;
- (IBAction)actionOnNotesHistory:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;
@property (weak, nonatomic) IBOutlet UITableView *tblCouponDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblcouponDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblOtherDiscount;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewStandardTotal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableDiscount_H;
- (IBAction)actionOnChemicalSensitivityList:(id)sender;


//Nilind 12 March

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_LblNonStandard_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewNonStantSubTotal_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_LblStandardService_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewStandardSubTotal_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_LblMaintService_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_LblBundleService_H;
@property (weak, nonatomic) IBOutlet UIButton *btnAddImageFooter;

@end
