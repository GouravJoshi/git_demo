//
//  FilterViewLead.swift
//  DPS
//
//  Created by Saavan Patidar on 28/02/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  //hjsdfhjsdh

import UIKit


// MARK: - -----------------------------------Date Picker Protocol-----------------------------------


extension FilterViewLead: DatePickerProtocol
{
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 1 {
            
            btnFromDate.setTitle(strDate, for: .normal)
            strFromDate = strDate
            
        } else if tag == 2 {
            
            btnToDate.setTitle(strDate, for: .normal)
            strToDate = strDate
            
        }
    }
}

/*protocol datePickerViewFilterLead : class{
    
    func setDateOnSelction(strDate : String ,tag : Int)
    
}

extension FilterViewLead: datePickerView {
    
    func setDateOnSelction(strDate: String, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 1 {
            
            btnFromDate.setTitle(strDate, for: .normal)
            strFromDate = strDate
            
        } else if tag == 2 {
            
            btnToDate.setTitle(strDate, for: .normal)
            strToDate = strDate
            
        }
    }
}
*/

class FilterViewLead: UIViewController {

    // MARK: - -----------------------------------Global Variables-----------------------------------
    
    var strFromDate = String()
    var strToDate = String()
    @objc var strType = String()

    // MARK: - -----------------------------------IBoutlets-----------------------------------

    @IBOutlet var btnFromDate: UIButton!
    @IBOutlet var btnToDate: UIButton!
    @IBOutlet var btnSave: UIButton!
    
    
    // MARK: - -----------------------------------View Life Cycle-----------------------------------

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // setting date from userDefaults
        
        let defs = UserDefaults.standard

        if strType == "WebLead" {
            
            btnFromDate.setTitle(defs.value(forKey: "fromdateWebLead") as? String, for: .normal)
            btnToDate.setTitle(defs.value(forKey: "todateWebLead") as? String, for: .normal)

        } else if  strType == "NonBillableiPad"{
            
            btnFromDate.setTitle(defs.value(forKey: "fromdateNonBillableiPad") as? String, for: .normal)
            btnToDate.setTitle(defs.value(forKey: "todateNonBillableiPad") as? String, for: .normal)
            
        } else {
            
            btnFromDate.setTitle(defs.value(forKey: "fromdateLead") as? String, for: .normal)
            btnToDate.setTitle(defs.value(forKey: "todateLead") as? String, for: .normal)
            
        }
        
        strFromDate = (btnFromDate.titleLabel?.text)!
        strToDate = (btnToDate.titleLabel?.text)!

        if (btnFromDate.titleLabel?.text) == nil {
            
            btnFromDate.setTitle("----Select----", for: .normal)
            
        }
        if (btnToDate.titleLabel?.text) == nil {
            
            btnToDate.setTitle("----Select----", for: .normal)
            
        }
        
        self.borderColor()
        
        // Do any additional setup after loading the view.
        
    }

    // MARK: - -----------------------------------Actions-----------------------------------

    @IBAction func action_FromDate(_ sender: UIButton) {
        
        self.gotoDatePickerView(sender: sender, strType: "Date")

        
    }
    
    @IBAction func action_ToDate(_ sender: UIButton) {
        
        self.gotoDatePickerView(sender: sender, strType: "Date")

        
    }
    
    @IBAction func action_Back(_ sender: Any) {
        
        self.dismiss(animated: false)
        
    }
    @IBAction func action_Save(_ sender: Any) {
        
        if btnFromDate.titleLabel?.text == "----Select----" {
            
            Global().displayAlertController(Alert, "Please select from date", self)
            
        } else if btnToDate.titleLabel?.text == "----Select----" {
            
            Global().displayAlertController(Alert, "Please select to date", self)
            
        } else {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
            let fromDate = dateFormatter.date(from:strFromDate)!
            let toDate = dateFormatter.date(from:strToDate)!
            
            
            if (toDate>fromDate) || (toDate == fromDate){
                
                if strType == "WebLead" {
                    
                    let userDef = UserDefaults.standard
                    userDef.set(strFromDate, forKey: "fromdateWebLead")
                    userDef.set(strToDate, forKey: "todateWebLead")
                    userDef.set(true, forKey: "isChangedFilterDate")
                    userDef.synchronize()
                    
                } else if strType == "NonBillableiPad" {
                    
                    let userDef = UserDefaults.standard
                    userDef.set(strFromDate, forKey: "fromdateNonBillableiPad")
                    userDef.set(strToDate, forKey: "todateNonBillableiPad")
                    userDef.set(true, forKey: "isChangedFilterDate")
                    userDef.synchronize()
                    
                } else {
                    
                    let userDef = UserDefaults.standard
                    userDef.set(strFromDate, forKey: "fromdateLead")
                    userDef.set(strToDate, forKey: "todateLead")
                    userDef.set(true, forKey: "isChangedFilterDate")
                    userDef.synchronize()
                    
                }
                
                self.dismiss(animated: false)
                
                
            } else {
                
                Global().displayAlertController(Alert, "From date should be less then To date", self)
                
            }
        }

    }
    
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self

        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    func borderColor() {
        
        btnFromDate.layer.cornerRadius = 5.0
        btnFromDate.layer.borderColor = UIColor.theme()?.cgColor
        btnFromDate.layer.borderWidth = 0.8
        btnFromDate.layer.shadowColor = UIColor.theme()?.cgColor
        btnFromDate.layer.shadowOpacity = 0.3
        btnFromDate.layer.shadowRadius = 3.0
        btnFromDate.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btnToDate.layer.cornerRadius = 5.0
        btnToDate.layer.borderColor = UIColor.theme()?.cgColor
        btnToDate.layer.borderWidth = 0.8
        btnToDate.layer.shadowColor = UIColor.theme()?.cgColor
        btnToDate.layer.shadowOpacity = 0.3
        btnToDate.layer.shadowRadius = 3.0
        btnToDate.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btnSave.layer.cornerRadius = 5.0
        btnSave.layer.borderColor = UIColor.theme()?.cgColor
        btnSave.layer.borderWidth = 0.8
        btnSave.layer.shadowColor = UIColor.theme()?.cgColor
        btnSave.layer.shadowOpacity = 0.3
        btnSave.layer.shadowRadius = 3.0
        btnSave.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
    }
    
}
