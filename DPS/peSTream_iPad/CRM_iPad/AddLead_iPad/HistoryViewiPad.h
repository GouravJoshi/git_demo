//
//  HistoryView.h
//  DPS
//
//  Created by Rakesh Jain on 29/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  Savan Patidar Test Commit.

#import <UIKit/UIKit.h>

@interface HistoryViewiPad : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scroll_SlideView;
@property (strong, nonatomic) IBOutlet UITableView *tblView_History;
- (IBAction)action_OutBox:(id)sender;
- (IBAction)action_Settings:(id)sender;
- (IBAction)action_refresh:(id)sender;
- (IBAction)action_Back:(id)sender;

@end
