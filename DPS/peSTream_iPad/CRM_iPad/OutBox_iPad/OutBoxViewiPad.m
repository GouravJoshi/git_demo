//
//  OutBoxView.m
//  DPS
//
//  Created by Rakesh Jain on 21/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "OutBoxViewiPad.h"
#import "AllImportsViewController.h"

#import "OutBoxTableViewCell.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "HistoryView.h"
#import "DPS-Swift.h"

//OutBoxCell
@interface OutBoxViewiPad ()
{
    Global *global;
    NSString *strServiceUrlMain;
}

@end

@implementation OutBoxViewiPad

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FetchFromCoreDataOutBox) name:@"OutBoxRefresh" object:nil];

    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];

    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

    _tblViewOutBox.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    // Do any additional setup after loading the view.
    
    [self FetchFromCoreDataOutBox];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
    }
    else{
        [self performSelectorInBackground:@selector(sendLeadToServer) withObject:nil];
        if (arrAllObj.count>0) {
            
             [self HudView];
            
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)HudView{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"Sending Leads to server.";
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        if([UIScreen mainScreen].bounds.size.height==1024)
        {
            hud.label.font=[UIFont systemFontOfSize:28];
            hud.margin = 10.f;//10.f
            hud.yOffset = 350.f;//150.f
        }
        else if ([UIScreen mainScreen].bounds.size.height==1366)
        {
            hud.label.font=[UIFont systemFontOfSize:32];
            hud.margin = 10.f;//10.f
            hud.yOffset = 500.f;//150.f
        }
        
        
    }
    else
    {
        if([UIScreen mainScreen].bounds.size.height==736)
        {
            hud.margin = 10.f;
            hud.yOffset = 250.f;
        }
        else if ([UIScreen mainScreen].bounds.size.height==667)
        {
            hud.margin = 10.f;
            hud.yOffset = 230.f;
        }
        else if ([UIScreen mainScreen].bounds.size.height==568)
        {
            hud.margin = 10.f;
            hud.yOffset = 200.f;
        }
        else
        {
            hud.margin = 10.f;
            hud.yOffset = 150.f;
        }
        
    }
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:2];
}
//============================================================================
//============================================================================
#pragma mark- Button Action METHODS
//============================================================================
//============================================================================

- (IBAction)actionBack:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    BOOL yesFromHistory=[defs boolForKey:@"fromHistoryTOutBox"];
    if (yesFromHistory) {
        
        [defs setBool:NO forKey:@"fromHistoryTOutBox"];
        [defs synchronize];
        
        for (UIViewController *controller in self.navigationController.viewControllers)
        {
            if ([controller isKindOfClass:[HistoryView class]])
            {
                [self.navigationController popToViewController:controller animated:NO];
                break;
            }
        }

    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName: UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"CRM_New_iPad" : @"DashBoard" bundle: nil];
        DashBoardNew_iPhoneVC
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardNew_iPhoneVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}

//============================================================================
//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    NSArray *sections = [self.fetchedResultsController sections];
    //    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    //    // totalCount=[sectionInfo numberOfObjects];
    //    return [sectionInfo numberOfObjects];
    return arrAllObj.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OutBoxTableViewCell *cell = (OutBoxTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"OutBoxCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
- (void)configureCell:(OutBoxTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    if (!(arrAllObj.count==0)) {
        
    // Fetch Record
    matches =[arrAllObj objectAtIndex:indexPath.row];
    
    
    NSString *alreadyAvailableLeadService=[matches valueForKey:@"finalJson"];
    
    NSData* data = [alreadyAvailableLeadService dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *e;
    NSDictionary *dictOffinalJson = [NSJSONSerialization JSONObjectWithData:data options:nil error:&e];
    
    //============================================================================
    //============================================================================

    NSDictionary *dictDataJson = dictOffinalJson;
    
    NSDictionary *dictDatacustomerExtDcDict = [dictDataJson valueForKey:@"Account"];
    
    cell.lblAccountNo.text=[NSString stringWithFormat:@"A/C :%@",[dictDatacustomerExtDcDict valueForKey:@"AccountNo"]];
    if ([[dictDatacustomerExtDcDict valueForKey:@"AccountNo"] isEqualToString:@""]) {
        NSDictionary *dictDatacustomerExtDcDict = [dictDataJson valueForKey:@"AccountCompany"];
        cell.lblAccountNo.text=[NSString stringWithFormat:@"A/C :%@",[dictDatacustomerExtDcDict valueForKey:@"Name"]];
    }
    //============================================================================
    //============================================================================

    cell.lblDatenTime.text=[dictDataJson valueForKey:@"createdDateTime"];
    

    //============================================================================
    //============================================================================
    NSArray *arrOfleadServiceExtDcsArray = [dictDataJson valueForKey:@"LeadServices"];

    NSString *strCountOfLead=[NSString stringWithFormat:@"%lu",(unsigned long)arrOfleadServiceExtDcsArray.count];

    cell.lblNoService.text=[NSString stringWithFormat:@"No. of Services :%@",strCountOfLead];

    //============================================================================
    //============================================================================
    }
  //  cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
}
//============================================================================
//============================================================================
#pragma mark- Upload Final Json METHOD
//============================================================================
//============================================================================
-(void)sendLeadToServer{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityOutBox=[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context];
    
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityOutBox];
    
//    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
//    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
//    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
//    
//    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
//    [requestNew setPredicate:predicate];

    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"datenTime" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerOutBox = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerOutBox setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerOutBox performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerOutBox fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
    for (int r=0; r<arrAllObj.count; r++) {
        
        matches =[arrAllObj objectAtIndex:r];
        
        NSString *alreadyAvailableLeadService=[matches valueForKey:@"finalJson"];
        
        NSData* data = [alreadyAvailableLeadService dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *e;
        NSDictionary *dictOffinalJson = [NSJSONSerialization JSONObjectWithData:data options:nil error:&e];
        
        //============================================================================
        //============================================================================
        NSArray *arrOfleadServiceExtDcsArray = [dictOffinalJson valueForKey:@"LeadServices"];
        
        NSDictionary *dictAccountNo = [dictOffinalJson valueForKey:@"Account"];
        
        NSString *strAccountNo=[dictAccountNo valueForKey:@"AccountNo"];
        
        BOOL isnetReachable=[global isNetReachable];
        
        if (isnetReachable) {
            
          //  [self postAddressInBackground:strAccountNo];
            
        }
        
        //To Update Address if there

        for (int j=0; j<arrOfleadServiceExtDcsArray.count; j++) {
            NSDictionary *dictDataMedia=[arrOfleadServiceExtDcsArray objectAtIndex:j];
             NSArray *arrOfMediaTosend=[dictDataMedia valueForKey:@"LeadMediaExtDcs"];
            for (int k=0; k<arrOfMediaTosend.count; k++) {
                NSDictionary *dictDataMediaToSend=[arrOfMediaTosend objectAtIndex:k];
                NSString *strType=[dictDataMediaToSend valueForKey:@"MediaTypeSysName"];
                NSString *strMediaName=[dictDataMediaToSend valueForKey:@"Name"];
                if ([strType isEqualToString:@"Image"]) {
                    [self uploadImage:strMediaName];
                } else {
                    [self uploadAudio:strMediaName];
                }
            }
            //MediaTypeSysName  MediaSourceSysName
        }
        
        //============================================================================
        //============================================================================

        [self sendingLeadToServer:matches :[NSString stringWithFormat:@"%d",r]];
    }
    }
}

//============================================================================
//============================================================================
#pragma mark- Post Address InBackground METHODS
//============================================================================
//============================================================================

-(void)postAddressInBackground :(NSString*)strAccountNo{
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Adress..."];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainCRM =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
    NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMainCRM,UrlGetPostAddressesByAccountNo,strAccountNo,UrlGetActivityListcompanyKey,strCompanyKey];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global responseOnCustomerAddress:strUrl  :strAccountNo :@"BackGround" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     if ([response isKindOfClass:[NSString class]]) {
                         
                         
                     } else {
                         
                         // [DejalBezelActivityView removeView];
                         
                     }
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
}

-(void)sendingLeadToServer :(NSManagedObject*)managedObj :(NSString *)strIndex{
    
    NSString *strFinalJson=[managedObj valueForKey:@"finalJson"];
    
    NSString *strdateTime=[managedObj valueForKey:@"datenTime"];
    
    NSData *requestData = [strFinalJson dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlSendLeadToServerNew];
    NSDictionary *dictTemp;
    
    NSString *strTypee=@"leadFromOutbox";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :strdateTime :strTypee withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     NSString *strException;
                     @try {
                         strException=[response valueForKey:@"ExceptionMessage"];
                     } @catch (NSException *exception) {
                         
                     } @finally {
                         
                     }
                     if (strException.length==0) {

                       //  UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Your Lead has been sent to server." message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //[alert show];
                       //  [self deletefromOutbox:0];

                     } else {
                         
                       //  UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to send lead to server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                       //  [alert show];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}
//============================================================================
//============================================================================
#pragma mark- FetchFromCoreDataOutBox METHODS
//============================================================================
//============================================================================

-(void)FetchFromCoreDataOutBox{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityOutBox=[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context];
    
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityOutBox];
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userEmailId = %@",[dictData valueForKey:@"Email"]];
    //[request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"datenTime" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerOutBox = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerOutBox setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerOutBox performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerOutBox fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        matches = arrAllObj[0];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
    NSString *strCountOfLead=[NSString stringWithFormat:@"%lu",(unsigned long)arrAllObj.count];
    _lbl_CountOfLead.text=[NSString stringWithFormat:@"OUTBOX :%@",strCountOfLead];
    [_tblViewOutBox reloadData];
}


//============================================================================
//============================================================================
#pragma mark- Upload Image METHOD
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];

    if (imagee == nil){
        
    }else{
        
    }
    
    NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlImageUpload];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Image Sent");
    }
    NSLog(@"Image Sent");
}


//============================================================================
//============================================================================
#pragma mark- Upload Audio METHOD
//============================================================================
//============================================================================


-(void)uploadAudio :(NSString*)strAudioName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    NSData *audioData;
    
    audioData = [NSData dataWithContentsOfFile:path];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlImageUpload];
    
    // setting up the request object now
    NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
    [request1 setURL:[NSURL URLWithString:urlString]];
    [request1 setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strAudioName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:audioData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request1 setHTTPBody:body];
    
    // now lets make the connection to the web
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Audio Sent");
    }
    NSLog(@"Audio Sent");
}

//============================================================================
//============================================================================
#pragma mark- Delete METHOD
//============================================================================
//============================================================================

-(void)deletefromOutbox :(int)IndexToDelete{
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context]];
    
//    NSPredicate *p=[NSPredicate predicateWithFormat:@"datenTime=%@",[RemoveIdv objectAtIndex:i]];
//    [allData setPredicate:p];

    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    if (Data.count==0) {
        
    }
    else {
        
        NSManagedObject * data = [Data objectAtIndex:IndexToDelete];
        [context deleteObject:data];
        NSError *saveError = nil;
        [context save:&saveError];
        
    }
    
    [self FetchFromCoreDataOutBox];
}

-(void)deleteFromOutBoxCoreData{
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context]];
    //    NSPredicate *p=[NSPredicate predicateWithFormat:@"datenTime=%@",[RemoveIdv objectAtIndex:i]];
    //    [allData setPredicate:p];
    NSError * error = nil;
    NSArray *Data = [context executeFetchRequest:allData error:&error];
    for (NSManagedObject * car in Data) {
        [context  deleteObject:car];
        NSError *saveError = nil;
        [context save:&saveError];
    }
}

@end
