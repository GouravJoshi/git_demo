//
//  WebLeads+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 16/10/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface WebLeads : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "WebLeads+CoreDataProperties.h"
