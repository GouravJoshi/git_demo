//
//  WebLeads+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 16/10/18.
//
//

#import "WebLeads+CoreDataProperties.h"

@implementation WebLeads (CoreDataProperties)

+ (NSFetchRequest<WebLeads *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"WebLeads"];
}

@dynamic arrOfLeads;
@dynamic companyKey;
@dynamic empId;
@dynamic userName;

@end
