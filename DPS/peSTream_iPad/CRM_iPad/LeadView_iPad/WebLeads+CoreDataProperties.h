//
//  WebLeads+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 16/10/18.
//
//

#import "WebLeads+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WebLeads (CoreDataProperties)

+ (NSFetchRequest<WebLeads *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *arrOfLeads;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *empId;
@property (nullable, nonatomic, copy) NSString *userName;

@end

NS_ASSUME_NONNULL_END
