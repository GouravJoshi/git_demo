//
//  RecordAudioView.h
//  DPS
//
//  Created by Rakesh Jain on 23/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface RecordAudioViewiPad : UIViewController<AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    NSTimer *timer;
    int totalsecond;
    NSTimer *twoMinTimer;
    double ticks;
    BOOL ispaused;
    NSDate *pauseStart, *previousFireDate;
}
- (IBAction)actionBack:(id)sender;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewMiceophone;
@property (strong, nonatomic) IBOutlet UIButton *Btn_Record;
@property (strong, nonatomic) IBOutlet UIButton *Btn_Stop;
- (IBAction)action_Record:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Timer;
- (IBAction)action_Stop:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Title;
@property (strong, nonatomic) NSString *strFromWhere;

@end
