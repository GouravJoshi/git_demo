//
//  AppointmentView.h
//  DPS
//
//  Created by Rakesh Jain on 20/06/16.
//  Copyright © 2016 Saavan. All rights reserved.

//  Saavan Patidar 2021
//  Commit 2021 New 

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "LeadDetail.h"
#import "PaymentInfo.h"
#import "LeadPreference.h"
#import "SoldServiceNonStandardDetail.h"
#import "SoldServiceStandardDetail.h"
#import "ImageDetail.h"
#import "EmailDetail.h"
#import "DocumentsDetail.h"
#import "ServiceFollowUpDcs+CoreDataClass.h"
#import "ServiceFollowUpDcs+CoreDataProperties.h"
#import "ProposalFollowUpDcs+CoreDataClass.h"
#import "ProposalFollowUpDcs+CoreDataProperties.h"

#import "ImageDetailsTermite+CoreDataProperties.h"
#import "ImageDetailsTermite+CoreDataClass.h"
#import "FloridaTermiteServiceDetail+CoreDataClass.h"
#import "FloridaTermiteServiceDetail+CoreDataProperties.h"

@interface AppointmentViewiPad : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entitytotalWorkOrders;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSFetchRequest *requestNewService;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entitySalesInfo;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,
    *entityDocumentsDetail,
    *entitySalesModifyDate,
    *entityServiceFollowUp,
    *entityProposalFollowUp,
    *entityLeadAgreementChecklistSetups,*entityElectronicAuthorizedForm,*entityLeadAppliedDiscounts,*entityContactNumberDetail,*entityRenewalServiceDetail;
    //Clark Pest
    NSEntityDescription *entityLeadCommercialScopeExtDc,*entityLeadCommercialTargetExtDc,*entityLeadCommercialMaintInfoExtDc,*entityLeadCommercialInitialInfoExtDc,*entityLeadCommercialDiscountExtDc,*entityLeadCommercialDetailExtDc,*entityLeadCommercialTermsExtDc,*entityLeadMarketingContentExtDc;
    
    NSEntityDescription *entityWorkOderDetailServiceAuto,
    *entityImageDetailServiceAuto,
    *entityEmailDetailServiceAuto,
    *entityPaymentInfoServiceAuto,
    *entityChemicalListDetailServiceAuto,
    *entityModifyDateServiceAuto,
    *entityCompanyDetailServiceAuto,
    *entityWOProductServiceAuto,
    *entityWOEquipmentServiceAuto,
    *entityMechanicalEquipment,
    *entityMechanicalServiceAddressPOCDetailDcs,
    *entityMechanicalBillingAddressPOCDetailDcs;
    
    
    NSEntityDescription *entityModifyDate,*entityCurrentService;
    NSFetchRequest *requestModifyDate,*requestSubWOCompleteTimeExtSerDcs,*requestSubWoEmployeeWorkingTimeExtSerDcs;
    NSSortDescriptor *sortDescriptorModifyDate,*sortDescriptorSubWOCompleteTimeExtSerDcs,*sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs;
    NSArray *sortDescriptorsModifyDate,*sortDescriptorsSubWOCompleteTimeExtSerDcs,*sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs;
    NSManagedObject *matchesModifyDate,*matchesSubWOCompleteTimeExtSerDcs,*matchesSubWoEmployeeWorkingTimeExtSerDcs;
    NSArray *arrAllObjModifyDate,*arrAllObjSubWOCompleteTimeExtSerDcs,*arrAllObjSubWoEmployeeWorkingTimeExtSerDcs;
    
    NSEntityDescription *entitySalesDynamic;
    NSFetchRequest *requestNewSalesDynamic;
    NSSortDescriptor *sortDescriptorSalesDynamic;
    NSArray *sortDescriptorsSalesDynamic;
    NSManagedObject *matchesSalesDynamic;
    NSArray *arrAllObjSalesDynamic;
    
    
    NSEntityDescription *entityServiceModifyDate;
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
    NSEntityDescription *entityServiceDynamic;
    NSFetchRequest *requestNewServiceDynamic;
    NSSortDescriptor *sortDescriptorServiceDynamic;
    NSArray *sortDescriptorsServiceDynamic;
    NSManagedObject *matchesServiceDynamic;
    NSArray *arrAllObjServiceDynamic;
    
    NSEntityDescription *entityMechanicalSubWorkOrder;
    NSFetchRequest *requestMechanicalSubWorkOrder;
    NSSortDescriptor *sortDescriptorMechanicalSubWorkOrder;
    NSArray *sortDescriptorsMechanicalSubWorkOrder;
    NSManagedObject *matchesMechanicalSubWorkOrder;
    NSArray *arrAllObjMechanicalSubWorkOrder;
    
    NSEntityDescription *entityMechanicalSubWorkOrderIssues,*entityMechanicalSubWorkOrderHelper,*entityMechanicalSubWorkOrderNotes,*entityMechanicalSubWorkOrderActualHours,*entityMechanicalSubWorkOrderIssuesRepairs,*entityMechanicalSubWorkOrderIssuesRepairsParts,*entityMechanicalSubWorkOrderIssuesRepairsLabour,*entitySubWOCompleteTimeExtSerDcs,*entitySubWoEmployeeWorkingTimeExtSerDcs,*entityMechanicalWoOtherDocExtSerDcs;
    
    NSEntityDescription *entityWorkOrderMechanical;
    NSFetchRequest *requestNewWorkOrderMechanical;
    NSSortDescriptor *sortDescriptorWorkOrderMechanical;
    NSArray *sortDescriptorsWorkOrderMechanical;
    NSManagedObject *matchesWorkOrderMechanical;
    NSArray *arrAllObjWorkOrderMechanical;
    
    NSEntityDescription *entityWorkOrderDocuments;
    NSFetchRequest *requestNewWorkOrderDocuments;
    NSSortDescriptor *sortDescriptorWorkOrderDocuments;
    NSArray *sortDescriptorsWorkOrderDocuments;
    NSManagedObject *matchesWorkOrderDocuments;
    NSArray *arrAllObjWorkOrderDocuments;
    
    
    
    //For Termite Flow
    //Texas
    NSEntityDescription *entityTermiteTexas,*entityImageDetailsTermite;
    NSFetchRequest *requestTermiteTexas;
    NSSortDescriptor *sortDescriptorTermiteTexas;
    NSArray *sortDescriptorsTermiteTexas;
    NSManagedObject *matchesTermiteTexas;
    NSArray *arrAllObjTermiteTexas;
    
    
    //Florida
    NSEntityDescription *entityTermiteFlorida;
    NSFetchRequest *requestTermiteFlorida;
    NSSortDescriptor *sortDescriptorTermiteFlorida;
    NSArray *sortDescriptorsTermiteFlorida;
    NSManagedObject *matchesTermiteFlorida,*matchesWorkOrderFlorida;
    NSArray *arrAllObjTermiteFlorida;
    
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    
    //Termite Image
    NSSortDescriptor *sortDescriptorImageDetail;
    NSFetchRequest *requestImageDetail;
    NSArray *sortDescriptorsImageDetail;
    
    //Akshay
    
    NSEntityDescription *entityWorkOrderAppliedDiscountExtSerDcs,*entityMechanicalAccountDiscountExtSerDcs;
    
    // Chnages for mechanical status
    NSEntityDescription *entityMechanicalSubWorkOrderActualHrs,
    *entityProblemImageDetail;
    NSFetchRequest *requestSubWorkOrderActualHrs;
    NSSortDescriptor *sortDescriptorSubWorkOrderActualHrs;
    NSArray *sortDescriptorsSubWorkOrderActualHrs;
    
    
}
- (IBAction)action_Refresh:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)hideKeyBoard:(id)sender;
- (IBAction)actionAllAppointments:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewAppointment;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTotalWorkOrders;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceAutomation;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsMechanicalSubWorkOrder;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsWorkOrderDocuments;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;

- (IBAction)action_Search:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UIButton *btn_Refreshh;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarr;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceDynamic;

- (IBAction)action_Todayy:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblLineAll;
@property (strong, nonatomic) IBOutlet UILabel *lblLineToday;
@property (strong, nonatomic) IBOutlet UITextField *txt_FirstName;
@property (strong, nonatomic) IBOutlet UITextField *txt_LastName;
@property (strong, nonatomic) IBOutlet UITextField *txt_AccountNo;
@property (strong, nonatomic) IBOutlet UITextField *Txt_Leadd;
@property (strong, nonatomic) IBOutlet UITextField *txt_WorkOrderNo;
@property (strong, nonatomic) IBOutlet UIButton *btn_StatusFilter;
- (IBAction)action_StatusFlter:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_FromDateFilter;
- (IBAction)action_FromDateFilter:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_ToDateFilter;
- (IBAction)action_ToDateFilter:(id)sender;
- (IBAction)action_ApplyFilter:(id)sender;
- (IBAction)action_CancelFilter:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *view_FilterCriteria;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView_Filter;
-(void)SalesAutomationFetchAfterSyncingData;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetailsMechanical;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalSubWorkOrder;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWOCompleteTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs;

- (IBAction)action_ClearAllFilter:(id)sender;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderActualHrs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWDO;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWDOPricing;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerProblemImageDetail;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerNpmaTermite;

@end

