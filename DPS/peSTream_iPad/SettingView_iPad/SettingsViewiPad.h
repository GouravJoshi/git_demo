//
//  SettingsView.h
//  DPS
//asdfasdf
//  Created by Rakesh Jain on 20/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewiPad : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSIndexPath* checkedIndexPath;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scroolView;
- (IBAction)action_EditCustomMsg:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEditCustomMsg;
- (IBAction)action_Refersh:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)action_Status:(id)sender;
- (IBAction)action_HistoricalDays:(id)sender;
- (IBAction)action_CustomMessage:(id)sender;
- (IBAction)action_Ok:(id)sender;
- (IBAction)action_CancelTblViewSettings:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewSettings;
@property (strong, nonatomic) IBOutlet UIView *view_Settings;
@property (strong, nonatomic) IBOutlet UITextView *txtViewCustomMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblHistoricalDayss;
@property (strong, nonatomic) IBOutlet UILabel *lblSelectedStatus;
@property (nonatomic, retain) NSIndexPath* checkedIndexPath;
@property (strong, nonatomic) IBOutlet UIView *viewSavenCancel;
@property (strong, nonatomic) IBOutlet UISwitch *imageCaptionSwitchSales;
@property (strong, nonatomic) IBOutlet UISwitch *imageCaptionSwitch;
- (IBAction)valueChangeImageCaptionSwitch:(id)sender;
- (IBAction)valueChangeImageCaptionSwitchSales:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_StatusOrMsg;

// new change for Appointment list settings

@property (strong, nonatomic) IBOutlet UIButton *btnSortByScheduleDate;
@property (strong, nonatomic) IBOutlet UIButton *btnSortByModifiedDate;
- (IBAction)action_SortByScheduleDate:(id)sender;
- (IBAction)action_SortByModifiedDate:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnAscendingScheduleDate;
@property (strong, nonatomic) IBOutlet UIButton *btnDescendingScheduleDate;
- (IBAction)action_AscendingScheduleDate:(id)sender;
- (IBAction)action_DescendingScheduleDate:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnAscendingModifiedDate;
@property (strong, nonatomic) IBOutlet UIButton *btnDescendingModifiedDate;
- (IBAction)action_AscendingModifiedDate:(id)sender;
- (IBAction)action_DescendingModifiedDate:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnAllAppointments;
- (IBAction)action_AllAppointments:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectStatusAppointments;
- (IBAction)acttion_StatusAppointments:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblSelectStatus;

@property (strong, nonatomic) IBOutlet UIButton *btnGraphDebug;
- (IBAction)action_GraphDebug:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDashboardWidgets;
- (IBAction)actionOnDashoardWidgets:(id)sender;
- (IBAction)action_WdoGraphDrawHistoryDays:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblWdoGraphDrawHistoryDays;

//Ruchika Added CRM Opportunity
@property (strong, nonatomic) IBOutlet UIButton *btnOpportunityAscending;
@property (strong, nonatomic) IBOutlet UIButton *btnOpportunityDescending;
- (IBAction)action_Opportunity_Ascending:(id)sender;
- (IBAction)action_Opportunity_Descending:(id)sender;
@end
