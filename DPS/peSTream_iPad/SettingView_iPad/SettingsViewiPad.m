//
//  SettingsView.m
//  DPS
//
//  Created by Rakesh Jain on 20/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "SettingsViewiPad.h"
#import "AllImportsViewController.h"

#import "SettingsTableViewCell.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"

#import "HistoryView.h"
#import "MBProgressHUD.h"
#import "DPS-Swift.h"

//SettingsCells
@interface SettingsViewiPad ()
{
    BOOL isStatusButton;
    NSMutableArray *arrOfStatus;
    NSArray *arrOfCustomMessage;
    NSMutableArray *arraySelecteValues;
    NSString *checkedCustomMessage;
    Global *global;
    NSString *strCompanyKeyy;
    NSString *strEmpNo,*strEmpName,*strCompanyKey,*strCompanyId,*strServiceUrlMain,*strSalesProcessCompanyId,*strServiceUrlMainServiceAutomation,*strGGQCompanyKey,*strServiceUrlMainSalesAutomation;

    //Changes For Appointments settings
    NSMutableArray *arrDataTblView,*arrOfLeadStatusSysNameSelected;
    UIView *viewForDate;
    UIView *viewBackGround;
    UITableView *tblData;
    NSArray *arrDashboardWidget;
    NSMutableArray *arrDashboardWidgetSelected;
    __weak IBOutlet UISwitch *btnAutoPropertyInfo;
    __weak IBOutlet UISwitch *btnSalesDynamicFormmLoad;

}
@end

@implementation SettingsViewiPad

- (void)viewDidLoad {
    [super viewDidLoad];
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    arrDashboardWidgetSelected = [[NSMutableArray alloc]init];
    //============================================================================
    //============================================================================

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    BOOL yesImageCaption=[defsLogindDetail boolForKey:@"imageCaptionSetting"];
    
    if (yesImageCaption) {
        
        [_imageCaptionSwitch setOn:YES];
        
    } else {
        
        [_imageCaptionSwitch setOn:NO];
        
    }
    
    
    BOOL yesImageCaptionSales=[defsLogindDetail boolForKey:@"imageCaptionSettingSales"];
    
    if (yesImageCaptionSales) {
        
        [_imageCaptionSwitchSales setOn:YES];
        
    } else {
        
        [_imageCaptionSwitchSales setOn:NO];
        
    }

    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpNo          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strCompanyId      =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.CompanyId"];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    strGGQCompanyKey=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.GGQCompanyKey"]];

    strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];

    strServiceUrlMainSalesAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    
    arrDashboardWidget = [dictLoginData valueForKeyPath:@"DashboardWidgetMasters"];
    [self setDashboardWidgets];
    //============================================================================
    //============================================================================
    
    // Uiview Boundary color
    [_view_Settings.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [_view_Settings.layer setBorderWidth: 2.0];

    
    _txtViewCustomMessage.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewCustomMessage.layer.borderWidth=1.0;
    _txtViewCustomMessage.layer.cornerRadius=5.0;
    
    [_btnEditCustomMsg setTitle:@"Edit" forState:UIControlStateNormal];
    [_txtViewCustomMessage setEditable:NO];

    //============================================================================
    //============================================================================
    //FOR Custom Message
    NSUserDefaults *prefsCustomMsg = [NSUserDefaults standardUserDefaults];
    NSString *strCustomMsg = [prefsCustomMsg stringForKey:@"CustomMessage"];
    if (strCustomMsg.length<1) {
        NSUserDefaults *prefsCustomMsg = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        [prefsCustomMsg setObject:@"" forKey:@"CustomMessage"];
        [prefsCustomMsg synchronize];
    }else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strCustomMsg = [prefs stringForKey:@"CustomMessage"];
        _txtViewCustomMessage.text=strCustomMsg;
    }
    //============================================================================
    //============================================================================

    //FOR Historical Days
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *days = [prefs stringForKey:@"HistoryDays"];
    if (days.length<1) {//if number of days not available set 100 as default
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        [prefs setObject:@"100" forKey:@"HistoryDays"];
        [prefs synchronize];
    }else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *days = [prefs stringForKey:@"HistoryDays"];
        _lblHistoricalDayss.text=days;
    }
    
    
    //
    
    //FOR Wdo Graph Draw History Days
    NSUserDefaults *prefsWdo = [NSUserDefaults standardUserDefaults];
    NSString *daysWdo = [prefsWdo stringForKey:@"WdoGraphDrawHistoryRecordsCount"];
    if (daysWdo.length<1) {//if number of days not available set 100 as default
        NSUserDefaults *prefsWdo = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        [prefsWdo setObject:@"10" forKey:@"WdoGraphDrawHistoryRecordsCount"];
        [prefsWdo synchronize];
        _lblWdoGraphDrawHistoryDays.text=@"10 rows";

    }else
    {
        NSUserDefaults *prefsWdo = [NSUserDefaults standardUserDefaults];
        NSString *daysWdo = [prefsWdo stringForKey:@"WdoGraphDrawHistoryRecordsCount"];
        _lblWdoGraphDrawHistoryDays.text=[NSString stringWithFormat:@"%@ rows",daysWdo];
    }
    
    [global defaultSelectedStatus];
    
    //============================================================================
    //============================================================================
    //FOR Selected Values
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *arrayvalus = [userDefaults objectForKey:@"SelectedStatusValues"];
    NSString *joinedComponents = [arrayvalus componentsJoinedByString:@","];
    if (joinedComponents.length==0) {
        _lblSelectedStatus.text=[NSString stringWithFormat:@"Choose Status"];
    } else {
        _lblSelectedStatus.text=[NSString stringWithFormat:@"Selected-(%@)",joinedComponents];
    }
    
    //============================================================================
    //============================================================================

    //============================================================================
    //============================================================================
    isStatusButton=YES;
    [_view_Settings setHidden:YES];
    //[_view_Settings removeFromSuperview];
    _tblViewSettings.rowHeight=UITableViewAutomaticDimension;
    _tblViewSettings.estimatedRowHeight=200;
    self.tblViewSettings.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    //============================================================================
    //============================================================================
    
    
    //============================================================================
    //============================================================================
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    NSArray *arrOfLeadStatusMasters=[defsLead valueForKey:@"LeadStatusMasters"];
    
    arrOfStatus =[[NSMutableArray alloc]init];
    
    for (int k=0;k<arrOfLeadStatusMasters.count; k++) {
        NSDictionary *dictData=arrOfLeadStatusMasters[k];
        [arrOfStatus addObject:[dictData valueForKey:@"StatusName"]];
    }

//    arrOfCustomMessage=@[@"First Custom Message",
//                         @"First Custom Message 2",
//                         @"First Custom Message 3",
//                         @"First Custom Message 4",
//                         @"First Custom Message 5 Large Test Testing For Autolayout Saav"];
    arraySelecteValues= [[NSMutableArray alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictData=[defs valueForKey:@"CustomMessageData"];
        arrOfCustomMessage=dictData;
    }else
    {
        if (strGGQCompanyKey.length==0) {
            
        } else {
            [self fetchCustomMessage];
        }
    }
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapSettings:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [_scroolView addGestureRecognizer:singleTapGestureRecognizer];
    
    //============================================================================
    //============================================================================

    
    //Changes For Appointments settings
    
    arrOfLeadStatusSysNameSelected = [[NSMutableArray alloc]init];
    
    [_btnSelectStatusAppointments.layer setCornerRadius:5.0f];
    [_btnSelectStatusAppointments.layer setBorderColor:[UIColor themeColor].CGColor];
    [_btnSelectStatusAppointments.layer setBorderWidth:0.8f];
    [_btnSelectStatusAppointments.layer setShadowColor:[UIColor themeColor].CGColor];
    [_btnSelectStatusAppointments.layer setShadowOpacity:0.3];
    [_btnSelectStatusAppointments.layer setShadowRadius:3.0];
    [_btnSelectStatusAppointments.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
    
    BOOL isAllAppoitments = [defsApp boolForKey:@"AllAppointments"];
    
    if (!isAllAppoitments) {
        
        [_lblSelectStatus setHidden:NO];
        [_btnSelectStatusAppointments setHidden:NO];
        [_btnAllAppointments setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
        NSArray *tempArr = [defsApp objectForKey:@"AppointmentStatus"];
        
        arrOfLeadStatusSysNameSelected = nil;
        
        arrOfLeadStatusSysNameSelected = [[NSMutableArray alloc]init];
        
        [arrOfLeadStatusSysNameSelected addObjectsFromArray:tempArr];
        
        if (tempArr.count>0) {
            
            arrDataTblView=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
            
            NSArray *arrOfLeadStatusMasters=[defsLead valueForKey:@"LeadStatusMasters"];
            
            for (int k=0;k<arrOfLeadStatusMasters.count; k++) {
                NSDictionary *dictData=arrOfLeadStatusMasters[k];
                [arrDataTblView addObject:dictData];
            }
            
            NSArray *objects1 = [NSArray arrayWithObjects:
                                 @"InComplete",
                                 @"InComplete",nil];
            
            NSArray *keys1 = [NSArray arrayWithObjects:
                              @"SysName",
                              @"StatusName",nil];
            
            NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects1 forKeys:keys1];
            
            [arrDataTblView addObject:dict_ToSend];
            
            NSArray *objectsReset = [NSArray arrayWithObjects:
                                     @"Reset",
                                     @"Reset",nil];
            
            NSArray *keysReset = [NSArray arrayWithObjects:
                                  @"SysName",
                                  @"StatusName",nil];
            
            NSDictionary *dict_ToSendReset = [[NSDictionary alloc] initWithObjects:objectsReset forKeys:keysReset];
            
            [arrDataTblView addObject:dict_ToSendReset];
            
            NSArray *objectsCompletePending = [NSArray arrayWithObjects:
                                               @"CompletePending",
                                               @"CompletePending",nil];
            
            NSArray *keysCompletePending = [NSArray arrayWithObjects:
                                            @"SysName",
                                            @"StatusName",nil];
            
            NSDictionary *dict_ToSendCompletePending = [[NSDictionary alloc] initWithObjects:objectsCompletePending forKeys:keysCompletePending];
            
            [arrDataTblView addObject:dict_ToSendCompletePending];
            
            if(arrOfLeadStatusSysNameSelected.count>0)
            {
                NSString *str = @"";
                for (int k1=0; k1<arrOfLeadStatusSysNameSelected.count; k1++) {
                    
                    NSString *strLocalSysNameSelected = arrOfLeadStatusSysNameSelected[k1];
                    
                    for (int k2=0; k2<arrDataTblView.count; k2++) {
                        
                        NSDictionary *dictDataLocal=arrDataTblView[k2];
                        
                        NSString *strLocalSysName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"SysName"]];
                        NSString *strLocalName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"StatusName"]];
                        
                        if ([strLocalSysNameSelected isEqualToString:strLocalSysName]) {
                            
                            if(str.length>0)
                            {
                                str = [NSString stringWithFormat:@"%@, %@",str,strLocalName];
                            }
                            else
                            {
                                str = [NSString stringWithFormat:@"%@",strLocalName];
                            }
                            
                        }
                        
                    }
                    
                }
                
                if(str.length>0)
                {
                    [_btnSelectStatusAppointments setTitle:str forState:UIControlStateNormal];
                }
                
                if(arrOfLeadStatusSysNameSelected.count==0)
                {
                    [_btnSelectStatusAppointments setTitle:@"----Select Status----" forState:UIControlStateNormal];
                    
                    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
                    NSArray *tempArr = [[NSArray alloc]init];
                    [defsApp setObject:tempArr forKey:@"AppointmentStatus"];
                    [defsApp synchronize];
                    
                }else{
                    
                    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
                    [defsApp setObject:arrOfLeadStatusSysNameSelected forKey:@"AppointmentStatus"];
                    [defsApp synchronize];
                    
                }
            }
            
        } else {
            
            [_btnSelectStatusAppointments setTitle:@"----Select Status----" forState:UIControlStateNormal];
            
        }
        
    } else {
        
        [_lblSelectStatus setHidden:YES];
        [_btnSelectStatusAppointments setHidden:YES];
        
        [_btnAllAppointments setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];

    }
    
    BOOL isSortByScheduleDate = [defsApp boolForKey:@"SortByScheduleDate"];
    
    if (isSortByScheduleDate) {
        
        [_btnSortByScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnSortByModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    } else {
        
        [_btnSortByModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnSortByScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    BOOL isSortByScheduleDateAscending = [defsApp boolForKey:@"SortByScheduleDateAscending"];
    
    if (isSortByScheduleDateAscending) {
        
        [_btnAscendingScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnDescendingScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    } else {
        
        [_btnDescendingScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnAscendingScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    BOOL isSortByModifiedDateAscending = [defsApp boolForKey:@"SortByModifiedDateAscending"];
    
    if (isSortByModifiedDateAscending) {
        
        [_btnAscendingModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnDescendingModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    } else {
        
        [_btnDescendingModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnAscendingModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    //RUCHIKA ADDED For CRM Opp
    BOOL isSortByOpportunitySubmittedDateAscending = [defsApp boolForKey:@"SortByOpportunitySubmittedDate"];
    
    if (isSortByOpportunitySubmittedDateAscending) {
        
        [_btnOpportunityAscending setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnOpportunityDescending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    } else {
        
        [_btnOpportunityDescending setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnOpportunityAscending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    UITapGestureRecognizer *singleTapGestureRecognizerNew = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizerNew.numberOfTapsRequired = 1;
    singleTapGestureRecognizerNew.enabled = YES;
    singleTapGestureRecognizerNew.cancelsTouchesInView = NO;
    [_scroolView addGestureRecognizer:singleTapGestureRecognizerNew];
    
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
    BOOL isGrapgDebug = [defsApp boolForKey:@"graphDebug"];
    
    if (isGrapgDebug) {
        
        [_btnGraphDebug setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    } else {
        
        [_btnGraphDebug setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isEnableAutoPropertyInformation"]) {
        [btnAutoPropertyInfo setOn:YES];
    } else {
        [btnAutoPropertyInfo setOn:NO];
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isLoadSalesDynamicFormInBackground"]) {
        [btnSalesDynamicFormmLoad setOn:YES];
    } else {
        [btnSalesDynamicFormmLoad setOn:NO];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)singleTapSettings:(UITapGestureRecognizer *)gesture
{
    [_txtViewCustomMessage resignFirstResponder];
    [_scroolView setContentOffset:CGPointMake(0,0) animated:YES];
}
//============================================================================
//============================================================================
#pragma mark- Actions Buttons Methods
//============================================================================
//============================================================================

- (IBAction)action_Refersh:(id)sender {
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Refresh All Masters"
                               message:@"Are you sure to refresh all the master lists.?"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"No-Don't Refresh" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Refresh" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             [self getMasterSalesAutomationDynamicForm];
                             
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];

}
-(void)HudView{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"Refreshing Please Wait...";
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        if([UIScreen mainScreen].bounds.size.height==1024)
        {
            hud.label.font=[UIFont systemFontOfSize:28];
            hud.margin = 10.f;//10.f
            hud.yOffset = 350.f;//150.f
        }
        else if ([UIScreen mainScreen].bounds.size.height==1366)
        {
            hud.label.font=[UIFont systemFontOfSize:32];
            hud.margin = 10.f;//10.f
            hud.yOffset = 500.f;//150.f
        }
        
        
        
    }
    else
    {
        if([UIScreen mainScreen].bounds.size.height==736)
        {
            hud.margin = 10.f;
            hud.yOffset = 250.f;
        }
        else if ([UIScreen mainScreen].bounds.size.height==667)
        {
            hud.margin = 10.f;
            hud.yOffset = 230.f;
        }
        else if ([UIScreen mainScreen].bounds.size.height==568)
        {
            hud.margin = 10.f;
            hud.yOffset = 200.f;
        }
        else
        {
            hud.margin = 10.f;
            hud.yOffset = 150.f;
        }
        
    }
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:15];
}

- (IBAction)backAction:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    BOOL fromAddAccountExisting=[defs boolForKey:@"fromAddAccountExisting"];
    BOOL fromAddAccount=[defs boolForKey:@"fromAddAccount"];
    BOOL fromHistory=[defs boolForKey:@"fromHistory"];
    BOOL fromAppointment=[defs boolForKey:@"AppointmentFlowNew"];
    if (fromAppointment) {
        
        [defs setBool:NO forKey:@"AppointmentFlowNew"];
        [defs synchronize];
        [self.navigationController popViewControllerAnimated:NO];

    }else if (fromAddAccountExisting)
    {
       /* [defs setBool:NO forKey:@"fromAddAccountExisting"];
        [defs synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        AddLeadViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddLeadViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];

//        for (UIViewController *controller in self.navigationController.viewControllers)
//        {
//            if ([controller isKindOfClass:[AddLeadView class]])
//            {
//                [self.navigationController popToViewController:controller animated:NO];
//                break;
//            }
//        }*/
    }
    else if (fromAddAccount)
    {
        [defs setBool:NO forKey:@"fromAddAccount"];
        [defs synchronize];
        
       /* UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        AddCustomerViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddCustomerViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];*/
#pragma mark - NILIND 19 fEB

        BOOL chkPop;
        chkPop=NO;
        /*for (UIViewController *controller in self.navigationController.viewControllers)
        {
            if ([controller isKindOfClass:[SearchContacts class]])
            {
                chkPop=YES;
                [self.navigationController popToViewController:controller animated:NO];
                break;
            }
        }*/
        if (chkPop==YES)
        {
            
        }
        else
        {
          /*  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"CRMiPad"
                                                                     bundle: nil];
            SearchContacts
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SearchContacts"];
            objByProductVC.strFromWhere = @"DashBoard";
            [self.navigationController pushViewController:objByProductVC animated:NO];*/
            
            /*UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                     bundle: nil];
            AddLeadViewiPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddLeadViewiPad"];
            objByProductVC.strFromWhere=@"DashBoard";
            
            [self.navigationController pushViewController:objByProductVC animated:NO];*/
        }
       
        


    }else if (fromHistory){
        [defs setBool:NO forKey:@"fromHistory"];
        [defs synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        HistoryViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HistoryViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        

//        for (UIViewController *controller in self.navigationController.viewControllers)
//        {
//            if ([controller isKindOfClass:[HistoryView class]])
//            {
//                [self.navigationController popToViewController:controller animated:NO];
//                break;
//            }
//        }
    }
    else{
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"CRM_New_iPad" bundle: nil];
            DashBoardNew_iPhoneVC
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardNew_iPhoneVC"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
//                                                                 bundle: nil];
//        DashBoardViewiPad
//        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewiPad"];
//        [self.navigationController pushViewController:objByProductVC animated:NO];

//        for (UIViewController *controller in self.navigationController.viewControllers)
//        {
//            if ([controller isKindOfClass:[DashBoardView class]])
//            {
//                [self.navigationController popToViewController:controller animated:NO];
//                break;
//            }
//        }
    }
}
- (IBAction)action_Status:(id)sender {
    if (arrOfStatus.count==0) {
        [global AlertMethod:Info :NoStatus];
    }else{
    [_tblViewSettings setContentOffset:CGPointZero animated:YES];
    _lbl_StatusOrMsg.text=@"Choose Status";
    arraySelecteValues= [[NSMutableArray alloc] init];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *arrayvalus = [userDefaults objectForKey:@"SelectedStatusValues"];
    for (int i=0; i<[arrayvalus count]; i++)
    {
        [arraySelecteValues addObject:arrayvalus[i]];
    }
    isStatusButton=YES;
    [_view_Settings setHidden:NO];
        _view_Settings.frame=CGRectMake(0, 80,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-80-50);
        _tblViewSettings.frame=CGRectMake(0, 43, [UIScreen mainScreen].bounds.size.width, _view_Settings.frame.size.height-43);
        _viewSavenCancel.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50,[UIScreen mainScreen].bounds.size.width, 50);
        [self.view addSubview:_view_Settings];
        [self.view addSubview:_viewSavenCancel];

    [_tblViewSettings reloadData];
    }
}

- (IBAction)action_WdoGraphDrawHistoryDays:(id)sender{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"WDO Graph Draw Records"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter no of record for Graph Draw History";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeNumberPad;
    }];
//    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.placeholder = @"password";
//        textField.textColor = [UIColor blueColor];
//        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textField.borderStyle = UITextBorderStyleRoundedRect;
//        textField.secureTextEntry = YES;
//    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        int textDays=[txtHistoricalDays.text intValue];
        if ((textDays>=1) && (textDays<=1000)) {
            NSLog(@"%@",txtHistoricalDays.text);
            _lblWdoGraphDrawHistoryDays.text=[NSString stringWithFormat:@"%@ rows",txtHistoricalDays.text];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            // saving an NSString
            [prefs setObject:txtHistoricalDays.text forKey:@"WdoGraphDrawHistoryRecordsCount"];
            [prefs synchronize];
        } else {
            [self performSelector:@selector(AlertViewForWdoGraphDrawHistoryDays) withObject:nil afterDelay:0.2];
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (IBAction)action_HistoricalDays:(id)sender {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Historical Data Days"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Historical days";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeNumberPad;
    }];
//    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.placeholder = @"password";
//        textField.textColor = [UIColor blueColor];
//        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textField.borderStyle = UITextBorderStyleRoundedRect;
//        textField.secureTextEntry = YES;
//    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        int textDays=[txtHistoricalDays.text intValue];
        if ((textDays>=1) && (textDays<=100)) {
            NSLog(@"%@",txtHistoricalDays.text);
            _lblHistoricalDayss.text=[NSString stringWithFormat:@"%@ Days",txtHistoricalDays.text];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            // saving an NSString
            [prefs setObject:txtHistoricalDays.text forKey:@"HistoryDays"];
            [prefs synchronize];
        } else {
            [self performSelector:@selector(AlertViewForHistoricalDays) withObject:nil afterDelay:0.2];
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (IBAction)action_CustomMessage:(id)sender {
    if (arrOfCustomMessage.count==0) {
        [global AlertMethod:Info :@"No Custom Message Available"];
    }else{
    
        _view_Settings.frame=CGRectMake(0, 80,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-80-50);
        _tblViewSettings.frame=CGRectMake(0, 43, [UIScreen mainScreen].bounds.size.width, _view_Settings.frame.size.height-43);
        _viewSavenCancel.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50,[UIScreen mainScreen].bounds.size.width, 50);
        [self.view addSubview:_view_Settings];
        [self.view addSubview:_viewSavenCancel];
        
    [_tblViewSettings setContentOffset:CGPointZero animated:YES];
    _lbl_StatusOrMsg.text=@"Select Custom Message";
    arraySelecteValues= [[NSMutableArray alloc] init];
    isStatusButton=NO;
    [_view_Settings setHidden:NO];
    [_tblViewSettings reloadData];
    }
}

- (IBAction)action_Ok:(id)sender {
    if (isStatusButton) {
        [_view_Settings setHidden:YES];
        [_view_Settings removeFromSuperview];
        [_viewSavenCancel removeFromSuperview];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:arraySelecteValues forKey:@"SelectedStatusValues"];
        [userDefaults synchronize];
        NSString *joinedComponents = [arraySelecteValues componentsJoinedByString:@","];
        if (joinedComponents.length==0) {
            _lblSelectedStatus.text=[NSString stringWithFormat:@"Choose Status"];
        } else {
            _lblSelectedStatus.text=[NSString stringWithFormat:@"Selected-(%@)",joinedComponents];
        }
    } else {
        self.checkedIndexPath = nil;
        [_view_Settings setHidden:YES];
        [_view_Settings removeFromSuperview];
        [_viewSavenCancel removeFromSuperview];
        NSUserDefaults *prefsCustomMsg = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        [prefsCustomMsg setObject:checkedCustomMessage forKey:@"CustomMessage"];
        [prefsCustomMsg synchronize];
        _txtViewCustomMessage.text=checkedCustomMessage;
    }
}
- (IBAction)action_CancelTblViewSettings:(id)sender {
    arraySelecteValues= [[NSMutableArray alloc] init];
    [_view_Settings setHidden:YES];
    [_view_Settings removeFromSuperview];
    [_viewSavenCancel removeFromSuperview];
    self.checkedIndexPath = nil;
}


- (IBAction)isEnableAutoPropertyInfoAction:(UISwitch *)sender {
    
    if (sender.isOn) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isEnableAutoPropertyInformation"];
    }else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isEnableAutoPropertyInformation"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)isSalesDynamicFormLoadAction:(UISwitch *)sender {
    
    if (sender.isOn) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoadSalesDynamicFormInBackground"];
    }else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLoadSalesDynamicFormInBackground"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//============================================================================
#pragma mark- Table View Delegate and Data Source Methods
//============================================================================
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (isStatusButton) {
//    if ([UIScreen mainScreen].bounds.size.height==667) {
//        return 40;
//    }else if ([UIScreen mainScreen].bounds.size.height==736) {
//        return 40;
//    }
//    else
//        return 20;
//    }else
//        return 200;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag==102) {
        
        return arrDataTblView.count;
        
    }
    else if (tableView.tag==103) {
        
        return arrDashboardWidget.count;
        
    }
    else {
        
        if (isStatusButton) {
            return arrOfStatus.count;
        } else {
            return arrOfCustomMessage.count;
        }
        
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==102) {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
        cell.textLabel.text=[dictData valueForKey:@"StatusName"];
        
        NSString *strStatusSysNameSelected  = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        
        if ([arrOfLeadStatusSysNameSelected containsObject:strStatusSysNameSelected]) {
            
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
        } else {
            
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
        }
        
        if ([UIScreen mainScreen].bounds.size.height>=1024) {
            cell.textLabel.font=[UIFont systemFontOfSize:20];
        }else if ([UIScreen mainScreen].bounds.size.height==667) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }else if ([UIScreen mainScreen].bounds.size.height==736) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }
        else
            cell.textLabel.font=[UIFont systemFontOfSize:15];
        
        return cell;
        
    }
    else if (tableView.tag==103) {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSDictionary *dictData=[arrDashboardWidget objectAtIndex:indexPath.row];
        
        NSString *strWidgetName  = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Title"]];

        cell.textLabel.text = strWidgetName;
        
        
        if ([arrDashboardWidgetSelected containsObject:strWidgetName]) {
            
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
        } else {
            
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
        }
        
        if ([UIScreen mainScreen].bounds.size.height>=1024) {
            cell.textLabel.font=[UIFont systemFontOfSize:20];
        }else if ([UIScreen mainScreen].bounds.size.height==667) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }else if ([UIScreen mainScreen].bounds.size.height==736) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }
        else
            cell.textLabel.font=[UIFont systemFontOfSize:15];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    
    else {
        
        SettingsTableViewCell *cell = (SettingsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SettingsCells" forIndexPath:indexPath];
        
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;

    }
    
}
- (void)configureCell:(SettingsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    // Fetch Record
    if (isStatusButton) {
        
        if (!(arrOfStatus.count==0)) {
            
        cell.LblDataSettings.text=[arrOfStatus objectAtIndex:indexPath.row];
        
        NSString *str=[arrOfStatus objectAtIndex:indexPath.row];
        
        long index=100000;
        
        for (int k=0; k<arraySelecteValues.count; k++) {
            if ([arraySelecteValues containsObject:str]) {
                index=indexPath.row;
                break;
            }
        }
        
        if (indexPath.row==index) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        }
    }else{
        
        if (!(arrOfCustomMessage.count==0)) {

         NSDictionary *dictData=[arrOfCustomMessage objectAtIndex:indexPath.row];
        cell.LblDataSettings.text=[dictData valueForKey:@"SMS_Message"];
        //do you stuff here
        if([self.checkedIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strCustomMsg = [prefs stringForKey:@"CustomMessage"];

            if ([[dictData valueForKey:@"SMS_Message"] isEqualToString:strCustomMsg]) {
                
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                self.checkedIndexPath = indexPath;
                
            }
            
            
        }
    }
    if ([UIScreen mainScreen].bounds.size.height>600) {
        cell.LblDataSettings.font=[UIFont systemFontOfSize:17];
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        cell.LblDataSettings.font=[UIFont systemFontOfSize:17];
    }
    else
        cell.LblDataSettings.font=[UIFont systemFontOfSize:15];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==102) {
        
        if ([UIScreen mainScreen].bounds.size.height==667) {
            return 50;
        }else if ([UIScreen mainScreen].bounds.size.height==736) {
            return 50;
        }
        else
            return 90;
        
    } else {
        
        return 50;

    }
}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//    }
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==102) {
        
        NSDictionary *dictStatus = [arrDataTblView objectAtIndex:indexPath.row];
        
        NSString *strStatusSysNameSelected  = [NSString stringWithFormat:@"%@",[dictStatus valueForKey:@"SysName"]];
        
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            [arrOfLeadStatusSysNameSelected addObject:[NSString stringWithFormat:@"%@",[dictStatus valueForKey:@"SysName"]]];
        }
        else
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
            
            for (int k1=0; k1<arrOfLeadStatusSysNameSelected.count; k1++) {
                
                // NSDictionary *dictDataTemp = arrSourceSelectedSysName[k1];
                
                NSString *strSourceId  = [NSString stringWithFormat:@"%@",arrOfLeadStatusSysNameSelected[k1]];
                
                if ([strSourceId isEqualToString:strStatusSysNameSelected]) {
                    
                    [arrOfLeadStatusSysNameSelected removeObjectAtIndex:k1];
                    
                    break;
                }
                
            }
            
            if(arrOfLeadStatusSysNameSelected.count==0)
            {
                [_btnSelectStatusAppointments setTitle:@"----Select Status----" forState:UIControlStateNormal];
            }
        }
        
        if(arrOfLeadStatusSysNameSelected.count>0)
        {
            NSString *str = @"";
            for (int k1=0; k1<arrOfLeadStatusSysNameSelected.count; k1++) {
                
                NSString *strLocalSysNameSelected = arrOfLeadStatusSysNameSelected[k1];
                
                for (int k2=0; k2<arrDataTblView.count; k2++) {
                    
                    NSDictionary *dictDataLocal=arrDataTblView[k2];
                    
                    NSString *strLocalSysName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"SysName"]];
                    NSString *strLocalName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"StatusName"]];
                    
                    if ([strLocalSysNameSelected isEqualToString:strLocalSysName]) {
                        
                        if(str.length>0)
                        {
                            str = [NSString stringWithFormat:@"%@, %@",str,strLocalName];
                        }
                        else
                        {
                            str = [NSString stringWithFormat:@"%@",strLocalName];
                        }
                        
                    }
                    
                }
                
            }
            
            if(str.length>0)
            {
                [_btnSelectStatusAppointments setTitle:str forState:UIControlStateNormal];
            }
            
            if(arrOfLeadStatusSysNameSelected.count==0)
            {
                [_btnSelectStatusAppointments setTitle:@"----Select Status----" forState:UIControlStateNormal];
                
                NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
                NSArray *tempArr = [[NSArray alloc]init];
                [defsApp setObject:tempArr forKey:@"AppointmentStatus"];
                [defsApp synchronize];

            }else{
             
                NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
                [defsApp setObject:arrOfLeadStatusSysNameSelected forKey:@"AppointmentStatus"];
                [defsApp synchronize];
                
            }
        }else {
            
            [_btnSelectStatusAppointments setTitle:@"----Select Status----" forState:UIControlStateNormal];
            
            NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
            NSArray *tempArr = [[NSArray alloc]init];
            [defsApp setObject:tempArr forKey:@"AppointmentStatus"];
            [defsApp synchronize];
            
        }
        
    }
    else if (tableView.tag==103)
    {
        
        NSDictionary *dictStatus = [arrDashboardWidget objectAtIndex:indexPath.row];
        
        NSString *strWidgetName  = [NSString stringWithFormat:@"%@",[dictStatus valueForKey:@"Title"]];
        
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        if ([arrDashboardWidgetSelected containsObject:strWidgetName])
        {
            [arrDashboardWidgetSelected removeObject:strWidgetName];
        }
        else
        {
            [arrDashboardWidgetSelected addObject:strWidgetName];
        }
        
    }
    else
    {
        
        if (isStatusButton) {
            NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
            UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
            if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                [arraySelecteValues addObject:[arrOfStatus objectAtIndex:indexPath.row]];
            }
            else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                [arraySelecteValues removeObject:[arrOfStatus objectAtIndex:indexPath.row]];
            }
        }else{
            //do work for checkmark
            if(self.checkedIndexPath)
            {
                UITableViewCell* uncheckCell = [tableView
                                                cellForRowAtIndexPath:self.checkedIndexPath];
                uncheckCell.accessoryType = UITableViewCellAccessoryNone;
            }
            if([self.checkedIndexPath isEqual:indexPath])
            {
                self.checkedIndexPath = nil;
            }
            else
            {
                UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                self.checkedIndexPath = indexPath;
                NSDictionary *dictData=[arrOfCustomMessage objectAtIndex:indexPath.row];
                checkedCustomMessage=[dictData valueForKey:@"SMS_Message"];
                //  checkedCustomMessage = [arrOfCustomMessage objectAtIndex:indexPath.row];
            }
            
            
            self.checkedIndexPath = nil;
            [_view_Settings setHidden:YES];
            [_view_Settings removeFromSuperview];
            [_viewSavenCancel removeFromSuperview];
            NSUserDefaults *prefsCustomMsg = [NSUserDefaults standardUserDefaults];
            // saving an NSString
            [prefsCustomMsg setObject:checkedCustomMessage forKey:@"CustomMessage"];
            [prefsCustomMsg synchronize];
            _txtViewCustomMessage.text=checkedCustomMessage;
            
        }
        
    }

}

//============================================================================
#pragma mark- Methods
//============================================================================

-(void)AlertViewForHistoricalDays{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter days between 1 to 100" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)AlertViewForWdoGraphDrawHistoryDays{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter rows between 1 to 1000" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)fetchCustomMessage{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Custom Message..."];
    
    //http://ggq.stagingsoftware.com/GoHandler.ashx?key=getSms&CompanyKey=rentokil
    //http://www.gogetquality.com/survey/GoHandler.ashx?key=getSms&cID=24&CompanyKey=rentokil
  //  NSString *strUrl=[NSString stringWithFormat:@"%@",@"http://ggq.stagingsoftware.com/GoHandler.ashx?key=getSms&CompanyKey=rentokil"];
    
     NSString *strUrl=[NSString stringWithFormat:@"%@%@",UrlGetCustomMessage,strGGQCompanyKey];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"GetCustomMessage" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     if (response.count==0) {
                         
                        // [global AlertMethod:Info :@"No Custom Message Available"];
                         
                     } else {
                         arrOfCustomMessage=(NSArray*)response;
                         if (!(arrOfCustomMessage.count==0)) {
                             
                             NSDictionary *dictData=[arrOfCustomMessage objectAtIndex:0];
                             
                             NSUserDefaults *prefsCustomMsg = [NSUserDefaults standardUserDefaults];
                             
                             NSString *strCustomMsgPresent=[prefsCustomMsg valueForKey:@"CustomMessage"];
                             if (strCustomMsgPresent.length>0) {
                                 
                             } else {
                                 checkedCustomMessage=[dictData valueForKey:@"SMS_Message"];
                                 [prefsCustomMsg setObject:checkedCustomMessage forKey:@"CustomMessage"];
                                 [prefsCustomMsg synchronize];
                                 _txtViewCustomMessage.text=checkedCustomMessage;

                             }
                         }
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}


//------------***************----------------*****************--------------------*****************------------------
//------------***************----------------*****************--------------------*****************------------------
//------------***************----------------*****************--------------------*****************------------------
//------------***************----------------*****************--------------------*****************------------------


//============================================================================
//============================================================================
#pragma mark Loading Employee Methods
//============================================================================
//============================================================================
-(void)getEmployeeListInBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getEmployeeList];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}

-(void)getEmployeeList{
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMain,UrlGetEmployeeList,strCompanyKey];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"getEmployeeList" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                   //  NSString *str=[NSString stringWithFormat:@"%@",response];
                    // UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Response" message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     // [alert show];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}


//============================================================================
//============================================================================
#pragma mark Lead Detail Master Methods
//============================================================================
//============================================================================
-(void)getLeadDetailMasterInBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getLeadDetailMaster];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}

-(void)getLeadDetailMaster{
    
   // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Leads Masters..."];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMain,UrlGetLeadDetailMaster,strCompanyKey];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"getLeadDetailMaster" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                    // NSString *str=[NSString stringWithFormat:@"%@",response];
                    // UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Response" message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     // [alert show];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}


//============================================================================
//============================================================================
#pragma mark Get Lead Count Methods
//============================================================================
//============================================================================
-(void)getLeadcountInBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getLeadcount];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}

-(void)getLeadcount{
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMain,UrlGetLeadDetailMaster,strCompanyKey];
    //============================================================================
    //=================logout===========================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"getLeadCount" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                    // NSString *str=[NSString stringWithFormat:@"%@",response];
                    // UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Response" message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     // [alert show];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}
//============================================================================
//============================================================================
#pragma mark Get Master Service Automation Methods
//============================================================================
//============================================================================
-(void)getMasterServiceAutomationInBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getMasterServiceAutomation];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}

-(void)getMasterServiceAutomation{
    
   // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Service Masters..."];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMainServiceAutomation,UrlGetMasterServiceAutomation,strCompanyKey];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"getMasterServiceAutomation" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                    // NSString *str=[NSString stringWithFormat:@"%@",response];
                    // UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Response" message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     // [alert show];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}

//============================================================================
//============================================================================
#pragma mark Get Master Product/Chemicals Service Automation Methods
//============================================================================
//============================================================================
-(void)getMasterProductChemicalsServiceAutomationInBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getMasterProductChemicalsServiceAutomation];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}

-(void)getMasterProductChemicalsServiceAutomation{
    
   // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading All Product Masters..."];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMainServiceAutomation,UrlGetMasterProductChemicalsServiceAutomation,strCompanyId];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"getMasterProductChemicalsServiceAutomation" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                    // NSString *str=[NSString stringWithFormat:@"%@",response];
                    // UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Response" message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     // [alert show];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}
//============================================================================
//============================================================================
#pragma mark Get Master Sales Automation Dynamic Form Masters Methods
//============================================================================
//============================================================================
-(void)getMasterSalesAutomationDynamicFormInBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getMasterSalesAutomationDynamicForm];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}

-(void)getMasterSalesAutomationDynamicForm{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Sales Masters..."];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMainSalesAutomation,UrlSalesDynamicFormMasters,strCompanyKey];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"MastersSalesDynamic" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
                 
                 [self getMasterServiceAutomationDynamicForm];
                 
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}
//============================================================================
//============================================================================
#pragma mark Get Master Service Automation Dynamic Form Masters Methods
//============================================================================
//============================================================================
-(void)getMasterServiceAutomationDynamicFormInBackground
{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self getMasterServiceAutomationDynamicForm];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
        });
    });
}

-(void)getMasterServiceAutomationDynamicForm{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Service Masters..."];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMainServiceAutomation,UrlServiceDynamicFormMasters,strCompanyKey];
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"MastersServiceDynamic" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
                 
                 [self DownloadAllMastersSettings];
                 
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}
-(void)DownloadAllMastersSettings{
    
    [self HudView];
    
    //============================================================================
    [self performSelector:@selector(getLeadcountInBackground) withObject:nil afterDelay:0.0];
    //============================================================================
    [self performSelector:@selector(getEmployeeListInBackground) withObject:nil afterDelay:2.0];
    //============================================================================
    [self performSelector:@selector(getLeadDetailMasterInBackground) withObject:nil afterDelay:4.0];
    //============================================================================
    [self performSelector:@selector(getMasterServiceAutomationInBackground) withObject:nil afterDelay:6.0];
    //============================================================================
    [self performSelector:@selector(getMasterProductChemicalsServiceAutomationInBackground) withObject:nil afterDelay:8.0];
    //============================================================================

}
-(void)getDashboardWidgets
{
    //        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

       // NSDictionary *dictLoginData = [NSUserDefaults standardUserDefaults]
//    if dictLoginData.value(forKey: "DashboardWidgetMasters") is NSArray
//    {
//        arrDashboardWidget = dictLoginData.value(forKey: "DashboardWidgetMasters") as! NSArray
//    }
    
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(void)textViewDidBeginEditing:(UITextView *)textField{
    
    [_scroolView setContentOffset:CGPointMake(0,textField.frame.origin.y-50) animated:YES];
    
}
-(void)textViewDidEndEditing:(UITextView *)textField
{
    
    [_scroolView setContentOffset:CGPointMake(0,0) animated:YES];
    
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }

    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

- (IBAction)action_EditCustomMsg:(id)sender {
    
    if ([_btnEditCustomMsg.titleLabel.text isEqualToString:@"Edit"]) {
        [_btnEditCustomMsg setTitle:@"Save" forState:UIControlStateNormal];
        [_txtViewCustomMessage setEditable:YES];
        [_txtViewCustomMessage becomeFirstResponder];
    } else {
        //checkedCustomMessage=_txtViewCustomMessage.text;
        NSUserDefaults *prefsCustomMsg = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        [prefsCustomMsg setObject:_txtViewCustomMessage.text forKey:@"CustomMessage"];
        [prefsCustomMsg synchronize];
        [_btnEditCustomMsg setTitle:@"Edit" forState:UIControlStateNormal];
        [_txtViewCustomMessage setEditable:NO];
    }
}
- (IBAction)valueChangeImageCaptionSwitch:(id)sender {
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];

    if ([sender isOn]) {
        
        [defs setBool:NO forKey:@"imageCaptionSetting"];
        //[defs setBool:NO forKey:@"graphDebug"];
        [defs synchronize];
        [_imageCaptionSwitch setOn:NO];
        
    } else {
        
        [defs setBool:YES forKey:@"imageCaptionSetting"];
        //[defs setBool:YES forKey:@"graphDebug"];
        [defs synchronize];
        [_imageCaptionSwitch setOn:YES];
        
    }
    
}

- (IBAction)valueChangeImageCaptionSwitchSales:(id)sender {
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    
    if ([sender isOn]) {
        
        [defs setBool:NO forKey:@"imageCaptionSettingSales"];
        [defs synchronize];
        [_imageCaptionSwitchSales setOn:NO];
        
    } else {
        
        [defs setBool:YES forKey:@"imageCaptionSettingSales"];
        [defs synchronize];
        [_imageCaptionSwitchSales setOn:YES];
        
    }

}

//============================================================================
//============================================================================
#pragma mark- ---------------------Appointments Settings Methods-----------------
//============================================================================
//============================================================================

- (IBAction)action_GraphDebug:(id)sender{
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnGraphDebug imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        [_btnGraphDebug setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];

        
        NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
        [defsApp setBool:YES forKey:@"graphDebug"];
        [defsApp synchronize];
        
    }else{
        
        [_btnGraphDebug setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
        NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
        [defsApp setBool:NO forKey:@"graphDebug"];
        [defsApp synchronize];
        
    }
    
}


- (IBAction)action_SortByScheduleDate:(id)sender {
    [_btnSortByScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnSortByModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
    [defsApp setBool:YES forKey:@"SortByScheduleDate"];
    [defsApp synchronize];
    
}
- (IBAction)action_SortByModifiedDate:(id)sender {
    [_btnSortByModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnSortByScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
    [defsApp setBool:NO forKey:@"SortByScheduleDate"];
    [defsApp synchronize];
    
}
- (IBAction)action_AscendingScheduleDate:(id)sender {
    
    [_btnAscendingScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnDescendingScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
    [defsApp setBool:YES forKey:@"SortByScheduleDateAscending"];
    [defsApp synchronize];
    
}
- (IBAction)action_DescendingScheduleDate:(id)sender {
    
    [_btnDescendingScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnAscendingScheduleDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
    [defsApp setBool:NO forKey:@"SortByScheduleDateAscending"];
    [defsApp synchronize];
    
    
}
- (IBAction)action_AscendingModifiedDate:(id)sender {
    
    [_btnAscendingModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnDescendingModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
    [defsApp setBool:YES forKey:@"SortByModifiedDateAscending"];
    [defsApp synchronize];
    
}
- (IBAction)action_DescendingModifiedDate:(id)sender {
    
    [_btnDescendingModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnAscendingModifiedDate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
    [defsApp setBool:NO forKey:@"SortByModifiedDateAscending"];
    [defsApp synchronize];
    
}
- (IBAction)action_AllAppointments:(id)sender {
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnAllAppointments imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        [_btnAllAppointments setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [_lblSelectStatus setHidden:YES];
        [_btnSelectStatusAppointments setHidden:YES];
        
        NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
        [defsApp setBool:YES forKey:@"AllAppointments"];
        [defsApp synchronize];
        
    }else{
        
        [_btnAllAppointments setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [_lblSelectStatus setHidden:NO];
        [_btnSelectStatusAppointments setHidden:NO];
        [_btnSelectStatusAppointments setTitle:@"----Select Status----" forState:UIControlStateNormal];
        
        NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
        [defsApp setBool:NO forKey:@"AllAppointments"];
        [defsApp synchronize];
        
        NSArray *tempArr = [defsApp objectForKey:@"AppointmentStatus"];
        
        arrOfLeadStatusSysNameSelected = nil;
        
        arrOfLeadStatusSysNameSelected = [[NSMutableArray alloc]init];
        
        [arrOfLeadStatusSysNameSelected addObjectsFromArray:tempArr];
        
        if (tempArr.count>0) {
            
            arrDataTblView=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
            
            NSArray *arrOfLeadStatusMasters=[defsLead valueForKey:@"LeadStatusMasters"];
            
            for (int k=0;k<arrOfLeadStatusMasters.count; k++) {
                NSDictionary *dictData=arrOfLeadStatusMasters[k];
                [arrDataTblView addObject:dictData];
            }
            
            NSArray *objects1 = [NSArray arrayWithObjects:
                                 @"InComplete",
                                 @"InComplete",nil];
            
            NSArray *keys1 = [NSArray arrayWithObjects:
                              @"SysName",
                              @"StatusName",nil];
            
            NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects1 forKeys:keys1];
            
            [arrDataTblView addObject:dict_ToSend];
            
            NSArray *objectsReset = [NSArray arrayWithObjects:
                                     @"Reset",
                                     @"Reset",nil];
            
            NSArray *keysReset = [NSArray arrayWithObjects:
                                  @"SysName",
                                  @"StatusName",nil];
            
            NSDictionary *dict_ToSendReset = [[NSDictionary alloc] initWithObjects:objectsReset forKeys:keysReset];
            
            [arrDataTblView addObject:dict_ToSendReset];
            
            NSArray *objectsCompletePending = [NSArray arrayWithObjects:
                                               @"CompletePending",
                                               @"CompletePending",nil];
            
            NSArray *keysCompletePending = [NSArray arrayWithObjects:
                                            @"SysName",
                                            @"StatusName",nil];
            
            NSDictionary *dict_ToSendCompletePending = [[NSDictionary alloc] initWithObjects:objectsCompletePending forKeys:keysCompletePending];
            
            [arrDataTblView addObject:dict_ToSendCompletePending];
            
            if(arrOfLeadStatusSysNameSelected.count>0)
            {
                NSString *str = @"";
                for (int k1=0; k1<arrOfLeadStatusSysNameSelected.count; k1++) {
                    
                    NSString *strLocalSysNameSelected = arrOfLeadStatusSysNameSelected[k1];
                    
                    for (int k2=0; k2<arrDataTblView.count; k2++) {
                        
                        NSDictionary *dictDataLocal=arrDataTblView[k2];
                        
                        NSString *strLocalSysName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"SysName"]];
                        NSString *strLocalName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"StatusName"]];
                        
                        if ([strLocalSysNameSelected isEqualToString:strLocalSysName]) {
                            
                            if(str.length>0)
                            {
                                str = [NSString stringWithFormat:@"%@, %@",str,strLocalName];
                            }
                            else
                            {
                                str = [NSString stringWithFormat:@"%@",strLocalName];
                            }
                            
                        }
                        
                    }
                    
                }
                
                if(str.length>0)
                {
                    [_btnSelectStatusAppointments setTitle:str forState:UIControlStateNormal];
                }
                
                if(arrOfLeadStatusSysNameSelected.count==0)
                {
                    [_btnSelectStatusAppointments setTitle:@"----Select Status----" forState:UIControlStateNormal];
                    
                    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
                    NSArray *tempArr = [[NSArray alloc]init];
                    [defsApp setObject:tempArr forKey:@"AppointmentStatus"];
                    [defsApp synchronize];
                    
                }else{
                    
                    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
                    [defsApp setObject:arrOfLeadStatusSysNameSelected forKey:@"AppointmentStatus"];
                    [defsApp synchronize];
                    
                }
            }
            
        } else {
            
            [_btnSelectStatusAppointments setTitle:@"----Select Status----" forState:UIControlStateNormal];
            
        }
        
    }
    
//    NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
//    NSArray *tempArr = [[NSArray alloc]init];
//    [defsApp setObject:tempArr forKey:@"AppointmentStatus"];
//    [defsApp synchronize];
//    [_btnSelectStatusAppointments setTitle:@"----Select Status----" forState:UIControlStateNormal];

}
- (IBAction)acttion_StatusAppointments:(id)sender {
        
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    NSArray *arrOfLeadStatusMasters=[defsLead valueForKey:@"LeadStatusMasters"];
    
    for (int k=0;k<arrOfLeadStatusMasters.count; k++) {
        NSDictionary *dictData=arrOfLeadStatusMasters[k];
        [arrDataTblView addObject:dictData];
    }

    NSArray *objects1 = [NSArray arrayWithObjects:
                         @"InComplete",
                         @"InComplete",nil];
    
    NSArray *keys1 = [NSArray arrayWithObjects:
                      @"SysName",
                      @"StatusName",nil];
    
    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects1 forKeys:keys1];

    [arrDataTblView addObject:dict_ToSend];
    
    NSArray *objectsReset = [NSArray arrayWithObjects:
                         @"Reset",
                         @"Reset",nil];
    
    NSArray *keysReset = [NSArray arrayWithObjects:
                      @"SysName",
                      @"StatusName",nil];
    
    NSDictionary *dict_ToSendReset = [[NSDictionary alloc] initWithObjects:objectsReset forKeys:keysReset];
    
    [arrDataTblView addObject:dict_ToSendReset];
    
    NSArray *objectsCompletePending = [NSArray arrayWithObjects:
                         @"CompletePending",
                         @"CompletePending",nil];
    
    NSArray *keysCompletePending = [NSArray arrayWithObjects:
                      @"SysName",
                      @"StatusName",nil];
    
    NSDictionary *dict_ToSendCompletePending = [[NSDictionary alloc] initWithObjects:objectsCompletePending forKeys:keysCompletePending];
    
    [arrDataTblView addObject:dict_ToSendCompletePending];

    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }
    else{
        tblData.tag=102;
        [self tableLoad:tblData.tag];
    }
    
}

//Ruchika Added CRM opportunity sorting
- (IBAction)action_Opportunity_Ascending:(id)sender {
    
    [_btnOpportunityAscending setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnOpportunityDescending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
    [defsApp setBool:YES forKey:@"SortByOpportunitySubmittedDate"];
    [defsApp synchronize];
    
}
- (IBAction)action_Opportunity_Descending:(id)sender {
    
    [_btnOpportunityDescending setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnOpportunityAscending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    NSUserDefaults *defsApp =[NSUserDefaults standardUserDefaults];
    [defsApp setBool:NO forKey:@"SortByOpportunitySubmittedDate"];
    [defsApp synchronize];
   
}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
        case 103:
        {
            [self setTableFrame];
            break;
        }
            
        default:
            break;
    }
    //  [tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
    
}

-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}

//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)singleTap:(UITapGestureRecognizer *)gesture {
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_txtViewCustomMessage resignFirstResponder];
    
    [viewForDate removeFromSuperview];
    [tblData removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
    if(arrDashboardWidgetSelected.count > 0)
    {
        NSString *str = [arrDashboardWidgetSelected componentsJoinedByString:@","];
        [_btnSelectDashboardWidgets setTitle:str forState:UIControlStateNormal];
    }
    else
    {
        [_btnSelectDashboardWidgets setTitle:@"----Select Widgets----" forState:UIControlStateNormal];

    }
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    [defs setObject:arrDashboardWidgetSelected forKey:@"DashboardWidgets"];
    [defs synchronize];
    
}

- (IBAction)actionOnDashoardWidgets:(id)sender
{
    if (arrDashboardWidget.count == 0)
    {
        [global AlertMethod:Info :@"No Data available"];
    }
    else
    {
        tblData.tag=103;
        [self tableLoad:tblData.tag];
    }
}
-(void)setDashboardWidgets
{
    NSArray *arr = [[NSArray alloc]init];
    arr = [[NSUserDefaults standardUserDefaults] valueForKey:@"DashboardWidgets"];
    arrDashboardWidgetSelected = [[NSMutableArray alloc]init];
    [arrDashboardWidgetSelected addObjectsFromArray:arr];
    
    
    if(arrDashboardWidgetSelected.count > 0)
    {
        NSString *str = [arrDashboardWidgetSelected componentsJoinedByString:@","];
        [_btnSelectDashboardWidgets setTitle:str forState:UIControlStateNormal];
    }
    else
    {
        [_btnSelectDashboardWidgets setTitle:@"----Select Widgets----" forState:UIControlStateNormal];

    }
    
    [_btnSelectDashboardWidgets.layer setCornerRadius:5.0f];
    [_btnSelectDashboardWidgets.layer setBorderColor:[UIColor themeColor].CGColor];
    [_btnSelectDashboardWidgets.layer setBorderWidth:0.8f];
    [_btnSelectDashboardWidgets.layer setShadowColor:[UIColor themeColor].CGColor];
    [_btnSelectDashboardWidgets.layer setShadowOpacity:0.3];
    [_btnSelectDashboardWidgets.layer setShadowRadius:3.0];
    [_btnSelectDashboardWidgets.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}


@end
