//
//  LoginViewControlleriPad.m
//  DPS
//  123 123
//  Created by Rakesh Jain on 20/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "LoginViewControlleriPad.h"
#import "DejalActivityView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import "ChangePasswordView.h"
#import "ServiceDetailAppointmentView.h"
#import "AppointmentView.h"
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>
#import "AllImportsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <FIRCrashlytics.h>
#import "DPS-Swift.h"
#import <FirebasePerformance/FirebasePerformance.h>

@interface LoginViewControlleriPad ()<CLLocationManagerDelegate>
{
    Global *global;
    CLLocationManager *locationManager;
    NSString *strForgotPassCompanykey,*strForgotPassUserName;

}
@end

@implementation LoginViewControlleriPad

- (void)viewDidLoad {
    
    [super viewDidLoad];
    //============================================================================
    locationManager = [[CLLocationManager alloc]init] ;
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    //============================================================================
    
    global = [[Global alloc] init];
    [Global addPaddingView:_txtCompanyKey];
    [Global addPaddingView:_txtUserName];
    [Global addPaddingView:_txtPassword];

    //============================================================================
    //============================================================================
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        
    }
    
    [self.navigationController.navigationBar setHidden:YES];
    
    //============================================================================
    //============================================================================
    
    
    /*
    if ([UIScreen mainScreen].bounds.size.height==667) {
        _const_ImgView_T.constant=50;
        _const_ImgView_B.constant=40;
        _const_imgView_H.constant=93;
        _const_ImgView_W.constant=200;
        _const_BackView_H.constant=320;
    }
    else if ([UIScreen mainScreen].bounds.size.height==736) {
        _const_ImgView_T.constant=50;
        _const_ImgView_B.constant=40;
        _const_imgView_H.constant=93;
        _const_ImgView_W.constant=200;
        _const_BackView_H.constant=320;
    }
    else if ([UIScreen mainScreen].bounds.size.height==480) {
        _const_ImgView_T.constant=10;
        _const_ImgView_B.constant=10;
        _const_imgView_H.constant=60;
        _const_ImgView_W.constant=130;
        _const_BackView_H.constant=300;
    }
    else if ([UIScreen mainScreen].bounds.size.height==568) {
        _const_ImgView_T.constant=30;
        _const_ImgView_B.constant=20;
        _const_imgView_H.constant=70;
        _const_ImgView_W.constant=150;
        _const_BackView_H.constant=300;
    }
    else if ([UIScreen mainScreen].bounds.size.height==1024) {
        _const_imgView_H.constant=139;
        _const_ImgView_W.constant=300;
        _const_BackView_H.constant=640;
    }else if ([UIScreen mainScreen].bounds.size.height==1366) {
        _const_imgView_H.constant=139;
        _const_ImgView_W.constant=300;
        _const_BackView_H.constant=640;
    }
    */
    
    //============================================================================
    //============================================================================
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    BOOL isAlreadyLoggedIn=[defs boolForKey:@"YesLoggedIn"];
    BOOL isFromPassword =[defs boolForKey:@"fromChangePass"];
    if (isAlreadyLoggedIn) {
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        BOOL isChangePassword=[defs boolForKey:@"YesChangePassword"];
        if (isChangePassword) {
            [self goToChangePassword];
        } else {
            [self goToDashBoard];
        }
    }
    else
    {
        NSUserDefaults *defsLogin = [NSUserDefaults standardUserDefaults];
        _txtCompanyKey.text=[defsLogin valueForKey:@"companyKey"];
        _txtUserName.text=[defsLogin valueForKey:@"username"];
        _txtPassword.text=[defsLogin valueForKey:@"password"];
        
        [self LoginMethod];
    }
    }
    else if(isFromPassword){
        
        [defs setBool:NO forKey:@"fromChangePass"];
        [defs synchronize];
        
        NSUserDefaults *defsLogin = [NSUserDefaults standardUserDefaults];
        _txtCompanyKey.text=[defsLogin valueForKey:@"CompanyKeyChangePass"];
        _txtUserName.text=[defsLogin valueForKey:@"usernameChangePass"];
        _txtPassword.text=[defsLogin valueForKey:@"confirmPass"];
        
    }
    //============================================================================
    //============================================================================

    // Do any additional setup after loading the view.
    
    strForgotPassCompanykey = @"";
    strForgotPassUserName = @"";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) logUser {
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    
    // TODO: Use the current user's information
    [[FIRCrashlytics crashlytics] setCustomValue:_txtCompanyKey.text forKey:@"Company Key"];
    [[FIRCrashlytics crashlytics] setCustomValue:_txtUserName.text forKey:@"User Name"];
    [[FIRCrashlytics crashlytics] setUserID:[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]]];
    //[[FIRCrashlytics crashlytics] setCustomValue:_txtPassword.text forKey:@"Other"];

    /*[[Crashlytics sharedInstance] setUserIdentifier:_txtCompanyKey.text];
    [[Crashlytics sharedInstance] setUserEmail:@""]; // _txtPassword.text
    [[Crashlytics sharedInstance] setUserName:_txtUserName.text];*/
    
    FIRTrace *trace = [FIRPerformance startTraceWithName:@"Company Key"];

    [trace setValue:_txtCompanyKey.text forAttribute:@"experiment"];
    
}

//============================================================================
//============================================================================
#pragma mark Action Buttons
//============================================================================
//============================================================================

- (IBAction)loginAction:(id)sender {
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
            NSString *strTitle = Alert;
            NSString *strMsg = ErrorInternetMsg;
            [global AlertMethod:strTitle :strMsg];
            [DejalBezelActivityView removeView];
    }
    else
    {
        [self LoginMethod];
    }
}

- (IBAction)forgotPass:(id)sender{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorInternetMsg;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
    }
    else
    {
        
    [self forotPasswordAlertview];
        
    }
    
}

-(IBAction)hideKeyBoard:(id)sender{
    [self resignFirstResponder];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [_txtCompanyKey resignFirstResponder];
    [_txtPassword resignFirstResponder];
    [_txtUserName resignFirstResponder];
    if (self.view.frame.origin.y==-100.0) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 100.0), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}

//============================================================================
//============================================================================
#pragma mark TextField Delegate Method
//============================================================================
//============================================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_txtCompanyKey)
    {
        [_txtUserName becomeFirstResponder];
        
    }
    else if (textField==_txtUserName)
    {
        [_txtPassword becomeFirstResponder];
    }
    else if (textField==_txtPassword)
    {
        [_txtPassword resignFirstResponder];
    }

    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    
    
    textField.leftView = leftView;
    
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;

    if(textField==_txtCompanyKey)
    {
        if (self.view.frame.origin.y==0.0) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 100.0), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        }
    }
    if(textField==_txtUserName)
    {
        if (self.view.frame.origin.y==0.0) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 100.0), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        }
    }
    if(textField==_txtPassword)
    {
        if (self.view.frame.origin.y==0.0) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 100.0), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        }
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (self.view.frame.origin.y==-100.0) {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 100.0), self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    }
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }

    if (textField.tag==1) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 200) ? NO : YES;
    }else if (textField.tag==2){
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 200) ? NO : YES;
    }else{
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 200) ? NO : YES;
    }
}

//============================================================================
//============================================================================
#pragma mark Login Method
//============================================================================
//============================================================================

-(void)goToDashBoard{
    
    //goToTask
    
    NSString *strBranchSysName = [NSString stringWithFormat:@"%@",[global strEmpBranchSysName]];
    
    if (strBranchSysName.length == 0) {
        
        [global displayAlertController:Alert :@"Please Re-login. Something went wrong" :self];
        
    } else {
        
        
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL iFGoToTaskView=[defs boolForKey:@"TaskNotifications"];
    if (iFGoToTaskView) {
        
        /*[defs setBool:NO forKey:@"TaskNotifications"];
        [defs synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        TaskViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TaskViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];*/
        
        WebService *objWebService =[[WebService alloc]init];
        [objWebService setDefaultCrmValue];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"CRM_New_iPad"
                                                                 bundle: nil];
        TaskVC_CRMNew_iPhone
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TaskVC_CRMNew_iPhone"];
        objByProductVC.strFromVC = @"DashBoardView";
        
        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
        [defs setObject:nil forKey:@"dictFilterTask"];
        [defs synchronize];
        
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }else{
        
//        AppointmentView *objAppointment=[[AppointmentView alloc]init];
//        [objAppointment SalesAutomationFetchAfterSyncingData];
        
        AppointmentVC *obj = [[AppointmentVC alloc]init];
        [obj salesAutomationFetchAfterSyncingData];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"CRM_New_iPad" bundle: nil];
        
      DashBoardNew_iPhoneVC  *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardNew_iPhoneVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    
}

-(void)goToChangePassword{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"CRM"
                                                             bundle: nil];
    ChangePasswordiPadNew
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChangePasswordiPadNew"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

-(NSString *)validationCheck
{
    NSString *errorMessage;
    if (_txtCompanyKey.text.length==0) {
        errorMessage = @"Please enter Company key";
    } else if (_txtUserName.text.length==0){
        errorMessage = @"Please enter UserName";
    } else if (_txtPassword.text.length==0){
        errorMessage = @"Please enter Password";
    }
    return errorMessage;
}

- (void)startValidating:(void (^)(NSString*))validatingFinished {
    NSString *errorMessage;
    if (_txtCompanyKey.text.length==0) {
        errorMessage = @"Please enter Company key";
    } else if (_txtUserName.text.length==0){
        errorMessage = @"Please enter UserName";
    } else if (_txtPassword.text.length==0){
        errorMessage = @"Please enter Password";
    }
    validatingFinished (errorMessage);
}

-(void)LoginMethod{
    
    NSString *errorMessage=[self validationCheck];
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
        [DejalBezelActivityView removeView];
    } else {
        
        if (self.view.frame.origin.y==-100.0) {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationBeginsFromCurrentState:YES];
            self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 100.0), self.view.frame.size.width, self.view.frame.size.height);
            [UIView commitAnimations];
        }
        [_txtCompanyKey resignFirstResponder];
        [_txtPassword resignFirstResponder];
        [_txtUserName resignFirstResponder];
        

        NSString *strTxtUserName=[_txtUserName.text stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceCharacterSet]];
        
        NSString *strTxtPassword=[_txtPassword.text stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceCharacterSet]];

        NSString *strTxtCompanyKey=[_txtCompanyKey.text stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceCharacterSet]];

        NSUserDefaults *defsUSername=[NSUserDefaults standardUserDefaults];
        [defsUSername setValue:strTxtUserName forKey:@"usernameChangePass"];
        [defsUSername setValue:strTxtCompanyKey forKey:@"CompanyKeyChangePass"];
        [defsUSername synchronize];
        
        NSString* strDeviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        NSString *DeviceVersion = [[UIDevice currentDevice] systemVersion];
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *DeviceName=[NSString stringWithCString:systemInfo.machine
                                                encoding:NSUTF8StringEncoding];
        
        NSLog(@"strDeviceId = %@, DeviceVersion = %@, DeviceName = %@,",strDeviceId,DeviceVersion,DeviceName);
        
   // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Logging In..."];
    NSArray *objects = [NSArray arrayWithObjects:strTxtCompanyKey,
    strTxtUserName,
    @"Employee",
    @"SalesProcess",
    strTxtPassword,
    @"false",
    @"Tablet",
    VersionNumber,
    VersionDate,
    strDeviceId,
    DeviceVersion,
    DeviceName,nil];
    
        // DeviceType  Phone Tablet
        
    NSArray *keys = [NSArray arrayWithObjects:@"CompanyKey",
                     @"EmailOrUserName",
                     @"LoginUserType",
                     @"ModuleSysName",
                     @"Password",
                     @"RememberMe",
                     @"DeviceType",
                     @"Version",
                     @"VersionDate",
                     @"DeviceId",
                     @"DeviceVersion",
                     @"DeviceName",nil];
    
    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
    {
       // Serialize the dictionary
       json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
       
       // If no errors, let's view the JSON
       if (json != nil && errorNew == nil)
       {
           jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
           NSLog(@"Login JSON: %@", jsonString);
       }
     }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlLogin];
        
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Logging In..."];

     //============================================================================
     //============================================================================

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrlPostMethod:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                  [DejalBezelActivityView removeView];
                 if (success)
                 {
                    // NSString *str=[NSString stringWithFormat:@"%@",response];
                    // UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Response" message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                   //  [alert show];
                      NSLog(@"Response on LoginMethod ========= %@",response);
                     [self AfterServerResponse:[self nestedDictionaryByReplacingNullsWithNil:response]];
                     
                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                     BOOL chkFirstLogin=[defs boolForKey:@"firstTimeLogin"];
                     if (chkFirstLogin==NO)
                     {
                         [self trackLocation];
                         [defs setBool:YES forKey:@"firstTimeLogin"];
                         [defs synchronize];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
        
    //============================================================================
    //============================================================================
  }
}

//============================================================================
//============================================================================
#pragma mark--- AfterServerResponse
//============================================================================
//============================================================================
-(void)checkifNotificationAllowed{
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]){ // Check it's iOS 8 and above
        UIUserNotificationSettings *grantedSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        
        if (grantedSettings.types == UIUserNotificationTypeNone) {
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:@"Notifications are not allowed.Please go to settings and allow"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      
                                      
                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                     if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                         NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                         [[UIApplication sharedApplication] openURL:url];
                                     } else {
                                         
                                         //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                         //                                     [alert show];
                                         
                                     }
                                     
                                 }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else if (grantedSettings.types & UIUserNotificationTypeSound & UIUserNotificationTypeAlert ){
            NSLog(@"Sound and alert permissions ");
        }
        else if (grantedSettings.types  & UIUserNotificationTypeAlert){
            NSLog(@"Alert Permission Granted");
        }
    }
}

-(void)AfterServerResponse :(NSDictionary *)ResponseDict{
    
   // [self performSelector:@selector(checkifNotificationAllowed) withObject:nil afterDelay:2.0];
    
    NSString *strTxtUserName=[_txtUserName.text stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceCharacterSet]];
    
    NSString *strTxtPassword=[_txtPassword.text stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceCharacterSet]];
    
    NSString *strTxtCompanyKey=[_txtCompanyKey.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];

    if (ResponseDict.count==0) {
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorServerResponse;
        [global AlertMethod:strTitle :strMsg];
    } else {
        NSString *strError =([[ResponseDict valueForKey:@"Error"] isKindOfClass:[NSNull class]]) ? @"" : [ResponseDict valueForKey:@"Error"];
        
        
        NSString *strErrorMessage =([[ResponseDict valueForKey:@"Message"] isKindOfClass:[NSNull class]]) ? @"" : [ResponseDict valueForKey:@"Message"];

        NSString *strException =([[ResponseDict valueForKey:@"ExceptionMessage"] isKindOfClass:[NSNull class]]) ? @"" : [ResponseDict valueForKey:@"ExceptionMessage"];
        
        if (strException.length>0 || [strErrorMessage isEqualToString:@"An error has occurred."]) {
            
            NSString *strTitle = Alert;
            NSString *strMsg = [strErrorMessage isEqualToString:@"An error has occurred."] ? strErrorMessage : Sorry;
            [global AlertMethod:strTitle :strMsg];
            
        }else {

            if (strError.length==0) {
                                
                //EmployeePhoto
                //============================================================================
                //============================================================================
                
                NSString *strEmployeePhoto =([[ResponseDict valueForKey:@"EmployeePhoto"] isKindOfClass:[NSNull class]]) ? @"" : [ResponseDict valueForKey:@"EmployeePhoto"];
                // [self checkForProfileImage :strEmployeePhoto];
                
                //============================================================================
                //============================================================================
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                
                //Change For Replacing Response for username
                NSMutableDictionary *dictRespo = [NSMutableDictionary new];
                dictRespo = [ResponseDict mutableCopy];
                NSMutableDictionary *dictCompany = [NSMutableDictionary new];
                dictCompany = [[ResponseDict valueForKey:@"Company"] mutableCopy];
                [dictCompany setValue:[ResponseDict valueForKey:@"LoggedInUserName"] forKey:@"Username"];
                [dictRespo setValue:dictCompany forKey:@"Company"];
                ResponseDict=dictRespo;
                //End Change
                
                [defs setObject:ResponseDict forKey:@"LoginDetails"];
                [defs setObject:ResponseDict forKey:@"LoginDetailsOrignal"];
                [defs setBool:YES forKey:@"YesLoggedIn"];
                [defs setValue:strEmployeePhoto forKey:@"EmployeePhoto"];
                // [defs setValue:ResponseDict forKey:@"LoginData"];
                [defs setValue:strTxtCompanyKey forKey:@"companyKey"];
                [defs setValue:strTxtUserName forKey:@"username"];
                [defs setValue:strTxtPassword forKey:@"password"];
                [defs synchronize];
                
                [self logUser];

                [self callMethodToUpdateFirebaseToken];
                [self setDashboardWidgets];
                [self goToDashBoard];
            } else if([strError isEqualToString:@"Move to Change password Screen"]){
                //============================================================================
                //============================================================================
                
                NSString *strEmployeePhoto =([[ResponseDict valueForKey:@"EmployeePhoto"] isKindOfClass:[NSNull class]]) ? @"" : [ResponseDict valueForKey:@"EmployeePhoto"];
                //[self checkForProfileImage :strEmployeePhoto];
                
                //============================================================================
                //============================================================================
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setBool:YES forKey:@"YesChangePassword"];
                
                /*
                //Change For Replacing Response for username
                NSMutableDictionary *dictRespo = [NSMutableDictionary new];
                dictRespo = [ResponseDict mutableCopy];
                NSMutableDictionary *dictCompany = [NSMutableDictionary new];
                dictCompany = [[ResponseDict valueForKey:@"Company"] mutableCopy];
                [dictCompany setValue:[ResponseDict valueForKey:@"LoggedInUserName"] forKey:@"Username"];
                [dictRespo setValue:dictCompany forKey:@"Company"];
                ResponseDict=dictRespo;
                //End Change
                 */
                
                [defs setObject:ResponseDict forKey:@"LoginDetails"];
                [defs setObject:ResponseDict forKey:@"LoginDetailsOrignal"];
                [defs setValue:strEmployeePhoto forKey:@"EmployeePhoto"];
                [defs setValue:strTxtPassword forKey:@"passwordForMatch"];
                //[defs setValue:ResponseDict forKey:@"LoginData"];
                [defs synchronize];
                
                // [self callMethodToUpdateFirebaseToken];
                [self setDashboardWidgets];
                [self goToChangePassword];
            }
            else if([strError isEqualToString:@"Invalid login attempt."]){
                NSString *strTitle = Alert;
                NSString *strMsg = InvalidLogin;
                [global AlertMethod:strTitle :strMsg];
            }
            
        }
        
    }
}


-(void)trackLocation
{
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    if (dictLoginData.count==0) {
        
        
        
    } else {
        NSString *strError =([[dictLoginData valueForKey:@"Error"] isKindOfClass:[NSNull class]]) ? @"" : [dictLoginData valueForKey:@"Error"];
        
        NSString *strException =([[dictLoginData valueForKey:@"ExceptionMessage"] isKindOfClass:[NSNull class]]) ? @"" : [dictLoginData valueForKey:@"ExceptionMessage"];
        
        if (strException.length>0) {
            
            
            
        } else {
            
            if (strError.length==0) {
                
                NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
                NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
                
                NSString* strDeviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
                NSLog(@"output is : %@", strDeviceId);
                Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
                NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
                if (netStatusWify1== NotReachable)
                {
                    // [global AlertMethod:@"ALert" :ErrorInternetMsg];
                    [DejalActivityView removeView];
                }
                else
                {
                    NSString *strUrl;
                    
                    strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,@"api/EmployeeCurrentLocation/AddEmployeeCurrentLocation"];
                    
                    CLLocationCoordinate2D coordinate = [global getLocation] ;
                    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
                    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
                    
                    
                    NSArray *key,*value;
                    key=[NSArray arrayWithObjects:
                         @"EmployeeNo",
                         @"DeviceId",
                         @"Latitude",
                         @"Longitude",
                         @"Speed",
                         @"IP",
                         @"CreatedDate",
                         @"CompanyId",
                         nil];
                    value=[NSArray arrayWithObjects:
                           strEmployeeNumber,
                           strDeviceId,
                           latitude,
                           longitude,
                           @"0",
                           [global getIPAddress],
                           [global strCurrentDate],
                           strCoreCompanyId,
                           nil];
                    
                    NSDictionary *userLocation=@{@"lat":[NSString stringWithFormat:@"%@",latitude],@"long":[NSString stringWithFormat:@"%@",longitude],@"time":[global strCurrentDate]};
                    [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:@"userOldLocation"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
                    
                    NSError *errorr;
                    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
                    
                    NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
                    
                    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
                    
                    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
                    
                    [request setHTTPMethod:@"POST"];
                    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
                    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                    
                    //Nilind 1 Feb
                    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
                    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
                    //ENd
                    
                    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
                    [request setHTTPBody: requestData];
                    @try {
                        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
                         {
                             NSData* jsonData = [NSData dataWithData:data];
                             
                             NSString* myString;
                             myString = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
                             
                             /* NSDictionary*  ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                              */
                             //[global AlertMethod:Alert :[NSString stringWithFormat:@"Input = %@ Response = %@",dict_ToSend,myString]];
                             NSLog(@"Input = %@  Location Tracking Response  = = = =  = %@",dict_ToSend,myString);
                         }];
                    }
                    @catch (NSException *exception) {
                        //[global AlertMethod:Alert :Sorry];
                    }
                    @finally {
                    }
                }
            }
        }
    }
}

//============================================================================
//============================================================================
#pragma mark--- Download Profile Pic Methods
//============================================================================
//============================================================================

-(void)checkForProfileImage :(NSString*)strImageName{
    NSUserDefaults *defsPhoto=[NSUserDefaults standardUserDefaults];
    NSString *strSavedPhoto=[defsPhoto valueForKey:@"EmployeePhoto"];
    
    if ([strSavedPhoto isEqualToString:strImageName]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",@"Saavan.png"]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (!fileExists) {
            [self DownloadProfileImageInBackground:strImageName];
        }
    } else {
        [self DownloadProfileImageInBackground:strImageName];
    }
}

-(void)DownloadProfileImageInBackground :(NSString*)strImageName{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        [self downloadImage:strImageName];
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    });
}
-(void)downloadImage :(NSString*)strImageName
{
   // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
   // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
   // NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];

    NSString *temp_Image_String = [[NSString alloc] initWithFormat:@"%@",strImageName];
    NSURL *url_For_Ad_Image = [[NSURL alloc] initWithString:temp_Image_String];
    NSData *data_For_Ad_Image = [[NSData alloc] initWithContentsOfURL:url_For_Ad_Image];
    UIImage *temp_Ad_Image = [[UIImage alloc] initWithData:data_For_Ad_Image];
    [self saveImage:temp_Ad_Image :strImageName];
}

- (void)saveImage: (UIImage*)image :(NSString*)strImageName {
    [self removeImage:@"Saavan.png"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",@"Saavan.png"]];
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}
- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

//============================================================================
//============================================================================
#pragma mark--- -----------------Null Conversion-------------------
//============================================================================
//============================================================================

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}
-(void)callMethodToUpdateFirebaseToken
{
    // call api to post token to server
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[userDefaults valueForKey:@"LoginDetails"];
    NSString *strEmpNo = [NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    NSString *strCompanyId = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.CompanyId"]];
    NSString *strTokenId = [userDefaults valueForKey:@"FirebaseToken"];
    if (strTokenId.length==0) {
        
        strTokenId=@"";
        
    }
    NSString *strCompanyKey = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSString *strServiceUrlMainSalesAutomation = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"]];
    
    NSDictionary *userDictionary = @{@"EmployeeNo":[NSString stringWithFormat:@"%@",strEmpNo],
                                     @"CompanyId":[NSString stringWithFormat:@"%@",strCompanyId],
                                     @"TokenId":strTokenId,
                                     @"CompanyKey": strCompanyKey,
                                     };
    
    //UrlAddUpdateDeviceRegistration
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",strServiceUrlMainSalesAutomation,UrlAddUpdateDeviceRegistration];
    
   // NSString *strUrl = @"http://tcrms.stagingsoftware.com/api/LeadNowAppToSalesProcess/AddUpdateDeviceRegistration";
    
    NSData *json;
    NSError *errorNew;
    NSString *jsonString;
    
    if ([NSJSONSerialization isValidJSONObject:userDictionary]) {//validate it
        // Serialize the dictionary
        
        json = [NSJSONSerialization dataWithJSONObject:userDictionary options:NSJSONWritingPrettyPrinted error:&errorNew];
        // If no errors, let's view the JSON
        
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Login JSON: %@", jsonString);
        }
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSURL* url = [NSURL URLWithString:strUrl];
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
        
        [request setHTTPMethod:@"POST"];//use POST
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSString *strHrmsCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.HrmsCompanyId"]];
        
        NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
        
        NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeId"]];
        
        NSString *strCreatedBy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"CreatedBy"]];
        
        NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeName"]];
        
        NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
        
        NSString *strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
        
        NSString *strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
        
        [request addValue:strHrmsCompanyId forHTTPHeaderField:@"HrmsCompanyId"];
        
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"IpAddress"];
        
        [request addValue:strEmployeeNumber forHTTPHeaderField:@"EmployeeNumber"];
        
        [request addValue:strEmployeeId forHTTPHeaderField:@"EmployeeId"];
        
        [request addValue:strCreatedBy forHTTPHeaderField:@"CreatedBy"];
        
        [request addValue:strEmployeeName forHTTPHeaderField:@"EmployeeName"];
        
        [request addValue:strCoreCompanyId forHTTPHeaderField:@"CoreCompanyId"];
        
        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
        
        [request addValue:strCompanyKey forHTTPHeaderField:@"CompanyKey"];
        
        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-length"];
        
        [request setHTTPBody:jsonData];//set data
        //use async way to connect network
        
        [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
         {
             if ([data length]>0 && error == nil)
             {
                 NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 
                 NSLog(@"resultsDictionary is %@",string);
                 
             }
             else if ([data length]==0 && error ==nil)
             {
                 NSLog(@" download data is null");
                 
             }
             
             else if( error!=nil)
                 
             {
                 NSLog(@" error is %@",error);
             }
             
         }];
    }
}

-(void)forotPasswordAlertview{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Forgot Password"
                                                                              message: @"Please enter your Company Key & Email Or UserName. Password reset link will be sent on registered email"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Company Key";
        textField.textColor = [UIColor blueColor];
        textField.text = strForgotPassCompanykey;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Username";
        textField.textColor = [UIColor blueColor];
        textField.text = strForgotPassUserName;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtEmailId = textfields[1];
        UITextField * txtCompanyKey = textfields[0];
        NSString *strEmailIdLocal = [NSString stringWithFormat:@"%@",txtEmailId.text];
        NSString *strCompanyKeyLocal = [NSString stringWithFormat:@"%@",txtCompanyKey.text];
        strForgotPassUserName = strEmailIdLocal;
        strForgotPassCompanykey = strCompanyKeyLocal;
        
        if ((strEmailIdLocal.length>0) && (strCompanyKeyLocal.length>0)) {
            
            [self callForGotPassAPI:strEmailIdLocal :strCompanyKeyLocal];
            
        }else{
            
            [self performSelector:@selector(blankAlertForgotPass) withObject:nil afterDelay:0.2];
            
        }
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)callForGotPassAPI :(NSString*)strEmailId :(NSString*)strCompanykeyLocal{
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,URLForgotPass];
    
    
    NSArray *objects = [NSArray arrayWithObjects:strCompanykeyLocal,
                        @"Employee",
                        strEmailId,
                        nil];
    
    NSArray *keys = [NSArray arrayWithObjects:@"CompanyKey",
                     @"LoginUserType",
                     @"EmailOrUserName",
                     nil];
    
    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSError *errorr;
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
    
    NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending Email...."];
    
    //============================================================================
    //============================================================================
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60000.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"" forHTTPHeaderField:@"BranchId"];
    [request addValue:@"" forHTTPHeaderField:@"BranchSysName"];
    [request addValue:@"true" forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:@"" forHTTPHeaderField:@"ClientTimeZone"];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    //ENd
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
     {
         
         [DejalActivityView removeView];
         [DejalBezelActivityView removeView];
         
         NSData* jsonData = [NSData dataWithData:data];
         
         NSDictionary  *ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
         
         if (ResponseDict.count>0) {
             
             BOOL isSuccess = [[ResponseDict valueForKey:@"IsSucceeded"] boolValue];
             
             NSArray *arrayMsg = [ResponseDict valueForKey:@"Errors"];
             
             NSString *strMsgs = [arrayMsg componentsJoinedByString:@","];
             
             if (isSuccess) {
                 
                 strForgotPassCompanykey = @"";
                 strForgotPassUserName = @"";
                 
                 [global displayAlertController:Info :strMsgs :self];
                 
             } else {
                 
                 UIAlertController *alert= [UIAlertController
                                            alertControllerWithTitle:Alert
                                            message:strMsgs
                                            preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action)
                                       {
                                           
                                           [self forotPasswordAlertview];
                                           
                                       }];
                 [alert addAction:yes];
                 [self presentViewController:alert animated:YES completion:nil];
                 
             }
             
         } else {
             
             [global displayAlertController:Alert :Sorry :self];
             
         }
         
         
     }];
    
}

-(void)blankAlertForgotPass{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:Alert
                               message:@"Please enter Company Key and Email Or UserName"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self forotPasswordAlertview];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)setDashboardWidgets
{
    NSArray *arrData = [[NSArray alloc]init];
    arrData = [[NSUserDefaults standardUserDefaults] valueForKey:@"DashboardWidgets"];
    if (!(arrData.count > 0))
    {
        NSDictionary *dict = [[NSUserDefaults standardUserDefaults] valueForKey:@"LoginDetails"];
        NSArray *arrDashboardWidgets = [ dict valueForKey:@"DashboardWidgetMasters"];
        
        NSMutableArray *arr = [[NSMutableArray alloc]init];
        for (int i = 0; i< arrDashboardWidgets.count ; i++)
        {
            NSDictionary *dict = [arrDashboardWidgets objectAtIndex:i];
            [arr addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]]];
        }
        [[NSUserDefaults standardUserDefaults]setObject:arr forKey:@"DashboardWidgets"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

@end
