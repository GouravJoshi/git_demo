//
//  WeeklyTimeSheetViewControlleriPad.h
//  DPS peSTream Quacito.com
//  peSTream Quacito.
//  Created by Saavan Patidar on 20/11/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeeklyTimeSheetViewControlleriPad : UIViewController<UITextFieldDelegate>

- (IBAction)action_Back:(id)sender;
- (IBAction)action_SeelctStartDate:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnStartDate;
- (IBAction)action_SelectEndDate:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEndDate;
- (IBAction)action_Search:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@end
