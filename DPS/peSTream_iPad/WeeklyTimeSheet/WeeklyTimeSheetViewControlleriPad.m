//
//  WeeklyTimeSheetViewControlleriPad.m
//  DPS
//
//  Created by Saavan Patidar on 20/11/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "WeeklyTimeSheetViewControlleriPad.h"
#import "AllImportsViewController.h"

@interface WeeklyTimeSheetViewControlleriPad ()
{
    Global *global;
    NSString *strEmpID,*strUserName,*strCompanyKey,*strEmpName,*strGlobalDateToShow,*strCompanyName,*strStarDate,*strEndDate;
    BOOL isStartDate;
    UIView *viewBackGround,*viewBackGroundOnView,*viewForDate;
    UITableView *tblData;
    UIDatePicker *pickerDate;
    NSDate *dateStartGlobal,*dateEndGlobal;
    float heightGlobalForTotal,heightGlobalForOther,heightGlobalScrollView;
}
@end

@implementation WeeklyTimeSheetViewControlleriPad

- (void)viewDidLoad {
    
    global = [[Global alloc] init];
    
    isStartDate=YES;
    
    strStarDate = @"Select Start Date";
    strEndDate = @"Select End Date";
    
    heightGlobalForTotal = 50;
    heightGlobalForOther = 50;
    heightGlobalScrollView = 0.0;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)action_Back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)action_SeelctStartDate:(id)sender {
    
    isStartDate=YES;
    
    [self addPickerViewDateTo];
    
}
- (IBAction)action_SelectEndDate:(id)sender {
    
    isStartDate=NO;
    
    [self addPickerViewDateTo];
    
}
- (IBAction)action_Search:(id)sender {
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [global displayAlertController:Alert :ErrorInternetMsg :self];
        
    }else{
        
        if ([strStarDate isEqualToString:@"Select Start Date"]) {
            
            [global displayAlertController:Alert :@"Please select start date" :self];
            
        }else if ([strEndDate isEqualToString:@"Select End Date"]) {
            
            [global displayAlertController:Alert :@"Please select end date" :self];
            
        }else{
            
            [self fetchWeeklyTimeSheet:@"Fetching Time Sheet..."];
            
        }
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //  [pickerDate setMinimumDate:[NSDate date]];
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGroundOnView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundOnView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGroundOnView];
    
    //============================================================================
    //============================================================================
    
    //   UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    //  singleTap1.numberOfTapsRequired = 1;
    //  [viewBackGround setUserInteractionEnabled:YES];
    //  [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGroundOnView addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    // btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGroundOnView removeFromSuperview];
}

-(void)setDateOnDone:(id)sender
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
    
    if (isStartDate) {
        
        if ([strEndDate isEqualToString:@"Select End Date"]) {
            
            dateStartGlobal = pickerDate.date;
            
            [_btnStartDate setTitle:strDate forState:UIControlStateNormal];
            strStarDate = strDate;
            
            NSDate *newDate1 = [pickerDate.date dateByAddingTimeInterval:60*60*24*6];
            NSString  *strDateLocal = [dateFormat stringFromDate:newDate1];
            dateEndGlobal = newDate1;
            [_btnEndDate setTitle:strDateLocal forState:UIControlStateNormal];
            strEndDate = strDateLocal;
            
        } else {
            
            dateStartGlobal = pickerDate.date;
            
            [_btnStartDate setTitle:strDate forState:UIControlStateNormal];
            strStarDate = strDate;
            
            NSDate *newDate1 = [pickerDate.date dateByAddingTimeInterval:60*60*24*6];
            NSString  *strDateLocal = [dateFormat stringFromDate:newDate1];
            dateEndGlobal = newDate1;
            [_btnEndDate setTitle:strDateLocal forState:UIControlStateNormal];
            strEndDate = strDateLocal;
            
            /*
             NSDate *endDate = [dateFormat dateFromString:strEndDate];
             
             NSTimeInterval secondsBetween = [pickerDate.date timeIntervalSinceDate:endDate];
             
             int numberOfDays = secondsBetween / 86400;
             
             if (numberOfDays==6) {
             
             dateStartGlobal = pickerDate.date;
             
             [_btnStartDate setTitle:strDate forState:UIControlStateNormal];
             strStarDate = strDate;
             
             NSDate *newDate1 = [pickerDate.date dateByAddingTimeInterval:60*60*24*6];
             NSString  *strDateLocal = [dateFormat stringFromDate:newDate1];
             dateEndGlobal = newDate1;
             [_btnEndDate setTitle:strDateLocal forState:UIControlStateNormal];
             strEndDate = strDateLocal;
             
             } else {
             
             [global displayAlertController:Alert :@"Please select date within 7 days" :self];
             
             [_btnStartDate setTitle:@"Select Start Date" forState:UIControlStateNormal];
             strStarDate = @"Select Start Date";
             
             [_btnEndDate setTitle:@"Select End Date" forState:UIControlStateNormal];
             strEndDate = @"Select End Date";
             }
             
             */
            
        }
        
        [viewForDate removeFromSuperview];
        [viewBackGroundOnView removeFromSuperview];
        
    } else {
        
        if ([strStarDate isEqualToString:@"Select Start Date"]) {
            
            dateEndGlobal = pickerDate.date;
            
            [_btnEndDate setTitle:strDate forState:UIControlStateNormal];
            strEndDate = strDate;
            
            //            NSDate *newDate1 = [pickerDate.date dateByAddingTimeInterval:60*60*24*6];
            //            NSString  *strDateLocal = [dateFormat stringFromDate:newDate1];
            //            [_btnEndDate setTitle:strDateLocal forState:UIControlStateNormal];
            //            strEndDate = strDateLocal;
            
            [viewForDate removeFromSuperview];
            [viewBackGroundOnView removeFromSuperview];
            
        } else {
            
            NSDate *endDate = [dateFormat dateFromString:strStarDate];
            
            NSTimeInterval secondsBetween = [pickerDate.date timeIntervalSinceDate:endDate];
            
            int numberOfDays = secondsBetween / 86400;
            
            if (numberOfDays==6) {
                
                dateEndGlobal = pickerDate.date;
                
                [_btnEndDate setTitle:strDate forState:UIControlStateNormal];
                strEndDate = strDate;
                
                [viewForDate removeFromSuperview];
                [viewBackGroundOnView removeFromSuperview];
                
            } else {
                
                [global displayAlertController:Alert :@"Please select date within 7 days" :self];
                
                //                [_btnStartDate setTitle:@"Select Start Date" forState:UIControlStateNormal];
                //                strStarDate = @"Select Start Date";
                
                [_btnEndDate setTitle:@"Select End Date" forState:UIControlStateNormal];
                strEndDate = @"Select End Date";
                
            }
            
        }
        
    }
    //    [viewForDate removeFromSuperview];
    //    [viewBackGroundOnView removeFromSuperview];
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}


-(void)fetchWeeklyTimeSheet :(NSString*)strLoaderText{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpName        =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    NSString *strEmpNoLoggedIn =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    //NSString *strLoggedInUserName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"LoggedInUserName"]];
    
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString  *strDateStartToSend = [dateFormat stringFromDate:dateStartGlobal];
    NSString  *strDateEndToSend = [dateFormat stringFromDate:dateEndGlobal];
    
    //strStarDate = @"2018-09-20";
    //strEndDate = @"2018-09-26";
    //strEmpNoLoggedIn = @"123";
    
    
    strStarDate = strDateStartToSend;
    strEndDate = strDateEndToSend;
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@?CompanyKey=%@&StartDate=%@&EndDate=%@&EmployeeNo=%@",strServiceUrlMainServiceAutomation,UrlGetEmployeeWorkingTime,strCompanyKey,strStarDate,strEndDate,strEmpNoLoggedIn];
    
    //strUrl = @"http://tsrs.stagingsoftware.com/api/MobileToServiceAuto/GetEmployeeWorkingTime?CompanyKey=Automation&StartDate=2018-09-20&EndDate=2018-09-26&EmployeeNo=123";
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:strLoaderText];
    
    
    NSString *strType=@"getWeeklyTimeSheet";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 
                 if (success)
                 {
                     if (response.count==0) {
                         
                         [global displayAlertController:Alert :NoDataAvailableee :self];
                         [DejalBezelActivityView removeView];
                         
                         for(UIView *view in _scrollView.subviews)
                         {
                             [view removeFromSuperview];
                         }
                         
                     } else {
                         
                         NSArray *arrTempKeys = (NSArray*)response;
                         
                                    /*
                                     NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
                                     NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"TestingJson.json"];
                                     NSError * error;
                                     NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
                                     NSDictionary *tempDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                                    arrTempKeys = (NSArray*)tempDict;
                                    */
                         
                         if ((arrTempKeys.count>0) && [arrTempKeys isKindOfClass:[NSArray class]]) {
                             
                             NSMutableArray *arrTotal = [[NSMutableArray alloc]init];
                             NSMutableArray *arrOfWorkOrderNo = [[NSMutableArray alloc]init];
                             NSMutableArray *arrOfDays = [[NSMutableArray alloc]init];
                             
                             // Response aaya yaha pr for time sheet
                             
                             for (int k=0; k<arrTempKeys.count; k++) {
                                 
                                 NSDictionary *dictData = arrTempKeys[k];
                                 
                                 NSString *strTotal = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"WorkOrderNo"]];
                                 
                                 if (strTotal.length==0) {
                                     
                                     strTotal = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Description"]];
                                     
                                 }
                                 
                                 if ([strTotal isEqualToString:@"Total"]) {
                                     
                                     [arrTotal addObject:dictData];
                                     
                                 }else{
                                     
                                     [arrOfWorkOrderNo addObject:strTotal];
                                     
                                 }
                                 
                             }
                             
                             NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrOfWorkOrderNo];
                             NSArray *arrayWithoutDuplicatesWorkOrderNo = [orderedSet array];
                             
                             NSString *strStartDateLocal = strStarDate;
                             
                             [arrOfDays addObject:strStartDateLocal];
                             
                             for (int k1=0; k1<6; k1++) {
                                 
                                 NSDate *startDateLocal = [dateFormat dateFromString:strStartDateLocal];
                                 NSDate *newDate1 = [startDateLocal dateByAddingTimeInterval:60*60*24*1];
                                 NSString  *strDateLocal = [dateFormat stringFromDate:newDate1];
                                 [arrOfDays addObject:strDateLocal];
                                 strStartDateLocal = strDateLocal;
                                 
                             }
                             
                             NSLog(@"arrodays=====%@",arrOfDays);
                             
                             [self createDynamicEmpSheet:arrayWithoutDuplicatesWorkOrderNo :arrOfDays :arrTempKeys :arrTotal];
                             
                         } else {
                             
                             [global displayAlertController:Alert :NoDataAvailableee :self];
                             [DejalBezelActivityView removeView];
                             
                             for(UIView *view in _scrollView.subviews)
                             {
                                 [view removeFromSuperview];
                             }
                             
                         }
                         
                     }
                 }
                 else
                 {
                     [global displayAlertController:Alert :NoDataAvailableee :self];
                     [DejalBezelActivityView removeView];
                     
                     for(UIView *view in _scrollView.subviews)
                     {
                         [view removeFromSuperview];
                     }
                     
                 }
             });
         }];
    });
    
}

- (CGFloat)heightForAttributedString:(NSAttributedString *)text maxWidth:(CGFloat)maxWidth {
    if ([text isKindOfClass:[NSString class]] && !text.length) {
        // no text means no height
        return 0;
    }
    
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    CGSize size = [text boundingRectWithSize:CGSizeMake(maxWidth, CGFLOAT_MAX) options:options context:nil].size;
    
    CGFloat height = ceilf(size.height) + 1; // add 1 point as padding
    
    return height;
}

- (CGFloat)heightForString:(NSString *)text font:(UIFont *)font maxWidth:(CGFloat)maxWidth {
    if (![text isKindOfClass:[NSString class]] || !text.length) {
        // no text means no height
        return 0;
    }
    
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    NSDictionary *attributes = @{ NSFontAttributeName : font };
    CGSize size = [text boundingRectWithSize:CGSizeMake(maxWidth, CGFLOAT_MAX) options:options attributes:attributes context:nil].size;
    CGFloat height = ceilf(size.height) + 1; // add 1 point as padding
    
    return height;
    
}
-(void)createDynamicEmpSheet :(NSArray*)arrOfEmpSheet :(NSArray*)arrOfHeaderTitle :(NSArray*)arrOfAllData :(NSArray*)arrTotal{
    
    heightGlobalScrollView = 0.0;
    [_scrollView setContentSize:CGSizeMake(0,0)];

    NSMutableArray *arrAllWorkingTime = [[NSMutableArray alloc]init];
    NSMutableArray *arrAllWorkingTimeHeader = [[NSMutableArray alloc]init];
    
    // getting global height for TOTAL fields
    
    for (int k3=0; k3<arrTotal.count; k3++) {
        
        NSDictionary *dictDataaaa = arrTotal[k3];
        
        UILabel *txtView = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,200,50)];
        
        txtView.text = [NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"WorkingTime"]];
        
        NSArray *items = [[NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"WorkingTime"]] componentsSeparatedByString:@"\n"];
        
        CGFloat heightTempp = 0.0;
        
        for (int p1=0; p1<items.count; p1++) {
            
            CGFloat heightTemppLocal = [self heightForString:[NSString stringWithFormat:@"%@",items[p1]] font:txtView.font maxWidth:200];
            
            if (heightTemppLocal<50) {
                
                heightTemppLocal = 50;
                
            }
            
            heightTempp = heightTemppLocal+heightTempp;
            
        }
        
        
        if (heightTempp<50) {
            
            if (heightGlobalForTotal<heightTempp) {
                heightGlobalForTotal = heightTempp;
            }
            //heightGlobalScrollView = heightGlobalForTotal+heightGlobalScrollView;
            
        } else {
            
            if (heightGlobalForTotal<heightTempp) {
                heightGlobalForTotal = heightTempp;
            }
            //heightGlobalScrollView = heightGlobalForTotal+heightGlobalScrollView;
            
        }
        
        
    }
    
    heightGlobalScrollView = heightGlobalForTotal+heightGlobalScrollView;
    
    for(UIView *view in _scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    CGFloat scrollViewHeight=0.0;
    CGFloat scrollViewWidth=0.0;
    
    scrollViewHeight=arrOfEmpSheet.count*60+60;
    scrollViewWidth=arrOfHeaderTitle.count*200+200;
    
    //_scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, scrollViewWidth, scrollViewHeight)];
    
    UIView *viewEmpSheetOnScroll=[[UIView alloc]initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, _scrollView.frame.size.height)];
    
    CGFloat xAxisHeaderTitle=10;
    
    for (int i=0; i<=arrOfHeaderTitle.count; i++) {
        
        UILabel *lblTitleHeader=[[UILabel alloc]init];
        lblTitleHeader.backgroundColor=[UIColor lightTextColor];
        lblTitleHeader.layer.borderWidth = 1.5f;
        lblTitleHeader.layer.cornerRadius = 0.0f;
        lblTitleHeader.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        if (i==0) {
            
            lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, 0, 200, 100);
            xAxisHeaderTitle=xAxisHeaderTitle+200;
            
            
        }else{
            
            lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, 0, 200, 100);
            xAxisHeaderTitle=xAxisHeaderTitle+200;
            
            
        }
        //lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, yAxisMain+60, 100, 50);
        lblTitleHeader.font=[UIFont systemFontOfSize:22];
        lblTitleHeader.backgroundColor=[UIColor lightTextColorTimeSheet];
        if (i==0) {
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"WO #"];
            
        }else {
            
            NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
            [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
            NSDate *endDate = [dateFormat1 dateFromString:arrOfHeaderTitle[i-1]];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"EEEE"];
            NSString  *strDate = [dateFormat stringFromDate:endDate];
            
            NSString *strDateTitle = [global convertDate:arrOfHeaderTitle[i-1]];
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@\n%@",strDate,strDateTitle];
            
        }
        
        lblTitleHeader.textAlignment=NSTextAlignmentCenter;
        lblTitleHeader.numberOfLines=700;
        [lblTitleHeader setAdjustsFontSizeToFitWidth:YES];
        
        [viewEmpSheetOnScroll addSubview:lblTitleHeader];
        
    }
    
    CGFloat yAxis=100;
    
    for (int k=0; k<=arrOfEmpSheet.count; k++) {
        
        CGFloat xAxis=210;
        
        UILabel *lblTitleEmpName=[[UILabel alloc]init];
        lblTitleEmpName.backgroundColor=[UIColor clearColor];
        lblTitleEmpName.layer.borderWidth = 1.5f;
        lblTitleEmpName.layer.cornerRadius = 0.0f;
        lblTitleEmpName.layer.borderColor = [[UIColor grayColor] CGColor];
        lblTitleEmpName.font=[UIFont systemFontOfSize:22];
        
        // This Logic is for Showing Total At Last
        
        if (arrOfEmpSheet.count==k) {
            
            lblTitleEmpName.backgroundColor=[UIColor lightTextColor];
            
            lblTitleEmpName.text=[NSString stringWithFormat:@"%@",@"Total"];
            
            lblTitleEmpName.frame=CGRectMake(10, yAxis, 200, heightGlobalForTotal);
            
            lblTitleEmpName.backgroundColor=[UIColor lightTextColorTimeSheet];
            
            
        } else {
            
            lblTitleEmpName.text=[NSString stringWithFormat:@"%@",arrOfEmpSheet[k]];
            
            //lblTitleEmpName.frame=CGRectMake(10, yAxis, 200, heightGlobalForOther);
            
            lblTitleEmpName.backgroundColor=[UIColor clearColor];
            
        }
        
        //lblTitleEmpName.text=[NSString stringWithFormat:@"%@",arrOfEmpSheet[k]];
        lblTitleEmpName.textAlignment=NSTextAlignmentCenter;
        lblTitleEmpName.numberOfLines=700;
        [lblTitleEmpName setAdjustsFontSizeToFitWidth:YES];
        [viewEmpSheetOnScroll addSubview:lblTitleEmpName];
        
        CGSize heightLable;
        
        // This Logic is for Showing Total At Last shdfjsd
        if (arrOfEmpSheet.count==k) {
            
            for (int k1=0; k1<7; k1++) {
                
                CGRect txtViewFrame = CGRectMake(xAxis, yAxis,200,50);
                UILabel *txtView = [[UILabel alloc]initWithFrame:CGRectMake(xAxis, yAxis,200,heightGlobalForTotal)];
                txtView.numberOfLines=10000;
                
                if (arrTotal.count>k1) {
                    
                    NSDictionary *dictDataaaa=arrTotal[k1];
                    txtView.text = [NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"WorkingTime"]];
                    
                    NSLog(@"%@",[[dictDataaaa valueForKey:@"WorkingTime"] componentsSeparatedByString:@"\n"]);
                    
                    NSArray *arrTemp = [[dictDataaaa valueForKey:@"WorkingTime"] componentsSeparatedByString:@"\n"];
                    
                    for (int s=0; s<arrTemp.count; s++) {
                        
                        [arrAllWorkingTime addObject:arrTemp[s]];
                        
                        NSArray *arrTempHeader = [arrTemp[s] componentsSeparatedByString:@"-" ];
                        
                        if (arrTempHeader.count>0) {
                            
                            if (arrTempHeader.count==2) {
                                
                                [arrAllWorkingTimeHeader addObject:arrTempHeader[0]];
                                
                            }else{
                                
                                NSString *str = [NSString stringWithFormat:@"%@ - %@",arrTempHeader[0],arrTempHeader[1]];
                                
                                [arrAllWorkingTimeHeader addObject:str];
                                
                            }
                            
                            //[arrAllWorkingTimeHeader addObject:arrTempHeader[0]];
                            
                        }
                        
                        //[arrAllWorkingTimeHeader addObject:arrTempHeader[0]];
                        
                    }
                    
                } else {
                    
                    txtView.text = [NSString stringWithFormat:@"%@",@""];
                    
                }
                
                txtViewFrame = CGRectMake(xAxis, yAxis,200,heightGlobalForTotal);
                
                txtView.frame =txtViewFrame;
                txtView.layer.borderWidth = 1.5f;
                txtView.layer.cornerRadius = 0.0f;
                txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                txtView.font=[UIFont systemFontOfSize:20];
                //txtView.delegate=self;
                //[txtView setEnabled:NO];
                UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                leftView.backgroundColor = [UIColor lightTextColor];
                //txtView.leftView = leftView;
                //txtView.leftViewMode = UITextFieldViewModeAlways;
                //txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtView.textAlignment=NSTextAlignmentCenter;
                txtView.tag=k*1000+k1+500;
                txtView.backgroundColor = [UIColor lightTextColorTimeSheet];
                [viewEmpSheetOnScroll addSubview:txtView];
                txtView.numberOfLines=10000;
                xAxis=xAxis+200;
                
            }
            
            if (heightLable.height<50) {
                
                yAxis=yAxis+heightGlobalForTotal;
                
                
            } else {
                
                yAxis=yAxis+heightGlobalForTotal;
                
            }
            
        } else {
            
            NSArray *arrEmpData=[self checkSameLocation:arrOfAllData :arrOfEmpSheet[k]];
            
            heightGlobalForOther = 50;
            
            for (int k3=0; k3<arrEmpData.count; k3++) {
                
                NSDictionary *dictDataaaa = arrEmpData[k3];
                
                UILabel *txtView = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,200,50)];
                
                txtView.text = [NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"WorkingTime"]];
                
                NSArray *items = [[NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"WorkingTime"]] componentsSeparatedByString:@"\n"];
                
                CGFloat heightTempp = 0.0;
                
                for (int p1=0; p1<items.count; p1++) {
                    
                    CGFloat heightTemppLocal = [self heightForString:[NSString stringWithFormat:@"%@",items[p1]] font:txtView.font maxWidth:200];
                    
                    if (heightTemppLocal<50) {
                        
                        heightTemppLocal = 50;
                        
                    }
                    
                    heightTempp = heightTemppLocal+heightTempp;
                    
                }
                
                
                if (heightTempp<50) {
                    
                    if (heightGlobalForOther<heightTempp) {
                        heightGlobalForOther = heightTempp;
                    }
                    // heightGlobalScrollView = heightGlobalScrollView+heightGlobalForOther;
                    
                } else {
                    
                    if (heightGlobalForOther<heightTempp) {
                        heightGlobalForOther = heightTempp;
                    }
                    //heightGlobalScrollView = heightGlobalScrollView+heightGlobalForOther;
                    
                }
                
            }
            
            heightGlobalScrollView = heightGlobalScrollView+heightGlobalForOther;
            
            lblTitleEmpName.text=[NSString stringWithFormat:@"%@",arrOfEmpSheet[k]];
            
            lblTitleEmpName.frame=CGRectMake(10, yAxis, 200, heightGlobalForOther);
            
            lblTitleEmpName.backgroundColor=[UIColor clearColor];
            
            for (int k1=0; k1<7; k1++) {
                
                CGRect txtViewFrame = CGRectMake(xAxis, yAxis,200,heightGlobalForOther);
                UILabel *txtView = [[UILabel alloc]initWithFrame:CGRectMake(xAxis, yAxis,200,heightGlobalForOther)];
                txtView.numberOfLines=10000;
                
                
                for (int p=0; p<arrEmpData.count; p++) {
                    
                    NSDictionary *dictDataaaa=arrEmpData[p];
                    
                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                    NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"WorkingDate"]]];
                    //Add the following line to display the time in the local time zone
                    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
                    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                    NSString* finalTime = [dateFormatter stringFromDate:newTime];
                    
                    if ([finalTime isEqualToString:arrOfHeaderTitle[k1]]) {
                        
                        txtView.text = [NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"WorkingTime"]];
                        break;
                    } else {
                        
                        txtView.text = [NSString stringWithFormat:@"%@",@""];
                        
                    }
                    
                }
                
                /*
                 if (arrEmpData.count>k1) {
                 
                 NSDictionary *dictDataaaa=arrEmpData[k1];
                 
                 NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                 [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                 [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                 NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"WorkingDate"]]];
                 //Add the following line to display the time in the local time zone
                 [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
                 //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
                 [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                 NSString* finalTime = [dateFormatter stringFromDate:newTime];
                 
                 if ([finalTime isEqualToString:arrOfHeaderTitle[k1]]) {
                 
                 txtView.text = [NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"WorkingTime"]];
                 
                 } else {
                 
                 txtView.text = [NSString stringWithFormat:@"%@",@""];
                 
                 }
                 
                 
                 } else {
                 
                 txtView.text = [NSString stringWithFormat:@"%@",@""];
                 
                 }
                 */
                
                txtViewFrame = CGRectMake(xAxis, yAxis,200,heightGlobalForOther);
                
                txtView.frame =txtViewFrame;
                txtView.layer.borderWidth = 1.5f;
                txtView.layer.cornerRadius = 0.0f;
                txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                [txtView setBackgroundColor:[UIColor clearColor]];
                txtView.font=[UIFont systemFontOfSize:20];
                //txtView.delegate=self;
                
                UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                leftView.backgroundColor = [UIColor clearColor];
                //txtView.leftView = leftView;
                //txtView.leftViewMode = UITextFieldViewModeAlways;
                //txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtView.numberOfLines=10000;
                txtView.tag=k*1000+k1+500;
                txtView.textAlignment=NSTextAlignmentCenter;
                [viewEmpSheetOnScroll addSubview:txtView];
                
                xAxis=xAxis+200;
                
            }
            
            if (heightLable.height<50) {
                
                yAxis=yAxis+heightGlobalForOther;
                
                
            } else {
                
                yAxis=yAxis+heightGlobalForOther;
                
            }
            
        }
        
        
        
    }
    
    // Showing Over All Totals
    
    NSArray* uniqueValues = [arrAllWorkingTimeHeader valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.%@", @"self"]];
    
    NSMutableString *strStingTotal = [[NSMutableString alloc] init];
    
    for (int q=0; q<uniqueValues.count; q++) {
        
        NSString *strHeader = uniqueValues[q];
        
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        
        for (int w=0; w<arrAllWorkingTime.count; w++) {
            
            NSString *strTemp = arrAllWorkingTime[w];
            
            if ([strTemp containsString:strHeader]) {
                
                NSArray *arrTempHeader = [arrAllWorkingTime[w] componentsSeparatedByString:@"-" ];
                
                if (arrTempHeader.count>0) {
                    
                    if (arrTempHeader.count==2) {
                        
                        [tempArray addObject:arrTempHeader[1]];
                        
                    }else{
                        
                        NSString *str = [NSString stringWithFormat:@"%@",arrTempHeader[2]];
                        
                        [tempArray addObject:str];
                        
                    }
                    
                    //[tempArray addObject:arrTempHeader[1]];
                    
                }
                
            }
            
        }
        
        // getting Total And adding into string
        //NSNumber * sum = [tempArray valueForKeyPath:@"@sum.self"];
        
        NSMutableArray *arrTempLocal = [[NSMutableArray alloc] init];
        
        for (int e=0; e<tempArray.count; e++) {
            
            NSArray *arrTime=[tempArray[e] componentsSeparatedByString:@":"];
            
            NSString *strHrs,*strMinutes;
            
            if (arrTime.count==1) {
                
                strHrs=arrTime[0];
                strMinutes=@"";
                
            }
            
            if (arrTime.count==2) {
                
                strHrs=arrTime[0];
                strMinutes=arrTime[1];
                
            }
            
            NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
            
            int totalsecondUpdatedEstTime=(int)secondTimeToIncrease;
            //totalsecondUpdatedEstTime=totalsecondUpdatedEstTime/60;
            
            [arrTempLocal addObject:[NSString stringWithFormat:@"%d",totalsecondUpdatedEstTime]];
            
        }
        
        NSNumber * sum = [arrTempLocal valueForKeyPath:@"@sum.self"];

        int value = [sum intValue];
        
        int minutes = (value / 60) % 60;
        int hours = value / 3600;
        
         //[NSString stringWithFormat:@"%02d:%02d",hours, minutes];
        
        NSString *strStringToShow = [NSString stringWithFormat:@"%@ - %@, ",strHeader,[NSString stringWithFormat:@"%02d:%02d",hours, minutes]];
        
        [strStingTotal appendString:strStringToShow];
        
    }
    
    NSString *strOverAllTotal = strStingTotal;
    
    UILabel *lblOverAllTotalTemp = [[UILabel alloc]initWithFrame:CGRectMake(viewEmpSheetOnScroll.frame.origin.x, yAxis+10, scrollViewWidth, 100)];
    
    CGFloat heightTemppLocal = [self heightForString:[NSString stringWithFormat:@"%@",strOverAllTotal] font:lblOverAllTotalTemp.font maxWidth:scrollViewWidth];
    
    if (heightTemppLocal<50) {
        
        heightTemppLocal = 50;
        
    }
    
    UILabel *lblOverAllTotal = [[UILabel alloc]initWithFrame:CGRectMake(10, yAxis+10, scrollViewWidth-10, heightTemppLocal)];
    
    lblOverAllTotal.text=strOverAllTotal;
    
    lblOverAllTotal.numberOfLines=30;
    
    lblOverAllTotal.backgroundColor = [UIColor yellowColor];
    
    [_scrollView addSubview:lblOverAllTotal];
    
    [_scrollView addSubview:viewEmpSheetOnScroll];
    
    //[_viewStopJobReason addSubview:_scrollView];
    
    [_scrollView setContentSize:CGSizeMake(scrollViewWidth,heightGlobalScrollView+50+heightTemppLocal+100)];
    
    _scrollView.scrollEnabled=true;
    _scrollView.showsHorizontalScrollIndicator=true;
    _scrollView.showsVerticalScrollIndicator=true;
    _scrollView.bounces=false;
    
}

-(NSMutableArray*)checkSameLocation :(NSArray*)arrAllData :(NSString*)strWorkOrderNoToCheck {
    
    NSMutableArray *arrSameLocation = [NSMutableArray new];
    for (int i=0;i<arrAllData.count;i++) {
        NSDictionary * dd = [arrAllData objectAtIndex:i];
        
        NSString *strTempp = [NSString stringWithFormat:@"%@",[dd valueForKey:@"WorkOrderNo"]];
        
        if (strTempp.length==0) {
            
            strTempp = [NSString stringWithFormat:@"%@",[dd valueForKey:@"Description"]];
            
        }
        
        if ([[NSString stringWithFormat:@"%@",strWorkOrderNoToCheck] isEqualToString:strTempp]) {
            [arrSameLocation addObject:dd];
        }
    }
    
    return arrSameLocation;
    
}

@end
