//
//  WebService.swift
//  AlamoFire_WebService
//
//  Created by admin on 22/12/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import Alamofire


@objc class WebService: NSObject,NSURLConnectionDelegate {
    
    @objc func downloadGraphXMLSales(strLeadId : String)
    {
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isNewGraph == %@", strLeadId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlFileName = "\(dictData.value(forKey: "leadXmlPath") ?? "")"
            
            var strURL = String()
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") {
                
                strURL = "\(value)"
                
            }
            //https://psasstaging.pestream.com//Documents/GraphXML/XMLGraph202092102455.xml
            strURL = strURL + "\(xmlFileName)"
            
            strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
            
            DispatchQueue.global(qos: .background).async {
                
                let url = URL(string:strURL)
                let data = try? Data(contentsOf: url!)
                
                if data != nil && (data?.count)! > 0 {
                    
                    DispatchQueue.main.async {
                        
                        savepdfDocumentDirectory(strFileName: xmlFileName, dataa: data!)
                        
                    }}
                
            }
        }
        
        
    }
    
    @objc func getDataFromCoreDB(strEntity: String , predicate : NSPredicate )-> NSArray   {
        
        let aryTemp = getDataFromLocal(strEntity: strEntity , predicate: predicate)
        
        if aryTemp.count > 0 {
            
            return aryTemp
            
        } else {
            
            return  NSArray()
            
        }
        
    }
    
    @objc func getOnlyNumberFromStringObjC(number : String) -> String {
        
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        return "\(cleanPhoneNumber)"
        
    }
    
    @objc func formattedNumberObjC(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXX-XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    
    @objc func setDefaultCrmValue() {
        
        setDefaultValuesForAccountFilter()
        setDefaultValuesForCompanyFilter()
        setDefaultValuesForContactFilter()
        setDefaultValuesForTimeLineFilter()
        
    }
    @objc func setDefaultTaskDataFromDashBoard() -> NSMutableDictionary {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        
        return dictTaskDetailsData
        
    }
    
    @objc func setDefaultActivityDataFromDashBoard() -> NSMutableDictionary {
        
        let dictActivityDetailsData = NSMutableDictionary()
        dictActivityDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictActivityDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_LeadId")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_LeadName")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictActivityDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        
        return dictActivityDetailsData
    }
    
    @objc  func deleteProblemImages(strWoId : String) {
        
        deleteAllRecordsFromDB(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
    }
    
    @objc  func saveWdoProblemImage(dictProblemImageDetail : NSDictionary , strWoId : String) {
        
        // Deleting Before Save
        
        //deleteAllRecordsFromDB(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        // Saving Data in DB
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("imageCaption")
        arrOfKeys.add("imageDescription")
        arrOfKeys.add("latitude")
        arrOfKeys.add("longitude")
        arrOfKeys.add("mobileId")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("problemIdentificationId")
        arrOfKeys.add("woImageId")
        arrOfKeys.add("woImagePath")
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ImageCaption") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ImageDescription") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "Latitude") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "Longitude") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "MobileId") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ModifiedBy") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ModifiedDate") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ProblemIdentificationId") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "WoImageId") ?? "")")
        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "WoImagePath") ?? "")")
        
        saveDataInDB(strEntity: Entity_ProblemImageDetails, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    class func savingEmployeeBlockTimeToDB(arrEmployeeBlockTime : NSArray) {
        
        for index in 0..<arrEmployeeBlockTime.count {
            
            let dictDataTemp = arrEmployeeBlockTime[index] as! NSDictionary
            
            // Saving Data in DB
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("strEmpId")
            
            arrOfKeys.add("blockType")
            arrOfKeys.add("descriptionBlockTime")
            arrOfKeys.add("employeeBlockTimeAssignmentId")
            arrOfKeys.add("employeeBlockTimeId")
            arrOfKeys.add("employeeName")
            arrOfKeys.add("employeeNo")
            arrOfKeys.add("fromDate")
            arrOfKeys.add("isActive")
            arrOfKeys.add("modifyDate")
            arrOfKeys.add("titleBlockTime")
            arrOfKeys.add("toDate")
            arrOfKeys.add("fromDateFormatted")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(Global().getEmployeeId())
            
            arrOfValues.add("\(dictDataTemp.value(forKey: "BlockType") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "Description") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "EmployeeBlockTimeAssignmentId") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "EmployeeBlockTimeId") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "EmployeeName") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "EmployeeNo") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "fromDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "IsActive") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "modifyDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "Title") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "toDate") ?? "")")
            arrOfValues.add(Global().getDueDateNeww("\(dictDataTemp.value(forKey: "fromDate") ?? "")"))//getDueDate
            
            saveDataInDB(strEntity: Entity_EmployeeBlockTimeAppointmentVC, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
        
    }
    
    class func savingCrmTaskToDB(arrTask : NSArray) {
        
        for index in 0..<arrTask.count {
            
            let dictDataTemp = arrTask[index] as! NSDictionary
            
            // Saving Data in DB
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("strEmpId")
            
            arrOfKeys.add("assignedByStr")
            arrOfKeys.add("assignedTo")
            arrOfKeys.add("assignedToStr")
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("clientModifiedDate")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("crmCompanyName")
            arrOfKeys.add("crmContactName")
            arrOfKeys.add("dueDate")
            arrOfKeys.add("endDate")
            arrOfKeys.add("followUpFromTaskId")
            arrOfKeys.add("fromDate")
            arrOfKeys.add("leadTaskId")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("modifyDate")
            arrOfKeys.add("priorityStr")
            arrOfKeys.add("reminderDate")
            arrOfKeys.add("status")
            arrOfKeys.add("taskName")
            arrOfKeys.add("taskTypeId")
            arrOfKeys.add("dueDateFormatted")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(Global().getEmployeeId())
            
            arrOfValues.add("\(dictDataTemp.value(forKey: "AssignedByStr") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "AssignedTo") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "AssignedToStr") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "ClientCreatedDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "ClientModifiedDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "CreatedBy") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "CreatedDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "CrmCompanyName") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "CrmContactName") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "DueDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "EndDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "FollowUpFromTaskId") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "DueDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "LeadTaskId") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "ModifiedBy") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "ModifiedDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "DueDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "PriorityStr") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "ReminderDate") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "Status") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "TaskName") ?? "")")
            arrOfValues.add("\(dictDataTemp.value(forKey: "TaskTypeId") ?? "")")
            arrOfValues.add(Global().getDueDateNeww("\(dictDataTemp.value(forKey: "DueDate") ?? "")"))//getDueDate
            
            saveDataInDB(strEntity: Entity_CrmTaskAppointmentVC, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
        
    }
    
    // MARK: - --------------------------------Saving Sales Auto Data To DB From Server----------------------------------
    // MARK: -
    
    class func savingSalesAutoDynamicFormDataToDB(arrayOfLeadsDynamicFormLocal : NSArray , leadIdLocal : String) {
        
        // Saving Data in DB
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("empId")
        arrOfKeys.add("leadId")
        arrOfKeys.add("arrFinalInspection")
        arrOfKeys.add("isSentToServer")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(leadIdLocal)
        arrOfValues.add(arrayOfLeadsDynamicFormLocal)
        arrOfValues.add("Yes")
        
        saveDataInDB(strEntity: Entity_SalesDynamicInspection, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    @objc func saveTagDetails(arrayTags : NSArray, strLeadId: String) {
        
        for item in arrayTags
        {
            let dict = item as! NSDictionary
            
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            arrOfKeys = ["leadId",
                         "userName",
                         "companyKey",
                         "leadTagId",
                         "tagMasterId"]
            arrOfValues = [strLeadId,
                           Global().getUserName(),
                           Global().getCompanyKey(),
                           "\(dict.value(forKey: "LeadTagId") ?? "")",
                           "\(dict.value(forKey: "TagMasterId") ?? "")"]
            
            saveDataInDB(strEntity: "LeadTagExtDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
    }
    
    @objc func saveUserdefinedataDetails(arrayUDF : NSArray, strLeadId: String) {
        
        for item in arrayUDF
        {
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            arrOfKeys = ["leadId",
                         "userName","companyKey",
                         "userdefinedata"]
            arrOfValues = [strLeadId,
                           Global().getUserName(),
                           Global().getCompanyKey(),
                           item ]
            saveDataInDB(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
    }
    
    
    // MARK: - --------------------------------Saving Plumbing Data To DB From Server----------------------------------
    // MARK: -
    
    class func savingPlumbingDynamicFormDataToDB(dictDynamicFormLocal : NSDictionary , subWoIdLocal : String) {
        
        // Saving Data in DB
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("empId")
        arrOfKeys.add("subWorkOrderId")
        arrOfKeys.add("arrFinalInspection")
        arrOfKeys.add("isSentToServer")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(subWoIdLocal)
        arrOfValues.add(dictDynamicFormLocal)
        arrOfValues.add("Yes")
        
        saveDataInDB(strEntity: Entity_PlumbingDynamicForm, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    // MARK: - --------------------------------Saving Service Auto Data To DB From Server----------------------------------
    // MARK: -
    
    class func savingServiceAutoDynamicFormDataToDB(dictDynamicFormLocal : NSDictionary , woIdLocal : String) {
        
        // Saving Data in DB
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("empId")
        arrOfKeys.add("workOrderId")
        arrOfKeys.add("arrFinalInspection")
        arrOfKeys.add("isSentToServer")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(woIdLocal)
        arrOfValues.add(dictDynamicFormLocal)
        arrOfValues.add("Yes")
        
        saveDataInDB(strEntity: Entity_ServiceDynamicForm, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    class func savingServiceAutoEquipmentDynamicFormDataToDB(dictDynamicFormLocal : NSDictionary , woIdLocal : String , strEquipmentId : String , strEquipmentIdType : String) {
        
        // Saving Data in DB
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("empId")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("arrFinalInspection")
        arrOfKeys.add("isSentToServer")
        arrOfKeys.add("woEquipId")
        arrOfKeys.add("mobileEquipId")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(woIdLocal)
        arrOfValues.add(dictDynamicFormLocal)
        arrOfValues.add("Yes")
        
        if strEquipmentIdType == "woEquipId" {
            
            arrOfValues.add(strEquipmentId)
            arrOfValues.add("")
            
        } else {
            
            arrOfValues.add("")
            arrOfValues.add(strEquipmentId)
            
        }
        
        saveDataInDB(strEntity: Entity_EquipmentDynamicForm, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    // MARK: - --------------------------------Saving Service Pest New Flow Data To DB From Server----------------------------------
    // MARK: -
    
    @objc func getDataFromDbToUpdateInObjectiveC(strEntity: String , predicate : NSPredicate , arrayOfKey: NSMutableArray , arrayOfValue: NSMutableArray)-> Bool {
        
        let context = getContext()
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
        fetchRequest.predicate = predicate
        do {
            
            let dataTemp = (try getContext().fetch(fetchRequest) as NSArray)
            
            let objToUpdate = dataTemp[0] as! NSManagedObject
            
            for index in 0..<arrayOfKey.count {
                
                objToUpdate.setValue(arrayOfValue[index], forKey: "\(arrayOfKey[index])")
                
            }
            
            do {
                try context.save()
            } catch _ as NSError  {
                
            } catch {
                
            }
            
            return true
            
        } catch
        {
            let fetchError = error as NSError
            print(fetchError)
            
            return false
            
        }
        
    }
    
    @objc func getDataFromCoreDataBaseArrayObjectiveC(strEntity: String , predicate : NSPredicate )-> NSArray   {
        
        let aryTemp = getDataFromLocal(strEntity: strEntity , predicate: predicate)
        
        if aryTemp.count > 0 {
            
            return aryTemp
            
        } else {
            
            return  NSArray()
            
        }
        
    }
    
    @objc func deleteServicePestNewFlowDB(strWoId : String , strServiceAddressId : String)  {
        
        deleteAllRecordsFromDB(strEntity: "WOServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "WOServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceComments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceConditionComments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServicePests", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceProducts", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceDeviceInspections", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        
        //Ruchika 19 Feb 2021
        
        deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: "ServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId ,Global().getCompanyKey()))
        
        // Db's Created by Ruchika for New Pest Flow
        
        deleteAllRecordsFromDB(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        deleteAllRecordsFromDB(strEntity: "ServiceAreaDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        deleteAllRecordsFromDB(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        deleteAllRecordsFromDB(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
    }
    
    @objc func deleteWdoDB(strWoId : String)  {
        
        deleteAllRecordsFromDB(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: Entity_WdoPricingMultipliers, predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: Entity_ProblemIdentificationPricing, predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        deleteAllRecordsFromDB(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
    }
    
    @objc func deleteDeviceDynamicDataDB(strWoId : String)  {
        
        deleteAllRecordsFromDB(strEntity: Entity_DeviceDynamicData, predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
    }
    
    @objc  func savingDeviceDynamicAllDataDB(arrOfDeviceDynamicFormData : NSArray) {
        
        for k in 0 ..< arrOfDeviceDynamicFormData.count {
            
            if arrOfDeviceDynamicFormData[k] is NSDictionary {
                
                let dictOfData = arrOfDeviceDynamicFormData[k] as! NSDictionary
                
                // Deleting Before Save
                
                deleteDeviceDynamicDataDB(strWoId: "\(dictOfData.value(forKey: "WorkorderId") ?? "")")
                
                // Saving Data in DB
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("workOrderId")
                arrOfKeys.add("arrFinalInspection")
                arrOfKeys.add("isSentToServer")
                arrOfKeys.add("serviceDeviceId")
                
                
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add("\(dictOfData.value(forKey: "WorkorderId") ?? "")")
                arrOfValues.add(dictOfData)
                arrOfValues.add("Yes")
                arrOfValues.add("\(dictOfData.value(forKey: "ServiceDeviceId") ?? "")")
                
                saveDataInDB(strEntity: Entity_DeviceDynamicData, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
        }
        
        
    }
    
    @objc  func savingTargets(arrTarget : NSArray , strLeadId : String) {
        
        if arrTarget.count > 0{
            
            //Delete Target And Target Images before Saving
            
            deleteAllRecordsFromDB(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@", strLeadId))
            
            deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@", strLeadId))
            
            for k in 0 ..< arrTarget.count {
                
                if arrTarget[k] is NSDictionary {
                    
                    let dictOfData = arrTarget[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("isChangedTargetDesc")
                    arrOfKeys.add("leadId")
                    arrOfKeys.add("leadCommercialTargetId")
                    arrOfKeys.add("mobileTargetId")
                    arrOfKeys.add("name")
                    arrOfKeys.add("targetDescription")
                    arrOfKeys.add("targetSysName")
                    arrOfKeys.add("wdoProblemIdentificationId")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    
                    let isChangedTargetDesc = dictOfData.value(forKey: "IsChangedTargetDesc") as! Bool
                    
                    if isChangedTargetDesc {
                        
                        arrOfValues.add("true")
                        
                    }else{
                        
                        arrOfValues.add("false")
                        
                    }
                    arrOfValues.add(strLeadId)
                    
                    if "\(dictOfData.value(forKey: "LeadCommercialTargetId") ?? "")" == "0" {
                        
                        arrOfValues.add("")
                        
                    }else{
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "LeadCommercialTargetId") ?? "")")
                        
                    }
                    
                    if "\(dictOfData.value(forKey: "MobileTargetId") ?? "")" == "0" {
                        
                        arrOfValues.add("")
                        
                    }else{
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "MobileTargetId") ?? "")")
                        
                    }
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "Name") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "TargetDescription") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "TargetSysName") ?? "")")
                    
                    if "\(dictOfData.value(forKey: "WdoProblemIdentificationId") ?? "")" == "0" {
                        
                        arrOfValues.add("")
                        
                    }else{
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "WdoProblemIdentificationId") ?? "")")
                        
                    }
                    
                    // Saving Target In DB
                    saveDataInDB(strEntity: Entity_LeadCommercialTargetExtDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    
                    // TargetImageDetailExtDc Save Target Image In Db
                    
                    if (dictOfData.value(forKey: "TargetImageDetailExtDc")) is NSArray {
                        
                        let arrTargetImageDetails = (dictOfData.value(forKey: "TargetImageDetailExtDc")) as! NSArray
                        
                        for k in 0 ..< arrTargetImageDetails.count {
                            
                            if arrTargetImageDetails[k] is NSDictionary {
                                
                                let dictOfData = arrTargetImageDetails[k] as! NSDictionary
                                
                                let arrOfKeys = NSMutableArray()
                                let arrOfValues = NSMutableArray()
                                
                                
                                arrOfKeys.add("companyKey")
                                arrOfKeys.add("userName")
                                arrOfKeys.add("leadId")
                                arrOfKeys.add("modifiedBy")
                                arrOfKeys.add("modifiedDate")
                                arrOfKeys.add("createdBy")
                                arrOfKeys.add("createdDate")
                                arrOfKeys.add("latitude")
                                arrOfKeys.add("longitude")
                                arrOfKeys.add("leadCommercialTargetId")
                                arrOfKeys.add("mobileTargetId")
                                arrOfKeys.add("leadImagePath")
                                arrOfKeys.add("leadImageType")
                                arrOfKeys.add("targetImageDetailId")
                                arrOfKeys.add("targetSysName")
                                arrOfKeys.add("descriptionImageDetail")
                                arrOfKeys.add("leadImageCaption")
                                arrOfKeys.add("isSync")
                                arrOfKeys.add("isDelete")
                                arrOfKeys.add("mobileTargetImageId")
                                
                                arrOfValues.add(Global().getCompanyKey())
                                arrOfValues.add(Global().getUserName())
                                arrOfValues.add(strLeadId)
                                arrOfValues.add("\(dictOfData.value(forKey: "ModifiedBy") ?? "")")
                                arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                                arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                                arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                                arrOfValues.add("\(dictOfData.value(forKey: "Latitude") ?? "")")
                                arrOfValues.add("\(dictOfData.value(forKey: "Longitude") ?? "")")
                                
                                if "\(dictOfData.value(forKey: "LeadCommercialTargetId") ?? "")" == "0" {
                                    
                                    arrOfValues.add("")
                                    
                                }else{
                                    
                                    arrOfValues.add("\(dictOfData.value(forKey: "LeadCommercialTargetId") ?? "")")
                                    
                                }
                                
                                if "\(dictOfData.value(forKey: "MobileTargetId") ?? "")" == "0" {
                                    
                                    arrOfValues.add("")
                                    
                                }else{
                                    
                                    arrOfValues.add("\(dictOfData.value(forKey: "MobileTargetId") ?? "")")
                                    
                                }
                                
                                arrOfValues.add("\(dictOfData.value(forKey: "LeadImagePath") ?? "")")
                                arrOfValues.add("\(dictOfData.value(forKey: "LeadImageType") ?? "")")
                                
                                if "\(dictOfData.value(forKey: "TargetImageDetailId") ?? "")" == "0" {
                                    
                                    arrOfValues.add("")
                                    
                                }else{
                                    
                                    arrOfValues.add("\(dictOfData.value(forKey: "TargetImageDetailId") ?? "")")
                                    
                                }
                                
                                arrOfValues.add("\(dictOfData.value(forKey: "TargetSysName") ?? "")")
                                arrOfValues.add("\(dictOfData.value(forKey: "DescriptionImageDetail") ?? "")")
                                arrOfValues.add("\(dictOfData.value(forKey: "LeadImageCaption") ?? "")")
                                arrOfValues.add("\(dictOfData.value(forKey: "IsSync") ?? "")" )
                                arrOfValues.add("\(dictOfData.value(forKey: "IsDelete") ?? "")" )
                                arrOfValues.add("\(dictOfData.value(forKey: "MobileTargetImageId") ?? "")")
                                
                                // Saving Target Image In DB
                                saveDataInDB(strEntity: Entity_TargetImageDetail, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        
    }
    
    @objc  func savingWDOAllDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String , strType : String , strWdoLeadId : String, isFromServiceHistory: Bool) {
        
        // logic for remooving Null Value
        
        //        var dictTempResponse: [AnyHashable : Any] = [:]
        //        dictTempResponse["response"] = dictWorkOrderDetail
        //        var dict: [AnyHashable : Any] = [:]
        //        dict = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictTempResponse as! [AnyHashable : Any]))
        //        let dictData = dict["response"] as? [AnyHashable : Any]
        //
        //        let dict1 = dictData! as NSDictionary
        
        //dictWorkOrderDetail = dict
        
        // Deleting Before Save
        
        deleteWdoDB(strWoId: strWoId)
        
        var arrOfData = NSArray()
        
        // Saving Wdo Inspection i.e Product DB
        
        if (dictWorkOrderDetail.value(forKey: Entity_WdoInspection)) is NSDictionary {
            
            let dictOfData = dictWorkOrderDetail.value(forKey: Entity_WdoInspection) as! NSDictionary
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            
            arrOfKeys.add("comments")
            arrOfKeys.add("cpcInspTagPosted")
            arrOfKeys.add("dateOfPrevPco")
            arrOfKeys.add("externalMaterial")
            arrOfKeys.add("furnishedUnfurnished")
            arrOfKeys.add("generalDescription")
            arrOfKeys.add("isBuildingPermit")
            arrOfKeys.add("isSeparateReport")
            arrOfKeys.add("isYearUnknown")
            arrOfKeys.add("monthlyAmount")
            arrOfKeys.add("nameOfPrevPco")
            arrOfKeys.add("numberOfStories")
            arrOfKeys.add("occupiedVacant")
            arrOfKeys.add("reportTypeId")
            arrOfKeys.add("reportTypeName")
            arrOfKeys.add("roofingMaterial")
            arrOfKeys.add("slabRaised")
            arrOfKeys.add("structureDescription")
            arrOfKeys.add("subareaAccess")
            arrOfKeys.add("tIPMasterId")
            arrOfKeys.add("followUpInspectionId")
            arrOfKeys.add("tIPMasterName")
            arrOfKeys.add("yearOfStructure")
            arrOfKeys.add("isTipWarranty")
            arrOfKeys.add("pastPcoTagPosted")
            
            arrOfKeys.add("isActive")
            arrOfKeys.add("orderedBy")
            arrOfKeys.add("orderedByAddress")
            arrOfKeys.add("propertyOwner")
            arrOfKeys.add("propertyOwnerAddress")
            arrOfKeys.add("reportSentTo")
            arrOfKeys.add("reportSentToAddress")
            arrOfKeys.add("woWdoInspectionId")
            arrOfKeys.add("wdoTermiteIssueCodeSysNames")
            arrOfKeys.add("reportSysName")
            arrOfKeys.add("wDOLeadId") // reportTypeSysName
            arrOfKeys.add("reportTypeSysName")
            arrOfKeys.add("totalTIPDiscount")
            arrOfKeys.add("leadInspectionFee")
            arrOfKeys.add("othersSubAreaAccess")
            arrOfKeys.add("othersRoofMaterials")
            arrOfKeys.add("othersExteriorMaterial")
            arrOfKeys.add("othersNumberofStories")
            arrOfKeys.add("othersCpcTagLocated")
            arrOfKeys.add("othersOtherPcoTag")
            arrOfKeys.add("isApplyTipDiscount")//isApplyTipDiscount
            arrOfKeys.add("othersStructureDescription")
            arrOfKeys.add("isEditedMonthlyAmount")
            arrOfKeys.add("otherDiscount")
            arrOfKeys.add("buildingPermitFee")
            arrOfKeys.add("garage")
            arrOfKeys.add("tIPTHPSType")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
            
            
            arrOfValues.add("\(dictOfData.value(forKey: "Comments") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CpcInspTagPosted") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "DateOfPrevPco") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ExternalMaterial") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "FurnishedUnfurnished") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "GeneralDescription") ?? "")")
            arrOfValues.add(dictOfData.value(forKey: "IsBuildingPermit") as! Bool)
            arrOfValues.add(dictOfData.value(forKey: "IsSeparateReport") as! Bool)
            arrOfValues.add(dictOfData.value(forKey: "IsYearUnknown") as! Bool)
            arrOfValues.add("\(dictOfData.value(forKey: "MonthlyAmount") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "NameOfPrevPco") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "NumberOfStories") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OccupiedVacant") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ReportTypeId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ReportTypeName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "RoofingMaterial") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "SlabRaised") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "StructureDescription") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "SubareaAccess") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TIPMasterId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "FollowUpInspectionId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TIPMasterName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "YearOfStructure") ?? "")")
            arrOfValues.add(dictOfData.value(forKey: "IsTipWarranty") as! Bool)
            arrOfValues.add("\(dictOfData.value(forKey: "PastPcoTagPosted") ?? "")")
            
            arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
            arrOfValues.add("\(dictOfData.value(forKey: "OrderedBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OrderedByAddress") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "PropertyOwner") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "PropertyOwnerAddress") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ReportSentTo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ReportSentToAddress") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "WoWdoInspectionId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "WdoTermiteIssueCodeSysNames") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ReportTypeSysName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "WDOLeadId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ReportTypeSysName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TotalTIPDiscount") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "LeadInspectionFee") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OthersSubAreaAccess") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OthersRoofMaterials") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OthersExteriorMaterial") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OthersNumberofStories") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OthersCpcTagLocated") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OthersOtherPcoTag") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsApplyTipDiscount") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OthersStructureDescription") ?? "")")
            arrOfValues.add("false")
            arrOfValues.add("\(dictOfData.value(forKey: "OtherDiscount") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BuildingPermitFee") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "Garage") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TIPTHPSType") ?? "")")
            
            saveDataInDB(strEntity: Entity_WdoInspection, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
        
        // Saving Problem Identifications
        
        let tempDictProblemIds = NSMutableDictionary()
        
        if (dictWorkOrderDetail.value(forKey: Entity_ProblemIdentifications)) is NSArray {
            
            arrOfData = dictWorkOrderDetail.value(forKey: Entity_ProblemIdentifications) as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let problemIdLocalNew = "\(Global().getReferenceNumber() ?? "")"
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifiedDate")
                    
                    
                    
                    arrOfKeys.add("finding")
                    arrOfKeys.add("findingFillIn")
                    arrOfKeys.add("infestation")
                    arrOfKeys.add("isChanged")
                    arrOfKeys.add("isLeadTest")
                    arrOfKeys.add("issuesCode")
                    arrOfKeys.add("problemIdentificationId")
                    arrOfKeys.add("mobileId")
                    arrOfKeys.add("recommendation")
                    arrOfKeys.add("recommendationCode")
                    arrOfKeys.add("recommendationCodeId")
                    arrOfKeys.add("recommendationFillIn")
                    arrOfKeys.add("subsectionCode")
                    arrOfKeys.add("subsectionCodeId")
                    arrOfKeys.add("isActive")
                    arrOfKeys.add("isAddToAgreement")
                    arrOfKeys.add("isBuildingPermit")
                    arrOfKeys.add("proposalDescription")
                    arrOfKeys.add("generalNotes")
                    arrOfKeys.add("generalNoteId")
                    arrOfKeys.add("isResolved")
                    arrOfKeys.add("resolvedDate")
                    arrOfKeys.add("isPrimary")
                    arrOfKeys.add("isSecondary")
                    arrOfKeys.add("recommendationChangeReasonMasterId")
                    arrOfKeys.add("isPrimaryNotSelected")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "Finding") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "FindingFillIn") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Infestation") ?? "")")
                    arrOfValues.add(dictOfData.value(forKey: "IsChanged") as! Bool)
                    arrOfValues.add("\(dictOfData.value(forKey: "IsLeadTest") ?? "")")//IsLeadTest
                    arrOfValues.add("\(dictOfData.value(forKey: "IssuesCode") ?? "")")
                    
                    if strType == "WdoLoadData" {
                        
                        arrOfValues.add("")
                        arrOfValues.add("\(problemIdLocalNew)")
                        tempDictProblemIds.setValue("\(problemIdLocalNew)", forKey: "\(dictOfData.value(forKey: "ProblemIdentificationId") ?? "")")
                        
                    }else{
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "ProblemIdentificationId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "MobileId") ?? "")")
                        
                    }
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "Recommendation") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "RecommendationCode") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "RecommendationCodeId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "RecommendationFillIn") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "SubsectionCode") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "SubsectionCodeId") ?? "")")
                    arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
                    arrOfValues.add(dictOfData.value(forKey: "IsAddToAgreement") as! Bool)
                    arrOfValues.add(dictOfData.value(forKey: "IsBuildingPermit") as! Bool)
                    arrOfValues.add("\(dictOfData.value(forKey: "ProposalDescription") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "GeneralNotes") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "GeneralNoteId") ?? "")")
                    
                    if dictOfData.value(forKey: "IsResolved") is Bool {
                        arrOfValues.add(dictOfData.value(forKey: "IsResolved") as! Bool)
                    } else {
                        arrOfValues.add(false)
                    }
                    //resolvedDate
                    arrOfValues.add("\(dictOfData.value(forKey: "ResolvedDate") ?? "")")
                    
                    if dictOfData.value(forKey: "IsPrimary") is Bool {
                        arrOfValues.add(dictOfData.value(forKey: "IsPrimary") as! Bool)
                    } else {
                        arrOfValues.add(false)
                    }
                    if dictOfData.value(forKey: "IsSecondary") is Bool {
                        arrOfValues.add(dictOfData.value(forKey: "IsSecondary") as! Bool)
                    } else {
                        arrOfValues.add(false)
                    }
                    arrOfValues.add("\(dictOfData.value(forKey: "RecommendationChangeReasonMasterId") ?? "")")
                    //arrOfValues.add("\(dictOfData.value(forKey: "RecommendationChangeReasonMasterId") ?? "")")
                    
                    if dictOfData.value(forKey: "IsPrimaryNotSelected") is Bool {
                        arrOfValues.add(dictOfData.value(forKey: "IsPrimaryNotSelected") as! Bool)
                    } else {
                        arrOfValues.add(false)
                    }
                    
                    //save underwarranty
                    arrOfKeys.add("isCoveredUnderWarranty")
                    if "\(dictOfData.value(forKey: "IsCoveredUnderWarranty") ?? "")".lowercased() == "true" ||  "\(dictOfData.value(forKey: "IsCoveredUnderWarranty") ?? "")".lowercased() == "1" {
                        arrOfValues.add("true")
                    } else {
                        arrOfValues.add("false")
                    }
                  
                    
                    saveDataInDB(strEntity: Entity_ProblemIdentifications, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    //Save Data for Problem Identification Pricing
                    
                    if (dictOfData.value(forKey: Entity_ProblemIdentificationPricing)) is NSDictionary {
                        
                        let problemIdPricingLocalNew = "\(Global().getReferenceNumber() ?? "")"
                        
                        let dictOfData = dictOfData.value(forKey: Entity_ProblemIdentificationPricing) as! NSDictionary
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("companyKey")
                        arrOfKeys.add("userName")
                        arrOfKeys.add("workOrderId")
                        arrOfKeys.add("createdBy")
                        arrOfKeys.add("createdDate")
                        arrOfKeys.add("modifiedBy")
                        arrOfKeys.add("modifiedDate")
                        
                        
                        arrOfKeys.add("problemIdentificationPricingId")
                        arrOfKeys.add("problemIdentificationId")
                        arrOfKeys.add("hours")
                        arrOfKeys.add("materials")
                        arrOfKeys.add("subFee")
                        arrOfKeys.add("linearFtSoilTreatment")
                        arrOfKeys.add("linearFtDrilledConcrete")
                        arrOfKeys.add("cubicFt")
                        arrOfKeys.add("subTotal")
                        arrOfKeys.add("discount")
                        arrOfKeys.add("total")
                        arrOfKeys.add("isActive")
                        arrOfKeys.add("isCalculate")
                        arrOfKeys.add("isDiscount")
                        arrOfKeys.add("isBidOnRequest")
                        arrOfKeys.add("isNoBidGiven")
                        
                        arrOfValues.add(Global().getCompanyKey())
                        arrOfValues.add(Global().getUserName())
                        arrOfValues.add(strWoId)
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                        
                        if strType == "WdoLoadData" {
                            
                            arrOfValues.add("\(problemIdPricingLocalNew)")
                            arrOfValues.add("\(problemIdLocalNew)")
                            
                        }else{
                            
                            arrOfValues.add("\(dictOfData.value(forKey: "ProblemIdentificationPricingId") ?? "")")
                            arrOfValues.add("\(dictOfData.value(forKey: "ProblemIdentificationId") ?? "")")
                            
                        }
                        
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "Hours") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "Materials") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "SubFee") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "LinearFtSoilTreatment") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "LinearFtDrilledConcrete") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "CubicFt") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "SubTotal") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "Discount") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "Total") ?? "")")
                        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
                        arrOfValues.add(dictOfData.value(forKey: "IsCalculate") as! Bool)
                        
                        var discoutVal = 0.0
                        
                        let strDiscount = "\(dictOfData.value(forKey: "Discount") ?? "")"
                        
                        discoutVal = (strDiscount as NSString).doubleValue
                        
                        if discoutVal > 0 {
                            
                            arrOfValues.add(true)
                            
                        }else{
                            
                            arrOfValues.add(false)
                            
                        }
                        
                        arrOfValues.add(dictOfData.value(forKey: "IsBidOnRequest") as! Bool)
                        
                        
                        let strIsNoBidGiven = "\(dictOfData.value(forKey: "IsNoBidGiven") ?? "")"
                        if strIsNoBidGiven.lowercased() == "true" || strIsNoBidGiven == "1" {
                            arrOfValues.add("true")
                        }else{
                            arrOfValues.add("false")
                        }
                        
                        // Jugad for change in cubic Rate
                        
                        arrOfKeys.add("isChangedCubicRate")
                        arrOfValues.add(false)
                        
                        saveDataInDB(strEntity: Entity_ProblemIdentificationPricing, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        
                    }
                    
                    
                    if strType == "WdoLoadData" {
                        
                        // Also Save Targets
                        
                        self.saveTargetForProblem(strMobileTargetId: Global().getReferenceNumber(), strWdoProblemIdentificationId: "\(dictOfData.value(forKey: "ProblemIdentificationId") ?? "")", strWoLeadId: strWdoLeadId, strIssueCode: "\(dictOfData.value(forKey: "IssuesCode") ?? "")".count == 0 ? "\(dictOfData.value(forKey: "SubsectionCode") ?? "")" : "\(dictOfData.value(forKey: "IssuesCode") ?? "")", strFinding: "\(dictOfData.value(forKey: "Finding") ?? "")", strFilling_IssueCode: "\(dictOfData.value(forKey: "FindingFillIn") ?? "")")
                        
                    }
                    
                }
                
            }
            
        }
        
        // Saving WDO Problem Imagess
        
        let arrOfProblemAndTargetImages = NSMutableArray()
        
        if (dictWorkOrderDetail.value(forKey: Entity_ProblemImageDetails)) is NSArray {
            
            arrOfData = dictWorkOrderDetail.value(forKey: Entity_ProblemImageDetails) as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let woImageIdLocalNew = "\(Global().getReferenceNumber() ?? "")"
                    
                    let dictProblemImageDetail = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workorderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("imageCaption")
                    arrOfKeys.add("imageDescription")
                    arrOfKeys.add("latitude")
                    arrOfKeys.add("longitude")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifiedDate")
                    arrOfKeys.add("mobileId")
                    arrOfKeys.add("problemIdentificationId")
                    arrOfKeys.add("woImageId")
                    arrOfKeys.add("woImagePath")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictProblemImageDetail.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictProblemImageDetail.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ImageCaption") ?? "")")
                    arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ImageDescription") ?? "")")
                    arrOfValues.add("\(dictProblemImageDetail.value(forKey: "Latitude") ?? "")")
                    arrOfValues.add("\(dictProblemImageDetail.value(forKey: "Longitude") ?? "")")
                    arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ModifiedBy") ?? "")")
                    arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ModifiedDate") ?? "")")
                    
                    if strType == "WdoLoadData" {
                        
                        let keyTemp = "\(dictProblemImageDetail.value(forKey: "ProblemIdentificationId") ?? "")"
                        
                        let idlocal = "\(tempDictProblemIds.value(forKey: keyTemp) ?? "")"
                        
                        arrOfValues.add("\(idlocal)")
                        arrOfValues.add("")
                        arrOfValues.add("\(woImageIdLocalNew)")
                        
                    }else{
                        
                        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "MobileId") ?? "")")
                        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "ProblemIdentificationId") ?? "")")
                        arrOfValues.add("\(dictProblemImageDetail.value(forKey: "WoImageId") ?? "")")
                        
                    }
                    
                    arrOfValues.add("\(dictProblemImageDetail.value(forKey: "WoImagePath") ?? "")")
                    
                    saveDataInDB(strEntity: Entity_ProblemImageDetails, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if strType == "WdoLoadData" {
                        
                        // Save Target Images also
                        
                        var strLeadCommercialId = String()
                        var strLeadCommercialMobileId = String()
                        
                        let arrayOfTargetsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", strWdoLeadId , "\(dictProblemImageDetail.value(forKey: "ProblemIdentificationId") ?? "")"))
                        
                        if arrayOfTargetsLocal.count > 0 {
                            
                            for k1 in 0 ..< arrayOfTargetsLocal.count {
                                
                                let dictData = arrayOfTargetsLocal[k1] as! NSManagedObject
                                
                                let id1 = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                                let id2 = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                                
                                if id1.count > 0 {
                                    
                                    strLeadCommercialId = id1
                                    
                                }else{
                                    
                                    strLeadCommercialMobileId = id2
                                    
                                }
                                
                                break
                                
                            }
                            
                        }
                        
                        arrOfProblemAndTargetImages.add("\(dictProblemImageDetail.value(forKey: "WoImagePath") ?? "")")
                        
                        let strImageNameSales = "\(dictProblemImageDetail.value(forKey: "WoImagePath") ?? "")".replacingOccurrences(of: "\\Documents\\ProblemIdentificationImages\\", with: "")
                        
                        self.saveTargetImageProblem(strLeadCommercialTargetId: strLeadCommercialId, strMobileTargetId: strLeadCommercialMobileId, strImagePath: strImageNameSales, imageResized: UIImage(), woImageId: "\(dictProblemImageDetail.value(forKey: "WoImageId") ?? "")", strWoLeadId: strWdoLeadId)
                        
                        
                    }
                    
                }
                
            }
            
        }
        
        // Download All Problem Images and Save in Document Directory arrOfWdoImagesToDownloadAndSaveInLead
        
        for k in 0 ..< arrOfProblemAndTargetImages.count {
            
            downloadProblemImage(from: arrOfProblemAndTargetImages[k] as! String)
            
        }
        
        // Saving Disclaimer
        
        if (dictWorkOrderDetail.value(forKey: Entity_Disclaimer)) is NSArray {
            
            arrOfData = dictWorkOrderDetail.value(forKey: Entity_Disclaimer) as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifiedDate")
                    
                    
                    
                    arrOfKeys.add("disclaimerMasterId")
                    arrOfKeys.add("disclaimerDescription")
                    arrOfKeys.add("disclaimerName")
                    arrOfKeys.add("isChanged")
                    //arrOfKeys.add("woWdoDisclaimerId") //Deepak 11 Nov
                    if isFromServiceHistory == false {
                        arrOfKeys.add("woWdoDisclaimerId")
                    }
                    arrOfKeys.add("fillIn")
                    arrOfKeys.add("isActive")
                    arrOfKeys.add("isDefault")
                    
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "DisclaimerMasterId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DisclaimerDescription") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DisclaimerName") ?? "")")
                    //arrOfValues.add(dictOfData.value(forKey: "IsChanged") as! Bool)
                    arrOfValues.add(true)
                    //arrOfValues.add("\(dictOfData.value(forKey: "WoWdoDisclaimerId") ?? "")")
                    if isFromServiceHistory == false {
                        arrOfValues.add("\(dictOfData.value(forKey: "WoWdoDisclaimerId") ?? "")")
                    }
                    arrOfValues.add("\(dictOfData.value(forKey: "FillIn") ?? "")")
                    arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
                    arrOfValues.add(dictOfData.value(forKey: "IsDefault") as! Bool)
                    
                    saveDataInDB(strEntity: Entity_Disclaimer, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        
        // Saving Entity_WdoPricingMultipliers
        
        if (dictWorkOrderDetail.value(forKey: Entity_WdoPricingMultipliers)) is NSDictionary {
            
            let dictOfData = dictWorkOrderDetail.value(forKey: Entity_WdoPricingMultipliers) as! NSDictionary
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            
            arrOfKeys.add("cubicFtRate")
            arrOfKeys.add("hourlyRate")
            arrOfKeys.add("isActive")
            arrOfKeys.add("linearFtDrilledConcreteRate")
            arrOfKeys.add("linearFtSoilTreatmentRate")
            arrOfKeys.add("materialMultiplier")
            arrOfKeys.add("subFeeMultiplier")
            arrOfKeys.add("woWdoPricingMultiplierId")
            
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
            
            arrOfValues.add("\(dictOfData.value(forKey: "CubicFtRate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "HourlyRate") ?? "")")
            arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
            arrOfValues.add("\(dictOfData.value(forKey: "LinearFtDrilledConcreteRate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "LinearFtSoilTreatmentRate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "MaterialMultiplier") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "SubFeeMultiplier") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "WoWdoPricingMultiplierId") ?? "")")
            
            saveDataInDB(strEntity: Entity_WdoPricingMultipliers, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
        
        // Saving General Notes In DB
        
        if (dictWorkOrderDetail.value(forKey: Entity_WoWdoGeneralNotesExtSerDcs)) is NSArray {
            
            arrOfData = dictWorkOrderDetail.value(forKey: Entity_WoWdoGeneralNotesExtSerDcs) as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifiedDate")
                    
                    
                    
                    arrOfKeys.add("generalNoteDescription")
                    arrOfKeys.add("generalNoteMasterId")
                    arrOfKeys.add("wdoGeneralNoteId")
                    arrOfKeys.add("title")
                    arrOfKeys.add("isActive")
                    arrOfKeys.add("fillIn")
                    arrOfKeys.add("type")
                    arrOfKeys.add("isChanged")
                    
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifiedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "GeneralNoteDescription") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "GeneralNoteMasterId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "WdoGeneralNoteId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Title") ?? "")")
                    arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
                    arrOfValues.add("\(dictOfData.value(forKey: "FillIn") ?? "")")//FillIn
                    arrOfValues.add("\(dictOfData.value(forKey: "Type") ?? "")")//type
                    arrOfValues.add(dictOfData.value(forKey: "IsChanged") as! Bool)
                    
                    saveDataInDB(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        // Saving Other Wo Details Also
        
        if strType == "WdoLoadData" {
            
            // Payment Info Saving
            
            //self.savingWdoWoDataDB(dictWorkOrderDetail: dictWorkOrderDetail, strWoId: strWoId)
            
            //self.savingWdoEmailDataDB(dictWorkOrderDetail: dictWorkOrderDetail, strWoId: strWoId)
            
            self.savingWdoImagesDataDB(dictWorkOrderDetail: dictWorkOrderDetail, strWoId: strWoId)
            
            //self.savingWdoPaymentDataDB(dictWorkOrderDetail: dictWorkOrderDetail, strWoId: strWoId)
            
        }
        
    }
    
    
    @objc func saveTargetForProblem(strMobileTargetId : String , strWdoProblemIdentificationId : String , strWoLeadId : String , strIssueCode : String , strFinding : String , strFilling_IssueCode : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("isChangedTargetDesc")
        arrOfKeys.add("leadId")
        arrOfKeys.add("leadCommercialTargetId")
        arrOfKeys.add("mobileTargetId")
        arrOfKeys.add("name")
        arrOfKeys.add("targetDescription")
        arrOfKeys.add("targetSysName")
        arrOfKeys.add("wdoProblemIdentificationId")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add("false")
        arrOfValues.add(strWoLeadId)
        arrOfValues.add("")
        arrOfValues.add(strMobileTargetId)
        arrOfValues.add(strIssueCode)
        
        // Check if tags are replaced
        
        let strReplacedString = replaceTags(strFillingLocal: "\(strFilling_IssueCode)", strFindingLocal: "\(strFinding)")
        
        if strReplacedString.count != 0 {
            
            arrOfValues.add(strReplacedString)
            
        }else {
            
            arrOfValues.add(strFinding)
            
        }
        
        arrOfValues.add("")
        arrOfValues.add(strWdoProblemIdentificationId)
        
        // Saving Target In DB
        saveDataInDB(strEntity: Entity_LeadCommercialTargetExtDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        Global().updateSalesModifydate(strWoLeadId as String)
        
    }
    
    @objc func saveTargetImageProblem(strLeadCommercialTargetId : String , strMobileTargetId : String , strImagePath : String , imageResized : UIImage , woImageId : String , strWoLeadId : String) {
        
        let coordinate = Global().getLocation()
        let latitude = "\(coordinate.latitude)"
        let longitude = "\(coordinate.longitude)"
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadId")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("latitude")
        arrOfKeys.add("longitude")
        arrOfKeys.add("leadCommercialTargetId")
        arrOfKeys.add("mobileTargetId")
        arrOfKeys.add("leadImagePath")
        arrOfKeys.add("leadImageType")
        arrOfKeys.add("targetImageDetailId")
        arrOfKeys.add("targetSysName")
        arrOfKeys.add("mobileTargetImageId")
        arrOfKeys.add("leadImageCaption")
        arrOfKeys.add("descriptionImageDetail")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoLeadId)
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfValues.add(latitude)
        arrOfValues.add(longitude)
        arrOfValues.add(strLeadCommercialTargetId)
        arrOfValues.add(strMobileTargetId)
        arrOfValues.add(strImagePath)
        arrOfValues.add("Target")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add(woImageId)
        arrOfValues.add("")
        arrOfValues.add("")
        
        // Saving Target In DB
        saveDataInDB(strEntity: Entity_TargetImageDetail, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        //saveImageDocumentDirectory(strFileName: strImagePath, image: imageResized)
        
        Global().updateSalesModifydate(strWoLeadId as String)
        
    }
    
    @objc func fetchWdoWo(strWoId : String) -> NSManagedObject {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            let objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
            return objWorkorderDetail
            
        }else{
            
            return NSManagedObject()
        }
        
    }
    
    @objc  func savingWdoWoDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String) {
        
        // Saving Entity_WdoWoDetail
        
        if (dictWorkOrderDetail.value(forKey: "WorkorderDetail")) is NSDictionary {
            
            let objWdoWoData = self.fetchWdoWo(strWoId: strWoId)
            
            let dictOfData = dictWorkOrderDetail.value(forKey: "WorkorderDetail") as! NSDictionary
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifyDate")
            
            arrOfKeys.add("workOrderNo")
            arrOfKeys.add("companyId")
            arrOfKeys.add("surveyID")
            arrOfKeys.add("departmentId")
            arrOfKeys.add("accountNo")
            arrOfKeys.add("thirdPartyAccountNo")
            arrOfKeys.add("employeeNo")
            arrOfKeys.add("firstName")
            arrOfKeys.add("middleName")
            arrOfKeys.add("lastName")
            arrOfKeys.add("companyName")
            arrOfKeys.add("primaryEmail")
            arrOfKeys.add("secondaryEmail")
            arrOfKeys.add("primaryPhone")
            arrOfKeys.add("secondaryPhone")
            arrOfKeys.add("scheduleOnStartDateTime")
            arrOfKeys.add("scheduleOnEndDateTime")
            arrOfKeys.add("scheduleStartDateTime")
            arrOfKeys.add("scheduleEndDateTime")
            arrOfKeys.add("serviceDateTime")
            arrOfKeys.add("totalEstimationTime")
            arrOfKeys.add("branchId")
            arrOfKeys.add("workorderStatus")
            arrOfKeys.add("atributes")
            arrOfKeys.add("specialInstruction")
            arrOfKeys.add("serviceInstruction")
            arrOfKeys.add("direction")
            arrOfKeys.add("otherInstruction")
            arrOfKeys.add("categoryName")
            arrOfKeys.add("subCategory")
            arrOfKeys.add("services")
            arrOfKeys.add("currentServices")
            arrOfKeys.add("lastServices")
            arrOfKeys.add("technicianComment")
            arrOfKeys.add("officeNotes")
            arrOfKeys.add("timeIn")
            arrOfKeys.add("timeOut")
            arrOfKeys.add("resetId")
            arrOfKeys.add("resetDescription")
            arrOfKeys.add("audioFilePath")
            arrOfKeys.add("invoiceAmount")
            arrOfKeys.add("productionAmount")
            arrOfKeys.add("previousBalance")
            arrOfKeys.add("tax")
            arrOfKeys.add("customerSignaturePath")
            arrOfKeys.add("technicianSignaturePath")
            arrOfKeys.add("electronicSignaturePath")
            arrOfKeys.add("invoicePath")
            arrOfKeys.add("billingAddress1")
            arrOfKeys.add("billingAddress2")
            arrOfKeys.add("billingCountry")
            arrOfKeys.add("billingState")
            arrOfKeys.add("billingCity")
            arrOfKeys.add("billingZipcode")
            arrOfKeys.add("servicesAddress1")
            arrOfKeys.add("serviceAddress2")
            arrOfKeys.add("serviceCountry")
            arrOfKeys.add("serviceState")
            arrOfKeys.add("serviceCity")
            arrOfKeys.add("serviceZipcode")
            arrOfKeys.add("serviceAddressLatitude")
            arrOfKeys.add("serviceAddressLongitude")
            arrOfKeys.add("isPDFGenerated")
            arrOfKeys.add("isMailSent")
            arrOfKeys.add("isElectronicSignatureAvailable")
            arrOfKeys.add("isFail")
            arrOfKeys.add("operateMedium")
            arrOfKeys.add("isActive")
            arrOfKeys.add("surveyStatus")
            arrOfKeys.add("modifiedFormatedDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("onMyWaySentSMSTime")
            arrOfKeys.add("routeNo")
            arrOfKeys.add("routeName")
            arrOfKeys.add("targets")
            arrOfKeys.add("deviceVersion")
            arrOfKeys.add("deviceName")
            arrOfKeys.add("versionNumber")
            arrOfKeys.add("versionDate")
            arrOfKeys.add("previousBalance")
            arrOfKeys.add("zdateScheduledStart")
            arrOfKeys.add("dateModified")
            arrOfKeys.add("isCustomerNotPresent")
            arrOfKeys.add("totalallowCrew")
            arrOfKeys.add("assignCrewIds")
            arrOfKeys.add("routeCrewList")
            arrOfKeys.add("zSync")
            arrOfKeys.add("arrivalDuration")
            arrOfKeys.add("keyMap")
            arrOfKeys.add("serviceNotes")
            arrOfKeys.add("problemDescription")
            arrOfKeys.add("serviceDescription")
            arrOfKeys.add("primaryServiceSysName")
            arrOfKeys.add("categorySysName")
            arrOfKeys.add("noChemical")
            arrOfKeys.add("serviceJobDescriptionId")
            arrOfKeys.add("serviceJobDescription")
            arrOfKeys.add("isEmployeePresetSignature")
            arrOfKeys.add("departmentType")
            arrOfKeys.add("tags")
            arrOfKeys.add("serviceGateCode")
            arrOfKeys.add("serviceMapCode")
            arrOfKeys.add("accountDescription")
            arrOfKeys.add("isClientApprovalReq")
            arrOfKeys.add("departmentSysName")
            arrOfKeys.add("departmentType")
            arrOfKeys.add("isResendInvoiceMail")
            arrOfKeys.add("cellNo")
            arrOfKeys.add("billingFirstName")
            arrOfKeys.add("billingMiddleName")
            arrOfKeys.add("billingLastName")
            arrOfKeys.add("billingCellNo")
            arrOfKeys.add("billingPrimaryEmail")
            arrOfKeys.add("billingSecondaryEmail")
            arrOfKeys.add("billingPrimaryPhone")
            arrOfKeys.add("billingSecondaryPhone")
            arrOfKeys.add("billingMapCode")
            arrOfKeys.add("billingPOCId")
            arrOfKeys.add("serviceFirstName")
            arrOfKeys.add("serviceMiddleName")
            arrOfKeys.add("serviceLastName")
            arrOfKeys.add("serviceCellNo")
            arrOfKeys.add("servicePrimaryEmail")
            arrOfKeys.add("serviceSecondaryEmail")
            arrOfKeys.add("servicePrimaryPhone")
            arrOfKeys.add("serviceSecondaryPhone")
            arrOfKeys.add("serviceMapCode")
            arrOfKeys.add("serviceGateCode")
            arrOfKeys.add("serviceNotes")
            arrOfKeys.add("serviceAddressImagePath")
            arrOfKeys.add("servicePOCId")
            arrOfKeys.add("timeInLatitude")
            arrOfKeys.add("timeInLongitude")
            arrOfKeys.add("timeOutLatitude")
            arrOfKeys.add("timeOutLongitude")
            arrOfKeys.add("onMyWaySentSMSTimeLatitude")
            arrOfKeys.add("onMyWaySentSMSTimeLongitude")
            arrOfKeys.add("termiteStateType")
            arrOfKeys.add("billingCounty")
            arrOfKeys.add("serviceCounty")
            arrOfKeys.add("billingSchoolDistrict")
            arrOfKeys.add("serviceSchoolDistrict")
            arrOfKeys.add("earliestStartTimeStr")
            arrOfKeys.add("latestStartTimeStr")
            arrOfKeys.add("driveTimeStr")
            arrOfKeys.add("scheduleOnStartDateTime")
            arrOfKeys.add("scheduleOnEndDateTime")
            arrOfKeys.add("modifiedFormatedDate")
            arrOfKeys.add("isRegularPestFlow")
            arrOfKeys.add("isRegularPestFlowOnBranch")
            arrOfKeys.add("serviceAddressId")
            arrOfKeys.add("isCollectPayment")
            arrOfKeys.add("isWhetherShow")
            arrOfKeys.add("relatedOpportunityNo")
            arrOfKeys.add("taxPercent")
            arrOfKeys.add("serviceReportFormat")
            arrOfKeys.add("isBatchReleased")
            arrOfKeys.add("isNPMATermite")
            arrOfKeys.add("branchSysName")
            arrOfKeys.add("rateMasterId")

            
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
            
            arrOfValues.add("\(objWdoWoData.value(forKey: "WorkOrderNo") ?? "")")
            arrOfValues.add("\(objWdoWoData.value(forKey: "CompanyId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "SurveyID") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "DepartmentId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "AccountNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ThirdPartyAccountNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "EmployeeNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "FirstName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "MiddleName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "LastName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CompanyName") ?? "")")
            
            let arrPrimaryEmailLocal = "\(dictOfData.value(forKey: "PrimaryEmail") ?? "")"
            
            if arrPrimaryEmailLocal.count == 0 {
                
                arrOfValues.add("")
                
            } else {
                
                arrOfValues.add(Global().strReplacedEmail("\(dictOfData.value(forKey: "PrimaryEmail") ?? "")"))
                
            }
            
            let arrSecondaryEmailLocal = "\(dictOfData.value(forKey: "SecondaryEmail") ?? "")"
            
            if arrSecondaryEmailLocal.count == 0 {
                
                arrOfValues.add("")
                
            } else {
                
                arrOfValues.add(Global().strReplacedEmail("\(dictOfData.value(forKey: "SecondaryEmail") ?? "")"))
                
            }
            
            arrOfValues.add("\(dictOfData.value(forKey: "PrimaryPhone") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "SecondaryPhone") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ScheduleOnStartDateTime") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ScheduleOnEndDateTime") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ScheduleStartDateTime") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ScheduleEndDateTime") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceDateTime") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TotalEstimationTime") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BranchId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "WorkorderStatus") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "Attributes") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "SpecialInstruction") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceInstruction") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "Direction") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OtherInstruction") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CategoryName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "SubCategory") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "Services") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CurrentServices") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "LastServices") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TechnicianComment") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OfficeNotes") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TimeIn") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TimeOut") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ResetId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ResetDescription") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "AudioFilePath") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "InvoiceAmount") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ProductionAmount") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "PreviousBalance") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "Tax") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CustomerSignaturePath") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TechnicianSignaturePath") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ElectronicSignaturePath") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "InvoicePath") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingAddress1") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingAddress2") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingCountry") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingState") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingCity") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingZipcode") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServicesAddress1") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddress2") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceCountry") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceState") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceCity") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceZipcode") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressLatitude") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressLongitude") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsPDFGenerated") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsMailSent") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsElectronicSignatureAvailable") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsFail") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OperateMedium") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsActive") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "SurveyStatus") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifiedFormatedDate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifiedBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OnMyWaySentSMSTime") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "RouteNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "RouteName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "Targets") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "DeviceVersion") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "DeviceName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "VersionNumber") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "VersionDate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "PreviousBalance") ?? "")")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss'"
            dateFormatter.timeZone = NSTimeZone.local
            let zdateScheduledStart = dateFormatter.date(from: "\(dictOfData.value(forKey: "ScheduleStartDateTime") ?? "")")!
            
            arrOfValues.add(zdateScheduledStart)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss'"
            dateFormatter1.timeZone = NSTimeZone.local
            let modifiedFormatedDate = dateFormatter1.date(from: "\(dictOfData.value(forKey: "ModifiedFormatedDate") ?? "")")!
            
            arrOfValues.add(modifiedFormatedDate)
            arrOfValues.add("\(dictOfData.value(forKey: "IsCustomerNotPresent") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TotalallowCrew") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "AssignCrewIds") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "RouteCrewList") ?? "")")
            arrOfValues.add("blank")
            arrOfValues.add("\(dictOfData.value(forKey: "ArrivalDuration") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "KeyMap") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceNotes") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ProblemDescription") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceDescription") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "PrimaryServiceSysName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CategorySysName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "NoChemical") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceJobDescriptionId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceJobDescription") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsEmployeePresetSignature") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "DepartmentType") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "Tags") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceGateCode") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceMapCode") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "AccountDescription") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsClientApprovalReq") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "DepartmentSysName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "DepartmentType") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsResendInvoiceMail") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CellNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingFirstName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingMiddleName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingLastName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingCellNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingPrimaryEmail") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingSecondaryEmail") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingPrimaryPhone") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingSecondaryPhone") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingMapCode") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingPOCId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceFirstName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceMiddleName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceLastName") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceCellNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServicePrimaryEmail") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceSecondaryEmail") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServicePrimaryPhone") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceSecondaryPhone") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceMapCode") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceGateCode") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceNotes") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressImagePath") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServicePOCId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TimeInLatitude") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TimeInLongitude") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TimeOutLatitude") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TimeOutLongitude") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OnMyWaySentSMSTimeLatitude") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "OnMyWaySentSMSTimeLongitude") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TermiteStateType") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingCounty") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceCounty") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BillingSchoolDistrict") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceSchoolDistrict") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "EarliestStartTimeStr") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "LatestStartTimeStr") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "DriveTimeStr") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ScheduleOnStartDateTime") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ScheduleOnEndDateTime") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifiedFormatedDate") ?? "")")
            
            if (dictOfData.value(forKey: "IsRegularPestFlow") as! Bool) == true {
                
                arrOfValues.add("true")
                
            } else {
                
                arrOfValues.add("false")
                
            }
            
            if (dictOfData.value(forKey: "IsRegularPestFlowOnBranch") as! Bool) == true {
                
                arrOfValues.add("true")
                
            } else {
                
                arrOfValues.add("false")
                
            }
            
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressId") ?? "")")
            
            if (dictOfData.value(forKey: "IsCollectPayment") as! Bool) == true {
                
                arrOfValues.add("true")
                
            } else {
                
                arrOfValues.add("false")
                
            }
            
            if (dictOfData.value(forKey: "IsWhetherShow") as! Bool) == true {
                
                arrOfValues.add("true")
                
            } else {
                
                arrOfValues.add("false")
                
            }
            
            arrOfValues.add("\(dictOfData.value(forKey: "RelatedOpportunityNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "TaxPercent") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ServiceReportFormat") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsBatchReleased") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "IsNPMATermite") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "BranchSysNameNew") ?? "")")//BranchSysName
            arrOfValues.add("\(dictOfData.value(forKey: "RateMasterId") ?? "")")

            
            
            
            deleteAllRecordsFromDB(strEntity: Entity_WdoWoDetail, predicate: NSPredicate(format: "workorderId == %@", strWoId))
            
            saveDataInDB(strEntity: Entity_WdoWoDetail, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            // Check if audio exist downlaod it
            
            if "\(dictOfData.value(forKey: "AudioFilePath") ?? "")".count > 0 {
                
                downloadAudio(from: "\(dictOfData.value(forKey: "AudioFilePath") ?? "")")
                
            }
            
            
        }//CategorySysName
        
    }
    
    @objc  func savingWdoEmailDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String) {
        
        // Saving Entity_WdoPaymentInfo
        
        deleteAllRecordsFromDB(strEntity: Entity_WdoEmailDetailServiceAuto, predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        let arrOfImagesData = dictWorkOrderDetail.value(forKey: "EmailDetail") as! NSArray
        
        for k in 0 ..< arrOfImagesData.count {
            
            if arrOfImagesData[k] is NSDictionary {
                
                let dictOfData = arrOfImagesData[k] as! NSDictionary
                
                //let dictOfData = dictOfImagesData.value(forKey: "EmailDetail") as! NSDictionary
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("workorderId")
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                
                arrOfKeys.add("woInvoiceMailId")
                arrOfKeys.add("emailId")
                arrOfKeys.add("subject")
                arrOfKeys.add("isCustomerEmail")
                arrOfKeys.add("isDefaultEmail")
                arrOfKeys.add("isMailSent")
                
                
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(strWoId)
                arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                
                arrOfValues.add("\(dictOfData.value(forKey: "WoInvoiceMailId") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "EmailId") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "Subject") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "IsCustomerEmail") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "GraphXmlPath") ?? "")")
                
                if (dictOfData.value(forKey: "IsDefaultEmail") as! Bool) == true {
                    
                    arrOfValues.add("true")
                    
                } else {
                    
                    arrOfValues.add("false")
                    
                }
                
                if (dictOfData.value(forKey: "IsMailSent") as! Bool) == true {
                    
                    arrOfValues.add("true")
                    
                } else {
                    
                    arrOfValues.add("false")
                    
                }
                
                saveDataInDB(strEntity: Entity_WdoEmailDetailServiceAuto, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
        }
        
    }
    
    @objc  func savingWdoImagesDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String) {
        
        let arrOfWdoImagesToDownloadAndSaveInLead = NSMutableArray()
        
        deleteAllRecordsFromDB(strEntity: Entity_WdoImages, predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        // Saving Entity_WdoImages
        
        let arrOfImagesData = dictWorkOrderDetail.value(forKey: "ImagesDetail") as! NSArray
        
        for k in 0 ..< arrOfImagesData.count {
            
            if arrOfImagesData[k] is NSDictionary {
                
                let imageIdLocalNew = "\(Global().getReferenceNumber() ?? "")"
                
                let dictOfData = arrOfImagesData[k] as! NSDictionary
                
                //let dictOfData = dictOfImagesData.value(forKey: Entity_WdoImages) as! NSDictionary
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("workorderId")
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                
                arrOfKeys.add("woImageId")
                arrOfKeys.add("woImagePath")
                arrOfKeys.add("woImageType")
                arrOfKeys.add("imageCaption")
                arrOfKeys.add("imageDescription")
                arrOfKeys.add("latitude")
                arrOfKeys.add("longitude")
                arrOfKeys.add("graphXmlPath")
                arrOfKeys.add("isProblemIdentifaction")
                
                
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                //arrOfValues.add(strWoId)
                arrOfValues.add("\(dictOfData.value(forKey: "WorkorderId") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                
                arrOfValues.add("\(imageIdLocalNew)")
                arrOfValues.add("\(dictOfData.value(forKey: "WoImagePath") ?? "")")
                
                arrOfWdoImagesToDownloadAndSaveInLead.add("\(dictOfData.value(forKey: "WoImagePath") ?? "")")
                
                arrOfValues.add("\(dictOfData.value(forKey: "WoImageType") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "ImageCaption") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "ImageDescription") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "Latitude") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "Longitude") ?? "")")
                arrOfValues.add("\(dictOfData.value(forKey: "GraphXmlPath") ?? "")")
                
                if (dictOfData.value(forKey: "IsProblemIdentifaction") as! Bool) == true {
                    
                    arrOfValues.add("true")
                    
                } else {
                    
                    arrOfValues.add("false")
                    
                }
                
                saveDataInDB(strEntity: Entity_WdoImages, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                
                // Also Save Wdo Images to Lead Images
                
                let strImageNameSales = "\(dictOfData.value(forKey: "WoImagePath") ?? "")".replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
                
                let arrOfKeysSales = NSMutableArray()
                let arrOfValuesSales = NSMutableArray()
                
                arrOfKeysSales.add("createdBy")
                arrOfKeysSales.add("createdDate")
                arrOfKeysSales.add("companyKey")
                arrOfKeysSales.add("userName")
                arrOfKeysSales.add("leadImagePath")
                arrOfKeysSales.add("leadImageType")
                arrOfKeysSales.add("modifiedBy")
                arrOfKeysSales.add("leadImageId")
                arrOfKeysSales.add("modifiedDate")
                arrOfKeysSales.add("leadId")
                arrOfKeysSales.add("leadImageCaption")
                arrOfKeysSales.add("descriptionImageDetail")
                arrOfKeysSales.add("latitude")
                arrOfKeysSales.add("longitude")
                
                arrOfValuesSales.add("") //createdBy
                arrOfValuesSales.add("") //createdDate
                arrOfValuesSales.add(Global().getCompanyKey())
                arrOfValuesSales.add(Global().getUserName())
                arrOfValuesSales.add(strImageNameSales)
                arrOfValuesSales.add("\(dictOfData.value(forKey: "WoImageType") ?? "")")
                arrOfValuesSales.add((Global().getEmployeeId()))
                arrOfValuesSales.add("\(imageIdLocalNew)") // woImageId
                arrOfValuesSales.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                arrOfValuesSales.add(strWoId)
                arrOfValuesSales.add("") // imageCaption
                arrOfValuesSales.add("") // imageDescription
                arrOfValuesSales.add("\(dictOfData.value(forKey: "Latitude") ?? "")") // latitude
                arrOfValuesSales.add("\(dictOfData.value(forKey: "Longitude") ?? "")") //  longitude
                
                saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeysSales, arrayOfValue: arrOfValuesSales)
                
                //saveImageDocumentDirectory(strFileName: strImageNameSales, image: imageResized!)
                
            }
            
        }
        
        // Download All Images and Save in Document Directory arrOfWdoImagesToDownloadAndSaveInLead
        
        for k in 0 ..< arrOfWdoImagesToDownloadAndSaveInLead.count {
            
            downloadImage(from: arrOfWdoImagesToDownloadAndSaveInLead[k] as! String)
            
        }
        
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadProblemImage(from strImageName: String) {
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        var strURL = String()
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
            
            strURL = "\(value)"
            
        }
        
        strURL = strURL + "\(strImageName)"
        
        strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
        
        strURL = replaceBackSlasheFromUrl(strUrl: strURL)
        
        let url = URL(string: strURL)!
        
        print("Download Started")
        
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            
            print("Download Finished")
            
            
            let image: UIImage? = UIImage(data: data)
            if image != nil {
                
                if data.count > 0 {
                    
                    saveImageDocumentDirectory(strFileName: strImageName, image: UIImage(data: data)!)
                    
                    let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\ProblemIdentificationImages\\", with: "")
                    
                    saveImageDocumentDirectory(strFileName: strImageNameSales, image: UIImage(data: data)!)
                    
                }
                
            }
            
        }
    }
    func downloadImage(from strImageName: String) {
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        var strURL = String()
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
            
            strURL = "\(value)"
            
        }
        
        strURL = strURL + "\(strImageName)"
        
        strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
        
        strURL = replaceBackSlasheFromUrl(strUrl: strURL)
        
        let url = URL(string: strURL)!
        
        print("Download Started")
        
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            
            print("Download Finished")
            
            if data.count > 0 {
                
                saveImageDocumentDirectory(strFileName: strImageName, image: UIImage(data: data)!)
                
                let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
                
                saveImageDocumentDirectory(strFileName: strImageNameSales, image: UIImage(data: data)!)
                
            }
            
        }
    }
    
    func downloadAudio(from strImageName: String) {
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        var strURL = String()
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
            
            strURL = "\(value)"
            
        }
        
        strURL = strURL + "\(strImageName)"
        
        strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
        
        strURL = replaceBackSlasheFromUrl(strUrl: strURL)
        
        let url = URL(string: strURL)!
        
        print("Download Started")
        
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            
            print("Download Finished")
            
            savepdfDocumentDirectory(strFileName: strImageName, dataa: data)
            
        }
    }
    
    @objc  func savingWdoPaymentDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String) {
        
        // Saving Entity_WdoPaymentInfo
        
        if (dictWorkOrderDetail.value(forKey: "PaymentInfo")) is NSDictionary {
            
            let dictOfData = dictWorkOrderDetail.value(forKey: "PaymentInfo") as! NSDictionary
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            //arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            
            arrOfKeys.add("woPaymentId")
            arrOfKeys.add("paymentMode")
            arrOfKeys.add("paidAmount")
            arrOfKeys.add("checkNo")
            arrOfKeys.add("drivingLicenseNo")
            arrOfKeys.add("expirationDate")
            arrOfKeys.add("checkFrontImagePath")
            arrOfKeys.add("checkBackImagePath")
            
            
            //arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoPaymentId") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "PaymentMode") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "PaidAmount") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CheckNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "DrivingLicenseNo") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "ExpirationDate") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CheckFrontImagePath") ?? "")")
            arrOfValues.add("\(dictOfData.value(forKey: "CheckBackImagePath") ?? "")")
            
            deleteAllRecordsFromDB(strEntity: Entity_WdoPaymentInfo, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
            
            saveDataInDB(strEntity: Entity_WdoPaymentInfo, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
        
    }
    
    @objc  func savingServicePestAllDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String , strType : String , strToDeleteAreasDevices : String) {
        
        // Deleting Data Before Inserting It  // WorkorderDetail
        
        var strServiceAddressIdTemp = String()
        
        if strType == "WithoutWoDetail" {
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
            
            if arryOfData.count > 0 {
                
                let objWorkorderDetail = arryOfData[0] as! NSManagedObject
                
                strServiceAddressIdTemp = "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")"
                
            }
            
        } else {
            
            if (dictWorkOrderDetail.value(forKey: "WorkorderDetail")) is NSDictionary {
                
                let dictWoData = (dictWorkOrderDetail.value(forKey: "WorkorderDetail")) as! NSDictionary
                
                strServiceAddressIdTemp = "\(dictWoData.value(forKey: "serviceAddressId") ?? "")"
                
            }
            
        }
        
        // Check If To Delete Service Address Id Based Devices and Area
        
        if strToDeleteAreasDevices == "Yes" {
            
            deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@", strServiceAddressIdTemp,Global().getCompanyKey()))
            
            deleteAllRecordsFromDB(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@", strServiceAddressIdTemp,Global().getCompanyKey()))
            
        }
        
        deleteServicePestNewFlowDB(strWoId: strWoId, strServiceAddressId: strServiceAddressIdTemp)
        
        var arrOfData = NSArray()
        
        // Saving Area In DB
        
        if (dictWorkOrderDetail.value(forKey: "ServiceAreas")) is NSArray {
            
            // Saving Area DB
            arrOfData = dictWorkOrderDetail.value(forKey: "ServiceAreas") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    var yesSaveInDB = true
                    
                    // Checking If To Delete Or Insert Record in DB
                    
                    let strId = "\(dictOfData.value(forKey: "AddressAreaId") ?? "")"
                    let strMobileId = "\(dictOfData.value(forKey: "MobileAddressAreaId") ?? "")"
                    let strModifyDateWebB = "\(dictOfData.value(forKey: "ModifyDate") ?? "")"
                    
                    if strId.count > 0 && strId != "0" {
                        
                        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && addressAreaId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strId))
                        
                        if arryOfData.count > 0 {
                            
                            // Already in Local Db no need To Save it Again
                            
                            let objAreaTemp = arryOfData[0] as! NSManagedObject
                            
                            let strModifyDateLocalDB = "\(objAreaTemp.value(forKey: "modifyDate") ?? "")"
                            
                            let strWebDate = Global().convertStringDateToDate(forPestFlow: strModifyDateWebB, "UTC") as Date
                            let strMobileDate = Global().convertStringDateToDate(forPestFlow: strModifyDateLocalDB, "UTC") as Date
                            
                            if strMobileDate < strWebDate {
                                
                                yesSaveInDB = true
                                
                                deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && addressAreaId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strId))
                                
                            }else{
                                
                                yesSaveInDB = false
                                
                            }
                            
                        }else{
                            
                            //deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && addressAreaId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strId))
                            
                            // Saveing Data Coming From Web
                            
                            yesSaveInDB = true
                            
                        }
                        
                    }else{
                        
                        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && mobileAddressAreaId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strMobileId))
                        
                        if arryOfData.count > 0 {
                            
                            // Already in Local Db no need To Save it Again
                            // yesSaveInDB = false
                            
                            let objAreaTemp = arryOfData[0] as! NSManagedObject
                            
                            let strModifyDateLocalDB = "\(objAreaTemp.value(forKey: "modifyDate") ?? "")"
                            
                            let strWebDate = Global().convertStringDateToDate(forPestFlow: strModifyDateWebB, "UTC") as Date
                            let strMobileDate = Global().convertStringDateToDate(forPestFlow: strModifyDateLocalDB, "UTC") as Date
                            
                            if strMobileDate < strWebDate {
                                
                                yesSaveInDB = true
                                
                                deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && mobileAddressAreaId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strMobileId))
                                
                            }else{
                                
                                yesSaveInDB = false
                                
                            }
                            
                        }else{
                            
                            //deleteAllRecordsFromDB(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && mobileAddressAreaId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strMobileId))
                            
                            // Saveing Data Coming From Web
                            yesSaveInDB = true
                            
                        }
                        
                    }
                    
                    if yesSaveInDB {
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("companyKey")
                        arrOfKeys.add("userName")
                        arrOfKeys.add("workOrderId")
                        
                        arrOfKeys.add("mobileAddressAreaId")
                        arrOfKeys.add("addressAreaName")
                        arrOfKeys.add("mobileParentAddressAreaId")
                        arrOfKeys.add("barcode")
                        arrOfKeys.add("serviceAddressId")
                        arrOfKeys.add("isActive")
                        arrOfKeys.add("noOfAreas")
                        arrOfKeys.add("noOfDevices")
                        arrOfKeys.add("noOfConditions")
                        arrOfKeys.add("parentAreaName")
                        arrOfKeys.add("isInspected")
                        arrOfKeys.add("isInspectingFirstTime")
                        arrOfKeys.add("addressAreaId")
                        arrOfKeys.add("isDeletedArea")
                        arrOfKeys.add("serviceAreaDescription")
                        
                        arrOfKeys.add("createdBy")
                        arrOfKeys.add("createdDate")
                        arrOfKeys.add("modifiedBy")
                        arrOfKeys.add("modifyDate")
                        
                        //       "\(dictOfData.value(forKey: "
                        //       ") ?? "")"
                        arrOfValues.add(Global().getCompanyKey())
                        arrOfValues.add(Global().getUserName())
                        arrOfValues.add(strWoId)
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "MobileAddressAreaId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "AddressAreaName") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "MobileParentAddressAreaId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "Barcode") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressId") ?? "")")
                        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
                        arrOfValues.add("\(dictOfData.value(forKey: "NoOfAreas") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "NoOfDevices") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "NoOfConditions") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ParentAreaName") ?? "")")
                        arrOfValues.add(dictOfData.value(forKey: "IsInspected") as! Bool)
                        arrOfValues.add(dictOfData.value(forKey: "isInspectingFirstTime") as! Bool)
                        arrOfValues.add("\(dictOfData.value(forKey: "AddressAreaId") ?? "")")
                        arrOfValues.add(dictOfData.value(forKey: "IsDeletedArea") as! Bool)
                        arrOfValues.add("\(dictOfData.value(forKey: "ServiceAreaDescription") ?? "")")
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                        
                        saveDataInDB(strEntity: "ServiceAreas", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                    }
                    
                }
                
            }
            
        }
        
        // Saving Device DB
        
        if (dictWorkOrderDetail.value(forKey: "ServiceDevices")) is NSArray {
            
            // Saving Device DB
            arrOfData = dictWorkOrderDetail.value(forKey: "ServiceDevices") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    var yesSaveInDB = true
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    // Deleting Record for Service Address Id
                    
                    let strId = "\(dictOfData.value(forKey: "DeviceId") ?? "")"
                    let strMobileId = "\(dictOfData.value(forKey: "MobileDeviceId") ?? "")"
                    let strModifyDateWebB = "\(dictOfData.value(forKey: "ModifyDate") ?? "")"
                    
                    if strId.count > 0 && strId != "0" {
                        
                        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && deviceId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strId))
                        
                        if arryOfData.count > 0 {
                            
                            // Already in Local Db no need To Save it Again
                            //yesSaveInDB = false
                            
                            let objAreaTemp = arryOfData[0] as! NSManagedObject
                            
                            let strModifyDateLocalDB = "\(objAreaTemp.value(forKey: "modifyDate") ?? "")"
                            
                            let strWebDate = Global().convertStringDateToDate(forPestFlow: strModifyDateWebB, "UTC") as Date
                            let strMobileDate = Global().convertStringDateToDate(forPestFlow: strModifyDateLocalDB, "UTC") as Date
                            
                            if strMobileDate < strWebDate {
                                
                                yesSaveInDB = true
                                
                                deleteAllRecordsFromDB(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && deviceId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strId))
                                
                            }else{
                                
                                yesSaveInDB = false
                                
                            }
                            
                        }else{
                            
                            //deleteAllRecordsFromDB(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && deviceId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strId))
                            
                            // Saveing Data Coming From Web
                            
                            yesSaveInDB = true
                            
                        }
                        
                    }else{
                        
                        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && mobileDeviceId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strMobileId))
                        
                        if arryOfData.count > 0 {
                            
                            // Already in Local Db no need To Save it Again
                            //yesSaveInDB = false
                            
                            let objAreaTemp = arryOfData[0] as! NSManagedObject
                            
                            let strModifyDateLocalDB = "\(objAreaTemp.value(forKey: "modifyDate") ?? "")"
                            
                            let strWebDate = Global().convertStringDateToDate(forPestFlow: strModifyDateWebB, "UTC") as Date
                            let strMobileDate = Global().convertStringDateToDate(forPestFlow: strModifyDateLocalDB, "UTC") as Date
                            
                            if strMobileDate < strWebDate {
                                
                                yesSaveInDB = true
                                
                                deleteAllRecordsFromDB(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && mobileDeviceId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strMobileId))
                                
                            }else{
                                
                                yesSaveInDB = false
                                
                            }
                            
                        }else{
                            
                            //deleteAllRecordsFromDB(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@ && mobileDeviceId == %@", strServiceAddressIdTemp,Global().getCompanyKey() , strMobileId))
                            
                            // Saveing Data Coming From Web
                            
                            yesSaveInDB = true
                            
                        }
                        
                    }
                    
                    if yesSaveInDB {
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("companyKey")
                        arrOfKeys.add("userName")
                        arrOfKeys.add("workOrderId")
                        arrOfKeys.add("createdBy")
                        arrOfKeys.add("createdDate")
                        arrOfKeys.add("modifiedBy")
                        arrOfKeys.add("modifyDate")
                        
                        
                        arrOfKeys.add("mobileDeviceId")
                        arrOfKeys.add("deviceId")
                        arrOfKeys.add("mobileAddressAreaId")
                        arrOfKeys.add("serviceAddressId")
                        arrOfKeys.add("deviceName")
                        arrOfKeys.add("barocode")
                        arrOfKeys.add("companyId")
                        arrOfKeys.add("deviceDescription")
                        arrOfKeys.add("deviceSysName")
                        arrOfKeys.add("devieTypeId")
                        arrOfKeys.add("isActive")
                        arrOfKeys.add("action")
                        arrOfKeys.add("activity")
                        arrOfKeys.add("parentAreaName")
                        arrOfKeys.add("isDeletedDevice")
                        
                        //       "\(dictOfData.value(forKey: "
                        //        ?? "")")
                        
                        arrOfValues.add(Global().getCompanyKey())
                        arrOfValues.add(Global().getUserName())
                        arrOfValues.add(strWoId)
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "MobileDeviceId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "DeviceId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "MobileAddressAreaId") ?? "")") // Captial Krna Hai
                        arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "DeviceName") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "Barocode") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "CompanyId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "DeviceDescription") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "DeviceSysName") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "DevieTypeId") ?? "")")
                        arrOfValues.add(true)//arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
                        arrOfValues.add("\(dictOfData.value(forKey: "Action") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "Activity") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ParentAreaName") ?? "")")
                        arrOfValues.add(dictOfData.value(forKey: "IsDeletedDevice") as! Bool)
                        
                        saveDataInDB(strEntity: "ServiceDevices", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                    }
                    
                }
                
            }
            
        }
        
        
        // Saving Material i.e Product DB
        
        if (dictWorkOrderDetail.value(forKey: "ServiceProducts")) is NSArray {
            
            arrOfData = dictWorkOrderDetail.value(forKey: "ServiceProducts") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    arrOfKeys.add("mobileServiceProductId")
                    arrOfKeys.add("serviceProductId")
                    arrOfKeys.add("mobileAssociationId")
                    arrOfKeys.add("associationType")
                    arrOfKeys.add("productMasterId")
                    arrOfKeys.add("serviceAddressId")
                    arrOfKeys.add("productCategoryMasterId")
                    arrOfKeys.add("quantity")
                    arrOfKeys.add("unitId")
                    arrOfKeys.add("concentration")
                    arrOfKeys.add("undilutedQty")
                    arrOfKeys.add("ePARegNo")
                    arrOfKeys.add("applicationMethodId")
                    arrOfKeys.add("appplicationEquipmentId")
                    arrOfKeys.add("lotNo")
                    arrOfKeys.add("applicationRateId")
                    arrOfKeys.add("squareft")
                    arrOfKeys.add("cubicft")
                    arrOfKeys.add("productComment")
                    arrOfKeys.add("serviceProductName")
                    arrOfKeys.add("linearft")
                    arrOfKeys.add("windVelocity")
                    arrOfKeys.add("airTemperature")
                    arrOfKeys.add("windDirection")
                    arrOfKeys.add("skyCondition")
                    arrOfKeys.add("strTargetIds")
                    arrOfKeys.add("activeIngredient")
                    arrOfKeys.add("undilutedUnit")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ") ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceProductId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceProductId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileAssociationId") ?? "")") // Captial Krna Hai
                    arrOfValues.add("\(dictOfData.value(forKey: "AssociationType") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ProductMasterId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ProductCategoryMasterId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Quantity") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "UnitId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Concentration") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "UndilutedQty") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "EPARegNo") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ApplicationMethodId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "AppplicationEquipmentId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "LotNo") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ApplicationRateId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Squareft") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Cubicft") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ProductComment") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceProductName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Linearft") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "WindVelocity") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "AirTemperature") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "WindDirection") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "SkyCondition") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "StrTargetIds") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ActiveIngredient") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "UndilutedUnit") ?? "")")
                    
                    saveDataInDB(strEntity: "ServiceProducts", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        
        // Saving Pests DB
        
        if (dictWorkOrderDetail.value(forKey: "ServicePests")) is NSArray {
            
            arrOfData = dictWorkOrderDetail.value(forKey: "ServicePests") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    arrOfKeys.add("mobileServiceAddressPestId")
                    arrOfKeys.add("serviceAddressPestId")
                    arrOfKeys.add("mobileAssociationId")
                    arrOfKeys.add("associationType")
                    arrOfKeys.add("serviceAddressId")
                    arrOfKeys.add("pestStatus")
                    arrOfKeys.add("quantity")
                    arrOfKeys.add("targetGroupId")
                    arrOfKeys.add("targetId")
                    arrOfKeys.add("targetName")
                    arrOfKeys.add("parentAreaName")
                    arrOfKeys.add("evidence")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceAddressPestId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressPestId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileAssociationId") ?? "")") // Captial Krna Hai
                    arrOfValues.add("\(dictOfData.value(forKey: "AssociationType") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "PestStatus") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Quantity") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "TargetGroupId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "TargetId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "TargetName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ParentAreaName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Evidence") ?? "")")
                    
                    saveDataInDB(strEntity: "ServicePests", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        // Saving Conditions DB
        
        if (dictWorkOrderDetail.value(forKey: "ServiceConditions")) is NSArray {
            
            arrOfData = dictWorkOrderDetail.value(forKey: "ServiceConditions") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    
                    arrOfKeys.add("mobileSAConduciveConditionId")
                    arrOfKeys.add("sAConduciveConditionId")
                    arrOfKeys.add("responsibilityId")
                    arrOfKeys.add("responsibilityName")
                    arrOfKeys.add("serviceAddressId")
                    arrOfKeys.add("priorityId")
                    arrOfKeys.add("conduciveConditionId")
                    arrOfKeys.add("conduciveConditiondDetail")
                    arrOfKeys.add("correctiveActionId")
                    arrOfKeys.add("correctiveActionName")
                    arrOfKeys.add("conditionStatus")
                    arrOfKeys.add("mobileServiceAddressAreaId")
                    arrOfKeys.add("parentAreaName")
                    arrOfKeys.add("sAConduciveConditionName")
                    arrOfKeys.add("conditionGroupId")
                    arrOfKeys.add("strCorrectiveActionIds")
                    arrOfKeys.add("strCreatedDate")
                    arrOfKeys.add("strModifyDate")
                    arrOfKeys.add("isActive")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileSAConduciveConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "SAConduciveConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ResponsibilityId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ResponsibilityName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "PriorityId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ConduciveConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ConduciveConditiondDetail") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CorrectiveActionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CorrectiveActionName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ConditionStatus") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceAddressAreaId") ?? "")") // Captial Krna Hai
                    arrOfValues.add("\(dictOfData.value(forKey: "ParentAreaName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "SAConduciveConditionName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ConditionGroupId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "StrCorrectiveActionIds") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "strCreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "strModifyDate") ?? "")")
                    arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
                    
                    saveDataInDB(strEntity: "ServiceConditions", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        
        // Saving Comments DB
        
        if (dictWorkOrderDetail.value(forKey: "ServiceComments")) is NSArray {
            
            arrOfData = dictWorkOrderDetail.value(forKey: "ServiceComments") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    
                    arrOfKeys.add("mobileServiceCommentId")
                    arrOfKeys.add("serviceCommentId")
                    arrOfKeys.add("mobileAssociationId")
                    arrOfKeys.add("associationType")
                    arrOfKeys.add("serviceAddressId")
                    arrOfKeys.add("serviceCommentDetail")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceCommentId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceCommentId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileAssociationId") ?? "")") // Captial Krna Hai
                    arrOfValues.add("\(dictOfData.value(forKey: "AssociationType") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceCommentDetail") ?? "")")
                    
                    saveDataInDB(strEntity: "ServiceComments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        
        // Saving  Condition Comments DB
        
        if (dictWorkOrderDetail.value(forKey: "ServiceConditionComments")) is NSArray {
            
            
            arrOfData = dictWorkOrderDetail.value(forKey: "ServiceConditionComments") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    
                    arrOfKeys.add("sAConditionCommentId")
                    arrOfKeys.add("mobileSAConditionId")
                    arrOfKeys.add("commentDetail")
                    //arrOfKeys.add("parentAreaName")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "SAConditionCommentId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileSAConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CommentDetail") ?? "")")
                    //arrOfValues.add("\(dictOfData.value(forKey: "ParentAreaName") ?? "")")
                    
                    saveDataInDB(strEntity: "ServiceConditionComments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        // Saving SAPestdocumnets
        
        if (dictWorkOrderDetail.value(forKey: "SAPestDocuments")) is NSArray {
            
            
            arrOfData = dictWorkOrderDetail.value(forKey: "SAPestDocuments") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    
                    arrOfKeys.add("mobileServiceAddressDocId")
                    arrOfKeys.add("parentAreaName")
                    arrOfKeys.add("mobileServiceAddressAreaId")
                    arrOfKeys.add("serviceAddressDocId")
                    arrOfKeys.add("serviceAddressDocName")
                    arrOfKeys.add("serviceAddressDocPath")
                    arrOfKeys.add("serviceAddressId")
                    arrOfKeys.add("documentType")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceAddressDocId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ParentAreaName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceAddressAreaId") ?? "")") // Captial Krna Hai
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressDocId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressDocName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressDocPath") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumentType") ?? "")")
                    
                    saveDataInDB(strEntity: "SAPestDocuments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        
        // Saving  Condition Documents DB
        
        if (dictWorkOrderDetail.value(forKey: "ServiceConditionDocuments")) is NSArray {
            
            
            arrOfData = dictWorkOrderDetail.value(forKey: "ServiceConditionDocuments") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    
                    arrOfKeys.add("documentPath")
                    arrOfKeys.add("documnetTitle")
                    arrOfKeys.add("sAConditionDocumentId")
                    arrOfKeys.add("mobileSAConditionId")
                    arrOfKeys.add("documentType")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumentPath") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumnetTitle") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "SAConditionDocumentId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileSAConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumentType") ?? "")")
                    
                    saveDataInDB(strEntity: "ServiceConditionDocuments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        
        // Saving  WOServiceAreas DB
        
        if (dictWorkOrderDetail.value(forKey: "WOServiceAreas")) is NSArray {
            
            
            arrOfData = dictWorkOrderDetail.value(forKey: "WOServiceAreas") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("mobileServiceAreaId")
                    arrOfKeys.add("serviceAreaId")
                    arrOfKeys.add("wOServiceAreaId")
                    
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceAreaId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAreaId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "WOServiceAreaId") ?? "")")
                    
                    saveDataInDB(strEntity: "WOServiceAreas", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        // Saving  WOServiceDevices DB
        
        if (dictWorkOrderDetail.value(forKey: "WOServiceDevices")) is NSArray {
            
            
            arrOfData = dictWorkOrderDetail.value(forKey: "WOServiceDevices") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("mobileServiceDeviceId")
                    arrOfKeys.add("serviceDeviceId")
                    arrOfKeys.add("wOServiceDeviceId")
                    
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceDeviceId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceDeviceId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "WOServiceDeviceId") ?? "")")
                    
                    saveDataInDB(strEntity: "WOServiceDevices", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        // Saving  ServiceDeviceInspection DB
        
        if (dictWorkOrderDetail.value(forKey: "ServiceDeviceInspections")) is NSArray {
            
            
            arrOfData = dictWorkOrderDetail.value(forKey: "ServiceDeviceInspections") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("actionId")
                    arrOfKeys.add("activity")
                    arrOfKeys.add("barcode")
                    arrOfKeys.add("comments")
                    arrOfKeys.add("deviceInspectionId")
                    arrOfKeys.add("deviceReplacementReasonId")
                    arrOfKeys.add("deviceReplacementReasonName")
                    arrOfKeys.add("isActive")
                    arrOfKeys.add("isDeletedDevice")
                    arrOfKeys.add("mobileServiceDeviceId")
                    arrOfKeys.add("companyId")
                    
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "ActionId") ?? "")")
                    var strActivity = ""
                    if dictOfData.value(forKey: "Activity") is String{
                        strActivity = ("\(dictOfData.value(forKey: "Activity") ?? "")")
                    }
                    if dictOfData.value(forKey: "Activity") is NSNumber{
                        strActivity = String(describing: dictOfData.value(forKey: "Activity") as? NSNumber ?? 0)
                    }
                    arrOfValues.add(Bool(strActivity) ?? 0)//IsActive
                    arrOfValues.add("\(dictOfData.value(forKey: "Barcode") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Comments") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DeviceInspectionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DeviceReplacementReasonId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DeviceReplacementReasonName") ?? "")")
                    arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)//IsActive
                    arrOfValues.add(dictOfData.value(forKey: "IsDeletedDevice") as! Bool)//IsActive
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceDeviceId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CompanyId") ?? "")")
                    
                    saveDataInDB(strEntity: "ServiceDeviceInspections", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
        // Saving New Db's Created by Ruchika for new pest flow
        
        self.savingServicePestTimesheetDataDB(dictWorkOrderDetail: dictWorkOrderDetail, strWoId: strWoId)
        self.savingServicePestServiceCategoryDataDB(dictWorkOrderDetail: dictWorkOrderDetail, strWoId: strWoId)
        self.savingServicePestServiceDeviceDocDataDB(dictWorkOrderDetail: dictWorkOrderDetail, strWoId: strWoId)
        self.savingServicePestServiceAreaDocDataDB(dictWorkOrderDetail: dictWorkOrderDetail, strWoId: strWoId)
        
    }
    
    @objc  func savingServicePestTimesheetDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String) {
        
        if (dictWorkOrderDetail.value(forKey: "WorkOrderBreakTimeExtDcs")) is NSArray {
            
            let arrOfData = dictWorkOrderDetail.value(forKey: "WorkOrderBreakTimeExtDcs") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("duration")
                    arrOfKeys.add("startTime")
                    arrOfKeys.add("status")
                    arrOfKeys.add("stopTime")
                    arrOfKeys.add("subject")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workorderId")
                    arrOfKeys.add("workOrderBreakTimeId")
                    
                    
                    
                    arrOfValues.add(Global().getCompanyKey())
                    
                    arrOfValues.add("")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "StartTimeStrNew") ?? "")")
                    
                    if "\(dictOfData.value(forKey: "EndTimeStr") ?? "")".lowercased() == ""{
                        
                        if "\(dictOfData.value(forKey: "BreakPauseStatus") ?? "")".lowercased() == "start".lowercased(){
                            
                            UserDefaults.standard.setValue("Start", forKey: "StartStopButtonTitle")
                            arrOfValues.add("START")
                            
                        }
                        else  if "\(dictOfData.value(forKey: "BreakPauseStatus") ?? "")".lowercased() == "pause".lowercased(){
                            
                            UserDefaults.standard.setValue("Pause", forKey: "StartStopButtonTitle")
                            arrOfValues.add("PAUSE")
                            
                        }
                        else{
                            arrOfValues.add("")
                        }
                    }
                    else{
                        arrOfValues.add("COMPLETE")
                    }
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "EndTimeStrNew") ?? "")")
                    
                    if "\(dictOfData.value(forKey: "BreakPauseStatus") ?? "")".lowercased() == "start".lowercased(){
                        
                        arrOfValues.add("START")
                        
                    }
                    else  if "\(dictOfData.value(forKey: "BreakPauseStatus") ?? "")".lowercased() == "pause".lowercased(){
                        
                        arrOfValues.add("PAUSE")
                        
                    }
                    else{
                        arrOfValues.add("\(dictOfData.value(forKey: "BreakPauseStatus") ?? "")")
                    }
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "WorkOrderBreakTimeId") ?? "")")
                    
                    
                    saveDataInDB(strEntity: "GeneralInfoWorkorderTimeSheet", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
    }
    
    @objc  func savingServicePestServiceAreaDocDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String) {
        
        if (dictWorkOrderDetail.value(forKey: "ServiceAreaDocuments")) is NSArray {
            
            let arrOfData = dictWorkOrderDetail.value(forKey: "ServiceAreaDocuments") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("addressAreaId")
                    arrOfKeys.add("documentPath")
                    arrOfKeys.add("documentType")
                    arrOfKeys.add("documnetTitle")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifiedDate")
                    arrOfKeys.add("serviceAreaDocumentId")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("workOrderNo")
                    
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add("")
                    arrOfValues.add("")
                    arrOfValues.add("\(dictOfData.value(forKey: "AddressAreaId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumentPath") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumentType") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumnetTitle") ?? "")")
                    
                    arrOfValues.add("")
                    arrOfValues.add("")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAreaDocumentId") ?? "")")
                    arrOfValues.add(strWoId)
                    arrOfValues.add("")
                    
                    saveDataInDB(strEntity: "ServiceAreaDocuments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
    }
    
    
    @objc  func savingServicePestServiceDeviceDocDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String) {
        
        if (dictWorkOrderDetail.value(forKey: "ServiceDeviceDocuments")) is NSArray {
            
            let arrOfData = dictWorkOrderDetail.value(forKey: "ServiceDeviceDocuments") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("deviceId")
                    arrOfKeys.add("documentPath")
                    arrOfKeys.add("documentType")
                    arrOfKeys.add("documnetTitle")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifiedDate")
                    arrOfKeys.add("serviceDeviceDocumentId")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("workOrderNo")
                    
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add("")
                    arrOfValues.add("")
                    arrOfValues.add("\(dictOfData.value(forKey: "DeviceId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumentPath") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumentType") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumnetTitle") ?? "")")
                    
                    arrOfValues.add("")
                    arrOfValues.add("")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceDeviceDocumentId") ?? "")")
                    arrOfValues.add(strWoId)
                    arrOfValues.add("")
                    
                    saveDataInDB(strEntity: "ServiceDeviceDocuments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
    }
    
    @objc  func savingServicePestServiceCategoryDataDB(dictWorkOrderDetail : NSDictionary , strWoId : String) {
        
        
        if (dictWorkOrderDetail.value(forKey: "WorkOrderServicesExtDc")) is NSArray {
            
            let arrOfData = dictWorkOrderDetail.value(forKey: "WorkOrderServicesExtDc") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("serviceCategoryId")
                    arrOfKeys.add("serviceCategoryName")
                    arrOfKeys.add("serviceCategorySysName")
                    arrOfKeys.add("serviceId")
                    arrOfKeys.add("serviceName")
                    arrOfKeys.add("servicePrice")
                    arrOfKeys.add("serviceQuantity")
                    arrOfKeys.add("serviceSysName")
                    arrOfKeys.add("taxable")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workorderId")
                    arrOfKeys.add("workorderServiceId")
                    
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add("")
                    arrOfValues.add("")
                    arrOfValues.add("")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "Price") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "Quantity") ?? "")")
                    arrOfValues.add("")
                    arrOfValues.add("\(dictOfData.value(forKey: "IsTaxable") ?? "")")
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "WorkorderServiceId") ?? "")")
                    
                    
                    saveDataInDB(strEntity: "ServiceCategoryGeneralInfo", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
    }
    
    func fetchAreasFromAreaMasterViaWoId(strWoId : String , strAddressAreaId : String , strMobileAddressAreaId : String , strWoStatus : String) -> NSArray {
        
        let arrMobileIdTemp = NSMutableArray()
        let arrAreaIdTemp = NSMutableArray()
        var  arrOfAreaOnWo = NSArray()
        
        let  arrOfArea = getDataFromCoreDataBaseArray(strEntity: "WOServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        let  arrOfServiceAreaOnAddressId = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "companyKey == %@",Global().getCompanyKey()))
        
        if arrOfArea.count > 0 {
            
            var orPredicate : [NSPredicate] = []
            
            for k in 0 ..< arrOfArea.count {
                
                let dictData = arrOfArea[k] as! NSManagedObject
                
                let strAreaIdLocal = "\(dictData.value(forKey: "serviceAreaId") ?? "")"
                let strMobileAreaIdLocal = "\(dictData.value(forKey: "mobileServiceAreaId") ?? "")"
                
                
                if strAreaIdLocal.count > 0 && strAreaIdLocal != "0" {
                    
                    arrAreaIdTemp.add(strAreaIdLocal)
                    
                    let subPredicate = NSPredicate(format: "addressAreaId == %@", strAreaIdLocal)
                    orPredicate.append(subPredicate)
                    
                }else{
                    
                    arrMobileIdTemp.add(strMobileAreaIdLocal)
                    
                    let subPredicate = NSPredicate(format: "mobileAddressAreaId == %@", strMobileAreaIdLocal)
                    orPredicate.append(subPredicate)
                    
                }
                
            }
            
            let orPredicate1 = NSCompoundPredicate(orPredicateWithSubpredicates: orPredicate)
            
            if strWoStatus == "Complete" {
                
                arrOfAreaOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: orPredicate1)
                
            }else{
                
                if strAddressAreaId.count > 0 && strAddressAreaId != "0"{
                    
                    var andPredicate : [NSPredicate] = []
                    let subPredicate = NSPredicate(format: "addressAreaId == %@", strAddressAreaId)
                    andPredicate.append(subPredicate)
                    
                    let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                    
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                    
                    arrOfAreaOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: predicate)
                    
                }else if strMobileAddressAreaId.count > 0 && strMobileAddressAreaId != "0"{
                    
                    var andPredicate : [NSPredicate] = []
                    let subPredicate = NSPredicate(format: "mobileAddressAreaId == %@", strMobileAddressAreaId)
                    andPredicate.append(subPredicate)
                    
                    let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                    
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                    
                    arrOfAreaOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: predicate)
                    
                }else{
                    
                    
                    let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
                    
                    if arryOfData.count > 0 {
                        
                        let objWorkorderDetail = arryOfData[0] as! NSManagedObject
                        
                        arrOfAreaOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey()))
                        
                    }
                    
                }
                
            }
            
        }
        
        return arrOfAreaOnWo
        
    }
    
    
    func fetchSubAreasFromAreaMasterViaWoId(strWoId : String , strParentAddressAreaId : String) -> NSArray {
        
        let arrMobileIdTemp = NSMutableArray()
        let arrAreaIdTemp = NSMutableArray()
        var  arrOfAreaOnWo = NSArray()
        
        let  arrOfArea = getDataFromCoreDataBaseArray(strEntity: "WOServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        let  arrOfServiceAreaOnAddressId = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: NSPredicate(format: "companyKey == %@",Global().getCompanyKey()))
        
        if arrOfArea.count > 0 {
            
            var orPredicate : [NSPredicate] = []
            
            for k in 0 ..< arrOfArea.count {
                
                let dictData = arrOfArea[k] as! NSManagedObject
                
                let strAreaIdLocal = "\(dictData.value(forKey: "serviceAreaId") ?? "")"
                let strMobileAreaIdLocal = "\(dictData.value(forKey: "mobileServiceAreaId") ?? "")"
                
                
                if strAreaIdLocal.count > 0 && strAreaIdLocal != "0" {
                    
                    arrAreaIdTemp.add(strAreaIdLocal)
                    
                    let subPredicate = NSPredicate(format: "addressAreaId == %@", strAreaIdLocal)
                    orPredicate.append(subPredicate)
                    
                }else{
                    
                    arrMobileIdTemp.add(strMobileAreaIdLocal)
                    
                    let subPredicate = NSPredicate(format: "mobileAddressAreaId == %@", strMobileAreaIdLocal)
                    orPredicate.append(subPredicate)
                    
                }
                
            }
            
            let orPredicate1 = NSCompoundPredicate(orPredicateWithSubpredicates: orPredicate)
            
            if strParentAddressAreaId.count > 0 && strParentAddressAreaId != "0"{
                
                var andPredicate : [NSPredicate] = []
                let subPredicate = NSPredicate(format: "mobileParentAddressAreaId == %@", strParentAddressAreaId)
                andPredicate.append(subPredicate)
                
                let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                
                let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                
                arrOfAreaOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: predicate)
                
            }else{
                
                //arrOfAreaOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: orPredicate1)
                
                var andPredicate : [NSPredicate] = []
                let subPredicate = NSPredicate(format: "mobileParentAddressAreaId == %@", strParentAddressAreaId)
                andPredicate.append(subPredicate)
                
                let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                
                let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                
                arrOfAreaOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceAreas", predicate: predicate)
                
            }
            
        }
        
        return arrOfAreaOnWo
        
    }
    
    func fetchDeviceFromDeviceMasterViaWoId(strWoId : String , strAddressDeviceId : String , strMobileAddressDeviceId : String , strAddressId : String , strWoStatus : String , strToCheckBlankAddressId : String) -> NSArray {
        
        let arrMobileIdTemp = NSMutableArray()
        let arrAreaIdTemp = NSMutableArray()
        var  arrOfDeviceOnWo = NSArray()
        
        let  arrOfArea = getDataFromCoreDataBaseArray(strEntity: "WOServiceDevices", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        if arrOfArea.count > 0 {
            
            var orPredicate : [NSPredicate] = []
            
            for k in 0 ..< arrOfArea.count {
                
                let dictData = arrOfArea[k] as! NSManagedObject
                
                let strAreaIdLocal = "\(dictData.value(forKey: "serviceDeviceId") ?? "")"
                let strMobileAreaIdLocal = "\(dictData.value(forKey: "mobileServiceDeviceId") ?? "")"
                
                
                if strAreaIdLocal.count > 0 && strAreaIdLocal != "0" {
                    
                    arrAreaIdTemp.add(strAreaIdLocal)
                    
                    let subPredicate = NSPredicate(format: "deviceId == %@", strAreaIdLocal)
                    orPredicate.append(subPredicate)
                    
                }else{
                    
                    arrMobileIdTemp.add(strMobileAreaIdLocal)
                    
                    let subPredicate = NSPredicate(format: "mobileDeviceId == %@", strMobileAreaIdLocal)
                    orPredicate.append(subPredicate)
                    
                }
                
            }
            
            let orPredicate1 = NSCompoundPredicate(orPredicateWithSubpredicates: orPredicate)
            
            if strWoStatus == "Complete" {
                
                arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: orPredicate1)
                
            }else{
                //serviceaddressblank  serviceaddressblankOnComplete
                
                if strToCheckBlankAddressId == "serviceaddressExist" {
                    
                    var andPredicate : [NSPredicate] = []
                    let subPredicate = NSPredicate(format: "mobileAddressAreaId == %@ || mobileAddressAreaId == %@", strAddressId,strAddressId)
                    andPredicate.append(subPredicate)
                    
                    let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                    
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                    
                    arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: predicate)
                    
                }else if strToCheckBlankAddressId == "serviceaddressblankOnComplete" {
                    
                    var andPredicate : [NSPredicate] = []
                    let subPredicate = NSPredicate(format: "mobileAddressAreaId == %@ || mobileAddressAreaId == %@", "","0")
                    andPredicate.append(subPredicate)
                    
                    let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                    
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                    
                    arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: predicate)
                    
                }else if strToCheckBlankAddressId == "serviceaddressblank" {
                    
                    var andPredicate : [NSPredicate] = []
                    let subPredicate = NSPredicate(format: "mobileAddressAreaId == %@ || mobileAddressAreaId == %@", "","0")
                    andPredicate.append(subPredicate)
                    
                    let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                    
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                    
                    arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: predicate)
                    
                }else if strAddressId.count > 0 && strAddressId != "0"{
                    
                    var andPredicate : [NSPredicate] = []
                    let subPredicate = NSPredicate(format: "mobileAddressAreaId == %@", strAddressId)
                    andPredicate.append(subPredicate)
                    
                    let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                    
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                    
                    arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: predicate)
                    
                }else if strAddressDeviceId.count > 0 && strAddressDeviceId != "0"{
                    
                    var andPredicate : [NSPredicate] = []
                    let subPredicate = NSPredicate(format: "deviceId == %@", strAddressDeviceId)
                    andPredicate.append(subPredicate)
                    
                    let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                    
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                    
                    arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: predicate)
                    
                }else if strMobileAddressDeviceId.count > 0 && strMobileAddressDeviceId != "0"{
                    
                    var andPredicate : [NSPredicate] = []
                    let subPredicate = NSPredicate(format: "mobileDeviceId == %@", strMobileAddressDeviceId)
                    andPredicate.append(subPredicate)
                    
                    let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                    
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                    
                    arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: predicate)
                    
                }else if strToCheckBlankAddressId == "Yes" {
                    
                    var andPredicate : [NSPredicate] = []
                    let subPredicate = NSPredicate(format: "mobileAddressAreaId == %@", "")
                    andPredicate.append(subPredicate)
                    
                    let andPredicate1 = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
                    
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicate1, andPredicate1])
                    
                    arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: predicate)
                    
                }else{
                    
                    //arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: orPredicate1)
                    
                    let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
                    
                    if arryOfData.count > 0 {
                        
                        let objWorkorderDetail = arryOfData[0] as! NSManagedObject
                        
                        arrOfDeviceOnWo = getDataFromCoreDataBaseArray(strEntity: "ServiceDevices", predicate: NSPredicate(format: "serviceAddressId == %@ && companyKey == %@", "\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")",Global().getCompanyKey()))
                        
                    }
                    
                }
                
            }
            
        }
        
        return arrOfDeviceOnWo
        
    }
    
    
    func getDuration(strStartDate: String, strPauseDate: String) -> String{
        
        if strStartDate.count == 0 {
            
            return "\(0)"
            
        }else{
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            
            
            var startDate = dateFormatter.date(from: strStartDate)
            if startDate == nil{
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.local
                dateFormatter2.dateFormat = "MM/dd/yyyy hh:mm:ss"
                startDate = dateFormatter2.date(from: strStartDate)
            }
            
            
            var pauseDate = dateFormatter.date(from: strPauseDate)
            if pauseDate == nil{
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone.local
                dateFormatter2.dateFormat = "MM/dd/yyyy hh:mm:ss"
                pauseDate = dateFormatter2.date(from: strPauseDate)
            }
            
            let resultedOffSet = pauseDate!.offsetFrom(date: startDate!)
            
            let result = getDurationInterval(strOffSet: resultedOffSet)
            print(result)
            return result
            
        }
        
    }
    
    func getDurationInterval(strOffSet : String) -> String{
        let resultantOffSet = strOffSet
        
        let arr = resultantOffSet.components(separatedBy: " ")
        var strDay = "0"
        var strHr = "0"
        var strMin = "0"
        var strSec = "0"
        
        if resultantOffSet.contains("d"){
            strDay = arr[0].components(separatedBy: "d")[0]
            strHr = arr[1].components(separatedBy: "h")[0]
            strMin = arr[2].components(separatedBy: "m")[0]
            strSec = arr[3].components(separatedBy: "s")[0]
            
        }
        else if resultantOffSet.contains("h"){
            strHr = arr[0].components(separatedBy: "h")[0]
            strMin = arr[1].components(separatedBy: "m")[0]
            strSec = arr[2].components(separatedBy: "s")[0]
            
        }
        else if arr.count == 1{
            strSec = arr[0].components(separatedBy: "s")[0]
        }
        else{
            strMin = arr[0].components(separatedBy: "m")[0]
            strSec = arr[1].components(separatedBy: "s")[0]
        }
        
        strDay = strDay.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strDay = strDay.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strHr = strHr.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strHr = strHr.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strMin = strMin.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strMin = strMin.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        strSec = strSec.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        strSec = strSec.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        let dayInt = Int(strDay)!
        let hrInt = Int(strHr)!
        let minInt = Int(strMin)!
        let secInt = Int(strSec)!
        
        print(dayInt)
        print(hrInt)
        print(minInt)
        print(secInt)
        
        var dayConvertsInHours = Int()
        if dayInt != 0{
            dayConvertsInHours = dayInt * 24
        }
        let result = (dayConvertsInHours * 60 * 60) + (hrInt * 60 * 60) + (minInt * 60) + secInt  //INSECONDS
        //String(minInt) + " Min " + String(secInt) + " secs" // 2 Min 12 secs
        //
        return String(result)
    }
    
    // MARK: - --------------------------------Saving NPMA Termite Data To DB From Server----------------------------------
    // MARK: -
    
    @objc  func savingNPMADataToCoreDB(dictWorkOrderDetail : NSDictionary , strWoId : String , strType : String) {
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAInspectionGeneralDescriptionExtSerDc")) is NSDictionary {
            
            self.savingWoNPMAInspectionGeneralDescriptionExtSerDcToCoreDB(dictData: dictWorkOrderDetail.value(forKey: "WoNPMAInspectionGeneralDescriptionExtSerDc") as! NSDictionary, strWoId: strWoId, strType: strType)
            
        }
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAInspectionOtherDetailExtSerDc")) is NSDictionary {
            
            self.savingWoNPMAInspectionOtherDetailExtSerDcToCoreDB(dictData: dictWorkOrderDetail.value(forKey: "WoNPMAInspectionOtherDetailExtSerDc") as! NSDictionary, strWoId: strWoId , strType: strType)
            
        }
        
        //WoNPMAInspectionObstructionExtSerDcs
        
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAInspectionObstructionExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAInspectionObstructionExtSerDcs")) is NSArray {
            
            let arrData = dictWorkOrderDetail.value(forKey: "WoNPMAInspectionObstructionExtSerDcs") as! NSArray
            
            for k in 0 ..< arrData.count{
                
                let dictDataLocal = arrData[k] as! NSDictionary
                
                self.savingWoNPMAInspectionObstructionExtSerDcsToCoreDB(dictData: dictDataLocal, strWoId: strWoId , strType: strType)
                
            }
            
        }
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAFinalizeReportDetailExtSerDc")) is NSDictionary {
            
            self.savingWoNPMAFinalizeReportDetailExtSerDcToCoreDB(dictData: dictWorkOrderDetail.value(forKey: "WoNPMAFinalizeReportDetailExtSerDc") as! NSDictionary, strWoId: strWoId , strType: strType)
            
        }
        
        // WoNPMAInspectionMoistureDetailExtSerDc
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAInspectionMoistureDetailExtSerDc")) is NSDictionary {
            
            self.savingWoNPMAInspectionMoistureDetailExtSerDcToCoreDB(dictData: dictWorkOrderDetail.value(forKey: "WoNPMAInspectionMoistureDetailExtSerDc") as! NSDictionary, strWoId: strWoId , strType: strType)
            
        }
        
        // WoNPMAProductDetailForSoilAndWoodExtSerDcs
        
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAProductDetailForSoilAndWoodExtSerDcs")) is NSArray {
            
            let arrData = dictWorkOrderDetail.value(forKey: "WoNPMAProductDetailForSoilAndWoodExtSerDcs") as! NSArray
            
            for k in 0 ..< arrData.count{
                
                let dictDataLocal = arrData[k] as! NSDictionary
                
                self.savingWoNPMAProductDetailForSoilAndWoodExtSerDcsToCoreDB(dictData: dictDataLocal, strWoId: strWoId , strType: strType)
                
            }
            
        }
        
        // WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs
        
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs")) is NSArray {
            
            let arrData = dictWorkOrderDetail.value(forKey: "WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs") as! NSArray
            
            for k in 0 ..< arrData.count{
                
                let dictDataLocal = arrData[k] as! NSDictionary
                
                self.savingWoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcsToCoreDB(dictData: dictDataLocal, strWoId: strWoId , strType: strType)
                
            }
            
        }
        
        // WoNPMAWorkorderServicePricingId
        
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAServicePricingExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAServicePricingExtSerDcs")) is NSArray {
            
            let arrData = dictWorkOrderDetail.value(forKey: "WoNPMAServicePricingExtSerDcs") as! NSArray
            
            for k in 0 ..< arrData.count{
                
                let dictDataLocal = arrData[k] as! NSDictionary
                
                self.savingEntity_WoNPMAServicePricingExtSerDcsToCoreDB(dictData: dictDataLocal, strWoId: strWoId , strType: strType)
                
            }
            
        }
        
        // saving Entity_WoNPMAServiceTermsExtSerDcsToCoreDB
        
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAServiceTermsExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAServiceTermsExtSerDcs")) is NSArray {
            
            let arrData = dictWorkOrderDetail.value(forKey: "WoNPMAServiceTermsExtSerDcs") as! NSArray
            
            for k in 0 ..< arrData.count{
                
                let dictDataLocal = arrData[k] as! NSDictionary
                
                self.savingEntity_WoNPMAServiceTermsExtSerDcsToCoreDB(dictData: dictDataLocal, strWoId: strWoId , strType: strType)
                
            }
            
        }
        
        
        self.savingWoNPMAConduciveConditionToCoreDB(dictData: dictWorkOrderDetail, strWoId: strWoId , strType: strType)
        self.savingWoNPMAConduciveConditionDocumentsToCoreDB(dictData: dictWorkOrderDetail, strWoId: strWoId , strType: strType)
        self.savingWoNPMAConduciveConditionCommentsToCoreDB(dictData: dictWorkOrderDetail, strWoId: strWoId , strType: strType)
        
    }
    
    func savingWoNPMAConduciveConditionToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        var arrOfData = NSArray()
        
        // Saving Conditions DB
        
        deleteAllRecordsFromDB(strEntity: "ServiceConditions", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        if (dictData.value(forKey: "ServiceConditions")) is NSArray {
            
            arrOfData = dictData.value(forKey: "ServiceConditions") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    
                    arrOfKeys.add("mobileSAConduciveConditionId")
                    arrOfKeys.add("sAConduciveConditionId")
                    arrOfKeys.add("responsibilityId")
                    arrOfKeys.add("responsibilityName")
                    arrOfKeys.add("serviceAddressId")
                    arrOfKeys.add("priorityId")
                    arrOfKeys.add("conduciveConditionId")
                    arrOfKeys.add("conduciveConditiondDetail")
                    arrOfKeys.add("correctiveActionId")
                    arrOfKeys.add("correctiveActionName")
                    arrOfKeys.add("conditionStatus")
                    arrOfKeys.add("mobileServiceAddressAreaId")
                    arrOfKeys.add("parentAreaName")
                    arrOfKeys.add("sAConduciveConditionName")
                    arrOfKeys.add("conditionGroupId")
                    arrOfKeys.add("strCorrectiveActionIds")
                    arrOfKeys.add("strCreatedDate")
                    arrOfKeys.add("strModifyDate")
                    arrOfKeys.add("serviceAddressAreaId")
                    arrOfKeys.add("isActive")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileSAConduciveConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "SAConduciveConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ResponsibilityId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ResponsibilityName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "PriorityId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ConduciveConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ConduciveConditiondDetail") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CorrectiveActionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CorrectiveActionName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ConditionStatus") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceAddressAreaId") ?? "")") // Captial Krna Hai
                    arrOfValues.add("\(dictOfData.value(forKey: "ParentAreaName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "SAConduciveConditionName") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ConditionGroupId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "StrCorrectiveActionIds") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "strCreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "strModifyDate") ?? "")")
                    
                    if "\(dictOfData.value(forKey: "MobileServiceAddressAreaId") ?? "")".count > 0 {
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "MobileServiceAddressAreaId") ?? "")")
                        
                    } else {
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "ServiceAddressAreaId") ?? "")")
                        
                    }
                    
                    arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
                    
                    saveDataInDB(strEntity: "ServiceConditions", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
    }
    
    func savingWoNPMAConduciveConditionDocumentsToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        var arrOfData = NSArray()
        
        deleteAllRecordsFromDB(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
        // Saving  Condition Documents DB
        
        if (dictData.value(forKey: "ServiceConditionDocuments")) is NSArray {
            
            
            arrOfData = dictData.value(forKey: "ServiceConditionDocuments") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    
                    arrOfKeys.add("documentPath")
                    arrOfKeys.add("documnetTitle")
                    arrOfKeys.add("sAConditionDocumentId")
                    arrOfKeys.add("mobileSAConditionId")
                    arrOfKeys.add("documentType")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumentPath") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumnetTitle") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "SAConditionDocumentId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileSAConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "DocumentType") ?? "")")
                    
                    saveDataInDB(strEntity: "ServiceConditionDocuments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
    }
    
    func savingWoNPMAConduciveConditionCommentsToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        var arrOfData = NSArray()
        
        // Saving  Condition Comments DB
        
        if (dictData.value(forKey: "ServiceConditionComments")) is NSArray {
            
            
            arrOfData = dictData.value(forKey: "ServiceConditionComments") as! NSArray
            
            for k in 0 ..< arrOfData.count {
                
                if arrOfData[k] is NSDictionary {
                    
                    let dictOfData = arrOfData[k] as! NSDictionary
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("companyKey")
                    arrOfKeys.add("userName")
                    arrOfKeys.add("workOrderId")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    
                    
                    
                    arrOfKeys.add("sAConditionCommentId")
                    arrOfKeys.add("mobileSAConditionId")
                    arrOfKeys.add("commentDetail")
                    //arrOfKeys.add("parentAreaName")
                    
                    //       "\(dictOfData.value(forKey: "
                    //       ?? "")")
                    
                    arrOfValues.add(Global().getCompanyKey())
                    arrOfValues.add(Global().getUserName())
                    arrOfValues.add(strWoId)
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "ModifyDate") ?? "")")
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "SAConditionCommentId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileSAConditionId") ?? "")")
                    arrOfValues.add("\(dictOfData.value(forKey: "CommentDetail") ?? "")")
                    //arrOfValues.add("\(dictOfData.value(forKey: "ParentAreaName") ?? "")")
                    
                    saveDataInDB(strEntity: "ServiceConditionComments", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
            }
            
        }
        
    }
    
    func savingWoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcsToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        let dictOfData = dictData
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        arrOfKeys.add("ePARegistrationNumber")
        arrOfKeys.add("isActive")
        arrOfKeys.add("isBaitStation")
        arrOfKeys.add("isPrimary")
        arrOfKeys.add("nameOfSystem")
        arrOfKeys.add("numberOfStationInstalled")
        arrOfKeys.add("woNPMAProductDetailId")
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
        
        arrOfValues.add("\(dictOfData.value(forKey: "EPARegistrationNumber") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsBaitStation") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsPrimary") as! Bool)
        arrOfValues.add("\(dictOfData.value(forKey: "NameOfSystem") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "NumberOfStationInstalled") ?? "")")
        
        if strType == "NPMALoadData" {
            
            arrOfValues.add(Global().getReferenceNumber())
            
        } else {
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoNPMAProductDetailId") ?? "")")
            
        }
        
        saveDataInDB(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func savingWoNPMAProductDetailForSoilAndWoodExtSerDcsToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        let dictOfData = dictData
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        arrOfKeys.add("concentration")
        arrOfKeys.add("ePARegistrationNumber")
        arrOfKeys.add("isActive")
        arrOfKeys.add("isPrimary")
        arrOfKeys.add("isSoil")
        arrOfKeys.add("isTreatmentCompleted")
        arrOfKeys.add("productMasterId")
        arrOfKeys.add("quantity")
        arrOfKeys.add("unitId")
        arrOfKeys.add("woNPMAProductDetailId")
        arrOfKeys.add("productCategoryId")
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
        
        arrOfValues.add("\(dictOfData.value(forKey: "Concentration") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "EPARegistrationNumber") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsPrimary") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsSoil") as! Bool)
        if dictOfData.value(forKey: "IsTreatmentCompleted") is Bool {
            arrOfValues.add(dictOfData.value(forKey: "IsTreatmentCompleted") as! Bool)
        } else {
            arrOfValues.add(false)
        }
        arrOfValues.add("\(dictOfData.value(forKey: "ProductMasterId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Quantity") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "UnitId") ?? "")")
        
        if strType == "NPMALoadData" {
            
            arrOfValues.add(Global().getReferenceNumber())
            
        } else {
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoNPMAProductDetailId") ?? "")")
            
        }
        
        arrOfValues.add("\(dictOfData.value(forKey: "ProductCategoryId") ?? "")")
        
        saveDataInDB(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func savingWoNPMAInspectionMoistureDetailExtSerDcToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        let dictOfData = dictData
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        arrOfKeys.add("correctiveActionDescriptionAndLocation")
        arrOfKeys.add("damageDescriptionAndLocation")
        arrOfKeys.add("evidenceOfPresenceDescription")
        arrOfKeys.add("fungiObservedDescriptionAndLocation")
        arrOfKeys.add("isCorrectiveAction")
        arrOfKeys.add("isDamage")
        arrOfKeys.add("isEvidenceOfMoisture")
        arrOfKeys.add("isEvidenceOfPresence")
        arrOfKeys.add("isFungiObserved")
        arrOfKeys.add("isNoVisibleEvidence")
        arrOfKeys.add("isVisibleEvidence")
        arrOfKeys.add("woNPMMoistureDetailId")
        arrOfKeys.add("isCorrectiveAction")
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
        
        arrOfValues.add("\(dictOfData.value(forKey: "CorrectiveActionDescriptionAndLocation") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "DamageDescriptionAndLocation") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "EvidenceOfPresenceDescription") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "FungiObservedDescriptionAndLocation") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsCorrectiveAction") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsDamage") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsEvidenceOfMoisture") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsEvidenceOfPresence") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsFungiObserved") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsNoVisibleEvidence") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsVisibleEvidence") as! Bool)
        
        if strType == "NPMALoadData" {
            
            arrOfValues.add(Global().getReferenceNumber())
            
        } else {
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoNPMMoistureDetailId") ?? "")")
            
        }
        
        arrOfValues.add(dictOfData.value(forKey: "IsCorrectiveAction") as! Bool)
        
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAInspectionMoistureDetailExtSerDc, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        
        saveDataInDB(strEntity: Entity_WoNPMAInspectionMoistureDetailExtSerDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func savingWoNPMAInspectionObstructionExtSerDcsToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        let dictOfData = dictData
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        arrOfKeys.add("areaId")
        arrOfKeys.add("inspectionObstructionDescription")
        arrOfKeys.add("isActive")
        arrOfKeys.add("locations")
        arrOfKeys.add("woNPMAInspectionObstructionId")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
        
        arrOfValues.add("\(dictOfData.value(forKey: "AreaId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "InspectionObstructionDescription") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
        arrOfValues.add("\(dictOfData.value(forKey: "Locations") ?? "")")
        
        if strType == "NPMALoadData" {
            
            arrOfValues.add(Global().getReferenceNumber())
            
        } else {
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoNPMAInspectionObstructionId") ?? "")")
            
        }
        
        saveDataInDB(strEntity: Entity_WoNPMAInspectionObstructionExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func savingEntity_WoNPMAServicePricingExtSerDcsToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        let dictOfData = dictData
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        arrOfKeys.add("serviceSysName")
        arrOfKeys.add("serviceId")
        arrOfKeys.add("serviceName")
        arrOfKeys.add("servicePrice")
        arrOfKeys.add("woNPMAServicePricingId")
        arrOfKeys.add("isRenewal")
        arrOfKeys.add("isDiscount")
        arrOfKeys.add("isActive")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
        
        arrOfValues.add("\(dictOfData.value(forKey: "ServiceSysName") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ServiceId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ServiceName") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ServicePrice") ?? "")")
        
        if strType == "NPMALoadData" {
            
            arrOfValues.add(Global().getReferenceNumber())
            
        } else {
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoNPMAServicePricingId") ?? "")")
            
        }
        
        
        arrOfValues.add(dictOfData.value(forKey: "IsRenewal") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsDiscount") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
        
        saveDataInDB(strEntity: Entity_WoNPMAServicePricingExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func savingEntity_WoNPMAServiceTermsExtSerDcsToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        let dictOfData = dictData
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        arrOfKeys.add("departmentId")
        arrOfKeys.add("termsId")
        arrOfKeys.add("termsnConditions")
        arrOfKeys.add("woNPMAServiceTermsId")
        arrOfKeys.add("termsTitle")
        arrOfKeys.add("isActive")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
        
        arrOfValues.add("\(dictOfData.value(forKey: "DepartmentId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "TermsId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "TermsnConditions") ?? "")")
        
        if strType == "NPMALoadData" {
            
            arrOfValues.add(Global().getReferenceNumber())
            
        } else {
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoNPMAServiceTermsId") ?? "")")
            
        }
        
        arrOfValues.add("\(dictOfData.value(forKey: "TermsTitle") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
        
        saveDataInDB(strEntity: Entity_WoNPMAServiceTermsExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func savingWoNPMAFinalizeReportDetailExtSerDcToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        let dictOfData = dictData
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        arrOfKeys.add("commentFor99B")//CommentFor99B
        arrOfKeys.add("isBasement")
        arrOfKeys.add("isOther")
        arrOfKeys.add("isSlab")
        arrOfKeys.add("woNPMFinalizeReportDetailId")
        arrOfKeys.add("otherComment")
        arrOfKeys.add("isCrawl")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
        
        arrOfValues.add("\(dictOfData.value(forKey: "CommentFor99B") ?? "")")//CommentFor99B
        arrOfValues.add(dictOfData.value(forKey: "IsBasement") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsOther") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsSlab") as! Bool)
        
        if strType == "NPMALoadData" {
            
            arrOfValues.add(Global().getReferenceNumber())
            
        } else {
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoNPMFinalizeReportDetailId") ?? "")")
            
        }
        
        arrOfValues.add("\(dictOfData.value(forKey: "OtherComment") ?? "")")//OtherComment
        arrOfValues.add(dictOfData.value(forKey: "IsCrawl") as! Bool)
        
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAFinalizeReportDetailExtSerDc, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        
        saveDataInDB(strEntity: Entity_WoNPMAFinalizeReportDetailExtSerDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func savingWoNPMAInspectionOtherDetailExtSerDcToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        let dictOfData = dictData
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        arrOfKeys.add("isInspectorMakeCompleteInspection")
        arrOfKeys.add("isNoTreatmentRecommended")
        arrOfKeys.add("isRecommendTreatment")
        arrOfKeys.add("locationListOfInfestation")
        arrOfKeys.add("locationListOfVisibleAndHiddenDamage")
        arrOfKeys.add("noTreatmentRecommendedDescription")
        arrOfKeys.add("recommendTreatmentDescription")
        arrOfKeys.add("woNPMInspectionOtherDetailId")
        arrOfKeys.add("deadInsectDecriptionAndLocation")
        arrOfKeys.add("isDeadInsect")
        arrOfKeys.add("isLiveInsect")
        arrOfKeys.add("isVisibleDemage")
        arrOfKeys.add("isVisibleEvidence")
        arrOfKeys.add("liveInsectDescriptionAndLocation")
        arrOfKeys.add("repairToBeDone")
        arrOfKeys.add("visibleDemageDescriptionAndLocation")
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
        
        arrOfValues.add(dictOfData.value(forKey: "IsInspectorMakeCompleteInspection") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsNoTreatmentRecommended") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsRecommendTreatment") as! Bool)
        arrOfValues.add("\(dictOfData.value(forKey: "LocationListOfInfestation") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "LocationListOfVisibleAndHiddenDamage") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "NoTreatmentRecommendedDescription") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "RecommendTreatmentDescription") ?? "")")
        
        if strType == "NPMALoadData" {
            
            arrOfValues.add(Global().getReferenceNumber())
            
        } else {
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoNPMInspectionOtherDetailId") ?? "")")
            
        }
        
        arrOfValues.add("\(dictOfData.value(forKey: "DeadInsectDecriptionAndLocation") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsDeadInsect") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsLiveInsect") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsVisibleDemage") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsVisibleEvidence") as! Bool)
        arrOfValues.add("\(dictOfData.value(forKey: "LiveInsectDescriptionAndLocation") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "RepairToBeDone") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "VisibleDemageDescriptionAndLocation") ?? "")")
        
        
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAInspectionOtherDetailExtSerDc, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        
        saveDataInDB(strEntity: Entity_WoNPMAInspectionOtherDetailExtSerDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func savingWoNPMAInspectionGeneralDescriptionExtSerDcToCoreDB(dictData : NSDictionary , strWoId : String , strType : String) {
        
        let dictOfData = dictData
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        arrOfKeys.add("approxSqFt")
        arrOfKeys.add("blockEquals")
        arrOfKeys.add("brickVeneer")
        arrOfKeys.add("color")
        arrOfKeys.add("constructionTypeSysName")
        arrOfKeys.add("crawlDoorHeight")
        arrOfKeys.add("crawlSpaceHeight")
        arrOfKeys.add("extraMaterialToCompleteJob")
        arrOfKeys.add("foundationTypeSysName")
        arrOfKeys.add("garege")
        arrOfKeys.add("hollowBlack")
        arrOfKeys.add("inspectedBy")
        arrOfKeys.add("inspectedDate")
        arrOfKeys.add("isControlOfExistingInfestation")
        arrOfKeys.add("isDamp")
        arrOfKeys.add("isDry")
        arrOfKeys.add("isDryerVentedOutside")
        arrOfKeys.add("isHP")
        arrOfKeys.add("isInside")
        arrOfKeys.add("isInstallMoistureBarrier")
        arrOfKeys.add("isOutside")
        arrOfKeys.add("isPerimeter")
        arrOfKeys.add("isPreventionOfInfestation")
        arrOfKeys.add("isRemoveWoodDebris")
        arrOfKeys.add("isVentsNecessary")
        arrOfKeys.add("isWet")
        arrOfKeys.add("linearFt")
        arrOfKeys.add("moistureMeterReading")
        arrOfKeys.add("numberOfSectionsInstalled")
        arrOfKeys.add("numberOfStories")
        arrOfKeys.add("originalTreatmentDate")
        arrOfKeys.add("porches")
        arrOfKeys.add("slab")
        arrOfKeys.add("stoneVeneer")
        arrOfKeys.add("structureType")
        arrOfKeys.add("typeOfSystem")
        arrOfKeys.add("ventsNumber")
        arrOfKeys.add("woNPMAInspectionId")
        arrOfKeys.add("workCompletedBy")
        arrOfKeys.add("workCompletedDate")
        arrOfKeys.add("type")
        arrOfKeys.add("isSlab")
        arrOfKeys.add("isBrickVeneer")
        arrOfKeys.add("isHollowBlack")
        arrOfKeys.add("isStoneVeneer")
        arrOfKeys.add("isPorches")
        arrOfKeys.add("isGarege")
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
        
        arrOfValues.add("\(dictOfData.value(forKey: "ApproxSqFt") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "BlockEquals") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "BrickVeneer") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Color") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ConstructionTypeSysName") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CrawlDoorHeight") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CrawlSpaceHeight") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "ExtraMaterialToCompleteJob") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "FoundationTypeSysName") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Garege") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "HollowBlack") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "InspectedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "InspectedDate") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsControlOfExistingInfestation") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsDamp") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsDry") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsDryerVentedOutside") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsHP") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsInside") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsInstallMoistureBarrier") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsOutside") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsPerimeter") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsPreventionOfInfestation") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsRemoveWoodDebris") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsVentsNecessary") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsWet") as! Bool)
        arrOfValues.add("\(dictOfData.value(forKey: "LinearFt") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "MoistureMeterReading") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "NumberOfSectionsInstalled") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "NumberOfStories") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "OriginalTreatmentDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Porches") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Slab") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "StoneVeneer") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "StructureType") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "TypeOfSystem") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "VentsNumber") ?? "")")
        
        if strType == "NPMALoadData" {
            
            arrOfValues.add(Global().getReferenceNumber())
            
        } else {
            
            arrOfValues.add("\(dictOfData.value(forKey: "WoNPMAInspectionId") ?? "")")
            
        }
        
        arrOfValues.add("\(dictOfData.value(forKey: "WorkCompletedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "WorkCompletedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Type") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsSlab") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsBrickVeneer") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsHollowBlack") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsStoneVeneer") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsPorches") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsGarege") as! Bool)
        
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAInspectionGeneralDescriptionExtSerDc, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        
        saveDataInDB(strEntity: Entity_WoNPMAInspectionGeneralDescriptionExtSerDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    
    class func callAPI(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        AF.request(url, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            
            switch(response.result) {
                
            case .success(_):
                if let data = response.value
                {
                    OnResultBlock(data as! NSDictionary,"success")
                }
                break
                
            case .failure(_):
                //print(response.result.error)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
                
            }
            
        }
        
    }
    
    
    //MARK:
    //MARK:  estimatedHeightOfLabel Function
    func estimatedHeightOfLabel(text: String , view : UIView) -> CGFloat {
        let size = CGSize(width: view.frame.width - 20 , height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        return rectangleHeight
    }
    
    @objc func estimatedHeightOfLabelForObjectiveC(text: String , view : UIView) -> CGFloat {
        let size = CGSize(width: view.frame.width - 20 , height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        return rectangleHeight
    }
    
    func estimatedHeightOfLabelNew(text: String , view : UIView , fontSize : CGFloat) -> CGFloat {
        let size = CGSize(width: view.frame.width - 20 , height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)]
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        return rectangleHeight
    }
    
    //MARK:
    //MARK: API Without Secureity Parameter
    
    class func callAPIBYpostSaveOfflineData(parameter:String,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let headers: HTTPHeaders = [
            "OfflineTreeTaggingDc": parameter,
            "Accept": "application/json"
        ]
        
        AF.request(strUrlwithString!, method: .post, parameters:nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    let dictdata = NSMutableDictionary()
                    let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                        "Result" == (k as AnyObject)as! String
                    })
                    if status{
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"success")
                    }
                    else {
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    
    
    
    
    
    
    
    
    class func callAPIBYGET(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    let dictdata = NSMutableDictionary()
                    let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                        "ServiceAddresses" == (k as AnyObject)as! String
                    })
                    if status{
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,true)
                    }
                    else {
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                    }
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,false)
                break
            }
        }
    }
    
    class func callAPIBYPOST(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        AF.request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    // print(data)
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,true)
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,false)
                break
            }
        }
    }
    
    class func callAPIBYGetPayment(headers:HTTPHeaders,parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        AF.request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: headers).responseString { response in
            
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    // print(data)
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,true)
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,false)
                break
            }
            
        }
        
    }
    
    class func callAPIWithImage(parameter:NSDictionary,url:String,image:UIImage,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        AF.upload(multipartFormData: { (multipartdata) in
            
            let imageData = image.jpegData(compressionQuality:0.5)
            multipartdata.append(imageData!, withName: withName, fileName: fileName, mimeType: "image/png")
            for (key, value) in parameter {
                multipartdata.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
            
        }, to: url).responseJSON { response in
            print(response.request ?? 0)  // original URL request
            print(response.response ?? 0) // URL response
            print(response.data ?? 0)     // server data
            print(response.result)   // result of response serialization
            
            if let JSON = response.value
            {
                
                if JSON is NSArray {
                    
                    let arrRes = JSON as! NSArray
                    
                    if arrRes.count > 0 {
                        
                        OnResultBlock(arrRes[0] as! NSDictionary,"success")
                        
                    } else {
                        
                        let dic = NSMutableDictionary.init()
                        
                        OnResultBlock(dic,"failure")
                        
                    }
                    
                } else {
                    
                    let dic = NSMutableDictionary.init()
                    
                    OnResultBlock(dic,"failure")
                    
                }
                
            }else{
                let dic = NSMutableDictionary.init()
                
                OnResultBlock(dic,"failure")
            }
        }
        
    }
    /*class func callAPIWithImage(parameter:NSDictionary,url:String,image:UIImage,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
     
     
     upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
     
     let imageData = image.jpegData(compressionQuality:0.5)
     multiPartFormData.append(imageData!, withName: withName, fileName: fileName, mimeType: "image/png")
     for (key, value) in parameter {
     multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
     }
     }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
     switch (encodingResult){
     case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
     upload.responseJSON { response in
     print(response.request ?? 0)  // original URL request
     print(response.response ?? 0) // URL response
     print(response.data ?? 0)     // server data
     print(response.result)   // result of response serialization
     
     if let JSON = response.result.value
     {
     
     if JSON is NSArray {
     
     let arrRes = JSON as! NSArray
     
     if arrRes.count > 0 {
     
     OnResultBlock(arrRes[0] as! NSDictionary,"success")
     
     } else {
     
     let dic = NSMutableDictionary.init()
     
     OnResultBlock(dic,"failure")
     
     }
     
     } else {
     
     let dic = NSMutableDictionary.init()
     
     OnResultBlock(dic,"failure")
     
     }
     
     }else{
     let dic = NSMutableDictionary.init()
     
     OnResultBlock(dic,"failure")
     }
     }
     case .failure(let encodingError):
     print(encodingError)
     let dic = NSMutableDictionary.init()
     dic.setValue("Connection Time Out ", forKey: "message")
     OnResultBlock(dic,"failure")
     }
     }
     }*/
    
    /*func requestWith(endUrl: String, imageData: Data?, parameters: NSMutableDictionary, onCompletion: ((_ dict: NSDictionary) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
     
     let url = endUrl /* your API url */
     
     let headers: HTTPHeaders = [
     /* "Authorization": "your_access_token",  in case you need authorization header */
     //"Content-type": "multipart/form-data"
     :]
     
     Alamofire.upload(multipartFormData: { (multipartFormData) in
     for (key, value) in parameters {
     multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
     }
     // multipartFormData.append("\(90)".data(using: String.Encoding.utf8)!, withName: "user_id")
     
     if let data = imageData{
     multipartFormData.append(data, withName: "File", fileName: "image.jpg", mimeType: "image/png")
     }
     
     }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
     switch result{
     case .success(let upload, _, _):
     upload.responseJSON { response in
     print("Succesfully uploaded")
     if let err = response.error{
     onError?(err)
     return
     }
     onCompletion?(NSDictionary())
     }
     case .failure(let error):
     print("Error in upload: \(error.localizedDescription)")
     onError?(error)
     }
     }
     }*/
    
    
    // MARK:
    // MARK: Get Device ID
    func get_DeviceID() -> String {
        let device = UIDevice.current
        let str_deviceID = device.identifierForVendor?.uuidString
        return str_deviceID!
    }
    
    // MARK:Blank check
    func checkIfBlank(strString : String) -> String {
        
        if strString == nil || strString.count == 0 || strString == "" {
            return ""
        }
        
        return strString
        
    }
    
    // MARK:
    // MARK: Get Current Time
    func get_currentTime() -> String
    {
        let timeFormat = DateFormatter.init()
        timeFormat.dateFormat = "mmddHHmmss"
        return timeFormat.string(from: Date.init())
        
    }
    
    func getApprovalCheckListMaster() -> NSArray {
        
        let strBranchSysName = "\(Global().getEmployeeBranchSysName() ?? "")"
        
        if(nsud.value(forKey: "MasterServiceAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "ApprovalCheckListMasterExtSerDcs") is NSArray){
                let aryTemp = (dictMaster.value(forKey: "ApprovalCheckListMasterExtSerDcs") as! NSArray).filter { (task) -> Bool in
                    
                    //return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("true"))  && "\((task as! NSDictionary).value(forKey: "BranchSysName")!)".contains("\(strBranchSysName)")
                    
                    return "\((task as! NSDictionary).value(forKey: "BranchSysName")!)".contains("\(strBranchSysName)")
                    
                }
                
                return aryTemp as NSArray
                
            }
            
        }
        return NSArray()
    }
    
    
    func checkIfStageTypeIsIsnpector(obj : NSManagedObject , checkflowStageType : Bool , checkflowStatus : Bool) -> Bool {
        //---------By Saavan Approval check
        
        var aryApprovalCheckList = NSMutableArray()
        aryApprovalCheckList =  getApprovalCheckListMaster().mutableCopy() as! NSMutableArray
        
        let departmentType = "\(obj.value(forKey: "departmentType") ?? "")"
        let serviceState = "\(obj.value(forKey: "serviceState") ?? "")"
        if departmentType == "Termite" && serviceState == "California" {
            
            if (DeviceType.IS_IPAD && (departmentType == "Termite" && serviceState == "California") && aryApprovalCheckList.count != 0)  {
                
                //---For Show status
                let wdoWorkflowStageId = "\(obj.value(forKey: "wdoWorkflowStageId") ?? "")" == "0" ? "" : "\(obj.value(forKey: "wdoWorkflowStageId") ?? "")"
                let wdoWorkflowStatus = "\(obj.value(forKey: "wdoWorkflowStatus") ?? "")"
                
                //Redirect krna hai Vo Webview View Jaha Sign Edit krega…
                if(wdoWorkflowStageId.count > 0){
                    let aryApprovalCheckListFilterObj = aryApprovalCheckList.filter { (task) -> Bool in
                        return ("\((task as! NSDictionary).value(forKey: "ApprovalCheckListId")!)".contains(wdoWorkflowStageId)  ) }
                    if aryApprovalCheckListFilterObj.count != 0 {
                        let objApprovalCheckList = aryApprovalCheckListFilterObj[0]as! NSDictionary
                        
                        let strWdoWorkflowStageType = "\(objApprovalCheckList.value(forKey: "WdoWorkflowStageType") ?? "")"
                        
                        if(checkflowStageType && checkflowStatus){
                            if strWdoWorkflowStageType.lowercased() == WdoWorkflowStageTypeEnum.Inspector.rawValue.lowercased()  && wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Pending.rawValue.lowercased() {
                                return true
                                
                            }
                        }
                        
                        else if(checkflowStageType){
                            if strWdoWorkflowStageType.lowercased() == WdoWorkflowStageTypeEnum.Inspector.rawValue.lowercased() {
                                if (wdoWorkflowStatus.lowercased() ==  WdoWorkflowStatusEnum.Complete.rawValue.lowercased()) || (wdoWorkflowStatus.lowercased() ==  WdoWorkflowStatusEnum.Completed.rawValue.lowercased())  {
                                    return false
                                }else{
                                    return true
                                }
                                
                                
                            }
                        }
                    }
                }
            }
        }
        
        return false
    }
    
    func wdoWorkOrderStatus(woObj: NSManagedObject) -> Bool {
        
        if woObj.value(forKey: "isBatchReleased") as? String == "true" || woObj.value(forKey: "isBatchReleased") as? String == "True" || woObj.value(forKey: "isBatchReleased") as? String == "1"{
            
            return true
            
        }else{
            
            //---For Show status
            let wdoWorkflowStageId = "\(woObj.value(forKey: "wdoWorkflowStageId") ?? "")" == "0" ? "" : "\(woObj.value(forKey: "wdoWorkflowStageId") ?? "")"
            //let wdoWorkflowStatus = "\(woObj.value(forKey: "wdoWorkflowStatus") ?? "")"
            let wdoWorkOrderStatus = "\(woObj.value(forKey: "workorderStatus") ?? "")"
            
            if(wdoWorkflowStageId.count > 0 && (wdoWorkOrderStatus.lowercased() == "completed" || wdoWorkOrderStatus.lowercased() == "complete")){
                
                if checkIfStageTypeIsIsnpector(obj: woObj, checkflowStageType: true, checkflowStatus: false) {
                    
                    // Is Inspector Stage Type mttlb WorkOrder Edit kr skte hain to esko false return kr raha hun.
                    
                    return false
                    
                }else{
                    
                    return true
                    
                }
                
            }else{
                
                return false
                
            }
        }
    }
    
    
    // MARK: - Convert String To Dictionary Function
    
    class func convertToDictionary(text: String) -> NSDictionary {
        do {
            
            let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? Dictionary<String, Any>
            // print(data)
            let firstElement: NSDictionary = data! as NSDictionary
            return firstElement
        } catch {
            print(error.localizedDescription)
        }
        return NSDictionary()
    }
    
    
    class func convertToArray(text: String) ->NSArray {
        do {
            let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? Array<Dictionary<String, Any>>
            //print(data)
            let firstElement: NSArray = data! as NSArray
            return firstElement
        }
        catch{
            print ("Handle error")
        }
        return NSArray()
    }
    
    class func GetNewDashBoardArray(dictJson:NSDictionary,url:String,responseStringComing:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        
        let url = URL(string: url)
        
        var requestLocal: NSMutableURLRequest! = nil
        
        if let url = url {
            
            requestLocal = NSMutableURLRequest(
                
                url: url,
                
                cachePolicy: .useProtocolCachePolicy,
                
                timeoutInterval: 60.0)
            
        }
        
        
        
        requestLocal.httpMethod = "GET"
        
        requestLocal.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        requestLocal.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
        
        requestLocal.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
        
        requestLocal.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
        
        requestLocal.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
        
        
        requestLocal.addValue("application/json", forHTTPHeaderField: "Accept")
        
        requestLocal.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
        
        requestLocal.addValue("IOS", forHTTPHeaderField: "Browser")
        
        NSURLConnection.sendAsynchronousRequest(requestLocal as URLRequest, queue: OperationQueue.main) { (responce, data, error) in
            
            do {
                
                
                
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray {
                    
                    
                    
                    //   print(jsonResult)
                    
                    
                    
                    DispatchQueue.main.async {
                        
                        
                        
                        let dictdata = NSMutableDictionary()
                        
                        
                        
                        dictdata.setValue(jsonResult, forKey: "data")
                        
                        
                        
                        OnResultBlock((dictdata) ,true)
                        
                        
                        
                    }
                    
                    
                    
                    //Use GCD to invoke the completion handler on the main thread
                    
                    
                    
                    
                    
                    
                    
                }else{
                    
                    let dic = NSMutableDictionary.init()
                    
                    
                    
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    
                    
                    
                    OnResultBlock(dic,false)
                    
                }
                
                
                
            } catch let error as NSError {
                
                
                
                DispatchQueue.main.async {
                    
                    
                    
                    
                    
                    let dic = NSMutableDictionary.init()
                    
                    
                    
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    
                    
                    
                    OnResultBlock(dic,false)
                    
                    
                    
                    print(error.localizedDescription)
                    
                    
                    
                }
                
                
                
            }
            
            //   OnResultBlock((ResponseDict) ,true)
            
            
            
        }
        
        
        
    }
    
    class func GetNewDashBoard(dictJson:NSDictionary,url:String,responseStringComing:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        
        let url = URL(string: url)
        
        var requestLocal: NSMutableURLRequest! = nil
        
        if let url = url {
            
            requestLocal = NSMutableURLRequest(
                
                url: url,
                
                cachePolicy: .useProtocolCachePolicy,
                
                timeoutInterval: 60.0)
            
        }
        
        
        
        requestLocal.httpMethod = "GET"
        
        requestLocal.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        requestLocal.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
        
        requestLocal.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
        
        requestLocal.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
        
        requestLocal.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
        
        
        requestLocal.addValue("application/json", forHTTPHeaderField: "Accept")
        
        requestLocal.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
        
        requestLocal.addValue("IOS", forHTTPHeaderField: "Browser")
        
        NSURLConnection.sendAsynchronousRequest(requestLocal as URLRequest, queue: OperationQueue.main) { (responce, data, error) in
            
            do {
                
                
                
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    
                    
                    
                    //   print(jsonResult)
                    
                    
                    
                    DispatchQueue.main.async {
                        
                        
                        
                        let dictdata = NSMutableDictionary()
                        
                        
                        
                        dictdata.setValue(jsonResult, forKey: "data")
                        
                        
                        
                        OnResultBlock((dictdata) ,true)
                        
                        
                        
                    }
                    
                    
                    
                    //Use GCD to invoke the completion handler on the main thread
                    
                    
                    
                    
                    
                    
                    
                }else{
                    
                    let dic = NSMutableDictionary.init()
                    
                    
                    
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    
                    
                    
                    OnResultBlock(dic,false)
                    
                }
                
                
                
            } catch let error as NSError {
                
                
                
                DispatchQueue.main.async {
                    
                    
                    
                    
                    
                    let dic = NSMutableDictionary.init()
                    
                    
                    
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    
                    
                    
                    OnResultBlock(dic,false)
                    
                    
                    
                    print(error.localizedDescription)
                    
                    
                    
                }
                
                
                
            }
            
            //   OnResultBlock((ResponseDict) ,true)
            
            
            
        }
        
        
        
    }
    
    // MARK: - Add headers when using a URLRequest
    
    class func postDataWithHeadersToServerDevice(dictJson:NSDictionary,url:String,strType:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        
        
        let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let headerss: HTTPHeaders = ["BranchId": Global().strEmpBranchID(),
                                     "BranchSysName": Global().strEmpBranchSysName(),
                                     "IsCorporateUser": Global().strIsCorporateUser(),
                                     "ClientTimeZone": Global().strClientTimeZone(),
                                     "Accept": "application/json",
                                     "VisitorIP": Global().getIPAddress(),
                                     "IpAddress": Global().getIPAddress(),
                                     "Browser": "IOS",
                                     "HrmsCompanyId": "\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")",
                                     "EmployeeNumber": "\(dict.value(forKeyPath: "EmployeeNumber") ?? "")",
                                     "EmployeeId": "\(dict.value(forKeyPath: "EmployeeId") ?? "")",
                                     "CreatedBy": "\(dict.value(forKeyPath: "CreatedBy") ?? "")",
                                     "CoreCompanyId": "\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")",
                                     "CompanyId": "\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")",
                                     "EmployeeName": "\(dict.value(forKeyPath: "EmployeeName") ?? "")",
                                     "EmployeeEmail": "\(dict.value(forKeyPath: "EmployeeEmail") ?? "")",
                                     "SalesProcessCompanyId": "\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")",
                                     "CompanyKey": "\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")"]
        
        if let url = URL(string: url) {
            
            AF.request(url, method: .post, parameters: dictJson as? Parameters, encoding: JSONEncoding.default, headers: headerss).responseJSON { response in
                
                switch response.result {
                case .success:
                    
                    if let value = response.value {
                        
                        debugPrint(response)
                        
                        if(strType == "dict")
                        {
                            if(value is NSDictionary){
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(value, forKey: "data")
                                dictdata .setValue("true", forKey: "Success")
                                OnResultBlock((dictdata) ,true)
                            }
                            else{
                                
                                let dic = NSMutableDictionary.init()
                                dic .setValue("false", forKey: "Success")
                                OnResultBlock(dic,false)
                                
                            }
                        }
                        else if(strType == "array")
                        {
                            if(value is NSArray){
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(value, forKey: "data")
                                dictdata .setValue("true", forKey: "Success")
                                OnResultBlock((dictdata) ,true)
                            }
                            else{
                                
                                let dic = NSMutableDictionary.init()
                                dic .setValue("false", forKey: "Success")
                                OnResultBlock(dic,false)
                                
                            }
                        }
                        else if(strType == "bool")
                        {
                            if(value is Bool){
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(value, forKey: "data")
                                dictdata .setValue("true", forKey: "Success")
                                OnResultBlock((dictdata) ,true)
                            }
                            else{
                                
                                let dic = NSMutableDictionary.init()
                                dic .setValue("false", forKey: "Success")
                                OnResultBlock(dic,false)
                                
                            }
                        }
                        else if(strType == "string")
                        {
                            if(value is NSString){
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(value, forKey: "data")
                                dictdata .setValue("true", forKey: "Success")
                                OnResultBlock((dictdata) ,true)
                            }
                            else{
                                
                                let dic = NSMutableDictionary.init()
                                dic .setValue("false", forKey: "Success")
                                OnResultBlock(dic,false)
                                
                            }
                        }
                        
                    }
                    
                case .failure(let error):
                    
                    DispatchQueue.main.async() {
                        
                        print(error)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("false", forKey: "Success")
                        OnResultBlock(dic,false)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    
    class func logAppErrorAPI(strErrorMsg : String , typeee : String) {
        
        let dictToSend = NSMutableDictionary()
        dictToSend.setValue(strErrorMsg, forKey: "ErrorMsg")
        dictToSend.setValue(typeee, forKey: "IsError")
        
        if(isInternetAvailable()){
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.CoreServiceModule.ServiceUrl")!)" + "/api/MobileAppToCore/LogAppError"
            
            WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strURL, strType: "bool"){ (responce, status) in
                
                print(responce)
                
                if(status)
                {
                    
                    if responce.value(forKey: "Success") as! NSString == "true"
                    {
                        
                        //   showAlertWithoutAnyAction(strtitle: "Info", strMessage: "Success", viewcontrol: self)
                        
                    }else{
                        
                        //   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                }else{
                    
                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
                
            }
            
        }else{
            
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        
    }
    
    class func postDataWithHeadersToServer(dictJson:NSDictionary,url:String,strType:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        
        let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let headerss: HTTPHeaders = ["BranchId": Global().strEmpBranchID(),
                                     "BranchSysName": Global().strEmpBranchSysName(),
                                     "IsCorporateUser": Global().strIsCorporateUser(),
                                     "ClientTimeZone": Global().strClientTimeZone(),
                                     "Accept": "application/json",
                                     "VisitorIP": Global().getIPAddress(),
                                     "IpAddress": Global().getIPAddress(),
                                     "Browser": "IOS",
                                     "HrmsCompanyId": "\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")",
                                     "EmployeeNumber": "\(dict.value(forKeyPath: "EmployeeNumber") ?? "")",
                                     "EmployeeId": "\(dict.value(forKeyPath: "EmployeeId") ?? "")",
                                     "CreatedBy": "\(dict.value(forKeyPath: "CreatedBy") ?? "")",
                                     "CoreCompanyId": "\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")",
                                     "CompanyId": "\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")",
                                     "EmployeeName": "\(dict.value(forKeyPath: "EmployeeName") ?? "")",
                                     "EmployeeEmail": "\(dict.value(forKeyPath: "EmployeeEmail") ?? "")",
                                     "SalesProcessCompanyId": "\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")",
                                     "CompanyKey": "\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")"]
        
        if let url = URL(string: url) {
            
            AF.request(url, method: .post, parameters: dictJson as? Parameters, encoding: JSONEncoding.default, headers: headerss).responseJSON { response in
                
                switch response.result {
                case .success:
                    
                    if let value = response.value {
                        
                        debugPrint(response)
                        
                        if(strType == "dict")
                        {
                            if(value is NSDictionary){
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(value, forKey: "data")
                                dictdata .setValue("true", forKey: "Success")
                                OnResultBlock((dictdata) ,true)
                            }
                            else{
                                
                                let dic = NSMutableDictionary.init()
                                dic .setValue("false", forKey: "Success")
                                OnResultBlock(dic,false)
                                
                            }
                        }
                        else if(strType == "array")
                        {
                            if(value is NSArray){
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(value, forKey: "data")
                                dictdata .setValue("true", forKey: "Success")
                                OnResultBlock((dictdata) ,true)
                            }
                            else{
                                
                                let dic = NSMutableDictionary.init()
                                dic .setValue("false", forKey: "Success")
                                OnResultBlock(dic,false)
                                
                            }
                        }
                        else if(strType == "bool")
                        {
                            if(value is Bool){
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(value, forKey: "data")
                                dictdata .setValue("true", forKey: "Success")
                                OnResultBlock((dictdata) ,true)
                            }
                            else{
                                
                                let dic = NSMutableDictionary.init()
                                dic .setValue("false", forKey: "Success")
                                OnResultBlock(dic,false)
                                
                            }
                        }
                        else if(strType == "string")
                        {
                            if(value is NSString){
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(value, forKey: "data")
                                dictdata .setValue("true", forKey: "Success")
                                OnResultBlock((dictdata) ,true)
                            }
                            else{
                                
                                let dic = NSMutableDictionary.init()
                                dic .setValue("false", forKey: "Success")
                                OnResultBlock(dic,false)
                                
                            }
                        }
                        else if(strType == "AppointmentPostBasicInfoGet")
                        {
                            
                            var arrOfData = NSArray()
                            let dictdata = NSMutableDictionary()
                            
                            // 03 August Service Basic Api Implementation
                            
                            if value is NSNull
                            {
                                let dic = NSMutableDictionary.init()
                                dic .setValue("\(alertSomeError) ", forKey: "message")
                                OnResultBlock(dic,false)
                            }
                            else
                            {
                                
                                // arrOfData = ((data as AnyObject) as! NSArray)
                                
                                if((value as AnyObject) is NSDictionary)
                                {
                                    
                                    let dictValue = value as! NSDictionary
                                    
                                    if (dictValue.value(forKey: "WorkOrderExtSerDcs") != nil) {
                                        
                                        arrOfData = (dictValue.value(forKey: "WorkOrderExtSerDcs") as! NSArray)
                                        
                                        
                                        if arrOfData.isKind(of: NSArray.self)
                                        {
                                            
                                            dictdata.setValue(value, forKey: "data")
                                            OnResultBlock((dictdata) ,true)
                                            
                                        }
                                        else
                                        {
                                            
                                            let dic = NSMutableDictionary.init()
                                            dic .setValue("\(alertSomeError) ", forKey: "message")
                                            OnResultBlock(dic,false)
                                            
                                        }
                                    }else{
                                        let dic = NSMutableDictionary.init()
                                        dic .setValue("\(alertSomeError) ", forKey: "message")
                                        OnResultBlock(dic,false)
                                    }
                                    
                                }else{
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }
                            
                        }
                        
                    }
                    
                case .failure(let error):
                    
                    DispatchQueue.main.async() {
                        
                        print(error)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("false", forKey: "Success")
                        OnResultBlock(dic,false)
                        
                    }
                    
                }
            }
            
        }
        
    }
    
    class func postRequestWithHeaders(dictJson:NSDictionary,url:String,responseStringComing:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.post.rawValue
            
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            if (nsud.value(forKey: "LoginDetails") != nil)
            {
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
                
            }
            
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            AF.request(request)
                .responseJSON {
                    response in
                    debugPrint(response)
                    
                    switch(response.result) {
                    case .success(_):
                        if let data = response.value
                        {
                            if(responseStringComing == "TimeLine")
                            {
                                if(data is NSDictionary){
                                    
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else{
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }
                            else if(responseStringComing == "TaskVC_CRMNew_iPhone" || responseStringComing == "ActivityVC_CRMNew_iPhone")
                            {
                                if(data is NSDictionary){
                                    
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else{
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }
                            else if(responseStringComing == "EditLeadNewVC"){
                                
                                if(data is Bool){
                                    
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else{
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "AddBlockTimeVC"){
                                if(data is NSDictionary){
                                    
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else{
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }
                            else{
                                
                                let dictdata = NSMutableDictionary()
                                let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                                    responseStringComing == (k as AnyObject)as! String
                                })
                                if status{
                                    
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                    
                                }
                                else {
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                    
                                }
                            }
                        }
                        break
                    case .failure(_):
                        print(response.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                        break
                    }
                    
                }
        }
        
    }
    
    class func postArrayRequestWithHeaders(dictJson:NSMutableArray,url:String,responseStringComing:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.post.rawValue
            
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            if (nsud.value(forKey: "LoginDetails") != nil)
            {
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
                
            }
            
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            AF.request(request)
                .responseJSON {
                    response in
                    debugPrint(response)
                    
                    switch(response.result) {
                    case .success(_):
                        if let data = response.value
                        {
                            if(responseStringComing == "TimeLine")
                            {
                                if(data is NSDictionary){
                                    
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else{
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }else if(responseStringComing == "Success")
                            {
                                if(data is String){
                                    
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(data, forKey: "status")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else{
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }else if(responseStringComing == "TaskVC_CRMNew_iPhone" || responseStringComing == "ActivityVC_CRMNew_iPhone")
                            {
                                if(data is NSDictionary){
                                    
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else{
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }
                            else if(responseStringComing == "EditLeadNewVC"){
                                
                                if(data is Bool){
                                    
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else{
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else{
                                
                                let dictdata = NSMutableDictionary()
                                let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                                    responseStringComing == (k as AnyObject)as! String
                                })
                                if status{
                                    
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                    
                                }
                                else {
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                    
                                }
                            }
                        }
                        break
                    case .failure(_):
                        print(response.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                        break
                    }
                    
                }
        }
        
    }
    
    class func postArrayRequestWithHeadersNewRuchika(dictJson:[String],url:String,responseStringComing:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        
        if let url = URL(string: url) {
            
            var request = URLRequest(url: url)
            
            request.httpMethod = HTTPMethod.post.rawValue
            
            
            
            request.httpMethod = "POST"
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            
            
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            
            
            if (nsud.value(forKey: "LoginDetails") != nil)
                
            {
                
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                
                
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                
                
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
                
                
                
            }
            
            
            
            
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
                
                return
                
            }
            
            request.httpBody = httpBody
            
            
            
            AF.request(request)
            
                .responseJSON {
                    
                    response in
                    
                    debugPrint(response)
                    
                    
                    
                    switch(response.result) {
                        
                    case .success(_):
                        
                        if let data = response.value
                            
                        {
                            
                            if(responseStringComing == "TimeLine")
                                
                            {
                                
                                if(data is NSArray){
                                    
                                    
                                    
                                    let dictdata = NSMutableDictionary()
                                    
                                    dictdata.setValue(data, forKey: "data")
                                    
                                    OnResultBlock((dictdata) ,true)
                                    
                                }
                                
                                else{
                                    
                                    
                                    
                                    let dic = NSMutableDictionary.init()
                                    
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    
                                    OnResultBlock(dic,false)
                                    
                                }
                                
                            }
                            
                            else if(responseStringComing == "Success")
                                    
                            {
                                
                                if(data is String){
                                    
                                    
                                    
                                    let dictdata = NSMutableDictionary()
                                    
                                    dictdata.setValue(data, forKey: "status")
                                    
                                    OnResultBlock((dictdata) ,true)
                                    
                                }
                                
                                else{
                                    
                                    
                                    
                                    let dic = NSMutableDictionary.init()
                                    
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    
                                    OnResultBlock(dic,false)
                                    
                                }
                                
                            }
                            
                            else if(responseStringComing == "TaskVC_CRMNew_iPhone" || responseStringComing == "ActivityVC_CRMNew_iPhone")
                                    
                            {
                                
                                if(data is NSDictionary){
                                    
                                    
                                    
                                    let dictdata = NSMutableDictionary()
                                    
                                    dictdata.setValue(data, forKey: "data")
                                    
                                    OnResultBlock((dictdata) ,true)
                                    
                                }
                                
                                else{
                                    
                                    
                                    
                                    let dic = NSMutableDictionary.init()
                                    
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    
                                    OnResultBlock(dic,false)
                                    
                                }
                                
                            }
                            
                            else if(responseStringComing == "EditLeadNewVC"){
                                
                                
                                
                                if(data is Bool){
                                    
                                    
                                    
                                    let dictdata = NSMutableDictionary()
                                    
                                    dictdata.setValue(data, forKey: "data")
                                    
                                    OnResultBlock((dictdata) ,true)
                                    
                                }
                                
                                else{
                                    
                                    
                                    
                                    let dic = NSMutableDictionary.init()
                                    
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    
                                    OnResultBlock(dic,false)
                                    
                                }
                                
                                
                                
                            }
                            
                            else{
                                
                                
                                
                                let dictdata = NSMutableDictionary()
                                
                                let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                                    
                                    responseStringComing == (k as AnyObject)as! String
                                    
                                })
                                
                                if status{
                                    
                                    
                                    
                                    dictdata.setValue(data, forKey: "data")
                                    
                                    OnResultBlock((dictdata) ,true)
                                    
                                    
                                    
                                }
                                
                                else {
                                    
                                    
                                    
                                    let dic = NSMutableDictionary.init()
                                    
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    
                                    OnResultBlock(dic,false)
                                    
                                    
                                    
                                }
                                
                            }
                            
                        }
                        
                        break
                        
                    case .failure(_):
                        
                        print(response.error ?? 0)
                        
                        let dic = NSMutableDictionary.init()
                        
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        
                        OnResultBlock(dic,false)
                        
                        break
                        
                    }
                    
                    
                    
                }
            
        }
        
        
        
    }
    
    class func getRequestWithHeaders(dictJson:NSDictionary,url:String,responseStringComing:String,strUserDefaultKey:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.get.rawValue
            
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            //            guard let httpBody = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
            //                return
            //            }
            //            request.httpBody = httpBody
            
            AF.request(request)
                .responseJSON {
                    response in
                    debugPrint(response)
                    
                    switch(response.result) {
                    case .success(_):
                        if let data = response.value
                        {
                            let dictdata = NSMutableDictionary()
                            let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                                responseStringComing == (k as AnyObject)as! String
                            })
                            if status{
                                
                                dictdata.setValue(data, forKey: "data")
                                OnResultBlock((dictdata) ,true)
                                
                            }
                            else {
                                
                                let dic = NSMutableDictionary.init()
                                dic .setValue("\(alertSomeError) ", forKey: "message")
                                OnResultBlock(dic,false)
                                
                            }
                            
                        }
                        break
                    case .failure(_):
                        print(response.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                        break
                    }
                    
                }
        }
        
    }
    
    class func getRequestWithHeadersNew(dictJson:NSDictionary,url:String,responseStringComing:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void)
    {
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.get.rawValue
            
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            request.timeoutInterval = 180
            
            //if (nsud.value(forKey: "LoginDetails") != nil)
            if nsud.value(forKey: "LoginDetails") != nil && nsud.value(forKey: "LoginDetails") is NSDictionary
            {
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
                
            }
            
            
            //            guard let httpBody = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
            //                return
            //            }
            //request.httpBody = httpBody
            
            AF.request(request)
                .responseJSON {
                    response in
                    debugPrint(response)
                    
                    if(responseStringComing == "SalesEmail_AppointmentVC")
                    {
                        
                        let dictdata = NSMutableDictionary()
                        
                        if(response.response?.statusCode == 200)
                        {
                            dictdata.setValue("Success", forKey: "data")
                            OnResultBlock((dictdata) ,true)
                        }
                        else
                        {
                            let dic = NSMutableDictionary.init()
                            dic .setValue("\(alertSomeError) ", forKey: "message")
                            OnResultBlock(dic,false)
                        }
                        
                    }
                    switch(response.result) {
                    case .success(_):
                        if let data = response.value
                        {
                            var arrOfData = NSArray()
                            var dictdata = NSMutableDictionary()
                            
                            if(responseStringComing == "AddTaskVC_CRMNew_iPhone")
                            {
                                dictdata = ((data as AnyObject) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                
                                OnResultBlock((dictdata) ,true)
                            }
                            else if(responseStringComing == "TaskVC_CRMNew_iPhone")
                            {
                                if((data as AnyObject) as! Bool)
                                {
                                    dictdata.setValue(String((data as AnyObject) as! Bool), forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }
                            // Akshay 7 Feb 2020 for crm contact association
                            else if(responseStringComing == "CRMConactNew_ContactDetails_Association")
                            {
                                
                                if((data as AnyObject) is NSDictionary)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "SalesAutoMaster_DashBoardVC")
                            {
                                
                                if((data as AnyObject) is NSDictionary)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "CRMConactNew_ContactDetails_About")
                            {
                                
                                if((data as AnyObject) is NSDictionary)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "WDO_See_More")
                            {
                                
                                if((data as AnyObject) is NSDictionary)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "BlockTime_AppointmentVC")
                            {
                                
                                if((data as AnyObject) is NSArray)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "SalesEmail_AppointmentVC")
                            {
                                
                                if((data as AnyObject) is NSString)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "SalesGetLead_AppointmentVC")
                            {
                                
                                if((data as AnyObject) is NSDictionary)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "ServiceGetWO_AppointmentVC")
                            {
                                
                                if((data as AnyObject) is NSDictionary)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "PlumbingGetWO_AppointmentVC")
                            {
                                
                                if((data as AnyObject) is NSDictionary)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }//SalesDynamicForm_AppointmentVC
                            else if(responseStringComing == "ActivityDetailsVC_CRMNew_iPhone" || responseStringComing ==  "TaskDetailsVC_CRMNew_iPhone")
                            {
                                if((data as AnyObject) is NSDictionary)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else if((data as AnyObject) is Bool)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }
                            else if(responseStringComing == "DeleteLeadWebLead")
                            {
                                if((data as AnyObject) is Bool)
                                {
                                    dictdata.setValue("Success", forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }
                            //-----------//
                            else if(responseStringComing == "metodUpdateSubWorkOrderStatus_AppointmentVC")
                            {
                                
                                if((data as AnyObject) is NSDictionary)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dictdata.setValue("Blank", forKey: "data")
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else if(responseStringComing == "GetEmployeeBranchName")
                            {
                                
                                if((data as AnyObject) is NSString)
                                {
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                
                            }
                            else
                            {
                                if data is NSNull
                                {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                                else
                                {
                                    
                                    // arrOfData = ((data as AnyObject) as! NSArray)
                                    
                                    
                                    if((data as AnyObject) is NSArray)
                                    {
                                        arrOfData = ((data as AnyObject) as! NSArray)
                                        
                                        
                                        if arrOfData.isKind(of: NSArray.self)
                                        {
                                            
                                            dictdata.setValue(data, forKey: "data")
                                            OnResultBlock((dictdata) ,true)
                                            
                                        }
                                        else
                                        {
                                            
                                            let dic = NSMutableDictionary.init()
                                            dic .setValue("\(alertSomeError) ", forKey: "message")
                                            OnResultBlock(dic,false)
                                            
                                        }
                                    }
                                    else{
                                        let dic = NSMutableDictionary.init()
                                        dic .setValue("\(alertSomeError) ", forKey: "message")
                                        OnResultBlock(dic,false)
                                    }
                                }
                                
                            }
                            
                        }
                        break
                    case .failure(_):
                        print(response.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                        break
                    }
                    
                }
        }
        
    }
    
    class func callAPIBYGETWithoutKey(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,true)
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,false)
                break
            }
        }
    }
    
    // To Check
    
    class func getRequestCRM(dictJson:NSDictionary,url:String,responseStringComing:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.get.rawValue
            
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            if (nsud.value(forKey: "LoginDetails") != nil)
            {
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
                
            }
            
            //            guard let httpBody = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
            //                return
            //            }
            //            request.httpBody = httpBody
            
            if ((responseStringComing == "GrabLead") || (responseStringComing == "AssociateContactCompany") || responseStringComing == "EmailHistoryMessage" || responseStringComing == "BlockTime")
            {
                
                AF.request(request).responseString {
                    response in
                    //debugPrint(response)
                    
                    switch(response.result) {
                    case .success(_):
                        if let data = response.value
                        {
                            
                            if ((responseStringComing == "GrabLead") || (responseStringComing == "AssociateContactCompany") || responseStringComing == "EmailHistoryMessage" || responseStringComing == "BlockTime")
                            {
                                let dictdata = NSMutableDictionary()
                                
                                var status = Bool()
                                if  (data ).count > 0
                                {
                                    status = true
                                }
                                else
                                {
                                    status = false
                                }
                                
                                
                                if status
                                {
                                    
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                    
                                }
                                else
                                {
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                    
                                }
                                
                            }
                            else
                            {
                                
                                var arrOfData = NSArray()
                                let dictdata = NSMutableDictionary()
                                
                                
                                arrOfData = ((data as AnyObject) as! NSArray)
                                
                                if arrOfData.isKind(of: NSArray.self) {
                                    
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                    
                                }
                                else {
                                    
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                    
                                }
                            }
                            
                        }
                        break
                    case .failure(_):
                        print(response.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,false)
                        break
                    }
                    
                }
            }
            else
            {
                AF.request(request)
                    .responseJSON {
                        response in
                        //debugPrint(response)
                        
                        switch(response.result) {
                        case .success(_):
                            if let data = response.value
                            {
                                //let arrOfData = NSArray()
                                let dictdata = NSMutableDictionary()
                                if(((data as AnyObject) is NSDictionary)){
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                    
                                }else if(((data as AnyObject) is NSArray)){
                                    dictdata.setValue(data, forKey: "data")
                                    OnResultBlock((dictdata) ,true)
                                }
                                else {
                                    let dic = NSMutableDictionary.init()
                                    dic .setValue("\(alertSomeError) ", forKey: "message")
                                    OnResultBlock(dic,false)
                                }
                            }
                            break
                        case .failure(_):
                            print(response.error ?? 0)
                            let dic = NSMutableDictionary.init()
                            dic .setValue("\(alertSomeError) ", forKey: "message")
                            OnResultBlock(dic,false)
                            break
                        }
                        
                    }
            }
        }
        
    }
    
    class func getRequestUsingNsUrlSession(dictJson:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        
        if let url = URL(string: url) {
            
            var request = URLRequest(url: url)
            
            request.httpMethod = HTTPMethod.get.rawValue
            
            request.httpMethod = "GET"
            
            //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            
            //request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            
            
            /*if (nsud.value(forKey: "LoginDetails") != nil)
             
             {
             
             let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
             
             request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
             
             
             
             request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
             
             
             
             request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
             
             
             
             request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
             
             
             
             request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
             
             
             
             
             
             request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
             
             
             
             
             
             request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
             
             
             
             request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
             
             
             
             }*/
            
            request.httpMethod = "GET"
            
            let dataTask = URLSession.shared.dataTask(with: request) {
                
                data,response,error in
                
                print("anything")
                
                do {
                    
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        //   print(jsonResult)
                        
                        DispatchQueue.main.async {
                            
                            let dictdata = NSMutableDictionary()
                            
                            dictdata.setValue(jsonResult, forKey: "data")
                            
                            OnResultBlock((dictdata) ,true)
                            
                        }
                        
                        //Use GCD to invoke the completion handler on the main thread
                        
                        
                        
                    }
                    
                } catch let error as NSError {
                    
                    DispatchQueue.main.async {
                        
                        
                        let dic = NSMutableDictionary.init()
                        
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        
                        OnResultBlock(dic,false)
                        
                        print(error.localizedDescription)
                        
                    }
                    
                }
                
            }
            
            dataTask.resume()
            
            
            
            
        }
        
        
        
    }
    
    class func postDataUsingNsUrlSession(parameters: NotifyCustomerRequest,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        
        if let url = URL(string: url) {
            
            var request = URLRequest(url: url)
            
            request.httpMethod = HTTPMethod.post.rawValue
            
            request.httpMethod = "POST"
            
            //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            
            //request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            
            if (nsud.value(forKey: "LoginDetails") != nil)
            {
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
            }
            
            request.httpMethod = "POST"
            
            request.httpBody = parameters.dictionaryData
            
            let dataTask = URLSession.shared.dataTask(with: request) {
                
                data,response,error in
                
                print("anything")
                
                do {
                    
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        //   print(jsonResult)
                        
                        DispatchQueue.main.async {
                            
                            let dictdata = NSMutableDictionary()
                            
                            dictdata.setValue(jsonResult, forKey: "data")
                            
                            OnResultBlock((dictdata) ,true)
                            
                        }
                        
                        //Use GCD to invoke the completion handler on the main thread
                        
                        
                        
                    }
                    
                } catch let error as NSError {
                    
                    DispatchQueue.main.async {
                        
                        
                        let dic = NSMutableDictionary.init()
                        
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        
                        OnResultBlock(dic,false)
                        
                        print(error.localizedDescription)
                        
                    }
                    
                }
                
            }
            
            dataTask.resume()
            
            
            
            
        }
        
        
        
    }
    
    // MARK: --------------- Target Code -----------------
    
    @objc  func deleteTargetImageDataFromDB(strLeadId: String, strHeaderTitle: String, strTargetSysName: String )
    {
        
        let arrayOfImagesSales = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && targetSysName == %@", strLeadId, strHeaderTitle, strTargetSysName))
        
        for i in 0 ..< arrayOfImagesSales.count
        {
            var obj = NSManagedObject()
            obj = arrayOfImagesSales[i] as! NSManagedObject
            
            let context = getContext()
            //save the object
            do
            {
                context.delete(obj)
                try context.save()
            }
            catch _ as NSError
            {
                
            }catch {
            }
        }
    }
    @objc  func fetchTargetImages(strLeadId: String, strHeaderTitle: String ) -> NSArray
    {
        
        let arrayOfImagesSales = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@", strLeadId, strHeaderTitle))
        
        return arrayOfImagesSales
        
    }
    @objc  func saveTargetImageToDB(strLeadId: String, strUserName: String , strCompanyKey: String , arrTargetImageDetail: NSArray )
    {
        for k in 0 ..< arrTargetImageDetail.count
        {
            
            if arrTargetImageDetail[k] is NSDictionary
            {
                
                let dictOfData = arrTargetImageDetail[k] as! NSDictionary
                
                // Deleting Before Save
                //deleteAllTargetImageDetail(strWoId: strLeadId)
                
                // Saving Data in DB
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                
                
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("leadImagePath")
                arrOfKeys.add("leadImageType")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("targetImageDetailId")
                arrOfKeys.add("modifiedDate")
                arrOfKeys.add("leadId")
                arrOfKeys.add("leadImageCaption")
                arrOfKeys.add("descriptionImageDetail")
                arrOfKeys.add("latitude")
                arrOfKeys.add("longitude")
                arrOfKeys.add("targetSysName")
                arrOfKeys.add("leadCommercialTargetId")
                arrOfKeys.add("mobileTargetId")
                arrOfKeys.add("mobileTargetImageId")
                arrOfKeys.add("isSync")
                arrOfKeys.add("isDelete")
                
                
                arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")" )
                arrOfValues.add(strCompanyKey )
                arrOfValues.add(strUserName)
                arrOfValues.add("\(dictOfData.value(forKey: "LeadImagePath") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "LeadImageType") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "ModifiedBy") ?? "")" )
                
                if "\(dictOfData.value(forKey: "TargetImageDetailId") ?? "")" == "0" {
                    
                    arrOfValues.add("")
                    
                }else{
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "TargetImageDetailId") ?? "")" )
                    
                }
                
                arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")" )
                //arrOfValues.add("\(dictOfData.value(forKey: "LeadId") ?? "")" )
                arrOfValues.add("\(strLeadId)" )
                
                arrOfValues.add("\(dictOfData.value(forKey: "LeadImageCaption") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "DescriptionImageDetail") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "Latitude") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "Longitude") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "TargetSysName") ?? "")" )
                
                if "\(dictOfData.value(forKey: "LeadCommercialTargetId") ?? "")" == "0" {
                    
                    arrOfValues.add("")
                    
                }else{
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "LeadCommercialTargetId") ?? "")" )
                    
                }
                
                if "\(dictOfData.value(forKey: "MobileTargetId") ?? "")" == "0" {
                    
                    arrOfValues.add("")
                    
                }else{
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileTargetId") ?? "")" )
                    
                }
                
                if "\(dictOfData.value(forKey: "MobileTargetImageId") ?? "")" == "0" {
                    
                    arrOfValues.add("")
                    
                }else{
                    
                    arrOfValues.add("\(dictOfData.value(forKey: "MobileTargetImageId") ?? "")" )
                    
                }
                
                arrOfValues.add("\(dictOfData.value(forKey: "IsSync") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "IsDelete") ?? "")" )
                
                saveDataInDB(strEntity: "TargetImageDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
        }
    }
    @objc func deleteAllTargetImageDetail(strWoId : String)  {
        
        deleteAllRecordsFromDB(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
        
    }
    
    @objc  func saveCRMImageToDB(strLeadId: String, strUserName: String , strCompanyKey: String , arrCRMImageDetail: NSArray )
    {
        for k in 0 ..< arrCRMImageDetail.count
        {
            
            if arrCRMImageDetail[k] is NSDictionary
            {
                
                let dictOfData = arrCRMImageDetail[k] as! NSDictionary
                
                var strImagePath = dictOfData.value(forKey: "Path") as! String
                strImagePath = strImagePath.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
                strImagePath = strImagePath.replacingOccurrences(of: "/", with: "\\", options: NSString.CompareOptions.literal, range: nil)
                
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                
                arrOfKeys.add("leadMediaId")
                arrOfKeys.add("title")
                arrOfKeys.add("path")
                arrOfKeys.add("docTypeSysName")
                arrOfKeys.add("mediaSourceSysName")
                arrOfKeys.add("fileType")
                arrOfKeys.add("isImageSyncforMobile")
                arrOfKeys.add("isSync")
                arrOfKeys.add("leadId")
                arrOfKeys.add("userName")
                arrOfKeys.add("companyKey")
                
                
                arrOfKeys.add("cRMImageCaption")
                arrOfKeys.add("cRMImageDescription")
                arrOfKeys.add("isAddendum")
                arrOfKeys.add("isInternalUsage")
                arrOfKeys.add("isImageEdited")
                
                
                arrOfValues.add("\(dictOfData.value(forKey: "LeadMediaId") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "Title") ?? "")" )
                arrOfValues.add(strImagePath)
                arrOfValues.add("\(dictOfData.value(forKey: "DocTypeSysName") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "MediaSourceSysName") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "FileType") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "IsImageSyncforMobile") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "IsSync") ?? "")" )
                arrOfValues.add(strLeadId)
                arrOfValues.add(strUserName)
                arrOfValues.add(strCompanyKey)
                
                arrOfValues.add("\(dictOfData.value(forKey: "CRMImageCaption") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "CRMImageDescription") ?? "")" )
                arrOfValues.add("\(dictOfData.value(forKey: "IsAddendum") ?? "")" == "1" ? "true" : "false")
                arrOfValues.add("\(dictOfData.value(forKey: "IsInternalUsage") ?? "")" == "1" ? "true" : "false")
                arrOfValues.add("\(dictOfData.value(forKey: "IsImageEdited") ?? "")" == "1" ? "true" : "false")
                
                
                
                
                saveDataInDB(strEntity: "CRMImagesDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
        }
    }
    
    
    class func postRequestWithHeadersResponseString(dictJson:NSDictionary,url:String,responseStringComing:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.post.rawValue
            
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            if (nsud.value(forKey: "LoginDetails") != nil)
            {
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
                
            }
            
            
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            var strEmployeeId = String()
            
            if let value = dictLoginData.value(forKeyPath: "EmployeeId") {
                
                strEmployeeId = "\(value)"
                
            }
            request.addValue(strEmployeeId, forHTTPHeaderField: "EmployeeId")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            AF.request(request).responseString {
                response in
                debugPrint(response)
                
                switch(response.result) {
                case .success(_):
                    if let data = response.value
                    {
                        /*let dictdata = NSMutableDictionary()
                         
                         let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                         responseStringComing == (k as AnyObject)as! String
                         })*/
                        let dictdata = NSMutableDictionary()
                        
                        var status = Bool()
                        if  data.count > 0
                        {
                            status = true
                        }
                        else
                        {
                            status = false
                        }
                        
                        
                        if status
                        {
                            
                            dictdata.setValue(data, forKey: "data")
                            OnResultBlock((dictdata) ,true)
                            
                        }
                        else
                        {
                            
                            let dic = NSMutableDictionary.init()
                            dic .setValue("\(alertSomeError) ", forKey: "message")
                            OnResultBlock(dic,false)
                            
                        }
                        
                    }
                    break
                case .failure(_):
                    print(response.error ?? 0)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("\(alertSomeError) ", forKey: "message")
                    OnResultBlock(dic,false)
                    break
                }
                
            }
            
            
        }
        
    }
    
    class func uploadImageServer(JsonData:String,JsonDataKey:String, data : Data ,strDataName : String,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        
        let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        AF.upload(multipartFormData: { (multipartdata) in
            
            if(data.count != 0){
                multipartdata.append(data, withName: "File", fileName: "\(strDataName)", mimeType: "image/jpeg") // application/octet-stream
            }
            //multipartdata.append(Data(JsonData.utf8), withName: "\(JsonDataKey)")
            
        }, to: url).responseJSON { response in
            
            if let data = response.value
            {
                let dictdata = NSMutableDictionary.init()
                
                dictdata.setValue(data, forKey: "data")
                OnResultBlock((dictdata) ,true)
                
            }else{
                let dic = NSMutableDictionary.init()
                dic.setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,false)
            }
        }
        
        
    }
    
    class func getRequestWithHeadersWithMultipleImage(JsonData:String,JsonDataKey:String, data : Data ,strDataName : String,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        
        let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
        var headers: HTTPHeaders = ["": ""]
        headers = ["BranchId": Global().strEmpBranchID(),
                   "BranchSysName": Global().strEmpBranchSysName(),
                   "IsCorporateUser": Global().strIsCorporateUser(),
                   "ClientTimeZone": Global().strClientTimeZone(),
                   "Accept": "application/json",
                   "VisitorIP": Global().getIPAddress(),
                   "Browser": "IOS",
                   "HrmsCompanyId": "\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")",
                   "EmployeeNumber": "\(dict.value(forKeyPath: "EmployeeNumber") ?? "")",
                   "EmployeeId": "\(dict.value(forKeyPath: "EmployeeId") ?? "")",
                   "CreatedBy": "\(dict.value(forKeyPath: "CreatedBy") ?? "")",
                   "CoreCompanyId": "\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")",
                   "EmployeeName": "\(dict.value(forKeyPath: "EmployeeName") ?? "")",
                   "SalesProcessCompanyId": "\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")",
                   "CompanyKey": "\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")"]
        
        
        AF.upload(multipartFormData: { (multipartdata) in
            
            if(data.count != 0){
                multipartdata.append(data, withName: "File", fileName: "\(strDataName)", mimeType: "image/jpeg") // application/octet-stream
            }
            multipartdata.append(Data(JsonData.utf8), withName: "\(JsonDataKey)")
            
        }, to: url).responseJSON { response in
            
            if let data = response.value
            {
                let dictdata = NSMutableDictionary.init()
                
                dictdata.setValue(data, forKey: "data")
                OnResultBlock((dictdata) ,true)
                
            }else{
                let dic = NSMutableDictionary.init()
                dic.setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,false)
            }
        }
        
        
    }
    
    /*class func getRequestWithHeadersWithMultipleImage(JsonData:String,JsonDataKey:String, data : Data ,strDataName : String,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
     let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
     var headers: HTTPHeaders = ["": ""]
     headers = ["BranchId": Global().strEmpBranchID(),
     "BranchSysName": Global().strEmpBranchSysName(),
     "IsCorporateUser": Global().strIsCorporateUser(),
     "ClientTimeZone": Global().strClientTimeZone(),
     "Accept": "application/json",
     "VisitorIP": Global().getIPAddress(),
     "Browser": "IOS",
     "HrmsCompanyId": "\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")",
     "EmployeeNumber": "\(dict.value(forKeyPath: "EmployeeNumber") ?? "")",
     "EmployeeId": "\(dict.value(forKeyPath: "EmployeeId") ?? "")",
     "CreatedBy": "\(dict.value(forKeyPath: "CreatedBy") ?? "")",
     "CoreCompanyId": "\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")",
     "EmployeeName": "\(dict.value(forKeyPath: "EmployeeName") ?? "")",
     "SalesProcessCompanyId": "\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")",
     "CompanyKey": "\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")"]
     
     if let url = URL(string: url) {
     Alamofire.upload(multipartFormData:{ multipartFormData in
     if(data.count != 0){
     // multipartFormData.append(data, withName: "File")
     multipartFormData.append(data, withName: "File", fileName: "\(strDataName)", mimeType: "image/jpeg") // application/octet-stream
     }
     multipartFormData.append(Data(JsonData.utf8), withName: "\(JsonDataKey)")
     },
     usingThreshold:UInt64.init(),
     to:url,
     method:.post,
     headers:nil,
     encodingCompletion: { encodingResult in
     switch encodingResult {
     case .success(let upload, _, _):
     upload.responseJSON { response in
     
     if let data = response.result.value
     {
     let dictdata = NSMutableDictionary.init()
     
     dictdata.setValue(data, forKey: "data")
     OnResultBlock((dictdata) ,true)
     
     }else{
     let dic = NSMutableDictionary.init()
     dic.setValue("\(alertSomeError) ", forKey: "message")
     OnResultBlock(dic,false)
     }
     }
     case .failure( _):
     let dic = NSMutableDictionary.init()
     dic.setValue("\(alertSomeError) ", forKey: "message")
     OnResultBlock(dic,false)
     }
     })
     }
     }*/
    
}


extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
    
    var dictionaryData: Data? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return data
    }
    
}
