//
//  Constant.swift
//  Petnod
//
//  Created by admin on 2/7/17.
//  Copyright © 2017 admin. All rights reserved.
//  Saavann Patidar
//  Saavan Patidar 2021

import Foundation
import CoreTelephony
import MessageUI
import CoreData
import MapKit
import SystemConfiguration
import AVKit


// MARK:------
var hasNotch: Bool {
    guard #available(iOS 11.0, *), let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else { return false }
    if UIDevice.current.orientation.isPortrait {
        return window.safeAreaInsets.top >= 44
    } else {
        return window.safeAreaInsets.left > 0 || window.safeAreaInsets.right > 0
    }
}
var notch_TopSafeArea               = 44.0
var notch_BottomSafeArea            = 34.0
var WithoutNotch_TopSafeArea        = 20.0
var WithoutNotch_BottomSafeArea     = 8.0
// MARK: -----
var nsud = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var mainStoryboard : UIStoryboard = UIStoryboard()
var storyboard_name = String()
var storyboardNewCRM = UIStoryboard.init(name: "CRM_New_iPhone", bundle: nil)
var storyboardNewCRMContact = UIStoryboard.init(name: "CRMContact_New_iPhone", bundle: nil)
var storyboardMainiPhone = UIStoryboard.init(name: "Main", bundle: nil)
var storyboardCRMiPhone = UIStoryboard.init(name: "CRM", bundle: nil)
var storyboardTask = UIStoryboard.init(name: "Task-Activity", bundle: nil)
var storyboardLeadProspect = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
var storyboardDashBoard = UIStoryboard.init(name: "DashBoard", bundle: nil)
var storyboardAppointment = UIStoryboard.init(name: "Appointment", bundle: nil)
var storyboardNewSalesSelectService = UIStoryboard.init(name: "NewSales_SelectService", bundle: nil)
var storyboardNewSalesGeneralInfo = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
var storyboardNewSalesInspection = UIStoryboard.init(name: "NewSales_Inspection", bundle: nil)
var storyboardNewSalesSendMail = UIStoryboard.init(name: "NewSales_SendEmail", bundle: nil)

var btnOverLay = UIButton()
var dict_Login_Data = NSMutableDictionary()
// MARK: Aler Messages
var strInternetImage = UIImage(named:"no_internet")!
var strDataNotFoundImage = UIImage(named:"no_data")!
var strSomthingWantWrong = UIImage(named:"404")!

var appThemeColor = "5FB2AF"
var orangeColor = "f5821f"
var primaryColor = "007a48"
var primaryGrayColor = "f7f7f7"
var primaryFontLatoBold = "Lato-Bold"
var primaryFontLatoRegular = "Lato-Regular"

var application_Version : String = "1.0.0"
var application_ID : String = "https://itunes.apple.com/in/app/greengene/id1429856765?mt=8"
var Platform = "iPhone"
var Website = "http://www.goanaturalgas.com/"

var flowTypeWdoSalesService = "WdoSalesServiceFlow"
var flowTypeWdoProblemId = "ProblemId"
var strSelectString = "----Select----"
var strOtherString = "Others"
var strOtherStringlowerCase = "other"
var AppName = "Goa Natural Gas (P) Ltd"
var alertMessage = "Alert!" //---Select---
var alertNoWo = "Unable to fetch data. Please refresh Appointments and try again."
var alertMailComposer = "Please configure your email-id in device to send email"
var Alert_SelectDevice = "Please select Device"
var alertInfo = "Information!"
var alertInternet = "No Internet Connection, try later!"
var alertDataNotFound = "Data is not Available!"
var alertSomeError = "Something went wrong please try again!"
var alertCalling = "Your device doesn't support this feature."
var alertLogout = "Are you sure want to logout?"
var Alert_OTP = "OTP required!"
var Alert_Required = "Required!"
var Alert_RequiredText = "Required!"
var Alert_Password = "Six digit's  required!"
var Alert_Mobile_limit = "Mobile number invalid!"
var Alert_Whatsapp_limit = "Whatsapp number invalid!"
var Alert_landline_limit = "Landline number invalid!"
var Alert_PinCode_limit = "Pincode  invalid!"
var Alert_EmailAddress = "Email address invalid!"
var Alert_CPassword = "Password did't match!"
var Alert_SelectOptionProfile = "Please Select an option for profile image"
var Alert_SelectOptionAddress = "Please Select an option for address proof"
var Alert_SelectOptionID = "Please Select an option for ID proof"
var Alert_SelectOptionOther = "Please Select an option for other documents"
var Alert_Gallery = "Gallery"
var Alert_Camera = "Camera"
var Alert_Preview = "Preview"
var Alert_SelectAddressDocument = "Please select address proof!"
var Alert_ID_proof = "Please select ID proof!"
var Alert_MeterImage = "Please capture meter image!"
var Alert_CameraAlert = "You don't have camera!"
var Alert_ReadingImageLimit = "Max limit reached!"
var Alert_PasswordLimit = "Password should be minimum 6 digits!"
var Alert_ProfileImage = "Profile Image required!"
var Alert_AadharNumberValid = "Aadhar number invalid!"
var Alert_PanNumberValid = "PAN card number invalid!"
var Alert_MeterReading = "Meter reading required!"
var Alert_MeterNumber = "Meter number not available,Please contact to admin."
var Alert_ReferCode = "ReferCode not available,Please contact to admin."
var Alert_AreaTitle = "Please enter Title."
var Alert_NoSubArea = "No Area Exist."
var Alert_SelectArea = "Please Select Area"
var Alert_SelectQty = "Please enter quantity"
var Alert_SelectUnitOfMeasurement = "Please enter Unit of Measurement"
var Alert_SelectConcentration = "Please select Concentration"
var Alert_SelectUndilutedQty = "Please enter Undiluted Quantity"
var Alert_SelectEPAReg = "Please enter EPA Reg. #"
var Alert_SelectTarget = "Please enter Target"
//var Alert_SelectQty = "Please enter Quantity"
var Alert_EnterComment = "Please enter Comment"
var Alert_EnterDeviceName = "Please enter Device Name"
var Alert_EnterBarcode = "Please enter Barcode"
var Alert_SelectType = "Please select Device Type"
var Alert_EnterTitle = "Please enter Title"
var Alert_SelectDoc = "Please Select Documents"
var Alert_DataLimit = "Please select less then 2 MB"
var Alert_DeviceExist = "Device already exist."
var Alert_AreaExist = "Area already exist"
var Alert_DeviceReason = "Please enter reason"
var Alert_DeviceBarcode = "Please enter barcode"
var Alert_DeviceComment = "Please enter comment"
var Alert_SelectAreaOnDevice = "Please select Area"
var Alert_SelectDisclaimer = "Please select disclaimer"
var Alert_EnterDisclaimerDesc = "Please enter description"
var Alert_EnterDisclaimerFillin = "Please enter fillin"
var Alert_EnterIssuesCode = "Please enter Finding"
var Alert_SelectSubsectionCode = "Please select Finding Code"
var Alert_EnterFillingSubsection = "Please enter subsection Filling"
var Alert_EnterFinding = "Please enter Finding Description"
var Alert_SelectRecommendationcode = "Please select recommendation code"
var Alert_EnterFillingRecommendation = "Please enter recommendation Filling"
var Alert_EnterRecommendation = "Please enter recommendation"
var Alert_SelectTipWarrantyType = "Please select tip warranty type"
var Alert_YearOfStructure = "Please enter valid year of structure"
var Alert_EnterMonthlyIncome = "Please enter Tip monthly amount"
var Alert_DisclaimerExist = "Disclaimer alredy exist."
var Alert_AreaAndParentAreaSame = "Area and parent area are same."
var Alert_ParentArea = "Sub area can't be a parent area"
var Alert_EnterMonthlyIncomeZero = "Please enter non zero monthly income"
var Alert_DataSynced = "Data synced successfully"
var Alert_CreateBid = "Please create bid to proceed."
var Alert_SelectGeneralNotes = "Please select general notes"
var Alert_EnterGeneralNotesDesc = "Please enter general notes description"
var Alert_SelectFollowUpInspection = "Please select follow up inspection"
var Alert_GeneralNotesExist = "General notes alredy exist."
var Alert_SelectServiceReportType = "Please select service report type."
var Alert_SelectOption = "Please select an option."
var alertRemove = "There are associated entities. Do you won't to replace with the existing ..??"
var alertNoGraphAvailable = "No Graph available for this work order."
var alertNoCustomMessageAvailable = "Custom Message not selected.Please go to Settings and select custom message."
var alertFilterClear = "Clearing filter will set filter to default values. Are you sure to continue.?"
var alertFilterClearNew = "Clearing filter will clear all fields. Are you sure,want to continue?"
var alertAudioVideoPermission = "If Audio Permission not allowed, Please go to settings and allow, If already allowed click on cancel"
// MARK: New Sales Flow Alert Messages
var alertSyncSalesMaster = "No Master Data Available.\n Please check your internet connection and Sync Masters again from menu options."
var alertReLaunchApp = "Either Login or Master Data Not Available.\n Please close the app and relaunch it again and Sync Masters from menu options."
var alertCategory = "Please select category"
var alertPackage = "Please select package"
var alertInitialPrice = "Enter initial price"
var alertInitialPriceForDiscount = "Discount price can't be greater than it's initial price"
var alertMinimumInitialPrice = "Initial price can not be less than its minimum price $: "
var alertMinimumMaintPrice = "Maintenance price can not be less than it's minimum price $: "
var alertUnit = "Enter unit"
var alertBundle = "Please select bundle"
var alertDepartment = "Enter select department"
var alertServiceFrequency = "Enter select service frequency"
var alertBillingFrequency = "Enter select billing frequency"
var alertServiceName = "Enter service name"
var alertServiceAlreadyExist = "Service already exist"
var alertServcie = "Please add service"
var alertCouopn = "Please enter valid coupon code"
var alertCouopnApplied = "Coupon already applied"
var alertInvalidCoupon = "Invalid coupon applied"
var alertEditPrice = "Edit in price will delete all the coupons, credits and discount associated with this service, Do you want to proceed ?"
var alertPlusService = "Either plus service frequency or billing frequency is different from it's parent service frequency or billing frequency, Do you want to proceed ?"

var alertSuccessfullSync = "Appointment synced successfully"
var alertSuccessfullAgreement = "Agreement sent successfully"
var alertSuccessfullProposal = "Proposal sent successfully"
var alert_EAF = "Internet connection is reqiured to fill payment authorization form"
//// MARK:
// MARK:  Core Data Entity Names
var Entity_Disclaimer = "WoWdoDisclaimerExtSerDcs"
var Entity_ProblemIdentifications = "WoWdoProblemIdentificationExtSerDcs"
var Entity_WdoInspection = "WoWdoInspectionExtSerDc"
var Entity_WdoPricingMultipliers = "WoWdoPricingMultiplierExtSerDc"
var Entity_DeviceDynamicData = "DeviceInspectionDynamicForm"
var Entity_ProblemIdentificationPricing = "WoWdoProblemIdentificationPricingExtSerDc"
var Entity_ProblemImageDetails = "WoWdoProblemIdentificationImagesExtSerDcs"
var Entity_LeadCommercialTargetExtDc = "LeadCommercialTargetExtDc"
var Entity_TargetImageDetail = "TargetImageDetail"
var Entity_WoWdoGeneralNotesExtSerDcs = "WoWdoGeneralNotesExtSerDcs"
var Entity_WdoImages = "ImageDetailsServiceAuto"
var Entity_WdoPaymentInfo = "PaymentInfoServiceAuto"
var Entity_WdoEmailDetailServiceAuto = "EmailDetailServiceAuto"
var Entity_WdoWoDetail = "WorkOrderDetailsService"
var Entity_SalesAutoModifyDate = "SalesAutoModifyDate"
var Entity_SalesDynamicInspection = "SalesDynamicInspection"
var Entity_SalesLeadDetail = "LeadDetail"
var Entity_ServiceAutoModifyDate = "ServiceAutoModifyDate"
var Entity_ServiceWorkOrderDetailsService = "WorkOrderDetailsService"
var Entity_ServiceDynamicForm = "ServiceDynamicForm"
var Entity_EquipmentDynamicForm = "EquipmentDynamicForm"
var Entity_CrmTaskAppointmentVC = "TaskAppointmentVC"
var Entity_EmployeeBlockTimeAppointmentVC = "EmployeeBlockTime"
var Entity_PlumbingDynamicForm = "MechanicalDynamicForm"
var Entity_PlumbingWorkOrderDetailsService = "WorkOrderDetailsService"

var Entity_WoNPMAInspectionGeneralDescriptionExtSerDc = "WoNPMAInspectionGeneralDescriptionExtSerDc"
var Entity_WoNPMAInspectionOtherDetailExtSerDc = "WoNPMAInspectionOtherDetailExtSerDc"
var Entity_WoNPMAInspectionObstructionExtSerDcs = "WoNPMAInspectionObstructionExtSerDcs"
var Entity_WoNPMAFinalizeReportDetailExtSerDc = "WoNPMAFinalizeReportDetailExtSerDc"
var Entity_WoNPMAInspectionMoistureDetailExtSerDc = "WoNPMAInspectionMoistureDetailExtSerDc"
var Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs = "WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs"
var Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs = "WoNPMAProductDetailForSoilAndWoodExtSerDcs"
var Entity_WoNPMAInspectionConduciveConditionExtSerDc = "ServiceConditions"
var Entity_WoNPMAInspectionServiceConditionComments = "ServiceConditionComments"
var Entity_WoNPMAInspectionServiceConditionDocuments = "ServiceConditionDocuments"
var Entity_WoNPMAServicePricingExtSerDcs = "WoNPMAServicePricingExtSerDcs"
var Entity_WoNPMAServiceTermsExtSerDcs = "WoNPMAServiceTermsExtSerDcs"

var Entity_D2DPestPacPaymentDC = "DTDLeadPaymentTransactionExtSerDc"
var Entity_D2DPestPacCreditCardDC = "DTDLeadPaymentCardInfoExtSerDc"

var arrayTopMenuOption = ["Map", "Lead", "Opportunity", "Task", "Activity"]

//// MARK:
// MARK:  Enums

var enumOccuppied = "1"
var enumVacant = "2"
var enumUnfurnished = "1"
var enumFurnished = "2"
var enumSlab = "1"
var enumRaised = "2"
var enumRaisedSlab = "3"
var enumReportComplete = "Complete Report"
var enumReportLimited = "Limited Report"
var enumReportSupplemental = "Supplemental Report"
var enumReportReinspection = "ReInspection Report"
var enumReportCompleteName = "CompleteReport"
var enumReportLimitedName = "LimitedReport"
var enumReportSupplementalName = "SupplementalReport"
var enumReportReinspectionName = "ReinspectionReport"
var enumSUBTERRANEANTERMITES = "SUBTERRANEANTERMITES"
var enumDRYWOODTERMITES = "DRYWOODTERMITES"
var enumFUNGUSDRYROT = "FUNGUSDRYROT"
var enumOTHERFINDINGS = "OTHERFINDINGS"
var enumFURTHERINSPECTION = "FURTHERINSPECTION"
var enumMaterial = "Products"
var enumCondition = "Condusive Conditions"
var enumComment = "Comments"
var enumPest = "Pests"
var enumDocument = "Document"
var enumInspectDevice = "Inspect Device"
var enumDevice = "Device"
var enumArea = "Area"
var enumNearBy = "Map"
var enumSignedAgreement = "View Signed Agreement"
var enumRefTypeCrmCompany = "CrmCompany"
var enumRefTypeCrmContact = "CrmContact"
var enumRefTypeWebLead = "WebLead"
var enumRefTypeOpportunity = "Lead"
var enumRefTypeLead = "Lead"
var enumRefTypeAccount = "Account"
var enumCommercial = "Commercial"
var enumResidential = "Residential"//residential


// Send Text Template
var enumSendTextTemplateWorkOrder = "WorkOrder Schedule Confirmation"
                    
//// MARK:
// MARK:  WEB SERVICES URL

//var BaseURL :String = "http://192.168.0.191/gngpl/webservices/webservices/"
var BaseURL :String = "http://www.goanaturalgas.com/staging_software/index.php/webservices/webservices/"

var URL_terms_conditions :String = BaseURL + "terms_conditions"
var URL_about_us :String = BaseURL + "about_us/"
var URL_contact_us :String = BaseURL + "contact_us_data/"
var URL_Dashboard :String = BaseURL + "get_dashboard_data/"
var URL_Content_Pages_Data :String = BaseURL + "content_pages_data/"

var URL_Get_drop_down_list :String = BaseURL + "drop_down/"
var URL_Otp_Verify :String = BaseURL + "otp_verification/"
var URL_resend_otp :String = BaseURL + "resend_otp/"

var URL_Save_connection_request :String = BaseURL + "save_connection_request/"
var URL_Save_Profile :String = BaseURL + "set_profile/"
var URL_Save_Image :String = BaseURL + "test/"

var URL_register :String = BaseURL + "register/"
var URL_uploadDocument :String = BaseURL + "upload_document/"
var URL_SavePayment :String = BaseURL + "connection_payment/"
var URL_login :String = BaseURL + "login/"
var URL_ChangePassword :String = BaseURL + "change_password/"
var URL_Feedback :String = BaseURL + "save_feedback/"
var URL_Save_meter_reading :String = BaseURL + "save_meter_reading/"
var URL_Edit :String = BaseURL + "edit_profile/"
var URL_Forgot_password :String = BaseURL + "forgot_password/"
var URL_Forgot_otp_verification :String = BaseURL + "forgot_otp_verification/"
var URL_Resend_otp_pass :String = BaseURL + "resend_otp_pass/"
var URL_Reset_password :String = BaseURL + "reset_password/"
var URL_FAQ :String = BaseURL + "get_faq/"
var URL_Update_notification :String = BaseURL + "update_notification/"

var URL_SavingCalculator :String = BaseURL + "saving_cal_dropdowns/"
var URL_refer_earn :String = BaseURL + "refer_earn/"
var URL_saving_calculator_Calculate :String = BaseURL + "saving_calculator/"
var URL_logout :String = BaseURL + "logout/"
var URL_refresh_tokens :String = BaseURL + "refresh_tokens/"
var URL_view_bill :String = BaseURL + "view_bill/"
var URL_notification_details :String = BaseURL + "notification_details/"



var isBackGround = false

// MARK:
// MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}

struct DeviceType{
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPHONE_XS = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPHONE_XR_XS_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
      static let IS_IPHONE_11 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
    static let IS_IPHONE_11Pro = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPHONE_11Pro_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH >= 1024.0
    static let IS_IPHONE_6_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH <= 667.0

}

func changeDateToStringTemp(date: Date)-> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
    let strDate = dateFormatter.string(from: date)
    return strDate
}

// MARK: - --------------Show Error / Network
// MARK: -
var btnRetry = UIButton()
var imgErrorView = UIImageView()
var lblError = UILabel()
var viewError = UIView()

func getMutableDictionaryFromNSManagedObjectArray(ary:NSArray)-> NSMutableArray

{

    let aryReturn = NSMutableArray()

    for item in ary {

        let obj = (item as AnyObject)as! NSManagedObject

        let keys = Array(obj.entity.attributesByName.keys)

        let dict = (obj.dictionaryWithValues(forKeys: keys) as NSDictionary).mutableCopy() as! NSMutableDictionary

        

        if(keys.contains("departmentId")){

            dict.setValue("\(dict.value(forKey: "departmentId")!)", forKey: "DepartmentId")

        }

        if(keys.contains("termsId")){

            dict.setValue("\(dict.value(forKey: "termsId")!)", forKey: "ServiceReportTermsAndConditionsId")

        }

        if(keys.contains("termsnConditions")){

            dict.setValue("\(dict.value(forKey: "termsnConditions")!)", forKey: "SR_TermsAndConditions")

        }

        if(keys.contains("termsTitle")){

            dict.setValue("\(dict.value(forKey: "termsTitle")!)", forKey: "TermsTitle")

        }
        
        aryReturn.add(dict)

    }

    

 

    return aryReturn

}

func addErrorImageOnView(strMessage : String , img : UIImage , controller : UIViewController)  {
    if DeviceType.IS_IPHONE_X {
        viewError.frame =  CGRect(x: 0, y: 100 , width: controller.view.frame.width, height: controller.view.frame.height - 100)
        
    }else{
        viewError.frame =  CGRect(x: 0, y: 84 , width: controller.view.frame.width, height: controller.view.frame.height - 84)
    }
    viewError.isUserInteractionEnabled = true
    imgErrorView.frame = CGRect(x: viewError.frame.width/2 - 125, y: viewError.frame.height/2 - 145 , width: 250, height: 250)
    imgErrorView.image = img
    // imgErrorView.backgroundColor = UIColor.purple
    imgErrorView.contentMode = .scaleAspectFit
    lblError.frame = CGRect(x: 20, y:  imgErrorView.frame.maxY + 5 , width: controller.view.frame.width - 40 , height: 40)
    lblError.numberOfLines = 4
    lblError.text = strMessage
    lblError.textAlignment = .center
    lblError.textColor = UIColor.gray
    lblError.font = UIFont(name:primaryFontLatoRegular,size:16)
    
    btnRetry = UIButton(type: .system)
    btnRetry.frame = CGRect(x: controller.view.frame.width/2 - 75, y:  lblError.frame.maxY + 20  , width: 150 , height: 30)
    btnRetry.setTitle("Retry", for: .normal)
    btnRetry.tag = 0
    btnRetry.setTitleColor(UIColor.gray, for: .normal)
    btnRetry.tintColor = UIColor.gray
    btnRetry.titleLabel?.textAlignment = .center
    btnRetry.layer.cornerRadius = 4.0
    btnRetry.layer.borderWidth = 1.0
    btnRetry.layer.borderColor = UIColor.lightGray.cgColor
    viewError.backgroundColor = UIColor.white
    viewError.addSubview(imgErrorView)
    viewError.addSubview(lblError)
    viewError.addSubview(btnRetry)
    controller.view.addSubview(viewError)
}
func removeErrorView(){
    btnRetry.removeFromSuperview()
    lblError.removeFromSuperview()
    imgErrorView.removeFromSuperview()
    viewError.removeFromSuperview()
}
// MARK:
// MARK: OTHER FUNCTION

func getPriorityMaster()->NSMutableArray
{
    let aryMaster = NSMutableArray()
    
    let dictDetailsFortblView = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
    
    let aryTemp = dictDetailsFortblView.value(forKey: "PriorityMasters") as! NSArray
    
    for item in aryTemp
    {
        let dict = item as! NSDictionary
        if(dict.value(forKey: "IsActive") as! Bool == true)
        {
            aryMaster.add(dict)
        }
    }
    
    return aryMaster
}
func getEmployeeList()-> NSArray
{
    let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
    return aryTemp
}
func getJson(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
func decimalValidation(textField : UITextField , range: NSRange , string : String) -> Bool
{
    guard let oldText = textField.text, let r = Range(range, in: oldText) else {
        return true
    }
    
    let newText = oldText.replacingCharacters(in: r, with: string)
    let isNumeric = newText.isEmpty || (Double(newText) != nil)
    let numberOfDots = newText.components(separatedBy: ".").count - 1
    
    let numberOfDecimalDigits: Int
    if let dotIndex = newText.index(of: ".") {
        numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
    } else {
        numberOfDecimalDigits = 0
    }
    
   
    return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2 && newText.count <= 12

   
    //return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
}

func converHTML(html:String)-> String
{
    var myScanner: Scanner!
    var text:NSString?
    var strHTML = ""
    
    myScanner = Scanner(string: html)
    
    while (myScanner.isAtEnd == false)
    {
        myScanner.scanUpTo("<", into: nil)
        
        myScanner.scanUpTo(">", into: &text)
        
        strHTML = html.replacingOccurrences(of: "\(text!)>", with: "")
    }
    strHTML = strHTML.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    
    return strHTML
}
func buttonRoundedThemeColor(sender : UIButton){
    
    sender.layer.cornerRadius = 22
    sender.layer.borderWidth = 1
    sender.layer.borderColor = UIColor.black.cgColor
    sender.layer.masksToBounds = false
    sender.layer.shadowColor = UIColor.darkGray.cgColor
    sender.layer.shadowOffset = CGSize(width: 1, height: 1);
    sender.layer.shadowOpacity = 0.5
    sender.backgroundColor = UIColor.theme()
    
}
func buttonRound(sender : UIButton){
    
    sender.layer.cornerRadius = 10
    sender.layer.borderWidth = 1
    sender.layer.borderColor = UIColor.clear.cgColor
    sender.layer.masksToBounds = false
    sender.layer.shadowColor = UIColor.darkGray.cgColor
    sender.layer.shadowOffset = CGSize(width: 1, height: 1);
    sender.layer.shadowOpacity = 0.5
    //sender.backgroundColor = UIColor.theme()
    
}
func setDefaultFilterValuesSignedAgreement(){
    
    let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
    
    var strCompanyKey = String()
    
    if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
        
        strCompanyKey = "\(value)"
        
    }
    
    // Setting Default Dates
    
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "MM/dd/yyyy"
    formatter.locale = Locale(identifier: "EST")
    let result = formatter.string(from: date)
    
    let earlyDate = Calendar.current.date(
        byAdding: .day,
        value: -1,
        to: Date())
    
    let resultFrom = formatter.string(from: earlyDate!)
    
    let dictParameters = NSMutableDictionary()
    dictParameters.setValue(resultFrom, forKey: "FromSigneddate")
    dictParameters.setValue(result, forKey: "ToSignedDate")
    dictParameters.setValue("", forKey: "AccountNo")
    dictParameters.setValue("", forKey: "LeadNo")
    dictParameters.setValue("", forKey: "CompanyName")
    dictParameters.setValue("", forKey: "FirstName")
    dictParameters.setValue("", forKey: "MiddleName")
    dictParameters.setValue("", forKey: "LastName")
    dictParameters.setValue(strCompanyKey, forKey: "CompanyKey")

    nsud.set(dictParameters, forKey: "FilterDictSignedAgreement")
    nsud.synchronize()
    
}


func buttonRoundedWithoutBackroundColor(sender : UIButton){
    
    sender.layer.cornerRadius = 22
    sender.layer.borderWidth = 1
    sender.layer.borderColor = UIColor.black.cgColor
    sender.layer.masksToBounds = false
    sender.layer.shadowColor = UIColor.black.cgColor
    sender.layer.shadowOffset = CGSize(width: 1, height: 1);
    sender.layer.shadowOpacity = 0.5
    sender.backgroundColor = UIColor.clear
    
}

func jsontoString(fromobject:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: fromobject, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
func rateForApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
    guard let url = URL(string : appId) else {
        completion(false)
        return
    }
    guard #available(iOS 10, *) else {
        completion(UIApplication.shared.openURL(url))
        return
    }
    UIApplication.shared.open(url, options: [:], completionHandler: completion)
}

// MARK: Core Data Fetch, Delete & Update Methods

func getContext() -> NSManagedObjectContext {
    
    var appDelegate1: AppDelegate?
    var context1: NSManagedObjectContext?
    
    appDelegate1 = UIApplication.shared.delegate as? AppDelegate
    context1 = appDelegate1?.managedObjectContext

    return context1!
    
}

// Deleting Data From DB

func deleteAllRecordsFromDB(strEntity: String , predicate : NSPredicate ) {
    let context = getContext()
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\(strEntity)")
    deleteFetch.predicate = predicate
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    
    do {
        try context.execute(deleteRequest)
        try context.save()
        
    } catch {
        print ("There was an error")
    }
}

func deleteAllRecordsWithoutPredicateFromDB(strEntity: String , predicate : NSPredicate ) {
    let context = getContext()
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\(strEntity)")
    //deleteFetch.predicate = predicate
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    
    do {
        try context.execute(deleteRequest)
        try context.save()
        
    } catch {
        print ("There was an error")
    }
}

func getDataFromLocal(strEntity: String , predicate : NSPredicate )-> NSArray {
    
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    fetchRequest.predicate = predicate
    do
    {
        return (try getContext().fetch(fetchRequest) as NSArray)
        
    }
    catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
    
}

func getDataFromLocalSorted(strEntity: String , predicate : NSPredicate  , sort : NSSortDescriptor)-> NSArray {
    
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    fetchRequest.predicate = predicate
    fetchRequest.sortDescriptors = [sort]
    do {
        return (try getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
    
}

func getDataFromCoreDataBaseArray(strEntity: String , predicate : NSPredicate )-> NSArray   {
    
    let aryTemp = getDataFromLocal(strEntity: strEntity , predicate: predicate)
    
    if aryTemp.count > 0 {
        
        return aryTemp
        
    } else {
        
        return  NSArray()
        
    }
    
}

func getDataFromCoreDataBaseArraySorted(strEntity: String , predicate : NSPredicate , sort : NSSortDescriptor)-> NSArray   {
    
    let aryTemp = getDataFromLocalSorted(strEntity: strEntity , predicate: predicate , sort: sort)
    
    if aryTemp.count > 0 {
        
        return aryTemp
        
    } else {
        
        return  NSArray()
        
    }
    
}

func saveDataInDB(strEntity: String , arrayOfKey: NSMutableArray , arrayOfValue: NSMutableArray)  {
    
    let arrayOfKeyNew =  arrayOfKey.map { ($0 as! String).lowerCasedFirstLetter() } as NSArray
    
    let context = getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    
    for index in 0..<arrayOfKeyNew.count {
        
        transc.setValue(arrayOfValue[index], forKey: "\(arrayOfKeyNew[index])")
        
    }
    
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}

// fetch data and update in core DB

func getDataFromDbToUpdate(strEntity: String , predicate : NSPredicate , arrayOfKey: NSMutableArray , arrayOfValue: NSMutableArray)-> Bool {
    
    let arrayOfKeyNew =  arrayOfKey.map { ($0 as! String).lowerCasedFirstLetter() } as NSArray

    let context = getContext()

    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    fetchRequest.predicate = predicate
    do {
        
        let dataTemp = (try getContext().fetch(fetchRequest) as NSArray)
        
        if dataTemp.count > 0 {
            
            let objToUpdate = dataTemp[0] as! NSManagedObject
            
            for index in 0..<arrayOfKeyNew.count {
                
                objToUpdate.setValue(arrayOfValue[index], forKey: "\(arrayOfKeyNew[index])")
                
            }
            
        }else{
            
            return false
        }

        
        do {
            try context.save()
        } catch _ as NSError  {
            
        } catch {
            
        }
        
        return true

    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
        
        return false

    }
    
}

func deleteDataFromDB(obj : NSManagedObject)  {
    let context = getContext()
    //save the object
    do {
        context.delete(obj)
        try context.save()
    } catch _ as NSError  {
        
    }catch {
    }
}


func saveDataInLocalArray(strEntity: String , strKey : String , data : NSMutableArray)  {
    let context = getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}
func saveDataInLocalDictionary(strEntity: String , strKey : String , data : NSMutableDictionary)  {
    let context = getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}


func updateTrueZSync(strCompanyKey : String, strUserName : String, strWoId : String)  {
    
    let strModifiedDate = Global().modifyDateService()
    
    let arrOfKeys = NSMutableArray()
    let arrOfValues = NSMutableArray()
    
    arrOfKeys.add("zSync")
    arrOfKeys.add("modifyDate")

    arrOfValues.add("yes")
    arrOfValues.add(strModifiedDate!)

    let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && companyKey == %@", strWoId, strUserName, strCompanyKey), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    
    if isSuccess {
        
        print("Edited and updated something successfully")
        
    }
    
}



// MARK: Document Directory Methods

func saveImageDocumentDirectory(strFileName : String , image : UIImage){
    let fileManager = FileManager.default
    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(strFileName)
    print("Save......" + paths)
    let imageData = image.jpegData(compressionQuality:0.5)
    fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
}

func saveImageDataDocumentDirectory(strFileName : String , imageData : Data){
    let fileManager = FileManager.default
    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(strFileName)
    print("Save......" + paths)
    //let imageData = image.jpegData(compressionQuality:0.5)
    fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
}

func savepdfDocumentDirectory(strFileName : String , dataa : Data){
    let fileManager = FileManager.default
    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(strFileName)
    print("Save......" + paths)
    let imageData = dataa
    fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
}

func GetImageFromDocumentDirectory(strFileName : String) -> UIImage{
    let fileManager = FileManager.default
    let imagePAth = (getDirectoryPath() as NSString).appendingPathComponent(strFileName)
    if fileManager.fileExists(atPath: imagePAth){
        return UIImage(contentsOfFile: imagePAth) ?? UIImage()
    }else{
        return UIImage()
    }
}

func GetDataFromDocumentDirectory(strFileName : String) -> Data{
    let fileManager = FileManager.default
    let imagePAth = (getDirectoryPath() as NSString).appendingPathComponent(strFileName)
    if fileManager.fileExists(atPath: imagePAth){
        
        let data: NSData? = NSData(contentsOfFile: imagePAth)
        
        return data! as Data
        
    }else{
        
        return Data()
        
    }
}

func checkIfImageExistAtPath(strFileName : String) -> Bool{
    let fileManager = FileManager.default
    let imagePAth = (getDirectoryPath() as NSString).appendingPathComponent(strFileName)
    if fileManager.fileExists(atPath: imagePAth){
        return true
    }else{
        return false
    }
}

func DeleteImageFromDocumentDirectory(strFileName : String){
    let fileManager = FileManager.default
    let imagePAth = (getDirectoryPath() as NSString).appendingPathComponent(strFileName)
    if fileManager.fileExists(atPath: imagePAth){
        try! fileManager.removeItem(atPath: imagePAth)
        print("Deleted......")
        
    }else{
        print("Something wronge.")
    }
}

func getDirectoryPath() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

func getDirectionMap(strTitle : String , strlat : String , strLong : String) {
       if !(strTitle == "<null>" || strTitle == "" || strTitle == "Not Available"){
           let latitude: CLLocationDegrees = Double(strlat)!
           let longitude: CLLocationDegrees = Double(strLong)!
           let regionDistance:CLLocationDistance = 10000
           let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
           let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
           let options = [
               MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
               MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
           ]
           let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
           let mapItem = MKMapItem(placemark: placemark)
           mapItem.name = strTitle
           mapItem.openInMaps(launchOptions: options)
       }
   }

// MARK:
// MARK: DATE
func getTodayString() -> String{
    let date = Date()
    let calender = Calendar.current
    let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
    let year = components.year
    let month = components.month
    let day = components.day
    let hour = components.hour
    let minute = components.minute
    let second = components.second
    let today_string = String(year!) + "_" + String(month!) + "_" + String(day!) + "_" + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
    return today_string
}

func getUniqueValueForId() -> String{
    let date = Date()
    let calender = Calendar.current
    let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
    let year = components.year
    let month = components.month
    let day = components.day
    let hour = components.hour
    let minute = components.minute
    let second = components.second
    let today_string = String(year!) + String(month!)  + String(day!) + String(hour!)  + String(minute!) +  String(second!)
    
    let dateFMT = DateFormatter()
    dateFMT.locale = Locale(identifier: "en_US_POSIX")
    dateFMT.dateFormat = "yyyyMMddHHmmssSSSS"
    let now = Date()
    
    let allowedChars = "0123456789"
    let allowedCharsCount = UInt32(allowedChars.characters.count)
    var randomString = ""
    for _ in 0 ..< 3 {
        
        let randomNum = Int(arc4random_uniform(allowedCharsCount))
        let randomIndex = allowedChars.index(allowedChars.startIndex, offsetBy: randomNum)
        let newCharacter = allowedChars[randomIndex]
        randomString += String(newCharacter)
        
    }
    
    print("%@%@", dateFMT.string(from: now),randomString)
    
    //return String(format: "%@%@", dateFMT.string(from: now),randomString)
    
    return today_string + randomString
    
}


func getUniqueString()-> String{
    var strName = "\(Date()).jpg".replacingOccurrences(of: "-", with: "")
    strName = strName.replacingOccurrences(of: " ", with: "")
    strName = strName.replacingOccurrences(of: "+", with: "")
    return  strName.replacingOccurrences(of: ":", with: "")
}


// MARK:
// MARK:  TotalTimeAfterCreated
func getTotalTimeAfterCreated(strdate : String) -> String {
   
    let startDate = serverDateToLocal(strDate: strdate, strServerTimeZone: "EST", strRequiredFormat: "MM/dd/yyyy hh:mm a")
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
    let formatedStartDate = dateFormatter.date(from: startDate)
    let currentDate = Date()
    let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year])
    let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate!, to: currentDate)
    
    var strDate = ""
    if(differenceOfDate.year != 0){
        strDate = "\(differenceOfDate.year!) y"
        return  strDate
    }
    if(differenceOfDate.month != 0){
        strDate = strDate +  (strDate.count != 0 ? " : \(differenceOfDate.month!) m" :  "\(differenceOfDate.month!) m")
        return  strDate
        
    }
    if(differenceOfDate.day != 0){
        strDate = strDate + (strDate.count != 0 ? " : \(differenceOfDate.day!) d" :  "\(differenceOfDate.day!) d")
        return  strDate
        
        
    }
    if(differenceOfDate.hour != 0){
        strDate = strDate + (strDate.count != 0 ? " : \(differenceOfDate.hour!) h" :  "\(differenceOfDate.hour!) h")
        return  strDate
        
    }
    
    if(differenceOfDate.minute != 0){
        strDate = strDate + (strDate.count != 0 ? " : \(differenceOfDate.minute!) min" :  "\(differenceOfDate.minute!) min")
        return  strDate
        
    }
    if(differenceOfDate.second != 0){
        strDate = strDate + (strDate.count != 0 ? " : \(differenceOfDate.second!) s" :  "\(differenceOfDate.second!) s")
        return  strDate
        
    }
    return "\(strDate)"
}
// MARK:
// MARK: txtFieldValidation
func txtFieldValidation(textField : UITextField , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let decimelnumberOnly = NSCharacterSet.init(charactersIn: "0123456789.")
    let strValidStr_Digit = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    let characterOnly = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz")
    let characteraAndNumber = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz0123456789")

    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValidnumber = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strcharacterOnly = characterOnly.isSuperset(of: stringFromTextField as CharacterSet)
    
    let strValidDecimal = decimelnumberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strValidDigitCharacter = strValidStr_Digit.isSuperset(of: stringFromTextField as CharacterSet)
    
    let strcharacterAndNumberOnly = characteraAndNumber.isSuperset(of: stringFromTextField as CharacterSet)

    
    if returnOnly == "NUMBER" {
        if strValidnumber == false{
            return false
        }
    }
    if returnOnly == "CHAR_DIGIT" {
        if strValidDigitCharacter == false{
            return false
        }
    }
    
    if returnOnly == "DECIMEL" {
        if strValidDecimal == false{
            return false
        }
    }
    
    if returnOnly == "CHAR" {
        if strcharacterOnly == false{
            return false
        }
    }
    
    if returnOnly == "CHARNUMBER" {
        if strcharacterAndNumberOnly == false{
            return false
        }
    }
    
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        if(returnOnly == "DECIMEL"){
            if(string == "."){
                if(textField.text!.contains(".")){
                    return false;
                }else{
                    return true;
                }
            }
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    if (string == ".") {
        return false;
    }
    return true;
}

func txtViewValidation(textField : UITextView , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValid = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    if returnOnly == "NUMBER" {
        if strValid == false{
            return false
        }
    }
    if returnOnly == "CHAR" {
        if strValid == true{
            return false
        }
    }
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    return true;
}


func validateEmail(email: String) -> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
}

func validatePassword(password: String) -> Bool{
    let passwordRegex = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,30}$"
    return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
}

func buttonEnable_OnView(tag : Int , btn : UIButton)  {
    btn.layer.cornerRadius = 2.0
    if(tag == 1){
        btn.layer.borderWidth = 0.0
        btn.backgroundColor = hexStringToUIColor(hex: primaryColor)
        btn.setTitleColor(UIColor.white, for: .normal)
    }else{
        btn.backgroundColor = UIColor.white
        btn.layer.borderWidth = 1.0
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.setTitleColor(UIColor.lightGray, for: .normal)
    }
}


func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func removeNullFromDict (dict : NSMutableDictionary) -> NSMutableDictionary
{
    let dic = dict;
    for (key, value) in dict {
        let val : NSObject = value as! NSObject;
        if(val.isEqual(NSNull()))
        {
            dic.setValue("", forKey: (key as? String)!)
        } else if(val.isEqual("<null>"))
        {
            dic.setValue("", forKey: (key as? String)!)
        }
        else
        {
            dic.setValue(value, forKey: key as! String)
        }
    }
    return dic;
}



// MARK:
// MARK: Loader
func allButtonUserintractionClose(controller : UIViewController){
    //   btnOverLay.backgroundColor = UIColor.black
    //  btnOverLay.alpha = 1.0
    btnOverLay.frame = controller.view.frame
    controller.view.addSubview(btnOverLay)
}
func allButtonUserintractionEnable(controller : UIViewController)  {
    btnOverLay.removeFromSuperview()
}


func imageResize(image: UIImage, newSize: CGSize) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage ?? UIImage()
}

func getMutableDictionaryFromNSManagedObject(obj:NSManagedObject)-> NSMutableDictionary
{
    let keys = Array(obj.entity.attributesByName.keys)
    
    let dict = (obj.dictionaryWithValues(forKeys: keys) as NSDictionary).mutableCopy() as! NSMutableDictionary
    
    return dict
}

// MARK:
// MARK: Local Directory

func getImagefromDirectory(strname : String) -> UIImage{
    
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    if let dirPath          = paths.first
    {
        if strname == ""{
            return UIImage()
        }else{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(strname)
            if(imageURL.path.count != 0){
                return  UIImage(contentsOfFile: imageURL.path)!
            }
            return UIImage()
        }
    }
    return UIImage()
}

func fileAvailableAtPath(strname : String) -> Bool {
    
    let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
    let url = NSURL(fileURLWithPath: path)
    if let pathComponent = url.appendingPathComponent(strname) {
        let filePath = pathComponent.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            return true
        } else {
            print("FILE NOT AVAILABLE")
            return false

        }
    } else {
        print("FILE PATH NOT AVAILABLE")
        return false

    }
    
}

func removeImageFromDirectory(itemName:String) {
    let fileManager = FileManager.default
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
    let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    guard let dirPath = paths.first else {
        return
    }
    let filePath = "\(dirPath)/\(itemName)"
    print("Delete file Name : \(itemName)")
    do {
        try fileManager.removeItem(atPath: filePath)
    } catch let error as NSError {
        print(error.debugDescription)
    }
}

// MARK:
// MARK: Related to date formet

func returnFilteredArray(array : NSMutableArray) -> NSMutableArray {
    
    let arrOfFilteredData = NSMutableArray()
    
    for item in array {
        
        if (item as AnyObject) is NSDictionary {
            
            let dict = (item as AnyObject) as! NSDictionary
            
            let arrDataActiveCheck = dict.allKeys as NSArray
            
            if arrDataActiveCheck.contains("IsActive")
            {
                if ( "\(dict.value(forKeyPath: "IsActive")!)" == "1" ||  "\(dict.value(forKeyPath: "IsActive")!)" == "true" ||  "\(dict.value(forKeyPath: "IsActive")!)" == "True" ) {
                    
                    arrOfFilteredData.add(dict)
                    
                }
            }
            else
            {
                arrOfFilteredData.add(dict)
            }
            
        }
        
    }
    
    return arrOfFilteredData
    
}

func returnFilteredArrayValues(array : NSMutableArray , strParameterName : String , strParameterValue : String) -> NSMutableArray {
    
    let arrOfFilteredData = NSMutableArray()
    
    for item in array {
        
        if (item as AnyObject) is NSDictionary {
            
            let dict = (item as AnyObject) as! NSDictionary
            
            if ( "\(dict.value(forKeyPath: strParameterName)!)" == strParameterValue) {
                
                arrOfFilteredData.add(dict)
                
            }
            
        }
        
    }
    
    return arrOfFilteredData
    
}


func dateTimeConvertor(str: String , formet : String , strFormatToBeConverted : String) -> String {
    if str != "" && str != "<null>" {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formet
        let date = dateFormatter.date(from: str)!
        dateFormatter.dateFormat = strFormatToBeConverted
        let dateString1 = dateFormatter.string(from: date)
        let finlString = "\(dateString1)"
        if(finlString == ""){
            return  " "
        }
        return finlString
    }else{
        return "Not Available"
    }
}

func dateStringToFormatedDateString(dateToConvert: String, dateFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let myDate = dateFormatter.date(from: dateToConvert)!
    dateFormatter.dateFormat = dateFormat
    let dateString = dateFormatter.string(from: myDate)
    return dateString
}

func dateStringToFormatedDateStringNew(dateToConvert: String,dateToConvertFormat: String, dateFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = dateToConvertFormat
    dateFormatter.locale = Locale(identifier: "EST")
    let myDate = dateFormatter.date(from: dateToConvert)!
    dateFormatter.dateFormat = dateFormat
    let dateString = dateFormatter.string(from: myDate)
    return dateString
}

func getTimeStringIn24HrsFormatFrom12HrsFormat(timeToConvert: String)-> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "hh:mm a"
    let myDate = dateFormatter.date(from: timeToConvert)!
    dateFormatter.dateFormat = "HH:mm"
    let dateString = dateFormatter.string(from: myDate)
    return dateString
}
// MARK:
// MARK: Animation

func ShakeAnimation(textfiled: UITextField) {
    let shake = CAKeyframeAnimation(keyPath: "transform.translation.x")
    shake.duration = 0.1
    shake.repeatCount = 3
    shake.autoreverses = true
    shake.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
    textfiled.layer.add(shake, forKey: "shake")
}

func setBorderColor(item: AnyObject) -> Void
{
    /*//let dict = (item as AnyObject)as! NSDictionary
    item.layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor
    item.layer.borderWidth = 1.0
    item.layer.cornerRadius = 5.0*/
    
    if(item is UITextView){

        (item as! UITextView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

        (item as! UITextView).layer.borderWidth = 1.0

        (item as! UITextView).layer.cornerRadius = 5.0

    }else if(item is UITextField){

        (item as! UITextField).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

        (item as! UITextField).layer.borderWidth = 1.0

        (item as! UITextField).layer.cornerRadius = 5.0

    }

    else if(item is UIView){

        (item as! UIView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

        (item as! UIView).layer.borderWidth = 1.0

        (item as! UIView).layer.cornerRadius = 5.0

    }

    else if(item is UIButton){

            (item as! UIButton).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UIButton).layer.borderWidth = 1.0

            (item as! UIButton).layer.cornerRadius = 5.0

        }

       else if(item is UILabel){

            (item as! UILabel).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UILabel).layer.borderWidth = 1.0

            (item as! UILabel).layer.cornerRadius = 5.0

        }
}

// MARK:
// MARK:  Active Data Only

func getActiveDataOnly(arrOfData : NSArray , strBranchId : String , strParameterName : String) -> NSMutableArray {
    
    let arrActiveData = NSMutableArray()
    
    for i in 0 ..< arrOfData.count {
        
        let dictData = arrOfData[i] as! NSDictionary
        
        let isActive = dictData.value(forKey: "IsActive") as! Bool
        
        var nsNumberBranchIdLocal = NSNumber()
        var strBranchIdLocal = String()
        
        if (dictData.value(forKey: strParameterName)) is NSNumber{
            
            nsNumberBranchIdLocal = dictData.value(forKey: strParameterName) as! NSNumber
            
            strBranchIdLocal =  String("\(nsNumberBranchIdLocal)")
            
        }else{
            
            strBranchIdLocal =  String("\(nsNumberBranchIdLocal)")
            
        }
        
        //let nsNumberBranchIdLocal = dictData.value(forKey: "BranchId") as! NSNumber
        //let strBranchIdLocal =  String("\(nsNumberBranchIdLocal)")
        
        if strBranchId.count > 0{
            
            if (isActive) && (strBranchId==strBranchIdLocal) {
                
                arrActiveData.add(dictData)
                
            }
            
        }else{
            
            if isActive {
                
                arrActiveData.add(dictData)
                
            }
            
        }

        
    }
    
    return arrActiveData
    
}

func getGroupDataOnly(arrOfData : NSArray , strGroupId : String , strKeyName : String) -> NSMutableArray {
    
    let arrActiveData = NSMutableArray()
    
    for i in 0 ..< arrOfData.count {
        
        let dictData = arrOfData[i] as! NSDictionary
        
        let isActive = dictData.value(forKey: "IsActive") as! Bool
        
        var nsNumberGroupIdLocal = NSNumber()
        var strGroupIdLocal =  String()
        
        if (dictData.value(forKey: strKeyName)) is NSNumber{
            
            nsNumberGroupIdLocal = dictData.value(forKey: strKeyName) as! NSNumber
            strGroupIdLocal =  String("\(nsNumberGroupIdLocal)")
            
        }else{
            
             strGroupIdLocal =  String("\(nsNumberGroupIdLocal)")
            
        }
        
//        let nsNumberGroupIdLocal = dictData.value(forKey: strKeyName) as! NSNumber
//        let strGroupIdLocal =  String("\(nsNumberGroupIdLocal)")
        
        if strGroupId.count > 0{
            
            if (isActive) && (strGroupId==strGroupIdLocal) {
                
                arrActiveData.add(dictData)
                
            }
            
        }
        
    }
    
    return arrActiveData
    
}

func getDataFromServiceAutoMasters(strTypeOfMaster : String , strBranchId : String) -> NSMutableArray {
    
    var arrayData = NSMutableArray()
    
    if  nsud.value(forKey: "MasterServiceAutomation") is NSDictionary {
        
        let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as!  NSDictionary
        
        let arrOfDataFromMaster = dictMaster.value(forKey: strTypeOfMaster) as! NSArray
        
        if ((strTypeOfMaster == "SkyConditions") || (strTypeOfMaster == "WindDirections") || (strTypeOfMaster == "Prioritys")){
            
            arrayData.addObjects(from: arrOfDataFromMaster as! [Any])
            
        }else{
            
            if arrOfDataFromMaster.count > 0 {
                
                if (strTypeOfMaster == "InventoryItems"){
                    
                    arrayData = getActiveDataOnly(arrOfData: arrOfDataFromMaster ,  strBranchId: strBranchId, strParameterName: "ItemType")
                    
                }else{
                    
                    arrayData = getActiveDataOnly(arrOfData: arrOfDataFromMaster ,  strBranchId: strBranchId, strParameterName: "BranchId")
                    
                }
                
            }
            
        }
        
    }
    
    return arrayData
    
}


func getWDOMasters(strMasterNameInDefaults : String , strTypeOfMaster : String , strBranchId : String , strToCheckActive : String , strToCheckBranch : String , strBranchParameterName : String) -> NSMutableArray {
    
    var arrayData = NSMutableArray()
    
    if  nsud.value(forKey: strMasterNameInDefaults) is NSDictionary {
        
        let dictMaster = nsud.value(forKey: strMasterNameInDefaults) as!  NSDictionary
        
        
        
        if(dictMaster.value(forKey: strTypeOfMaster) is NSArray){
            let arrOfDataFromMaster = dictMaster.value(forKey: strTypeOfMaster) as! NSArray
            if arrOfDataFromMaster.count > 0  {
                
                if strToCheckActive == "Yes" && strToCheckBranch == "Yes"{
                    
                    let arrActiveData = NSMutableArray()
                    
                    for i in 0 ..< arrOfDataFromMaster.count {
                        
                        let dictData = arrOfDataFromMaster[i] as! NSDictionary
                        
                        var nsNumberGroupIdLocal = NSNumber()
                        var strGroupIdLocal =  String()
                        
                        if (dictData.value(forKey: strBranchParameterName)) is NSNumber{
                            nsNumberGroupIdLocal = dictData.value(forKey: strBranchParameterName) as! NSNumber
                            strGroupIdLocal =  String("\(nsNumberGroupIdLocal)")
                        }else{
                            strGroupIdLocal =  String("\(nsNumberGroupIdLocal)")
                        }
                        
                        let isActive = dictData.value(forKey: "IsActive") as! Bool
                        
                        if (isActive) && (strBranchId==strGroupIdLocal) {
                            
                            arrActiveData.add(dictData)
                            
                        }
                        
                    }
                    
                    arrayData = arrActiveData
                    
                } else if strToCheckActive == "Yes" {
                    
                    let arrActiveData = NSMutableArray()
                    
                    for i in 0 ..< arrOfDataFromMaster.count {
                        
                        let dictData = arrOfDataFromMaster[i] as! NSDictionary
                        
                        let isActive = dictData.value(forKey: "IsActive") as! Bool
                        
                        if isActive {
                            
                            arrActiveData.add(dictData)
                            
                        }
                        
                    }
                    
                    arrayData = arrActiveData
                    
                }else if strToCheckBranch == "Yes"{
                    
                    let arrActiveData = NSMutableArray()
                    
                    for i in 0 ..< arrOfDataFromMaster.count {
                        
                        let dictData = arrOfDataFromMaster[i] as! NSDictionary

                        var nsNumberGroupIdLocal = NSNumber()
                        var strGroupIdLocal =  String()
                        
                        if (dictData.value(forKey: strBranchParameterName)) is NSNumber{
                            
                            nsNumberGroupIdLocal = dictData.value(forKey: strBranchParameterName) as! NSNumber
                            strGroupIdLocal =  String("\(nsNumberGroupIdLocal)")
                            
                        }else{
                            
                            strGroupIdLocal =  String("\(nsNumberGroupIdLocal)")
                            
                        }
                        
                        if (strBranchId==strGroupIdLocal) {
                                
                                arrActiveData.add(dictData)
                                
                        }
                        
                    }
                    
                    arrayData = arrActiveData
                }else {
                    
                    let arrActiveData = NSMutableArray()
                    
                    for i in 0 ..< arrOfDataFromMaster.count {
                        
                        let dictData = arrOfDataFromMaster[i] as! NSDictionary
                        
                        arrActiveData.add(dictData)
                        
                    }
                    
                    arrayData = arrActiveData
                    
                }
                
            }
        }
        
     
        
    }
    
    return arrayData
    
}

func replaceTags(strFillingLocal : String , strFindingLocal : String) -> String {
    
    var strReplacedString = ""
    
//    let strFillingLocal = "\(txtFilling_IssueCode.text!)"
//    let strFindingLocal = "\(txtviewFinding.text!)"
    
    var arrFillingLocal = NSArray()
    let arrMutableFillingLocal = NSMutableArray()
    
    arrFillingLocal = strFillingLocal.components(separatedBy: "//") as NSArray
    
    if arrFillingLocal.count > 0 {
        
        for k1 in 0 ..< arrFillingLocal.count {
            
            arrMutableFillingLocal.add(arrFillingLocal[k1])
            
        }
        
    }
    
    print(arrMutableFillingLocal)
    
    var arrFindingLocalToReplace = NSArray()
    
    arrFindingLocalToReplace = strFindingLocal.components(separatedBy: "##") as NSArray
    
    print(arrFindingLocalToReplace)
    
    // Logic for replacing
    
    if arrFindingLocalToReplace.count > 0 {
        
        for k1 in 0 ..< arrFindingLocalToReplace.count {
            
            let strString = arrFindingLocalToReplace[k1] as! String
            
            if k1 % 2 == 0 {
                
                if strReplacedString.count > 0 {
                    
                    strReplacedString = strReplacedString + strString
                    
                    
                }else {
                    
                    strReplacedString = strString
                    
                }
                
            }else{
                
                if arrMutableFillingLocal.count > 0 {
                    
                    let strString = arrMutableFillingLocal[0] as! String
                    
                    //arrMutableFillingLocal.removeObject(at: 0)
                    
                    if k1 == (arrFindingLocalToReplace.count-1) {
                        
                        if arrFindingLocalToReplace.count % 2 == 0 {
                            
                            if arrFindingLocalToReplace.count > 0 {
                                
                                for k2 in k1 ..< arrFindingLocalToReplace.count {
                                    
                                    let strString = arrFindingLocalToReplace[k2] as! String
                                    
                                    strReplacedString = strReplacedString + strString
                                    
                                }
                                
                            }
                            
                        }else{
                            
                            arrMutableFillingLocal.removeObject(at: 0)
                            
                            if strString.count > 0 {
                                
                                strReplacedString = strReplacedString + strString.trimmingCharacters(in: .whitespaces)
                                
                            }
                            
                        }
                        
                    }else{
                        
                        arrMutableFillingLocal.removeObject(at: 0)
                        
                        if strString.count > 0 {
                            
                            strReplacedString = strReplacedString + strString.trimmingCharacters(in: .whitespaces)
                            
                        }
                        
                    }
                    
                }else{
                    
                    // nothing left to replace so putting rest of the string
                    
                    let strString = arrFindingLocalToReplace[k1] as! String

                    if strString.count > 0 {
                        
                        if k1 % 2 == 0 {
                            
                            strReplacedString = strReplacedString + strString

                        }else{
                            
                            strReplacedString = strReplacedString + "##" + strString + "##"
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    print(strReplacedString)
    
    return strReplacedString
    
}

// MARK:
// MARK:  Internet validation

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}


func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
    
}
func showToastForSomeTime(title : String , message : String , time : Int, viewcontrol : UIViewController){
    let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
    viewcontrol.present(alert, animated: true, completion: nil)
    // change to desired number of seconds (in this case 5 seconds)
    let when = DispatchTime.now() + 2
    DispatchQueue.main.asyncAfter(deadline: when){
        // your code with delay
        alert.dismiss(animated: true, completion: nil)
    }
    
}

// MARK:
// MARK:  Calling & Message & OpenMap & Attribute string

func setAttributeText(lblText : UILabel) {
    let lblPocCell = NSAttributedString(string: lblText.text!,attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
    lblText.attributedText = lblPocCell
}

func checkNullValue(str : String) -> String! {
    var strValue = String()
    if str == "nil" || str == "<null>" {
        strValue = ""
    }else{
        strValue = "\(str)"
    }
    return strValue
    
}
func handleNegativeValue(strValue: String) -> String {
    
    var strNonNegativeValue = strValue
    
    let valueDouble = Double(strValue) ?? 0
    if valueDouble < 0
    {
        strNonNegativeValue = "0"
    }
    return strNonNegativeValue
    
}

func makeCornerRadius(value:CGFloat, view:UIView, borderWidth: CGFloat, borderColor:UIColor)
{
    view.layer.cornerRadius = value
    view.layer.borderWidth = borderWidth
    view.layer.borderColor = borderColor.cgColor
    view.layer.masksToBounds = true
}

func isWoCompleted(objWo : NSManagedObject) -> Bool {
    
    let strStatus = "\(objWo.value(forKey: "workorderStatus") ?? "")"
    
    if strStatus == "Completed" {
        
        return true
        
    }else if strStatus == "Complete" {
        
        return true
        
    }else if strStatus == "complete" {
        
        return true
        
    }else if strStatus == "completed" {
        
        return true
        
    }else {
        
        // Yaha False return krna hai.
        return false
        
    }
    
}

func htmlAttributedString(strHtmlString : String) -> NSMutableAttributedString {
    
    let description = strHtmlString.trimmingCharacters(in: .whitespacesAndNewlines)
    
    guard let data = description.data(using: .utf8, allowLossyConversion: true) else {
        return NSMutableAttributedString()
    }
    if let attributedString = try? NSMutableAttributedString(data: data,  options: [.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],  documentAttributes: nil)
    {
        
        if DeviceType.IS_IPAD{
            
            let uiFont = UIFont.systemFont(ofSize: 22)
            
            attributedString.removeAttribute(NSAttributedString.Key.font, range: NSRange(location: 0, length: attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.font, value: uiFont, range: NSRange(location: 0, length: attributedString.length))
            
        }else{
            
            let uiFont = UIFont.systemFont(ofSize: 16)
            
            attributedString.removeAttribute(NSAttributedString.Key.font, range: NSRange(location: 0, length: attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.font, value: uiFont, range: NSRange(location: 0, length: attributedString.length))
            
        }
        
        return attributedString
        
    }
    return NSMutableAttributedString()

    
}

func htmlAttributedStringNewSales(strHtmlString : String) -> NSMutableAttributedString {
    
    let description = strHtmlString.trimmingCharacters(in: .whitespacesAndNewlines)
    
    let data = Data(description.utf8)
    
    if let attributedString = try? NSMutableAttributedString(data: data,  options: [.documentType: NSAttributedString.DocumentType.html],  documentAttributes: nil)
    {
        
        if DeviceType.IS_IPAD{
            
            let uiFont = UIFont.systemFont(ofSize: 16)
            
            attributedString.removeAttribute(NSAttributedString.Key.font, range: NSRange(location: 0, length: attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.font, value: uiFont, range: NSRange(location: 0, length: attributedString.length))
            
        }else{
            
            let uiFont = UIFont.systemFont(ofSize: 14)
            
            attributedString.removeAttribute(NSAttributedString.Key.font, range: NSRange(location: 0, length: attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.font, value: uiFont, range: NSRange(location: 0, length: attributedString.length))
            
        }
        
        return attributedString
        
    }
    
    return NSMutableAttributedString()
    
}

func htmlAttributesStringNew1(html: String, font: UIFont) -> NSMutableAttributedString {
    let options : [NSAttributedString.DocumentReadingOptionKey : Any] =
        [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
         NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue]
    
    guard let data = html.data(using: .utf8, allowLossyConversion: true) else {
        return NSMutableAttributedString()
    }
    
    guard let attr = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil) else {
        return NSMutableAttributedString()
    }
    var attrs = attr.attributes(at: 0, effectiveRange: nil)
    attrs[NSAttributedString.Key.font] = font
    attr.setAttributes(attrs, range: NSRange(location: 0, length: attr.length))
    return attr
    
}

func getAttributedHtmlStringUnicode(strText : String) -> NSAttributedString {
    
    var attributedStringHTML: NSAttributedString? = nil
    do {
        if let data = strText.data(using: .unicode) {
            attributedStringHTML = try NSAttributedString(
                data: data,
                options: [.documentType: NSAttributedString.DocumentType.html],//, NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14)],
                documentAttributes: nil)
        }
    } catch {
    }
    
    if attributedStringHTML == nil
    {
        return NSAttributedString(string: "")
    }
    else
    {
        return attributedStringHTML!
    }
}

func setDefaultValuesForContactFilter() {
    //Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
    var dictLoginData = NSDictionary()
    dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

    var keys = NSArray()
    var values = NSArray()
    keys = ["CompanyKey",
            "FromDate",
            "ToDate",
            "FirstName",
            "MiddleName",
            "LastName",
            "PrimaryEmail",
            "PrimaryPhone",
            "CellPhone1",
            "CrmCompanyName",
            "Website",
            "Owner"]
    
    values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "\(dictLoginData.value(forKey: "EmployeeId")!)"]
    
    let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
    
    nsud.setValue(dictToSend, forKey: "CrmContactFilter")
    nsud.synchronize()
    
}

func setDefaultValuesForCompanyFilter() {
    //Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
    var dictLoginData = NSDictionary()
    dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

    var keys = NSArray()
    var values = NSArray()
    keys = ["CompanyKey",
            "FromDate",
            "ToDate",
            "Name",
            "PrimaryEmail",
            "PrimaryPhone",
            "CellPhone1",
            "Website",
            "AddressPropertyTypeId","CreatedBy"]
    
    values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              [],"\(Global().getEmployeeId() ?? "")"]
    
    let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
    
    nsud.setValue(dictToSend, forKey: "CrmCompanyFilter")
    nsud.synchronize()
    
}

func setDefaultValuesForTimeLineFilter() {
                
    //Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
    let dictFilterData = NSMutableDictionary()
    
     dictFilterData.setValue("", forKey: "SelectedInteraction")
     dictFilterData.setValue("", forKey: "SelectedDate")
     dictFilterData.setValue("", forKey: "SelectedUserName")
     dictFilterData.setValue("", forKey: "FromDate")
     dictFilterData.setValue("", forKey: "Todate")
     nsud.setValue(dictFilterData, forKey: "DPS_FilterTimeLine")
    
}

func setDefaultValuesForAccountFilter() {
    //Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
    var dictLoginData = NSDictionary()
    dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

    var keys = NSArray()
    var values = NSArray()
    keys = ["CompanyKey",
            "FromDate",
            "ToDate",
            "FirstName",
            "MiddleName",
            "LastName",
            "AccountName",
            "AccountNo",
            "CrmCompanyName",
            "Phone",
            "Email",
            "Address",
            "CityName",
            "StateId",
            "Zipcode"]
    
    values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              ""]
    
    let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
    
    nsud.setValue(dictToSend, forKey: "CrmAccountFilter")
    nsud.synchronize()
    
}

func setDefaultValuesForContactFilterAssociation() {
    //Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
    var dictLoginData = NSDictionary()
    dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

    var keys = NSArray()
    var values = NSArray()
    keys = ["CompanyKey",
            "FromDate",
            "ToDate",
            "FirstName",
            "MiddleName",
            "LastName",
            "PrimaryEmail",
            "PrimaryPhone",
            "CellPhone1",
            "CrmCompanyName",
            "Website",
            "Owner"]
    
    values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "\(dictLoginData.value(forKey: "EmployeeId")!)"]
    
    let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
    
    nsud.setValue(dictToSend, forKey: "CrmContactFilterAssociation")
    nsud.synchronize()
    
}

func setDefaultValuesForCompanyFilterAssociation() {
    //Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
    var dictLoginData = NSDictionary()
    dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

    var keys = NSArray()
    var values = NSArray()

    
    keys = ["CompanyKey",
            "FromDate",
            "ToDate",
            "Name",
            "PrimaryEmail",
            "PrimaryPhone",
            "CellPhone1",
            "Website",
            "AddressPropertyTypeId","CreatedBy"]
    
    values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              [],"\(Global().getEmployeeId() ?? "")"]
    
    
    let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
    
    nsud.setValue(dictToSend, forKey: "CrmCompanyFilterAssociation")
    nsud.synchronize()
    
}

func setDefaultValuesForAccountFilterAssociation() {
    //Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
    var dictLoginData = NSDictionary()
    dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

    var keys = NSArray()
    var values = NSArray()
    keys = ["CompanyKey",
            "FromDate",
            "ToDate",
            "FirstName",
            "MiddleName",
            "LastName",
            "AccountName",
            "AccountNo",
            "CrmCompanyName",
            "Phone",
            "Email",
            "Address",
            "CityName",
            "StateId",
            "Zipcode"]
    
    values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              ""]
    
    let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
    
    nsud.setValue(dictToSend, forKey: "CrmAccountFilterAssociation")
    nsud.synchronize()
    
}

func firstCharactersFromString(type : String , first : String , second : String) -> String {
    
    if type == "FirstLastName" {
        
        if  (first.count > 0) && (second.count > 0){
            
            return String(first.first!) + String(second.first!)
            
        }else if (first.count > 0) {
            
            return String(first.first!)
            
        }else if (second.count > 0) {
            
            return String(second.first!)
            
        }else {
            
            return ""
            
        }
        
    } else {
        
        if (first.count > 0) {
            
            return String(first.first!)
            
        }else{
            
            return ""
            
        }
        
    }
    
}



// MARK: - --------------------------------Alert Loader----------------------------------
// MARK: -


 func loader_Show(controller: UIViewController , strMessage : String , title : String ,style : UIAlertController.Style) -> UIAlertController {
    let alert = UIAlertController(title: title, message:strMessage, preferredStyle: style)
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 5, y: 5, width: 50, height: 50))
    loadingIndicator.hidesWhenStopped = true
    if #available(iOS 13.0, *) {
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
    } else {
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
    }
    loadingIndicator.startAnimating();
    alert.view.addSubview(loadingIndicator)
    
    //controller.present(alert, animated: false, completion: nil)
    return alert
}


// MARK: - --------------------------------String Contains----------------------------------
// MARK: -

func checkIfStringContains(strString : String , strChar : String , countt : Int)-> Bool {
    
    let countString =  strString.components(separatedBy:strChar)
    print(countString.count-1)
    return  countString.count-1 >= countt ? true : false
    
}

func convertToDictionaryyy(text: String) -> [String: Any]? {
    do {
        
        let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? Dictionary<String, Any>
        // print(data)
        let firstElement: NSDictionary = data! as NSDictionary
        return firstElement as? [String : Any]
    } catch {
        print(error.localizedDescription)
    }
    return NSDictionary() as? [String : Any]
}

func addressFormattedByGoogle(value : PlaceDetails) -> String {

   var strAddress = ""

    if(value.name != nil){

        strAddress = "\(value.name!)"

    }

    if(value.locality != nil){

           strAddress = strAddress + ",\(value.locality!)"

       }

    if(value.administrativeAreaCode != nil){

        

        let administrativeAreaCode = "\(value.administrativeAreaCode!)"


        let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(administrativeAreaCode)")

                       if dictStateDataTemp.count > 0 {

                         //  strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"

                        strAddress = strAddress + ",\(dictStateDataTemp.value(forKey: "Name")!)"

                           

                       }

          }

    if(value.postalCode != nil){

              strAddress = strAddress + ",\(value.postalCode!)"

          }

    return strAddress

}



func formatEnteredAddress(value : String) -> NSMutableDictionary {

    let dictAddressData  = NSMutableDictionary()

    

    var aryTempAddress = value.split(separator: ",")

    var strAddressLine1 = "" , strAddressLine2 = "" , strCityName = "" , strStateName = "" , strZipCode = ""


    if(aryTempAddress.count > 3){

       //-------ZipCode-----------

        

        let tempstrZipCode = "\(aryTempAddress.last ?? "")".replacingOccurrences(of: " ", with: "")

        if (tempstrZipCode.isNumeric) {

            if(tempstrZipCode.count == 5){

                strZipCode = tempstrZipCode

            }

        }

        

        //-------State-----------

        aryTempAddress.removeLast()

        strStateName = "\(aryTempAddress.last ?? "")"

        

        //-------City-----------

        aryTempAddress.removeLast()

        strCityName = "\(aryTempAddress.last ?? "")"

        

        //-------Addrss-1----------

        aryTempAddress.removeLast()


        strAddressLine1 = "\(aryTempAddress.first ?? "")"

        

        

        //-------Addrss-2----------

        aryTempAddress.removeFirst()

        for item in aryTempAddress {

            strAddressLine2 = strAddressLine2 + ",\(item)"

        }

        

        if(strAddressLine2.count != 0){

            strAddressLine2 = String(strAddressLine2.dropFirst(1))

        }

        

        

        //----------Blank Check -----------

        if strZipCode.prefix(1) == " " {

            strZipCode = "\(strZipCode)".replacingOccurrences(of: " ", with: "")

        }

        if strAddressLine1.prefix(1) == " " {
            strAddressLine1 = String(strAddressLine1.dropFirst())
        }
        
        if strAddressLine2.prefix(1) == " " {
            strAddressLine2 = String(strAddressLine2.dropFirst())
        }
        
        if strCityName.prefix(1) == " " {
            strCityName = String(strCityName.dropFirst())
        }
        
        if strStateName.prefix(1) == " " {
            strStateName = String(strStateName.dropFirst())
        }

        dictAddressData.setValue(strAddressLine1, forKey: "AddresLine1")

        dictAddressData.setValue(strAddressLine2, forKey: "AddresLine2")

        dictAddressData.setValue(strCityName, forKey: "CityName")

        dictAddressData.setValue(strStateName, forKey: "StateName")

        dictAddressData.setValue(strZipCode, forKey: "ZipCode")

    }

    return dictAddressData

}

// MARK: - --------------------------------Getting No Of Area And Deleting Areas Sub Areas----------------------------------
// MARK: -


func updateFromIds(strType : String , strKey : String , strWoId : String , arrOfIds : NSArray , strEntityName : String) {
    
    for k in 0 ..< arrOfIds.count {

        let idddd = arrOfIds[k] as! String
        
        // Deleting  Area
        
        if strType == "Update"{
            
            // isDeletedArea ko true krna hai yaha par
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add(strKey)
            arrOfValues.add(true)
            
            var isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@", strWoId, idddd,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
             isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && mobileAddressAreaId == %@ && companyKey == %@", strWoId, idddd,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            if isSuccess {
                
                
                
            }
            
        }else{
            
            deleteAllRecordsFromDB(strEntity: strEntityName, predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@",strWoId, idddd ,Global().getCompanyKey()))

        }
        
    }
    
}

func updateServicePestModifyDate(strWoId : String) {
    
    let arrayWO = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        if(arrayWO.count > 0){
        let woObj = arrayWO.object(at: 0) as! NSManagedObject
            let wdoWorkflowStageId = "\(woObj.value(forKey: "wdoWorkflowStageId") ?? "")"

        if WebService().checkIfStageTypeIsIsnpector(obj: woObj, checkflowStageType: true, checkflowStatus: false) && wdoWorkflowStageId != ""{
            
            // Is Inspector Stage Type mttlb WorkOrder Edit kr skte hain to esko false return kr raha hun.
            var strCurrentDateFormatted = String()
            strCurrentDateFormatted = Global().modifyDateService()
            
            Global().fetchServicePestWo(toUpdateModifyDate: strWoId, Global().modifyDateService())
            Global().fetchServicePestWo(toUpdateModifyDateJugad: strWoId)
            Global().fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
        }
        else if(wdoWorkflowStageId == ""){
           
            var strCurrentDateFormatted = String()
            strCurrentDateFormatted = Global().modifyDateService()
            
            Global().fetchServicePestWo(toUpdateModifyDate: strWoId, Global().modifyDateService())
            Global().fetchServicePestWo(toUpdateModifyDateJugad: strWoId)
            Global().fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
        }
    }

}


func updateServicePestModifyDateOld(strWoId : String) {
    
    var strCurrentDateFormatted = String()
    strCurrentDateFormatted = Global().modifyDateService()
    
    Global().fetchServicePestWo(toUpdateModifyDate: strWoId, Global().modifyDateService())
    Global().fetchServicePestWo(toUpdateModifyDateJugad: strWoId)
    Global().fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)

}

func updateModifyDateForWdoNpma(strWoId : String) {
    
    var strCurrentDateFormatted = String()
    strCurrentDateFormatted = Global().modifyDateService()
    
    // Sync Ni krna hai pr date update kr do taki baad mai sync ho jaye.
    //Global().fetchServicePestWo(toUpdateModifyDate: strWoId, Global().modifyDateService())
    Global().fetchServicePestWo(toUpdateModifyDateJugad: strWoId)
    Global().fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)

}

func addSelectInArray(strName : String , array : NSMutableArray) -> NSMutableArray {
    
    let  arrayTemp = NSMutableArray()
    
    let dictTemp = NSDictionary.init(object: strSelectString, forKey: strName as NSCopying)
    
    arrayTemp.add(dictTemp)
    
    arrayTemp.addObjects(from: array as! [Any])
    
    return arrayTemp
    
}

func changeStringDateToGivenFormatWithoutT(strDate: String , strRequiredFormat: String) -> String

{

    let strTimeIn  = strDate

    

    var dateFormatter = DateFormatter()

    dateFormatter.timeZone = NSTimeZone.local

    dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"

    var date1 = Date()

    var strDateFinal = String()

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"

    }

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        

    }

    

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"

    }

    

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

    }

    

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"

    }

    

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"

    }

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "MM/dd/yyyy"

    }

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"

        //MM/dd/yyyy HH:mm

    }

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"

        //MM/dd/yyyy HH:mm

    }

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }
    else{
        
        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy/MM/dd"
        //2022-06-14
    }
    if dateFormatter.date(from: strTimeIn) != nil

    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal

    }

    

    return ""

    

}

func addSelectInArrayNew(strName : String , array : NSMutableArray ,strNameNew : String) -> NSMutableArray {
    
    let  arrayTemp = NSMutableArray()
    
    let dictTemp = NSMutableDictionary()
    
    dictTemp.setValue(strSelectString, forKey: strName)
    dictTemp.setValue(strSelectString, forKey: strNameNew)

    arrayTemp.add(dictTemp)

    arrayTemp.addObjects(from: array as! [Any])
    
    return arrayTemp
    
}

func callingFunction(number : NSString) -> Bool{
    let str = number.replacingOccurrences(of: " ", with:"")
    
    if let url = URL(string: "tel://\(str)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        return true
    }
    else {
        return false
    }
}

func changeStringDateToGivenFormat(strDate: String , strRequiredFormat: String) -> String
{
    let strTimeIn  = strDate
    
    var dateFormatter = DateFormatter()
    dateFormatter.timeZone = NSTimeZone.local
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    var date1 = Date()
    var strDateFinal = String()
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy"
    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
        //MM/dd/yyyy HH:mm
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        //MM/dd/yyyy HH:mm
    }//"hh:mm a"
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "HH:mm:ss"
        
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "hh:mm:ss a"
        
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "HH:mm"
        
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "hh:mm a"
        
    }
    

    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    
    
    return ""
    
}

func changeStringDateToGivenFormatEST(strDate: String , strRequiredFormat: String) -> String
{
    let strTimeIn  = strDate
    
    var dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(abbreviation: "EST")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    var date1 = Date()
    var strDateFinal = String()
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "MM/dd/yyyy"
    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
        //MM/dd/yyyy HH:mm
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        //MM/dd/yyyy HH:mm
    }//"hh:mm a"
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "HH:mm"
        //MM/dd/yyyy HH:mm
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "HH:mm:ss"
        
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    
    return ""
    
}

func changeStringDateToESTDate(strDate: String , strRequiredFormat: String) -> Date
{
    let strTimeIn  = strDate
    
    var dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(abbreviation: "EST")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    var date1 = Date()
    var strDateFinal = String()
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "MM/dd/yyyy"
    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
        //MM/dd/yyyy HH:mm
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        //MM/dd/yyyy HH:mm
    }//"hh:mm a"
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.dateFormat = "HH:mm:ss"
        
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    
    return date1
    
}


func changeStringDateToStringDateInAnyTimeZoneFormat(strDate: String, strTimeZone: String  , strRequiredFormat: String) -> String
{
    let strTimeIn  = strDate
    
    var dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    var date1 = Date()
    var strDateFinal = String()
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "MM/dd/yyyy"
    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
        //MM/dd/yyyy HH:mm
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        //MM/dd/yyyy HH:mm
    }//"hh:mm a"
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "HH:mm"
        //MM/dd/yyyy HH:mm
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "HH:mm:ss"
        
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return strDateFinal
    }
    
    return ""
    
}

func changeStringDateToDateOnlyInAnyTimeZomeFormat(strDate: String, strTimeZone: String  , strRequiredFormat: String) -> Date
{
    let strTimeIn  = strDate
    
    var dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    var date1 = Date()
    var strDateFinal = String()
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
    }
    
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "MM/dd/yyyy"
    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
        //MM/dd/yyyy HH:mm
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        //MM/dd/yyyy HH:mm
    }//"hh:mm a"
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    else
    {
        dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: strTimeZone)
        dateFormatter.dateFormat = "HH:mm:ss"
        
    }
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        date1 = Date()
        strDateFinal = String()
        date1 = dateFormatter.date(from: strTimeIn)!
        dateFormatter.dateFormat = strRequiredFormat
        strDateFinal = dateFormatter.string(from: date1)
        return date1
    }
    
    return date1
    
}

func serverDateToLocal(strDate:String , strServerTimeZone: String, strRequiredFormat: String) -> String {
    
    let strTimeIn  = strDate

    var dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)
    
    if dateFormatter.date(from: strTimeIn) != nil
    {
        let dt = dateFormatter.date(from: strDate)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = strRequiredFormat
        return dateFormatter.string(from: dt!)
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)

    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        let dt = dateFormatter.date(from: strDate)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = strRequiredFormat
        return dateFormatter.string(from: dt!)
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)

    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        let dt = dateFormatter.date(from: strDate)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = strRequiredFormat
        return dateFormatter.string(from: dt!)
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)

    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        let dt = dateFormatter.date(from: strDate)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = strRequiredFormat
        return dateFormatter.string(from: dt!)
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)

    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        let dt = dateFormatter.date(from: strDate)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = strRequiredFormat
        return dateFormatter.string(from: dt!)
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)

    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        let dt = dateFormatter.date(from: strDate)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = strRequiredFormat
        return dateFormatter.string(from: dt!)
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)

    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        let dt = dateFormatter.date(from: strDate)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = strRequiredFormat
        return dateFormatter.string(from: dt!)
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)

    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        let dt = dateFormatter.date(from: strDate)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = strRequiredFormat
        return dateFormatter.string(from: dt!)
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)

    }
    if dateFormatter.date(from: strTimeIn) != nil
    {
        let dt = dateFormatter.date(from: strDate)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = strRequiredFormat
        return dateFormatter.string(from: dt!)
    }
    else
    {
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
        dateFormatter.timeZone = TimeZone(abbreviation: strServerTimeZone)

    }
    

    return ""
    
}

func validateTimeFormat(textField : UITextField , range : NSRange , string : NSString) -> Bool {
    
    if range.location == 0 && (string == " ") {
        return false
    }
    else {
        
        if Int(string.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted).location) != NSNotFound {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return false
        } else {
            let text = textField.text
            let length: Int = text!.count
            var shouldReplace = true
            
            if !(string == "") {
                switch length {
                case 2:
                    textField.text = text! + (":")
                default:
                    break
                }
                if length > 4 {
                    shouldReplace = false
                }
            }
            
            return shouldReplace
        }
    }
    
}
func syncAllTask(strUserName:String, strCompanyKey:String)
{
    let aryNonSyncTask = NSMutableArray()
    let aryJsonToPost = NSMutableArray()
    
    let arrayAllObject = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@"  ,strUserName, strCompanyKey))
    
    for item in arrayAllObject
    {
        
        if("\((item as! NSManagedObject).value(forKey: "leadTaskId")!)".count == 0)
        {
            aryNonSyncTask.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
        }
        
    }
    
    if(aryNonSyncTask.count > 0)
    {
        for item in aryNonSyncTask
        {
            let dictData = item as! NSDictionary
            
            let aryOfKeys = NSMutableArray()
            let aryOfValues = NSMutableArray()
            
            aryOfKeys.add("AssignedTo")
            aryOfKeys.add("ChildActivities")
            aryOfKeys.add("CreatedBy")
            aryOfKeys.add("Description")
            aryOfKeys.add("DueDate")
            aryOfKeys.add("Id")
            aryOfKeys.add("LeadTaskId")
            aryOfKeys.add("Priority")
            aryOfKeys.add("RefId")
            aryOfKeys.add("RefType")
            aryOfKeys.add("ReminderDate")
            aryOfKeys.add("Status")
            aryOfKeys.add("Tags")
            aryOfKeys.add("TaskName")
            
            aryOfValues.add("\(dictData.value(forKey: "assignedTo")!)")
            
            aryOfValues.add(dictData.value(forKey: "childActivities") as! NSArray)
            aryOfValues.add("\(dictData.value(forKey: "createdBy")!)")
            aryOfValues.add("\(dictData.value(forKey: "taskDescription")!)")
            aryOfValues.add("\(dictData.value(forKey: "dueDate")!)")
            aryOfValues.add("1")
            aryOfValues.add("\(dictData.value(forKey: "leadTaskId")!)")
            aryOfValues.add("\(dictData.value(forKey: "priority")!)")
            aryOfValues.add("\(dictData.value(forKey: "refId")!)")
            aryOfValues.add("\(dictData.value(forKey: "refType")!)")
            aryOfValues.add("\(dictData.value(forKey: "reminderDate")!)")
            aryOfValues.add("\(dictData.value(forKey: "status")!)")
            aryOfValues.add("\(dictData.value(forKey: "tags")!)")
            aryOfValues.add("\(dictData.value(forKey: "taskName")!)")
            
            let dictJson = NSDictionary.init(objects: aryOfValues as! [Any], forKeys: aryOfKeys as! [NSCopying])
            
            aryJsonToPost.add(dictJson)
        }
        
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(aryJsonToPost) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: aryJsonToPost, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strHrmsCompanyId = "\(dictLoginData.value(forKeyPath: "Company.HrmsCompanyId")!)"
        
        let strEmployeeNumber = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
        let strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let strCreatedBy = "\(dictLoginData.value(forKey: "CreatedBy")!)"
        let strEmployeeName = "\(dictLoginData.value(forKey: "EmployeeName")!)"
        let strCoreCompanyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId")!)"
        let strSalesProcessCompanyId1 = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
        let strCompanyKeyyy = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let strUrlSaved = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
        
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strUrl = strUrlSaved + UrlAddUpdateTaskGlobalMultiple
        let url = URL(string: strUrl)
        
        let request11 = NSMutableURLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        
        request11.httpMethod = "POST"
        request11.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request11.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request11.addValue("application/json", forHTTPHeaderField: "Accept")
        request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
        request11.addValue("IOS", forHTTPHeaderField: "Browser")
        
        request11.addValue(strHrmsCompanyId, forHTTPHeaderField: "HrmsCompanyId")
        
        request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "IpAddress")
        
        request11.addValue(strEmployeeNumber, forHTTPHeaderField: "EmployeeNumber")
        
        request11.addValue(strEmployeeId, forHTTPHeaderField: "EmployeeId")
        
        request11.addValue(strCreatedBy, forHTTPHeaderField: "CreatedBy")
        
        request11.addValue(strEmployeeName, forHTTPHeaderField: "EmployeeName")
        
        request11.addValue(strCoreCompanyId, forHTTPHeaderField: "CoreCompanyId")
        
        request11.addValue(strSalesProcessCompanyId1, forHTTPHeaderField: "SalesProcessCompanyId")
        
        request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
        
        request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
        
        request11.setValue("\(requestData!.count)", forHTTPHeaderField: "Content-Length")
        
        request11.httpBody = requestData
        
        URLSession.shared.dataTask(with: request11 as URLRequest) { (data, response, error) in
            
            if(error == nil)
            {
                print("Background Syncing Success Task")
            }
            }.resume()
    }
}

func syncAllActivity(strUserName:String, strCompanyKey:String)
{
    let aryNonSyncActivity = NSMutableArray()
    let aryJsonToPost = NSMutableArray()
    
    let arrayAllObject = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@"  ,strUserName, strCompanyKey))
    
    for item in arrayAllObject
    {
        
        if("\((item as! NSManagedObject).value(forKey: "activityId")!)".count == 0)
        {
            aryNonSyncActivity.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
        }
        
    }
    
    if(aryNonSyncActivity.count > 0)
    {
        for item in aryNonSyncActivity
        {
            let dictData = item as! NSDictionary
            
            let aryOfKeys = NSMutableArray()
            let aryOfValues = NSMutableArray()
            
            
            aryOfKeys.add("ToDate")
            aryOfKeys.add("LogTypeId")
            aryOfKeys.add("Address2")
            aryOfKeys.add("EmployeeExtDcs")
            aryOfKeys.add("ActivityTime")
            aryOfKeys.add("ChildTasks")
            aryOfKeys.add("CountryId")
            aryOfKeys.add("ActivityCommentExtDcs")
            aryOfKeys.add("CityName")
            aryOfKeys.add("Agenda")
            aryOfKeys.add("StateId")
            aryOfKeys.add("CreatedDate")
            aryOfKeys.add("RefId")
            aryOfKeys.add("CreatedBy")
            aryOfKeys.add("ZipCode")
            aryOfKeys.add("ModifiedDate")
            aryOfKeys.add("Id")
            aryOfKeys.add("ActivityLogTypeMasterExtDc")
            aryOfKeys.add("Participants")
            aryOfKeys.add("ActivityId")
            aryOfKeys.add("Address1")
            aryOfKeys.add("LeadExtDcs")
            aryOfKeys.add("ModifiedBy")
            aryOfKeys.add("FromDate")
            aryOfKeys.add("RefType")
            
            // values
            aryOfValues.add("\(dictData.value(forKey: "toDate")!)")
            aryOfValues.add("\(dictData.value(forKey: "logTypeId")!)")
            aryOfValues.add("\(dictData.value(forKey: "addressLine2")!)")
            aryOfValues.add(NSArray())//EmployeeExtDcs
            aryOfValues.add("\(dictData.value(forKey: "activityTime")!)")
            aryOfValues.add(dictData.value(forKey: "childTasks") as! NSArray)
            aryOfValues.add("\(dictData.value(forKey: "countryId")!)")
            
            aryOfValues.add(NSArray())//ActivityCommentExtDcs
            aryOfValues.add("\(dictData.value(forKey: "cityName")!)")
            aryOfValues.add("\(dictData.value(forKey: "agenda")!)")
            
            aryOfValues.add("\(dictData.value(forKey: "stateId")!)")
            
            aryOfValues.add("\(dictData.value(forKey: "createdDate")!)")
            aryOfValues.add("\(dictData.value(forKey: "refId")!)")
            aryOfValues.add("\(dictData.value(forKey: "createdBy")!)")
            aryOfValues.add("\(dictData.value(forKey: "zipcode")!)")
            aryOfValues.add("\(dictData.value(forKey: "modifiedDate")!)")
            aryOfValues.add("") // id
            aryOfValues.add(NSArray()) //ActivityLogTypeMasterExtDc
            aryOfValues.add("\(dictData.value(forKey: "participants")!)")
            aryOfValues.add("\(dictData.value(forKey: "activityId")!)")
            aryOfValues.add("\(dictData.value(forKey: "addressLine1")!)")
            aryOfValues.add(NSArray())//LeadExtDcs
            
            //Nilind
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
            
            aryOfValues.add("\(strEmpID)")//modifiedBy
            
            aryOfValues.add("\(dictData.value(forKey: "fromDate")!)")
            aryOfValues.add("\(dictData.value(forKey: "refType")!)")
            
            let dictJson = NSDictionary.init(objects: aryOfValues as! [Any], forKeys: aryOfKeys as! [NSCopying])
            
            aryJsonToPost.add(dictJson)
        }
        
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(aryJsonToPost) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: aryJsonToPost, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strHrmsCompanyId = "\(dictLoginData.value(forKeyPath: "Company.HrmsCompanyId")!)"
        
        let strEmployeeNumber = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
        let strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let strCreatedBy = "\(dictLoginData.value(forKey: "CreatedBy")!)"
        let strEmployeeName = "\(dictLoginData.value(forKey: "EmployeeName")!)"
        let strCoreCompanyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId")!)"
        let strSalesProcessCompanyId1 = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
        let strCompanyKeyyy = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let strUrlSaved = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
        
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strUrl = strUrlSaved + UrlAddUpdateActivityMultiple
        let url = URL(string: strUrl)
        
        let request11 = NSMutableURLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        
        request11.httpMethod = "POST"
        request11.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request11.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request11.addValue("application/json", forHTTPHeaderField: "Accept")
        request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
        request11.addValue("IOS", forHTTPHeaderField: "Browser")
        
        request11.addValue(strHrmsCompanyId, forHTTPHeaderField: "HrmsCompanyId")
        
        request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "IpAddress")
        
        request11.addValue(strEmployeeNumber, forHTTPHeaderField: "EmployeeNumber")
        
        request11.addValue(strEmployeeId, forHTTPHeaderField: "EmployeeId")
        
        request11.addValue(strCreatedBy, forHTTPHeaderField: "CreatedBy")
        
        request11.addValue(strEmployeeName, forHTTPHeaderField: "EmployeeName")
        
        request11.addValue(strCoreCompanyId, forHTTPHeaderField: "CoreCompanyId")
        
        request11.addValue(strSalesProcessCompanyId1, forHTTPHeaderField: "SalesProcessCompanyId")
        
        request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
        
        request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
        
        request11.setValue("\(requestData!.count)", forHTTPHeaderField: "Content-Length")
        
        request11.httpBody = requestData
        
        URLSession.shared.dataTask(with: request11 as URLRequest) { (data, response, error) in
            
            if(error == nil)
            {
                print("Background Syncing Success Activity")
            }
            }.resume()
    }
}

func getLowerCasedDictionary (dictDetail: NSDictionary) -> NSDictionary
{
    let allKey = dictDetail.allKeys as NSArray
    let allValues = dictDetail.allValues as NSArray
    let allKeyNew = NSMutableArray()
    for item in allKey
    {
        var str = item as! String
        str = str.lowerCasedFirstLetter()//lowercased()
        allKeyNew.add(str)
    }
    let dict_ToSend = NSDictionary.init(objects: allValues as! [Any], forKeys: allKeyNew as! [NSCopying])
    
    return dict_ToSend
}

func getStateFromShortName(strStateShortName : String) ->  NSDictionary {
    
    var dictStateData = NSDictionary()
            
    if let path = Bundle.main.path(forResource: StateJsonSwift, ofType: "json") {
        do {
            
            let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
            let arrOfState = (jsonResult as! NSArray).mutableCopy() as! NSMutableArray
            
            for k in 0..<arrOfState.count {
                
                let dictStateDataLocal = arrOfState[k] as! NSDictionary
                var strStateShortNameLocal: String? = nil
                if let value = dictStateDataLocal["StateShortName"] {
                    strStateShortNameLocal = "\(value)"
                }
                if (strStateShortName == strStateShortNameLocal) {
                    dictStateData = dictStateDataLocal
                    break
                    
                }
            }
            
        } catch {
            
            //Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
    }
    
    return dictStateData

}

func getStateDataFromStateName(strStateName : String) ->  NSDictionary {
    
    var dictStateData = NSDictionary()
            
    if let path = Bundle.main.path(forResource: StateJsonSwift, ofType: "json") {
        do {
            
            let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
            let arrOfState = (jsonResult as! NSArray).mutableCopy() as! NSMutableArray
            
            for k in 0..<arrOfState.count {
                
                let dictStateDataLocal = arrOfState[k] as! NSDictionary
                var strStateNameLocal: String? = nil
                if let value = dictStateDataLocal["Name"] {
                    strStateNameLocal = "\(value)"
                }
                
                var strStateNameLocalShort: String? = nil
                if let valueShort = dictStateDataLocal["StateShortName"] {
                    strStateNameLocalShort = "\(valueShort)"
                }
                
                if (strStateNameLocalShort)!.caseInsensitiveCompare(strStateName) == .orderedSame
                {
                    dictStateData = dictStateDataLocal
                    break
                    
                }

                if (strStateNameLocal)!.caseInsensitiveCompare(strStateName) == .orderedSame
                {
                    dictStateData = dictStateDataLocal
                    break
                    
                }

            }
            
        } catch {
            
            //Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
    }
    
    return dictStateData

}

func isPestPacIntegartion() -> Bool {
    // Check if PestPac Integration Enabled or Not
    var dictLoginDataTemp = NSDictionary()
    dictLoginDataTemp = nsud.value(forKey: "LoginDetails") as! NSDictionary
    if (dictLoginDataTemp.value(forKeyPath: "Company.CompanyConfig.IsPestPacIntegration") as! Bool) == true {
        if (dictLoginDataTemp.value(forKeyPath: "Company.PestPacIntegrationConfig.IsPaymentIntegration") as! Bool) == true {
            return true
        }else {
            return false
        }
    }else{
        return false
    }
}


extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension String {
    func heightForString(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        
        return label.frame.height + 50
    }
    func heightForStringTextView(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UITextView(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        //label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        
        return label.frame.height + 50
    }
    func lowerCasedFirstLetter() -> String {
        return prefix(1).lowercased() + dropFirst()
    }
}

extension Dictionary {
    mutating func mergeDict(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}

extension NSDictionary {
    
    public static func + (lhs: NSDictionary, rhs: NSDictionary) -> NSDictionary {
        let dictionary = NSMutableDictionary()
        for (key, value) in lhs {
            if let key = key as? NSCopying {
                dictionary[key] = value
            }
        }
        for (key, valueR) in rhs {
            if let key = key as? NSCopying {
                if let valueL = lhs[key] {
                    switch (valueL, valueR) {
                    case (let dictionaryL as NSDictionary, let dictionaryR as NSDictionary):
                        dictionary[key] = dictionaryL + dictionaryR
                        break
                    case (let arrayL as NSArray, let arrayR as NSArray):
                        dictionary[key] = arrayL + arrayR
                    default:
                        dictionary[key] = valueR
                    }
                }
                else {
                    dictionary[key] = valueR
                }
            }
        }
        return dictionary
    }
    
}

extension NSMutableDictionary {
    
    public static func += (lhs: inout NSMutableDictionary, rhs: NSDictionary) {
        for (key, valueR) in rhs {
            if let valueL = lhs[key] {
                switch (valueL, valueR) {
                case (let dictionaryL as NSDictionary, let dictionaryR as NSDictionary):
                    lhs[key] = dictionaryL + dictionaryR
                case (let arrayL as NSArray, let arrayR as NSArray):
                    lhs[key] = arrayL + arrayR
                default:
                    lhs[key] = valueR
                }
            }
            else {
                lhs[key] = valueR
            }
        }
    }
    
}

extension NSArray {
    
    public static func + (lhs: NSArray, rhs: NSArray) -> NSArray {
        let array = NSMutableArray()
        lhs.forEach { array.add($0) }
        rhs.forEach { array.add($0) }
        return array
    }
    
}

extension NSMutableArray {
    
    public static func += (lhs: inout NSMutableArray, rhs: NSArray) {
        for value in rhs {
            lhs.add(value)
        }
    }
    
}


extension UIImageView {
    func dowloadFromServer(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func dowloadFromServer(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        dowloadFromServer(url: url, contentMode: mode)
    }
    func load(url: URL, strImageName: String)
    {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url)
            {
                if let image = UIImage(data: data)
                {
                    DispatchQueue.main.async
                        {
                            
                            self?.image = image
                            saveImageDocumentDirectory(strFileName: strImageName , image: image)
                            
                    }
                }
            }
        }
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
extension String {
//    var html2AttributedString: NSAttributedString? {
//        return Data(utf8).html2AttributedString
//    }
//    var html2String: String {
//        return html2AttributedString?.string ?? ""
//    }
    
    var html2AttributedString: NSAttributedString? {
         return Data(utf8).html2AttributedString
     }
     var html2String: String {
         
         if(html2AttributedString?.string.trimmingCharacters(in: .whitespaces) ?? "").count > 2{
             let last2 = String(html2AttributedString?.string.trimmingCharacters(in: .whitespaces) ?? "").suffix(1)
             if(last2.contains("\n")){
                 return String(String(html2AttributedString?.string.trimmingCharacters(in: .whitespaces) ?? "").dropLast(1))
             }
             
             
         }

         return html2AttributedString?.string ?? ""
     }
}

extension String {
    func htmlAttributedStringInGlobal(fontSize: CGFloat = 22.0) -> NSAttributedString? {
        let fontName = UIFont.systemFont(ofSize: fontSize).fontName
        
        
        /*let strTemp = self
        let strStyleString = (String(format: "<style>body{font-family: '%@'; font-size:%fpx;}</style>", fontName, fontSize))
        let string = strStyleString.appending(strTemp)*/
        
        let string = self.appending(String(format: "<style>body{font-family: '%@'; font-size:%fpx;}</style>", fontName, fontSize))
        guard let data = string.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        
        guard let html = try? NSMutableAttributedString (
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
        
    }
}

public enum PlaceType: String {
    case all = ""
    case geocode
    case address
    case establishment
    case regions = "(regions)"
    case cities = "(cities)"
}

open class Place: NSObject {
    public let id: String
    public let mainAddress: String
    public let secondaryAddress: String
    
    override open var description: String {
        get { return "\(mainAddress), \(secondaryAddress)" }
    }
    
    public init(id: String, mainAddress: String, secondaryAddress: String) {
        self.id = id
        self.mainAddress = mainAddress
        self.secondaryAddress = secondaryAddress
    }
    
    convenience public init(prediction: [String: Any]) {
        let structuredFormatting = prediction["structured_formatting"] as? [String: Any]
        
        self.init(
            id: prediction["place_id"] as? String ?? "",
            mainAddress: structuredFormatting?["main_text"] as? String ?? "",
            secondaryAddress: structuredFormatting?["secondary_text"] as? String ?? ""
        )
    }
}

open class PlaceDetails: CustomStringConvertible {
    public let formattedAddress: String
    open var name: String? = nil

    open var streetNumber: String? = nil
    open var route: String? = nil
    open var postalCode: String? = nil
    open var country: String? = nil
    open var countryCode: String? = nil

    open var locality: String? = nil
    open var subLocality: String? = nil
    open var administrativeArea: String? = nil
    open var administrativeAreaCode: String? = nil
    open var subAdministrativeArea: String? = nil
    
    open var coordinate: CLLocationCoordinate2D? = nil
    
    init?(json: [String: Any]) {
        guard let result = json["result"] as? [String: Any],
            let formattedAddress = result["formatted_address"] as? String
            else { return nil }
        
        self.formattedAddress = formattedAddress
        self.name = result["name"] as? String
        
        if let addressComponents = result["address_components"] as? [[String: Any]] {
            streetNumber = get("street_number", from: addressComponents, ofType: .short)
            route = get("route", from: addressComponents, ofType: .short)
            postalCode = get("postal_code", from: addressComponents, ofType: .long)
            country = get("country", from: addressComponents, ofType: .long)
            countryCode = get("country", from: addressComponents, ofType: .short)
            
            locality = get("locality", from: addressComponents, ofType: .long)
            subLocality = get("sublocality", from: addressComponents, ofType: .long)
            administrativeArea = get("administrative_area_level_1", from: addressComponents, ofType: .long)
            administrativeAreaCode = get("administrative_area_level_1", from: addressComponents, ofType: .short)
            subAdministrativeArea = get("administrative_area_level_2", from: addressComponents, ofType: .long)
        }
        
        if let geometry = result["geometry"] as? [String: Any],
            let location = geometry["location"] as? [String: Any],
            let latitude = location["lat"] as? CLLocationDegrees,
            let longitude = location["lng"] as? CLLocationDegrees {
            coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
    }
    
    open var description: String {
        return "\nAddress: \(formattedAddress)\ncoordinate: (\(coordinate?.latitude ?? 0), \(coordinate?.longitude ?? 0))\n"
    }
}

private extension PlaceDetails {
    
    enum ComponentType: String {
        case short = "short_name"
        case long = "long_name"
    }
    func get(_ component: String, from array: [[String: Any]], ofType: ComponentType) -> String? {
        return (array.first { ($0["types"] as? [String])?.contains(component) == true })?[ofType.rawValue] as? String
    }
}
func formattedNumber(number: String) -> String {
    if(number.count == 0){
           return ""
       }
    let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    let mask = "XXX-XXX-XXXX"

    var result = ""
    var index = cleanPhoneNumber.startIndex
    for ch in mask where index < cleanPhoneNumber.endIndex {
        if ch == "X" {
            result.append(cleanPhoneNumber[index])
            index = cleanPhoneNumber.index(after: index)
        } else {
            result.append(ch)
        }
    }
    return result
}

func convertDoubleToCurrency(amount: Double) -> String{
       let numberFormatter = NumberFormatter()

       numberFormatter.numberStyle = .currency
       numberFormatter.locale = Locale(identifier: "en_US")
       return numberFormatter.string(from: NSNumber(value: amount))!
   }

func getImageFromDocumentDirectory(strImageName : String) -> UIImage {
    
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).map(\.path)
    let documentsDirectory = paths[0]
    let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(strImageName)").path
    let imagee = UIImage(contentsOfFile: path) ?? UIImage()
    
    return imagee
    
}

func getOnlyNumberFromString(number : String) -> String {
    
    let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()

    return "\(cleanPhoneNumber)"
    
}

func getSourceNameFromId(sourceId: String) -> String
{
    var sourceName = String()
    sourceName = ""
    
    let defsLogindDetail = UserDefaults.standard
    
    if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary
    {
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary

        if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray
        {
            let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
            for item in arrOfData
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "SourceId") ?? "")" == sourceId
                {
                    sourceName = "\(dict.value(forKey: "Name") ?? "")"
                    break
                }
                
            }
        }
    }
    
//    let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
//    let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
//    for item in arrOfData
//    {
//        let dict = item as! NSDictionary
//
//        if "\(dict.value(forKey: "SourceId") ?? "")" == sourceId
//        {
//            sourceName = "\(dict.value(forKey: "Name") ?? "")"
//            break
//        }
//
//    }
    return sourceName
}

extension NSMutableAttributedString {

    func trimmedAttributedString(set: CharacterSet) -> NSMutableAttributedString {

        let invertedSet = set.inverted

        var range = (string as NSString).rangeOfCharacter(from: invertedSet)
        let loc = range.length > 0 ? range.location : 0

        range = (string as NSString).rangeOfCharacter(
                            from: invertedSet, options: .backwards)
//        let len = (range.length > 0 ? NSMaxRange(range) : string.characters.count) - loc
        let len = (range.length > 0 ? NSMaxRange(range) : string.count) - loc

        let r = self.attributedSubstring(from: NSMakeRange(loc, len))
        return NSMutableAttributedString(attributedString: r)
    }
}
func changeDateToString(date: Date)-> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy"
    let strDate = dateFormatter.string(from: date)
    return strDate
}

func replaceBackSlasheFromUrl(strUrl : String) -> String {
    
    var imgUrlNew = strUrl
    
    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\", with: "/")
    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\", with: "/")
    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\", with: "/")
    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\", with: "/")
    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\", with: "/")
    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
    
    return imgUrlNew
    
}

func getIconNameViaTaskTypeId(strId : String) -> String {
    //IconName
    
    var strIconName = "Default"
    
    let formatNew = "TaskTypeId == " + strId
    
    if strId == "" || strId == "<null>" {
        
        strIconName = "Default"
        
    } else {
        
        let dictTotalLeadCountResponse = (nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary).mutableCopy()as! NSMutableDictionary

        let aryTemp = (dictTotalLeadCountResponse.value(forKey: "TaskTypeMasters") as! NSArray).mutableCopy() as! NSMutableArray
            let resultPredicate = NSPredicate(format: formatNew)
        let arrayTaskTypeMasters = (aryTemp.filtered(using: resultPredicate) as NSArray).mutableCopy() as! NSMutableArray
        if arrayTaskTypeMasters.count > 0 {
            
            let dictData = arrayTaskTypeMasters[0] as! NSDictionary
            
            strIconName = "\(dictData.value(forKey: "IconName") ?? "Default")"
            
            if strIconName == "Call" || strIconName == "Email" || strIconName == "Lunch" || strIconName == "Meeting" || strIconName == "Deadline" || strIconName == "Task" || strIconName == "Default"{
                

            }else{
                
                strIconName = "Default"

            }
            
        }
        
    }
    
    return strIconName + "_CRMM"
    
}

func getYesterdaysDate()-> Date{
    
    
       let timeInterval = floor(Date().timeIntervalSinceReferenceDate / 86400) * 86400
       let yesterdaydate = Date(timeIntervalSinceReferenceDate: timeInterval - 86400)
       print(yesterdaydate)
       
       return yesterdaydate
    
 /*   var dateComponents = DateComponents()
    dateComponents.timeZone = TimeZone(abbreviation: "UTC")
    dateComponents.setValue(-1, for: .timeZone);
    
    let now = Date() // Current date
    let tomorrow = Calendar.current.date(byAdding: dateComponents, to: now)
    
    return tomorrow!*/

}

func getTodaysDate()-> Date{
    
    let timeInterval = floor(Date().timeIntervalSinceReferenceDate / 86400) * 86400
    let currentdate = Date(timeIntervalSinceReferenceDate: timeInterval)
    print(currentdate)
    
    return currentdate
}

func getTomorrowsDate()-> Date{
    
    
    let timeInterval = floor(Date().timeIntervalSinceReferenceDate / 86400) * 86400
    let tomorrowdate = Date(timeIntervalSinceReferenceDate: timeInterval + 86400)
    print(tomorrowdate)
    
    return tomorrowdate
    
    
   /* var dateComponents = DateComponents()
    dateComponents.timeZone = TimeZone(abbreviation: "UTC")
    dateComponents.setValue(1, for: .day);
    
    let now = Date() // Current date
    let tomorrow = Calendar.current.date(byAdding: dateComponents, to: now)
    
    return tomorrow!*/

}
class TopView:UIView{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
       setUpView()
     }
    
    private func setUpView(){
        
        self.backgroundColor = hexStringToUIColor(hex: appThemeColor)
    }
}


// 13 April 2020 Akshay

func addActivity(type:String,dictAssociate:NSMutableDictionary){
    
    let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
    let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
    let agenda = type == "Call" ? "Call Customer" : "Text Customer"
   
    // for current time and date
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    let strCurrentTime = formatter.string(from: Date())
    formatter.dateFormat = "MM/dd/yyyy"
    let strCurrentDate = formatter.string(from: Date())
  
    
    ///////////////////////////////////////////////
    let dictToSend = NSMutableDictionary()
    dictToSend.setValue("0", forKey: "Id")
    dictToSend.setValue(strCurrentDate, forKey: "FromDate")//
    dictToSend.setValue(strCurrentDate, forKey: "ToDate")
    dictToSend.setValue(strEmpID, forKey: "Participants")//
    dictToSend.setValue("", forKey: "LogTypeId")
   
    dictToSend.setValue(agenda, forKey: "Agenda")
    
    
    dictToSend.setValue("", forKey: "Address1")
    dictToSend.setValue("", forKey: "Address2")
    dictToSend.setValue("", forKey: "CityName")
    dictToSend.setValue("", forKey: "StateId")
    dictToSend.setValue("", forKey: "CountryId")
    dictToSend.setValue("", forKey: "ZipCode")
    
    dictToSend.setValue(Global().getModifiedDate(), forKey: "CreatedDate")
    dictToSend.setValue(NSArray(), forKey: "ActivityLogTypeMasterExtDc")
    dictToSend.setValue("", forKey: "RefType")
    dictToSend.setValue("", forKey: "RefId")
    dictToSend.setValue("", forKey: "ActivityId")
    dictToSend.setValue(NSArray(), forKey: "ActivityCommentExtDcs")
    dictToSend.setValue(NSArray(), forKey: "LeadExtDcs")
    dictToSend.setValue(NSArray(), forKey: "EmployeeExtDcs")
    dictToSend.setValue("\(dictLoginData.value(forKey: "CreatedBy")!)", forKey: "CreatedBy")
    dictToSend.setValue(Global().getModifiedDate(), forKey: "ModifiedDate")
    dictToSend.setValue("", forKey: "ModifiedBy")
    dictToSend.setValue("", forKey: "ChildTasks")
  
    dictToSend.setValue(strCurrentTime, forKey: "ActivityTime")
    
     var strACID = ""
     var strLeadId = ""
     var strWebLeadId = ""
     var strCrmContactId = ""
     var strCrmCompanyId = ""
    
    if let acId = dictAssociate.value(forKey: "AccountId"){
        
        strACID = "\(acId)"
    }
    
    if let leadId = dictAssociate.value(forKey: "LeadId"){
        
        strLeadId = "\(leadId)"
    }
    
    if let webleadId = dictAssociate.value(forKey: "WebLeadId"){
        
        strWebLeadId = "\(webleadId)"
    }
    
    if let contactId = dictAssociate.value(forKey: "CrmContactId"){
        
        strCrmContactId = "\(contactId)"
    }
    if let companyId = dictAssociate.value(forKey: "CrmCompanyId"){
        
        strCrmCompanyId = "\(companyId)"
    }
     
     dictToSend.setValue(strACID == "0" ? "" : strACID, forKey: "AccountId")
     dictToSend.setValue(strLeadId == "0" ? "" : strLeadId, forKey: "LeadId")
     dictToSend.setValue(strWebLeadId == "0" ? "" : strWebLeadId, forKey: "WebLeadId")
     dictToSend.setValue(strCrmContactId == "0" ? "" : strCrmContactId, forKey: "CrmContactId")
     dictToSend.setValue(strCrmCompanyId == "0" ? "" : strCrmCompanyId, forKey: "CrmCompanyId")
        
     dictToSend.setValue("", forKey: "LeadTaskId")
    
    let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddUpdateActivityGlobal
    var jsonString = String()
    
    if(JSONSerialization.isValidJSONObject(dictToSend) == true)
    {
        let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
        jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        print(jsonString)
    }
    let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
    let strTypee = "addActivity"
   
    Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
        
        if(success)
        {
            print((response as NSDictionary?)!)
        }
    }
}


//MARK: ------------------------- Nilind New Sales Function ----------------------------


func getLeadDetail(strLeadId: String, strUserName: String) -> NSManagedObject {
    
    var matches = NSManagedObject()
    let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadId))
    
    if arryOfData.count > 0
    {
        matches = arryOfData.object(at: 0) as! NSManagedObject
    }
    return matches
}



//MARK: ---------------- Get Category, Service and Frequency Object Methods --------------------


func getFrequency() -> (arrServiceFreq: NSArray, arrBillingFreq: NSArray)
{
    var arrTempServiceFreq = NSArray()
    var arrTempBillingFreq = NSArray()

    
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "Frequencies") is NSArray){
                arrTempServiceFreq = dictMaster.value(forKey: "Frequencies") as! NSArray
            
                //Frequency as per type
                
                let arrTemp = NSMutableArray()
                let arrTemp2 = NSMutableArray()

                if (arrTempServiceFreq.count  > 0)
                {
                    for item in arrTempServiceFreq
                    {
                        let dict = item as! NSDictionary
                        let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                        if strType == "Service" || strType == "Both" || strType == ""
                        {
                            arrTemp.add(dict)
                        }
                        if strType == "Billing" || strType == "Both" || strType == ""
                        {
                            arrTemp2.add(dict)
                        }
                        
                    }
                }
                
                arrTempServiceFreq = arrTemp as NSArray
                arrTempBillingFreq = arrTemp2 as NSArray

            }
        }
    return (arrTempServiceFreq,arrTempBillingFreq)
}

func getCategoryObjectFromSysName(strSysName: String) -> NSDictionary
{
    var dictObject = NSDictionary()

    let arrCategory = Global().getCategoryDeptWiseGlobal()! as NSArray
    
    for itemCategory in arrCategory
    {
        let dictCategory = itemCategory as! NSDictionary
        
        if strSysName == "\(dictCategory.value(forKey: "SysName") ?? "")"
        {
            dictObject = dictCategory
            break
        }
    }
    
    return dictObject
}
func getCategoryObjectFromServiceSysName(strServiceSysName: String) -> NSDictionary
{
    var dictObject = NSDictionary()

    let arrCategory = Global().getCategoryDeptWiseGlobal()! as NSArray
    
    for item in arrCategory
    {
        let dict = item as! NSDictionary
        
        if dict.value(forKey: "Services") is NSArray
        {
            let arrServiceTemp = dict.value(forKey: "Services") as! NSArray
            
            for itemService in arrServiceTemp
            {
                let dictServiceNew = itemService as! NSDictionary
                
                if "\(dictServiceNew.value(forKey: "SysName" ) ?? "")" == strServiceSysName
                {
                    dictObject = dict

                    break
                }
            }
        }
    }
    
    return dictObject

}
func getPackageObjectFromPackageId(strServiceSysName: String, strPackageId: String) -> NSDictionary
{
    var isFound = false
    var dictObject = NSDictionary()
    
    let arrCategory = Global().getCategoryDeptWiseGlobal()! as NSArray
    
    for item in arrCategory
    {
        let dict = item as! NSDictionary
        
        if dict.value(forKey: "Services") is NSArray
        {
            let arrServiceTemp = dict.value(forKey: "Services") as! NSArray
            
            for itemService in arrServiceTemp
            {
                let dictServiceNew = itemService as! NSDictionary
                
                if "\(dictServiceNew.value(forKey: "SysName" ) ?? "")" == strServiceSysName
                {
                    if dictServiceNew.value(forKey: "ServicePackageDcs") is NSArray
                    {
                        let arrPackage = dictServiceNew.value(forKey: "ServicePackageDcs") as! NSArray
                        
                        for itemPackage in  arrPackage
                        {
                            let dictPackage = itemPackage as! NSDictionary
                            
                            if "\(dictPackage.value(forKey: "ServicePackageId") ?? "")" == strPackageId
                            {
                                isFound = true
                                dictObject = dictPackage
                                break
                            }
                        }
                        
                    }
                }
                if isFound
                {
                    break
                }
            }
            
        }
        
        if isFound
        {
            break
        }
    }
    
    return dictObject

}

func getFrequencyObjectFromIdAndSysName(strId : String, strSysName: String) -> NSDictionary {
   
    var dictObject = NSDictionary()

    
    if(nsud.value(forKey: "MasterSalesAutomation") != nil){
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMaster.value(forKey: "Frequencies") is NSArray){
            let arrFrequency = dictMaster.value(forKey: "Frequencies") as! NSArray
            
            var strVal = ""
            
            if !strId.isEmpty
            {
                strVal = strId
            }
            if !strSysName.isEmpty
            {
                strVal = strSysName
            }
            
            for item in arrFrequency
            {
                let dict = item as! NSDictionary
                
                if strId.count > 0
                {
                    if "\(dict.value(forKey: "FrequencyId") ?? "")" == strVal
                    {
                        dictObject = dict
                        break
                    }
                }
                else
                {
                    if "\(dict.value(forKey: "SysName") ?? "")" == strVal
                    {
                        dictObject = dict
                        break
                    }
                }
                
            }
        }
    }

    return dictObject
}

func getServiceObjectFromIdAndSysName(strId : String, strSysName: String) -> NSDictionary {
   
    var dictObject = NSDictionary()
    let arrCategory = Global().getCategoryDeptWiseGlobal()! as NSArray

    var strVal = ""
    
    if !strId.isEmpty
    {
        strVal = strId
    }
    if !strSysName.isEmpty
    {
        strVal = strSysName
    }
    
    for item in arrCategory
    {
        let dict = item as! NSDictionary
        
        if dict.value(forKey: "Services") is NSArray
        {
            let arrService = dict.value(forKey: "Services") as! NSArray
            
            for itemService in arrService
            {
                let dictService = itemService as! NSDictionary
                
                if strId.count > 0
                {
                    if strVal == "\(dictService.value(forKey: "ServiceMasterId") ?? "")"
                    {
                        dictObject = dictService
                        
                        break
                    }
                }
                else
                {
                    if strVal == "\(dictService.value(forKey: "SysName") ?? "")"
                    {
                        dictObject = dictService
                        
                        break
                    }
                }
                
                
            }
        }
    }
    
    return dictObject
}

func getDepartmentObjectFromDeptSysName(strDeptSysName: String) -> NSDictionary {
   
    var dictObject = NSDictionary()
    
    if(nsud.value(forKey: "LeadDetailMaster") != nil){
        
        let dictMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
        
        if(dictMaster.value(forKey: "BranchMasters") is NSArray){
            
            let arrBranchMaster = dictMaster.value(forKey: "BranchMasters") as! NSArray
            
            for item in arrBranchMaster
            {
                let dictBranch = item as! NSDictionary
                
                if dictBranch.value(forKey: "Departments") is NSArray
                {
                    let arrDepartments = dictBranch.value(forKey: "Departments") as! NSArray

            
                    for itemDept in arrDepartments {
                        
                        let dictDept = itemDept as! NSDictionary
                        
                        if "\(dictDept.value(forKey: "SysName") ?? "")" == strDeptSysName
                        {
                            dictObject = dictDept
                            break
                        }

                    }

                }
                if dictObject.count > 0
                {
                    break
                }
                
            }
        }
        
    }
    
    return dictObject
}

func getBranchWiseDepartment(strBranchSysName: String) -> NSArray {
    
    let arrDept = NSMutableArray()
    
    if(nsud.value(forKey: "LeadDetailMaster") != nil){
        
        let dictMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
        
        if(dictMaster.value(forKey: "BranchMasters") is NSArray){
            
            let arrBranchMaster = dictMaster.value(forKey: "BranchMasters") as! NSArray
            
            for item in arrBranchMaster
            {
                let dictBranch = item as! NSDictionary
                
                if dictBranch.value(forKey: "Departments") is NSArray
                {
                    let arrDepartments = dictBranch.value(forKey: "Departments") as! NSArray

            
                    for itemDept in arrDepartments {
                        
                        let dictDept = itemDept as! NSDictionary
                        
                        if "\(dictBranch.value(forKey: "SysName") ?? "")" == strBranchSysName && "\(dictBranch.value(forKey: "IsActive") ?? "")" == "1"
                        {
                            arrDept.add(dictDept)
                        }
                        
                    }

                }
  
            }
        }
        
    }
    
    return arrDept as NSArray
}

func getDeptSysNameFromServiceSysName(strServiceSysName : String) -> String {
    
    if(nsud.value(forKey: "LeadDetailMaster") != nil){
        
        let dictMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
        
        if(dictMaster.value(forKey: "ServiceMasters") is NSArray){
            
            let arrServiceMasters = dictMaster.value(forKey: "ServiceMasters") as! NSArray
            
            for item in arrServiceMasters
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "ServiceSysName") ?? "")" == strServiceSysName
                {
                    return "\(dict.value(forKey: "DepartmentSysName") ?? "")"
                }
            }
        }
        
    }
        
    return ""
}

func getBundleObjectFromId(strId : String) -> NSDictionary {
    
    var dictObject = NSDictionary()
    
    if(nsud.value(forKey: "MasterSalesAutomation") != nil){
        
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMaster.value(forKey: "ServiceBundles") is NSArray){
            
            let arrBundle = dictMaster.value(forKey: "ServiceBundles") as! NSArray
           
            let arrObject = arrBundle.filter { (dict) -> Bool in
                
                return "\((dict as! NSDictionary).value(forKey: "ServiceBundleId") ?? "")".caseInsensitiveCompare(strId) == .orderedSame
            } as NSArray
               
            if arrObject.count > 0
            {
                dictObject = arrObject.object(at: 0) as! NSDictionary
            }
        }
    }
    
    return dictObject

}

func getCouponDiscountMaster() -> NSArray {
    
    var arrObject = NSArray()
    
    if(nsud.value(forKey: "MasterSalesAutomation") != nil){
        
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMaster.value(forKey: "DiscountSetupMasterCoupon") is NSArray){
            
            arrObject = dictMaster.value(forKey: "DiscountSetupMasterCoupon") as! NSArray
        }
    }
    
    return arrObject
}

func getCouponObject(strDisocuntName : String) -> NSDictionary {
    
    var dictObject = NSDictionary()
    var arrObject = NSArray()

    if(nsud.value(forKey: "MasterSalesAutomation") != nil){
        
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMaster.value(forKey: "DiscountSetupMasterCoupon") is NSArray){
            
            let arr = dictMaster.value(forKey: "DiscountSetupMasterCoupon") as! NSArray
            arrObject = arr.filter { (dict) -> Bool in
                
                return "\((dict as! NSDictionary).value(forKey: "DiscountCode") ?? "")".caseInsensitiveCompare(strDisocuntName) == .orderedSame
            } as NSArray
        }
    }
    if arrObject.count > 0
    {
        dictObject = arrObject.object(at: 0) as! NSDictionary
    }
    
    return dictObject
}
func getAgreementCheckListObject(strCheckListId : String) -> NSDictionary {
    
    var dictObject = NSDictionary()
    var arrObject = NSArray()

    if(nsud.value(forKey: "MasterSalesAutomation") != nil){
        
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMaster.value(forKey: "AgreementChecklist") is NSArray){
            
            let arr = dictMaster.value(forKey: "AgreementChecklist") as! NSArray
            arrObject = arr.filter { (dict) -> Bool in
                
                return "\((dict as! NSDictionary).value(forKey: "AgreementChecklistId") ?? "")".caseInsensitiveCompare(strCheckListId) == .orderedSame
            } as NSArray
        }
    }
    
    if arrObject.count > 0
    {
        dictObject = arrObject.object(at: 0) as! NSDictionary
    }
    
    return dictObject
}

//End

extension String {

    var isNumeric: Bool {

        guard self.count > 0 else { return false }

        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

        return Set(self).isSubset(of: nums)

    }

}

func attributedTextFontUpdate(str : NSAttributedString , strMinFont : Int) -> NSAttributedString {
    let newStr = str.mutableCopy() as! NSMutableAttributedString
    newStr.beginEditing()
    newStr.enumerateAttribute(.font, in: NSRange(location: 0, length: newStr.string.utf16.count)) { (value, range, stop) in
        if let oldFont = value as? UIFont {
            var fontSize = strMinFont
            if(Int(oldFont.pointSize) > strMinFont){
                fontSize = Int(oldFont.pointSize)
            }
            let newFont = oldFont.withSize(CGFloat(fontSize)) // whatever size you need
            newStr.addAttribute(.font, value: newFont, range: range)
        }
    }
    newStr.endEditing()
     return newStr
}
var fontMin = 14
extension NSAttributedString {
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)

        return boundingBox.height
    }

}


//LogIN Check
func Check_Login_Session_expired() -> Bool  {
   
    if nsud.value(forKey: "LoginDetails") is NSDictionary {
        return false
    }
    return true
}

func Login_Session_Alert(viewcontrol : UIViewController) {
    let alert = UIAlertController(title: "ALert", message: "Some error occured Please login again.", preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "Go to Login", style: .default, handler: { (nil) in
       
    
        if(DeviceType.IS_IPAD){
            
            for controller in viewcontrol.navigationController!.viewControllers as Array {
                if controller.isKind(of: LoginViewControlleriPad.self) {
                    viewcontrol.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            
//            let story = UIStoryboard(name: "MainiPad", bundle:nil)
//            let vc = story.instantiateViewController(withIdentifier: "LoginViewControlleriPad") as! LoginViewControlleriPad
//            UIApplication.shared.windows.first?.rootViewController = vc
//            UIApplication.shared.windows.first?.makeKeyAndVisible()
        
        }else{
            
            let story = UIStoryboard(name: "Main", bundle:nil)
            let vc = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            UIApplication.shared.windows.first?.rootViewController = vc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        
            
        
        }
        
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}

func isScheduleThirdParty() -> Bool {
    if(nsud.value(forKey: "LoginDetails") != nil && nsud.value(forKey: "LoginDetails") is NSDictionary){
        let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let BlankScheduleinThirdParty = "\(dict.value(forKeyPath: "Company.CompanyConfig.BlankScheduleinThirdParty") ?? "")"
        if BlankScheduleinThirdParty.lowercased() == "true" || BlankScheduleinThirdParty == "1"  {
            return true
        }
        return false
    }
    return false
}


