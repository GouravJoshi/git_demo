//
//  EquipmentsViewController.h
//  DPS
//
//  Created by Saavan Patidar on 16/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "WorkOrderEquipmentDetails+CoreDataProperties.h"
#import "AppDelegate.h"
#import "EquipmentDynamicForm+CoreDataClass.h"
#import "WorkOrderDetailsService+CoreDataProperties.h"

@interface EquipmentsViewControlleriPad : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityWoEquipment;
    NSManagedObjectContext *contextWoEquipment;
    NSFetchRequest *requestWoEquipment;
    NSSortDescriptor *sortDescriptorWoEquipment;
    NSArray *sortDescriptorsWoEquipment;
    NSManagedObject *matchesWoEquipment;
    NSArray *arrAllObjWoEquipment;
    
    NSManagedObjectContext *context;
    NSEntityDescription *entityModifyDateServiceAuto;
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;

    
    NSEntityDescription *entitySalesDynamic;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;

    UIView *MainViewForm,*ViewFormSections,*viewForm,*viewSection;
    UIButton *BtnMainView;
    
    //New change on 24 august 2016 for dynamic form
    NSMutableArray *arrayViews;
    NSMutableArray *arrayOfButtonsMain;
    NSMutableArray *arrOfHeightsViews;
    NSMutableArray *arrOfYAxisViews;
    NSMutableArray *arrOfButtonImages;

}

@property (strong, nonatomic) IBOutlet UIView *view_IntsallNewEquipments;
@property (strong, nonatomic) IBOutlet UIButton *btnDepartmentName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_LeadnAccNo;
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_Category;
- (IBAction)action_Category:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_Equipment;
- (IBAction)action_Equipments:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_EquipmentsName;
- (IBAction)actin_EquipmentName:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_Area;
- (IBAction)action_Area:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_AssociatedWith;
- (IBAction)action_AssociatedWith:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_InstalledOn;
- (IBAction)action_InstalledOn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_NextServiceDate;
- (IBAction)action_NextServiceDate:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_WarrantyDate;
@property (strong, nonatomic) IBOutlet UIView *viewSavenCancel;
- (IBAction)action_WarrantyDate:(id)sender;
- (IBAction)action_AddNewEquipment:(id)sender;
- (IBAction)action_SaveFinalData:(id)sender;
- (IBAction)action_Cancel:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtView_Notes;
@property (strong, nonatomic) IBOutlet UIView *view_SavenCancelButtons;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVieww;
@property (strong, nonatomic) IBOutlet UIView *view_EquipmentList;
@property (strong, nonatomic) IBOutlet UITableView *equipmentTblview;
@property (strong, nonatomic)  NSString *strStatusWo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWoEquipmentService;
@property (strong, nonatomic) NSDictionary *dictJsonDynamicFormEquip;
@property (strong, nonatomic) NSArray *arrChemicalListEquip;
@property (strong, nonatomic) NSDictionary *dictOfChemicalListEquip;
@property (strong, nonatomic) NSArray *arrOfChemicalListEquip;
@property (strong, nonatomic) NSArray *arrOfChemicalListOtherEquip;
@property (strong, nonatomic) NSManagedObject *workOrderDetailEquip;
@property (strong, nonatomic) NSManagedObject *paymentInfoDetailEquip;
@property (strong, nonatomic) NSArray *arrAllObjImageDetailEquip;
@property (strong, nonatomic) NSString *strPaymentModesEquip;
@property (strong, nonatomic) NSString *strDepartmentIddEquip;
@property (strong, nonatomic) NSString *strPaidAmountEquip;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) IBOutlet UIView *viewBackgroundDynamicForm;
@property (strong, nonatomic) IBOutlet UIView *viewEquipmentDynamicForm;
- (IBAction)action_CancelDynamcView:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewDynamicForm;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic;
- (IBAction)action_FinalSaveDynamicForm:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_SerialNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_ModelNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_Manufaturer;

@property (strong, nonatomic) IBOutlet UIView *viewTextFieldSearch;
@property (strong, nonatomic) IBOutlet UILabel *lblSearchHead;
@property (strong, nonatomic) IBOutlet UITableView *tblViewTxtSearchResults;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldSearch;
- (IBAction)action_SaveSearchResult:(id)sender;
- (IBAction)action_CancelSearchResult:(id)sender;
- (IBAction)action_AddEquipments:(id)sender;
- (IBAction)action_CancelEquipmetAddView:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAddEquip;
@property (strong, nonatomic) IBOutlet UIView *scannerPreviewView;
- (IBAction)action_OpenScannerView:(id)sender;
- (IBAction)action_CancelScanning:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnTorch;
- (IBAction)action_TorchMode:(id)sender;

@end
