//
//  EditImageViewControlleriPad.h
//  DPS
//
//  Created by Saavan Patidar on 19/04/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditImageViewControlleriPad : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewSignature;
- (IBAction)actionOnSaveSign:(id)sender;
- (IBAction)actionOnClearSign:(id)sender;
- (IBAction)actionOnExitSign:(id)sender;
@property (nonatomic, strong) UIImageView *mySignatureImage;
@property (nonatomic, assign) CGPoint lastContactPoint1, lastContactPoint2, currentPoint;
@property (nonatomic, assign) CGRect imageFrame;
@property (nonatomic, assign) BOOL fingerMoved;
@property (weak, nonatomic) IBOutlet UILabel *lblInpectorName;
@property(weak,nonatomic)NSString *strType;
@property(weak,nonatomic)UIImage *imageToEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)action_SelectColor:(id)sender;
- (IBAction)action_SelectWidth:(id)sender;

@end
