//
//  ServiceDocumentsViewController.m
//  DPS
//
//  Created by Saavan Patidar on 20/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ServiceDocumentsViewControlleriPad.h"
#import "AllImportsViewController.h"
#import "ServiceDocumentTableViewCell.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "ServiceDocuments+CoreDataClass.h"
#import "ServiceDocuments+CoreDataProperties.h"
#import <SafariServices/SafariServices.h>
#import "MBProgressHUD.h"
#import "ServiceDocumentsViewControlleriPad.h"

@interface ServiceDocumentsViewControlleriPad ()<UITableViewDataSource,UITableViewDelegate>
{
    Global *global;
    NSString *strEmpID,*strUserName,*strCompanyKey,*strServiceUrlMain;
}
@end

@implementation ServiceDocumentsViewControlleriPad

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];

    _tblViewDocs.rowHeight=UITableViewAutomaticDimension;
    _tblViewDocs.estimatedRowHeight=200;
    _tblViewDocs.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    [self fetchDocuments];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [global getUnsignedDocumentsWoNos:strServiceUrlMain :strCompanyKey];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//============================================================================
#pragma mark- Button Action METHODS
//============================================================================

- (IBAction)action_Back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)action_Refresh:(id)sender {
    
    [self fetchDocuments];
    
}
//============================================================================
#pragma mark- Fetching Documents METHODS
//============================================================================

-(void)fetchDocuments{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [self FetchFromCoreDataToShowServiceDocs];
        
    }else{
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMain,UrlServiceDocuments,strCompanyKey,UrlServiceDocumentsAccountNo,_strAccountNo];
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Documents..."];
        
        //============================================================================
        //============================================================================
        
        NSString *strType=@"getDocumentService";
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         //  arrOfTaskList=response;
                         if (response.count==0) {
                             
                             UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Information..!!" message:@"No Data Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             [alert show];
                             [_tblViewDocs setHidden:YES];
                             
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                         } else {
                             [_tblViewDocs setHidden:NO];
                             [self coreDataSaveServiceDocs:response];
                         }
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
    }
}
//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================
-(void)coreDataSaveServiceDocs :(NSDictionary*)dictDataa{
    
    NSArray *arrDocs=[dictDataa valueForKey:@"CustomerDocuments"];
    if (arrDocs.count==0) {
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Information..!!" message:@"No Data Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];

        [self dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        
    //==================================================================================
    //==================================================================================
    [self deleteFromCoreDataServiceDocs];
    //==================================================================================
    //==================================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    
    
    for (int k=0; k<arrDocs.count; k++) {
        
        NSDictionary *dictDocss=arrDocs[k];
        
        entityServiceDocs=[NSEntityDescription entityForName:@"ServiceDocuments" inManagedObjectContext:context];
        ServiceDocuments *objServiceDocs = [[ServiceDocuments alloc]initWithEntity:entityServiceDocs insertIntoManagedObjectContext:context];
        objServiceDocs.userName=strUserName;
        objServiceDocs.companyKey=strCompanyKey;
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'"];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"ScheduleStartDate"]]];
        objServiceDocs.scheduleStartDate=newTime;
        objServiceDocs.leadDocumentId=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"LeadDocumentId"]];
        objServiceDocs.leadId=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"LeadId"]];
        objServiceDocs.accountNo=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"AccountNo"]];
        objServiceDocs.docType=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"DocType"]];
        objServiceDocs.title=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"Title"]];
        objServiceDocs.fileName=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"FileName"]];
        objServiceDocs.descriptions=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"Descriptions"]];
        objServiceDocs.isAgreementSigned=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"IsAgreementSigned"]];
        objServiceDocs.signatureLinkforAgree=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"SignatureLinkforAgree"]];
        objServiceDocs.leadNumber=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"LeadNumber"]];

        NSError *error1;
        [context save:&error1];
        
    }
    
    //==================================================================================
    //==================================================================================
    [self FetchFromCoreDataToShowServiceDocs];
    //==================================================================================
    //==================================================================================
    }
}
-(void)deleteFromCoreDataServiceDocs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityServiceDocs=[NSEntityDescription entityForName:@"ServiceDocuments" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceDocuments" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ AND userName = %@",_strAccountNo,strUserName];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)FetchFromCoreDataToShowServiceDocs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityServiceDocs=[NSEntityDescription entityForName:@"ServiceDocuments" inManagedObjectContext:context];
    requestServiceDocs = [[NSFetchRequest alloc] init];
    [requestServiceDocs setEntity:entityServiceDocs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ AND userName = %@",_strAccountNo,strUserName];
     [requestServiceDocs setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"scheduleStartDate" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestServiceDocs setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceDocs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceDocs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceDocs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceDocs performFetch:&error];
    arrAllObjServiceDocs = [self.fetchedResultsControllerServiceDocs fetchedObjects];
    if ([arrAllObjServiceDocs count] == 0)
    {
        [_tblViewDocs setHidden:YES];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Information..!!" message:@"No Data Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [_tblViewDocs setHidden:NO];
        [_tblViewDocs reloadData];
    }
    
}
//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    NSArray *sections = [self.fetchedResultsController sections];
    //    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    //    // totalCount=[sectionInfo numberOfObjects];
    //    return [sectionInfo numberOfObjects];
    return arrAllObjServiceDocs.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ServiceDocumentTableViewCell *cell = (ServiceDocumentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"serviceDoc" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
- (void)configureCell:(ServiceDocumentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    NSManagedObject *dictDocs=[arrAllObjServiceDocs objectAtIndex:indexPath.row];
    NSString *strDoctType=[dictDocs valueForKey:@"docType"];
    if ([strDoctType isEqualToString:@"Agreement"]) {
        
        NSString *strIsAgreementSigned=[dictDocs valueForKey:@"isAgreementSigned"];
        if ([strIsAgreementSigned isEqualToString:@"1"]) {
            
           cell.lbl_Title.text=[dictDocs valueForKey:@"title"];
            
        }
        else
        {
            
            if ([[NSString stringWithFormat:@"%@",[dictDocs valueForKey:@"title"]]isEqualToString:@"Unsigned Agreement"])
            {
                cell.lbl_Title.text=[dictDocs valueForKey:@"title"];
                
            }
            else
            {
                cell.lbl_Title.text=@"Get Customer Signature on Agreement";
                cell.lbl_Title.textColor=[UIColor blueColor];
            }
        }
    }
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:[dictDocs valueForKey:@"scheduleStartDate"]];

    cell.lbl_Description.text=finalTime;//[dictDocs valueForKey:@"scheduleStartDate"];
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Processing Please wait..." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [alert show];
    [global autoDismissAlerView:alert];
    
    NSManagedObject *dictDocs=[arrAllObjServiceDocs objectAtIndex:indexPath.row];
    
    NSString *strDoctType=[dictDocs valueForKey:@"docType"];
    if ([strDoctType isEqualToString:@"Agreement"]) {
        
        NSString *strIsAgreementSigned=[dictDocs valueForKey:@"isAgreementSigned"];
        if ([strIsAgreementSigned isEqualToString:@"1"]) {
            
            [self oPenPdfView:[NSString stringWithFormat:@"%@",[dictDocs valueForKey:@"fileName"]]];
            
        }
        else
        {
            if ([[NSString stringWithFormat:@"%@",[dictDocs valueForKey:@"title"]]isEqualToString:@"Unsigned Agreement"])
            {
                [self oPenPdfView:[NSString stringWithFormat:@"%@",[dictDocs valueForKey:@"fileName"]]];
                
            }
            else
            {
                [self oPenLinkOnWeb:[NSString stringWithFormat:@"%@",[dictDocs valueForKey:@"signatureLinkforAgree"]]];
            }
            
        }
    }
    else
    {
        
        
    }
    
}

-(void)oPenLinkOnWeb :(NSString *)strUrl{
    
    NSURL *URL = [NSURL URLWithString:strUrl];
    if (URL)
    {
      /*  if ([SFSafariViewController class] != nil)
        {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:strUrl]) {
            }
        }*/
        
        if (![[UIApplication sharedApplication] openURL:URL]) {
        }
        
    } else {
        // will have a nice alert displaying soon.
    }
}
-(void)oPenPdfView :(NSString *)strUrl{
    
    NSString *urlString = [NSString stringWithFormat:@"%@",strUrl];
    
    NSString *strNewString=[urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
//    NSURL *url = [NSURL URLWithString:strNewString];
//
//    NSData *data = [NSData dataWithContentsOfURL:url];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSArray* foo = [strNewString componentsSeparatedByString: @"/"];
    NSString* lastName = [foo objectAtIndex: foo.count-1];
    
    NSString *removed = [lastName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *mainPath = [documentsDirectory stringByAppendingPathComponent: removed];
  
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:mainPath];
    
    if (exists) {
        
        [self oPenPDFfile :mainPath];
        
    } else {
        
        //[self HudView];
        strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:strNewString];
      
        NSData *data = [NSData dataWithContentsOfURL:url];
      
        [data writeToFile:mainPath atomically:YES];

        [self oPenPDFfile :mainPath];
    }
    
    //[data writeToFile:mainPath atomically:YES];

    
    
}
-(void)oPenPDFfile :(NSString*)strPath{
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
    frameFor_view_CustomerInfo.origin.x=0;
    frameFor_view_CustomerInfo.origin.y=20;
    
    [_pdfView setFrame:frameFor_view_CustomerInfo];
    
    [self.view addSubview:_pdfView];

    NSURL *targetURL = [NSURL fileURLWithPath:strPath];
    NSURLRequest *request_pdf = [NSURLRequest requestWithURL:targetURL];
    [_webViewww loadRequest:request_pdf];

}
- (IBAction)action_BackPdf:(id)sender {
    
    [_pdfView removeFromSuperview];
    
}
-(void)HudView{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"Downloading PDF Please Wait...";
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        if([UIScreen mainScreen].bounds.size.height==1024)
        {
            hud.label.font=[UIFont systemFontOfSize:28];
            hud.margin = 10.f;//10.f
            hud.yOffset = 350.f;//150.f
        }
        else if ([UIScreen mainScreen].bounds.size.height==1366)
        {
            hud.label.font=[UIFont systemFontOfSize:32];
            hud.margin = 10.f;//10.f
            hud.yOffset = 500.f;//150.f
        }
        
        
        
    }
    else
    {
        if([UIScreen mainScreen].bounds.size.height==736)
        {
            hud.margin = 10.f;
            hud.yOffset = 250.f;
        }
        else if ([UIScreen mainScreen].bounds.size.height==667)
        {
            hud.margin = 10.f;
            hud.yOffset = 230.f;
        }
        else if ([UIScreen mainScreen].bounds.size.height==568)
        {
            hud.margin = 10.f;
            hud.yOffset = 200.f;
        }
        else
        {
            hud.margin = 10.f;
            hud.yOffset = 150.f;
        }
        
    }
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:1];
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //[_loaderWebView startAnimating];
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Searching..."];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    // [_loaderWebView stopAnimating];
    [DejalActivityView removeView];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"The File you are looking for does not exist.Please try again later."
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [_pdfView removeFromSuperview];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    [DejalActivityView removeView];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
@end
