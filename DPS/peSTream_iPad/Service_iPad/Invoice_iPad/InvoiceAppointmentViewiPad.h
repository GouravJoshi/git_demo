//
//  InvoiceAppointmentView.h
//  DPS
//
//  Created by Rakesh Jain on 28/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "TermiteInspectionFloridaViewControlleriPad.h"
@interface InvoiceAppointmentViewiPad : UIViewController<NSFetchedResultsControllerDelegate,AVAudioPlayerDelegate,UIActionSheetDelegate,UITextFieldDelegate>
{
    AVAudioPlayer *LandingSong;
    NSManagedObject *matchesImageDetail;
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSEntityDescription *entityWorkOderDetailServiceAuto,*entityModifyDateServiceAuto;
    NSFetchRequest *requestNewService;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
    NSEntityDescription *entityImageDetail,*entityImageDetailsTermite;
    NSEntityDescription *entityPaymentInfoServiceAuto;
    
    NSEntityDescription *entitySalesDynamic;
    NSFetchRequest *requestNew;
    
    
    NSFetchRequest *requestImageDetail;
    NSSortDescriptor *sortDescriptorImageDetail;
    NSArray *sortDescriptorsImageDetail;
    NSArray *arrAllObjImageDetail;
    
    
    NSEntityDescription *entityWoEquipment;
    NSManagedObjectContext *contextWoEquipment;
    NSFetchRequest *requestWoEquipment;
    NSSortDescriptor *sortDescriptorWoEquipment;
    NSArray *sortDescriptorsWoEquipment;
    NSManagedObject *matchesWoEquipment;
    NSArray *arrAllObjWoEquipment;
    
    
    //For Termite Changes
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    NSEntityDescription *entityTermiteTexas;
    
    
    
    
    //Florida
    NSEntityDescription *entityTermiteFlorida;
    NSFetchRequest *requestTermiteFlorida;
    NSSortDescriptor *sortDescriptorTermiteFlorida;
    NSArray *sortDescriptorsTermiteFlorida;
    NSManagedObject *matchesTermiteFlorida;
    //NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjTermiteFlorida;
    
    
}
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVieww;
@property (strong, nonatomic) IBOutlet UIView *view_CustomerInfo;
@property (strong, nonatomic) IBOutlet UITextField *txt_AccountNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_WorkorderNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_Customer;
@property (strong, nonatomic) IBOutlet UITextField *txt_PrimaryPhone;
@property (strong, nonatomic) IBOutlet UITextField *txt_SecondaryPhone;
@property (strong, nonatomic) IBOutlet UITextField *txt_PrimaryEmail;
@property (strong, nonatomic) IBOutlet UITextField *txt_Cell;
@property (strong, nonatomic) IBOutlet UITextField *txt_SecondaryEmail;
@property (strong, nonatomic) IBOutlet UITextField *txt_Technician;
@property (strong, nonatomic) IBOutlet UITextView *txtView_BillingAddress;
@property (strong, nonatomic) IBOutlet UITextView *txt_ServiceAddress;
- (IBAction)action_FinalSaveContinue:(id)sender;
- (IBAction)action_TechSign:(id)sender;
- (IBAction)action_CustomerSign:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_CustomerSign;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_TechSign;
@property (strong, nonatomic) IBOutlet UIButton *btnCustNotPrsent;
- (IBAction)action_CustNotPrsent:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtView_OfficeNotes;
@property (strong, nonatomic) IBOutlet UITextView *txtView_TechComment;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeOut;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeinn;
@property (strong, nonatomic) IBOutlet UILabel *lblChargesCurrentService;
@property (strong, nonatomic) IBOutlet UILabel *lblTax;
@property (strong, nonatomic) IBOutlet UILabel *lblTotaldue;
@property (strong, nonatomic) IBOutlet UILabel *lblPaymentType;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPaid;
@property (strong, nonatomic) IBOutlet UIView *viewPaymentInfo;
@property (strong, nonatomic) NSDictionary *dictJsonDynamicForm;
@property (strong, nonatomic) NSArray *arrChemicalList;
@property (strong, nonatomic) NSDictionary *dictOfChemicalList;
@property (strong, nonatomic) NSArray *arrOfChemicalList;
@property (strong, nonatomic) NSArray *arrOfChemicalListOther;
@property (strong, nonatomic) NSMutableArray *arrOfEquipmentLists;
- (IBAction)action_RecordAudio:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnPlayAudio;
- (IBAction)action_Play:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAudioStatus;
- (IBAction)action_AfterImage:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ViewCustSign_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ViewCust_Top;
- (IBAction)action_Refresh:(id)sender;
@property (strong, nonatomic) NSManagedObject *workOrderDetail;
@property (strong, nonatomic) NSManagedObject *paymentInfoDetail;
@property (strong, nonatomic) NSArray *arrAllObjImageDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceAutomation;
- (IBAction)action_ServiceDocumentss:(id)sender;
@property (strong, nonatomic) NSString *strPaymentModes;
@property (strong, nonatomic) NSString *strDepartmentIdd;
@property (strong, nonatomic) NSString *strPaidAmount;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AccountNos;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) IBOutlet UIButton *btn_RecordAudio;
@property (strong, nonatomic) IBOutlet UIButton *btn_AfterImage;
@property (strong, nonatomic) IBOutlet UIButton *btn_TechSignn;
@property (strong, nonatomic) IBOutlet UIButton *btn_CustSignn;
@property (strong, nonatomic) IBOutlet UIButton *btn_SavenContinue;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_SavenContinue_B;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CustomerSign;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg1;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg2;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg3;
- (IBAction)action_ButtonImg1:(id)sender;
- (IBAction)action_ButtonImg2:(id)sender;
- (IBAction)action_ButtonImg3:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_AfterImage_B;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_AfterImage_BtoImgView;
//Nilind ADD PAYMENT TYPE 22 Feb
@property (strong, nonatomic) IBOutlet UIView *paymentTypeView;
- (IBAction)action_Cash:(id)sender;
- (IBAction)action_Check:(id)sender;
- (IBAction)action_CreditCard:(id)sender;
- (IBAction)action_Bill:(id)sender;
- (IBAction)action_AutoChargerCust:(id)sender;
- (IBAction)action_PaymentPending:(id)sender;
- (IBAction)action_NoCharge:(id)sender;



@property (strong, nonatomic) IBOutlet UIButton *btnCash;
@property (strong, nonatomic) IBOutlet UIButton *btnCheck;
@property (strong, nonatomic) IBOutlet UIButton *btnCreditCard;
@property (strong, nonatomic) IBOutlet UIButton *btnBill;
@property (strong, nonatomic) IBOutlet UIButton *btnAutoCharger;
@property (strong, nonatomic) IBOutlet UIButton *btnPaymentPending;
@property (strong, nonatomic) IBOutlet UIButton *btnNoCharge;
@property (strong, nonatomic) IBOutlet UILabel *lblLinePaymentInfo;
@property (strong, nonatomic) IBOutlet UIView *lastViewAfterImage;

//CheckView
@property (strong, nonatomic) IBOutlet UIView *checkVieww;
@property (strong, nonatomic) IBOutlet UITextField *txtAmount;
@property (strong, nonatomic) IBOutlet UITextField *txtCheckNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_DrivingLicenseNo;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckExpDate;

- (IBAction)action_CheckExpDate:(id)sender;
- (IBAction)action_CheckFrontImage:(id)sender;
- (IBAction)action_CheckBackImage:(id)sender;

//AmountView
@property (strong, nonatomic) IBOutlet UITextField *txtAmountSingle;
@property (strong, nonatomic) IBOutlet UIView *amountViewSingle;


//End
@property (strong, nonatomic) IBOutlet UIView *view_EquipmentList;
@property (strong, nonatomic) IBOutlet UITableView *tblview_EquipmentList;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic;

//
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenCash;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenCheck;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenCreditCard;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenBill;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenAutoChargeCustomer;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenPaymentPending;
@property (strong, nonatomic) IBOutlet UIButton *btnHiddenNoCharge;

@property (strong, nonatomic) IBOutlet UIButton *btnServiceJobDescriptionTitle;
- (IBAction)action_ServiceJobDescriptionTitle:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textView_ServiceJobDescriptions;
@property (strong, nonatomic) IBOutlet UIView *viewServiceJobDescriptions;
@property (weak, nonatomic) IBOutlet UICollectionView *imageCollectionView;
@property (strong, nonatomic) IBOutlet UIView *viewTermsnConditions;
@property (strong, nonatomic) IBOutlet UITextView *txtViewTermsnConditions;
@property (strong, nonatomic) IBOutlet UIButton *btnHidden;
@property (strong, nonatomic) IBOutlet UIView *view_SavenContinue;
@property (strong, nonatomic) IBOutlet UIView *viewCustomerSignature;
@property (strong, nonatomic) IBOutlet UIView *view_TechSignature;
@property (strong, nonatomic) IBOutlet UILabel *lblCustomerNotPresent;

//Graph
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewGraph;
@property (strong, nonatomic) IBOutlet UIView *viewGraphImage;
@property (strong, nonatomic) IBOutlet UIView *viewFooter;

- (IBAction)action_GraphImage:(id)sender;
- (IBAction)action_AddGraph:(id)sender;
- (IBAction)action_CloseGraphView:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;
@property (strong, nonatomic) IBOutlet UIButton *btnAddGraphhh;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWoEquipmentService;


//For Termite Changes

@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UIView *view3;
@property (strong, nonatomic) IBOutlet UIView *view4;
@property (strong, nonatomic) IBOutlet UIView *view5;
@property (strong, nonatomic) IBOutlet UIView *view5Sub;
@property (strong, nonatomic) IBOutlet UIView *view6;
@property (strong, nonatomic) IBOutlet UIView *view7;
@property (strong, nonatomic) IBOutlet UIView *view8;
@property (strong, nonatomic) IBOutlet UIView *view9;
@property (strong, nonatomic) IBOutlet UIView *view10;
@property (strong, nonatomic) IBOutlet UIView *view11;
@property (strong, nonatomic) IBOutlet UIView *view12;
@property (strong, nonatomic) IBOutlet UIView *view13;

//View1
@property (strong, nonatomic) IBOutlet UITextField *txt_InspectedAddress;
@property (strong, nonatomic) IBOutlet UITextField *txt_City;
@property (strong, nonatomic) IBOutlet UITextField *txt_ZipCode;

//View2
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxA;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxB;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxC;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxD;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxE;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxF;

//View3
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxG;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxH;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxI;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBoxJ;


//View4
@property (strong, nonatomic) IBOutlet UITextField *txt_1A;
@property (strong, nonatomic) IBOutlet UITextField *txt_1B;
@property (strong, nonatomic) IBOutlet UITextField *txt_1C;
@property (strong, nonatomic) IBOutlet UITextField *txt_ZipCodeView4;
@property (strong, nonatomic) IBOutlet UITextField *txt_CityView4;
@property (strong, nonatomic) IBOutlet UITextField *txt_State;
@property (strong, nonatomic) IBOutlet UITextField *txt_TelephoneNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_1D;
@property (strong, nonatomic) IBOutlet UITextField *txt_2;
@property (strong, nonatomic) IBOutlet UITextField *txt_3;
@property (strong, nonatomic) IBOutlet UITextField *txt_4B;
@property (strong, nonatomic) IBOutlet UITextField *txt_PersonPurchasingInspection;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_1E;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_1E;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_4A;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_4A;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_3_4A;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_4_4A;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_5_4A;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_4C;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_4C;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_4C;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_4C;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_5_4C;
@property (strong, nonatomic) IBOutlet UIButton *btnInspectionDate;
@property (strong, nonatomic) IBOutlet UITextField *txt_4A_Other;


//View5

@property (strong, nonatomic) IBOutlet UITextField *txt_5;
@property (strong, nonatomic) IBOutlet UITextField *txt_6BSpecify;
@property (strong, nonatomic) IBOutlet UITextField *txt_7BSpecify;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_6A;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_6A;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_6B;

@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_5_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_6_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_7_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_8_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_9_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_10_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_11_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_12_6B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_13_6B;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_7A;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_7A;


@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_5_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_6_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_7_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_8_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_9_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_10_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_11_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_12_7B;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_13_7B;


//View 6
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8_PreviousTreatment;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8_PreviousTreatment;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8A_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8A_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8A_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8A_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8A_PreviousTreatment;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8A_PreviousTreatment;


@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8B_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8B_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8B_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8B_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8B_PreviousTreatment;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8B_PreviousTreatment;


//View 7
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8C_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8C_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8C_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8C_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8C_PreviousTreatment;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8C_PreviousTreatment;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8D_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8D_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8D_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8D_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8D_PreviousTreatment;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8D_PreviousTreatment;


@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8E_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8E_ActiveInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8E_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8E_PreviousInfestation;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_8E_PreviousTreatment;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_8E_PreviousTreatment;
@property (strong, nonatomic) IBOutlet UITextField *txt_8E;



//View8
@property (strong, nonatomic) IBOutlet UITextField *txt_8F;
@property (strong, nonatomic) IBOutlet UITextField *txt_8G;
@property (strong, nonatomic) IBOutlet UITextField *txt_8GD;
@property (strong, nonatomic) IBOutlet UITextField *txt_9;
@property (strong, nonatomic) IBOutlet UITextField *txt_9BSpecifyReason;
@property (strong, nonatomic) IBOutlet UITextField *txt_9B;
@property (strong, nonatomic) IBOutlet UITextField *txt_10A;

@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_9;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_9;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_9A;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_9A;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_9B;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_9B;

//View9

@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_10A_Subterranean;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_10A_Subterranean;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_10A_Subterranean;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_10A_Subterranean;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_10A_Drywood;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_10A_Drywood;
@property (strong, nonatomic) IBOutlet UIButton *btn_DateTreatmentByInspectionCompany;


@property (strong, nonatomic) IBOutlet UITextField *txt_DateOfTreatment;
@property (strong, nonatomic) IBOutlet UITextField *txt_CommonNameOfInsect;
@property (strong, nonatomic) IBOutlet UITextField *txt_NameOfPesticide;
@property (strong, nonatomic) IBOutlet UITextField *txt_ListInsects;


@property (strong, nonatomic) IBOutlet UIButton *btnRadio_1_10A_Contract;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio_2_10A_Contract;


//Vie10
@property (strong, nonatomic) IBOutlet UIButton *btnBuyerIntials;
@property (strong, nonatomic) IBOutlet UIButton *btnAddGraph;
@property (strong, nonatomic) IBOutlet UIButton *btnInspectorSignature;
@property (strong, nonatomic) IBOutlet UITextField *txt_AdditionalComments;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBuyerIntials;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAddGraph;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewInspectorSign;
@property (strong, nonatomic) IBOutlet UIButton *btnAddGraphImg;
@property (strong, nonatomic) IBOutlet UITextField *txt_Specifys_TexasOffiicial_WoodDestroyingInsect;


//View11
@property (strong, nonatomic) IBOutlet UIButton *btnCertifiedApplicatorSign;
@property (strong, nonatomic) IBOutlet UIButton *btnBuyerIntialsPurchaser;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCertifiedApplicatorSign;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBuyerIntialsPurchaser;
@property (strong, nonatomic) IBOutlet UITextField *txt_12B;
@property (strong, nonatomic) IBOutlet UITextField *txt_ApplicatorLicenseNumber;

@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_1_12A;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_2_12A;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_3_12A;
@property (strong, nonatomic) IBOutlet UIButton *btnChkBox_4_12A;
@property (strong, nonatomic) IBOutlet UIButton *btn_DatePosted;


//View12

@property (strong, nonatomic) IBOutlet UIButton *btnFinalSavenContinue;

@property (strong, nonatomic) IBOutlet UIButton *btnBuyersInitial_7B;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_BuyerInitial_7B;
@property (strong, nonatomic) IBOutlet UIButton *btn_DateOfPurchaserOfProperty;
//View for Collectionview 7A
@property (strong, nonatomic) IBOutlet UIView *viewForcollectionView7A;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView7A;
- (IBAction)actionOnAddImageForCollectionView7A:(id)sender;

//View For ForcollectionView8
@property (strong, nonatomic) IBOutlet UIView *viewForcollectionView8;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView8;
- (IBAction)actionOnAddImageForCollectionView8:(id)sender;
- (IBAction)actionOnNotesHistory:(id)sender;

#pragma mark- FLORIDA
@property (strong, nonatomic) IBOutlet UIView *view1Florida;
@property (strong, nonatomic) IBOutlet UIView *view2Florida;
@property (strong, nonatomic) IBOutlet UIView *view3Florida;
@property (strong, nonatomic) IBOutlet UIView *view4Florida;
@property (strong, nonatomic) IBOutlet UIView *view5Florida;


//View1
@property (weak, nonatomic) IBOutlet UITextField *txtInspectionCompany;
@property (weak, nonatomic) IBOutlet UITextField *txtBusinessLicenseNo;
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNo;

@property (weak, nonatomic) IBOutlet UITextField *txtComapnyCity;
@property (weak, nonatomic) IBOutlet UITextField *txtComapnyState;
@property (weak, nonatomic) IBOutlet UITextField *txtComapnyZipcode;

@property (weak, nonatomic) IBOutlet UITextField *txtDateOfInspection;
@property (weak, nonatomic) IBOutlet UIButton *btnDateOfInspection;
//- (IBAction)actionOnBtnDateOfInspection:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt1InspectorNameAndIdentificationNo;
@property (weak, nonatomic) IBOutlet UITextField *txt2InspectorNameAndIdentificationNo;
@property (weak, nonatomic) IBOutlet UITextField *txtAddressOfPropoertyInspected;
@property (weak, nonatomic) IBOutlet UITextField *txtStructurePropertyInspected;
@property (weak, nonatomic) IBOutlet UITextField *txtInspectionAndReportRequestedBy;
@property (weak, nonatomic) IBOutlet UITextField *txtInspectionAndReportRequestedTo;

//View2
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxA;
//- (IBAction)actionOnBtnCheckBoxA:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxB;
//- (IBAction)actionOnBtnCheckBoxB:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxLiveWDO;
//- (IBAction)actionOnBtnChkBoxLiveWDO:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtLiveWDO;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxEvidenceOfWDO;
//- (IBAction)actionOnbtnCheckBoxEvidenceOfWDO:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewEvidenceOfWDO;
@property (weak, nonatomic) IBOutlet UIButton *btnChkBoxDAMAGEcausedbyWDO;
//- (IBAction)actionOnbtnChkBoxDAMAGEcausedbyWDO:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDAMAGEcausedbyWDO;

//View3

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxAttic;
//- (IBAction)actionOnBtnCheckBoxAttic:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAtticSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAtticReason;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxInterior;
//- (IBAction)actionOnBtnChkBoxInterior:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldInteriorSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txtViewInteriorReason;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxExterior;
//- (IBAction)actionOnCheckBoxExterior:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldExteriorSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txtViewExteriorReason;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxCrawlPlace;
//- (IBAction)actionOnBtnChkBoxCrawlPlace:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldCrawlPlaceSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txViewCrawlPlaceReason;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBoxOther;
//- (IBAction)actionOnBtnCheckBoxOther:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldOtherSpecificArea;
@property (weak, nonatomic) IBOutlet UITextView *txtViewOtherReason;

//View4
@property (weak, nonatomic) IBOutlet UIButton *btnRadioEvidencForYes;
//- (IBAction)actionOnBtnRadioEvidenceYes:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnRadioEvidenceForNo;
//- (IBAction)actionOnBtnRadioEvidenceNO:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtFieldForEvidence;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForNoticeOfInspection;

@property (weak, nonatomic) IBOutlet UIButton *btnRadioCompanyStructureForYes;
//- (IBAction)actionOnBtnRadioCompanyStructureYes:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRadioCompnayStructureForNo;
//- (IBAction)actionOnBtnRadioCompanyStructureNo:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForCommonNameOfOrganism;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForNameOfPesticideUsed;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForTermsAndCondition;
@property (weak, nonatomic) IBOutlet UIButton *btnChkBoxWholeStructure;
//- (IBAction)actionOnBtnChckBoxWholeStructure:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnChkBoxSpotTreatment;
//- (IBAction)actionOnBtnChkBoxSpotTreatment:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForMethodOfTreatment;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForSpecifyTreatmentNotice;

//View5
@property (weak, nonatomic) IBOutlet UITextView *txtViewForComment;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSignatureofLicenseAgent;
@property (weak, nonatomic) IBOutlet UIButton *btnSignatureLicenseAgent;
//- (IBAction)actionOnbtnSignatureLicenseAgent:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldDateForView5;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAddressOfPropertyInspectedView5;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForInspectionDateForView5;
@property (strong, nonatomic) NSString *strTermiteType;

@property (weak, nonatomic) IBOutlet UIButton *btnDateOfLicenceeAgent;
//- (IBAction)actionOnBtnDateOfLicenceeAgent:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnInspectionDateFloridaView5;

//- (IBAction)actionOnBtnInspectionDate:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;

@property (weak, nonatomic) IBOutlet UIButton *btnCashNew;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckNew;
@property (weak, nonatomic) IBOutlet UIButton *btnCreditCardNew;
@property (weak, nonatomic) IBOutlet UIButton *btnBillNew;
@property (weak, nonatomic) IBOutlet UIButton *btnAutoChargeCustomerNew;
@property (weak, nonatomic) IBOutlet UIButton *btnPaymentPendingNew;
@property (weak, nonatomic) IBOutlet UIButton *btnNoChargeNew;
@end
