//
//  SignViewController.m
//  DemoTable
//
//  Created by Rakesh Jain on 08/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ServiceSignViewControlleriPad.h"
#import "Global.h"

@interface ServiceSignViewControlleriPad ()
{
    BOOL isSignImage;
    UIView *viewForSign;
    NSString *strSignImageName;
    NSData *signImageData;
    BOOL image;
    Global *global;
}
@end

@implementation ServiceSignViewControlleriPad
//Nilind For Signature 8 Sep
@synthesize mySignatureImage;
@synthesize lastContactPoint1, lastContactPoint2, currentPoint;
@synthesize imageFrame;
@synthesize fingerMoved;
//..........................
- (void)viewDidLoad {
    [super viewDidLoad];
    
    global=[[Global alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strType;
    strType=[defs valueForKey:@"signatureType"];
    _strType=strType;
    if ([strType isEqualToString:@"fromTechService"])
    {
        if([NSString stringWithFormat:@"%@",[defs valueForKey:@"inspectorNameService"]].length==0)
        {
             _lblInpectorName.text=@"N/A";
        }
        else
        {
            _lblInpectorName.text=[defs valueForKey:@"inspectorNameService"];
        }
    }
    else if ([strType isEqualToString:@"fromCustService"])
    {
        if([NSString stringWithFormat:@"%@",[defs valueForKey:@"customerNameService"]].length==0)
        {
             _lblInpectorName.text=@"N/A";
        }
        else
        {
            _lblInpectorName.text=[defs valueForKey:@"customerNameService"];
        }
    }
    
    // else  commented by akshay on 29 jUne 2018
        
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"isFromQuote"] isEqualToString:@"yes"])
    {
        if([NSString stringWithFormat:@"%@",[defs valueForKey:@"inspectorNameService"]].length==0)
        {
            _lblInpectorName.text=@"N/A";
        }
        else
        {
            _lblInpectorName.text=[defs valueForKey:@"inspectorNameService"];
        }
    }
    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"isFromElectronicAuthorizedForm"] isEqualToString:@"yes"])
    {
        if([NSString stringWithFormat:@"%@",[defs valueForKey:@"nameTitle"]].length==0)
        {
            _lblInpectorName.text=@"N/A";
        }
        else
        {
            _lblInpectorName.text=[defs valueForKey:@"nameTitle"];
        }
    }
    
    viewForSign=[[UIView alloc]init];
    viewForSign.frame=CGRectMake(_lblInpectorName.frame.origin.x, 290, [UIScreen mainScreen].bounds.size.width-(_lblInpectorName.frame.origin.x)*2, 600);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        viewForSign.frame=CGRectMake(_lblInpectorName.frame.origin.x, 185, [UIScreen mainScreen].bounds.size.width-(_lblInpectorName.frame.origin.x)*2, 200);
    }
    // NILIND SIGN
    
    //....................
#pragma mark- TEMP nilind 8 septs
    
    isSignImage=false;
    mySignatureImage.image = nil;
    
    

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    CGFloat screenWidth = screenRect.size.width;
    NSLog(@"Screen Height is = = %f, Screen Width Is == %f",screenHeight,screenWidth);
    
    
    mySignatureImage.image = nil;
    imageFrame = CGRectMake(0,0,viewForSign.bounds.size.width,viewForSign.bounds.size.height);
    [viewForSign.layer setCornerRadius:5.0f];
    [viewForSign.layer setBorderColor:[UIColor blackColor].CGColor];
    [viewForSign.layer setBorderWidth:0.8f];
    [viewForSign.layer setShadowColor:[UIColor blackColor].CGColor];
    [viewForSign.layer setShadowOpacity:0.3];
    [viewForSign.layer setShadowRadius:3.0];
    [viewForSign.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    //allocate an image view and add to the main view
    mySignatureImage = [[UIImageView alloc] initWithImage:nil];
    mySignatureImage.frame = imageFrame;
    mySignatureImage.backgroundColor = [UIColor whiteColor];
    [viewForSign addSubview:mySignatureImage];
    [self.view addSubview:viewForSign];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    mySignatureImage.image = nil;
    //......................................


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- SIGNATURE CODE 8 SEPT
//when one or more fingers touch down in a view or window
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //did our finger moved yet?
    fingerMoved = NO;
    UITouch *touch = [touches anyObject];
    
    //    //just clear the image if the user tapped twice on the screen
    //    if ([touch tapCount] == 2) {
    //        mySignatureImage.image = nil;
    //        return;
    //    }
    
    //we need 3 points of contact to make our signature smooth using quadratic bezier curve
    currentPoint = [touch locationInView:mySignatureImage];
    lastContactPoint1 = [touch previousLocationInView:mySignatureImage];
    lastContactPoint2 = [touch previousLocationInView:mySignatureImage];
    
}


//when one or more fingers associated with an event move within a view or window
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    //well its obvious that our finger moved on the screen
    fingerMoved = YES;
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass: [_viewSignature class]]) {
        NSLog(@"finger moved on imageview");
        isSignImage=true;
        //your touch was in a uipickerview ... do whatever you have to do
    }
    //save previous contact locations
    lastContactPoint2 = lastContactPoint1;
    lastContactPoint1 = [touch previousLocationInView:mySignatureImage];
    //save current location
    currentPoint = [touch locationInView:mySignatureImage];
    
    //find mid points to be used for quadratic bezier curve
    CGPoint midPoint1 = [self midPoint:lastContactPoint1 withPoint:lastContactPoint2];
    CGPoint midPoint2 = [self midPoint:currentPoint withPoint:lastContactPoint1];
    
    //create a bitmap-based graphics context and makes it the current context
    UIGraphicsBeginImageContext(imageFrame.size);
    
    //draw the entire image in the specified rectangle frame
    [mySignatureImage.image drawInRect:CGRectMake(0, 0, imageFrame.size.width, imageFrame.size.height)];
    
    //set line cap, width, stroke color and begin path
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 3.0f);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    
    //begin a new new subpath at this point
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), midPoint1.x, midPoint1.y);
    //create quadratic Bézier curve from the current point using a control point and an end point
    CGContextAddQuadCurveToPoint(UIGraphicsGetCurrentContext(),
                                 lastContactPoint1.x, lastContactPoint1.y, midPoint2.x, midPoint2.y);
    
    //set the miter limit for the joins of connected lines in a graphics context
    CGContextSetMiterLimit(UIGraphicsGetCurrentContext(), 2.0);
    
    //paint a line along the current path
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    
    //set the image based on the contents of the current bitmap-based graphics context
    mySignatureImage.image = UIGraphicsGetImageFromCurrentImageContext();
    
    //remove the current bitmap-based graphics context from the top of the stack
    UIGraphicsEndImageContext();
    
    //lastContactPoint = currentPoint;
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // UITouch *touch = [touches anyObject];
    
    //if the finger never moved draw a point
    if(!fingerMoved) {
        // mySignatureImage.image=nil;
        NSLog(@"finger not moved...!!!");
        
    }
}

//calculate midpoint between two points
- (CGPoint) midPoint:(CGPoint )p0 withPoint: (CGPoint) p1 {
    return (CGPoint) {
        (p0.x + p1.x) / 2.0,
        (p0.y + p1.y) / 2.0
    };
}
- (IBAction)actionOnSaveSign:(id)sender
{
    if (mySignatureImage.image==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please add Sign to proceed further." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
            NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
            [formatterDate setDateFormat:@"MMddyyyy"];
            [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
            NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"HHmmss"];
            [formatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString *strTime = [formatter stringFromDate:[NSDate date]];
            strSignImageName = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Sign_%@%@.png",strDate,strTime];
            //Documents\Signature\31_Customer.png
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strSignImageName]];
            signImageData = UIImagePNGRepresentation(mySignatureImage.image);
            [signImageData writeToFile:path atomically:YES];
        
            if ([_strType isEqualToString:@"fromTechService"])
            {
                
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:strSignImageName forKey:@"imagePath"];
            [defs setValue:@"fromInspectorSignService" forKey:@"fromInspectorSignService"];
            [defs synchronize];

            }
            else if ([_strType isEqualToString:@"fromCustService"])
            {
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setValue:strSignImageName forKey:@"imagePath"];
                [defs setValue:@"fromCustomerSignService" forKey:@"fromCustomerSignService"];
                [defs synchronize];
                
            }
        
            //else  commented by akshay on 29 jUne 2018
                
            if([[[NSUserDefaults standardUserDefaults] valueForKey:@"isFromQuote"] isEqualToString:@"yes"])
            {
                strSignImageName = [NSString stringWithFormat:@"\\Documents\\QuoteSignature\\Sign_%@%@.png",strDate,strTime];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strSignImageName]];
                signImageData = UIImagePNGRepresentation(mySignatureImage.image);
                [signImageData writeToFile:path atomically:YES];
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setValue:strSignImageName forKey:@"imagePathQuote"];
                [defs synchronize];
            }
        
            else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"isFromElectronicAuthorizedForm"] isEqualToString:@"yes"])
            {
                strSignImageName = [NSString stringWithFormat:@"\\Signatures\\Sign_%@%@.png",strDate,strTime];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strSignImageName]];
                signImageData = UIImagePNGRepresentation(mySignatureImage.image);
                [signImageData writeToFile:path atomically:YES];
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setValue:strSignImageName forKey:@"imagePathElectronic"];
                [defs synchronize];
                
            }
            [self dismissViewControllerAnimated:YES completion:nil];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Sign Saved" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
            [alert show];
        
            [global autoDismissAlerView:alert];
        
        
          }

}
- (IBAction)actionOnClearSign:(id)sender
{
    mySignatureImage.image = nil;
    strSignImageName=nil;
    signImageData=nil;
    isSignImage=false;
    image=YES;

}
- (IBAction)actionOnExitSign:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
