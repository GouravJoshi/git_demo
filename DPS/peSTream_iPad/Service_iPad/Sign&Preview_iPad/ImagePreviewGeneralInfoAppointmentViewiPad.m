//
//  ImagePreviewGeneralInfoAppointmentView.m
//  DPS
//
//  Created by Rakesh Jain on 28/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ImagePreviewGeneralInfoAppointmentViewiPad.h"
#import "AllImportsViewController.h"
#import "GeneralInfoAppointmentView.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"

@interface ImagePreviewGeneralInfoAppointmentViewiPad ()
{
    Global *global;
    NSString *strImageNameToDelete;
    int indexToDelete;
    NSMutableArray *arrOfDeletedImages;
}

@end

@implementation ImagePreviewGeneralInfoAppointmentViewiPad

- (void)viewDidLoad {
    [super viewDidLoad];
    [_textViewImageCaption setEditable:NO];
    [_textViewImageDescription setEditable:NO];
    arrOfDeletedImages=[[NSMutableArray alloc]init];
    _textViewImageCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _textViewImageCaption.layer.borderWidth=1.0;
    _textViewImageCaption.layer.cornerRadius=5.0;

    
    _textViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _textViewImageDescription.layer.borderWidth=1.0;
    _textViewImageDescription.layer.cornerRadius=5.0;

    //============================================================================
    //============================================================================
    indexToDelete=-1;
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

    
    activeIndex=0;
    activeIndex=[_indexOfImage intValue];
    arrayImages = [[NSMutableArray alloc] init];
    _arrayOfImages=_arrOfImages;
    arrayImages =_arrayOfImages;
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
       // [global AlertMethod:Alert :ErrorInternetMsg];
        //[self goToGeneralInfoView];
        [self ShowFirstImage];
    }
    else
    {
       // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Downloading Image..."];
        //[self performSelector:@selector(stopdejal) withObject:nil afterDelay:0.5];
        [self downloadingImagess];
    }
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerLeft:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    UISwipeGestureRecognizer *gestureRecognizerLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizerLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizerLeft];

    BOOL isCompletedStatusMechanical=[global isCompletedSatusMechanical:_statusOfWorkOrder];

    if (isCompletedStatusMechanical) {
        
        _btnDelete.hidden=YES;
        [_btnEditDescription setEnabled:NO];
        [_btnEditCaption setEnabled:NO];
        [_btnEditImage setHidden:YES];
        
    } else {
        
        _btnDelete.hidden=NO;
        [_btnEditDescription setEnabled:YES];
        [_btnEditCaption setEnabled:YES];
        [_btnEditImage setHidden:NO];

    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
//============================================================================
#pragma mark Action Button
//============================================================================
//============================================================================

- (IBAction)action_DeleteImage:(id)sender {
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Are you sure you want to delete the image ?"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* Preview = [UIAlertAction actionWithTitle:@"Yes-Delete" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  if (indexToDelete==-1) {
                                      
                                      UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to delete Image.Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                      [alert show];
                                      
                                  } else {
                                      _indexOfImage=@"7";
                                      [self removeImage:strImageNameToDelete];
                                  }
                              }];
    [alert addAction:Preview];
    UIAlertAction* Capture = [UIAlertAction actionWithTitle:@"No-Don't Delete" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                              }];
    [alert addAction:Capture];
    [self presentViewController:alert animated:YES completion:nil];

}

- (IBAction)action_Back:(id)sender {

    [self dismissViewControllerAnimated:NO completion:nil];
    
   // [self goToGeneralInfoView];
}

-(void)goToGeneralInfoView{
//    for (UIViewController *controller in self.navigationController.viewControllers)
//    {
//        if ([controller isKindOfClass:[GeneralInfoAppointmentView class]])
//        {
//            [self.navigationController popToViewController:controller animated:NO];
//            break;
//        }
//    }

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServicePad"
                                                             bundle: nil];
    GeneralInfoAppointmentViewiPad
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"GeneralInfoAppointmentViewiPad"];
    objByProductVC.dictOfWorkOrders=_dictOfWorkOrdersImagePreview;
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

//============================================================================
//============================================================================
#pragma mark -----------------------Load Image-------------------------------
//============================================================================
//============================================================================

- (UIImage*)loadImage :(NSString*)name {
    
    indexToDelete=activeIndex;
    lblCount.text = [NSString stringWithFormat:@"%d/%lu",activeIndex+1,(unsigned long)[arrayImages count]];
    
    if ([_arrOfImageCaptionsSaved[activeIndex] isEqualToString:@"No Caption Available..!!"]) {
        _textViewImageCaption.textColor = [UIColor lightGrayColor];
    }else{
        _textViewImageCaption.textColor = [UIColor blackColor];
    }
    _textViewImageCaption.text=_arrOfImageCaptionsSaved[activeIndex];
    
    
    if ([_arrOfImageDescriptionSaved[activeIndex] isEqualToString:@"No Description Available..!!"]) {
        _textViewImageDescription.textColor = [UIColor lightGrayColor];
    }else{
        _textViewImageDescription.textColor = [UIColor blackColor];
    }
    _textViewImageDescription.text=_arrOfImageDescriptionSaved[activeIndex];

    
    if ([_indexOfImage isEqualToString:@"7"]) {
        
    } else {
        
        if ([_arrOfImageCaptionsSaved[activeIndex] isEqualToString:@"No Caption Available..!!"]) {
            _textViewImageCaption.textColor = [UIColor lightGrayColor];
        }else{
            _textViewImageCaption.textColor = [UIColor blackColor];
        }
        
        _textViewImageCaption.text=_arrOfImageCaptionsSaved[activeIndex];
        
        
        
        if ([_arrOfImageDescriptionSaved[activeIndex] isEqualToString:@"No Description Available..!!"]) {
            _textViewImageDescription.textColor = [UIColor lightGrayColor];
        }else{
            _textViewImageDescription.textColor = [UIColor blackColor];
        }
        
        _textViewImageDescription.text=_arrOfImageDescriptionSaved[activeIndex];

        
        NSString *str = [_arrayOfImages objectAtIndex:activeIndex];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }

        name=result;
        

    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    imgView.image=image;
    //Nilind
   /* UILabel *lb=[[UILabel alloc]init];
    lb.frame=CGRectMake(0, 0, 320, 30);
    lb.backgroundColor=[UIColor redColor];
    lb.textColor=[UIColor blackColor];
    lb.text=name;
    [imgView addSubview:lb];*/
   /* UIFont *font = [UIFont boldSystemFontOfSize:12];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(0, 150, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    [name drawInRect:CGRectIntegral(rect) withFont:font];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();*/
    
    
    //End

    if (image==nil) {
       // [global AlertMethod:Info :NoPreview];
    }
    strImageNameToDelete=name;
    
    
    
    
    return image;
}

//============================================================================
//============================================================================
#pragma mark -----------------------Swipe Handler-------------------------------
//============================================================================
//============================================================================

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    _indexOfImage=@"7";
    if (!(_arrOfImages.count==0)) {
    if (activeIndex<[arrayImages count]) {
        activeIndex++;
        
        if (activeIndex==[arrayImages count]) {
            activeIndex=[arrayImages count]-1;
        }
        else
        {
            indexToDelete=activeIndex;
            NSString *str = [_arrayOfImages objectAtIndex:activeIndex];
            NSString *result;
            NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }else{
                result=str;
            }
            if (result.length==0) {
                NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
                if (equalRange.location != NSNotFound) {
                    result = [str substringFromIndex:equalRange.location + equalRange.length];
                }
            }
            [self loadImage:result];
        }
    }
    }
    else{
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    //  NSLog(@"Swipe received.");
}

-(void)swipeHandlerLeft:(UISwipeGestureRecognizer *)recognizer {
    _indexOfImage=@"7";
    if (!(_arrOfImages.count==0)) {
    if (activeIndex>0) {
        activeIndex--;
        indexToDelete=activeIndex;
        NSString *str = [_arrayOfImages objectAtIndex:activeIndex];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        [self loadImage:result];
    }
    }else{
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self dismissViewControllerAnimated:NO completion:nil];

    }
    // NSLog(@"Swipe received.");
}

//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================

-(void)downloadingImagess{
    
    for (int k=0; k<_arrOfImages.count; k++) {
        
    NSString *str = [_arrOfImages objectAtIndex:k];
        
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
   // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self stopdejal];
        [self ShowFirstImage];
    } else {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
       // NSURL *url = [NSURL URLWithString:strNewString];

        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
        UIImage *image = [UIImage imageWithData:photoData];
        [self saveImageAfterDownload1:image :result];
        
        [self ShowFirstImage];

    }
    }
    
    [self stopdejal];
    
}


- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
}

-(void)ShowFirstImage{
    
    if (!(_arrOfImages.count==0)) {
    NSString *str = [_arrayOfImages objectAtIndex:activeIndex];
        
    indexToDelete=activeIndex;
        
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage:result];
    }else{
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
}
-(void)stopdejal{
    [DejalActivityView removeView];
}
-(void)startDejal{
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Downloading Image..."];
}


- (void)removeImage:(NSString *)filename
{
    if (!(_arrOfImages.count==0)) {
        
        
        
        if (activeIndex==0) {
            activeIndex=0;
        } else if (activeIndex==1){
            activeIndex=0;
        }else if (activeIndex==2){
            activeIndex=1;
        }else if (activeIndex>2){
            activeIndex=activeIndex-1;
        }
        
        [arrOfDeletedImages addObject:_arrOfImages[indexToDelete]];
        
        [_arrOfImageCaptionsSaved removeObjectAtIndex:indexToDelete];
        [_arrOfImageDescriptionSaved removeObjectAtIndex:indexToDelete];
        
        NSUserDefaults *defsss1=[NSUserDefaults standardUserDefaults];
        [defsss1 setObject:_arrOfImageCaptionsSaved forKey:@"imageCaption"];
        [defsss1 setObject:_arrOfImageDescriptionSaved forKey:@"imageDescription"];
        [defsss1 setBool:YES forKey:@"yesEditedImageDescription"];
        [defsss1 setBool:YES forKey:@"yesEditedImageCaption"];
        [defsss1 synchronize];

        [_arrOfImages removeObjectAtIndex:indexToDelete];

        arrayImages = [[NSMutableArray alloc] init];
        _arrayOfImages=_arrOfImages;
        arrayImages =_arrayOfImages;
        [self ShowFirstImage];

        NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
        [defsss setObject:arrOfDeletedImages forKey:@"DeletedImages"];
        [defsss setBool:YES forKey:@"yesDeletedImageInPreview"];
        [defsss setObject:_arrOfImageCaptionsSaved forKey:@"imageCaption"];
        [defsss setObject:_arrOfImageDescriptionSaved forKey:@"imageDescription"];
        [defsss synchronize];
        
        
        BOOL isFromCheckFrontImage=[defsss boolForKey:@"forCheckFrontImageDelete"];
        BOOL isFromCheckBackImage=[defsss boolForKey:@"forCheckBackImageDelete"];
        if (isFromCheckFrontImage) {
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:@"Front" forKey:@"forWhichCheckImage"];
            [defs synchronize];
            
        }else if (isFromCheckBackImage){
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:@"Back" forKey:@"forWhichCheckImage"];
            [defs synchronize];

        }else{
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:@"KuchNhi" forKey:@"forWhichCheckImage"];
            [defs synchronize];

        }
        
    }
}
+(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
{
    
    UIFont *font = [UIFont boldSystemFontOfSize:12];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    [text drawInRect:CGRectIntegral(rect) withFont:font];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(void)textViewDidBeginEditing:(UITextView *)textField{
    
    
}
-(void)textViewDidEndEditing:(UITextView *)textField
{
    
    
    
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }

    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
   
    if (textView.tag==7) {
        
        if ([_textViewImageCaption.textColor isEqual:[UIColor lightGrayColor]])
        {
            _textViewImageCaption.text = @"";
            _textViewImageCaption.textColor =[UIColor blackColor];
        }
    } else {
        
        if ([_textViewImageDescription.textColor isEqual:[UIColor lightGrayColor]])
        {
            _textViewImageDescription.text = @"";
            _textViewImageDescription.textColor =[UIColor blackColor];
        }
    }
    
//   else  if ([_textViewImageCaption.textColor isEqual:[UIColor lightGrayColor]])
//    {
//        _textViewImageCaption.text = @"";
//        _textViewImageCaption.textColor =[UIColor blackColor];
//    }
//   else  if ([_textViewImageDescription.textColor isEqual:[UIColor lightGrayColor]])
//   {
//       _textViewImageDescription.text = @"";
//       _textViewImageDescription.textColor =[UIColor blackColor];
//   }
    
    if (textView.tag==7) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textView.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textView.text length] + [text length] - range.length;
        return (newLength > 250) ? NO : YES;
    }

    if (textView.tag==8) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textView.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textView.text length] + [text length] - range.length;
        return (newLength > 250) ? NO : YES;
    }

    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_textViewImageCaption resignFirstResponder];
    [_textViewImageDescription resignFirstResponder];
    
}

- (IBAction)action_EditCaption:(id)sender {
    
    if ([_btnEditCaption.titleLabel.text isEqualToString:@"Edit Caption"]) {
        [_btnEditCaption setTitle:@"Save" forState:UIControlStateNormal];
        [_textViewImageCaption setEditable:YES];
        [_textViewImageCaption becomeFirstResponder];
    } else {
        
        if (_textViewImageCaption.text.length==0) {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:@"Please enter to add caption"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      [_textViewImageCaption setEditable:YES];
                                      [_textViewImageCaption becomeFirstResponder];
                                      
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
            
            
        } else {
            
            [_arrOfImageCaptionsSaved replaceObjectAtIndex:indexToDelete withObject:_textViewImageCaption.text];
            NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
            [defsss setObject:_arrOfImageCaptionsSaved forKey:@"imageCaption"];
            [defsss setBool:YES forKey:@"yesEditedImageCaption"];
            [defsss synchronize];
            [_btnEditCaption setTitle:@"Edit Caption" forState:UIControlStateNormal];
            [_textViewImageCaption setEditable:NO];
            
        }
    }
}

- (IBAction)action_EditImage:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    [defs setBool:YES forKey:@"yesEditImage"];
    [defs setValue:strImageNameToDelete forKey:@"editImagePath"];
    [defs synchronize];
    
    [self dismissViewControllerAnimated:NO completion:nil];

}

- (IBAction)action_EditDescription:(id)sender {
    
    if ([_btnEditDescription.titleLabel.text isEqualToString:@"Edit Description"]) {
        [_btnEditDescription setTitle:@"Save" forState:UIControlStateNormal];
        [_textViewImageDescription setEditable:YES];
        [_textViewImageDescription becomeFirstResponder];
    } else {
        
        if (_textViewImageDescription.text.length==0) {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:@"Please enter to add description"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      [_textViewImageDescription setEditable:YES];
                                      [_textViewImageDescription becomeFirstResponder];
                                      
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
            
            
        } else {
            
            [_arrOfImageDescriptionSaved replaceObjectAtIndex:indexToDelete withObject:_textViewImageDescription.text];
            NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
            [defsss setObject:_arrOfImageDescriptionSaved forKey:@"imageDescription"];
            [defsss setBool:YES forKey:@"yesEditedImageDescription"];
            [defsss synchronize];
            [_btnEditDescription setTitle:@"Edit Description" forState:UIControlStateNormal];
            [_textViewImageDescription setEditable:NO];
            
        }
    }

}
@end
