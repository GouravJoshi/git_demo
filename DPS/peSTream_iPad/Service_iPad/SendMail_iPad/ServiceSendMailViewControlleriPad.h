//
//  SendMailViewController.h
//  DPS
//
//  Created by Rakesh Jain on 02/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface ServiceSendMailViewControlleriPad : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityWorkOderDetailServiceAuto,
    *entityImageDetailServiceAuto,
    *entityEmailDetailServiceAuto,
    *entityPaymentInfoServiceAuto,
    *entityChemicalListDetailServiceAuto,
    *entityModifyDateServiceAuto,
    *entityCompanyDetailServiceAuto,
    *entityWOProductServiceAuto,
    *entityProblemImageDetail;
    NSEntityDescription *entityServiceModifyDate;
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
    NSEntityDescription *entityServiceDynamic;
    NSFetchRequest *requestNewServiceDynamic;
    NSSortDescriptor *sortDescriptorServiceDynamic;
    NSArray *sortDescriptorsServiceDynamic;
    NSManagedObject *matchesServiceDynamic;
    NSArray *arrAllObjServiceDynamic;
    
    NSEntityDescription *entityWorkOrderDocuments;
    NSFetchRequest *requestNewWorkOrderDocuments;
    NSSortDescriptor *sortDescriptorWorkOrderDocuments;
    NSArray *sortDescriptorsWorkOrderDocuments;
    NSManagedObject *matchesWorkOrderDocuments;
    NSArray *arrAllObjWorkOrderDocuments;
    
    
    //For Termite Flow
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    NSEntityDescription *entityTermiteTexas;
    
    //Termite Image
    NSEntityDescription *entityImageDetailsTermite;
    NSSortDescriptor *sortDescriptorImageDetail;
    NSFetchRequest *requestImageDetail;
    NSArray *sortDescriptorsImageDetail;
    
    //Florida
    NSEntityDescription *entityTermiteFlorida;
    NSFetchRequest *requestTermiteFlorida;
    NSSortDescriptor *sortDescriptorTermiteFlorida;
    NSArray *sortDescriptorsTermiteFlorida;
    NSManagedObject *matchesTermiteFlorida;
    NSManagedObject *matchesWorkOrderFlorida;
    NSArray *arrAllObjTermiteFlorida;
    
    
}
@property (strong, nonatomic) IBOutlet UIButton *btnSurvey;
@property (weak, nonatomic) IBOutlet UILabel *lblFromValue;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailId;
- (IBAction)actionOnAdd:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblEmailData;
- (IBAction)actionOnSendEmail:(id)sender;
- (IBAction)actionOnContinueToSetup:(id)sender;
@property (weak, nonatomic) NSString *isCustomerPresent;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceAutomation;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceDynamic;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsWorkOrderDocuments;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerProblemImageDetail;


- (IBAction)action_Back:(id)sender;

//Document
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_SendmailTable_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_DocumentTable_H;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewSendMail;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constView_H;
@property (weak, nonatomic) IBOutlet UITableView *table_Document;


@property (strong, nonatomic) IBOutlet UIButton *btnSendMail;
@property (strong, nonatomic) NSString *fromWhere;
@property (strong, nonatomic) NSString *strWoIdd;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWDO;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWDOPricing;
@property (weak, nonatomic) IBOutlet UILabel *lblMainHeader;
@property(strong,nonatomic) NSString *strHeaderValue;
@property(strong,nonatomic) UIImage *strBackRoundImage;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerNpmaTermite;
-(void)callApiToSyncDataForWdoPendingApprovalAgreementToServer:(NSString *)str :(NSString *)strWdoWoIdLocal;

@end
