//
//  GeneralInfoAppointmentView.m
//  DPS
//
//  Created by Rakesh Jain on 28/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
#import "TermiteSelectStateViewControlleriPad.h"
#import "GeneralInfoAppointmentViewiPad.h"
#import "AllImportsViewController.h"
#import "AppointmentView.h"
#import "ServiceDetailAppointmentView.h"
#import "ImagePreviewGeneralInfoAppointmentView.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "AppDelegate.h"
#import "WorkOrderDetailsService.h"
#import "WorkOrderDetailsService+CoreDataProperties.h"
#import "ServiceAutoModifyDate.h"
#import "ServiceAutoModifyDate+CoreDataProperties.h"
#import "ServiceSendMailViewController.h"
#import "ServiceDocumentsViewController.h"
//#import "ImageDetailsServiceAuto.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
#import "EmailDetailServiceAuto.h"
#import "EmailDetailServiceAuto+CoreDataProperties.h"
#import <QuartzCore/QuartzCore.h>
#import <sys/utsname.h>
#import "EditImageViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "DPS-Swift.h"


@interface GeneralInfoAppointmentViewiPad ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,CLLocationManagerDelegate>
{
    Global *global;
    NSString *strWorkOrderId,*strResetReasonToShow,*strResertId,*strEmpID,*strUserName,*strCompanyKey,*strGlobalWorkOrderStatus,*strEmpName,*strFromEmail,*strDepartmentType;
    BOOL isResetReason,isComplete,isNoEmail;
    NSDictionary *dictDetailsFortblView;
    NSMutableArray *arrDataTblView,*arrOfBeforeImageAll,*arrOfCrewCheckBoxes,*arrOfCrewTagSelected,*arrOfCrew,*arrOfImageCaption,*arrOfPrimaryEmailNew,*arrOfSecondaryEmailNew,*arrOfPrimaryEmailAlreadySaved,*arrOfSecondaryEmailAlreadySaved,*arrOfImageDescription,*arrOfImagenameCollewctionView;
    UITableView *tblData;
    UIView *viewBackGround;
    NSString *strEmailIdGlobal,*strTotalAllowedCrew,*strEmailIdGlobalSecondary,*strStartTimeFormatted;
    BOOL yesEditedSomething;
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    
    //Graph Image
    NSMutableArray *arrOfImagenameCollewctionViewGraph,*arrOfImageCaptionGraph,*arrOfImageDescriptionGraph,*arrOfBeforeImageAllGraph;
    NSMutableArray *arrImageLattitude,*arrImageLongitude,*arrImageGraphLattitude,*arrImageGraphLongitude;
    NSMutableArray *arrOfServiceAddImageName;
    NSString *strServiceAddImage;
    NSString *strTimeInLat,*strTimeInLong,*strServicePOCId,*strBillingPOCId;
    
    
}

@end

@implementation GeneralInfoAppointmentViewiPad

- (void)viewDidLoad {
    
    yesEditedSomething=NO;
    _btnUploadServiceAddImage.tag=123456;
    
    arrOfCrewCheckBoxes=[[NSMutableArray alloc]init];
    arrOfCrewTagSelected=[[NSMutableArray alloc]init];
    arrOfCrew=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfPrimaryEmailNew=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailNew=[[NSMutableArray alloc]init];
    arrOfPrimaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    
    //Graph
    arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    
    //Graph
    arrOfBeforeImageAllGraph=[[NSMutableArray alloc]init];
    
    arrImageLattitude=[[NSMutableArray alloc]init];
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    arrOfServiceAddImageName=[[NSMutableArray alloc]init];
    
    
    //Graph
    _viewGraphImage.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _viewGraphImage.layer.borderWidth=1.0;
    _viewGraphImage.layer.cornerRadius=5.0;
    
    [super viewDidLoad];
    
    _txtView_TechComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_TechComment.layer.borderWidth=1.0;
    _txtView_TechComment.layer.cornerRadius=5.0;
    
    _lbl_StartTime.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _lbl_StartTime.layer.borderWidth=1.0;
    _lbl_StartTime.layer.cornerRadius=5.0;
    
    
    //============================================================================
    //============================================================================
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    arrOfBeforeImageAll=[[NSMutableArray alloc]init];
    
    global = [[Global alloc] init];
    [self getClockStatus];
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strWorkOrderId=[defsLead valueForKey:@"LeadId"];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    
    NSString *strServiceReportType=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportType"]];
    
    if ([strServiceReportType isEqualToString:@"CompanyEmail"]) {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportEmail"]];
        
    } else {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
        
    }
    
    _lbl_EmpName.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeName"]];
    
    [self fetchWorkOrderFromDataBase];
    [self fetchImageDetailFromDataBase];
    
    _txtView_TechComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_TechComment.layer.borderWidth=1.0;
    
    /*
     _txtView_TechComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     _txtView_TechComment.layer.borderWidth=1.0;
     
     _lbl_SpecialInstruction.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     _lbl_SpecialInstruction.layer.borderWidth=1.0;
     
     _lbl_ServiceInstruction.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     _lbl_ServiceInstruction.layer.borderWidth=1.0;
     
     _lbl_Directions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     _lbl_Directions.layer.borderWidth=1.0;
     
     _lbl_Attributes.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     _lbl_Attributes.layer.borderWidth=1.0;
     
     _lbl_OtherInstructions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
     _lbl_OtherInstructions.layer.borderWidth=1.0;
     
     */
    
    _txtView_ResetDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_ResetDescription.layer.borderWidth=1.0;
    
    if (isComplete) {
        
        [_btnUploadServiceAddImage setEnabled:NO];
        // [self ForSavenContinueView];
        [_btn_Status setTitle:@"Complete" forState:UIControlStateNormal];
        [_btn_Status setEnabled:NO];
        [_txt_PrimaryEmailId setEnabled:NO];
        [_txt_PrimaryEmailId setHidden:YES];
        [_lbl_PrimEmail setHidden:NO];
        
        [_txt_SecondaryEmail setEnabled:NO];
        [_txt_SecondaryEmail setHidden:YES];
        [_lbl_SecEmail setHidden:NO];
        
        [_txt_PrimaryPhone setEnabled:NO];
        [_txt_PrimaryPhone setHidden:YES];
        [_lbl_PrimePhone setHidden:NO];
        
        [_txt_SecondaryPhone setEnabled:NO];
        [_txt_SecondaryPhone setHidden:YES];
        [_lbl_SeconPhone setHidden:NO];
        
        [_btn_NoEmail setEnabled:NO];
        [_txtView_TechComment setEditable:NO];
        [self performSelector:@selector(ForSavenContinueView) withObject:nil afterDelay:1.0];
        
        [_btnAddGraphhh setEnabled:NO];
        
        _btn_ServiceAddressPOCDetailDcs.enabled=NO;
        _btn_BillingAddressPOCDetailDcs.enabled=NO;

    }else if (isResetReason){
        
        _btn_ServiceAddressPOCDetailDcs.enabled=NO;
        _btn_BillingAddressPOCDetailDcs.enabled=NO;

        [_btnUploadServiceAddImage setEnabled:NO];
        
        //  [self ForResetReasonView];
        [_btn_Status setTitle:@"Reset" forState:UIControlStateNormal];
        [_btn_Status setEnabled:NO];
        [_txt_PrimaryEmailId setEnabled:NO];
        [_txt_PrimaryEmailId setHidden:NO];
        [_lbl_PrimEmail setHidden:YES];
        
        [_txt_SecondaryEmail setEnabled:NO];
        [_txt_SecondaryEmail setHidden:NO];
        [_lbl_SecEmail setHidden:YES];
        
        [_txt_PrimaryPhone setEnabled:NO];
        [_txt_PrimaryPhone setHidden:NO];
        [_lbl_PrimePhone setHidden:YES];
        
        [_txt_SecondaryPhone setEnabled:NO];
        [_txt_SecondaryPhone setHidden:NO];
        [_lbl_SeconPhone setHidden:YES];
        
        [_btn_NoEmail setEnabled:NO];
        [_txtView_TechComment setEditable:NO];
        [self performSelector:@selector(ForResetReasonView) withObject:nil afterDelay:1.0];
        
        _lbl_StartTime.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        _lbl_StartTime.layer.borderWidth=1.0;
        
        _lbl_TechComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        _lbl_TechComment.layer.borderWidth=1.0;
        
        [_btnAddGraphhh setEnabled:YES];
        
    }else{
        
        _btn_ServiceAddressPOCDetailDcs.enabled=YES;
        _btn_BillingAddressPOCDetailDcs.enabled=YES;

        [_btnUploadServiceAddImage setEnabled:YES];
        
        [_btnAddGraphhh setEnabled:YES];
        
        // [self ForSavenContinueView];
        [_btn_Status setTitle:@"Incomplete" forState:UIControlStateNormal];
        [_btn_Status setEnabled:YES];
        [_txt_PrimaryEmailId setEnabled:YES];
        [_txt_PrimaryEmailId setHidden:NO];
        [_lbl_PrimEmail setHidden:YES];
        
        [_txt_SecondaryEmail setEnabled:YES];
        [_txt_SecondaryEmail setHidden:NO];
        [_lbl_SecEmail setHidden:YES];
        
        [_txt_PrimaryPhone setEnabled:YES];
        [_txt_PrimaryPhone setHidden:NO];
        [_lbl_PrimePhone setHidden:YES];
        
        [_txt_SecondaryPhone setEnabled:YES];
        [_txt_SecondaryPhone setHidden:NO];
        [_lbl_SeconPhone setHidden:YES];
        
        [_btn_NoEmail setEnabled:YES];
        [_txtView_TechComment setEditable:YES];
        [self performSelector:@selector(ForSavenContinueView) withObject:nil afterDelay:1.0];
        
        _lbl_StartTime.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        _lbl_StartTime.layer.borderWidth=1.0;
        
        _lbl_TechComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        _lbl_TechComment.layer.borderWidth=1.0;
        
    }
    //============================================================================
    //============================================================================
    
    // Do any additional setup after loading the view.
    //Nilind 27 Feb
    //_const_ScrollVieww_H.constant=_const_ScrollVieww_H.constant+200;
    //[_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_const_ScrollVieww_H.constant)];
    
    //End
    
    //    UITapGestureRecognizer *singleTap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedScrollView)];
    //    singleTap3.numberOfTapsRequired = 1;
    //    [_scrollVieww setUserInteractionEnabled:YES];
    //    [_scrollVieww addGestureRecognizer:singleTap3];
    
    
    //    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
    //    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    //    singleTapGestureRecognizer.enabled = YES;
    //    [viewBackAlertt setUserInteractionEnabled:YES];
    //   // singleTapGestureRecognizer.cancelsTouchesInView = NO;
    //    [viewBackAlertt addGestureRecognizer:singleTapGestureRecognizer];
    
    
    // self.imageCollectionView.collectionViewLayout = [[CustomImageFlowLayout alloc] init];
    
    
    [_btn_ServiceAddressPOCDetailDcs.layer setCornerRadius:5.0f];
    [_btn_ServiceAddressPOCDetailDcs.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_ServiceAddressPOCDetailDcs.layer setBorderWidth:0.8f];
    [_btn_ServiceAddressPOCDetailDcs.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_ServiceAddressPOCDetailDcs.layer setShadowOpacity:0.3];
    [_btn_ServiceAddressPOCDetailDcs.layer setShadowRadius:3.0];
    [_btn_ServiceAddressPOCDetailDcs.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btn_BillingAddressPOCDetailDcs.layer setCornerRadius:5.0f];
    [_btn_BillingAddressPOCDetailDcs.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_BillingAddressPOCDetailDcs.layer setBorderWidth:0.8f];
    [_btn_BillingAddressPOCDetailDcs.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_BillingAddressPOCDetailDcs.layer setShadowOpacity:0.3];
    [_btn_BillingAddressPOCDetailDcs.layer setShadowRadius:3.0];
    [_btn_BillingAddressPOCDetailDcs.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

}
-(void)singleTap{
    
    [txtFieldCaption resignFirstResponder];
    [txtViewImageDescription resignFirstResponder];
    
}
-(void)tapDetectedScrollView{
    
    [txtFieldCaption resignFirstResponder];
    [txtViewImageDescription resignFirstResponder];
    [_txtView_TechComment resignFirstResponder];
    [_txtView_ResetDescription resignFirstResponder];
    [_txt_PrimaryPhone resignFirstResponder];
    [_txt_PrimaryEmailId resignFirstResponder];
    [_txt_SecondaryEmail resignFirstResponder];
    [_txt_SecondaryPhone resignFirstResponder];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    //Graph
    [self fetchGraphImageDetailFromDataBase];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        /*  UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
         EditImageViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewControlleriPad"];
         [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
        
        ////Akshay 22 may 2019
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        DrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        /////
        
    } else {
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isFromBackServiceDynamci"];
        
        if (isFromBack) {
            
            arrOfBeforeImageAll=nil;
            arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            
            arrImageLattitude=nil;
            arrImageLattitude=[[NSMutableArray alloc]init];
            arrImageLongitude=nil;
            arrImageLongitude=[[NSMutableArray alloc]init];
            
            arrImageGraphLattitude=nil;
            arrImageGraphLattitude=[[NSMutableArray alloc]init];
            arrImageGraphLongitude=nil;
            arrImageGraphLongitude=[[NSMutableArray alloc]init];
            
            [defsBack setBool:NO forKey:@"isFromBackServiceDynamci"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBase];
            
        }
        
        
        //    _lblOne.clipsToBounds = YES;
        //    _lblOne.layer.masksToBounds = YES;
        //    _lblOne.layer.cornerRadius = 40.0;
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreview"];
        if (yesFromDeleteImage) {
            
            yesEditedSomething=YES;
            
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreview"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfBeforeImageAll.count; k++) {
                
                NSDictionary *dictdat=arrOfBeforeImageAll[k];
                
                NSString *strImageName;
                
                if ([dictdat isKindOfClass:[NSString class]]) {
                    
                    strImageName=arrOfBeforeImageAll[k];
                    
                } else {
                    
                    strImageName=[dictdat valueForKey:@"woImagePath"];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImages"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                        
                    }
                }
                //            if (yesFoundName) {
                //
                //                [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                //
                //            }
            }
            if (!(arrTempBeforeImage.count==0)) {
                // arrOfBeforeImageAll=nil;
                // arrOfBeforeImageAll=[[NSMutableArray alloc]init];
                [arrOfBeforeImageAll removeObjectsInArray:arrTempBeforeImage];
            }
            
            yesEditedSomething=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImages"];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew synchronize];
            
            [self saveImageToCoreData];

        }
        
        //Graph
        //Change For Image Description  yesEditedImageDescription
        BOOL yesEditedImageCaptionGraph=[defsSS boolForKey:@"yesEditedImageCaptionGraph"];
        if (yesEditedImageCaptionGraph) {
            
            yesEditedSomething=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
            [defsnew synchronize];
            
            [self saveImageToCoreData];

        }
        
        //Graph
        //Change For Image Description  yesEditedImageDescription
        BOOL yesEditedImageDescriptionGraph=[defsSS boolForKey:@"yesEditedImageDescriptionGraph"];
        if (yesEditedImageDescriptionGraph) {
            
            yesEditedSomething=YES;
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
            [defsnew synchronize];
            
            [self saveImageToCoreData];

        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            yesEditedSomething=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            
            [self saveImageToCoreData];

        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            yesEditedSomething=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            
            [self saveImageToCoreData];

        }
        
        [self downloadingImagesThumbNailCheck];
        
        //Graph
        [self downloadGraphImage];
        
        
        
    }
    
    
    
    [_lbl_PrimEmail setHidden:YES];
    [_lbl_SecEmail setHidden:YES];
    [_txt_PrimaryEmailId setHidden:YES];
    [_txt_SecondaryEmail setHidden:YES];
    
    if ([_btn_Status.titleLabel.text isEqualToString:@"Reset"]) {
        
        [self ForResetReasonView];
        
    } else {
        
        [self ForSavenContinueView];
        
    }
    
    [_imageCollectionView reloadData];
    
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    
    NSArray *arrOfUnsignedDocs=[defsss objectForKey:@"ListOfUnsignedDocs"];
    
    NSString *strWoNos=[matchesWorkOrder valueForKey:@"workOrderNo"];
    
    if ([arrOfUnsignedDocs containsObject:strWoNos]) {
        
        [_btnServiceDocs setImage:[UIImage imageNamed:@"document_iconR"] forState:UIControlStateNormal];
        
    } else {
        
        [_btnServiceDocs setImage:[UIImage imageNamed:@"document_icon"] forState:UIControlStateNormal];
        
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
#pragma mark- Button Actions Methods
//============================================================================

- (IBAction)action_History:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"WDOiPad"
                                                             bundle: nil];
    ServiceHistory_iPadVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistory_iPadVC"];
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    
    /*UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ServiceHistoryMechanical
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistoryMechanical"];
    [self.navigationController pushViewController:objByProductVC animated:NO];*/
    
}


- (IBAction)action_Back:(id)sender {
    
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentVC class]]) {
                index=k1;
                //break;
            }
        }
        AppointmentVC *myController = (AppointmentVC *)[self.navigationController.viewControllers objectAtIndex:index];
        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
        [self.navigationController popToViewController:myController animated:NO];
        
    }
    else
    {
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentViewiPad class]]) {
                index=k1;
                //break;
            }
        }
        AppointmentViewiPad *myController = (AppointmentViewiPad *)[self.navigationController.viewControllers objectAtIndex:index];
        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
        [self.navigationController popToViewController:myController animated:NO];
    }
    
}

- (IBAction)action_StartTime:(id)sender {
    
    if (_lbl_StartTime.text.length==0) {
        
        [self changeTimeStart];
        
    } else {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:Alert
                                   message:@"Are you sure you want to reset start time"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Yes-Reset" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  [self changeTimeStart];
                                  
                              }];
        [alert addAction:yes];
        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                             }];
        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)changeTimeStart{
    
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
    NSDate *currentDate=[NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *stringCurrentDate = [formatter stringFromDate:currentDate];
    strStartTimeFormatted=stringCurrentDate;
    
    NSDateFormatter *formatterNew = [[NSDateFormatter alloc] init];
    [formatterNew setDateFormat:@"MM/dd/yyyy hh:mm a"];
    [formatterNew setTimeZone:[NSTimeZone localTimeZone]];
    NSString *stringCurrentDateNew = [formatterNew stringFromDate:currentDate];
    
    _lbl_StartTime.text=stringCurrentDateNew;
    
    //Nilind 08 Dec
    
    //Lat long code
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    strTimeInLat=latitude;
    strTimeInLong=longitude;
    //End
    
    
}

- (IBAction)action_PreviewBeforeImage:(id)sender
{
    
    _btnUploadServiceAddImage.tag=123456;
    
    /*
     UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
     delegate:self
     cancelButtonTitle:@"Cancel"
     destructiveButtonTitle:nil
     otherButtonTitles:@"Capture New", @"Gallery", nil];
     
     [actionSheet showInView:self.view];
     
     */
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                                  
                                  
                              }else{
                                  
                                  if (arrOfBeforeImageAll.count<10)
                                  {
                                      NSLog(@"The CApture Image.");
                                      
                                      NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                      
                                      BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                                      
                                      if (isfirstTimeAudio) {
                                          
                                          [defs setBool:NO forKey:@"firstCamera"];
                                          [defs synchronize];
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }else{
                                          BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                          
                                          if (isCameraPermissionAvailable) {
                                              
                                              UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                              imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                              imagePickController.delegate=(id)self;
                                              imagePickController.allowsEditing=TRUE;
                                              [self presentViewController:imagePickController animated:YES completion:nil];
                                              
                                              
                                          }else{
                                              
                                              UIAlertController *alert= [UIAlertController
                                                                         alertControllerWithTitle:@"Alert"
                                                                         message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                              
                                              UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction * action)
                                                                    {
                                                                        
                                                                        
                                                                        
                                                                    }];
                                              [alert addAction:yes];
                                              UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                           NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                           [[UIApplication sharedApplication] openURL:url];
                                                                       } else {
                                                                           
                                                                           //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                           //                                     [alert show];
                                                                           
                                                                       }
                                                                       
                                                                   }];
                                              [alert addAction:no];
                                              [self presentViewController:alert animated:YES completion:nil];
                                          }
                                      }
                                  }
                                  else
                                  {
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                      [alert show];
                                  }
                              }
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                                 
                                 
                             }else{
                                 
                                 if (arrOfBeforeImageAll.count<10)
                                 {
                                     
                                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                     
                                     BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                                     
                                     if (isfirstTimeAudio) {
                                         
                                         [defs setBool:NO forKey:@"firstGallery"];
                                         [defs synchronize];
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }else{
                                         BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                         
                                         if (isCameraPermissionAvailable) {
                                             
                                             UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                             imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                             imagePickController.delegate=(id)self;
                                             imagePickController.allowsEditing=TRUE;
                                             [self presentViewController:imagePickController animated:YES completion:nil];
                                             
                                             
                                         }else{
                                             
                                             UIAlertController *alert= [UIAlertController
                                                                        alertControllerWithTitle:@"Alert"
                                                                        message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       
                                                                       
                                                                   }];
                                             [alert addAction:yes];
                                             UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                        handler:^(UIAlertAction * action)
                                                                  {
                                                                      
                                                                      if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                          NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                          [[UIApplication sharedApplication] openURL:url];
                                                                      } else {
                                                                          
                                                                          //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                          //                                     [alert show];
                                                                          
                                                                      }
                                                                      
                                                                  }];
                                             [alert addAction:no];
                                             [self presentViewController:alert animated:YES completion:nil];
                                         }
                                     }
                                 }else{
                                     
                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                     [alert show];
                                 }
                             }
                             
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}

//============================================================================
#pragma mark- Action Sheet and Image Picker
//============================================================================

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==5)
    {
        NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
        
        arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dictData=arrOfImagesDetail[k];
                
                [arrOfBeforeImages addObject:dictData];
                
            }else{
                
                [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
                
            }
            
        }
        
        arrOfImagesDetail=arrOfBeforeImages;
        
        if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
            [global AlertMethod:Info :NoBeforeImg];
        }else if (arrOfImagesDetail.count==0){
            [global AlertMethod:Info :NoBeforeImg];
        }
        else {
            NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
            for (int k=0; k<arrOfImagesDetail.count; k++) {
                if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dict=arrOfImagesDetail[k];
                    [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
                }else{
                    
                    [arrOfImagess addObject:arrOfImagesDetail[k]];
                    
                }
            }
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                     bundle: nil];
            ImagePreviewGeneralInfoAppointmentViewiPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
            objByProductVC.arrOfImages=arrOfImagess;
            objByProductVC.indexOfImage=@"7";
            objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;
            //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
            [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
            
        }
    }
    else if (buttonIndex == 0)
    {
        
        if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
            
            
        }else{
            
            if (arrOfBeforeImageAll.count<10)
            {
                NSLog(@"The CApture Image.");
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                
                BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                
                if (isfirstTimeAudio) {
                    
                    [defs setBool:NO forKey:@"firstCamera"];
                    [defs synchronize];
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }else{
                    BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                    
                    if (isCameraPermissionAvailable) {
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                        
                    }else{
                        
                        UIAlertController *alert= [UIAlertController
                                                   alertControllerWithTitle:@"Alert"
                                                   message:@"Camera Permission not allowed.Please go to settings and allow"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action)
                                              {
                                                  
                                                  
                                                  
                                              }];
                        [alert addAction:yes];
                        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                             {
                                                 
                                                 if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                     NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                     [[UIApplication sharedApplication] openURL:url];
                                                 } else {
                                                     
                                                     //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                     //                                     [alert show];
                                                     
                                                 }
                                                 
                                             }];
                        [alert addAction:no];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    else if (buttonIndex == 1)
    {
        if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
            
            
        }else{
            
            if (arrOfBeforeImageAll.count<10)
            {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                
                BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                
                if (isfirstTimeAudio) {
                    
                    [defs setBool:NO forKey:@"firstGallery"];
                    [defs synchronize];
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }else{
                    BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                    
                    if (isCameraPermissionAvailable) {
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                        
                    }else{
                        
                        UIAlertController *alert= [UIAlertController
                                                   alertControllerWithTitle:@"Alert"
                                                   message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action)
                                              {
                                                  
                                                  
                                                  
                                              }];
                        [alert addAction:yes];
                        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                             {
                                                 
                                                 if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                     NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                     [[UIApplication sharedApplication] openURL:url];
                                                 } else {
                                                     
                                                     //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                     //                                     [alert show];
                                                     
                                                 }
                                                 
                                             }];
                        [alert addAction:no];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }
            }else{
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }
    }
}

- (IBAction)action_SavenContinue:(id)sender {
    
    [self.view endEditing:true];
    
    BOOL isCompulsory =[self isCompulsoryImage];
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
    {
        if ([strDepartmentType isEqualToString:@"Termite"]) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServiceiPad"
                                                                     bundle: nil];
            TermiteSelectStateViewControlleriPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TermiteSelectStateViewControlleriPad"];
            objByProductVC.strWorkOrder=strWorkOrderId;
            objByProductVC.strWorkOrderStatus=strGlobalWorkOrderStatus;
            objByProductVC.strTermiteStateType=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"termiteStateType"]];

            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        } else {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServiceiPad"
                                                                     bundle: nil];
            ServiceDetailAppointmentViewiPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDetailAppointmentViewiPad"];
            objByProductVC.matchesWorkOrderZSync=matchesWorkOrder;
            [self.navigationController pushViewController:objByProductVC animated:NO];
        }
        
    }
    else
    {
        
        // int allowedCrew=[strTotalAllowedCrew intValue];
        
        if (isResetReason) {
            
            if (!isNoEmail) {
                
                NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
                NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
                
                /*
                 if (_txt_PrimaryEmailId.text.length==0) {
                 
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter email to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                 [alert show];
                 
                 } else if (![emailPredicate evaluateWithObject:_txt_PrimaryEmailId.text]){
                 
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter valid email to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                 [alert show];
                 
                 }
                 else
                 */
                
                if (_lbl_StartTime.text.length==0){
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter the Start Time" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                    [alert show];
                    
                }
                else if ([_btn_ResetReason.titleLabel.text isEqualToString:@"Choose Reset Reason"]){
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select reset reason" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                    [alert show];
                    
                }
                else if (isCompulsory) {
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take atleast one before image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                //Nilind 24 Feb  //TEMP COMMENT
                /*else if (arrOfCrewTagSelected.count>allowedCrew){
                 
                 NSString *strMessageToShow=[NSString stringWithFormat:@"Please select only %d Crew",allowedCrew];
                 
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:strMessageToShow delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                 [alert show];
                 
                 }*/
                //End
                else{
                    [self saveEmailIdPrimaryToCoreData];
                    [self saveImageToCoreData];
                    if (yesEditedSomething) {
                        NSLog(@"Yes Edited Something In Db");
                        [self updateModifyDate];
                        
                    }
                    
                    [self updateWorkOrderDetail];
                    // [self performSelector:@selector(goToAppointment) withObject:nil afterDelay:1.0];
                    [self goToAppointment];
                }
                
            } else {
                
                if (_lbl_StartTime.text.length==0)
                {
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please provide start time to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                    [alert show];
                    
                }
                else
                    if ([_btn_ResetReason.titleLabel.text isEqualToString:@"Choose Reset Reason"])
                    {
                        
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select reset reason" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                        [alert show];
                        
                    }
                    else if (isCompulsory) {
                        
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take atleast one before image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                        
                    }
                //Nilind 24 Feb TEMP COMMENT
                /*else if (arrOfCrewTagSelected.count>allowedCrew){
                 
                 NSString *strMessageToShow=[NSString stringWithFormat:@"Please select only %d Crew",allowedCrew];
                 
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:strMessageToShow delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                 [alert show];
                 
                 }*/
                    else{
                        [self deleteEmailIdFromCoreData];
                        // [self saveEmailIdPrimaryToCoreData];
                        [self saveImageToCoreData];
                        if (yesEditedSomething) {
                            NSLog(@"Yes Edited Something In Db");
                            [self updateModifyDate];
                            
                        }
                        
                        [self updateWorkOrderDetail];
                        // [self performSelector:@selector(goToAppointment) withObject:nil afterDelay:1.0];
                        [self goToAppointment];
                        
                    }
            }
            
        } else {
            
            if (_lbl_StartTime.text.length==0){
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please provide start time to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
                [alert show];
                
            }
            else if (isCompulsory) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take atleast one before image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            
            //Nilind 24 Feb /temp comment
            /*
             else if (arrOfCrewTagSelected.count>allowedCrew){
             
             NSString *strMessageToShow=[NSString stringWithFormat:@"Please select only %d Crew",allowedCrew];
             
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:strMessageToShow delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
             [alert show];
             
             }*/
            else{
                if (!isNoEmail) {
                    [self saveEmailIdPrimaryToCoreData];
                }else{
                    [self deleteEmailIdFromCoreData];
                }
                [self saveImageToCoreData];
                if (yesEditedSomething) {
                    NSLog(@"Yes Edited Something In Db");
                    [self updateModifyDate];
                    
                }
                
                [self updateWorkOrderDetail];
                
                if ([strDepartmentType isEqualToString:@"Termite"]) {
                    
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServiceiPad"
                                                                             bundle: nil];
                    TermiteSelectStateViewControlleriPad
                    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TermiteSelectStateViewControlleriPad"];
                    objByProductVC.strWorkOrder=strWorkOrderId;
                    objByProductVC.strWorkOrderStatus=strGlobalWorkOrderStatus;
                    objByProductVC.strTermiteStateType=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"termiteStateType"]];

                    [self.navigationController pushViewController:objByProductVC animated:NO];
                    
                } else {
                    
                    
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServiceiPad"
                                                                             bundle: nil];
                    ServiceDetailAppointmentViewiPad
                    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDetailAppointmentViewiPad"];
                    objByProductVC.matchesWorkOrderZSync=matchesWorkOrder;
                    [self.navigationController pushViewController:objByProductVC animated:NO];
                }
                
            }
        }
    }
}
-(void)goToAppointment{
    
    /*
     if (isCompulsory) {
     
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take atleast one before image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
     [alert show];
     
     }
     */
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    [defsss setBool:YES forKey:@"isFromSurveyToSendEmail"];
    [defsss synchronize];
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServiceiPad"
                                                             bundle: nil];
    ServiceSendMailViewControlleriPad
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewControlleriPad"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(BOOL)isCompulsoryImage{
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    BOOL isCompulsory=[defsss boolForKey:@"isCompulsoryBeforeImageService"];
    if (isCompulsory) {
        
        if (arrOfBeforeImageAll.count==0) {
            
            isCompulsory=YES;
            
        } else {
            
            isCompulsory=NO;
            
        }
        
    } else {
        
        isCompulsory=NO;
    }
    
    if(isResetReason){
        isCompulsory=NO;
    }
    if ([strGlobalWorkOrderStatus isEqualToString:@"Complete"] || [strGlobalWorkOrderStatus isEqualToString:@"Completed"]) {
        
        isCompulsory=NO;
        
    }
    
    return isCompulsory;
    
}
- (IBAction)action_Cancel:(id)sender {
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentVC class]]) {
                index=k1;
                //break;
            }
        }
        AppointmentVC *myController = (AppointmentVC *)[self.navigationController.viewControllers objectAtIndex:index];
        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
        [self.navigationController popToViewController:myController animated:NO];
        
    }
    else
    {
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentViewiPad class]]) {
                index=k1;
                //break;
            }
        }
        AppointmentViewiPad *myController = (AppointmentViewiPad *)[self.navigationController.viewControllers objectAtIndex:index];
        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
        [self.navigationController popToViewController:myController animated:NO];
    }
    
}

-(void)ShowValues{
    
    // NSDictionary *dictofWorkorderDetail=[_dictOfWorkOrders valueForKey:@"WorkorderDetail"];
    
    //changes for multiple POC
    [self fetchServiceAddressPOCDetailDcsFromDB];
    [self fetchBillingAddressPOCDetailDcsFromDB];

    strServicePOCId=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"servicePOCId"]];
    strBillingPOCId=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingPOCId"]];

    arrOfCrew=[matchesWorkOrder valueForKey:@"routeCrewList"];
    
    strTotalAllowedCrew=[matchesWorkOrder valueForKey:@"totalallowCrew"];
    strDepartmentType=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"departmentType"]];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    NSString *strFirstName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"firstName"]];
    NSString *strMiddleName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"middleName"]];
    NSString *strLastName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"lastName"]];
    
    NSMutableArray *arrOfName=[[NSMutableArray alloc]init];
    
    if (!(strFirstName.length==0)) {
        [arrOfName addObject:strFirstName];
    }
    if (!(strMiddleName.length==0)) {
        
        [arrOfName addObject:strMiddleName];
        
    }
    if (!(strLastName.length==0)) {
        
        [arrOfName addObject:strLastName];
        
    }
    
    NSString *strFullName=[arrOfName componentsJoinedByString:@" "];
    
    [defs setObject:strFullName forKey:@"customerNameService"];
    //[matchesWorkOrder valueForKey:@"EmployeeNo"]
    [defs setObject:strEmpName forKey:@"inspectorNameService"];
    [defs synchronize];
    
    NSString *strStatuss=[matchesWorkOrder valueForKey:@"workorderStatus"];
    strGlobalWorkOrderStatus=strStatuss;
    if([strStatuss caseInsensitiveCompare:@"Reset"] == NSOrderedSame){
        isResetReason=YES;
        isComplete=NO;
    }else if([strStatuss caseInsensitiveCompare:@"Incomplete"] == NSOrderedSame){
        isResetReason=NO;
        isComplete=NO;
    }else{
        isResetReason=NO;
        isComplete=YES;
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:[matchesWorkOrder valueForKey:@"timeIn"]];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:[matchesWorkOrder valueForKey:@"timeIn"]];
    }
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    //[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=[matchesWorkOrder valueForKey:@"timeIn"];
    }
    
    strStartTimeFormatted=finalTime;
    
    //New change for Formatted Date
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=[matchesWorkOrder valueForKey:@"timeIn"];
    }
    
    _lbl_StartTime.text=finalTime;
    
    _lbl_FName.text=strFullName;
    
    _lbl_Middlename.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"middleName"]];
    
    //Nilind 27 Feb
    
    
    _lblKeyMapValue.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"keyMap"]];
    _lblServiceDescriptionValue.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceDescription"]];
    
    //End
    
    
    _lbl_LName.text=[matchesWorkOrder valueForKey:@"lastName"];
    
    _lbl_CompanyName.text=[matchesWorkOrder valueForKey:@"companyName"];
    
    _txt_PrimaryEmailId.text=[matchesWorkOrder valueForKey:@"primaryEmail"];
    
    _lbl_PrimEmail.text=[matchesWorkOrder valueForKey:@"primaryEmail"];
    
    
    strEmailIdGlobal=[matchesWorkOrder valueForKey:@"primaryEmail"];
    
    strEmailIdGlobalSecondary=[matchesWorkOrder valueForKey:@"secondaryEmail"];
    
    
    //Multiple Primary Email
    
    NSString *strTemPrimaryEmail=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"primaryEmail"]];
    NSString *strTemSecondaryEmail=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"secondaryEmail"]];
    
    NSArray *arrTempPrimaryEmail,*arrTempsecondaryEmail;
    if (!(strTemPrimaryEmail.length==0) &&!([strTemPrimaryEmail isEqualToString:@"(null)"])) {
        
        [_btnPrimaryEmail setTitle:[matchesWorkOrder valueForKey:@"primaryEmail"] forState:UIControlStateNormal];
        arrTempPrimaryEmail=[strTemPrimaryEmail componentsSeparatedByString:@","];
        
    }else{
        
        [_btnPrimaryEmail setTitle:@"" forState:UIControlStateNormal];
        
    }
    
    if (!(strTemSecondaryEmail.length==0) &&!([strTemSecondaryEmail isEqualToString:@"(null)"])) {
        
        [_btnSecondaryEmail setTitle:[matchesWorkOrder valueForKey:@"secondaryEmail"] forState:UIControlStateNormal];
        arrTempsecondaryEmail=[strTemSecondaryEmail componentsSeparatedByString:@","];
        
    }else{
        
        [_btnSecondaryEmail setTitle:@"" forState:UIControlStateNormal];
        
    }
    
    if (arrTempPrimaryEmail.count>0) {
        
        [arrOfPrimaryEmailNew addObjectsFromArray:arrTempPrimaryEmail];
        [arrOfPrimaryEmailAlreadySaved addObjectsFromArray:arrTempPrimaryEmail];
        
    }
    if (arrTempsecondaryEmail.count>0) {
        
        [arrOfSecondaryEmailNew addObjectsFromArray:arrTempsecondaryEmail];
        [arrOfSecondaryEmailAlreadySaved addObjectsFromArray:arrTempsecondaryEmail];
        
    }
    
    
    
    //    if (_txt_PrimaryEmailId.text.length==0) {
    //
    //        [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
    //        isNoEmail=YES;
    //
    //    }
    
    if (_btnPrimaryEmail.titleLabel.text.length==0) {
        
        [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        isNoEmail=YES;
        
    }
    
    _txt_SecondaryEmail.text=[matchesWorkOrder valueForKey:@"secondaryEmail"];
    
    _lbl_SecEmail.text=[matchesWorkOrder valueForKey:@"secondaryEmail"];
    
    _txt_PrimaryPhone.text=[matchesWorkOrder valueForKey:@"primaryPhone"];
    
    _lbl_PrimePhone.text=[matchesWorkOrder valueForKey:@"primaryPhone"];
    
    _txt_SecondaryPhone.text=[matchesWorkOrder valueForKey:@"secondaryPhone"];
    
    _lbl_SeconPhone.text=[matchesWorkOrder valueForKey:@"secondaryPhone"];
    
    _lbl_WorkOrderNo.text=[matchesWorkOrder valueForKey:@"workOrderNo"];
    
    _lbl_AccNo.text=[matchesWorkOrder valueForKey:@"accountNo"];
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];//Order accountNo
    _lbl_AccountNo.text=[NSString stringWithFormat:@"A/C #: %@, Order #: %@",[matchesWorkOrder valueForKey:@"accountNo"],[matchesWorkOrder valueForKey:@"workOrderNo"]];
    [defsss setValue:[matchesWorkOrder valueForKey:@"accountNo"] forKey:@"workOrderAccountNo"];
    [defsss setValue:_lbl_AccountNo.text forKey:@"lblName"];
    [defsss setValue:[matchesWorkOrder valueForKey:@"accountNo"] forKey:@"AccNoService"];
    [defsss setValue:[matchesWorkOrder valueForKey:@"departmentId"] forKey:@"DepartmentIdService"];
    [defsss synchronize];
    
    
    NSUserDefaults *defsssNo1=[NSUserDefaults standardUserDefaults];
    [defsssNo1 setValue:@"" forKey:@"lblThirdPartyAccountNo"];
    [defsssNo1 synchronize];
    NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"thirdPartyAccountNo"]];
    if (strThirdPartyAccountNo.length>0) {
        
        _lbl_AccNo.text=strThirdPartyAccountNo;
        _lbl_AccountNo.text=[NSString stringWithFormat:@"A/C #: %@, Order #: %@",strThirdPartyAccountNo,[matchesWorkOrder valueForKey:@"workOrderNo"]];
        NSUserDefaults *defsssNo=[NSUserDefaults standardUserDefaults];
        [defsssNo setValue:_lbl_AccountNo.text forKey:@"lblThirdPartyAccountNo"];
        [defsssNo synchronize];
        
    }

    _lbl_Category.text=[matchesWorkOrder valueForKey:@"categoryName"];
    
    _lbl_SubCategory.text=[matchesWorkOrder valueForKey:@"subCategory"];
    
    _lbl_Services.text=[matchesWorkOrder valueForKey:@"services"];
    
    CGSize s11 = [[matchesWorkOrder valueForKey:@"services"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(125, MAXFLOAT)];
    
    if (s11.height<42) {
        
    } else {
        _const_Services_H.constant=s11.height;
    }
    
    
    _lbl_EmpNo.text=[matchesWorkOrder valueForKey:@"employeeNo"];
    
    // _lbl_EmpName.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"middleName"]];
    
    _lblRotesName.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"routeName"]];
    
    _lbl_Routes.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"routeNo"]];
    
    //_lbl_SchStartDate.text=[global ChangeDateToLocalDateOtherNew:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"scheduleStartDateTime"]]];
    
    _lbl_SchStartDate.text=[global ChangeDateToLocalDateOtherServiceAppointmentsUpdated:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"scheduleStartDateTime"]]];
    
    _txtViewAccountDescription.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"accountDescription"]];
    
    
    // _lbl_SchEndDate.text=[global ChangeDateToLocalDateOtherNew:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"scheduleEndDateTime"]]];
    
    _lbl_SchEndDate.text=[global ChangeDateToLocalDateOtherServiceAppointmentsUpdated:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"scheduleEndDateTime"]]];
    
    NSString *strBillingcountry=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingCountry"]];
    
    if ([strBillingcountry caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strBillingcountry=@"United States";
    }
    
    NSString *strbillingAddress2=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingAddress2"]];
    if (strbillingAddress2.length==0) {
        
        _lbl_BillingAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@",[matchesWorkOrder valueForKey:@"billingAddress1"],[matchesWorkOrder valueForKey:@"billingCity"],[matchesWorkOrder valueForKey:@"billingState"],[matchesWorkOrder valueForKey:@"billingZipcode"]];
        
    } else {
        
        _lbl_BillingAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"billingAddress1"],[matchesWorkOrder valueForKey:@"billingAddress2"],[matchesWorkOrder valueForKey:@"billingCity"],[matchesWorkOrder valueForKey:@"billingState"],[matchesWorkOrder valueForKey:@"billingZipcode"]];
        
    }
    
    NSString *strTempString1=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"billingAddress1"],[matchesWorkOrder valueForKey:@"billingAddress2"],[matchesWorkOrder valueForKey:@"billingCity"],[matchesWorkOrder valueForKey:@"billingState"],[matchesWorkOrder valueForKey:@"billingZipcode"]];
    
    strTempString1=[strTempString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CGSize sBilling = [strTempString1 sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(125, MAXFLOAT)];
    
    if (sBilling.height<62) {
        
    } else {
        _const_BillingAddress_H.constant=sBilling.height;
    }
    
    NSString *strServiceAddress=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceCountry"]];
    
    if ([strServiceAddress caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strServiceAddress=@"United States";
    }
    
    NSString *strServiceAddress2=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingAddress2"]];
    if (strServiceAddress2.length==0) {
        
        _lbl_ServiceAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@",[matchesWorkOrder valueForKey:@"servicesAddress1"],[matchesWorkOrder valueForKey:@"serviceCity"],[matchesWorkOrder valueForKey:@"serviceState"],[matchesWorkOrder valueForKey:@"serviceZipcode"]];
        
        
    }else{
        
        _lbl_ServiceAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"servicesAddress1"],[matchesWorkOrder valueForKey:@"serviceAddress2"],[matchesWorkOrder valueForKey:@"serviceCity"],[matchesWorkOrder valueForKey:@"serviceState"],[matchesWorkOrder valueForKey:@"serviceZipcode"]];
        
        
    }
    
    
    NSString *strTempString2=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"servicesAddress1"],[matchesWorkOrder valueForKey:@"serviceAddress2"],[matchesWorkOrder valueForKey:@"serviceCity"],[matchesWorkOrder valueForKey:@"serviceState"],[matchesWorkOrder valueForKey:@"serviceZipcode"]];
    
    strTempString2=[strTempString2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CGSize sService = [strTempString2 sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(125, MAXFLOAT)];
    
    if (sService.height<62) {
        
    } else {
        _const_ServiceAddress_H.constant=sService.height;
    }
    
    _lbl_SpecialInstruction.text=[matchesWorkOrder valueForKey:@"specialInstruction"];
    
    NSString *strTempString3=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"specialInstruction"]];
    
    strTempString3=[strTempString3 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CGSize sSpecial = [strTempString3 sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(125, MAXFLOAT)];
    
    if (sSpecial.height<62) {
        
    } else {
        _const_SpecialInstruction_H.constant=sSpecial.height;
    }
    
    _lbl_ServiceInstruction.text=[matchesWorkOrder valueForKey:@"serviceInstruction"];
    
    
    NSString *strTempString=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceInstruction"]];
    
    strTempString=[strTempString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CGSize sServiceInstruction = [strTempString sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(125, MAXFLOAT)];
    
    if (sServiceInstruction.height<62) {
        
    } else {
        _const_ServiceInstruction_H.constant=sServiceInstruction.height;
    }
    
    _lbl_Directions.text=[matchesWorkOrder valueForKey:@"direction"];
    
    NSString *strTempString4=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"direction"]];
    
    strTempString4=[strTempString4 stringByReplacingOccurrencesOfString:@" " withString:@""];
    //strTempString4 = [strTempString4 stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    CGSize sdirection = [strTempString4 sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(125, MAXFLOAT)];
    
    if (sdirection.height<62) {
        
    } else {
        _const_Directions_H.constant=sdirection.height;
    }
    
    _lbl_Targets.text=[matchesWorkOrder valueForKey:@"targets"];
    
    NSString *strTempString7=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"targets"]];
    
    strTempString7=[strTempString7 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CGSize Targetsatributes = [strTempString7 sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(125, MAXFLOAT)];
    
    if (Targetsatributes.height<62) {
        
    } else {
        // _const_Attributes_H.constant=satributes.height;
    }
    
    _lbl_Attributes.text=[matchesWorkOrder valueForKey:@"atributes"];
    
    NSString *strTempString5=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"atributes"]];
    
    strTempString5=[strTempString5 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CGSize satributes = [strTempString5 sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(125, MAXFLOAT)];
    
    if (satributes.height<62) {
        
    } else {
        _const_Attributes_H.constant=satributes.height;
    }
    
    _lbl_OtherInstructions.text=[matchesWorkOrder valueForKey:@"otherInstruction"];
    
    NSString *strTempString6=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"otherInstruction"]];
    
    strTempString6=[strTempString6 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CGSize sotherInstruction = [strTempString6 sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(125, MAXFLOAT)];
    
    if (sotherInstruction.height<62) {
        
    } else {
        _const_OtherInstructions_H.constant=sotherInstruction.height;
    }
    
    _txtView_TechComment.text=[matchesWorkOrder valueForKey:@"technicianComment"];
    
    _txtView_ResetDescription.text=[matchesWorkOrder valueForKey:@"resetDescription"];
    
    strResertId=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"resetId"]];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    dictDetailsFortblView=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    if (strResertId.length==0) {
        //Yaha default wala set kar de
        
        if ([dictDetailsFortblView isKindOfClass:[NSString class]]) {
            
        }else{
            
            //Default LogType
            
            if (!(dictDetailsFortblView.count==0)) {
                
                arrDataTblView=[[NSMutableArray alloc]init];
                NSArray *arrOfLogType=[dictDetailsFortblView valueForKey:@"ResetReasons"];
                if ([arrOfLogType isKindOfClass:[NSArray class]]) {
                    
                    if (arrOfLogType.count>0) {
                        
                        [arrDataTblView addObjectsFromArray:arrOfLogType];
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:0];
                        [_btn_ResetReason setTitle:[dictData valueForKey:@"ResetReason"] forState:UIControlStateNormal];
                        strResertId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ResetId"]];
                        
                    }else{
                        
                        [_btn_ResetReason setTitle:@"Choose Reset Reason" forState:UIControlStateNormal];
                        
                    }
                    

                }else{
                    [_btn_ResetReason setTitle:@"Choose Reset Reason" forState:UIControlStateNormal];
                }
            }
            
        }
        
    } else {
        NSArray *arrOfResetReasons;
        
        if ([dictDetailsFortblView isKindOfClass:[NSString class]]) {
            
            arrOfResetReasons=nil;
            
        }else{
            
            arrOfResetReasons=[dictDetailsFortblView valueForKey:@"ResetReasons"];
            
        }
        
        for (int k=0; k<arrOfResetReasons.count; k++) {
            
            NSDictionary *dictData=arrOfResetReasons[k];
            NSString *strIdd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ResetId"]];
            if ([strIdd isEqualToString:strResertId]) {
                
                [_btn_ResetReason setTitle:[dictData valueForKey:@"ResetReason"] forState:UIControlStateNormal];
                break;
                
            }
        }
    }
    
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
        
        [_btn_StartTime setEnabled:NO];
        
    }else
    {
        
        [_btn_StartTime setEnabled:YES];
        
    }
    //Nilind 24 Feb
    _lblCrewView.hidden=YES;
    _const_Crew_H.constant=0;
    // [self createCrewView];
    //End
    _txtCellNo.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"cellNo"]];
    _billingContactName.text=[NSString stringWithFormat:@"%@ %@ %@",[matchesWorkOrder valueForKey:@"billingFirstName"],[matchesWorkOrder valueForKey:@"billingMiddleName"],[matchesWorkOrder valueForKey:@"billingLastName"]];
    _billingContactCellNo.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingCellNo"]];
    _billingContactMapCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingMapCode"]];
    _billingContactPrimaryEmail.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingPrimaryEmail"]];
    _billingContactSecondayEmail.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingSecondaryEmail"]];
    _billingContactPrimaryPhone.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingPrimaryPhone"]];
    _billingContactSecondaryPhone.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingSecondaryPhone"]];
    
    _serviceContactName.text=[NSString stringWithFormat:@"%@ %@ %@",[matchesWorkOrder valueForKey:@"serviceFirstName"],[matchesWorkOrder valueForKey:@"serviceMiddleName"],[matchesWorkOrder valueForKey:@"serviceLastName"]];
    _serviceContactCellNo.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceCellNo"]];
    _serviceContactMapCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceMapCode"]];
    _serviceContactGateCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceGateCode"]];
    
    _serviceContactPrimaryEmail.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"servicePrimaryEmail"]];
    _serviceContactSecondaryEmail.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceSecondaryEmail"]];
    _serviceContactPrimaryPhone.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"servicePrimaryPhone"]];
    _serviceContactSecondaryPhone.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceSecondaryPhone"]];
    _serviceContactNotes.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceNotes"]];
    
    strServiceAddImage= [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceAddressImagePath"]];
    [self downloadImageServiceAddImage:strServiceAddImage];
    
    
    _lblBillingCounty.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingCounty"]];
    _lblBillingSchoolDistrict.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingSchoolDistrict"]];
    _lblServiceCounty.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceCounty"]];
    _lblServiceSchoolDistrict.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceSchoolDistrict"]];

    _lblDriveTime.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"driveTimeStr"]];
    _lblLatestStartTime.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"latestStartTimeStr"]];
    _lblEarliestStartTime.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"earliestStartTimeStr"]];
    
}
-(void)updateWorkOrderDetail{
    
    
    if (yesEditedSomething) {
        
        NSLog(@"Yes Edited Something In Db");
        
        ////Yaha Par zSyn ko yes karna hai
        [matchesWorkOrder setValue:@"yes" forKey:@"zSync"];
        
    }
    
    [matchesWorkOrder setValue:arrOfCrew forKey:@"routeCrewList"];
    
    NSMutableArray *arrOfSelectedCrewIds=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfCrewTagSelected.count; k++) {
        
        NSString *strTag=arrOfCrewTagSelected[k];
        
        NSDictionary *dictDataCrewId=[arrOfCrew objectAtIndex:[strTag intValue]];
        
        [arrOfSelectedCrewIds addObject:[dictDataCrewId valueForKey:@"EmployeeNo"]];
        
    }
    
    NSString *strAssignedCrewIds=[arrOfSelectedCrewIds componentsJoinedByString:@","];
    
    [matchesWorkOrder setValue:strAssignedCrewIds forKey:@"assignCrewIds"];
    
    [matchesWorkOrder setValue:_txtView_TechComment.text forKey:@"technicianComment"];
    [matchesWorkOrder setValue:_btnPrimaryEmail.currentTitle forKey:@"primaryEmail"];
    [matchesWorkOrder setValue:_btnSecondaryEmail.currentTitle forKey:@"secondaryEmail"];
    [matchesWorkOrder setValue:_txt_PrimaryPhone.text forKey:@"primaryPhone"];
    [matchesWorkOrder setValue:_txt_SecondaryPhone.text forKey:@"secondaryPhone"];
    [matchesWorkOrder setValue:_txtView_ResetDescription.text forKey:@"resetDescription"];
    [matchesWorkOrder setValue:strResertId forKey:@"resetId"];
    [matchesWorkOrder setValue:_btn_Status.titleLabel.text forKey:@"workorderStatus"];
    [matchesWorkOrder setValue:strStartTimeFormatted forKey:@"timeIn"];
    [matchesWorkOrder setValue:_txtCellNo.text forKey:@"cellNo"];
    
    //Nilind 08 Dec
    [matchesWorkOrder setValue:strTimeInLat forKey:@"timeInLatitude"];
    [matchesWorkOrder setValue:strTimeInLong forKey:@"timeInLongitude"];
    //End
    
    //Nilind 28 Feb
    
    //End
    
    
    NSString *DeviceVersion = [[UIDevice currentDevice] systemVersion];
    NSString *VersionDatee = VersionDate;
    NSString *VersionNumberr = VersionNumber;
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *DeviceName=[NSString stringWithCString:systemInfo.machine
                                            encoding:NSUTF8StringEncoding];
    
    NSLog(@"DeviceVersion===%@----VersionDatee===%@----VersionNumberr===%@----DeviceName===%@",DeviceVersion,VersionDatee,VersionNumberr,DeviceName);
    
    [matchesWorkOrder setValue:VersionDatee forKey:@"versionDate"];
    [matchesWorkOrder setValue:VersionNumberr forKey:@"versionNumber"];
    [matchesWorkOrder setValue:DeviceVersion forKey:@"deviceVersion"];
    [matchesWorkOrder setValue:DeviceName forKey:@"deviceName"];
    [matchesWorkOrder setValue:strServiceAddImage forKey:@"serviceAddressImagePath"];
    
    
    //Update POC Details
    
    NSDictionary *dictDataServicePOC,*dictDataBillingPOC;
    
    for (int k=0; k<arrAllObjMechanicalServiceAddressPOCDetailDcs.count; k++) {
        
        NSDictionary *dictData=arrAllObjMechanicalServiceAddressPOCDetailDcs[k];
        
        NSString *strContactID=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        if ([strContactID isEqualToString:strServicePOCId]) {
            
            dictDataServicePOC=dictData;
            break;
            
        }
        
    }
    
    for (int k=0; k<arrAllObjMechanicalBillingAddressPOCDetailDcs.count; k++) {
        
        NSDictionary *dictData=arrAllObjMechanicalBillingAddressPOCDetailDcs[k];
        
        NSString *strContactID=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        if ([strContactID isEqualToString:strBillingPOCId]) {
            
            dictDataBillingPOC=dictData;
            break;
            
        }
        
    }
    
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"FirstName"] forKey:@"serviceFirstName"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"MiddleName"] forKey:@"serviceMiddleName"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"LastName"] forKey:@"serviceLastName"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"PrimaryEmail"] forKey:@"servicePrimaryEmail"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"SecondaryEmail"] forKey:@"serviceSecondaryEmail"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"PrimaryPhone"] forKey:@"servicePrimaryPhone"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"SecondaryPhone"] forKey:@"serviceSecondaryPhone"];
    
    
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"FirstName"] forKey:@"billingFirstName"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"MiddleName"] forKey:@"billingMiddleName"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"LastName"] forKey:@"billingLastName"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"PrimaryEmail"] forKey:@"billingPrimaryEmail"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"SecondaryEmail"] forKey:@"billingSecondaryEmail"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"PrimaryPhone"] forKey:@"billingPrimaryPhone"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"SecondaryPhone"] forKey:@"billingSecondaryPhone"];

    NSError *error;
    [context save:&error];
    
}
-(void)fetchWorkOrderFromDataBase{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [self.fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        [self ShowValues];
        
        NSString *strDepartmentTYPE= [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"departmentType"]];
        
        if ([strDepartmentTYPE caseInsensitiveCompare:@"Mechanical"] == NSOrderedSame) {
            
            [global AlertMethod:Alert :@"Something went wrong. Please process again."];
            
            NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
            
            NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
            
            if ([strAppointmentFlow isEqualToString:@"New"])
            {
                
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
                AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
                
            }
            else
            {
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
                AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
                [self.navigationController pushViewController:objByProductVC animated:NO];
            }
            
        }

    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
    [self deleteBeforeImagesBeforeSaving];
    //Graph
    [self deleteGraphImagesBeforeSaving];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    for (int k=0; k<arrOfBeforeImageAll.count; k++)
    {
        
        if ([arrOfBeforeImageAll[k] isKindOfClass:[NSString class]]) {
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfBeforeImageAll objectAtIndex:k]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }else{
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            NSDictionary *dictData=[arrOfBeforeImageAll objectAtIndex:k];
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    
    
    //Graph
    for (int k=0; k<arrOfImagenameCollewctionViewGraph.count; k++)
    {
        // Image Detail Entity
        entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
        
        ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
        
        if ([arrOfImagenameCollewctionViewGraph[k] isKindOfClass:[NSString class]])
        {
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfImagenameCollewctionViewGraph objectAtIndex:k]];
            objImageDetail.woImageType=@"Graph";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionGraph objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionGraph objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
        }
        else
        {
            NSDictionary *dictData=[arrOfImagenameCollewctionViewGraph objectAtIndex:k];
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"Graph";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionGraph objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionGraph objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    
    //........................................................................
}

-(void)deleteBeforeImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"Before"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(_btnUploadServiceAddImage.tag==101)
    {
        
        NSLog(@"Yes Edited Something In Db");
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        // NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
        NSString  *strImageNamess = [NSString stringWithFormat:@"CustomerAddressImages\\ServiceAddImg%@%@.jpg",strDate,strTime];
        
        [arrOfServiceAddImageName addObject:strImageNamess];
        UIImage *image=[info objectForKey: UIImagePickerControllerEditedImage];
        

        //_imgViewServiceAddress.image=image;
        [self resizeImage:image :strImageNamess];
        strServiceAddImage=strImageNamess;
        [self saveImageAfterDownloadServiceAdd :image :strImageNamess];
        [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    }
    else
    {
        yesEditedSomething=YES;
        NSLog(@"Yes Edited Something In Db");
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
        
        [arrOfBeforeImageAll addObject:strImageNamess];
        [arrOfImagenameCollewctionView addObject:strImageNamess];
        [_imageCollectionView reloadData];
        [self performSelector:@selector(ForSavenContinueView) withObject:nil afterDelay:1.0];
       // UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];

        [self resizeImage:chosenImage :strImageNamess];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
        //Lat long code
               
               CLLocationCoordinate2D coordinate = [global getLocation] ;
               NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
               NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
               [arrImageLattitude addObject:latitude];
               [arrImageLongitude addObject:longitude];
        
        //imageCaption
        
        NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
        
        BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
        
        if (yesImageCaption) {
            
            [self alertViewCustom];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
            [self saveImageToCoreData];

        }
       
    }
    
}

//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
        [_scrollVieww setContentOffset:bottomOffset animated:YES];
        
        [self saveImageToCoreData];

    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}
-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
    [_scrollVieww setContentOffset:bottomOffset animated:YES];
    
    [self saveImageToCoreData];

}

-(void)alertToEnterImageCaption{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Caption Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Image Description Here...";
        textField.tag=8;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        UITextField * txtImageDescriptions = textfields[1];
        if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
            
            if (txtHistoricalDays.text.length>0) {
                
                [arrOfImageCaption addObject:txtHistoricalDays.text];
                
            } else {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtImageDescriptions.text.length>0) {
                
                [arrOfImageDescription addObject:txtImageDescriptions.text];
                
            } else {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            }
            
            CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
            [_scrollVieww setContentOffset:bottomOffset animated:YES];
            
            
        } else {
            
            [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
        [_scrollVieww setContentOffset:bottomOffset animated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    
    //NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}



/*
 -(UIImage*) drawText:(NSString*) text
 inImage:(UIImage*)  image
 {
 
 NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
 [formatterDate setDateFormat:@"MM/dd/yyyy"];
 [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
 NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
 NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
 [formatter setDateFormat:@"HH:mm:ss"];
 [formatter setTimeZone:[NSTimeZone localTimeZone]];
 NSString *strTime = [formatter stringFromDate:[NSDate date]];
 
 text=[NSString stringWithFormat:@"%@ %@",strDate,strTime];
 
 UIFont *font = [UIFont boldSystemFontOfSize:22];
 UIGraphicsBeginImageContext(image.size);
 [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
 CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
 [[UIColor whiteColor] set];
 [text drawInRect:CGRectIntegral(rect) withFont:font];
 UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
 UIGraphicsEndImageContext();
 
 return newImage;
 }
 */


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

-(void)fetchImageDetailFromDataBase
{
    
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageLattitude=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetail.count; j++) {
            
            matchesImageDetail=arrAllObjImageDetail[j];
            NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"Before"]) {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                NSString *lattitude=[matchesImageDetail valueForKey:@"latitude"];
                NSString *longitude=[matchesImageDetail valueForKey:@"longitude"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,
                                           lattitude.length==0 ? @"" : lattitude,
                                           longitude.length==0 ? @"" : longitude,nil];
                
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",
                                         @"latitude",
                                         @"longitude",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrOfBeforeImageAll addObject:dict_ToSendLeadInfo];
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
                
                //Lat long
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageLongitude addObject:strLong];
                    
                }
                
                
                
                
            }
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

- (IBAction)action_NoEmail:(id)sender {
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btn_NoEmail imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        _txt_PrimaryEmailId.text=@"";
        [_btnPrimaryEmail setTitle:@"" forState:UIControlStateNormal];
        arrOfPrimaryEmailNew=nil;
        arrOfPrimaryEmailNew=[[NSMutableArray alloc]init];
        isNoEmail=YES;
        
    }else{
        
        [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        isNoEmail=NO;
        
    }
    
}
- (IBAction)hideKeyBoard:(id)sender{
    
    [self resignFirstResponder];
    
}
- (IBAction)action_ResetReasonMaster:(id)sender {
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfLogType;
    
    if ([dictDetailsFortblView isKindOfClass:[NSString class]]) {
        
        arrOfLogType=nil;
        
    }else{
        
        arrOfLogType=[dictDetailsFortblView valueForKey:@"ResetReasons"];
        
    }
    
    if ([arrOfLogType isKindOfClass:[NSArray class]]) {
        for (int k=0; k<arrOfLogType.count; k++) {
            
            NSDictionary *dictDataa=arrOfLogType[k];
            [arrDataTblView addObject:dictDataa];
            
        }
    }
    //    }else{
    //        [global AlertMethod:Info :NoDataAvailable];
    //    }
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailableReset];
    }else{
        tblData.tag=104;
        [self tableLoad:tblData.tag];
    }
    
}

-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 104:
        {
            [self setTableFrame];
            break;
        }
        case 105:
        {
            [self setTableFrameForEmailIds :@"primary"];
            break;
        }
        case 106:
        {
            [self setTableFrameForEmailIds :@"secondary"];
            break;
        }
        case 108:
        {
            [self setTableFrame];
            break;
        }
        case 109:
        {
            [self setTableFrame];
            break;
        }
    
        default:
            break;
    }
    //[tblData setContentOffset:CGPointZero animated:YES];
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}
-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [_scrollVieww addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}

-(void)setTableFrameForEmailIds :(NSString*)strType
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [_scrollVieww addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    UILabel *lblMsg=[[UILabel alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y-40, tblData.bounds.size.width, 30)];
    lblMsg.text=@"Swipe to delete & tap to edit";
    lblMsg.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    // [viewBackGround addSubview:lblMsg];
    
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 42)];
    
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(methodOnDoneEditingEmailIds) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGround addSubview:btnDone];
    
    
    UIButton *btnAddNew=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.size.width/2+2+tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 42)];
    
    [btnAddNew setTitle:@"Add New" forState:UIControlStateNormal];
    [btnAddNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnAddNew.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    if ([strType isEqualToString:@"primary"]) {
        
        [btnAddNew addTarget:self action:@selector(alertForPrimaryEmailSaving) forControlEvents:UIControlEventTouchDown];
        
    } else {
        
        [btnAddNew addTarget:self action:@selector(alertForSecondaryEmailSaving) forControlEvents:UIControlEventTouchDown];
        
    }
    
    [viewBackGround addSubview:btnAddNew];
    
}

-(void)methodOnDoneEditingEmailIds{
    
    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
    
    if (strMessage.length==0) {
        
        strMessage=@"";
        
    }
    
    [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
    
    NSString *strMessageSec=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
    
    if (strMessageSec.length==0) {
        
        strMessageSec=@"";
        
    }
    
    [_btnSecondaryEmail setTitle:strMessageSec forState:UIControlStateNormal];
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [tblData removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [txtFieldCaption resignFirstResponder];
    [txtViewImageDescription resignFirstResponder];
    
}

- (IBAction)action_BtnStatus:(id)sender {
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Choose Status"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Incomplete" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              yesEditedSomething=YES;
                              NSLog(@"Yes Edited Something In Db");
                              [_btn_Status setTitle:@"Incomplete" forState:UIControlStateNormal];
                              [_view_ResetReasons removeFromSuperview];
                              isResetReason=NO;
                              [self ForSavenContinueView];
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Reset" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             yesEditedSomething=YES;
                             NSLog(@"Yes Edited Something In Db");
                             [_btn_Status setTitle:@"Reset" forState:UIControlStateNormal];
                             isResetReason=YES;
                             [self ForResetReasonView];
                             
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
-(void)ForSavenContinueView{
    
    //  float heightCollectionView = (float)(arrOfImagenameCollewctionView.count)/3;
    
    // int roundedUp = ceil(heightCollectionView); NSLog(@"%d",roundedUp);
    
    if (arrOfImagenameCollewctionView.count==0) {
        
        _imageCollectionView.frame=CGRectMake(_imageCollectionView.frame.origin.x, _imageCollectionView.frame.origin.y, _imageCollectionView.frame.size.width, 0);
        
    } else {
        
        _imageCollectionView.frame=CGRectMake(_imageCollectionView.frame.origin.x, _imageCollectionView.frame.origin.y, _imageCollectionView.frame.size.width, [UIScreen mainScreen].bounds.size.height*20/100);
        
    }
    
    _btn_Cancel.frame=CGRectMake(_btn_Cancel.frame.origin.x, _imageCollectionView.frame.origin.y+_imageCollectionView.frame.size.height+10, _btn_Cancel.frame.size.width, _btn_Cancel.frame.size.height);
    
    _btn_SavenContinue.frame=CGRectMake(_btn_SavenContinue.frame.origin.x, _imageCollectionView.frame.origin.y+_imageCollectionView.frame.size.height+10, _btn_SavenContinue.frame.size.width, _btn_SavenContinue.frame.size.height);
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _btn_SavenContinue.frame.origin.y+_btn_SavenContinue.frame.size.height+15);
    frameFor_view_CustomerInfo.origin.x=0;
    frameFor_view_CustomerInfo.origin.y=_btn_Status.frame.size.height+_btn_Status.frame.origin.y;
    
    [_view_SavenContinue setFrame:frameFor_view_CustomerInfo];
    
    [_scrollVieww addSubview:_view_SavenContinue];
    
    //  _scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, [UIScreen mainScreen].bounds.size.width-2,2250);
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_btn_Status.frame.size.height+_btn_Status.frame.origin.y+_view_SavenContinue.frame.size.height+100)];
    
}
-(void)ForResetReasonView{
    
    //@"MasterServiceAutomation"
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _view_ResetReasons.frame.size.height);
    frameFor_view_CustomerInfo.origin.x=0;
    frameFor_view_CustomerInfo.origin.y=_btn_Status.frame.size.height+_btn_Status.frame.origin.y+10;
    
    [_view_ResetReasons setFrame:frameFor_view_CustomerInfo];
    
    [_scrollVieww addSubview:_view_ResetReasons];
    
    
    //    float heightCollectionView = (float)(arrOfImagenameCollewctionView.count)/2;
    //
    //    int roundedUp = ceil(heightCollectionView); NSLog(@"%d",roundedUp);
    
    if (arrOfImagenameCollewctionView.count==0) {
        
        _imageCollectionView.frame=CGRectMake(_imageCollectionView.frame.origin.x, _imageCollectionView.frame.origin.y, _imageCollectionView.frame.size.width, 0);
        
    }else{
        
        _imageCollectionView.frame=CGRectMake(_imageCollectionView.frame.origin.x, _imageCollectionView.frame.origin.y, _imageCollectionView.frame.size.width, [UIScreen mainScreen].bounds.size.height*20/100);
        
    }
    
    _btn_Cancel.frame=CGRectMake(_btn_Cancel.frame.origin.x, _imageCollectionView.frame.origin.y+_imageCollectionView.frame.size.height+10, _btn_Cancel.frame.size.width, _btn_Cancel.frame.size.height);
    
    _btn_SavenContinue.frame=CGRectMake(_btn_SavenContinue.frame.origin.x, _imageCollectionView.frame.origin.y+_imageCollectionView.frame.size.height+10, _btn_SavenContinue.frame.size.width, _btn_SavenContinue.frame.size.height);
    
    CGRect frameFor_ViewSavenContinue=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _btn_SavenContinue.frame.origin.y+_btn_SavenContinue.frame.size.height+15);
    frameFor_ViewSavenContinue.origin.x=0;
    frameFor_ViewSavenContinue.origin.y=_view_ResetReasons.frame.size.height+_view_ResetReasons.frame.origin.y+10;
    
    [_view_SavenContinue setFrame:frameFor_ViewSavenContinue];
    
    [_scrollVieww addSubview:_view_SavenContinue];
    
    //  _scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, [UIScreen mainScreen].bounds.size.width-2,2450);
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_btn_Status.frame.size.height+_btn_Status.frame.origin.y+_view_ResetReasons.frame.size.height+_view_SavenContinue.frame.size.height+100)];
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TextView Delegates METHOD-----------------
//============================================================================
//============================================================================


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
    
    if([text isEqualToString:@"\n"])
    {
        [_txtView_TechComment resignFirstResponder];
        [_txtView_ResetDescription resignFirstResponder];
        [txtViewImageDescription resignFirstResponder];
        [textView resignFirstResponder];
        return NO;
    }
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}

//============================================================================
//============================================================================
#pragma mark- Text Field Delegate Methods
//============================================================================
//============================================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    if (textField.tag==1) {
        if (textField.text.length==0) {
            
            [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            isNoEmail=YES;
            
        }else{
            
            [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            isNoEmail=NO;
            
        }
    }
    
    [_scrollVieww setContentOffset:CGPointMake(0,textField.frame.origin.y-50) animated:YES];
    
}

-(void)textViewDidBeginEditing:(UITextView *)textField{
    
    if (textField.tag==6) {
        
        [_scrollVieww setContentOffset:CGPointMake(0,textField.frame.origin.y-50) animated:YES];
        
    } else {
        
        [_scrollVieww setContentOffset:CGPointMake(0,_view_ResetReasons.frame.origin.y+textField.frame.origin.y-50) animated:YES];
        
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag==1) {
        if (textField.text.length==0) {
            
            [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            isNoEmail=YES;
            
        }else{
            
            [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            isNoEmail=NO;
            
        }
    }
    [textField resignFirstResponder];
    
    _txt_PrimaryEmailId.text =  [_txt_PrimaryEmailId.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    _txt_SecondaryEmail.text =  [_txt_SecondaryEmail.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
    if (textField.tag==7) {
        //Image Caption
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 250) ? NO : YES;
    }else  if (textField.tag==8) {
        //Image Description
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 250) ? NO : YES;
    }else  if (textField.tag==3) {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@"+0123456789" :_txt_PrimaryPhone.text];
        
        return isNuberOnly;
    }
    else  if (textField.tag==4) {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@"+0123456789" :_txt_SecondaryPhone.text];
        
        return isNuberOnly;
    }
    else  if (textField.tag==10)
    {
        if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame)
        {
            return NO;
        }
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :20 :(int)textField.text.length :@"+0123456789" :_txtCellNo.text];
        
        return isNuberOnly;
    }
    else if (textField.tag==1) {
        
        if (textField.text.length==0) {
            
            [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
            isNoEmail=YES;
            
        }else{
            
            [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            isNoEmail=NO;
            
        }
        return YES;
        
    } else {
        return YES;
    }
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==108) {
        
        return arrAllObjMechanicalServiceAddressPOCDetailDcs.count;
        
    }else if (tableView.tag==109) {
        
        return arrAllObjMechanicalBillingAddressPOCDetailDcs.count;
        
    }else
        
    return [arrDataTblView count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if (tableView.tag==108) {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrAllObjMechanicalServiceAddressPOCDetailDcs.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 108:
                {
                    NSDictionary *dictData=[arrAllObjMechanicalServiceAddressPOCDetailDcs objectAtIndex:indexPath.row];
                    
                    NSMutableArray *arrName=[[NSMutableArray alloc]init];
                    
                    NSString *FirstName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
                    
                    if (FirstName.length>0) {
                        
                        [arrName addObject:FirstName];
                        
                    }
                    
                    NSString *MiddleName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
                    
                    if (MiddleName.length>0) {
                        
                        [arrName addObject:MiddleName];
                        
                    }
                    
                    NSString *LastName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
                    
                    if (LastName.length>0) {
                        
                        [arrName addObject:LastName];
                        
                    }
                    
                    cell.textLabel.text=[arrName componentsJoinedByString:@" "];
                    
                    if ([strServicePOCId isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]]]) {
                        
                        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                        
                    }else{
                        
                        [cell setAccessoryType:UITableViewCellAccessoryNone];
                        
                    }
                    break;
                }
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
        
    }else if (tableView.tag==109) {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrAllObjMechanicalBillingAddressPOCDetailDcs.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 109:
                {
                    NSDictionary *dictData=[arrAllObjMechanicalBillingAddressPOCDetailDcs objectAtIndex:indexPath.row];
                    
                    NSMutableArray *arrName=[[NSMutableArray alloc]init];
                    
                    NSString *FirstName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
                    
                    if (FirstName.length>0) {
                        
                        [arrName addObject:FirstName];
                        
                    }
                    
                    NSString *MiddleName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
                    
                    if (MiddleName.length>0) {
                        
                        [arrName addObject:MiddleName];
                        
                    }
                    
                    NSString *LastName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
                    
                    if (LastName.length>0) {
                        
                        [arrName addObject:LastName];
                        
                    }
                    
                    cell.textLabel.text=[arrName componentsJoinedByString:@" "];
                    
                    if ([strBillingPOCId isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]]]) {
                        
                        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                        
                    }else{
                        
                        [cell setAccessoryType:UITableViewCellAccessoryNone];
                        
                    }
                    break;
                }
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
        
    }else if (!(arrDataTblView.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 104:
            {
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                cell.textLabel.text=[dictData valueForKey:@"ResetReason"];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                break;
            }
            case 105:
            {
                cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                break;
            }
            case 106:
            {
                cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                break;
            }
                
            default:
                break;
        }
        
    }
    if ([UIScreen mainScreen].bounds.size.height==667) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }
    else
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    cell.textLabel.numberOfLines=2;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==108){
        
        NSDictionary *dictData=[arrAllObjMechanicalServiceAddressPOCDetailDcs objectAtIndex:indexPath.row];
        
        NSMutableArray *arrName=[[NSMutableArray alloc]init];
        
        NSString *FirstName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
        
        if (FirstName.length>0) {
            
            [arrName addObject:FirstName];
            
        }
        
        NSString *MiddleName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
        
        if (MiddleName.length>0) {
            
            [arrName addObject:MiddleName];
            
        }
        
        NSString *LastName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
        
        if (LastName.length>0) {
            
            [arrName addObject:LastName];
            
        }
        
        strServicePOCId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        //[_btn_ServiceAddressPOCDetailDcs setTitle:[arrName componentsJoinedByString:@" "] forState:UIControlStateNormal];
        
        _serviceContactName.text=[arrName componentsJoinedByString:@" "];
        
        //        _serviceContactPrimaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PrimaryEmail"]]];
        //
        //        _serviceContactSecondaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SecondaryEmail"]]];
        //
        //        _serviceContactPrimaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PrimaryPhone"]]];
        //
        //        _serviceContactSecondaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SecondaryPhone"]]];
        
        _serviceContactPrimaryEmail.text=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictData valueForKey:@"PrimaryEmail"]]];
        
        _serviceContactSecondaryEmail.text=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictData valueForKey:@"SecondaryEmail"]]];
        
        _serviceContactPrimaryPhone.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PrimaryPhone"]];
        
        _serviceContactSecondaryPhone.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SecondaryPhone"]];
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    } else if (tableView.tag==109){
        
        NSDictionary *dictData=[arrAllObjMechanicalBillingAddressPOCDetailDcs objectAtIndex:indexPath.row];
        
        NSMutableArray *arrName=[[NSMutableArray alloc]init];
        
        NSString *FirstName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
        
        if (FirstName.length>0) {
            
            [arrName addObject:FirstName];
            
        }
        
        NSString *MiddleName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
        
        if (MiddleName.length>0) {
            
            [arrName addObject:MiddleName];
            
        }
        
        NSString *LastName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
        
        if (LastName.length>0) {
            
            [arrName addObject:LastName];
            
        }
        
        strBillingPOCId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        //[_btn_BillingAddressPOCDetailDcs setTitle:[arrName componentsJoinedByString:@" "] forState:UIControlStateNormal];
        
        //        _billingContactPrimaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PrimaryEmail"]]];
        //
        //        _billingContactSecondayEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SecondaryEmail"]]];
        //
        //        _billingContactPrimaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PrimaryPhone"]]];
        //
        //        _billingContactSecondaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SecondaryPhone"]]];
        
        _billingContactPrimaryEmail.text=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictData valueForKey:@"PrimaryEmail"]]];
        
        _billingContactSecondayEmail.text=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictData valueForKey:@"SecondaryEmail"]]];
        
        _billingContactPrimaryPhone.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PrimaryPhone"]];
        
        _billingContactSecondaryPhone.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SecondaryPhone"]];
        
        _billingContactName.text=[arrName componentsJoinedByString:@" "];
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }else if (!(arrDataTblView.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 104:
            {
                yesEditedSomething=YES;
                NSLog(@"Yes Edited Something In Db");
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                [_btn_ResetReason setTitle:[dictData valueForKey:@"ResetReason"] forState:UIControlStateNormal];
                strResertId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ResetId"]];
                break;
            }
            case 105:
            {
                
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Edit Email-Id"
                                                                                          message: @""
                                                                                   preferredStyle:UIAlertControllerStyleAlert];
                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    textField.placeholder = @"Enter Email-Id Here...";
                    textField.tag=7;
                    textField.delegate=self;
                    textField.text=arrOfPrimaryEmailNew[indexPath.row];
                    textField.textColor = [UIColor blackColor];
                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                    textField.borderStyle = UITextBorderStyleRoundedRect;
                    textField.keyboardType=UIKeyboardTypeDefault;
                }];
                [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    NSArray * textfields = alertController.textFields;
                    UITextField * txtHistoricalDays = textfields[0];
                    if (txtHistoricalDays.text.length>0) {
                        
                        txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
                                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
                        NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
                        
                        if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
                        {
                            
                            [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                            
                        }else if ([txtHistoricalDays.text containsString:@" "])
                        {
                            
                            [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                            
                        }else{
                            
                            if ([arrOfPrimaryEmailNew containsObject:txtHistoricalDays.text]) {
                                
                                if ([arrOfPrimaryEmailNew[indexPath.row] isEqualToString:txtHistoricalDays.text]) {
                                    
                                    [arrOfPrimaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                    
                                    arrDataTblView=[[NSMutableArray alloc]init];
                                    [arrDataTblView addObjectsFromArray:arrOfPrimaryEmailNew];
                                    [tblData reloadData];
                                    [self tablViewForPrimaryEmailIds];
                                    
                                } else {
                                    
                                    //AlertViewForAlreadyEmailPresentOnEdit
                                    [self performSelector:@selector(AlertViewForAlreadyEmailPresentOnEdit) withObject:nil afterDelay:0.2];
                                    
                                }
                                
                            } else {
                                
                                
                                [arrOfPrimaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                
                                arrDataTblView=[[NSMutableArray alloc]init];
                                [arrDataTblView addObjectsFromArray:arrOfPrimaryEmailNew];
                                [tblData reloadData];
                                [self tablViewForPrimaryEmailIds];
                                
                            }
                        }
                    } else {
                        
                        [self performSelector:@selector(AlertViewForEmptyEmailEdit) withObject:nil afterDelay:0.2];
                        
                    }
                }]];
                [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                    [self tablViewForPrimaryEmailIds];
                    
                }]];
                [self presentViewController:alertController animated:YES completion:nil];
                break;
            }
            case 106:
            {
                
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Edit Email-Id"
                                                                                          message: @""
                                                                                   preferredStyle:UIAlertControllerStyleAlert];
                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                    textField.placeholder = @"Enter Email-Id Here...";
                    textField.tag=7;
                    textField.delegate=self;
                    textField.text=arrOfSecondaryEmailNew[indexPath.row];
                    textField.textColor = [UIColor blackColor];
                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                    textField.borderStyle = UITextBorderStyleRoundedRect;
                    textField.keyboardType=UIKeyboardTypeDefault;
                }];
                [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    NSArray * textfields = alertController.textFields;
                    UITextField * txtHistoricalDays = textfields[0];
                    if (txtHistoricalDays.text.length>0) {
                        
                        txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
                                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
                        NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
                        
                        if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
                        {
                            
                            [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                            
                        }else if ([txtHistoricalDays.text containsString:@" "])
                        {
                            
                            [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                            
                        }else{
                            
                            if ([arrOfSecondaryEmailNew containsObject:txtHistoricalDays.text]) {
                                
                                if ([arrOfSecondaryEmailNew[indexPath.row] isEqualToString:txtHistoricalDays.text]) {
                                    
                                    [arrOfSecondaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                    
                                    arrDataTblView=[[NSMutableArray alloc]init];
                                    [arrDataTblView addObjectsFromArray:arrOfSecondaryEmailNew];
                                    [tblData reloadData];
                                    [self tablViewForSecondaryEmailIds];
                                    
                                } else {
                                    
                                    
                                    //AlertViewForAlreadyEmailPresentOnEdit
                                    [self performSelector:@selector(AlertViewForAlreadyEmailPresentOnEdit) withObject:nil afterDelay:0.2];
                                    
                                }
                                
                                
                            } else {
                                
                                [arrOfSecondaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                
                                arrDataTblView=[[NSMutableArray alloc]init];
                                [arrDataTblView addObjectsFromArray:arrOfSecondaryEmailNew];
                                [tblData reloadData];
                                [self tablViewForSecondaryEmailIds];
                                
                            }
                        }
                        
                    } else {
                        
                        [self performSelector:@selector(AlertViewForEmptyEmailEdit) withObject:nil afterDelay:0.2];
                        
                    }
                }]];
                [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                    [self tablViewForSecondaryEmailIds];
                    
                }]];
                [self presentViewController:alertController animated:YES completion:nil];
                
                break;
            }
            default:
                break;
        }
        
    }
    [tblData removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==105) {
        
        return true;
        
    }else if (tableView.tag==106){
        
        return true;
        
    }else
        
        return false;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==105) {
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            [arrOfPrimaryEmailNew removeObjectAtIndex:indexPath.row];
            arrDataTblView=[[NSMutableArray alloc]init];
            [arrDataTblView addObjectsFromArray:arrOfPrimaryEmailNew];
            [tblData reloadData];
        }
    }else if (tableView.tag==106){
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            [arrOfSecondaryEmailNew removeObjectAtIndex:indexPath.row];
            arrDataTblView=[[NSMutableArray alloc]init];
            [arrDataTblView addObjectsFromArray:arrOfSecondaryEmailNew];
            [tblData reloadData];
            
        }
    }
}

-(void)updateModifyDate{
    
    //============================================================================
    //============================================================================
#pragma mark- ---------******** Modify Date Save *******----------------
    //============================================================================
    //============================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityModifyDateServiceAuto];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
    }
    else
    {
        matchesServiceModifyDate=arrAllObjServiceModifyDate[0];
        
        NSString *strCurrentDateFormatted = [global modifyDateService];
        
        [matchesServiceModifyDate setValue:strCurrentDateFormatted forKey:@"modifyDate"];
        
        [global fetchWorkOrderFromDataBaseForServiceToUpdateModifydate:strWorkOrderId :strCurrentDateFormatted];
        
        //Final Save
        NSError *error;
        [context save:&error];
    }
}

-(void)goToDocumentView{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServiceiPad"
                                                             bundle: nil];
    ServiceDocumentsViewControlleriPad
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDocumentsViewControlleriPad"];
    objByProductVC.strAccountNo=[matchesWorkOrder valueForKey:@"accountNo"];
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    
}
- (IBAction)action_ServiceDocuments:(id)sender {
    
    [self goToDocumentView];
    
}
-(void)deleteEmailIdFromCoreData{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,strEmailIdGlobal];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)deleteEmailIdFromCoreDataNew :(NSString*)strEmailIdToDelete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,strEmailIdToDelete];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteEmailIdFromCoreDataSecondary{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,strEmailIdGlobalSecondary];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


-(void)saveEmailIdPrimaryToCoreData
{
    
    for (int k=0; k<arrOfPrimaryEmailAlreadySaved.count; k++) {
        
        [self deleteEmailIdFromCoreDataNew:arrOfPrimaryEmailAlreadySaved[k]];
        
    }
    
    for (int k=0; k<arrOfPrimaryEmailNew.count; k++) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        entityEmailDetailService= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        [request setEntity:entityEmailDetailService];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,arrOfPrimaryEmailNew[k]];
        
        [request setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [request setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerServiceEmail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerServiceEmail setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerServiceEmail performFetch:&error1];
        arrAllObj = [self.fetchedResultsControllerServiceEmail fetchedObjects];
        
        if (arrAllObj.count==0)
        {
            [self saveToCoreDataNewEmail :arrOfPrimaryEmailNew[k]];
            // [self saveToCoreDataNewEmailSecondary];
            
        }else
        {
            
            BOOL notMatched,notMatchedSecondaryEmail;
            
            notMatched=YES;
            notMatchedSecondaryEmail=YES;
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                NSString *strEmailToCompare=[matches valueForKey:@"emailId"];
                
                if ([strEmailToCompare isEqualToString:arrOfPrimaryEmailNew[k]]) {
                    
                    notMatched=NO;
                    
                    [matches setValue:arrOfPrimaryEmailNew[k] forKey:@"emailId"];
                    NSError *error1;
                    [context save:&error1];
                    
                }
            }
            
            if (notMatched) {
                
                [self saveToCoreDataNewEmail:arrOfPrimaryEmailNew[k]];
                
            }
        }
    }
    
    [self saveEmailIdSecondaryToCoreData];
    
}

-(void)saveEmailIdSecondaryToCoreData
{
    
    for (int k=0; k<arrOfSecondaryEmailAlreadySaved.count; k++) {
        
        [self deleteEmailIdFromCoreDataNew:arrOfSecondaryEmailAlreadySaved[k]];
        
    }
    
    for (int k=0; k<arrOfSecondaryEmailNew.count; k++) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        entityEmailDetailService= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        [request setEntity:entityEmailDetailService];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,arrOfSecondaryEmailNew[k]];
        
        [request setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [request setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerServiceEmail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerServiceEmail setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerServiceEmail performFetch:&error1];
        arrAllObj = [self.fetchedResultsControllerServiceEmail fetchedObjects];
        
        if (arrAllObj.count==0)
        {
            [self saveToCoreDataNewEmail :arrOfSecondaryEmailNew[k]];
            // [self saveToCoreDataNewEmailSecondary];
            
        }else
        {
            
            BOOL notMatched,notMatchedSecondaryEmail;
            
            notMatched=YES;
            notMatchedSecondaryEmail=YES;
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                NSString *strEmailToCompare=[matches valueForKey:@"emailId"];
                
                if ([strEmailToCompare isEqualToString:arrOfSecondaryEmailNew[k]]) {
                    
                    notMatched=NO;
                    
                    [matches setValue:arrOfSecondaryEmailNew[k] forKey:@"emailId"];
                    NSError *error1;
                    [context save:&error1];
                    
                }
            }
            
            if (notMatched) {
                
                [self saveToCoreDataNewEmail:arrOfSecondaryEmailNew[k]];
                
            }
        }
    }
}

-(void)saveToCoreDataNewEmail :(NSString*)strEmailTosave{
    
    if (strEmailTosave.length==0) {
        
    } else {
        
        entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        
        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailService insertIntoManagedObjectContext:context];
        
        objEmailDetail.createdBy=@"0";;
        objEmailDetail.createdDate=@"0";
        objEmailDetail.emailId=strEmailTosave;
        objEmailDetail.isCustomerEmail=@"true";
        objEmailDetail.isMailSent=@"true";
        objEmailDetail.companyKey=strCompanyKey;
        objEmailDetail.modifiedBy=@"";
        objEmailDetail.modifiedDate=[global modifyDate];
        objEmailDetail.subject=@"";
        objEmailDetail.userName=strUserName;
        objEmailDetail.workorderId=strWorkOrderId;
        objEmailDetail.woInvoiceMailId=@"";
        NSError *error1;
        [context save:&error1];
        
    }
}
-(void)saveToCoreDataNewEmailSecondary{
    
    if (_txt_SecondaryEmail.text.length>0) {
        
        entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        
        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailService insertIntoManagedObjectContext:context];
        
        objEmailDetail.createdBy=@"0";
        objEmailDetail.createdDate=@"0";
        objEmailDetail.emailId=_txt_SecondaryEmail.text;
        objEmailDetail.isCustomerEmail=@"true";
        objEmailDetail.isMailSent=@"true";
        objEmailDetail.companyKey=strCompanyKey;
        objEmailDetail.modifiedBy=@"";
        objEmailDetail.modifiedDate=[global modifyDate];
        objEmailDetail.subject=@"";
        objEmailDetail.userName=strUserName;
        objEmailDetail.workorderId=strWorkOrderId;
        objEmailDetail.woInvoiceMailId=@"";
        NSError *error1;
        [context save:&error1];
        
    }
}

-(void)saveToCoreDataEmailOfEmployee{
    entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    
    EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailService insertIntoManagedObjectContext:context];
    
    objEmailDetail.createdBy=@"0";;
    objEmailDetail.createdDate=@"0";
    objEmailDetail.emailId=strFromEmail;
    objEmailDetail.isCustomerEmail=@"true";
    objEmailDetail.isMailSent=@"true";
    objEmailDetail.companyKey=strCompanyKey;
    objEmailDetail.modifiedBy=@"";
    objEmailDetail.modifiedDate=[global modifyDate];
    objEmailDetail.subject=@"";
    objEmailDetail.userName=strUserName;
    objEmailDetail.workorderId=strWorkOrderId;
    objEmailDetail.woInvoiceMailId=@"";
    NSError *error1;
    [context save:&error1];
}

//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        //[global AlertMethod:Info :NoBeforeImg];
        _const_ImgView_H.constant=0;
        _const_SaveContinue_B.constant=162;
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        // [_imageCollectionView reloadData];
        
    }else if (arrOfImagesDetail.count==0){
        //[global AlertMethod:Info :NoBeforeImg];
        _const_ImgView_H.constant=0;
        _const_SaveContinue_B.constant=162;
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        // [_imageCollectionView reloadData];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        if (arrOfImagess.count==1) {
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:YES];
            [_buttonImg3 setHidden:YES];
        } else if (arrOfImagess.count==2){
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:NO];
            [_buttonImg3 setHidden:YES];
        }else{
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:NO];
            [_buttonImg3 setHidden:NO];
        }
        _const_ImgView_H.constant=100;
        _const_SaveContinue_B.constant=62;
        
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionView addObjectsFromArray:arrOfImagess];
        //[_imageCollectionView reloadData];
        
        [self downloadImages:arrOfImagess];
        
        //        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        //        dispatch_async(myQueue, ^{
        //
        //            [self downloadImages:arrOfImagess];
        //
        //            dispatch_async(dispatch_get_main_queue(), ^{
        //                // Update the UI
        //            });
        //        });
        
        [self ForSavenContinueView];
    }
    
    [self ForSavenContinueView];
    
}

-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImage:str:k];
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload1: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
        }
    }
    [_imageCollectionView reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    if (indexx==arrOfImagenameCollewctionView.count-1) {
        
        [_imageCollectionView reloadData];
        
    }
    
    [_imageCollectionView reloadData];
    
    //    [self ShowFirstImage : name : indexx];
    
}

//============================================================================
//============================================================================
#pragma mark -----------------------Load Image-------------------------------
//============================================================================
//============================================================================

- (UIImage*)loadImage :(NSString*)name :(int)indexxx {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    //Nilind
    
    /* UIFont *font = [UIFont boldSystemFontOfSize:12];
     UIGraphicsBeginImageContext(image.size);
     [image drawInRect:CGRectMake(0,150,100,50)];
     CGRect rect = CGRectMake(0,10, 100, 50);
     [[UIColor redColor] set];
     [name drawInRect:CGRectIntegral(rect) withFont:font];
     image = UIGraphicsGetImageFromCurrentImageContext();
     
     */
    //End
    if (indexxx==0) {
        
        [_buttonImg1 setImage:image forState:UIControlStateNormal];
        
    } else if(indexxx==1) {
        
        [_buttonImg2 setImage:image forState:UIControlStateNormal];
        
        
    }else{
        
        [_buttonImg3 setImage:image forState:UIControlStateNormal];
        
    }
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    
    
    
    
    
    return image;
}

//============================================================================
//============================================================================
#pragma mark -----------------------Action Button ImageView-------------------------------
//============================================================================
//============================================================================

- (IBAction)action_ButtonImg1:(id)sender {
    
    [self goingToPreview : @"0"];
    
}

- (IBAction)action_ButtonImg2:(id)sender {
    
    [self goingToPreview : @"1"];
    
}

- (IBAction)action_ButtonImg3:(id)sender {
    
    [self goingToPreview : @"2"];
    
}
-(void)goingToPreview :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ImagePreviewGeneralInfoAppointmentViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaption;
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescription;
        //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}

//============================================================================
#pragma mark- ------------Creating Crew View------------------
//============================================================================

-(void)createCrewView{
    
    if ([arrOfCrew isKindOfClass:[NSArray class]]) {
        
        int finalHeightOfCrewView=0.0,yAxisOfLabel=20;
        
        for (int k=0; k<arrOfCrew.count; k++) {
            
            NSDictionary *dictDataCrew=arrOfCrew[k];
            
            NSString *strSelected=[NSString stringWithFormat:@"%@",[dictDataCrew valueForKey:@"Selected"]];
            
            if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                
                if ([strSelected isEqualToString:@"1"] || [strSelected isEqualToString:@"true"]) {
                    
                    CGRect btnFrame = CGRectMake(0, yAxisOfLabel+4,25,25);
                    
                    CGSize s = [[dictDataCrew valueForKey:@"EmployeeName"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                    
                    if (s.height<30) {
                        s.height=30;
                    }
                    
                    finalHeightOfCrewView=finalHeightOfCrewView+s.height;
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(30, yAxisOfLabel, self.view.frame.size.width-35, s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:8];
                    // lbl.backgroundColor=[UIColor redColor];
                    lbl.text =[dictDataCrew valueForKey:@"EmployeeName"];
                    //Nilind 24 Feb
                    // [_crewView addSubview:lbl];
                    //
                    
                    yAxisOfLabel=yAxisOfLabel+s.height;
                    
                    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    btn.frame = btnFrame;
                    NSString *title = [dictDataCrew valueForKey:@"EmployeeName"];
                    btn.tag=k;
                    btn.titleLabel.textColor = [UIColor clearColor];
                    [btn setTitle:title forState:UIControlStateNormal];
                    
                    
                    if ([strSelected isEqualToString:@"1"] || [strSelected isEqualToString:@"true"] || [strSelected caseInsensitiveCompare:@"true"] == NSOrderedSame) {
                        
                        [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                        
                        NSString *strTagss=[NSString stringWithFormat:@"%ld",(long)btn.tag];
                        
                        [arrOfCrewTagSelected addObject:strTagss];
                        
                    } else {
                        
                        NSString *strTagss=[NSString stringWithFormat:@"%ld",(long)btn.tag];
                        
                        for (int l=0; l<arrOfCrewTagSelected.count; l++) {
                            
                            NSString *strTagToCheck=[NSString stringWithFormat:@"%@",arrOfCrewTagSelected[l]];
                            
                            if ([strTagss isEqualToString:strTagToCheck]) {
                                
                                [arrOfCrewTagSelected removeObjectAtIndex:l];
                                
                                break;
                                
                            }
                        }
                        
                        [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                        
                    }
                    
                    [btn addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                    
                    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                        [btn setEnabled:NO];
                    } else {
                        [btn setEnabled:YES];
                    }
                    
                    [arrOfCrewCheckBoxes addObject:btn];
                    [_crewView addSubview:btn];
                    
                    
                }
                
            } else {
                
                CGRect btnFrame = CGRectMake(0, yAxisOfLabel+4,25,25);
                
                CGSize s = [[dictDataCrew valueForKey:@"EmployeeName"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                
                if (s.height<30) {
                    s.height=30;
                }
                
                finalHeightOfCrewView=finalHeightOfCrewView+s.height;
                
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(30, yAxisOfLabel, self.view.frame.size.width-35, s.height)];
                [lbl setAdjustsFontSizeToFitWidth:YES];
                [lbl setNumberOfLines:8];
                // lbl.backgroundColor=[UIColor redColor];
                lbl.text =[dictDataCrew valueForKey:@"EmployeeName"];
                [_crewView addSubview:lbl];
                
                yAxisOfLabel=yAxisOfLabel+s.height;
                
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame = btnFrame;
                NSString *title = [dictDataCrew valueForKey:@"EmployeeName"];
                btn.tag=k;
                btn.titleLabel.textColor = [UIColor clearColor];
                [btn setTitle:title forState:UIControlStateNormal];
                
                
                if ([strSelected isEqualToString:@"1"] || [strSelected isEqualToString:@"true"] || [strSelected caseInsensitiveCompare:@"true"] == NSOrderedSame) {
                    
                    [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                    
                    NSString *strTagss=[NSString stringWithFormat:@"%ld",(long)btn.tag];
                    
                    [arrOfCrewTagSelected addObject:strTagss];
                    
                } else {
                    
                    NSString *strTagss=[NSString stringWithFormat:@"%ld",(long)btn.tag];
                    
                    for (int l=0; l<arrOfCrewTagSelected.count; l++) {
                        
                        NSString *strTagToCheck=[NSString stringWithFormat:@"%@",arrOfCrewTagSelected[l]];
                        
                        if ([strTagss isEqualToString:strTagToCheck]) {
                            
                            [arrOfCrewTagSelected removeObjectAtIndex:l];
                            
                            break;
                            
                        }
                    }
                    
                    [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                    
                }
                
                [btn addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                
                if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                    [btn setEnabled:NO];
                } else {
                    [btn setEnabled:YES];
                }
                
                [arrOfCrewCheckBoxes addObject:btn];
                [_crewView addSubview:btn];
                
            }
            
            
        }
        
#pragma mark- Nilind 24 Feb //temp comment
        
        _const_Crew_H.constant=arrOfCrew.count*10+finalHeightOfCrewView;//40
        
        
    }
    else{
        
        _const_Crew_H.constant=0;//40
        
        
        
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Check Box Button Methods--------------
//============================================================================
//============================================================================
-(void)checkCheckBoxes:(id)sender{
    
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
    
    UIButton *btn=(UIButton*)sender;
    
    int taggg=btn.tag;
    
    UIButton *btnCheckBox=arrOfCrewCheckBoxes[taggg];
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"uncheck.png"];
    
    UIImage *img = [btnCheckBox imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        NSString *strTagss=[NSString stringWithFormat:@"%ld",(long)btn.tag];
        
        [arrOfCrewTagSelected addObject:strTagss];
        
        [btnCheckBox setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
        NSDictionary *dictDataCrewList=arrOfCrew[btn.tag];
        
        NSArray *objValuearrResponseInspection=[NSArray arrayWithObjects:
                                                [dictDataCrewList valueForKey:@"EmployeeNo"],
                                                [dictDataCrewList valueForKey:@"EmployeeName"],
                                                @"true",nil];
        
        NSArray *objKeyarrResponseInspection=[NSArray arrayWithObjects:
                                              @"EmployeeNo",
                                              @"EmployeeName",
                                              @"Selected",nil];
        
        NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
        
        [arrOfCrew replaceObjectAtIndex:btn.tag withObject:dictOfarrResponseInspection];
        
    }
    else{
        NSDictionary *dictDataCrewList=arrOfCrew[btn.tag];
        
        NSArray *objValuearrResponseInspection=[NSArray arrayWithObjects:
                                                [dictDataCrewList valueForKey:@"EmployeeNo"],
                                                [dictDataCrewList valueForKey:@"EmployeeName"],
                                                @"false",nil];
        
        NSArray *objKeyarrResponseInspection=[NSArray arrayWithObjects:
                                              @"EmployeeNo",
                                              @"EmployeeName",
                                              @"Selected",nil];
        
        NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
        
        [arrOfCrew replaceObjectAtIndex:btn.tag withObject:dictOfarrResponseInspection];
        
        [btnCheckBox setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
        NSString *strTagss=[NSString stringWithFormat:@"%ld",(long)btn.tag];
        
        for (int l=0; l<arrOfCrewTagSelected.count; l++) {
            
            NSString *strTagToCheck=[NSString stringWithFormat:@"%@",arrOfCrewTagSelected[l]];
            
            if ([strTagss isEqualToString:strTagToCheck]) {
                
                [arrOfCrewTagSelected removeObjectAtIndex:l];
                
                break;
                
            }
        }
    }
    NSLog(@"arrOfCrewTagSelected are====%@",arrOfCrewTagSelected);
}


//============================================================================
//============================================================================
#pragma mark- ----------------------New Methods For Emails--------------------
//============================================================================
//============================================================================

- (IBAction)action_PrimaryEmail:(id)sender {
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
        
        NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
        
        if (strMessage.length==0) {
            
        } else {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@""
                                       message:strMessage
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
    }else{
        
        [self tablViewForPrimaryEmailIds];
        
    }
    
}
- (IBAction)action_SecondaryEmail:(id)sender {
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
        
        NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
        if (strMessage.length==0) {
            
        } else {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@""
                                       message:strMessage
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
    }else{
        
        [self tablViewForSecondaryEmailIds];
        
    }
    
}

-(void)alertForPrimaryEmailSaving{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Add Email-Id"
                                                                              message: strMessage
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Email-Id Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    //    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    //        textField.placeholder = @"password";
    //        textField.textColor = [UIColor blueColor];
    //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    //        textField.borderStyle = UITextBorderStyleRoundedRect;
    //        textField.secureTextEntry = YES;
    //    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        if (txtHistoricalDays.text.length>0) {
            
            txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
            {
                
                [self performSelector:@selector(AlertViewForNonValidPrimaryEmail) withObject:nil afterDelay:0.2];
                
            }else if ([txtHistoricalDays.text containsString:@" "])
            {
                
                [self performSelector:@selector(AlertViewForNonValidPrimaryEmail) withObject:nil afterDelay:0.2];
                
            }else{
                
                if ([arrOfPrimaryEmailNew containsObject:txtHistoricalDays.text]) {
                    
                    [self performSelector:@selector(AlertViewForAlreadyExitPrimaryEmail) withObject:nil afterDelay:0.2];
                    
                } else {
                    
                    [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
                    isNoEmail=NO;
                    [arrOfPrimaryEmailNew addObject:txtHistoricalDays.text];
                    
                    [arrOfPrimaryEmailAlreadySaved addObject:txtHistoricalDays.text];
                    
                    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
                    
                    [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
                    
                    [self tablViewForPrimaryEmailIds];
                    
                }
            }
            
        } else {
            
            [self performSelector:@selector(AlertViewForEmptyPrimaryEmail) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self tablViewForPrimaryEmailIds];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)AlertViewForEmptyPrimaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter to add Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForPrimaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForAlreadyExitPrimaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Email-Id Already Present. Please provide another one"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForPrimaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForEmptyEmailEdit{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter to edit Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)AlertViewForNonValidPrimaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter valid Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForPrimaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForNonValidEmailOnEdit{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter valid Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForAlreadyEmailPresentOnEdit{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Email-Id Already Present. Please provide another one"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)AlertViewForEmptySecondaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter to add Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForSecondaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForNonValidSecondaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter valid Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForSecondaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForAlreadyPresentSecondaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Emai-Id Already Saved. Please provide another one"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForSecondaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)alertForSecondaryEmailSaving{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Add Email-Id"
                                                                              message: strMessage
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Email-Id Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    //    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    //        textField.placeholder = @"password";
    //        textField.textColor = [UIColor blueColor];
    //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    //        textField.borderStyle = UITextBorderStyleRoundedRect;
    //        textField.secureTextEntry = YES;
    //    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        if (txtHistoricalDays.text.length>0) {
            
            txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
            {
                
                [self performSelector:@selector(AlertViewForNonValidSecondaryEmail) withObject:nil afterDelay:0.2];
                
            }else if ([txtHistoricalDays.text containsString:@" "])
            {
                
                [self performSelector:@selector(AlertViewForNonValidSecondaryEmail) withObject:nil afterDelay:0.2];
                
            }else{
                
                if ([arrOfSecondaryEmailNew containsObject:txtHistoricalDays.text]) {
                    
                    [self performSelector:@selector(AlertViewForAlreadyPresentSecondaryEmail) withObject:nil afterDelay:0.2];
                    
                    
                } else {
                    
                    [arrOfSecondaryEmailNew addObject:txtHistoricalDays.text];
                    
                    [arrOfSecondaryEmailAlreadySaved addObject:txtHistoricalDays.text];
                    
                    NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
                    
                    [_btnSecondaryEmail setTitle:strMessage forState:UIControlStateNormal];
                    
                    [self tablViewForSecondaryEmailIds];
                    
                }
            }
        } else {
            
            [self performSelector:@selector(AlertViewForEmptySecondaryEmail) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self tablViewForSecondaryEmailIds];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)tablViewForPrimaryEmailIds{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    [arrDataTblView addObjectsFromArray:arrOfPrimaryEmailNew];
    
    if (arrDataTblView.count==0) {
        //[global AlertMethod:Info :NoDataAvailableReset];
        //[self alertForPrimaryEmailSaving];
        
        tblData.tag=105;
        [self tableLoad:tblData.tag];
        
    }else{
        tblData.tag=105;
        [self tableLoad:tblData.tag];
    }
    
}
-(void)tablViewForSecondaryEmailIds{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    [arrDataTblView addObjectsFromArray:arrOfSecondaryEmailNew];
    
    if (arrDataTblView.count==0) {
        //[global AlertMethod:Info :NoDataAvailableReset];
        //[self alertForSecondaryEmailSaving];
        tblData.tag=106;
        [self tableLoad:tblData.tag];
        
    }else{
        tblData.tag=106;
        [self tableLoad:tblData.tag];
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //Graph
    if (collectionView==_collectionViewGraph) {
        
        return arrOfImagenameCollewctionViewGraph.count;
        
    } else {
        
        return arrOfImagenameCollewctionView.count;
        
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView==_imageCollectionView) {
        
        
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        //    cell.selected=YES;
        //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        
        NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
        
    } else {
        
        //Graph
        static NSString *identifier = @"ServiceGraphCollectionViewCell";
        
        ServiceGraphCollectionViewCell *cell = (ServiceGraphCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        //    cell.selected=YES;
        //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        NSString *str;
        if ([[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictDataa=[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
            str = [dictDataa valueForKey:@"woImagePath"];
            
        } else {
            
            str = [arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
        }
        
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        return cell;
        
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView==_collectionViewGraph) {
        
        //Graph
        NSString *str;
        if ([[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictDataa=[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
            str = [dictDataa valueForKey:@"woImagePath"];
            
        } else {
            
            str = [arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewGraph : strIndex];
            
        }
        
    } else {
        
        
        NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreview : strIndex];
            
        }
        
    }
}
//Graph
-(void)deleteGraphImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"Graph"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

//Graph
-(void)fetchGraphImageDetailFromDataBase
{
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    arrOfBeforeImageAllGraph=nil;
    arrOfBeforeImageAllGraph=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=nil;
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=nil;
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    
    arrOfBeforeImageAllGraph=nil;
    arrOfBeforeImageAllGraph=[[NSMutableArray alloc]init];
    
    arrOfImagenameCollewctionViewGraph=nil;
    arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    NSArray *arrAllObjImageDetailNew = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetailNew count] == 0)
    {
        
        
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetailNew.count; j++) {
            
            NSManagedObject *matchesImageDetailNew=arrAllObjImageDetailNew[j];
            NSString *woImageType=[matchesImageDetailNew valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"Graph"]) {
                
                NSString *companyKey=[matchesImageDetailNew valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetailNew valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetailNew valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetailNew valueForKey:@"woImageId"];
                NSString *woImagePath=[matchesImageDetailNew valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetailNew valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetailNew valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetailNew valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetailNew valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetailNew valueForKey:@"modifiedBy"];
                NSString *lattitude=[matchesImageDetailNew valueForKey:@"latitude"];
                NSString *longitude=[matchesImageDetailNew valueForKey:@"longitude"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,
                                           lattitude.length==0 ? @"" : lattitude,
                                           longitude.length==0 ? @"" : longitude,nil];
                
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",
                                         @"latitude",
                                         @"longitude",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrOfBeforeImageAllGraph addObject:dict_ToSendLeadInfo];
                [arrOfImagenameCollewctionViewGraph addObject:dict_ToSendLeadInfo];
                
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                    
                } else
                {
                    
                    [arrOfImageCaptionGraph addObject:strImageCaption];
                    
                }
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionGraph addObject:strImageDescription];
                    
                }
                
                //Lat long
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLongitude addObject:strLong];
                    
                }
                
                
                
                
            }
        }
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


//Graph
-(void)downloadGraphImage
{
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    NSArray *arrOfImagesDetail=arrOfBeforeImageAllGraph;
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        arrOfImagenameCollewctionViewGraph=nil;
        arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
        
    }else if (arrOfImagesDetail.count==0){
        arrOfImagenameCollewctionViewGraph=nil;
        arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        arrOfImagenameCollewctionViewGraph=nil;
        arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionViewGraph addObjectsFromArray:arrOfImagess];
        
        [self downloadImagesGraph:arrOfImagess];
        
    }
}

//Graph
//For Graph Download Code
//============================================================================
//============================================================================
#pragma mark -----------------------Download Graph Images-------------------------------
//============================================================================
//============================================================================

-(void)downloadImagesGraph :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImageGraph:str:k];
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload1Graph: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
        }
    }
    [_collectionViewGraph reloadData];
}

-(void)ShowFirstImageGraph :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1Graph: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    if (indexx==arrOfImagenameCollewctionViewGraph.count-1) {
        
        [_collectionViewGraph reloadData];
        
    }
    
    [_collectionViewGraph reloadData];
    
    //    [self ShowFirstImage : name : indexx];
    
}

//Graph
-(void)goingToPreviewGraph :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAllGraph;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        GraphImagePreviewViewControlleriPad
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImagePreviewViewControlleriPad"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.strLeadId=strWorkOrderId;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaptionGraph;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescriptionGraph;
        objImagePreviewSalesAuto.strFromWhere=@"Service";
        objImagePreviewSalesAuto.statusOfWorkOrder=[NSString stringWithFormat:@"%@",strGlobalWorkOrderStatus];
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}

//Graph
- (IBAction)action_GraphImage:(id)sender
{
    
    _btnUploadServiceAddImage.tag=123456;
    
    [_collectionViewGraph reloadData];
    
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewFooter.frame.origin.y-_viewGraphImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewGraphImage.frame.size.height);
    [_viewGraphImage setFrame:frameFor_view_BeforeImageInfo];
    [self.view addSubview:_viewGraphImage];
    
}
//Graph
- (IBAction)action_AddGraph:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"firstGraphImage"];
    [defs setBool:YES forKey:@"servicegraph"];
    [defs setBool:YES forKey:@"isForNewGraph"];// Akshay 14 May 2019
    [defs synchronize];
    
    // commented by Akshay 14 may 2019
    /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
     EditGraphViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewControlleriPad"];
     objSignViewController.strLeadId=strWorkOrderId;
     objSignViewController.strCompanyKey=strCompanyKey;
     objSignViewController.strUserName=strUserName;
     [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
    
    // Akshay 14 may 2019
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
    objSignViewController.strLeadId=[matchesWorkOrder valueForKey:@"workorderId"];
    objSignViewController.strCompanyKey=strCompanyKey;
    objSignViewController.strUserName=strUserName;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    //////////////////
    
}
//Graph
- (IBAction)action_CloseGraphView:(id)sender {
    
    [_viewGraphImage removeFromSuperview];
    
}
#pragma mark- ------------ CODE FOR SERVICE ADD IMAGE ----------------



- (IBAction)actionOnUploadServiceAddImage:(id)sender
{
    
    _btnUploadServiceAddImage.tag=101;
    
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                                  
                                  
                              }
                              else
                              {
                                  
                                  NSLog(@"The CApture Image.");
                                  
                                  NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                  
                                  BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                                  
                                  if (isfirstTimeAudio) {
                                      
                                      [defs setBool:NO forKey:@"firstCamera"];
                                      [defs synchronize];
                                      
                                      UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                      imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                      imagePickController.delegate=(id)self;
                                      imagePickController.allowsEditing=TRUE;
                                      [self presentViewController:imagePickController animated:YES completion:nil];
                                      
                                  }
                                  else
                                  {
                                      BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                      
                                      if (isCameraPermissionAvailable) {
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                          
                                      }else{
                                          
                                          UIAlertController *alert= [UIAlertController
                                                                     alertControllerWithTitle:@"Alert"
                                                                     message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                          
                                          UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                      handler:^(UIAlertAction * action)
                                                                {
                                                                    
                                                                    
                                                                    
                                                                }];
                                          [alert addAction:yes];
                                          UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                     handler:^(UIAlertAction * action)
                                                               {
                                                                   
                                                                   if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                       NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                       [[UIApplication sharedApplication] openURL:url];
                                                                   } else {
                                                                       
                                                                       //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                       //                                     [alert show];
                                                                       
                                                                   }
                                                                   
                                                               }];
                                          [alert addAction:no];
                                          [self presentViewController:alert animated:YES completion:nil];
                                      }
                                  }
                                  
                              }
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                                 
                                 
                             }
                             else
                             {
                                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                 
                                 BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                                 
                                 if (isfirstTimeAudio) {
                                     
                                     [defs setBool:NO forKey:@"firstGallery"];
                                     [defs synchronize];
                                     
                                     UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                     imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                     imagePickController.delegate=(id)self;
                                     imagePickController.allowsEditing=TRUE;
                                     [self presentViewController:imagePickController animated:YES completion:nil];
                                     
                                 }else{
                                     BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                     
                                     if (isCameraPermissionAvailable) {
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                         
                                     }else{
                                         
                                         UIAlertController *alert= [UIAlertController
                                                                    alertControllerWithTitle:@"Alert"
                                                                    message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                         
                                         UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                     handler:^(UIAlertAction * action)
                                                               {
                                                                   
                                                                   
                                                                   
                                                               }];
                                         [alert addAction:yes];
                                         UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                    handler:^(UIAlertAction * action)
                                                              {
                                                                  
                                                                  if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                      NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                      [[UIApplication sharedApplication] openURL:url];
                                                                  } else {
                                                                      
                                                                      //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                      //                                     [alert show];
                                                                      
                                                                  }
                                                                  
                                                              }];
                                         [alert addAction:no];
                                         [self presentViewController:alert animated:YES completion:nil];
                                     }
                                 }
                                 
                             }
                             
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)downloadImageServiceAddImage :(NSString*)str
{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
    NSUserDefaults *defsAddImage=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDataAddImage=[defsAddImage valueForKey:@"LoginDetails"];
    NSString *strServiceAddImageUrl=[dictLoginDataAddImage valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
    NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/%@",strServiceAddImageUrl,str];
    NSLog(@"Service Add Image URL %@",strUrl);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageServiceAdd:str];
    } else {
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownloadServiceAdd: image : result];
                
            });
        });
    }
    
}
-(void)ShowFirstImageServiceAdd :(NSString*)str
{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImageServiceAdd : result];
}
- (void)saveImageAfterDownloadServiceAdd: (UIImage*)image :(NSString*)name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    [self ShowFirstImageServiceAdd : name];
    
}
- (UIImage*)loadImageServiceAdd :(NSString*)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (image==nil) {
        
        _imgViewServiceAddress.image = [UIImage imageNamed:@"NoImage.jpg"];
        
    } else {
        
        _imgViewServiceAddress.image=image;
    }
    return image;
}
- (IBAction)actionOnNotesHistory:(id)sender
{
    //Nilind
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
//                                                             bundle: nil];
//    ServiceNotesHistoryViewController
//    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
//    objByProductVC.strTypeOfService=@"service";
//    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    //End
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceNotesHistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
    objByProductVC.strWoId= strWorkOrderId;
    NSString *strWoNos=[matchesWorkOrder valueForKey:@"workOrderNo"];
    objByProductVC.strWorkOrderNo= strWoNos;
    objByProductVC.isFromWDO = @"YES";
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ClockInOutViewControlleriPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewControlleriPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}


// change to select POC

-(void)fetchServiceAddressPOCDetailDcsFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalServiceAddressPOCDetailDcs=[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context];
    requestMechanicalServiceAddressPOCDetailDcs = [[NSFetchRequest alloc] init];
    [requestMechanicalServiceAddressPOCDetailDcs setEntity:entityMechanicalServiceAddressPOCDetailDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestMechanicalServiceAddressPOCDetailDcs setPredicate:predicate];
    
    sortDescriptorMechanicalServiceAddressPOCDetailDcs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalServiceAddressPOCDetailDcs = [NSArray arrayWithObject:sortDescriptorMechanicalServiceAddressPOCDetailDcs];
    
    [requestMechanicalServiceAddressPOCDetailDcs setSortDescriptors:sortDescriptorsMechanicalServiceAddressPOCDetailDcs];
    
    self.fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalServiceAddressPOCDetailDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs performFetch:&error];
    arrAllObjMechanicalServiceAddressPOCDetailDcs = [self.fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs fetchedObjects];
    if ([arrAllObjMechanicalServiceAddressPOCDetailDcs count] == 0)
    {
        
        
        
    }
    else
    {
        
        NSManagedObject *obj=arrAllObjMechanicalServiceAddressPOCDetailDcs[0];
        
        arrAllObjMechanicalServiceAddressPOCDetailDcs=[obj valueForKey:@"pocDetails"];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)fetchBillingAddressPOCDetailDcsFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalBillingAddressPOCDetailDcs=[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context];
    requestMechanicalBillingAddressPOCDetailDcs = [[NSFetchRequest alloc] init];
    [requestMechanicalBillingAddressPOCDetailDcs setEntity:entityMechanicalBillingAddressPOCDetailDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestMechanicalBillingAddressPOCDetailDcs setPredicate:predicate];
    
    sortDescriptorMechanicalBillingAddressPOCDetailDcs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalBillingAddressPOCDetailDcs = [NSArray arrayWithObject:sortDescriptorMechanicalBillingAddressPOCDetailDcs];
    
    [requestMechanicalBillingAddressPOCDetailDcs setSortDescriptors:sortDescriptorsMechanicalBillingAddressPOCDetailDcs];
    
    self.fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalBillingAddressPOCDetailDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs performFetch:&error];
    arrAllObjMechanicalBillingAddressPOCDetailDcs = [self.fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs fetchedObjects];
    if ([arrAllObjMechanicalBillingAddressPOCDetailDcs count] == 0)
    {
        
        
        
    }
    else
    {
        
        NSManagedObject *obj=arrAllObjMechanicalBillingAddressPOCDetailDcs[0];
        
        arrAllObjMechanicalBillingAddressPOCDetailDcs=[obj valueForKey:@"pocDetails"];
        
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}


- (IBAction)action_ServiceAddressPOCDetailDcs:(id)sender{
    
    if (arrAllObjMechanicalServiceAddressPOCDetailDcs.count==0) {
        
        [global AlertMethod:Alert :NoPOCAvailableee];
        
    }else{
        
        tblData.tag=108;
        [self tableLoad:108];
        
    }
    
}
- (IBAction)action_BillingAddressPOCDetailDcs:(id)sender{
    
    if (arrAllObjMechanicalBillingAddressPOCDetailDcs.count==0) {
        
        [global AlertMethod:Alert :NoPOCAvailableee];
        
    }else{
        
        tblData.tag=109;
        [self tableLoad:109];
        
    }
    
}

@end


