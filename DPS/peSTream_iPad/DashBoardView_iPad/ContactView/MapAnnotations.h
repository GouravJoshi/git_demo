//
//  MapAnnotations.h
//  CaptureTheirFlag
//
//  Created by Rakesh jain on 12/08/15.
//  Copyright (c) 2015 Sankalp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnnotations : NSObject  <MKAnnotation> {
    CLLocationCoordinate2D coordinate;
    NSUInteger tag;
    NSString *title;
    NSString *subtitle;
}

//coordinate property must be readonly
@property(nonatomic,readonly) CLLocationCoordinate2D coordinate;
@property(nonatomic) NSUInteger tag;
@property(nonatomic) NSUInteger countVal;

//@property(nonatomic,retain) NSString *title;
//@property(nonatomic,retain) NSString *subtitle;

//programmer provided init function to create the annotation objects
-(id)initWithCoordinate:(CLLocationCoordinate2D)c withTag:(NSUInteger)t withTitle:(NSString *)tl withSubtitle: (NSString *)s withCount: (NSInteger) count;


@end
