//
//  MapAnnotations.m
//  CaptureTheirFlag
//
//  Created by Rakesh jain on 12/08/15.
//  Copyright (c) 2015 Sankalp. All rights reserved.
//

#import "MapAnnotations.h"

@implementation MapAnnotations
@synthesize coordinate;
@synthesize tag;
@synthesize title;
@synthesize subtitle;
@synthesize countVal;


-(id)initWithCoordinate:(CLLocationCoordinate2D)c withTag:(NSUInteger)t withTitle:(NSString *)tl withSubtitle:	(NSString *)s withCount:(NSInteger)count
{
    if(self = [super init])
    {
        coordinate = c;
        tag = t;
        title = tl;
        subtitle = s;
        countVal = count;
    }
    return self;
    
}

@end
