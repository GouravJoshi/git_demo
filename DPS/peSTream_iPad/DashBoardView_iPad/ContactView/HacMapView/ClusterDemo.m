//
//  ClusterDemo.m
//  DPS
//
//  Created by Rakesh Jain on 08/02/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import "ClusterDemo.h"
#import "Global.h"
@interface ClusterDemo ()
{
    Global *global;
}
@end

@implementation ClusterDemo

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.mapView.mapDelegate = self;
    global=[[Global alloc]init];
      /*  NSArray *data = @[
                          @{kLatitude:@48.47352, kLongitude:@3.87426,  kTitle : @"Title 1", kSubtitle : @"",            kIndex : @0},
                          @{kLatitude:@52.59758, kLongitude:@-1.93061, kTitle : @"Title 2", kSubtitle : @"Subtitle 2",  kIndex : @1},
                          @{kLatitude:@48.41370, kLongitude:@3.43531,  kTitle : @"Title 3", kSubtitle : @"Subtitle 3",  kIndex : @2},
                          @{kLatitude:@48.31921, kLongitude:@18.10184, kTitle : @"Title 4", kSubtitle : @"Subtitle 4",  kIndex : @3},
                          @{kLatitude:@47.84302, kLongitude:@22.81101, kTitle : @"Title 5", kSubtitle : @"Subtitle 5",  kIndex : @4},
                          @{kLatitude:@60.88622, kLongitude:@26.83792, kTitle : @"Title 6", kSubtitle : @""          ,  kIndex : @5}
                          ];*/
    NSArray *data = @[
                      @{kLatitude:@22.7196, kLongitude:@75.8577,  kTitle : @"Title 1", kSubtitle : @"",            kIndex : @0},
                      @{kLatitude:@23.2599, kLongitude:@77.4126, kTitle : @"Title 2", kSubtitle : @"Subtitle 2",  kIndex : @1},
                      @{kLatitude:@21.1458, kLongitude:@79.0882,  kTitle : @"Title 3", kSubtitle : @"Subtitle 3",  kIndex : @2},
                      @{kLatitude:@26.9124, kLongitude:@75.7873, kTitle : @"Title 4", kSubtitle : @"Subtitle 4",  kIndex : @3},
                      @{kLatitude:@19.0760, kLongitude:@72.8777, kTitle : @"Title 5", kSubtitle : @"Subtitle 5",  kIndex : @4},
                      @{kLatitude:@11.4064, kLongitude:@76.6932, kTitle : @"Title 6", kSubtitle : @""          ,  kIndex : @5},
                      @{kLatitude:@44.466995, kLongitude:@-73.170959, kTitle : @"Title 6", kSubtitle : @""          ,  kIndex : @6},
                       @{kLatitude:@27.498928, kLongitude:@-82.574821, kTitle : @"Title 6", kSubtitle : @""          ,  kIndex : @7},
                       @{kLatitude:@40.728157, kLongitude:@-74.077644, kTitle : @"Title 6", kSubtitle : @""          ,  kIndex : @8},
                        @{kLatitude:@33.787914, kLongitude:@-117.853104, kTitle : @"Title 6", kSubtitle : @""          ,  kIndex : @9},
                        @{kLatitude:@43.073051, kLongitude:@-89.401230, kTitle : @"Title 6", kSubtitle : @""          ,  kIndex : @10}
                      ];
    
        //[self.mapView.coordinateQuadTree buildTreeWithArray:data];
        [self.mapView.coordinateQuadTree buildTreeWithExample];
       // self.mapView.backgroundAnnotation = [UIColor redColor];
        self.mapView.borderAnnotation = [UIColor whiteColor];
        self.mapView.textAnnotation = [UIColor whiteColor];
    
    
    
    
    
//[self.mapView.coordinateQuadTree buildTreeWithExample];
    [self.mapView.coordinateQuadTree buildTreeWithArray:data];

    self.mapView.compassFrame = CGRectMake(10, 10, 25, 25);
    self.mapView.legalFrame = CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-50, CGRectGetHeight([UIScreen mainScreen].bounds)-50, 50, 50);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
# pragma mark - HACMKMapViewDelegate

-(void)viewForAnnotationView:(HAClusterAnnotationView *)annotationView annotation:(HAClusterAnnotation *)annotation
{
    if (annotation.index % 2 == 0) {
        if (annotation.index == 2) {
            annotationView.canShowCallout = NO;
        }
        //@"banana.png"
//@"orange.png"
        //annotationView.image = [UIImage imageNamed:@"banana.png"];
      // annotationView.image = [UIImage imageNamed:@"orange.png"];

    }else{
        //annotationView.image = [UIImage imageNamed:@"contactMap.png"];
        //annotationView.image = [UIImage imageNamed:@"banana.png"];
    }
    annotationView.canShowCallout=NO;
    //annotationView.image = [UIImage imageNamed:@"banana.png"];
    
    HAClusterAnnotation *clusterAnnotation = (HAClusterAnnotation *)annotation;

    UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(annotationView.frame.origin.x-10, annotationView.frame.origin.y-10, 40, 40)];
    //lbl.center=pinView.center;
    [lbl setCenter:annotationView.center];
    lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)clusterAnnotation.count];
    lbl.textColor = [UIColor redColor];
    lbl.layer.masksToBounds = YES;
    lbl.layer.cornerRadius = lbl.frame.size.width/2;
    [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.backgroundColor = [UIColor purpleColor];

    [lbl removeFromSuperview];
  //  [self.mapView addSubview:lbl];
   // [annotationView bringSubviewToFront:lbl];
    
}
-(void)viewForAnnotationView:(HAClusterAnnotationView *)annotationView clusteredAnnotation:(HAClusterAnnotation *)annotation
{
    annotationView.canShowCallout=NO;
    
}
-(void)didSelectAnnotationView:(HAClusterAnnotation *)annotationView{
    NSLog(@"You ara select annotation index %ld", (long)annotationView.index);
    [global AlertMethod:@"Demo" :@"Alert"];
    // [self.mapView mapView:self.mapView didSelectAnnotationView:annotationView];
     //[self.mapView updateMapViewAnnotationsWithAnnotations:annotations];
   
}

- (IBAction)actionOnBack:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
