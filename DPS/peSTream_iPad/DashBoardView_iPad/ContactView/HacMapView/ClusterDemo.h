//
//  ClusterDemo.h
//  DPS
//
//  Created by Rakesh Jain on 08/02/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HACMKMapView.h"

@interface ClusterDemo : UIViewController<HACMKMapViewDelegate>
@property (weak, nonatomic) IBOutlet HACMKMapView *mapView;
- (IBAction)actionOnBack:(id)sender;

@end
