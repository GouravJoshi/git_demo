//
//  NotesViewController.m
//  DPS
//
//  Created by Saavan Patidar on 28/09/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "NotesViewController.h"
#import "AllImportsViewController.h"
#import "tblCell.h"

@interface NotesViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    Global *global;
    NSMutableArray *arrOfNotesList;
    NSString *strEmpID,*strCompanyKey,*strCompanyId,*strServiceUrlMain,*strUserName,*strStateIdSelected;
    UIRefreshControl *refreshControl;

}

@end

@implementation NotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Initialize the refresh control.
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor lightTextColor];
    refreshControl.tintColor = [UIColor themeColor];
    [refreshControl addTarget:self
                       action:@selector(reloadTableData)
             forControlEvents:UIControlEventValueChanged];
    NSString *title = [NSString stringWithFormat:@"Pull to refresh"];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    refreshControl.attributedTitle = attributedTitle;
    [_tblViewNotes addSubview:refreshControl];
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
    
    arrOfNotesList = [[NSMutableArray alloc]init];
    
    _txtView_Notes.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_Notes.layer.borderWidth=1.0;
    _txtView_Notes.layer.cornerRadius=5.0;
    
    
    _tblViewNotes.rowHeight=UITableViewAutomaticDimension;
    _tblViewNotes.estimatedRowHeight=200;
    _tblViewNotes.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    
    _txtView_Notes.text = @"";
    
    [self fetchNotesHistory];

    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:singleTapGestureRecognizer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)singleTap:(UITapGestureRecognizer *)gesture {
    [_txtView_Notes resignFirstResponder];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [_txtView_Notes resignFirstResponder];
}
#pragma mark-  ------------- Tableview Delegates methods -------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return arrOfNotesList.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    tblCell *cell = (tblCell *)[tableView dequeueReusableCellWithIdentifier:@"NotesContactsCell" forIndexPath:indexPath];
    
    NSDictionary *dictNotesData = arrOfNotesList[indexPath.row];
    
    cell.lblNotes.text=[NSString stringWithFormat:@"%@",[dictNotesData valueForKey:@"Note"]];
    cell.lblNotesCreatedBy.text=[NSString stringWithFormat:@"%@",[dictNotesData valueForKey:@"CreatedByName"]];
   // cell.lblNotesCreatedDate.text=[NSString stringWithFormat:@"%@",[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:[NSString stringWithFormat:@"%@",[dictNotesData valueForKey:@"CreatedDate"]]]];
      cell.lblNotesCreatedDate.text=[NSString stringWithFormat:@"%@",[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:[NSString stringWithFormat:@"%@",[dictNotesData valueForKey:@"ClientCreatedDate"]]]];
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = tableView.frame;
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-20, 50)];
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    NSString *sectionName;
    
    sectionName = @"Notes History";
    
    title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-20, 50)];
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    title.text = sectionName;
    title.backgroundColor=[UIColor clearColor];
    title.textColor=[UIColor blackColor];
    title.font=[UIFont boldSystemFontOfSize:20];
    
    headerView.backgroundColor=[UIColor colorWithRed:232.0f/255.0f green:232.0f/255.0f blue:232.0f/255.0f alpha:1.0f];
    [headerView addSubview:title];
    
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 50;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 5;
    
}

#pragma mark-  ------------- Button Action methods -------------

- (IBAction)action_Back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)action_AddNotes:(id)sender {
    
    [_txtView_Notes resignFirstResponder];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [global displayAlertController:Alert :ErrorInternetMsg :self];
        
    }else{
     
        if (_txtView_Notes.text.length>0) {
            
            [self addNotes];
            
        } else {
            
            [global displayAlertController:Alert :@"Please enter note to add" :self];
            
        }
        
    }
}


#pragma mark-  ------------- TextView Delegates methods -------------

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)texth
{
    if (range.location == 0 && [texth isEqualToString:@" "]) {
        return NO;
    }
    else
        return YES;
    
}


- (void)reloadTableData
{
    
    [refreshControl endRefreshing];
    [self fetchNotesHistory];
    
}
-(void)fetchNotesHistory{
    
    [_tblViewNotes setHidden:YES];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [global displayAlertController:Alert :ErrorInternetMsg :self];
        
    }else{
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",strServiceUrlMain,UrlGetNotes,_strLeadNumber,UrlGetNotesNo,_strOpportunityNumber,UrlGetContactDetailByContactIdCompanykey,strCompanyKey];
        
        //strUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",strServiceUrlMain,UrlGetNotes,_strLeadNumber,UrlGetNotesNo,_strOpportunityNumber,UrlGetContactDetailByContactIdCompanykey,strCompanyKey];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Notes..."];
        
        //============================================================================
        //============================================================================
        
        NSString *strType=@"getLeadOrOpportunity";
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         if (response.count==0) {
                             
                             [global displayAlertController:Alert :NoNotesDataAvailableee :self];
                             
                         } else {
                             
                             NSArray *arrTempKeys = (NSArray*)response;
                             
                             if (arrTempKeys.count>0) {
                                 
                                 [_tblViewNotes setHidden:NO];
                                 
                                 arrTempKeys = [[arrTempKeys reverseObjectEnumerator] allObjects];
                                 
                                 arrOfNotesList = [[NSMutableArray alloc]init];
                                 
                                 [arrOfNotesList addObjectsFromArray:arrTempKeys];

                                 [_tblViewNotes reloadData];
                                 
                             } else {
                                 
                                 [global displayAlertController:Alert :NoNotesDataAvailableee :self];
                                 [DejalBezelActivityView removeView];
                                 
                             }
                             
                         }
                     }
                     else
                     {
                         [global displayAlertController:Alert :NoNotesDataAvailableee :self];
                     }
                 });
             }];
        });
    }
}


static NSString * extracted() {
    return UrlAddNotes;
}

-(void)addNotes{
    
    NSArray *objects1 = [NSArray arrayWithObjects:
                         strCompanyKey,
                         _strLeadNumber,
                         ([_txtView_Notes.text isEqualToString:@""]) ? @"" : _txtView_Notes.text,
                         _strOpportunityNumber,
                         strEmpID,nil];
    
    NSArray *keys1 = [NSArray arrayWithObjects:
                      @"CompanyKey",
                      @"LeadNumber",
                      @"Note",
                      @"OpportunityNumber",
                      @"CreatedBy",nil];
    
    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects1 forKeys:keys1];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Adding Notes List Json: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,extracted()];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Adding Notes..."];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"POST"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    [requestLocal setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [requestLocal setHTTPBody:requestData];
    
    @try {
        
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             //        [[NSURLSession sharedSession] dataTaskWithRequest:requestLocal completionHandler:^(NSData *data,NSURLResponse *response,NSError *error)
             //         {
             NSData* jsonData = [NSData dataWithData:data];
             
             NSString *responseString1 = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
             
             if (responseString1.length==0) {
                 
                 [DejalBezelActivityView removeView];
                 [global displayAlertController:Alert :Sorry :self];
                 
             } else {
                 
                 [global displayAlertController:Alert :@"Notes added successfully" :self];
                 
                 NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                 [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                 NSString *strCurrentDate = [dateFormat stringFromDate:[NSDate date]];
                 
                 NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                 NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                 NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];

                 NSArray *objects1 = [NSArray arrayWithObjects:
                                      strCompanyKey,
                                      _strLeadNumber,
                                      ([_txtView_Notes.text isEqualToString:@""]) ? @"" : _txtView_Notes.text,
                                      _strOpportunityNumber,
                                      strEmpID,
                                      strEmployeeName,
                                      @"",
                                      @"",
                                      @"",
                                      @"",
                                      strCurrentDate,
                                      strCurrentDate,nil];
                 
                 NSArray *keys1 = [NSArray arrayWithObjects:
                                   @"CompanyKey",
                                   @"LeadNumber",
                                   @"Note",
                                   @"OpportunityNumber",
                                   @"CreatedBy",
                                   @"CreatedByName",
                                   @"LeadId",
                                   @"LeadNoteId",
                                   @"RefId",
                                   @"RefType",
                                   @"CreatedDate",
                                   @"ClientCreatedDate",nil];
                 
                 NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects1 forKeys:keys1];
                 
                 [_tblViewNotes setHidden:NO];
                 
                 //arrOfNotesList = [[NSMutableArray alloc]init];
                 
                 [arrOfNotesList addObject:dict_ToSend];
                 
                 NSArray *tempArray = (NSArray*)arrOfNotesList;
                 
                 tempArray = [[tempArray reverseObjectEnumerator] allObjects];
                 
                 arrOfNotesList = [[NSMutableArray alloc]init];
                 
                 [arrOfNotesList addObjectsFromArray:tempArray];
                 
                 [_tblViewNotes reloadData];
                 
                 _txtView_Notes.text=@"";

                // [self fetchNotesHistory];
                 
             }
             
             [DejalBezelActivityView removeView];
             
         }];
    }
    @catch (NSException *exception) {
        
        [global displayAlertController:Alert :Sorry :self];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }
}

@end
