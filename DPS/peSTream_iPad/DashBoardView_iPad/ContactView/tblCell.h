//
//  tblCell.h
//  lead
//
//  Created by Navin Patidar on 9/21/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

@interface tblCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnTask;
@property (strong, nonatomic) IBOutlet UIButton *btnActivity;
@property (strong, nonatomic) IBOutlet UIButton *btnNotes;
@property (strong, nonatomic) IBOutlet UIButton *btnOpportunity;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (strong, nonatomic) IBOutlet UIButton *btnAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
@property (strong, nonatomic) IBOutlet UIButton *btncellNo;

// Opportunity
@property (strong, nonatomic) IBOutlet UILabel *lblOpprtunityNo;
@property (strong, nonatomic) IBOutlet UILabel *lblOpprtunityACNO;
@property (strong, nonatomic) IBOutlet UILabel *lblOpprtunityCompanyName;
@property (strong, nonatomic) IBOutlet UILabel *lblOpprtunityTechName;
@property (strong, nonatomic) IBOutlet UILabel *lblOpprtunityName;

//Leads
@property (strong, nonatomic) IBOutlet UILabel *lblLeadFN;
@property (strong, nonatomic) IBOutlet UILabel *lblLeadLN;
@property (strong, nonatomic) IBOutlet UILabel *lblLeadCity;
@property (strong, nonatomic) IBOutlet UILabel *lblLeadZip;
@property (strong, nonatomic) IBOutlet UILabel *lblLeadBranch;
@property (strong, nonatomic) IBOutlet UILabel *lblLeadStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblLeadCompanyName;
@property (strong, nonatomic) IBOutlet UILabel *lblLeadSourceName;
@property (strong, nonatomic) IBOutlet UIButton *btnEditLead;
@property (strong, nonatomic) IBOutlet UIButton *btnLeadPrimaryPhone;
@property (strong, nonatomic) IBOutlet UIButton *btnLeadSecondaryPhone;
@property (strong, nonatomic) IBOutlet UIButton *btnLeadCell;
@property (weak, nonatomic) IBOutlet UIButton *btnDead;


//Matching Accounts
@property (strong, nonatomic) IBOutlet UILabel *lblMatchingAccountNo;
@property (strong, nonatomic) IBOutlet UILabel *lblMatchingAccountThirdpartyAccNO;
@property (strong, nonatomic) IBOutlet UILabel *lblMatchingAccountPrimaryContact;
@property (strong, nonatomic) IBOutlet UILabel *lblCompanynameMatchingAccounts;
@property (strong, nonatomic) IBOutlet UIButton *btnMatchingAccountsPrimaryPhone;
@property (strong, nonatomic) IBOutlet UIButton *btnMatchingAccountsSecondaryPhone;
@property (strong, nonatomic) IBOutlet UIButton *btnMatchingAccountsCell;

@property (weak, nonatomic) IBOutlet UIView *viewMatchingAccountOppName;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFldMatchingAccountOpportunityName;

@property (weak, nonatomic) IBOutlet UIView *viewMatchingAccountThirdPartyAccNo;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFldMatchingAccountThirdPartyAccNo;

@property (weak, nonatomic) IBOutlet UIView *viewMatchingAccountCompanyName;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFldMatchingAccountCompanyName;

@property (weak, nonatomic) IBOutlet UIView *viewMatchingAccountPrimaryContact;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFldMatchingAccountPrimaryContact;
@property (weak, nonatomic) IBOutlet UIButton *btnMatchingAccountPrimaryContact;

@property (weak, nonatomic) IBOutlet UIView *viewMatchingAccountPrimaryPhone;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFldMatchingAccountPrimaryPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnMatchingAccountPrimaryPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnMatchingAccountMessagePrimaryPhone;



@property (weak, nonatomic) IBOutlet UIView *viewMatchingAccountSecondaryPhone;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFldMatchingAccountSecondaryPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnMatchingAccountSecondaryPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnMatchingAccountMessageSecondaryPhone;



@property (weak, nonatomic) IBOutlet UIView *viewMatchingAccountCell;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFldMatchingAccountCellNo;
@property (weak, nonatomic) IBOutlet UIButton *btnMatchingAccountCellNo;
@property (weak, nonatomic) IBOutlet UIButton *btnMatchingAccountMessageCellNo;



@property (weak, nonatomic) IBOutlet UIView *viewMatchingAccountEmail;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFldMatchingAccountEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnMatchingAccountEmail;

@property (weak, nonatomic) IBOutlet UIView *viewMatchingAddress;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtFldMatchingAccountAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnMatchingAccountAddress;



//Notes
@property (strong, nonatomic) IBOutlet UILabel *lblNotes;
@property (strong, nonatomic) IBOutlet UILabel *lblNotesCreatedBy;
@property (strong, nonatomic) IBOutlet UILabel *lblNotesCreatedDate;

//CRM NILIND
//In Lead
@property (weak, nonatomic) IBOutlet UIButton *btnCellNoNew;
@property (weak, nonatomic) IBOutlet UILabel *lblLeadName;

// akshay// 6 Dec
@property (weak, nonatomic) IBOutlet UIButton *btnGrab;
@property (weak, nonatomic) IBOutlet UIButton *btnCreateFollowUp;
@property (weak, nonatomic) IBOutlet UIButton *btnRelease;

// Statistics
@property (strong, nonatomic) IBOutlet UILabel *lblStats;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewStats;
//Search Contacts
@property (strong, nonatomic) IBOutlet UIButton *btn_PrimaryPhone_SearchContact;
@property (strong, nonatomic) IBOutlet UIButton *btn_SecondaryPhone_SearchContact;
@property (strong, nonatomic) IBOutlet UIButton *btn_PrimaryEmail_SearchContact;
@property (strong, nonatomic) IBOutlet UIButton *btn_SecondaryEmail_SearchContact;
@property (strong, nonatomic) IBOutlet UIButton *btn_Cell_SearchContact;

@property (strong, nonatomic) IBOutlet UILabel *lblName_SearchContact;
@property (weak, nonatomic) IBOutlet UILabel *lblNameSearchCompanyName;

// Mechanical Send Mail

@property (weak, nonatomic) IBOutlet UILabel *lblMech_Email;
@property (strong, nonatomic) IBOutlet UIButton *btnMech_ServiceReport;
@property (strong, nonatomic) IBOutlet UIButton *btnMech_Invocie;

//Block Time Cell

@property (weak, nonatomic) IBOutlet UILabel *lblBlockTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBlockTimeScheduleFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblBlockTimeScheduleTo;
@property (weak, nonatomic) IBOutlet UILabel *lblBlockTimeDescriptions;


// For SIgned Agreements
@property (strong, nonatomic) IBOutlet UIButton *btn_SignedAgreementsCall;
@property (strong, nonatomic) IBOutlet UIButton *btn_SignedAgreementsAddress;
@property (strong, nonatomic) IBOutlet UIButton *btn_SignedAgreementsEmail;
@property (strong, nonatomic) IBOutlet UIButton *btn_SignedAgreementsPrint;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SignedAgreementsAccountNo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SignedAgreementsTechName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SignedAgreementsCompanyName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SignedAgreementsSalesRep;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SignedAgreementsOpportunityName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SignedAgreementsSignedDate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SignedAgreementsScheduleDate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SignedAgreementsEstValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SignedAgreementsStatus;
@property (strong, nonatomic) IBOutlet UIButton *btn_SignedAgreementsAddresslbl;
@property (strong, nonatomic) IBOutlet UIButton *btn_SignedAgreementsEmaillbl;


// For Non Billable Hours
@property (weak, nonatomic) IBOutlet UILabel *lbl_NonBillableFromTime;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NonBillableToTime;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NonBillableTimeSlot;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NonBillableDepartment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NonBillableDescription;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NonBillableDuration;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NonBillableTimeSlotDuration;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NonBillableTimeInOut;


// For Images View Global
@property (strong, nonatomic) IBOutlet UIImageView *imgView_GlobalImageVC;
@property (weak, nonatomic) IBOutlet UILabel *lbl_GlobalImageViewCaption;
@property (weak, nonatomic) IBOutlet UILabel *lbl_GlobalImageViewDescriptions;
@property (weak, nonatomic) IBOutlet UIButton *btnAddendum;
@property (weak, nonatomic) IBOutlet UILabel *lblIncludeInAddendum;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorImage;
@property (weak, nonatomic) IBOutlet UIButton *btnDownload;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToInternal;
@property (weak, nonatomic) IBOutlet UILabel *lblAddToInternal;
@end
