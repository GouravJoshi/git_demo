//
//  tblCell.m
//  lead
//
//  Created by Navin Patidar on 9/21/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

#import "tblCell.h"

@implementation tblCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    self.selectionStyle = UITableViewCellSelectionStyleNone;

    // Configure the view for the selected state
}

@end
