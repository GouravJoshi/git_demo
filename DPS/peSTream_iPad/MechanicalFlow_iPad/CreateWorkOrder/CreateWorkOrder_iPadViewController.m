//
//  CreateWorkOrder_iPadViewController.m
//  DPS
//
//  Created by Akshay Hastekar on 28/03/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#define ACCEPTABLE_CHARACTERS @"0123456789"

#import "CreateWorkOrder_iPadViewController.h"

@interface AccountDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelAccountNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelAccountName;
@property (weak, nonatomic) IBOutlet UILabel *labelPrimaryContactName;
@property (weak, nonatomic) IBOutlet UILabel *labelPrimaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;

@property (weak, nonatomic) IBOutlet UIButton *buttonGenerateWorkOrder;


@end

@implementation AccountDetailsCell


@end

/////////////////////////////////////////////

@interface CreateWorkOrder_iPadViewController ()

@end

@implementation CreateWorkOrder_iPadViewController
{
    Global *global;
    NSDictionary *dictLoginDetails;
    NSString *strServiceUrlMainServiceAutomation;
    NSString *strCompanyKey;
    NSArray *arrayAccountDetails;
    NSMutableArray *arrayDepartment;
    NSDictionary *dictSalesLeadMaster;
    NSArray *arrayBranch;
    UITableView *tblData;
    UIButton *buttonBackground;
    NSDictionary *dictBranch;
    NSDictionary *dictDepartment;
    NSDictionary *dictSearch;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    global = [[Global alloc] init];
    arrayDepartment = [NSMutableArray new];
    strCompanyKey = @"";
    _buttonTransparent_SelectDepartment.hidden = YES;
    _viewPopUpSelectDepartment.hidden = YES;
    
    // configure pop up tableView
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.tag = 10;
    tblData.dataSource=self;
    tblData.delegate=self;
    // configure UI
    [self configureUI:_textFieldAccountNumber];
    [self configureUI:_textFieldAccountNumber];
    [self configureUI:_textFieldAccountName];
    [self configureUI:_textFieldFirstName];
    [self configureUI:_textFieldMiddleName];
    [self configureUI:_textFieldLastName];
    [self configureUI:_textFieldPhoneNumber];
    [self configureUI:_textFieldEmail];
    [self configureUI:_textFieldCompanyName];
    [self configureUI:_textFieldAddress];
    [self configureUI:_textFieldZipCode];
    
    [self configureUI:_textFieldCounty];
    [self configureUI:_textFieldSchoolDictict];
    
    [self configureUI:_buttonBranch];
    [self configureUI:_buttonDepartment];
    
    
    // add padding
    [self addPaddingInTextFields:_textFieldAccountNumber];
    [self addPaddingInTextFields:_textFieldAccountNumber];
    [self addPaddingInTextFields:_textFieldAccountName];
    [self addPaddingInTextFields:_textFieldFirstName];
    [self addPaddingInTextFields:_textFieldMiddleName];
    [self addPaddingInTextFields:_textFieldLastName];
    [self addPaddingInTextFields:_textFieldPhoneNumber];
    [self addPaddingInTextFields:_textFieldEmail];
    [self addPaddingInTextFields:_textFieldCompanyName];
    [self addPaddingInTextFields:_textFieldAddress];
    [self addPaddingInTextFields:_textFieldZipCode];
    [self addPaddingInTextFields:_textFieldCounty];
    [self addPaddingInTextFields:_textFieldSchoolDictict];
    
    // get log in details
    NSUserDefaults *userDef=[NSUserDefaults standardUserDefaults];
    
    dictLoginDetails = [userDef objectForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[dictLoginDetails valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    strCompanyKey = [[dictLoginDetails valueForKey:@"Company"] valueForKey:@"CompanyKey"];
    
    arrayBranch = [[userDef objectForKey:@"LeadDetailMaster"] valueForKey:@"BranchMasters"];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    dictBranch = nil;
    dictDepartment = nil;
}
#pragma mark - UITableView delegate and data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==101)
    {
        return arrayBranch.count;
    }
    if(tableView.tag==102)
    {
        return arrayDepartment.count;
    }
    return arrayAccountDetails.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*static NSString *identifier=@"cell";
     UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
     
     if (cell==nil)
     {
     cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
     }*/
    AccountDetailsCell *cellAccDetails = [tableView dequeueReusableCellWithIdentifier:@"AccountDetailsCell"];
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if(cellAccDetails==nil)
    {
        cellAccDetails = [[AccountDetailsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AccountDetailsCell"];
    }
    
    if(tableView.tag==101)
    {//branch
        cell.textLabel.text = [[arrayBranch objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==102)
    {//department
        cell.textLabel.text = [[arrayDepartment objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        return cell;
    }
    
    if([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"AccountNo"] isKindOfClass:[NSNull class]])
    {
        cellAccDetails.labelAccountNumber.text = @"";
    }
    else
    {
        cellAccDetails.labelAccountNumber.text = [NSString stringWithFormat:@"%@",[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"AccountNo"]];
    }
    if(([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"AccountName"] isKindOfClass:[NSNull class]]))
    {
        cellAccDetails.labelAccountName.text = @"";
    }
    else
    {
        cellAccDetails.labelAccountName.text = [[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"AccountName"];
    }
    if(([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"PrimaryContactName"] isKindOfClass:[NSNull class]]))
    {
        cellAccDetails.labelPrimaryContactName.text = @"";
    }
    else
    {
        cellAccDetails.labelPrimaryContactName.text = [[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"PrimaryContactName"];
    }
    
    if(([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"PrimaryPhone"] isKindOfClass:[NSNull class]]))
    {
        cellAccDetails.labelPrimaryPhone.text = @"";
    }
    else
    {
        cellAccDetails.labelPrimaryPhone.text = [[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"PrimaryPhone"];
    }
    
    if(([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"Email"] isKindOfClass:[NSNull class]]))
    {
        cellAccDetails.labelEmail.text = @"";
    }
    else
    {
        cellAccDetails.labelEmail.text = [[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"Email"];
    }
    NSString *strAddress = @"";
    if([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"Address1"]length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@",[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"Address1"]];
    }
    
    if([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"Address2"]length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@ %@",strAddress,[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"Address2"]];
    }
    
    if([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"cityname"]length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@ %@",strAddress,[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"cityname"]];
    }
    if([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"StateName"]length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@ %@",strAddress,[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"StateName"]];
    }
    if([[NSString stringWithFormat:@"%@",[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"zipcode"]] length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@ %@",strAddress,[NSString stringWithFormat:@"%@",[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"zipcode"]]];
    }
    
//    if([[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"CountryName"]length]>0)
//    {
//        strAddress = [NSString stringWithFormat:@"%@ %@",strAddress,[[arrayAccountDetails objectAtIndex:indexPath.row] valueForKey:@"CountryName"]];
//    }
    
    cellAccDetails.labelAddress.text = strAddress;
    
    cellAccDetails.buttonGenerateWorkOrder.tag = indexPath.row;
    [cellAccDetails.buttonGenerateWorkOrder addTarget:self action:@selector(actionOnGenerateWorkOrder:) forControlEvents:UIControlEventTouchUpInside];
    
    return cellAccDetails;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(tableView.tag==101)
    {
        [arrayDepartment removeAllObjects];
        for(NSDictionary *dict in [[arrayBranch objectAtIndex:indexPath.row] valueForKey:@"Departments"])
        {
            if([[dict valueForKey:@"IsActive"] integerValue]==1&&[[dict valueForKey:@"DepartmentTypeSysName"] isEqualToString:@"Mechanical"])
            {
                [arrayDepartment addObject:dict];
            }
        }
        [_buttonBranch setTitle:[[arrayBranch objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
        
        dictBranch = [arrayBranch objectAtIndex:indexPath.row];
    }
    if(tableView.tag==102)
    {
        [_buttonDepartment setTitle:[[arrayDepartment objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
        
        dictDepartment = [arrayDepartment objectAtIndex:indexPath.row];
    }
    
    [tblData removeFromSuperview];
    [buttonBackground removeFromSuperview];
}

#pragma mark - UIButton action

- (IBAction)actionOnSearch:(id)sender
{
    BOOL isEnteredAnyValue = NO;
    // please enter at least one search criteria
    NSMutableDictionary *dictJsonObject = [NSMutableDictionary new];
    
    [dictJsonObject setValue:strCompanyKey forKey:@"CompanyKey"];
    
    if(_textFieldAccountNumber.text.length>0)
    {
        [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldAccountNumber.text] forKey:@"AccountNo"];
        isEnteredAnyValue = YES;
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"AccountNo"];
    }
    if(_textFieldAccountName.text.length>0)
    {
        [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldAccountName.text] forKey:@"AccountName"];
        isEnteredAnyValue = YES;
        
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"AccountName"];
    }
    
    if(_textFieldFirstName.text.length>0)
    {
        [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldFirstName.text] forKey:@"FirstName"];
        isEnteredAnyValue = YES;
        
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"FirstName"];
    }
    
    if(_textFieldMiddleName.text.length>0)
    {
        [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldMiddleName.text] forKey:@"MiddleName"];
        isEnteredAnyValue = YES;
        
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"MiddleName"];
    }
    
    if(_textFieldLastName.text.length>0)
    {
        [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldLastName.text] forKey:@"LastName"];
        isEnteredAnyValue = YES;
        
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"LastName"];
    }
    
    if(_textFieldPhoneNumber.text.length>0)
    {
        [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldPhoneNumber.text] forKey:@"PhoneNo"];
        isEnteredAnyValue = YES;
        
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"PhoneNo"];
    }
    
    if(_textFieldEmail.text.length>0)
    {
        _textFieldEmail.text =  [_textFieldEmail.text stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if([self IsValidEmail:_textFieldEmail.text]==true)
        {
            [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldEmail.text] forKey:@"Email"];
            isEnteredAnyValue = YES;
            
        }
        else
        {
            [global AlertMethod:Alert :@"please enter valid email"];
        }
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"Email"];
    }
    
    if(_textFieldCompanyName.text.length>0)
    {
        [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldCompanyName.text] forKey:@"Name"];
        isEnteredAnyValue = YES;
        
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"Name"];
    }
    
    if(_textFieldAddress.text.length>0)
    {
        [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldAddress.text] forKey:@"Address"];
        isEnteredAnyValue = YES;
        
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"Address"];
    }
    
    if(_textFieldZipCode.text.length>0)
    {
        [dictJsonObject setValue:[NSString stringWithFormat:@"%@",_textFieldZipCode.text] forKey:@"Zip"];
        isEnteredAnyValue = YES;
        
    }
    else
    {
        [dictJsonObject setValue:@"" forKey:@"Zip"];
    }
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:Alert :ErrorInternetMsg];
    }
    else
    {
        if(isEnteredAnyValue == NO)
        {
            [global AlertMethod:Alert :@"Please enter search criteria"];
        }
        else
        {
            [self callAPIToSearchAccounts:dictJsonObject];
        }
    }
}

-(void)actionOnGenerateWorkOrder:(id)sender
{
    
    UIButton *btn = (UIButton*)sender;
    dictSearch = [arrayAccountDetails objectAtIndex:btn.tag];
    _buttonTransparent_SelectDepartment.hidden = NO;
    _viewPopUpSelectDepartment.hidden = NO;
    [_buttonBranch setTitle:@"Select Branch" forState:UIControlStateNormal];
    [_buttonDepartment setTitle:@"Select Department" forState:UIControlStateNormal];
    [arrayDepartment removeAllObjects];

}

- (IBAction)actionOnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
// pop up view select department

- (IBAction)actionOnSelectBranch:(id)sender
{
    
    if (arrayBranch.count>0) {
        
        tblData.tag = 101;
        [self setTableFrame];
        [tblData reloadData];
        
    } else {
        
        [global displayAlertController:Alert :NoDataAvailableee :self];
        
    }

}

- (IBAction)actionOnSelectDepartment:(id)sender
{
    
    if (arrayDepartment.count>0) {
        
        tblData.tag = 102;
        [self setTableFrame];
        [tblData reloadData];
        
    } else {
        
        [global displayAlertController:Alert :NoDataAvailableee :self];
        
    }
    
}

- (IBAction)actionOnTransparentButton_PopUpView_SelectDepartment:(id)sender
{
    _viewPopUpSelectDepartment.hidden=YES;
    _buttonTransparent_SelectDepartment.hidden=YES;
}

- (IBAction)actionOnCancel_SelectDepartment:(id)sender
{
    _viewPopUpSelectDepartment.hidden=YES;
    _buttonTransparent_SelectDepartment.hidden=YES;
}

- (IBAction)actionOnAdd_SelectDepartment:(id)sender
{
    if(!(dictBranch.count>0))
    {
        [global AlertMethod:@"Alert!" :@"Please select branch"];
    }
    else if(!(dictDepartment.count>0))
    {
        [global AlertMethod:@"Alert!" :@"Please select department"];
        
    }
    else
    {
        _viewPopUpSelectDepartment.hidden=YES;
        _buttonTransparent_SelectDepartment.hidden=YES;
        [self performSegueWithIdentifier:@"CreateWorkOrder2_iPadViewController" sender:nil];
    }
    
}
#pragma mark - UITextField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    if([[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
    //    {
    //        return NO;
    //    }
    
    if(!(textField.text.length>0)&&[string isEqualToString:@" "])
    {
        return NO;
    }
    if(textField == _textFieldAccountNumber||textField == _textFieldPhoneNumber||textField == _textFieldZipCode)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if(textField==_textFieldPhoneNumber)
        {
            if([string isEqualToString:filtered]==YES)
            {
                if(range.length + range.location > textField.text.length)
                {
                    return NO;
                }
                
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 15;
            }
        }
        if(textField == _textFieldZipCode)
        {
            if([string isEqualToString:filtered]==YES)
            {
                if(range.length + range.location > textField.text.length)
                {
                    return NO;
                }
                
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 5;
            }
        }
        return [string isEqualToString:filtered];
    }
    
    return YES;
    
}
#pragma mark - Segue Method
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"CreateWorkOrder2_iPadViewController"])
    {
        CreateWorkOrder2_iPadViewController *vc = [segue destinationViewController];
        vc.dictBranch = dictBranch;
        vc.dictDepartment = dictDepartment;
        vc.dictSearchData = dictSearch;
        
        if(_textFieldAccountNumber.text.length>0)
        {
            vc.strAccountNumber = [NSString stringWithFormat:@"%@",_textFieldAccountNumber.text];
        }
        if(_textFieldPhoneNumber.text.length>0)
        {
            vc.strPhone = [NSString stringWithFormat:@"%@",_textFieldPhoneNumber.text];
        }
        if(_textFieldEmail.text.length>0)
        {
            vc.strEmail = [NSString stringWithFormat:@"%@",_textFieldEmail.text];
        }
        if(_textFieldCompanyName.text.length>0)
        {
            vc.strCompanyName = [NSString stringWithFormat:@"%@",_textFieldCompanyName.text];
        }
    }
}

#pragma mark - Web Service
-(void)callAPIToSearchAccounts:(NSMutableDictionary *)jsonArray
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,URLSEARCHACCOUNT];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonArray options:NSJSONWritingPrettyPrinted error:Nil];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonDatastr %@",str);
    NSURL * serviceUrl = [NSURL URLWithString:strUrl];
    NSLog(@"REquest URL >> %@",serviceUrl);
    NSLog(@"REquest XML >> %@",str);
    
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:serviceUrl];
    
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *__block serviceResponse;
    NSError *__block serviceError;
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&serviceResponse error:&serviceError];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [DejalBezelActivityView removeView];
            
        });
        
        if (responseData)
        {
            [self parsePostApiData:responseData];
        }
        else
        {
            
        }
    });
}

-(void)parsePostApiData:(NSData *)response
{
    id jsonObject = Nil;
    
    NSString *charlieSendString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSLog(@"ResponseString %@",charlieSendString);
    
    if (response==nil)
    {
        NSLog(@"No internet connection.");
    }
    else
    {
        NSError *error = Nil;
        jsonObject =[NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            NSLog(@"Probably An Array %@",jsonObject);
            
            arrayAccountDetails = (NSArray*)jsonObject;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _heightViewMainContainer.constant = _heightViewMainContainer.constant+(arrayAccountDetails.count*387.0)+20.0;
                [_tableViewAccountDetails reloadData];
            });
            
            
            //            if([[[jsonObject objectAtIndex:0] valueForKey:@"Value"] boolValue]==1)
            //            {
            ////                dispatch_async(dispatch_get_main_queue(), ^{
            ////
            ////                });
            //            }
            //            else
            //            {
            //                [global AlertMethod:@"Message" :[[jsonObject objectAtIndex:1] valueForKey:@"Value"]];
            //            }
        }
        else
        {
            NSLog(@"Probably A Dictionary");
            NSDictionary *jsonDictionary=(NSDictionary *)jsonObject;
            NSLog(@"jsonDictionary %@",[jsonDictionary description]);
        }
    }
}

-(void)showPopUpView
{
    //    _viewPopUpSelectDepartment
}

-(void)setTableFrame
{
    buttonBackground=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    buttonBackground.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    
    [buttonBackground addTarget:self action:@selector(actionOnBackground:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: buttonBackground];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    
    tblData.layer.borderWidth = 1.0;
    tblData.layer.borderColor = [[UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0] CGColor];
    
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
    }
    
    [self.view addSubview:tblData];
}
-(void)actionOnBackground:(id)sender
{
    [tblData removeFromSuperview];
    [buttonBackground removeFromSuperview];
}

-(void)configureUI:(UIView*)view
{
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
    view.layer.cornerRadius = 4.0;
}

-(void)addPaddingInTextFields:(UITextField*)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
@end
