//
//  CreateWorkOrder2_iPadViewController.m
//  DPS
//
//  Created by Akshay Hastekar on 04/04/18.
//  Copyright © 2018 Saavan. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS @"0123456789"

#import "CreateWorkOrder2_iPadViewController.h"

@interface CreateWorkOrder2_iPadViewController ()

@end

@implementation CreateWorkOrder2_iPadViewController
{
    BOOL isTimeAndMaterial;
    BOOL isSplitPrice;
    BOOL isAfterHour;
    BOOL isSpecificHour;
    BOOL isRequireClientApproval;
    BOOL isTaxExempt;
    BOOL isScheduleServiceOrder;
    BOOL isSpecificTime;
    NSArray *arrayService;
    NSMutableArray *arraySource;
    UITableView *tblData;
    UIButton *buttonBackground;
    NSDictionary *dictService;
    NSDictionary *dictSource;
    Global *global;
    NSArray *arrayJobType;
    NSDictionary *dictLoginDetails;
    NSString *strCompanyKey;
    NSString *strServiceUrlMainServiceAutomation;
    NSArray *arrayServiceAddress;
    NSDictionary *dictServiceAddress;
    NSArray *arrayAddressSubType;
    NSString *strServiceAddressID;
    NSMutableArray *arraySpecificTime;
    NSArray *arrayRangeTime;
    NSDictionary *dictSpecificOrRangeTime;
    UIDatePicker *pickerDate;
    UIView *viewBackGround1;
    UIView *viewBackGroundSource;
    UIView *viewForDate;
    NSString *strPickerType;
    NSString *strSpecificTime;
    NSMutableArray *arrayPrimaryRoutes;
    NSDictionary *dictPrimaryRoute;
    NSString *strEstimateTime;
    NSMutableArray *arrayCountries;
    NSMutableArray *arrayState;
    NSDictionary *dictCountry;
    NSDictionary *dictState;
    NSString *strCompanyID;
    NSString *strEmployeeNumber;
    NSString *strEmployeeID;
    NSString *strLoggedInUserName;
    NSString *strInitialEstimateDuration;
    UIButton *buttonOK;
    NSMutableArray *arrIndex;
    NSMutableArray *arrSourceSelectedValue;
    NSMutableArray *arrSourceSelectedId;
    NSMutableArray *arraySourceSelected;
    NSString *strServiceDate;
    BOOL isMechanic;
    NSMutableArray *arrayTaxcode;
    NSString *strTaxcodeSysName;
    NSArray *arrayContact_ServiceAddress;
    NSString *strServicePOCid;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // changes by saavan for flate rate and TM Selection
    
    [_buttonFlatRate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_buttonTimeAndMaterial setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    
    NSLog(@"%@",_dictSearchData);
    global = [[Global alloc] init];
    isTimeAndMaterial = NO;
    isAfterHour = NO;
    isSpecificHour = NO;
    isTaxExempt = NO;
    isRequireClientApproval = NO;
    isScheduleServiceOrder = YES;
    isSpecificTime = YES;
    strServiceAddressID = @"";
    strPickerType = @"";
    strEstimateTime = @"";
    strInitialEstimateDuration = @"";
    strTaxcodeSysName = @"";
    strServicePOCid = @"";
    arraySpecificTime = [[NSMutableArray alloc]init];
    arrayPrimaryRoutes = [[NSMutableArray alloc]init];
    arrIndex = [[NSMutableArray alloc]init];
    arrSourceSelectedValue = [[NSMutableArray alloc] init];
    arrSourceSelectedId = [[NSMutableArray alloc] init];
    arraySource = [[NSMutableArray alloc]init];
    arraySourceSelected = [[NSMutableArray alloc] init];
    arrayTaxcode = [[NSMutableArray alloc]init];
    
    // configure pop up tableView
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.tag = 10;
    tblData.dataSource=self;
    tblData.delegate=self;
    
    arrayJobType = @[@"New Construction",@"Repair/Remodeling Restoration"];
    arrayAddressSubType = @[@"Residential",@"Commercial"];
    
    [_buttonSelectJobType setTitle:@"Repair/Remodeling Restoration" forState:UIControlStateNormal];
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dictSalesLeadMaster=[defs valueForKey:@"LeadDetailMaster"];
    arrayService = [dictSalesLeadMaster valueForKey:@"ServiceMasters"];
    arraySource = [[dictSalesLeadMaster valueForKey:@"SourceMasters"] mutableCopy];
    
    if(_dictBranch.count>0)
    {
        _textFieldBranch.text = [_dictBranch valueForKey:@"Name"];
    }
    if(_dictDepartment.count>0)
    {
        _textFieldDepartment.text = [_dictDepartment valueForKey:@"Name"];
    }
    
    if([NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"AccountNo"]].length>0)
    {
        _textFieldAccountNumber.text = [NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"AccountNo"]];
        [_textFieldAccountNumber resignFirstResponder];
        _strAccountNumber = [NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"AccountNo"]];
    }
    if([NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"PrimaryContactName"]].length>0&& ![[_dictSearchData valueForKey:@"PrimaryContactName"] isKindOfClass:[NSNull class]] && ![[_dictSearchData valueForKey:@"PrimaryContactName"] isEqualToString:@"<null>"])
    {
        _textFieldContact.text = [NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"PrimaryContactName"]];
        [_textFieldContact resignFirstResponder];
    }
    
    if([NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"PrimaryPhone"]].length>0&&![[_dictSearchData valueForKey:@"PrimaryPhone"] isKindOfClass:[NSNull class]])
    {
        _textFieldPrimaryPhone.text = [NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"PrimaryPhone"]];
        [_textFieldPrimaryPhone resignFirstResponder];
    }
    
    if([NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"SecondaryPhone"]].length>0&&![[_dictSearchData valueForKey:@"SecondaryPhone"] isKindOfClass:[NSNull class]])
    {
        _textFieldSecondaryPhone.text = [NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"SecondaryPhone"]];
        [_textFieldSecondaryPhone resignFirstResponder];
    }
    
    if([NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"Email"]].length>0&& ![[_dictSearchData valueForKey:@"Email"] isKindOfClass:[NSNull class]])
    {
        _textFieldPrimaryEmail.text = [NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"Email"]];
        [_textFieldPrimaryEmail resignFirstResponder];
    }
    if([NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"CompanyName"]].length>0)
    {
        _textFieldCompanyName.text = [NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"CompanyName"]];
        [_textFieldCompanyName resignFirstResponder];
    }
    if([NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"CellNo"]].length>0&&![[_dictSearchData valueForKey:@"CellNo"] isKindOfClass:[NSNull class]])
    {
        _textFieldCellNumber.text = [NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"CellNo"]];
        [_textFieldCellNumber resignFirstResponder];
    }
    
    if([NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"AccountAlert"]].length>0 && ![[_dictSearchData valueForKey:@"AccountAlert"] isKindOfClass:[NSNull class]])
    {
        _textFieldAccountAlert.text = [NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"AccountAlert"]];
        [_textFieldAccountAlert resignFirstResponder];
    }
    
    [self configureUI:_textFieldAccountNumber];
    [self configureUI:_textFieldContact];
    [self configureUI:_textFieldCompanyName];
    [self configureUI:_textFieldPrimaryEmail];
    [self configureUI:_textFieldPrimaryPhone];
    [self configureUI:_textFieldSecondaryPhone];
    [self configureUI:_textFieldCellNumber];
    [self configureUI:_textFieldBranch];
    [self configureUI:_textFieldDepartment];
    [self configureUI:_textFieldAccountAlert];
    [self configureUI:_textFieldPriceNotExceed];
    [self configureUI:_textFieldLabor];
    [self configureUI:_textFieldPart];
    [self configureUI:_textViewJobDescription];
    [self configureUI:_textFieldTaxExemptionNumber];
    [self configureUI:_buttonSelectAddress_ServiceAddress];
    [self configureUI:_btnContact_ServiceAddress];//_textFieldContact_ServiceAddress
    [self configureUI:_textFieldAddressLine1_ServiceAddress];
    [self configureUI:_textFieldAddressLine2_ServiceAddress];
    [self configureUI:_buttonSelectCountry_ServiceAddress];
    [self configureUI:_buttonSelectState_ServiceAddress];
    [self configureUI:_textFieldCity_ServiceAddress];
    [self configureUI:_textFieldZipCode_ServiceAddress];
    [self configureUI:_textFieldLatitude_ServiceAddress];
    [self configureUI:_textFieldLongitude_ServiceAddress];
    [self configureUI:_textFieldMapCode_ServiceAddress];
    [self configureUI:_textFieldServiceGateCode_ServiceAddress];
    [self configureUI:_textViewServiceNotes];
    [self configureUI:_textViewDirection];
    [self configureUI:_buttonServiceDate];
    [self configureUI:_buttonSpecificOrRangeTime];
    [self configureUI:_buttonSelectPrimaryRoute];
    [self configureUI:_textViewSpecialInstruction];
    [self configureUI:_textViewServiceInstruction];
    [self configureUI:_textViewInternalInstruction];
    [self configureUI:_textViewSetUpInstruction];
    [self configureUI:_buttonSelectService];
    [self configureUI:_buttonSelectJobType];
    [self configureUI:_buttonEstimateTime];
    [self configureUI:_buttonSelectSource];
    [self configureUI:_buttonSelectServiceAddress];
    [self configureUI:_buttonSelectSpecificHour];
    [self configureUI:_textFieldPONumbers];
    [self configureUI:_textFieldCounty];
    [self configureUI:_textFieldSchoolDistict];
    [self configureUI:_buttonAddressSubType];
    [self configureUI:_btnSelectTaxcode];
    
    
    // add padding
    [self addPaddingInTextFields:_textFieldAccountNumber];
    [self addPaddingInTextFields:_textFieldContact];
    [self addPaddingInTextFields:_textFieldCompanyName];
    [self addPaddingInTextFields:_textFieldPrimaryEmail];
    [self addPaddingInTextFields:_textFieldPrimaryPhone];
    [self addPaddingInTextFields:_textFieldSecondaryPhone];
    [self addPaddingInTextFields:_textFieldCellNumber];
    [self addPaddingInTextFields:_textFieldBranch];
    [self addPaddingInTextFields:_textFieldDepartment];
    [self addPaddingInTextFields:_textFieldAccountAlert];
    [self addPaddingInTextFields:_textFieldPriceNotExceed];
    [self addPaddingInTextFields:_textFieldLabor];
    [self addPaddingInTextFields:_textFieldPart];
    [self addPaddingInTextFields:_textFieldTaxExemptionNumber];
    [self addPaddingInTextFields:_textFieldContact_ServiceAddress];
    [self addPaddingInTextFields:_textFieldAddressLine1_ServiceAddress];
    [self addPaddingInTextFields:_textFieldAddressLine2_ServiceAddress];
    [self addPaddingInTextFields:_textFieldCity_ServiceAddress];
    [self addPaddingInTextFields:_textFieldZipCode_ServiceAddress];
    [self addPaddingInTextFields:_textFieldLatitude_ServiceAddress];
    [self addPaddingInTextFields:_textFieldLongitude_ServiceAddress];
    [self addPaddingInTextFields:_textFieldMapCode_ServiceAddress];
    [self addPaddingInTextFields:_textFieldPONumbers];
    [self addPaddingInTextFields:_textFieldCounty];
    [self addPaddingInTextFields:_textFieldSchoolDistict];
    
    
    _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewTimeAndMaterial.constant;
    _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewHolidayAndSpecificHour.constant;
    _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewSpecificHour.constant;
    _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewTaxExemption.constant;
    
    _heightViewTimeAndMaterial.constant = 0.0;
    _heightViewHolidayAndSpecificHour.constant = 0.0;
    _heightViewSpecificHour.constant = 0.0;
    _heightViewTaxExemption.constant = 0.0;
    
    NSUserDefaults *userDef=[NSUserDefaults standardUserDefaults];
    
    dictLoginDetails = [userDef objectForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[dictLoginDetails valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    strCompanyKey = [[dictLoginDetails valueForKey:@"Company"] valueForKey:@"CompanyKey"];
    strCompanyID = [NSString stringWithFormat:@"%@",[[[[dictLoginDetails valueForKey:@"Company"] valueForKey:@"CompanyConfig"] valueForKey:@"BillingServiceModule"] valueForKey:@"CompanyId"]];
    
    strEmployeeNumber = [NSString stringWithFormat:@"%@",[dictLoginDetails valueForKey:@"EmployeeNumber"]];
    strEmployeeID = [NSString stringWithFormat:@"%@",[dictLoginDetails valueForKey:@"EmployeeId"]];
    strLoggedInUserName = [NSString stringWithFormat:@"%@",[dictLoginDetails valueForKey:@"LoggedInUserName"]];
    
    [self fetchJsonState];
    [self fetchJsonCountry];
    
    isMechanic=YES;
    [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    // filter taxcode and add into array
    /* let defsLogindDetail = UserDefaults.standard
     let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
     let strBranchName = Global().getBranchName(fromSysNam: strBranchNameGlobal)*/
    NSDictionary *dictTempTaxcode =  [defs valueForKey:@"MasterSalesAutomation"];
    NSString *strBranchName = [global getBranchNameFromSysNam:[global strEmpBranchSysName]];
    if(dictTempTaxcode.count>0)
    {
        NSArray *arrayTemp = [dictTempTaxcode valueForKey:@"TaxMaster"];
        
        arrayTaxcode = [[NSMutableArray alloc] init];
        
        for(NSDictionary *dict in arrayTemp)
        {
            if([[_dictBranch valueForKey:@"SysName"] isEqualToString:[dict valueForKey:@"BranchSysName"]])
            {
                [arrayTaxcode addObject:dict];
            }
        }
        
        //arrayTaxcode = [[NSMutableArray arrayWithArray:arrayTemp] mutableCopy];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSString *string = [NSString stringWithFormat:@"%ld/%ld/%ld",(long)month,(long)day, (long)year];
    strServiceDate = string;
    [_buttonServiceDate setTitle:string forState:UIControlStateNormal];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:Alert :@"No internet connection"];
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"please wait..."];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [self getServiceAddress];
            [self getAllRangeOfTime];
            [self getAccountPastDue];
            
        });
        
    }
    
}

#pragma mark - UITableView delegate and data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==101)
    {
        return arrayService.count;
    }
    if(tableView.tag==102)
    {
        return arraySource.count;
    }
    if(tableView.tag==103)
    {
        return arrayJobType.count;
    }
    if(tableView.tag==104)
    {
        return arrayServiceAddress.count;
    }
    if(tableView.tag==105)
    {
        return arrayAddressSubType.count;
    }
    if(tableView.tag==106)
    {
        return arraySpecificTime.count;
    }
    if(tableView.tag==107)
    {
        return arrayRangeTime.count;
    }
    if(tableView.tag==108)
    {
        return arrayPrimaryRoutes.count;
    }
    if(tableView.tag==109)
    {
        return arrayCountries.count;
    }
    if(tableView.tag==110)
    {
        return arrayState.count;
    }
    if(tableView.tag==111)
    {
        return arrayTaxcode.count;
    }
    if(tableView.tag==112)
    {
        return arrayContact_ServiceAddress.count;
    }
    return 5;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    if(tableView.tag==101)
    {//service
        cell.textLabel.text = [[arrayService objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==102)
    {//source
        cell.textLabel.text = [[arraySource objectAtIndex:indexPath.row] valueForKey:@"SourceFullName"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        
        NSDictionary *dictData=[arraySource objectAtIndex:indexPath.row];
        cell.textLabel.text=[dictData valueForKey:@"Name"];
        NSString *str=[dictData valueForKey:@"SourceSysName"];
        long index=10000000;
        
        for (int k=0; k<arrSourceSelectedId.count; k++)//arrSourceSelectedValue
        {
            if ([arrSourceSelectedId containsObject:str])//arrSourceSelectedValue
            {
                index=indexPath.row;
                break;
            }
        }
        
        if (indexPath.row==index)
        {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        return cell;
    }
    if(tableView.tag==103)
    {
        cell.textLabel.text = [arrayJobType objectAtIndex:indexPath.row];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==104)
    {
        cell.textLabel.text = [[arrayServiceAddress objectAtIndex:indexPath.row] valueForKey:@"CombinedAddress"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==105)
    {
        cell.textLabel.text = [arrayAddressSubType objectAtIndex:indexPath.row];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }//arraySpecificTime
    
    if(tableView.tag==106)
    {
        cell.textLabel.text = [arraySpecificTime objectAtIndex:indexPath.row];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==107)
    {
        cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@",[[arrayRangeTime objectAtIndex:indexPath.row] valueForKey:@"StartInterval"],[[arrayRangeTime objectAtIndex:indexPath.row] valueForKey:@"EndInterval"]];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==108)
    {
        cell.textLabel.text = [[arrayPrimaryRoutes objectAtIndex:indexPath.row] valueForKey:@"RouteName"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==109)
    {
        cell.textLabel.text = [[arrayCountries objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==110)
    {
        cell.textLabel.text = [[arrayState objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==111)
    {
        cell.textLabel.text = [[arrayTaxcode objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==112)
    {
        cell.textLabel.text = [[arrayContact_ServiceAddress objectAtIndex:indexPath.row] valueForKey:@"ContactName"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(tableView.tag==101)
    {
        [_buttonSelectService setTitle:[[arrayService objectAtIndex:indexPath.row] valueForKey:@"ServiceSysName"] forState:UIControlStateNormal];
        
        dictService = [arrayService objectAtIndex:indexPath.row];
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        
    }
    if(tableView.tag==102)
    {
        [_buttonSelectSource setTitle:[[arraySource objectAtIndex:indexPath.row] valueForKey:@"SourceFullName"] forState:UIControlStateNormal];
        
        dictSource = [arraySource objectAtIndex:indexPath.row];
        NSString *strSourceIdSelected  = [NSString stringWithFormat:@"%@",[dictSource valueForKey:@"SourceId"]];
        
        NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
        UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
        if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
            [arrIndex addObject:[NSString stringWithFormat:@"%lu",(unsigned long)indexPath.row]];
            [arrSourceSelectedValue addObject:[dictSource valueForKey:@"Name"]];
            [arrSourceSelectedId addObject:[dictSource valueForKey:@"SourceSysName"]];
            NSLog(@"ArrIndex After add>>%@",arrIndex);
            NSLog(@"arrSourceSelectedId After add>>%@",arrSourceSelectedId);
            [arraySourceSelected addObject:dictSource];
            
        }
        else
        {
            [cellNew setAccessoryType:UITableViewCellAccessoryNone];
            NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [arrIndex removeObject:str];
            [arrSourceSelectedId removeObject:[dictSource valueForKey:@"SourceSysName"]];
            [arrSourceSelectedValue removeObject:[dictSource valueForKey:@"Name"]];
            NSLog(@"ArrIndex After Remove>>%@",arrIndex);
            NSLog(@"arrSourceSelectedId After remove>>%@",arrSourceSelectedId);
            
            
            for (int k1=0; k1<arraySourceSelected.count; k1++) {
                
                NSDictionary *dictDataTemp = arraySourceSelected[k1];
                
                NSString *strSourceId  = [NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"SourceId"]];
                
                if ([strSourceId isEqualToString:strSourceIdSelected]) {
                    
                    [arraySourceSelected removeObjectAtIndex:k1];
                    
                    break;
                }
                
            }
            
            if(arraySourceSelected.count==0)
            {
                [_buttonSelectSource setTitle:@"Select Source" forState:UIControlStateNormal];
            }
            
            //[arraySourceSelected removeObjectAtIndex:indexPath.row];
            
        }
        
        if(arraySourceSelected.count>0)
        {
            NSString *str = @"";
            for(NSDictionary *dict in arraySourceSelected)
            {
                if(str.length>0)
                {
                    str = [NSString stringWithFormat:@"%@,%@",str,[dict valueForKey:@"SourceFullName"]];
                }
                else
                {
                    str = [NSString stringWithFormat:@"%@",[dict valueForKey:@"SourceFullName"]];
                }
            }
            if(str.length>0)
            {
                [_buttonSelectSource setTitle:str forState:UIControlStateNormal];
            }
        }
        
    }
    if(tableView.tag==103)
    {
        [_buttonSelectJobType setTitle:[arrayJobType objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        
    }
    if(tableView.tag==104)
    {
        [_buttonSelectServiceAddress setTitle:[[arrayServiceAddress objectAtIndex:indexPath.row] valueForKey:@"CombinedAddress"] forState:UIControlStateNormal];
        
        dictServiceAddress = [arrayServiceAddress objectAtIndex:indexPath.row];
        
        strServiceAddressID = [NSString stringWithFormat:@"%@",[dictServiceAddress valueForKey:@"AddressTypeId"]];
        
        if([[dictServiceAddress valueForKey:@"AddressContactExtSerDcs"] count]>0 || ![[dictServiceAddress valueForKey:@"AddressContactExtSerDcs"] isEqualToString:@"<null>"] )
        {
            arrayContact_ServiceAddress = [dictServiceAddress valueForKey:@"AddressContactExtSerDcs"];
            
            BOOL isPrimaryFound = NO;
            
            for (NSDictionary *dictCont in arrayContact_ServiceAddress)
            {
                if([[dictCont valueForKey:@"IsPrimary"] intValue] == 1)
                {
                    [_btnContact_ServiceAddress setTitle:[dictCont valueForKey:@"ContactName"] forState:UIControlStateNormal];
                    strServicePOCid = [dictCont valueForKey:@"CrmContactId"];
                    isPrimaryFound = YES;
                    break;
                }
            }
            if(isPrimaryFound == NO)// then show 0 index contact name
            {
                [_btnContact_ServiceAddress setTitle:[[arrayContact_ServiceAddress firstObject]valueForKey:@"ContactName"] forState:UIControlStateNormal];
            }
        }
        
        
        [self showServiceAddressData:dictServiceAddress];
        [_buttonSelectPrimaryRoute setTitle:@"Select" forState:UIControlStateNormal];
        [self getAllPrimaryRoutes];
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==105)
    {
        [_buttonAddressSubType setTitle:[arrayAddressSubType objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        
    }
    if(tableView.tag==106)
    {
        [_buttonSelectSpecificHour setTitle:[arraySpecificTime objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==107)
    {
        [_buttonSpecificOrRangeTime setTitle:[NSString stringWithFormat:@"%@ - %@",[[arrayRangeTime objectAtIndex:indexPath.row] valueForKey:@"StartInterval"],[[arrayRangeTime objectAtIndex:indexPath.row] valueForKey:@"EndInterval"]] forState:UIControlStateNormal];
        
        dictSpecificOrRangeTime = [arrayRangeTime objectAtIndex:indexPath.row];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        
    }
    if(tableView.tag==108)
    {
        [_buttonSelectPrimaryRoute setTitle:[[arrayPrimaryRoutes objectAtIndex:indexPath.row] valueForKey:@"RouteName"] forState:UIControlStateNormal];
        
        dictPrimaryRoute = [arrayPrimaryRoutes objectAtIndex:indexPath.row];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        
    }
    if(tableView.tag==109)
    {
        [_buttonSelectCountry_ServiceAddress setTitle:[[arrayCountries objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
        dictCountry = [arrayCountries objectAtIndex:indexPath.row];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        
    }
    if(tableView.tag==110)
    {
        [_buttonSelectState_ServiceAddress setTitle:[[arrayState objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
        
        dictState = [arrayState objectAtIndex:indexPath.row];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        
    }
    if(tableView.tag==111)
    {
        [_btnSelectTaxcode setTitle:[[arrayTaxcode objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
        
        strTaxcodeSysName = [[arrayTaxcode objectAtIndex:indexPath.row] valueForKey:@"SysName"];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==112)
    {
        [_btnContact_ServiceAddress setTitle:[[arrayContact_ServiceAddress objectAtIndex:indexPath.row] valueForKey:@"ContactName"] forState:UIControlStateNormal];
        strServicePOCid = [[arrayContact_ServiceAddress objectAtIndex:indexPath.row] valueForKey:@"CrmContactId"];
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
}

#pragma mark - UITextField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==_textFieldPriceNotExceed|| textField==_textFieldLatitude_ServiceAddress || textField==_textFieldLongitude_ServiceAddress || textField==_textFieldLabor||textField==_textFieldPart)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :100 :(int)textField.text.length :@".0123456789" :textField.text];
        
        if (isNuberOnly) {
            return YES;
        } else {
            return NO;
        }
        
        //        NSArray *sep = [string componentsSeparatedByString:@"."];
        //        if([sep count] >= 2)
        //        {
        //            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
        //            if(!([sepStr length]>2))
        //            {
        //
        //                return true;
        //            }
        //
        //            return false;
        //        }
    }
    
    if(textField == _textFieldZipCode_ServiceAddress)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if([string isEqualToString:filtered]==YES)
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 5;
        }
        
        return [string isEqualToString:filtered];
    }
    return YES;
    
}

#pragma mark - UITextView delegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(textView.text.length>=5000)
    {
        return false;
    }
    return true;
}

#pragma mark - UIButton action

- (IBAction)actionOnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)methodBack{
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k1=0; k1<arrstack.count; k1++) {
        if ([[arrstack objectAtIndex:k1] isKindOfClass:[CreateWorkOrder_iPadViewController class]]) {
            index=k1;
            //break;
        }
    }
    CreateWorkOrder_iPadViewController *myController = (CreateWorkOrder_iPadViewController *)[self.navigationController.viewControllers objectAtIndex:index];
    // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    [self.navigationController popToViewController:myController animated:NO];
    
}

- (IBAction)actionOnSelectService:(id)sender
{
    tblData.tag = 101;
    [self setTableFrame:0];
    [tblData reloadData];
}
- (IBAction)actionOnSelectSource:(id)sender
{
    tblData.tag = 102;
    [self setTableFrame:102];
    [tblData reloadData];
}
- (IBAction)actionOnFlatRate:(id)sender
{
    [_buttonFlatRate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_buttonTimeAndMaterial setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    isTimeAndMaterial = NO;
    _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewTimeAndMaterial.constant;
    _heightViewTimeAndMaterial.constant = 0.0;
    _heightViewTimeAndMaterial.constant = 0.0;
    [_buttonSplitPrice setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
}

- (IBAction)actionOnTimeAndMaterial:(id)sender
{
    [_buttonTimeAndMaterial setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_buttonFlatRate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    isTimeAndMaterial = YES;
    
    if(isSplitPrice)
    {
        _heightViewTimeAndMaterial.constant = 450.0;
    }
    else
    {
        _heightViewTimeAndMaterial.constant = 250.0;
    }
    
    _heightMainViewContainer.constant = _heightMainViewContainer.constant+_heightViewTimeAndMaterial.constant;
}

- (IBAction)actionOnEstimateTime:(id)sender
{
    strPickerType = @"EstimateTime";
    [self addPickerViewDateTo:@""];
}

- (IBAction)actionOnSplitPrice:(id)sender
{
    //.png
    if(isSplitPrice)
    {
        _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewTimeAndMaterial.constant;
        _heightViewSplitPrice.constant = 0.0;
        _heightViewTimeAndMaterial.constant = 250.0;
        _heightMainViewContainer.constant = _heightMainViewContainer.constant+_heightViewTimeAndMaterial.constant;
        [_buttonSplitPrice setImage:[UIImage imageNamed:@"check_box_1"] forState:UIControlStateNormal];
        isSplitPrice = NO;
    }
    else
    {
        _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewTimeAndMaterial.constant;
        _heightViewSplitPrice.constant = 200.0;
        _heightViewTimeAndMaterial.constant = 450.0;
        _heightMainViewContainer.constant = _heightMainViewContainer.constant+_heightViewTimeAndMaterial.constant;
        [_buttonSplitPrice setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        isSplitPrice = YES;
    }
}
- (IBAction)actionOnStandardHour:(id)sender
{
    if(isAfterHour)
    {
        [_buttonStandardHour setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonAfterHour setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isAfterHour = NO;
        
        _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewHolidayAndSpecificHour.constant;
        
        _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewSpecificHour.constant;
        
        _heightViewHolidayAndSpecificHour.constant = 0.0;
        _heightViewSpecificHour.constant = 0.0;
    }
}
- (IBAction)actionOnAfterHour:(id)sender
{
    if(isAfterHour==NO)
    {
        [_buttonAfterHour setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonStandardHour setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isAfterHour = YES;
        _heightViewHolidayAndSpecificHour.constant = 50.0;
        _heightMainViewContainer.constant = _heightMainViewContainer.constant+_heightViewHolidayAndSpecificHour.constant;
    }
}

- (IBAction)actionOnHolidayHour:(id)sender
{
    if(isSpecificHour)
    {
        [_buttonHolidayHour setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonSpecificHour setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isSpecificHour = NO;
        
        _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewSpecificHour.constant;
        _heightViewSpecificHour.constant = 0.0;
    }
}

- (IBAction)actionOnSpecificHour:(id)sender
{
    if(isSpecificHour==NO)
    {
        [_buttonSpecificHour setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        
        [_buttonHolidayHour setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isSpecificHour = YES;
        _heightViewSpecificHour.constant = 50.0;
        _heightMainViewContainer.constant = _heightMainViewContainer.constant+_heightViewSpecificHour.constant;
    }
}

- (IBAction)actionOnRequireClientApproval:(id)sender
{
    if(isRequireClientApproval)
    {
        [_buttonRequireClientApproval setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        isRequireClientApproval = NO;
    }
    else
    {
        [_buttonRequireClientApproval setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        isRequireClientApproval = YES;
    }
}
- (IBAction)actionOnSelectAddress_ServiceAddress:(id)sender
{
    tblData.tag = 104;
    [self setTableFrame:0];
    [tblData reloadData];
}
- (IBAction)actionOnSelectCountry_ServiceAddress:(id)sender
{
    if(arrayCountries.count>0)
    {
        tblData.tag = 109;
        [self setTableFrame:0];
        [tblData reloadData];
    }
}
- (IBAction)actionOnSelectState_ServiceAddress:(id)sender
{
    if(arrayState.count>0)
    {
        tblData.tag = 110;
        [self setTableFrame:0];
        [tblData reloadData];
    }
}
- (IBAction)actionOnIsTaxExempt:(id)sender
{
    if(isTaxExempt)
    {
        [_buttonIsTaxExempt setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        isTaxExempt = NO;
        _heightMainViewContainer.constant = _heightMainViewContainer.constant-_heightViewTaxExemption.constant;
        _heightViewTaxExemption.constant = 0.0;
        
    }
    else
    {
        [_buttonIsTaxExempt setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        isTaxExempt = YES;
        _heightViewTaxExemption.constant = 100.0;
        _heightMainViewContainer.constant = _heightMainViewContainer.constant+_heightViewTaxExemption.constant;
    }
}

- (IBAction)actionOnServiceDate_OtherInformation:(id)sender
{
    strPickerType = @"ServiceDate";
    [self addPickerViewDateTo:@""];
}
- (IBAction)actionOnScheduleServiceOrder:(id)sender
{
    if(isScheduleServiceOrder==NO)
    {
        [_buttonScheduleServiceOrder setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonScheduleEstimate setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isScheduleServiceOrder = YES;
    }
}
- (IBAction)actionOnScheduleEstimate:(id)sender
{
    if(isScheduleServiceOrder==YES)
    {
        [_buttonScheduleEstimate setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonScheduleServiceOrder setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isScheduleServiceOrder = NO;
    }
}

- (IBAction)actionOnSpecificTime:(id)sender
{
    if(isSpecificTime==NO)
    {
        [_buttonSpecificTime setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonRangeTime setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isSpecificTime = YES;
        [_buttonSpecificOrRangeTime setTitle:@"Select" forState:UIControlStateNormal];
    }
}
- (IBAction)actionOnRangeTime:(id)sender
{
    if(isSpecificTime==YES)
    {
        [_buttonRangeTime setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonSpecificTime setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isSpecificTime = NO;
        [_buttonSpecificOrRangeTime setTitle:@"Select" forState:UIControlStateNormal];
        
    }
}
- (IBAction)actionOnSpecificOrRangeTime:(id)sender
{
    if(isSpecificTime)
    {
        //show picker
        strPickerType = @"SpecificTime";
        [self addPickerViewDateTo:@""];
    }
    else
    {
        // reload table
        tblData.tag = 107;
        [self setTableFrame:0];
        [tblData reloadData];
    }
}
- (IBAction)actionOnSelectRoute:(id)sender
{
    if(arrayPrimaryRoutes.count>0)
    {
        tblData.tag = 108;
        [self setTableFrame:0];
        [tblData reloadData];
    }
}
- (IBAction)actionOnSelectSpecificHour:(id)sender
{
    if(arraySpecificTime.count>0)
    {
        tblData.tag = 106;
        [self setTableFrame:0];
        [tblData reloadData];
    }
}
- (IBAction)actionOnSave:(id)sender
{
    
    BOOL isSelectedSubWorkOrderRate = [self checkIfSelectedSubWoType];
    
    //
    //    if(!(_textFieldAccountNumber.text.length>0))
    //    {
    //        [global AlertMethod:Alert :@"Please enter account number"];
    //    }
    if (!(_textFieldContact.text.length>0))
    {
        [global AlertMethod:Alert :@"Please enter contact"];
    }
    else if(!(dictService.count>0))
    {
        [global AlertMethod:Alert :@"Please select service"];
    }
    //    else if(!(dictSource.count>0))
    //    {
    //        [global AlertMethod:Alert :@"Please select source"];
    //    }
    else if(!(arraySourceSelected.count>0))
    {
        [global AlertMethod:Alert :@"Please select source"];
    }
    else if([_buttonSelectJobType.titleLabel.text isEqualToString:@"Select"])
    {
        [global AlertMethod:Alert :@"Please select job type"];
    }
    else if (!isSelectedSubWorkOrderRate) {
        
        [global AlertMethod:Alert :@"Please provide sub work order rate"];
    }
    else if(!(_textViewJobDescription.text.length>0))
    {
        [global AlertMethod:Alert :@"Please enter job description"];
    }
    else if (([_btnContact_ServiceAddress.currentTitle isEqualToString:@"Select Contact"]))//_textFieldContact_ServiceAddress
    {
        [global AlertMethod:Alert :@"Please select contact below service address"];
    }
    else if (!(_textFieldAddressLine1_ServiceAddress.text.length>0))
    {
        [global AlertMethod:Alert :@"Please enter address line1"];
    }
    else if([_buttonSelectCountry_ServiceAddress.titleLabel.text isEqualToString:@"Select Country"])
    {
        [global AlertMethod:Alert :@"Please select country"];
    }
    else if([_buttonSelectState_ServiceAddress.titleLabel.text isEqualToString:@"Select State"])
    {
        [global AlertMethod:Alert :@"Please select state"];
    }
    else if (!(_textFieldCity_ServiceAddress.text.length>0))
    {
        [global AlertMethod:Alert :@"Please enter city"];
    }
    else if (!(_textFieldZipCode_ServiceAddress.text.length>0))
    {
        [global AlertMethod:Alert :@"Please enter zip code"];
    }
    else if(!(dictPrimaryRoute.count>0))
    {
        [global AlertMethod:Alert :@"Please select primary route"];
    }
    //    else if (!(_textFieldLatitude_ServiceAddress.text.length>0))
    //    {
    //        [global AlertMethod:Alert :@"Please enter latitude"];
    //    }
    //    else if (!(_textFieldLongitude_ServiceAddress.text.length>0))
    //    {
    //        [global AlertMethod:Alert :@"Please enter longitude"];
    //    }
    else
    {
        NSString *strDirection = @"";
        NSString *InternalInstruction = @"";
        NSString *OtherInstruction = @"";
        NSString *ServiceInstruction = @"";
        NSString *SpecialInstructions = @"";
        NSString *strRouteMasterID = @"";
        NSString *strServiceAddressId = @"";
        NSString *strAfterHrs = @"";
        NSString *strSourceID = @"";
        NSString *strServiceSysName = @"";
        NSString *strServicePOCID = @"";
        NSString *strWOType = @"";
        NSString *strSubWOType = @"";
        NSString *strPlumbingEstimateTime = @"";
        NSString *strPriceNotExceed = @"";
        NSString *strLabourPercent = @"";
        NSString *strPartPercent = @"";
        NSString *strIsHolidayHrs = @"";
        NSString *strScheduleStartDate = @"";
        NSString *strScheduleEndDate = @"";
        NSString *strIsStandard = @"";
        NSString *strTime = @"";
        NSString *strJOBType = @"";
        NSString *strTaxExempt = @"";
        NSString *strIsTaxExempt = @"";
        NSString *strNotes = @"";
        NSString *strMapCode = @"";
        NSString *strGateCode = @"";
        NSString *strCustomerPONumber = @"";//CustomerPONumber
        NSString *strIsRequiredClientApproval = @"";
        NSString *strLaborType = @"";
        
        if(isRequireClientApproval== YES)
        {
            strIsRequiredClientApproval = @"true";
        }
        else
        {
            strIsRequiredClientApproval = @"false";
        }
        if(_textFieldPONumbers.text.length>0)
        {
            strCustomerPONumber = _textFieldPONumbers.text;
        }
        if(_textViewDirection.text.length>0)
        {
            strDirection = _textViewDirection.text;
        }
        if(_textViewInternalInstruction.text.length>0)
        {
            InternalInstruction = _textViewInternalInstruction.text;
        }
        if(_textViewServiceInstruction.text.length>0)
        {
            ServiceInstruction = _textViewServiceInstruction.text;
        }
        if(_textViewSpecialInstruction.text.length>0)
        {
            SpecialInstructions = _textViewSpecialInstruction.text;
        }
        if(dictPrimaryRoute.count>0)
        {
            strRouteMasterID = [NSString stringWithFormat:@"%@",[dictPrimaryRoute valueForKey:@"RouteId"]];
        }
        if(dictServiceAddress.count>0)
        {
            strServiceAddressId = [NSString stringWithFormat:@"%@",[dictServiceAddress valueForKey:@"CustomerAddressId"]];
        }
        //        if(dictSource.count>0)
        //        {
        //            strSourceID = [NSString stringWithFormat:@"%@",[dictSource valueForKey:@"SourceId"]];
        //        }
        if(arraySourceSelected.count>0)
        {
            NSMutableArray *arraySourceIds = [NSMutableArray new];
            for(NSDictionary *dict in arraySourceSelected)
            {
                //                strSourceID = [strSourceID stringByAppendingString:@"%@",[NSString stringWithFormat:@"%@",[dict valueForKey:@"SourceId"]]];
                [arraySourceIds addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SourceId"]]];
                
                strSourceID = [arraySourceIds componentsJoinedByString:@","];
                
            }
            //            strSourceID = [NSString stringWithFormat:@"%@",[dictSource valueForKey:@"SourceId"]];
        }
        if(dictService.count>0)
        {
            strServiceSysName = [dictService valueForKey:@"ServiceSysName"];
            strServicePOCID =  [NSString stringWithFormat:@"%@",[dictService valueForKey:@"ServiceId"]];
        }
        if(![_buttonSelectSpecificHour.titleLabel.text isEqualToString:@"Select"])
        {
            strAfterHrs = _buttonSelectSpecificHour.titleLabel.text;
        }
        if(isScheduleServiceOrder)
        {
            strWOType = @"SO";
        }
        else
        {
            strWOType = @"SE";
        }
        if(isTimeAndMaterial)
        {
            strSubWOType = @"TM";
            if(_buttonEstimateTime.titleLabel.text.length>0)
            {
                strPlumbingEstimateTime = _buttonEstimateTime.titleLabel.text;
            }
            if(_textFieldPriceNotExceed.text.length>0)
            {
                strPriceNotExceed = _textFieldPriceNotExceed.text;
            }
        }
        else
        {
            strSubWOType = @"FR";
            strPlumbingEstimateTime = @"";
            strPriceNotExceed = @"";
            
        }
        if(isSplitPrice)
        {
            float lbrAndPart = [_textFieldLabor.text floatValue]+[_textFieldPart.text floatValue];
            if(lbrAndPart<100.0)
            {
                [global AlertMethod:Alert :@"Labor And Part Percent Must Be 100%"];
                return;
            }
            else
            {
                strLabourPercent = _textFieldLabor.text;
                strPartPercent = _textFieldPart.text;
            }
        }
        else
        {
            strLabourPercent = @"";
            strPartPercent = @"";
        }
        
        if(isAfterHour)
        {
            strIsStandard = @"false";
            if(isSpecificHour)
            {
                strIsHolidayHrs = @"false";
                if(![_buttonSelectSpecificHour.titleLabel.text isEqualToString:@"Select"])
                {
                    strAfterHrs = _buttonSelectSpecificHour.titleLabel.text;
                }
                else
                {
                    strAfterHrs = @"";
                }
            }
            else
            {
                strIsHolidayHrs = @"true";
                strAfterHrs = @"";
                
            }
        }
        else
        {
            strIsHolidayHrs = @"false";
            strIsStandard = @"true";
        }
        
        if(isSpecificHour)
        {
            strIsHolidayHrs = @"false";
        }
        else
        {
            strIsHolidayHrs = @"true";
        }
        
        
        if(isSpecificTime)
        {
            if([_buttonSpecificOrRangeTime.titleLabel.text isEqualToString:@"Select"])
            {
                [global AlertMethod:Alert :@"Please select specific time"];
                return;
            }
            else
            {
                strScheduleStartDate = [NSString stringWithFormat:@"%@ %@",_buttonServiceDate.titleLabel.text,_buttonSpecificOrRangeTime.titleLabel.text];
                strTime = _buttonSpecificOrRangeTime.titleLabel.text;
            }
        }
        else
        {
            if([_buttonSpecificOrRangeTime.titleLabel.text isEqualToString:@"Select"])
            {
                [global AlertMethod:Alert :@"Please select specific time"];
                return;
            }
            else
            {
                NSArray *arry = [_buttonSpecificOrRangeTime.titleLabel.text componentsSeparatedByString:@"-"];
                if(arry.count>0)
                {
                    strScheduleStartDate = [NSString stringWithFormat:@"%@ %@",_buttonServiceDate.titleLabel.text,[arry firstObject]];
                    strTime = [arry firstObject];
                }
            }
        }
        
        // working for schedule end date
        if(dictService.count>0)
        {
            /********************************************/
            NSArray *components1 = [strTime componentsSeparatedByString:@":"];
            NSArray *components2 = [[dictService valueForKey:@"InitialEstimatedDuration"] componentsSeparatedByString:@":"];
            
            NSInteger hours1   = [[components1 objectAtIndex:0] integerValue];
            NSInteger minutes1 = [[components1 objectAtIndex:1] integerValue];
            NSInteger seconds1 = [[components1 objectAtIndex:2] integerValue];
            
            NSNumber *time1 = [NSNumber numberWithInteger:(hours1 * 60 * 60) + (minutes1 * 60) + seconds1];
            
            NSInteger hours2   = [[components2 objectAtIndex:0] integerValue];
            NSInteger minutes2 = [[components2 objectAtIndex:1] integerValue];
            NSInteger seconds2 = [[components2 objectAtIndex:2] integerValue];
            
            NSNumber *time2 = [NSNumber numberWithInteger:(hours2 * 60 * 60) + (minutes2 * 60) + seconds2];
            
            NSString *str1 = [time1 stringValue];
            NSString *str2 = [time2 stringValue];
            float totalTime = [str1 floatValue]+[str2 floatValue];
            NSString *str3 = [NSString stringWithFormat:@"%f",totalTime];
            
            int seconds = [str3 intValue] % 60;
            int minutes = ([str3 intValue] / 60) % 60;
            int hours = [str3 intValue] / 3600;
            
            
            NSString *strTotalHR = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
            NSLog(@"%@",[NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds]);
            
            /********************************************/
            
            strScheduleEndDate = [NSString stringWithFormat:@"%@ %@",_buttonServiceDate.titleLabel.text,strTotalHR];
            
        }
        else
        {
            [global AlertMethod:Alert :@"Please select service"];
        }
        
        if([_buttonSelectJobType.titleLabel.text isEqualToString:@"New Construction"])
        {
            strJOBType = @"NewConstruction";
        }
        else
        {
            strJOBType = @"Repair/Remodeling Restoration";
        }
        
        if(isTaxExempt)
        {
            strIsTaxExempt = @"true";
            if(_textFieldTaxExemptionNumber.text.length>0)
            {
                strTaxExempt = _textFieldTaxExemptionNumber.text;
            }
            else
            {
                strTaxExempt = @"";
            }
        }
        else
        {
            strIsTaxExempt = @"false";
            strTaxExempt = @"";
        }
        
        if(_textViewServiceNotes.text.length>0)
        {
            strNotes = _textViewServiceNotes.text;
        }
        else
        {
            strNotes = @"";
        }
        if(_textFieldMapCode_ServiceAddress.text.length>0)
        {
            strMapCode = _textFieldMapCode_ServiceAddress.text;
        }
        else
        {
            strMapCode = @"";
        }
        if(_textFieldServiceGateCode_ServiceAddress.text.length>0)
        {
            strGateCode =_textFieldServiceGateCode_ServiceAddress.text;
        }
        else
        {
            strGateCode = @"";
        }
        
        if (isMechanic) {
            strLaborType=@"L";
        } else {
            strLaborType=@"H";
        }
        
        //[global strCurrentDate]
        
        // @"CreatedBy":[dictLoginDetails valueForKey:@"EmployeeName"],  before this was going
        
        NSDictionary *dict1 = @{@"CompanyKey":strCompanyKey,
                                @"CreatedBy":[dictLoginDetails valueForKey:@"EmployeeName"],
                                @"CreatedDate":[global strCurrentDate],
                                @"EquipmentCode":@"",
                                @"EquipmentName":@"",
                                @"IsActive":@"true",
                                @"Manufacturer":@"",
                                @"ModelNumber":@"",
                                @"ModifiedBy":[dictLoginDetails valueForKey:@"EmployeeName"],
                                @"ModifiedDate":[global strCurrentDate],
                                @"Priority":@"High",
                                @"SerialNumber":@"",
                                @"ServiceIssue":_textViewJobDescription.text,
                                @"SubWorkOrderId":@"",
                                @"SubWorkOrderIssueId":@""
                                };
        NSMutableArray *arrayObj = [NSMutableArray new];
        [arrayObj addObject:dict1];
        
        NSDictionary *dict = @{@"AccountId":[NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"AccountId"]],
                               @"AccountNo":[NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"AccountNo"]],
                               @"AfterHrsDuration":strAfterHrs,
                               @"BillingAddressId":strServiceAddressId,
                               @"BillingPOCId":[NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"PrimaryContactId"]],
                               @"CompanyId":strCompanyID,
                               @"Directions":strDirection,
                               @"EmployeeNo":strEmployeeNumber,
                               @"InternalInstruction":InternalInstruction,
                               @"IsHolidayHrs":strIsHolidayHrs,
                               @"IsSameAsServiceAddress":@"true",
                               @"JobType":strJOBType,
                               @"LaborPercent":strLabourPercent,
                               @"LoggedInEmployeeId":strEmployeeID,
                               @"LoggedInuserName":strLoggedInUserName,
                               @"OtherInstruction":@"",
                               @"PartPercent":strPartPercent,
                               @"PlumbingEstimationTime":strPlumbingEstimateTime,
                               @"PriceNotToExceed":strPriceNotExceed,
                               @"PrimaryServiceSysName":strServiceSysName,
                               @"RouteMasterId":strRouteMasterID,
                               @"ScheduleEndDate":strScheduleEndDate,
                               @"ScheduleStartDate":strScheduleStartDate,
                               @"ServiceAddressId":strServiceAddressId,
                               @"ServiceInstruction":ServiceInstruction,
                               @"ServicePOCId":strServicePOCid,//@"ServicePOCId":[NSString stringWithFormat:@"%@",[_dictSearchData valueForKey:@"PrimaryContactId"]]
                               @"SourceId":strSourceID,
                               @"SpecialInstructions":SpecialInstructions,
                               @"SubWOType":strSubWOType,
                               @"WorkOrderType":strWOType,
                               @"isStandard":strIsStandard,
                               @"MapCode":strMapCode,
                               @"GateCode":strGateCode,
                               @"IsTaxExempt":strIsTaxExempt,
                               @"TaxExemption":strTaxExempt,
                               @"Notes":strNotes,
                               @"SubWorkOrderIssueDcs":arrayObj,
                               @"CustomerPONumber":strCustomerPONumber,
                               @"IsClientApprovalReq":strIsRequiredClientApproval,
                               @"LaborType":strLaborType,
                               @"TaxSysName":strTaxcodeSysName
                               };
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            [global AlertMethod:Alert :ErrorInternetMsg];
        }
        else
        {
            
            NSArray *arr = [NSArray new];
            
            NSString *strJson = [global convertIntoJson:dict :arr :@"dict" :@"Create Workorder json"];
            
            [self postFinalDataOnServer:dict];
            
        }
    }
}

- (IBAction)actionOnCancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionOnJobType:(id)sender
{
    tblData.tag = 103;
    [self setTableFrame:0];
    [tblData reloadData];
}

- (IBAction)actionOnAddressSubType:(id)sender
{
    tblData.tag = 105;
    [self setTableFrame:0];
    [tblData reloadData];
}

- (IBAction)actionOnTaxcode:(id)sender {
    if(arrayTaxcode.count==0)
    {
        [global AlertMethod:Alert :NoDataAvailable];
    }
    else
    {
        tblData.tag = 111;
        [self setTableFrame:0];
        [tblData reloadData];
    }
}

-(void)configureUI:(UIView*)view
{
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
    view.layer.cornerRadius = 4.0;
}

-(void)addPaddingInTextFields:(UITextField*)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

-(void)setTableFrame:(int)tag
{
    if(tag==102)
    {
        buttonBackground=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        buttonBackground.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
        
        [buttonBackground addTarget:self action:@selector(actionOnBackground:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: buttonBackground];
        
        tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
        
        tblData.layer.borderWidth = 1.0;
        tblData.layer.borderColor = [[UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0] CGColor];
        
        if([UIScreen mainScreen].bounds.size.height>1000)
        {
            tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
        }
        
        buttonOK = [[UIButton alloc] initWithFrame:CGRectMake((tblData.frame.size.width/2)-50, CGRectGetMaxY(tblData.frame)+10, 100, 50)];
        
        buttonOK.backgroundColor = [UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0];
        [buttonOK setTitle:@"OK" forState:UIControlStateNormal];
        //        buttonOK.titleLabel.text = @"OK";
        buttonOK.titleLabel.textColor = [UIColor whiteColor];
        buttonOK.titleLabel.font = [UIFont systemFontOfSize:22.0];
        [buttonOK addTarget:self action:@selector(actionOnOk_Source:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self.view addSubview:tblData];
        [self.view addSubview:buttonOK];
        
    }
    else
    {
        buttonBackground=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        buttonBackground.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
        
        [buttonBackground addTarget:self action:@selector(actionOnBackground:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: buttonBackground];
        
        tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
        
        tblData.layer.borderWidth = 1.0;
        tblData.layer.borderColor = [[UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0] CGColor];
        
        if([UIScreen mainScreen].bounds.size.height>1000)
        {
            tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
        }
        
        [self.view addSubview:tblData];
    }
}

-(void)addPickerViewDateTo :(NSString*)strDateString
{
    pickerDate=[[UIDatePicker alloc]init];
    
    if([strPickerType isEqualToString:@"SpecificTime"])
    {
        pickerDate.datePickerMode = UIDatePickerModeTime;
    }
    else if ([strPickerType isEqualToString:@"EstimateTime"])
    {
        pickerDate.datePickerMode = UIDatePickerModeTime;
        [pickerDate setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    }
    else if ([strPickerType isEqualToString:@"ServiceDate"])
    {
            pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    }
    
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    
    // [pickerDate setMinimumDate:[NSDate date]];
    
    //    if (!(strDateString.length==0))
    //    {
    //        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //        [dateFormat setDateFormat:@"MM/dd/yyyy"];
    //        NSDate *dateToSett = [dateFormat dateFromString:strDateString];
    //
    //        if (dateToSett==nil)
    //        {
    //
    //        }
    //        else
    //        {
    //            pickerDate.date =dateToSett;
    //        }
    //    }
    
    //        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround1.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround1];
    
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround1 setUserInteractionEnabled:YES];
    [viewBackGround1 addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround1 addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT TIME";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)tapDetectedOnBackGroundView
{
    [viewForDate removeFromSuperview];
    [viewBackGround1 removeFromSuperview];
    
}
-(void)actionOnBackground:(id)sender
{
    if(tblData.tag==102)
    {
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        [buttonOK removeFromSuperview];
    }
    else
    {
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    
}

- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround1 removeFromSuperview];
}
-(void)actionOnOk_Source:(id)sender
{
    [tblData removeFromSuperview];
    [buttonBackground removeFromSuperview];
    [buttonOK removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    if([strPickerType isEqualToString:@"SpecificTime"])
    {
        [dateFormat setDateFormat:@"hh:mm:ss a"];
        
        strSpecificTime = [dateFormat stringFromDate:pickerDate.date];
        [_buttonSpecificOrRangeTime setTitle:strSpecificTime forState:UIControlStateNormal];
    }
    if([strPickerType isEqualToString:@"EstimateTime"])
    {
        [dateFormat setDateFormat:@"hh:mm"];
        
        strEstimateTime = [dateFormat stringFromDate:pickerDate.date];
        [_buttonEstimateTime setTitle:strEstimateTime forState:UIControlStateNormal];
    }
    else if ([strPickerType isEqualToString:@"ServiceDate"])
    {
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        
        strServiceDate = [dateFormat stringFromDate:pickerDate.date];
        [_buttonServiceDate setTitle:strServiceDate forState:UIControlStateNormal];
    }
    
    [viewForDate removeFromSuperview];
    [viewBackGround1 removeFromSuperview];
}

-(void)getServiceAddress
{
    //http://tsrs.stagingsoftware.com/api/MobileToServiceAuto/GetCustomerServiceAddress?companyKey=automation&accountNumber=582
    
    
    NSURL *strURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?companyKey=%@&accountNumber=%@",strServiceUrlMainServiceAutomation,URLGetSERVICEADDRESS,strCompanyKey,_strAccountNumber]];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
    
    NSURLSessionDataTask *data = [session dataTaskWithURL:strURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        //
        //            [DejalBezelActivityView removeView];
        //        });
        
        NSError *erro = nil;
        
        if (data!=nil)
        {
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
            
            if (json.count > 0)
            {
                NSLog(@"%@",json);
                arrayServiceAddress = json;
                if(arrayServiceAddress.count>0)
                {
                    strServiceAddressID = [NSString stringWithFormat:@"%@",[[arrayServiceAddress firstObject] valueForKey:@"AddressTypeId"]];
                    dictServiceAddress = [arrayServiceAddress firstObject];
                    arrayContact_ServiceAddress = [dictServiceAddress valueForKey:@"AddressContactExtSerDcs"];
                    [self getAllPrimaryRoutes];
                    [self getSpecificHour];
                    dispatch_sync(dispatch_get_main_queue(),^{
                        
                        if ([[dictServiceAddress valueForKey:@"AddressContactExtSerDcs"] isKindOfClass:[NSArray class]]) {
                            
                            if([[dictServiceAddress valueForKey:@"AddressContactExtSerDcs"] count]>0)
                            {
                                BOOL isPrimaryFound = NO;
                                
                                for (NSDictionary *dictCont in arrayContact_ServiceAddress)
                                {
                                    if([[dictCont valueForKey:@"IsPrimary"] intValue] == 1)
                                    {
                                        [_btnContact_ServiceAddress setTitle:[dictCont valueForKey:@"ContactName"] forState:UIControlStateNormal];
                                        strServicePOCid = [dictCont valueForKey:@"CrmContactId"];
                                        isPrimaryFound = YES;
                                        break;
                                    }
                                }
                                if(isPrimaryFound == NO)// then show 0 index contact name
                                {
                                    [_btnContact_ServiceAddress setTitle:[[arrayContact_ServiceAddress firstObject]valueForKey:@"ContactName"] forState:UIControlStateNormal];
                                }
                                
                            }
                            
                        }
                        
                        /*
                        if([[dictServiceAddress valueForKey:@"AddressContactExtSerDcs"] count]>0 || ![[dictServiceAddress valueForKey:@"AddressContactExtSerDcs"] isEqualToString:@"<null>"] )
                        {
                            BOOL isPrimaryFound = NO;
                            
                            for (NSDictionary *dictCont in arrayContact_ServiceAddress)
                            {
                                if([[dictCont valueForKey:@"IsPrimary"] intValue] == 1)
                                {
                                    [_btnContact_ServiceAddress setTitle:[dictCont valueForKey:@"ContactName"] forState:UIControlStateNormal];
                                    strServicePOCid = [dictCont valueForKey:@"CrmContactId"];
                                    isPrimaryFound = YES;
                                    break;
                                }
                            }
                            if(isPrimaryFound == NO)// then show 0 index contact name
                            {
                                [_btnContact_ServiceAddress setTitle:[[arrayContact_ServiceAddress firstObject]valueForKey:@"ContactName"] forState:UIControlStateNormal];
                            }
                            
                        }
                         */
                        
                        [self showServiceAddressData:[arrayServiceAddress firstObject]];
                    });
                }
            }
        }
        dispatch_sync(dispatch_get_main_queue(),^{
            
            
        });
    }];
    
    [data resume];
}

-(void)showServiceAddressData:(NSDictionary*)dict
{
    [_buttonSelectServiceAddress setTitle:[dict valueForKey:@"CombinedAddress"] forState:UIControlStateNormal];
    _textFieldContact_ServiceAddress.text = _textFieldContact.text;
    
    if([[dict valueForKey:@"Address1"] length]>0&&![[dict valueForKey:@"Address1"] isEqualToString:@"<null>"])
    {
        _textFieldAddressLine1_ServiceAddress.text = [dict valueForKey:@"Address1"];
    }
    
    if(![[dict valueForKey:@"Address2"] isKindOfClass:[NSNull class]]&&[[dict valueForKey:@"Address2"] length]>0&&![[dict valueForKey:@"Address2"] isEqualToString:@"<null>"])
    {
        _textFieldAddressLine2_ServiceAddress.text = [dict valueForKey:@"Address2"];
    }
    
    if(![[dict valueForKey:@"CityName"] isKindOfClass:[NSNull class]]&&[[dict valueForKey:@"CityName"] length]>0&&![[dict valueForKey:@"CityName"] isEqualToString:@"<null>"])
    {
        _textFieldCity_ServiceAddress.text = [dict valueForKey:@"CityName"];
    }
    
    if(![[NSString stringWithFormat:@"%@",[dict valueForKey:@"Latitude"]] isKindOfClass:[NSNull class]]&&[[NSString stringWithFormat:@"%@",[dict valueForKey:@"Latitude"]] length]>0&&![[NSString stringWithFormat:@"%@",[dict valueForKey:@"Latitude"]] isEqualToString:@"<null>"])
    {
        _textFieldLatitude_ServiceAddress.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Latitude"]];
    }
    if(![[NSString stringWithFormat:@"%@",[dict valueForKey:@"Longitude"]] isKindOfClass:[NSNull class]]&&[[NSString stringWithFormat:@"%@",[dict valueForKey:@"Longitude"]] length]>0&&![[NSString stringWithFormat:@"%@",[dict valueForKey:@"Longitude"]] isEqualToString:@"<null>"])
    {
        _textFieldLongitude_ServiceAddress.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Longitude"]];
    }
    if(![[NSString stringWithFormat:@"%@",[dict valueForKey:@"MapCode"]] isKindOfClass:[NSNull class]]&&[[NSString stringWithFormat:@"%@",[dict valueForKey:@"MapCode"]] length]>0&&![[NSString stringWithFormat:@"%@",[dict valueForKey:@"MapCode"]] isEqualToString:@"<null>"])
    {
        _textFieldMapCode_ServiceAddress.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"MapCode"]];
    }
    if(![[NSString stringWithFormat:@"%@",[dict valueForKey:@"GateCode"]] isKindOfClass:[NSNull class]]&&[[NSString stringWithFormat:@"%@",[dict valueForKey:@"GateCode"]] length]>0&&![[NSString stringWithFormat:@"%@",[dict valueForKey:@"GateCode"]] isEqualToString:@"<null>"])
    {
        _textFieldServiceGateCode_ServiceAddress.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"GateCode"]];
    }
    
    if(![[dict valueForKey:@"Notes"] isKindOfClass:[NSNull class]]&&[[dict valueForKey:@"Notes"] length]>0&&![[dict valueForKey:@"Notes"] isEqualToString:@"<null>"])
    {
        _textViewServiceNotes.text = [dict valueForKey:@"Notes"];
    }
    
    if(![[dict valueForKey:@"Direction"] isKindOfClass:[NSNull class]]&&[[dict valueForKey:@"Direction"] length]>0&&![[dict valueForKey:@"Direction"] isEqualToString:@"<null>"])
    {
        _textViewDirection.text = [dict valueForKey:@"Direction"];
    }
    
    if(![[dict valueForKey:@"Zipcode"] isKindOfClass:[NSNull class]]&&[[dict valueForKey:@"Zipcode"] length]>0&&![[dict valueForKey:@"Zipcode"] isEqualToString:@"<null>"])
    {
        _textFieldZipCode_ServiceAddress.text = [dict valueForKey:@"Zipcode"];
    }
    
    if(![[dict valueForKey:@"County"] isKindOfClass:[NSNull class]]&&[[dict valueForKey:@"County"] length]>0&&![[dict valueForKey:@"County"] isEqualToString:@"<null>"])
    {
        _textFieldCounty.text = [dict valueForKey:@"County"];
    }
    
    if(![[dict valueForKey:@"SchoolDistrict"] isKindOfClass:[NSNull class]]&&[[dict valueForKey:@"SchoolDistrict"] length]>0&&![[dict valueForKey:@"SchoolDistrict"] isEqualToString:@"<null>"])
    {
        _textFieldSchoolDistict.text = [dict valueForKey:@"SchoolDistrict"];
    }
    
    // show country name and state name
    for(NSDictionary* dictCountry in arrayCountries)
    {
        if([[dict valueForKey:@"CountryId"] intValue]==[[dictCountry valueForKey:@"CountryId"] intValue])
        {
            [_buttonSelectCountry_ServiceAddress setTitle:[NSString stringWithFormat:@"%@",[dictCountry valueForKey:@"Name"]] forState:UIControlStateNormal];
            break;
            
        }
    }
    
    for(NSDictionary* dictState in arrayState)
    {
        if([[dict valueForKey:@"StateId"] intValue]==[[dictState valueForKey:@"StateId"] intValue])
        {
            [_buttonSelectState_ServiceAddress setTitle:[NSString stringWithFormat:@"%@",[dictState valueForKey:@"Name"]] forState:UIControlStateNormal];
            break;
            
        }
    }
    
    // resign keyboard
    [_textFieldContact_ServiceAddress resignFirstResponder];
    
    [_textFieldAddressLine1_ServiceAddress resignFirstResponder];
    
    [_textFieldAddressLine2_ServiceAddress resignFirstResponder];
    
    [_textFieldCity_ServiceAddress resignFirstResponder];
    
    [_textFieldLatitude_ServiceAddress resignFirstResponder];
    
    [_textFieldLongitude_ServiceAddress resignFirstResponder];
    
    [_textFieldMapCode_ServiceAddress resignFirstResponder];
    
    [_textFieldServiceGateCode_ServiceAddress resignFirstResponder];
    
    [_textViewServiceNotes resignFirstResponder];
    
    [_textViewDirection resignFirstResponder];
}
-(void)getSpecificHour
{
    NSURL *strURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?companyKey=%@&accountNo=%@&ServiceAddressId=%@",strServiceUrlMainServiceAutomation,URLGetHourConfigByAccountForMobileAsync,strCompanyKey,_strAccountNumber,strServiceAddressID]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    //http://tsrs.stagingsoftware.com/api/MobileToServiceAuto/GetHourConfigByAccountForMobileAsync?companyKey=automation&accountNo=582&ServiceAddressId=31260
    
    //http://tsrs.stagingsoftware.com/api/MobileToServiceAuto/GetHourConfigByAccountForMobileAsync?companyKey=Automation&accountNo=1211&ServiceAddressId=389
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
    });
    
    NSURLSessionDataTask *data = [session dataTaskWithURL:strURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [DejalBezelActivityView removeView];
        });
        
        NSError *erro = nil;
        
        if (data!=nil)
        {
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
            if (json.count > 0)
            {
                NSArray *jsonArry;
                for(NSDictionary *dict in json)
                {
                    //  NSString *serviceADD_ID = [NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceAddressId"]];
                    NSString *hrsDepartmentSysName = [dict valueForKey:@"DepartmentSysName"];
                    
                    if([hrsDepartmentSysName isEqualToString:[_dictDepartment valueForKey:@"SysName"]])//[serviceADD_ID isEqualToString:strServiceAddressID]
                    {
                        jsonArry = [dict valueForKey:@"AfterHourRateConfigDcs"];
                        
                        for(NSDictionary *dic in jsonArry)
                        {
                            NSString *dats1 = [NSString stringWithFormat:@"%@",[dic valueForKey:@"FromTime"]];
                            NSString *dats2 = [NSString stringWithFormat:@"%@",[dic valueForKey:@"ToTime"]];
                            
                            NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
                            [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
                            [dateFormatter3 setDateFormat:@"HH:mm:ss"];
                            NSDate *date1 = [dateFormatter3 dateFromString:dats1];
                            NSDate *date2 = [dateFormatter3 dateFromString:dats2];
                            NSLog(@"date1 : %@", date1);
                            
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            [formatter setDateFormat:@"hh:mm a"];
                            NSLog(@"Current Date: %@", [formatter stringFromDate:date1]);
                            NSLog(@"Current Date: %@", [formatter stringFromDate:date2]);
                            
                            NSString *strFromTime = [formatter stringFromDate:date1];
                            
                            NSString *strToTime = [formatter stringFromDate:date2];
                            
                            [arraySpecificTime addObject:[NSString stringWithFormat:@"%@ - %@",strFromTime,strToTime]];
                        }
                    }
                }
                NSLog(@"%@",json);
            }
        }
        dispatch_sync(dispatch_get_main_queue(),^{
            
            
        });
    }];
    
    [data resume];
}

-(void)getAllRangeOfTime
{
    NSURL *strURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?companyKey=%@",MainUrl,URLGETALLRANGEOFTIME,strCompanyKey]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    //
    //        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
    //    });
    
    NSURLSessionDataTask *data = [session dataTaskWithURL:strURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        //
        //            [DejalBezelActivityView removeView];
        //        });
        
        NSError *erro = nil;
        
        if (data!=nil)
        {
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
            if (json.count > 0)
            {
                arrayRangeTime = json;
            }
        }
        dispatch_sync(dispatch_get_main_queue(),^{
            
            
        });
    }];
    
    [data resume];
}

-(void)getAccountPastDue
{
    NSURL *strURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?companyKey=%@&accountNo=%@",MainUrl,URLGETACCOUNTPASTDUEBYACCOUNTNO,strCompanyKey,_strAccountNumber]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    //
    //        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
    //    });
    
    NSURLSessionDataTask *data = [session dataTaskWithURL:strURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        //
        //            [DejalBezelActivityView removeView];
        //        });
        
        //[DejalBezelActivityView removeView];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [DejalBezelActivityView removeView];
        });
        
        //        NSError *erro = nil;
        
        if (data!=nil)
        {
            NSString *strResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"%@",strResponse);
            if(strResponse.length>0)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    _labelTotalPastDue.text = [NSString stringWithFormat:@"$%.2f",[strResponse floatValue]];
                    
                    
                });
            }
            //            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
            //            if (json.count > 0)
            //            {
            //
            //            }
            
        }
        dispatch_sync(dispatch_get_main_queue(),^{
            
            
        });
    }];
    
    [data resume];
}

-(void)getAllPrimaryRoutes
{
    NSString *strBranchID = @"";
    strBranchID  = [NSString stringWithFormat:@"%@",[_dictBranch valueForKey:@"BranchMasterId"]];
    
    NSString *strAddressLatitude = @"";
    strAddressLatitude  = [NSString stringWithFormat:@"%@",_textFieldLatitude_ServiceAddress.text];
    
    NSString *strAddressLongitude = @"";
    strAddressLongitude = [NSString stringWithFormat:@"%@",_textFieldLongitude_ServiceAddress.text];
    
    NSString *str_ServiceDate = @"";
    str_ServiceDate  = _buttonServiceDate.titleLabel.text;
    
    NSString *strCustomerAddID = @"";
    strCustomerAddID = [NSString stringWithFormat:@"%@",[dictServiceAddress valueForKey:@"CustomerAddressId"]];
    
    
    NSURL *strURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?CompanyKey=%@&accountNumber=%@&addressLatitude=%@&addressLongitude=%@&scheduleDate=%@&customerAddressId=%@&branchid=%@",MainUrl,URLGetRoutesByCustomerServiceAddressAndDateNew,strCompanyKey,_strAccountNumber,strAddressLatitude,strAddressLongitude,str_ServiceDate,strCustomerAddID,strBranchID]];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
    });
    
    NSURLSessionDataTask *data = [session dataTaskWithURL:strURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [DejalBezelActivityView removeView];
        });
        
        NSError *erro = nil;
        
        if (data!=nil)
        {
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
            if (json.count > 0)
            {
                [arrayPrimaryRoutes removeAllObjects];
                for(NSDictionary *dict in json)
                {
                    if([[dict valueForKey:@"CustomerServiceDcs"] count]>0)
                    {
                        for(NSDictionary *dict1 in [dict valueForKey:@"CustomerServiceDcs"])
                        {
                            [arrayPrimaryRoutes addObject:dict1];
                        }
                    }
                }
            }
        }
        dispatch_sync(dispatch_get_main_queue(),^{
            
            
        });
    }];
    
    [data resume];
}
-(void)postFinalDataOnServer:(NSDictionary*)dictJson
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@?companyKey=%@",MainUrl,URLCreatePlumbingWorkOrderForMobile,strCompanyKey];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictJson options:NSJSONWritingPrettyPrinted error:Nil];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonDatastr %@",str);
    NSURL * serviceUrl = [NSURL URLWithString:strUrl];
    NSLog(@"REquest URL >> %@",serviceUrl);
    NSLog(@"REquest XML >> %@",str);
    
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:serviceUrl];
    
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *__block serviceResponse;
    NSError *__block serviceError;
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&serviceResponse error:&serviceError];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [DejalBezelActivityView removeView];
            
        });
        
        if (responseData)
        {
            [self parsePostApiData:responseData];
        }
        else
        {
            //        AlertViewClass *a = [[AlertViewClass alloc] init];
            //        [a showMessage:@"Cannot connect to internet." title:@"Skillgrok"];
        }
        
    });
}
-(void)parsePostApiData:(NSData *)response
{
    id jsonObject = Nil;
    
    NSString *charlieSendString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSLog(@"ResponseString %@",charlieSendString);
    
    if (response==nil)
    {
        NSLog(@"No internet connection.");
    }
    else
    {
        NSError *error = Nil;
        jsonObject =[NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            NSLog(@"Probably An Array %@",jsonObject);
            
            if([[[jsonObject objectAtIndex:1] valueForKey:@"Value"] boolValue]==1)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [global AlertMethod:@"Message" :[[jsonObject objectAtIndex:0] valueForKey:@"Value"]];
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
            else
            {
                [global AlertMethod:@"Message" :[[jsonObject objectAtIndex:0] valueForKey:@"Value"]];
            }
        }
        else
        {
            NSLog(@"Probably A Dictionary");
            NSDictionary *jsonDictionary=(NSDictionary *)jsonObject;
            NSLog(@"jsonDictionary %@",[jsonDictionary description]);
        }
    }
}
-(void)fetchJsonCountry
{
    arrayCountries=[[NSMutableArray alloc]init];
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:CountryJson];
    NSError * error;
    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    NSArray *arrOfCountry = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
    [arrayCountries addObjectsFromArray:arrOfCountry];
}
-(void)fetchJsonState
{
    arrayState=[[NSMutableArray alloc]init];
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:StateJson];
    NSError * error;
    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    NSArray *arrOfState = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
    [arrayState addObjectsFromArray:arrOfState];
}

- (IBAction)action_Mechanic:(id)sender {
    
    isMechanic=YES;
    [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
}

- (IBAction)action_Helper:(id)sender {
    
    isMechanic=NO;
    [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
}

-(BOOL)checkIfSelectedSubWoType{
    
    BOOL isSelected = YES;
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"RadioButton-Unselected.png"];
    
    UIImage *img = [_buttonFlatRate imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    
    UIImage *imageToCheckFor1 = [UIImage imageNamed:@"RadioButton-Unselected.png"];
    
    UIImage *img1 = [_buttonTimeAndMaterial imageForState:UIControlStateNormal];
    
    NSData *imgData11 = UIImagePNGRepresentation(imageToCheckFor1);
    
    NSData *imgData21 = UIImagePNGRepresentation(img1);
    
    BOOL isCompare1 =  [imgData11 isEqual:imgData21];
    
    if((isCompare==true) && (isCompare1==true))
    {
        
        isSelected = NO;
        
    }
    
    return isSelected;
    
}
- (IBAction)actionOnContact_ServiceAddress:(id)sender {
    
    if(arrayContact_ServiceAddress.count>0)
    {
        // show pop up list
        tblData.tag = 112;
        [self setTableFrame:0];
        [tblData reloadData];
    }
}


@end

