//
//  CreateWorkOrder_iPadViewController.h
//  DPS
//
//  Created by Akshay Hastekar on 28/03/18.
//  Copyright © 2018 Saavan. All rights reserved.
//
//  peStream

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"
@interface CreateWorkOrder_iPadViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textFieldAccountNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAccountName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldFirstName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldMiddleName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCompanyName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldZipCode;
@property (weak, nonatomic) IBOutlet UITableView *tableViewAccountDetails;

@property (weak, nonatomic) IBOutlet UIView *viewMainContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewMainContainer;

@property (weak, nonatomic) IBOutlet UIView *viewUpper;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewUpper;

@property (strong, nonatomic) IBOutlet UIView *viewPopUpSelectDepartment;

@property (weak, nonatomic) IBOutlet UIButton *buttonTransparent_SelectDepartment;

@property (weak, nonatomic) IBOutlet UIButton *buttonBranch;
@property (weak, nonatomic) IBOutlet UIButton *buttonDepartment;

@property (weak, nonatomic) IBOutlet UITextField *textFieldCounty;
@property (weak, nonatomic) IBOutlet UITextField *textFieldSchoolDictict;

@end
