//
//  CreateWorkOrder2_iPadViewController.h
//  DPS
//
//  Created by Akshay Hastekar on 04/04/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface CreateWorkOrder2_iPadViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate>

// properties

@property(strong,nonatomic) NSString *strAccountNumber;
@property(strong,nonatomic) NSString *strPhone;
@property(strong,nonatomic) NSString *strEmail;
@property(strong,nonatomic) NSString *strCompanyName;
@property(strong,nonatomic) NSDictionary *dictBranch;
@property(strong,nonatomic) NSDictionary *dictDepartment;
@property(strong,nonatomic) NSDictionary *dictSearchData;



// outlets

@property (weak, nonatomic) IBOutlet UITextField *textFieldAccountNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldContact;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCompanyName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPrimaryEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPrimaryPhone;
@property (weak, nonatomic) IBOutlet UITextField *textFieldSecondaryPhone;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCellNumber;

@property (weak, nonatomic) IBOutlet UITextField *textFieldBranch;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDepartment;

@property (weak, nonatomic) IBOutlet UITextField *textFieldAccountAlert;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalPastDue;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPriceNotExceed;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLabor;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPart;
@property (weak, nonatomic) IBOutlet UITextView *textViewJobDescription;
@property (weak, nonatomic) IBOutlet UITextField *textFieldTaxExemptionNumber;

// Service Address Outlets

@property (weak, nonatomic) IBOutlet UIButton *buttonSelectAddress_ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldContact_ServiceAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnContact_ServiceAddress;

@property (weak, nonatomic) IBOutlet UITextField *textFieldAddressLine1_ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAddressLine2_ServiceAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectCountry_ServiceAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectState_ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCity_ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldZipCode_ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLatitude_ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLongitude_ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldMapCode_ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldServiceGateCode_ServiceAddress;
@property (weak, nonatomic) IBOutlet UITextView *textViewServiceNotes;
@property (weak, nonatomic) IBOutlet UITextView *textViewDirection;

@property (weak, nonatomic) IBOutlet UIButton *buttonAddressSubType;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCounty;
@property (weak, nonatomic) IBOutlet UITextField *textFieldSchoolDistict;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectTaxcode;


// Other Information Outlets
@property (weak, nonatomic) IBOutlet UIButton *buttonServiceDate;
@property (weak, nonatomic) IBOutlet UIButton *buttonScheduleServiceOrder;
@property (weak, nonatomic) IBOutlet UIButton *buttonScheduleEstimate;
@property (weak, nonatomic) IBOutlet UIButton *buttonSpecificTime;
@property (weak, nonatomic) IBOutlet UIButton *buttonRangeTime;
@property (weak, nonatomic) IBOutlet UIButton *buttonSpecificOrRangeTime;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectPrimaryRoute;
@property (weak, nonatomic) IBOutlet UITextView *textViewSpecialInstruction;
@property (weak, nonatomic) IBOutlet UITextView *textViewServiceInstruction;
@property (weak, nonatomic) IBOutlet UITextView *textViewInternalInstruction;
@property (weak, nonatomic) IBOutlet UITextView *textViewSetUpInstruction;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPONumbers;

// UIButton Outlets

@property (weak, nonatomic) IBOutlet UIButton *buttonSelectService;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectJobType;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectSource;
@property (weak, nonatomic) IBOutlet UIButton *buttonFlatRate;
@property (weak, nonatomic) IBOutlet UIButton *buttonTimeAndMaterial;
@property (weak, nonatomic) IBOutlet UIButton *buttonEstimateTime;
@property (weak, nonatomic) IBOutlet UIButton *buttonSplitPrice;
@property (weak, nonatomic) IBOutlet UIButton *buttonStandardHour;
@property (weak, nonatomic) IBOutlet UIButton *buttonAfterHour;
@property (weak, nonatomic) IBOutlet UIButton *buttonHolidayHour;
@property (weak, nonatomic) IBOutlet UIButton *buttonSpecificHour;
@property (weak, nonatomic) IBOutlet UIButton *buttonRequireClientApproval;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectServiceAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonIsTaxExempt;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectSpecificHour;

@property (weak, nonatomic) IBOutlet UIView *viewTimeAndMaterial;
@property (weak, nonatomic) IBOutlet UIView *viewSplitPrice;
@property (weak, nonatomic) IBOutlet UIView *viewSpecificHour;
@property (weak, nonatomic) IBOutlet UIView *viewTaxExemption;

//Constraints outlets

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewTimeAndMaterial;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewSplitPrice;
@property (weak, nonatomic) IBOutlet UIView *viewHolidayAndSpecificHour;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewHolidayAndSpecificHour;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightButtonSelectSpecificHour;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMainViewContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewSpecificHour;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewTaxExemption;


@property (strong, nonatomic) IBOutlet UIButton *btnMechanic;
@property (strong, nonatomic) IBOutlet UIButton *btnHelper;
- (IBAction)action_Mechanic:(id)sender;
- (IBAction)action_Helper:(id)sender;

@end

