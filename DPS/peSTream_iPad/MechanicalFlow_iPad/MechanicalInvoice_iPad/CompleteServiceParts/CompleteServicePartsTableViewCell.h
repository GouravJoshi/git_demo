//
//  CompleteServicePartsTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 23/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompleteServicePartsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblPartName;
@property (weak, nonatomic) IBOutlet UILabel *lblPartDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet UILabel *lblAmountValue;
@property (weak, nonatomic) IBOutlet UILabel *lblIssueId;
@property (weak, nonatomic) IBOutlet UIButton *btnApproveForParts;
@property (weak, nonatomic) IBOutlet UIButton *btnDeclineForParts;
@property (strong, nonatomic) IBOutlet UIButton *btnViewMore;
@end
