//
//  MechanicalInvoiceViewController.h
//  DPS
//  peSTream
//  Created by Saavan Patidar on 31/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream test test etstet

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface MechanicalInvoiceViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UIActionSheetDelegate,NSFetchedResultsControllerDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UITextViewDelegate,AVAudioPlayerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entitySubWorkOrderIssues,*entityMechanicalSubWorkOrderHelper,*entityMechanicalSubWorkOrderNotes,*entityMechanicalSubWorkOrderActualHrs,*entitySubWorkOrderIssuesRepair,*entitySubWorkOrderIssuesRepairParts,*entitySubWorkOrderIssuesRepairLabour,*entityPaymentInfoForMechanicalServiceAuto,*entityWorkOrder,*entityPaymentInfoServiceAuto,*entitySubWOCompleteTimeExtSerDcs,*entitySubWoEmployeeWorkingTimeExtSerDcs,*entityTempSubWoEmployeeWorkingTimeExtSerDcs;
    
    
    NSManagedObjectContext *context;
    
    
    NSFetchRequest *requestSubWorkOrderIssues,*requestSubWorkOrderHelper,*requestSubWorkOrderNotes,*requestSubWorkOrderActualHrs,*requestSubWorkOrderIssuesRepair,*requestSubWorkOrderIssuesRepairParts,*requestSubWorkOrderIssuesRepairLabour,*requestPaymentInfo,*requestNewWorkOrder,*requestSubWOCompleteTimeExtSerDcs,*requestSubWoEmployeeWorkingTimeExtSerDcs,*requestTempSubWoEmployeeWorkingTimeExtSerDcs;
    
    
    NSSortDescriptor *sortDescriptorSubWorkOrderIssues,*sortDescriptorSubWorkOrderHelper,*sortDescriptorSubWorkOrderNotes,*sortDescriptorSubWorkOrderActualHrs,*sortDescriptorSubWorkOrderIssuesRepair,*sortDescriptorSubWorkOrderIssuesRepairParts,*sortDescriptorSubWorkOrderIssuesRepairLabour,*sortDescriptorPaymentInfo,*sortDescriptorWorkOrder,*sortDescriptorSubWOCompleteTimeExtSerDcs,*sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs;
    
    
    NSArray *sortDescriptorsSubWorkOrderIssues,*sortDescriptorsSubWorkOrderHelper,*sortDescriptorsSubWorkOrderNotes,*sortDescriptorsSubWorkOrderActualHrs,*sortDescriptorsSubWorkOrderIssuesRepair,*sortDescriptorsSubWorkOrderIssuesRepairParts,*sortDescriptorsSubWorkOrderIssuesRepairLabour,*sortDescriptorsPaymentInfo,*sortDescriptorsWorkOrder,*sortDescriptorsSubWOCompleteTimeExtSerDcs,*sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs;
    
    
    NSManagedObject *matchesSubWorkOrderIssues,*matchesSubWorkOrderHelper,*matchesSubWorkOrderNotes,*matchesSubWorkOrderActualHrs,*matchesSubWorkOrderIssuesRepair,*matchesSubWorkOrderIssuesRepairParts,*matchesSubWorkOrderIssuesRepairLabour,*matchesSubWOCompleteTimeExtSerDcs,*matchesSubWoEmployeeWorkingTimeExtSerDcs;
    
    
    NSArray *arrAllObjSubWorkOrderIssues,*arrAllObjSubWorkOrderHelper,*arrAllObjSubWorkOrderNotes,*arrAllObjSubWorkOrderActualHrs,*arrAllObjSubWorkOrderIssuesRepair,*arrAllObjSubWorkOrderIssuesRepairParts,*arrAllObjSubWorkOrderIssuesRepairLabour,*arrAllObjSubWOCompleteTimeExtSerDcs,*arrAllObjSubWoEmployeeWorkingTimeExtSerDcs,*arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs;
    
    //Nilind 27 June
    
    NSEntityDescription *entityImageDetail,*entityModifyDateServiceAuto,*entityEmailDetailService;
    NSFetchRequest *requestImageDetail;
    NSSortDescriptor *sortDescriptorImageDetail;
    NSArray *sortDescriptorsImageDetail;
    NSManagedObject *matchesImageDetail;
    NSArray *arrAllObjImageDetail;
    
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    
    NSEntityDescription *entityMechanicalSubWorkOrder;
    NSFetchRequest *requestMechanicalSubWorkOrder;
    NSSortDescriptor *sortDescriptorMechanicalSubWorkOrder;
    NSArray *sortDescriptorsMechanicalSubWorkOrder;
    NSManagedObject *matchesMechanicalSubWorkOrder;
    NSArray *arrAllObjMechanicalSubWorkOrder;
    
    //End
    
    NSEntityDescription *entitySalesDynamic;
    NSFetchRequest *requestNew;
    
    // Akshay
    NSEntityDescription *entityAccountDiscount,*entityWorkOrderAppliedDiscounts;
    NSFetchRequest *requestAccountDiscount,*requestWorkOrderAppliedDiscount;
    NSSortDescriptor *sortDescriptorAccDiscount,*sortDescriptorWorkOrderAppliedDiscount;
    NSArray *sortDescriptorsAccDiscount,*sortDescriptorsWorkOrderAppliedDiscount;
    // Akshay End
    
    NSEntityDescription *entityPspPlans;
    NSFetchRequest *requestPspPlans;
    NSSortDescriptor *sortDescriptorPspPlans;
    NSArray *sortDescriptorsPspPlans;
    NSManagedObject *matchesPspPlans;
    NSArray *arrAllObjPspPlans;
    
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerPspPlans;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

//View Customer Detail
@property (strong, nonatomic) IBOutlet UIView *viewForCustomerDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkOrderNo;
@property (weak, nonatomic) IBOutlet UILabel *lblAccountNo;
@property (weak, nonatomic) IBOutlet UILabel *lblSubworkOrderNo;
@property (weak, nonatomic) IBOutlet UILabel *lblNameValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceAddressValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingAddressValue;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailValue;

@property (weak, nonatomic) IBOutlet UILabel *lblTechanicianNameValue;


//View ServiceSummary
@property (strong, nonatomic) IBOutlet UIView *viewForServiceSummary;

//View Approve Table
@property (strong, nonatomic) IBOutlet UIView *viewForApproveTable;
@property (weak, nonatomic) IBOutlet UITableView *tblApprove;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableApprove_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_LblApprove_H;
@property (weak, nonatomic) IBOutlet UILabel *lblApprove;

//View Decline Table
@property (strong, nonatomic) IBOutlet UIView *viewForDeclineTable;
@property (weak, nonatomic) IBOutlet UITableView *tblDecline;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableDecline_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_LblDecline_H;
@property (weak, nonatomic) IBOutlet UILabel *lblDecline;

//View Complete Table
@property (strong, nonatomic) IBOutlet UIView *viewForCompleteTable;
@property (weak, nonatomic) IBOutlet UITableView *tblComplete;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableComplete_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_LblComplete_H;
@property (weak, nonatomic) IBOutlet UILabel *lblComplete;

//View Sign
@property (strong, nonatomic) IBOutlet UIView *viewForSign;
@property (weak, nonatomic) IBOutlet UIImageView *imgSignCheckBox;
@property (weak, nonatomic) IBOutlet UIView *viewInspectorSign;
@property (weak, nonatomic) IBOutlet UIImageView *imgInspectorSign;
@property (weak, nonatomic) IBOutlet UIButton *btnInspectorSign;
- (IBAction)actionOnCustomerNotPresent:(id)sender;
- (IBAction)actionOnInspectorSign:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewCustomerSign;
@property (weak, nonatomic) IBOutlet UIImageView *imgCustomerSign;
@property (weak, nonatomic) IBOutlet UIButton *btnCustomerSign;
- (IBAction)actionOnCustomerSign:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
- (IBAction)actionOnSubmit:(id)sender;

//ViewPayment Details
@property (strong, nonatomic) IBOutlet UIView *viewForPaymentDetail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewHours_H;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalHrs;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForHours;

@property (weak, nonatomic) IBOutlet UILabel *lblValueTotalApprovedRepairs;

@property (weak, nonatomic) IBOutlet UITextField *txtOtherDiscounts;
@property (weak, nonatomic) IBOutlet UIButton *btnDiagnosticCharge;

- (IBAction)actionOnDiagnosticCharge:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTipCharge;
- (IBAction)actionOnTipCharge:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForDiagnosticeCharge;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTipCharge;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForSubtotal;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTax;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmountDue;
@property (weak, nonatomic) IBOutlet UILabel *lblAmountPaid;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmountPaid;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForPSPCharge;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForPSPDiscount;


//View Other PaymentDetails
//@property (strong, nonatomic) IBOutlet UIView *viewForOtherPaymentDetail;

//View Service Issue
@property (strong, nonatomic) IBOutlet UIView *viewForServiceIssue;
@property (weak, nonatomic) IBOutlet UITableView *tblViewServiceIssue;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)actionBack:(id)sender;

//View Payment Mode
@property (strong, nonatomic) IBOutlet UIView *viewForPaymentMode;
- (IBAction)actionOnCash:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCash;
- (IBAction)actionOnCheck:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
- (IBAction)actionOnCreditCard:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCreditCard;
- (IBAction)actionOnBill:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBill;
- (IBAction)actionOnAutoChargeCustomer:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAutoChargeCustomer;
- (IBAction)actionOnPaymentPending:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPaymentPending;
- (IBAction)actionOnNoChange:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNoChange;

//View For CheckDetail
@property (strong, nonatomic) IBOutlet UIView *viewForCheckDetail;
@property (weak, nonatomic) IBOutlet UITextField *txtAmountCheckView;
@property (weak, nonatomic) IBOutlet UITextField *txtCheckNo;

@property (weak, nonatomic) IBOutlet UITextField *txtDrivingLicenseNo;
@property (weak, nonatomic) IBOutlet UIButton *btnExpirationDate;
- (IBAction)actionOnExpirationDate:(id)sender;
- (IBAction)actionOnCheckFrontImage:(id)sender;
- (IBAction)actionOnCheckBackImage:(id)sender;

//View For Single Amountdiag
@property (strong, nonatomic) IBOutlet UIView *viewForSingleAmount;
@property (weak, nonatomic) IBOutlet UITextField *txtAmountSingleAmount;
@property (strong, nonatomic) IBOutlet UIView *viewForTermsConditions;
@property (weak, nonatomic) IBOutlet UITextView *txtViewTermsConditions;

//Nilind 21 th June
@property(strong,nonatomic)NSString *strWorlOrderId,*strSubWorkOderId,*strWoType;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssues;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderHelper;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderNotes;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderActualHrs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepair;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairParts;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairLabour;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerPaymentInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
//Nilind 27 June
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceEmail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalSubWorkOrder;
//End


//View Approve ServiceParts
@property (strong, nonatomic) IBOutlet UIView *viewForApproveTableServiceParts;
@property (weak, nonatomic) IBOutlet UITableView *tblApproveServiceParts;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableApproveParts_H;

//View Decline ServiceParts
@property (strong, nonatomic) IBOutlet UIView *viewForDeclineTableServiceParts;
@property (weak, nonatomic) IBOutlet UITableView *tblDeclineServiceParts;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableDeclineParts_H;

//View Complete ServiceParts
@property (strong, nonatomic) IBOutlet UIView *viewForCompleteTableServiceParts;
@property (weak, nonatomic) IBOutlet UITableView *tblCompletedServiceParts;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableCompleteParts_H;

@property (weak, nonatomic) IBOutlet UILabel *lblApproveParts;
@property (weak, nonatomic) IBOutlet UILabel *lblDeclinedParts;
@property (weak, nonatomic) IBOutlet UILabel *lblCompletedParts;

//View For Final Save

@property (weak, nonatomic) IBOutlet UIView *viewForFinalSave;
- (IBAction)actionOnSaveContinue:(id)sender;
- (IBAction)actionOnCancelBottom:(id)sender;
- (IBAction)actionOnAudio:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRecord;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
- (IBAction)actionOnPlayAudio:(id)sender;

//View For After Image//
@property (strong, nonatomic) IBOutlet UIView *viewForAfterImage;
@property (weak, nonatomic) IBOutlet UICollectionView *afterImageCollectionView;
- (IBAction)actionOnAfterImgView:(id)sender;
- (IBAction)actionAddAfterImages:(id)sender;
- (IBAction)actionOnCancelAfterImage:(id)sender;

//View For Cooment/////////
@property (strong, nonatomic) IBOutlet UIView *viewForComments;
@property (weak, nonatomic) IBOutlet UITextView *txtViewTechnicianComment;
@property (weak, nonatomic) IBOutlet UITextView *txtViewOfficeNotes;
@property (strong, nonatomic) IBOutlet UIButton *btnCustomerNotPresent;
@property (strong, nonatomic) IBOutlet UIButton *btnCompleteWorkOrder;
@property (strong, nonatomic) IBOutlet UIButton *btnCashL;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckL;
@property (strong, nonatomic) IBOutlet UIButton *btnCreditL;
@property (strong, nonatomic) IBOutlet UIButton *btnBillL;
@property (strong, nonatomic) IBOutlet UIButton *btnAutoL;
@property (strong, nonatomic) IBOutlet UIButton *btnPayPendingL;
@property (strong, nonatomic) IBOutlet UIButton *btnNoChargeL;
@property (strong, nonatomic) IBOutlet UILabel *lblTechName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ApproveParts_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_DeclineParts_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_CompletedParts_H;
@property (strong, nonatomic) IBOutlet UILabel *lblValueForTotal;

@property (strong, nonatomic) IBOutlet UIView *view_CustomerInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic;


@property (weak, nonatomic) IBOutlet UIButton *btnMileageCharge;
- (IBAction)actionOnMileageCharge:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblMileage;
@property (weak, nonatomic) IBOutlet UITextField *txtMiles;

@property (strong, nonatomic) IBOutlet UIButton *btnServiceJobDescriptionTitle;
- (IBAction)action_ServiceJobDescriptionTitle:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textView_ServiceJobDescriptions;
@property (strong, nonatomic) IBOutlet UIView *viewServiceJobDescriptions;
@property (strong, nonatomic) IBOutlet UITextField *txt_MiscellaneousCharge;
- (IBAction)action_ServiceAddressMapView:(id)sender;
- (IBAction)action_BillingAddressMapView:(id)sender;
- (IBAction)action_Email:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtIncDecActualHours;
@property (strong, nonatomic) IBOutlet UIButton *btnIncrase;
@property (strong, nonatomic) IBOutlet UIButton *btnDecrease;
- (IBAction)action_IncreaseActualHours:(id)sender;
- (IBAction)action_DecreaseActualHours:(id)sender;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderdetails;
@property (strong, nonatomic) NSManagedObject *objWorkOrderdetails;

@property (strong, nonatomic) IBOutlet UIButton *btnPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnMinus;

@property (strong, nonatomic) IBOutlet UILabel *lblServiceSummary;
@property (strong, nonatomic) IBOutlet UILabel *lblBillableHours;
@property (strong, nonatomic) IBOutlet UIButton *btnAddBillableLaborHrs;
- (IBAction)action_AddBillableLaborHrs:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewOfficeNotes;
@property (strong, nonatomic) IBOutlet UIButton *btnCollapsePaymentView;
- (IBAction)action_CollapsePaymentView:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewPaymentnNotes;
@property (strong, nonatomic) IBOutlet UIView *viewTMBillableHrs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWOCompleteTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs;

@property (strong, nonatomic) IBOutlet UIView *view_EmployeeTimeSheet;
- (IBAction)action_collapseEmployeeSheetView:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *view_ButtonCollapseEmployeeSheet;
@property (strong, nonatomic) IBOutlet UIButton *btnCollapseExpandEmpSheet;
@property (strong, nonatomic) IBOutlet UIView *view_DynamicCompleteEmpTime;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_MembershipPlanCharge_H;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_MembershipPlanSaving_H;

// Akshay  Start //
@property (strong, nonatomic) IBOutlet UIView *viewCredit;
@property (strong, nonatomic) IBOutlet UIView *viewPartLaborCreditApply;
@property (weak, nonatomic) IBOutlet UIButton *buttonLaborPrice;
@property (weak, nonatomic) IBOutlet UIButton *buttonPartPrice;

@property (weak, nonatomic) IBOutlet UIButton *buttonSelectCredit_Apply;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectCredit_Add;
@property (weak, nonatomic) IBOutlet UITableView *tableViewCredit;
@property (weak, nonatomic) IBOutlet UIView *viewSubTotal;
@property (weak, nonatomic) IBOutlet UILabel *labelSubTotal;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerAccountDiscount;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderAppliedDiscount;

@property (weak, nonatomic) IBOutlet UITableView *tableViewCreditPaymntDetail
;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightPaymentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTblViewPaymntDetail;
@property (weak, nonatomic) IBOutlet UILabel *labelCreditsPaymentDetailViewLeft;
@property (weak, nonatomic) IBOutlet UILabel *labelCreditsPaymentDetailViewRight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewCredit;

@property (weak, nonatomic) IBOutlet UIView *viewPayment;
@property (strong, nonatomic) IBOutlet UILabel *lblTxtIncDec;

// Akshay End //

@property (strong, nonatomic) IBOutlet UITextField *txtFld_TripChargeQty;
@property (weak, nonatomic) IBOutlet UILabel *lblTripChargeBasedOnQty;

@property (weak, nonatomic) IBOutlet UITableView *tableViewCreditPaymntDetailLeft;
@property (weak, nonatomic) IBOutlet UITableView *tableViewCreditPaymntDetailRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTblViewPaymntDetailLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTblViewPaymntDetailRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightPaymntDetailLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightPaymntDetailRight;
@property (weak, nonatomic) IBOutlet UIButton *btnNoMembership;
- (IBAction)actionOnNoMembership:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNewPlan;
- (IBAction)actionOnNewPlan:(id)sender;
- (IBAction)actionOnSelectPlan:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblNoMembershipPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMembershipPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTotalAmountNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForMembershipChargeNewPaln;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForMembershipSavingNewPlan;
@property (weak, nonatomic) IBOutlet UITextField *txtOtherDiscountNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForDiagnosticChargeNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTripChargeNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForSubtotalNewPlan;
@property (weak, nonatomic) IBOutlet UIButton *btnDiagnosticChargeNewPlan;
@property (weak, nonatomic) IBOutlet UIButton *btnTripChargeNewPlan;
- (IBAction)actionOnTripChargeNewPlan:(id)sender;
- (IBAction)actionOnDiagnosticChargeNewPlan:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblValueForTaxNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTotalNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmountPaidNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmountDueNewPlan;
@property (weak, nonatomic) IBOutlet UIButton *btnMileageChargeNewPlan;
- (IBAction)actionOnMileageChargeNewPlan:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblMileageNewPlan;
@property (weak, nonatomic) IBOutlet UITextField *txtMilesNewPlan;
@property (strong, nonatomic) IBOutlet UITextField *txtMiscellaneousChargeNewPlan;
@property (strong, nonatomic) IBOutlet UITextField *txtFld_TripChargeQtyNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblTripChargeBasedOnQtyNewPlan;
@property (weak, nonatomic) IBOutlet UIView *viewOld;
@property (weak, nonatomic) IBOutlet UIView *viewNew;


@property (weak, nonatomic) IBOutlet UIView *view_PaymentTypeCollapseExpand;
@property (weak, nonatomic) IBOutlet UIButton *btnPaymentTypeCollapseExpand;
- (IBAction)actionOnPaymentTypeCollapseExpand:(id)sender;

@end
