//
//  MechanicalInvoiceViewController.m
//  DPS_
//
//  Created by Saavan Patidar on 31/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//
#import "AllImportsViewController.h"
#import "MechanicalInvoiceViewController.h"
#import "ApproveTableViewCell.h"
#import "DeclineTableViewCell.h"
#import "CompletedTableViewCell.h"
#import "PaymentInfoServiceAuto.h"
#import "PaymentInfoServiceAuto+CoreDataProperties.h"
#import "ApproveServicePartsTableViewCell.h"
#import "DeclineServicePartsTableViewCell.h"
#import "CompleteServicePartsTableViewCell.h"
#import "PspPlans+CoreDataProperties.h"
#import "PspPlans+CoreDataClass.h"
#import "DPS-Swift.h"

//// Akshay Start //
@interface InvoiceCreditCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelCredit;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIButton *buttonViewMore;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeleteCredit;

@end

@implementation InvoiceCreditCell

@end

@interface CreditPaymentDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelCreditName;

@property (weak, nonatomic) IBOutlet UILabel *labelAmount;

@end
@implementation CreditPaymentDetailCell

@end
//// Akshay End //


// cell payment detail left side

@interface CreditPaymentDetailLeftCellInvoice : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelCreditName;

@property (weak, nonatomic) IBOutlet UILabel *labelAmount;

@end
@implementation CreditPaymentDetailLeftCellInvoice

@end

// cell payment detail Right side

@interface CreditPaymentDetailRightCellInvoice : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelCreditName;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;

@end
@implementation CreditPaymentDetailRightCellInvoice

@end


@interface MechanicalInvoiceViewController ()
{
    
    AVAudioPlayer *LandingSong;
    UITableView *tblData;
    NSMutableArray *arrayArriveIndexpath,*arrDeclineIndexPath,*arrIndexGlobalToShowEquipHeader,*arrIndexGlobalToShowEquipHeaderApproved,*arrIndexGlobalToShowEquipHeaderDeclined,*arrIndexGlobalToShowEquipHeaderCompleted,*arrIndexGlobalToShowEquipHeaderApprovedTM,*arrIndexGlobalToShowEquipHeaderDeclinedTM,*arrIndexGlobalToShowEquipHeaderCompletedTM,*arrPickerServiceJobDescriptions;
    BOOL chkHide,checkView,isCheckFrontImage, isCheckList, isHoliday,isGlobalFullTax,isGlobalPartTax,isGlobalLaborTax,isGlobalVendorTax, isIncludeDetailOnInvoice, isApprovedAvailable, isCompletedStatusMechanical, isSendDocWithOutSign, isExpandPaymentView, isForFirstTime,isPreSetSignGlobal, isExpandEmpSheet,isPaymentTypeCollapseExpand;
    NSArray *buttons;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    NSString *strDate,*strEmployeeId,*strGlobalServiceJobDescriptionId,*strGlobalServiceJobDescriptionsText;
    Global *global;
    NSMutableArray *arrOFImagesName,*arrOfCheckBackImage;
    
    //Nilind 21 June
    
    NSString *strSubWorkOrderIdGlobal,*strWorkOrderId,*strWorkOrderAccNo,*strWorkOrderAddressSubType,*strWorkOrderAddressId, *strAfterHrsDuration,*strCompanyName;
    NSMutableArray *arrOfSubWorkServiceIssues,*arrOfSubWorkServiceIssuesRepair,*arrOfSubWorkServiceIssuesRepairParts,*arrOfSubWorkServiceIssuesRepairLabour;
    NSString *strButtonClick,*strGlobalPaymentMode,*strUserName,*strGlobalWorkOrderId;
    BOOL isPaymentInfo,chkForUpdateWorkOrder;
    NSManagedObject *matchesPaymentInfo;
    NSManagedObject *matchesWorkOrder;
    NSString *strCustomerSign,*strTechnicianSign,*strCustomerNotPresent;
    UIImage *imageTemp;
    NSDictionary *dictMechanicalMasters,*dictJsonDynamicForm;
    NSArray *arrMechanicalMasters;
    NSMutableArray *arrDiagnostic,*arrTripCharge,*arrMileageCharge;
    BOOL chkTextEdit;NSString* paidAmount;
    NSMutableArray *arrIssueId,*arrAllParts;
    NSString *strButtonClickParts,*strWorkOrderStatuss;
    NSMutableArray *arrApproveId,*arrDeclinedId,*arrCompleted;
    NSMutableArray *arrApproveParts,*arrDeclinedParts,*arrCompletedParts;
    NSMutableArray *arrApp,*arrDecl,*arrComp;
    
    //nILIND 27 jUNE
    NSString * strEmpID,*strCompanyKey,*strEmpName,*strGlobalAudio,*strTax;
    NSMutableArray *arrOfBeforeImageAll,*arrOfImagenameCollewctionView,*arrOfImageCaption,*arrOfImageDescription,*arrOfPrimaryEmailNew,*arrOfSecondaryEmailNew,*arrOfPrimaryEmailAlreadySaved,*arrOfSecondaryEmailAlreadySaved,*arrOfTags,*arrDataTblView,*arrOfSubWorkOrder,*arrOfHoursConfig;
    BOOL isFromBeforeImage ,yesEditedSomething, isClientApproval;
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    NSManagedObject *objSubWorkOrderToSend,*matchesFinalSubWorkdOrder,*matchesFinalWorkOrder;
    NSString *strTripChargeId,*strDiagnosticChargeId,*strAmountPaidInSubWorkOrder,*strWoStatus;
    NSString *strDepartmentSysName,*strServiceUrlMainServiceAutomation,*strChargeAmountMiles,*strMileageName,*strDiagnosticChargeName,*strTripChargeName,*strGlobalCompleteTimeEmpSheet,*strGlobalCompleteAmountEmpSheet;
    
    NSInteger totalsecond;
    BOOL isLaborType, isWarranty, isEditLabor, isStandardSubWorkOrder;
    
    //eND
    
    UIView *MainViewForm,*ViewFormSections,*viewForm,*viewSection;
    NSMutableArray *arrayOfButtonsMain,*arrOfTimeSlotsEmployeeWise,*arrOfHeaderTitleForSlots,*arrOfEmployeesAssignedInSubWorkOrder,*arrOfWorkingDate,*arrOfGlobalDynamicEmpSheetFinal,*arrOfGlobalSubWOCompleteTimeExtSerDcs,*arrOfTimeSlotsEmployeeWiseTempNew,*arrOfHeaderTitleForSlotsTempNew,*arrOfEmployeesAssignedInSubWorkOrderTempNew,*arrOfWorkingDateTempNew;
    NSDictionary *dictDeptNameByKey;
    CGFloat paymentDetailHeight, paymentModeHeight, officeNotesHeight, checkDetailHeight, singleAmountHeight, employeeSheetHeight;
    NSString *strPartPriceType,*strEmployeeNoLoggedIn;
    NSInteger indexDate;
    
    // Akshay Start //
    NSMutableArray *arrayAccountDiscountCredits;
    NSMutableArray *arrayMasterCredits;
    NSMutableArray *arrayCopyOfMasterCredits;
    NSMutableArray *arrayAppliedDiscounts;
    int viewCreditHeight;
    BOOL isSubTotalViewHeightAdded;
    BOOL isUpdateViewsFrame;
    NSDictionary *dictSelectedDiscount;
    NSDictionary *dictAddDiscount;
    BOOL isLaborPrice;
    int heightPaymntDetailView;
    
    float totalLaborAmountLocal;
    float totalPartAmountLocal;
    
    float totalLaborAmountLocalCopy;
    float totalPartAmountLocalCopy;
    BOOL isPopUpViewFromMaster;// part labor popup view me apply button hai jo ki common hai master credit or account discount ke liye,isiliye ye var banaya identify karne ke liye ki apply button per action kaha se ho raha hai
    
    BOOL isLaborPartViewAddedToSuperView;
    // Akshay End //
    
    NSMutableArray *arrMechanicalPSPMasters,*arrPSPMasters;
    NSString *strPSPDiscount,*strPSPCharge,*strAccountPSPId,*strPspMasterId,*strDiagnosticChargeIdNewPlan,*strTripChargeIdNewPlan;
    NSString *strPSPDiscountPercent1,*strPSPDiscountAmount1,*strPSPCharge1,*strAccountPSPId1,*strPspMasterId1,*strPSPDiscountPercent2,*strPSPDiscountAmount2,*strPSPCharge2,*strAccountPSPId2,*strPspMasterId2,*strDiscountGlobalNewPlan,*strDiscountGlobal,*strAccountNo,*strChargeAmountMilesNewPlan;
    BOOL isFirstPlanSelected,isFirstPlan,chkPlanClick,isOldPlan,chkTextEditNewPlan,IsShowPricingAndNotesOnApp,IsAmountEdited;
    NSArray *arrPlanStatusResponse;
    NSMutableDictionary *dictAmountTotalRepairs,*dictOfAllAmountSeparately;
    
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewEmpTimeSheet;

@end

@implementation MechanicalInvoiceViewController
@synthesize strWoType,scrollViewEmpTimeSheet;


-(NSDate *)beginningOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    
    [components setHour:00];
    [components setMinute:00];
    [components setSecond:00];
    
    NSDate *datee=[[NSDate alloc]init];//
    
    datee=[cal dateFromComponents:components];
    
    return datee;
    
}


-(NSDate *)endOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [cal components:( NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    
    NSDate *datee=[[NSDate alloc]init];
    
    datee=[cal dateFromComponents:components];
    
    return datee;
    
}
- (void)viewDidLoad {
    
    // Hidding lbltrip charge
    
    [_lblValueForTipCharge setHidden:YES];
    [_lblValueForTripChargeNewPlan setHidden:YES];
    
    NSUserDefaults *defs1=[NSUserDefaults standardUserDefaults];
    [defs1 setBool:NO forKey:@"BackFromSendMailAfterComplete"];
    [defs1 synchronize];
    
    
    isPreSetSignGlobal=NO;
    [super viewDidLoad];
    
    // Akshay Start //
    arrayAccountDiscountCredits = [[NSMutableArray alloc]init];
    arrayMasterCredits = [[NSMutableArray alloc]init];
    arrayCopyOfMasterCredits = [[NSMutableArray alloc] init];
    arrayAppliedDiscounts = [[NSMutableArray alloc] init];
    //viewCreditHeight = 145;// after minus tableview height and sub total view height from credit view
    isLaborPrice = YES;
    
    // heightPaymntDetailViewLeft = 950-90;//90 is height of tableview which I have added later for credit
    
    isLaborPartViewAddedToSuperView = NO;
    // Akshay End //
    
    
    [_btnCollapsePaymentView setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    [_btnCollapseExpandEmpSheet setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    isCheckList=NO;
    isApprovedAvailable=NO;
    strWorkOrderStatuss=@"";
    chkForUpdateWorkOrder=NO;
    isStandardSubWorkOrder=NO;
    strGlobalPaymentMode=@"Cash";
    strGlobalPaymentMode=@"Bill";
    [_btnPaymentTypeCollapseExpand setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    isPaymentTypeCollapseExpand=NO;
    IsAmountEdited = NO;
    
    // change to set company based payment mode
    NSUserDefaults *defsPaymentMode=[NSUserDefaults standardUserDefaults];
    strGlobalPaymentMode = [defsPaymentMode valueForKey:@"CompanyPaymentMode"];
    
    IsShowPricingAndNotesOnApp = [defsPaymentMode boolForKey:@"IsShowPricingAndNotesOnApp"];
    //IsShowPricingAndNotesOnApp = YES;
    
    isIncludeDetailOnInvoice=YES;
    isSendDocWithOutSign=NO;
    isExpandPaymentView=NO;
    isForFirstTime=YES;
    isGlobalFullTax=NO;
    isGlobalPartTax=NO;
    isGlobalLaborTax=NO;
    isGlobalVendorTax=NO;
    
    strSubWorkOrderIdGlobal=_strSubWorkOderId;
    strWorkOrderId=_strWorlOrderId;
    
    // [self deleteMechanicalSubWOPaymentDetailDcs];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmployeeNoLoggedIn=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strEmployeeId          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"]];
    
    global=[[Global alloc]init];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodToCheckStatus];
    
    
    chkHide=NO;checkView=NO;
    arrayArriveIndexpath=[[NSMutableArray alloc]init];
    arrDeclineIndexPath=[[NSMutableArray alloc]init];
    arrApproveId=[[NSMutableArray alloc]init];
    arrDeclinedId=[[NSMutableArray alloc]init];
    buttons = @[_btnCash, _btnCheck, _btnCreditCard, _btnBill,_btnAutoChargeCustomer,_btnPaymentPending,_btnNoChange];
    
    
    arrOFImagesName=[[NSMutableArray alloc] init];
    arrOfCheckBackImage=[[NSMutableArray alloc] init];
    
    //nILIND
    
    yesEditedSomething=NO;
    arrOfBeforeImageAll=[[NSMutableArray alloc]init];
    arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    arrOfPrimaryEmailNew=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailNew=[[NSMutableArray alloc]init];
    arrOfPrimaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrOfTags=[[NSMutableArray alloc]init];
    arrOfSubWorkOrder=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    
    
    //eND
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [viewBackGround addGestureRecognizer:singleTapGestureRecognizer];
    
    BOOL isNetAvailable=[global isNetReachable];
    
    if (isNetAvailable) {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
        
        strTax=[global getTaxForMechanicalService:strServiceUrlMainServiceAutomation :strCompanyKey :strWorkOrderId];
        //strTax = @"10.0";
        
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
        
    }
    
    [self fetchPaymentInfoFromDataBase];
    // [self fetchWorkOrderFromDataBase];
    [self fetchWorkOrderFromDataBaseForMechanical];
    [self fetchSubWorkOrderFromDataBase];
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    [self getHoursConfiFromMaster];
    [self fetchSubWorkOrderActualHrsFromDataBaseForMechanical];
    [self fetchLocalMasters];
    [self fetchImageDetailFromDataBaseMechanical];
    
    //For Parts
#pragma mark - ***************** FOR PARTS *****************
    [self fetchAllParts];
    [self fetchCount];
    //End
#pragma mark - ***************** FOR ISSUE *****************
    if ([strWoType isEqualToString:@"FR"])
    {
        [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
        [self calculateTotal:_txtOtherDiscounts.text];
        [self fetchRepairCount];
    }
#pragma mark - ***************** FOR PARTS *****************
    else if ([strWoType isEqualToString:@"TM"])
    {
        // [_tblApproveServiceParts reloadData];
        //[_tblDeclineServiceParts reloadData];
        //[_tblCompletedServiceParts reloadData];
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
    [self methodBorderColor];
    [self tableCreate];
    [self addView:@"add"];
    
    
    if ([strWoType isEqualToString:@"FR"])
    {
        
        [self getApprovedListFR];
        
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        
        [self getApprovedListTM];
        
    }
    
    _lblTitle.text=[defsLogindDetail valueForKey:@"lblNameNew"];
    
    [_tblApprove reloadData];
    [_tblDecline reloadData];
    [_tblComplete reloadData];
    
    [self addView:@"Notadd"];
    
    [self fetchServiceJobDescriptionsFromMaster];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodToCheckStatus];
    
    [self fetchEmployeeTimeSheetDataFromDb:_strWorlOrderId :_strSubWorkOderId];
    
    _tableViewCredit.delegate=self;
    
    [self performSelector:@selector(setPaymentModeBasedOnConfig) withObject:nil afterDelay:0.10];
    [self performSelector:@selector(callMethod) withObject:nil afterDelay:0.5];
    
}

-(void)callMethod{
    
    strPSPCharge=[matchesFinalSubWorkdOrder valueForKey:@"pSPCharges"];
    strPSPDiscount=[matchesFinalSubWorkdOrder valueForKey:@"pSPDiscount"];
    strAccountPSPId=[matchesFinalSubWorkdOrder valueForKey:@"accountPSPId"];
    strAccountPSPId1=[matchesFinalSubWorkdOrder valueForKey:@"accountPSPId"];
    strPspMasterId=[matchesFinalSubWorkdOrder valueForKey:@"pSPMasterId"];
    
    BOOL isNetAvailable=[global isNetReachable];
    
    if (isNetAvailable) {
        
        //  [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Please Wait..."];
        
        NSLog(@"getTaxForMechanicalService called");
        
        [self performSelector:@selector(removeDejalActivity) withObject:nil afterDelay:40.0];
        [self performSelector:@selector(removeDejalActivity) withObject:nil afterDelay:150.0];
        
        strTax=[global getTaxForMechanicalService:strServiceUrlMainServiceAutomation :strCompanyKey :strWorkOrderId];
        //strTax = @"10.0";
        
        [self getPlan];
        
        //[self fetchIfMemberShipPresentInWorkOrder];
        
        
    }else{
        
        [self getPlan];
        
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
        
    }
    
    [self addView:@"Notadd"];
    
}

-(void)removeDejalActivity{
    
    NSLog(@"Called Remove Dejal");
    [DejalActivityView removeView];
    [DejalBezelActivityView removeView];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    NSUserDefaults *defs1=[NSUserDefaults standardUserDefaults];
    BOOL isBackFromSendMail=[defs1 boolForKey:@"BackFromSendMailAfterComplete"];
    
    if (isBackFromSendMail) {
        
        strWorkOrderStatuss=@"Completed";
        
        isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
        
        [defs1 setBool:NO forKey:@"BackFromSendMailAfterComplete"];
        [defs1 synchronize];
        
        
    }
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodToCheckStatus];
    
    NSString *strIns,*strCust;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strIns=[defs valueForKey:@"fromInspectorSignService"];
    strCust=[defs valueForKey:@"fromCustomerSignService"];
    
    if ([strIns isEqualToString:@"fromInspectorSignService"])
    {
        // isEditedInSalesAuto=YES;
        NSLog(@"Global mopdify date set to YES");
        NSString *strImageName;
        strImageName=[defs valueForKey:@"imagePath"];
        strTechnicianSign=strImageName;
        UIImage *imageSign;
        imageSign=[self loadImage:strImageName];
        _imgInspectorSign.image=imageSign;
        [defs setValue:@"abc" forKey:@"fromInspectorSignService"];
        [defs setValue:@"abc" forKey:@"imagePath"];
        [defs synchronize];
    }
    
    if ([strCust isEqualToString:@"fromCustomerSignService"])
    {
        // isEditedInSalesAuto=YES;
        NSLog(@"Global mopdify date set to YES");
        NSString *strImageName;
        strImageName=[defs valueForKey:@"imagePath"];
        strCustomerSign=strImageName;
        UIImage *imageSign;
        imageSign=[self loadImage:strImageName];
        _imgCustomerSign.image=imageSign;
        [defs setValue:@"xyz" forKey:@"fromCustomerSignService"];
        [defs setValue:@"xyz" forKey:@"imagePath"];
        [defs synchronize];
        
    }
    
    //Nilind 27 June
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
        EditImageViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewControlleriPad"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isFromBackServiceDynamci"];
        
        if (isFromBack) {
            
            arrOfBeforeImageAll=nil;
            arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            [defsBack setBool:NO forKey:@"isFromBackServiceDynamci"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseMechanical];
            
        }
        
        
        //    _lblOne.clipsToBounds = YES;
        //    _lblOne.layer.masksToBounds = YES;
        //    _lblOne.layer.cornerRadius = 40.0;
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreview"];
        if (yesFromDeleteImage) {
            
            yesEditedSomething=YES;
            
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreview"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfBeforeImageAll.count; k++) {
                
                NSDictionary *dictdat=arrOfBeforeImageAll[k];
                
                NSString *strImageName;
                
                if ([dictdat isKindOfClass:[NSString class]]) {
                    
                    strImageName=arrOfBeforeImageAll[k];
                    
                } else {
                    
                    strImageName=[dictdat valueForKey:@"woImagePath"];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImages"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++)
                {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare])
                    {
                        
                        [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                        
                    }
                }
                //            if (yesFoundName) {
                //
                //                [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                //
                //            }
            }
            if (!(arrTempBeforeImage.count==0)) {
                // arrOfBeforeImageAll=nil;
                // arrOfBeforeImageAll=[[NSMutableArray alloc]init];
                [arrOfBeforeImageAll removeObjectsInArray:arrTempBeforeImage];
            }
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImages"];
            [defsnew synchronize];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            yesEditedSomething=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            yesEditedSomething=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            
        }
        
        [self downloadingImagesThumbNailCheck];
        
    }
    
    [_afterImageCollectionView reloadData];
    
    //End
    
    //Nilind 28 June
    
    [_btnPlay setTitle:@"Play" forState:UIControlStateNormal];
    BOOL yesAudioAvailable=[defs boolForKey:@"yesAudio"];
    if (yesAudioAvailable)
    {
        // isEditedInSalesAuto=YES;
        NSLog(@"Global mopdify date set to YES");
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        strGlobalAudio=[defs valueForKey:@"AudioNameService"];
        
        if (strGlobalAudio.length>0) {
            [matchesFinalWorkOrder setValue:strGlobalAudio forKey:@"audioFilePath"];
        }else{
            [matchesFinalWorkOrder setValue:@"" forKey:@"audioFilePath"];
        }
        NSError *error;
        [context save:&error];
        
    }
    
    //End
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodToCheckStatus];
    
    if (isCompletedStatusMechanical) {
        
        _viewCredit.userInteractionEnabled=false;
        
    } else {
        
        _viewCredit.userInteractionEnabled=true;
        
    }
}

-(void)setPaymentModeBasedOnConfig{
    
    //strGlobalPaymentMode = @"Check";
    
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        [self setButtonImage:_btnCheck];
        checkView=YES;
        [_viewForSingleAmount setHidden:YES];
        [self addView:@"add"];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Cash"])
    {
        [self setButtonImage:_btnCash];
        checkView=NO;
        [_viewForSingleAmount setHidden:NO];
        [self addView:@"add"];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Bill"])
    {
        [self setButtonImage:_btnBill];
        checkView=NO;
        [_viewForSingleAmount setHidden:YES];
        [self addView:@"add"];
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"AutoChargeCustomer"])
    {
        [self setButtonImage:_btnAutoChargeCustomer];
        checkView=NO;
        [_viewForSingleAmount setHidden:YES];
        
        [self addView:@"add"];
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"CreditCard"])
    {
        [self setButtonImage:_btnCreditCard];
        checkView=NO;
        [_viewForSingleAmount setHidden:NO];
        
        [self addView:@"add"];
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"PaymentPending"])
    {
        [self setButtonImage:_btnPaymentPending];
        [_viewForSingleAmount setHidden:YES];
        checkView=NO;
        [self addView:@"add"];
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"])
    {
        [self setButtonImage:_btnNoChange];
        [_viewForSingleAmount setHidden:YES];
        checkView=NO;
        [self addView:@"add"];
        
    }
}
// Akshay Start //
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _heightTblViewPaymntDetail.constant = 0.0;
    [self getAccDiscountAppliedDiscountAndMasterCredits];
    [self callToChangeCalculationsOverAll];
}
// Akshay End //

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- ***************** Button Action *****************

- (IBAction)action_DashBoardView:(id)sender {
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert!"
                               message:@"Are you sure to move to DashBoard"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName: UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"CRM_New_iPad" : @"DashBoard" bundle: nil];
        DashBoardNew_iPhoneVC
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardNew_iPhoneVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        ////////////////////////////////
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (IBAction)actionOnCustomerNotPresent:(UIButton*)sender
{
    chkForUpdateWorkOrder=YES;
    
    if (sender.selected==NO)
    {
        strCustomerNotPresent=@"true";
        [_imgSignCheckBox setImage:[UIImage imageNamed:@"check_box_2.png"]];
        sender.selected=YES;
        _viewCustomerSign.hidden=YES;
    }
    else
    {
        strCustomerNotPresent=@"false";
        [_imgSignCheckBox setImage:[UIImage imageNamed:@"check_box_1.png"]];
        sender.selected=NO;
        _viewCustomerSign.hidden=NO;
    }
    
}

- (IBAction)actionOnInspectorSign:(id)sender
{
    chkForUpdateWorkOrder=YES;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromTechService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
    ServiceSignViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"ServiceSignViewControlleriPad"];
    // objSignViewController.strType=@"inspector";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
}
- (IBAction)actionOnCustomerSign:(id)sender
{
    chkForUpdateWorkOrder=YES;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromCustService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
    ServiceSignViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"ServiceSignViewControlleriPad"];
    // objSignViewController.strType=@"customer";
    
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
}

- (IBAction)actionOnCancel:(id)sender
{
    //[self goToSubWorkOrderDetailView];
    [self goToStartRepair];
}

- (IBAction)actionOnSubmit:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"Appointment" forKey:@"WhichScreen"];
    [defs synchronize];
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    SendMailMechanicaliPadViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailMechanicaliPadViewController"];
    objSendMail.strWorkOrderId=strWorkOrderId;
    objSendMail.strSubWorkOrderId=_strSubWorkOderId;
    objSendMail.strFromWhere=@"S";
    [self.navigationController pushViewController:objSendMail animated:NO];
}
- (IBAction)actionOnDiagnosticCharge:(id)sender
{
    tblData.tag=10;
    [self tableLoad:tblData.tag];
    
}

- (IBAction)actionOnDiagnosticChargeNewPlan:(id)sender
{
    [self.view endEditing:YES];
    if (arrDiagnostic.count==0) {
        
        [global AlertMethod:Alert :@"No diagnostic charge available"];
        
    } else {
        
        tblData.tag=30;
        [self tableLoad:tblData.tag];
        
    }
}

- (IBAction)actionOnTipCharge:(id)sender
{
    [self.view endEditing:YES];
    tblData.tag=20;
    [self tableLoad:tblData.tag];
    
}
- (IBAction)actionOnTripChargeNewPlan:(id)sender
{
    [self.view endEditing:YES];
    
    if (arrTripCharge.count==0) {
        
        [global AlertMethod:Alert :@"No trip charge available"];
        
    } else {
        
        tblData.tag=40;
        [self tableLoad:tblData.tag];
        
    }
}
- (IBAction)actionOnCash:(id)sender
{
    [self.view endEditing:YES];
    strGlobalPaymentMode=@"Cash";
    [self setButtonImage:_btnCash];
    checkView=NO;
    [_viewForSingleAmount setHidden:NO];
    [self addView:@"Notadd"];
    
}
- (IBAction)actionOnCheck:(id)sender
{
    [self.view endEditing:YES];
    strGlobalPaymentMode=@"Check";
    [self setButtonImage:_btnCheck];
    checkView=YES;
    [_viewForSingleAmount setHidden:YES];
    [self addView:@"add"];
    
}
- (IBAction)actionOnCreditCard:(id)sender
{
    [self.view endEditing:YES];
    strGlobalPaymentMode=@"CreditCard";
    [self setButtonImage:_btnCreditCard];
    checkView=NO;
    [_viewForSingleAmount setHidden:NO];
    [self addView:@"Notadd"];
    
}
- (IBAction)actionOnBill:(id)sender
{
    [self.view endEditing:YES];
    [self setButtonImage:_btnBill];
    checkView=NO;
    [_viewForSingleAmount setHidden:YES];
    strGlobalPaymentMode=@"Bill";
    [self addView:@"Notadd"];
    
}
- (IBAction)actionOnAutoChargeCustomer:(id)sender
{
    [self.view endEditing:YES];
    [self setButtonImage:_btnAutoChargeCustomer];
    checkView=NO;
    [_viewForSingleAmount setHidden:YES];
    strGlobalPaymentMode=@"AutoChargeCustomer";
    [self addView:@"Notadd"];
    
}
- (IBAction)actionOnPaymentPending:(id)sender
{
    [self.view endEditing:YES];
    [self setButtonImage:_btnPaymentPending];
    checkView=NO;
    [_viewForSingleAmount setHidden:YES];
    strGlobalPaymentMode=@"PaymentPending";
    [self addView:@"Notadd"];
    
}
- (IBAction)actionOnNoChange:(id)sender
{
    [self.view endEditing:YES];
    strGlobalPaymentMode=@"NoCharge";
    [self setButtonImage:_btnNoChange];
    checkView=NO;
    [self addView:@"Notadd"];
    [_viewForSingleAmount setHidden:YES];
    
    
}
- (IBAction)actionOnExpirationDate:(id)sender
{
    [self.view endEditing:YES];
    [self addPickerViewDateTo];
}

- (IBAction)actionBack:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    
    //[self goToSubWorkOrderDetailView];
    
    [self saveImageToCoreData];
    
    [self goToStartRepair];
}

-(void)goToStartRepair{
    
    if (isCompletedStatusMechanical) {
        
        
    }else{
        
        [_objSubWorkOrderdetails setValue:@"Running" forKey:@"subWOStatus"];
        
        NSError *error2;
        [context save:&error2];
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setObject:@"Running" forKey:@"WoStatus"];
        [defs synchronize];
        
    }
    
    if ([strWoType isEqualToString:@"TM"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalStartRepairTM
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairTM"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=_objWorkOrderdetails;
        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderdetails;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalStartRepairViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairViewController"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=_objWorkOrderdetails;
        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderdetails;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
}


-(void)goToSubWorkOrderDetailView{
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    [defss setInteger:-1 forKey:@"sectionToOpen"];
    [defss synchronize];
    
    if ([strWoType isEqualToString:@"TM"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=matchesWorkOrder;
        objByProductVC.objSubWorkOrderdetails=matchesFinalSubWorkdOrder;
        objByProductVC.strWoType=strWoType;
        
        NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
        [defsId setValue:strSubWorkOrderIdGlobal forKey:@"SubWorkOrderId"];
        [defsId synchronize];
        
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalSubWorkOrderDetailsViewControlleriPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControlleriPad"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=matchesWorkOrder;
        objByProductVC.objSubWorkOrderdetails=matchesFinalSubWorkdOrder;
        objByProductVC.strWoType=strWoType;
        
        NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
        [defsId setValue:strSubWorkOrderIdGlobal forKey:@"SubWorkOrderId"];
        [defsId synchronize];
        
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    
}

- (IBAction)actionOnCheckFrontImage:(id)sender
{
    isCheckFrontImage=YES;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
    
    [actionSheet showInView:self.view];
    
    
}

- (IBAction)actionOnCheckBackImage:(id)sender
{
    isCheckFrontImage=NO;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
    
    [actionSheet showInView:self.view];
}

- (IBAction)actionOnSaveContinue:(id)sender
{
    
    BOOL isMileageAlert=[self mileageAlert];
    
    if (_txtIncDecActualHours.text.length==0) {
        
        [global AlertMethod:Alert :@"Time is required to update billable labor hours."];
        
    }else if (_txtIncDecActualHours.text.length<5) {
        
        [global AlertMethod:Alert :@"Time is required in hh:mm(00:00) to update billable labor hours."];
        
    } else if (isMileageAlert) {
        
        [global AlertMethod:Alert :@"Please enter miles"];
        
    } else {
        
        //        [self finalSavePaymentInfo];
        //        [self updateWorkOrderDetail];
        [self checkifTimeEnteredIsWrong];
        
    }
}

-(void)checkifTimeEnteredIsWrong{
    
    if (_txtIncDecActualHours.text.length>=5) {
        
        NSArray *arrTime=[_txtIncDecActualHours.text componentsSeparatedByString:@":"];
        
        NSString *strHrs,*strMinutes;
        
        if (arrTime.count==1) {
            
            strHrs=arrTime[0];
            strMinutes=@"";
            
        }
        
        if (arrTime.count==2) {
            
            strHrs=arrTime[0];
            strMinutes=arrTime[1];
            
        }
        
        int strMinutesInt=[strMinutes intValue];
        
        if (strMinutesInt>59) {
            
            [global AlertMethod:Alert :@"Please provide total minutes less then 60"];
            
        } else {
            
            [self finalSavePaymentInfo];
            [self updateWorkOrderDetail];
            
        }
        
    }else{
        
        [self finalSavePaymentInfo];
        [self updateWorkOrderDetail];
        
    }
}

-(void)saveInDbMethods :(NSString*)strToCreateSubWorkOrder{
    
    [self finalSavePaymentInfo];
    [self updateWorkOrderDetail];
    
}


-(void)alertForCreateWorkorder{
    
    if (isApprovedAvailable) {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Confirmation...!!!"
                                   message:@"Do you want to create follow up workorder for pending items?"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  [matchesFinalSubWorkdOrder setValue:@"False" forKey:@"isCreateWOforInCompleteIssue"];
                                  NSError *error;
                                  [context save:&error];
                                  
                                  [self alertIfCustomerNotPresent];
                                  
                              }];
        [alert addAction:yes];
        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Create" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                                 [matchesFinalSubWorkdOrder setValue:@"True" forKey:@"isCreateWOforInCompleteIssue"];
                                 NSError *error;
                                 [context save:&error];
                                 
                                 [self alertIfCustomerNotPresent];
                                 
                             }];
        [alert addAction:no];
        UIAlertAction* cancell = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      
                                  }];
        [alert addAction:cancell];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        
        [matchesFinalSubWorkdOrder setValue:@"False" forKey:@"isCreateWOforInCompleteIssue"];
        NSError *error;
        [context save:&error];
        
        [self alertIfCustomerNotPresent];
        
    }
}

-(void)newMethodToGoToSendMail{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"Appointment" forKey:@"WhichScreen"];
    [defs synchronize];
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    SendMailMechanicaliPadViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailMechanicaliPadViewController"];
    objSendMail.strWorkOrderId=strWorkOrderId;
    objSendMail.strSubWorkOrderId=_strSubWorkOderId;
    objSendMail.strFromWhere=@"S";
    [self.navigationController pushViewController:objSendMail animated:NO];
    
}

-(void)alertIfCustomerNotPresent{
    
    if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"]) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        BOOL IsCustomerSignatureSR =[defs boolForKey:@"IsCustomerSignatureSR"];
        if (IsCustomerSignatureSR) {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Confirmation...!!!"
                                       message:@"Do you want to send documents with signature link?"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      [matchesFinalSubWorkdOrder setValue:@"False" forKey:@"isSendDocWithOutSign"];
                                      NSError *error;
                                      [context save:&error];
                                      isSendDocWithOutSign=NO;
                                      
                                      if (isSendDocWithOutSign) {
                                          [global fetchSubWorkOrderFromDBToUpdateStatus:strWorkOrderId :@"CompletePending"];
                                      } else {
                                          [global fetchSubWorkOrderFromDBToUpdateStatus:strWorkOrderId :@"Completed"];
                                      }
                                      
                                      [self newMethodToGoToSendMail];
                                      
                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Send" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                     [matchesFinalSubWorkdOrder setValue:@"True" forKey:@"isSendDocWithOutSign"];
                                     [matchesFinalSubWorkdOrder setValue:@"CompletePending" forKey:@"subWOStatus"];
                                     isSendDocWithOutSign=YES;
                                     
                                     if (isSendDocWithOutSign) {
                                         [global fetchSubWorkOrderFromDBToUpdateStatus:strWorkOrderId :@"CompletePending"];
                                     } else {
                                         [global fetchSubWorkOrderFromDBToUpdateStatus:strWorkOrderId :@"Completed"];
                                     }
                                     
                                     NSError *error;
                                     [context save:&error];
                                     
                                     [self newMethodToGoToSendMail];
                                     
                                 }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
            
        } else {
            
            [self newMethodToGoToSendMail];
            
        }
    } else {
        
        [self newMethodToGoToSendMail];
        
    }
}

-(BOOL)mileageAlert{
    
    BOOL mileageAlert = NO;
    
    
    if ([_btnMileageCharge.titleLabel.text isEqualToString:@"--Mileage Charge--"] || [_btnMileageCharge.currentTitle isEqualToString:@"--Mileage Charge--"]) {
        
        mileageAlert = NO;
        
    } else {
        
        if ((_txtMiles.text==0) || (_txtMiles.text.length==0)) {
            
            mileageAlert = YES;
            
        } else {
            
            mileageAlert = NO;
            
        }
        
    }
    
    return mileageAlert;
}

- (IBAction)actionOnCancelBottom:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)action_ServiceHistory:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ServiceHistoryMechanical
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistoryMechanical"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)goToServiceHistory{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ServiceHistoryMechanical
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistoryMechanical"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

#pragma mark- ****************** Tabel Delegate Methods ******************
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGRect frame = tableView.frame;
    NSManagedObject *dictData=arrOfSubWorkServiceIssues[section];
    
    //UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, _tblViewServiceIssue.frame.size.width-20, 50)];
    UILabel *title,*titleEquip;
    if (tableView==_tblApprove)
    {
        title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblApprove.frame.size.width-20, 40)];
        
        //Change for equip header
        
        NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
        
        if ([arrIndexGlobalToShowEquipHeaderApproved containsObject:strSecton]) {
            titleEquip = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblApprove.frame.size.width-20, 40)];
            titleEquip.text = [NSString stringWithFormat:@"Equipment :%@",[dictData valueForKey:@"equipmentName"]];
            titleEquip.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, titleEquip.frame.size.height+titleEquip.frame.origin.y+5, _tblApprove.frame.size.width-20, 40)];
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
        } else {
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblApprove.frame.size.width-20, 40)];
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            title.backgroundColor=[UIColor lightGrayColor];
            
        }
        
        // End change for equip header
        
    }
    else if (tableView==_tblDecline)
    {
        title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblDecline.frame.size.width-20, 40)];
        //Change for equip header
        
        NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
        
        if ([arrIndexGlobalToShowEquipHeaderDeclined containsObject:strSecton]) {
            titleEquip = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblDecline.frame.size.width-20, 40)];
            titleEquip.text = [NSString stringWithFormat:@"Equipment :%@",[dictData valueForKey:@"equipmentName"]];
            titleEquip.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, titleEquip.frame.size.height+titleEquip.frame.origin.y+5, _tblDecline.frame.size.width-20, 40)];
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
        } else {
            
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
            title.backgroundColor=[UIColor lightGrayColor];
        }
        
        // End change for equip header
        
    }
    else if (tableView==_tblComplete)
    {
        title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblComplete.frame.size.width-20, 40)];
        //Change for equip header
        
        NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
        
        if ([arrIndexGlobalToShowEquipHeaderCompleted containsObject:strSecton]) {
            titleEquip = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblComplete.frame.size.width-20, 40)];
            titleEquip.text = [NSString stringWithFormat:@"Equipment :%@",[dictData valueForKey:@"equipmentName"]];
            titleEquip.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, titleEquip.frame.size.height+titleEquip.frame.origin.y+5, _tblComplete.frame.size.width-20, 40)];
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
        } else {
            
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
            title.backgroundColor=[UIColor lightGrayColor];
        }
        
        // End change for equip header
        
    }
    else if (tableView==_tblApproveServiceParts)
    {
        title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblApproveServiceParts.frame.size.width-20, 40)];
        //Change for equip header
        
        NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
        
        if ([arrIndexGlobalToShowEquipHeaderApprovedTM containsObject:strSecton]) {
            titleEquip = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblApproveServiceParts.frame.size.width-20, 40)];
            titleEquip.text = [NSString stringWithFormat:@"Equipment :%@",[dictData valueForKey:@"equipmentName"]];
            titleEquip.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, titleEquip.frame.size.height+titleEquip.frame.origin.y+5, _tblApproveServiceParts.frame.size.width-20, 40)];
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
        } else {
            
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
            title.backgroundColor=[UIColor lightGrayColor];
        }
        
        // End change for equip header
        
    }
    else if (tableView==_tblDeclineServiceParts)
    {
        title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblDeclineServiceParts.frame.size.width-20, 40)];
        //Change for equip header
        
        NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
        
        if ([arrIndexGlobalToShowEquipHeaderApprovedTM containsObject:strSecton]) {
            titleEquip = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblDeclineServiceParts.frame.size.width-20, 40)];
            titleEquip.text = [NSString stringWithFormat:@"Equipment :%@",[dictData valueForKey:@"equipmentName"]];
            titleEquip.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, titleEquip.frame.size.height+titleEquip.frame.origin.y+5, _tblDeclineServiceParts.frame.size.width-20, 40)];
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
        } else {
            
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
            title.backgroundColor=[UIColor lightGrayColor];
        }
        
        // End change for equip header
        
    }
    else if (tableView==_tblCompletedServiceParts)
    {
        title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblCompletedServiceParts.frame.size.width-20, 40)];
        //Change for equip header
        
        NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
        
        if ([arrIndexGlobalToShowEquipHeaderApprovedTM containsObject:strSecton]) {
            titleEquip = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblCompletedServiceParts.frame.size.width-20, 40)];
            titleEquip.text = [NSString stringWithFormat:@"Equipment :%@",[dictData valueForKey:@"equipmentName"]];
            titleEquip.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, titleEquip.frame.size.height+titleEquip.frame.origin.y+5, _tblCompletedServiceParts.frame.size.width-20, 40)];
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
        } else {
            
            title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
            
            title.backgroundColor=[UIColor lightGrayColor];
        }
        
        // End change for equip header
        
    }
    title.backgroundColor=[UIColor lightGrayColor];
    title.font=[UIFont boldSystemFontOfSize:22];
    titleEquip.font=[UIFont boldSystemFontOfSize:22];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [headerView addSubview:titleEquip];
    [headerView addSubview:title];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (tableView==_tblApprove)
    {
        //return 50;
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
            {
                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                {
                }
                else
                {
                    k1++;
                }
                
            }
            
        }
        if (k1==0)
        {
            return 0;
        }
        else
        {
            NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
            if ([arrIndexGlobalToShowEquipHeaderApproved containsObject:strSecton]) {
                
                return 100;
                
            }else{
                
                return 50;
                
            }
            
        }
        
    }
    else if (tableView==_tblDecline)
    {
        //return 50;
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
            {
                
                k1++;
                
                //                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                //                {
                //
                //
                //                }else{
                //
                //                    k1++;
                //
                //                }
                
            }
            
        }
        
        if (k1==0)
        {
            return 0;
        }
        else
        {
            NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
            if ([arrIndexGlobalToShowEquipHeaderDeclined containsObject:strSecton]) {
                
                return 100;
                
            }else{
                
                return 50;
                
            }
            
        }
    }
    else if (tableView==_tblComplete)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                if([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                    {
                        k1++;
                        
                    }
                }
                
            }
            
        }
        
        if (k1==0)
        {
            return 0;
        }
        else
        {
            NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
            if ([arrIndexGlobalToShowEquipHeaderCompleted containsObject:strSecton]) {
                
                return 100;
                
            }else{
                
                return 50;
                
            }
            
        }
    }
    else if (tableView==_tblApproveServiceParts)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        int k1=0;
        
        for (int k=0; k<arrAllParts.count; k++)//arrOfSubWorkServiceIssuesRepairParts
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
            {
                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                {
                    
                    
                }else{
                    
                    k1++;
                    
                }
            }
            
        }
        if (k1==0)
        {
            return 0;
        }
        else
        {
            NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
            if ([arrIndexGlobalToShowEquipHeaderApprovedTM containsObject:strSecton]) {
                
                return 100;
                
            }else{
                
                return 50;
                
            }
            
        }
    }
    else if (tableView==_tblDeclineServiceParts)
    {
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        int k1=0;
        
        for (int k=0; k<arrAllParts.count; k++)//arrOfSubWorkServiceIssuesRepairParts
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
            {
                
                k1++;
                
                //                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                //                {
                //
                //
                //                }else
                //                {
                //
                //                    k1++;
                //
                //                }
                
            }
            
        }
        
        if (k1==0)
        {
            return 0;
        }
        else
        {
            NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
            if ([arrIndexGlobalToShowEquipHeaderDeclinedTM containsObject:strSecton]) {
                
                return 100;
                
            }else{
                
                return 50;
                
            }
            
        }
    }
    else if (tableView==_tblCompletedServiceParts)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        int k1=0;
        
        for (int k=0; k<arrAllParts.count; k++)//arrOfSubWorkServiceIssuesRepairParts
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                if([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                    {
                        k1++;
                        
                    }
                }
                
            }
            
            
            
        }
        if (k1==0)
        {
            return 0;
            
        }
        else
        {
            NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
            if ([arrIndexGlobalToShowEquipHeaderCompletedTM containsObject:strSecton]) {
                
                return 100;
                
            }else{
                
                return 50;
                
            }
            
        }
    }
    else
    {
        return 0;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==_tblApprove)
    {
        return arrOfSubWorkServiceIssues.count;////return 2;
    }
    else if (tableView==_tblDecline)
    {
        return arrOfSubWorkServiceIssues.count;//return 2;
    }
    else if (tableView==_tblComplete)
    {
        return arrOfSubWorkServiceIssues.count;
    }
    else if (tableView==_tblApproveServiceParts)
    {
        return arrOfSubWorkServiceIssues.count;
    }
    else if (tableView==_tblDeclineServiceParts)
    {
        return arrOfSubWorkServiceIssues.count;
    }
    else if (tableView==_tblCompletedServiceParts)
    {
        return arrOfSubWorkServiceIssues.count;
    }
    else
        return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblApprove)
    {
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
            {
                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                {
                }
                else
                {
                    k1++;
                }
                
            }
            
        }
        
        return k1;
        //return 2;
    }
    else if (tableView==_tblComplete)
    {
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                if([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                    {
                        k1++;
                        
                    }
                }
                
            }
            
        }
        
        return k1;
    }
    else if (tableView==_tblDecline)
    {
        //return 2;
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
            {
                
                k1++;
                
                //                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                //                {
                //
                //
                //                }else{
                //
                //                    k1++;
                //
                //                }
                
            }
            
        }
        
        return k1;
        
    }
    else if(tableView==_tblApproveServiceParts)
    {
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        int k1=0;
        
        for (int k=0; k<arrAllParts.count; k++)//arrOfSubWorkServiceIssuesRepairParts
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
            {
                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                {
                    
                    
                }else{
                    
                    k1++;
                    
                }
            }
            
        }
        
        return k1;
        
    }
    else if(tableView==_tblDeclineServiceParts)
    {
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        int k1=0;
        
        for (int k=0; k<arrAllParts.count; k++)//arrOfSubWorkServiceIssuesRepairParts
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
            {
                
                k1++;
                
                //                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                //                {
                //
                //
                //                }else
                //                {
                //
                //                    k1++;
                //
                //                }
                
            }
            
        }
        
        return k1;
        
    }
    else if(tableView==_tblCompletedServiceParts)
    {
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        int k1=0;
        
        for (int k=0; k<arrAllParts.count; k++)//arrOfSubWorkServiceIssuesRepairParts
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                if([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                    {
                        k1++;
                        
                    }
                }
                
            }
            
            /*if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
             {
             if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
             {
             k1++;
             
             }
             
             }*/
            
        }
        
        return k1;
        
    }
    // Akshay Start //
    else if(tableView == _tableViewCredit)
    {
        return arrayAppliedDiscounts.count;
    }
    else if(tableView == _tableViewCreditPaymntDetail)
    {
        return arrayAppliedDiscounts.count;
    }
    else if(tableView == _tableViewCreditPaymntDetailLeft)
    {
        return arrayAppliedDiscounts.count;
    }
    else if(tableView == _tableViewCreditPaymntDetailRight)
    {
        return arrayAppliedDiscounts.count;
    }
    else if (tableView.tag==10)
    {
        return arrDiagnostic.count;
    }
    else if (tableView.tag==20)
    {
        return arrTripCharge.count;
    }
    else if (tableView.tag==30)
    {
        return arrDiagnostic.count;
    }
    else if (tableView.tag==40)
    {
        return arrTripCharge.count;
    }
    else if (tableView.tag==50)
    {
        return arrPSPMasters.count;
    }
    else if (tableView.tag==60)
    {
        return arrMileageCharge.count;
    }
    else if (tableView.tag==70)
    {
        return arrMileageCharge.count;
    }
    // Akshay Start //
    else if(tableView.tag==80)
    {
        return arrayAccountDiscountCredits.count;
    }
    else if(tableView.tag == 90)
    {
        return arrayMasterCredits.count;
    }
    // Akshay End //
    else if (tableView.tag==107) {
        
        return arrPickerServiceJobDescriptions.count;
        
    }
    else
        return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblApprove)
    {
        
        ApproveTableViewCell *cell = (ApproveTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ApproveTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        return cell;
        
        
    }
    else if (tableView==_tblDecline)
    {
        
        static NSString *identifier=@"DeclineTableViewCell";
        DeclineTableViewCell *cell=[_tblDecline dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[DeclineTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
        
        
        NSString *strnonStdLaborAmt,*strnonStdPartAmt,*strnonStdRepairAmt,*cellRepairLaborId,*strRepairQty;
        
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        NSString *strIssueRepairIdToCheck;
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                if ([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"])
                {
                    [arrTemp addObject:dictIssuesRepairData];
                    
                    //                    if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
                    //                    {
                    //
                    //
                    //                    }else{
                    //
                    //                        [arrTemp addObject:dictIssuesRepairData];
                    //
                    //                    }
                }
                
            }//forKey:@"customerFeedback"
            
        }
        
        NSManagedObject *objTemp=arrTemp[indexPath.row];
        if ([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"customerFeedback"]]isEqualToString:@"1"])
        {
            // cell.btnApprove.backgroundColor=[UIColor colorWithRed:50.0f/255 green:164.0f/255 blue:159.0f/255 alpha:1];
            //cell.btnDecline.backgroundColor=[UIColor colorWithRed:170.0f/255 green:170.0f/255 blue:170.0f/255 alpha:1];
        }
        else
        {
            //cell.btnDecline.backgroundColor=[UIColor colorWithRed:50.0f/255 green:164.0f/255 blue:159.0f/255 alpha:1];
            // cell.btnApprove.backgroundColor=[UIColor colorWithRed:170.0f/255 green:170.0f/255 blue:170.0f/255 alpha:1];
            
        }
        
        float totalCostAdjustment=0;
        
        NSString *strcostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"costAdjustment"]];
        
        totalCostAdjustment=[strcostAdjustment floatValue];
        
        NSString *strPartCostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"partCostAdjustment"]];
        
        totalCostAdjustment=totalCostAdjustment+[strPartCostAdjustment floatValue];
        
        cell.lblValueForRepair.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairName"]];
        
        cell.lblValueForDescription.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
        if (cell.lblValueForDescription.text.length==0)
        {
            
            cell.lblValueForDescription.text=@"N/A";
            
        }
        strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
        
        
        strRepairQty =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
        cell.txtFldQty.delegate=self;
        cell.txtFldQty.accessibilityHint=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
        //cell.txtFldQty.leftView.hidden=YES;
        cell.txtFldQty.tag=0;
        cell.txtFldQty.text=strRepairQty;
        
        if ((strRepairQty.length==0) || [strRepairQty isEqualToString:@""] || [strRepairQty isEqualToString:@"(null)"] || [strRepairQty isEqualToString:@"0"]) {
            
            strRepairQty = @"1";
            cell.txtFldQty.text=@"";
            
        }
        
        //Fetch Parts
        
        [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical:strIssueIdToCheck :strIssueRepairIdToCheck];
        
        NSMutableArray *arrTempParts=[[NSMutableArray alloc]init];
        
        float totalCostPartss=0;
        
        for (int j=0; j<arrOfSubWorkServiceIssuesRepairParts.count; j++)
        {
            
            NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairParts[j];
            NSString *strAddedAfterApproval=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isAddedAfterApproval"]];
            
            if ([strWoType isEqualToString:@"TM"]) {
                strAddedAfterApproval=@"false";
            }
            
            if ([strAddedAfterApproval isEqualToString:@"0"]||[strAddedAfterApproval isEqualToString:@"false"]||[strAddedAfterApproval isEqualToString:@"False"])
            {
                NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[objTemp valueForKey:@"partName"],[objTemp valueForKey:@"qty"]];
                //multiplier   qty  unitPrice
                
                NSString *strMulti=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"multiplier"]];
                float multip=[strMulti floatValue];
                
                NSString *strqtyy=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
                float qtyy=[strqtyy floatValue];
                
                NSString *strunitPricee=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"unitPrice"]];
                float unitPricee=[strunitPricee floatValue];
                
                float finalPriceParts=unitPricee*qtyy*multip;
                
                totalCostPartss=totalCostPartss+finalPriceParts;
                
                [arrTempParts addObject:strPartsnQty];
                
            }
        }
        
        strnonStdLaborAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdLaborAmt"]];
        strnonStdPartAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdPartAmt"]];
        strnonStdRepairAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdRepairAmt"]];
        
        cellRepairLaborId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairLaborId"]];
        
        cell.lblValueForPart.text=[arrTempParts componentsJoinedByString:@", "];
        
        if (cell.lblValueForPart.text.length==0) {
            
            cell.lblValueForPart.text=@"N/A";
            
        }
        //Fetch LAbor
        
        [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :strIssueRepairIdToCheck];
        
        float totalCostLaborss=0;
        
        NSString *strLaborHrs,*strHelperHrs;
        for (int j=0; j<arrOfSubWorkServiceIssuesRepairLabour.count; j++) {
            
            NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairLabour[j];
            
            NSString *strWarranty=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isWarranty"]];
            
            if ([strWarranty isEqualToString:@"0"]||[strWarranty isEqualToString:@"false"]||[strWarranty isEqualToString:@"False"])
            {
                NSString *strLaborCostt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborCost"]];
                float laborCostt=[strLaborCostt floatValue];
                
                totalCostLaborss=totalCostLaborss+laborCostt;
                
                NSString *strLaborType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborType"]];
                
                if ([strLaborType isEqualToString:@"H"]) {
                    
                    strHelperHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                    
                } else {
                    
                    strLaborHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                    
                }
                
            }
            
        }
        
        float overAllAmt=totalCostPartss+totalCostLaborss+totalCostAdjustment;
        
        // Condition change for if laborcost is 0 then no cost adjustment is to be added
        
        if (totalCostLaborss<=0) {
            
            overAllAmt = overAllAmt-[strcostAdjustment floatValue];
            
        }
        
        if (cellRepairLaborId.length==0) {
            
            if ((totalCostPartss==0) && (totalCostLaborss==0)) {
                
                if ([strnonStdRepairAmt isEqualToString:@"0"]) {
                    
                    strnonStdRepairAmt=@"";
                    
                }
                
                if (strnonStdRepairAmt.length==0) {
                    
                    overAllAmt=overAllAmt+[strnonStdLaborAmt floatValue]+[strnonStdPartAmt floatValue];
                    
                } else {
                    
                    overAllAmt=overAllAmt+[strnonStdRepairAmt floatValue];
                    
                }
                
            }
            
        }
        
        //Change for saving Overall Amount All Time in DB
        
        NSString *strOverAllAmt=[NSString stringWithFormat:@"%f",overAllAmt];
        
        NSString *strWorkOrderIDd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"workorderId"]];
        
        NSString *strSubWorkOrderIdd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderId"]];
        
        SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
        
        [objSync fetchRepairToUpdateRepairAmt:strWorkOrderIDd :strSubWorkOrderIdd :strIssueIdToCheck :strIssueRepairIdToCheck :strOverAllAmt];
        
        cell.lblValueForAmount.text=[NSString stringWithFormat:@"%.02f",overAllAmt];
        
        int repairQty = [strRepairQty intValue];
        
        float amountAfterAddingQty = [cell.lblValueForAmount.text floatValue]*repairQty;
        
        cell.lblTotalAmount.text=[NSString stringWithFormat:@"%.02f",amountAfterAddingQty];
        
        if (strLaborHrs.length==0) {
            strLaborHrs=@"00:00";
        }
        if (strHelperHrs.length==0) {
            strHelperHrs=@"00:00";
        }
        
        cell.lblValueForLR.text=[global ChangeDateFormatOnGeneralInfoTime :strLaborHrs];
        cell.lblValueForHLR.text=[global ChangeDateFormatOnGeneralInfoTime :strHelperHrs];
        // NSManagedObject *record = [arrAllObjSubWorkOrderIssuesRepair objectAtIndex:indexPath.row];
        
        cell.btnViewMore.tag=indexPath.row*1000+indexPath.section;;
        [cell.btnViewMore addTarget:self action:@selector(action_ViewMorePartDescriptionDeclineRepair:) forControlEvents:UIControlEventTouchDown];
        
        if ([NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]].length>22) {
            
            cell.btnViewMore.hidden=NO;
            
        } else {
            
            cell.btnViewMore.hidden=YES;
            
        }
        
        return cell;
    }
    
    else if (tableView==_tblComplete)
    {
        
        static NSString *identifier=@"CompletedTableViewCell";
        CompletedTableViewCell *cell=[_tblComplete dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[CompletedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        NSString *strnonStdLaborAmt,*strnonStdPartAmt,*strnonStdRepairAmt,*cellRepairLaborId,*strRepairQty;
        
        NSString *strIssueRepairIdToCheck;
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                
                if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
                    {
                        [arrTemp addObject:dictIssuesRepairData];
                    }
                }
                
                // [arrTemp addObject:dictIssuesRepairData];
                
            }//forKey:@"customerFeedback"
            
        }
        
        NSManagedObject *objTemp=arrTemp[indexPath.row];
        if ([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"customerFeedback"]]isEqualToString:@"1"])
        {
        }
        else
        {
        }
        
        float totalCostAdjustment=0;
        
        NSString *strcostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"costAdjustment"]];
        
        totalCostAdjustment=[strcostAdjustment floatValue];
        
        NSString *strPartCostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"partCostAdjustment"]];
        
        totalCostAdjustment=totalCostAdjustment+[strPartCostAdjustment floatValue];
        
        cell.lblValueForRepair.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairName"]];
        
        cell.lblValueForDescription.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
        if (cell.lblValueForDescription.text.length==0)
        {
            
            cell.lblValueForDescription.text=@"N/A";
            
        }
        strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
        
        
        strnonStdLaborAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdLaborAmt"]];
        strnonStdPartAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdPartAmt"]];
        strnonStdRepairAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdRepairAmt"]];
        
        cellRepairLaborId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairLaborId"]];
        
        
        strRepairQty =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
        cell.txtFldQty.delegate=self;
        cell.txtFldQty.accessibilityHint=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
        //cell.txtFldQty.leftView.hidden=YES;
        cell.txtFldQty.tag=0;
        cell.txtFldQty.text=strRepairQty;
        
        if ((strRepairQty.length==0) || [strRepairQty isEqualToString:@""] || [strRepairQty isEqualToString:@"(null)"] || [strRepairQty isEqualToString:@"0"]) {
            
            strRepairQty = @"1";
            cell.txtFldQty.text=@"";
            
        }
        
        //Fetch Parts
        
        [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical:strIssueIdToCheck :strIssueRepairIdToCheck];
        
        NSMutableArray *arrTempParts=[[NSMutableArray alloc]init];
        
        float totalCostPartss=0;
        
        for (int j=0; j<arrOfSubWorkServiceIssuesRepairParts.count; j++)
        {
            
            NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairParts[j];
            
            NSString *strAddedAfterApproval=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isAddedAfterApproval"]];
            
            if ([strWoType isEqualToString:@"TM"]) {
                strAddedAfterApproval=@"false";
            }
            
            if ([strAddedAfterApproval isEqualToString:@"0"]||[strAddedAfterApproval isEqualToString:@"false"]||[strAddedAfterApproval isEqualToString:@"False"])
            {
                
                NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[objTemp valueForKey:@"partName"],[objTemp valueForKey:@"qty"]];
                //multiplier   qty  unitPrice
                
                NSString *strMulti=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"multiplier"]];
                float multip=[strMulti floatValue];
                
                NSString *strqtyy=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
                float qtyy=[strqtyy floatValue];
                
                NSString *strunitPricee=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"unitPrice"]];
                float unitPricee=[strunitPricee floatValue];
                
                float finalPriceParts=unitPricee*qtyy*multip;
                
                totalCostPartss=totalCostPartss+finalPriceParts;
                
                [arrTempParts addObject:strPartsnQty];
                
            }
            
        }
        
        cell.lblValueForPart.text=[arrTempParts componentsJoinedByString:@", "];
        
        if (cell.lblValueForPart.text.length==0) {
            
            cell.lblValueForPart.text=@"N/A";
            
        }
        //Fetch LAbor
        
        [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :strIssueRepairIdToCheck];
        
        float totalCostLaborss=0;
        
        NSString *strLaborHrs,*strHelperHrs;
        for (int j=0; j<arrOfSubWorkServiceIssuesRepairLabour.count; j++) {
            
            NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairLabour[j];
            
            NSString *strWarranty=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isWarranty"]];
            
            if ([strWarranty isEqualToString:@"0"]||[strWarranty isEqualToString:@"false"]||[strWarranty isEqualToString:@"False"])
            {
                NSString *strLaborCostt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborCost"]];
                float laborCostt=[strLaborCostt floatValue];
                
                totalCostLaborss=totalCostLaborss+laborCostt;
                
                NSString *strLaborType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborType"]];
                
                if ([strLaborType isEqualToString:@"H"]) {
                    
                    strHelperHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                    
                } else {
                    
                    strLaborHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                    
                }
                
            }
            
        }
        
        float overAllAmt=totalCostPartss+totalCostLaborss+totalCostAdjustment;
        
        // Condition change for if laborcost is 0 then no cost adjustment is to be added
        
        if (totalCostLaborss<=0) {
            
            overAllAmt = overAllAmt-[strcostAdjustment floatValue];
            
        }
        
        if (cellRepairLaborId.length==0) {
            
            if ((totalCostPartss==0) && (totalCostLaborss==0)) {
                
                if ([strnonStdRepairAmt isEqualToString:@"0"]) {
                    
                    strnonStdRepairAmt=@"";
                    
                }
                
                if (strnonStdRepairAmt.length==0) {
                    
                    overAllAmt=overAllAmt+[strnonStdLaborAmt floatValue]+[strnonStdPartAmt floatValue];
                    
                } else {
                    
                    overAllAmt=overAllAmt+[strnonStdRepairAmt floatValue];
                    
                }
                
            }
            
        }
        
        //Change for saving Overall Amount All Time in DB
        
        NSString *strOverAllAmt=[NSString stringWithFormat:@"%f",overAllAmt];
        
        NSString *strWorkOrderIDd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"workorderId"]];
        
        NSString *strSubWorkOrderIdd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderId"]];
        
        SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
        
        [objSync fetchRepairToUpdateRepairAmt:strWorkOrderIDd :strSubWorkOrderIdd :strIssueIdToCheck :strIssueRepairIdToCheck :strOverAllAmt];
        
        cell.lblValueForAmount.text=[NSString stringWithFormat:@"%.02f",overAllAmt];
        
        int repairQty = [strRepairQty intValue];
        
        float amountAfterAddingQty = [cell.lblValueForAmount.text floatValue]*repairQty;
        
        cell.lblTotalAmount.text=[NSString stringWithFormat:@"%.02f",amountAfterAddingQty];
        
        
        if (strLaborHrs.length==0) {
            strLaborHrs=@"00:00";
        }
        if (strHelperHrs.length==0) {
            strHelperHrs=@"00:00";
        }
        
        cell.lblValueForLR.text=[global ChangeDateFormatOnGeneralInfoTime :strLaborHrs];
        cell.lblValueForHLR.text=[global ChangeDateFormatOnGeneralInfoTime :strHelperHrs];
        // NSManagedObject *record = [arrAllObjSubWorkOrderIssuesRepair objectAtIndex:indexPath.row];
        
        
        cell.btnViewMore.tag=indexPath.row*1000+indexPath.section;;
        [cell.btnViewMore addTarget:self action:@selector(action_ViewMorePartDescriptionCompleteRepair:) forControlEvents:UIControlEventTouchDown];
        
        if ([NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]].length>22) {
            
            cell.btnViewMore.hidden=NO;
            
        } else {
            
            cell.btnViewMore.hidden=YES;
            
        }
        
        return cell;
    }
    else if (tableView==_tblApproveServiceParts)
    {
        static NSString *identifier=@"ApproveServicePartsTableViewCell";
        ApproveServicePartsTableViewCell *cell=[_tblApproveServiceParts dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[ApproveServicePartsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        isApprovedAvailable=YES;
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        //Nilind 26 June
        //[self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
            {
                // [arrTemp addObject:dictIssuesRepairData];
                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                {
                    
                }
                else
                {
                    [arrTemp addObject:dictIssuesRepairData];
                }
            }
        }
        
        NSManagedObject *dictIssuesPartData=arrTemp[indexPath.row];
        
        
        //End
        
        
        //[self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        
        
        // NSManagedObject *dictIssuesPartData=arrOfSubWorkServiceIssuesRepairParts[indexPath.row];
        // NSManagedObject *dictIssuesPartData=arrApproveParts[indexPath.row];
        NSString *strMulti=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"multiplier"]];
        float multip=[strMulti floatValue];
        
        NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"qty"]];
        float qtyy=[strqtyy floatValue];
        
        NSString *strunitPricee=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"unitPrice"]];
        float unitPricee=[strunitPricee floatValue];
        
        float finalPriceParts=unitPricee*qtyy*multip;
        float totalCostPartss=0;
        totalCostPartss=totalCostPartss+finalPriceParts;
        
        cell.lblAmountValue.text=[NSString stringWithFormat:@"%.02f",totalCostPartss];
        
        
        NSString *strPartsnQty=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partName"]];
        cell.lblPartName.text=strPartsnQty;
        if (cell.lblPartName.text.length==0) {
            
            cell.lblPartName.text=@"N/A";
            
        }
        cell.lblQty.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"qty"]];
        if (cell.lblQty.text.length==0) {
            
            cell.lblQty.text=@"N/A";
        }
        cell.lblPartDesc.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]];
        if (cell.lblPartDesc.text.length==0) {
            
            cell.lblPartDesc.text=@"N/A";
        }
        
        cell.lblIssueId.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"issueRepairPartId"]];
        
        cell.btnViewMore.tag=indexPath.row*1000+indexPath.section;;
        [cell.btnViewMore addTarget:self action:@selector(action_ViewMorePartDescriptionApprove:) forControlEvents:UIControlEventTouchDown];
        
        if ([NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]].length>22) {
            
            cell.btnViewMore.hidden=NO;
            
        } else {
            
            cell.btnViewMore.hidden=YES;
            
        }
        
        return cell;
    }
    else if (tableView==_tblDeclineServiceParts)
    {
        static NSString *identifier=@"DeclineServicePartsTableViewCell";
        DeclineServicePartsTableViewCell *cell=[_tblDeclineServiceParts dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[DeclineServicePartsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        //Nilind 26 June
        //[self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
            {
                
                
                [arrTemp addObject:dictIssuesRepairData];
                
                //                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                //                {
                //
                //                }
                //                else
                //                {
                //
                //                   [arrTemp addObject:dictIssuesRepairData];
                //
                //                }
            }
        }
        
        NSManagedObject *dictIssuesPartData=arrTemp[indexPath.row];
        
        NSString *strMulti=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"multiplier"]];
        float multip=[strMulti floatValue];
        
        NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"qty"]];
        float qtyy=[strqtyy floatValue];
        
        NSString *strunitPricee=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"unitPrice"]];
        float unitPricee=[strunitPricee floatValue];
        
        float finalPriceParts=unitPricee*qtyy*multip;
        float totalCostPartss=0;
        totalCostPartss=totalCostPartss+finalPriceParts;
        
        cell.lblAmountValue.text=[NSString stringWithFormat:@"%.02f",totalCostPartss];
        
        
        NSString *strPartsnQty=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partName"]];
        cell.lblPartName.text=strPartsnQty;
        if (cell.lblPartName.text.length==0) {
            
            cell.lblPartName.text=@"N/A";
            
        }
        cell.lblQty.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"qty"]];
        if (cell.lblQty.text.length==0) {
            
            cell.lblQty.text=@"N/A";
        }
        cell.lblPartDesc.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]];
        if (cell.lblPartDesc.text.length==0) {
            
            cell.lblPartDesc.text=@"N/A";
        }
        
        cell.lblIssueId.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"issueRepairPartId"]];
        
        cell.btnViewMore.tag=indexPath.row*1000+indexPath.section;;
        [cell.btnViewMore addTarget:self action:@selector(action_ViewMorePartDescriptionDecline:) forControlEvents:UIControlEventTouchDown];
        
        if ([NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]].length>22) {
            
            cell.btnViewMore.hidden=NO;
            
        } else {
            
            cell.btnViewMore.hidden=YES;
            
        }
        
        return cell;
    }
    else if (tableView==_tblCompletedServiceParts)
    {
        static NSString *identifier=@"CompleteServicePartsTableViewCell";
        CompleteServicePartsTableViewCell *cell=[_tblCompletedServiceParts dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[CompleteServicePartsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        
        //Nilind 26 June
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        //[self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                
                if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
                    {
                        [arrTemp addObject:dictIssuesRepairData];
                    }
                }
                
                // [arrTemp addObject:dictIssuesRepairData];
                
            }//forKey:@"customerFeedback"
            
            /*if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
             {
             [arrTemp addObject:dictIssuesRepairData];
             
             
             }*/
        }
        
        NSManagedObject *dictIssuesPartData=arrTemp[indexPath.row];
        
        
        // [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        
        
        //  NSManagedObject *dictIssuesPartData=arrOfSubWorkServiceIssuesRepairParts[indexPath.row];
        
        NSString *strMulti=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"multiplier"]];
        float multip=[strMulti floatValue];
        
        //NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"qty"]];
        NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"actualQty"]];
        
        float qtyy=[strqtyy floatValue];
        
        NSString *strunitPricee=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"unitPrice"]];
        float unitPricee=[strunitPricee floatValue];
        
        float finalPriceParts=unitPricee*qtyy*multip;
        float totalCostPartss=0;
        totalCostPartss=totalCostPartss+finalPriceParts;
        
        cell.lblAmountValue.text=[NSString stringWithFormat:@"%.02f",totalCostPartss];
        
        
        NSString *strPartsnQty=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partName"]];
        cell.lblPartName.text=strPartsnQty;
        if (cell.lblPartName.text.length==0) {
            
            cell.lblPartName.text=@"N/A";
            
        }
        //cell.lblQty.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"qty"]];
        cell.lblQty.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"actualQty"]];
        
        if (cell.lblQty.text.length==0) {
            
            cell.lblQty.text=@"N/A";
        }
        cell.lblPartDesc.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]];
        if (cell.lblPartDesc.text.length==0) {
            
            cell.lblPartDesc.text=@"N/A";
        }
        
        cell.lblIssueId.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"issueRepairPartId"]];
        
        cell.btnViewMore.tag=indexPath.row*1000+indexPath.section;;
        [cell.btnViewMore addTarget:self action:@selector(action_ViewMorePartDescriptionComplete:) forControlEvents:UIControlEventTouchDown];
        
        if ([NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]].length>22) {
            
            cell.btnViewMore.hidden=NO;
            
        } else {
            
            cell.btnViewMore.hidden=YES;
            
        }
        
        return cell;
    }
    // Akshay Start //
    else if (tableView == _tableViewCredit)
    {
        InvoiceCreditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvoiceCreditCell"];
        if(cell==nil)
        {
            cell = [[InvoiceCreditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InvoiceCreditCell"];
        }
        cell.labelCredit.text = [[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountSysName"];
        cell.labelAmount.text = [NSString stringWithFormat:@"$%.2f",[[[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"appliedDiscountAmt"] floatValue]];
        
        cell.labelDescription.text = [[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountDescription"];
        cell.buttonDeleteCredit.tag = indexPath.row;
        // [cell.buttonDeleteCredit addTarget:self action:@selector(actionOnDeleteCredit:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.buttonViewMore addTarget:self action:@selector(actionOnViewMore_Cedit:) forControlEvents:UIControlEventTouchDown];
        
        
        
        [cell.buttonDeleteCredit addTarget:self action:@selector(actionOnDeleteCredit:) forControlEvents:UIControlEventTouchDown];
        
        cell.buttonViewMore.tag = indexPath.row;
        if ([NSString stringWithFormat:@"%@",[[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountDescription"]].length>22) {
            
            cell.buttonViewMore.hidden=NO;
            
        } else {
            
            cell.buttonViewMore.hidden=YES;
            
        }
        //        [cell.buttonDeleteCredit setBackgroundColor:[UIColor yellowColor]];
        //        cell.buttonDeleteCredit.userInteractionEnabled = YES;
        //        cell.contentView.userInteractionEnabled = YES;
        //        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        return cell;
        
    }
    else if(tableView== _tableViewCreditPaymntDetail)
    {
        CreditPaymentDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreditPaymentDetailCell"];
        if(cell==nil)
        {
            cell = [[CreditPaymentDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreditPaymentDetailCell"];
        }
        cell.labelCreditName.text = [[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountSysName"];
        // [cell.labelCreditName setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightSemibold]];
        
        cell.labelAmount.text = [NSString stringWithFormat:@"$%.2f",[[[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"appliedDiscountAmt"] floatValue]];
        cell.labelAmount.textAlignment = NSTextAlignmentLeft;
        // [cell.labelAmount setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightSemibold]];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10)];
        separatorLineView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:separatorLineView];
        return cell;
        
    }
    else if(tableView== _tableViewCreditPaymntDetailLeft)
    {
        CreditPaymentDetailLeftCellInvoice *cell = [tableView dequeueReusableCellWithIdentifier:@"CreditPaymentDetailLeftCellInvoice"];
        if(cell==nil)
        {
            cell = [[CreditPaymentDetailLeftCellInvoice alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreditPaymentDetailLeftCellInvoice"];
        }
        cell.labelCreditName.text = [[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountSysName"];
        // [cell.labelCreditName setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightSemibold]];
        
        cell.labelAmount.text = [NSString stringWithFormat:@"$%.2f",[[[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"appliedDiscountAmt"] floatValue]];
        
        // [cell.labelAmount setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightSemibold]];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10)];
        separatorLineView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:separatorLineView];
        return cell;
        
    }
    else if(tableView== _tableViewCreditPaymntDetailRight)
    {
        CreditPaymentDetailRightCellInvoice *cell = [tableView dequeueReusableCellWithIdentifier:@"CreditPaymentDetailRightCellInvoice"];
        if(cell==nil)
        {
            cell = [[CreditPaymentDetailRightCellInvoice alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreditPaymentDetailRightCellInvoice"];
        }
        cell.labelCreditName.text = [[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountSysName"];
        cell.labelAmount.text = [NSString stringWithFormat:@"$%.2f",[[[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"appliedDiscountAmt"] floatValue]];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10)];
        separatorLineView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:separatorLineView];
        return cell;
        
    }
    // Akshay End //
    else if (tableView.tag==107) {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrPickerServiceJobDescriptions.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 107:
                {
                    NSDictionary *dictData=[arrPickerServiceJobDescriptions objectAtIndex:indexPath.row];
                    
                    NSString *title = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Title"]];
                    
                    cell.textLabel.text=title;
                    
                    if ([strGlobalServiceJobDescriptionId isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]]]) {
                        
                        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                        
                    }else{
                        
                        [cell setAccessoryType:UITableViewCellAccessoryNone];
                        
                    }
                    break;
                }
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
        
    }
    else
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (tblData.tag==10)
        {
            NSDictionary *dict=[arrDiagnostic objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        else if (tblData.tag==20)
        {
            NSDictionary *dict=[arrTripCharge objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        else if (tblData.tag==30)
        {
            NSDictionary *dict=[arrDiagnostic objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        else if (tblData.tag==40)
        {
            NSDictionary *dict=[arrTripCharge objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        else if (tblData.tag==50)
        {
            
            //            if (indexPath.row==0) {
            //
            //                cell.textLabel.text=@"No Membership Plan";
            //
            //            } else {
            
            NSDictionary *dict=[arrPSPMasters objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"Title"];
            
            //            }
            
        }
        else if (tblData.tag==60)
        {
            NSDictionary *dict=[arrMileageCharge objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        else if (tblData.tag==70)
        {
            NSDictionary *dict=[arrMileageCharge objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        // Akshay Start //
        else if (tblData.tag==80)
        {
            NSDictionary *dict=[arrayAccountDiscountCredits objectAtIndex:indexPath.row];//
            cell.textLabel.text = [dict valueForKey:@"sysName"];
            
        }
        
        else if (tblData.tag==90)
        {
            NSDictionary *dict=[arrayMasterCredits objectAtIndex:indexPath.row];
            cell.textLabel.text = [dict valueForKey:@"SysName"];
        }
        
        // Akshay End //
        if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
        {
            cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        else
        {
            cell.textLabel.font=[UIFont systemFontOfSize:22];
        }
        return cell;
    }
}

-(void)action_ViewMorePartDescriptionApprove:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    //Nilind 26 June
    //[self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    for (int k=0; k<arrAllParts.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrAllParts[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
        NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
        {
            // [arrTemp addObject:dictIssuesRepairData];
            if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
            {
                
            }
            else
            {
                [arrTemp addObject:dictIssuesRepairData];
            }
        }
    }
    
    NSManagedObject *dictIssuesPartData=arrTemp[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]];
    [global AlertMethod:@"Part Description" :strJobDesc];
    
}

-(void)action_ViewMorePartDescriptionComplete:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    
    
    //Nilind 26 June
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    //[self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
    for (int k=0; k<arrAllParts.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrAllParts[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
        NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
        {
            
            if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
            {
                if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
                {
                    [arrTemp addObject:dictIssuesRepairData];
                }
            }
            
            // [arrTemp addObject:dictIssuesRepairData];
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *dictIssuesPartData=arrTemp[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]];
    [global AlertMethod:@"Part Description" :strJobDesc];
    
}

-(void)action_ViewMorePartDescriptionDecline:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    
    //Nilind 26 June
    //[self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    for (int k=0; k<arrAllParts.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrAllParts[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
        NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
        {
            
            
            [arrTemp addObject:dictIssuesRepairData];
            
            //                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
            //                {
            //
            //                }
            //                else
            //                {
            //
            //                   [arrTemp addObject:dictIssuesRepairData];
            //
            //                }
        }
    }
    
    NSManagedObject *dictIssuesPartData=arrTemp[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]];
    [global AlertMethod:@"Part Description" :strJobDesc];
    
}


-(void)action_ViewMorePartDescriptionApproveRepair:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
        
        NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
        {
            if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
            {
                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                {
                }
                else
                {
                    [arrTemp addObject:dictIssuesRepairData];
                }
            }
            
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp=arrTemp[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
    [global AlertMethod:@"Repair Description" :strJobDesc];
    
}

-(void)action_ViewMorePartDescriptionCompleteRepair:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
        NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
        {
            
            if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
            {
                if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
                {
                    [arrTemp addObject:dictIssuesRepairData];
                }
            }
            
            // [arrTemp addObject:dictIssuesRepairData];
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp=arrTemp[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
    [global AlertMethod:@"Repair Description" :strJobDesc];
    
}

-(void)action_ViewMorePartDescriptionDeclineRepair:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
        NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
        {
            if ([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"])
            {
                [arrTemp addObject:dictIssuesRepairData];
                
                //                    if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
                //                    {
                //
                //
                //                    }else{
                //
                //                        [arrTemp addObject:dictIssuesRepairData];
                //
                //                    }
            }
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp=arrTemp[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
    [global AlertMethod:@"Repair Description" :strJobDesc];
    
}

-(void)getApprovedListFR{
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[k];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                    {
                        
                        
                        
                    }
                    else
                    {
                        [arrTemp addObject:dictIssuesData];
                    }
                }
            }//forKey:@"customerFeedback"
        }
    }
    
    arrIndexGlobalToShowEquipHeaderApproved=[[NSMutableArray alloc]init];
    
    for (int l=0; l<arrTemp.count; l++) {
        
        NSManagedObject *objTemp=arrTemp[l];
        NSString *strEquipCode=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"equipmentCode"]];
        NSString *strEquipCodeSubWorkOrderIssueId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
        
        for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
            
            NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
            NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
            NSString *strEquipCodeSubWorkOrderIssueId1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strEquipCode isEqualToString:strEquipCode1] && [strEquipCodeSubWorkOrderIssueId isEqualToString:strEquipCodeSubWorkOrderIssueId1]) {
                
                NSString *strIndex=[NSString stringWithFormat:@"%d",m];
                
                [arrIndexGlobalToShowEquipHeaderApproved addObject:strIndex];
                
                break;
            }
            
        }
    }
    [self getDeclinedListFR];
    
}

-(void)getDeclinedListFR{
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[k];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        // NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                if ([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"])
                {
                    [arrTemp addObject:dictIssuesData];
                }
            }
        }
    }
    
    arrIndexGlobalToShowEquipHeaderDeclined=[[NSMutableArray alloc]init];
    
    for (int l=0; l<arrTemp.count; l++) {
        
        NSManagedObject *objTemp=arrTemp[l];
        NSString *strEquipCode=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"equipmentCode"]];
        NSString *strEquipCodeSubWorkOrderIssueId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
        
        for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
            
            NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
            NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
            NSString *strEquipCodeSubWorkOrderIssueId1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strEquipCode isEqualToString:strEquipCode1] && [strEquipCodeSubWorkOrderIssueId isEqualToString:strEquipCodeSubWorkOrderIssueId1]) {
                
                NSString *strIndex=[NSString stringWithFormat:@"%d",m];
                
                [arrIndexGlobalToShowEquipHeaderDeclined addObject:strIndex];
                
                break;
            }
            
        }
    }
    
    [self getCompletedListFR];
}

-(void)getCompletedListFR{
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[k];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        // NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
                    {
                        [arrTemp addObject:dictIssuesData];
                    }
                }
            }
        }
    }
    
    arrIndexGlobalToShowEquipHeaderCompleted=[[NSMutableArray alloc]init];
    
    for (int l=0; l<arrTemp.count; l++) {
        
        NSManagedObject *objTemp=arrTemp[l];
        NSString *strEquipCode=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"equipmentCode"]];
        NSString *strEquipCodeSubWorkOrderIssueId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
        
        for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
            
            NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
            NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
            NSString *strEquipCodeSubWorkOrderIssueId1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strEquipCode isEqualToString:strEquipCode1] && [strEquipCodeSubWorkOrderIssueId isEqualToString:strEquipCodeSubWorkOrderIssueId1]) {
                
                NSString *strIndex=[NSString stringWithFormat:@"%d",m];
                
                [arrIndexGlobalToShowEquipHeaderCompleted addObject:strIndex];
                
                break;
            }
            
        }
    }
    
}

-(void)getApprovedListTM{
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[k];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
            {
                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                {
                    
                }
                else
                {
                    [arrTemp addObject:dictIssuesData];
                }
            }
        }
    }
    
    arrIndexGlobalToShowEquipHeaderApprovedTM=[[NSMutableArray alloc]init];
    
    for (int l=0; l<arrTemp.count; l++) {
        
        NSManagedObject *objTemp=arrTemp[l];
        NSString *strEquipCode=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"equipmentCode"]];
        NSString *strEquipCodeSubWorkOrderIssueId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
        
        for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
            
            NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
            NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
            NSString *strEquipCodeSubWorkOrderIssueId1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strEquipCode isEqualToString:strEquipCode1] && [strEquipCodeSubWorkOrderIssueId isEqualToString:strEquipCodeSubWorkOrderIssueId1]) {
                
                NSString *strIndex=[NSString stringWithFormat:@"%d",m];
                
                [arrIndexGlobalToShowEquipHeaderApprovedTM addObject:strIndex];
                
                break;
            }
        }
    }
    [self getDeclinedListTM];
    
}

-(void)getDeclinedListTM{
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[k];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        for (int k=0; k<arrAllParts.count; k++)
        {
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
            {
                [arrTemp addObject:dictIssuesData];
            }
        }
    }
    
    arrIndexGlobalToShowEquipHeaderDeclinedTM=[[NSMutableArray alloc]init];
    
    for (int l=0; l<arrTemp.count; l++) {
        
        NSManagedObject *objTemp=arrTemp[l];
        NSString *strEquipCode=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"equipmentCode"]];
        NSString *strEquipCodeSubWorkOrderIssueId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
        
        for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
            
            NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
            NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
            NSString *strEquipCodeSubWorkOrderIssueId1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strEquipCode isEqualToString:strEquipCode1] && [strEquipCodeSubWorkOrderIssueId isEqualToString:strEquipCodeSubWorkOrderIssueId1]) {
                
                NSString *strIndex=[NSString stringWithFormat:@"%d",m];
                
                [arrIndexGlobalToShowEquipHeaderDeclinedTM addObject:strIndex];
                
                break;
            }
            
        }
    }
    
    [self getCompletedListTM];
}

-(void)getCompletedListTM{
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[k];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        for (int k=0; k<arrAllParts.count; k++)
        {
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
            {
                if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
                    {
                        [arrTemp addObject:dictIssuesData];
                    }
                }
            }
        }
    }
    
    arrIndexGlobalToShowEquipHeaderCompletedTM=[[NSMutableArray alloc]init];
    
    for (int l=0; l<arrTemp.count; l++) {
        
        NSManagedObject *objTemp=arrTemp[l];
        NSString *strEquipCode=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"equipmentCode"]];
        NSString *strEquipCodeSubWorkOrderIssueId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
        
        for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
            
            NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
            NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
            NSString *strEquipCodeSubWorkOrderIssueId1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strEquipCode isEqualToString:strEquipCode1] && [strEquipCodeSubWorkOrderIssueId isEqualToString:strEquipCodeSubWorkOrderIssueId1]) {
                
                NSString *strIndex=[NSString stringWithFormat:@"%d",m];
                
                [arrIndexGlobalToShowEquipHeaderCompletedTM addObject:strIndex];
                
                break;
            }
        }
    }
    
}

- (void)configureCell:(ApproveTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    isApprovedAvailable=YES;
    NSString *strnonStdLaborAmt,*strnonStdPartAmt,*strnonStdRepairAmt,*cellRepairLaborId,*strRepairQty;
    
    // cell.btnApprove.tag=indexPath.row;//indexPath.row;
    
    // cell.btnDecline.tag=indexPath.row;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
        
        NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck])
        {
            if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
            {
                if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                {
                }
                else
                {
                    [arrTemp addObject:dictIssuesRepairData];
                }
            }
            
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp=arrTemp[indexPath.row];
    if ([[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"customerFeedback"]]isEqualToString:@"1"])
    {
        // cell.btnApprove.backgroundColor=[UIColor colorWithRed:50.0f/255 green:164.0f/255 blue:159.0f/255 alpha:1];
        //cell.btnDecline.backgroundColor=[UIColor colorWithRed:170.0f/255 green:170.0f/255 blue:170.0f/255 alpha:1];
    }
    else
    {
        //cell.btnDecline.backgroundColor=[UIColor colorWithRed:50.0f/255 green:164.0f/255 blue:159.0f/255 alpha:1];
        // cell.btnApprove.backgroundColor=[UIColor colorWithRed:170.0f/255 green:170.0f/255 blue:170.0f/255 alpha:1];
        
    }
    
    cell.lblValueForRepair.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairName"]];
    
    cell.lblValueForDescription.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
    if (cell.lblValueForDescription.text.length==0)
    {
        
        cell.lblValueForDescription.text=@"N/A";
        
    }
    strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
    
    
    strRepairQty =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
    cell.txtFldQty.delegate=self;
    cell.txtFldQty.accessibilityHint=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
    //cell.txtFldQty.leftView.hidden=YES;
    cell.txtFldQty.tag=0;
    cell.txtFldQty.text=strRepairQty;
    
    if ((strRepairQty.length==0) || [strRepairQty isEqualToString:@""] || [strRepairQty isEqualToString:@"(null)"] || [strRepairQty isEqualToString:@"0"]) {
        
        strRepairQty = @"1";
        cell.txtFldQty.text=@"";
        
    }
    
    //Fetch Parts
    
    [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical:strIssueIdToCheck :strIssueRepairIdToCheck];
    
    NSMutableArray *arrTempParts=[[NSMutableArray alloc]init];
    
    float totalCostAdjustment=0;
    
    NSString *strcostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"costAdjustment"]];
    
    totalCostAdjustment=[strcostAdjustment floatValue];
    
    NSString *strPartCostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"partCostAdjustment"]];
    
    totalCostAdjustment=totalCostAdjustment+[strPartCostAdjustment floatValue];
    
    float totalCostPartss=0;
    
    for (int j=0; j<arrOfSubWorkServiceIssuesRepairParts.count; j++)
    {
        
        NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairParts[j];
        
        NSString *strAddedAfterApproval=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isAddedAfterApproval"]];
        
        if ([strWoType isEqualToString:@"TM"]) {
            strAddedAfterApproval=@"false";
        }
        
        if ([strAddedAfterApproval isEqualToString:@"0"]||[strAddedAfterApproval isEqualToString:@"false"]||[strAddedAfterApproval isEqualToString:@"False"])
        {
            NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[objTemp valueForKey:@"partName"],[objTemp valueForKey:@"qty"]];
            //multiplier   qty  unitPrice
            
            NSString *strMulti=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"multiplier"]];
            float multip=[strMulti floatValue];
            
            NSString *strqtyy=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
            float qtyy=[strqtyy floatValue];
            
            NSString *strunitPricee=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"unitPrice"]];
            float unitPricee=[strunitPricee floatValue];
            
            float finalPriceParts=unitPricee*qtyy*multip;
            
            totalCostPartss=totalCostPartss+finalPriceParts;
            
            [arrTempParts addObject:strPartsnQty];
            
        }
        
    }
    
    cell.lblValueForPart.text=[arrTempParts componentsJoinedByString:@", "];
    
    if (cell.lblValueForPart.text.length==0) {
        
        cell.lblValueForPart.text=@"N/A";
        
    }
    
    strnonStdLaborAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdLaborAmt"]];
    strnonStdPartAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdPartAmt"]];
    strnonStdRepairAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdRepairAmt"]];
    cellRepairLaborId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairLaborId"]];
    
    //Fetch LAbor
    
    [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :strIssueRepairIdToCheck];
    
    float totalCostLaborss=0;
    
    NSString *strLaborHrs,*strHelperHrs;
    for (int j=0; j<arrOfSubWorkServiceIssuesRepairLabour.count; j++)
    {
        
        NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairLabour[j];
        
        NSString *strWarranty=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isWarranty"]];
        
        if ([strWarranty isEqualToString:@"0"]||[strWarranty isEqualToString:@"false"]||[strWarranty isEqualToString:@"False"])
        {
            NSString *strLaborCostt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborCost"]];
            float laborCostt=[strLaborCostt floatValue];
            
            totalCostLaborss=totalCostLaborss+laborCostt;
            
            NSString *strLaborType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborType"]];
            
            if ([strLaborType isEqualToString:@"H"]) {
                
                strHelperHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                
            }
            else
            {
                
                strLaborHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                
            }
            
        }
        
    }
    
    float overAllAmt=totalCostPartss+totalCostLaborss+totalCostAdjustment;
    
    // Condition change for if laborcost is 0 then no cost adjustment is to be added
    
    if (totalCostLaborss<=0) {
        
        overAllAmt = overAllAmt-[strcostAdjustment floatValue];
        
    }
    
    if (cellRepairLaborId.length==0) {
        
        if ((totalCostPartss==0) && (totalCostLaborss==0)) {
            
            if ([strnonStdRepairAmt isEqualToString:@"0"]) {
                
                strnonStdRepairAmt=@"";
                
            }
            
            if (strnonStdRepairAmt.length==0) {
                
                overAllAmt=overAllAmt+[strnonStdLaborAmt floatValue]+[strnonStdPartAmt floatValue];
                
            } else {
                
                overAllAmt=overAllAmt+[strnonStdRepairAmt floatValue];
                
            }
            
        }
        
    }
    
    //Change for saving Overall Amount All Time in DB
    
    NSString *strOverAllAmt=[NSString stringWithFormat:@"%f",overAllAmt];
    
    NSString *strWorkOrderIDd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"workorderId"]];
    
    NSString *strSubWorkOrderIdd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderId"]];
    
    SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
    
    [objSync fetchRepairToUpdateRepairAmt:strWorkOrderIDd :strSubWorkOrderIdd :strIssueIdToCheck :strIssueRepairIdToCheck :strOverAllAmt];
    
    cell.lblValueForAmount.text=[NSString stringWithFormat:@"%.02f",overAllAmt];
    
    int repairQty = [strRepairQty intValue];
    
    float amountAfterAddingQty = [cell.lblValueForAmount.text floatValue]*repairQty;
    
    cell.lblTotalAmount.text=[NSString stringWithFormat:@"%.02f",amountAfterAddingQty];
    
    if (strLaborHrs.length==0) {
        strLaborHrs=@"00:00";
    }
    if (strHelperHrs.length==0) {
        strHelperHrs=@"00:00";
    }
    
    cell.lblValueForLR.text=[global ChangeDateFormatOnGeneralInfoTime :strLaborHrs];
    cell.lblValueForHLR.text=[global ChangeDateFormatOnGeneralInfoTime :strHelperHrs];
    // NSManagedObject *record = [arrAllObjSubWorkOrderIssuesRepair objectAtIndex:indexPath.row];
    
    //cell.lblIssueId.text=strIssueRepairIdToCheck;
    
    
    cell.btnViewMore.tag=indexPath.row*1000+indexPath.section;;
    [cell.btnViewMore addTarget:self action:@selector(action_ViewMorePartDescriptionApproveRepair:) forControlEvents:UIControlEventTouchDown];
    
    if ([NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]].length>22) {
        
        cell.btnViewMore.hidden=NO;
        
    } else {
        
        cell.btnViewMore.hidden=YES;
        
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView==_tblApprove)
    {
    }
    else if (tableView==_tblDecline)
    {
    }
    else if (tableView==_tblComplete)
    {
    }else if (tableView.tag==107) {
        
        NSDictionary *dictData=[arrPickerServiceJobDescriptions objectAtIndex:indexPath.row];
        NSString *strServiceJobDescription = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescription"]];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [strServiceJobDescription dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
         _textView_ServiceJobDescriptions.attributedText = attributedString;
        
        //_textView_ServiceJobDescriptions.text=strServiceJobDescription;
        
        strGlobalServiceJobDescriptionsText = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescription"]];
        strGlobalServiceJobDescriptionId = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]];
        [_btnServiceJobDescriptionTitle setTitle:[dictData valueForKey:@"Title"] forState:UIControlStateNormal];
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }
    else
    {
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
                
            case 10:
            {
                NSDictionary *dict=[arrDiagnostic objectAtIndex:indexPath.row];
                
                [_btnDiagnosticCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    _lblValueForDiagnosticeCharge.text=[NSString stringWithFormat:@"$%.02f",0.00];
                    
                    strDiagnosticChargeId=@"";
                    
                } else {
                    
                    float amtDC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    _lblValueForDiagnosticeCharge.text=[NSString stringWithFormat:@"$%.02f",amtDC];
                    
                    strDiagnosticChargeId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
                    
                }
                
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
                
#pragma mark - ***************** FOR PARTS *****************
                // [self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateTotal:_txtOtherDiscounts.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self calculateTotalForParts:_txtOtherDiscounts.text];
                }
                break;
            }
            case 20:
            {
                NSDictionary *dict=[arrTripCharge objectAtIndex:indexPath.row];
                [_btnTipCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    _lblValueForTipCharge.text=[NSString stringWithFormat:@"$%.02f",0.00];
                    
                    strTripChargeId=@"";
                    
                } else {
                    
                    float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    _lblValueForTipCharge.text=[NSString stringWithFormat:@"$%.02f",amtTC];
                    
                    strTripChargeId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
                    
                    _txtFld_TripChargeQty.text = @"1";
                }
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
#pragma mark - ***************** FOR PARTS *****************
                //[self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateTotal:_txtOtherDiscounts.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self calculateTotalForParts:_txtOtherDiscounts.text];
                }
                
                [self calculateTripChargeBasedOnQty];
                
                break;
            }
            case 30:
            {
                NSDictionary *dict=[arrDiagnostic objectAtIndex:indexPath.row];
                
                [_btnDiagnosticChargeNewPlan setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    _lblValueForDiagnosticChargeNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
                    
                    strDiagnosticChargeIdNewPlan=@"";
                    
                } else {
                    
                    float amtDC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    _lblValueForDiagnosticChargeNewPlan.text=[NSString stringWithFormat:@"$%.02f",amtDC];
                    
                    strDiagnosticChargeIdNewPlan=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
                    
                }
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
                
#pragma mark - ***************** FOR PARTS *****************
                // [self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                
                break;
            }
            case 40:
            {
                NSDictionary *dict=[arrTripCharge objectAtIndex:indexPath.row];
                [_btnTripChargeNewPlan setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    _lblValueForTripChargeNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
                    
                    strTripChargeIdNewPlan=@"";
                    
                } else {
                    
                    float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    _lblValueForTripChargeNewPlan.text=[NSString stringWithFormat:@"$%.02f",amtTC];
                    
                    strTripChargeIdNewPlan=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
                    
                }
                
                [self calculateTripChargeBasedOnQty];
                
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
#pragma mark - ***************** FOR PARTS *****************
                //[self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                
                
                break;
            }
            case 50:
            {
                
                //                if (indexPath.row==0) {
                //
                //                    [_btnSelectPlan setTitle:@"No Membership Plan" forState:UIControlStateNormal];
                //                    _lblNewMembershipPlan.text=@"No Membership Plan";
                //                    _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",@"00.00"];
                //                    _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%@",@"00.00"];
                //                    strPspMasterId2=@"";
                //                    strAccountPSPId2=@"";
                //                    strPSPCharge2=@"";
                //                    strPSPDiscountPercent2=@"";
                //                    strPSPDiscountAmount2=@"";
                //
                //                } else {
                
                NSDictionary *dict=[arrPSPMasters objectAtIndex:indexPath.row];
                [_btnSelectPlan setTitle:[dict valueForKey:@"Title"] forState:UIControlStateNormal];
                _lblNewMembershipPlan.text=[dict valueForKey:@"Title"];
                _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
                double tempValueTemporary=[[dict valueForKey:@"UnitCharges"] doubleValue];
                _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary];
                
                double memberShipDiscout,memberShipCharge,value;
                memberShipCharge=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
                memberShipDiscout=[[dict valueForKey:@"Discount"]doubleValue];
                
                value=memberShipCharge*(memberShipDiscout/100);
                
                _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value];
                
                //Change For Plans
                
                strPspMasterId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
                strAccountPSPId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
                strPSPCharge2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitCharges"]];
                strPSPDiscountPercent2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
                
                strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
                strAccountPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
                strPSPCharge=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitCharges"]];
                strPSPDiscount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
                
                double memberShipDiscout2,memberShipCharge2,value2;
                memberShipCharge2=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
                memberShipDiscout2=[strPSPDiscountPercent2 doubleValue];
                
                value2=memberShipCharge2*(memberShipDiscout2/100);
                strPSPDiscountAmount2=[NSString stringWithFormat:@"%.02f",value2];
                _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value2];
                _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                double tempValueTemporary1=[[dict valueForKey:@"UnitCharges"] doubleValue];
                _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary1];
                
                //End Plans
                
                
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
                
                //                }
                
                
                break;
            }
            case 60:
            {
                NSDictionary *dict=[arrMileageCharge objectAtIndex:indexPath.row];
                [_btnMileageCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    strChargeAmountMiles=[NSString stringWithFormat:@"%@",@"0.00"];
                    
                    _lblMileage.text=[NSString stringWithFormat:@"%.02f",0.00];
                    
                    _txtMiles.text=@"";
                } else {
                    
                    float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    strChargeAmountMiles=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
                    
                    amtTC=amtTC*[_txtMiles.text floatValue];
                    
                    _lblMileage.text=[NSString stringWithFormat:@"%.02f",amtTC];
                    
                }
                
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
#pragma mark - ***************** FOR PARTS *****************
                //[self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateTotal:_txtOtherDiscounts.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self calculateTotalForParts:_txtOtherDiscounts.text];
                }
                
                break;
            }
            case 70:
            {
                NSDictionary *dict=[arrMileageCharge objectAtIndex:indexPath.row];
                [_btnMileageChargeNewPlan setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    strChargeAmountMilesNewPlan=[NSString stringWithFormat:@"%@",@"0.00"];
                    
                    _lblMileageNewPlan.text=[NSString stringWithFormat:@"%.02f",0.00];
                    
                    _txtMilesNewPlan.text=@"";
                    
                } else {
                    
                    float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    strChargeAmountMilesNewPlan=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
                    
                    amtTC=amtTC*[_txtMilesNewPlan.text floatValue];
                    
                    _lblMileageNewPlan.text=[NSString stringWithFormat:@"%.02f",amtTC];
                    
                }
                
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
#pragma mark - ***************** FOR PARTS *****************
                //[self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                
                
                break;
            }
            case 80:
            {
                dictSelectedDiscount = [arrayAccountDiscountCredits objectAtIndex:indexPath.row];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [_buttonSelectCredit_Apply setTitle:[dictSelectedDiscount valueForKey:@"sysName"] forState:UIControlStateNormal];
                    
                });
                
                
                break;
            }
            case 90:
            {
                dictAddDiscount=[arrayMasterCredits objectAtIndex:indexPath.row];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [_buttonSelectCredit_Add setTitle:[dictAddDiscount valueForKey:@"SysName"] forState:UIControlStateNormal];
                    
                });
                
                break;
            }
        }
        [tblData removeFromSuperview];
        [viewBackGround removeFromSuperview];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblApprove)
    {
        return tableView.rowHeight;
    }
    else if (tableView==_tblDecline)
    {
        return tableView.rowHeight;
    }
    else if (tableView==_tblComplete)
    {
        return tableView.rowHeight;
    }
    else if (tableView==_tblApproveServiceParts)
    {
        return tableView.rowHeight;
    }
    else if (tableView==_tblDeclineServiceParts)
    {
        return tableView.rowHeight;
    }
    else if (tableView==_tblCompletedServiceParts)
    {
        return tableView.rowHeight;
    }
    else if(tableView==_tableViewCredit)
    {
        return 220;
    }
    else if(tableView==_tableViewCreditPaymntDetail)
    {
        return 60;
    }
    else if(tableView==_tableViewCreditPaymntDetailLeft)
    {
        return 60;
    }
    else if(tableView==_tableViewCreditPaymntDetailRight)
    {
        return 60;
    }
    else
    {
        return 80;
    }
}
/*
 -(void)buttonClickOnArrive:(UIButton*)btn
 {
 NSLog(@"clicke on row tag %ld",(long)btn.tag);
 chkHide=YES;
 NSIndexPath *indexpath = [_tblViewServiceIssue indexPathForCell:(MechanicalClientApprovalTableViewCell *)btn.superview.superview];
 [_tblViewServiceIssue reloadData];
 
 
 }
 -(void)buttonClickOnDecline:(UIButton*)btn
 {
 chkHide=NO;
 //    UIButton *button=(UIButton *)btn;
 NSIndexPath *indexpath = [_tblViewServiceIssue indexPathForCell:(MechanicalClientApprovalTableViewCell *)btn.superview.superview];
 
 [_tblViewServiceIssue reloadData];
 
 
 }
 */
//============================================================================
#pragma mark- **************** DATE PICKER METHOD ****************
//============================================================================

-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate:[NSDate date]];
    
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDate = [dateFormat stringFromDate:pickerDate.date];
    [_btnExpirationDate setTitle:strDate forState:UIControlStateNormal];
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}

-(void)tapDetectedOnBackGroundView{
    [viewBackGround removeFromSuperview];
    [_viewPartLaborCreditApply removeFromSuperview];
    isLaborPartViewAddedToSuperView = NO;
}
#pragma mark- ************** TAP METHODS **************

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(isLaborPartViewAddedToSuperView == NO)
    {
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
    }
    
    // [_viewPartLaborCreditApply removeFromSuperview];
}
- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    [_viewPartLaborCreditApply removeFromSuperview];
    isLaborPartViewAddedToSuperView = NO;
}

#pragma mark- **************** Action Sheet Method ****************

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        {
            if (isCheckFrontImage)
            {
                
                if (arrOFImagesName.count==0) {
                    
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                }else
                {
                    [self performSelector:@selector(getImagePreviewFrontImage) withObject:nil afterDelay:1.0];
                }
                
            }
            else
            {
                
                if (arrOfCheckBackImage.count==0) {
                    
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                }else
                {
                    [self performSelector:@selector(getImagePreviewBackImage) withObject:nil afterDelay:1.0];
                }
            }
        }
    }
    else if (buttonIndex==1)
    {
        NSArray *arrTemp;
        if (isCheckFrontImage) {
            
            arrTemp=arrOFImagesName;
            
        }else{
            
            arrTemp=arrOfCheckBackImage;
            
        }
        
        if (arrTemp.count>=1) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check front image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        } else {
            NSLog(@"The CApture Image.");
            
            BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
                
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Camera Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if ( ( (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                             //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                             //                                     [alert show];
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
    }
    else if (buttonIndex==2)
    {
        NSArray *arrTemp;
        if (isCheckFrontImage) {
            
            arrTemp=arrOFImagesName;
            
        }else{
            
            arrTemp=arrOfCheckBackImage;
            
        }
        
        if (arrTemp.count>=1) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check back image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        } else {
            
            BOOL isCameraPermissionAvailable=[global isGalleryPermission];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
                
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Gallery Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if ( ((&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                             //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                             //                                     [alert show];
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
    }
}

#pragma mark- **************** Image Picker Method ****************

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // isFromImagePicker=YES;
    NSLog(@"Yes Something Edited");
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\CheckImg%@%@.jpg",strDate,strTime];
    
    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    [self resizeImage:image :strImageNamess];
    
    //[self saveImage:image :strImageNamess];
    
    if (isCheckFrontImage)
    {
        
        [arrOFImagesName addObject:strImageNamess];
        
    }
    else
    {
        
        [arrOfCheckBackImage addObject:strImageNamess];
        
    }
    
    [arrOfBeforeImageAll addObject:strImageNamess];
    [arrOfImagenameCollewctionView addObject:strImageNamess];
    [_afterImageCollectionView reloadData];
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    [self resizeImage:chosenImage :strImageNamess];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    //imageCaption
    
    NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
    
    BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
    
    if (yesImageCaption) {
        
        [self alertViewCustom];
        
    } else {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
    }
    
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

- (UIImage*)loadImage:(NSString *)imageName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      imageName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

#pragma mark- ****************** Local Methods ******************
#pragma mark- ****** Local Total Caculation ******
-(void)calculateTotal:(NSString*)strDiscount
{
    NSString *strIssueRepairIdToCheck;
    NSMutableArray *arrTotal,*arrTemp;
    arrTotal=[[NSMutableArray alloc]init];
    arrTemp=[[NSMutableArray alloc]init];
    
    
    //Nilind
    
    for (int i=0; i<arrOfSubWorkServiceIssues.count; i++)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[i];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                
            }
        }
    }
    
    //End
    
    float OverAllCostAdjustment=0.0,OverAllPartCostAdjustment=0.0,OverAllLaborCost=0.0,OverAllPartCostPrice=0.0,OverAllPartSalesPrice=0.0;
    
    for (int k=0; k<arrTemp.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrTemp[k];
        
        NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
        
        NSString *strnonStdRepairAmt=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"nonStdRepairAmt"]];
        NSString *strnonStdLaborAmt=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"nonStdLaborAmt"]];
        NSString *strnonStdPartAmt=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"nonStdPartAmt"]];
        
        NSString *strRepairMasterIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"repairMasterId"]];
        
        NSString *strRepairQty=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"qty"]];
        
        if ([[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"1"]|| [[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"true"])
        {
            if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
            {
                
                strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"issueRepairId"]];
                
                [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical:[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]] :strIssueRepairIdToCheck];
                //Parts Caculation
                NSMutableArray *arrTempParts=[[NSMutableArray alloc]init];
                
                float totalCostAdjustment=0;
                
                NSString *strcostAdjustment=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"costAdjustment"]];
                
                totalCostAdjustment=[strcostAdjustment floatValue];
                
                OverAllCostAdjustment=OverAllCostAdjustment+[strcostAdjustment floatValue];
                
                NSString *strPartCostAdjustment=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"partCostAdjustment"]];
                
                totalCostAdjustment=totalCostAdjustment+[strPartCostAdjustment floatValue];
                
                OverAllPartCostAdjustment=OverAllPartCostAdjustment+[strPartCostAdjustment floatValue];
                
                float totalCostPartss=0;
                
                for (int j=0; j<arrOfSubWorkServiceIssuesRepairParts.count; j++)
                {
                    
                    
                    NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairParts[j];
                    NSString *strAddedAfterApproval=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isAddedAfterApproval"]];
                    
                    if ([strWoType isEqualToString:@"TM"]) {
                        strAddedAfterApproval=@"false";
                    }
                    
                    if ([strAddedAfterApproval isEqualToString:@"0"]||[strAddedAfterApproval isEqualToString:@"false"]||[strAddedAfterApproval isEqualToString:@"False"])
                    {
                        
                        
                        NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[objTemp valueForKey:@"partName"],[objTemp valueForKey:@"qty"]];
                        //multiplier   qty  unitPrice
                        
                        NSString *strMulti=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"multiplier"]];
                        float multip=[strMulti floatValue];
                        
                        NSString *strqtyy=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
                        float qtyy=[strqtyy floatValue];
                        
                        NSString *strunitPricee=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"unitPrice"]];
                        float unitPricee=[strunitPricee floatValue];
                        
                        float price = unitPricee*qtyy;
                        
                        OverAllPartCostPrice = OverAllPartCostPrice+price;
                        
                        float finalPriceParts=unitPricee*qtyy*multip;
                        
                        OverAllPartSalesPrice = OverAllPartSalesPrice+finalPriceParts;
                        
                        totalCostPartss=totalCostPartss+finalPriceParts;
                        
                        [arrTempParts addObject:strPartsnQty];
                    }
                    
                }
                
                [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :strIssueRepairIdToCheck];
                
                float totalCostLaborss=0;
                
                NSString *strLaborHrs,*strHelperHrs;
                for (int j=0; j<arrOfSubWorkServiceIssuesRepairLabour.count; j++)
                {
                    
                    NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairLabour[j];
                    
                    NSString *strWarranty=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isWarranty"]];
                    if ([strWarranty isEqualToString:@"0"]||[strWarranty isEqualToString:@"false"]||[strWarranty isEqualToString:@"False"])
                    {
                        NSString *strLaborCostt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborCost"]];
                        float laborCostt=[strLaborCostt floatValue];
                        
                        totalCostLaborss=totalCostLaborss+laborCostt;
                        
                        OverAllLaborCost=OverAllLaborCost+[strLaborCostt floatValue];
                        
                        NSString *strLaborType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborType"]];
                        
                        if ([strLaborType isEqualToString:@"H"])
                        {
                            
                            strHelperHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                            
                        }
                        else
                        {
                            
                            strLaborHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                            
                        }
                        
                    }
                    
                }
                
                float overAllAmt=totalCostPartss+totalCostLaborss+totalCostAdjustment;
                
                // Condition change for if laborcost is 0 then no cost adjustment is to be added
                
                if (totalCostLaborss<=0) {
                    
                    overAllAmt = overAllAmt-[strcostAdjustment floatValue];
                    
                }
                
                if (strRepairMasterIdToCheck.length==0) {
                    
                    if ((totalCostPartss==0) && (totalCostLaborss==0)) {
                        
                        if ([strnonStdRepairAmt isEqualToString:@"0"]) {
                            
                            strnonStdRepairAmt=@"";
                            
                        }
                        
                        if (strnonStdRepairAmt.length==0) {
                            
                            overAllAmt=overAllAmt+[strnonStdLaborAmt floatValue]+[strnonStdPartAmt floatValue];
                            OverAllLaborCost=OverAllLaborCost+[strnonStdLaborAmt floatValue];
                            OverAllPartCostPrice=OverAllPartCostPrice+[strnonStdPartAmt floatValue];
                            
                        } else {
                            
                            overAllAmt=overAllAmt+[strnonStdRepairAmt floatValue];
                            
                        }
                        
                    }
                    
                }
                
                //[arrTotal addObject:[NSString stringWithFormat:@"%.02f",overAllAmt]];
                
                int repairQty = [strRepairQty intValue];
                
                float amountAfterAddingQty = overAllAmt*repairQty;
                
                [arrTotal addObject:[NSString stringWithFormat:@"%.02f",amountAfterAddingQty]];
                
            }
        }
        
        
        NSLog(@"ARRAY TOTAL %@",arrTotal);
    }
    double sumTotal=0;
    double otherDiscounts,subTotal,tax,amountDue;
    for(int i=0;i<arrTotal.count;i++)
    {
        sumTotal=sumTotal+[[arrTotal objectAtIndex:i]doubleValue];
    }
    
    if ([strWoType isEqualToString:@"FR"]) {
        
        totalLaborAmountLocal=OverAllLaborCost;
        totalPartAmountLocal=OverAllPartSalesPrice;
        totalLaborAmountLocalCopy=OverAllLaborCost;
        totalPartAmountLocalCopy=OverAllPartSalesPrice;
    }
    
    _lblValueTotalApprovedRepairs.text=[NSString stringWithFormat:@"$%.02f",sumTotal];
    _lblValueForTotalAmount.text=[NSString stringWithFormat:@"$%.02f",sumTotal];
    _lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"%@",_lblValueForTotalAmount.text];
    
    [self calculatePspDiscountAgain];
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    // akshay
    float laborAppliedCredits = 0.0 ,partAppliedCredits = 0.0;
    bool isLaborTax = [self isItLaborTax];
    
    if(isLaborTax)
    {
        
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            if([[dict valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnLaborPrice"] boolValue]==true)
            {
                laborAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
            
            if([[dict valueForKey:@"applyOnPartPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnPartPrice"] boolValue]==true)
            {
                partAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
            
        }
        
    }
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se
    
    subTotal=sumTotal+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblBillableHours.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_lblValueForPSPCharge.text]+[self getNumberString:_txt_MiscellaneousCharge.text]-[self getNumberString:_lblValueForPSPDiscount.text]-([strDiscount doubleValue] + [self getNumberString:_labelCreditsPaymentDetailViewLeft.text]);
    
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
        
    }
    
    if ([_lblValueForPSPCharge.text doubleValue]>0) {
        
        _const_MembershipPlanCharge_H.constant=50;
        
    }else{
        
        _const_MembershipPlanCharge_H.constant=0;
        
    }
    
    if ([_lblValueForPSPDiscount.text doubleValue]>0) {
        
        _const_MembershipPlanSaving_H.constant=50;
        
    }else{
        
        _const_MembershipPlanSaving_H.constant=0;
        
    }
    
    
    
    //tax=(subTotal*10)/100;
    // _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    
    // Akshay Start //
    //    totalLaborAmountLocal = OverAllLaborCost;
    //    totalPartAmountLocal =  subTotalMaterialForDiscount;
    //    totalLaborAmountLocalCopy = OverAllLaborCost;
    //    totalPartAmountLocalCopy = subTotalMaterialForDiscount;
    // Akshay End //
    
    tax=[self taxCalculation:subTotal :subTotal :OverAllLaborCost :OverAllPartCostPrice :OverAllPartSalesPrice :OverAllCostAdjustment :OverAllPartCostAdjustment :strDiscount];
    
    //-(double)taxCalculation :(double)totalPrice :(float)subTotal :(float)subTotalLabor :(float)subTotalMaterialCost :(float)subTotalMaterialCostSales :(float)strCostAdj :(float)strPartCostAdj{
    
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    //Nilind 19 July
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    //End
    
    
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    
    _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    
    amountDue=[[self getNumberStringNew:_lblValueForAmountDue.text] doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
    double amountPaid;
    amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
    _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
    _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    
    if (IsShowPricingAndNotesOnApp) {

        if (!IsAmountEdited) {

    _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
    _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

        }
        
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    } else {
        
        if (IsShowPricingAndNotesOnApp) {

            if (!IsAmountEdited) {

        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

            }
            
        }
        
    }
    
    [self setAmountOnTxtFld];
    
}

-(double)getNumberString:(NSString*)str
{
    double value;
    NSString *strDiscount = [str stringByReplacingOccurrencesOfString:@"$" withString:@""];
    strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    value=[strDiscount doubleValue];
    return value;
}
-(NSString*)getNumberStringNew:(NSString*)str
{
    NSString *value;
    NSString *strDiscount = [str stringByReplacingOccurrencesOfString:@"$" withString:@""];
    strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    value=[NSString stringWithFormat:@"%@",strDiscount];
    return value;
}

-(void)fetchLocalMasters
{
    //strDepartmentSysName=@"RCDept";
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    arrMechanicalMasters=[dictMechanicalMasters valueForKey:@"ChargeMasterExtSerDc"];
    arrDiagnostic=[[NSMutableArray alloc]init];
    arrTripCharge=[[NSMutableArray alloc]init];
    arrMileageCharge=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrOfTempChargeMasters=[[NSMutableArray alloc]init];
    
    
    for(int i=0;i<arrMechanicalMasters.count;i++)
    {
        NSDictionary *dictDataa=[arrMechanicalMasters objectAtIndex:i];
        
        NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
        NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
        NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
        
        
        if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartmentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:strWorkOrderAddressId]) {
            
            [arrOfTempChargeMasters addObject:dictDataa];
            
        }
    }
    
    if (arrOfTempChargeMasters.count==0) {
        
        for (int k=0; k<arrMechanicalMasters.count; k++) {
            
            NSDictionary *dictDataa=arrMechanicalMasters[k];
            //   BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
            //   if (isActive) {
            
            // Changes for fetching Via company dept account address
            
            
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
            NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
            
            
            if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartmentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:@""]) {
                
                [arrOfTempChargeMasters addObject:dictDataa];
                
            }
        }
        
    }
    
    if (arrOfTempChargeMasters.count==0) {
        
        for (int k=0; k<arrMechanicalMasters.count; k++) {
            
            NSDictionary *dictDataa=arrMechanicalMasters[k];
            //   BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
            //   if (isActive) {
            
            // Changes for fetching Via company dept account address
            
            
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
            NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
            // NSString *strLocalAddressSubType=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AddressType"]];
            
            
            if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartmentSysName] && [strLocalAccountNo isEqualToString:@""] && [strLocalAddressId isEqualToString:@""]) {
                
                [arrOfTempChargeMasters addObject:dictDataa];
                
            }
        }
        
    }
    
    NSDictionary *dictDataTemp=[[NSDictionary alloc]initWithObjectsAndKeys:@"--Diagnostic Charge--",@"ChargeName", nil];
    [arrDiagnostic addObject:dictDataTemp];
    
    dictDataTemp=[[NSDictionary alloc]initWithObjectsAndKeys:@"--Trip Charge--",@"ChargeName", nil];
    [arrTripCharge addObject:dictDataTemp];
    
    dictDataTemp=[[NSDictionary alloc]initWithObjectsAndKeys:@"--Mileage Charge--",@"ChargeName", nil];
    [arrMileageCharge addObject:dictDataTemp];
    
    for(int i=0;i<arrOfTempChargeMasters.count;i++)
    {
        NSDictionary *dict=[arrOfTempChargeMasters objectAtIndex:i];
        if ([[dict valueForKey:@"ChargeType"]isEqualToString:@"Diagnostic"])
        {
            [arrDiagnostic addObject:dict];
        }
        else if ([[dict valueForKey:@"ChargeType"]isEqualToString:@"Trip"])
        {
            [arrTripCharge addObject:dict];
        }
        else if ([[dict valueForKey:@"ChargeType"]isEqualToString:@"Mileage"])
        {
            [arrMileageCharge addObject:dict];
        }
    }
    
}

#pragma mark- ****** Local Methods For BorderColor *******

-(void)methodBorderColor
{
    
    _textView_ServiceJobDescriptions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _textView_ServiceJobDescriptions.layer.borderWidth=1.0;
    _textView_ServiceJobDescriptions.layer.cornerRadius=5.0;
    
    _txtOtherDiscounts.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtOtherDiscounts.layer.borderWidth=1.0;
    _txtOtherDiscounts.layer.cornerRadius=5.0;
    
    _txtAmountSingleAmount.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtAmountSingleAmount.layer.borderWidth=1.0;
    _txtAmountSingleAmount.layer.cornerRadius=5.0;
    
    _txtCheckNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtCheckNo.layer.borderWidth=1.0;
    _txtCheckNo.layer.cornerRadius=5.0;
    
    _txtAmountCheckView.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtAmountCheckView.layer.borderWidth=1.0;
    _txtAmountCheckView.layer.cornerRadius=5.0;
    
    _txtDrivingLicenseNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtDrivingLicenseNo.layer.borderWidth=1.0;
    _txtDrivingLicenseNo.layer.cornerRadius=5.0;
    
    [_btnTipCharge.layer setCornerRadius:5.0f];
    [_btnTipCharge.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnTipCharge.layer setBorderWidth:0.8f];
    [_btnTipCharge.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnTipCharge.layer setShadowOpacity:0.3];
    [_btnTipCharge.layer setShadowRadius:3.0];
    [_btnTipCharge.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnDiagnosticCharge.layer setCornerRadius:5.0f];
    [_btnDiagnosticCharge.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnDiagnosticCharge.layer setBorderWidth:0.8f];
    [_btnDiagnosticCharge.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnDiagnosticCharge.layer setShadowOpacity:0.3];
    [_btnDiagnosticCharge.layer setShadowRadius:3.0];
    [_btnDiagnosticCharge.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnMileageCharge.layer setCornerRadius:5.0f];
    [_btnMileageCharge.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnMileageCharge.layer setBorderWidth:0.8f];
    [_btnMileageCharge.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnMileageCharge.layer setShadowOpacity:0.3];
    [_btnMileageCharge.layer setShadowRadius:3.0];
    [_btnMileageCharge.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectPlan.layer setCornerRadius:5.0f];
    [_btnSelectPlan.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectPlan.layer setBorderWidth:0.8f];
    [_btnSelectPlan.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectPlan.layer setShadowOpacity:0.3];
    [_btnSelectPlan.layer setShadowRadius:3.0];
    [_btnSelectPlan.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnDiagnosticChargeNewPlan.layer setCornerRadius:5.0f];
    [_btnDiagnosticChargeNewPlan.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnDiagnosticChargeNewPlan.layer setBorderWidth:0.8f];
    [_btnDiagnosticChargeNewPlan.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnDiagnosticChargeNewPlan.layer setShadowOpacity:0.3];
    [_btnDiagnosticChargeNewPlan.layer setShadowRadius:3.0];
    [_btnDiagnosticChargeNewPlan.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnTripChargeNewPlan.layer setCornerRadius:5.0f];
    [_btnTripChargeNewPlan.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnTripChargeNewPlan.layer setBorderWidth:0.8f];
    [_btnTripChargeNewPlan.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnTripChargeNewPlan.layer setShadowOpacity:0.3];
    [_btnTripChargeNewPlan.layer setShadowRadius:3.0];
    [_btnTripChargeNewPlan.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnMileageChargeNewPlan.layer setCornerRadius:5.0f];
    [_btnMileageChargeNewPlan.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnMileageChargeNewPlan.layer setBorderWidth:0.8f];
    [_btnMileageChargeNewPlan.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnMileageChargeNewPlan.layer setShadowOpacity:0.3];
    [_btnMileageChargeNewPlan.layer setShadowRadius:3.0];
    [_btnMileageChargeNewPlan.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}

#pragma mark- ****** Local Methods For Image *******

-(void)setButtonImage:(UIButton*)btn
{
    
    for (int k1=0; k1<buttons.count; k1++) {
        
        UIButton *button=buttons[k1];
        
        if ([button isEqual:btn])
        {//RadioButton-Selected.png
            // [button setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            
            if (k1==0) {
                
                [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                
            }else if (k1==3){
                
                [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==4){
                
                [_btnAutoChargeCustomer setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==5){
                
                [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==6){
                
                [_btnNoChange setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==2){
                
                [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==1){
                
                [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }
        }
        else
        {
            // [button setImage:[UIImage imageNamed:@"RadioButton-UnSelected.png"] forState:UIControlStateNormal];
            if (k1==0) {
                
                [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                
            }else if (k1==3){
                
                [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==4){
                
                [_btnAutoChargeCustomer setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==5){
                
                [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==6){
                
                [_btnNoChange setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==2){
                
                [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==1){
                
                [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }
            
        }
        
        
    }
    
}

-(void)openGalleryy
{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera
{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)getImagePreviewFrontImage
{
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"forCheckFrontImageDelete"];
        [defs setBool:NO forKey:@"forCheckBackImageDelete"];
        [defs synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ImagePreviewGeneralInfoAppointmentViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
        objByProductVC.arrOfImages=arrOFImagesName;
        objByProductVC.indexOfImage=@"0";
        //objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;//strWorkOrderStatuss;
        //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
        // [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
-(void)getImagePreviewBackImage
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"forCheckBackImageDelete"];
    [defs setBool:NO forKey:@"forCheckFrontImageDelete"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                             bundle: nil];
    ImagePreviewGeneralInfoAppointmentViewiPad
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
    objByProductVC.arrOfImages=arrOfCheckBackImage;
    objByProductVC.indexOfImage=@"0";
    //objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;//strWorkOrderStatuss;
    //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    // [self.navigationController pushViewController:objByProductVC animated:NO];
}
#pragma mark- ****** Local Table Create Method ******

-(void)tableCreate
{
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
}
-(void)setTableFrame:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, 500);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    UILabel *lblOk;
    lblOk=[[UILabel alloc]init];
    lblOk.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    lblOk.textColor=[UIColor whiteColor];
    lblOk.text=@"OK";
    lblOk.textAlignment=NSTextAlignmentCenter;
    lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-80, tblData.frame.origin.y+tblData.frame.size.height+2, 220, 50);
    if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
    {
        lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-60, tblData.frame.origin.y+tblData.frame.size.height+2, 120, 40);
    }
    lblOk.font=[UIFont systemFontOfSize:22];
    lblOk.layer.borderWidth=1.0;
    lblOk.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    lblOk.layer.cornerRadius=5.0;
    [viewBackGround addSubview:lblOk];
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
        case 20:
        {
            [self setTableFrame:i];
            break;
        }
        case 30:
        {
            [self setTableFrame:i];
            break;
        }
        case 40:
        {
            [self setTableFrame:i];
            break;
        }
        case 50:
        {
            [self setTableFrame:i];
            break;
        }
        case 60:
        {
            [self setTableFrame:i];
            break;
        }
        case 70:
        {
            [self setTableFrame:i];
            break;
        }
        case 80:
        {
            [self setTableFrame:i];
            break;
        }
        case 90:
        {
            [self setTableFrame:i];
            break;
        }
        case 107:
        {
            [self setTableFrame:i];
            break;
        }
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
}
#pragma mark- ****** Local AddView ******

-(void)addView :(NSString*)strTypee
{
    if(isUpdateViewsFrame == NO)// Akshay
    {
        
        if (isForFirstTime) {
            isForFirstTime=NO;
            paymentDetailHeight=_viewForPaymentDetail.frame.size.height;
            if([strWoType isEqualToString:@"TM"])
            {
                paymentDetailHeight = 1750;
            }
            else
            {
                paymentDetailHeight = 1600;
            }
            paymentModeHeight=_viewForPaymentMode.frame.size.height;
            officeNotesHeight=_viewOfficeNotes.frame.size.height;
            checkDetailHeight=_viewForCheckDetail.frame.size.height;
            singleAmountHeight=_viewForSingleAmount.frame.size.height;
            employeeSheetHeight=_view_EmployeeTimeSheet.frame.size.height;
        }
        
        if ([strWoType isEqualToString:@"TM"])
        {
            //_const_ViewHours_H.constant=100;
            //_const_ViewHours_H.constant=100;
            [_viewTMBillableHrs setHidden:NO];
            
            _tblApproveServiceParts.scrollEnabled=NO;
            _tblDeclineServiceParts.scrollEnabled=NO;
            _tblCompletedServiceParts.scrollEnabled=NO;
            
#pragma mark - ***************** FOR PARTS *****************
            _const_TableApproveParts_H.constant=_tblApproveServiceParts.rowHeight*arrApp.count+_tblApproveServiceParts.numberOfSections*50+arrIndexGlobalToShowEquipHeaderApprovedTM.count*50;//arrOfSubWorkServiceIssuesRepair.count;
            _const_TableDeclineParts_H.constant=_tblDeclineServiceParts.rowHeight*arrDecl.count+_tblDeclineServiceParts.numberOfSections*50+arrIndexGlobalToShowEquipHeaderDeclinedTM.count*50;//arrOfSubWorkServiceIssuesRepair.count;
            _const_TableCompleteParts_H.constant=_tblCompletedServiceParts.rowHeight*arrComp.count+_tblCompletedServiceParts.numberOfSections*50+arrIndexGlobalToShowEquipHeaderCompletedTM.count*50;
            
            if(arrApp.count==0)
            {
                //_const_ApproveParts_H.constant=0;
                _viewForApproveTableServiceParts.hidden=YES;
            }
            else
            {
                //_const_ApproveParts_H.constant=50;
                _viewForApproveTableServiceParts.hidden=NO;
                
            }
            if(arrDecl.count==0)
            {
                //_const_DeclineParts_H.constant=0;
                _viewForDeclineTableServiceParts.hidden=YES;
                
                
            }
            else
            {
                //_const_DeclineParts_H.constant=50;
                _viewForDeclineTableServiceParts.hidden=NO;
                
                
            }
            if(arrComp.count==0)
            {
                //_const_CompletedParts_H.constant=0;
                _viewForCompleteTableServiceParts.hidden=YES;
                
            }
            else
            {
                //_const_CompletedParts_H.constant=50;
                _viewForCompleteTableServiceParts.hidden=NO;
                
                
            }
            
            _tblApproveServiceParts.frame=CGRectMake(_tblApproveServiceParts.frame.origin.x, CGRectGetMaxY(_lblApproveParts.frame)+5, _tblApproveServiceParts.frame.size.width, _const_TableApproveParts_H.constant);
            
            _tblDeclineServiceParts.frame=CGRectMake(_tblDeclineServiceParts.frame.origin.x, CGRectGetMaxY(_lblDeclinedParts.frame)+5, _tblDeclineServiceParts.frame.size.width, _const_TableDeclineParts_H.constant);
            
            _tblCompletedServiceParts.frame=CGRectMake(_tblCompletedServiceParts.frame.origin.x, CGRectGetMaxY(_lblCompletedParts.frame)+5, _tblCompletedServiceParts.frame.size.width, _const_TableCompleteParts_H.constant);
            
            _viewForCustomerDetail.frame=CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, _viewForCustomerDetail.frame.size.height);
            
            [_viewForCustomerDetail setFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, _viewForCustomerDetail.frame.size.height)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForCustomerDetail];
            }
            
            
            //Yahaa call krna DB
            if ([strTypee isEqualToString:@"add"]) {
                [self fetchChecklistSavedInDB];
            }
            
            
            _viewForServiceSummary.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame)+100, [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height);
            
            [_viewForServiceSummary setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame)+100, [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForServiceSummary];
            }
            
            
            if(arrComp.count==0)
            {
                _viewForCompleteTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, 0);
                
                [_viewForCompleteTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, 0)];
            }
            else
            {
                _viewForCompleteTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableCompleteParts_H.constant+100);
                
                [_viewForCompleteTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableCompleteParts_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForCompleteTableServiceParts];
            }
            
            if(arrApp.count==0)
            {
                
                _viewForApproveTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width, 0);
                
                [_viewForApproveTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width, 0)];
            }
            else
            {
                
                _viewForApproveTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableApproveParts_H.constant+100);
                
                [_viewForApproveTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableApproveParts_H.constant+100)];
                
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForApproveTableServiceParts];
            }
            
            if(arrDecl.count==0)
            {
                _viewForDeclineTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width,0);
                
                [_viewForDeclineTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width,0)];
            }
            else
            {
                _viewForDeclineTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+10, [UIScreen mainScreen].bounds.size.width, _const_TableDeclineParts_H.constant+100);
                
                [_viewForDeclineTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+10, [UIScreen mainScreen].bounds.size.width, _const_TableDeclineParts_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForDeclineTableServiceParts];
            }
            
            
            // Add EmployeeSheet
            //view_ButtonCollapseEmployeeSheet
            
            if (!isIncludeDetailOnInvoice) {
                
                [_view_ButtonCollapseEmployeeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame),[UIScreen mainScreen].bounds.size.width,_view_ButtonCollapseEmployeeSheet.frame.size.height)];
                
            } else {
                
                [_view_ButtonCollapseEmployeeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForDeclineTableServiceParts.frame.size.height+_viewForDeclineTableServiceParts.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_view_ButtonCollapseEmployeeSheet.frame.size.height)];
                
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_view_ButtonCollapseEmployeeSheet];
            }
            
            CGFloat HeightToSet=0;
            
            if (isExpandEmpSheet) {
                HeightToSet=employeeSheetHeight;
                [_view_EmployeeTimeSheet setHidden:NO];
            } else {
                HeightToSet=0;
                [_view_EmployeeTimeSheet setHidden:YES];
            }
            
            [_view_EmployeeTimeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_ButtonCollapseEmployeeSheet.frame.size.height+_view_ButtonCollapseEmployeeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_view_EmployeeTimeSheet];
            }
            
            if (isIncludeDetailOnInvoice) {
                
                _lblServiceSummary.hidden=NO;
                _viewPaymentnNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y+100,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height);
                
                [_viewPaymentnNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y+100-100,[UIScreen mainScreen].bounds.size.width,160)];
                
            } else {
                
                _viewForApproveTableServiceParts.hidden=YES;
                _viewForDeclineTableServiceParts.hidden=YES;
                _viewForCompleteTableServiceParts.hidden=YES;
                _lblServiceSummary.hidden=YES;
                
                _viewPaymentnNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height);
                
                [_viewPaymentnNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,160)];
                
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewPaymentnNotes];
            }
            
            HeightToSet=0;
            
            if (isExpandPaymentView) {
                HeightToSet=paymentDetailHeight;
                [_viewForPaymentDetail setHidden:NO];
            } else {
                HeightToSet=0;
                [_viewForPaymentDetail setHidden:YES];
            }
            
            _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,HeightToSet);
            
            [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if (!IsShowPricingAndNotesOnApp) {
                
                [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,0)];
                [_viewForPaymentDetail setHidden:YES];
                [_btnCollapsePaymentView setHidden:YES];

            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForPaymentDetail];
            }
            
            if (isPaymentTypeCollapseExpand) {
                HeightToSet=paymentModeHeight;
                [_viewForPaymentMode setHidden:NO];
            } else {
                HeightToSet=0;
                [_viewForPaymentMode setHidden:YES];
            }
            
            [_view_PaymentTypeCollapseExpand setHidden:NO];
            if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                
                HeightToSet=0;
                [_viewForPaymentMode setHidden:YES];
                [_view_PaymentTypeCollapseExpand setHidden:YES];

            }
            
            _view_PaymentTypeCollapseExpand.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewPaymentnNotes.frame)+5,[UIScreen mainScreen].bounds.size.width,_view_PaymentTypeCollapseExpand.frame.size.height);

            _viewForPaymentMode.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_view_PaymentTypeCollapseExpand.frame)+5,[UIScreen mainScreen].bounds.size.width,HeightToSet);
            
            [_viewForPaymentMode setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x,_view_PaymentTypeCollapseExpand.frame.origin.y+_view_PaymentTypeCollapseExpand.frame.size.height+5,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForPaymentMode];
                [_scrollView addSubview:_view_PaymentTypeCollapseExpand];

            }
            
            if (checkView==YES)
            {
                
                if (isPaymentTypeCollapseExpand) {
                    HeightToSet=checkDetailHeight;
                    [_viewForCheckDetail setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewForCheckDetail setHidden:YES];
                }
                
                if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                    
                    HeightToSet=0;
                    [_viewForCheckDetail setHidden:YES];
                    
                } _viewForCheckDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x,_viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet);
                
                [_viewForCheckDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x,_viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewForCheckDetail];
                }
                
                if (isExpandPaymentView) {
                    HeightToSet=officeNotesHeight;
                    [_viewOfficeNotes setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewOfficeNotes setHidden:YES];
                }
                
                if (!IsShowPricingAndNotesOnApp) {
                    
                    //HeightToSet=officeNotesHeight;
                    //[_viewOfficeNotes setHidden:NO];
                    
                    if (isPaymentTypeCollapseExpand) {
                        HeightToSet=officeNotesHeight;
                        [_viewOfficeNotes setHidden:NO];
                    } else {
                        HeightToSet=0;
                        [_viewOfficeNotes setHidden:YES];
                    }
                    
                }
                
                _viewOfficeNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCheckDetail.frame)+5,_viewForCustomerDetail.frame.size.width, HeightToSet);
                
                [_viewOfficeNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForCheckDetail.frame.origin.y+_viewForCheckDetail.frame.size.height+5,_viewForCustomerDetail.frame.size.width, HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewOfficeNotes];
                }
            }
            else
            {
                [_viewForCheckDetail removeFromSuperview];
                
                if (isPaymentTypeCollapseExpand) {
                    HeightToSet=singleAmountHeight;
                    [_viewForSingleAmount setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewForSingleAmount setHidden:YES];
                }
                
                if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                    
                    HeightToSet=0;
                    [_viewForSingleAmount setHidden:YES];
                    
                }
                
                [self showHideAmountView];
                
                _viewForSingleAmount.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet);
                
                [_viewForSingleAmount setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewForSingleAmount];
                }
                
                if (isExpandPaymentView) {
                    HeightToSet=officeNotesHeight;
                    [_viewOfficeNotes setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewOfficeNotes setHidden:YES];
                }
                
                if (!IsShowPricingAndNotesOnApp) {
                    
                    //HeightToSet=officeNotesHeight;
                   // [_viewOfficeNotes setHidden:NO];
                    
                    if (isPaymentTypeCollapseExpand) {
                        HeightToSet=officeNotesHeight;
                        [_viewOfficeNotes setHidden:NO];
                    } else {
                        HeightToSet=0;
                        [_viewOfficeNotes setHidden:YES];
                    }
                    
                }
                
                _viewOfficeNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForSingleAmount.frame.origin.y+_viewForSingleAmount.frame.size.height,_viewForCustomerDetail.frame.size.width, HeightToSet);
                
                [_viewOfficeNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForSingleAmount.frame.origin.y+_viewForSingleAmount.frame.size.height,_viewForCustomerDetail.frame.size.width, HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewOfficeNotes];
                }
                
            }
            
            
            
            _viewForComments.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewOfficeNotes.frame.origin.y+_viewOfficeNotes.frame.size.height,_viewForCustomerDetail.frame.size.width, _viewForComments.frame.size.height);
            
            [_viewForComments setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewOfficeNotes.frame.origin.y+_viewOfficeNotes.frame.size.height,_viewForCustomerDetail.frame.size.width, _viewForComments.frame.size.height)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForComments];
            }
            
            NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
            
            BOOL isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
            
            CGFloat heightServiceJobDesc=0.0;
            
            if (isNoServiceJobDescriptions) {
                
                heightServiceJobDesc=0.0;
                [_viewServiceJobDescriptions setHidden:YES];
                
            }else{
                
                heightServiceJobDesc=_viewServiceJobDescriptions.frame.size.height;
                [_viewServiceJobDescriptions setHidden:NO];
            }
            
            _viewServiceJobDescriptions.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForComments.frame)+5,[UIScreen mainScreen].bounds.size.width,heightServiceJobDesc);
            
            [_viewServiceJobDescriptions setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForComments.frame)+5,[UIScreen mainScreen].bounds.size.width,heightServiceJobDesc)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewServiceJobDescriptions];
            }
            
            _viewForSign.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewServiceJobDescriptions.frame),_viewForCustomerDetail.frame.size.width, _viewForSign.frame.size.height);
            
            [_viewForSign setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewServiceJobDescriptions.frame),_viewForCustomerDetail.frame.size.width, _viewForSign.frame.size.height)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForSign];
            }
            
            //_viewForSign.backgroundColor=[UIColor redColor];
            
            _viewForTermsConditions.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForTermsConditions.frame.size.height);
            
            [_viewForTermsConditions setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForTermsConditions.frame.size.height)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForTermsConditions];
            }
            
            [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_viewForTermsConditions.frame)+50)];
            
        }
        else if ([strWoType isEqualToString:@"FR"])
        {
#pragma mark - ***************** FOR ISSUE *****************
            
            _const_ViewHours_H.constant=0;
            
            _const_TableApprove_H.constant=0;
            _const_TableDecline_H.constant=0;
            _const_TableComplete_H.constant=0;
            
            _tblApprove.scrollEnabled=NO;
            _tblDecline.scrollEnabled=NO;
            _tblComplete.scrollEnabled=NO;
            
            _const_TableApprove_H.constant=_tblApprove.rowHeight*arrApp.count+_tblApprove.numberOfSections*50+arrIndexGlobalToShowEquipHeaderApproved.count*50;
            _const_TableDecline_H.constant=_tblDecline.rowHeight*arrDecl.count+_tblDecline.numberOfSections*50+arrIndexGlobalToShowEquipHeaderDeclined.count*50;
            _const_TableComplete_H.constant=_tblComplete.rowHeight*arrComp.count+_tblComplete.numberOfSections*50+arrIndexGlobalToShowEquipHeaderCompleted.count*50;
            
            NSLog(@"Constant Height TableView Approve====%f",_const_TableApprove_H.constant);
            NSLog(@"Constant Height TableView Decline====%f",_const_TableDecline_H.constant);
            NSLog(@"Constant Height TableView Complete====%f",_const_TableComplete_H.constant);
            
            //_const_TableComplete_H.constant=_const_TableComplete_H.constant+100;
            //NSLog(@"Constant Height TableView Complete====%f",_const_TableComplete_H.constant);
            
            if(arrApp.count==0)
            {
                //_lblApproveParts.hidden=YES;
                //_const_LblApprove_H.constant=0;
                _viewForApproveTable.hidden=YES;
            }
            else
            {
                //_lblApproveParts.hidden=NO;
                //_const_LblApprove_H.constant=50;
                _viewForApproveTable.hidden=NO;
                
            }
            if(arrDecl.count==0)
            {
                // _lblDeclinedParts.hidden=YES;
                //_const_LblDecline_H.constant=0;
                _viewForDeclineTable.hidden=YES;
                
            }
            else
            {
                //_lblDeclinedParts.hidden=NO;
                //_const_LblDecline_H.constant=50;
                _viewForDeclineTable.hidden=NO;
                
            }
            if(arrComp.count==0)
            {
                // _lblCompletedParts.hidden=YES;
                //_const_LblComplete_H.constant=0;
                _viewForCompleteTable.hidden=YES;
                
            }
            else
            {
                //_lblCompletedParts.hidden=YES;
                // _const_LblComplete_H.constant=50;
                _viewForCompleteTable.hidden=NO;
                
            }
            
            
            _tblApprove.frame=CGRectMake(_tblApprove.frame.origin.x, _tblApprove.frame.origin.y, _tblApprove.frame.size.width, _const_TableApprove_H.constant);
            
            _tblDecline.frame=CGRectMake(_tblDecline.frame.origin.x, _tblDecline.frame.origin.y, _tblDecline.frame.size.width, _const_TableDecline_H.constant);
            
            _tblComplete.frame=CGRectMake(_tblComplete.frame.origin.x, _tblComplete.frame.origin.y, _tblComplete.frame.size.width, _const_TableComplete_H.constant);
            
            
            _viewForCustomerDetail.frame=CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, _viewForCustomerDetail.frame.size.height);
            
            [_viewForCustomerDetail setFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, _viewForCustomerDetail.frame.size.height)];
            
            if ([strTypee isEqualToString:@"add"]) {
                
                [_scrollView addSubview:_viewForCustomerDetail];
                [self fetchChecklistSavedInDB];
                
            }
            
            
            //Yahaa call krna DB
            
            // [self fetchChecklistSavedInDB];
            
            
            _viewForServiceSummary.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame), [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height);
            
            //  _viewForServiceSummary.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height);
            
            [_viewForServiceSummary setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame)+100, [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForServiceSummary];
            }
            
            if(arrComp.count==0)
            {
                _viewForCompleteTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, 0);
                
                [_viewForCompleteTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, 0)];
            }
            else
            {
                _viewForCompleteTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableComplete_H.constant+100);
                [_viewForCompleteTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableComplete_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForCompleteTable];
            }
            
            if(arrApp.count==0)
            {
                _viewForApproveTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTable.frame)+5, [UIScreen mainScreen].bounds.size.width,0);
                
                [_viewForApproveTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTable.frame)+5, [UIScreen mainScreen].bounds.size.width,0)];
            }
            else
            {
                _viewForApproveTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTable.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableApprove_H.constant+100);
                [_viewForApproveTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTable.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableApprove_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForApproveTable];
            }
            
            if(arrDecl.count==0)
            {
                _viewForDeclineTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTable.frame)+5, [UIScreen mainScreen].bounds.size.width, 0);
                
                [_viewForDeclineTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTable.frame)+5, [UIScreen mainScreen].bounds.size.width, 0)];
            }
            else
            {
                _viewForDeclineTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTable.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableDecline_H.constant+100);
                
                [_viewForDeclineTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTable.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableDecline_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForDeclineTable];
            }
            
            [_viewTMBillableHrs setHidden:YES];
            
            
            // Add EmployeeSheet
            //view_ButtonCollapseEmployeeSheet
            
            if (!isIncludeDetailOnInvoice) {
                
                [_view_ButtonCollapseEmployeeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame),[UIScreen mainScreen].bounds.size.width,_view_ButtonCollapseEmployeeSheet.frame.size.height)];
                
            } else {
                
                [_view_ButtonCollapseEmployeeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForDeclineTable.frame.size.height+_viewForDeclineTable.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_view_ButtonCollapseEmployeeSheet.frame.size.height)];
                
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_view_ButtonCollapseEmployeeSheet];
            }
            
            CGFloat HeightToSet=0;
            
            if (isExpandEmpSheet) {
                HeightToSet=employeeSheetHeight;
                [_view_EmployeeTimeSheet setHidden:NO];
            } else {
                HeightToSet=0;
                [_view_EmployeeTimeSheet setHidden:YES];
            }
            
            [_view_EmployeeTimeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_ButtonCollapseEmployeeSheet.frame.size.height+_view_ButtonCollapseEmployeeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_view_EmployeeTimeSheet];
            }
            
            
            if (isIncludeDetailOnInvoice) {
                
                _lblServiceSummary.hidden=NO;
                
                _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewForPaymentDetail.frame.size.height);
                
                [_viewPaymentnNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height)];
                
            } else {
                
                _lblServiceSummary.hidden=YES;
                _viewForApproveTable.hidden=YES;
                _viewForDeclineTable.hidden=YES;
                _viewForCompleteTable.hidden=YES;
                
                _viewPaymentnNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height);
                
                [_viewPaymentnNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height)];
                
            }
            
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewPaymentnNotes];
            }
            
            HeightToSet=0;
            
            if (isExpandPaymentView) {
                HeightToSet=paymentDetailHeight;
                [_viewForPaymentDetail setHidden:NO];
            } else {
                HeightToSet=0;
                [_viewForPaymentDetail setHidden:YES];
            }
            
            _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,_viewForPaymentDetail.frame.size.height);
            
            [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if (!IsShowPricingAndNotesOnApp) {
                
                [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,0)];
                [_viewForPaymentDetail setHidden:YES];
                [_btnCollapsePaymentView setHidden:YES];
                
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForPaymentDetail];
            }
            
            if (isPaymentTypeCollapseExpand) {
                HeightToSet=paymentModeHeight;
                [_viewForPaymentMode setHidden:NO];
            } else {
                HeightToSet=0;
                [_viewForPaymentMode setHidden:YES];
            }
            
            [_view_PaymentTypeCollapseExpand setHidden:NO];
            if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                
                HeightToSet=0;
                [_viewForPaymentMode setHidden:YES];
                [_view_PaymentTypeCollapseExpand setHidden:YES];
                
            }
            
            _view_PaymentTypeCollapseExpand.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,[UIScreen mainScreen].bounds.size.width,_view_PaymentTypeCollapseExpand.frame.size.height);
            
            _viewForPaymentMode.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_view_PaymentTypeCollapseExpand.frame)+5,[UIScreen mainScreen].bounds.size.width,_viewForPaymentMode.frame.size.height);
            
            [_viewForPaymentMode setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x,_view_PaymentTypeCollapseExpand.frame.origin.y+_view_PaymentTypeCollapseExpand.frame.size.height+5,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForPaymentMode];
                [_scrollView addSubview:_view_PaymentTypeCollapseExpand];

            }
            
            if (checkView==YES)
            {
                
                if (isPaymentTypeCollapseExpand) {
                    HeightToSet=checkDetailHeight;
                    [_viewForCheckDetail setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewForCheckDetail setHidden:YES];
                }
                
                if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                    
                    HeightToSet=0;
                    [_viewForCheckDetail setHidden:YES];
                    
                }
                
                _viewForCheckDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentMode.frame)+5,_viewForCustomerDetail.frame.size.width,_viewForCheckDetail.frame.size.height);
                
                [_viewForCheckDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewForCheckDetail];
                }
                
                if (isExpandPaymentView) {
                    HeightToSet=officeNotesHeight;
                    [_viewOfficeNotes setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewOfficeNotes setHidden:YES];
                }
                
                if (!IsShowPricingAndNotesOnApp) {
                    
                    //HeightToSet=officeNotesHeight;
                    //[_viewOfficeNotes setHidden:NO];
                    
                    if (isPaymentTypeCollapseExpand) {
                        HeightToSet=officeNotesHeight;
                        [_viewOfficeNotes setHidden:NO];
                    } else {
                        HeightToSet=0;
                        [_viewOfficeNotes setHidden:YES];
                    }
                    
                }
                
                _viewOfficeNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCheckDetail.frame)+5,_viewForCustomerDetail.frame.size.width, _viewOfficeNotes.frame.size.height);
                
                [_viewOfficeNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x,_viewForCheckDetail.frame.origin.y+_viewForCheckDetail.frame.size.height+5,_viewForCustomerDetail.frame.size.width, HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewOfficeNotes];
                }
            }
            else
            {
                [_viewForCheckDetail removeFromSuperview];
                
                if (isPaymentTypeCollapseExpand) {
                    HeightToSet=singleAmountHeight;
                    [_viewForSingleAmount setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewForSingleAmount setHidden:YES];
                }
                
                if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                    
                    HeightToSet=0;
                    [_viewForSingleAmount setHidden:YES];
                    
                }
                
                [self showHideAmountView];

                _viewForSingleAmount.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentMode.frame)+5,_viewForCustomerDetail.frame.size.width,_viewForSingleAmount.frame.size.height);
                
                [_viewForSingleAmount setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewForSingleAmount];
                }
                
                if (isExpandPaymentView) {
                    HeightToSet=officeNotesHeight;
                    [_viewOfficeNotes setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewOfficeNotes setHidden:YES];
                }
                
                if (!IsShowPricingAndNotesOnApp) {
                    
                    //HeightToSet=officeNotesHeight;
                    //[_viewOfficeNotes setHidden:NO];
                    
                    if (isPaymentTypeCollapseExpand) {
                        HeightToSet=officeNotesHeight;
                        [_viewOfficeNotes setHidden:NO];
                    } else {
                        HeightToSet=0;
                        [_viewOfficeNotes setHidden:YES];
                    }
                    
                }
                
                _viewOfficeNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSingleAmount.frame),_viewForCustomerDetail.frame.size.width, _viewOfficeNotes.frame.size.height);
                
                [_viewOfficeNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForSingleAmount.frame.origin.y+_viewForSingleAmount.frame.size.height,_viewForCustomerDetail.frame.size.width, HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewOfficeNotes];
                }
                
            }
            
            _viewForComments.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewOfficeNotes.frame.origin.y+_viewOfficeNotes.frame.size.height,_viewForCustomerDetail.frame.size.width, _viewForComments.frame.size.height);
            
            [_viewForComments setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewOfficeNotes.frame.origin.y+_viewOfficeNotes.frame.size.height,_viewForCustomerDetail.frame.size.width, _viewForComments.frame.size.height)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForComments];
            }
            
            NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
            
            BOOL isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
            
            CGFloat heightServiceJobDesc=0.0;
            
            if (isNoServiceJobDescriptions) {
                
                heightServiceJobDesc=0.0;
                [_viewServiceJobDescriptions setHidden:YES];
                
            }else{
                
                heightServiceJobDesc=_viewServiceJobDescriptions.frame.size.height;
                [_viewServiceJobDescriptions setHidden:NO];
            }
            
            _viewServiceJobDescriptions.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForComments.frame)+5,[UIScreen mainScreen].bounds.size.width,heightServiceJobDesc);
            
            [_viewServiceJobDescriptions setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForComments.frame)+5,[UIScreen mainScreen].bounds.size.width,heightServiceJobDesc)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewServiceJobDescriptions];
            }
            
            _viewForSign.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewServiceJobDescriptions.frame),_viewForCustomerDetail.frame.size.width, _viewForSign.frame.size.height);
            
            [_viewForSign setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewServiceJobDescriptions.frame),_viewForCustomerDetail.frame.size.width, _viewForSign.frame.size.height)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForSign];
            }
            
            //_viewForSign.backgroundColor=[UIColor redColor];
            _viewForTermsConditions.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForTermsConditions.frame.size.height);
            [_viewForTermsConditions setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForTermsConditions.frame.size.height)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForTermsConditions];
            }
            
            [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_viewForTermsConditions.frame)+50)]   ;
        }
        
    }
    else
    {
        if (isForFirstTime) {
            isForFirstTime=NO;
            paymentDetailHeight=_viewForPaymentDetail.frame.size.height;
            paymentModeHeight=_viewForPaymentMode.frame.size.height;
            officeNotesHeight=_viewOfficeNotes.frame.size.height;
            checkDetailHeight=_viewForCheckDetail.frame.size.height;
            singleAmountHeight=_viewForSingleAmount.frame.size.height;
            employeeSheetHeight=_view_EmployeeTimeSheet.frame.size.height;
        }
        
        if ([strWoType isEqualToString:@"TM"])
        {
            [_viewTMBillableHrs setHidden:NO];
            
            _tblApproveServiceParts.scrollEnabled=NO;
            _tblDeclineServiceParts.scrollEnabled=NO;
            _tblCompletedServiceParts.scrollEnabled=NO;
            
#pragma mark - ***************** FOR PARTS *****************
            _const_TableApproveParts_H.constant=_tblApproveServiceParts.rowHeight*arrApp.count+_tblApproveServiceParts.numberOfSections*50+arrIndexGlobalToShowEquipHeaderApprovedTM.count*50;//arrOfSubWorkServiceIssuesRepair.count;
            _const_TableDeclineParts_H.constant=_tblDeclineServiceParts.rowHeight*arrDecl.count+_tblDeclineServiceParts.numberOfSections*50+arrIndexGlobalToShowEquipHeaderDeclinedTM.count*50;//arrOfSubWorkServiceIssuesRepair.count;
            _const_TableCompleteParts_H.constant=_tblCompletedServiceParts.rowHeight*arrComp.count+_tblCompletedServiceParts.numberOfSections*50+arrIndexGlobalToShowEquipHeaderCompletedTM.count*50;
            
            if(arrApp.count==0)
            {
                //_const_ApproveParts_H.constant=0;
                _viewForApproveTableServiceParts.hidden=YES;
            }
            else
            {
                //_const_ApproveParts_H.constant=50;
                _viewForApproveTableServiceParts.hidden=NO;
                
            }
            if(arrDecl.count==0)
            {
                //_const_DeclineParts_H.constant=0;
                _viewForDeclineTableServiceParts.hidden=YES;
                
                
            }
            else
            {
                //_const_DeclineParts_H.constant=50;
                _viewForDeclineTableServiceParts.hidden=NO;
                
                
            }
            if(arrComp.count==0)
            {
                //_const_CompletedParts_H.constant=0;
                _viewForCompleteTableServiceParts.hidden=YES;
                
            }
            else
            {
                //_const_CompletedParts_H.constant=50;
                _viewForCompleteTableServiceParts.hidden=NO;
                
            }
            
            /*  CGRect frameRect3 = _viewForCustomerDetail.frame;
             frameRect3.origin.y = 5;
             _viewForCustomerDetail.frame = frameRect3;*/
            
            
            CGRect frameRect = _tblApproveServiceParts.frame;
            frameRect.size.height = _const_TableApproveParts_H.constant;
            frameRect.origin.y = CGRectGetMaxY(_lblApproveParts.frame)+5;
            _tblApproveServiceParts.frame = frameRect;
            
            // _tblApproveServiceParts.frame=CGRectMake(_tblApproveServiceParts.frame.origin.x, CGRectGetMaxY(_lblApproveParts.frame)+5, _tblApproveServiceParts.frame.size.width, _const_TableApproveParts_H.constant);
            
            
            CGRect frameRect1 = _tblDeclineServiceParts.frame;
            frameRect1.size.height = _const_TableDeclineParts_H.constant;
            frameRect1.origin.y = CGRectGetMaxY(_lblDeclinedParts.frame)+5;
            _tblDeclineServiceParts.frame = frameRect1;
            
            // _tblDeclineServiceParts.frame=CGRectMake(_tblDeclineServiceParts.frame.origin.x, CGRectGetMaxY(_lblDeclinedParts.frame)+5, _tblDeclineServiceParts.frame.size.width, _const_TableDeclineParts_H.constant);
            
            
            CGRect frameRect2 = _tblCompletedServiceParts.frame;
            frameRect2.size.height = _const_TableCompleteParts_H.constant;
            frameRect2.origin.y = CGRectGetMaxY(_lblCompletedParts.frame)+5;
            _tblCompletedServiceParts.frame = frameRect2;
            
            
            // _tblCompletedServiceParts.frame=CGRectMake(_tblCompletedServiceParts.frame.origin.x, CGRectGetMaxY(_lblCompletedParts.frame)+5, _tblCompletedServiceParts.frame.size.width, _const_TableCompleteParts_H.constant);
            
            
            
            CGRect frameRect3 = _viewForCustomerDetail.frame;
            frameRect3.origin.y = 5;
            _viewForCustomerDetail.frame = frameRect3;
            
            //            _viewForCustomerDetail.frame=CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, _viewForCustomerDetail.frame.size.height);
            //
            //            [_viewForCustomerDetail setFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, _viewForCustomerDetail.frame.size.height)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForCustomerDetail];
            }
            
            
            //Yahaa call krna DB
            if ([strTypee isEqualToString:@"add"]) {
                [self fetchChecklistSavedInDB];
            }
            
            CGRect frameRect4 = _viewForServiceSummary.frame;
            frameRect4.origin.y = CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame)+100;
            _viewForServiceSummary.frame = frameRect4;
            
            
            //            _viewForServiceSummary.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame)+100, [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height);
            //
            //            [_viewForServiceSummary setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame)+100, [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForServiceSummary];
            }
            
            
            if(arrComp.count==0)
            {
                CGRect frameRect4 = _viewForCompleteTableServiceParts.frame;
                frameRect4.origin.y = CGRectGetMaxY(_viewForServiceSummary.frame)+5;
                _viewForCompleteTableServiceParts.frame = frameRect4;
                
                //                _viewForCompleteTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, 0);
                //
                //                [_viewForCompleteTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, 0)];
            }
            else
            {
                
                CGRect frameRect4 = _viewForCompleteTableServiceParts.frame;
                frameRect4.origin.y = CGRectGetMaxY(_viewForServiceSummary.frame)+5;
                _viewForCompleteTableServiceParts.frame = frameRect4;
                
                //                _viewForCompleteTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableCompleteParts_H.constant+100);
                //
                //                [_viewForCompleteTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableCompleteParts_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForCompleteTableServiceParts];
            }
            
            if(arrApp.count==0)
            {
                
                CGRect frameRect5 = _viewForApproveTableServiceParts.frame;
                frameRect5.origin.y = CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5;
                _viewForApproveTableServiceParts.frame = frameRect5;
                
                
                /* _viewForApproveTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width, 0);
                 
                 [_viewForApproveTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width, 0)];*/
            }
            else
            {
                CGRect frameRect5 = _viewForApproveTableServiceParts.frame;
                frameRect5.origin.y = CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5;
                _viewForApproveTableServiceParts.frame = frameRect5;
                
                
                //                _viewForApproveTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableApproveParts_H.constant+100);
                //
                //                [_viewForApproveTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableApproveParts_H.constant+100)];
                
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForApproveTableServiceParts];
            }
            
            if(arrDecl.count==0)
            {
                
                CGRect frameRect5 = _viewForDeclineTableServiceParts.frame;
                frameRect5.origin.y = CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+5;
                _viewForDeclineTableServiceParts.frame = frameRect5;
                
                
                //                _viewForDeclineTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width,0);
                //
                //                [_viewForDeclineTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+5, [UIScreen mainScreen].bounds.size.width,0)];
            }
            else
            {
                
                CGRect frameRect5 = _viewForDeclineTableServiceParts.frame;
                frameRect5.origin.y = CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+10;
                _viewForDeclineTableServiceParts.frame = frameRect5;
                
                //                _viewForDeclineTableServiceParts.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+10, [UIScreen mainScreen].bounds.size.width, _const_TableDeclineParts_H.constant+100);
                //
                //                [_viewForDeclineTableServiceParts setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTableServiceParts.frame)+10, [UIScreen mainScreen].bounds.size.width, _const_TableDeclineParts_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForDeclineTableServiceParts];
            }
            
            
            // Add EmployeeSheet
            //view_ButtonCollapseEmployeeSheet
            
            if (!isIncludeDetailOnInvoice) {
                
                //                CGRect frameRect = _view_ButtonCollapseEmployeeSheet.frame;
                //                frameRect.size.height = _view_ButtonCollapseEmployeeSheet.frame.size.height;
                //                frameRect.origin.y = CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame);
                //                _view_ButtonCollapseEmployeeSheet.frame = frameRect;
                
                [_view_ButtonCollapseEmployeeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame),[UIScreen mainScreen].bounds.size.width,_view_ButtonCollapseEmployeeSheet.frame.size.height)];
                
            } else {
                
                
                //                CGRect frameRect = _view_ButtonCollapseEmployeeSheet.frame;
                //                frameRect.size.height = _view_ButtonCollapseEmployeeSheet.frame.size.height;
                //                frameRect.origin.y = CGRectGetMaxY(_viewForDeclineTableServiceParts.frame)+5;
                //                _view_ButtonCollapseEmployeeSheet.frame = frameRect;
                
                [_view_ButtonCollapseEmployeeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForDeclineTableServiceParts.frame.size.height+_viewForDeclineTableServiceParts.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_view_ButtonCollapseEmployeeSheet.frame.size.height)];
                
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_view_ButtonCollapseEmployeeSheet];
            }
            
            CGFloat HeightToSet=0;
            
            if (isExpandEmpSheet) {
                HeightToSet=employeeSheetHeight;
                [_view_EmployeeTimeSheet setHidden:NO];
            } else {
                HeightToSet=0;
                [_view_EmployeeTimeSheet setHidden:YES];
            }
            
            //            CGRect frameRect12 = _view_EmployeeTimeSheet.frame;
            //            frameRect12.size.height = HeightToSet;
            //            frameRect12.origin.y = CGRectGetMaxY(_view_ButtonCollapseEmployeeSheet.frame);
            //            _view_EmployeeTimeSheet.frame = frameRect12;
            
            
            
            [_view_EmployeeTimeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_ButtonCollapseEmployeeSheet.frame.size.height+_view_ButtonCollapseEmployeeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_view_EmployeeTimeSheet];
            }
            
            if (isIncludeDetailOnInvoice) {
                
                _lblServiceSummary.hidden=NO;
                //                CGRect frameRect = _viewPaymentnNotes.frame;
                //                frameRect.size.height = 160;
                //                frameRect.origin.y = CGRectGetMaxY(_view_EmployeeTimeSheet.frame)+1;
                //                _viewPaymentnNotes.frame = frameRect;
                
                
                _viewPaymentnNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y+100,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height);
                
                [_viewPaymentnNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y+100-100,[UIScreen mainScreen].bounds.size.width,160)];
                
            } else {
                
                _viewForApproveTableServiceParts.hidden=YES;
                _viewForDeclineTableServiceParts.hidden=YES;
                _viewForCompleteTableServiceParts.hidden=YES;
                _lblServiceSummary.hidden=YES;
                
                
                //                CGRect frameRect = _viewPaymentnNotes.frame;
                //                frameRect.size.height = 160;
                //                frameRect.origin.y = CGRectGetMaxY(_view_EmployeeTimeSheet.frame);
                //                _viewPaymentnNotes.frame = frameRect;
                
                
                _viewPaymentnNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height);
                
                [_viewPaymentnNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,160)];
                
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewPaymentnNotes];
            }
            
            HeightToSet=0;
            
            if (isExpandPaymentView) {
                //   HeightToSet=paymentDetailHeight;
                
                HeightToSet=heightPaymntDetailView;
                [_viewForPaymentDetail setHidden:NO];
            } else {
                HeightToSet=0;
                [_viewForPaymentDetail setHidden:YES];
            }
            
            //            CGRect frameRect15 = _viewForPaymentDetail.frame;
            //            frameRect15.size.height = HeightToSet;
            //            _viewForPaymentDetail.frame = frameRect15;
            
            
            _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,HeightToSet);
            
            [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if (!IsShowPricingAndNotesOnApp) {

                [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,0)];
                [_viewForPaymentDetail setHidden:YES];
                [_btnCollapsePaymentView setHidden:YES];

            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForPaymentDetail];
            }
            
            if (isPaymentTypeCollapseExpand) {
                HeightToSet=paymentModeHeight;
                [_viewForPaymentMode setHidden:NO];
            } else {
                HeightToSet=0;
                [_viewForPaymentMode setHidden:YES];
            }
            
            [_view_PaymentTypeCollapseExpand setHidden:NO];
            if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                
                HeightToSet=0;
                [_viewForPaymentMode setHidden:YES];
                [_view_PaymentTypeCollapseExpand setHidden:YES];
                
            }
            
            //            CGRect frameRect112 = _viewForPaymentMode.frame;
            //            frameRect112.size.height = HeightToSet;
            //            frameRect112.origin.y = CGRectGetMaxY(_viewForPaymentDetail.frame)+5;
            //            _viewForPaymentMode.frame = frameRect112;
            
            _view_PaymentTypeCollapseExpand.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,[UIScreen mainScreen].bounds.size.width,_view_PaymentTypeCollapseExpand.frame.size.height);

            _viewForPaymentMode.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_view_PaymentTypeCollapseExpand.frame)+5,[UIScreen mainScreen].bounds.size.width,HeightToSet);
            
            [_viewForPaymentMode setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x,_view_PaymentTypeCollapseExpand.frame.origin.y+_view_PaymentTypeCollapseExpand.frame.size.height+5,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForPaymentMode];
                [_scrollView addSubview:_view_PaymentTypeCollapseExpand];

            }
            
            if (checkView==YES)
            {
                
                if (isPaymentTypeCollapseExpand) {
                    HeightToSet=checkDetailHeight;
                    [_viewForCheckDetail setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewForCheckDetail setHidden:YES];
                }
                
                if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                    
                    HeightToSet=0;
                    [_viewForCheckDetail setHidden:YES];
                    
                }
                
                if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                    
                    HeightToSet=0;
                    [_viewForCheckDetail setHidden:YES];
                    
                }
                
                //                CGRect frameRect2 = _viewForCheckDetail.frame;
                //                frameRect2.size.height = HeightToSet;
                //                frameRect2.origin.y = CGRectGetMaxY(_viewForPaymentMode.frame)+5;
                //                _viewForCheckDetail.frame = frameRect2;
                
                
                _viewForCheckDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x,_viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet);
                
                [_viewForCheckDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x,_viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewForCheckDetail];
                }
                
                if (isPaymentTypeCollapseExpand) {
                    HeightToSet=officeNotesHeight;
                    [_viewOfficeNotes setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewOfficeNotes setHidden:YES];
                }
                if (!IsShowPricingAndNotesOnApp) {
                    
                    //HeightToSet=officeNotesHeight;
                    //[_viewOfficeNotes setHidden:NO];
                    
                    if (isPaymentTypeCollapseExpand) {
                        HeightToSet=officeNotesHeight;
                        [_viewOfficeNotes setHidden:NO];
                    } else {
                        HeightToSet=0;
                        [_viewOfficeNotes setHidden:YES];
                    }
                    
                }
                
                //                CGRect frameRect = _viewOfficeNotes.frame;
                //                frameRect.size.height = HeightToSet;
                //                frameRect.origin.y = CGRectGetMaxY(_viewForPaymentMode.frame)+5;
                //                _viewOfficeNotes.frame = frameRect;
                
                _viewOfficeNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCheckDetail.frame)+5,_viewForCustomerDetail.frame.size.width, HeightToSet);
                
                [_viewOfficeNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForCheckDetail.frame.origin.y+_viewForCheckDetail.frame.size.height+5,_viewForCustomerDetail.frame.size.width, HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewOfficeNotes];
                }
            }
            else
            {
                [_viewForCheckDetail removeFromSuperview];
                
                if (isPaymentTypeCollapseExpand) {
                    HeightToSet=singleAmountHeight;
                    [_viewForSingleAmount setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewForSingleAmount setHidden:YES];
                }
                
                if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                    
                    HeightToSet=0;
                    [_viewForSingleAmount setHidden:YES];
                    
                }
                
                [self showHideAmountView];

                //                CGRect frameRect = _viewForSingleAmount.frame;
                //                frameRect.size.height = HeightToSet;
                //                frameRect.origin.y = CGRectGetMaxY(_viewForPaymentMode.frame)+5;
                //                _viewForSingleAmount.frame = frameRect;
                
                _viewForSingleAmount.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet);
                
                [_viewForSingleAmount setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewForSingleAmount];
                }
                
                if (isExpandPaymentView) {
                    HeightToSet=officeNotesHeight;
                    [_viewOfficeNotes setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewOfficeNotes setHidden:YES];
                }
                
                if (!IsShowPricingAndNotesOnApp) {
                    
                    //HeightToSet=officeNotesHeight;
                    //[_viewOfficeNotes setHidden:NO];
                    
                    if (isPaymentTypeCollapseExpand) {
                        HeightToSet=officeNotesHeight;
                        [_viewOfficeNotes setHidden:NO];
                    } else {
                        HeightToSet=0;
                        [_viewOfficeNotes setHidden:YES];
                    }
                    
                }
                //                CGRect frameRect1 = _viewOfficeNotes.frame;
                //                frameRect1.size.height = HeightToSet;
                //                frameRect1.origin.y = CGRectGetMaxY(_viewForSingleAmount.frame);
                //                _viewOfficeNotes.frame = frameRect1;
                
                _viewOfficeNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForSingleAmount.frame.origin.y+_viewForSingleAmount.frame.size.height,_viewForCustomerDetail.frame.size.width, HeightToSet);
                
                [_viewOfficeNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForSingleAmount.frame.origin.y+_viewForSingleAmount.frame.size.height,_viewForCustomerDetail.frame.size.width, HeightToSet)];
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewOfficeNotes];
                }
            }
            
            //            CGRect frameRect13 = _viewForComments.frame;
            //            frameRect13.origin.y = CGRectGetMaxY(_viewOfficeNotes.frame);
            //            _viewForComments.frame = frameRect13;
            
            
            _viewForComments.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewOfficeNotes.frame.origin.y+_viewOfficeNotes.frame.size.height,_viewForCustomerDetail.frame.size.width, _viewForComments.frame.size.height);
            
            [_viewForComments setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewOfficeNotes.frame.origin.y+_viewOfficeNotes.frame.size.height,_viewForCustomerDetail.frame.size.width, _viewForComments.frame.size.height)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForComments];
            }
            
            NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
            
            BOOL isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
            
            CGFloat heightServiceJobDesc=0.0;
            
            if (isNoServiceJobDescriptions) {
                
                heightServiceJobDesc=0.0;
                [_viewServiceJobDescriptions setHidden:YES];
                
            }else{
                
                heightServiceJobDesc=_viewServiceJobDescriptions.frame.size.height;
                [_viewServiceJobDescriptions setHidden:NO];
            }
            
            
            //            CGRect frameRect14 = _viewServiceJobDescriptions.frame;
            //            frameRect14.origin.y = CGRectGetMaxY(_viewForComments.frame)+5;
            //            _viewServiceJobDescriptions.frame = frameRect14;
            
            
            _viewServiceJobDescriptions.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForComments.frame)+5,[UIScreen mainScreen].bounds.size.width,heightServiceJobDesc);
            
            [_viewServiceJobDescriptions setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForComments.frame)+5,[UIScreen mainScreen].bounds.size.width,heightServiceJobDesc)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewServiceJobDescriptions];
            }
            
            
            //            CGRect frameRect5 = _viewForSign.frame;
            //            frameRect5.origin.y = CGRectGetMaxY(_viewServiceJobDescriptions.frame);
            //            _viewForSign.frame = frameRect5;
            
            
            _viewForSign.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewServiceJobDescriptions.frame),_viewForCustomerDetail.frame.size.width, _viewForSign.frame.size.height);
            
            [_viewForSign setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewServiceJobDescriptions.frame),_viewForCustomerDetail.frame.size.width, _viewForSign.frame.size.height)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForSign];
            }
            
            //_viewForSign.backgroundColor=[UIColor redColor];
            
            
            
            //            CGRect frameRect6 = _viewForTermsConditions.frame;
            //            frameRect6.origin.y = CGRectGetMaxY(_viewForSign.frame);
            //            _viewForTermsConditions.frame = frameRect6;
            
            
            _viewForTermsConditions.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForTermsConditions.frame.size.height);
            
            [_viewForTermsConditions setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForTermsConditions.frame.size.height)];
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForTermsConditions];
            }
            
            [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_viewForTermsConditions.frame)+50)];
            
        }
        else if ([strWoType isEqualToString:@"FR"])
        {
#pragma mark - ***************** FOR ISSUE *****************
            
            _const_ViewHours_H.constant=0;
            
            _const_TableApprove_H.constant=0;
            _const_TableDecline_H.constant=0;
            _const_TableComplete_H.constant=0;
            
            _tblApprove.scrollEnabled=NO;
            _tblDecline.scrollEnabled=NO;
            _tblComplete.scrollEnabled=NO;
            
            _const_TableApprove_H.constant=_tblApprove.rowHeight*arrApp.count+_tblApprove.numberOfSections*50+arrIndexGlobalToShowEquipHeaderApproved.count*50;
            _const_TableDecline_H.constant=_tblDecline.rowHeight*arrDecl.count+_tblDecline.numberOfSections*50+arrIndexGlobalToShowEquipHeaderDeclined.count*50;
            _const_TableComplete_H.constant=_tblComplete.rowHeight*arrComp.count+_tblComplete.numberOfSections*50+arrIndexGlobalToShowEquipHeaderCompleted.count*50;
            
            NSLog(@"Constant Height TableView Approve====%f",_const_TableApprove_H.constant);
            NSLog(@"Constant Height TableView Decline====%f",_const_TableDecline_H.constant);
            NSLog(@"Constant Height TableView Complete====%f",_const_TableComplete_H.constant);
            
            //_const_TableComplete_H.constant=_const_TableComplete_H.constant+100;
            //NSLog(@"Constant Height TableView Complete====%f",_const_TableComplete_H.constant);
            
            if(arrApp.count==0)
            {
                //_lblApproveParts.hidden=YES;
                //_const_LblApprove_H.constant=0;
                _viewForApproveTable.hidden=YES;
            }
            else
            {
                //_lblApproveParts.hidden=NO;
                //_const_LblApprove_H.constant=50;
                _viewForApproveTable.hidden=NO;
                
            }
            if(arrDecl.count==0)
            {
                // _lblDeclinedParts.hidden=YES;
                //_const_LblDecline_H.constant=0;
                _viewForDeclineTable.hidden=YES;
                
            }
            else
            {
                //_lblDeclinedParts.hidden=NO;
                //_const_LblDecline_H.constant=50;
                _viewForDeclineTable.hidden=NO;
                
            }
            if(arrComp.count==0)
            {
                // _lblCompletedParts.hidden=YES;
                //_const_LblComplete_H.constant=0;
                _viewForCompleteTable.hidden=YES;
                
            }
            else
            {
                //_lblCompletedParts.hidden=YES;
                // _const_LblComplete_H.constant=50;
                _viewForCompleteTable.hidden=NO;
                
            }
            
            
            _tblApprove.frame=CGRectMake(_tblApprove.frame.origin.x, _tblApprove.frame.origin.y, _tblApprove.frame.size.width, _const_TableApprove_H.constant);
            
            _tblDecline.frame=CGRectMake(_tblDecline.frame.origin.x, _tblDecline.frame.origin.y, _tblDecline.frame.size.width, _const_TableDecline_H.constant);
            
            _tblComplete.frame=CGRectMake(_tblComplete.frame.origin.x, _tblComplete.frame.origin.y, _tblComplete.frame.size.width, _const_TableComplete_H.constant);
            
            
            _viewForCustomerDetail.frame=CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, _viewForCustomerDetail.frame.size.height);
            
            [_viewForCustomerDetail setFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, _viewForCustomerDetail.frame.size.height)];
            
            if ([strTypee isEqualToString:@"add"]) {
                
                [_scrollView addSubview:_viewForCustomerDetail];
                [self fetchChecklistSavedInDB];
                
            }
            
            
            //Yahaa call krna DB
            
            // [self fetchChecklistSavedInDB];
            
            
            _viewForServiceSummary.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame), [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height);
            
            //  _viewForServiceSummary.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height);
            
            [_viewForServiceSummary setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame)+100, [UIScreen mainScreen].bounds.size.width, _viewForServiceSummary.frame.size.height)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForServiceSummary];
            }
            
            if(arrComp.count==0)
            {
                _viewForCompleteTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, 0);
                
                [_viewForCompleteTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, 0)];
            }
            else
            {
                _viewForCompleteTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableComplete_H.constant+100);
                [_viewForCompleteTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForServiceSummary.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableComplete_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForCompleteTable];
            }
            
            if(arrApp.count==0)
            {
                _viewForApproveTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTable.frame)+5, [UIScreen mainScreen].bounds.size.width,0);
                
                [_viewForApproveTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTable.frame)+5, [UIScreen mainScreen].bounds.size.width,0)];
            }
            else
            {
                _viewForApproveTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTable.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableApprove_H.constant+100);
                [_viewForApproveTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCompleteTable.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableApprove_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForApproveTable];
            }
            
            if(arrDecl.count==0)
            {
                _viewForDeclineTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTable.frame)+5, [UIScreen mainScreen].bounds.size.width, 0);
                
                [_viewForDeclineTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTable.frame)+5, [UIScreen mainScreen].bounds.size.width, 0)];
            }
            else
            {
                _viewForDeclineTable.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTable.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableDecline_H.constant+100);
                
                [_viewForDeclineTable setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForApproveTable.frame)+5, [UIScreen mainScreen].bounds.size.width, _const_TableDecline_H.constant+100)];
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForDeclineTable];
            }
            
            [_viewTMBillableHrs setHidden:YES];
            
            
            // Add EmployeeSheet
            //view_ButtonCollapseEmployeeSheet
            
            if (!isIncludeDetailOnInvoice) {
                
                [_view_ButtonCollapseEmployeeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCustomerDetail.frame)+5+CGRectGetMaxY(ViewFormSections.frame),[UIScreen mainScreen].bounds.size.width,_view_ButtonCollapseEmployeeSheet.frame.size.height)];
                
            } else {
                
                [_view_ButtonCollapseEmployeeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForDeclineTable.frame.size.height+_viewForDeclineTable.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_view_ButtonCollapseEmployeeSheet.frame.size.height)];
                
            }
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_view_ButtonCollapseEmployeeSheet];
            }
            
            CGFloat HeightToSet=0;
            
            if (isExpandEmpSheet) {
                HeightToSet=employeeSheetHeight;
                [_view_EmployeeTimeSheet setHidden:NO];
            } else {
                HeightToSet=0;
                [_view_EmployeeTimeSheet setHidden:YES];
            }
            
            [_view_EmployeeTimeSheet setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_ButtonCollapseEmployeeSheet.frame.size.height+_view_ButtonCollapseEmployeeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_view_EmployeeTimeSheet];
            }
            
            
            if (isIncludeDetailOnInvoice) {
                
                _lblServiceSummary.hidden=NO;
                
                
                /* akshay
                 _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewForPaymentDetail.frame.size.height);*/
                
                
                [_viewPaymentnNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height)];
                
                _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewPaymentnNotes.frame)+5,[UIScreen mainScreen].bounds.size.width,_viewForPaymentDetail.frame.size.height);
                
            } else {
                
                _lblServiceSummary.hidden=YES;
                _viewForApproveTable.hidden=YES;
                _viewForDeclineTable.hidden=YES;
                _viewForCompleteTable.hidden=YES;
                
                _viewPaymentnNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height);
                
                [_viewPaymentnNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _view_EmployeeTimeSheet.frame.size.height+_view_EmployeeTimeSheet.frame.origin.y,[UIScreen mainScreen].bounds.size.width,_viewPaymentnNotes.frame.size.height)];
                
                // _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewPaymentnNotes.frame)+5,[UIScreen mainScreen].bounds.size.width,_viewForPaymentDetail.frame.size.height);
                
            }
            
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewPaymentnNotes];
            }
            
            HeightToSet=0;
            
            if (isExpandPaymentView) {
                HeightToSet=paymentDetailHeight;
                [_viewForPaymentDetail setHidden:NO];
                // akshay
                CGRect frameRect = _viewForPaymentDetail.frame;
                frameRect.size.height = heightPaymntDetailView;
                _viewForPaymentDetail.frame = frameRect;
                //
                
                _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,_viewForPaymentDetail.frame.size.height);
                
                [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,heightPaymntDetailView)];
                
                if (!IsShowPricingAndNotesOnApp) {
                    
                    [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,0)];
                    [_viewForPaymentDetail setHidden:YES];
                    [_btnCollapsePaymentView setHidden:YES];
                    
                }
                
            }
            else
            {
                HeightToSet=0;
                [_viewForPaymentDetail setHidden:YES];
                
                // akshay
                CGRect frameRect = _viewForPaymentDetail.frame;
                frameRect.size.height = 0;
                _viewForPaymentDetail.frame = frameRect;
                //
                
                _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,_viewForPaymentDetail.frame.size.height);
                
                [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,0)];
                
                if (!IsShowPricingAndNotesOnApp) {
                    
                    [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,0)];
                    [_viewForPaymentDetail setHidden:YES];
                    [_btnCollapsePaymentView setHidden:YES];

                }
                
            }
            
            //             _viewForPaymentDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,_viewForPaymentDetail.frame.size.height);
            //
            //            [_viewForPaymentDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewPaymentnNotes.frame.size.height+_viewPaymentnNotes.frame.origin.y+0,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForPaymentDetail];
            }
            
            if (isPaymentTypeCollapseExpand) {
                HeightToSet=paymentModeHeight;
                [_viewForPaymentMode setHidden:NO];
            } else {
                HeightToSet=0;
                [_viewForPaymentMode setHidden:YES];
            }
            
            [_view_PaymentTypeCollapseExpand setHidden:NO];
            if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                
                HeightToSet=0;
                [_viewForPaymentMode setHidden:YES];
                [_view_PaymentTypeCollapseExpand setHidden:YES];
                
            }
            
            CGRect frameRect2 = _view_PaymentTypeCollapseExpand.frame;
            frameRect2.size.height = _view_PaymentTypeCollapseExpand.frame.size.height;
            frameRect2.origin.y = CGRectGetMaxY(_viewForPaymentDetail.frame)+5;
            _view_PaymentTypeCollapseExpand.frame = frameRect2;
            
            CGRect frameRect21 = _viewForPaymentMode.frame;
            frameRect21.size.height = HeightToSet;
            frameRect21.origin.y = CGRectGetMaxY(_view_PaymentTypeCollapseExpand.frame)+5;
            _viewForPaymentMode.frame = frameRect21;
            
            
            //            _viewForPaymentMode.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,[UIScreen mainScreen].bounds.size.width,_viewForPaymentMode.frame.size.height);
            //
            //            [_viewForPaymentMode setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x,_viewForPaymentDetail.frame.origin.y+_viewForPaymentDetail.frame.size.height+5,[UIScreen mainScreen].bounds.size.width,HeightToSet)];
            
            
            if ([strTypee isEqualToString:@"add"]) {
                
                [_scrollView addSubview:_viewForPaymentMode];
                [_scrollView addSubview:_view_PaymentTypeCollapseExpand];
                
            }
            
            if (checkView==YES)
            {
                
                if (isPaymentTypeCollapseExpand) {
                    HeightToSet=checkDetailHeight;
                    [_viewForCheckDetail setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewForCheckDetail setHidden:YES];
                }
                if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                    
                    HeightToSet=0;
                    [_viewForCheckDetail setHidden:YES];
                    
                }
                
                CGRect frameRect1 = _viewForCheckDetail.frame;
                frameRect1.size.height = HeightToSet;
                frameRect1.origin.y = CGRectGetMaxY(_viewForPaymentMode.frame)+5;
                _viewForCheckDetail.frame = frameRect1;
                
                
                /* Akshay    _viewForCheckDetail.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentMode.frame)+5,_viewForCustomerDetail.frame.size.width,_viewForCheckDetail.frame.size.height);
                 
                 [_viewForCheckDetail setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet)];
                 */
                
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewForCheckDetail];
                }
                
                if (isExpandPaymentView) {
                    HeightToSet=officeNotesHeight;
                    [_viewOfficeNotes setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewOfficeNotes setHidden:YES];
                }
                
                if (!IsShowPricingAndNotesOnApp) {
                    
                    //HeightToSet=officeNotesHeight;
                    //[_viewOfficeNotes setHidden:NO];
                    
                    if (isPaymentTypeCollapseExpand) {
                        HeightToSet=officeNotesHeight;
                        [_viewOfficeNotes setHidden:NO];
                    } else {
                        HeightToSet=0;
                        [_viewOfficeNotes setHidden:YES];
                    }
                    
                }
                CGRect frameRect3 = _viewOfficeNotes.frame;
                frameRect3.size.height = HeightToSet;
                frameRect3.origin.y = CGRectGetMaxY(_viewForCheckDetail.frame)+5;
                _viewOfficeNotes.frame = frameRect3;
                
                
                /* Akshay    _viewOfficeNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForCheckDetail.frame)+5,_viewForCustomerDetail.frame.size.width, _viewOfficeNotes.frame.size.height);
                 
                 [_viewOfficeNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x,_viewForCheckDetail.frame.origin.y+_viewForCheckDetail.frame.size.height+5,_viewForCustomerDetail.frame.size.width, HeightToSet)];*/
                
                
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewOfficeNotes];
                }
            }
            else
            {
                [_viewForCheckDetail removeFromSuperview];
                
                if (isPaymentTypeCollapseExpand) {
                    HeightToSet=singleAmountHeight;
                    [_viewForSingleAmount setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewForSingleAmount setHidden:YES];
                }
                
                if ((IsShowPricingAndNotesOnApp) && (!isExpandPaymentView)) {
                    
                    HeightToSet=0;
                    [_viewForSingleAmount setHidden:YES];
                    
                }
                
                [self showHideAmountView];

                CGRect frameRect3 = _viewForSingleAmount.frame;
                frameRect3.size.height = HeightToSet;
                frameRect3.origin.y = CGRectGetMaxY(_viewForPaymentMode.frame)+5;
                _viewForSingleAmount.frame = frameRect3;
                
                
                /* Akshay   _viewForSingleAmount.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentMode.frame)+5,_viewForCustomerDetail.frame.size.width,_viewForSingleAmount.frame.size.height);
                 
                 [_viewForSingleAmount setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForPaymentMode.frame.origin.y+_viewForPaymentMode.frame.size.height+5,_viewForCustomerDetail.frame.size.width,HeightToSet)];*/
                
                
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewForSingleAmount];
                }
                
                if (isExpandPaymentView) {
                    HeightToSet=officeNotesHeight;
                    [_viewOfficeNotes setHidden:NO];
                } else {
                    HeightToSet=0;
                    [_viewOfficeNotes setHidden:YES];
                }
                
                if (!IsShowPricingAndNotesOnApp) {
                    
                    //HeightToSet=officeNotesHeight;
                    //[_viewOfficeNotes setHidden:NO];
                    
                    if (isPaymentTypeCollapseExpand) {
                        HeightToSet=officeNotesHeight;
                        [_viewOfficeNotes setHidden:NO];
                    } else {
                        HeightToSet=0;
                        [_viewOfficeNotes setHidden:YES];
                    }
                    
                }
                CGRect frameRect4 = _viewOfficeNotes.frame;
                frameRect4.size.height = HeightToSet;
                frameRect4.origin.y = CGRectGetMaxY(_viewForSingleAmount.frame)+5;
                _viewOfficeNotes.frame = frameRect4;
                
                /* Akshay _viewOfficeNotes.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSingleAmount.frame),_viewForCustomerDetail.frame.size.width, _viewOfficeNotes.frame.size.height);
                 
                 [_viewOfficeNotes setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewForSingleAmount.frame.origin.y+_viewForSingleAmount.frame.size.height,_viewForCustomerDetail.frame.size.width, HeightToSet)];*/
                if ([strTypee isEqualToString:@"add"]) {
                    [_scrollView addSubview:_viewOfficeNotes];
                }
                
            }
            
            
            CGRect frameRect5 = _viewForComments.frame;
            frameRect5.origin.y = CGRectGetMaxY(_viewOfficeNotes.frame)+5;
            _viewForComments.frame = frameRect5;
            
            
            /* Akshay  _viewForComments.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewOfficeNotes.frame.origin.y+_viewOfficeNotes.frame.size.height,_viewForCustomerDetail.frame.size.width, _viewForComments.frame.size.height);
             
             [_viewForComments setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, _viewOfficeNotes.frame.origin.y+_viewOfficeNotes.frame.size.height,_viewForCustomerDetail.frame.size.width, _viewForComments.frame.size.height)];*/
            
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForComments];
            }
            
            NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
            
            BOOL isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
            
            CGFloat heightServiceJobDesc=0.0;
            
            if (isNoServiceJobDescriptions) {
                
                heightServiceJobDesc=0.0;
                [_viewServiceJobDescriptions setHidden:YES];
                
            }else{
                
                heightServiceJobDesc=_viewServiceJobDescriptions.frame.size.height;
                [_viewServiceJobDescriptions setHidden:NO];
            }
            
            
            CGRect frameRect6 = _viewServiceJobDescriptions.frame;
            frameRect6.size.height = heightServiceJobDesc;
            frameRect6.origin.y = CGRectGetMaxY(_viewForComments.frame)+5;
            _viewServiceJobDescriptions.frame = frameRect6;
            
            
            /* Akshay  _viewServiceJobDescriptions.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForComments.frame)+5,[UIScreen mainScreen].bounds.size.width,heightServiceJobDesc);
             
             [_viewServiceJobDescriptions setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForComments.frame)+5,[UIScreen mainScreen].bounds.size.width,heightServiceJobDesc)];*/
            
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewServiceJobDescriptions];
            }
            
            CGRect frameRect8 = _viewForSign.frame;
            frameRect8.origin.y = CGRectGetMaxY(_viewServiceJobDescriptions.frame)+5;
            _viewForSign.frame = frameRect8;
            
            
            
            /* Akshay
             _viewForSign.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewServiceJobDescriptions.frame),_viewForCustomerDetail.frame.size.width, _viewForSign.frame.size.height);
             
             [_viewForSign setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewServiceJobDescriptions.frame),_viewForCustomerDetail.frame.size.width, _viewForSign.frame.size.height)];
             */
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForSign];
            }
            
            //_viewForSign.backgroundColor=[UIColor redColor];
            
            CGRect frameRect9 = _viewForTermsConditions.frame;
            frameRect9.origin.y = CGRectGetMaxY(_viewForSign.frame)+5;
            _viewForTermsConditions.frame = frameRect9;
            
            
            /* Akshay
             _viewForTermsConditions.frame=CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForTermsConditions.frame.size.height);
             [_viewForTermsConditions setFrame:CGRectMake(_viewForCustomerDetail.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _viewForTermsConditions.frame.size.height)];
             */
            
            if ([strTypee isEqualToString:@"add"]) {
                [_scrollView addSubview:_viewForTermsConditions];
            }
            
            [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_viewForTermsConditions.frame)+50)]   ;
        }
        
    }
    
    _buttonSelectCredit_Apply.layer.cornerRadius = 2.0;
    _buttonSelectCredit_Apply.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _buttonSelectCredit_Apply.layer.borderWidth = 1.0;
    _buttonSelectCredit_Add.layer.borderColor   = [UIColor lightGrayColor].CGColor;
    _buttonSelectCredit_Add.layer.cornerRadius = 2.0;
    _buttonSelectCredit_Add.layer.borderWidth = 1.0;
}
//Nilind 21 June
//============================================================================
//============================================================================
#pragma mark- ----------------Core Data Fetch Methods----------------
//============================================================================
//============================================================================

-(void)fetchSubWorkOrderIssuesFromDataBaseForMechanical
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssues = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssues setEntity:entitySubWorkOrderIssues];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssues setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssues = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssues = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssues];
    
    [requestSubWorkOrderIssues setSortDescriptors:sortDescriptorsSubWorkOrderIssues];
    
    self.fetchedResultsControllerSubWorkOrderIssues = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssues managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssues setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssues performFetch:&error];
    arrAllObjSubWorkOrderIssues = [self.fetchedResultsControllerSubWorkOrderIssues fetchedObjects];
    arrIssueId=[[NSMutableArray alloc]init];
    
    if ([arrAllObjSubWorkOrderIssues count] == 0)
    {
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        // [_tblViewServiceIssue reloadData];
        // [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssues.count; k++) {
            
            matchesSubWorkOrderIssues=arrAllObjSubWorkOrderIssues[k];
            NSLog(@"%@",[matchesSubWorkOrderIssues valueForKey:@"workorderId"]);
            [arrIssueId addObject:[matchesSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
            [arrOfSubWorkServiceIssues addObject:matchesSubWorkOrderIssues];
            
        }
        
        
        // change for equip header
        
        NSMutableArray *tempArrOfArrangedIssues=[[NSMutableArray alloc]init];
        
        NSMutableArray *arrOfIndexesToShowEquipHeader=[[NSMutableArray alloc]init];
        
        NSIndexSet* indexes = [arrOfSubWorkServiceIssues indexesOfObjectsPassingTest:^BOOL (NSManagedObject* tempObj, NSUInteger idx, BOOL *stop) {
            return [[tempObj valueForKey:@"equipmentCode"]  isEqualToString: @""];
        }];
        NSArray* recordsForNoEquip = [arrOfSubWorkServiceIssues objectsAtIndexes:indexes];
        
        if (recordsForNoEquip.count>0) {
            
            [tempArrOfArrangedIssues addObjectsFromArray:recordsForNoEquip];
            
            [arrOfSubWorkServiceIssues removeObjectsAtIndexes:indexes];
            
            [arrOfIndexesToShowEquipHeader addObject:@""];
            
        }
        
        while (arrOfSubWorkServiceIssues.count>0) {
            
            NSManagedObject *objNewTemp=arrOfSubWorkServiceIssues[0];
            NSString *strEquipCode=[NSString stringWithFormat:@"%@",[objNewTemp valueForKey:@"equipmentCode"]];
            
            [arrOfIndexesToShowEquipHeader addObject:strEquipCode];
            
            NSIndexSet* indexes = [arrOfSubWorkServiceIssues indexesOfObjectsPassingTest:^BOOL (NSManagedObject* tempObj, NSUInteger idx, BOOL *stop) {
                return [[tempObj valueForKey:@"equipmentCode"]  isEqualToString: strEquipCode];
            }];
            NSArray* recordsForNoEquip = [arrOfSubWorkServiceIssues objectsAtIndexes:indexes];
            
            [tempArrOfArrangedIssues addObjectsFromArray:recordsForNoEquip];
            
            [arrOfSubWorkServiceIssues removeObjectsAtIndexes:indexes];
            
        }
        
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        
        arrIssueId=[[NSMutableArray alloc]init];
        arrOfSubWorkServiceIssues=tempArrOfArrangedIssues;
        
        for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
            
            NSManagedObject *objTemp=arrOfSubWorkServiceIssues[k];
            [arrIssueId addObject:[objTemp valueForKey:@"subWorkOrderIssueId"]];
            
        }
        
        /*
         arrIndexGlobalToShowEquipHeader=[[NSMutableArray alloc]init];
         
         for (int l=0; l<arrOfIndexesToShowEquipHeader.count; l++) {
         
         //NSManagedObject *objTemp=arrOfIndexesToShowEquipHeader[l];
         NSString *strEquipCode=[NSString stringWithFormat:@"%@",arrOfIndexesToShowEquipHeader[l]];
         
         for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
         
         NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
         NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
         if ([strEquipCode isEqualToString:strEquipCode1]) {
         
         NSString *strIndex=[NSString stringWithFormat:@"%d",m];
         
         [arrIndexGlobalToShowEquipHeader addObject:strIndex];
         
         break;
         }
         
         }
         }
         */
        
        //End Change for equip header
        
        
    }
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    //indexToShow=intGlobalSection;
    
    //[self adjustViewHeightsonCollapsingSections];
    
}

-(void)fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        // [_tblViewServiceIssue reloadData];
        //[_tblApprove reloadData];
        //[self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        
        arrApproveId=[[NSMutableArray alloc]init];
        arrDeclinedId=[[NSMutableArray alloc]init];
        arrCompleted=[[NSMutableArray alloc]init];
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepair.count; k++) {
            
            matchesSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[k];
            NSString *str=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepair valueForKey:@"customerFeedback"]];
            NSString *strComplete=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepair valueForKey:@"isCompleted"]];
            if([str isEqualToString:@"1"]||[str isEqualToString:@"True"]||[str isEqualToString:@"true"])
            {
                
                if ([strComplete isEqualToString:@"1"]||[strComplete isEqualToString:@"true"]||[strComplete isEqualToString:@"True"])
                {
                    [arrCompleted addObject:@""];
                }
                else
                {
                    [arrApproveId addObject:@"a"];
                }
            }
            else
            {
                [arrDeclinedId addObject:@"a"];
            }
            
            
            [arrOfSubWorkServiceIssuesRepair addObject:matchesSubWorkOrderIssuesRepair];
            
        }
        //[_tblApprove reloadData];
        [self addView:@"Notadd"];
        //[self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    //issueRepairPartId
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    NSPredicate *predicate;
    
    if ([strWoType isEqualToString:@"TM"]) {
        
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch];
        
    } else {
        
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
        
    }
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
        arrApproveParts=[[NSMutableArray alloc]init];
        arrDeclinedParts=[[NSMutableArray alloc]init];
        arrCompletedParts=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"workorderId"]);
            [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
            
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"customerFeedback"]];
            NSString *strComplete=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"isCompleted"]];
            
            if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
            {
                
                if ([strComplete isEqualToString:@"1"]||[strComplete isEqualToString:@"true"]||[strComplete isEqualToString:@"True"])
                {
                    
                    [arrCompletedParts addObject:@""];
                    
                }else{
                    
                    [arrApproveParts addObject:@""];
                    
                }
            }
            else
            {
                
                [arrDeclinedParts addObject:@""];
                
            }
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :(NSString*)strIssueRepairIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueRepairIdToFetch];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairLabour = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairLabour count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairLabour.count; k++)
        {
            
            matchesSubWorkOrderIssuesRepairLabour=arrAllObjSubWorkOrderIssuesRepairLabour[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairLabour valueForKey:@"workorderId"]);
            [arrOfSubWorkServiceIssuesRepairLabour addObject:matchesSubWorkOrderIssuesRepairLabour];
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

//Nilind 22 June
-(void)fetchWorkOrderFromDataBaseForMechanical
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    NSArray *arrAllObjWorkOrder;
    arrAllObjWorkOrder = [self.fetchedResultsControllerWorkOrderDetails
                          fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        matchesWorkOrder=arrAllObjWorkOrder[0];
        if (strTax.length==0)
        {
            strTax=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"tax"]];
            strAccountNo=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"accountNo"]];
            if (strTax.length==0)
            {
                strTax=@"0";
            }
        }
        
        _lblTechName.text=[global getEmployeeNameViaEMPId:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"employeeNo"]]];
        
        matchesFinalWorkOrder=arrAllObjWorkOrder[0];
        [self showValuesMechanicalGeneralInfo];
        
        //Terms and Conditions
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
        NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceReportTermsAndConditions"];
        
        NSString *strTremsnConditions;
        if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
            
            for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                
                
                NSDictionary *dictData=arrOfServiceJobDescriptionTemplate[k];
                
                NSLog(@"Terms And Conditons===%@",dictData);
                
                if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]] isEqualToString:[NSString stringWithFormat:@"%@",[matchesFinalWorkOrder valueForKey:@"departmentId"]]]) {
                    
                    strTremsnConditions=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SR_TermsAndConditions"]];
                    
                    break;
                    
                }
            }
            
        }else{
            
            strTremsnConditions=@"N/A";
            
        }
        
        
        NSAttributedString *attributedStringTermsnConditions = [[NSAttributedString alloc]
                                                                initWithData: [strTremsnConditions dataUsingEncoding:NSUnicodeStringEncoding]
                                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                                documentAttributes: nil
                                                                error: nil
                                                                ];
        
        _txtViewTermsConditions.attributedText=attributedStringTermsnConditions;
        
        
        NSAttributedString *attributedStringTermsnConditionsNew = [[NSAttributedString alloc]
                                                                   initWithData: [[self getHTML] dataUsingEncoding:NSUnicodeStringEncoding]
                                                                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                                   documentAttributes: nil
                                                                   error: nil
                                                                   ];
        
        _txtViewTermsConditions.attributedText=attributedStringTermsnConditionsNew;
        
        [_txtViewTermsConditions setEditable:NO];
        
        strWorkOrderAccNo=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"accountNo"]];
        
        strWorkOrderAddressId=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceAddressId"]];
        
        strWorkOrderAddressSubType=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"addressSubType"]];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)calculatePspDiscountAgain{
    
    _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%.02f",0.0];
    
    NSString *strstrPspMasterId=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkdOrder valueForKey:@"pSPMasterId"]];
    
    strstrPspMasterId = strPspMasterId1;
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    
    
    //strPurchaseStepOrder=@"2";
    NSMutableArray *arrMechanicalPSPMasters=[[NSMutableArray alloc]init];
    // NSMutableArray *arrPSPMasters=[[NSMutableArray alloc]init];
    arrMechanicalPSPMasters=[dictMechanicalMasters valueForKey:@"PSPMasterExtSerDc"];
    
    //StepOrder
    NSMutableArray *arrTemp;
    arrTemp=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrMechanicalPSPMasters objectAtIndex:i];
        NSString *strType;
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Type"]];
        if([strDepartmentSysName isEqualToString:[dict valueForKey:@"DepartmentSysName"]]&&[strType isEqualToString:@"General"])
        {
            [arrTemp addObject:dict];
            
        }
    }
    for(int i=0;i<arrTemp.count;i++)
    {
        NSDictionary *dict=[arrTemp objectAtIndex:i];
        NSString *strPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
        if ([strPSPId isEqualToString:strstrPspMasterId])
        {
            //_lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
            
            double memberShipDiscout,memberShipCharge,value;
            memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
            memberShipDiscout=[[dict valueForKey:@"Discount"]doubleValue];
            
            value=memberShipCharge*(memberShipDiscout/100);
            
            _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%.02f",value];
            break;
        }
    }
    
}
- (NSString *)getHTML {
    NSDictionary *exportParams = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
    NSData *htmlData = [_txtViewTermsConditions.attributedText dataFromRange:NSMakeRange(0, _txtViewTermsConditions.attributedText.length) documentAttributes:exportParams error:nil];
    return [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
}

-(void)showValuesMechanicalGeneralInfo
{
    
    strGlobalServiceJobDescriptionId=[matchesWorkOrder valueForKey:@"serviceJobDescriptionId"];
    _textView_ServiceJobDescriptions.text=[matchesWorkOrder valueForKey:@"serviceJobDescription"];
    NSString *strServiceJobDescription = [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceJobDescription"]];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [strServiceJobDescription dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
     _textView_ServiceJobDescriptions.attributedText = attributedString;
    
    strGlobalServiceJobDescriptionsText=[matchesWorkOrder valueForKey:@"serviceJobDescription"];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    if ([dictdata isKindOfClass:[NSString class]]) {
        
        //[global AlertMethod:Alert :NoDataAvailableServiceJobDescriptions];
        
    }else{
        
        NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceJobDescriptionTemplate"];
        if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
            
            for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                
                NSDictionary *dictData=[arrOfServiceJobDescriptionTemplate objectAtIndex:k];
                
                if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]] isEqualToString:strGlobalServiceJobDescriptionId]) {
                    
                    [_btnServiceJobDescriptionTitle setTitle:[dictData valueForKey:@"Title"] forState:UIControlStateNormal];
                    
                }
            }
        }
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    NSString *strFirstName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"firstName"]];
    NSString *strMiddleName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"middleName"]];
    NSString *strLastName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"lastName"]];
    
    NSString *audioPath=[NSString stringWithFormat:@"%@",[matches valueForKey:@"audioFilePath"]];
    if (audioPath.length>0) {
        
        if ([audioPath isEqualToString:@"(null)"]) {
            strGlobalAudio=@"";
        } else {
            
            NSLog(@"audioPath====%@",audioPath);
            strGlobalAudio=audioPath;
            [self downloadingAudio:audioPath];
            
        }
    }
    
    NSMutableArray *arrOfName=[[NSMutableArray alloc]init];
    
    if (!(strFirstName.length==0)) {
        [arrOfName addObject:strFirstName];
    }
    if (!(strMiddleName.length==0)) {
        
        [arrOfName addObject:strMiddleName];
        
    }
    if (!(strLastName.length==0)) {
        
        [arrOfName addObject:strLastName];
        
    }
    
    NSString *strFullName=[arrOfName componentsJoinedByString:@" "];
    NSString *strStatuss=[matchesWorkOrder valueForKey:@"workorderStatus"];
    //
    [defs setObject:strFullName forKey:@"customerNameService"];
    //    [defs setObject:strWorkOrderStatuss forKey:@"WoStatus"];
    [defs synchronize];
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:[matchesWorkOrder valueForKey:@"timeIn"]];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:[matchesWorkOrder valueForKey:@"timeIn"]];
    }
    _lblNameValue.text=strFullName;
    
    _lblEmailValue.attributedText=[global getUnderLineAttributedString:[matchesWorkOrder valueForKey:@"primaryEmail"]];
    
    _lblWorkOrderNo.text=[matchesWorkOrder valueForKey:@"workOrderNo"];
    
    _lblAccountNo.text=[matchesWorkOrder valueForKey:@"accountNo"];
    
    NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"thirdPartyAccountNo"]];
    
    if (strThirdPartyAccountNo.length>0) {
        
        _lblAccountNo.text=strThirdPartyAccountNo;
        
    }else{
        
        strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"accountNo"]];
        _lblAccountNo.text=strThirdPartyAccountNo;
        
    }
    
    
    NSString *strBillingcountry=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingCountry"]];
    
    if ([strBillingcountry caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strBillingcountry=@"United States";
    }
    
    NSString *strbillingAddress2=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingAddress2"]];
    if (strbillingAddress2.length==0) {
        
        _lblBillingAddressValue.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"billingAddress1"],[matchesWorkOrder valueForKey:@"billingCity"],[matchesWorkOrder valueForKey:@"billingState"],strBillingcountry,[matchesWorkOrder valueForKey:@"billingZipcode"]];
        
    } else {
        
        _lblBillingAddressValue.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"billingAddress1"],[matchesWorkOrder valueForKey:@"billingAddress2"],[matchesWorkOrder valueForKey:@"billingCity"],[matchesWorkOrder valueForKey:@"billingState"],strBillingcountry,[matchesWorkOrder valueForKey:@"billingZipcode"]];
        
    }
    
    
    NSMutableArray *arrOfTempAddress=[[NSMutableArray alloc]init];
    
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingAddress1"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingAddress1"]]];
        
    }
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingAddress2"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingAddress2"]]];
        
    }
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingCity"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingCity"]]];
        
    }
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingState"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingState"]]];
        
    }
    //    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"CountryName"]].length==0)) {
    //
    //        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"CountryName"]]];
    //
    //    }
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingZipcode"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"billingZipcode"]]];
        
    }
    
    NSString *strCombinedAddress;
    
    if (arrOfTempAddress.count>0) {
        
        strCombinedAddress=[arrOfTempAddress componentsJoinedByString:@", "];
        
    }
    
    _lblBillingAddressValue.attributedText=[global getUnderLineAttributedString:strCombinedAddress];
    
    
    NSString *strTempString1=[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"billingAddress1"],[matchesWorkOrder valueForKey:@"billingAddress2"],[matchesWorkOrder valueForKey:@"billingCity"],[matchesWorkOrder valueForKey:@"billingState"],strBillingcountry,[matchesWorkOrder valueForKey:@"billingZipcode"]];
    
    strTempString1=[strTempString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *strServiceAddress=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceCountry"]];
    
    _txtViewTechnicianComment.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"technicianComment"]];
    _txtViewOfficeNotes.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"officeNotes"]];
    _txtViewOfficeNotes.text=[NSString stringWithFormat:@"%@",[matchesFinalWorkOrder valueForKey:@"officeNotes"]];
    _txtViewTechnicianComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewTechnicianComment.layer.borderWidth=1.0;
    
    _txtViewTechnicianComment.layer.cornerRadius=5.0;
    
    _txtViewOfficeNotes.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewOfficeNotes.layer.borderWidth=1.0;
    
    _txtViewOfficeNotes.layer.cornerRadius=5.0;
    
    
    if ([strServiceAddress caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strServiceAddress=@"United States";
    }
    
    NSString *strServiceAddress2=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingAddress2"]];
    if (strServiceAddress2.length==0) {
        
        _lblServiceAddressValue.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"servicesAddress1"],[matchesWorkOrder valueForKey:@"serviceCity"],[matchesWorkOrder valueForKey:@"serviceState"],strServiceAddress,[matchesWorkOrder valueForKey:@"serviceZipcode"]];
        
        
    }else
    {
        
        _lblServiceAddressValue.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"servicesAddress1"],[matchesWorkOrder valueForKey:@"serviceAddress2"],[matchesWorkOrder valueForKey:@"serviceCity"],[matchesWorkOrder valueForKey:@"serviceState"],strServiceAddress,[matchesWorkOrder valueForKey:@"serviceZipcode"]];
        
    }
    
    
    arrOfTempAddress=[[NSMutableArray alloc]init];
    
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"servicesAddress1"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"servicesAddress1"]]];
        
    }
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"serviceAddress2"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"serviceAddress2"]]];
        
    }
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"serviceCity"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"serviceCity"]]];
        
    }
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"serviceState"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"serviceState"]]];
        
    }
    //    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"CountryName"]].length==0)) {
    //
    //        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"CountryName"]]];
    //
    //    }
    if (!([NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"serviceZipcode"]].length==0)) {
        
        [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [matchesWorkOrder valueForKey:@"serviceZipcode"]]];
        
    }
    
    if (arrOfTempAddress.count>0) {
        
        strCombinedAddress=[arrOfTempAddress componentsJoinedByString:@", "];
        
    }
    
    _lblServiceAddressValue.attributedText=[global getUnderLineAttributedString:strCombinedAddress];
    
}
-(void)fetchSubWorkOrderFromDataBase
{
    
    NSManagedObject *matchesSubWo;
    matchesSubWo=[global fetchMechanicalSubWorkOrderObj:strWorkOrderId :_strSubWorkOderId];
    matchesFinalSubWorkdOrder=[global fetchMechanicalSubWorkOrderObj:strWorkOrderId :_strSubWorkOderId];
    //[self ShowValues];
    strCustomerSign=[matchesSubWo valueForKey:@"completeSWO_CustSignPath"];
    strTechnicianSign=[matchesSubWo valueForKey:@"completeSWO_TechSignPath"];
    strAmountPaidInSubWorkOrder=[matchesSubWo valueForKey:@"amtPaid"];
    strWoStatus=[matchesSubWo valueForKey:@"subWOStatus"];
    strWoType=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkdOrder valueForKey:@"subWOType"]];
    
    _lblValueForPSPCharge.text=[NSString stringWithFormat:@"$%@",[matchesFinalSubWorkdOrder valueForKey:@"pSPCharges"]];
    // _lblValueForPSPCharge.text=@"100.00";
    _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%@",[matchesFinalSubWorkdOrder valueForKey:@"pSPDiscount"]];
    // _lblValueForPSPDiscount.text=@"146.12";
    
    // to check if Membership exist or not to show hide membership plan charge and saving label
    
    if ([_lblValueForPSPCharge.text doubleValue]>0) {
        
        _const_MembershipPlanCharge_H.constant=50;
        
    }else{
        
        _const_MembershipPlanCharge_H.constant=0;
        
    }
    
    if ([_lblValueForPSPDiscount.text doubleValue]>0) {
        
        _const_MembershipPlanSaving_H.constant=50;
        
    }else{
        
        _const_MembershipPlanSaving_H.constant=0;
        
    }
    
    strWorkOrderStatuss=[NSString stringWithFormat:@"%@",[matchesSubWo valueForKey:@"subWOStatus"]];
    _lblSubworkOrderNo.text=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkdOrder valueForKey:@"subWorkOrderNo"]];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodToCheckStatus];
    
    strDepartmentSysName=[NSString stringWithFormat:@"%@",[matchesSubWo valueForKey:@"departmentSysName"]];
    
    NSString *strPSPSavingPercent=[global getMemberShipCHargeForMechanical:strServiceUrlMainServiceAutomation :strCompanyKey :strDepartmentSysName :strWorkOrderAccNo];
    
    if (!isCompletedStatusMechanical) {
        
        double memberShipDiscout,memberShipCharge,value;
        memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
        memberShipDiscout=[strPSPSavingPercent doubleValue];
        value=memberShipCharge*(memberShipDiscout/100);
        _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%.02f",value];
        
    }
    
    [self downloadingImagess:strCustomerSign];
    if(CGSizeEqualToSize(imageTemp.size, CGSizeZero))
    {
        imageTemp=[UIImage imageNamed:@"NoImage.jpg"];
    }
    _imgCustomerSign.image=imageTemp;
    
    [self downloadingImagess:strTechnicianSign];
    if(CGSizeEqualToSize(imageTemp.size, CGSizeZero))//if([imageSignInspector isEqual:nil])
    {
        imageTemp=[UIImage imageNamed:@"NoImage.jpg"];
    }
    _imgInspectorSign.image=imageTemp;
    
    strCustomerNotPresent=[matchesSubWo valueForKey:@"completeSWO_IsCustomerNotPresent"];
    
    NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesSubWo valueForKey:@"isStdPrice"]];
    
    if ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]||[strIsActive isEqualToString:@"True"]) {
        
        isStandardSubWorkOrder=YES;
        
    } else {
        
        isStandardSubWorkOrder=NO;
        
    }
    
    if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"])
    {
        [_imgSignCheckBox setImage:[UIImage imageNamed:@"check_box_2.png"]];
        _viewCustomerSign.hidden=YES;
    }
    else
    {
        [_imgSignCheckBox setImage:[UIImage imageNamed:@"check_box_1.png"]];
        _viewCustomerSign.hidden=NO;
        
    }
    strTripChargeId=[matchesSubWo valueForKey:@"tripChargeMasterId"];
    strDiagnosticChargeId=[matchesSubWo valueForKey:@"diagnosticChargeMasterId"];
    strMileageName=[matchesSubWo valueForKey:@"mileageChargesName"];
    strDiagnosticChargeName=[matchesSubWo valueForKey:@"diagnosticChargeName"];
    strTripChargeName=[matchesSubWo valueForKey:@"tripChargeName"];
    _txtMiles.text=[matchesSubWo valueForKey:@"totalMileage"];
    
    NSString *strDateService=[NSString stringWithFormat:@"%@",[matchesSubWo valueForKey:@"serviceDateTime"]];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateService];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=strDateService;
        
    }
    
    _lblDateValue.text=[NSString stringWithFormat:@"%@",finalTime];
    
    _txtOtherDiscounts.text=[matchesSubWo valueForKey:@"discountAmt"];
    
    [self fetchLocalMasters];
    
    for (int i=0; i<arrDiagnostic.count; i++)
    {
        NSDictionary *dict=[arrDiagnostic objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeName"] ]isEqualToString:strDiagnosticChargeName])
        {
            [_btnDiagnosticCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
            
            float amtDC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
            
            _lblValueForDiagnosticeCharge.text=[NSString stringWithFormat:@"$%.02f",amtDC];
            
        }
    }
    for (int i=0; i<arrTripCharge.count; i++)
    {
        NSDictionary *dict=[arrTripCharge objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeName"] ]isEqualToString:strTripChargeName])
        {
            [_btnTipCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
            
            float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
            
            _lblValueForTipCharge.text=[NSString stringWithFormat:@"$%.02f",amtTC];
        }
    }
    
    _txtFld_TripChargeQty.text=[NSString stringWithFormat:@"%@",[matchesSubWo valueForKey:@"tripChargesQty"]];
    _lblTripChargeBasedOnQty.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTipCharge.text]*[_txtFld_TripChargeQty.text intValue]];
    
    //[self calculateTripChargeBasedOnQty];
    
    //Mileage Value
    for (int i=0; i<arrMileageCharge.count; i++)
    {
        NSDictionary *dict=[arrMileageCharge objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeName"] ]isEqualToString:strMileageName])
        {
            [_btnMileageCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
            
            float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
            
            strChargeAmountMiles=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
            
            amtTC = amtTC*[_txtMiles.text floatValue];
            
            _lblMileage.text=[NSString stringWithFormat:@"%.02f",amtTC];
        }
    }
    
    //
    
    
    
    //isHoliday
    
    NSString *strIsHoliday=[NSString stringWithFormat:@"%@",[matchesSubWo valueForKey:@"isHolidayHrs"]];
    
    if ([strIsHoliday isEqualToString:@"true"] || [strIsHoliday isEqualToString:@"1"]) {
        
        isHoliday=YES;
        
    } else {
        
        isHoliday=NO;
        
    }
    
    //AfterHrsDuration
    
    strAfterHrsDuration=[NSString stringWithFormat:@"%@",[matchesSubWo valueForKey:@"afterHrsDuration"]];
    
    _txt_MiscellaneousCharge.text=[NSString stringWithFormat:@"%@",[matchesSubWo valueForKey:@"totalMileage"]];
    
    NSString *strIncludeDetailOnInvoice=[NSString stringWithFormat:@"%@",[matchesSubWo valueForKey:@"isIncludeDetailOnInvoice"]];
    
    if ([strIncludeDetailOnInvoice isEqualToString:@"1"] || [strIncludeDetailOnInvoice isEqualToString:@"true"] || [strIncludeDetailOnInvoice isEqualToString:@"True"]) {
        
        isIncludeDetailOnInvoice=YES;
        
    } else {
        
        isIncludeDetailOnInvoice=NO;
        
    }
    
    [self checkForPresetSign:matchesSubWo];
}
#pragma mark- ---------******** Payment Info Save *******----------------
//============================================================================
//============================================================================

-(void)fetchPaymentInfoFromDataBase
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityPaymentInfoForMechanicalServiceAuto=[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
    requestPaymentInfo = [[NSFetchRequest alloc] init];
    [requestPaymentInfo setEntity:entityPaymentInfoForMechanicalServiceAuto];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestPaymentInfo setPredicate:predicate];
    
    sortDescriptorPaymentInfo = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsPaymentInfo = [NSArray arrayWithObject:sortDescriptorPaymentInfo];
    
    [requestPaymentInfo setSortDescriptors:sortDescriptorsPaymentInfo];
    
    self.fetchedResultsControllerPaymentInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestPaymentInfo managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerPaymentInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerPaymentInfo performFetch:&error];
    NSArray *arrAllObjPaymentInfo;
    
    arrAllObjPaymentInfo = [self.fetchedResultsControllerPaymentInfo fetchedObjects];
    if ([arrAllObjPaymentInfo count] == 0)
    {
        
        //strGlobalPaymentMode=@"Bill";
        if ([strGlobalPaymentMode isEqualToString:@"Bill"])
        {
            [self setButtonImage:_btnBill];
            checkView=NO;
            [_viewForSingleAmount setHidden:YES];
            [self addView:@"Notadd"];
            
        }
    }
    else
    {
        matchesPaymentInfo=arrAllObjPaymentInfo[0];
        isPaymentInfo=YES;
        
        //NSString* woPaymentId=[matchesPaymentInfo valueForKey:@"woPaymentId"];
        NSString* workorderId=[matchesPaymentInfo valueForKey:@"workorderId"];
        strGlobalWorkOrderId=workorderId;
        NSString* paymentMode=[matchesPaymentInfo valueForKey:@"paymentMode"];
        paidAmount=[matchesPaymentInfo valueForKey:@"paidAmount"];
        NSString* checkNo=[matchesPaymentInfo valueForKey:@"checkNo"];
        NSString* drivingLicenseNo=[matchesPaymentInfo valueForKey:@"drivingLicenseNo"];
        NSString* expirationDate=[matchesPaymentInfo valueForKey:@"expirationDate"];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        // [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSDate* newTime = [dateFormatter dateFromString:expirationDate];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSString* finalTime = [dateFormatter stringFromDate:newTime];
        
        if (finalTime.length==0)
        {
            //strGlobalDateToShow=expirationDate;
            [_btnExpirationDate setTitle:@"Choose Expiration Date" forState:UIControlStateNormal];
        }
        else
        {
            expirationDate=finalTime;
            //strGlobalDateToShow=finalTime;
            [_btnExpirationDate setTitle:expirationDate forState:UIControlStateNormal];
        }
        
        NSString* checkFrontImagePath=[matchesPaymentInfo valueForKey:@"checkFrontImagePath"];
        if (checkFrontImagePath.length>0)
        {
            [arrOFImagesName addObject:checkFrontImagePath];
        }
        NSString* checkBackImagePath=[matchesPaymentInfo valueForKey:@"checkBackImagePath"];
        if (checkBackImagePath.length>0) {
            [arrOfCheckBackImage addObject:checkBackImagePath];
        }
        if ([paymentMode isEqualToString:@"Check"])
        {
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=paidAmount;

                }
                
            }
        }
        else
        {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountSingleAmount.text=paidAmount;

                }
                
            }
            
        }
        //Nilind 17 June
        if (IsShowPricingAndNotesOnApp) {

            if (!IsAmountEdited) {

        _txtAmountSingleAmount.text=[NSString stringWithFormat:@"$%@",paidAmount];

            }
            
        }
        _lblValueForAmountDue.text=paidAmount;//@"$250.00";
        //End
        _txtCheckNo.text=checkNo;
        _txtDrivingLicenseNo.text=[matchesPaymentInfo valueForKey:@"drivingLicenseNo"];
        
        strGlobalPaymentMode=paymentMode;
        //strGlobalPaymentMode=@"Bill";
        if ([strGlobalPaymentMode isEqualToString:@"Check"])
        {
            [self setButtonImage:_btnCheck];
            checkView=YES;
            [_viewForSingleAmount setHidden:NO];
            [self addView:@"Notadd"];
        }
        else if ([strGlobalPaymentMode isEqualToString:@"Cash"])
        {
            [self setButtonImage:_btnCash];
            checkView=NO;
            [_viewForSingleAmount setHidden:NO];
            [self addView:@"Notadd"];
        }
        else if ([strGlobalPaymentMode isEqualToString:@"Bill"])
        {
            [self setButtonImage:_btnBill];
            checkView=NO;
            [_viewForSingleAmount setHidden:YES];
            [self addView:@"Notadd"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"AutoChargeCustomer"])
        {
            [self setButtonImage:_btnAutoChargeCustomer];
            checkView=NO;
            [_viewForSingleAmount setHidden:YES];
            
            [self addView:@"Notadd"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"CreditCard"])
        {
            [self setButtonImage:_btnCreditCard];
            checkView=NO;
            [_viewForSingleAmount setHidden:NO];
            
            [self addView:@"Notadd"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"PaymentPending"])
        {
            [self setButtonImage:_btnPaymentPending];
            [_viewForSingleAmount setHidden:YES];
            checkView=NO;
            [self addView:@"Notadd"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"])
        {
            [self setButtonImage:_btnNoChange];
            [_viewForSingleAmount setHidden:YES];
            checkView=NO;
            [self addView:@"Notadd"];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}
-(void)finalSavePaymentInfo
{
    
    BOOL yesZero;
    yesZero=NO;
    
    // [self deleteMechanicalSubWOPaymentDetailDcs];
    
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        if (_txtAmountCheckView.text.length==0)
        {
            [global AlertMethod:@"Alert!!" :@"Please enter amount"];
        }
        else if (_txtCheckNo.text.length==0)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter Check #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else if (_txtDrivingLicenseNo.text.length==0)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Driving License #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }else if (_btnExpirationDate.titleLabel.text.length==0 ||[_btnExpirationDate.titleLabel.text isEqualToString:@"Choose Expiration Date"] )
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Check Expiration Date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }else if (arrOFImagesName.count==0) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Front Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else if (arrOfCheckBackImage.count==0)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Back Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else
        {
            
            [self savePaymentInfoFirstTime:@"Check"];
            [self goToSendMail];
            
        }
    }
    else if([strGlobalPaymentMode isEqualToString:@"Bill"]||[strGlobalPaymentMode isEqualToString:@"PaymentPending"]||[strGlobalPaymentMode isEqualToString:@"AutoChargeCustomer"]||[strGlobalPaymentMode isEqualToString:@"NoCharge"])
    {
        
        if (strTechnicianSign.length==0) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Sign the Work Order" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"])
        {
            
            [self goToSendMail];
            
        }
        else
        {
            if (strCustomerSign.length==0) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }else{
                
                [self goToSendMail];
                
            }
            
        }
        
        
    }
    else
    {
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"123456789"];
        NSRange range = [_txtAmountSingleAmount.text rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            // no ( or ) in the string
            yesZero=YES;
        } else {
            // ( or ) are present
            yesZero=NO;
        }
        
        if (_txtAmountSingleAmount.text.length==0)
        {
            [global AlertMethod:@"Alert!!" :@"Please enter amount"];
        }else if (yesZero){
            
            [global AlertMethod:@"Alert!!" :@"Please enter amount"];
            
        }else if (strTechnicianSign.length==0) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Sign the Work Order" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"])
        {
            
            [self savePaymentInfoFirstTime:@"NoCheck"];
            [self goToSendMail];
            
        }
        else
        {
            if (strCustomerSign.length==0) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }else{
                
                [self savePaymentInfoFirstTime:@"NoCheck"];
                [self goToSendMail];
                
            }
            
        }
    }
}
-(void)savePaymentInfoFirstTime :(NSString*)type
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    
    entityPaymentInfoForMechanicalServiceAuto=[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
    
    MechanicalSubWOPaymentDetailDcs *objPaymentInfoServiceAuto = [[MechanicalSubWOPaymentDetailDcs alloc]initWithEntity:entityPaymentInfoForMechanicalServiceAuto insertIntoManagedObjectContext:context];
    
    objPaymentInfoServiceAuto.subWoPaymentId=[global getReferenceNumber];
    
    objPaymentInfoServiceAuto.workorderId=strWorkOrderId;
    
    NSString *strTypePaymentToSend;
    
    if ([strGlobalPaymentMode isEqualToString:@"CreditCard"]) {
        objPaymentInfoServiceAuto.paidAmount=_txtAmountSingleAmount.text;
        strTypePaymentToSend=@"CreditCard";
        
    } else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"NoCharge";
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Auto ChargeCustomer"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"AutoChargeCustomer";
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"PaymentPending"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"PaymentPending";
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Bill"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"Bill";
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"NoCharge";
        
    }
    else
    {
        //For Cash Payment
        objPaymentInfoServiceAuto.paidAmount=_txtAmountSingleAmount.text;
        strTypePaymentToSend=strGlobalPaymentMode;
        
    }
    
    objPaymentInfoServiceAuto.paymentMode=strTypePaymentToSend;
    
    if ([type isEqualToString:@"Check"])
    {
        
        objPaymentInfoServiceAuto.paidAmount=_txtAmountCheckView.text;
        objPaymentInfoServiceAuto.checkNo=_txtCheckNo.text;
        objPaymentInfoServiceAuto.drivingLicenseNo=_txtDrivingLicenseNo.text;
        if ([_btnExpirationDate.titleLabel.text isEqualToString:@"Choose Expiration Date"])
        {
            objPaymentInfoServiceAuto.expirationDate=@"";
        }
        else
        {
            objPaymentInfoServiceAuto.expirationDate=_btnExpirationDate.titleLabel.text;
        }
        //objPaymentInfoServiceAuto.expirationDate=_btnExpirationDate.titleLabel.text;
        if (!(arrOFImagesName.count==0))
        {
            objPaymentInfoServiceAuto.checkFrontImagePath=arrOFImagesName[0];
        }
        else
        {
            objPaymentInfoServiceAuto.checkFrontImagePath=@"";
        }
        if (!(arrOfCheckBackImage.count==0))
        {
            objPaymentInfoServiceAuto.checkBackImagePath=arrOfCheckBackImage[0];
        }
        else
        {
            objPaymentInfoServiceAuto.checkBackImagePath=@"";
        }
    }
    else
    {
        //objPaymentInfoServiceAuto.paidAmount=_txtAmountSingleAmount.text;
        objPaymentInfoServiceAuto.checkNo=@"";
        objPaymentInfoServiceAuto.drivingLicenseNo=@"";
        objPaymentInfoServiceAuto.expirationDate=@"";
        objPaymentInfoServiceAuto.checkFrontImagePath=@"";
        objPaymentInfoServiceAuto.checkBackImagePath=@"";
    }
    
    objPaymentInfoServiceAuto.createdDate=[global strCurrentDate];;
    
    objPaymentInfoServiceAuto.createdBy=[NSString stringWithFormat:@"%@",strEmployeeId];
    
    objPaymentInfoServiceAuto.modifiedDate=[global strCurrentDate];;
    
    objPaymentInfoServiceAuto.modifiedBy=[NSString stringWithFormat:@"%@",strEmployeeId];
    
    objPaymentInfoServiceAuto.userName=strUserName;
    objPaymentInfoServiceAuto.subWorkOrderId=strSubWorkOrderIdGlobal;
    objPaymentInfoServiceAuto.recieptPath=@"";
    
    NSError *error;
    [context save:&error];
    
}
-(void)goToSendMail
{
    [self alertForCreateWorkorder];
    
    /*
     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
     [defs setValue:@"Appointment" forKey:@"WhichScreen"];
     [defs synchronize];
     
     UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
     SendMailMechanicaliPadViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailMechanicaliPadViewController"];
     objSendMail.strWorkOrderId=strWorkOrderId;
     objSendMail.strSubWorkOrderId=_strSubWorkOderId;
     objSendMail.strFromWhere=@"S";
     [self.navigationController pushViewController:objSendMail animated:NO];
     */
    
}
-(void)updateWorkOrderDetail
{
    /*
     if (yesEditedSomething) {
     
     NSLog(@"Yes Edited Something In Db");
     
     ////Yaha Par zSyn ko yes karna hai
     [matchesWorkOrder setValue:@"yes" forKey:@"zSync"];
     
     }
     */
    if (chkForUpdateWorkOrder==YES)
    {
        //Update Workorder Detail
        //Update Subworkorder Detail
        
        [matchesFinalSubWorkdOrder setValue:strTechnicianSign forKey:@"completeSWO_TechSignPath"];
        if([_imgSignCheckBox.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
        {
            [matchesFinalSubWorkdOrder setValue:@"" forKey:@"completeSWO_CustSignPath"];
        }
        else
        {
            [matchesFinalSubWorkdOrder setValue:strCustomerSign forKey:@"completeSWO_CustSignPath"];
        }
        
    }
    // Update WorkOrder
    [matchesFinalSubWorkdOrder setValue:strCustomerNotPresent forKey:@"completeSWO_IsCustomerNotPresent"];
    
    if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"])
    {
        
        [matchesFinalSubWorkdOrder setValue:@"true" forKey:@"completeSWO_IsCustomerNotPresent"];
        
    }else{
        
        [matchesFinalSubWorkdOrder setValue:@"false" forKey:@"completeSWO_IsCustomerNotPresent"];
        
    }
    
    [matchesFinalWorkOrder setValue:strGlobalAudio forKey:@"audioFilePath"];
    [matchesFinalWorkOrder setValue:_txtViewTechnicianComment.text forKey:@"technicianComment"];
    [matchesFinalWorkOrder setValue:_txtViewOfficeNotes.text forKey:@"officeNotes"];
    [matchesFinalSubWorkdOrder setValue:_txtViewOfficeNotes.text forKey:@"officeNotes"];
    
    //Update Subworkorder
    
    NSString *strALreadyPaidAmt=[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForAmountPaid.text]];
    
    NSString *strAmtBeingPaid;
    
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        
        strAmtBeingPaid=[NSString stringWithFormat:@"%@",_txtAmountCheckView.text];
        
    }else{
        
        if ([strGlobalPaymentMode isEqualToString:@"CreditCard"]) {
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        } else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"Auto ChargeCustomer"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"PaymentPending"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"Bill"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else
        {
            //For Cash Payment
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",_txtAmountSingleAmount.text];
            
        }
        
    }
    
    double totalAmtPaids=[strALreadyPaidAmt doubleValue]+[strAmtBeingPaid doubleValue];
    
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",totalAmtPaids] forKey:@"amtPaid"];
    ///
    // [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForAmountPaid.text]] forKey:@"amtPaid"];
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _txtOtherDiscounts.text]] forKey:@"discountAmt"];
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueTotalApprovedRepairs.text]] forKey:@"totalApprovedAmt"];
    [matchesFinalSubWorkdOrder setValue:strTripChargeId forKey:@"tripChargeMasterId"];
    [matchesFinalSubWorkdOrder setValue:strDiagnosticChargeId forKey:@"diagnosticChargeMasterId"];
    [matchesFinalSubWorkdOrder setValue:@"" forKey:@"otherTripChargesAmt"];
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString:_lblValueForTax.text]] forKey:@"taxAmt"];
    [matchesFinalSubWorkdOrder setValue:@"Completed" forKey:@"subWOStatus"];
    [matchesFinalWorkOrder setValue:@"Completed" forKey:@"workorderStatus"];
    [matchesFinalSubWorkdOrder setValue:strCompanyKey forKey:@"companyKey"];
    [matchesFinalSubWorkdOrder setValue:@"true" forKey:@"isCompleted"];
    
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForTipCharge.text]] forKey:@"otherTripChargesAmt"];
    
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%@",_txtFld_TripChargeQty.text] forKey:@"tripChargesQty"];
    
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForDiagnosticeCharge.text]] forKey:@"otherDiagnosticChargesAmt"];
    
    
    if ([_btnMileageCharge.titleLabel.text isEqualToString:@"--Mileage Charge--"] || [_btnMileageCharge.currentTitle isEqualToString:@"--Mileage Charge--"]) {
        
        //-- Trip Charge --
        [matchesFinalSubWorkdOrder setValue:@"" forKey:@"mileageChargesName"];
        
    }else{
        
        [matchesFinalSubWorkdOrder setValue:_btnMileageCharge.titleLabel.text forKey:@"mileageChargesName"];
        
        float amtTC = [strChargeAmountMiles floatValue]*[_txtMiles.text floatValue];
        
        NSString *strTotalMileageAmount = [NSString stringWithFormat:@"%.02f",amtTC];
        
        //[matchesFinalSubWorkdOrder setValue:strChargeAmountMiles forKey:@"mileageChargesAmt"];
        [matchesFinalSubWorkdOrder setValue:strTotalMileageAmount forKey:@"mileageChargesAmt"];
        [matchesFinalSubWorkdOrder setValue:_txtMiles.text forKey:@"totalMileage"];
        
    }
    
    //DiagnosticChargeName TripChargeName
    
    if ([_btnDiagnosticCharge.titleLabel.text isEqualToString:@"----Select Diagnostic Charge----"]) {
        
        //-- Trip Charge --
        [matchesFinalSubWorkdOrder setValue:@"" forKey:@"diagnosticChargeName"];
        
    }else{
        
        [matchesFinalSubWorkdOrder setValue:_btnDiagnosticCharge.titleLabel.text forKey:@"diagnosticChargeName"];
        
    }
    if ([_btnTipCharge.titleLabel.text isEqualToString:@"----Select Trip Charge----"]) {
        
        //-- Trip Charge --
        [matchesFinalSubWorkdOrder setValue:@"" forKey:@"tripChargeName"];
        
    }else{
        
        [matchesFinalSubWorkdOrder setValue:_btnTipCharge.titleLabel.text forKey:@"tripChargeName"];
        
    }
    
    if (isGlobalFullTax) {
        
        [matchesFinalSubWorkdOrder setValue:@"true" forKey:@"isChargeTaxOnFullAmount"];
        
    } else {
        
        [matchesFinalSubWorkdOrder setValue:@"false" forKey:@"isChargeTaxOnFullAmount"];
        
    }
    
    if (isGlobalVendorTax) {
        
        [matchesFinalSubWorkdOrder setValue:@"true" forKey:@"isVendorPayTax"];
        
    } else {
        
        [matchesFinalSubWorkdOrder setValue:@"false" forKey:@"isVendorPayTax"];
        
    }
    
    if (isGlobalPartTax) {
        
        [matchesFinalSubWorkdOrder setValue:@"true" forKey:@"isPartTaxable"];
        
    } else {
        
        [matchesFinalSubWorkdOrder setValue:@"false" forKey:@"isPartTaxable"];
        
    }
    
    if (isGlobalLaborTax) {
        
        [matchesFinalSubWorkdOrder setValue:@"true" forKey:@"isLaborTaxable"];
        
    } else {
        
        [matchesFinalSubWorkdOrder setValue:@"false" forKey:@"isLaborTaxable"];
        
    }
    
    [matchesFinalSubWorkdOrder setValue:strPartPriceType forKey:@"partPriceType"];
    
    [matchesWorkOrder setValue:strGlobalServiceJobDescriptionId forKey:@"serviceJobDescriptionId"];
    [matchesWorkOrder setValue:_textView_ServiceJobDescriptions.text forKey:@"serviceJobDescription"];
    
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _txt_MiscellaneousCharge.text]] forKey:@"miscChargeAmt"];
    
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%f",[self getNumberString: _lblValueForHours.text]] forKey:@"laborHrsPrice"];
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%ld",(long)totalsecond] forKey:@"actualTimeInt"];
    
    
    
    NSArray *arrTime=[_txtIncDecActualHours.text componentsSeparatedByString:@":"];
    
    NSString *strHrs,*strMinutes;
    
    if (arrTime.count==1) {
        
        strHrs=arrTime[0];
        strMinutes=@"";
        
    }
    
    if (arrTime.count==2) {
        
        strHrs=arrTime[0];
        strMinutes=arrTime[1];
        
    }
    
    NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
    
    secondTimeToIncrease=secondTimeToIncrease/60;
    
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%f",[self getNumberString: _lblBillableHours.text]] forKey:@"laborHrsPrice"];
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%ld",(long)secondTimeToIncrease] forKey:@"actualTimeInt"];
    
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%f",[self getNumberString: _lblValueForHours.text]] forKey:@"actualLaborHrsPrice"];
    [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%ld",(long)totalsecond] forKey:@"calculatedActualTimeInt"];
    
    if (isPreSetSignGlobal) {
        
        [matchesFinalWorkOrder setValue:@"true" forKey:@"isEmployeePresetSignature"];
        
    } else {
        
        [matchesFinalWorkOrder setValue:@"false" forKey:@"isEmployeePresetSignature"];
        
    }
    
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"redio_button_2.png"];
    
    UIImage *img = [_btnNoMembership imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        // Left Plan
        
        [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString:_lblValueForPSPCharge.text]] forKey:@"pSPCharges"];
        [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString:_lblValueForPSPDiscount.text]] forKey:@"pSPDiscount"];
        [matchesFinalSubWorkdOrder setValue:strPspMasterId1 forKey:@"pSPMasterId"];
        [matchesFinalSubWorkdOrder setValue:strAccountPSPId1 forKey:@"accountPSPId"];
        
        if ([strAccountPSPId1 isEqualToString:@"0"] || [strAccountPSPId1 isEqualToString:@"(null)"])
        {
            strAccountPSPId1=@"";
        }
        [matchesFinalSubWorkdOrder setValue:strAccountPSPId1 forKey:@"accountPSPId"];
        
    }else{
        
        // Right Plan
        
        [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString:_lblValueForMembershipChargeNewPaln.text]] forKey:@"pSPCharges"];
        [matchesFinalSubWorkdOrder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString:_lblValueForMembershipSavingNewPlan.text]] forKey:@"pSPDiscount"];
        [matchesFinalSubWorkdOrder setValue:strPspMasterId2 forKey:@"pSPMasterId"];
        [matchesFinalSubWorkdOrder setValue:strAccountPSPId2 forKey:@"accountPSPId"];
        
        if ([strAccountPSPId2 isEqualToString:@"0"] || [strAccountPSPId2 isEqualToString:@"(null)"])
        {
            strAccountPSPId2=@"";
        }
        [matchesFinalSubWorkdOrder setValue:strAccountPSPId2 forKey:@"accountPSPId"];
        
    }
    
    
    NSError *error;
    [context save:&error];
    
    if (isSendDocWithOutSign) {
        [global fetchSubWorkOrderFromDBToUpdateStatus:strWorkOrderId :@"CompletePending"];
    } else {
        [global fetchSubWorkOrderFromDBToUpdateStatus:strWorkOrderId :@"Completed"];
    }
    
    [self saveEmployeeTimeSheetSlotWise:arrOfGlobalDynamicEmpSheetFinal];
    
    [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
    
}

#pragma mark- **************** Download Image  ****************

-(void)downloadingImagess:(NSString *)str
{
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation1=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }
    if (result.length==0)
    {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSString *strUrl;// = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
    strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation1,str];//str
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists)
    {
        [self stopdejal];
        //[self ShowFirstImage];
    }
    else
    {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        // NSURL *url = [NSURL URLWithString:strNewString];
        strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
        UIImage *image = [UIImage imageWithData:photoData];
        if (result.length==0)
        {
            result=str;
        }
        [self saveImageAfterDownload1:image :result];
    }
    
    // [self stopdejal];
    imageTemp= [self loadImage:result];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
}
-(void)stopdejal{
    [DejalActivityView removeView];
}
-(void)startDejal
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Downloading Image..."];
}
//Nilind 19 June
#pragma mark- *************** Core Data Fetch Methods For Parts ***************

-(void)fetchSubWorkOrderIssuesPartsFromDataBase:(NSString *)strSubWorkOrderIssueIdToFetch
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        arrCompletedParts=[[NSMutableArray alloc]init];
        arrApproveParts=[[NSMutableArray alloc]init];
        arrDeclinedParts=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++)
        {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"workorderId"]);
            [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"customerFeedback"]];
            NSString *strComplete=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"isCompleted"]];
            
            if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
            {
                
                if ([strComplete isEqualToString:@"1"]||[strComplete isEqualToString:@"true"]||[strComplete isEqualToString:@"True"])
                {
                    
                    [arrCompletedParts addObject:matchesSubWorkOrderIssuesRepairParts];
                    
                }else{
                    
                    [arrApproveParts addObject:matchesSubWorkOrderIssuesRepairParts];
                    
                }
            }else{
                
                [arrDeclinedParts addObject:matchesSubWorkOrderIssuesRepairParts];
                
            }
            
        }
        
    }
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else
    {
    }
    [self addView:@"Notadd"];
    
}


-(void)fetchAllParts
{
    arrAllParts=[[NSMutableArray alloc]init];
    //    for (int i=0; i<arrIssueId.count; i++)
    //    {
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        //  arrOfSubWorkServiceIssuesRepairParts=nil;
        //arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
    }
    else
    {
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++)
        {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"workorderId"]);
            //[arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
            [arrAllParts addObject:matchesSubWorkOrderIssuesRepairParts];
            
        }
        
    }
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else
    {
    }
    
    //   }
    
    
}
-(void)fetchCount
{
    arrApp=[[NSMutableArray alloc]init];
    arrDecl=[[NSMutableArray alloc]init];
    
    arrComp=[[NSMutableArray alloc]init];
    
    
    for (int i=0; i<arrIssueId.count; i++)
    {
        NSString *strIssueId=[arrIssueId objectAtIndex:i];
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            
            if ([strIssueId isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
            {
                NSString *strComplete=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
                if ([strComplete isEqualToString:@"true"]||[strComplete isEqualToString:@"True"]||[strComplete isEqualToString:@"1"])
                {
                    [arrComp addObject:dictIssuesRepairData];
                    
                }
                else
                {
                    [arrApp addObject:dictIssuesRepairData];
                }
                
                // [arrApp addObject:dictIssuesRepairData];
            }
            if ([strIssueId isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
            {
                [arrDecl addObject:dictIssuesRepairData];
            }
        }
    }
    
}



-(void)fetchRepairCount
{
    arrApp=[[NSMutableArray alloc]init];
    arrDecl=[[NSMutableArray alloc]init];
    
    arrComp=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrTempApproved=[[NSMutableArray alloc]init];
    NSMutableArray *arrTempDeclined=[[NSMutableArray alloc]init];
    NSMutableArray *arrTempCompleted=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrIssueId.count; i++)
    {
        NSString *strIssueId=[arrIssueId objectAtIndex:i];
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            
            if ([strIssueId isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
            {
                NSString *strComplete=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
                if ([strComplete isEqualToString:@"true"]||[strComplete isEqualToString:@"True"]||[strComplete isEqualToString:@"1"])
                {
                    [arrComp addObject:dictIssuesRepairData];
                    [arrTempCompleted addObject:[self getEquipCodeViaId:strIssueId]];
                    
                }
                else
                {
                    [arrApp addObject:dictIssuesRepairData];
                    [arrTempApproved addObject:[self getEquipCodeViaId:strIssueId]];
                }
                
                // [arrApp addObject:dictIssuesRepairData];
            }
            if ([strIssueId isEqualToString:strIssueRepairIdToCheck]&&([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
            {
                [arrDecl addObject:dictIssuesRepairData];
                [arrTempDeclined addObject:[self getEquipCodeViaId:strIssueId]];
            }
        }
    }
    
    /*
     arrIndexGlobalToShowEquipHeaderApproved=[[NSMutableArray alloc]init];
     
     for (int l=0; l<arrTempApproved.count; l++) {
     
     //NSManagedObject *objTemp=arrOfIndexesToShowEquipHeader[l];
     NSString *strEquipCode=[NSString stringWithFormat:@"%@",arrTempApproved[l]];
     
     for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
     
     NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
     NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
     if ([strEquipCode isEqualToString:strEquipCode1]) {
     
     NSString *strIndex=[NSString stringWithFormat:@"%d",m];
     
     [arrIndexGlobalToShowEquipHeaderApproved addObject:strIndex];
     
     break;
     }
     
     }
     }
     
     arrIndexGlobalToShowEquipHeaderDeclined=[[NSMutableArray alloc]init];
     
     for (int l=0; l<arrTempDeclined.count; l++) {
     
     //NSManagedObject *objTemp=arrOfIndexesToShowEquipHeader[l];
     NSString *strEquipCode=[NSString stringWithFormat:@"%@",arrTempDeclined[l]];
     
     for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
     
     NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
     NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
     if ([strEquipCode isEqualToString:strEquipCode1]) {
     
     NSString *strIndex=[NSString stringWithFormat:@"%d",m];
     
     [arrIndexGlobalToShowEquipHeaderDeclined addObject:strIndex];
     
     break;
     }
     
     }
     }
     
     arrIndexGlobalToShowEquipHeaderCompleted=[[NSMutableArray alloc]init];
     
     for (int l=0; l<arrTempCompleted.count; l++) {
     
     //NSManagedObject *objTemp=arrOfIndexesToShowEquipHeader[l];
     NSString *strEquipCode=[NSString stringWithFormat:@"%@",arrTempCompleted[l]];
     
     for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
     
     NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
     NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
     if ([strEquipCode isEqualToString:strEquipCode1]) {
     
     NSString *strIndex=[NSString stringWithFormat:@"%d",m];
     
     [arrIndexGlobalToShowEquipHeaderCompleted addObject:strIndex];
     
     break;
     }
     
     }
     }
     */
    
}


-(NSString*)getEquipCodeViaId :(NSString*)strId{
    
    NSString *strEquipCodes;
    
    strEquipCodes=@"";
    
    for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
        
        NSManagedObject *objTemp=arrOfSubWorkServiceIssues[k];
        
        NSString *strTempId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strTempId isEqualToString:strId]) {
            
            strEquipCodes=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"equipmentCode"]];
            
            break;
        }
        
    }
    return strEquipCodes;
}

-(void)calculateTotalForParts:(NSString*)strDiscount
{
    
    NSString *strIssueRepairIdToCheck;
    NSMutableArray *arrTotal,*arrTemp;
    arrTotal=[[NSMutableArray alloc]init];
    arrTemp=[[NSMutableArray alloc]init];
    
    
    //Nilind
    
    for (int i=0; i<arrOfSubWorkServiceIssues.count; i++)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[i];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                
            }
        }
    }
    
    //End
    
    for (int k=0; k<arrTemp.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrTemp[k];
        NSString *strIsCompletedCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"isCompleted"]];
        
        //  NSManagedObject *objTemp=arrTemp[k];
        
        if ([[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"1"]|| [[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@""])
        {
            if ([strIsCompletedCheck isEqualToString:@"1"]||[strIsCompletedCheck isEqualToString:@"true"]||[strIsCompletedCheck isEqualToString:@"True"])
                
            {
                NSString *strMulti=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"multiplier"]];
                float multip=[strMulti floatValue];
                
                //NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"qty"]];
                NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"actualQty"]];
                
                float qtyy=[strqtyy floatValue];
                
                NSString *strunitPricee=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"unitPrice"]];
                float unitPricee=[strunitPricee floatValue];
                
                float finalPriceParts=unitPricee*qtyy*multip;
                float totalCostPartss=0;
                totalCostPartss=totalCostPartss+finalPriceParts;
                [arrTotal addObject:[NSString stringWithFormat:@"%.02f",totalCostPartss]];
                
            }
            
        }
        NSLog(@"ARRAY TOTAL %@",arrTotal);
    }
    double sumTotal=0;
    double otherDiscounts,subTotal,tax,amountDue;
    for(int i=0;i<arrTotal.count;i++)
    {
        sumTotal=sumTotal+[[arrTotal objectAtIndex:i]doubleValue];
    }
    
    if ([strWoType isEqualToString:@"TM"]) {
        
        totalLaborAmountLocal = [self getNumberString:_lblBillableHours.text];
        totalLaborAmountLocalCopy = [self getNumberString:_lblBillableHours.text];
        //  totalLaborAmountLocal=0.0;
        totalPartAmountLocal=sumTotal;
        // totalLaborAmountLocalCopy=0.0;
        totalPartAmountLocalCopy=sumTotal;
        
    }
    
    _lblValueTotalApprovedRepairs.text=[NSString stringWithFormat:@"$%.02f",sumTotal];
    _lblValueForTotalAmount.text=[NSString stringWithFormat:@"$%.02f",sumTotal];
    _lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"%@",_lblValueForTotalAmount.text];
    
    [self calculatePspDiscountAgain];
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    
    // akshay
    float laborAppliedCredits = 0.0 ,partAppliedCredits = 0.0;
    bool isLaborTax = [self isItLaborTax];
    
    if(isLaborTax)
    {
        
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            if([[dict valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnLaborPrice"] boolValue]==true)
            {
                laborAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
            
            if([[dict valueForKey:@"applyOnPartPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnPartPrice"] boolValue]==true)
            {
                partAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
            
        }
        
    }
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se
    
    subTotal=sumTotal+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblBillableHours.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_lblValueForPSPCharge.text]+[self getNumberString:_txt_MiscellaneousCharge.text]-[self getNumberString:_lblValueForPSPDiscount.text]-([strDiscount doubleValue] + [self getNumberString:_labelCreditsPaymentDetailViewLeft.text]);
    
    
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
        
    }
    
    if ([_lblValueForPSPCharge.text doubleValue]>0) {
        
        _const_MembershipPlanCharge_H.constant=50;
        
    }else{
        
        _const_MembershipPlanCharge_H.constant=0;
        
    }
    
    if ([_lblValueForPSPDiscount.text doubleValue]>0) {
        
        _const_MembershipPlanSaving_H.constant=50;
        
    }else{
        
        _const_MembershipPlanSaving_H.constant=0;
        
    }
    
    //tax=(subTotal*10)/100;
    //_lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    tax=[self taxCalculationForParts:subTotal :@"sd"];
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    //Nilind 19 July
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    //End
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    
    amountDue=[[self getNumberStringNew:_lblValueForAmountDue.text] doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
    
    _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",[strAmountPaidInSubWorkOrder doubleValue]];
    _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    /*
     
     amountDue=finalTotalWithTax-[strAmountPaidInSubWorkOrder doubleValue];
     double amountPaid;
     amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
     _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
     _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
     
     
     */
    
    if (IsShowPricingAndNotesOnApp) {

        if (!IsAmountEdited) {

    _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
    _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

        }
        
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    } else {
        
        if (IsShowPricingAndNotesOnApp) {

            if (!IsAmountEdited) {

        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

            }
            
        }
        
    }
    
    [self setAmountOnTxtFld];
    

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)texth

{
    if (range.location == 0 && [texth isEqualToString:@" "]) {
        return NO;
    }
    else
        return YES;
    
    
}

#pragma mark- **************** Textfield Delegate Methods ****************

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField == _txtAmountSingleAmount) {
        
        IsAmountEdited = YES;
        
    }
    
    if (textField == _txtAmountCheckView) {
        
        IsAmountEdited = YES;
        
    }
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    int tagTextFld=(int)textField.tag;
    
    if(textField==_txtFld_TripChargeQty)
    {
        BOOL isNumberOnly =  [global isNumberOnly:string :range :3 :(int)textField.text.length :@"0123456789"];
        
        if (isNumberOnly) {
            
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtFld_TripChargeQty.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtFld_TripChargeQty.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            [self calculateTripChargeBasedOnQtyOnTextFldEdit:string];
            
        }
        
        return isNumberOnly;
        
    }else if(textField==_txtFld_TripChargeQtyNewPlan)
    {
        BOOL isNumberOnly =  [global isNumberOnly:string :range :3 :(int)textField.text.length :@"0123456789"];
        
        if (isNumberOnly) {
            
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtFld_TripChargeQtyNewPlan.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtFld_TripChargeQtyNewPlan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            [self calculateTripChargeBasedOnQtyOnTextFldEdit:string];
            
        }
        
        return isNumberOnly;
        
    }
    else if (tagTextFld>499) {
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        else
        {
            NSString *text = textField.text;
            NSInteger length = text.length;
            BOOL shouldReplace = YES;
            
            if (![string isEqualToString:@""])
            {
                switch (length)
                {
                    case 2:
                        textField.text = [text stringByAppendingString:@":"];
                        break;
                        
                    default:
                        break;
                }
                if (length==4) {
                    
                    NSString *lastStrText = [text substringFromIndex: [text length] - 1];
                    NSString *finalStrText = [NSString stringWithFormat:@"%@%@",lastStrText,string];
                    int valueEntered = [finalStrText intValue];
                    if (valueEntered>59) {
                        
                        [global AlertMethod:Alert :@"Minutes can't be greater then 59"];
                        shouldReplace = NO;
                        
                    }
                }
                if (length > 4)
                    shouldReplace = NO;
            }
            
            return shouldReplace;
        }
        return YES;
        
    }else if(textField==_txtCheckNo)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 7;
    }
    else if(textField==_txtDrivingLicenseNo)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 15;
    }else if(textField==_txtAmountSingleAmount)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtAmountSingleAmount.text];
        
        return isNuberOnly;
    }else if(textField==_txtAmountCheckView)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtAmountCheckView.text];
        
        return isNuberOnly;
    }else if(textField==_txtOtherDiscounts)
    {
        NSString *newString = [_txtOtherDiscounts.text stringByReplacingCharactersInRange:range withString:string];
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] >= 2)
        {
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            if (!([sepStr length]>2))
            {
                if ([sepStr length]==2 && [string isEqualToString:@"."])
                {
                    return NO;
                }
                else
                {
                    chkTextEdit=YES;
                    if([string isEqualToString:@""])
                    {
                        NSString *str1 =_txtOtherDiscounts.text;                        NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                        string=[NSString stringWithFormat:@"%@%@",newString,string];
                        
                    }
                    else
                    {
                        NSString *strDiscount = [_txtOtherDiscounts.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                        strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
                    }
#pragma mark - ***************** FOR ISSUE *****************
                    //[self calculateTotal:string];
#pragma mark - ***************** FOR PARTS *****************
                    if ([strWoType isEqualToString:@"FR"])
                    {
                        [self calculateTotal:string];
                    }
                    else if ([strWoType isEqualToString:@"TM"])
                    {
                        [self calculateTotalForParts:string];
                    }
                    
                    
                    return YES;
                    
                }
            }
            else
            {
                return NO;
            }
        }
        else
        {
            chkTextEdit=YES;
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtOtherDiscounts.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtOtherDiscounts.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
#pragma mark - ***************** FOR ISSUE *****************
            
            //[self calculateTotal:string];
#pragma mark - ***************** FOR PARTS *****************
            
            //[self calculateTotalForParts:string];
            if ([strWoType isEqualToString:@"FR"])
            {
                [self calculateTotal:string];
            }
            else if ([strWoType isEqualToString:@"TM"])
            {
                [self calculateTotalForParts:string];
            }
            
            return YES;
        }
        
    }
    else if(textField==_txtOtherDiscountNewPlan)
    {
        NSString *newString = [_txtOtherDiscountNewPlan.text stringByReplacingCharactersInRange:range withString:string];
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] >= 2)
        {
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            if (!([sepStr length]>2))
            {
                if ([sepStr length]==2 && [string isEqualToString:@"."])
                {
                    return NO;
                }
                else
                {
                    chkTextEditNewPlan=YES;
                    if([string isEqualToString:@""])
                    {
                        NSString *str1 =_txtOtherDiscountNewPlan.text;                        NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                        string=[NSString stringWithFormat:@"%@%@",newString,string];
                        
                    }
                    else
                    {
                        NSString *strDiscount = [_txtOtherDiscountNewPlan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                        strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
                    }
                    
                    strDiscountGlobalNewPlan=string;
#pragma mark - ***************** FOR ISSUE *****************
                    //[self calculateTotal:string];
#pragma mark - ***************** FOR PARTS *****************
                    if ([strWoType isEqualToString:@"FR"])
                    {
                        [self calculateForRepairsNewPlan:string];
                    }
                    else if ([strWoType isEqualToString:@"TM"])
                    {
                        [self calculateForPartsNewPlan:string];
                    }
                    
                    
                    return YES;
                    
                }
            }
            else
            {
                return NO;
            }
        }
        else
        {
            chkTextEditNewPlan=YES;
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtOtherDiscountNewPlan.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtOtherDiscountNewPlan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            strDiscountGlobalNewPlan=string;
#pragma mark - ***************** FOR ISSUE *****************
            
            //[self calculateTotal:string];
#pragma mark - ***************** FOR PARTS *****************
            
            //[self calculateTotalForParts:string];
            if ([strWoType isEqualToString:@"FR"])
            {
                [self calculateForRepairsNewPlan:string];
            }
            else if ([strWoType isEqualToString:@"TM"])
            {
                [self calculateForPartsNewPlan:string];
            }
            
            return YES;
        }
        
    }else if (textField==_txtAmountCheckView)
    {
        NSString *newString = [_txtAmountCheckView.text stringByReplacingCharactersInRange:range withString:string];
        
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] >= 2)
        {
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            return !([sepStr length]>2);
        }
        return YES;
    }
    else if (textField==_txtAmountSingleAmount)
    {
        
        NSString *newString = [_txtAmountSingleAmount.text stringByReplacingCharactersInRange:range withString:string];
        
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] >= 2)
        {
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            return !([sepStr length]>2);
        }
        return YES;
    }
    else if (textField==_txtMiles){
        //isNumberOnlyWithDecimal
        //BOOL isNuberOnly=[global isNumbernDecimalOnly:string :range :10 :(int)textField.text.length :@".0123456789"];
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtMiles.text];
        
        if (isNuberOnly) {
            
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtMiles.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtMiles.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            float amtTC = [strChargeAmountMiles floatValue]*[string floatValue];
            
            _lblMileage.text=[NSString stringWithFormat:@"%.02f",amtTC];
            
            if ([strWoType isEqualToString:@"FR"])
            {
                [self calculateTotal:_txtOtherDiscounts.text];
            }
            else if ([strWoType isEqualToString:@"TM"])
            {
                [self calculateTotalForParts:_txtOtherDiscounts.text];
            }
            
        }
        
        return isNuberOnly;
        
    }
    else if (textField==_txtMilesNewPlan){
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtMilesNewPlan.text];
        
        //BOOL isNuberOnly=[global isNumbernDecimalOnly:string :range :10 :(int)textField.text.length :@".0123456789"];
        
        if (isNuberOnly) {
            
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtMilesNewPlan.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtMilesNewPlan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            float amtTC = [strChargeAmountMilesNewPlan floatValue]*[string floatValue];
            
            _lblMileageNewPlan.text=[NSString stringWithFormat:@"%.02f",amtTC];
            
            if ([strWoType isEqualToString:@"FR"])
            {
                [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
            }
            else if ([strWoType isEqualToString:@"TM"])
            {
                [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
            }
            
        }
        
        return isNuberOnly;
        
    }else if (textField==_txt_MiscellaneousCharge){
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txt_MiscellaneousCharge.text];
        
        if (isNuberOnly) {
            
            NSString *newString = [_txt_MiscellaneousCharge.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            if([sep count] >= 2)
            {
                NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                if (!([sepStr length]>2))
                {
                    if ([sepStr length]==2 && [string isEqualToString:@"."])
                    {
                        return NO;
                    }
                    else
                    {
                        [self performSelector:@selector(addingMiscellenousCharge) withObject:nil afterDelay:0.5];
                    }
                }else {
                    return NO;
                }
            }
            
        }
        
        return isNuberOnly;
        
    }
    else if (textField==_txtMiscellaneousChargeNewPlan){
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtMiscellaneousChargeNewPlan.text];
        
        //BOOL isNuberOnly=[global isNumbernDecimalOnly:string :range :10 :(int)textField.text.length :@".0123456789"];  addingMiscellenousChargeNewPlan
        
        if (isNuberOnly) {
            
            NSString *newString = [_txtMiscellaneousChargeNewPlan.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            if([sep count] >= 2)
            {
                NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                if (!([sepStr length]>2))
                {
                    if ([sepStr length]==2 && [string isEqualToString:@"."])
                    {
                        return NO;
                    }
                    else
                    {
                        [self performSelector:@selector(addingMiscellenousChargeNewPlan) withObject:nil afterDelay:0.5];
                    }
                }else {
                    return NO;
                }
            }
            
        }
        
        return isNuberOnly;
        
    }else if (textField==_txtIncDecActualHours){
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        else
        {
            NSString *text = textField.text;
            NSInteger length = text.length;
            BOOL shouldReplace = YES;
            
            if (![string isEqualToString:@""])
            {
                switch (length)
                {
                    case 2:
                        textField.text = [text stringByAppendingString:@":"];
                        break;
                        
                    default:
                        break;
                }
                if (length > 4)
                    shouldReplace = NO;
            }
            
            return shouldReplace;
        }
        return YES;
        
    }
    else
    {
        return YES;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    //if (!IsShowPricingAndNotesOnApp) {
        
        if (textField == _txtAmountSingleAmount) {
            
            _txtAmountCheckView.text = _txtAmountSingleAmount.text;
            
        } else if (textField == _txtAmountCheckView){
            
            _txtAmountSingleAmount.text = _txtAmountCheckView.text;
            
        }
        
    //}
    
    [self calculateTripChargeBasedOnQty];
    
    int tagTextFld=(int)textField.tag;
    
    if (tagTextFld>499) {
        
        NSString *strTextx;
        
        if (textField.text.length>=5) {
            
            strTextx=textField.text;
            
        } else if (textField.text.length==4){
            
            NSString *strTemp=@"0";//  0:00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==3){
            
            NSString *strTemp=@"00";//  :00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==2){
            
            //            NSString *strTemp=@"00:";//  00
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=@":00";//  00
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==1){
            
            //            NSString *strTemp=@"00:0";//  0
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=[NSString stringWithFormat:@"0%@:00",textField.text];
            //NSString *strTemp=@"0:00";
            //strTextx=[textField.text stringByAppendingString:strTemp];
            strTextx=strTemp;
        } else if (textField.text.length==0){
            
            strTextx=@"00:00";
            
        }
        
        
        NSString *lastStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        //NSString *lastSecondStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        NSString *finalStrText = [NSString stringWithFormat:@"%@",lastStrText];
        int valueEntered = [finalStrText intValue];
        if (valueEntered>59) {
            
            [global AlertMethod:Alert :@"Minutes can't be greater then 59"];
            
        }else{
            
            textField.text=strTextx;
            
            if ([textField.text isEqualToString:@"00:00"]) {
                
                textField.text =@"";
                textField.placeholder=@"00:00";
                
            }
            
            NSArray *arrTime=[strTextx componentsSeparatedByString:@":"];
            
            NSString *strHrs,*strMinutes;
            
            if (arrTime.count==1) {
                
                strHrs=arrTime[0];
                strMinutes=@"";
                
            }
            
            if (arrTime.count==2) {
                
                strHrs=arrTime[0];
                strMinutes=arrTime[1];
                
            }
            
            NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
            
            int totalsecondUpdatedEstTime=(int)secondTimeToIncrease;
            totalsecondUpdatedEstTime=totalsecondUpdatedEstTime/60;
            
            //NSString *strTagTextFld=textField.placeholder;
            NSString *strTagTextFld=textField.leftView.accessibilityHint;
            
            NSArray *arrOfTagsTxtFld=[strTagTextFld componentsSeparatedByString:@","];
            
            //[self replaceBillableDurationInEmpSheet:arrOfTagsTxtFld :strTextx];
            [self replaceBillableDurationInEmpSheet:arrOfTagsTxtFld :[NSString stringWithFormat:@"%d",totalsecondUpdatedEstTime]];
            
            if (totalsecondUpdatedEstTime==0) {
                
                textField.text=@"00:00";
                
            }
        }
        
    }
    else{
        
        if (textField==_txtIncDecActualHours) {
            
            if (_txtIncDecActualHours.text.length==0) {
                
                [global AlertMethod:Alert :@"Time is required to update billable labor hours."];
                
                float totalPriceLabour=0.0;
                _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
                
            }else if (_txtIncDecActualHours.text.length<5) {
                
                [global AlertMethod:Alert :@"Time is required in hh:mm(00:00) to update billable labor hours."];
                
                float totalPriceLabour=0.0;
                _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
                
            } else {
                
                if (_txtIncDecActualHours.text.length==5) {
                    
                    float hrsTime=[global ChangeTimeMechanicalInvoice:_txtIncDecActualHours.text];
                    
                    hrsTime=hrsTime/3600;
                    
                    float strStdPrice=0.0;
                    
                    if (!(arrOfHoursConfig.count==0)) {
                        
                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                        
                        strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                        //float totalPriceLabour=hrsTime*strStdPrice;
                        
                        float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                        
                        if (totalPriceLabour>0) {
                            
                        } else {
                            totalPriceLabour=0.0;
                        }
                        _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
                    }
                    
                    if ([strWoType isEqualToString:@"FR"])
                    {
                        [self calculateTotal:_txtOtherDiscounts.text];
                    }
                    else if ([strWoType isEqualToString:@"TM"])
                    {
                        [self calculateTotalForParts:_txtOtherDiscounts.text];
                    }
                    
                    NSArray *arrTime=[_txtIncDecActualHours.text componentsSeparatedByString:@":"];
                    
                    NSString *strHrs,*strMinutes;
                    
                    if (arrTime.count==1) {
                        
                        strHrs=arrTime[0];
                        strMinutes=@"";
                        
                    }
                    
                    if (arrTime.count==2) {
                        
                        strHrs=arrTime[0];
                        strMinutes=arrTime[1];
                        
                    }
                    
                    NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
                    
                    totalsecond=totalsecond+secondTimeToIncrease;
                    totalsecond=totalsecond/60;
                    
                    
                }else{
                    
                    [global AlertMethod:Alert :@"Time is required in hh:mm(00:00) to update billable labor hours."];
                    
                    float totalPriceLabour=0.0;
                    _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
                    
                }
            }
            
        }
        
    }
}

-(void)replaceBillableDurationInEmpSheet :(NSArray*)tag :(NSString*)strDuration{
    
    
    NSString *strTagI=tag[0];
    NSString *strTagJ=tag[1];
    NSString *strTagK=tag[2];
    
    int tagI=[strTagI intValue];
    int tagJ=[strTagJ intValue];
    int tagK=[strTagK intValue];
    
    
    //NSMutableArray *arrMainTemp=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictMainTemp=[[NSMutableDictionary alloc]init];
    NSMutableArray *arrDateWiseDataTemp=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictDateWiseDataTemp=[[NSMutableDictionary alloc]init];
    NSMutableArray *arrDateTemp=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictDateTemp=[[NSMutableDictionary alloc]init];
    
    dictMainTemp=arrOfGlobalDynamicEmpSheetFinal[tagI];
    
    NSString *strHelper=[dictMainTemp valueForKey:@"Helper"];
    
    BOOL isLaborType;
    if ([strHelper isEqualToString:@"No"]) {
        
        isLaborType=YES;
        
    } else {
        
        isLaborType=NO;
        
    }
    
    arrDateWiseDataTemp=[dictMainTemp valueForKey:@"DateWiseData"];
    
    dictDateWiseDataTemp=arrDateWiseDataTemp[tagJ];
    
    arrDateTemp=[dictDateWiseDataTemp valueForKey:@"DateWise"];
    
    dictDateTemp=arrDateTemp[tagK];
    
    NSString *strworkingType=[dictDateTemp valueForKey:@"workingType"];
    BOOL isHolidayyyy;
    if ([strworkingType isEqualToString:@"3"]) {
        isHolidayyyy=YES;
    } else {
        isHolidayyyy=NO;
    }
    
    NSString *strAmount= [global methodToCalculateLaborPriceGlobal:strDuration :isLaborType :arrOfHoursConfig :true :isHolidayyyy :[dictDateTemp valueForKey:@"timeSlot"]];
    
    [dictDateTemp setValue:strDuration forKey:@"billableDurationInMin"];
    [dictDateTemp setValue:strAmount forKey:@"billableAmt"];
    
    [arrDateTemp replaceObjectAtIndex:tagK withObject:dictDateTemp];
    
    [dictDateWiseDataTemp setValue:arrDateTemp forKey:@"DateWise"];
    
    [arrDateWiseDataTemp replaceObjectAtIndex:tagJ withObject:dictDateWiseDataTemp];
    
    [dictMainTemp setValue:arrDateWiseDataTemp forKey:@"DateWiseData"];
    
    [arrOfGlobalDynamicEmpSheetFinal replaceObjectAtIndex:tagI withObject:dictMainTemp];
    
    [self getCompletedSubWOCompleteTimeExtSerDcs :@"add"];
    
}


-(void)addingMiscellenousCharge{
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
    
}


#pragma mark - ***************** FOR AFTER IMAGE  *****************

- (IBAction)actionOnAfterImgView:(id)sender
{
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewForFinalSave.frame.origin.y-_viewForAfterImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewForAfterImage.frame.size.height);
    [_viewForAfterImage setFrame:frameFor_view_BeforeImageInfo];
    [self.view addSubview:_viewForAfterImage];
}

- (IBAction)actionAddAfterImages:(id)sender
{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              if (isCompletedStatusMechanical) {
                                  
                                  
                              }else{
                                  
                                  if (arrOfBeforeImageAll.count<10)
                                  {
                                      NSLog(@"The CApture Image.");
                                      
                                      NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                      
                                      BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                                      
                                      if (isfirstTimeAudio) {
                                          
                                          [defs setBool:NO forKey:@"firstCamera"];
                                          [defs synchronize];
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }else{
                                          BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                          
                                          if (isCameraPermissionAvailable) {
                                              
                                              UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                              imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                              imagePickController.delegate=(id)self;
                                              imagePickController.allowsEditing=TRUE;
                                              [self presentViewController:imagePickController animated:YES completion:nil];
                                              
                                              
                                          }else{
                                              
                                              UIAlertController *alert= [UIAlertController
                                                                         alertControllerWithTitle:@"Alert"
                                                                         message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                              
                                              UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction * action)
                                                                    {
                                                                        
                                                                        
                                                                        
                                                                    }];
                                              [alert addAction:yes];
                                              UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                           NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                           [[UIApplication sharedApplication] openURL:url];
                                                                       } else {
                                                                           
                                                                           //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                           //                                     [alert show];
                                                                           
                                                                       }
                                                                       
                                                                   }];
                                              [alert addAction:no];
                                              [self presentViewController:alert animated:YES completion:nil];
                                          }
                                      }
                                  }
                                  else
                                  {
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                      [alert show];
                                  }
                              }
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if (isCompletedStatusMechanical)
                             {
                                 
                                 
                             }
                             else
                             {
                                 
                                 if (arrOfBeforeImageAll.count<10)
                                 {
                                     
                                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                     
                                     BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                                     
                                     if (isfirstTimeAudio)
                                     {
                                         
                                         [defs setBool:NO forKey:@"firstGallery"];
                                         [defs synchronize];
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }else{
                                         BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                         
                                         if (isCameraPermissionAvailable) {
                                             
                                             UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                             imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                             imagePickController.delegate=(id)self;
                                             imagePickController.allowsEditing=TRUE;
                                             [self presentViewController:imagePickController animated:YES completion:nil];
                                             
                                             
                                         }else{
                                             
                                             UIAlertController *alert= [UIAlertController
                                                                        alertControllerWithTitle:@"Alert"
                                                                        message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       
                                                                       
                                                                   }];
                                             [alert addAction:yes];
                                             UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                        handler:^(UIAlertAction * action)
                                                                  {
                                                                      
                                                                      if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                          NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                          [[UIApplication sharedApplication] openURL:url];
                                                                      } else {
                                                                          
                                                                          //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                          //                                     [alert show];
                                                                          
                                                                      }
                                                                      
                                                                  }];
                                             [alert addAction:no];
                                             [self presentViewController:alert animated:YES completion:nil];
                                         }
                                     }
                                 }
                                 else
                                 {
                                     
                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                     [alert show];
                                 }
                             }
                             
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)actionOnCancelAfterImage:(id)sender
{
    [_viewForAfterImage removeFromSuperview];
}
-(void)fetchImageDetailFromDataBaseMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetail.count; j++) {
            
            matchesImageDetail=arrAllObjImageDetail[j];
            NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"Before"]) {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,nil];
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrOfBeforeImageAll addObject:dict_ToSendLeadInfo];
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
            }
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        
    }else if (arrOfImagesDetail.count==0){
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionView addObjectsFromArray:arrOfImagess];
        
        [self downloadImages:arrOfImagess];
        
    }
}

-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImage:str:k];
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload1: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
        }
    }
    [_afterImageCollectionView reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    if (indexx==arrOfImagenameCollewctionView.count-1) {
        
        [_afterImageCollectionView reloadData];
        
    }
    [_afterImageCollectionView reloadData];
}

//============================================================================
//============================================================================
#pragma mark -----------------------Load Image-------------------------------
//============================================================================
//============================================================================

- (UIImage*)loadImage :(NSString*)name :(int)indexxx {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    return image;
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
    [self deleteBeforeImagesBeforeSaving];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    for (int k=0; k<arrOfBeforeImageAll.count; k++)
    {
        
        if ([arrOfBeforeImageAll[k] isKindOfClass:[NSString class]]) {
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfBeforeImageAll objectAtIndex:k]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            
            NSError *error1;
            [context save:&error1];
            
        }else{
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            NSDictionary *dictData=[arrOfBeforeImageAll objectAtIndex:k];
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    //........................................................................
}

-(void)deleteBeforeImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"Before"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}
//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 105, [UIScreen mainScreen].bounds.size.width-20, 430)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 50)];
    
    lblCaption.text=@"Enter image caption below...";
    lblCaption.font=[UIFont systemFontOfSize:22];
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 100)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:22];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+105, viewAlertt.bounds.size.width-20, 50)];
    
    lbl.text=@"Enter image description below...";
    lbl.font=[UIFont systemFontOfSize:22];
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+55, viewAlertt.bounds.size.width-20, 200)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:22];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+435, viewAlertt.frame.size.width/2-20, 50)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    btnSave.titleLabel.font=[UIFont systemFontOfSize:22];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,50)];
    [btnDone setTitle:@"No Caption" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont systemFontOfSize:22];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        //        CGPoint bottomOffset = CGPointMake(0, _scrollViewGeneralInfo.contentSize.height - _scrollViewGeneralInfo.bounds.size.height);
        //        [_scrollViewGeneralInfo setContentOffset:bottomOffset animated:YES];
        
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}


-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    //    CGPoint bottomOffset = CGPointMake(0, _scrollViewGeneralInfo.contentSize.height - _scrollViewGeneralInfo.bounds.size.height);
    //    [_scrollViewGeneralInfo setContentOffset:bottomOffset animated:YES];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrOfImagenameCollewctionView.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"CellCollectionService";
    
    GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    //    cell.selected=YES;
    //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
    NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    if (image==nil) {
        
        cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
        
    } else {
        
        cell.imageBefore.image = image;
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    if (image==nil) {
        
        
        
    }else{
        
        NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        [self goingToPreview : strIndex];
        
    }
}
-(void)goingToPreview :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ImagePreviewGeneralInfoAppointmentViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.statusOfWorkOrder=strWorkOrderStatuss;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaption;
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescription;
        //objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}




#pragma mark- *************** AUDIO METHODS ***************

- (IBAction)actionOnAudio:(id)sender
{
    
    [self recordAudio];
    
}
- (IBAction)actionOnPlayAudio:(id)sender
{
    [self playAudio];
}

-(void)recordAudio
{
    LandingSong=nil;
    if (LandingSong.isPlaying)
    {
        
    }
    else
    {
        [LandingSong stop];
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL isfirstTimeAudio=[defs boolForKey:@"firstAudio"];
    
    if (isfirstTimeAudio) {
        
        [defs setBool:NO forKey:@"firstAudio"];
        [defs synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        RecordAudioViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioViewiPad"];
        objByProductVC.strFromWhere=@"ServiceInvoice";
        [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
        
        
    } else {
        
        BOOL isAudioPermissionAvailable=[global isAudioPermissionAvailable];
        
        if (isAudioPermissionAvailable) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                     bundle: nil];
            RecordAudioViewiPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioViewiPad"];
            objByProductVC.strFromWhere=@"ServiceInvoice";
            [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
            
            
        }else{
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:AlertAudioVideoPermission//@"Audio Permission not allowed.Please go to settings and allow"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      
                                      
                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                     if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                         NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                         [[UIApplication sharedApplication] openURL:url];
                                     } else {
                                         
                                         //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                         //                                     [alert show];
                                         
                                     }
                                     
                                 }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}


-(void)playAudio
{
    if ([_btnPlay.titleLabel.text isEqualToString:@"Play"])
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        BOOL yesAudio=[defs boolForKey:@"yesAudio"];
        if (yesAudio) {
            [_btnPlay setTitle:@"Stop" forState:UIControlStateNormal];
            //  _lblAudioStatus.text=@"Audio Available";
            //[defs setBool:NO forKey:@"yesAudio"];
            [defs synchronize];
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            NSString *strAudioName=[defs valueForKey:@"AudioNameService"];
            [self playLandingAudio:strAudioName];
        }else{
            // _lblAudioStatus.text=@"Audio Not Available";
            [_btnPlay setTitle:@"Play" forState:UIControlStateNormal];
        }
    } else {
        [_btnPlay setTitle:@"Play" forState:UIControlStateNormal];
        // [LandingSong stop];
        LandingSong=nil;
        if (LandingSong.isPlaying)
        {
            
        }
        else
        {
            [LandingSong stop];
        }
    }
    
    
}
//============================================================================
#pragma mark- Play Audio
//============================================================================

-(void)playLandingAudio :(NSString *)strAudioName
{
    
    strAudioName=[global strNameBackSlashIssue:strAudioName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    
    [global playAudio:path :self.view];
    
    /*
     // NSString *sounfiles1=[[NSBundle mainBundle]pathForResource:@"Flag_Intro Audio" ofType:@"mp3"];
     NSURL *url=[NSURL fileURLWithPath:path];
     LandingSong=[[AVAudioPlayer alloc]initWithContentsOfURL:url error:nil];
     LandingSong.delegate = self;
     [LandingSong play];
     if (LandingSong.isPlaying)
     {
     
     }
     else
     {
     [LandingSong stop];
     }
     */
}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    [_btnPlay setTitle:@"Play" forState:UIControlStateNormal];
    
}


//============================================================================
//============================================================================
#pragma mark -----------------------Download Audio-------------------------------
//============================================================================
//============================================================================

-(void)downloadingAudio :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strSalesssUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strSalesssUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        // [self ShowFirstImage :str];
        // _lblAudioStatus.text=@"Audio Available";
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:result forKey:@"AudioNameService"];
        [defs synchronize];
        
    } else {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        // NSURL *url = [NSURL URLWithString:strNewString];
        strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *audioData = [NSData dataWithContentsOfURL:photoURL];
        [self saveAudioAfterDownload1:audioData :result];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:result forKey:@"AudioNameService"];
        [defs setBool:YES forKey:@"yesAudio"];
        [defs synchronize];
        
        // _lblAudioStatus.text=@"Audio Available";
    }
}
- (void)saveAudioAfterDownload1: (NSData*)audioData :(NSString*)name {
    
    name=[global strNameBackSlashIssue:name];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    [audioData writeToFile:path atomically:YES];
    
}
#pragma mark ***************** ActualHours Caculation *****************
-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanical
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    NSMutableArray *arrOfSubWorkServiceActualHrs,*arrOfHrsTotal;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"" forKey:@"subWOActualHourId"];
        [defs synchronize];
        
        // [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
        arrOfSubWorkServiceActualHrs=nil;
        arrOfSubWorkServiceActualHrs=[[NSMutableArray alloc]init];
        
        // isJobStarted=NO;
        if (![strWoStatus isEqualToString:@"OnRoute"])
        {
            //isJobStarted=YES;
        }
        
    }
    else
    {
        arrOfSubWorkServiceActualHrs=nil;
        arrOfSubWorkServiceActualHrs=[[NSMutableArray alloc]init];
        
        arrOfHrsTotal=nil;
        arrOfHrsTotal=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strTimeOutJob=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"timeOut"]];
            
            NSString *strSubWorkOrderStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"status"]];
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:@"" forKey:@"subWOActualHourId"];
            [defs synchronize];
            
            //[_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
            //isJobStarted=NO;
            
            if (![strWoStatus isEqualToString:@"OnRoute"]) {
                // isJobStarted=YES;
            }
            
            if ([strSubWorkOrderStatus isEqualToString:@"OnRoute"]) {
                
                
                
            }
            else
            {
                
                if (strTimeOutJob.length==0){
                    
                    //isJobStarted=YES;
                    
                    NSString *strSubWOActualHourId=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWOActualHourId"]];
                    
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setValue:strSubWOActualHourId forKey:@"subWOActualHourId"];
                    [defs synchronize];
                    
                    // [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
                    
                    break;
                    
                }
                
            }
            
        }
        
        
        //Calculate Time For Job Duration
        
        float totalPriceLabour = 0.0;
        
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++)
        {
            
            matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strSubWorkOrderStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"status"]];
            
            if ([strSubWorkOrderStatus isEqualToString:@"OnRoute"]) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                
                BOOL isIncludeTravelTime=[defs boolForKey:@"IncludeTravelTime"];
                
                if (isIncludeTravelTime) {
                    
                    NSDate *timeInDate=[global ChangeDateToLocalDateMechanicalUTC:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]]];
                    
                    NSString *strTimeOUT=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
                    
                    if (!(strTimeOUT.length==0)) {
                        
                        totalPriceLabour=[global totalPriceCalcualtionForLabor:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]] :[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"] :arrOfHoursConfig :isStandardSubWorkOrder] +totalPriceLabour;
                        
                        
                        // strTimeOUT=[global strCurrentDateFormattedForMechanical];//////////
                        
                        //}
                        
                        NSDate *timeOutDate=[global ChangeDateToLocalDateMechanicalTimeOut:[NSString stringWithFormat:@"%@",strTimeOUT]];
                        
                        NSTimeInterval secondsBetween = [timeOutDate timeIntervalSinceDate:timeInDate];
                        
                        int hrs = secondsBetween;
                        
                        int seconds = hrs % 60;
                        int minutes = (hrs / 60) % 60;
                        int hours = hrs / 3600;
                        NSLog(@"Total time %@",[NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds]);
                        int actualHrsInSec,actualMinInSec,totalTime,actualMinInSecSec;
                        actualHrsInSec=hours*3600;
                        actualMinInSec=minutes*60;
                        actualMinInSecSec=seconds*60;
                        totalTime=actualHrsInSec+actualMinInSec+seconds;
                        NSLog(@"Total time %@",[NSString stringWithFormat:@"%d",totalTime]);
                        //[arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",hrs]];
                        [arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",totalTime]];
                        [arrOfSubWorkServiceActualHrs addObject:matchesSubWorkOrderActualHrs];
                        
                    }
                }
                
            } else {
                
                NSDate *timeInDate=[global ChangeDateToLocalDateMechanicalUTC:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]]];
                
                NSString *strTimeOUT=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
                
                if (!(strTimeOUT.length==0)) {
                    
                    totalPriceLabour=[global totalPriceCalcualtionForLabor:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]] :[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"] :arrOfHoursConfig :isStandardSubWorkOrder] +totalPriceLabour;
                    
                    
                    // strTimeOUT=[global strCurrentDateFormattedForMechanical];//////////
                    
                    //}
                    
                    NSDate *timeOutDate=[global ChangeDateToLocalDateMechanicalTimeOut:[NSString stringWithFormat:@"%@",strTimeOUT]];
                    
                    NSTimeInterval secondsBetween = [timeOutDate timeIntervalSinceDate:timeInDate];
                    
                    int hrs = secondsBetween;
                    
                    int seconds = hrs % 60;
                    int minutes = (hrs / 60) % 60;
                    int hours = hrs / 3600;
                    NSLog(@"Total time %@",[NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds]);
                    int actualHrsInSec,actualMinInSec,totalTime,actualMinInSecSec;
                    actualHrsInSec=hours*3600;
                    actualMinInSec=minutes*60;
                    actualMinInSecSec=seconds*60;
                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                    NSLog(@"Total time %@",[NSString stringWithFormat:@"%d",totalTime]);
                    //[arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",hrs]];
                    [arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",totalTime]];
                    [arrOfSubWorkServiceActualHrs addObject:matchesSubWorkOrderActualHrs];
                    
                }
            }
            
        }
        
        _lblValueForHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
        
        long sum = 0;
        for (NSString *num in arrOfHrsTotal)
        {
            sum += [num intValue];
        }
        
        totalsecond=sum;
        NSString *strSum;
        strSum=[NSString stringWithFormat:@"%ld",sum];
        //  [self methodToCalculateLaborPrice:strSum];
        NSDictionary *dictTimeClock=[self createTimemapForSeconds:sum];
        
        NSString *strTimeFormat=[NSString stringWithFormat:@"%02d hrs:%02d min",[[dictTimeClock valueForKey:@"h"] intValue],[[dictTimeClock valueForKey:@"m"] intValue]];
        _lblTotalHrs.text=[NSString stringWithFormat:@"%@ (%@)",@"Total Hours",strTimeFormat];
        
        //billable Labor hours
        
        _txtIncDecActualHours.text=[NSString stringWithFormat:@"%02d:%02d",[[dictTimeClock valueForKey:@"h"] intValue],[[dictTimeClock valueForKey:@"m"] intValue]];
        
        _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
        
        [self setValueOnBillableHours];
        
        //for complete status
        
        if (isCompletedStatusMechanical) {
            
            NSString *strlblValueForHours=[matchesFinalSubWorkdOrder valueForKey:@"actualLaborHrsPrice"];
            NSString *strlblBillableHours=[matchesFinalSubWorkdOrder valueForKey:@"laborHrsPrice"];
            NSString *strlblactualTimeInt=[matchesFinalSubWorkdOrder valueForKey:@"actualTimeInt"];
            NSString *strlblcalculatedActualTimeInt=[matchesFinalSubWorkdOrder valueForKey:@"calculatedActualTimeInt"];
            _lblBillableHours.text=[NSString stringWithFormat:@"$%@",strlblBillableHours];;
            _lblValueForHours.text=[NSString stringWithFormat:@"$%@",strlblValueForHours];
            
            [self setValueOnBillableHours];
            
            strlblactualTimeInt=[NSString stringWithFormat:@"%d",[strlblactualTimeInt intValue]*60];
            strlblcalculatedActualTimeInt=[NSString stringWithFormat:@"%d",[strlblcalculatedActualTimeInt intValue]*60];
            
            
            long sumstrlblcalculatedActualTimeInt =[strlblcalculatedActualTimeInt longLongValue];
            NSDictionary *dictTimeClock1=[self createTimemapForSeconds:sumstrlblcalculatedActualTimeInt];
            
            NSString *strTimeFormat1=[NSString stringWithFormat:@"%02d hrs:%02d min",[[dictTimeClock1 valueForKey:@"h"] intValue],[[dictTimeClock1 valueForKey:@"m"] intValue]];
            _lblTotalHrs.text=[NSString stringWithFormat:@"%@ (%@)",@"Total Hours",strTimeFormat1];
            
            long sumlblactualTimeInt =[strlblactualTimeInt longLongValue];
            NSDictionary *dictTimeClock2=[self createTimemapForSeconds:sumlblactualTimeInt];
            
            NSString *strTimeFormat2=[NSString stringWithFormat:@"%02d:%02d",[[dictTimeClock2 valueForKey:@"h"] intValue],[[dictTimeClock2 valueForKey:@"m"] intValue]];
            _txtIncDecActualHours.text=[NSString stringWithFormat:@"%@",strTimeFormat2];
            
        }
        
        
    }
    
    if (error)
    {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    }
    else
    {
    }
}

-(BOOL)isHoliDayDate :(NSDate*)dateToCheck{
    
    BOOL isHolidayy;
    
    isHolidayy=NO;
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictData=[defrs valueForKey:@"MasterAllMechanical"];
    NSArray *arrOfHolidayDate=[dictData valueForKey:@"CompanyHolidaysMasterExtSerDc"];
    
    for(int i=0;i<arrOfHolidayDate.count;i++)
    {
        
        NSDictionary *dictHolidayData=arrOfHolidayDate[i];
        
        NSString *strHolidayDateInMaster=[NSString stringWithFormat:@"%@",[dictHolidayData valueForKey:@"HolidayDate"]];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//2017-12-28T00:00:00
        NSDate* newTime = [dateFormatter dateFromString:strHolidayDateInMaster];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString* finalTime = [dateFormatter stringFromDate:newTime];
        NSDate* holidayedateInMaster = [dateFormatter dateFromString:finalTime];
        
        BOOL isSameDate=[self isSameDay:holidayedateInMaster otherDay:dateToCheck];
        
        if (isSameDate) {
            
            isHolidayy=YES;
            
            break;
            
        }else{
            
            isHolidayy=NO;
            
        }
        
    }
    
    return isHolidayy;
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

-(NSDictionary*)createTimemapForSeconds:(NSInteger)seconds{
    int hours = floor(seconds /  (60 * 60) );
    
    float minute_divisor = seconds % (60 * 60);
    int minutes = floor(minute_divisor / 60);
    
    float seconds_divisor = seconds % 60;
    seconds = ceil(seconds_divisor);
    
    NSDictionary * timeMap = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:hours], [NSNumber numberWithInt:minutes], [NSNumber numberWithInt:seconds], nil] forKeys:[NSArray arrayWithObjects:@"h", @"m", @"s", nil]];
    
    NSLog(@"timeMap>>%@",timeMap);
    
    return timeMap;
}
-(void)methodToCalculateLaborPrice :(NSString *)strTimeEntered
{
    float hrsTime=[strTimeEntered floatValue];
    
    int hrs = (int)hrsTime;
    
    int actualHrsInSec,actualMinInSec,totalTime,actualSecInSec;
    
    int seconds = hrs % 60;
    int minutes = (hrs / 60) % 60;
    int hours = hrs / 3600;
    
    actualHrsInSec=hours*3600;
    actualMinInSec=minutes*60;
    actualSecInSec=seconds;
    totalTime=actualHrsInSec+actualMinInSec;
    
    hrsTime=totalTime;
    
    hrsTime=hrsTime/3600;
    
    NSDictionary *dictDataHours=arrOfHoursConfig[0];
    
    float strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
    
    if (!isStandardSubWorkOrder)
    {
        
        
    }
    
    float totalPriceLabour=hrsTime*strStdPrice;
    
    //float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
    
    //_lblValueForddHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
    
}

-(void)getHoursConfiFromMaster{
    
    arrOfHoursConfig=nil;
    arrOfHoursConfig=[[NSMutableArray alloc]init];
    arrOfHoursConfig=[global getHoursConfiFromMaster:strCompanyKey :strDepartmentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType];
    
}

-(void)deleteMechanicalSubWOPaymentDetailDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete entitySubWorkOrderIssuesRepairParts Data
    entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",_strSubWorkOderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)fetchDepartmentNameBySysName
{
    
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    
    
    //Branch Master Fetch
    
    NSArray *arrBranchMaster;
    arrBranchMaster=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    
    NSMutableArray *arrDeptVal,*arrDeptKey;
    arrDeptVal=[[NSMutableArray alloc]init];
    arrDeptKey=[[NSMutableArray alloc]init];
    
    NSDictionary *dictMasterKeyValue;
    for (int i=0; i<arrBranchMaster.count; i++)
    {
        NSDictionary *dict=[arrBranchMaster objectAtIndex:i];
        NSArray *arrDepartments=[dict valueForKey:@"Departments"];
        for (int k=0; k<arrDepartments.count; k++)
        {
            NSDictionary *dict1=[arrDepartments objectAtIndex:k];
            [arrDeptVal addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"Name"]]];
            [arrDeptKey addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"SysName"]]];
        }
        
    }
    dictMasterKeyValue=[NSDictionary dictionaryWithObjects:arrDeptVal forKeys:arrDeptKey];
    dictDeptNameByKey=dictMasterKeyValue;
    NSLog(@"dictMasterKeyValue>>%@",dictMasterKeyValue);
}

-(void)fetchChecklistSavedInDB{
    // subWorkOrderId
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"MechanicalDynamicForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
        isCheckList=NO;
        
    }
    else
    {
        
        isCheckList=YES;
        matches=arrAllObj[0];
        
        NSArray *arrTemp = [matches valueForKey:@"arrFinalInspection"];
        
        if ([arrTemp isKindOfClass:[NSDictionary class]]) {
            
            //   arrResponseInspection =[[NSMutableArray alloc]init];
            dictJsonDynamicForm=(NSDictionary*)arrTemp;
            //  [arrResponseInspection addObject:dictdata];
            
        } else {
            
            dictJsonDynamicForm=arrTemp[0];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
    
    [self showChekListView];
    
}

-(void)showChekListView{
    
    if (!isCheckList) {
        
        //  [BtnMainView removeFromSuperview];
        [arrayOfButtonsMain removeLastObject];
        [ViewFormSections removeFromSuperview];
        [MainViewForm removeFromSuperview];
        [ViewFormSections setFrame:CGRectMake(0, 0, 0, 0)];
        [MainViewForm setFrame:CGRectMake(0, 0, 0, 0)];
        
        CGRect frameFor_view_CustomerInfo=CGRectMake(10, 0, self.view.frame.size.width-20, 0);
        frameFor_view_CustomerInfo.origin.x=10;
        frameFor_view_CustomerInfo.origin.y=0;
        
        //  [_view_CustomerInfo setFrame:frameFor_view_CustomerInfo];
        
        //  [_scrollView addSubview:_view_CustomerInfo];
        
        
    } else {
        
        BOOL isMainValueThere;
        
        isMainValueThere=NO;
        
        CGRect frameFor_view_CustomerInfo=CGRectMake(10, 0, self.view.frame.size.width-20, _view_CustomerInfo.frame.size.height);
        frameFor_view_CustomerInfo.origin.x=10;
        frameFor_view_CustomerInfo.origin.y=0;
        
        //  [_view_CustomerInfo setFrame:frameFor_view_CustomerInfo];
        
        //  [_scrollView addSubview:_view_CustomerInfo];
        
        
        // _scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, [UIScreen mainScreen].bounds.size.width-2,_scrollVieww.frame.size.height+_view_CustomerInfo.frame.size.height);
        
        //[_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_scrollVieww.contentSize.height+_view_CustomerInfo.frame.size.height)];
        
        //============================================================================
        //============================================================================
        
        
        CGFloat scrollViewHeight=0.0;
        
        NSDictionary *dictMain=dictJsonDynamicForm;
        
        UIButton *BtnMainView;
        
        
        UILabel *lblTitleSection1=[[UILabel alloc]init];
        lblTitleSection1.backgroundColor=[UIColor lightGrayColor];
        lblTitleSection1.layer.borderWidth = 1.5f;
        lblTitleSection1.layer.cornerRadius = 4.0f;
        lblTitleSection1.layer.borderColor = [[UIColor grayColor] CGColor];
        
        if (!MainViewForm.frame.size.height) {
            
            lblTitleSection1.frame=CGRectMake(0, _viewForCustomerDetail.frame.origin.y+_viewForCustomerDetail.frame.size.height, [UIScreen mainScreen].bounds.size.width, 52);
            
        }else{
            
            lblTitleSection1.frame=CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 52);
            
        }
        //lblTitleSection1.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50);
        lblTitleSection1.font=[UIFont systemFontOfSize:24];
        lblTitleSection1.text=[NSString stringWithFormat:@"%@",@"Check List"] ;
        lblTitleSection1.textAlignment=NSTextAlignmentCenter;
        [lblTitleSection1 setAdjustsFontSizeToFitWidth:YES];
        [_scrollView addSubview:lblTitleSection1];
        
        
        
        if (!MainViewForm.frame.size.height) {
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, _viewForCustomerDetail.frame.origin.y+_viewForCustomerDetail.frame.size.height+60, [UIScreen mainScreen].bounds.size.width, 52)];
        }
        else{
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+10+60, [UIScreen mainScreen].bounds.size.width, 52)];
        }
        
        //   BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, lblTitleSection1.frame.origin.y+lblTitleSection1.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 52)];
        
        //  [BtnMainView addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        BtnMainView.titleLabel.font=[UIFont systemFontOfSize:22];
        [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentSysName"] forState:UIControlStateNormal];
        
        
#pragma mark- -------------------BY NILIND 23 FEB--------------------------
        
        //Nilind 23 Feb
        
        // [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentSysName"] forState:UIControlStateNormal];//dictDeptNameByKey
        NSString *strDept=[dictDeptNameByKey valueForKey:[dictMain valueForKey:@"DepartmentSysName"]];
        if ([strDept isEqual:nil]||[strDept isEqualToString:@""])
        {
            strDept=@"No Department Available";
        }
        [BtnMainView setTitle:strDept forState:UIControlStateNormal];
        //End
        
        
        // UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 40, 40)];
        // imgView.image=[UIImage imageNamed:@"minus_icon.png"];
        
        //scrollViewHeight=scrollViewHeight+BtnMainView.frame.size.height;
        //  [BtnMainView addSubview:imgView];
        
        [_scrollView addSubview:BtnMainView];
        
        [arrayOfButtonsMain addObject:BtnMainView];
        
        // Form Creation
        MainViewForm=[[UIView alloc]init];
        
        MainViewForm.backgroundColor=[UIColor clearColor];
        
        MainViewForm.frame=CGRectMake(0, BtnMainView.frame.origin.y+BtnMainView.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 1000);
        
        [_scrollView addSubview:MainViewForm];
        
        NSArray *arrSections=[dictMain valueForKey:@"Sections"];
        
        
        int yAxisViewFormSections=0.0;
        int heightVIewFormSections=0.0;
        int heightMainViewFormSections=0.0;
        
        
        for (int j=0; j<arrSections.count; j++)
        {
            NSDictionary *dictSection=[arrSections objectAtIndex:j];
            NSArray *arrControls=[dictSection valueForKey:@"Controls"];
            
            // Section Creation Start
            heightVIewFormSections=0.0;
            ViewFormSections=[[UIView alloc]init];
            ViewFormSections.frame=CGRectMake(0, yAxisViewFormSections, [UIScreen mainScreen].bounds.size.width, 400);
            
            ViewFormSections.backgroundColor=[UIColor whiteColor];
            UILabel *lblTitleSection=[[UILabel alloc]init];
            lblTitleSection.backgroundColor=[UIColor lightGrayColor];
            lblTitleSection.layer.borderWidth = 1.5f;
            lblTitleSection.layer.cornerRadius = 4.0f;
            lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
            
            lblTitleSection.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50);
            lblTitleSection.font=[UIFont systemFontOfSize:22];
            lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictSection valueForKey:@"Name"]] ;
            lblTitleSection.textAlignment=NSTextAlignmentCenter;
            [lblTitleSection setAdjustsFontSizeToFitWidth:YES];
            [ViewFormSections addSubview:lblTitleSection];
            
            ViewFormSections.layer.borderWidth = 1.5f;
            ViewFormSections.layer.cornerRadius = 4.0f;
            ViewFormSections.layer.borderColor = [[UIColor grayColor] CGColor];
            
            [MainViewForm addSubview:ViewFormSections];
            
            CGFloat xPositionOfControls= 0.0;
            CGFloat yPositionOfControls = 0.0;
            CGFloat heightOfControls= 0.0;
            CGFloat widthOfControls= 0.0;
            
            BOOL isValuePrsent;
            isValuePrsent=NO;
            
            int viewHeight = 0;
            
            NSMutableArray *arrOfValuepresent=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrControls.count; k++)
            {
                NSDictionary *dictControls=[arrControls objectAtIndex:k];
                UILabel *lblTitleControl=[[UILabel alloc]init];
                lblTitleControl.backgroundColor=[UIColor blackColor];
                lblTitleControl.frame=CGRectMake(0, 50*k+35, [UIScreen mainScreen].bounds.size.width, 50);
                lblTitleControl.text=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Label"]] ;
                lblTitleControl.font = [UIFont fontWithName:@"Helvetica-Bold" size:22];
                lblTitleControl.font=[UIFont systemFontOfSize:20];
                lblTitleControl.textAlignment=NSTextAlignmentCenter;
                // [ViewFormSections addSubview:lblTitleControl];
                
                NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                if (strIndexOfValue.length==0) {
                    // isValuePrsent=NO;
                } else {
                    
                    
                    
                    isValuePrsent=YES;
                    NSArray *arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    
                    if (!yPositionOfControls) {
                        yPositionOfControls=50;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width-30, s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:8];
                    lbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    NSString *heightttt=[NSString stringWithFormat:@"%f",s.height];
                    
                    // [arrOfValuepresent addObject:heightttt];
                    
                    if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textarea"]) {
                        
                        //     CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                        UITextView *lblItem = [[UITextView alloc] init];
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, 65);
                        [lblItem setFont:[UIFont systemFontOfSize:20]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        lblItem.text =[dictControls valueForKey:@"Value"];
                        lblItem.editable=NO;
                        [view addSubview:lblItem];
                        //  view.backgroundColor=[UIColor redColor];
                        
                        int tempHeight=lblItem.frame.size.height;
                        
                        viewHeight=viewHeight+tempHeight;
                        
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, lblItem.frame.size.height+lbl.frame.size.height+15)];
                        
                        heightttt=[NSString stringWithFormat:@"%f",lblItem.frame.size.height+lbl.frame.size.height+15];
                        [arrOfValuepresent addObject:heightttt];
                        
                    }
                    else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]) {
                        
                        NSArray *arrOfOptions=[dictControls valueForKey:@"Options"];
                        NSArray *arrOfValues=[[dictControls valueForKey:@"Value"] componentsSeparatedByString:@","];
                        NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
                        
                        for (int k=0; k<arrOfValues.count; k++) {
                            
                            for (int l=0; l<arrOfOptions.count; l++) {
                                
                                NSDictionary *dictData=arrOfOptions[l];
                                NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                                
                                if ([strValue isEqualToString:arrOfValues[k]]) {
                                    //Name
                                    if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]) {
                                        [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                                        
                                    } else {
                                        [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Label"]]];
                                        
                                    }
                                }
                            }
                        }
                        
                        NSString *strValue=[dictControls valueForKey:@"Label"];
                        strValue =[strValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        // NSString *strTextView=[[dictControls valueForKey:@"Value"] stringByReplacingOccurrencesOfString:@"," withString:@", \n \n"];
                        
                        NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@"\n \n"];
                        
                        CGSize s11 = [strTextView sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(lbl.frame.size.width-20, MAXFLOAT)];
                        UILabel *lblItem = [[UILabel alloc] init];
                        
                        if (s11.height<50) {
                            s11.height=50;
                        }
                        
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                        [lblItem setFont:[UIFont systemFontOfSize:20]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:1000];
                        lblItem.text =strTextView;//[dictControls valueForKey:@"Value"];
                        
                        //  lblItem.backgroundColor=[UIColor grayColor];
                        
                        [view addSubview:lblItem];
                        
                        int tempHeight=lblItem.frame.size.height;
                        
                        viewHeight=viewHeight+tempHeight;
                        
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, lblItem.frame.size.height+lbl.frame.size.height+15)];
                        
                        heightttt=[NSString stringWithFormat:@"%f",lblItem.frame.size.height+lbl.frame.size.height+15];
                        [arrOfValuepresent addObject:heightttt];
                        
                    }
                    else {
                        
                        CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(lbl.frame.size.width-20, MAXFLOAT)];
                        UILabel *lblItem = [[UILabel alloc] init];
                        
                        if (s11.height<50) {
                            s11.height=50;
                        }
                        
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                        [lblItem setFont:[UIFont systemFontOfSize:20]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:1000];
                        lblItem.text =[dictControls valueForKey:@"Value"];
                        
                        // lblItem.backgroundColor=[UIColor grayColor];
                        
                        [view addSubview:lblItem];
                        
                        int tempHeight=lblItem.frame.size.height;
                        
                        viewHeight=viewHeight+tempHeight;
                        
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, lblItem.frame.size.height+lbl.frame.size.height+15)];
                        
                        heightttt=[NSString stringWithFormat:@"%f",lblItem.frame.size.height+lbl.frame.size.height+15];
                        [arrOfValuepresent addObject:heightttt];
                        
                    }
                    
                    //                CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                    //                UILabel *lblItem = [[UILabel alloc] init];
                    //                lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width, s11.height);
                    //                [lblItem setFont:[UIFont systemFontOfSize:15]];
                    //                //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                    //                [lblItem setNumberOfLines:2];
                    //                lblItem.text =[dictControls valueForKey:@"Value"];
                    //                [view addSubview:lblItem];
                    //
                    //                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, lbl.frame.size.height+12+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=yPositionOfControls+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, lbl.frame.size.height+12+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
            }
            
            if (!isValuePrsent) {
                
                // [BtnMainView removeFromSuperview];
                [arrayOfButtonsMain removeLastObject];
                ViewFormSections.frame=CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, ViewFormSections.frame.size.height-400);
                [lblTitleSection removeFromSuperview];
                //isMainValueThere=NO;
            }
            else{
                isMainValueThere=YES;
                int heightTemporay=0;
                
                for (int k=0; k<arrOfValuepresent.count; k++) {
                    
                    NSString *strValue=arrOfValuepresent[k];
                    
                    heightTemporay=heightTemporay+[strValue intValue];
                    
                }
                
                viewHeight=40+heightTemporay+(int)arrOfValuepresent.count*10;//Label Title ki height ==40 and labeltitilsect
                
                [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+35)];
                
                heightMainViewFormSections=viewHeight+35;
                
                MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width, heightMainViewFormSections+35);
                
                yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+45;
                
            }
        }
        
        if (!(arrSections.count==0)) {
            
            // scrollViewHeight=scrollViewHeight+MainViewForm.frame.size.height;
            
            CGFloat heightTemp=MainViewForm.frame.size.height;
            
            NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
            
            CGFloat YAxisTemp=MainViewForm.frame.origin.y;
            
            NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
            _scrollView.backgroundColor=[UIColor clearColor];
            
        }else{
            // [BtnMainView removeFromSuperview];
            [arrayOfButtonsMain removeLastObject];
            [ViewFormSections removeFromSuperview];
        }
        
        
        if (!isMainValueThere) {
            
            // [BtnMainView removeFromSuperview];
            [arrayOfButtonsMain removeLastObject];
            [ViewFormSections removeFromSuperview];
            [MainViewForm removeFromSuperview];
            [ViewFormSections setFrame:CGRectMake(0, 0, 0, 0)];
            [MainViewForm setFrame:CGRectMake(0, 0, 0, 0)];
            
        }
        //   _scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, [UIScreen mainScreen].bounds.size.width-2, _scrollVieww.frame.size.height+_view_CustomerInfo.frame.size.height+scrollViewHeight+100);
        
        //  [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_scrollVieww.contentSize.height+_view_CustomerInfo.frame.size.height+scrollViewHeight+100)];
        
    }
    //ViewFormSections.backgroundColor=[UIColor redColor];
}

-(void)methodToCheckStatus{
    
    if (isCompletedStatusMechanical) {
        
        [_txtFld_TripChargeQty setEnabled:NO];
        [_txtOtherDiscounts setEnabled:NO];
        [_txtCheckNo setEnabled:NO];
        [_txtAmountCheckView setEnabled:NO];
        [_txtViewOfficeNotes setEditable:NO];
        [_txtDrivingLicenseNo setEnabled:NO];
        [_txtAmountSingleAmount setEnabled:NO];
        [_txtViewTechnicianComment setEditable:NO];
        [_txtViewTermsConditions setEditable:NO];
        
        [_btnBill setEnabled:NO];
        [_btnCash setEnabled:NO];
        [_btnAutoL setEnabled:NO];
        [_btnBillL setEnabled:NO];
        [_btnCashL setEnabled:NO];
        [_btnCheck setEnabled:NO];
        [_btnAutoChargeCustomer setEnabled:NO];
        [_btnCashL setEnabled:NO];
        [_btnRecord setEnabled:NO];
        [_btnCreditL setEnabled:NO];
        [_btnCreditCard setEnabled:NO];
        [_btnNoChange setEnabled:NO];
        [_btnNoChargeL setEnabled:NO];
        [_btnPayPendingL setEnabled:NO];
        [_btnPaymentPending setEnabled:NO];
        [_btnTipCharge setEnabled:NO];
        [_btnDiagnosticCharge setEnabled:NO];
        [_btnCustomerSign setEnabled:NO];
        [_btnCustomerNotPresent setEnabled:NO];
        [_btnInspectorSign setEnabled:NO];
        [_btnCompleteWorkOrder setEnabled:NO];
        [_btnExpirationDate setEnabled:NO];
        [_btnMileageCharge setEnabled:NO];
        [_txtMiles setEnabled:NO];
        [_txt_MiscellaneousCharge setEnabled:NO];
        _txtIncDecActualHours.enabled=NO;
        _btnPlus.enabled=NO;
        _btnMinus.enabled=NO;
        _btnAddBillableLaborHrs.enabled=NO;
        _textView_ServiceJobDescriptions.editable=NO;
        [_btnServiceJobDescriptionTitle setEnabled:NO];
    }else{
        
        [_txtFld_TripChargeQty setEnabled:YES];
        [_btnServiceJobDescriptionTitle setEnabled:YES];
        _textView_ServiceJobDescriptions.editable=YES;
        _btnAddBillableLaborHrs.enabled=YES;
        _btnPlus.enabled=YES;
        _btnMinus.enabled=YES;
        _txtIncDecActualHours.enabled=NO;
        [_txt_MiscellaneousCharge setEnabled:YES];
        [_txtOtherDiscounts setEnabled:YES];
        [_txtCheckNo setEnabled:YES];
        [_txtAmountCheckView setEnabled:YES];
        [_txtViewOfficeNotes setEditable:YES];
        [_txtDrivingLicenseNo setEnabled:YES];
        [_txtAmountSingleAmount setEnabled:YES];
        [_txtViewTechnicianComment setEditable:YES];
        [_txtViewTermsConditions setEditable:NO];
        
        [_btnBill setEnabled:YES];
        [_btnCash setEnabled:YES];
        [_btnAutoL setEnabled:YES];
        [_btnBillL setEnabled:YES];
        [_btnCashL setEnabled:YES];
        [_btnCheck setEnabled:YES];
        [_btnAutoChargeCustomer setEnabled:YES];
        [_btnCashL setEnabled:YES];
        [_btnRecord setEnabled:YES];
        [_btnCreditL setEnabled:YES];
        [_btnCreditCard setEnabled:YES];
        [_btnNoChange setEnabled:YES];
        [_btnNoChargeL setEnabled:YES];
        [_btnPayPendingL setEnabled:YES];
        [_btnPaymentPending setEnabled:YES];
        [_btnTipCharge setEnabled:YES];
        [_btnDiagnosticCharge setEnabled:YES];
        [_btnCustomerSign setEnabled:YES];
        [_btnCustomerNotPresent setEnabled:YES];
        [_btnInspectorSign setEnabled:YES];
        [_btnCompleteWorkOrder setEnabled:YES];
        [_btnExpirationDate setEnabled:YES];
        [_btnMileageCharge setEnabled:YES];
        [_txtMiles setEnabled:YES];
        
        
        NSUserDefaults *defsSign=[NSUserDefaults standardUserDefaults];
        BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignService"];
        NSString *strSignUrl=[defsSign valueForKey:@"ServiceTechSignPath"];
        if ((isPreSetSign) && (strSignUrl.length>0) && isCompletedStatusMechanical) {
            [_btnInspectorSign setEnabled:NO];
        } else if ((isPreSetSign) && (strSignUrl.length>0) && !isCompletedStatusMechanical){
            [_btnInspectorSign setEnabled:NO];
        } else if ((!isPreSetSign) || (strSignUrl.length<1)){
            [_btnInspectorSign setEnabled:YES];
        }else{
            isPreSetSignGlobal=NO;
            [_btnInspectorSign setEnabled:YES];
        }
        
    }
}


- (IBAction)actionOnMileageCharge:(id)sender{
    
    tblData.tag=60;
    [self tableLoad:tblData.tag];
    
}
- (IBAction)actionOnMileageChargeNewPlan:(id)sender{
    
    tblData.tag=70;
    [self tableLoad:tblData.tag];
    
    
}
-(double)taxCalculation :(double)totalPrice :(float)subTotal :(float)subTotalLabor :(float)subTotalMaterialCost :(float)subTotalMaterialCostSales :(float)strCostAdj :(float)strPartCostAdj :(NSString*)strDiscount{
    
    double tax=0.0;
    
    NSArray *arrOfTaxMaster=[global getSalesTaxFromMaster:strCompanyKey :strDepartmentSysName :[matchesFinalSubWorkdOrder valueForKey:@"propertyType"]];
    
    if (!(arrOfTaxMaster.count==0)) {
        
        //Check According To Condition to apply tax on what
        
        BOOL isLaborTax=NO,isMaterialTax=NO,isVendorPayTax=NO,isFullTax=NO;
        
        NSDictionary *dictTaxData=arrOfTaxMaster[0];
        
        NSString *strVendorPayTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"VendorPayTax"]];
        
        if ([strVendorPayTax isEqualToString:@"true"] || [strVendorPayTax isEqualToString:@"True"] || [strVendorPayTax isEqualToString:@"1"]) {
            
            isVendorPayTax=YES;
            
        }
        
        if ([strWorkOrderAddressSubType isEqualToString:@"Commercial"]) {//Commercial Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMPartPriceType"]];
                
            }
            
        } else {//Resedential Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMPartPriceType"]];
                
            }
            
        }
        
        isGlobalVendorTax=isVendorPayTax;
        
        if (isFullTax) {
            
            isGlobalFullTax=isFullTax;
            
            double taxValue;
            taxValue=[strTax doubleValue];
            tax=(totalPrice*taxValue)/100;
            
        } else {
            
            tax=[self calculationOverAllNewLaborMaterialReturn:subTotal :subTotalLabor :subTotalMaterialCost :subTotalMaterialCostSales :isLaborTax :isMaterialTax :strCostAdj :strPartCostAdj :strPartPriceType :strDiscount];
            
            isGlobalPartTax=isMaterialTax;
            isGlobalLaborTax=isLaborTax;
            
        }
        
    }
    
    return tax;
}


-(double)calculationOverAllNewLaborMaterialReturn :(float)subTotal :(float)subTotalLabor :(float)subTotalMaterialCost  :(float)subTotalMaterialCostSales :(BOOL)isLaborTax :(BOOL)isMaterialTax :(float)strCostAdj :(float)strPartCostAdj :(NSString*)strTypeOfPrice :(NSString*)strDiscount{
    
    
    //    // Akshay Start //
    //    totalLaborAmountLocal = subTotalLabor;
    //    totalPartAmountLocal =  subTotalMaterialCostSales;
    //    totalLaborAmountLocalCopy = subTotalLabor;
    //    totalPartAmountLocalCopy = subTotalMaterialCostSales;
    //    // Akshay End //
    
    
    double tax = 0.0,amountDue,taxMaterial = 0.0,taxLabor = 0.0;
    amountDue=0;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    
    
    if (isLaborTax) {
        
        float laborAppliedCredits = 0.0;
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            if([[dict valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnLaborPrice"] boolValue]==true)
            {
                laborAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
        }
        
        //[self calculateTripChargeBasedOnQty];
        
        // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se
        
        float subTotalLaborTemp=subTotalLabor+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_lblValueForPSPCharge.text]+strCostAdj+[self getNumberString:_txt_MiscellaneousCharge.text]-[self getNumberString:_lblValueForPSPDiscount.text]-([strDiscount doubleValue] + laborAppliedCredits);  //-[strDiscount doubleValue]
        
        // Condition change for if laborcost is 0 then no cost adjustment is to be added
        
        if (subTotalLabor<=0) {
            
            subTotalLaborTemp = subTotalLaborTemp-strCostAdj;
            
        }
        
        if (subTotalLaborTemp>0) {
            
            taxLabor=(subTotalLaborTemp*taxValue)/100;
            
        }
        
    }
    
    if (isMaterialTax) {
        
        float subTotalMaterialTemp=subTotalMaterialCost+strPartCostAdj;
        
        if ([strTypeOfPrice isEqualToString:@"Sales Price"]) {
            
            subTotalMaterialTemp=subTotalMaterialCostSales+strPartCostAdj;
            
        }
        
        if (subTotalMaterialTemp>0) {
            
            taxMaterial=(subTotalMaterialTemp*taxValue)/100;
            
        }
        
    }
    
    
    tax=taxMaterial+taxLabor;
    
    return tax;
}


-(double)taxCalculationForParts :(double)totalPrice :(NSString*)strTypeOfPlan{
    
    double tax=0.0;
    
    NSArray *arrOfTaxMaster=[global getSalesTaxFromMaster:strCompanyKey :strDepartmentSysName :[matchesFinalSubWorkdOrder valueForKey:@"propertyType"]];
    
    if (!(arrOfTaxMaster.count==0)) {
        
        //Check According To Condition to apply tax on what
        
        BOOL isLaborTax=NO,isMaterialTax=NO,isVendorPayTax=NO,isFullTax=NO;
        
        NSString *strPartPriceType;
        
        NSDictionary *dictTaxData=arrOfTaxMaster[0];
        
        NSString *strVendorPayTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"VendorPayTax"]];
        
        if ([strVendorPayTax isEqualToString:@"true"] || [strVendorPayTax isEqualToString:@"True"] || [strVendorPayTax isEqualToString:@"1"]) {
            
            isVendorPayTax=YES;
            
        }
        
        if ([strWorkOrderAddressSubType isEqualToString:@"Commercial"]) {//Commercial Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMPartPriceType"]];
                
            }
            
        } else {//Resedential Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMPartPriceType"]];
                
            }
            
        }
        
        
        isGlobalVendorTax=isVendorPayTax;
        
        if (isFullTax) {
            
            isGlobalFullTax=TRUE;
            
            double taxValue;
            taxValue=[strTax doubleValue];
            tax=(totalPrice*taxValue)/100;
            
        } else {
            
            tax=[self calculationTaxForMaterialnLaborForParts :isLaborTax :isMaterialTax :strPartPriceType :strTypeOfPlan];
            isGlobalPartTax=isMaterialTax;
            isGlobalLaborTax=isLaborTax;
            
        }
    }
    
    return tax;
}

-(double)calculationTaxForMaterialnLaborForParts :(BOOL)isLaborTax :(BOOL)isMaterialTax :(NSString*)strTypeOfprice :(NSString*)strTypeOfPlan{
    
    float totalCostPartss=0.0,totalCostPartsSales=0,subTotalMaterialForDiscount=0.0;
    NSMutableArray *arrTotal,*arrTemp;
    arrTotal=[[NSMutableArray alloc]init];
    arrTemp=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrOfSubWorkServiceIssues.count; i++)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[i];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                
            }
        }
    }
    
    for (int k=0; k<arrTemp.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrTemp[k];
        
        if ([[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"1"]|| [[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@""])
        {
            NSString *strMulti=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"multiplier"]];
            float multip=[strMulti floatValue];
            
            //NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"qty"]];
            NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"actualQty"]];
            
            float qtyy=[strqtyy floatValue];
            
            NSString *strunitPricee=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"unitPrice"]];
            float unitPricee=[strunitPricee floatValue];
            float costPriceParts=unitPricee*qtyy;
            float finalPriceParts=unitPricee*qtyy*multip;
            
            // Akshay
            subTotalMaterialForDiscount=subTotalMaterialForDiscount+finalPriceParts;
            //
            totalCostPartss=totalCostPartss+costPriceParts;
            totalCostPartsSales=totalCostPartsSales+finalPriceParts;
            
        }
        
    }
    
    
    //    // Akshay Start //
    //    totalLaborAmountLocal = [self getNumberString:_lblBillableHours.text];
    //    totalPartAmountLocal =  subTotalMaterialForDiscount;
    //    totalLaborAmountLocalCopy = [self getNumberString:_lblBillableHours.text];
    //    totalPartAmountLocalCopy = subTotalMaterialForDiscount;
    //    // Akshay End //
    
    double tax = 0.0,amountDue,taxMaterial = 0.0,taxLabor = 0.0;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    double taxValue;
    taxValue=[strTax doubleValue];
    
    if (isMaterialTax) {
        
        float subTotalMaterialTemp=totalCostPartss;
        
        if ([strTypeOfprice isEqualToString:@"Sales Price"]) {
            
            subTotalMaterialTemp=totalCostPartsSales;
            
        }
        
        if (subTotalMaterialTemp>0) {
            
            taxMaterial=(subTotalMaterialTemp*taxValue)/100;
            
        }
        
    }
    
    if (isLaborTax) {
        
        float laborAppliedCredits = 0.0;
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            if([[dict valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnLaborPrice"] boolValue]==true)
            {
                laborAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
        }
        
        //[self calculateTripChargeBasedOnQty];
        
        // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se
        
        float subTotalLAborTemp =[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblBillableHours.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_lblValueForPSPCharge.text]+[self getNumberString:_txt_MiscellaneousCharge.text]-[self getNumberString:_lblValueForPSPDiscount.text]-([strDiscount doubleValue] + laborAppliedCredits);  // -[strDiscount doubleValue]
        
        if (subTotalLAborTemp>0) {
            
            taxLabor=(subTotalLAborTemp*taxValue)/100;
            
        }
    }
    
    tax=taxMaterial+taxLabor;
    
    return tax;
    
}


#pragma mark- Service Job Description Method

- (IBAction)action_ServiceJobDescriptionTitle:(id)sender {
    
    
    if (arrPickerServiceJobDescriptions.count==0) {
        
        [global AlertMethod:Alert :NoDataAvailableServiceJobDescriptions];
        
    }else{
        
        tblData.tag=107;
        [self tableLoad:107];
        
    }
}

-(void)fetchServiceJobDescriptionsFromMaster{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    if ([dictdata isKindOfClass:[NSString class]]) {
        
        
    }else{
        
        arrPickerServiceJobDescriptions=nil;
        arrPickerServiceJobDescriptions=[[NSMutableArray alloc]init];
        
        NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceJobDescriptionTemplate"];
        if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
            
            NSDictionary *dictTemp=@{@"CompanyId":@"",@"IsActive":@"",@"ServiceJobDescription":@"",@"ServiceJobDescriptionId":@"",@"Title":@"Select"};
            
            [arrPickerServiceJobDescriptions addObject:dictTemp];
            
            for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                
                NSDictionary *dictData=[arrOfServiceJobDescriptionTemplate objectAtIndex:k];
                
                BOOL isActiveeee=[[dictData valueForKey:@"IsActive"] boolValue];
                
                if (isActiveeee) {
                    
                    [arrPickerServiceJobDescriptions addObject:dictData];
                    
                }
            }
            
        }else{
            
            
        }
    }
}
-(void)updateServiceJobDescription{
    
    [matchesWorkOrder setValue:strGlobalServiceJobDescriptionId forKey:@"serviceJobDescriptionId"];
    // [matchesWorkOrder setValue:[self getHTML] forKey:@"serviceJobDescription"];
    [matchesWorkOrder setValue:_textView_ServiceJobDescriptions.text forKey:@"serviceJobDescription"];
    
    NSError *error;
    [context save:&error];
    
}

- (IBAction)action_ServiceAddressMapView:(id)sender {
    
    [global redirectOnAppleMap:self :_lblServiceAddressValue.text];
    
}

- (IBAction)action_BillingAddressMapView:(id)sender {
    
    [global redirectOnAppleMap:self :_lblBillingAddressValue.text];
    
}

- (IBAction)action_Email:(id)sender {
    
    [global emailComposer:_lblEmailValue.text :@"" :@"" :self];
    
}
- (IBAction)action_IncreaseActualHours:(id)sender {
    
    if (_txtIncDecActualHours.text.length==0) {
        
        [global AlertMethod:Alert :@"Time is required to increase or decrease actual hours time."];
        
    }else if (_txtIncDecActualHours.text.length<5) {
        
        [global AlertMethod:Alert :@"Time is required in hh:mm(00:00) to increase or decrease actual hours time."];
        
    } else {
        
        NSArray *arrTime=[_txtIncDecActualHours.text componentsSeparatedByString:@":"];
        
        NSString *strHrs,*strMinutes;
        
        if (arrTime.count==1) {
            
            strHrs=arrTime[0];
            strMinutes=@"";
            
        }
        
        if (arrTime.count==2) {
            
            strHrs=arrTime[0];
            strMinutes=arrTime[1];
            
        }
        
        NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
        
        totalsecond=totalsecond+secondTimeToIncrease;
        
        if (totalsecond>0) {
            
        } else {
            totalsecond=0;
        }
        
        NSDictionary *dictTimeClock=[self createTimemapForSeconds:totalsecond];
        
        NSString *strTimeFormat=[NSString stringWithFormat:@"%02d hrs:%02d min",[[dictTimeClock valueForKey:@"h"] intValue],[[dictTimeClock valueForKey:@"m"] intValue]];
        _lblTotalHrs.text=[NSString stringWithFormat:@"%@ (%@)",@"Total Hours",strTimeFormat];
        
        [self methodToCalculateLaborPriceNew:_txtIncDecActualHours.text :@"Plus"];
        
        _txtIncDecActualHours.text=@"";
    }
    
}

- (IBAction)action_DecreaseActualHours:(id)sender {
    
    if (_txtIncDecActualHours.text.length==0) {
        
        [global AlertMethod:Alert :@"Time is required to increase or decrease actual hours time."];
        
    }else if (_txtIncDecActualHours.text.length<5) {
        
        [global AlertMethod:Alert :@"Time is required in hh:mm(00:00) to increase or decrease actual hours time."];
        
    }
    else {
        NSArray *arrTime=[_txtIncDecActualHours.text componentsSeparatedByString:@":"];
        
        NSString *strHrs,*strMinutes;
        
        if (arrTime.count==1) {
            
            strHrs=arrTime[0];
            strMinutes=@"";
            
        }
        
        if (arrTime.count==2) {
            
            strHrs=arrTime[0];
            strMinutes=arrTime[1];
            
        }
        
        NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
        
        totalsecond=totalsecond-secondTimeToIncrease;
        
        if (totalsecond>0) {
            
        } else {
            totalsecond=0;
        }
        
        NSDictionary *dictTimeClock=[self createTimemapForSeconds:totalsecond];
        
        NSString *strTimeFormat=[NSString stringWithFormat:@"%02d hrs:%02d min",[[dictTimeClock valueForKey:@"h"] intValue],[[dictTimeClock valueForKey:@"m"] intValue]];
        _lblTotalHrs.text=[NSString stringWithFormat:@"%@ (%@)",@"Total Hours",strTimeFormat];
        
        [self methodToCalculateLaborPriceNew:_txtIncDecActualHours.text :@"Minus"];
        
        _txtIncDecActualHours.text=@"";
    }
    
}


-(void)methodToCalculateLaborPriceNew :(NSString *)strTimeEntered :(NSString*)strType{
    
    isLaborType=YES;
    isWarranty=NO;
    
    if (strTimeEntered.length==5) {
        
        NSDictionary *dictDataAfterHourRateToBeUsed=[[NSDictionary alloc]init];
        
        if (!isWarranty) {
            
            if (isLaborType) {
                
                //Logic For Price Calculation
                
                float hrsTime=[global ChangeTimeMechanicalInvoice:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        // Change for holiday and After hours change
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                            
                        } else {
                            
                            // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                            
                            if (strAfterHrsDuration.length==0) {
                                
                                strStdPrice=0.0;
                                
                            } else {
                                
                                NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                
                                NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                
                                strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                
                                
                                NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                
                                for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                    
                                    NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                    
                                    NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                    NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                    
                                    if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                        
                                        dictDataAfterHourRateToBeUsed=dictDataAHC;
                                        break;
                                        
                                    }
                                    
                                }
                                
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborPrice"] floatValue];
                                
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                float alreadyPresentPrice=[self getNumberString:_lblBillableHours.text];
                
                if ([strType isEqualToString:@"Plus"]) {
                    
                    totalPriceLabour=totalPriceLabour+alreadyPresentPrice;
                    
                } else {
                    
                    totalPriceLabour=alreadyPresentPrice-totalPriceLabour;
                    
                }
                if (totalPriceLabour>0) {
                    
                } else {
                    totalPriceLabour=0.0;
                }
                
                _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
                
            } else {
                
                //Logic For Price Calculation
                
                float hrsTime=[global ChangeTimeMechanicalInvoice:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];
                            
                        } else {
                            
                            if ((dictDataAfterHourRateToBeUsed==nil) || ([dictDataAfterHourRateToBeUsed isKindOfClass:[NSNull class]])) {
                                
                                
                                
                            } else {
                                
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                                
                            }
                            
                            strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                            
                        }
                        
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                float alreadyPresentPrice=[self getNumberString:_lblBillableHours.text];
                
                if ([strType isEqualToString:@"Plus"]) {
                    
                    totalPriceLabour=totalPriceLabour+alreadyPresentPrice;
                    
                } else {
                    
                    totalPriceLabour=alreadyPresentPrice-totalPriceLabour;
                    
                }
                if (totalPriceLabour>0) {
                    
                } else {
                    totalPriceLabour=0.0;
                }
                
                _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
            }
            
        }else{
            
            // Is Warranty Checked Hai
            
            if (isLaborType) {
                
                //Logic For Price Calculation
                
                float hrsTime=[global ChangeTimeMechanicalInvoice:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"WrntyStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        // Change for holiday and After hours change
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborWrntyPrice"] floatValue];
                            
                        } else {
                            
                            // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                            
                            if (strAfterHrsDuration.length==0) {
                                
                                strStdPrice=0.0;
                                
                            } else {
                                
                                NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                
                                NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                
                                strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                
                                
                                NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                
                                for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                    
                                    NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                    
                                    NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                    NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                    
                                    if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                        
                                        dictDataAfterHourRateToBeUsed=dictDataAHC;
                                        break;
                                        
                                    }
                                    
                                }
                                
                                
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborWrntyPrice"] floatValue];
                                
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                float alreadyPresentPrice=[self getNumberString:_lblBillableHours.text];
                
                if ([strType isEqualToString:@"Plus"]) {
                    
                    totalPriceLabour=totalPriceLabour+alreadyPresentPrice;
                    
                } else {
                    
                    totalPriceLabour=alreadyPresentPrice-totalPriceLabour;
                    
                }
                if (totalPriceLabour>0) {
                    
                } else {
                    totalPriceLabour=0.0;
                }
                
                _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
                
            } else {
                
                //Logic For Price Calculation
                
                float hrsTime=[global ChangeTimeMechanicalInvoice:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HelperWrntyStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayHelperWrntyPrice"] floatValue];
                            
                        } else {
                            
                            strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperWrntyPrice"] floatValue];
                            
                        }
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                float alreadyPresentPrice=[self getNumberString:_lblBillableHours.text];
                
                if ([strType isEqualToString:@"Plus"]) {
                    
                    totalPriceLabour=totalPriceLabour+alreadyPresentPrice;
                    
                } else {
                    
                    totalPriceLabour=alreadyPresentPrice-totalPriceLabour;
                    
                }
                
                if (totalPriceLabour>0) {
                    
                } else {
                    totalPriceLabour=0.0;
                }
                
                _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
            }
            
            
        }
        
    }else{
        
        
    }
    
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
    
}


- (IBAction)action_AddBillableLaborHrs:(id)sender {
    
    if (_txtIncDecActualHours.text.length==0) {
        
        [global AlertMethod:Alert :@"Time is required to update billable labor hours."];
        
    }else if (_txtIncDecActualHours.text.length<5) {
        
        [global AlertMethod:Alert :@"Time is required in hh:mm(00:00) to update billable labor hours."];
        
    } else {
        
        if (_txtIncDecActualHours.text.length==5) {
            
            float hrsTime=[global ChangeTimeMechanicalInvoice:_txtIncDecActualHours.text];
            
            hrsTime=hrsTime/3600;
            
            float strStdPrice=0.0;
            
            if (!(arrOfHoursConfig.count==0)) {
                
                NSDictionary *dictDataHours=arrOfHoursConfig[0];
                
                strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                if (totalPriceLabour>0) {
                    
                } else {
                    totalPriceLabour=0.0;
                }
                _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",totalPriceLabour];
            }
            
            if ([strWoType isEqualToString:@"FR"])
            {
                [self calculateTotal:_txtOtherDiscounts.text];
            }
            else if ([strWoType isEqualToString:@"TM"])
            {
                [self calculateTotalForParts:_txtOtherDiscounts.text];
            }
            
        }else{
            
            [global AlertMethod:Alert :@"Time is required in hh:mm(00:00) to update billable labor hours."];
            
        }
    }
}


- (IBAction)action_CollapsePaymentView:(id)sender {
    
    if ([_btnCollapsePaymentView.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnCollapsePaymentView setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        isExpandPaymentView=YES;
        isUpdateViewsFrame = YES;
        if(arrayAppliedDiscounts.count==0)
        {
            //            if([strWoType isEqualToString:@"FR"])
            //            {
            //                heightPaymntDetailView = paymentDetailHeight;
            //
            //               // 1128-110;// 110 for View Dynamic Complete Emp Time, its hidden if WO is of FR type
            //
            //            }
            //            else
            //            {
            ////            heightPaymntDetailView = 1128;
            //                heightPaymntDetailView = paymentDetailHeight;
            //            }
            
            heightPaymntDetailView = paymentDetailHeight;
        }
        else
        {
            //            if([strWoType isEqualToString:@"FR"])
            //            {
            //               // heightPaymntDetailView = (1128-110)+(int)(arrayAppliedDiscounts.count*250);// 110 for View Dynamic Complete Emp Time, its hidden if WO is of FR type
            //
            //                heightPaymntDetailView = paymentDetailHeight+(int)(arrayAppliedDiscounts.count*250);
            //
            //            }
            //            else
            //            {
            //               // heightPaymntDetailView = 1128+(int)(arrayAppliedDiscounts.count*250);
            //                heightPaymntDetailView = paymentDetailHeight+(int)(arrayAppliedDiscounts.count*250);
            //            }
            
            heightPaymntDetailView = paymentDetailHeight+(int)(arrayAppliedDiscounts.count*220)+(int)(arrayAppliedDiscounts.count*60);
            
            
        }
        [self addView:@"Notadd"];
    } else {
        //Band Hoga
        [_btnCollapsePaymentView setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        isExpandPaymentView=NO;
        [self addView:@"Notadd"];
    }
    
}

#pragma mark- Preset Signature Methods

//Preset Signature Changes
-(void)checkForPresetSign :(NSManagedObject*)matchesSubWo{
    
    NSUserDefaults *defsSign=[NSUserDefaults standardUserDefaults];
    
    BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignService"];
    
    NSString *strSignUrl=[defsSign valueForKey:@"ServiceTechSignPath"];
    
    if ((isPreSetSign) && (strSignUrl.length>0) && isCompletedStatusMechanical) {
        
        strSignUrl=[strSignUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        NSString *result;
        NSRange equalRange = [strSignUrl rangeOfString:@"Documents" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strSignUrl substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=strSignUrl;
        }
        
        equalRange = [result rangeOfString:@"EmployeeSignatures"];
        if (equalRange.location != NSNotFound) {
            result = [result substringFromIndex:equalRange.location];
        }else{
            result=result;
        }
        
        strTechnicianSign=result;
        
        [self downloadingImagessTechnicianPreSet:strSignUrl];
        
        [_btnInspectorSign setEnabled:NO];
        
        isPreSetSignGlobal=YES;
        
    } else if ((isPreSetSign) && (strSignUrl.length>0) && !isCompletedStatusMechanical){
        
        isPreSetSignGlobal=YES;
        
        [_btnInspectorSign setEnabled:NO];
        
        strSignUrl=[strSignUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        NSString *result;
        NSRange equalRange = [strSignUrl rangeOfString:@"Documents" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strSignUrl substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=strSignUrl;
        }
        
        equalRange = [result rangeOfString:@"EmployeeSignatures"];
        if (equalRange.location != NSNotFound) {
            result = [result substringFromIndex:equalRange.location];
        }else{
            result=result;
        }
        
        strTechnicianSign=result;
        
        [self downloadingImagessTechnicianIfPreset:strTechnicianSign];
        
    } else if ((!isPreSetSign) || (strSignUrl.length<1)){
        
        isPreSetSignGlobal=NO;
        
        [_btnInspectorSign setEnabled:YES];
        
    } else{
        
        isPreSetSignGlobal=NO;
        [_btnInspectorSign setEnabled:YES];
        
    }
    
}

-(void)downloadingImagessTechnicianPreSet :(NSString*)str{
    
    //    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [strTechnicianSign rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [strTechnicianSign substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=strTechnicianSign;
    }
    if (result.length==0) {
        NSRange equalRange = [strTechnicianSign rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strTechnicianSign substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@",str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :strTechnicianSign];
    } else {
        
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            strNewString=[strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :strTechnicianSign];
                
            });
        });
        
    }
}

-(void)ShowFirstImageTechPreSet :(NSString*)str{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    _imgInspectorSign.image=[self loadImage:result];
    
}

-(void)downloadingImagessTechnicianIfPreset :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation1=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.HrmsServiceModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@//Documents//%@",strServiceUrlMainServiceAutomation1,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :str];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            strNewString=[strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :str];
                
            });
        });
        
    }
}

-(void)ShowFirstImageTech :(NSString*)str{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    _imgInspectorSign.image=[self loadImage:result];
    
}

-(void)downloadingImagessTechnician :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation1=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation1,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :str];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            // NSURL *url = [NSURL URLWithString:strNewString];
            strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :str];
                
            });
        });
        
    }
}

//=======================================
//=======================================
#pragma mark- Employee Sheet Changes
//=======================================
//=======================================

-(void)fetchEmployeeTimeSheetDataFromDb :(NSString*)strWorkOrderNew :(NSString*)strSubWorkOrderNew{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    requestSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestSubWoEmployeeWorkingTimeExtSerDcs setEntity:entitySubWoEmployeeWorkingTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderNew,strSubWorkOrderNew];
    
    [requestSubWoEmployeeWorkingTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:YES];
    sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs];
    
    [requestSubWoEmployeeWorkingTimeExtSerDcs setSortDescriptors:sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs];
    
    self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWoEmployeeWorkingTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs performFetch:&error];
    arrAllObjSubWoEmployeeWorkingTimeExtSerDcs = [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs fetchedObjects];
    
    if ([arrAllObjSubWoEmployeeWorkingTimeExtSerDcs count] == 0)
    {
        
        NSMutableArray *arrTemporary=[self fetchTempEmployeeTimeSheetDataFromDb:_strWorlOrderId :_strSubWorkOderId];
        
        if (arrTemporary.count>0) {
            
            [self logicAfterFetchingFromDb:arrTemporary :@"null"];
            
        }
        
    }
    else
    {
        
        [self logicAfterFetchingFromDb:arrAllObjSubWoEmployeeWorkingTimeExtSerDcs :@"ValuePresent"];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}


-(void)logicAfterFetchingFromDb :(NSArray*)arrData :(NSString*)strType{
    
    arrOfTimeSlotsEmployeeWise=[[NSMutableArray alloc]init];
    arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
    arrOfEmployeesAssignedInSubWorkOrder=[[NSMutableArray alloc]init];
    arrOfWorkingDate=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrWorkingDateTemp=[[NSMutableArray alloc]init];
    NSMutableArray *arrWorkingDateTempForMultipleDates=[[NSMutableArray alloc]init];// all dates were not coming uske liye change kiye.
    
    [arrOfHeaderTitleForSlots addObject:@"Standard"];
    
    NSMutableArray *arrTempForWorkingDate=[[NSMutableArray alloc]init];
    
    [arrTempForWorkingDate addObjectsFromArray:arrData];
    
    if (![strType isEqualToString:@"null"]) {
        
        NSMutableArray *arrTemporary=[self fetchTempEmployeeTimeSheetDataFromDb:_strWorlOrderId :_strSubWorkOderId];
        
        if (arrTemporary.count>0) {
            
            // [arrOfTimeSlotsEmployeeWise addObjectsFromArray:arrTemporary];
            [arrTempForWorkingDate addObjectsFromArray:arrTemporary];
        }
        
    }
    
    for (int k=0; k<arrTempForWorkingDate.count; k++) {
        
        NSManagedObject *objTemp=arrTempForWorkingDate[k];
        
        if ([objTemp isKindOfClass:[NSManagedObject class]]) {
            
            NSArray *keys = [[[objTemp entity] attributesByName] allKeys];
            NSDictionary *dict = [objTemp dictionaryWithValuesForKeys:keys];
            
            NSDictionary *dictObjTemp=dict;
            
            NSMutableDictionary *dictObjMutableTemp=[[NSMutableDictionary alloc]init];
            
            [dictObjMutableTemp addEntriesFromDictionary:dictObjTemp];
            
            [arrOfTimeSlotsEmployeeWise addObject:dictObjMutableTemp];
            
        } else {
            
            [arrOfTimeSlotsEmployeeWise addObject:objTemp];
            
        }
        
        
        NSString *strWorkingDate=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingDate"]];
        NSString *strSubWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderActualHourId"]];
        
        if (![arrWorkingDateTemp containsObject:strSubWorkOrderActualHourId] || ![arrWorkingDateTempForMultipleDates containsObject:strWorkingDate]) {
            
            [arrOfWorkingDate addObject:objTemp];
            [arrWorkingDateTemp addObject:strSubWorkOrderActualHourId];
            [arrWorkingDateTempForMultipleDates addObject:strWorkingDate];
            
        }
        
        NSString *strworkingType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingType"]];
        NSString *strTitleSlotsComing=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"timeSlotTitle"]];
        
        NSString *strTitleToSet;
        if ([strworkingType isEqualToString:@"1"]) {
            strTitleToSet=@"Standard";
        } else if ([strworkingType isEqualToString:@"2"]){
            strTitleToSet=strTitleSlotsComing;
            
            if(strTitleToSet.length==0){
                
                strTitleToSet=@"N/A";
                
            }
            
            if (![arrOfHeaderTitleForSlots containsObject:strTitleToSet]) {
                
                [arrOfHeaderTitleForSlots addObject:strTitleToSet];
                
            }
            
        } else if ([strworkingType isEqualToString:@"3"]){
            strTitleToSet=@"Holiday";
        }
        
        
        NSString *strEmployeesAssignedInSubWorkOrder=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
        
        if (![arrOfEmployeesAssignedInSubWorkOrder containsObject:strEmployeesAssignedInSubWorkOrder]) {
            
            [arrOfEmployeesAssignedInSubWorkOrder addObject:strEmployeesAssignedInSubWorkOrder];
            
        }
        
    }
    
    [arrOfHeaderTitleForSlots addObject:@"Holiday"];
    
    arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
    arrOfHeaderTitleForSlots=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
    
    /*
     if (![strType isEqualToString:@"null"]) {
     
     NSMutableArray *arrTemporary=[self fetchTempEmployeeTimeSheetDataFromDb:_strWorlOrderId :_strSubWorkOderId];
     
     if (arrTemporary.count>0) {
     
     [arrOfTimeSlotsEmployeeWise addObjectsFromArray:arrTemporary];
     
     }
     
     }
     */
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
    NSMutableArray *arrOfEmpIdss=[[NSMutableArray alloc]init];
    arrOfEmpIdss=arrOfEmployeesAssignedInSubWorkOrder;
    
    arrOfEmployeesAssignedInSubWorkOrder=[[NSMutableArray alloc]init];
    
    if ([dictEmployeeList isKindOfClass:[NSString class]]) {
        
        
    } else {
        
        if (!(dictEmployeeList.count==0)) {
            NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
            for (int k=0; k<arrOfEmployee.count; k++) {
                NSDictionary *dictDatata=arrOfEmployee[k];
                NSString *strEmpIdsss=[NSString stringWithFormat:@"%@",[dictDatata valueForKey:@"EmployeeNo"]];
                if ([arrOfEmpIdss containsObject:strEmpIdsss]) {
                    [arrOfEmployeesAssignedInSubWorkOrder addObject:dictDatata];
                }
            }
        }
    }
    
    NSMutableArray *arrOfMainTempEmployeeTimeSlot=[[NSMutableArray alloc]init];
    
    // Logic for creating final Array list for Employee Time Slot Wise
    
    for (int q=0; q<arrOfEmployeesAssignedInSubWorkOrder.count; q++) {
        
        NSMutableDictionary *dictOfMainObj=[[NSMutableDictionary alloc]init];
        
        NSDictionary *dictObjEmp=arrOfEmployeesAssignedInSubWorkOrder[q];
        
        NSString *strEmployeeNoInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"EmployeeNo"]];
        NSString *strEmployeeNameInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"FullName"]];
        
        [dictOfMainObj setValue:strEmployeeNoInsideLoop forKey:@"EmployeeNo"];
        [dictOfMainObj setValue:strEmployeeNameInsideLoop forKey:@"EmployeeName"];
        
        // Change for Labor Type
        
        if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoLoggedIn]) {
            
            BOOL isMechanicLocal = [global isMechanicTypeForLoggedInUser:_strWorlOrderId :_strSubWorkOderId];
            
            if (isMechanicLocal) {
                
                [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                
            } else {
                
                [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                
            }
            
            //[dictOfMainObj setValue:@"No" forKey:@"Helper"];
            
            
        } else {
            
            BOOL isMechanicLocal = [global isMechanicType:_strWorlOrderId :_strSubWorkOderId :strEmployeeNoInsideLoop];
            
            if (isMechanicLocal) {
                
                [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                
            } else {
                
                [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                
            }
            //[dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
            
        }
        
        NSMutableArray *arrOfTempEmployeeListFiltered=[[NSMutableArray alloc]init];
        
        for (int q1=0; q1<arrOfWorkingDate.count; q1++) {
            
            NSMutableDictionary *dictOfMainObjDateWise=[[NSMutableDictionary alloc]init];
            
            NSDictionary *dictObjWorkingDate=arrOfWorkingDate[q1];
            
            NSString *strSubWorkOrderActualHourIdInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"subWorkOrderActualHourId"]];
            NSString *strWorkingDateInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"workingDate"]];
            
            //[dictOfMainObj setValue:strSubWorkOrderActualHourIdInsideLoop forKey:@"ActualHrId"];
            [dictOfMainObjDateWise setValue:strWorkingDateInsideLoop forKey:@"WorkingDate"];
            
            NSMutableArray *arrOfTempDateWiseObj=[[NSMutableArray alloc]init];
            
            for (int q2=0; q2<arrOfHeaderTitleForSlots.count; q2++) {
                
                NSString *strHeaderTitleInsideLoop=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[q2]];
                
                NSMutableDictionary *dictTemoObjEmployeeWiseSlots=[[NSMutableDictionary alloc]init];
                
                for (int q3=0; q3<arrOfTimeSlotsEmployeeWise.count; q3++) {
                    
                    NSDictionary *dictObjTimeSlotsEmployeeWise=arrOfTimeSlotsEmployeeWise[q3];
                    
                    NSString *strEmployeeNoTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"employeeNo"]];
                    NSString *strWorkingDateTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingDate"]];
                    NSString *strActualHrIdTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"subWorkOrderActualHourId"]];
                    NSString *strWorkingTypeTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingType"]];
                    NSString *strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlotTitle"]];
                    NSString *strtimeSlotTitleEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlot"]];
                    
                    BOOL isMatchecd = [global isSlotMatched:strtimeSlotTitleEmployeeWiseInsideLoop :strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3 :strHeaderTitleInsideLoop];
                    
                    
                    if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Standard"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"]) {
                        
                        [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                        
                    }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Holiday"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                        
                        [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                        
                    }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && isMatchecd &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"] &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                        
                        [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                        
                    }
                    
                }
                
                // if dictTemoObjEmployeeWiseSlots is null to apna pura data banaunga mai
                
                if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                    
                    NSString *strDateNew =[global strCurrentDate];
                    
                    [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"actualAmt"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"actualDurationInMin"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"billableAmt"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"billableDurationInMin"];
                    [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"createdBy"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"Mobile" forKey:@"createdByDevice"];
                    [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"createdDate"];
                    //[dictTemoObjEmployeeWiseSlots setObject:strEmployeeNameInsideLoop forKey:@"employeeName"];
                    [dictTemoObjEmployeeWiseSlots setObject:strEmployeeNoInsideLoop forKey:@"employeeNo"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isActive"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isApprove"];
                    [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"modifiedBy"];
                    [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"modifiedDate"];
                    [dictTemoObjEmployeeWiseSlots setObject:[global getReferenceNumber] forKey:@"subWOEmployeeWorkingTimeId"];
                    [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderActualHourIdInsideLoop forKey:@"subWorkOrderActualHourId"];
                    [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderIdGlobal forKey:@"subWorkOrderId"];
                    [dictTemoObjEmployeeWiseSlots setObject:strWorkingDateInsideLoop forKey:@"workingDate"];
                    [dictTemoObjEmployeeWiseSlots setObject:strWorkOrderId forKey:@"workorderId"];
                    
                    if ([strHeaderTitleInsideLoop isEqualToString:@"Standard"]) {
                        
                        [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"1" forKey:@"workingType"];
                        
                    } else if ([strHeaderTitleInsideLoop isEqualToString:@"Holiday"]){
                        
                        [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"3" forKey:@"workingType"];
                        
                    } else{
                        
                        NSString *strSlotsToSet,*strTitleSlotToSet;
                        
                        if (arrOfHoursConfig.count>0) {
                            
                            NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
                            
                            NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
                            
                            for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                                
                                NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                                NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                NSString *strTimeSlotsNew =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                
                                BOOL isMatched = [global isSlotMatched:strTitle :strTimeSlotsNew :strHeaderTitleInsideLoop];
                                
                                if (isMatched) {
                                    
                                    strSlotsToSet=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                    strTitleSlotToSet = [NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                    break;
                                }
                            }
                        }
                        
                        if (strSlotsToSet.length==0) {
                            
                            strSlotsToSet=@"N/A";
                            
                        }
                        
                        [dictTemoObjEmployeeWiseSlots setObject:strSlotsToSet forKey:@"timeSlot"];
                        [dictTemoObjEmployeeWiseSlots setObject:strTitleSlotToSet forKey:@"timeSlotTitle"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"2" forKey:@"workingType"];
                        
                    }
                    
                }
                
                [dictTemoObjEmployeeWiseSlots setValue:[dictOfMainObj valueForKey:@"Helper"] forKey:@"isHelper"];
                
                [arrOfTempDateWiseObj addObject:dictTemoObjEmployeeWiseSlots];
                
            }
            
            
            [dictOfMainObjDateWise setValue:arrOfTempDateWiseObj forKey:@"DateWise"];
            
            [arrOfTempEmployeeListFiltered addObject:dictOfMainObjDateWise];
            
        }
        [dictOfMainObj setValue:arrOfTempEmployeeListFiltered forKey:@"DateWiseData"];
        [arrOfMainTempEmployeeTimeSlot addObject:dictOfMainObj];
        //[arrOfMainTempEmployeeTimeSlot addObject:arrOfTempEmployeeListFiltered];
        
    }
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:arrOfMainTempEmployeeTimeSlot])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:arrOfMainTempEmployeeTimeSlot options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Employee Time Sheet arrOfMainTempEmployeeTimeSlot Json object final to set on view: %@", jsonString);
        }
    }
    
    arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
    [arrOfGlobalDynamicEmpSheetFinal addObjectsFromArray:arrOfMainTempEmployeeTimeSlot];
    [self createDynamicEmpSheet:arrOfGlobalDynamicEmpSheetFinal :arrOfHeaderTitleForSlots];
    
}

-(NSMutableArray*)fetchTempEmployeeTimeSheetDataFromDb :(NSString*)strWorkOrderNew :(NSString*)strSubWorkOrderNew{
    
    NSMutableArray *arrTempDataall;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTempSubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"TempSubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    requestTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setEntity:entityTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderNew,strSubWorkOrderNew];
    
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:YES];
    sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setSortDescriptors:sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTempSubWoEmployeeWorkingTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs performFetch:&error];
    arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs = [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs fetchedObjects];
    
    if ([arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs count] == 0)
    {
        
        
    }
    else
    {
        arrOfTimeSlotsEmployeeWiseTempNew=[[NSMutableArray alloc]init];
        arrOfHeaderTitleForSlotsTempNew=[[NSMutableArray alloc]init];
        arrOfEmployeesAssignedInSubWorkOrderTempNew=[[NSMutableArray alloc]init];
        arrOfWorkingDateTempNew=[[NSMutableArray alloc]init];
        
        NSMutableArray *arrWorkingDateTemp=[[NSMutableArray alloc]init];
        NSMutableArray *arrWorkingDateTempForMultipleDates=[[NSMutableArray alloc]init];// all dates were not coming uske liye change kiye.
        
        [arrOfHeaderTitleForSlotsTempNew addObject:@"Standard"];
        
        for (int k=0; k<arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs.count; k++) {
            
            NSManagedObject *objTemp=arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs[k];
            
            NSArray *keys = [[[objTemp entity] attributesByName] allKeys];
            NSDictionary *dict = [objTemp dictionaryWithValuesForKeys:keys];
            
            NSDictionary *dictObjTemp=dict;
            
            NSMutableDictionary *dictObjMutableTemp=[[NSMutableDictionary alloc]init];
            
            [dictObjMutableTemp addEntriesFromDictionary:dictObjTemp];
            
            [arrOfTimeSlotsEmployeeWiseTempNew addObject:dictObjMutableTemp];
            
            NSString *strWorkingDate=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingDate"]];
            NSString *strSubWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderActualHourId"]];
            
            //            if (![arrWorkingDateTemp containsObject:strSubWorkOrderActualHourId] || ![arrWorkingDateTempForMultipleDates containsObject:strWorkingDate]) {
            //
            //                [arrOfWorkingDateTempNew addObject:objTemp];
            //                [arrWorkingDateTemp addObject:strSubWorkOrderActualHourId];
            //                [arrWorkingDateTempForMultipleDates addObject:strWorkingDate];
            //
            //            }
            
            if (![arrWorkingDateTemp containsObject:strWorkingDate]) {
                
                [arrOfWorkingDateTempNew addObject:objTemp];
                [arrWorkingDateTemp addObject:strWorkingDate];
                
            }
            
            NSString *strworkingType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingType"]];
            NSString *strTitleSlotsComing=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"timeSlotTitle"]];
            
            NSString *strTitleToSet;
            if ([strworkingType isEqualToString:@"1"]) {
                strTitleToSet=@"Standard";
            } else if ([strworkingType isEqualToString:@"2"]){
                strTitleToSet=strTitleSlotsComing;
                
                if(strTitleToSet.length==0){
                    
                    strTitleToSet=@"N/A";
                    
                }
                
                if (![arrOfHeaderTitleForSlotsTempNew containsObject:strTitleToSet]) {
                    
                    [arrOfHeaderTitleForSlotsTempNew addObject:strTitleToSet];
                    
                }
                
            } else if ([strworkingType isEqualToString:@"3"]){
                strTitleToSet=@"Holiday";
            }
            
            
            NSString *strEmployeesAssignedInSubWorkOrder=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
            
            if (![arrOfEmployeesAssignedInSubWorkOrderTempNew containsObject:strEmployeesAssignedInSubWorkOrder]) {
                
                [arrOfEmployeesAssignedInSubWorkOrderTempNew addObject:strEmployeesAssignedInSubWorkOrder];
                
            }
            
        }
        
        [arrOfHeaderTitleForSlotsTempNew addObject:@"Holiday"];
        
        arrOfHeaderTitleForSlotsTempNew=[[NSMutableArray alloc]init];
        arrOfHeaderTitleForSlotsTempNew=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        
        NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
        NSMutableArray *arrOfEmpIdss=[[NSMutableArray alloc]init];
        arrOfEmpIdss=arrOfEmployeesAssignedInSubWorkOrderTempNew;
        
        arrOfEmployeesAssignedInSubWorkOrderTempNew=[[NSMutableArray alloc]init];
        
        if ([dictEmployeeList isKindOfClass:[NSString class]]) {
            
            
        } else {
            
            if (!(dictEmployeeList.count==0)) {
                NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
                for (int k=0; k<arrOfEmployee.count; k++) {
                    NSDictionary *dictDatata=arrOfEmployee[k];
                    NSString *strEmpIdsss=[NSString stringWithFormat:@"%@",[dictDatata valueForKey:@"EmployeeNo"]];
                    if ([arrOfEmpIdss containsObject:strEmpIdsss]) {
                        [arrOfEmployeesAssignedInSubWorkOrderTempNew addObject:dictDatata];
                    }
                }
            }
        }
        
        
        
        NSMutableArray *arrOfMainTempEmployeeTimeSlot=[[NSMutableArray alloc]init];
        
        // Logic for creating final Array list for Employee Time Slot Wise
        
        for (int q=0; q<arrOfEmployeesAssignedInSubWorkOrderTempNew.count; q++) {
            
            
            NSMutableDictionary *dictOfMainObj=[[NSMutableDictionary alloc]init];
            
            NSDictionary *dictObjEmp=arrOfEmployeesAssignedInSubWorkOrderTempNew[q];
            
            NSString *strEmployeeNoInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"EmployeeNo"]];
            NSString *strEmployeeNameInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"FullName"]];
            
            [dictOfMainObj setValue:strEmployeeNoInsideLoop forKey:@"EmployeeNo"];
            [dictOfMainObj setValue:strEmployeeNameInsideLoop forKey:@"EmployeeName"];
            
            // Change for Labor Type
            
            if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoLoggedIn]) {
                
                BOOL isMechanicLocal = [global isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderIdGlobal];
                
                if (isMechanicLocal) {
                    
                    [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                    
                } else {
                    
                    [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                    
                }
                
                //[dictOfMainObj setValue:@"No" forKey:@"Helper"];
                
                
            } else {
                
                BOOL isMechanicLocal = [global isMechanicType:strWorkOrderId :strSubWorkOrderIdGlobal :strEmployeeNoInsideLoop];
                
                if (isMechanicLocal) {
                    
                    [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                    
                } else {
                    
                    [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                    
                }
                
                //[dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                
            }
            
            NSMutableArray *arrOfTempEmployeeListFiltered=[[NSMutableArray alloc]init];
            
            for (int q1=0; q1<arrOfWorkingDateTempNew.count; q1++) {
                
                NSMutableDictionary *dictOfMainObjDateWise=[[NSMutableDictionary alloc]init];
                
                NSDictionary *dictObjWorkingDate=arrOfWorkingDateTempNew[q1];
                
                NSString *strSubWorkOrderActualHourIdInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"subWorkOrderActualHourId"]];
                NSString *strWorkingDateInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"workingDate"]];
                
                //[dictOfMainObj setValue:strSubWorkOrderActualHourIdInsideLoop forKey:@"ActualHrId"];
                [dictOfMainObjDateWise setValue:strWorkingDateInsideLoop forKey:@"WorkingDate"];
                
                NSMutableArray *arrOfTempDateWiseObj=[[NSMutableArray alloc]init];
                
                for (int q2=0; q2<arrOfHeaderTitleForSlotsTempNew.count; q2++) {
                    
                    CGFloat actualAmountTemp=0.0,billableAmountTemp=0.0,actualDurationTemp=0.0,billableDurationTemp=0.0;
                    
                    NSString *strHeaderTitleInsideLoop=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlotsTempNew[q2]];
                    
                    NSMutableDictionary *dictTemoObjEmployeeWiseSlots=[[NSMutableDictionary alloc]init];
                    
                    for (int q3=0; q3<arrOfTimeSlotsEmployeeWiseTempNew.count; q3++) {
                        
                        NSDictionary *dictObjTimeSlotsEmployeeWise=arrOfTimeSlotsEmployeeWiseTempNew[q3];
                        
                        NSString *strEmployeeNoTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"employeeNo"]];
                        NSString *strWorkingDateTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingDate"]];
                        NSString *strActualHrIdTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"subWorkOrderActualHourId"]];
                        NSString *strWorkingTypeTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingType"]];
                        NSString *strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlotTitle"]];
                        NSString *strtimeSlotTitleEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlot"]];
                        
                        BOOL isMatchecd = [global isSlotMatched:strtimeSlotTitleEmployeeWiseInsideLoop :strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3 :strHeaderTitleInsideLoop];
                        
                        NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualAmt"]];
                        NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualDurationInMin"]];
                        NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableAmt"]];
                        NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableDurationInMin"]];
                        
                        /*
                         NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualAmt"]];
                         NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualDurationInMin"]];
                         NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableAmt"]];
                         NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableDurationInMin"]];
                         */
                        
                        //                        actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                        //                        actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                        //                        billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                        //                        billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                        
                        strActualHrIdTimeSlotsEmployeeWiseInsideLoop = strSubWorkOrderActualHourIdInsideLoop;
                        
                        if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Standard"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"]) {
                            
                            actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                            actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                            billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                            billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Holiday"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                            
                            actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                            actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                            billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                            billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && isMatchecd &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"] &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                            
                            actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                            actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                            billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                            billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        }else{
                            
                            //                            actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                            //                            actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                            //                            billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                            //                            billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                            
                        }
                        
                        if ([dictTemoObjEmployeeWiseSlots count] > 0) {
                            
                            if (dictTemoObjEmployeeWiseSlots.count==4) {
                                
                            } else {
                                
                                [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualAmountTemp] forKey:@"actualAmt"];
                                [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualDurationTemp] forKey:@"actualDurationInMin"];
                                [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableAmountTemp] forKey:@"billableAmt"];
                                [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableDurationTemp] forKey:@"billableDurationInMin"];
                                //[dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%@",strActualHrIdTimeSlotsEmployeeWiseInsideLoop] forKey:@"subWorkOrderActualHourId"];
                                
                            }
                        }
                        
                    }
                    
                    
                    /*
                     // this was commented before don't know why but i m uncommenting it to check
                     // if dictTemoObjEmployeeWiseSlots is null to apna pura data banaunga mai
                     
                     if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                     
                     NSString *strDateNew =[global strCurrentDate];
                     
                     [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"actualAmt"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"actualDurationInMin"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"billableAmt"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"billableDurationInMin"];
                     [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"createdBy"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"Mobile" forKey:@"createdByDevice"];
                     [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"createdDate"];
                     //[dictTemoObjEmployeeWiseSlots setObject:strEmployeeNameInsideLoop forKey:@"employeeName"];
                     [dictTemoObjEmployeeWiseSlots setObject:strEmployeeNoInsideLoop forKey:@"employeeNo"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isActive"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isApprove"];
                     [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"modifiedBy"];
                     [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"modifiedDate"];
                     [dictTemoObjEmployeeWiseSlots setObject:[global getReferenceNumber] forKey:@"subWOEmployeeWorkingTimeId"];
                     [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderActualHourIdInsideLoop forKey:@"subWorkOrderActualHourId"];
                     [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderIdGlobal forKey:@"subWorkOrderId"];
                     [dictTemoObjEmployeeWiseSlots setObject:strWorkingDateInsideLoop forKey:@"workingDate"];
                     [dictTemoObjEmployeeWiseSlots setObject:strWorkOrderId forKey:@"workorderId"];
                     
                     if ([strHeaderTitleInsideLoop isEqualToString:@"Standard"]) {
                     
                     [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"1" forKey:@"workingType"];
                     
                     } else if ([strHeaderTitleInsideLoop isEqualToString:@"Holiday"]){
                     
                     [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"3" forKey:@"workingType"];
                     
                     } else{
                     
                     NSString *strSlotsToSet;
                     
                     if (arrOfHoursConfig.count>0) {
                     
                     NSString *strSlotsToSet,*strTitleSlotToSet;
                     
                     if (arrOfHoursConfig.count>0) {
                     
                     NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
                     
                     NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
                     
                     for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                     
                     NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                     NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                     NSString *strTimeSlotsNew =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                     
                     BOOL isMatched = [global isSlotMatched:strTitle :strTimeSlotsNew :strHeaderTitleInsideLoop];
                     
                     if (isMatched) {
                     
                     strSlotsToSet=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                     strTitleSlotToSet = [NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                     break;
                     }
                     }
                     }
                     
                     if (strSlotsToSet.length==0) {
                     
                     strSlotsToSet=@"N/A";
                     
                     }
                     
                     [dictTemoObjEmployeeWiseSlots setObject:strSlotsToSet forKey:@"timeSlot"];
                     [dictTemoObjEmployeeWiseSlots setObject:strTitleSlotToSet forKey:@"timeSlotTitle"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"2" forKey:@"workingType"];
                     
                     }
                     
                     if (strSlotsToSet.length==0) {
                     
                     strSlotsToSet=@"N/A";
                     
                     }
                     
                     [dictTemoObjEmployeeWiseSlots setObject:strSlotsToSet forKey:@"timeSlot"];
                     [dictTemoObjEmployeeWiseSlots setObject:strHeaderTitleInsideLoop forKey:@"timeSlotTitle"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"2" forKey:@"workingType"];
                     
                     }
                     
                     }
                     // yaha tak comment tha
                     */
                    
                    if (dictTemoObjEmployeeWiseSlots.count>1) {
                        
                        [dictTemoObjEmployeeWiseSlots setValue:strSubWorkOrderActualHourIdInsideLoop forKey:@"subWorkOrderActualHourId"];
                        
                        [dictTemoObjEmployeeWiseSlots setValue:[dictOfMainObj valueForKey:@"Helper"] forKey:@"isHelper"];
                        
                        [arrOfTempDateWiseObj addObject:dictTemoObjEmployeeWiseSlots];
                        
                    }
                }
                
                [dictOfMainObjDateWise setValue:arrOfTempDateWiseObj forKey:@"DateWise"];
                
                [arrOfTempEmployeeListFiltered addObject:dictOfMainObjDateWise];
                
            }
            [dictOfMainObj setValue:arrOfTempEmployeeListFiltered forKey:@"DateWiseData"];
            [arrOfMainTempEmployeeTimeSlot addObject:dictOfMainObj];
            //[arrOfMainTempEmployeeTimeSlot addObject:arrOfTempEmployeeListFiltered];
            
        }
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:arrOfMainTempEmployeeTimeSlot])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrOfMainTempEmployeeTimeSlot options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Employee Time Sheet Temporary arrOfMainTempEmployeeTimeSlot Json object final to set on view: %@", jsonString);
            }
        }
        
        arrTempDataall=[[NSMutableArray alloc]init];
        
        for (int q=0; q<arrOfMainTempEmployeeTimeSlot.count; q++) {
            
            NSDictionary *dictDataTemp=arrOfMainTempEmployeeTimeSlot[q];
            
            NSArray *arrTempData=[dictDataTemp valueForKey:@"DateWiseData"];
            
            for (int w=0; w<arrTempData.count; w++) {
                
                NSDictionary *dictDataTemp1=arrTempData[w];
                
                NSArray *arrTempData1=[dictDataTemp1 valueForKey:@"DateWise"];
                
                for (int r=0; r<arrTempData1.count; r++) {
                    
                    NSDictionary *dictDataTemp2=arrTempData1[r];
                    
                    [arrTempDataall addObject:dictDataTemp2];
                    
                }
            }
        }
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return arrTempDataall;
}

-(NSMutableArray*)fetchTempEmployeeTimeSheetDataFromDbIfMainTableIsNull :(NSString*)strWorkOrderNew :(NSString*)strSubWorkOrderNew{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTempSubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"TempSubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    requestTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setEntity:entityTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderNew,strSubWorkOrderNew];
    
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:YES];
    sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setSortDescriptors:sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTempSubWoEmployeeWorkingTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs performFetch:&error];
    arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs = [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs fetchedObjects];
    
    if ([arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs count] == 0)
    {
        
        
    }
    else
    {
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs;
}

-(void)createDynamicEmpSheet :(NSArray*)arrOfEmpSheet :(NSArray*)arrOfHeaderTitle{
    
    NSMutableArray *arrHeight=[[NSMutableArray alloc]init];
    
    for(UIView *view in scrollViewEmpTimeSheet.subviews)
    {
        [view removeFromSuperview];
    }
    
    arrOfHeaderTitle=arrOfHeaderTitleForSlots;
    //    arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
    //    [arrOfGlobalDynamicEmpSheetFinal addObjectsFromArray:arrOfEmpSheet];
    
    CGFloat scrollViewHeight=0.0;
    CGFloat scrollViewWidth=0.0;
    
    scrollViewHeight=arrOfGlobalDynamicEmpSheetFinal.count*60+60;
    scrollViewWidth=arrOfHeaderTitle.count*100+250;
    
    //scrollViewEmpTimeSheet=[[UIScrollView alloc]initWithFrame:CGRectMake(scrollViewEmpTimeSheet.frame.origin.x, scrollViewEmpTimeSheet.frame.origin.y, scrollViewWidth, scrollViewHeight)];
    
    UIView *viewEmpSheetOnScroll=[[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollViewWidth, scrollViewEmpTimeSheet.frame.size.height)];
    
    CGFloat xAxisHeaderTitle=210;
    CGFloat yAxisMain=0;
    for (int j=0; j<arrOfGlobalDynamicEmpSheetFinal.count; j++) {
        
        xAxisHeaderTitle=10;
        
        [arrHeight addObject:@"1"];
        
        NSDictionary *dictDataEmp=arrOfGlobalDynamicEmpSheetFinal[j];
        
        UILabel *lblTitleEmployeeName=[[UILabel alloc]init];
        lblTitleEmployeeName.backgroundColor=[UIColor clearColor];
        //lblTitleEmployeeName.layer.borderWidth = 1.5f;
        //lblTitleEmployeeName.layer.cornerRadius = 4.0f;
        lblTitleEmployeeName.layer.borderColor = [[UIColor grayColor] CGColor];
        lblTitleEmployeeName.frame=CGRectMake(10, yAxisMain, [UIScreen mainScreen].bounds.size.width-10, 50);
        lblTitleEmployeeName.font=[UIFont boldSystemFontOfSize:22];
        //lblTitleEmployeeName.text=[NSString stringWithFormat:@"%@",[dictDataEmp valueForKey:@"EmployeeName"]];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[dictDataEmp valueForKey:@"EmployeeName"]]];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        lblTitleEmployeeName.attributedText=attributeString;
        
        lblTitleEmployeeName.textAlignment=NSTextAlignmentLeft;
        lblTitleEmployeeName.numberOfLines=700;
        [lblTitleEmployeeName setAdjustsFontSizeToFitWidth:YES];
        [viewEmpSheetOnScroll addSubview:lblTitleEmployeeName];
        
        NSArray *arrDataDateWise=[dictDataEmp valueForKey:@"DateWiseData"];
        
        yAxisMain=yAxisMain;
        
        for (int i=0; i<=arrOfHeaderTitleForSlots.count; i++) {
            
            UILabel *lblTitleHeader=[[UILabel alloc]init];
            lblTitleHeader.backgroundColor=[UIColor lightTextColor];
            lblTitleHeader.layer.borderWidth = 1.5f;
            lblTitleHeader.layer.cornerRadius = 0.0f;
            lblTitleHeader.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            if (i==0) {
                
                lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, yAxisMain+60, 200, 50);
                xAxisHeaderTitle=xAxisHeaderTitle+200;
                
                
            }else{
                
                lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, yAxisMain+60, 100, 50);
                xAxisHeaderTitle=xAxisHeaderTitle+100;
                
                
            }
            //lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, yAxisMain+60, 100, 50);
            lblTitleHeader.font=[UIFont systemFontOfSize:22];
            lblTitleHeader.backgroundColor=[UIColor lightTextColorTimeSheet];
            if (i==0) {
                
                lblTitleHeader.text=[NSString stringWithFormat:@"%@",@""];
                
            } else if(i==1){
                
                lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Regular"];
                
            }else if(i==arrOfHeaderTitleForSlots.count){
                
                lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Holiday"];
                
            }else {
                
                //lblTitleHeader.text=[NSString stringWithFormat:@"%@",arrOfHeaderTitle[i]];
                lblTitleHeader.text=[global slotHeaderTitleFromTimeSlot:arrOfHoursConfig :[NSString stringWithFormat:@"%@",arrOfHeaderTitle[i-1]]];
                
            }
            
            lblTitleHeader.textAlignment=NSTextAlignmentCenter;
            lblTitleHeader.numberOfLines=700;
            [lblTitleHeader setAdjustsFontSizeToFitWidth:YES];
            
            [viewEmpSheetOnScroll addSubview:lblTitleHeader];
            
        }
        
        yAxisMain=yAxisMain;
        
        CGFloat yAxis=yAxisMain+120;
        
        for (int k=0; k<arrDataDateWise.count; k++) {
            
            CGFloat xAxis=210;
            
            scrollViewHeight=yAxis+100;
            
            NSDictionary *dictDataDatee=arrDataDateWise[k];
            
            UILabel *lblTitleWorkingDate=[[UILabel alloc]init];
            lblTitleWorkingDate.backgroundColor=[UIColor clearColor];
            lblTitleWorkingDate.layer.borderWidth = 1.5f;
            lblTitleWorkingDate.layer.cornerRadius = 0.0f;
            lblTitleWorkingDate.layer.borderColor = [[UIColor grayColor] CGColor];
            lblTitleWorkingDate.frame=CGRectMake(10, yAxis, 200, 50);
            lblTitleWorkingDate.font=[UIFont systemFontOfSize:22];
            lblTitleWorkingDate.text=[NSString stringWithFormat:@"%@",[dictDataDatee valueForKey:@"WorkingDate"]];
            lblTitleWorkingDate.text=[global changeDtaeEmpTimeSheet:[NSString stringWithFormat:@"%@",[dictDataDatee valueForKey:@"WorkingDate"]]];
            lblTitleWorkingDate.textAlignment=NSTextAlignmentCenter;
            lblTitleWorkingDate.numberOfLines=700;
            [lblTitleWorkingDate setAdjustsFontSizeToFitWidth:YES];
            lblTitleWorkingDate.backgroundColor=[UIColor lightTextColorTimeSheet];
            
            [viewEmpSheetOnScroll addSubview:lblTitleWorkingDate];
            
            NSArray *arrEmpData=[dictDataDatee valueForKey:@"DateWise"];
            
            
            NSString *strWorkingDate=[self ChangeDateToLocalDateMechanicale:[NSString stringWithFormat:@"%@",[dictDataDatee valueForKey:@"WorkingDate"]]];
            
            BOOL chkForEdit=[self funPayPeriod:strWorkingDate];
            
            for (int k1=0; k1<arrEmpData.count; k1++) {
                [arrHeight addObject:@"1"];
                NSDictionary *dictDataaaa=arrEmpData[k1];
                CGRect txtViewFrame = CGRectMake(xAxis, yAxis,100,50);
                UITextField *txtView= [[UITextField alloc] init];
                //txtView.text = [self getHrAndMins:[dictDataaaa valueForKey:@"actualDurationInMin"]];
                txtView.text = [global getHrAndMinsGlobal:[dictDataaaa valueForKey:@"billableDurationInMin"]];
                
                if ([txtView.text isEqualToString:@"00:00"]) {
                    
                    txtView.text =@"";
                    txtView.placeholder=@"00:00";
                    
                }
                
                txtView.frame =txtViewFrame;
                txtView.layer.borderWidth = 1.5f;
                txtView.layer.cornerRadius = 0.0f;
                txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                [txtView setBackgroundColor:[UIColor clearColor]];
                txtView.font=[UIFont systemFontOfSize:20];
                txtView.delegate=self;
                
                UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                leftView.backgroundColor = [UIColor clearColor];
                txtView.leftView = leftView;
                txtView.leftViewMode = UITextFieldViewModeAlways;
                txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                
                txtView.tag=k*1000+k1+500;
                
                if (isCompletedStatusMechanical) {
                    
                    txtView.enabled=false;
                    
                } else {
                    
                    //txtView.enabled=true;
                    
                    //                    NSString *strWorkingDate=[self ChangeDateToLocalDateMechanicale:[NSString stringWithFormat:@"%@",[dictDataaaa valueForKey:@"workingDate"]]];
                    //
                    //                    BOOL chkForEdit=[self funPayPeriod:strWorkingDate];
                    
                    if(chkForEdit)
                    {
                        txtView.enabled=YES;
                    }
                    else
                    {
                        txtView.enabled=NO;
                        //txtView.layer.borderColor = [[UIColor redColor] CGColor];
                    }
                    
                }
                
                NSString *strTags=[NSString stringWithFormat:@"%d,%d,%d",j,k,k1];
                //UIColor *color = [UIColor clearColor];
                //txtView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:strTags attributes:@{NSForegroundColorAttributeName: color}];
                
                // Changes For Tags
                
                txtView.leftView.accessibilityHint=strTags;
                txtView.leftView.hidden=YES;
                
                [viewEmpSheetOnScroll addSubview:txtView];
                
                xAxis=xAxis+100;
                
            }
            
            
            yAxis=yAxis+50;
            
        }
        
        yAxisMain=yAxis;
    }
    
    scrollViewEmpTimeSheet=[[UIScrollView alloc]initWithFrame:CGRectMake(scrollViewEmpTimeSheet.frame.origin.x, scrollViewEmpTimeSheet.frame.origin.y, [UIScreen mainScreen].bounds.size.width, scrollViewHeight)];
    
    viewEmpSheetOnScroll.frame=CGRectMake(viewEmpSheetOnScroll.frame.origin.x, viewEmpSheetOnScroll.frame.origin.y, [UIScreen mainScreen].bounds.size.width, scrollViewHeight);
    
    [viewEmpSheetOnScroll setFrame:CGRectMake(viewEmpSheetOnScroll.frame.origin.x, viewEmpSheetOnScroll.frame.origin.y, [UIScreen mainScreen].bounds.size.width, scrollViewHeight)];
    
    [scrollViewEmpTimeSheet addSubview:viewEmpSheetOnScroll];
    
    _view_EmployeeTimeSheet.frame=CGRectMake(_view_EmployeeTimeSheet.frame.origin.x, _view_EmployeeTimeSheet.frame.origin.y, [UIScreen mainScreen].bounds.size.width, scrollViewHeight);
    
    [_view_EmployeeTimeSheet setFrame:CGRectMake(_view_EmployeeTimeSheet.frame.origin.x, _view_EmployeeTimeSheet.frame.origin.y, [UIScreen mainScreen].bounds.size.width, scrollViewHeight)];
    
    employeeSheetHeight=_view_EmployeeTimeSheet.frame.size.height;
    
    [_view_EmployeeTimeSheet addSubview:scrollViewEmpTimeSheet];
    
    //viewEmpSheetOnScroll.backgroundColor=[UIColor greenColor];
    //_view_EmployeeTimeSheet.backgroundColor=[UIColor redColor];
    
    [scrollViewEmpTimeSheet setContentSize:CGSizeMake(scrollViewWidth,scrollViewHeight)];
    
    scrollViewEmpTimeSheet.scrollEnabled=true;
    scrollViewEmpTimeSheet.showsHorizontalScrollIndicator=true;
    scrollViewEmpTimeSheet.showsVerticalScrollIndicator=true;
    
    [self getCompletedSubWOCompleteTimeExtSerDcs :@"add"];
    
}

-(void)getCompletedSubWOCompleteTimeExtSerDcs :(NSString*)strType{
    
    for(UIView *view in _view_DynamicCompleteEmpTime.subviews)
    {
        [view removeFromSuperview];
    }
    
    strGlobalCompleteTimeEmpSheet=@"0";
    strGlobalCompleteAmountEmpSheet=@"0";
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *dictDataRegular=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictDataHoliday=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictDataOthers=[[NSMutableDictionary alloc]init];
    
    for (int j=0; j<arrOfGlobalDynamicEmpSheetFinal.count; j++) {
        
        NSDictionary *dictDataTemp=arrOfGlobalDynamicEmpSheetFinal[j];
        
        NSArray *arrTemp=[dictDataTemp valueForKey:@"DateWiseData"];
        
        for (int k=0; k<arrTemp.count; k++) {
            
            NSDictionary *dictDataTemp1=arrTemp[k];
            
            NSArray *arrTemp1=[dictDataTemp1 valueForKey:@"DateWise"];
            
            for (int p=0; p<arrTemp1.count; p++) {
                
                NSDictionary *dictDataTemp2=arrTemp1[p];
                
                NSString *strWorkingTypeComing=[dictDataTemp2 valueForKey:@"workingType"];
                
                if ([strWorkingTypeComing isEqualToString:@"1"]) {
                    
                    if (dictDataRegular.count>1) {
                        
                        NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]];
                        NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]];
                        NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]];
                        NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]];
                        
                        NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataRegular valueForKey:@"actualAmt"]];
                        NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataRegular valueForKey:@"actualDurationInMin"]];
                        NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataRegular valueForKey:@"billableAmt"]];
                        NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataRegular valueForKey:@"billableDurationInMin"]];
                        
                        float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                        float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                        float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                        float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                        
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdByDevice"]] forKey:@"createdByDevice"];
                        
                        
                        
                    } else {
                        
                        NSString *strDateLocal =[global strCurrentDate];
                        
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]] forKey:@"actualDurationInMin"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]] forKey:@"billableAmt"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]] forKey:@"billableDurationInMin"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdBy"]] forKey:@"createdBy"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"createdDate"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",@"true"] forKey:@"isActive"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"modifiedBy"]] forKey:@"modifiedBy"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"modifiedDate"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[global getReferenceNumber]] forKey:@"subWOCompleteTimeId"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"subWorkOrderId"]] forKey:@"subWorkOrderId"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"timeSlot"]] forKey:@"timeSlot"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workingType"]] forKey:@"workingType"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workorderId"]] forKey:@"workorderId"];
                        [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdByDevice"]] forKey:@"createdByDevice"];
                        
                        
                    }
                }else if ([strWorkingTypeComing isEqualToString:@"3"]){
                    
                    if (dictDataHoliday.count>1) {
                        
                        NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]];
                        NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]];
                        NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]];
                        NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]];
                        
                        NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataHoliday valueForKey:@"actualAmt"]];
                        NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataHoliday valueForKey:@"actualDurationInMin"]];
                        NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataHoliday valueForKey:@"billableAmt"]];
                        NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataHoliday valueForKey:@"billableDurationInMin"]];
                        
                        float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                        float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                        float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                        float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                        
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdByDevice"]] forKey:@"createdByDevice"];
                        
                        
                        
                    } else {
                        
                        NSString *strDateLocal =[global strCurrentDate];
                        
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]] forKey:@"actualDurationInMin"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]] forKey:@"billableAmt"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]] forKey:@"billableDurationInMin"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdBy"]] forKey:@"createdBy"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"createdDate"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",@"true"] forKey:@"isActive"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"modifiedBy"]] forKey:@"modifiedBy"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"modifiedDate"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[global getReferenceNumber]] forKey:@"subWOCompleteTimeId"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"subWorkOrderId"]] forKey:@"subWorkOrderId"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"timeSlot"]] forKey:@"timeSlot"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workingType"]] forKey:@"workingType"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workorderId"]] forKey:@"workorderId"];
                        [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdByDevice"]] forKey:@"createdByDevice"];
                        
                    }
                }
            }
            
        }
    }
    
    if (dictDataRegular.count>1) {
        
        [arrTemp addObject:dictDataRegular];
        
    }
    
    
    
    //
    
    for (int i=0; i<arrOfHeaderTitleForSlots.count; i++) {
        
        dictDataOthers = [[NSMutableDictionary alloc]init];
        
        NSString *strHeaderName=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[i]];
        
        for (int j=0; j<arrOfGlobalDynamicEmpSheetFinal.count; j++) {
            
            NSDictionary *dictDataTemp=arrOfGlobalDynamicEmpSheetFinal[j];
            
            NSArray *arrTemp=[dictDataTemp valueForKey:@"DateWiseData"];
            
            for (int k=0; k<arrTemp.count; k++) {
                
                NSDictionary *dictDataTemp1=arrTemp[k];
                
                NSArray *arrTemp1=[dictDataTemp1 valueForKey:@"DateWise"];
                
                for (int p=0; p<arrTemp1.count; p++) {
                    
                    NSDictionary *dictDataTemp2=arrTemp1[p];
                    
                    //NSString *strTitleComing=[dictDataTemp2 valueForKey:@"timeSlotTitle"];
                    
                    //NSString *strtimeSlotTitleEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlot"]];
                    NSString *strTitleComing=[dictDataTemp2 valueForKey:@"timeSlotTitle"];
                    NSString *strTitleComingNew=[dictDataTemp2 valueForKey:@"timeSlot"];
                    BOOL isMatchedTitle = [global isSlotMatched:strTitleComing :strTitleComingNew :strHeaderName];
                    
                    NSString *strWorkingTypeComing=[dictDataTemp2 valueForKey:@"workingType"];
                    
                    if ([strWorkingTypeComing isEqualToString:@"2"]) {
                        
                        if (isMatchedTitle) {
                            
                            if (dictDataOthers.count>1) {
                                
                                NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]];
                                NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]];
                                NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]];
                                NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]];
                                
                                NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataOthers valueForKey:@"actualAmt"]];
                                NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataOthers valueForKey:@"actualDurationInMin"]];
                                NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataOthers valueForKey:@"billableAmt"]];
                                NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataOthers valueForKey:@"billableDurationInMin"]];
                                
                                float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                                float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                                float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                                float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                                
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdByDevice"]] forKey:@"createdByDevice"];
                                
                                
                            } else {
                                
                                NSString *strDateLocal =[global strCurrentDate];
                                
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]] forKey:@"actualDurationInMin"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]] forKey:@"billableAmt"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]] forKey:@"billableDurationInMin"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdBy"]] forKey:@"createdBy"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"createdDate"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",@"true"] forKey:@"isActive"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"modifiedBy"]] forKey:@"modifiedBy"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"modifiedDate"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[global getReferenceNumber]] forKey:@"subWOCompleteTimeId"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"subWorkOrderId"]] forKey:@"subWorkOrderId"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"timeSlot"]] forKey:@"timeSlot"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workingType"]] forKey:@"workingType"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workorderId"]] forKey:@"workorderId"];
                                [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdByDevice"]] forKey:@"createdByDevice"];
                                
                            }
                        }
                    }
                    
                }
                
            }
        }
        
        if (dictDataOthers.count>1) {
            
            [arrTemp addObject:dictDataOthers];
            
        }
        
    }
    
    if (dictDataHoliday.count>1) {
        
        [arrTemp addObject:dictDataHoliday];
        
    }
    
    arrOfGlobalSubWOCompleteTimeExtSerDcs=[[NSMutableArray alloc]init];
    
    [arrOfGlobalSubWOCompleteTimeExtSerDcs addObjectsFromArray:arrTemp];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:arrTemp])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:arrTemp options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Completed Employee Sheet Data jason: %@", jsonString);
        }
    }
    
    
    if ([strWoType isEqualToString:@"TM"])
    {
        
        //_const_ViewHours_H.constant=arrOfGlobalSubWOCompleteTimeExtSerDcs.count*60;
        
        [_viewPaymentnNotes setFrame:CGRectMake(_viewPaymentnNotes.frame.origin.x, _viewPaymentnNotes.frame.origin.y, _viewPaymentnNotes.frame.size.width, 160)];
        
        CGFloat yAxisMain=0.0;
        
        NSMutableArray *arrTempCountToAddViewHeightOfViewDynamicCompleteEmpTime=[[NSMutableArray alloc]init];
        
        for (int l=0; l<arrOfGlobalSubWOCompleteTimeExtSerDcs.count; l++) {
            
            NSDictionary *dictDataCompleteTime=arrOfGlobalSubWOCompleteTimeExtSerDcs[l];
            
            BOOL isValuePresent;
            
            isValuePresent  = YES;
            
            if ([strType isEqualToString:@"add"]) {
                
                UILabel *lblTitle=[[UILabel alloc]init];
                lblTitle.backgroundColor=[UIColor clearColor];
                ////lblTitleEmployeeName.layer.borderWidth = 1.5f;
                //lblTitleEmployeeName.layer.cornerRadius = 4.0f;
                //lblTitleEmployeeName.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                lblTitle.frame=CGRectMake(20, yAxisMain, 500, 50);
                lblTitle.font=[UIFont boldSystemFontOfSize:22];
                
                NSString *strType=[NSString stringWithFormat:@"%@",[dictDataCompleteTime valueForKey:@"workingType"]];
                if ([strType isEqualToString:@"1"]) {
                    
                    lblTitle.text=[NSString stringWithFormat:@"%@ : %@",@"Regular Hours",[self getHrAndMins:[dictDataCompleteTime valueForKey:@"billableDurationInMin"]]];
                    
                } else if ([strType isEqualToString:@"3"]){
                    
                    lblTitle.text=[NSString stringWithFormat:@"%@ : %@",@"Holiday Hours",[self getHrAndMins:[dictDataCompleteTime valueForKey:@"billableDurationInMin"]]];
                    
                } else{
                    
                    NSString *strTitleToShow = [global slotHeaderTitleFromTimeSlot:arrOfHoursConfig :[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[l]]];
                    
                    lblTitle.text=[NSString stringWithFormat:@"%@ Hours : %@",strTitleToShow,[self getHrAndMins:[dictDataCompleteTime valueForKey:@"billableDurationInMin"]]];
                    
                }
                
                lblTitle.textAlignment=NSTextAlignmentLeft;
                lblTitle.numberOfLines=700;
                [lblTitle setAdjustsFontSizeToFitWidth:YES];
                
                
                
                UILabel *lblbillableAmt=[[UILabel alloc]init];
                lblbillableAmt.backgroundColor=[UIColor clearColor];
                ////lblTitleEmployeeName.layer.borderWidth = 1.5f;
                //lblTitleEmployeeName.layer.cornerRadius = 4.0f;
                //lblTitleEmployeeName.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                lblbillableAmt.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-220, yAxisMain, 200, 50);
                lblbillableAmt.font=[UIFont systemFontOfSize:22];
                
                NSString *strAmountTemp=[NSString stringWithFormat:@"%@",[dictDataCompleteTime valueForKey:@"billableAmt"]];
                
                CGFloat amountBillableTemp=[strAmountTemp floatValue];
                
                // rounding off values to show
                
                amountBillableTemp = ceil(amountBillableTemp);
                
                lblbillableAmt.text=[NSString stringWithFormat:@"$%.02f",amountBillableTemp];
                lblbillableAmt.text=[NSString stringWithFormat:@"$%.02f",[strAmountTemp floatValue]];
                
                lblbillableAmt.textAlignment=NSTextAlignmentRight;
                lblbillableAmt.numberOfLines=700;
                [lblbillableAmt setAdjustsFontSizeToFitWidth:YES];
                
                if (amountBillableTemp>0) {
                    
                    isValuePresent = YES;
                    [arrTempCountToAddViewHeightOfViewDynamicCompleteEmpTime addObject:@"1"];
                    
                }else{
                    
                    isValuePresent = NO;
                    
                }
                
                if (isValuePresent) {
                    
                    [_view_DynamicCompleteEmpTime addSubview:lblTitle];
                    [_view_DynamicCompleteEmpTime addSubview:lblbillableAmt];
                    
                }
                
                
            } else {
                
                
            }
            if (isValuePresent) {
                
                yAxisMain=yAxisMain+60;
                
            }
            
            CGFloat billableDurationAlreadyPresent=[strGlobalCompleteTimeEmpSheet floatValue];
            CGFloat billableAmtAlreadyPresent=[strGlobalCompleteAmountEmpSheet floatValue];
            
            CGFloat billableDuration=[[dictDataCompleteTime valueForKey:@"billableDurationInMin"] floatValue];
            CGFloat billableAmt=[[dictDataCompleteTime valueForKey:@"billableAmt"] floatValue];
            
            CGFloat totalCompleteBillableDuration=billableDurationAlreadyPresent+billableDuration;
            CGFloat totalCompleteBillableAmt=billableAmtAlreadyPresent+billableAmt;
            
            strGlobalCompleteTimeEmpSheet=[NSString stringWithFormat:@"%f",totalCompleteBillableDuration];
            strGlobalCompleteAmountEmpSheet=[NSString stringWithFormat:@"%f",totalCompleteBillableAmt];
            
        }
        
        _const_ViewHours_H.constant=arrTempCountToAddViewHeightOfViewDynamicCompleteEmpTime.count*60;
        
    }
    
    [self setValueOnBillableHours];
    
}


-(void)calculateValuesOnEditingEmpTime{
    
    strGlobalCompleteTimeEmpSheet=@"0";
    strGlobalCompleteAmountEmpSheet=@"0";
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *dictDataRegular=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictDataHoliday=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictDataOthers=[[NSMutableDictionary alloc]init];
    
    for (int j=0; j<arrOfGlobalDynamicEmpSheetFinal.count; j++) {
        
        NSDictionary *dictDataTemp=arrOfGlobalDynamicEmpSheetFinal[j];
        
        NSArray *arrTemp=[dictDataTemp valueForKey:@"DateWiseData"];
        
        for (int k=0; k<arrTemp.count; k++) {
            
            NSDictionary *dictDataTemp1=arrTemp[k];
            
            NSArray *arrTemp1=[dictDataTemp1 valueForKey:@"DateWise"];
            
            NSDictionary *dictDataTemp2=arrTemp1[k];
            
            NSString *strWorkingTypeComing=[dictDataTemp2 valueForKey:@"workingType"];
            
            if ([strWorkingTypeComing isEqualToString:@"1"]) {
                
                if (dictDataRegular.count>1) {
                    
                    NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]];
                    NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]];
                    NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]];
                    NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]];
                    
                    NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataRegular valueForKey:@"actualAmt"]];
                    NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataRegular valueForKey:@"actualDurationInMin"]];
                    NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataRegular valueForKey:@"billableAmt"]];
                    NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataRegular valueForKey:@"billableDurationInMin"]];
                    
                    float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                    float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                    float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                    float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                    
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                    
                    
                    
                } else {
                    
                    NSString *strDateLocal =[global strCurrentDate];
                    
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]] forKey:@"actualDurationInMin"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]] forKey:@"billableAmt"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]] forKey:@"billableDurationInMin"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdBy"]] forKey:@"createdBy"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"createdDate"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",@"true"] forKey:@"isActive"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"modifiedBy"]] forKey:@"modifiedBy"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"modifiedDate"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[global getReferenceNumber]] forKey:@"subWOCompleteTimeId"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"subWorkOrderId"]] forKey:@"subWorkOrderId"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"timeSlot"]] forKey:@"timeSlot"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workingType"]] forKey:@"workingType"];
                    [dictDataRegular setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workorderId"]] forKey:@"workorderId"];
                    
                }
            }else if ([strWorkingTypeComing isEqualToString:@"3"]){
                
                if (dictDataHoliday.count>1) {
                    
                    NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]];
                    NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]];
                    NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]];
                    NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]];
                    
                    NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataHoliday valueForKey:@"actualAmt"]];
                    NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataHoliday valueForKey:@"actualDurationInMin"]];
                    NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataHoliday valueForKey:@"billableAmt"]];
                    NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataHoliday valueForKey:@"billableDurationInMin"]];
                    
                    float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                    float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                    float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                    float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                    
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                    
                    
                    
                } else {
                    
                    NSString *strDateLocal =[global strCurrentDate];
                    
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]] forKey:@"actualDurationInMin"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]] forKey:@"billableAmt"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]] forKey:@"billableDurationInMin"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdBy"]] forKey:@"createdBy"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"createdDate"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",@"true"] forKey:@"isActive"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"modifiedBy"]] forKey:@"modifiedBy"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"modifiedDate"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[global getReferenceNumber]] forKey:@"subWOCompleteTimeId"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"subWorkOrderId"]] forKey:@"subWorkOrderId"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"timeSlot"]] forKey:@"timeSlot"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workingType"]] forKey:@"workingType"];
                    [dictDataHoliday setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workorderId"]] forKey:@"workorderId"];
                    
                }
            }
        }
    }
    
    if (dictDataRegular.count>1) {
        
        [arrTemp addObject:dictDataRegular];
        
    }
    
    
    
    //
    
    for (int i=0; i<arrOfHeaderTitleForSlots.count; i++) {
        
        dictDataOthers = [[NSMutableDictionary alloc]init];
        
        NSString *strHeaderName=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[i]];
        
        for (int j=0; j<arrOfGlobalDynamicEmpSheetFinal.count; j++) {
            
            NSDictionary *dictDataTemp=arrOfGlobalDynamicEmpSheetFinal[j];
            
            NSArray *arrTemp=[dictDataTemp valueForKey:@"DateWiseData"];
            
            for (int k=0; k<arrTemp.count; k++) {
                
                NSDictionary *dictDataTemp1=arrTemp[k];
                
                NSArray *arrTemp1=[dictDataTemp1 valueForKey:@"DateWise"];
                
                NSDictionary *dictDataTemp2=arrTemp1[k];
                
                //NSString *strTitleComing=[dictDataTemp2 valueForKey:@"timeSlotTitle"];
                NSString *strTitleComing=[dictDataTemp2 valueForKey:@"timeSlot"];
                NSString *strTitleComingNew=[dictDataTemp2 valueForKey:@"timeSlot"];
                BOOL isMatchedTitle = [global isSlotMatched:strTitleComing :strTitleComingNew :strHeaderName];
                
                NSString *strWorkingTypeComing=[dictDataTemp2 valueForKey:@"workingType"];
                
                if ([strWorkingTypeComing isEqualToString:@"2"]) {
                    
                    if (isMatchedTitle) {
                        
                        if (dictDataOthers.count>1) {
                            
                            NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]];
                            
                            NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataOthers valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataOthers valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataOthers valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictDataOthers valueForKey:@"billableDurationInMin"]];
                            
                            float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                            float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                            float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                            float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                            
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                            
                            
                        } else {
                            
                            NSString *strDateLocal =[global strCurrentDate];
                            
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualDurationInMin"]] forKey:@"actualDurationInMin"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableAmt"]] forKey:@"billableAmt"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"billableDurationInMin"]] forKey:@"billableDurationInMin"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"createdBy"]] forKey:@"createdBy"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"createdDate"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",@"true"] forKey:@"isActive"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"modifiedBy"]] forKey:@"modifiedBy"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",strDateLocal] forKey:@"modifiedDate"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[global getReferenceNumber]] forKey:@"subWOCompleteTimeId"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"subWorkOrderId"]] forKey:@"subWorkOrderId"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"timeSlot"]] forKey:@"timeSlot"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"actualAmt"]] forKey:@"actualAmt"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workingType"]] forKey:@"workingType"];
                            [dictDataOthers setObject:[NSString stringWithFormat:@"%@",[dictDataTemp2 valueForKey:@"workorderId"]] forKey:@"workorderId"];
                            
                        }
                    }
                }
            }
        }
        
        if (dictDataOthers.count>1) {
            
            [arrTemp addObject:dictDataOthers];
            
        }
        
    }
    
    if (dictDataHoliday.count>1) {
        
        [arrTemp addObject:dictDataHoliday];
        
    }
    
    arrOfGlobalSubWOCompleteTimeExtSerDcs=[[NSMutableArray alloc]init];
    
    [arrOfGlobalSubWOCompleteTimeExtSerDcs addObjectsFromArray:arrTemp];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:arrTemp])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:arrTemp options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Completed Employee Sheet Data jason: %@", jsonString);
        }
    }
    
    for (int l=0; l<arrOfGlobalSubWOCompleteTimeExtSerDcs.count; l++) {
        
        NSDictionary *dictDataCompleteTime=arrOfGlobalSubWOCompleteTimeExtSerDcs[l];
        
        CGFloat billableDurationAlreadyPresent=[strGlobalCompleteTimeEmpSheet floatValue];
        CGFloat billableTimeAlreadyPresent=[strGlobalCompleteAmountEmpSheet floatValue];
        
        CGFloat billableDuration=[[dictDataCompleteTime valueForKey:@"billableDurationInMin"] floatValue];
        CGFloat billableTime=[[dictDataCompleteTime valueForKey:@"billableAmt"] floatValue];
        
        CGFloat totalCompleteBillableDuration=billableDurationAlreadyPresent+billableDuration;
        CGFloat totalCompleteBillableTime=billableTimeAlreadyPresent+billableTime;
        
        strGlobalCompleteTimeEmpSheet=[NSString stringWithFormat:@"%f",totalCompleteBillableDuration];
        strGlobalCompleteAmountEmpSheet=[NSString stringWithFormat:@"%f",totalCompleteBillableTime];
        
    }
    
    [self setValueOnBillableHours];
    
}


-(void)setValueOnBillableHours{
    
    CGFloat amountBillableHours=[strGlobalCompleteAmountEmpSheet floatValue];
    
    float minutesToSend=[strGlobalCompleteTimeEmpSheet floatValue];
    
    if (minutesToSend>0) {
        
        [_txtIncDecActualHours setHidden:NO];
        [_lblTxtIncDec setHidden:NO];
        
    } else {
        
        [_txtIncDecActualHours setHidden:YES];
        [_lblTxtIncDec setHidden:YES];
        
    }
    
    _txtIncDecActualHours.text=[NSString stringWithFormat:@"%@",[self getHrAndMins:[NSString stringWithFormat:@"%f",minutesToSend]]];
    
    _lblBillableHours.text=[NSString stringWithFormat:@"$%.02f",amountBillableHours];
    //_lblBillableHours.text=[NSString stringWithFormat:@"$%f",amountBillableHours];
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
    
}

-(NSString*)getHrAndMins:(NSString*)strMins
{
    int seconds = (int)[strMins integerValue]*60;
    int minutes = (seconds / 60) % 60;
    int hours = seconds / 3600;
    return [NSString stringWithFormat:@"%02d:%02d",hours,minutes];
    
}


- (IBAction)action_collapseEmployeeSheetView:(id)sender {
    
    if ([_btnCollapseExpandEmpSheet.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnCollapseExpandEmpSheet setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        isExpandEmpSheet=YES;
        isUpdateViewsFrame = YES;
        [self addView:@"Notadd"];
    } else {
        //Band Hoga
        [_btnCollapseExpandEmpSheet setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        isExpandEmpSheet=NO;
        isUpdateViewsFrame = YES;
        [self addView:@"Notadd"];
    }
    
}

-(void)deleteSubWoEmployeeWorkingTimeExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete SubWoEmployeeWorkingTimeExtSerDcs Data//SubWoEmployeeWorkingTimeExtSerDcs
    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",_strSubWorkOderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

// TempSubWoEmployeeWorkingTimeExtSerDcs
-(void)deleteTempSubWoEmployeeWorkingTimeExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete SubWoEmployeeWorkingTimeExtSerDcs Data//SubWoEmployeeWorkingTimeExtSerDcs
    entityTempSubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"TempSubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"TempSubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",_strSubWorkOderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteSubWOCompleteTimeExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete SubWoEmployeeWorkingTimeExtSerDcs Data//SubWoEmployeeWorkingTimeExtSerDcs
    entitySubWOCompleteTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWOCompleteTimeExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"SubWOCompleteTimeExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorlOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


-(void)saveSubWOCompleteTimeExtSerDcs{
    
    [self deleteSubWOCompleteTimeExtSerDcs];
    
    for (int k=0; k<arrOfGlobalSubWOCompleteTimeExtSerDcs.count; k++) {
        
        entitySubWOCompleteTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWOCompleteTimeExtSerDcs" inManagedObjectContext:context];
        
        SubWOCompleteTimeExtSerDcs *objSubWOCompleteTimeExtSerDcs = [[SubWOCompleteTimeExtSerDcs alloc]initWithEntity:entitySubWOCompleteTimeExtSerDcs insertIntoManagedObjectContext:context];
        
        NSDictionary *dictOfSubWOCompleteTimeExtSerDcs =arrOfGlobalSubWOCompleteTimeExtSerDcs[k];
        
        objSubWOCompleteTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"actualAmt"]];
        
        int tempActualMin=[[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"actualDurationInMin"]] intValue];
        
        int tempBillableMin=[[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"billableDurationInMin"]] intValue];
        
        if ((tempActualMin>0) || (tempBillableMin>0)) {
            
            objSubWOCompleteTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%d",tempActualMin];
            objSubWOCompleteTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"billableAmt"]];
            objSubWOCompleteTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%d",tempBillableMin];
            objSubWOCompleteTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"createdBy"]];
            objSubWOCompleteTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"createdByDevice"]];
            objSubWOCompleteTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"createdDate"]];
            objSubWOCompleteTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"isActive"]];
            objSubWOCompleteTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"modifiedBy"]];
            objSubWOCompleteTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"modifiedDate"]];
            objSubWOCompleteTimeExtSerDcs.subWOCompleteTimeId=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"subWOCompleteTimeId"]];
            objSubWOCompleteTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"subWorkOrderId"]];
            objSubWOCompleteTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"timeSlot"]];
            objSubWOCompleteTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"workingType"]];
            objSubWOCompleteTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"workorderId"]];
            
            NSError *errorSubWOCompleteTimeExtSerDcs;
            [context save:&errorSubWOCompleteTimeExtSerDcs];
            
        } else {
            
            
        }
        
    }
}

-(void)saveEmployeeTimeSheetSlotWise :(NSArray*)arrEmpSlots{
    
    [self deleteSubWoEmployeeWorkingTimeExtSerDcs];
    [self deleteTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    for (int k=0; k<arrEmpSlots.count; k++) {
        
        NSDictionary *dictData=arrEmpSlots[k];
        
        NSArray *arrTempp=[dictData valueForKey:@"DateWiseData"];
        
        for (int j=0; j<arrTempp.count; j++) {
            
            NSDictionary *dictDataTemp=arrTempp[j];
            
            NSArray *arrTempNew=[dictDataTemp valueForKey:@"DateWise"];
            
            for (int i=0; i<arrTempNew.count; i++) {
                
                NSDictionary *dictDataaa=arrTempNew[i];
                
                NSString *strBillableAmt=[dictDataaa valueForKey:@"billableAmt"];
                NSString *strActualAmt=[dictDataaa valueForKey:@"actualAmt"];
                NSString *strBillableDuration=[dictDataaa valueForKey:@"billableDurationInMin"];
                NSString *strActualDuration=[dictDataaa valueForKey:@"actualDurationInMin"];
                
                float BillAmount=[strBillableAmt floatValue];
                float ActualAmount=[strActualAmt floatValue];
                float BillDuration=[strBillableDuration floatValue];
                float ActualDuration=[strActualDuration floatValue];
                
                // commented this condition to save even 0 values and sync
                
                // if ((BillAmount>0) || (ActualAmount>0) || (BillDuration>0) || (ActualDuration>0)) {
                
                entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
                
                SubWoEmployeeWorkingTimeExtSerDcs *objSubWoEmployeeWorkingTimeExtSerDcs = [[SubWoEmployeeWorkingTimeExtSerDcs alloc]initWithEntity:entitySubWoEmployeeWorkingTimeExtSerDcs insertIntoManagedObjectContext:context];
                
                
                NSDictionary *dictOfSubWoEmployeeWorkingTimeExtSerDcs =arrTempNew[i];
                
                
                int tempActualMin=[[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"actualDurationInMin"]] intValue];
                
                int tempBillableMin=[[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"billableDurationInMin"]] intValue];
                
                objSubWoEmployeeWorkingTimeExtSerDcs.subWOEmployeeWorkingTimeId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWOEmployeeWorkingTimeId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workorderId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWorkOrderId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWorkOrderActualHourId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"employeeNo"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.employeeName=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"employeeName"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.workingDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workingDate"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.timeSlotTitle=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"timeSlotTitle"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"timeSlot"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"actualDurationInMin"]];
                //                    objSubWoEmployeeWorkingTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"actualAmt"]];
                //                    objSubWoEmployeeWorkingTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"billableDurationInMin"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%d",tempActualMin];
                objSubWoEmployeeWorkingTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%d",tempBillableMin];
                objSubWoEmployeeWorkingTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"billableAmt"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workingType"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isApprove"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isActive"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdDate"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdBy"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"modifiedDate"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"modifiedBy"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdByDevice"]];
                
                // Change for Labor Type
                
                if ([[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isHelper"]] isEqualToString:@"Yes"]) {
                    
                    objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"True";
                    
                } else {
                    
                    objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"False";
                    
                }
                
                NSError *errorSubWoEmployeeWorkingTimeExtSerDcs;
                [context save:&errorSubWoEmployeeWorkingTimeExtSerDcs];
                
                //  }
            }
        }
    }
    
    [self saveSubWOCompleteTimeExtSerDcs];
}

//Goto Emp Sheet

-(void)goToEmpTimeSheet{
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    EmpTimeSheetViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EmpTimeSheetViewController"];
    objSignViewController.strWorkOrderId=strWorkOrderId;
    objSignViewController.strSubWorkOrderIdGlobal=strSubWorkOrderIdGlobal;
    objSignViewController.arrOfHoursConfig=arrOfHoursConfig;
    
    if (isCompletedStatusMechanical) {
        
        objSignViewController.strWorkOrderStatus=@"Complete";
        
    } else {
        
        objSignViewController.strWorkOrderStatus=@"InComplete";
        
    }
    
    objSignViewController.strAfterHrsDuration=strAfterHrsDuration;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

- (void)createEmpTimeSheetButton
{
    // create a new button
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake((self.view.bounds.size.width - 150),
                                                                 (self.view.bounds.size.height - 200),
                                                                 100, 100)];
    button.backgroundColor=[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1];
    button.layer.cornerRadius = button.frame.size.width/2;
    button.clipsToBounds = YES;
    
    
    // add drag listener
    [button addTarget:self action:@selector(wasDragged:withEvent:)
     forControlEvents:UIControlEventTouchDragInside];
    
    // center and size
    button.frame = CGRectMake((self.view.bounds.size.width - 150),
                              (self.view.bounds.size.height - 200),
                              100, 100);
    
    UIButton *buttonInside = [[UIButton alloc]initWithFrame:CGRectMake((button.frame.size.width-50)/2.0,
                                                                       (button.frame.size.height-50)/2.0,
                                                                       50, 50)];
    [buttonInside setImage:[UIImage imageNamed:@"add_white.png"] forState:UIControlStateNormal];
    buttonInside.layer.cornerRadius = button.frame.size.width/2;
    buttonInside.clipsToBounds = YES;
    [buttonInside addTarget:self action:@selector(goToEmpTimeSheet) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    [button addSubview:buttonInside];
    
}

- (void)wasDragged:(UIButton *)button withEvent:(UIEvent *)event
{
    // get the touch
    UITouch *touch = [[event touchesForView:button] anyObject];
    
    // get delta
    CGPoint previousLocation = [touch previousLocationInView:button];
    CGPoint location = [touch locationInView:button];
    CGFloat delta_x = location.x - previousLocation.x;
    CGFloat delta_y = location.y - previousLocation.y;
    
    // move button
    button.center = CGPointMake(button.center.x + delta_x,
                                button.center.y + delta_y);
}


#pragma mark- Pay Period Check

-(BOOL)funPayPeriod :(NSString *)strWorkingDate
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictMaster=[defs valueForKey:@"MasterAllMechanical"];
    NSDictionary *dictPayPeriodDc=[dictMaster valueForKey:@"PayPeriodDc"];
    
    NSArray *arrPayPeriodScheduleDcs=[dictPayPeriodDc valueForKey:@"PayPeriodScheduleDcs"];
    
    bool chkForEndOfPayPeriod=[[NSString stringWithFormat:@"%@",[dictPayPeriodDc valueForKey:@"IsEndofPayPeriod"]]boolValue];
    
    NSMutableArray *arrFinalPayPeriodDcs,*arrDate;
    arrFinalPayPeriodDcs=[[NSMutableArray alloc]init];
    arrDate=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrCompareDate1,*arrCompareDate2;
    arrCompareDate1=[[NSMutableArray alloc]init];
    arrCompareDate2=[[NSMutableArray alloc]init];
    NSString *strStartDate=@"01/01/2016 23:59:59";
    for(int i=0;i<arrPayPeriodScheduleDcs.count;i++)
    {
        
        NSDictionary *dict=[arrPayPeriodScheduleDcs objectAtIndex:i];
        
        NSString *strPayPeriodDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PayPeriodScheduleDate"]];
        
        strPayPeriodDate=[self ChangeDateToLocalDateMechanicalePayPeriod:strPayPeriodDate];
        NSDateFormatter *dateForMateNew=[[NSDateFormatter alloc]init];
        // [dateForMateNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateForMateNew setTimeZone:[NSTimeZone localTimeZone]];
        [dateForMateNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        NSDate *dateNew=[dateForMateNew dateFromString:strPayPeriodDate];
        [arrFinalPayPeriodDcs addObject:strPayPeriodDate];
        [arrDate addObject:dateNew];
        
        //New Functionality
        
        NSTimeInterval t= -86400;
        NSString *strBeforeHours;
        strBeforeHours=[self updateHours:strPayPeriodDate :t];
        [arrCompareDate2 addObject:strBeforeHours];
        
    }
    NSLog(@" String = %@ Dates = %@",arrFinalPayPeriodDcs,arrDate);
    [arrCompareDate1 addObject:strStartDate];
    [arrCompareDate1 addObjectsFromArray:arrFinalPayPeriodDcs];
    [arrCompareDate1 removeLastObject];
    NSLog(@" Compare Date1  = %@ Compare Date2 = %@",arrCompareDate1,arrCompareDate2);
    
    //Logic 2
    //Find Afte and Before Hours Payperiod Date
    
    
    
    NSString *strMaxDate=[self findMaxDate:arrDate];
    
    NSLog(@"maxDate>>%@",strMaxDate);
    //Find Max Date
    
    NSMutableArray *arrFinalPayPeriodDcsUpdated;
    arrFinalPayPeriodDcsUpdated=[[NSMutableArray alloc]init];
    [arrFinalPayPeriodDcsUpdated addObjectsFromArray:arrFinalPayPeriodDcs];
    
    
    if(chkForEndOfPayPeriod)
    {
        NSLog(@"TRUE");
        return YES;
    }
    else
    {
        NSLog(@"FALSE");
        //Add after hours from max date
        
        NSString *strAfterHours=[NSString stringWithFormat:@"%@",[dictPayPeriodDc valueForKey:@"AfterEndOfPayPeriodHoursForMobile"]];
        
        
        if(strAfterHours.length>0)
        {
            NSLog(@"add after hours");
            for (int i=0; i<arrFinalPayPeriodDcs.count; i++)
            {
                NSNumber *afterHours;
                afterHours=[self dateToSecondConvert:strAfterHours];
                
                strAfterHours=[NSString stringWithFormat:@"%@",afterHours];
                
                NSDictionary *dict=[arrPayPeriodScheduleDcs objectAtIndex:i];
                
                NSString *strPayPeriodDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PayPeriodScheduleDate"]];
                strPayPeriodDate=[self ChangeDateToLocalDateMechanicalePayPeriod:strPayPeriodDate];
                
                strAfterHours=[self updateHours:strPayPeriodDate :[strAfterHours intValue]];
                
                [arrFinalPayPeriodDcsUpdated replaceObjectAtIndex:i withObject:strAfterHours];
                
            }
        }
        //Subtract before hours from max date
        
        NSString *strBeforeHours=[NSString stringWithFormat:@"%@",[dictPayPeriodDc valueForKey:@"BeforeEndOfPayPeriodHoursForMobile"]];
        if(strBeforeHours.length>0)
        {
            NSLog(@"subtract before hours");
            
            for (int i=0; i<arrFinalPayPeriodDcs.count; i++)
            {
                NSNumber *beforeHours;
                beforeHours=[self dateToSecondConvert:strBeforeHours];
                
                beforeHours=@(- beforeHours.doubleValue);
                strBeforeHours=[NSString stringWithFormat:@"%@",beforeHours];
                
                NSDictionary *dict=[arrPayPeriodScheduleDcs objectAtIndex:i];
                
                NSString *strPayPeriodDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PayPeriodScheduleDate"]];
                strPayPeriodDate=[self ChangeDateToLocalDateMechanicalePayPeriod:strPayPeriodDate];
                
                strBeforeHours=[self updateHoursNew :strPayPeriodDate :[strBeforeHours intValue]];
                
                [arrFinalPayPeriodDcsUpdated replaceObjectAtIndex:i withObject:strBeforeHours];
                
                strBeforeHours=[NSString stringWithFormat:@"%@",[dictPayPeriodDc valueForKey:@"BeforeEndOfPayPeriodHoursForMobile"]];
                
                
            }
        }
        BOOL chkForEdit;
        chkForEdit=[self checkForPayPeriodDate:arrCompareDate1 CompareArray:arrCompareDate2 :arrFinalPayPeriodDcsUpdated CurrentDate:[self getCurrentDateTime] WorkingDate:strWorkingDate];
        return chkForEdit;
        //
    }
}

-(NSString*)updateHoursNew: (NSString*)strMaxDate : (NSTimeInterval)secondsInHours
{
    NSDateFormatter *dateForMateNew=[[NSDateFormatter alloc]init];
    [dateForMateNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    [dateForMateNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    NSDate *dateEightHoursAhead = [[dateForMateNew dateFromString:strMaxDate] dateByAddingTimeInterval:secondsInHours];
    
    NSString* newTime = [dateForMateNew stringFromDate:dateEightHoursAhead];
    
    if(newTime==nil)
    {
        dateForMateNew=[[NSDateFormatter alloc]init];
        [dateForMateNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        
        [dateForMateNew setDateFormat:@"MM/dd/yyyy"];
        
        dateEightHoursAhead = [[dateForMateNew dateFromString:strMaxDate] dateByAddingTimeInterval:secondsInHours];
        
        [dateForMateNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        
        newTime = [dateForMateNew stringFromDate:dateEightHoursAhead];
    }
    
    NSDate *dateNew=[dateForMateNew dateFromString:newTime];
    
    NSString* strPayPeriodDate=[dateForMateNew stringFromDate:dateNew];
    
    // NSDate *dateNew=[dateForMateNew dateFromString:newTime];
    return strPayPeriodDate;
}

-(NSDate *)stringToDate:(NSString *)strDate
{
    NSDateFormatter *dateForMateNew=[[NSDateFormatter alloc]init];
    //  [dateForMateNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateForMateNew setTimeZone:[NSTimeZone localTimeZone]];
    [dateForMateNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSDate *dateFromString=[dateForMateNew dateFromString:strDate];
    return dateFromString;
}
-(BOOL)checkForPayPeriodDate: (NSArray*)arrCompareDate1  CompareArray: (NSArray*)arrCompareDate2 : (NSArray*)arrFinalPayPeriodDcsUpdated CurrentDate: (NSString *)strCurrentDate WorkingDate: (NSString*)strWorkingDate
{
    
    
    
    //New Logic
    
    
    
    BOOL chkForEdit;
    
    chkForEdit=YES;
    
    NSString *strCurrentDateOnly;
    
    NSString *strWorkingDateOnly=strWorkingDate;
    
    strCurrentDateOnly=[self ChangeDateToLocalDateMechanicalePayPeriod:strCurrentDate];
    
    
    
    NSDate *workingDate,*currentDateOnly;
    
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    
    [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
    
    //[dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    [dateFormatterNew setDateFormat:@"MM/dd/yyyy"];
    
    workingDate=[dateFormatterNew dateFromString:strWorkingDateOnly];
    
    //currentDateOnly=[dateFormatterNew dateFromString:strCurrentDateOnly];
    
    currentDateOnly=[self stringToDate:strCurrentDateOnly];
    
    
    
    NSString *timeOnly;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    /* [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
     
     [dateFormatter setDateFormat:@"HH:mm:ss"];
     
     timeOnly = [dateFormatter stringFromDate:workingDate];*/
    
    
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss "];
    
    NSDate* newTime = [dateFormatter dateFromString:strCurrentDate];
    
    //dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    timeOnly = [dateFormatter stringFromDate:newTime];
    
    
    
    
    
    
    
    NSDate *workingDateTimeOnly;
    
    workingDateTimeOnly=[self getDateWithTime:strWorkingDate];
    
    
    
    
    
    NSNumber *timeHours;
    
    timeHours=[self dateToSecondConvert:timeOnly];
    
    timeOnly=[NSString stringWithFormat:@"%@",timeHours];
    
    
    
    if([currentDateOnly compare:workingDate]==NSOrderedSame || [currentDateOnly compare:workingDate]==NSOrderedAscending) //   current date < working date current date before working date
        
    {
        
        NSString* newUpdatedTime=[self updateHours:strWorkingDate :[timeOnly intValue]];
        
        workingDateTimeOnly=[self getDateWithTime:newUpdatedTime];
        
    }
    
    else
        
    {
        
        
        
        NSTimeInterval secondsInHours = 23*60*60 + 59*60 + 59;
        
        NSString* newUpdatedTime=[self updateHours:strWorkingDate :secondsInHours];
        
        workingDateTimeOnly=[self getDateWithTime:newUpdatedTime];
        
    }
    
    
    
    
    
    NSString *strFinalDateChosen;
    
    strFinalDateChosen=@"";
    
    for (int i=0;i<arrCompareDate1.count; i++)
        
    {
        
        NSString *strDate1,*strDate2;
        
        
        
        strDate1=[arrCompareDate1 objectAtIndex:i];
        
        strDate2=[arrCompareDate2 objectAtIndex:i];
        
        
        
        
        
        if((([[self stringToDate:strDate1] compare:workingDateTimeOnly]==NSOrderedSame || [[self stringToDate:strDate1] compare:workingDateTimeOnly]==NSOrderedAscending))&&([workingDateTimeOnly compare:[self stringToDate:strDate2]]==NSOrderedSame || [workingDateTimeOnly compare:[self stringToDate:strDate2]]==NSOrderedAscending) )
            
        {
            
            strFinalDateChosen=[arrFinalPayPeriodDcsUpdated objectAtIndex:i];
            
            break;
            
        }
        
        
        
    }
    
    
    
    if([[self stringToDate:strFinalDateChosen] compare:currentDateOnly]==NSOrderedSame || [[self stringToDate:strFinalDateChosen] compare:currentDateOnly]==NSOrderedAscending)
        
    {
        
        if([[self stringToDate:strFinalDateChosen] compare:currentDateOnly]==NSOrderedAscending)
            
        {
            
            NSLog(@"Working");
            
        }
        
        if([[self stringToDate:strFinalDateChosen] compare:currentDateOnly]==NSOrderedSame)
            
        {
            
            NSLog(@"same");
            
        }
        
        chkForEdit=YES;
        
        chkForEdit=NO;
        
        
        
        
    }
    
    else
        
    {
        
        chkForEdit=NO;
        
        chkForEdit=YES;
        
        
    }
    
    
    
    
    
    
    
    if([[self stringToDate:@"09/26/2018 19:59:59"] compare:[self stringToDate:@"09/26/2018 19:59:59"]]==NSOrderedSame || [[self stringToDate:@"09/26/2018 19:59:59"] compare:[self stringToDate:@"09/26/2018 19:59:59"]]==NSOrderedAscending)
        
    {
        
        if([[self stringToDate:@"09/26/2018 19:59:59"] compare:[self stringToDate:@"09/26/2018 19:59:59"]]==NSOrderedSame)
            
        {
            
            NSLog(@"Same");
            
        }
        
        if([[self stringToDate:@"09/26/2018 19:59:59"] compare:[self stringToDate:@"09/27/2018 19:59:59"]]==NSOrderedAscending)
            
        {
            
            NSLog(@"AScending");
            
        }
        
        if([[self stringToDate:@"09/27/2018 19:59:59"] compare:[self stringToDate:@"09/26/2018 19:59:59"]]==NSOrderedDescending)
            
        {
            
            NSLog(@"DeScending");
            
        }
        
    }
    
    
    
    
    
    return chkForEdit;
    
    
    
}


-(NSString *)ChangeDateToLocalDateMechanicalePayPeriod :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    // [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];//@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss z"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss.SSS"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    // newTime = [dateFormatterNew dateFromString:strDateToConvert];
    NSString* finalTime = [dateFormatterNew stringFromDate:newTime];
    
    return finalTime;
}
- (NSNumber *)dateToSecondConvert:(NSString *)string {
    
    NSArray *components = [string componentsSeparatedByString:@":"];
    
    NSInteger hours   = [[components objectAtIndex:0] integerValue];
    NSInteger minutes = [[components objectAtIndex:1] integerValue];
    //NSInteger seconds = [[components objectAtIndex:2] integerValue];
    
    return [NSNumber numberWithInteger:(hours * 60 * 60) + (minutes * 60) ];
}

-(NSString*)updateHours: (NSString*)strMaxDate : (NSTimeInterval)secondsInHours
{
    NSDateFormatter *dateForMateNew=[[NSDateFormatter alloc]init];
    [dateForMateNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    NSDate *dateEightHoursAhead = [[dateForMateNew dateFromString:strMaxDate] dateByAddingTimeInterval:secondsInHours];
    
    NSString* newTime = [dateForMateNew stringFromDate:dateEightHoursAhead];
    
    if(newTime==nil)
    {
        dateForMateNew=[[NSDateFormatter alloc]init];
        [dateForMateNew setDateFormat:@"MM/dd/yyyy"];
        
        dateEightHoursAhead = [[dateForMateNew dateFromString:strMaxDate] dateByAddingTimeInterval:secondsInHours];
        
        [dateForMateNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        
        newTime = [dateForMateNew stringFromDate:dateEightHoursAhead];
    }
    
    NSDate *dateNew=[dateForMateNew dateFromString:newTime];
    
    NSString* strPayPeriodDate=[dateForMateNew stringFromDate:dateNew];
    
    // NSDate *dateNew=[dateForMateNew dateFromString:newTime];
    return strPayPeriodDate;
}
-(NSString *)findMaxDate:(NSArray*)arrDate
{
    NSString *strMaxDate;
    
    
    NSDate *maxDate = [arrDate valueForKeyPath:@"@max.self"];
    
    NSDateFormatter *dateForMateNew=[[NSDateFormatter alloc]init];
    [dateForMateNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    strMaxDate=[dateForMateNew stringFromDate:maxDate];
    
    indexDate=[arrDate indexOfObject:maxDate];
    return strMaxDate;
}

-(NSString *)ChangeDateToLocalDateMechanicale :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];//@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss z"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss.SSS"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatterNew setDateFormat:@"MM/dd/yyyy"];
    // newTime = [dateFormatterNew dateFromString:strDateToConvert];
    NSString* finalTime = [dateFormatterNew stringFromDate:newTime];
    
    return finalTime;
}
-(NSDate *)getDateWithTime:(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];//@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss.SSS"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    return newTime;
}


-(NSString *)getCurrentDate
{
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatterNew setDateFormat:@"MM/dd/yyyy"];
    
    NSString *strDate=[dateFormatterNew stringFromDate:[NSDate date]];
    return strDate;
}
-(NSString *)getCurrentDateTime
{
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    NSString *strDate=[dateFormatterNew stringFromDate:[NSDate date]];
    return strDate;
}

// akshay start //
- (IBAction)actionOnSelectCredit_Apply:(id)sender {
    
    if (arrayAccountDiscountCredits.count==0) {
        
        [global AlertMethod:Alert :@"No credit available"];
        
    } else {
        
        tblData.tag=80;
        [self tableLoad:tblData.tag];
    }
    
}
- (IBAction)actionOnSelectCredit_Add:(id)sender {
    
    if (arrayMasterCredits.count==0) {
        
        [global AlertMethod:Alert :@"No credit available"];
        
    } else {
        
        tblData.tag=90;
        [self tableLoad:tblData.tag];
    }
}

- (IBAction)actionOnApplyCredit:(id)sender {
    
    
    if([_buttonSelectCredit_Apply.titleLabel.text isEqualToString:@"Select Credit"])
    {
        [global AlertMethod:Alert :@"Please select credit"];
        return;
    }
    isPopUpViewFromMaster = NO;
    [self addPartLaborPopUpView];
    
    
}
- (IBAction)actionOnAddCredit:(id)sender {
    
    if([_buttonSelectCredit_Add.titleLabel.text isEqualToString:@"Select Credit"])
    {
        [global AlertMethod:Alert :@"Please select credit"];
        return;
    }
    isPopUpViewFromMaster = YES;
    
    [self addPartLaborPopUpView];
    
}

-(void)fetchAccountDiscountForMechanical
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAccountDiscount=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
    requestAccountDiscount = [[NSFetchRequest alloc] init];
    [requestAccountDiscount setEntity:entityAccountDiscount];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestAccountDiscount setPredicate:predicate];
    
    sortDescriptorAccDiscount = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsAccDiscount = [NSArray arrayWithObject:sortDescriptorAccDiscount];
    
    [requestAccountDiscount setSortDescriptors:sortDescriptorsAccDiscount];
    
    self.fetchedResultsControllerAccountDiscount = [[NSFetchedResultsController alloc] initWithFetchRequest:requestAccountDiscount managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerAccountDiscount setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerAccountDiscount performFetch:&error];
    NSArray* arrAccountDiscount = [self.fetchedResultsControllerAccountDiscount fetchedObjects];
    
    if ([arrAccountDiscount count] == 0)
    {
        
    }
    else
    {
        for(NSManagedObject *accDiscount in arrAccountDiscount)
        {
            NSArray *keys = [[[accDiscount entity] attributesByName] allKeys];
            NSDictionary *dict = [accDiscount dictionaryWithValuesForKeys:keys];
            [arrayAccountDiscountCredits addObject:dict];
        }
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)fetchWorkOrderAppliedDiscounts
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrderAppliedDiscounts=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    requestWorkOrderAppliedDiscount = [[NSFetchRequest alloc] init];
    [requestWorkOrderAppliedDiscount setEntity:entityWorkOrderAppliedDiscounts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdGlobal];
    
    [requestWorkOrderAppliedDiscount setPredicate:predicate];
    
    sortDescriptorWorkOrderAppliedDiscount = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:YES];
    sortDescriptorsWorkOrderAppliedDiscount = [NSArray arrayWithObject:sortDescriptorWorkOrderAppliedDiscount];
    //
    [requestWorkOrderAppliedDiscount setSortDescriptors:sortDescriptorsWorkOrderAppliedDiscount];
    
    self.fetchedResultsControllerWorkOrderAppliedDiscount = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWorkOrderAppliedDiscount managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderAppliedDiscount setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderAppliedDiscount performFetch:&error];
    NSArray* arrAppliedDiscounts = [self.fetchedResultsControllerWorkOrderAppliedDiscount fetchedObjects];
    
    NSLog(@"%@",arrAppliedDiscounts);
    
    
    if ([arrAppliedDiscounts count] == 0)
    {
        heightPaymntDetailView = paymentDetailHeight;
        
        _heightPaymentView.constant =  1500;
        _heightTblViewPaymntDetail.constant = 0.0;
        _heightViewCredit.constant = 110;
        isUpdateViewsFrame = YES;
        [self addView:@""];
        _labelCreditsPaymentDetailViewRight.text = @"$0.00";
        _labelCreditsPaymentDetailViewLeft.text = @"$0.00";
        [_tableViewCredit reloadData];
        _heightTblViewPaymntDetailLeft.constant = 0.0;
        _heightTblViewPaymntDetailRight.constant = 0.0;
        [_tableViewCreditPaymntDetailLeft reloadData];
        [_tableViewCreditPaymntDetailRight reloadData];
        
        _heightPaymntDetailLeft.constant = _viewOld.frame.size.height;
        _heightPaymntDetailRight.constant = _viewNew.frame.size.height;
        
    }
    else
    {
        for(NSManagedObject *appliedDiscount in arrAppliedDiscounts)
        {
            NSArray *keys = [[[appliedDiscount entity] attributesByName] allKeys];
            NSDictionary *dict = [appliedDiscount dictionaryWithValuesForKeys:keys];
            [arrayAppliedDiscounts addObject:dict];
        }
        
        if(arrayAppliedDiscounts.count>0)
        {
            totalLaborAmountLocal = totalLaborAmountLocalCopy;
            totalPartAmountLocal = totalPartAmountLocalCopy;
            [self calculateDiscountPrice];
        }
        /* _labelSubTotal.text = @"0.00";
         if(arrayAppliedDiscounts.count>0)
         {
         for(NSDictionary *dict in arrayAppliedDiscounts)
         {
         _labelSubTotal.text = [NSString stringWithFormat:@"%.2f",[_labelSubTotal.text floatValue]+[[dict valueForKey:@"discountAmount"] floatValue]];
         }
         viewCreditHeight = 195+(int)arrayAppliedDiscounts.count*250;
         
         heightPaymntDetailView = 970 + ((int)arrayAppliedDiscounts.count*60)+60;// +60 for header view total credits
         _heightPaymentViewLeft.constant = 860 + ((int)arrayAppliedDiscounts.count*60)+60;
         
         _heightTblViewPaymntDetailLeft.constant = ((int)arrayAppliedDiscounts.count*60)+60;
         
         isUpdateViewsFrame = YES;
         [self addView];
         [_tableViewCreditPaymntDetailLeft reloadData];
         [_tableViewCredit reloadData];
         }*/
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)actionOnDeleteCredit:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Are you sure you want to delete credit from account?" delegate:self cancelButtonTitle:@"Delete From Account" otherButtonTitles:@"NO",@"YES", nil];
    
    
    alertView.tag = btn.tag;
    [alertView show];
}

- (IBAction)actionOnLaborPrice:(id)sender {
    
    if(!isLaborPrice)
    {
        isLaborPrice = YES;
        [_buttonLaborPrice setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonPartPrice setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
}
- (IBAction)actionOnPartPrice:(id)sender {
    
    if(isLaborPrice)
    {
        isLaborPrice = NO;
        [_buttonPartPrice setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonLaborPrice setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
}
- (IBAction)actionOnApplyPartLabor:(id)sender {
    
    if(isPopUpViewFromMaster)
    {// this action triggered from master credits
        
        // create discount object from master
        NSDictionary *dictTemp = @{@"companyKey":[NSString stringWithFormat:@"%@",strCompanyKey],
                                   @"userName":[NSString stringWithFormat:@"%@",strUserName],
                                   @"workorderId":[NSString stringWithFormat:@"%@",strWorkOrderId],
                                   @"accountNo":[NSString stringWithFormat:@"%@",_lblAccountNo.text],
                                   @"discountAmount":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"DiscountAmount"]],
                                   @"discountSetupSysName":@"",// as dicussed leave blank
                                   @"discountSetupName":@"",// as dicussed leave blank
                                   @"discountCode":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"DiscountCode"]],
                                   @"isDiscountPercent":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"IsDiscountPercent"]],
                                   @"discountPercent":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"DiscountPercent"]],
                                   @"sysName":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"SysName"]],
                                   @"name":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"Name"]],
                                   @"isActive":@"true",
                                   @"isDelete":@"false",
                                   @"discountDescription":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"Description"]],
                                   @"departmentSysName":[NSString stringWithFormat:@"%@",strDepartmentSysName],
                                   @"createdByDevice":@"Mobile"
                                   };
        
        [self saveAccountDiscount:dictTemp];// save to account discount table
        [arrayAccountDiscountCredits removeAllObjects];
        [self fetchAccountDiscountForMechanical];
        
        // remove added credit(in account discount) from master list
        for(NSDictionary *dict in [arrayMasterCredits copy])
        {
            if([[dict valueForKey:@"SysName"] isEqualToString:[dictAddDiscount valueForKey:@"SysName"]])
            {
                [arrayMasterCredits removeObject:dict];
                [_buttonSelectCredit_Add setTitle:@"Select Credit" forState:UIControlStateNormal];
            }
        }
        
        // create applied discount from account discount
        
        for(NSDictionary *dict in arrayAccountDiscountCredits)
        {
            if([[dict valueForKey:@"sysName"] isEqualToString:[dictAddDiscount valueForKey:@"SysName"]])
            {
                [self methodToApplyDiscount:dict];
                break;
            }
        }
        [self callToChangeCalculationsOverAll];
    }
    else
    { // this action triggered from account discount
        
        for(NSDictionary *tempDict in arrayAppliedDiscounts)
        {
            if([[dictSelectedDiscount valueForKey:@"sysName"] isEqualToString:[tempDict valueForKey:@"discountSysName"]])
            {
                [global AlertMethod:@"Alert!" :@"This credit aleady applied"];
                return;
            }
        }
        
        [self methodToApplyDiscount:dictSelectedDiscount];
        [self callToChangeCalculationsOverAll];
    }
    
    
}

- (IBAction)actionOnCancelPartLabor:(id)sender {
    
    [viewBackGround removeFromSuperview];
    [_viewPartLaborCreditApply removeFromSuperview];
    isLaborPartViewAddedToSuperView = NO;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"YES"])
    {
        NSDictionary *dictDeletedCredit = [arrayAppliedDiscounts objectAtIndex:alertView.tag];
        
        
        // remove deleted applied discount from Account Discount list
        
        /*
         for(NSDictionary *dictDiscount in arrayAccountDiscountCredits)
         {
         if([[dictDeletedCredit valueForKey:@"discountSysName"]isEqualToString:[dictDiscount valueForKey:@"sysName"]])
         {
         [self deleteAccountDiscountFromLocalDB:dictDiscount];
         break;
         }
         }*/
        
        [self deleteAppliedDiscountFromLocalDB:dictDeletedCredit];
        
        [self getAccDiscountAppliedDiscountAndMasterCredits];
        
        [_buttonSelectCredit_Apply setTitle:@"Select Credit" forState:UIControlStateNormal];
        
        if(isLaborPrice)
        {
            totalLaborAmountLocal = totalLaborAmountLocal+ [[dictDeletedCredit valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        else
        {
            totalPartAmountLocal = totalPartAmountLocal+ [[dictDeletedCredit valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        [self callToChangeCalculationsOverAll];
    }
    else if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete From Account"])
    {
        NSDictionary *dictDeletedCredit = [arrayAppliedDiscounts objectAtIndex:alertView.tag];
        
        
        // remove deleted applied discount from Account Discount list
        
        for(NSDictionary *dictDiscount in arrayAccountDiscountCredits)
        {
            if([[dictDeletedCredit valueForKey:@"discountSysName"]isEqualToString:[dictDiscount valueForKey:@"sysName"]])
            {
                [self deleteAccountDiscountFromLocalDB:dictDiscount];
                break;
            }
        }
        
        [self deleteAppliedDiscountFromLocalDB:dictDeletedCredit];
        
        [self getAccDiscountAppliedDiscountAndMasterCredits];
        
        [_buttonSelectCredit_Apply setTitle:@"Select Credit" forState:UIControlStateNormal];
        
        if(isLaborPrice)
        {
            totalLaborAmountLocal = totalLaborAmountLocal+ [[dictDeletedCredit valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        else
        {
            totalPartAmountLocal = totalPartAmountLocal+ [[dictDeletedCredit valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        [self callToChangeCalculationsOverAll];
    }
    else
    {
        
    }
}
-(void)deleteAccountDiscountFromLocalDB:(NSDictionary*)dictDelete
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Account Discount Data
    entityAccountDiscount=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"sysName = %@",[dictDelete valueForKey:@"sysName"]];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)deleteAppliedDiscountFromLocalDB:(NSDictionary*)dictDelete
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Account Discount Data
    entityWorkOrderAppliedDiscounts=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"discountSysName = %@",[dictDelete valueForKey:@"discountSysName"]];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

-(void)saveAppliedDiscount:(NSDictionary*)dictAppliedDicount
{
    
    entityWorkOrderAppliedDiscounts=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    
    WorkOrderAppliedDiscountExtSerDcs *objWorkOrderAppliedDiscount = [[WorkOrderAppliedDiscountExtSerDcs alloc]initWithEntity:entityWorkOrderAppliedDiscounts insertIntoManagedObjectContext:context];
    
    
    objWorkOrderAppliedDiscount.companyKey = [NSString stringWithFormat:@"%@",strCompanyKey];
    
    objWorkOrderAppliedDiscount.userName = strUserName;
    
    objWorkOrderAppliedDiscount.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"subWorkOrderId"]];
    
    objWorkOrderAppliedDiscount.workOrderId=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"workOrderId"]];
    
    objWorkOrderAppliedDiscount.workOrderAppliedDiscountId=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"workOrderAppliedDiscountId"]];
    
    objWorkOrderAppliedDiscount.appliedDiscountAmt=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"appliedDiscountAmt"]];
    
    
    //objWorkOrderAppliedDiscount.isDiscountPercent=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"isDiscountPercent"]];
    
    
    if ([[dictAppliedDicount valueForKey:@"isDiscountPercent"] isEqualToString:@"1"] || [[dictAppliedDicount valueForKey:@"isDiscountPercent"] isEqualToString:@"True"] || [[dictAppliedDicount valueForKey:@"isDiscountPercent"] isEqualToString:@"true"])
    {
        objWorkOrderAppliedDiscount.isDiscountPercent=[NSString stringWithFormat:@"%@",@"true"];
        
    }
    else
    {
        objWorkOrderAppliedDiscount.isDiscountPercent=[NSString stringWithFormat:@"%@",@"false"];
    }
    
    objWorkOrderAppliedDiscount.discountSysName=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountSysName"]];
    
    objWorkOrderAppliedDiscount.discountType=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountType"]];
    
    objWorkOrderAppliedDiscount.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"isActive"]]];
    
    objWorkOrderAppliedDiscount.createdBy=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"createdBy"]];
    
    objWorkOrderAppliedDiscount.createdDate=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"createdDate"]];
    
    objWorkOrderAppliedDiscount.modifiedBy=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"modifiedBy"]];
    
    objWorkOrderAppliedDiscount.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"modifiedDate"]]];
    
    objWorkOrderAppliedDiscount.createdByDevice=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"createdByDevice"]];
    
    objWorkOrderAppliedDiscount.discountCode=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountCode"]];
    
    objWorkOrderAppliedDiscount.discountAmount=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountAmount"]];
    
    objWorkOrderAppliedDiscount.discountDescription=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountDescription"]];
    
    objWorkOrderAppliedDiscount.discountName=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountName"]];
    
    objWorkOrderAppliedDiscount.discountPercent=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountPercent"]];
    
    objWorkOrderAppliedDiscount.applyOnPartPrice=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"applyOnPartPrice"]];
    
    objWorkOrderAppliedDiscount.applyOnLaborPrice=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"applyOnLaborPrice"]];
    
    NSError *error4;
    [context save:&error4];
    
}
-(void)saveAccountDiscount:(NSDictionary*)dictAccountDiscount
{
    entityAccountDiscount=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
    
    AccountDiscountExtSerDcs *objAccountDiscount = [[AccountDiscountExtSerDcs alloc]initWithEntity:entityAccountDiscount insertIntoManagedObjectContext:context];
    
    
    objAccountDiscount.companyKey=strCompanyKey;
    objAccountDiscount.userName=strUserName;
    objAccountDiscount.workorderId=[NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"workorderId"]];
    objAccountDiscount.accountNo = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"accountNo"]];
    
    objAccountDiscount.discountAmount = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountAmount"]];
    
    objAccountDiscount.discountSetupSysName = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountSetupSysName"]];
    objAccountDiscount.discountSetupName = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountSetupName"]];
    objAccountDiscount.discountCode = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountCode"]];
    
    objAccountDiscount.isDiscountPercent = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"isDiscountPercent"]];
    objAccountDiscount.discountPercent = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountPercent"]];
    objAccountDiscount.sysName = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"sysName"]];
    
    objAccountDiscount.name = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"name"]];
    
    objAccountDiscount.isActive = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"isActive"]];
    
    objAccountDiscount.isDelete = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"isDelete"]];
    
    objAccountDiscount.discountDescription = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountDescription"]];
    
    objAccountDiscount.departmentSysName = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"departmentSysName"]];
    objAccountDiscount.createdByDevice = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"createdByDevice"]];
    
    
    NSError *error22222;
    [context save:&error22222];
}

-(void)getAccDiscountAppliedDiscountAndMasterCredits
{
    [arrayMasterCredits removeAllObjects];
    [arrayCopyOfMasterCredits removeAllObjects];
    [arrayAccountDiscountCredits removeAllObjects];
    [arrayAppliedDiscounts removeAllObjects];
    
    [self fetchAccountDiscountForMechanical];
    [self fetchWorkOrderAppliedDiscounts];
    
    NSDictionary  *dictSalesLeadMaster;
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    dictSalesLeadMaster=[defs2 valueForKey:@"MasterSalesAutomation"];
    arrayMasterCredits = [[dictSalesLeadMaster valueForKey:@"DiscountSetupMasterCredit"] mutableCopy];
    NSLog(@"%@",arrayMasterCredits);
    
    arrayCopyOfMasterCredits = [arrayMasterCredits mutableCopy];
    
    for(NSDictionary *dictDiscount in arrayAccountDiscountCredits)
    {
        for(NSDictionary *dictMaster in [arrayMasterCredits copy])
        {
            if([[dictDiscount valueForKey:@"sysName"] isEqualToString:[dictMaster valueForKey:@"SysName"]])
            {
                [arrayMasterCredits removeObject:dictMaster];
            }
        }
    }
}
-(void)calculateDiscountPrice
{
    NSMutableDictionary *dictAppliedDiscount = [[NSMutableDictionary alloc] init];
    
    for(int i=0;i<arrayAppliedDiscounts.count;i++)
    {
        dictAppliedDiscount = [[arrayAppliedDiscounts objectAtIndex:i] mutableCopy];
        
        if([[dictAppliedDiscount valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"] || [[dictAppliedDiscount valueForKey:@"applyOnLaborPrice"]boolValue]==YES)
        { // discount apply on labor price
            
            if([[dictAppliedDiscount valueForKey:@"isDiscountPercent"] boolValue]==YES)
            { // if discount percent
                
                float discountPercentage = [[dictAppliedDiscount valueForKey:@"discountPercent"] floatValue];
                float discountAmountFromPercentage = (totalLaborAmountLocalCopy*discountPercentage)/100.0;
                
                if(totalLaborAmountLocal==0) //if labor price becomes 0
                {
                    discountAmountFromPercentage = 0;
                    /*[dictAppliedDiscount setValue:@"0" forKey:@"appliedDiscountAmt"];
                     [self updateAppliedDiscount:dictAppliedDiscount];*/
                    
                    [self deleteAppliedDiscountFromLocalDB:dictAppliedDiscount];
                    [arrayAppliedDiscounts removeObjectAtIndex:i];
                    i = i-1;
                    [self showZeroDiscountAmountAlert];
                    return;
                    
                }
                else if(totalLaborAmountLocal>0 && totalLaborAmountLocal<discountAmountFromPercentage) // if labor price is more than 0 but less than discount amount
                {
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",totalLaborAmountLocal] forKey:@"appliedDiscountAmt"];
                    totalLaborAmountLocal = 0.0;
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
                else
                {
                    totalLaborAmountLocal = totalLaborAmountLocal-discountAmountFromPercentage;
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",discountAmountFromPercentage] forKey:@"appliedDiscountAmt"];
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
            }
            else // discount in amount
            {
                float discountAmount = [[dictAppliedDiscount valueForKey:@"discountAmount"] floatValue];
                if(totalLaborAmountLocal == 0)
                {
                    /*[dictAppliedDiscount setValue:@"0" forKey:@"appliedDiscountAmt"];
                     [self updateAppliedDiscount:dictAppliedDiscount];*/
                    
                    [self deleteAppliedDiscountFromLocalDB:dictAppliedDiscount];
                    [arrayAppliedDiscounts removeObjectAtIndex:i];
                    i = i-1;
                    [self showZeroDiscountAmountAlert];
                    return;
                    
                }
                else if(totalLaborAmountLocal>0 && totalLaborAmountLocal<discountAmount)
                {
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",totalLaborAmountLocal] forKey:@"appliedDiscountAmt"];
                    totalLaborAmountLocal = 0.0;
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
                else
                {
                    totalLaborAmountLocal = totalLaborAmountLocal-discountAmount;
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",discountAmount] forKey:@"appliedDiscountAmt"];
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
            }
        }
        else if([[dictAppliedDiscount valueForKey:@"applyOnPartPrice"] isEqualToString:@"true"]||[[dictAppliedDiscount valueForKey:@"applyOnPartPrice"] boolValue]==YES)
        {
            if([[dictAppliedDiscount valueForKey:@"isDiscountPercent"] boolValue]==YES)
            { // if discount percent
                
                float discountPercentage = [[dictAppliedDiscount valueForKey:@"discountPercent"] floatValue];
                float discountAmountFromPercentage = (totalPartAmountLocalCopy*discountPercentage)/100.0;
                if(totalPartAmountLocal==0) //if part price becomes 0
                {
                    discountAmountFromPercentage = 0;
                    /* [dictAppliedDiscount setValue:@"0" forKey:@"appliedDiscountAmt"];
                     [self updateAppliedDiscount:dictAppliedDiscount];*/
                    
                    [self deleteAppliedDiscountFromLocalDB:dictAppliedDiscount];
                    [arrayAppliedDiscounts removeObjectAtIndex:i];
                    i = i-1;
                    [self showZeroDiscountAmountAlert];
                    return;
                    
                }
                else if(totalPartAmountLocal>0 && totalPartAmountLocal<discountAmountFromPercentage) // if part price is more than 0 but less than discount amount
                {
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",totalPartAmountLocal] forKey:@"appliedDiscountAmt"];
                    totalPartAmountLocal = 0.0;
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
                else
                {
                    totalPartAmountLocal = totalPartAmountLocal-discountAmountFromPercentage;
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",discountAmountFromPercentage] forKey:@"appliedDiscountAmt"];
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
            }
            else // discount amount
            {
                float discountAmount = [[dictAppliedDiscount valueForKey:@"discountAmount"] floatValue];
                if(totalPartAmountLocal == 0) // if part sale amount is 0
                {
                    /*[dictAppliedDiscount setValue:@"0" forKey:@"appliedDiscountAmt"];
                     [self updateAppliedDiscount:dictAppliedDiscount];*/
                    
                    [self deleteAppliedDiscountFromLocalDB:dictAppliedDiscount];
                    [arrayAppliedDiscounts removeObjectAtIndex:i];
                    i = i-1;
                    [self showZeroDiscountAmountAlert];
                    return;
                    
                }
                else if(totalPartAmountLocal>0 && totalPartAmountLocal<discountAmount)
                { // part sale amount more than 0 but less than discount amount
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",totalPartAmountLocal] forKey:@"appliedDiscountAmt"];
                    totalPartAmountLocal = 0.0;
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
                else
                {// part sale amount is more than 0 and discount price
                    totalPartAmountLocal = totalPartAmountLocal-discountAmount;
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",discountAmount] forKey:@"appliedDiscountAmt"];
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
            }
        }
        [arrayAppliedDiscounts replaceObjectAtIndex:i withObject:dictAppliedDiscount];
        
    }
    [_tableViewCredit reloadData];
    [self updateView];
}

-(void)updateAppliedDiscount:(NSDictionary*)dict
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAccountDiscount=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    requestAccountDiscount = [[NSFetchRequest alloc] init];
    [requestAccountDiscount setEntity:entityAccountDiscount];
    
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND subWorkOrderId = %@",[dict valueForKey:@"workOrderId"],[dict valueForKey:@"subWorkOrderId"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"discountSysName = %@",[dict valueForKey:@"discountSysName"]];
    
    [requestAccountDiscount setPredicate:predicate];
    
    sortDescriptorAccDiscount = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsAccDiscount = [NSArray arrayWithObject:sortDescriptorAccDiscount];
    
    [requestAccountDiscount setSortDescriptors:sortDescriptorsAccDiscount];
    
    self.fetchedResultsControllerAccountDiscount = [[NSFetchedResultsController alloc] initWithFetchRequest:requestAccountDiscount managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerAccountDiscount setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerAccountDiscount performFetch:&error];
    NSArray* arrAccountDiscount = [self.fetchedResultsControllerAccountDiscount fetchedObjects];
    
    if ([arrAccountDiscount count] == 0)
    {
        
    }
    else
    {
        for(NSManagedObject *accDiscount in arrAccountDiscount)
        {
            [accDiscount setValue:[dict valueForKey:@"appliedDiscountAmt"] forKey:@"appliedDiscountAmt"];
        }
    }
    
    NSError *error4;
    [context save:&error4];
    
}

-(void)updateView // this method updates payment detail view  and applied credit view(i.e credit view)
{
    _labelSubTotal.text = @"0.00";
    float totalCredits = 0.0;
    if(arrayAppliedDiscounts.count>0)
    {
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            //            _labelSubTotal.text = [NSString stringWithFormat:@"$%.2f",[_labelSubTotal.text floatValue]+[[dict valueForKey:@"appliedDiscountAmt"] floatValue]];
            totalCredits = totalCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        _labelSubTotal.text = [NSString stringWithFormat:@"$%.2f",totalCredits];
        
        heightPaymntDetailView = paymentDetailHeight + ((int)arrayAppliedDiscounts.count*60)+((int)arrayAppliedDiscounts.count*220);
        
        _heightPaymentView.constant = 1500+((int)arrayAppliedDiscounts.count*60)+((int)arrayAppliedDiscounts.count*220);
        
        _heightTblViewPaymntDetail.constant = ((int)arrayAppliedDiscounts.count*60);
        
        _heightViewCredit.constant = 110+((int)arrayAppliedDiscounts.count*220)+50;// 50 is for sub total view(bottom)
        
        // Height Calculation for
        
        _heightTblViewPaymntDetailLeft.constant = ((int)arrayAppliedDiscounts.count*60)+10;
        _heightTblViewPaymntDetailRight.constant = ((int)arrayAppliedDiscounts.count*60)+10;
        [_tableViewCreditPaymntDetailLeft reloadData];
        [_tableViewCreditPaymntDetailRight reloadData];
        
        _heightPaymntDetailLeft.constant = 1058 + _heightTblViewPaymntDetailLeft.constant-100;
        _heightPaymntDetailRight.constant = 1058 + _heightTblViewPaymntDetailRight.constant-100;
        
        float totalCredits = 0.0;
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            totalCredits = totalCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        
        _labelCreditsPaymentDetailViewRight.text = [NSString stringWithFormat:@"$%.2f",totalCredits];
        _labelCreditsPaymentDetailViewLeft.text = [NSString stringWithFormat:@"$%.2f",totalCredits];
        
        [_tableViewCreditPaymntDetail reloadData];
        
        isUpdateViewsFrame = YES;
        [self addView:@""];
        
        [_tableViewCredit reloadData];
    }
}


-(void)addPartLaborPopUpView
{
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor = [UIColor blackColor];
    viewBackGround.alpha = 0.5;
    [self.view addSubview: viewBackGround];
    
    
    CGRect frameFor_viewPopUpPurchaseOrder=CGRectMake(40, 20, [UIScreen mainScreen].bounds.size.width-80,_viewPartLaborCreditApply.frame.size.height);
    
    [_viewPartLaborCreditApply setFrame:frameFor_viewPopUpPurchaseOrder];
    
    _viewPartLaborCreditApply.center = CGPointMake(self.view.frame.size.width  / 2,
                                                   
                                                   self.view.frame.size.height / 2);
    
    isLaborPrice = YES;
    [_buttonLaborPrice setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_buttonPartPrice setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self.view addSubview:_viewPartLaborCreditApply];
    isLaborPartViewAddedToSuperView = YES;
    
}

-(void)methodToApplyDiscount:(NSDictionary*)dict
{
    NSString *strApplyOnLbrPrice,*strApplyOnPartPrice = @"";
    if(isLaborPrice)
    {
        strApplyOnLbrPrice = @"true";
        strApplyOnPartPrice = @"false";
    }
    else
    {
        strApplyOnLbrPrice = @"false";
        strApplyOnPartPrice = @"true";
    }
    NSDictionary *dictTemp = @{@"appliedDiscountAmt":@"0",
                               @"applyOnLaborPrice":strApplyOnLbrPrice,
                               @"applyOnPartPrice":strApplyOnPartPrice,
                               @"companyKey":[NSString stringWithFormat:@"%@",[dict valueForKey:@"companyKey"]],
                               @"createdBy":[NSString stringWithFormat:@"%@",strUserName],
                               @"createdByDevice":@"Mobile",
                               @"createdDate":[NSString stringWithFormat:@"%@",[global strCurrentDate]],
                               @"discountAmount":[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountAmount"]],
                               @"discountCode":[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountCode"]],
                               @"discountDescription":[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]],
                               @"discountName":[NSString stringWithFormat:@"%@",[dict valueForKey:@"name"]],
                               @"discountPercent":[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountPercent"]],
                               @"discountSysName":[NSString stringWithFormat:@"%@",[dict valueForKey:@"sysName"]],
                               @"discountType":@"Credit",
                               @"isActive":@"true",
                               @"isDiscountPercent":[NSString stringWithFormat:@"%@",[dict valueForKey:@"isDiscountPercent"]],
                               @"modifiedBy":[NSString stringWithFormat:@"%@",strUserName],
                               @"modifiedDate":[NSString stringWithFormat:@"%@",[self getCurrentDateAndTime]],
                               @"subWorkOrderId":[NSString stringWithFormat:@"%@",strSubWorkOrderIdGlobal],
                               @"userName":[NSString stringWithFormat:@"%@",[dict valueForKey:@"userName"]],
                               @"workOrderAppliedDiscountId":@"",
                               @"workOrderId":[NSString stringWithFormat:@"%@",strWorkOrderId]
                               
                               };
    
    [self saveAppliedDiscount:dictTemp];
    
    [arrayAppliedDiscounts removeAllObjects];
    [self fetchWorkOrderAppliedDiscounts];
    
    [viewBackGround removeFromSuperview];
    [_viewPartLaborCreditApply removeFromSuperview];
    isLaborPartViewAddedToSuperView = NO;
}
-(void)actionOnViewMore_Cedit:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    [global AlertMethod:@"Description" :[[arrayAppliedDiscounts objectAtIndex:btn.tag] valueForKey:@"discountDescription"]];
}
-(NSString*)getCurrentDateAndTime
{
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    return [dateFormatter stringFromDate:[NSDate date]];
}
// Call on all change action

-(void)callToChangeCalculationsOverAll{
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
}
-(BOOL)isItLaborTax
{
    NSArray *arrOfTaxMaster=[global getSalesTaxFromMaster:strCompanyKey :strDepartmentSysName :[matchesFinalSubWorkdOrder valueForKey:@"propertyType"]];
    BOOL isLaborTax=NO,isMaterialTax=NO,isVendorPayTax=NO,isFullTax=NO;
    
    if (!(arrOfTaxMaster.count==0)) {
        
        //Check According To Condition to apply tax on what
        
        
        
        NSDictionary *dictTaxData=arrOfTaxMaster[0];
        
        NSString *strVendorPayTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"VendorPayTax"]];
        
        if ([strVendorPayTax isEqualToString:@"true"] || [strVendorPayTax isEqualToString:@"True"] || [strVendorPayTax isEqualToString:@"1"]) {
            
            isVendorPayTax=YES;
            
        }
        
        if ([strWorkOrderAddressSubType isEqualToString:@"Commercial"]) {//Commercial Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMPartPriceType"]];
                
            }
            
        } else {//Resedential Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMPartPriceType"]];
                
            }
            
        }
        
        isGlobalVendorTax=isVendorPayTax;
        
        if (isFullTax) {
            
            isGlobalFullTax=isFullTax;
            
            double taxValue;
            //            taxValue=[strTax doubleValue];
            //            tax=(totalPrice*taxValue)/100;
            
        } else {
            
            // tax=[self calculationOverAllNewLaborMaterialReturn:subTotal :subTotalLabor :subTotalMaterialCost :subTotalMaterialCostSales :isLaborTax :isMaterialTax :strCostAdj :strPartCostAdj :strPartPriceType :strDiscount];
            
            isGlobalPartTax=isMaterialTax;
            isGlobalLaborTax=isLaborTax;
            
        }
        
    }
    return isLaborTax;
}
// Akshay End //


-(void)showZeroDiscountAmountAlert// ye alert tab dikhega jab discount amount zero hoga
{
    [global displayAlertController:Alert :@"Credit can not be applied" :self];
}
// Akshay End //

-(void)calculateTripChargeBasedOnQty{
    
    _lblTripChargeBasedOnQty.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTipCharge.text]*[_txtFld_TripChargeQty.text intValue]];
    
    _lblTripChargeBasedOnQtyNewPlan.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTripChargeNewPlan.text]*[_txtFld_TripChargeQtyNewPlan.text intValue]];
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    
}

-(void)calculateTripChargeBasedOnQtyOnTextFldEdit :(NSString*)strTripQty{
    
    _lblTripChargeBasedOnQty.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTipCharge.text]*[strTripQty intValue]];
    
    _lblTripChargeBasedOnQtyNewPlan.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTripChargeNewPlan.text]*[strTripQty intValue]];
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
    
}

//=====================================
//=====================================
#pragma mark- MemberShip New Plan Changes
//=====================================
//=====================================

- (IBAction)actionOnSelectPlan:(id)sender
{
    if (arrPSPMasters.count==0) {
        
        [global AlertMethod:Alert :@"No Plan available"];
        
    } else {
        
        tblData.tag=50;
        [self tableLoad:tblData.tag];
        
    }
    
}

-(void)fetchForOldNoMembership:(NSString*)strId
{
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    
    
    //strPurchaseStepOrder=@"2";
    arrMechanicalPSPMasters=[[NSMutableArray alloc]init];
    arrPSPMasters=[[NSMutableArray alloc]init];
    arrMechanicalPSPMasters=[dictMechanicalMasters valueForKey:@"PSPMasterExtSerDc"];
    
    //StepOrder
    NSMutableArray *arrTemp;
    arrTemp=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrMechanicalPSPMasters objectAtIndex:i];
        NSString *strType;
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Type"]];
        if([strDepartmentSysName isEqualToString:[dict valueForKey:@"DepartmentSysName"]]&&[strType isEqualToString:@"General"])
        {
            [arrTemp addObject:dict];
            
        }
    }
    for(int i=0;i<arrTemp.count;i++)
    {
        NSDictionary *dict=[arrTemp objectAtIndex:i];
        NSString *strPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
        if ([strPSPId isEqualToString:strId])
        {
            //_lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
            _lblValueForPSPCharge.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
            
            double memberShipDiscout,memberShipCharge,value;
            memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
            memberShipDiscout=[[dict valueForKey:@"Discount"]doubleValue];
            
            value=memberShipCharge*(memberShipDiscout/100);
            
            //_lblValueForMembershipPlanSavings.text=[NSString stringWithFormat:@"$%.02f",value];
            _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%.02f",value];
            
            break;
        }
    }
    
    
}



-(void)fetchMemberShipPlans :(NSString*)strPurchaseStepOrder{
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    
    
    //strPurchaseStepOrder=@"2";
    arrMechanicalPSPMasters=[[NSMutableArray alloc]init];
    arrPSPMasters=[[NSMutableArray alloc]init];
    arrMechanicalPSPMasters=[dictMechanicalMasters valueForKey:@"PSPMasterExtSerDc"];
    
    //StepOrder
    NSMutableArray *arrTemp;
    arrTemp=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrMechanicalPSPMasters objectAtIndex:i];
        NSString *strType;
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Type"]];
        if([strDepartmentSysName isEqualToString:[dict valueForKey:@"DepartmentSysName"]]&&[strType isEqualToString:@"General"])
        {
            [arrTemp addObject:dict];
            
        }
    }
    
    
    //    for(int i=[strPurchaseStepOrder intValue]-1;i<arrTemp.count;i++)
    //    {
    //        NSDictionary *dict=[arrTemp objectAtIndex:i];
    //        [arrPSPMasters addObject:dict];
    //    }
    
    for(int i=0;i<arrTemp.count;i++)
    {
        NSDictionary *dict=[arrTemp objectAtIndex:i];
        
        NSString *strStepOrder=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StepOrder"]];
        
        int stepOrderInMaster=[strStepOrder intValue];
        
        int strPurchaseStepOrderPresent=[strPurchaseStepOrder intValue];
        
        if (stepOrderInMaster>=strPurchaseStepOrderPresent) {
            
            [arrPSPMasters addObject:dict];
            
        }
    }
    
}


-(void)fetchPSPMasters:(NSString*)strPurchaseStepOrder
{
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    
    
    //strPurchaseStepOrder=@"2";
    arrMechanicalPSPMasters=[[NSMutableArray alloc]init];
    arrPSPMasters=[[NSMutableArray alloc]init];
    arrMechanicalPSPMasters=[dictMechanicalMasters valueForKey:@"PSPMasterExtSerDc"];
    
    //StepOrder
    NSMutableArray *arrTemp;
    arrTemp=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrMechanicalPSPMasters objectAtIndex:i];
        NSString *strType;
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Type"]];
        if([strDepartmentSysName isEqualToString:[dict valueForKey:@"DepartmentSysName"]]&&[strType isEqualToString:@"General"])
        {
            [arrTemp addObject:dict];
            
        }
    }
    
    
    //    for(int i=[strPurchaseStepOrder intValue]-1;i<arrTemp.count;i++)
    //    {
    //        NSDictionary *dict=[arrTemp objectAtIndex:i];
    //        [arrPSPMasters addObject:dict];
    //    }
    
    for(int i=0;i<arrTemp.count;i++)
    {
        NSDictionary *dict=[arrTemp objectAtIndex:i];
        
        NSString *strStepOrder=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StepOrder"]];
        
        int stepOrderInMaster=[strStepOrder intValue];
        
        int strPurchaseStepOrderPresent=[strPurchaseStepOrder intValue];
        
        if (stepOrderInMaster>=strPurchaseStepOrderPresent) {
            
            [arrPSPMasters addObject:dict];
            
        }
    }
    
    
    for(int i=0;i<arrPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrPSPMasters objectAtIndex:i];
        NSString *strPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
        
        if ([strPSPId isEqualToString:strPspMasterId])
        {
            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
            
            double memberShipDiscout,memberShipCharge,value;
            memberShipCharge=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
            memberShipDiscout=[[dict valueForKey:@"Discount"]doubleValue];
            
            value=memberShipCharge*(memberShipDiscout/100);
            
            _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value];
            
            break;
        }
    }
}

- (IBAction)actionOnNoMembership:(id)sender
{
    
    isFirstPlanSelected=YES;
    isFirstPlan=YES;
    _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
    _viewOld.layer.borderWidth=2.0;
    _viewOld.layer.cornerRadius=5.0;
    
    _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewNew.layer.borderWidth=2.0;
    _viewNew.layer.cornerRadius=5.0;
    
    _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
    _viewNew.backgroundColor=[UIColor whiteColor];
    
    chkPlanClick=YES;
    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
    [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
    NSDictionary *dict;
    
    dict=[arrPlanStatusResponse objectAtIndex:0];
    //dict=[arrPSPMasters objectAtIndex:0];
    
    strPSPCharge=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForPSPCharge.text]];
    strPSPDiscount=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForPSPDiscount.text]];
    
    strPSPCharge1=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForPSPCharge.text]];
    strPSPDiscountAmount1=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForPSPDiscount.text]];
    
    
    [self methodCalculationOverAll];
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    } else {
        
        if (IsShowPricingAndNotesOnApp) {

            if (!IsAmountEdited) {

        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

            }
                
        }
        
    }
    
    isOldPlan=YES;
    
    [self showAmountInTextFieldToCharge];
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self methodTotalRepairCalculateNew];
        [self methodToCalulateFRTotal];
        [self methodCalculateFRTotalNewPlan];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
    }
    
    // Akshay Start //
    [self calculateDiscountPrice];
    [_tableViewCreditPaymntDetailLeft reloadData];
    [self callToChangeCalculationsOverAll];
    // Akshay End //
    

}

- (IBAction)actionOnNewPlan:(id)sender
{
    isFirstPlan=NO;
    isFirstPlanSelected=NO;
    _viewNew.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
    _viewNew.layer.borderWidth=2.0;
    _viewNew.layer.cornerRadius=5.0;
    
    _viewOld.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewOld.layer.borderWidth=2.0;
    _viewOld.layer.cornerRadius=5.0;
    
    _viewNew.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
    _viewOld.backgroundColor=[UIColor whiteColor];
    
    chkPlanClick=YES;
    
    [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
    NSDictionary *dict;
    dict=[arrPlanStatusResponse objectAtIndex:1];
    //dict=[arrPSPMasters objectAtIndex:1];
    
    // strAccountPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
    strPSPCharge=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForMembershipChargeNewPaln.text]];
    strPSPDiscount=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForMembershipSavingNewPlan.text]];
    strPSPCharge2=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForMembershipChargeNewPaln.text]];
    strPSPDiscountAmount2=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForMembershipSavingNewPlan.text]];
    
    [self methodCalculationOverAll];
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    } else {
        
        if (IsShowPricingAndNotesOnApp) {

            if (!IsAmountEdited) {

        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

            }
            
        }
        
    }
    
    isOldPlan=NO;
    
    [self showAmountInTextFieldToCharge];
    
    //    [self methodTotalRepairCalculateNew];
    //    [self methodToCalulateFRTotal];
    //    [self methodCalculateFRTotalNewPlan];
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self methodTotalRepairCalculateNew];
        [self methodToCalulateFRTotal];
        [self methodCalculateFRTotalNewPlan];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
    }
    
    // Akshay Start //
    [self calculateDiscountPrice];
    [_tableViewCreditPaymntDetailRight reloadData];
    [self callToChangeCalculationsOverAll];
    // Akshay End //
    

}

-(void)methodCalculationOverAll{
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
        [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
        [self calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    
}

-(void)showAmountInTextFieldToCharge{
    
    if (isOldPlan) {
        
        if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
        {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        }
        else
        {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
            
        }
        
    } else {
        
        if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
        {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        }
        else
        {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                    
            }
            
        }
        
    }
    NSLog(@"amount is equal to======%@---%@",_txtAmountSingleAmount.text,_txtAmountCheckView.text);
    
    

}
-(void)methodTotalRepairCalculateNew{
    
    //NSArray *arrValues=[dictAmountTotalRepairs allValues];
    NSArray *arrValues=[self getArrayOfTotalRepairs];
    
    double sum=0;
    
    for (int k=0; k<arrValues.count; k++) {
        
        sum=sum+[arrValues[k] doubleValue];
        
    }
    
    
    _lblValueTotalApprovedRepairs.text=[NSString stringWithFormat:@"$%.2f",sum];
    _lblValueForTotalAmount.text=[NSString stringWithFormat:@"$%.2f",sum];
    //_lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"$%.2f",sum];
    _lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"%@",_lblValueForTotalAmount.text];
    
    //End
    [self taxCalculation];
    [self taxCalculationNewPlan];
    
    [self methodToCalulateMembershipCharge];
    
}

-(void)methodToCalulateFRTotal{
    
    double sumTotal=0;
    double subTotal,tax,amountDue;
    amountDue=0;
    
    NSArray *arrValues=[dictAmountTotalRepairs allValues];
    
    double sum=0;
    
    for (int k=0; k<arrValues.count; k++) {
        
        sum=sum+[arrValues[k] doubleValue];
        
    }
    sumTotal=sumTotal+sum;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se
    
    subTotal=sumTotal-[_txtOtherDiscounts.text doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForPSPCharge.text]-[self getNumberString:_lblValueForPSPDiscount.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txt_MiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    // strTax=@"10";
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    tax=[self taxCalculation];
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
        
    }
    
    
    //Nilind 19 July
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    //End
    
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        // _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        
        
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
            
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                
            }
            
        }
        
    }
    [self methodToSetAmountFinally];
    

}

-(void)methodCalculateFRTotalNewPlan{
    
    double sumTotalNewPlan=0;
    double subTotalNewPlan,taxNewPlan,amountDueNewPlan;
    amountDueNewPlan=0;
    
    NSArray *arrValues=[dictAmountTotalRepairs allValues];
    
    double sum=0;
    
    for (int k=0; k<arrValues.count; k++) {
        
        sum=sum+[arrValues[k] doubleValue];
        
    }
    sumTotalNewPlan=sumTotalNewPlan+sum;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se
    
    subTotalNewPlan=sumTotalNewPlan-[_txtOtherDiscountNewPlan.text doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];
    if (subTotalNewPlan>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotalNewPlan];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotalNewPlan=0.00;
    }
    
    
    double taxValueNewPlan;
    // strTax=@"10";
    taxValueNewPlan=[strTax doubleValue];
    taxNewPlan=(subTotalNewPlan*taxValueNewPlan)/100;
    taxNewPlan=[self taxCalculationNewPlan];
    
    if (taxNewPlan>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",taxNewPlan];
        
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        taxNewPlan=0.00;
    }
    
    
    //Nilind 19 July
    
    double totalNewNewPlan;
    totalNewNewPlan=subTotalNewPlan+taxNewPlan;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNewNewPlan];
    
    //End
    
    
    double  finalTotalWithTaxNewPlan;
    finalTotalWithTaxNewPlan=subTotalNewPlan+taxNewPlan;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        // _lblAmountPaid.hidden=NO;
        //_lblValueForAmountPaid.hidden=NO;
        // _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTaxNewPlan];
    }
    else
    {
        // _lblAmountPaid.hidden=NO;
        // _lblValueForAmountPaid.hidden=NO;
        
        amountDueNewPlan=[[NSString stringWithFormat:@"%.02f",finalTotalWithTaxNewPlan]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaidNewPlan;
        
        amountPaidNewPlan=[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountPaidNewPlan];
        
        
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDueNewPlan];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        
        if (isOldPlan) {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
            
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                
            }
            
        }
        
    }
    
    [self methodToSetAmountFinally];
    

}

-(void)calculateForRepairsNewPlan:(NSString *)strDiscount
{
    //Calculation for Membership Plan
    NSArray *arrTotal;
    arrTotal=[self getArrayOfTotalRepairs];
    double sumTotalNewPlan=0;
    double subTotalNewPlan,taxNewPlan,amountDueNewPlan;
    amountDueNewPlan=0;
    for(int i=0;i<arrTotal.count;i++)
    {
        sumTotalNewPlan=sumTotalNewPlan+[[arrTotal objectAtIndex:i]doubleValue];
    }
    //_lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"$%.02f",sumTotalNewPlan];
    
    [self methodTotalRepairCalculateNew];
    
    if (chkTextEditNewPlan==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscountNewPlan.text]];
    }
    strDiscount=strDiscountGlobalNewPlan;
    
    //    float amtTC = [strChargeAmountMilesNewPlan floatValue]*[_txtMilesNewPlan.text floatValue];
    //
    //    _lblMileageNewPlan.text=[NSString stringWithFormat:@"%f",amtTC];
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se
    
    subTotalNewPlan=sumTotalNewPlan-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];
    if (subTotalNewPlan>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotalNewPlan];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotalNewPlan=0.00;
    }
    
    
    double taxValueNewPlan;
    // strTax=@"10";
    taxValueNewPlan=[strTax doubleValue];
    taxNewPlan=(subTotalNewPlan*taxValueNewPlan)/100;
    taxNewPlan=[self taxCalculationNewPlan];
    if (taxNewPlan>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",taxNewPlan];
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        taxNewPlan=0.00;
    }
    
    
    //Nilind 19 July
    
    double totalNewNewPlan;
    totalNewNewPlan=subTotalNewPlan+taxNewPlan;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNewNewPlan];
    
    //End
    
    
    double  finalTotalWithTaxNewPlan;
    finalTotalWithTaxNewPlan=subTotalNewPlan+taxNewPlan;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        // _lblAmountPaid.hidden=NO;
        //_lblValueForAmountPaid.hidden=NO;
        // _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTaxNewPlan];
    }
    else
    {
        // _lblAmountPaid.hidden=NO;
        // _lblValueForAmountPaid.hidden=NO;
        
        amountDueNewPlan=[[NSString stringWithFormat:@"%.02f",finalTotalWithTaxNewPlan]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaidNewPlan;
        
        amountPaidNewPlan=[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountPaidNewPlan];
        
        
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDueNewPlan];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        
        if (isOldPlan) {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                    
            }
            
        }
        
    }
    [self methodToSetAmountFinally];
    
    [self setAmountOnTxtFld];
    

}

-(void)calculateForPartsNewPlan:(NSString*)strDiscount
{
    NSArray *arrTotal;
    arrTotal=[self getArrayOfTotalParts];
    double sumTotal=0;
    double otherDiscounts,subTotal,tax,amountDue;
    for(int i=0;i<arrTotal.count;i++)
    {
        sumTotal=sumTotal+[[arrTotal objectAtIndex:i]doubleValue];
    }
    
    totalPartAmountLocal = sumTotal;
    totalPartAmountLocalCopy = sumTotal;
    totalLaborAmountLocal=0.0;
    totalLaborAmountLocalCopy=0.0;
    
    //_lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"$%.02f",sumTotal];
    _lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"%@",_lblValueForTotalAmount.text];
    
    if (chkTextEditNewPlan==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscountNewPlan.text]];
    }
    strDiscount=strDiscountGlobalNewPlan;
    [self methodToCalulateMembershipCharge];
    
    //    float amtTC = [strChargeAmountMilesNewPlan floatValue]*[_txtMilesNewPlan.text floatValue];
    //
    //    _lblMileageNewPlan.text=[NSString stringWithFormat:@"%f",amtTC];
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se
    
    subTotal=sumTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblBillableHours.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];;
    
    if (subTotal>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    tax=[self taxCalculationForParts:subTotal :@"NewPlan"];
    
    if (tax>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    //Nilind 19 July
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    //End
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%@",strAmountPaidInSubWorkOrder];
        
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if (IsShowPricingAndNotesOnApp) {

        if (!IsAmountEdited) {

    _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
    _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

        }
        
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    } else {
        
        if (IsShowPricingAndNotesOnApp) {

            if (!IsAmountEdited) {

        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

            }
            
        }
        
    }
    [self methodToSetAmountFinally];
    
    [self setAmountOnTxtFld];
    

}

-(double)taxCalculation{
    
    double tax=0.0;
    
    NSArray *arrOfTaxMaster=[global getSalesTaxFromMaster:strCompanyKey :strDepartmentSysName :[matchesFinalSubWorkdOrder valueForKey:@"propertyType"]];
    
    if (!(arrOfTaxMaster.count==0)) {
        
        //Check According To Condition to apply tax on what
        
        BOOL isLaborTax=NO,isMaterialTax=NO,isVendorPayTax=NO,isFullTax=NO;
        
        NSString *strPartPriceType;
        
        NSDictionary *dictTaxData=arrOfTaxMaster[0];
        
        NSString *strVendorPayTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"VendorPayTax"]];
        
        if ([strVendorPayTax isEqualToString:@"true"] || [strVendorPayTax isEqualToString:@"True"] || [strVendorPayTax isEqualToString:@"1"]) {
            
            isVendorPayTax=YES;
            
        }
        
        if ([strWorkOrderAddressSubType isEqualToString:@"Commercial"]) {//Commercial Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMPartPriceType"]];
                
            }
            
        } else {//Resedential Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMPartPriceType"]];
                
            }
            
        }
        
        
        
        if (isFullTax) {
            
            float subTotal=0.0;
            
            NSArray *arrOfAllDict=[dictOfAllAmountSeparately allValues];
            
            if ([arrOfAllDict isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfAllDict.count; k++) {
                    
                    NSDictionary *dictDataTemp=arrOfAllDict[k];
                    
                    if ([dictDataTemp isKindOfClass:[NSString class]] || (dictDataTemp==nil)) {
                        
                        
                        
                    } else {
                        
                        float overallcost=[[dictDataTemp valueForKey:@"overallcost"] floatValue];
                        
                        subTotal=subTotal+overallcost;
                        
                    }
                    
                }
                
                tax=[self calculationOverAllNewReturn:subTotal];
                
            }
            
            
        } else {
            
            float subTotal=0.0,subTotalLabor=0.0,subTotalMaterial=0.0,costAdj=0.0,costPartAdj=0.0,subTotalMaterialForDiscount=0.0;
            
            NSArray *arrOfAllDict=[dictOfAllAmountSeparately allValues];
            
            if ([arrOfAllDict isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfAllDict.count; k++) {
                    
                    NSDictionary *dictDataTemp=arrOfAllDict[k];
                    
                    if ([dictDataTemp isKindOfClass:[NSString class]] || (dictDataTemp==nil)) {
                        
                        
                        
                    } else {
                        
                        float overallcost=[[dictDataTemp valueForKey:@"overallcost"] floatValue];
                        
                        subTotal=subTotal+overallcost;
                        
                        float overallcostLabor=[[dictDataTemp valueForKey:@"laborcost"] floatValue];
                        
                        subTotalLabor=subTotalLabor+overallcostLabor;
                        
                        float overallcostMaterial=[[dictDataTemp valueForKey:@"partcost"] floatValue];
                        
                        if ([strPartPriceType isEqualToString:@"Sales Price"]) {
                            
                            overallcostMaterial=[[dictDataTemp valueForKey:@"partcostsales"] floatValue];
                            
                        }
                        subTotalMaterial=subTotalMaterial+overallcostMaterial;
                        
                        subTotalMaterialForDiscount=subTotalMaterialForDiscount+[[dictDataTemp valueForKey:@"partcostsales"] floatValue];
                        
                        float overallcostAdj=[[dictDataTemp valueForKey:@"costAdjustment"] floatValue];
                        
                        costAdj=costAdj+overallcostAdj;
                        
                        // Condition change for if laborcost is 0 then no cost adjustment is to be added
                        
                        if (overallcostLabor<=0) {
                            
                            costAdj = costAdj - overallcostAdj;
                            
                        }
                        
                        float overallcostPartAdj=[[dictDataTemp valueForKey:@"partCostAdjustment"] floatValue];
                        
                        costPartAdj=costPartAdj+overallcostPartAdj;
                        
                        
                    }
                    
                }
                
                //                // Akshay Start //
                //                totalLaborAmountLocal = subTotalLabor;
                //                totalPartAmountLocal =  subTotalMaterialForDiscount;
                //                totalLaborAmountLocalCopy = subTotalLabor;
                //                totalPartAmountLocalCopy = subTotalMaterialForDiscount;
                //                // Akshay End //
                
                tax=[self calculationOverAllNewLaborMaterialReturn:subTotal :subTotalLabor :subTotalMaterial :isLaborTax :isMaterialTax :costAdj :costPartAdj];
                
            }
            
        }
        
    }
    
    return tax;
}
-(double)taxCalculationNewPlan{
    
    double tax=0.0;
    
    NSArray *arrOfTaxMaster=[global getSalesTaxFromMaster:strCompanyKey :strDepartmentSysName :[matchesFinalSubWorkdOrder valueForKey:@"propertyType"]];
    
    if (!(arrOfTaxMaster.count==0)) {
        
        //Check According To Condition to apply tax on what
        
        BOOL isLaborTax=NO,isMaterialTax=NO,isVendorPayTax=NO,isFullTax=NO;
        
        NSString *strPartPriceType;
        
        NSDictionary *dictTaxData=arrOfTaxMaster[0];
        
        NSString *strVendorPayTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"VendorPayTax"]];
        
        if ([strVendorPayTax isEqualToString:@"true"] || [strVendorPayTax isEqualToString:@"True"] || [strVendorPayTax isEqualToString:@"1"]) {
            
            isVendorPayTax=YES;
            
        }
        
        if ([strWorkOrderAddressSubType isEqualToString:@"Commercial"]) {//Commercial Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMPartPriceType"]];
                
            }
            
        } else {//Resedential Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMPartPriceType"]];
                
            }
            
        }
        
        
        
        if (isFullTax) {
            
            float subTotal=0.0;
            
            NSArray *arrOfAllDict=[dictOfAllAmountSeparately allValues];
            
            if ([arrOfAllDict isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfAllDict.count; k++) {
                    
                    NSDictionary *dictDataTemp=arrOfAllDict[k];
                    
                    if ([dictDataTemp isKindOfClass:[NSString class]] || (dictDataTemp==nil)) {
                        
                        
                        
                    } else {
                        
                        float overallcost=[[dictDataTemp valueForKey:@"overallcost"] floatValue];
                        
                        subTotal=subTotal+overallcost;
                        
                    }
                    
                }
                
                tax=[self calculationOverAllNewReturnNewPlan:subTotal];
                
            }
            
            
        } else {
            
            float subTotal=0.0,subTotalLabor=0.0,subTotalMaterial=0.0,costAdj=0.0,costPartAdj=0.0,subTotalMaterialForDiscount=0.0;
            
            NSArray *arrOfAllDict=[dictOfAllAmountSeparately allValues];
            
            if ([arrOfAllDict isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfAllDict.count; k++) {
                    
                    NSDictionary *dictDataTemp=arrOfAllDict[k];
                    
                    if ([dictDataTemp isKindOfClass:[NSString class]] || (dictDataTemp==nil)) {
                        
                        
                        
                    } else {
                        
                        float overallcost=[[dictDataTemp valueForKey:@"overallcost"] floatValue];
                        
                        subTotal=subTotal+overallcost;
                        
                        float overallcostLabor=[[dictDataTemp valueForKey:@"laborcost"] floatValue];
                        
                        subTotalLabor=subTotalLabor+overallcostLabor;
                        
                        float overallcostMaterial=[[dictDataTemp valueForKey:@"partcost"] floatValue];
                        
                        if ([strPartPriceType isEqualToString:@"Sales Price"]) {
                            
                            overallcostMaterial=[[dictDataTemp valueForKey:@"partcostsales"] floatValue];
                            
                        }
                        subTotalMaterial=subTotalMaterial+overallcostMaterial;
                        
                        subTotalMaterialForDiscount=subTotalMaterialForDiscount+[[dictDataTemp valueForKey:@"partcostsales"] floatValue];
                        
                        float overallcostAdj=[[dictDataTemp valueForKey:@"costAdjustment"] floatValue];
                        
                        costAdj=costAdj+overallcostAdj;
                        
                        // Condition change for if laborcost is 0 then no cost adjustment is to be added
                        
                        if (overallcostLabor<=0) {
                            
                            costAdj = costAdj - overallcostAdj;
                            
                        }
                        
                        float overallcostPartAdj=[[dictDataTemp valueForKey:@"partCostAdjustment"] floatValue];
                        
                        costPartAdj=costPartAdj+overallcostPartAdj;
                        
                        
                    }
                    
                }
                
                //                // Akshay Start //
                //                totalLaborAmountLocal = subTotalLabor;
                //                totalPartAmountLocal =  subTotalMaterialForDiscount;
                //                totalLaborAmountLocalCopy = subTotalLabor;
                //                totalPartAmountLocalCopy = subTotalMaterialForDiscount;
                //                // Akshay End //
                
                tax=[self calculationOverAllNewLaborMaterialReturnNewPlan:subTotal :subTotalLabor :subTotalMaterial :isLaborTax :isMaterialTax :costAdj :costPartAdj];
                
            }
            
            
            
        }
        
    }
    
    return tax;
}

-(void)methodToCalulateMembershipCharge{
    
    
    if ((strAccountPSPId.length==0) || (strAccountPSPId==0)) {
        
        double memberShipDiscout,memberShipCharge,value;
        memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
        memberShipDiscout=[strPSPDiscountPercent1 doubleValue];
        
        value=memberShipCharge*(memberShipDiscout/100);
        strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
        _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%.02f",value];
        _lblValueForPSPCharge.text=[NSString stringWithFormat:@"$%@",@"00.00"];
        
    } else {
        
        double memberShipDiscout,memberShipCharge,value;
        memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
        memberShipDiscout=[strPSPDiscountPercent1 doubleValue];
        
        value=memberShipCharge*(memberShipDiscout/100);
        strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
        _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%.02f",value];
        _lblValueForPSPCharge.text=[NSString stringWithFormat:@"$%@",@"00.00"];
        strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
        
    }
    
    
    double memberShipDiscout2,memberShipCharge2,value2;
    memberShipCharge2=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
    memberShipDiscout2=[strPSPDiscountPercent2 doubleValue];
    
    value2=memberShipCharge2*(memberShipDiscout2/100);
    strPSPDiscountAmount2=[NSString stringWithFormat:@"%.02f",value2];
    _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value2];
    
    
    _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",strPSPCharge2];
    double tempValueTemporary=[strPSPCharge2 doubleValue];
    _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary];
    
    
}

-(void)methodToSetAmountFinally{
    
    if (isFirstPlanSelected) {
        
        if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
        {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        }else{
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
            
        }
        
    } else {
        
        if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
        {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        }else{
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                
            }
            
        }
    }
    

}


-(double)calculationOverAllNewReturn :(float)subTotal{
    
    double tax= 0.0,amountDue;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    strDiscount=strDiscountGlobal;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se
    
    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForPSPCharge.text]-[self getNumberString:_lblValueForPSPDiscount.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txt_MiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
            
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                
            }
            
        }
        
    }
    [self methodToSetAmountFinally];
    

    return tax;
}

-(double)calculationOverAllNewLaborMaterialReturnNewPlan :(float)subTotal :(float)subTotalLabor :(float)subTotalMaterial :(BOOL)isLaborTax :(BOOL)isMaterialTax :(float)strCostAdj :(float)strPartCostAdj{
    
    double tax = 0.0,amountDue,taxMaterial = 0.0,taxLabor = 0.0;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscountNewPlan.text]];
    }
    
    strDiscount=strDiscountGlobalNewPlan;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se
    
    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];
    if (subTotal>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    
    
    if (isLaborTax) {
        
        float laborAppliedCredits = 0.0;
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            if([[dict valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnLaborPrice"] boolValue]==true)
            {
                laborAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
        }
        
        //[self calculateTripChargeBasedOnQty];
        
        // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se
        
        float subTotalLaborTemp=[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+subTotalLabor+strCostAdj+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-([strDiscount doubleValue]+laborAppliedCredits); //-[strDiscount doubleValue]
        
        if (subTotalLaborTemp>0) {
            
            taxLabor=(subTotalLaborTemp*taxValue)/100;
            
        }
        
    }
    
    if (isMaterialTax) {
        
        float subTotalMaterialTemp=subTotalMaterial+strPartCostAdj;
        
        if (subTotalMaterialTemp>0) {
            
            taxMaterial=(subTotalMaterialTemp*taxValue)/100;
            
        }
        
    }
    
    
    tax=taxMaterial+taxLabor;
    
    if (tax>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
            
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                
            }
            
        }
        
    }
    [self methodToSetAmountFinally];


    return tax;
}
-(NSArray *)getArrayOfTotalRepairs
{
    NSString *strIssueRepairIdToCheck;
    NSMutableArray *arrTotal,*arrTemp;
    arrTotal=[[NSMutableArray alloc]init];
    arrTemp=[[NSMutableArray alloc]init];
    
    //Nilind
    
    for (int i=0; i<arrOfSubWorkServiceIssues.count; i++)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[i];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                
            }
        }
    }
    
    //End
    
    
    for (int k=0; k<arrTemp.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrTemp[k];
        
        NSString *strnonStdRepairAmt=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"nonStdRepairAmt"]];
        NSString *strnonStdLaborAmt=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"nonStdLaborAmt"]];
        NSString *strnonStdPartAmt=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"nonStdPartAmt"]];
        
        NSString *strRepairMasterIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"repairMasterId"]];
        
        if ([[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"1"]|| [[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"true"])
        {
            
            strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"issueRepairId"]];
            
            //  [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical:[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]] :strIssueRepairIdToCheck];
            //Parts Caculation
            NSMutableArray *arrTempParts=[[NSMutableArray alloc]init];
            
            float totalCostPartss=0;
            
            for (int j=0; j<arrOfSubWorkServiceIssuesRepairParts.count; j++)
            {
                
                
                NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairParts[j];
                NSString *strAddedAfterApproval=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isAddedAfterApproval"]];
                
                if ([strIssueRepairIdToCheck isEqualToString:[objTemp valueForKey:@"issueRepairId"]]) {
                    
                    if ([strAddedAfterApproval isEqualToString:@"0"]||[strAddedAfterApproval isEqualToString:@"false"]||[strAddedAfterApproval isEqualToString:@"False"])
                    {
                        
                        
                        NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[objTemp valueForKey:@"partName"],[objTemp valueForKey:@"qty"]];
                        //multiplier   qty  unitPrice
                        
                        NSString *strMulti=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"multiplier"]];
                        float multip=[strMulti floatValue];
                        
                        NSString *strqtyy=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
                        float qtyy=[strqtyy floatValue];
                        
                        NSString *strunitPricee=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"unitPrice"]];
                        float unitPricee=[strunitPricee floatValue];
                        
                        float finalPriceParts=unitPricee*qtyy*multip;
                        
                        totalCostPartss=totalCostPartss+finalPriceParts;
                        
                        [arrTempParts addObject:strPartsnQty];
                        
                    }
                    
                }
                
            }
            
            // [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :strIssueRepairIdToCheck];
            
            float totalCostAdjustment=0;
            
            NSString *strcostAdjustment=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"costAdjustment"]];
            
            totalCostAdjustment=[strcostAdjustment floatValue];
            
            NSString *strPartCostAdjustment=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"partCostAdjustment"]];
            
            totalCostAdjustment=totalCostAdjustment+[strPartCostAdjustment floatValue];
            
            float totalCostLaborss=0;
            
            NSString *strLaborHrs,*strHelperHrs;
            for (int j=0; j<arrOfSubWorkServiceIssuesRepairLabour.count; j++)
            {
                
                NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairLabour[j];
                
                if ([strIssueRepairIdToCheck isEqualToString:[objTemp valueForKey:@"issueRepairId"]]) {
                    
                    NSString *strWarranty=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isWarranty"]];
                    if ([strWarranty isEqualToString:@"0"]||[strWarranty isEqualToString:@"false"]||[strWarranty isEqualToString:@"False"])
                    {
                        NSString *strLaborCostt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborCost"]];
                        float laborCostt=[strLaborCostt floatValue];
                        
                        totalCostLaborss=totalCostLaborss+laborCostt;
                        
                        NSString *strLaborType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborType"]];
                        
                        if ([strLaborType isEqualToString:@"H"])
                        {
                            
                            strHelperHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                            
                        }
                        else
                        {
                            
                            strLaborHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                            
                        }
                        
                    }
                }
                
            }
            
            float overAllAmt=totalCostPartss+totalCostLaborss+totalCostAdjustment;
            
            // Condition change for if laborcost is 0 then no cost adjustment is to be added
            
            if (totalCostLaborss<=0) {
                
                overAllAmt = overAllAmt-[strcostAdjustment floatValue];
                
            }
            
            if (strRepairMasterIdToCheck.length==0) {
                
                if ((totalCostPartss==0) && (totalCostLaborss==0)) {
                    
                    if ([strnonStdRepairAmt isEqualToString:@"0"]) {
                        
                        strnonStdRepairAmt=@"";
                        
                    }
                    
                    if (strnonStdRepairAmt.length==0) {
                        
                        overAllAmt=overAllAmt+[strnonStdLaborAmt floatValue]+[strnonStdPartAmt floatValue];
                        
                    } else {
                        
                        overAllAmt=overAllAmt+[strnonStdRepairAmt floatValue];
                        
                    }
                    
                }
                
            }
            
            [arrTotal addObject:[NSString stringWithFormat:@"%.02f",overAllAmt]];
            
            totalLaborAmountLocal=totalCostLaborss;
            totalPartAmountLocal=totalCostPartss;
            totalLaborAmountLocalCopy=totalCostLaborss;
            totalPartAmountLocalCopy=totalCostPartss;
            
        }
        
        NSLog(@"ARRAY TOTAL %@",arrTotal);
    }
    return  arrTotal;
    
}

-(double)calculationOverAllNewLaborMaterialReturn :(float)subTotal :(float)subTotalLabor :(float)subTotalMaterial :(BOOL)isLaborTax :(BOOL)isMaterialTax :(float)strCostAdj :(float)strPartCostAdj{
    
    double tax = 0.0,amountDue,taxMaterial = 0.0,taxLabor = 0.0;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    strDiscount=strDiscountGlobal;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se
    
    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForPSPCharge.text]-[self getNumberString:_lblValueForPSPDiscount.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txt_MiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    
    
    if (isLaborTax) {
        
        float laborAppliedCredits = 0.0;
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            if([[dict valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnLaborPrice"] boolValue]==true)
            {
                laborAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
        }
        
        //[self calculateTripChargeBasedOnQty];
        
        // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se
        
        float subTotalLaborTemp=[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForPSPCharge.text]-[self getNumberString:_lblValueForPSPDiscount.text]+[self getNumberString:_lblMileage.text]+subTotalLabor+strCostAdj+[self getNumberString:_txt_MiscellaneousCharge.text]-([strDiscount doubleValue]+laborAppliedCredits); //-[strDiscount doubleValue]
        
        if (subTotalLaborTemp>0) {
            
            taxLabor=(subTotalLaborTemp*taxValue)/100;
            
        }
        
    }
    
    if (isMaterialTax) {
        
        float subTotalMaterialTemp=subTotalMaterial+strPartCostAdj;
        
        if (subTotalMaterialTemp>0) {
            
            taxMaterial=(subTotalMaterialTemp*taxValue)/100;
            
        }
        
    }
    
    
    tax=taxMaterial+taxLabor;
    
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
            
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                
            }
            
        }
        
    }
    [self methodToSetAmountFinally];
    

    return tax;
}

-(NSArray *)getArrayOfTotalParts
{
    NSString *strIssueRepairIdToCheck;
    NSMutableArray *arrTotal,*arrTemp;
    arrTotal=[[NSMutableArray alloc]init];
    arrTemp=[[NSMutableArray alloc]init];
    
    //Nilind
    
    for (int i=0; i<arrOfSubWorkServiceIssues.count; i++)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[i];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                
            }
        }
    }
    
    //End
    
    for (int k=0; k<arrTemp.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrTemp[k];
        
        
        NSManagedObject *objTemp=arrTemp[k];
        
        if ([[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"1"]|| [[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@""])
        {
            NSString *strMulti=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"multiplier"]];
            float multip=[strMulti floatValue];
            
            NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"qty"]];
            float qtyy=[strqtyy floatValue];
            
            NSString *strunitPricee=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"unitPrice"]];
            float unitPricee=[strunitPricee floatValue];
            
            float finalPriceParts=unitPricee*qtyy*multip;
            float totalCostPartss=0;
            totalCostPartss=totalCostPartss+finalPriceParts;
            [arrTotal addObject:[NSString stringWithFormat:@"%.02f",totalCostPartss]];
            
        }
        
        NSLog(@"ARRAY TOTAL %@",arrTotal);
        
    }
    return arrTotal;
    
}


-(double)calculationOverAllNewReturnNewPlan :(float)subTotal{
    
    double tax= 0.0,amountDue;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    strDiscount=strDiscountGlobalNewPlan;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se
    
    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];
    if (subTotal>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    if (tax>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
            
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                
            }
            
        }
        
    }
    [self methodToSetAmountFinally];
    

    return tax;
}


#pragma mark-  -------------------- GET PLAN -------------------

-(void)deletePspPlansFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityPspPlans=[NSEntityDescription entityForName:@"PspPlans" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"PspPlans" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ && companyKey = %@ && empId = %@ && userName = %@",[NSString stringWithFormat:@"%@",[matchesFinalSubWorkdOrder valueForKey:@"accountNo"]],strCompanyKey,strEmpID,strUserName];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(NSArray*)fetchPspPlansFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityPspPlans=[NSEntityDescription entityForName:@"PspPlans" inManagedObjectContext:context];
    requestPspPlans= [[NSFetchRequest alloc] init];
    [requestPspPlans setEntity:entityPspPlans];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ && companyKey = %@ && empId = %@ && userName = %@",[NSString stringWithFormat:@"%@",[matchesFinalSubWorkdOrder valueForKey:@"accountNo"]],strCompanyKey,strEmpID,strUserName];
    
    [requestPspPlans setPredicate:predicate];
    
    sortDescriptorPspPlans = [[NSSortDescriptor alloc] initWithKey:@"accountNo" ascending:NO];
    sortDescriptorsPspPlans = [NSArray arrayWithObject:sortDescriptorPspPlans];
    
    [requestPspPlans setSortDescriptors:sortDescriptorsPspPlans];
    
    self.fetchedResultsControllerPspPlans = [[NSFetchedResultsController alloc] initWithFetchRequest:requestPspPlans managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerPspPlans setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerPspPlans performFetch:&error];
    arrAllObjPspPlans = [self.fetchedResultsControllerPspPlans fetchedObjects];
    
    return arrAllObjPspPlans;
    
}

-(void)getPlan
{
    //  [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
        
        NSArray *arrTemp =  [self fetchPspPlansFromDB];
        
        if (arrTemp.count>0) {
            
            NSManagedObject *objTemp = arrTemp[0];
            
            NSArray *arrPlansDB = [objTemp valueForKey:@"arrOfPlans"];
            
            [self setPlansNewMethod:arrPlansDB : @"Offline"];
            
        } else {
            
            [self noInternetThenPSP];
            
        }
        
    }
    else
    {
        NSString *strUrl;
        strUrl = [NSString stringWithFormat:@"%@%@%@&departmentSysName=%@&accountNo=%@",strServiceUrlMainServiceAutomation,UrlGetPlanMechanical,strCompanyKey,strDepartmentSysName,[NSString stringWithFormat:@"%@",[matchesFinalSubWorkdOrder valueForKey:@"accountNo"]]],
        
        NSLog(@"GET PLAN  URl-----%@",strUrl);
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        NSURL *url = [NSURL URLWithString:strUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        
        if (jsonData==nil)
        {
            
            NSString *strTitle = Alert;
            NSString *strMsg = Sorry;
            [global AlertMethod:strTitle :strMsg];
            [DejalActivityView removeView];
            [DejalBezelActivityView removeView];
            
        }
        else
        {
            
            NSArray* responseArr = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            [self setPlansNewMethod:responseArr : @"Online"];
            
        }
    }
}

-(void)setPlansNewMethod :(NSArray*)responseArr :(NSString*)strType{
    
    if (responseArr.count==0)
    {
        
        [self noInternetThenPSP];
        
        
    }
    else
    {
        
        if ([strType isEqualToString:@"Online"]) {
            
            // Deleting already existing plans from DB
            
            [self deletePspPlansFromDB];
            
            // Saving Plans Into DB
            
            entityPspPlans=[NSEntityDescription entityForName:@"PspPlans" inManagedObjectContext:context];
            
            PspPlans *objPspPlans = [[PspPlans alloc]initWithEntity:entityPspPlans insertIntoManagedObjectContext:context];
            
            objPspPlans.companyKey=strCompanyKey;
            objPspPlans.accountNo=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkdOrder valueForKey:@"accountNo"]];
            objPspPlans.arrOfPlans=responseArr;
            objPspPlans.empId=strEmpID;
            objPspPlans.userName=strUserName;
            
            NSError *error2;
            [context save:&error2];
            
        }
        
        NSDictionary *dict;
        arrPlanStatusResponse=responseArr;
        if(responseArr.count==1)
        {
            dict=[responseArr objectAtIndex:0];
            
            _lblNewMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPTitle"]];
            [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
            
            _lblNoMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPTitle"]];
            [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
            
            NSString *strPurchaseStepOrder;
            strPurchaseStepOrder=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PurchaseStepOrder"]];
            
            strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            //  strAccountPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForPSPCharge.text]];
            strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForPSPDiscount.text]];
            
            
            strPspMasterId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            strAccountPSPId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            // strPSPCharge1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPCharges"]];
            strPSPDiscountPercent1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
            
            double memberShipDiscout,memberShipCharge,value;
            memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
            memberShipDiscout=[strPSPDiscountPercent1 doubleValue];
            
            value=memberShipCharge*(memberShipDiscout/100);
            strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
            _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%.02f",value];
            
            if (strAccountPSPId1.length==0) {
                
                //   _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                
            }else{
                
                //   _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",@"00.00"];
                
            }
            if ([strAccountPSPId1 isEqualToString:@"0"]) {
                
                //  _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                
            }
            
            
            // [self fetchPSPMasters:strPurchaseStepOrder];
            [self fetchMemberShipPlans:strPurchaseStepOrder];
            
            if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
                
                _txtAmountCheckView.text=@"";
                _txtAmountSingleAmount.text=@"";
                
            } else {
                
                if (IsShowPricingAndNotesOnApp) {

                    if (!IsAmountEdited) {

                _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
                _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                    }
                    
                }
                    
            }
            
            isOldPlan=NO;
            
            _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
            _viewOld.layer.borderWidth=2.0;
            _viewOld.layer.cornerRadius=5.0;
            
            _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
            _viewNew.layer.borderWidth=2.0;
            _viewNew.layer.cornerRadius=5.0;
            
            _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
            _viewNew.backgroundColor=[UIColor whiteColor];
            
            
            [_lblNewMembershipPlan setHidden:YES];
            [_btnNewPlan setHidden:YES];
            [_btnSelectPlan setHidden:YES];
            [_viewNew setHidden:YES];
            
        }
        else if(responseArr.count==2)
        {
            
            [_lblNewMembershipPlan setHidden:NO];
            [_btnNewPlan setHidden:NO];
            [_btnSelectPlan setHidden:NO];
            [_viewNew setHidden:NO];
            
            dict=[responseArr objectAtIndex:0];
            
            _lblNoMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPTitle"]];
            [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
            //   strAccountPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            
            strPspMasterId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            strAccountPSPId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            strPSPCharge1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPCharges"]];
            
            NSString *strPSP = [NSString stringWithFormat:@"%@",[matchesFinalSubWorkdOrder valueForKey:@"pSPMasterId"]];
            
            if (strPSP.length>0) {
                
                
                
            }else{
                
//                strPSPCharge1 = @"";
//                _lblValueForPSPCharge.text = @"$00.00";
                
            }
            
            strPSPDiscountPercent1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
            
            double memberShipDiscout,memberShipCharge,value;
            memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
            memberShipDiscout=[strPSPDiscountPercent1 doubleValue];
            
            value=memberShipCharge*(memberShipDiscout/100);
            strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
            _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%.02f",value];
            if (strAccountPSPId1.length==0) {
                
                // _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                
            }else{
                
                // _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",@"00.00"];
                
            }
            
            if ([strAccountPSPId1 isEqualToString:@"0"]) {
                
                // _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                
            }
            
            // For Second IndexPath
            
            dict=[responseArr objectAtIndex:1];
            
            _lblNewMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPTitle"]];
            [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
            
            NSString *strPurchaseStepOrder;
            strPurchaseStepOrder=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PurchaseStepOrder"]];
            
            strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForPSPCharge.text]];
            strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForPSPDiscount.text]];
            
            [_btnSelectPlan setTitle:[dict valueForKey:@"PSPTitle"] forState:UIControlStateNormal];
            
            
            strPspMasterId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            strAccountPSPId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            strPSPCharge2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPCharges"]];
            strPSPDiscountPercent2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
            
            double memberShipDiscout2,memberShipCharge2,value2;
            memberShipCharge2=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
            memberShipDiscout2=[strPSPDiscountPercent2 doubleValue];
            
            value2=memberShipCharge2*(memberShipDiscout2/100);
            strPSPDiscountAmount2=[NSString stringWithFormat:@"%.02f",value2];
            _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value2];
            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
            double tempValueTemporary=[[dict valueForKey:@"PSPCharges"] doubleValue];
            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary];
            
            if (strPSP.length>0) {
                
                
                
            }else{
                
               // strPSPCharge2 = @"";
                //_lblValueForMembershipChargeNewPaln.text = @"$00.00";
                
            }
            
            [self fetchMemberShipPlans:strPurchaseStepOrder];
            
            _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
            _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
            _viewOld.layer.borderWidth=2.0;
            _viewOld.layer.cornerRadius=5.0;
            
            _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
            _viewNew.backgroundColor=[UIColor whiteColor];
            _viewNew.layer.borderWidth=2.0;
            _viewNew.layer.cornerRadius=5.0;
            
            if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
                
                _txtAmountCheckView.text=@"";
                _txtAmountSingleAmount.text=@"";
                
            } else {
                
                if (IsShowPricingAndNotesOnApp) {

                    if (!IsAmountEdited) {

                _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
                _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                    }
                    
                }
                
            }
            
            //NSString *strPSP = [NSString stringWithFormat:@"%@",[matchesFinalSubWorkdOrder valueForKey:@"pSPMasterId"]];
            
            if (strPSP.length>0) {
                
                // Change for showing selection matchesFinalSubWorkorder
                
                if ([[matchesFinalSubWorkdOrder valueForKey:@"pSPMasterId"] isEqualToString:strPspMasterId2]) {
                    
                    isFirstPlan=NO;
                    isFirstPlanSelected=NO;
                    _viewNew.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
                    _viewNew.layer.borderWidth=2.0;
                    _viewNew.layer.cornerRadius=5.0;
                    
                    _viewOld.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                    _viewOld.layer.borderWidth=2.0;
                    _viewOld.layer.cornerRadius=5.0;
                    
                    _viewNew.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
                    _viewOld.backgroundColor=[UIColor whiteColor];
                    
                    chkPlanClick=YES;
                    
                    [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
                    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
                    
                     strPSPCharge2 = @"";
                    _lblValueForMembershipChargeNewPaln.text = @"$00.00";
                    
                } else if ([[matchesFinalSubWorkdOrder valueForKey:@"pSPMasterId"] isEqualToString:strPspMasterId1]){
                    
                    isFirstPlanSelected=YES;
                    isFirstPlan=YES;
                    _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
                    _viewOld.layer.borderWidth=2.0;
                    _viewOld.layer.cornerRadius=5.0;
                    
                    _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                    _viewNew.layer.borderWidth=2.0;
                    _viewNew.layer.cornerRadius=5.0;
                    
                    _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
                    _viewNew.backgroundColor=[UIColor whiteColor];
                    
                    chkPlanClick=YES;
                    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
                    [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
                    
                    strPSPCharge1 = @"";
                    _lblValueForPSPCharge.text= @"$00.00";
                    
                }else{
                    
                    BOOL isFound;
                    
                    isFound = NO;
                    
                    for (int k=0; k<arrPSPMasters.count; k++) {
                        
                        NSDictionary *dictMasters = arrPSPMasters[k];
                        
                        NSString *strPspMasterId = [NSString stringWithFormat:@"%@",[dictMasters valueForKey:@"PSPMasterId"]];
                        
                        if ([strPspMasterId isEqualToString:[matchesFinalSubWorkdOrder valueForKey:@"pSPMasterId"]]) {
                            
                            dict=dictMasters;
                            
                            NSMutableArray *arrTempValuesMasters = [[NSMutableArray alloc]init];
                            
                            [arrTempValuesMasters addObjectsFromArray:arrPlanStatusResponse];
                            
                            [arrTempValuesMasters replaceObjectAtIndex:1 withObject:dict];
                            
                            arrPlanStatusResponse = arrTempValuesMasters;
                            
                            _lblNewMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
                            [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
                            
                            strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
                            strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForPSPCharge.text]];
                            strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForPSPDiscount.text]];
                            
                            [_btnSelectPlan setTitle:[dict valueForKey:@"Title"] forState:UIControlStateNormal];
                            
                            strPspMasterId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
                            strAccountPSPId2=[NSString stringWithFormat:@"%@",@""];
                            strPSPCharge2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitCharges"]];
                            strPSPDiscountPercent2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
                            
                            double memberShipDiscout2,memberShipCharge2,value2;
                            memberShipCharge2=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
                            memberShipDiscout2=[strPSPDiscountPercent2 doubleValue];
                            
                            value2=memberShipCharge2*(memberShipDiscout2/100);
                            strPSPDiscountAmount2=[NSString stringWithFormat:@"%.02f",value2];
                            _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value2];
                            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
                            double tempValueTemporary=[[dict valueForKey:@"UnitCharges"] doubleValue];
                            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary];
                            
                            strPSPCharge2 = @"";
                            _lblValueForMembershipChargeNewPaln.text = @"$00.00";
                            
                            isFirstPlan=NO;
                            isFirstPlanSelected=NO;
                            _viewNew.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
                            _viewNew.layer.borderWidth=2.0;
                            _viewNew.layer.cornerRadius=5.0;
                            
                            _viewOld.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                            _viewOld.layer.borderWidth=2.0;
                            _viewOld.layer.cornerRadius=5.0;
                            
                            _viewNew.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
                            _viewOld.backgroundColor=[UIColor whiteColor];
                            
                            chkPlanClick=YES;
                            
                            [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
                            [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
                            
                            isFound = YES;
                            
                            break ;
                            
                        }
                        
                        
                    }
                    
                    if (!isFound) {
                        
                        [self noInternetThenPSP];
                        
                    }
                    
                }
                
            }
            
            isOldPlan=YES;
            
        }
        
        [self methodCalculationOverAll];
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
        
    }
    [DejalActivityView removeView];
    [DejalBezelActivityView removeView];
    

}


-(void)noInternetThenPSP{
    
    [_viewNew setHidden:YES];
    [_btnNewPlan setHidden:YES];
    [_lblNewMembershipPlan setHidden:YES];
    _lblNoMembershipPlan.text=@"No Membership Plan";
    [_btnNoMembership setEnabled:NO];
    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
    
    _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
    _viewOld.layer.borderWidth=2.0;
    _viewOld.layer.cornerRadius=5.0;
    _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
    
    strPspMasterId2=@"";
    strAccountPSPId2=@"";
    strPspMasterId1=@"";
    strAccountPSPId1=@"";
    
}

-(void)addingMiscellenousChargeNewPlan{
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    
}


-(void)setAmountOnTxtFld{
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"redio_button_2.png"];
    
    UIImage *img = [_btnNoMembership imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        if (IsShowPricingAndNotesOnApp) {

            if (!IsAmountEdited) {

        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

            }
            
        }
        
        if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];

                }
                
            }
            
        }
        
    }else{
        
        if (IsShowPricingAndNotesOnApp) {

            if (!IsAmountEdited) {

        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

            }
            
        }
        
        if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0) {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                
            }
            
        }
        
    }
    

}


-(void)fetchIfMemberShipPresentInWorkOrder{
    
    strPSPCharge=[matchesFinalSubWorkdOrder valueForKey:@"pSPCharges"];
    strPSPDiscount=[matchesFinalSubWorkdOrder valueForKey:@"pSPDiscount"];
    strAccountPSPId=[matchesFinalSubWorkdOrder valueForKey:@"accountPSPId"];
    strAccountPSPId1=[matchesFinalSubWorkdOrder valueForKey:@"accountPSPId"];
    strPspMasterId=[matchesFinalSubWorkdOrder valueForKey:@"pSPMasterId"];
    
    NSString *strPurchaseStepOrder;
    strPurchaseStepOrder = @"0";
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    arrMechanicalPSPMasters=[[NSMutableArray alloc]init];
    arrPSPMasters=[[NSMutableArray alloc]init];
    arrMechanicalPSPMasters=[dictMechanicalMasters valueForKey:@"PSPMasterExtSerDc"];
    NSMutableArray *arrTemp;
    arrTemp=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrMechanicalPSPMasters objectAtIndex:i];
        NSString *strType;
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Type"]];
        if([strDepartmentSysName isEqualToString:[dict valueForKey:@"DepartmentSysName"]]&&[strType isEqualToString:@"General"])
        {
            //PSPMasterId
            
            NSString *strLocalPspMasterId = [NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            
            if ([strLocalPspMasterId isEqualToString:strPspMasterId]) {
                
                strPurchaseStepOrder = [NSString stringWithFormat:@"%@",[dict valueForKey:@"StepOrder"]];
                
            }else{
                
                // [arrTemp addObject:dict];
                
            }
            
            [arrTemp addObject:dict];
            
        }
    }
    
    
    for(int i=0;i<arrTemp.count;i++)
    {
        NSDictionary *dict=[arrTemp objectAtIndex:i];
        
        NSString *strStepOrder=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StepOrder"]];
        
        int stepOrderInMaster=[strStepOrder intValue];
        
        int strPurchaseStepOrderPresent=[strPurchaseStepOrder intValue];
        
        if (stepOrderInMaster>=strPurchaseStepOrderPresent) {
            
            [arrPSPMasters addObject:dict];
            
        }
        
    }
    
    if (arrPSPMasters.count>0) {
        
        NSDictionary *dict;
        dict=[arrPSPMasters objectAtIndex:0];
        
        _lblNewMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
        [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
        
        _lblNoMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
        [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
        
        //            strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
        //            strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForPSPCharge.text]];
        //            strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForPSPDiscount.text]];
        
        strPspMasterId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
        strPSPDiscountPercent1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];//AccountPSPId
        //strAccountPSPId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
        
        double memberShipDiscout,memberShipCharge,value;
        memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
        memberShipDiscout=[strPSPDiscountPercent1 doubleValue];
        
        value=memberShipCharge*(memberShipDiscout/100);
        strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
        _lblValueForPSPDiscount.text=[NSString stringWithFormat:@"$%.02f",value];
        
        if (strAccountPSPId1.length==0) {
            
            //   _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
            
        }else{
            
            //   _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",@"00.00"];
            
        }
        if ([strAccountPSPId1 isEqualToString:@"0"]) {
            
            //  _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
            
        }
        
        if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        } else {
            
            if (IsShowPricingAndNotesOnApp) {

                if (!IsAmountEdited) {

            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                }
                
            }
            
        }
        
        isOldPlan=NO;
        
        _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
        _viewOld.layer.borderWidth=2.0;
        _viewOld.layer.cornerRadius=5.0;
        
        _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
        _viewNew.layer.borderWidth=2.0;
        _viewNew.layer.cornerRadius=5.0;
        
        _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
        _viewNew.backgroundColor=[UIColor whiteColor];
        
        
        [_lblNewMembershipPlan setHidden:YES];
        [_btnNewPlan setHidden:YES];
        [_btnSelectPlan setHidden:YES];
        [_viewNew setHidden:YES];
        
        if (arrPSPMasters.count>1) {
            
            NSDictionary *dict;
            
            [_lblNewMembershipPlan setHidden:NO];
            [_btnNewPlan setHidden:NO];
            [_btnSelectPlan setHidden:NO];
            [_viewNew setHidden:NO];
            
            dict=[arrPSPMasters objectAtIndex:1];
            
            _lblNewMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
            [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
            [_btnSelectPlan setTitle:[dict valueForKey:@"Title"] forState:UIControlStateNormal];
            
            
            strPspMasterId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            strPSPCharge2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitCharges"]];
            strPSPDiscountPercent2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
            //strAccountPSPId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            
            double memberShipDiscout2,memberShipCharge2,value2;
            memberShipCharge2=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
            memberShipDiscout2=[strPSPDiscountPercent2 doubleValue];
            
            value2=memberShipCharge2*(memberShipDiscout2/100);
            strPSPDiscountAmount2=[NSString stringWithFormat:@"%.02f",value2];
            _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value2];
            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
            double tempValueTemporary=[[dict valueForKey:@"UnitCharges"] doubleValue];
            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary];
            
            
            _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
            _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
            _viewOld.layer.borderWidth=2.0;
            _viewOld.layer.cornerRadius=5.0;
            
            _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
            _viewNew.backgroundColor=[UIColor whiteColor];
            _viewNew.layer.borderWidth=2.0;
            _viewNew.layer.cornerRadius=5.0;
            
            if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0) {
                
                _txtAmountCheckView.text=@"";
                _txtAmountSingleAmount.text=@"";
                
            } else {
                
                if (IsShowPricingAndNotesOnApp) {

                    if (!IsAmountEdited) {

                _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
                _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];

                    }
                    
                }
                
            }
            
            isOldPlan=YES;
            
        }
        
        //[arrPSPMasters removeObjectAtIndex:0];
        
    } else {
        
        [self noInternetThenPSP];
        
    }
    

}

- (IBAction)actionOnPaymentTypeCollapseExpand:(id)sender{
    
    if ([_btnPaymentTypeCollapseExpand.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnPaymentTypeCollapseExpand setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        isPaymentTypeCollapseExpand=YES;
        [self addView:@"Notadd"];
    
    } else {
        
        //Band Hoga
        [_btnPaymentTypeCollapseExpand setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        isPaymentTypeCollapseExpand=NO;
        [self addView:@"Notadd"];
        
    }
    
}

-(void)showHideAmountView{
    
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        //[_viewForSingleAmount setHidden:NO];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Cash"])
    {
        //[_viewForSingleAmount setHidden:NO];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Bill"])
    {
        [_viewForSingleAmount setHidden:YES];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"AutoChargeCustomer"])
    {
        [_viewForSingleAmount setHidden:YES];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"CreditCard"])
    {
        //[_viewForSingleAmount setHidden:NO];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"PaymentPending"])
    {
        [_viewForSingleAmount setHidden:YES];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"])
    {
        [_viewForSingleAmount setHidden:YES];
    }
    
}

-(void)checkToSetAmtNull{
    
    if (!IsShowPricingAndNotesOnApp) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    
}

@end

