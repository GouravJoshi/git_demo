//
//  DeclineTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 03/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//asdasds

#import <UIKit/UIKit.h>

@interface DeclineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblValueForRepair;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForLR;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForHLR;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForPart;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblValueStatus;
@property (strong, nonatomic) IBOutlet UIButton *btnViewMore;

@property (strong, nonatomic) IBOutlet UITextField *txtFldQty;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalAmount;

@end
