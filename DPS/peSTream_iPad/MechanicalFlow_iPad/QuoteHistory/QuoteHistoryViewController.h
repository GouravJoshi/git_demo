//
//  QuoteHistoryViewController.h
//  DPS
//  peSTream
//  Created by Akshay Hastekar on 17/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//  peSTream

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface QuoteHistoryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)NSString *strWorkOrderID;

@property (weak, nonatomic) IBOutlet UITableView *tableViewQuoteHistory;
@property (strong, nonatomic) IBOutlet UIButton *btnAddQuote;
- (IBAction)action_CreateQuote:(id)sender;
// managed object
@property (strong, nonatomic) NSManagedObject *objWorkOrderDetail;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderDetail;
@property (strong, nonatomic) NSString *strFromWhere;
- (IBAction)action_Refresh:(id)sender;

//  peStream

@end
