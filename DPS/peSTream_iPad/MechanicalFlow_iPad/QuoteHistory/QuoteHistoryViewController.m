//
//  QuoteHistoryViewController.m
//  DPS
//  peSTream
//  Created by Akshay Hastekar on 17/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//  peSTream

#import "QuoteHistoryViewController.h"
#import "AllImportsViewController.h"
@interface QuoteHistorycell:UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelQuoteNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelWorkOrderNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelAccountNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelCustomerName;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelSubmittedOn;
@property (weak, nonatomic) IBOutlet UILabel *labelSubmittedBy;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;

@end

@implementation QuoteHistorycell

@end



@interface QuoteHistoryViewController ()

@end

@implementation QuoteHistoryViewController
{
    NSMutableArray *arrayQuoteHistory;
    Global *global;
}
#pragma mark - View's life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrayQuoteHistory = [NSMutableArray new];
    
    global = [[Global alloc] init];
    _tableViewQuoteHistory.rowHeight=UITableViewAutomaticDimension;
    _tableViewQuoteHistory.estimatedRowHeight=200;
    _tableViewQuoteHistory.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    [_tableViewQuoteHistory setHidden:YES];
    [_tableViewQuoteHistory setSeparatorColor:[UIColor colorWithRed:(95/255.0) green:(178/255.0) blue:(175/255.0) alpha:1.0]];
    
    
    if(_strWorkOrderID.length>0)
    {
        
        [_btnAddQuote setHidden:NO];
        
    }else{
        
        [_btnAddQuote setHidden:YES];
        
    }
    NSString *strWorkOrderStatuss=[_objSubWorkOrderDetail valueForKey:@"subWOStatus"];
    
    BOOL isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    
    if (isCompletedStatusMechanical) {
        
        [_btnAddQuote setHidden:YES];
        
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    arrayQuoteHistory = [NSMutableArray new];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Quote History..."];
    
    [self performSelector:@selector(getQuoteHistory) withObject:nil afterDelay:0.1];
    
}

#pragma mark - UITableView delegate and datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayQuoteHistory.count;
}

/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 
 CGFloat stringHght = [self getTextHeightByWidth:[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"ServiceAddressName"] textFont:[UIFont systemFontOfSize:22] textWidth:self.view.frame.size.width-400];
 if(stringHght>50.0)
 {
 return 375.0+stringHght;
 }
 
 return 425;
 }
 */

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QuoteHistorycell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuoteHistorycell" forIndexPath:indexPath];
    if(cell==nil)
    {
        cell = [[QuoteHistorycell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"QuoteHistorycell"];
    }
    
    NSString *strQuoteNumber = [NSString stringWithFormat:@"%@",[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"QuoteNo"]];
    NSString *strWorkOrderNumber = [NSString stringWithFormat:@"%@",[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"WorkOrderNo"]];
    NSString *strAccountNumber = [NSString stringWithFormat:@"%@",[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"AccountNo"]];
    NSString *strCustomerName = [NSString stringWithFormat:@"%@",[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"EmployeeName"]];
    NSString *strAddress = [NSString stringWithFormat:@"%@",[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"ServiceAddressName"]];
    NSString *strSubmittedOn = [NSString stringWithFormat:@"%@",[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"CreatedDate"]];
    
    strSubmittedOn=[global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:strSubmittedOn];
    
    
    NSString *strSubmittedBy = [NSString stringWithFormat:@"%@",[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"SubmittedBy"]];
    //NSString *strStatus = [NSString stringWithFormat:@"%@",[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"QuoteStatus"]];
    
    NSString *strStatus=[NSString stringWithFormat:@"%@",[[arrayQuoteHistory objectAtIndex:indexPath.row] valueForKey:@"QuoteStatus"]];
    
    
    if ([strStatus isEqualToString:@"1"]){
        strStatus=@"New";
        
    }else if ([strStatus isEqualToString:@"2"]){
        strStatus=@"Quoted";
        
    }else if ([strStatus isEqualToString:@"3"]){
        strStatus=@"Signed";
        
    }else if ([strStatus isEqualToString:@"4"]){
        strStatus=@"Declined";
        
    }else if ([strStatus isEqualToString:@"5"]){
        strStatus=@"Completed";
    }
    
    
    
    
    /* ye wali condition mene pehle lagayi thi
     if ([strStatus isEqualToString:@"0"]) {
     strStatus=@"Pending";
     
     }else if ([strStatus isEqualToString:@"1"]){
     strStatus=@"Approved";
     
     }else if ([strStatus isEqualToString:@"2"]){
     strStatus=@"Received";
     
     }else if ([strStatus isEqualToString:@"3"]){
     strStatus=@"Decline";
     }
     */
    
    if([strQuoteNumber length]>0 && strQuoteNumber!=nil && ![strQuoteNumber isKindOfClass:[NSNull class]] && ![strQuoteNumber isEqualToString:@"<null>"])
    {
        cell.labelQuoteNumber.text = strQuoteNumber;
    }
    else
    {
        cell.labelQuoteNumber.text = @"";
    }
    
    if([strWorkOrderNumber length]>0 && strWorkOrderNumber!=nil && ![strWorkOrderNumber isKindOfClass:[NSNull class]] && ![strWorkOrderNumber isEqualToString:@"<null>"])
    {
        cell.labelWorkOrderNumber.text = strWorkOrderNumber;
    }
    else
    {
        cell.labelWorkOrderNumber.text = @"";
    }
    
    if([strAccountNumber length]>0 && strAccountNumber!=nil && ![strAccountNumber isKindOfClass:[NSNull class]] && ![strAccountNumber isEqualToString:@"<null>"])
    {
        cell.labelAccountNumber.text = strAccountNumber;
    }
    else
    {
        cell.labelAccountNumber.text = @"";
    }
    
    if([strCustomerName length]>0 && strCustomerName!=nil && ![strCustomerName isKindOfClass:[NSNull class]] && ![strCustomerName isEqualToString:@"<null>"])
    {
        cell.labelCustomerName.text = strCustomerName;
    }
    else
    {
        cell.labelCustomerName.text = @"";
    }
    
    if([strAddress length]>0 && strAddress!=nil && ![strAddress isKindOfClass:[NSNull class]] && ![strAddress isEqualToString:@"<null>"])
    {
        cell.labelAddress.text = strAddress;
    }
    else
    {
        cell.labelAddress.text = @"";
    }
    
    if([strSubmittedOn length]>0 && strSubmittedOn!=nil && ![strSubmittedOn isKindOfClass:[NSNull class]] && ![strSubmittedOn isEqualToString:@"<null>"])
    {
        cell.labelSubmittedOn.text = strSubmittedOn;
    }
    else
    {
        cell.labelSubmittedOn.text = @"";
    }
    
    if([strSubmittedBy length]>0 && strSubmittedBy!=nil && ![strSubmittedBy isKindOfClass:[NSNull class]] && ![strSubmittedBy isEqualToString:@"<null>"])
    {
        cell.labelSubmittedBy.text = strSubmittedBy;
    }
    else
    {
        cell.labelSubmittedBy.text = @"";
    }
    
    if([strStatus length]>0 && strStatus!=nil && ![strStatus isKindOfClass:[NSNull class]] && ![strStatus isEqualToString:@"<null>"])
    {
        cell.labelStatus.text = strStatus;
    }
    else
    {
        cell.labelStatus.text = @"";
    }
    
    return cell;
}

#pragma mark - UIButton action

- (IBAction)actionOnBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Fetch Quote History

-(void)getQuoteHistory
{
    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
    NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.CoreServiceModule.ServiceUrl"];
    NSString *strEmpNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    //strEmpNo=@"87";
    NSString *strUrl;
    
    if(_strWorkOrderID.length>0 )
    {
        strUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",strServiceUrlMain,UrlGetQuoteHistory,strCompanyKey,UrlPoEmployeeNo,strEmpNo,UrlWONumber,_strWorkOrderID];
    }
    else
    {
        strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMain,UrlGetQuoteHistory,strCompanyKey,UrlPoEmployeeNo,strEmpNo];
    }
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             
             if ((ResponseDict==nil) || ResponseDict.count==0) {
                 
                 [_tableViewQuoteHistory setHidden:YES];
                 [global AlertMethod:Alert :@"No data available"];
                 [DejalBezelActivityView removeView];
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDict =dictData;
                 
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                 
                 if (strException.length==0) {
                     
                     [_tableViewQuoteHistory setHidden:NO];
                     
                     NSArray *tempArray=(NSArray*)ResponseDict;
                     
                     if(_strWorkOrderID.length>0)
                     {
                         
                         for(NSDictionary *dict in tempArray)
                         {
                             if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"WorkOrderNo"]] isEqualToString:_strWorkOrderID])
                             {
                                 [arrayQuoteHistory addObject:dict];
                             }
                         }
                     }
                     else
                     {
                         arrayQuoteHistory = [tempArray mutableCopy];
                     }
                     
                     
                     [_tableViewQuoteHistory reloadData];
                     
                     [DejalBezelActivityView removeView];
                     
                 } else {
                     
                     [_tableViewQuoteHistory setHidden:YES];
                     ResponseDict=nil;
                     [global AlertMethod:Alert :@"No data available"];
                     [DejalBezelActivityView removeView];
                     
                 }
                 
             }
             
         }];
    }
    @catch (NSException *exception) {
        
        [_tableViewQuoteHistory setHidden:YES];
        [global AlertMethod:Alert :Sorry];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }
}


- (CGFloat)getTextHeightByWidth:(NSString*)text textFont:(UIFont*)textFont textWidth:(float)textWidth {
    
    if (!text) {
        return 0;
    }
    CGSize boundingSize = CGSizeMake(textWidth, CGFLOAT_MAX);
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{ NSFontAttributeName: textFont }];
    
    CGRect rect = [attributedText boundingRectWithSize:boundingSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize requiredSize = rect.size;
    return requiredSize.height;
}
-(void)getMethod{
    
    //Test Method not in use
    
    
}
- (IBAction)action_CreateQuote:(id)sender {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"createQuoteiPad" bundle:nil];
    QuoteViewController *vc = [sb instantiateViewControllerWithIdentifier:@"QuoteViewController"];
    vc.objWorkOrderDetail =_objWorkOrderDetail;
    vc.objSubWorkOrderDetail =_objSubWorkOrderDetail;
    vc.strFromWhere=@"sdf";
    //[self.navigationController pushViewController:vc animated:NO];
    [self presentViewController:vc animated:YES completion:nil];
    
}
- (IBAction)action_Refresh:(id)sender {
    
    arrayQuoteHistory = [NSMutableArray new];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Quote History..."];
    
    [self performSelector:@selector(getQuoteHistory) withObject:nil afterDelay:0.1];
    
    
}
@end

