//
//  MechanicalDocumentsViewController.m
//  DPS
//
//  Created by Saavan Patidar on 12/09/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "MechanicalDocumentsViewController.h"
#import "AllImportsViewController.h"
#import "ServiceDocumentTableViewCell.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "ServiceDocuments+CoreDataClass.h"
#import "ServiceDocuments+CoreDataProperties.h"
#import <SafariServices/SafariServices.h>
#import "MBProgressHUD.h"
#import "ServiceDocumentsViewControlleriPad.h"

@interface MechanicalDocumentsViewController ()<UITableViewDataSource,UITableViewDelegate,UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate,SFSafariViewControllerDelegate>
{
    Global *global;
    NSString *strEmpID,*strUserName,*strCompanyKey,*strServiceUrlMain,*strDocNameGlobal;
    UIRefreshControl *refreshControl;
    NSData *dataToUpload;
    NSIndexPath *indexPathSelectedGlobal;
}

@end

@implementation MechanicalDocumentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialize the refresh control.
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor lightTextColor];
    refreshControl.tintColor = [UIColor themeColor];
    [refreshControl addTarget:self
                       action:@selector(reloadTableData)
             forControlEvents:UIControlEventValueChanged];
    NSString *title = [NSString stringWithFormat:@"Pull to refresh customer sales documents"];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    refreshControl.attributedTitle = attributedTitle;
    [_tblViewDocs addSubview:refreshControl];
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    _tblViewDocs.rowHeight=UITableViewAutomaticDimension;
    _tblViewDocs.estimatedRowHeight=200;
    _tblViewDocs.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    [self FetchFromCoreDataToShowWoOtherDocExtSerDcs];
    [self fetchDocuments];
    // Do any additional setup after loading the view.
    
    _txtView_Title.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_Title.layer.borderWidth=1.0;
    _txtView_Title.layer.cornerRadius=5.0;
    
    _txtView_Description.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_Description.layer.borderWidth=1.0;
    _txtView_Description.layer.cornerRadius=5.0;
    
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
        [self.navigationController.navigationBar setHidden:YES];
        
    }else{
        
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
        [self.navigationController.navigationBar setHidden:YES];
        
    }
    
    if ([_strWorkStatus isEqualToString:@"Complete"]) {
        
        [_btn_AddDocument setHidden:YES];
        
    } else {
        
        [_btn_AddDocument setHidden:NO];
        
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [global getUnsignedDocumentsWoNos:strServiceUrlMain :strCompanyKey];
    
}

- (void)reloadTableData
{
    
    [refreshControl endRefreshing];
    [self FetchFromCoreDataToShowWoOtherDocExtSerDcs];
    [self fetchDocuments];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
//============================================================================
#pragma mark- Button Action METHODS
//============================================================================

- (IBAction)action_Back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)action_Refresh:(id)sender {
    
    [self fetchDocuments];
    
}

- (IBAction)action_AddDocument:(id)sender {
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        _txtView_Description.text=@"";
        _txtView_Title.text=@"";
        _lbl_FileName.text=@"";
        
        _view_UploadDocument.frame=CGRectMake(_view_UploadDocument.frame.origin.x, _view_UploadDocument.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        
        [self.view addSubview:_view_UploadDocument];
        
    }else {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    }
    
}
- (IBAction)action_SelectDocument:(id)sender {
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              NSLog(@"The CApture Image.");
                              
                              NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                              
                              BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                              
                              if (isfirstTimeAudio) {
                                  
                                  [defs setBool:NO forKey:@"firstCamera"];
                                  [defs synchronize];
                                  
                                  UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                  imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                  imagePickController.delegate=(id)self;
                                  imagePickController.allowsEditing=TRUE;
                                  [self presentViewController:imagePickController animated:YES completion:nil];
                                  
                              }else{
                                  BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                  
                                  if (isCameraPermissionAvailable) {
                                      
                                      UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                      imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                      imagePickController.delegate=(id)self;
                                      imagePickController.allowsEditing=TRUE;
                                      [self presentViewController:imagePickController animated:YES completion:nil];
                                      
                                      
                                  }else{
                                      
                                      UIAlertController *alert= [UIAlertController
                                                                 alertControllerWithTitle:@"Alert"
                                                                 message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                      
                                      UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action)
                                                            {
                                                                
                                                                
                                                                
                                                            }];
                                      [alert addAction:yes];
                                      UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                   NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                   [[UIApplication sharedApplication] openURL:url];
                                                               } else {
                                                                   
                                                               }
                                                           }];
                                      [alert addAction:no];
                                      [self presentViewController:alert animated:YES completion:nil];
                                  }
                              }
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                             
                             BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                             
                             if (isfirstTimeAudio) {
                                 
                                 [defs setBool:NO forKey:@"firstGallery"];
                                 [defs synchronize];
                                 
                                 UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                 imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                 imagePickController.delegate=(id)self;
                                 imagePickController.allowsEditing=TRUE;
                                 [self presentViewController:imagePickController animated:YES completion:nil];
                                 
                             }else{
                                 BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                 
                                 if (isCameraPermissionAvailable) {
                                     
                                     UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                     imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                     imagePickController.delegate=(id)self;
                                     imagePickController.allowsEditing=TRUE;
                                     [self presentViewController:imagePickController animated:YES completion:nil];
                                     
                                     
                                 }else{
                                     
                                     UIAlertController *alert= [UIAlertController
                                                                alertControllerWithTitle:@"Alert"
                                                                message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                preferredStyle:UIAlertControllerStyleAlert];
                                     
                                     UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               
                                                           }];
                                     [alert addAction:yes];
                                     UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                handler:^(UIAlertAction * action)
                                                          {
                                                              
                                                              if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                  NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                  [[UIApplication sharedApplication] openURL:url];
                                                              } else {
                                                                  
                                                              }
                                                          }];
                                     [alert addAction:no];
                                     [self presentViewController:alert animated:YES completion:nil];
                                 }
                             }
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Other" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  NSArray *types = @[(NSString*)kUTTypeImage,(NSString*)kUTTypeSpreadsheet,(NSString*)kUTTypePresentation,(NSString*)kUTTypeDatabase,(NSString*)kUTTypeFolder,(NSString*)kUTTypeZipArchive,(NSString*)kUTTypeVideo];
                                  
                                  types = @[(NSString*)kUTTypeSpreadsheet,(NSString*)kUTTypePresentation,(NSString*)kUTTypePDF,(NSString*)kUTTypeText];
                                  
                                  UIDocumentMenuViewController *documentProviderMenu =
                                  [[UIDocumentMenuViewController alloc] initWithDocumentTypes:types
                                                                                       inMode:UIDocumentPickerModeImport];
                                  
                                  documentProviderMenu.delegate = self;
                                  
                                  // for iPad: make the presentation a Popover
                                  documentProviderMenu.modalPresentationStyle = UIModalPresentationPopover;
                                  documentProviderMenu.popoverPresentationController.sourceView = self.view;
                                  [self presentViewController:documentProviderMenu animated:YES completion:nil];
                                  
                                  UIPopoverPresentationController *popController = [documentProviderMenu popoverPresentationController];
                                  popController.permittedArrowDirections = UIPopoverArrowDirectionUnknown;
                                  UINavigationController* nav = [UINavigationController new];
                                  popController.barButtonItem = nav.navigationItem.leftBarButtonItem;
                                  
                                  // [self presentViewController:documentProviderMenu animated:YES completion:nil];
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)action_UploadDocument:(id)sender {
    
    if (dataToUpload==nil) {
        
        [global displayAlertController:Alert :@"Please select document to upload" :self];
        
    } else if (_txtView_Title.text.length==0){
        
        [global displayAlertController:Alert :@"Please enter title to proceed" :self];
        
        //[_txtView_Title becomeFirstResponder];
        
    } else{
        
        //[self.view resignFirstResponder];
        
        BOOL isNetReachable=[global isNetReachable];
        
        if (isNetReachable) {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Uploading Document..."];
            
            [self performSelector:@selector(methodToUplod) withObject:nil afterDelay:0.2];
            
            
        }else {
            
            [global AlertMethod:Alert :ErrorInternetMsg];
            
        }
        
    }
    
}

-(void)methodToUplod{
    NSArray *objValue=[NSArray arrayWithObjects:
                       @"0",
                       _strWorkOrderId,
                       _strWorkOrderNo,
                       strCompanyKey,
                       _txtView_Title.text,
                       _txtView_Description.text.length==0 ? @"" : _txtView_Description.text,
                       [global strCurrentDate],
                       strEmpID,
                       [global strCurrentDate],
                       strEmpID,
                       @"Mobile",nil];
    
    NSArray *objKey=[NSArray arrayWithObjects:
                     @"WOOtherDocId",
                     @"WorkOrderId",
                     @"WorkOrderNo",
                     @"CompanyKey",
                     @"WOOtherDocTitle",
                     @"WoOtherDocDescription",
                     @"CreatedDate",
                     @"CreatedBy",
                     @"ModifiedDate",
                     @"ModifiedBy",
                     @"CreatedByDevice",nil];
    
    NSDictionary *dict_ToSend=[[NSDictionary alloc] initWithObjects:objValue forKeys:objKey];
    
    NSMutableDictionary *dictDataa = [[NSMutableDictionary alloc]init];
    
    [dictDataa setObject:dict_ToSend forKey:@"WoOtherDocExtSerDc"];
    
    NSMutableArray *arrTemp =[[NSMutableArray alloc]init];
    
    [arrTemp addObject:dictDataa];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Json for Wo Other DOcs: %@", jsonString);
        }
    }
    
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlUploadMechanicalWoDocument];
    
    [self uplodDocumentToServer:strUrl :jsonString :dataToUpload :strDocNameGlobal];
    
}

-(void)uplodDocumentToServer :(NSString*)stringUrl :(NSString*)jsonString :(NSData*)dataToSend :(NSString*)strNameDoc{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:stringUrl]];
    [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"woOtherDocExtSerDc"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // For Other Data like image pdf etc.....
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strNameDoc] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:dataToSend]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    // NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSError *jsonParsingError = nil;
    NSMutableDictionary *responseJson = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&jsonParsingError];
    
    
    if ([responseJson isKindOfClass:[NSDictionary class]]) {
        
        NSArray *arrKeys = [responseJson allKeys];
        
        if (arrKeys.count>0) {
            
            if ([arrKeys containsObject:@"WOOtherDocPath"]) {
                
                dataToUpload = nil;
                
                NSDictionary *dictData = (NSMutableDictionary*)(responseJson);
                
                dictData = [global nestedDictionaryByReplacingNullsWithNil:dictData];
                
                [self saveWoOtherDocExtSerDcsToDB:dictData];
                
                [self reloadTableData];
                
            } else {
                
                [global displayAlertController:Alert :@"Unable to upload .Please try again later" :self];
                
            }
            
        } else {
            
            [global displayAlertController:Alert :@"Unable to upload .Please try again later" :self];
            
        }
        
    } else {
        
        [global displayAlertController:Alert :@"Unable to upload .Please try again later" :self];
        
    }
    
    [_view_UploadDocument removeFromSuperview];
    
    [DejalBezelActivityView removeView];
    
}

- (IBAction)action_CancelDocuments:(id)sender {
    
    [_view_UploadDocument removeFromSuperview];
    
}


//============================================================================
#pragma mark- Fetching Documents METHODS
//============================================================================

-(void)saveWoOtherDocExtSerDcsToDB :(NSDictionary*)dictWoOtherDocExtSerDcs{
    
    //WoOtherDocExtSerDcs Detail Entity
    entityMechanicalWoOtherDocExtSerDcs=[NSEntityDescription entityForName:@"WoOtherDocExtSerDcs" inManagedObjectContext:context];
    
    WoOtherDocExtSerDcs *objWoOtherDocExtSerDcs = [[WoOtherDocExtSerDcs alloc]initWithEntity:entityMechanicalWoOtherDocExtSerDcs insertIntoManagedObjectContext:context];
    
    objWoOtherDocExtSerDcs.companyKey=strCompanyKey;
    objWoOtherDocExtSerDcs.userName=strUserName;
    objWoOtherDocExtSerDcs.workOrderId=_strWorkOrderId;
    objWoOtherDocExtSerDcs.createdByDevice = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"CreatedByDevice"]];
    objWoOtherDocExtSerDcs.wOOtherDocId = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WOOtherDocId"]];
    objWoOtherDocExtSerDcs.workOrderNo = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WorkOrderNo"]];
    objWoOtherDocExtSerDcs.leadNo = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"LeadNo"]];
    objWoOtherDocExtSerDcs.wOOtherDocPath = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WOOtherDocPath"]];
    objWoOtherDocExtSerDcs.wOOtherDocTitle = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WOOtherDocTitle"]];
    objWoOtherDocExtSerDcs.woOtherDocDescription = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WoOtherDocDescription"]];
    objWoOtherDocExtSerDcs.createdDate = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"CreatedDate"]];
    objWoOtherDocExtSerDcs.createdBy = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"CreatedBy"]];
    objWoOtherDocExtSerDcs.modifiedDate = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"ModifiedDate"]];
    objWoOtherDocExtSerDcs.modifiedBy = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"ModifiedBy"]];
    
    NSError *errorObjWoOtherDocExtSerDcs;
    [context save:&errorObjWoOtherDocExtSerDcs];
    
    [self FetchFromCoreDataToShowWoOtherDocExtSerDcs];
    
}

-(void)checkIfDataAvailableOrNot{
    
    if ((arrAllObjWoOtherDocExtSerDcs.count>0) || (arrAllObjServiceDocs.count>0)) {
        
        [_tblViewDocs setHidden:NO];
        [_tblViewDocs reloadData];
        
    } else {
        
        [_tblViewDocs setHidden:YES];
        [_tblViewDocs reloadData];
        [global displayAlertController:Info :NoDataAvailableee :self];
        //[self dismissViewControllerAnimated:YES completion:nil];
        
    }
}

-(void)fetchDocuments{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [self FetchFromCoreDataToShowServiceDocs];
        
    }else{
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMain,UrlServiceDocuments,strCompanyKey,UrlServiceDocumentsAccountNo,_strAccountNo];
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Documents..."];
        
        //============================================================================
        //============================================================================
        
        NSString *strType=@"getDocumentService";
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         //  arrOfTaskList=response;
                         if (response.count==0) {
                             
                             //[self checkIfDataAvailableOrNot];
                             
                         } else {
                             
                             //[self checkIfDataAvailableOrNot];
                             [self coreDataSaveServiceDocs:response];
                         }
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
    }
}

//============================================================================
#pragma mark-  METHODS
//============================================================================

-(void)deleteFromCoreDataMechanicalDocs :(NSString*)wOOtherDocId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityServiceDocs=[NSEntityDescription entityForName:@"WoOtherDocExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"WoOtherDocExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@ AND wOOtherDocId = %@",_strWorkOrderId,strUserName,wOOtherDocId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self FetchFromCoreDataToShowWoOtherDocExtSerDcs];
    
}
-(void)coreDataSaveServiceDocs :(NSDictionary*)dictDataa{
    
    NSArray *arrDocs=[dictDataa valueForKey:@"CustomerDocuments"];
    if (arrDocs.count==0) {
        
        //[self checkIfDataAvailableOrNot];
        
    } else {
        
        //==================================================================================
        //==================================================================================
        [self deleteFromCoreDataServiceDocs];
        //==================================================================================
        //==================================================================================
        
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        
        
        for (int k=0; k<arrDocs.count; k++) {
            
            NSDictionary *dictDocss=arrDocs[k];
            
            entityServiceDocs=[NSEntityDescription entityForName:@"ServiceDocuments" inManagedObjectContext:context];
            ServiceDocuments *objServiceDocs = [[ServiceDocuments alloc]initWithEntity:entityServiceDocs insertIntoManagedObjectContext:context];
            objServiceDocs.userName=strUserName;
            objServiceDocs.companyKey=strCompanyKey;
            
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'"];
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
            NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"ScheduleStartDate"]]];
            objServiceDocs.scheduleStartDate=newTime;
            objServiceDocs.leadDocumentId=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"LeadDocumentId"]];
            objServiceDocs.leadId=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"LeadId"]];
            objServiceDocs.accountNo=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"AccountNo"]];
            objServiceDocs.docType=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"DocType"]];
            objServiceDocs.title=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"Title"]];
            objServiceDocs.fileName=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"FileName"]];
            objServiceDocs.descriptions=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"Descriptions"]];
            objServiceDocs.isAgreementSigned=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"IsAgreementSigned"]];
            objServiceDocs.signatureLinkforAgree=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"SignatureLinkforAgree"]];
            objServiceDocs.leadNumber=[NSString stringWithFormat:@"%@",[dictDocss valueForKey:@"LeadNumber"]];
            
            NSError *error1;
            [context save:&error1];
            
        }
        
        //==================================================================================
        //==================================================================================
        [self FetchFromCoreDataToShowServiceDocs];
        //==================================================================================
        //==================================================================================
    }
}
-(void)deleteFromCoreDataServiceDocs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityServiceDocs=[NSEntityDescription entityForName:@"ServiceDocuments" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceDocuments" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ AND userName = %@",_strAccountNo,strUserName];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)FetchFromCoreDataToShowServiceDocs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityServiceDocs=[NSEntityDescription entityForName:@"ServiceDocuments" inManagedObjectContext:context];
    requestServiceDocs = [[NSFetchRequest alloc] init];
    [requestServiceDocs setEntity:entityServiceDocs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ AND userName = %@",_strAccountNo,strUserName];
    [requestServiceDocs setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"scheduleStartDate" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestServiceDocs setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceDocs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceDocs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceDocs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceDocs performFetch:&error];
    arrAllObjServiceDocs = [self.fetchedResultsControllerServiceDocs fetchedObjects];
    if ([arrAllObjServiceDocs count] == 0)
    {
        [self checkIfDataAvailableOrNot];
    }
    else
    {
        [self checkIfDataAvailableOrNot];
    }
    
}

-(void)FetchFromCoreDataToShowWoOtherDocExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWoOtherDocExtSerDcs=[NSEntityDescription entityForName:@"WoOtherDocExtSerDcs" inManagedObjectContext:context];
    requestWoOtherDocExtSerDcs = [[NSFetchRequest alloc] init];
    [requestWoOtherDocExtSerDcs setEntity:entityWoOtherDocExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",_strWorkOrderId,strUserName];
    [requestWoOtherDocExtSerDcs setPredicate:predicate];
    
    sortDescriptorWoOtherDocExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:YES];
    sortDescriptorsWoOtherDocExtSerDcs = [NSArray arrayWithObject:sortDescriptorWoOtherDocExtSerDcs];
    
    [requestWoOtherDocExtSerDcs setSortDescriptors:sortDescriptorsWoOtherDocExtSerDcs];
    
    self.fetchedResultsControllerWoOtherDocExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoOtherDocExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoOtherDocExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoOtherDocExtSerDcs performFetch:&error];
    arrAllObjWoOtherDocExtSerDcs = [self.fetchedResultsControllerWoOtherDocExtSerDcs fetchedObjects];
    if ([arrAllObjWoOtherDocExtSerDcs count] == 0)
    {
        //[self checkIfDataAvailableOrNot];
    }
    else
    {
        //[self checkIfDataAvailableOrNot];
    }
    
}

//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"WorkOrder Other Documents";
            break;
        case 1:
            sectionName = @"Customer Sales Documents";
            break;
            // ...
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = tableView.frame;
    
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"WorkOrder Other Documents";
            break;
        case 1:
            sectionName = @"Customer Sales Documents";
            break;
            // ...
        default:
            sectionName = @"";
            break;
    }
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, frame.size.width-20, 50)];
    title.text = sectionName;
    title.backgroundColor=[UIColor clearColor];
    title.textColor=[UIColor blackColor];
    title.font=[UIFont boldSystemFontOfSize:20];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    headerView.backgroundColor=[UIColor lightTextColorTimeSheet];
    [headerView addSubview:title];
    
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 50;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 5;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section==0) {
        return arrAllObjWoOtherDocExtSerDcs.count;
    } else {
        return arrAllObjServiceDocs.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ServiceDocumentTableViewCell *cell = (ServiceDocumentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"serviceDoc" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
- (void)configureCell:(ServiceDocumentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    
    if (indexPath.section==1) {
        
        NSManagedObject *dictDocs=[arrAllObjServiceDocs objectAtIndex:indexPath.row];
        NSString *strDoctType=[dictDocs valueForKey:@"docType"];
        if ([strDoctType isEqualToString:@"Agreement"]) {
            
            NSString *strIsAgreementSigned=[dictDocs valueForKey:@"isAgreementSigned"];
            if ([strIsAgreementSigned isEqualToString:@"1"]) {
                
                cell.lbl_Title.text=[dictDocs valueForKey:@"title"];
                
            }else{
                
                cell.lbl_Title.text=@"Get Customer Signature on Agreement";
                cell.lbl_Title.textColor=[UIColor blueColor];
            }
        }
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        NSString* finalTime = [dateFormatter stringFromDate:[dictDocs valueForKey:@"scheduleStartDate"]];
        
        cell.lbl_Description.text=finalTime;//[dictDocs valueForKey:@"scheduleStartDate"];
        cell.lbl_TitleHeader.text=@"Scheduled Start Date:";
    } else {
        
        NSManagedObject *dictDocs=[arrAllObjWoOtherDocExtSerDcs objectAtIndex:indexPath.row];
        cell.lbl_Title.text=[dictDocs valueForKey:@"wOOtherDocTitle"];
        cell.lbl_Description.text=[global ChangeDateToLocalDateMechanicalAllDates:[dictDocs valueForKey:@"createdDate"]];
        cell.lbl_TitleHeader.text=@"Created Date:";
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    indexPathSelectedGlobal = indexPath;
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching please wait..."];
    
    [self performSelector:@selector(methodOnDidSelectRowAtIndexPath) withObject:nil afterDelay:0.2];
    
}

-(void)methodOnDidSelectRowAtIndexPath{
    
    if (indexPathSelectedGlobal.section==1) {
        
        NSManagedObject *dictDocs=[arrAllObjServiceDocs objectAtIndex:indexPathSelectedGlobal.row];
        
        NSString *strDoctType=[dictDocs valueForKey:@"docType"];
        if ([strDoctType isEqualToString:@"Agreement"]) {
            
            NSString *strIsAgreementSigned=[dictDocs valueForKey:@"isAgreementSigned"];
            if ([strIsAgreementSigned isEqualToString:@"1"]) {
                
                [self oPenPdfView:[NSString stringWithFormat:@"%@",[dictDocs valueForKey:@"fileName"]]];
                
            } else {
                
                [self oPenLinkOnWeb:[NSString stringWithFormat:@"%@",[dictDocs valueForKey:@"signatureLinkforAgree"]]];
                
            }
        } else {
            
            
        }
    }else{
        
        NSManagedObject *dictDocs=[arrAllObjWoOtherDocExtSerDcs objectAtIndex:indexPathSelectedGlobal.row];
        
        [self oPenPdfView:[NSString stringWithFormat:@"%@",[dictDocs valueForKey:@"wOOtherDocPath"]]];
        
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        
        if ([_strWorkStatus isEqualToString:@"Complete"]) {
            
            return false;
            
        }else{
            
            return true;
            
        }
        
    }else{
        
        return false;
        
    }
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        if (indexPath.section==0) {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:Alert
                                       message:@"Are you sure to delete"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                     NSManagedObject *dictDocs=[arrAllObjWoOtherDocExtSerDcs objectAtIndex:indexPath.row];
                                     NSString *strDocId = [NSString stringWithFormat:@"%@",[dictDocs valueForKey:@"WOOtherDocId"]];
                                     
                                     [DejalBezelActivityView activityViewForView:self.view withLabel:@"Deleting..."];
                                     
                                     NSString *strUrl=[NSString stringWithFormat:@"%@%@%@",strServiceUrlMain,UrlDeleteMechanicalWoDocument,strDocId];
                                     
                                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                                         
                                         [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"SendEmailServiceAutomation" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                                          {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [DejalBezelActivityView removeView];
                                                  if (success)
                                                  {
                                                      NSString *returnString =[response valueForKey:@"ReturnMsg"];
                                                      if ([returnString isEqualToString:@"true"] || [returnString isEqualToString:@"True"]) {
                                                          
                                                          [global displayAlertController:Info :@"Deleted Successfully" :self];
                                                          
                                                          [self deleteFromCoreDataMechanicalDocs:strDocId];
                                                          
                                                          [_tblViewDocs reloadData];

                                                      } else {
                                                          
                                                          [global displayAlertController:Info :@"Unable to delete. Please try again later" :self];
                                                          
                                                      }
                                                      
                                                  }
                                                  else
                                                  {
                                                      NSString *strTitle = Alert;
                                                      NSString *strMsg = Sorry;
                                                      [global AlertMethod:strTitle :strMsg];
                                                  }
                                              });
                                          }];
                                     });
                                     
                                     //.................................................................
                                     
                                 }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else{
            
        }
        
    } else {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    }
    
}
-(void)oPenLinkOnWeb :(NSString *)strUrl{
    
    strUrl=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSURL *URL = [NSURL URLWithString:strUrl];
    
    [DejalBezelActivityView removeView];
    
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:URL]) {
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }
}
-(void)oPenPdfView :(NSString *)strUrl{
    
    NSString *urlString = [NSString stringWithFormat:@"%@",strUrl];
    
    NSString *strNewString=[urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSArray* foo = [strNewString componentsSeparatedByString: @"/"];
    NSString* lastName = [foo objectAtIndex: foo.count-1];
    
    NSString *removed = [lastName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *mainPath = [documentsDirectory stringByAppendingPathComponent: removed];
    
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:mainPath];
    
    if (exists) {
        
        [self oPenPDFfile :mainPath];
        
    } else {
        
        strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSURL *url = [NSURL URLWithString:strNewString];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        [data writeToFile:mainPath atomically:YES];
        
        [self oPenPDFfile :mainPath];
    }
    
    [DejalBezelActivityView removeView];
    
}

-(void)oPenPDFfile :(NSString*)strPath{
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
    frameFor_view_CustomerInfo.origin.x=0;
    frameFor_view_CustomerInfo.origin.y=20;
    
    [_pdfView setFrame:frameFor_view_CustomerInfo];
    
    [self.view addSubview:_pdfView];
    
    NSURL *targetURL = [NSURL fileURLWithPath:strPath];
    NSURLRequest *request_pdf = [NSURLRequest requestWithURL:targetURL];
    [_webViewww loadRequest:request_pdf];
    
}
- (IBAction)action_BackPdf:(id)sender {
    
    [_pdfView removeFromSuperview];
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //[_loaderWebView startAnimating];
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Searching..."];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    // [_loaderWebView stopAnimating];
    [DejalActivityView removeView];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"The File you are looking for does not exist.Please try again later."
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [_pdfView removeFromSuperview];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    [DejalActivityView removeView];
}


#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString  *strImageNamess = [NSString stringWithFormat:@"CustomerDoc_%@%@.jpg",strDate,strTime];
    _lbl_FileName.text=strImageNamess;
    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
    [self resizeImage:image :strImageNamess];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    //NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
    //NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    //[imageData writeToFile:path atomically:YES];
    
    NSLog(@"%@", [[NSByteCountFormatter new] stringFromByteCount:imageData.length]);
    NSLog(@"File size is : %.2f MB",(float)imageData.length/1024.0f/1024.0f);
    
    NSString *strDataInKb = [[NSByteCountFormatter new] stringFromByteCount:imageData.length];
    
    float dataInMB = [strDataInKb floatValue]/1024;
    
    if (dataInMB>2) {
        
        [global displayAlertController:Alert :@"Size can not be more then 2 MB" :self];
        
    } else {
        strDocNameGlobal = imageName;
        _lbl_FileName.text=[NSString stringWithFormat:@"%@ (Size: %f MB)",imageName,dataInMB];
        dataToUpload = imageData;
    }
    
    return [UIImage imageWithData:imageData];
    
}

#pragma mark- ---------------------Document Picker DELEGATE METHOD---------------------


-(void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url{
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    NSString *strDataInKb = [[NSByteCountFormatter new] stringFromByteCount:data.length];
    
    float dataInMB = [strDataInKb floatValue]/1024;
    
    if (dataInMB>2) {
        
        [global displayAlertController:Alert :@"Size can not be more then 2 MB" :self];
        
    } else {
        
        NSString *strName = [NSString stringWithFormat:@"%@",url];
        NSArray* foo = [strName componentsSeparatedByString: @"/"];
        NSString* lastName = [foo objectAtIndex: foo.count-1];
        
        NSString *strName1 = [NSString stringWithFormat:@"%@",lastName];
        NSArray* foo1 = [strName1 componentsSeparatedByString: @"."];
        NSString* lastName1 = [foo1 objectAtIndex: foo1.count-1];
        
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        NSString  *strFileName = [NSString stringWithFormat:@"CustomerDoc_%@%@.%@",strDate,strTime,lastName1];
        strDocNameGlobal = strFileName;
        _lbl_FileName.text=[NSString stringWithFormat:@"%@ (Size: %f MB)",strFileName,dataInMB];
        
        dataToUpload = data;
        
    }
}

-(void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker{
    
    documentPicker.delegate = self;
    
    [self presentViewController:documentPicker animated:YES completion:nil];
    
}

-(void)documentMenuWasCancelled:(UIDocumentMenuViewController *)documentMenu{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)texth
{
    if (range.location == 0 && [texth isEqualToString:@" "]) {
        return NO;
    }
    else
        return YES;
    
}

@end

