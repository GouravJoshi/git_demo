//
//  MechanicalDocumentsViewController.h
//  DPS
//
//  Created by Saavan Patidar on 12/09/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface MechanicalDocumentsViewController : UIViewController<NSFetchedResultsControllerDelegate>
{
    //WoOtherDocExtSerDcs
    AppDelegate *appDelegate;
    NSEntityDescription *entityServiceDocs,*entityWoOtherDocExtSerDcs,*entityMechanicalWoOtherDocExtSerDcs;
    NSManagedObjectContext *context;
    NSFetchRequest *requestServiceDocs,*requestWoOtherDocExtSerDcs;
    NSSortDescriptor *sortDescriptor,*sortDescriptorWoOtherDocExtSerDcs;
    NSArray *sortDescriptors,*sortDescriptorsWoOtherDocExtSerDcs;
    NSManagedObject *matchesServiceDocs,*matchesWoOtherDocExtSerDcs;
    NSArray *arrAllObjServiceDocs,*arrAllObjWoOtherDocExtSerDcs;
}
- (IBAction)action_Back:(id)sender;
- (IBAction)action_Refresh:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewDocs;
@property (strong, nonatomic) NSString *strAccountNo;
@property (strong, nonatomic) NSString *strWorkOrderId;//isCompletedStatusMechanical
@property (strong, nonatomic) NSString *strWorkStatus;
@property (strong, nonatomic) NSString *strWorkOrderNo;
@property (strong, nonatomic) NSString *strLeadNo;
- (IBAction)action_BackPdf:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *webViewww;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceDocs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWoOtherDocExtSerDcs;
@property (strong, nonatomic) IBOutlet UIView *pdfView;
- (IBAction)action_AddDocument:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_AddDocument;
@property (strong, nonatomic) IBOutlet UIView *view_UploadDocument;
- (IBAction)action_SelectDocument:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtView_Title;
@property (strong, nonatomic) IBOutlet UITextView *txtView_Description;
- (IBAction)action_UploadDocument:(id)sender;
- (IBAction)action_CancelDocuments:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_FileName;

@end
