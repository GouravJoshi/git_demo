//
//  ReceivedPOViewController.h
//  DPS
//  peStream
//  Created by Akshay Hastekar on 21/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceivedPOViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textFieldPurchaseOrderNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldVendor;
@property (weak, nonatomic) IBOutlet UITextField *textFieldVendorLocation;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDate;
@property (weak, nonatomic) IBOutlet UITextView *textFieldReceiveItemNote;
@property (weak, nonatomic) IBOutlet UITableView *tableViewPart;
@property (weak, nonatomic) IBOutlet UITextView *textViewPONotes;

// dictionary as a property, it will contain data from dashboard
@property(strong, nonatomic) NSDictionary *dictReceivedPO;

// constraints outlet
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMainView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTableViewReceivedPO;

@end
