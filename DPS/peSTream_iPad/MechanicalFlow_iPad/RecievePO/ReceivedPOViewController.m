//
//  ReceivedPOViewController.m
//  DPS
//
//  Created by Akshay Hastekar on 21/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#define ACCEPTABLE_CHARACTERS @"0123456789"


#import "ReceivedPOViewController.h"
#import "AllImportsViewController.h"
@interface ReceivePOCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *buttonSelectPart;
@property (weak, nonatomic) IBOutlet UITextField *textFieldItemType;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCategory;
@property (weak, nonatomic) IBOutlet UITextField *textFieldItemName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldOrderQuantity;
@property (weak, nonatomic) IBOutlet UITextField *textFieldQtyReceived;
@property (weak, nonatomic) IBOutlet UITextField *textFieldBuySaleRatio;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPrice;
@property (weak, nonatomic) IBOutlet UITextField *textFieldSerialNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldModelNumber;
@property (weak, nonatomic) IBOutlet UIButton *buttonManufacturer;


@end

@implementation ReceivePOCell


@end
///////////////////////////////

@interface ReceivedPOViewController ()

@end

@implementation ReceivedPOViewController
{
    Global *global;
    NSMutableArray *arrayPart;
    NSMutableDictionary *dictData;
    NSArray *arrayManufacturer;
    UITableView *tblData;
    UIButton *buttonBackground;
    NSDictionary *dictManufacturer;
}
#pragma mark - View's life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dictData = [NSMutableDictionary new];
    arrayPart = [NSMutableArray new];
    dictData = [_dictReceivedPO mutableCopy];
    global = [[Global alloc] init];
    
    // get manufacturer list
    {
        NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
        
        arrayManufacturer = [defs2 valueForKeyPath:@"MasterServiceAutomation.InventoryMasters.Manufacturers"];
        
        NSLog(@"%@",arrayManufacturer);
    }
    // configure pop up table view
    {
        tblData=[[UITableView alloc]init];
        tblData.frame=CGRectMake(0, 0, 300, 500);
        tblData.dataSource=self;
        tblData.delegate=self;
    }
    
    // add padding
    [self addPaddingInTextFields:_textFieldPurchaseOrderNumber];
    [self addPaddingInTextFields:_textFieldVendor];
    [self addPaddingInTextFields:_textFieldVendorLocation];
    [self addPaddingInTextFields:_textFieldDate];
    
    [self configureUI:_textFieldPurchaseOrderNumber];
    [self configureUI:_textFieldVendor];
    [self configureUI:_textFieldVendorLocation];
    [self configureUI:_textFieldDate];
    [self configureUI:_textFieldReceiveItemNote];
    [self configureUI:_textViewPONotes];
    
    _heightMainView.constant = _heightMainView.constant-_heightTableViewReceivedPO.constant;
    _heightTableViewReceivedPO.constant = 0.0;
    arrayPart = [[_dictReceivedPO valueForKey:@"PurchaseOrderDetailDcs"] mutableCopy];
    // show data
    
    _textFieldPurchaseOrderNumber.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"PurchaseOrderNumber"]];
    _textFieldVendor.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorName"]];
    _textFieldVendorLocation.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorLocationName"]];
    _textFieldDate.text = [global ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PurchaseDate"]]];
    if([[dictData valueForKey:@"PONotes"] length]>0)
    {
        _textViewPONotes.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"PONotes"]];
    }
    else
    {
        _textViewPONotes.text = @"";
    }
    
    if([arrayPart count]>0)
    {
        _heightMainView.constant = _heightMainView.constant-_heightTableViewReceivedPO.constant;
        _heightTableViewReceivedPO.constant = 0.0;
        _heightTableViewReceivedPO.constant = 605*[arrayPart count];
        _heightMainView.constant = _heightMainView.constant+_heightTableViewReceivedPO.constant;//578
        [_tableViewPart reloadData];
        for(int i=0;i<arrayPart.count;i++)
        {
            NSMutableDictionary *dict = [[arrayPart objectAtIndex:i] mutableCopy];
            [dict setValue:@"yes" forKey:@"partSelected"];
            [dict setValue:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Quantity"]] forKey:@"quantityReceived"];
            [dict setValue:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Quantity"]] forKey:@"orderQuantity"];
            [dict setValue:@"" forKey:@"buySaleRatio"];
            [dict setValue:@"" forKey:@"price"];
            [dict setValue:@"" forKey:@"manufacturer"];
            [dict setValue:@"" forKey:@"manufacturerId"];
            [dict setValue:@"" forKey:@"serialNumber"];
            [dict setValue:@"" forKey:@"modelNumber"];
            
            [arrayPart replaceObjectAtIndex:i withObject:dict];
        }
    }
    else
    {
        _heightMainView.constant = _heightMainView.constant-_heightTableViewReceivedPO.constant;
        _heightTableViewReceivedPO.constant = 0.0;
    }
}

#pragma mark - UITableView's delegate and data sour

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if(tableView.tag==10)
    {
        return arrayPart.count;
    }
    return arrayManufacturer.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    if(tableView.tag==10)
    {
        ReceivePOCell *cellPO = [tableView dequeueReusableCellWithIdentifier:@"ReceivePOCell"];
        if(cellPO==nil)
        {
            cellPO = [[ReceivePOCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReceivePOCell"];
        }
        
        // add padding
        [self addPaddingInTextFields:cellPO.textFieldItemType];
        [self addPaddingInTextFields:cellPO.textFieldCategory];
        [self addPaddingInTextFields:cellPO.textFieldItemName];
        [self addPaddingInTextFields:cellPO.textFieldOrderQuantity];
        [self addPaddingInTextFields:cellPO.textFieldQtyReceived];
        [self addPaddingInTextFields:cellPO.textFieldBuySaleRatio];
        [self addPaddingInTextFields:cellPO.textFieldPrice];
        [self addPaddingInTextFields:cellPO.textFieldSerialNumber];
        [self addPaddingInTextFields:cellPO.textFieldModelNumber];
        
        // configure UI
        [self configureUI:cellPO.textFieldItemType];
        [self configureUI:cellPO.textFieldCategory];
        [self configureUI:cellPO.textFieldItemName];
        [self configureUI:cellPO.textFieldOrderQuantity];
        [self configureUI:cellPO.textFieldQtyReceived];
        [self configureUI:cellPO.textFieldBuySaleRatio];
        [self configureUI:cellPO.textFieldPrice];
        [self configureUI:cellPO.buttonManufacturer];
        [self configureUI:cellPO.textFieldSerialNumber];
        [self configureUI:cellPO.textFieldModelNumber];
        
        cellPO.textFieldQtyReceived.tag = indexPath.row;
        cellPO.textFieldBuySaleRatio.tag = indexPath.row;
        cellPO.textFieldPrice.tag = indexPath.row;
        cellPO.textFieldSerialNumber.tag = indexPath.row;
        cellPO.textFieldModelNumber.tag = indexPath.row;
        
        if([[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"partSelected"] isEqualToString:@"yes"])
        {
            [cellPO.buttonSelectPart setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cellPO.buttonSelectPart setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        }
        cellPO.buttonSelectPart.tag = indexPath.row;
        [cellPO.buttonSelectPart addTarget:self action:@selector(actionOnSelectPart:) forControlEvents:UIControlEventTouchUpInside];
        
        cellPO.buttonManufacturer.tag = indexPath.row;
        [cellPO.buttonManufacturer addTarget:self action:@selector(actionOnSelectManufacturer:) forControlEvents:UIControlEventTouchUpInside];
        
        if([[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"manufacturer"] length]>0)
        {
            [cellPO.buttonManufacturer setTitle:[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"manufacturer"] forState:UIControlStateNormal];
        }
        else
        {
            [cellPO.buttonManufacturer setTitle:@"Select" forState:UIControlStateNormal];
        }
        
        cellPO.textFieldItemType.text = [NSString stringWithFormat:@"%@",[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"ItemTypeName"]];
        cellPO.textFieldItemName.text = [NSString stringWithFormat:@"%@",[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"ItemName"]];
        cellPO.textFieldCategory.text = [NSString stringWithFormat:@"%@",[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"CategoryName"]];
        cellPO.textFieldOrderQuantity.text = [NSString stringWithFormat:@"%@",[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"orderQuantity"]];
        
        cellPO.textFieldQtyReceived.text = [NSString stringWithFormat:@"%@",[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"quantityReceived"]];
        
        return cellPO;
    }
    
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = [[arrayManufacturer objectAtIndex:indexPath.row] valueForKey:@"Manufacturer"];
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(tableView.tag==10)
    {
        
    }
    else
    {
        dictManufacturer = [arrayManufacturer objectAtIndex:tableView.tag];
        
        NSMutableDictionary *dict = [[arrayPart objectAtIndex:tableView.tag] mutableCopy];
        [dict setValue:[[arrayManufacturer objectAtIndex:indexPath.row] valueForKey:@"Manufacturer"] forKey:@"manufacturer"];
        
        [dict setValue:[[arrayManufacturer objectAtIndex:indexPath.row] valueForKey:@"ManufacturerId"] forKey:@"manufacturerId"];
        
        [arrayPart replaceObjectAtIndex:tableView.tag withObject:dict];
        [_tableViewPart reloadData];
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        [_tableViewPart reloadData];
    }
}

#pragma mark  - UITextField delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(!(textField.text.length>0)&&[string isEqualToString:@" "])
    {
        return NO;
    }
    NSInteger row = textField.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    ReceivePOCell *tappedCell = (ReceivePOCell *)[_tableViewPart cellForRowAtIndexPath:indexPath];
    if(textField==tappedCell.textFieldPrice||textField==tappedCell.textFieldBuySaleRatio)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :100 :(int)textField.text.length :@".0123456789" :textField.text];
        
        if (isNuberOnly)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    if(textField==tappedCell.textFieldQtyReceived)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if([string isEqualToString:filtered]==YES)
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
        }
        return [string isEqualToString:filtered];
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSInteger row = textField.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    ReceivePOCell *tappedCell = (ReceivePOCell *)[_tableViewPart cellForRowAtIndexPath:indexPath];
    if(textField==tappedCell.textFieldQtyReceived)
    {
        NSMutableDictionary *dict = [[arrayPart objectAtIndex:indexPath.row] mutableCopy];
        
        if(textField.text>0)
        {
            [dict setValue:[NSString stringWithFormat:@"%@",textField.text] forKey:@"quantityReceived"];
        }
        else
        {
            [dict setValue:@"" forKey:@"quantityReceived"];
        }
        [arrayPart replaceObjectAtIndex:indexPath.row withObject:dict];
    }
    if(textField==tappedCell.textFieldBuySaleRatio)
    {
        NSMutableDictionary *dict = [[arrayPart objectAtIndex:indexPath.row] mutableCopy];
        
        if(textField.text>0)
        {
            [dict setValue:[NSString stringWithFormat:@"%@",textField.text] forKey:@"buySaleRatio"];
        }
        else
        {
            [dict setValue:@"" forKey:@"buySaleRatio"];
        }
        
        [arrayPart replaceObjectAtIndex:indexPath.row withObject:dict];
    }
    if(textField==tappedCell.textFieldPrice)
    {
        NSMutableDictionary *dict = [[arrayPart objectAtIndex:indexPath.row] mutableCopy];
        
        if(textField.text>0)
        {
            [dict setValue:[NSString stringWithFormat:@"%@",textField.text] forKey:@"price"];
        }
        else
        {
            [dict setValue:@"" forKey:@"price"];
        }
        
        [arrayPart replaceObjectAtIndex:indexPath.row withObject:dict];
    }
    
    if(textField==tappedCell.textFieldSerialNumber)
    {
        NSMutableDictionary *dict = [[arrayPart objectAtIndex:indexPath.row] mutableCopy];
        
        if(textField.text>0)
        {
            [dict setValue:[NSString stringWithFormat:@"%@",textField.text] forKey:@"serialNumber"];
        }
        else
        {
            [dict setValue:@"" forKey:@"serialNumber"];
        }
        
        [arrayPart replaceObjectAtIndex:indexPath.row withObject:dict];
    }
    
    if(textField==tappedCell.textFieldModelNumber)
    {
        NSMutableDictionary *dict = [[arrayPart objectAtIndex:indexPath.row] mutableCopy];
        
        if(textField.text>0)
        {
            [dict setValue:[NSString stringWithFormat:@"%@",textField.text] forKey:@"modelNumber"];
        }
        else
        {
            [dict setValue:@"" forKey:@"modelNumber"];
        }
        
        [arrayPart replaceObjectAtIndex:indexPath.row withObject:dict];
    }
    
}

#pragma mark - UIButton action
-(void)actionOnSelectPart:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    NSMutableDictionary *dict = [[arrayPart objectAtIndex:btn.tag] mutableCopy];
    
    if([[dict valueForKey:@"partSelected"] isEqualToString:@"yes"])
    {
        [dict setValue:@"no" forKey:@"partSelected"];
        [arrayPart replaceObjectAtIndex:btn.tag withObject:dict];
    }
    else
    {
        [dict setValue:@"yes" forKey:@"partSelected"];
        [arrayPart replaceObjectAtIndex:btn.tag withObject:dict];
    }
    
    [_tableViewPart reloadData];
}
-(void)actionOnSelectManufacturer:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    tblData.tag = btn.tag;
    [self setTableFrame];
    [tblData reloadData];
}

-(void)actionOnBackground:(id)sender
{
    [tblData removeFromSuperview];
    [buttonBackground removeFromSuperview];
}

- (IBAction)actionOnSave:(id)sender
{
    [self.view endEditing:YES];
    NSString *strItemNote = @"";
    if(_textFieldReceiveItemNote.text.length>0)
    {
        strItemNote = _textFieldReceiveItemNote.text;
    }
    if(arrayPart.count>0)// for standard purchase order
    {
        NSMutableArray *arrayJson = [NSMutableArray new];
        BOOL isItemSeltected = NO;
        for(NSDictionary *dictTemp in arrayPart)
        {
            if([[dictTemp valueForKey:@"partSelected"] isEqualToString:@"yes"])
            {
                isItemSeltected = YES;
            }
        }
        
        if(isItemSeltected==NO)
        {
            [global AlertMethod:Alert :@"Please select atleast one item to receive PO"];
            return;
        }
        else
        {
            for(NSDictionary *dict in arrayPart)
            {
                if([[dict valueForKey:@"partSelected"] isEqualToString:@"yes"])
                {
                    if(!([[dict valueForKey:@"quantityReceived"] length]>0))
                    {
                        [global AlertMethod:Alert :@"Please enter quantity received"];
                        return;
                    }
                    if(!([[dict valueForKey:@"buySaleRatio"] length]>0))
                    {
                        [global AlertMethod:Alert :@"Please enter buy sale ratio"];
                        return;
                    }
                    
                    if(!([[dict valueForKey:@"price"] length]>0))
                    {
                        [global AlertMethod:Alert :@"Please enter price"];
                        return;
                    }
                    if(!([[dict valueForKey:@"manufacturer"] length]>0))
                    {
                        [global AlertMethod:Alert :@"Please select manufacturer"];
                        return;
                    }
                    
                    // Removed validation for Serial No And Model no
                    /*
                    if(!([[dict valueForKey:@"serialNumber"] length]>0))
                    {
                        [global AlertMethod:Alert :@"Please enter serial number"];
                        return;
                    }
                    if(!([[dict valueForKey:@"modelNumber"] length]>0))
                    {
                        [global AlertMethod:Alert :@"Please enter model number"];
                        return;
                    }
                    */
                }
            }
            
            for(NSDictionary *dictTemp in arrayPart)
            {
                if([[dictTemp valueForKey:@"partSelected"] isEqualToString:@"yes"])
                {
                    NSDictionary *dictJson = @{
                                               @"RecieveItemId":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"ItemMasterId"]],
                                               @"PurchaseOrderDetailId":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"PurchaseDetailId"]],
                                               @"QuantityReceived":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"quantityReceived"]],
                                               @"Price":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"price"]],
                                               @"BuySaleRatio":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"buySaleRatio"]],
                                               @"IsActive":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"IsActive"]],
                                               @"CompanyId":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CompanyId"]],
                                               @"ItemTypeName":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"ItemTypeName"]],
                                               @"CategoryName":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"CategoryName"]],
                                               @"ItemName":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"ItemName"]],
                                               @"OrdereQuantity":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"Quantity"]],
                                               @"TotalReceivedQuantity":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"quantityReceived"]],
                                               @"ManufacturerId":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"manufacturerId"]],
                                               @"ModelNumber":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"modelNumber"]],
                                               @"SerialNumber":[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"serialNumber"]]
                                               };
                    
                    [arrayJson addObject:dictJson];
                }
            }
        }
        
        
        NSDictionary *dict = @{@"PurchaseOrderId":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PurchaseOrderId"]],
                               @"VenderId":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorMasterId"]],
                               @"VenderLocationId":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorLocationId"]],
                               @"Date":[NSString stringWithFormat:@"%@",_textFieldDate.text],
                               @"PurchaseOrderNote":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PONotes"]],
                               @"ReceiveItemNote":strItemNote,
                               @"IsActive":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsActive"]],
                               @"CompanyId":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CompanyId"]],
                               @"PurchaseOrderNumber":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PurchaseOrderNumber"]],
                               @"VenderName":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorName"]],
                               @"VenderLocationName":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorLocationName"]],
                               @"RecieveItemType":[dictData valueForKey:@"POType"],
                               @"PODescription":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PODescription"]],
                               @"RecieveItemDetailDcs":arrayJson,//array
                               @"RecieveItemId":@""
                               
                               };
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"please wait..."];
        
        DELAY(1.0);
        [self syncDataOnServer:dict];
    }
    else// for generic purchase order
    {
        NSMutableArray *arrayJson = [NSMutableArray new];
        NSDictionary *dict = @{@"PurchaseOrderId":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PurchaseOrderId"]],
                               @"VenderId":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorMasterId"]],
                               @"VenderLocationId":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorLocationId"]],
                               @"Date":[NSString stringWithFormat:@"%@",_textFieldDate.text],
                               @"PurchaseOrderNote":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PONotes"]],
                               @"ReceiveItemNote":strItemNote,
                               @"IsActive":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsActive"]],
                               @"CompanyId":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CompanyId"]],
                               @"PurchaseOrderNumber":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PurchaseOrderNumber"]],
                               @"VenderName":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorName"]],
                               @"VenderLocationName":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorLocationName"]],
                               @"RecieveItemType":[dictData valueForKey:@"POType"],
                               @"PODescription":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PODescription"]],
                               @"RecieveItemDetailDcs":arrayJson,//array,
                               @"RecieveItemId":@""
                               
                               };
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"please wait..."];
        
        DELAY(1.0);
        [self syncDataOnServer:dict];
    }
}

- (IBAction)actionOnCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionOnBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - General Methods
-(void)configureUI:(UIView*)view
{
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
    view.layer.cornerRadius = 4.0;
}

-(void)addPaddingInTextFields:(UITextField*)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}


-(void)setTableFrame
{
    buttonBackground=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    buttonBackground.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    
    [buttonBackground addTarget:self action:@selector(actionOnBackground:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: buttonBackground];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    
    tblData.layer.borderWidth = 1.0;
    tblData.layer.borderColor = [[UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0] CGColor];
    
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
    }
    
    [self.view addSubview:tblData];
}

#pragma mark - web api
-(void)syncDataOnServer:(NSDictionary*)dictJson
{
    
    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
    NSString *strCompanyKeyTemp = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.CoreServiceModule.ServiceUrl"];
    // NSString *strEmpNoTemp=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@/api/MobileAppToCore/ManageReceiveItem?companyKey=%@",strServiceUrlMain,strCompanyKeyTemp];

    //NSString *strUrl = @"http://tcrs.stagingsoftware.com//api/MobileAppToCore/ManageReceiveItem?companyKey=Automation";
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictJson options:NSJSONWritingPrettyPrinted error:Nil];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonDatastr %@",str);
    NSURL * serviceUrl = [NSURL URLWithString:strUrl];
    NSLog(@"REquest URL >> %@",serviceUrl);
    NSLog(@"REquest XML >> %@",str);
    
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:serviceUrl];
    
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *__block serviceResponse;
    NSError *__block serviceError;
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&serviceResponse error:&serviceError];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [DejalBezelActivityView removeView];
            
        });
        
        if (responseData)
        {
            [self parsePostApiData:responseData];
        }
        else
        {
            [global AlertMethod:Alert :@"Something went wrong."];
        }
        
    });
}

-(void)parsePostApiData:(NSData *)response
{
    id jsonObject = Nil;
    
    NSString *charlieSendString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSLog(@"ResponseString %@",charlieSendString);
    
    if (response==nil)
    {
        NSLog(@"No internet connection.");
    }
    else
    {
        NSError *error = Nil;
        jsonObject =[NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            NSLog(@"Probably An Array %@",jsonObject);
            
            if([[[jsonObject objectAtIndex:0] valueForKey:@"Value"] boolValue]==1)
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [global AlertMethod:@"Message" :[[jsonObject objectAtIndex:1] valueForKey:@"Value"]];
                    [self.navigationController popViewControllerAnimated:YES];
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [global AlertMethod:@"Message" :[[jsonObject objectAtIndex:1] valueForKey:@"Value"]];
                    
                });
                
                
            }
        }
        else
        {
            NSLog(@"Probably A Dictionary");
            NSDictionary *jsonDictionary=(NSDictionary *)jsonObject;
            NSLog(@"jsonDictionary %@",[jsonDictionary description]);
        }
    }
    
    //sleep(3);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
       [self dismissViewControllerAnimated:YES completion:nil];
        
    });
    //[self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
