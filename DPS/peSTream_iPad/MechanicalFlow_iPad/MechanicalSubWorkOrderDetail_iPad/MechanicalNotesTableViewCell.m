//
//  MechanicalNotesTableViewCell.m
//  DPS
//  peSTream
//  Created by Saavan Patidar on 30/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream

#import "MechanicalNotesTableViewCell.h"

@implementation MechanicalNotesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
