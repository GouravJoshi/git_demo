//
//  MechanicalServiceIssueTableViewCell.h
//  DPS
//  peSTream
//  Created by Saavan Patidar on 29/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream

#import <UIKit/UIKit.h>

@interface MechanicalServiceIssueTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblRepairName;
@property (strong, nonatomic) IBOutlet UILabel *lblLR;
@property (strong, nonatomic) IBOutlet UILabel *lblHLR;
@property (strong, nonatomic) IBOutlet UILabel *lblPart;
@property (strong, nonatomic) IBOutlet UILabel *lblAmt;
@property (strong, nonatomic) IBOutlet UILabel *lblRepairDesc;
@property (strong, nonatomic) IBOutlet UIButton *btnAddParts;
@property (strong, nonatomic) IBOutlet UIButton *btnAddLabour;
@property (strong, nonatomic) IBOutlet UIButton *btnDeleteRepair;
@property (strong, nonatomic) IBOutlet UISwitch *switchStatus;
@property (strong, nonatomic) IBOutlet UIButton *btnViewMore;
@property (strong, nonatomic) IBOutlet UITextField *txtFldQty;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalAmount;
@end
