//
//  MechanicalNotesTableViewCell.h
//  DPS
//  peSTream
//  Created by Saavan Patidar on 30/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream

#import <UIKit/UIKit.h>

@interface MechanicalNotesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblAddedOn;
@property (strong, nonatomic) IBOutlet UILabel *lblAddedBy;
@property (strong, nonatomic) IBOutlet UILabel *lblNoteDesc;
@property (strong, nonatomic) IBOutlet UIButton *btnViewMoreNoteDesc;

@end
