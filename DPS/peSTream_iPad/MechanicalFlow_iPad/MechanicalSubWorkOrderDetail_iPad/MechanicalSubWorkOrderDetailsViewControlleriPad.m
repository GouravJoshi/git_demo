//
//  MechanicalSubWorkOrderDetailsViewController.m
//  DPS
//  peSTream
//  Created by Saavan Patidar on 29/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream



//Tags in view are as follows------

//For tableview starts from 1
//For Views starts from 201
//For Pop tablview starts from 101
//For txtviews starts from 301


#import "MechanicalSubWorkOrderDetailsViewControlleriPad.h"
#import "DPS-Swift.h"

@interface MechanicalSubWorkOrderDetailsViewControlleriPad ()
{
    
    Global *global;
    NSString *strWorkOrderId,*strSubWorkOrderIdGlobal,*strPriorityIdGlobal,*strEmpID,*strUserName,*strCompanyKey,*strEmpName,*strHelperIdGlobal,*strRepairIdGlobal,*strDepartMentSysName,*strSubWorkOrderIssueIdGlobal,*strGlobalAmtRepair,*strWoStatus,*strWorkOrderStatuss,*strRepairOptionIdGlobal,*strNotificationTypeName,*strWorkOrderAccNo,*strWorkOrderAddressSubType,*strWorkOrderAddressId, *strAfterHrsDuration,*strCompanyName,*strPartCostAdjustmentGlobal,*strAmountTotalRepairRunTime,*strEmployeeNoLoggedIn,*strEmployeeNo;
    NSMutableArray *arrOfSubWorkServiceIssues,*arrDataTblView,*arrOfSubWorkServiceHelper,*arrOfSubWorkServiceNotes,*arrOfSubWorkServiceActualHrs,*arrOfHrsTotal,*arrOfSubWorkServiceIssuesRepair,*arrOfSubWorkServiceIssuesRepairParts,*arrOfSubWorkServiceIssuesRepairLabour,*arrOfPriceLookup,*arrOfHoursConfig,*arrOfPartsGlobalMasters,*arrOfBeforeImageAll,*arrOfImagenameCollewctionView,*arrOfImageCaption,*arrOfImageDescription,*arrOfSelectedRepairOption,*arrOfMechanicalWoEquipment,*arrOfHeaderTitleForSlots,*arrOfEmployeesAssignedInSubWorkOrder,*arrOfGlobalDynamicEmpSheetFinal;
    UIView *viewBackAlertt,*viewBackGround,*viewBackGroundOnView;
    UITableView *tblData;
    NSDictionary *dictDetailsFortblView,*dictDetailsMastersMechanical,*dictLaborInfoToSave;
    int indexToShow, secondsTimer, minutesTimer, hoursTimer, totalsecond ,indexRepairGlobal, intGlobalSection,indexRepairOptionGlobal;
    BOOL isCollapseAllViews , isStandardRepair, isStandardSubWorkOrder, isJobStarted, isClientApproved, yesEditedSomething, isImageTaken, isHoliday, isTotalRepairCost, isEditIssues, isCompletedStatusMechanical, isMechanic;
    NSTimer *jobMinTimer;
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    float multiplierGlobal;
    NSManagedObject *objSelectedEquipment,*objIssuesToEditEquipment;
    NSMutableArray *arrOfTimeSlotsEmployeeWise,*arrOfWorkingDate;
    
}

@property (strong, nonatomic) IBOutlet UIButton *btnAddServiceIssue;
@property (strong, nonatomic) IBOutlet UIButton *btnAddNotes;
@property (strong, nonatomic) IBOutlet UIButton *btnAddHelper;
@property (strong, nonatomic) IBOutlet UIImageView *btnArrowDown;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewEmpTimeSheet;

@end


@implementation MechanicalSubWorkOrderDetailsViewControlleriPad
@synthesize scrollViewEmpTimeSheet;

- (void)viewDidLoad {
    
    intGlobalSection=-1;
    isImageTaken=NO;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"" forKey:@"ScannedResult"];
    [defs setBool:NO forKey:@"IsScannerView"];
    [defs setBool:NO forKey:@"IsScannerViewEquip"];
    [defs synchronize];
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    isMechanic=NO;
    [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    isEditIssues=NO;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    BOOL isFromOtherPresentedView=[defs boolForKey:@"isFromOtherPresentedView"];
    
    BOOL isFromScannerEquip=[defs boolForKey:@"IsScannerViewEquip"];
    
    if (isFromScannerEquip) {
        
        NSString *strScannedResult=[defs valueForKey:@"ScannedResult"];
        
        if (strScannedResult.length>0) {
            
            [self fetchWoEquipmentFromDBToFilterScannedEquip:strScannedResult];
            
        }
        
        //[global AlertMethod:@"Scanned-Code" :strScannedResult];
        
        [defs setValue:@"" forKey:@"ScannedResult"];
        [defs setBool:YES forKey:@"IsScannerViewEquip"];
        [defs synchronize];
        
        
    }else if (isImageTaken) {
        isImageTaken=NO;
        
    }else if (isFromOtherPresentedView){
        
        [defs setBool:NO forKey:@"isFromOtherPresentedView"];
        [defs synchronize];
        
        [self allMethodsCalling];
        [self methodViewWillAppear];
        
    }
    else{
        
        [self allMethodsCalling];
        
        [self methodViewWillAppear];
        
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//============================================================================
//============================================================================
#pragma mark- ----------------Void Methods----------------
//============================================================================
//============================================================================

-(void)allMethodsCalling{
    
    [self methodAllocation];
    
    [self methodBorderColor];
    
    [self addViewOnLoading];
    
    [self loadValues];
    
    [self getHoursConfiFromMasters];
    
    //[self getPricLookupFromMaster];
    
    [self LoadProfileImage];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderHelperFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderNotesFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderActualHrsFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    
    [self fetchImageDetailFromDataBaseMechanical];
    
    [self getPartsMaster];
    
    [self methodStatus];
    
}

-(void)hideAllTxtViewsnFields{
    
    [_txtViewIssuesDesc resignFirstResponder];
    [_txtViewReason resignFirstResponder];
    [_txtRepairAmt resignFirstResponder];
    [_txtViewAddNotesDesc resignFirstResponder];
    [_txtRepairCostAdjustment resignFirstResponder];
    [_txtViewRepairDescriptions resignFirstResponder];
    
}
-(void)methodDonePriorityTableview{
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
}

-(void)methodCancelPriorityTableview{
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
}

-(void)methodBack{
    
    [jobMinTimer invalidate];
    [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k1=0; k1<arrstack.count; k1++) {
        if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalGeneralInfoViewController class]]) {
            index=k1;
            //break;
        }
    }
    MechanicalGeneralInfoViewController *myController = (MechanicalGeneralInfoViewController *)[self.navigationController.viewControllers objectAtIndex:index];
    // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    [self.navigationController popToViewController:myController animated:NO];
    
}

-(void)addViewOnLoading{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //Adding _viewSubWorkOrderDetails
        
        CGRect frameFor_view_SubWorkOrderInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,_viewSubWorkOrderDetails.frame.size.height);
        [_viewSubWorkOrderDetails setFrame:frameFor_view_SubWorkOrderInfo];
        [_scrollVieww addSubview:_viewSubWorkOrderDetails];
        
        //Adding _view_ServiceIssues
        
        CGRect frameFor_view_ServiceIssues=CGRectMake(0, _viewSubWorkOrderDetails.frame.origin.y+_viewSubWorkOrderDetails.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssues.count*60+70+50);
        [_view_ServiceIssues setFrame:frameFor_view_ServiceIssues];
        [_scrollVieww addSubview:_view_ServiceIssues];
        
        _tblViewServiceIssues.tag=1;
        _tblViewServiceIssues.frame=CGRectMake(_tblViewServiceIssues.frame.origin.x, _tblViewServiceIssues.frame.origin.y, _tblViewServiceIssues.frame.size.width, arrOfSubWorkServiceIssues.count*60+70+50);
        
        //Adding _viewHelper
        
        CGRect frameFor_viewHelper=CGRectMake(0, _view_ServiceIssues.frame.origin.y+_view_ServiceIssues.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceHelper.count*80+70+70+50);
        [_viewHelper setFrame:frameFor_viewHelper];
        [_scrollVieww addSubview:_viewHelper];
        
        _tblviewHelper.tag=2;
        _tblviewHelper.frame=CGRectMake(_tblviewHelper.frame.origin.x, _tblviewHelper.frame.origin.y, _tblviewHelper.frame.size.width, arrOfSubWorkServiceHelper.count*80+70+70+50);
        
        
        //Adding _viewNotes
        
        CGRect frameFor_viewNotes=CGRectMake(0, _viewHelper.frame.origin.y+_viewHelper.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceNotes.count*175+70+50);
        [_viewNotes setFrame:frameFor_viewNotes];
        [_scrollVieww addSubview:_viewNotes];
        
        _tblViewNotes.tag=3;
        _tblViewNotes.frame=CGRectMake(_tblViewNotes.frame.origin.x, _tblViewNotes.frame.origin.y, _tblViewNotes.frame.size.width, arrOfSubWorkServiceNotes.count*175+70+50);
        
        //Adding viewClientApproval
        
        CGRect frameFor_viewClientApproval=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_viewClientApproval.frame.size.height);
        [_viewClientApproval setFrame:frameFor_viewClientApproval];
        //  [_scrollVieww addSubview:_viewClientApproval];
        
        //Adding viewAdditionalInfo
        
        CGRect frameFor_viewAdditionalInfo=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height);
        [_view_AdditionalInfo setFrame:frameFor_viewAdditionalInfo];
        [_scrollVieww addSubview:_view_AdditionalInfo];
        
        _tblViewNotes.tag=3;
        _tblViewNotes.frame=CGRectMake(_tblViewNotes.frame.origin.x, _tblViewNotes.frame.origin.y, _tblViewNotes.frame.size.width, arrOfSubWorkServiceNotes.count*175+70+50);
        
        [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height+_view_AdditionalInfo.frame.origin.y)];
        
    });
    
}

-(void)adjustViewHeights{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //Adding _view_ServiceIssues
        
        CGRect frameFor_view_ServiceIssues=CGRectMake(0, _viewSubWorkOrderDetails.frame.origin.y+_viewSubWorkOrderDetails.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssues.count*60+70);
        [_view_ServiceIssues setFrame:frameFor_view_ServiceIssues];
        //  [_scrollVieww addSubview:_view_ServiceIssues];
        
        _tblViewServiceIssues.frame=CGRectMake(_tblViewServiceIssues.frame.origin.x, _tblViewServiceIssues.frame.origin.y, _tblViewServiceIssues.frame.size.width, arrOfSubWorkServiceIssues.count*60+70+50);
        
        //Adding _viewHelper
        
        CGRect frameFor_viewHelper=CGRectMake(0, _view_ServiceIssues.frame.origin.y+_view_ServiceIssues.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceHelper.count*80+70+70+50);
        [_viewHelper setFrame:frameFor_viewHelper];
        // [_scrollVieww addSubview:_viewHelper];
        
        _tblviewHelper.frame=CGRectMake(_tblviewHelper.frame.origin.x, _tblviewHelper.frame.origin.y, _tblviewHelper.frame.size.width, arrOfSubWorkServiceHelper.count*80+70+70+50);
        
        
        //Adding _viewNotes
        
        CGRect frameFor_viewNotes=CGRectMake(0, _viewHelper.frame.origin.y+_viewHelper.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceNotes.count*175+70+50);
        [_viewNotes setFrame:frameFor_viewNotes];
        // [_scrollVieww addSubview:_viewNotes];
        
        _tblViewNotes.frame=CGRectMake(_tblViewNotes.frame.origin.x, _tblViewNotes.frame.origin.y, _tblViewNotes.frame.size.width, arrOfSubWorkServiceNotes.count*175+70+50);
        
        //Adding viewClientApproval
        
        CGRect frameFor_viewClientApproval=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_viewClientApproval.frame.size.height);
        [_viewClientApproval setFrame:frameFor_viewClientApproval];
        // [_scrollVieww addSubview:_viewClientApproval];
        
        
        //Adding viewAdditionalInfo
        
        CGRect frameFor_viewAdditionalInfo=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height);
        [_view_AdditionalInfo setFrame:frameFor_viewAdditionalInfo];
        //[_scrollVieww addSubview:_view_AdditionalInfo];
        
        [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height+_view_AdditionalInfo.frame.origin.y)];
        
        [self adjustViewHeightsonCollapsingSections];
        
    });
    
}
-(void)adjustViewHeightsonCollapsingSections{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //Adding _view_ServiceIssues
        
        if (isCollapseAllViews) {
            
            CGRect frameFor_view_ServiceIssues=CGRectMake(0, _viewSubWorkOrderDetails.frame.origin.y+_viewSubWorkOrderDetails.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssues.count*60+70+50);
            [_view_ServiceIssues setFrame:frameFor_view_ServiceIssues];
            //  [_scrollVieww addSubview:_view_ServiceIssues];
            
            _tblViewServiceIssues.frame=CGRectMake(_tblViewServiceIssues.frame.origin.x, _tblViewServiceIssues.frame.origin.y, _tblViewServiceIssues.frame.size.width, arrOfSubWorkServiceIssues.count*60+70+50);
            
        } else {
            
            NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexToShow];
            
            NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
            
            int k1=0;
            
            for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
                
                NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
                
                NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
                
                if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                    
                    k1++;
                    
                }
                
            }
            
            
            CGRect frameFor_view_ServiceIssues=CGRectMake(0, _viewSubWorkOrderDetails.frame.origin.y+_viewSubWorkOrderDetails.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssues.count*60+70+380*k1+50);
            [_view_ServiceIssues setFrame:frameFor_view_ServiceIssues];
            //  [_scrollVieww addSubview:_view_ServiceIssues];
            
            _tblViewServiceIssues.frame=CGRectMake(_tblViewServiceIssues.frame.origin.x, _tblViewServiceIssues.frame.origin.y, _tblViewServiceIssues.frame.size.width, arrOfSubWorkServiceIssues.count*60+70+380*k1+50);
            
        }
        
        //Adding _viewHelper
        
        CGRect frameFor_viewHelper=CGRectMake(0, _view_ServiceIssues.frame.origin.y+_view_ServiceIssues.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceHelper.count*80+70+70+50);
        [_viewHelper setFrame:frameFor_viewHelper];
        // [_scrollVieww addSubview:_viewHelper];
        
        _tblviewHelper.frame=CGRectMake(_tblviewHelper.frame.origin.x, _tblviewHelper.frame.origin.y, _tblviewHelper.frame.size.width, arrOfSubWorkServiceHelper.count*80+70+70+50);
        
        
        //Adding _viewNotes
        
        CGRect frameFor_viewNotes=CGRectMake(0, _viewHelper.frame.origin.y+_viewHelper.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceNotes.count*175+70+50);
        [_viewNotes setFrame:frameFor_viewNotes];
        // [_scrollVieww addSubview:_viewNotes];
        
        _tblViewNotes.frame=CGRectMake(_tblViewNotes.frame.origin.x, _tblViewNotes.frame.origin.y, _tblViewNotes.frame.size.width, arrOfSubWorkServiceNotes.count*175+70+50);
        
        //Adding viewClientApproval
        
        CGRect frameFor_viewClientApproval=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_viewClientApproval.frame.size.height);
        [_viewClientApproval setFrame:frameFor_viewClientApproval];
        // [_scrollVieww addSubview:_viewClientApproval];
        
        //Adding viewAdditionalInfo
        
        CGRect frameFor_viewAdditionalInfo=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height);
        [_view_AdditionalInfo setFrame:frameFor_viewAdditionalInfo];
        //[_scrollVieww addSubview:_view_AdditionalInfo];
        
        [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height+_view_AdditionalInfo.frame.origin.y)];
        
    });
}

-(void)LoadProfileImage{
    
    NSUserDefaults *defsPhoto=[NSUserDefaults standardUserDefaults];
    NSString *strSavedPhoto=[defsPhoto valueForKey:@"EmployeePhoto"];
    NSString *urlString = [NSString stringWithFormat:@"%@",strSavedPhoto];
    NSString *strNewString=[urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    [_imgViewProfile sd_setImageWithURL:[NSURL URLWithString:strNewString] placeholderImage:[UIImage imageNamed:@"profile.png"] options:SDWebImageRefreshCached];
    
}

-(void)methodBorderColor{
    
    _txtViewTechComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewTechComment.layer.borderWidth=1.0;
    _txtViewTechComment.layer.cornerRadius=5.0;
    
    _txtViewAdditionalInfo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAdditionalInfo.layer.borderWidth=1.0;
    _txtViewAdditionalInfo.layer.cornerRadius=5.0;
    
    _txtViewIssuesDesc.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewIssuesDesc.layer.borderWidth=1.0;
    _txtViewIssuesDesc.layer.cornerRadius=5.0;
    
    _txtViewRepairDescriptions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewRepairDescriptions.layer.borderWidth=1.0;
    _txtViewRepairDescriptions.layer.cornerRadius=5.0;
    
    _txtViewAddNotesDesc.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAddNotesDesc.layer.borderWidth=1.0;
    _txtViewAddNotesDesc.layer.cornerRadius=5.0;
    
    _txtViewReason.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewReason.layer.borderWidth=1.0;
    _txtViewReason.layer.cornerRadius=5.0;
    
    [_btnEmail.layer setCornerRadius:5.0f];
    [_btnEmail.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnEmail.layer setBorderWidth:0.8f];
    [_btnEmail.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnEmail.layer setShadowOpacity:0.3];
    [_btnEmail.layer setShadowRadius:3.0];
    [_btnEmail.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectReason.layer setCornerRadius:5.0f];
    [_btnSelectReason.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectReason.layer setBorderWidth:0.8f];
    [_btnSelectReason.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectReason.layer setShadowOpacity:0.3];
    [_btnSelectReason.layer setShadowRadius:3.0];
    [_btnSelectReason.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnPrioityAddServiceIssues.layer setCornerRadius:5.0f];
    [_btnPrioityAddServiceIssues.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnPrioityAddServiceIssues.layer setBorderWidth:0.8f];
    [_btnPrioityAddServiceIssues.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnPrioityAddServiceIssues.layer setShadowOpacity:0.3];
    [_btnPrioityAddServiceIssues.layer setShadowRadius:3.0];
    [_btnPrioityAddServiceIssues.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    
    [_btn_AddEquipments.layer setCornerRadius:5.0f];
    [_btn_AddEquipments.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_AddEquipments.layer setBorderWidth:0.8f];
    [_btn_AddEquipments.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_AddEquipments.layer setShadowOpacity:0.3];
    [_btn_AddEquipments.layer setShadowRadius:3.0];
    [_btn_AddEquipments.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectRepair.layer setCornerRadius:5.0f];
    [_btnSelectRepair.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectRepair.layer setBorderWidth:0.8f];
    [_btnSelectRepair.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectRepair.layer setShadowOpacity:0.3];
    [_btnSelectRepair.layer setShadowRadius:3.0];
    [_btnSelectRepair.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectOption.layer setCornerRadius:5.0f];
    [_btnSelectOption.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectOption.layer setBorderWidth:0.8f];
    [_btnSelectOption.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectOption.layer setShadowOpacity:0.3];
    [_btnSelectOption.layer setShadowRadius:3.0];
    [_btnSelectOption.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectHelper.layer setCornerRadius:5.0f];
    [_btnSelectHelper.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectHelper.layer setBorderWidth:0.8f];
    [_btnSelectHelper.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectHelper.layer setShadowOpacity:0.3];
    [_btnSelectHelper.layer setShadowRadius:3.0];
    [_btnSelectHelper.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}

-(void)loadValues{
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strEmployeeNoLoggedIn=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strCompanyName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmployeeNo     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strWoStatus       =[defsLead valueForKey:@"WoStatus"];
    
    dictDetailsFortblView=[defsLead valueForKey:@"TotalLeadCountResponse"];
    
    dictDetailsMastersMechanical=[defsLead valueForKey:@"MasterServiceAutomation"];
    
    strWorkOrderId=[defsLead valueForKey:@"LeadId"];
    
    _objSubWorkOrderdetails=[global fetchMechanicalSubWorkOrderObj:strWorkOrderId :_strSubWorkOrderId];
    
    _objWorkOrderdetails=[global fetchMechanicalWorkOrderObj:strWorkOrderId];
    
    //
    
    strWorkOrderAccNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"accountNo"]];
    
    strWorkOrderAddressId=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"serviceAddressId"]];
    
    strWorkOrderAddressSubType=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"addressSubType"]];
    
    //_lblCustNamenAccNo.text=[defsLead valueForKey:@"lblName"];
    _lblCustNamenAccNo.text=[NSString stringWithFormat:@"%@, Sub Work Order #: %@",[defsLead valueForKey:@"lblName"],[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderNo"]]];
    
    strWorkOrderStatuss=[_objSubWorkOrderdetails valueForKey:@"subWOStatus"];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodStatus];
    
    _strWoType=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWOType"]];
    
    strSubWorkOrderIdGlobal=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderId"]];
    strDepartMentSysName=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"departmentSysName"]];
    _lblWorkOrderNi.text=[NSString stringWithFormat:@"Workorder #: %@",[_objWorkOrderdetails valueForKey:@"workOrderNo"]];
    _lblAccountNo.text=[NSString stringWithFormat:@"%@ %@",[_objWorkOrderdetails valueForKey:@"accountNo"],[defsLead valueForKey:@"customerNameService"]];
    
    NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"thirdPartyAccountNo"]];
    
    if (strThirdPartyAccountNo.length>0) {
        
        _lblAccountNo.text=[NSString stringWithFormat:@"%@ %@",strThirdPartyAccountNo,[defsLead valueForKey:@"customerNameService"]];;
        
    }else{
        
        strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"accountNo"]];
        _lblAccountNo.text=[NSString stringWithFormat:@"%@ %@",strThirdPartyAccountNo,[defsLead valueForKey:@"customerNameService"]];;
        
    }
    
    _lblSubWorkOrderNo.text=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderId"]];//subWOStatus
    _lblStatus.text=[NSString stringWithFormat:@"Status: %@",[_objWorkOrderdetails valueForKey:@"workorderStatus"]];
    _lblStatus.text=[NSString stringWithFormat:@"Status: %@",[_objSubWorkOrderdetails valueForKey:@"subWOStatus"]];
    
    if ([_lblStatus.text isEqualToString:@"Status: Inspection"]) {
        
        _lblStatus.text=[NSString stringWithFormat:@"Status: %@",@"In Progress"];
        
    }
    
    [self methodToCheckIfValuesAreNull];
    
    _lblPhoneNo.text=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"primaryPhone"]];
    
    NSString *strServiceAddress=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"serviceCountry"]];
    
    if ([strServiceAddress caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strServiceAddress=@"United States";
    }
    NSString *strServiceAddress2=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"billingAddress2"]];
    if (strServiceAddress2.length==0) {
        
        _lblAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[_objWorkOrderdetails valueForKey:@"servicesAddress1"],[_objWorkOrderdetails valueForKey:@"serviceCity"],[_objWorkOrderdetails valueForKey:@"serviceState"],strServiceAddress,[_objWorkOrderdetails valueForKey:@"serviceZipcode"]];
        
        
    }else{
        
        _lblAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",[_objWorkOrderdetails valueForKey:@"servicesAddress1"],[_objWorkOrderdetails valueForKey:@"serviceAddress2"],[_objWorkOrderdetails valueForKey:@"serviceCity"],[_objWorkOrderdetails valueForKey:@"serviceState"],strServiceAddress,[_objWorkOrderdetails valueForKey:@"serviceZipcode"]];
        
        
    }
    [_btnEmail setTitle:[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"primaryEmail"]] forState:UIControlStateNormal];
    
    NSString *strTotalEstTimeSubWorkOrder=[NSString stringWithFormat:@"Est. Time: %@",[_objSubWorkOrderdetails valueForKey:@"totalEstimationTime"]];
    
    if ([NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"totalEstimationTime"]].length>5) {
        
        NSRange equalRange = [strTotalEstTimeSubWorkOrder rangeOfString:@":" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strTotalEstTimeSubWorkOrder = [strTotalEstTimeSubWorkOrder substringToIndex:equalRange.location];
        }
    }
    [_btnEstTime setTitle:strTotalEstTimeSubWorkOrder forState:UIControlStateNormal];
    
    strGlobalAmtRepair=@"";
    
    NSString *strIsActive=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isStdPrice"]];
    
    if ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) {
        
        isStandardSubWorkOrder=YES;
        
    } else {
        
        isStandardSubWorkOrder=NO;
        
    }
    
    //isHoliday
    
    NSString *strIsHoliday=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isHolidayHrs"]];
    
    if ([strIsHoliday isEqualToString:@"true"] || [strIsHoliday isEqualToString:@"1"]) {
        
        isHoliday=YES;
        
    } else {
        
        isHoliday=NO;
        
    }
    
    //AfterHrsDuration
    
    strAfterHrsDuration=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"afterHrsDuration"]];
    
    //isClientApproved
    NSString *strIsClientApprovalRequired=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isClientApprovalReq"]];
    
    if ([strIsClientApprovalRequired isEqualToString:@"true"] || [strIsClientApprovalRequired isEqualToString:@"1"]) {
        
        NSString *strIsClientApproved=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isClientApproved"]];
        
        if ([strIsClientApproved isEqualToString:@"true"] || [strIsClientApproved isEqualToString:@"1"]) {
            
            isClientApproved=YES;
            
        }else{
            
            isClientApproved=NO;
            
        }
    } else {
        
        isClientApproved=YES;
        
    }
    
    //Additional Info
    
    _txtViewAdditionalInfo.text=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"inspectionresults"]];
    
    _txtViewTechComment.text=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"technicianComment"]];
    
}
-(void)methodAllocation{
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    
    indexToShow=[[defss valueForKey:@"sectionToOpen"] intValue];
    
    intGlobalSection=indexToShow;
    
    if (indexToShow==-1) {
        
        isCollapseAllViews=YES;
        
    } else {
        
        isCollapseAllViews=NO;
        
    }
    
    isStandardRepair=YES;
    global = [[Global alloc] init];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodStatus];
    
    arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
    arrOfSubWorkServiceHelper=[[NSMutableArray alloc]init];
    arrOfSubWorkServiceNotes=[[NSMutableArray alloc]init];
    arrDataTblView=[[NSMutableArray alloc]init];
    arrOfSubWorkServiceActualHrs=[[NSMutableArray alloc]init];
    arrOfHrsTotal=[[NSMutableArray alloc]init];
    arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
    arrOfBeforeImageAll=[[NSMutableArray alloc]init];
    arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    
    [_tblViewServiceIssues reloadData];
    [_tblviewHelper reloadData];
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
    // [self adjustViewHeights];
    
}


-(void)methodTableViewAllocation{
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    // arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
}


-(void)methodAddServiceIssuesView{
    
    [_btn_AddEquipments setTitle:@"----Select Equipment----" forState:UIControlStateNormal];
    [_btnSaveServiceIssues setTitle:@"Save Issue" forState:UIControlStateNormal];
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, 105, [UIScreen mainScreen].bounds.size.width,_viewAddServiceIssues.frame.size.height);
    [_viewAddServiceIssues setFrame:frameFor_view_ServiceIssues];
    [viewBackGround addSubview:_viewAddServiceIssues];
    
    [self setDeafultPriorityHigh];
}

-(void)setDeafultPriorityHigh{
    
    NSArray *arrOfStatus=[dictDetailsFortblView valueForKey:@"PriorityMasters"];
    
    for (int k=0; k<arrOfStatus.count; k++) {
        
        NSDictionary *dictDataa=arrOfStatus[k];
        
        NSString *strSysNamePriority=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
        
        if ([strSysNamePriority isEqualToString:@"High"]) {
            
            [_btnPrioityAddServiceIssues setTitle:@"High" forState:UIControlStateNormal];
            strPriorityIdGlobal=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"PriorityId"]];
            
            break;
        }
    }
    
}

-(void)methodAddServiceIssuesViewOnEdit{
    
    //[_btn_AddEquipments setTitle:@"----Select Equipment----" forState:UIControlStateNormal];
    //objIssuesToEditEquipment
    [_btnSaveServiceIssues setTitle:@"Update Issue" forState:UIControlStateNormal];
    
    NSString *strequipmentNoFromDB=[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"equipmentNo"]];
    NSString *strequipmentNameFromDB=[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"equipmentName"]];
    NSString *strServiceIssuesFromDB=[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"serviceIssue"]];
    
    if ((strequipmentNoFromDB.length>0) || (strequipmentNameFromDB.length>0)) {
        
        NSDictionary *dictDataEquipFromDB=[[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"equipmentCode"]],@"itemcode",[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"equipmentName"]],@"itemName",[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"serialNumber"]],@"serialNumber",[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"modelNumber"]],@"modelNumber",[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"manufacturer"]],@"manufacturer",[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"equipmentNo"]],@"itemNo",[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"equipmentCustomName"]],@"ItemCustomName", nil];
        
        objSelectedEquipment=(NSManagedObject*)dictDataEquipFromDB;
        
        [_btn_AddEquipments setTitle:[dictDataEquipFromDB valueForKey:@"itemName"] forState:UIControlStateNormal];
        
    } else {
        
        [_btn_AddEquipments setTitle:@"----Select Equipment----" forState:UIControlStateNormal];
        
    }
    
    _txtViewIssuesDesc.text=strServiceIssuesFromDB;
    
    NSString *strServiceIssuesProrityFromDB=[NSString stringWithFormat:@"%@",[objIssuesToEditEquipment valueForKey:@"priority"]];
    
    NSArray *arrOfStatus=[dictDetailsFortblView valueForKey:@"PriorityMasters"];
    
    for (int k=0; k<arrOfStatus.count; k++) {
        
        NSDictionary *dictDataa=arrOfStatus[k];
        NSString *strServiceIssuesProrityFromMaster=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"PriorityId"]];
        if ([strServiceIssuesProrityFromDB isEqualToString:strServiceIssuesProrityFromMaster]) {
            
            [_btnPrioityAddServiceIssues setTitle:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Name"]] forState:UIControlStateNormal];
            strPriorityIdGlobal=[NSString stringWithFormat:@"%@",strServiceIssuesProrityFromMaster];
            
        }
    }
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, 105, [UIScreen mainScreen].bounds.size.width,_viewAddServiceIssues.frame.size.height);
    [_viewAddServiceIssues setFrame:frameFor_view_ServiceIssues];
    [viewBackGround addSubview:_viewAddServiceIssues];
    
    [self setDeafultPriorityHigh];
    
}

-(void)methodAddReasonStopJobView{
    
    _txtViewReason.text=@"";
    [_btnSelectReason setTitle:@"----Select Reason----" forState:UIControlStateNormal];
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width,_viewStopJobReason.frame.size.height);
    [_viewStopJobReason setFrame:frameFor_view_ServiceIssues];
    [viewBackGround addSubview:_viewStopJobReason];
    
}

-(void)methodAddRepairView{
    
    strRepairIdGlobal = @"";
    _txtTotalRepairCostNonStandard.text=@"";
    _const_NonStandardRepairAmtView_H.constant=0;
    [_txtRepairAmt setEnabled:NO];
    _txtRepairAmt.text=@"";
    _txtRepairAmt.placeholder=@"";
    _txtRepairQty.text=@"1";
    _txtRepairCostAdjustment.text=@"";
    strPartCostAdjustmentGlobal=@"";
    _txtViewRepairDescriptions.text=@"";
    isStandardRepair=YES;
    [_btnSelectRepair setTitle:@"----Select Repair----" forState:UIControlStateNormal];
    [_btnSelectOption setTitle:@"----Select Option----" forState:UIControlStateNormal];
    [_btnSelectOption setHidden:NO];
    [_lblSelectOption setHidden:NO];
    _lblRepairStdorNonStd.text=@"Repair";
    [_txtRepairName setHidden:YES];
    [_btnSelectRepair setHidden:NO];
    [_btnDropDownRepair setHidden:NO];
    
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width,_view_AddRepair.frame.size.height);
    [_view_AddRepair setFrame:frameFor_view_ServiceIssues];
    [viewBackGround addSubview:_view_AddRepair];
    
}


-(void)methodAddNotesView{
    
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, 105, [UIScreen mainScreen].bounds.size.width,_viewAddNotes.frame.size.height);
    [_viewAddNotes setFrame:frameFor_view_ServiceIssues];
    [viewBackGround addSubview:_viewAddNotes];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // [tblData removeFromSuperview];
    // [viewBackGround removeFromSuperview];
    
}

-(void)clickAddRepair:(UIButton*)sender
{
    
    if (isJobStarted) {
        
        NSLog(@"%ld",(long)sender.tag);
        UIButton *button=(UIButton *) sender;
        arrOfSelectedRepairOption=[[NSMutableArray alloc] init];
        intGlobalSection=(int)button.tag;
        indexToShow=intGlobalSection;
        isCollapseAllViews=NO;
        NSDictionary *dictDatTemp=arrOfSubWorkServiceIssues[button.tag];
        strSubWorkOrderIssueIdGlobal=[NSString stringWithFormat:@"%@",[dictDatTemp valueForKey:@"subWorkOrderIssueId"]];
        [self methodAddRepairView];
        
    } else {
        
        [global AlertMethod:Alert :alertStartJob];
        
    }
    
}
-(void)clickEditIssues:(UIButton*)sender
{
    
    if (isJobStarted) {
        
        isEditIssues=YES;
        UIButton *button=(UIButton *) sender;
        NSDictionary *dictDatTemp=arrOfSubWorkServiceIssues[button.tag];
        NSString *strId=[NSString stringWithFormat:@"%@",[dictDatTemp valueForKey:@"subWorkOrderIssueId"]];
        [self fetchSubWorkOrderIssuesFromDataBaseForMechanicalToEditIssuesAndEquipment :strId];
        
    } else {
        
        [global AlertMethod:Alert :alertStartJob];
        
    }
    
}

-(void)clickMinusSection:(UIButton*)sender
{
    
    NSLog(@"%ld",(long)sender.tag);
    //UIButton *button=(UIButton *) sender;
    indexToShow=-1;
    intGlobalSection=-1;
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    [defss setInteger:indexToShow forKey:@"sectionToOpen"];
    [defss synchronize];
    
    [_tblViewServiceIssues reloadData];
    isCollapseAllViews=YES;
    [self adjustViewHeightsonCollapsingSections];
    
}

-(void)clickPlusSection:(UIButton*)sender
{
    
    if (isCompletedStatusMechanical) {
        
        NSLog(@"%ld",(long)sender.tag);
        UIButton *button=(UIButton *) sender;
        indexToShow=button.tag;
        intGlobalSection=indexToShow;
        NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
        [defss setInteger:indexToShow forKey:@"sectionToOpen"];
        [defss synchronize];
        
        NSDictionary *dictDatTemp=arrOfSubWorkServiceIssues[button.tag];
        strSubWorkOrderIssueIdGlobal=[NSString stringWithFormat:@"%@",[dictDatTemp valueForKey:@"subWorkOrderIssueId"]];
        
        [_tblViewServiceIssues reloadData];
        isCollapseAllViews=NO;
        [self adjustViewHeightsonCollapsingSections];
        
    }else{
        
        if (isJobStarted) {
            
            NSLog(@"%ld",(long)sender.tag);
            UIButton *button=(UIButton *) sender;
            indexToShow=button.tag;
            intGlobalSection=indexToShow;
            NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
            [defss setInteger:indexToShow forKey:@"sectionToOpen"];
            [defss synchronize];
            
            NSDictionary *dictDatTemp=arrOfSubWorkServiceIssues[button.tag];
            strSubWorkOrderIssueIdGlobal=[NSString stringWithFormat:@"%@",[dictDatTemp valueForKey:@"subWorkOrderIssueId"]];
            
            [_tblViewServiceIssues reloadData];
            isCollapseAllViews=NO;
            [self adjustViewHeightsonCollapsingSections];
            
        } else {
            
            [global AlertMethod:Alert :alertStartJob];
            
        }
        
    }
    
}


-(void)methodToOpenSection :(int)intSection{
    
    indexToShow=intSection;
    intGlobalSection=indexToShow;
    NSDictionary *dictDatTemp=arrOfSubWorkServiceIssues[intSection];
    strSubWorkOrderIssueIdGlobal=[NSString stringWithFormat:@"%@",[dictDatTemp valueForKey:@"subWorkOrderIssueId"]];
    
    isCollapseAllViews=NO;
    
    [self adjustViewHeightsonCollapsingSections];
    
}

-(void)getPricLookupFromMaster :(NSString*)strCategorySysName{
    
    arrOfPriceLookup=nil;
    arrOfPriceLookup=[[NSMutableArray alloc]init];
    arrOfPriceLookup=[global getPricLookupFromMaster:strCompanyKey :strDepartMentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType :strCategorySysName];
    
}

-(void)getPartsMaster{
    
    arrOfPartsGlobalMasters=nil;
    arrOfPartsGlobalMasters=[[NSMutableArray alloc]init];
    arrOfPartsGlobalMasters=[global getPartsMaster:strDepartMentSysName :@""];
    
}


-(void)getHoursConfiFromMasters{
    
    arrOfHoursConfig=nil;
    arrOfHoursConfig=[[NSMutableArray alloc]init];
    arrOfHoursConfig=[global getHoursConfiFromMaster:strCompanyKey :strDepartMentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType];
    
}

-(void)methodCalculationRepairAmt :(int)indexRepair{
    
    NSDictionary *dictDataAfterHourRateToBeUsed=[[NSDictionary alloc]init];
    
    indexRepairOptionGlobal=indexRepair;
    
    //    if (isStandardSubWorkOrder) {
    
    NSDictionary *dictDataRepairMaster=arrOfSelectedRepairOption[indexRepair];
    
    //Labor Price Logic
    
    NSDictionary *dictDataRepairMasterOld=arrDataTblView[indexRepairGlobal];
    
    _txtRepairCostAdjustment.text=[NSString stringWithFormat:@"%@",[dictDataRepairMasterOld valueForKey:@"CostAdjustment"]];
    
    strPartCostAdjustmentGlobal=[NSString stringWithFormat:@"%@",[dictDataRepairMasterOld valueForKey:@"PartCostAdjustment"]];
    
    //_txtRepairCostAdjustment.text=@"0.00";
    
    float hrsLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"LaborHours"]]];
    
    hrsLR=hrsLR/3600;
    
    float strStdPrice=0.0;
    
    if (!(arrOfHoursConfig.count==0)) {
        
        NSDictionary *dictDataHours=arrOfHoursConfig[0];
        
        strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
        
        if (!isStandardSubWorkOrder) {
            
            // Change for holiday and After hours change
            
            if (isHoliday) {
                
                strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                
            } else {
                
                // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                
                if (strAfterHrsDuration.length==0) {
                    
                    strStdPrice=0.0;
                    
                } else {
                    
                    NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                    
                    NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                    NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                    
                    strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                    strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                    
                    
                    NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                    
                    for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                        
                        NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                        
                        NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                        NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                        
                        if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                            
                            dictDataAfterHourRateToBeUsed=dictDataAHC;
                            break;
                            
                        }
                        
                    }
                    
                    
                    strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborPrice"] floatValue];
                    
                }
            }
            
            
        }
        
    }
    
    
    
    float totalPriceLabour=hrsLR*strStdPrice;
    
    //Helper Price Logic
    
    float hrsHLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"HelperHours"]]];
    
    hrsHLR=hrsHLR/3600;
    
    float strStdPriceHLR=0.0;
    
    if (!(arrOfHoursConfig.count==0)) {
        
        NSDictionary *dictDataHours=arrOfHoursConfig[0];
        
        strStdPriceHLR=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
        
        if (!isStandardSubWorkOrder) {
            
            if (isHoliday) {
                
                strStdPriceHLR=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];
                
            } else {
                
                if ((dictDataAfterHourRateToBeUsed==nil) || ([dictDataAfterHourRateToBeUsed isKindOfClass:[NSNull class]])) {
                    
                    
                    
                } else {
                    
                    strStdPriceHLR=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                    
                }
                
            }
            
        }
        
    }
    
    float totalPriceHelper=hrsHLR*strStdPriceHLR;
    
    //Adding Cost Adjustment To Price Repair
    
    float costAdjustMentToAdd=[_txtRepairCostAdjustment.text floatValue];
    
    //Adding Part Cost Adjustment To Price Repair
    
    float partCostAdjustMentToAdd=[[NSString stringWithFormat:@"%@",[dictDataRepairMasterOld valueForKey:@"PartCostAdjustment"]] floatValue];
    
    
    //Final Price Logic
    
    float finalPriceLRHLR=totalPriceLabour+totalPriceHelper+costAdjustMentToAdd+partCostAdjustMentToAdd;
    
    //Parts Logic From Master
    
    float totalLookUpPrice=0;
    
    NSArray *ArrOfPartsMaster=[dictDataRepairMaster valueForKey:@"RepairPartMasterExtSerDc"];
    
    for (int k=0; k<ArrOfPartsMaster.count; k++) {
        
        NSDictionary *dictData=ArrOfPartsMaster[k];
        
        NSString *strPartCode=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PartCode"]];
        
        float valueLookUpPrice=[self fetchPartDetailViaPartCode:strPartCode];
        
        float QTY=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Qty"]] floatValue];
        
        NSString *strCategorySysNameLocal=[self fetchPartCategorySysNameDetailViaPartCode:strPartCode];
        
        NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",valueLookUpPrice] :strCategorySysNameLocal :@"Standard"];
        
        float multiplier=[strMultiplier floatValue];
        
        multiplierGlobal=multiplier;
        
        valueLookUpPrice=valueLookUpPrice*QTY*multiplier;
        
        totalLookUpPrice=totalLookUpPrice+valueLookUpPrice;
        
    }
    
    //Final Price Logic after part and repair adding
    
    float finalOverAllPrice=totalLookUpPrice+finalPriceLRHLR;
    
    _txtRepairAmt.text=[NSString stringWithFormat:@"%.02f",finalOverAllPrice];
    
    dictLaborInfoToSave = @{@"LaborHours":[NSString stringWithFormat:@"%@",[dictDataRepairMaster             valueForKey:@"LaborHours"]],
                            @"HelperHours":[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"HelperHours"]],
                            @"totalPriceLabour":[NSString stringWithFormat:@"%f",totalPriceLabour],
                            @"totalPriceHelper": [NSString stringWithFormat:@"%f",totalPriceHelper],
                            @"DescRepair": [NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"RepairDesc"]],
                            };
    
    
    //    } else {
    //
    //
    //
    //    }
    
    strGlobalAmtRepair=_txtRepairAmt.text;
    
    [self calculateTotalRepairAmount:_txtRepairQty.text :_txtRepairAmt.text];
    
}

-(NSString*)logicForFetchingMultiplier :(NSString*)strPriceToLookUp :(NSString*)strCategorySysName :(NSString*)strType{
    
    NSString *strMultiplier;
    
    [self getPricLookupFromMaster:strCategorySysName];

    strMultiplier=[global logicForFetchingMultiplier:strPriceToLookUp :strCategorySysName :strType :arrOfPriceLookup];
    
    return strMultiplier;
}

-(void)methodStatus{
    
    if (isCompletedStatusMechanical) {
        
        [_btnAddNotes setHidden:YES];
        [_btnAddHelper setHidden:YES];
        [_btnAddServiceIssue setHidden:YES];
        [_btnStartnStopJOB setEnabled:NO];
        [_btnSelectHelper setEnabled:NO];
        [_btnClientApproval setEnabled:NO];
        [_btnTimeClock setHidden:YES];
        [_btnStartnStopJOB setHidden:YES];
        [_btnAdditionalInfo setHidden:YES];
        [_btnPO setEnabled:NO];
        [_btnCreateQuote setEnabled:NO];
        [_txtViewTechComment setEditable:NO];
        [_btnClientApproval setHidden:YES];
        [_btnMechanic setEnabled:NO];
        [_btnHelper setEnabled:NO];
        
    }else{
        
        [_txtViewTechComment setEditable:YES];
        [_btnAddNotes setHidden:NO];
        [_btnAddHelper setHidden:NO];
        [_btnAddServiceIssue setHidden:NO];
        [_btnStartnStopJOB setEnabled:YES];
        [_btnSelectHelper setEnabled:YES];
        [_btnClientApproval setEnabled:YES];
        [_btnTimeClock setHidden:NO];
        [_btnStartnStopJOB setHidden:NO];
        [_btnAdditionalInfo setHidden:NO];
        [_btnPO setEnabled:YES];
        [_btnCreateQuote setEnabled:YES];
        [_btnClientApproval setHidden:NO];
        [_btnMechanic setEnabled:YES];
        [_btnHelper setEnabled:YES];
        
        //isClientApproved
        NSString *strIsClientApprovalRequired=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isClientApprovalReq"]];
        
        if ([strIsClientApprovalRequired isEqualToString:@"true"] || [strIsClientApprovalRequired isEqualToString:@"1"] || [strIsClientApprovalRequired isEqualToString:@"True"]) {
            
            [_btnClientApproval setHidden:NO];
            
            
        }else{
            
            [_btnClientApproval setHidden:YES];
            
        }
        
    }
    //[_btnClientApproval setHidden:NO];
}

-(void)goToSendMail
{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    SendMailMechanicaliPadViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailMechanicaliPadViewController"];
    objSendMail.strWorkOrderId=strWorkOrderId;
    objSendMail.strFromWhere=@"S";
    objSendMail.strFromWhere=@"S";
    [self.navigationController pushViewController:objSendMail animated:NO];
    
}

//============================================================================
//============================================================================
#pragma mark- ----------------Core Data Fetch Methods----------------
//============================================================================
//============================================================================

-(void)fetchSubWorkOrderIssuesFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssues = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssues setEntity:entitySubWorkOrderIssues];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    //predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssues setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssues = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssues = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssues];
    
    [requestSubWorkOrderIssues setSortDescriptors:sortDescriptorsSubWorkOrderIssues];
    
    self.fetchedResultsControllerSubWorkOrderIssues = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssues managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssues setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssues performFetch:&error];
    arrAllObjSubWorkOrderIssues = [self.fetchedResultsControllerSubWorkOrderIssues fetchedObjects];
    if ([arrAllObjSubWorkOrderIssues count] == 0)
    {
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        [_tblViewServiceIssues reloadData];
        [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssues.count; k++) {
            
            matchesSubWorkOrderIssues=arrAllObjSubWorkOrderIssues[k];
            
            [arrOfSubWorkServiceIssues addObject:matchesSubWorkOrderIssues];
            
        }
        [_tblViewServiceIssues reloadData];
        [self adjustViewHeights];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    indexToShow=intGlobalSection;
    
    [self adjustViewHeightsonCollapsingSections];
    
}

-(void)fetchSubWorkOrderIssuesFromDataBaseForMechanicalToEditIssuesAndEquipment :(NSString*)strId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssues = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssues setEntity:entitySubWorkOrderIssues];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strId];
    
    [requestSubWorkOrderIssues setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssues = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssues = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssues];
    
    [requestSubWorkOrderIssues setSortDescriptors:sortDescriptorsSubWorkOrderIssues];
    
    self.fetchedResultsControllerSubWorkOrderIssues = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssues managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssues setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssues performFetch:&error];
    NSArray *arrOfIssuesFetched = [self.fetchedResultsControllerSubWorkOrderIssues fetchedObjects];
    if ([arrOfIssuesFetched count] == 0)
    {
        
        
        
    }
    else
    {
        objIssuesToEditEquipment=arrOfIssuesFetched[0];
        [self methodAddServiceIssuesViewOnEdit];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderHelperFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    arrAllObjSubWorkOrderHelper = [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    if ([arrAllObjSubWorkOrderHelper count] == 0)
    {
        arrOfSubWorkServiceHelper=nil;
        arrOfSubWorkServiceHelper=[[NSMutableArray alloc]init];
        [_tblviewHelper reloadData];
        
        [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceHelper=nil;
        arrOfSubWorkServiceHelper=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderHelper.count; k++) {
            
            matchesSubWorkOrderHelper=arrAllObjSubWorkOrderHelper[k];
            
            NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderHelper valueForKey:@"isActive"]];
            
            if ([strIsActive caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsActive isEqualToString:@"1"]) {
                
                [arrOfSubWorkServiceHelper addObject:matchesSubWorkOrderHelper];
                
            }
            
        }
        [_tblviewHelper reloadData];
        
        [self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderHelperToDeleteFromDataBaseForMechanical :(NSString *)strEmployeeNoToDelete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && employeeNo = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strEmployeeNoToDelete];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    arrAllObjSubWorkOrderHelper = [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    if ([arrAllObjSubWorkOrderHelper count] == 0)
    {
        [_tblviewHelper reloadData];
        
        [self adjustViewHeights];
        
    }
    else
    {
        
        matchesSubWorkOrderHelper=arrAllObjSubWorkOrderHelper[0];
        
        [matchesSubWorkOrderHelper setValue:@"false" forKey:@"isActive"];
        
        //Final Save
        NSError *error;
        [context save:&error];
        
        [self fetchSubWorkOrderHelperFromDataBaseForMechanical];
        
        [_tblviewHelper reloadData];
        
        [self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderNotesFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
    requestSubWorkOrderNotes = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderNotes setEntity:entityMechanicalSubWorkOrderNotes];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderNotes setPredicate:predicate];
    
    sortDescriptorSubWorkOrderNotes = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderNotes = [NSArray arrayWithObject:sortDescriptorSubWorkOrderNotes];
    
    [requestSubWorkOrderNotes setSortDescriptors:sortDescriptorsSubWorkOrderNotes];
    
    self.fetchedResultsControllerSubWorkOrderNotes = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderNotes managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderNotes setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderNotes performFetch:&error];
    arrAllObjSubWorkOrderNotes = [self.fetchedResultsControllerSubWorkOrderNotes fetchedObjects];
    if ([arrAllObjSubWorkOrderNotes count] == 0)
    {
        arrOfSubWorkServiceNotes=nil;
        arrOfSubWorkServiceNotes=[[NSMutableArray alloc]init];
        [_tblViewNotes reloadData];
        
        [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceNotes=nil;
        arrOfSubWorkServiceNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderNotes.count; k++) {
            
            matchesSubWorkOrderNotes=arrAllObjSubWorkOrderNotes[k];
            
            [arrOfSubWorkServiceNotes addObject:matchesSubWorkOrderNotes];
            
        }
        [_tblViewNotes reloadData];
        
        [self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"" forKey:@"subWOActualHourId"];
        [defs synchronize];
        
        [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
        arrOfSubWorkServiceActualHrs=nil;
        arrOfSubWorkServiceActualHrs=[[NSMutableArray alloc]init];
        
        isJobStarted=NO;
        if (![strWoStatus isEqualToString:@"OnRoute"]) {
            isJobStarted=YES;
        }
        
    }
    else
    {
        arrOfSubWorkServiceActualHrs=nil;
        arrOfSubWorkServiceActualHrs=[[NSMutableArray alloc]init];
        
        arrOfHrsTotal=nil;
        arrOfHrsTotal=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strTimeOutJob=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
            
            NSString *strSubWorkOrderStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"status"]];
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:@"" forKey:@"subWOActualHourId"];
            [defs synchronize];
            
            [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
            
            isJobStarted=NO;
            
            if (![strWoStatus isEqualToString:@"OnRoute"]) {
                isJobStarted=YES;
            }
            
            if ([strSubWorkOrderStatus isEqualToString:@"OnRoute"]) {
                
                
                
            } else {
                
                if (strTimeOutJob.length==0){
                    
                    isJobStarted=YES;
                    
                    NSString *strSubWOActualHourId=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWOActualHourId"]];
                    
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setValue:strSubWOActualHourId forKey:@"subWOActualHourId"];
                    [defs synchronize];
                    
                    [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
                    
                    break;
                    
                }
                
            }
            
        }
        
        
        //Calculate Time For Job Duration
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strSubWorkOrderStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"status"]];
            
            if ([strSubWorkOrderStatus isEqualToString:@"OnRoute"]) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                
                BOOL isIncludeTravelTime=[defs boolForKey:@"IncludeTravelTime"];
                
                if (isIncludeTravelTime) {
                    
                    NSDate *timeInDate=[global ChangeDateToLocalDateMechanicalUTC:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]]];
                    
                    NSString *strTimeOUT=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
                    
                    if (strTimeOUT.length==0) {
                        
                        strTimeOUT=[global strCurrentDateFormattedForMechanical];
                        
                    }
                    
                    NSDate *timeOutDate=[global ChangeDateToLocalDateMechanicalTimeOut:[NSString stringWithFormat:@"%@",strTimeOUT]];
                    
                    NSTimeInterval secondsBetween = [timeOutDate timeIntervalSinceDate:timeInDate];
                    
                    int hrs = secondsBetween;
                    
                    [arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",hrs]];
                    
                    [arrOfSubWorkServiceActualHrs addObject:matchesSubWorkOrderActualHrs];
                    
                }
                
            } else {
                
                NSDate *timeInDate=[global ChangeDateToLocalDateMechanicalUTC:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]]];
                
                NSString *strTimeOUT=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
                
                if (strTimeOUT.length==0) {
                    
                    strTimeOUT=[global strCurrentDateFormattedForMechanical];
                    
                }
                
                NSDate *timeOutDate=[global ChangeDateToLocalDateMechanicalTimeOut:[NSString stringWithFormat:@"%@",strTimeOUT]];
                
                NSTimeInterval secondsBetween = [timeOutDate timeIntervalSinceDate:timeInDate];
                
                int hrs = secondsBetween;
                
                [arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",hrs]];
                
                [arrOfSubWorkServiceActualHrs addObject:matchesSubWorkOrderActualHrs];
                
            }
            
        }
        
        NSInteger sum = 0;
        for (NSString *num in arrOfHrsTotal)
        {
            sum += [num intValue];
        }
        
        totalsecond=(int)sum;
        
        NSDictionary *dictTimeClock=[self createTimemapForSeconds:(int)sum];
        
        NSString *strTimeFormat=[NSString stringWithFormat:@"%02d:%02d:%02d",[[dictTimeClock valueForKey:@"h"] intValue],[[dictTimeClock valueForKey:@"m"] intValue],[[dictTimeClock valueForKey:@"s"] intValue]];
        
        [_btnTimeClock setTitle:strTimeFormat forState:UIControlStateNormal];
        
        
        if ([_btnStartnStopJOB.titleLabel.text isEqualToString:@"Pause Job"] || [_btnStartnStopJOB.currentTitle isEqualToString:@"Pause Job"]) {
            
            [self timerStarthere];
            
        } else {
            
            
            
        }
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}

-(NSDictionary*)createTimemapForSeconds:(int)seconds{
    int hours = floor(seconds /  (60 * 60) );
    
    float minute_divisor = seconds % (60 * 60);
    int minutes = floor(minute_divisor / 60);
    
    float seconds_divisor = seconds % 60;
    seconds = ceil(seconds_divisor);
    
    NSDictionary * timeMap = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:hours], [NSNumber numberWithInt:minutes], [NSNumber numberWithInt:seconds], nil] forKeys:[NSArray arrayWithObjects:@"h", @"m", @"s", nil]];
    
    return timeMap;
}

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOut{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strID=[defs valueForKey:@"subWOActualHourId"];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strID];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
    }
    else
    {
        
        matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[0];
        
        [matchesSubWorkOrderActualHrs setValue:[global strCurrentDateFormattedForMechanical] forKey:@"timeOut"];
        [matchesSubWorkOrderActualHrs setValue:[global strCurrentDateFormattedForMechanical] forKey:@"mobileTimeOut"];
        
        //_btnSelectReason.titleLabel.text
        [matchesSubWorkOrderActualHrs setValue:_btnSelectReason.titleLabel.text forKey:@"reason"];
        [matchesSubWorkOrderActualHrs setValue:_txtViewReason.text forKey:@"actHrsDescription"];
        
        // [matchesSubWorkOrderActualHrs setValue:@"Inspection" forKey:@"status"];
        
        BOOL isNetReachable=[global isNetReachable];
        
        if (isNetReachable) {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
            
            strNotificationTypeName=@"MechanicalActualHrsSyncInspectionFR1";
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:strNotificationTypeName object:nil];
            
            SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
            
            [objSync syncMechanicalActualHours:strWorkOrderId :strSubWorkOrderIdGlobal :strID :strNotificationTypeName];
            
        }
        
        NSError *error2;
        [context save:&error2];
        
        [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"" forKey:@"subWOActualHourId"];
        [defs synchronize];
        
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    NSArray *arrtemp = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrtemp count] == 0)
    {
        
    }
    else
    {
        
        for (int k=0; k<arrtemp.count; k++) {
            
            NSManagedObject *objTemp=arrtemp[k];
            
            NSString *strStatus=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"status"]];
            
            if ([strStatus isEqualToString:@"OnRoute"]) {
                
                strWoStatus=@"Inspection";
                
                [objTemp setValue:[global strCurrentDateFormattedForMechanical] forKey:@"timeOut"];
                // [objTemp setValue:@"Inspection" forKey:@"status"];
                
                NSString *strTimeOut = [global strCurrentDateFormattedForMechanical];
                
                [objTemp setValue:strTimeOut forKey:@"mobileTimeOut"];
                
                [self metodUpdateSubWorkOrderStatus:strTimeOut];
                
                [objTemp setValue:ReasonOnRoute forKey:@"reason"];
                
                [_objSubWorkOrderdetails setValue:@"Inspection" forKey:@"subWOStatus"];
                [_objSubWorkOrderdetails setValue:@"Start" forKey:@"clockStatus"];
                
                _lblStatus.text=[NSString stringWithFormat:@"Status: %@",[_objSubWorkOrderdetails valueForKey:@"subWOStatus"]];
                
                if ([_lblStatus.text isEqualToString:@"Status: Inspection"]) {
                    
                    _lblStatus.text=[NSString stringWithFormat:@"Status: %@",@"In Progress"];
                    
                }
                
                NSError *error2;
                [context save:&error2];
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:@"Inspection" forKey:@"WoStatus"];
                isJobStarted=YES;
                [defs synchronize];
                
                break;
                
            }
            
        }
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}

-(void)metodUpdateSubWorkOrderStatus :(NSString*)strTimeOut{
    
    isJobStarted=YES;
    strWoStatus=@"Inspection";
    NSString *strStatusToSend;
    
    strStatusToSend=@"OnRoute";
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"]];
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&subWorkOrderId=%@&userName=%@&status=%@&%@%@",strServiceUrlMainServiceAutomation,UrlMechanicalSubWorkOrderStatusUpdateInspection,strCompanyKey,strSubWorkOrderIdGlobal,strUserName,strStatusToSend,UrlMechanicalSubWorkOrderTimeOut,strTimeOut];
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    strUrl = [NSString stringWithFormat:@"%@%@%@&subWorkOrderId=%@&userName=%@&status=%@&%@%@&Latitude=%@&Longitude=%@",strServiceUrlMainServiceAutomation,UrlMechanicalSubWorkOrderStatusUpdateInspection,strCompanyKey,strSubWorkOrderIdGlobal,strUserName,strStatusToSend,UrlMechanicalSubWorkOrderTimeOut,strTimeOut,latitude,longitude];
    
    NSLog(@"Mechanical Work Order Status Update URl-----%@",strUrl);
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Status..."];
    
    //============================================================================
    //============================================================================
    
    NSString *strType;
    
    strType=@"MechanicalSubWorkOrderStatusDispatch";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [DejalBezelActivityView removeView];
                 
                 if (success)
                 {
                     
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
                 
                 [self addStartActualHoursFromMobileToDB];
                 
             });
         }];
    });
    //============================================================================
    //============================================================================
    
}


-(void)fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        [_tblViewServiceIssues reloadData];
        
        [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepair.count; k++) {
            
            matchesSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[k];
            
            [arrOfSubWorkServiceIssuesRepair addObject:matchesSubWorkOrderIssuesRepair];
            
        }
        [_tblViewServiceIssues reloadData];
        
        [self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    //issueRepairPartId
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueRepairIdTofetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
            [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :(NSString*)strIssueRepairIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueRepairIdToFetch];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairLabour = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairLabour count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairLabour.count; k++) {
            
            matchesSubWorkOrderIssuesRepairLabour=arrAllObjSubWorkOrderIssuesRepairLabour[k];
            
            [arrOfSubWorkServiceIssuesRepairLabour addObject:matchesSubWorkOrderIssuesRepairLabour];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

//============================================================================
//============================================================================
#pragma mark- ----------------Button Action Methods----------------
//============================================================================
//============================================================================

- (IBAction)action_SaveAdditionalInfo:(id)sender {
    
    if (_txtViewAdditionalInfo.text.length==0) {
        
        [global AlertMethod:Alert :@"Please enter Additional info to add"];
        
    } else {
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateAdditionalInfo:strWorkOrderId :_strSubWorkOrderId :_txtViewAdditionalInfo.text];
        
        [_txtViewAdditionalInfo resignFirstResponder];
    }
    
}


- (IBAction)action_DashBoardView:(id)sender {
    
    //    UIAlertController *alert= [UIAlertController
    //                               alertControllerWithTitle:@"Alert!"
    //                               message:@"Are you sure to move to DashBoard"
    //                               preferredStyle:UIAlertControllerStyleAlert];
    //
    //    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
    //                                                handler:^(UIAlertAction * action)
    //                          {
    //
    //                          }];
    //    [alert addAction:yes];
    //    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
    //                                               handler:^(UIAlertAction * action)
    //                         {
    //
    //                             [jobMinTimer invalidate];
    //                             jobMinTimer=nil;
    //                             [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
    //
    //                             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
    //                                                                                      bundle: nil];
    //                             DashBoardViewiPad
    //                             *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewiPad"];
    //                             [self.navigationController pushViewController:objByProductVC animated:NO];
    //
    //                         }];
    //    [alert addAction:no];
    //    [self presentViewController:alert animated:YES completion:nil];
    
    [self goToEquipMentHistory];
    
}


- (IBAction)action_ServiceHistory:(id)sender {
    
    [self saveImageToCoreData];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isFromOtherPresentedView"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ServiceHistoryMechanical
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistoryMechanical"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
- (IBAction)action_Documents:(id)sender {
    
    [self saveImageToCoreData];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isFromOtherPresentedView"];
    [defs synchronize];
    
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServiceiPad"
    //                                                             bundle: nil];
    //    ServiceDocumentsViewControlleriPad
    //    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDocumentsViewControlleriPad"];
    //    objByProductVC.strAccountNo=[_objWorkOrderdetails valueForKey:@"accountNo"];
    //    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    MechanicalDocumentsViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalDocumentsViewController"];
    objByProductVC.strAccountNo=[_objWorkOrderdetails valueForKey:@"accountNo"];
    objByProductVC.strWorkOrderId = strWorkOrderId;
    objByProductVC.strWorkOrderNo = [NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"workOrderNo"]];
    objByProductVC.strLeadNo = strWorkOrderId;
    
    if (isCompletedStatusMechanical) {
        
        objByProductVC.strWorkStatus=@"Complete";
        
    } else {
        
        objByProductVC.strWorkStatus=@"InComplete";
        
    }
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    
}
- (IBAction)action_KnowledgeBase:(id)sender {
    
    [self saveImageToCoreData];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isFromOtherPresentedView"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    MechanicalDynamicViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalDynamicViewController"];
    // objByProductVC.matchesWorkOrderZSync=matchesWorkOrder;
    objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    //[self.navigationController pushViewController:objByProductVC animated:NO];
    
}

- (IBAction)action_Back:(id)sender {
    
    [self saveTechComments];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    [defsBack setBool:YES forKey:@"isFromBackServiceDynamci"];
    [defsBack synchronize];
    
    [jobMinTimer invalidate];
    jobMinTimer=nil;
    [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
    
    //[self.navigationController popViewControllerAnimated:YES];
    
    [self goToSubWorkOrderDetailView];
    
    //[self methodBack];
    
}

-(void)goToSubWorkOrderDetailView{
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    [defss setInteger:-1 forKey:@"sectionToOpen"];
    [defss synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    MechanicalGeneralInfoViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalGeneralInfoViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

- (IBAction)action_PrimaryEmail:(id)sender{
    
    NSString *strMessage=_btnEmail.titleLabel.text;
    
    if (strMessage.length==0) {
        
    } else {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@""
                                   message:strMessage
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

- (IBAction)action_AddServiceIssues:(id)sender {
    
    if (isJobStarted) {
        
        [_btnPrioityAddServiceIssues setTitle:@"----Select Priority----" forState:UIControlStateNormal];
        [_btn_AddEquipments setTitle:@"----Select Equipment----" forState:UIControlStateNormal];
        objSelectedEquipment=nil;
        strPriorityIdGlobal=@"";
        _txtViewIssuesDesc.text=@"";
        [_txtViewIssuesDesc becomeFirstResponder];
        isEditIssues=NO;
        [self methodAddServiceIssuesView];
        
    } else {
        
        [global AlertMethod:Alert :alertStartJob];
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                k1++;
                
            }
            
        }
        
        return k1;
        
        
    }else if (tableView.tag==2){
        
        return arrOfSubWorkServiceHelper.count;
        
    }else if (tableView.tag==3){
        
        return arrOfSubWorkServiceNotes.count;
        
    }else if (tableView.tag==105){
        
        return arrOfSelectedRepairOption.count;
        
    }
    else if (tableView.tag==106){
        
        return arrOfMechanicalWoEquipment.count;
        
    }else {
        
        return arrDataTblView.count;
        
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==1) {
        
        if (indexPath.section==indexToShow) {
            
            return tableView.rowHeight;
            
        } else {
            
            return 0;
            
        }
        
    }else if (tableView.tag==2){
        
        return 80;
        
    }else if (tableView.tag==3){
        
        return tableView.rowHeight;
        
    }
    else {
        
        return 80;
        
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==1) {
        
        MechanicalServiceIssueTableViewCell *cell = (MechanicalServiceIssueTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MechanicalServiceIssueTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
        
    }else if (tableView.tag==2){
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        }
        
        NSManagedObject *dictDataHelper=arrOfSubWorkServiceHelper[indexPath.row];
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[global getEmployeeNameViaId:[NSString stringWithFormat:@"%@",[dictDataHelper valueForKey:@"employeeNo"]]]];
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        NSString *strLaborType = [NSString stringWithFormat:@"%@",[dictDataHelper valueForKey:@"laborType"]];
        
        if ([strLaborType isEqualToString:@"L"]) {
            
            cell.detailTextLabel.text=[NSString stringWithFormat:@"Labor Type : %@",@"Mechanic"];
            
        } else {
            
            cell.detailTextLabel.text=[NSString stringWithFormat:@"Labor Type : %@",@"Helper"];
            
        }
        
        cell.detailTextLabel.font=[UIFont systemFontOfSize:18];
        cell.detailTextLabel.numberOfLines=1;
        
        return cell;
        
        
    }else if (tableView.tag==3){
        
        MechanicalNotesTableViewCell *cell = (MechanicalNotesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MechanicalNotesTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        // Configure Table View Cell
        [self configureCellNotes:cell atIndexPath:indexPath];
        
        return cell;
        
    }else if (tableView.tag==105){
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrOfSelectedRepairOption.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 105:
                {
                    //Select option
                    NSDictionary *dictData=[arrOfSelectedRepairOption objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"RepairLaborTitle"];
                    break;
                }
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
        
        
    }else if (tableView.tag==106){
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSManagedObject *dictData=[arrOfMechanicalWoEquipment objectAtIndex:indexPath.row];
        cell.textLabel.text=[dictData valueForKey:@"itemName"];
        
        NSString *strItemName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"itemName"]];
        NSString *strItemCode=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"itemcode"]];
        NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"itemCustomName"]];
        
        if ([strItemCustomName isEqualToString:@"(null)"]) {
            
            strItemCustomName=@"";
            
        }
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        if (strItemName.length>0) {
            
            [arrTemp addObject:strItemName];
            
        }
        
        if (strItemCode.length>0) {
            
            [arrTemp addObject:strItemCode];
            
        }
        
        if (strItemCustomName.length>0) {
            
            [arrTemp addObject:strItemCustomName];
            
        }
        
        if (arrTemp.count==1) {
            
            cell.textLabel.text=[NSString stringWithFormat:@"Equip.Name(#):%@",[dictData valueForKey:@"itemName"]];
            
        } else if (arrTemp.count==2){
            
            cell.textLabel.text=[NSString stringWithFormat:@"Equip.Name(#):%@(%@)",[dictData valueForKey:@"itemName"],[dictData valueForKey:@"itemcode"]];
            
        } else if (arrTemp.count==3){
            
            cell.textLabel.text=[NSString stringWithFormat:@"Equip.Name(#):%@(%@)-%@",[dictData valueForKey:@"itemName"],[dictData valueForKey:@"itemcode"],[dictData valueForKey:@"itemCustomName"]];
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
    }
    else{
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrDataTblView.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 101:
                {
                    //_Btn_Priority
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"Name"];
                    break;
                }
                case 102:
                {
                    //Select Repair
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"RepairName"];
                    
                    //  NSString *strSTringUrl=[@"http://thrs.stagingsoftware.com//Documents//EmployeeSignatures/2702545479_20170511055323099_download (1).jpg" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    //   [cell.imageView setImageWithURL:[NSURL URLWithString:strSTringUrl] placeholderImage:[UIImage imageNamed:@"NoImage.jpg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    
                    break;
                }
                case 103:
                {
                    //Select Helper
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"FullName"];
                    break;
                }
                case 104:
                {
                    //Select Reason
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"ResetReason"];
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    break;
                }
                    
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
    }
}

- (void)configureCell:(MechanicalServiceIssueTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSString *strnonStdLaborAmt,*strnonStdPartAmt,*strnonStdRepairAmt,*strRepairQty;
    
    BOOL isNonStandard;
    
    isNonStandard=NO;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }
        
    }
    
    NSManagedObject *objTemp=arrTemp[indexPath.row];
    
    strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
    
    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanicalAndCalculateValuesAndUpdateDB:strIssueRepairIdToCheck];
    
    float totalCostAdjustment=0;
    
    NSString *strcostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"costAdjustment"]];
    
    totalCostAdjustment=[strcostAdjustment floatValue];
    
    NSString *strPartCostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"partCostAdjustment"]];
    
    totalCostAdjustment=totalCostAdjustment+[strPartCostAdjustment floatValue];
    
    cell.lblRepairName.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairName"]];
    cell.lblRepairDesc.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
    
    if (cell.lblRepairDesc.text.length==0) {
        
        cell.lblRepairDesc.text=@"N/A";
        
    }
    
    strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
    
    if ([NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairMasterId"]].length==0) {
        
        isNonStandard=YES;
        
    } else {
        
        isNonStandard=NO;
        
    }
    
    strnonStdLaborAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdLaborAmt"]];
    strnonStdPartAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdPartAmt"]];
    strnonStdRepairAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdRepairAmt"]];
    
    strRepairQty =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
    cell.txtFldQty.delegate=self;
    cell.txtFldQty.accessibilityHint=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
    //cell.txtFldQty.leftView.hidden=YES;
    cell.txtFldQty.tag=0;
    cell.txtFldQty.text=strRepairQty;
    
    if ((strRepairQty.length==0) || [strRepairQty isEqualToString:@""] || [strRepairQty isEqualToString:@"(null)"] || [strRepairQty isEqualToString:@"0"]) {
        
        strRepairQty = @"1";
        cell.txtFldQty.text=@"";
        
    }
    
    //Fetch Parts
    
    [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical:strIssueIdToCheck :strIssueRepairIdToCheck];
    
    NSMutableArray *arrTempParts=[[NSMutableArray alloc]init];
    
    float totalCostPartss=0;
    
    for (int j=0; j<arrOfSubWorkServiceIssuesRepairParts.count; j++) {
        
        NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairParts[j];
        
        NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[objTemp valueForKey:@"partName"],[objTemp valueForKey:@"qty"]];
        //multiplier   qty  unitPrice
        
        NSString *strMulti=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"multiplier"]];
        float multip=[strMulti floatValue];
        
        NSString *strqtyy=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
        float qtyy=[strqtyy floatValue];
        
        NSString *strunitPricee=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"unitPrice"]];
        float unitPricee=[strunitPricee floatValue];
        
        float finalPriceParts=unitPricee*qtyy*multip;
        
        
        NSString *strIsAddedAfterApproval=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isAddedAfterApproval"]];
        
        if ([strIsAddedAfterApproval isEqualToString:@"true"] || [strIsAddedAfterApproval isEqualToString:@"1"]) {
            
        }else{
            
            totalCostPartss=totalCostPartss+finalPriceParts;
            
            [arrTempParts addObject:strPartsnQty];
            
        }
        
    }
    
    cell.lblPart.text=[arrTempParts componentsJoinedByString:@", "];
    
    if (cell.lblPart.text.length==0) {
        
        cell.lblPart.text=@"N/A";
        
    }
    //Fetch LAbor
    
    [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :strIssueRepairIdToCheck];
    
    float totalCostLaborss=0;
    
    NSString *strLaborHrs,*strHelperHrs;
    for (int j=0; j<arrOfSubWorkServiceIssuesRepairLabour.count; j++) {
        
        NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairLabour[j];
        
        NSString *strLaborCostt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborCost"]];
        
        float laborCostt=[strLaborCostt floatValue];
        
        NSString *strIsWarrantyHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isWarranty"]];
        
        if ([strIsWarrantyHrs isEqualToString:@"true"] || [strIsWarrantyHrs isEqualToString:@"1"]) {
            
        }else{
            
            totalCostLaborss=totalCostLaborss+laborCostt;
            
        }
        
        NSString *strLaborType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborType"]];
        
        if ([strLaborType isEqualToString:@"H"]) {
            
            strHelperHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
            
        } else {
            
            strLaborHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
            
        }
        
    }
    
    float overAllAmt=totalCostPartss+totalCostLaborss+totalCostAdjustment;
    
    // Condition change for if laborcost is 0 then no cost adjustment is to be added
    
    if (totalCostLaborss<=0) {
        
        overAllAmt = overAllAmt-[strcostAdjustment floatValue];
        
    }
    
    // For Non standard Amount calculation
    
    if (isNonStandard) {
        
        if ((totalCostPartss==0) && (totalCostLaborss==0)) {
            
            if ([strnonStdRepairAmt isEqualToString:@"0"]) {
                
                strnonStdRepairAmt=@"";
                
            }
            
            if (strnonStdRepairAmt.length==0) {
                
                overAllAmt=overAllAmt+[strnonStdLaborAmt floatValue]+[strnonStdPartAmt floatValue];
                
            } else {
                
                overAllAmt=overAllAmt+[strnonStdRepairAmt floatValue];
                
            }
            
        }
        
    }
    
    
    //Change for saving Overall Amount All Time in DB
    
    NSString *strOverAllAmt=[NSString stringWithFormat:@"%f",overAllAmt];
    
    NSString *strWorkOrderIDd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"workorderId"]];
    
    NSString *strSubWorkOrderIdd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderId"]];
    
    SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
    
    [objSync fetchRepairToUpdateRepairAmt:strWorkOrderIDd :strSubWorkOrderIdd :strIssueIdToCheck :strIssueRepairIdToCheck :strOverAllAmt];
    
    cell.lblAmt.text=[NSString stringWithFormat:@"%.02f",overAllAmt];
    
    int repairQty = [strRepairQty intValue];
    
    float amountAfterAddingQty = [strOverAllAmt floatValue]*repairQty;
    
    cell.lblTotalAmount.text=[NSString stringWithFormat:@"%.02f",amountAfterAddingQty];
    
    if (strLaborHrs.length==0) {
        strLaborHrs=@"00:00";
    }
    if (strHelperHrs.length==0) {
        strHelperHrs=@"00:00";
    }
    
    cell.lblLR.text=[global ChangeTimeToHrnMin:strLaborHrs];
    cell.lblHLR.text=[global ChangeTimeToHrnMin:strHelperHrs];
    
    
    cell.btnAddParts.tag=indexPath.row*1000+indexPath.section;
    cell.btnAddLabour.tag=indexPath.row*1000+indexPath.section;
    cell.btnDeleteRepair.tag=indexPath.row*1000+indexPath.section;
    
    [cell.btnAddParts addTarget:self action:@selector(action_btnAddParts:) forControlEvents:UIControlEventTouchDown];
    [cell.btnAddLabour addTarget:self action:@selector(action_btnAddLabour:) forControlEvents:UIControlEventTouchDown];
    [cell.btnDeleteRepair addTarget:self action:@selector(action_btnDeleteRepair:) forControlEvents:UIControlEventTouchDown];
    
    if (isCompletedStatusMechanical) {
        
        [cell.btnDeleteRepair setEnabled:NO];
        
    }else{
        
        [cell.btnDeleteRepair setEnabled:YES];
        
    }
    
    
    cell.btnViewMore.tag=indexPath.row*1000+indexPath.section;;
    [cell.btnViewMore addTarget:self action:@selector(action_ViewMoreJobDescription:) forControlEvents:UIControlEventTouchDown];
    
    if ([NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]].length>43) {
        
        cell.btnViewMore.hidden=NO;
        
    } else {
        
        cell.btnViewMore.hidden=YES;
        
    }
    
}


-(void)action_ViewMoreJobDescription:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }
        
    }
    
    NSManagedObject *objTemp=arrTemp[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
    [global AlertMethod:@"Repair Description" :strJobDesc];
    
}

-(void)action_btnAddParts:(id)sender
{
    [jobMinTimer invalidate];
    [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
    
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp1=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp1 addObject:dictIssuesRepairData];
            
        }
        
    }
    
    NSManagedObject *objTemp1=arrTemp1[row];
    
    strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"issueRepairId"]];
    //subWorkOrderIssueId
    NSString *strSubWorkOrderIssuesIdToSend=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"subWorkOrderIssueId"]];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    AddPartsiPadViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"AddPartsiPadViewController"];
    objSignViewController.strWorkOrderId=strWorkOrderId;
    objSignViewController.strSubWorkOrderId=strSubWorkOrderIdGlobal;
    objSignViewController.strIssuePartsIdToFetch=strIssueRepairIdToCheck;
    objSignViewController.objSubWorkOrderdetails=_objSubWorkOrderdetails;
    objSignViewController.strSubWorkOrderIssuesIdToFetch=strSubWorkOrderIssuesIdToSend;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

-(void)action_btnAddLabour:(id)sender
{
    
    [jobMinTimer invalidate];
    [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
    
    UIButton *btn = (UIButton *)sender;
    
    int row= btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }
        
    }
    
    NSManagedObject *objTemp=arrTemp[row];
    
    strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
    
    NSString *strSubWorkOrderIssuesIdToSend=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    AddLaboriPadViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"AddLaboriPadViewController"];
    objSignViewController.strWorkOrderId=strWorkOrderId;
    objSignViewController.strSubWorkOrderId=strSubWorkOrderIdGlobal;
    objSignViewController.strIssueRepairIdToFetch=strIssueRepairIdToCheck;
    objSignViewController.objSubWorkOrderdetails=_objSubWorkOrderdetails;
    objSignViewController.strSubWorkOrderIssuesIdToFetch=strSubWorkOrderIssuesIdToSend;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

-(void)action_btnDeleteRepair:(id)sender
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert!"
                                  message:@"Are you sure you want to delete"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             UIButton *btn = (UIButton *)sender;
                             int row= btn.tag/1000;
                             
                             int section =btn.tag%1000;
                             
                             NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
                             
                             NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
                             
                             NSString *strIssueRepairIdToCheck;
                             
                             NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                             
                             for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
                                 
                                 NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
                                 
                                 NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
                                 
                                 if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                                     
                                     [arrTemp addObject:dictIssuesRepairData];
                                     
                                 }
                                 
                             }
                             
                             NSManagedObject *objTemp=arrTemp[row];
                             
                             strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
                             
                             [self deleteRepairFromDB:strWorkOrderId :strSubWorkOrderIdGlobal :strIssueIdToCheck :strIssueRepairIdToCheck :section :row];
                             
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                             [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


- (void)configureCellNotes:(MechanicalNotesTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *dictDataNotes=arrOfSubWorkServiceNotes[indexPath.row];
    
    cell.lblAddedBy.text=[NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"createdBy"]];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    NSString *strEmpNameLocal = [NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    NSString *strLoggedInUserNameLocal = [NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"LoggedInUserName"]];
    
    if ([cell.lblAddedBy.text isEqualToString:strLoggedInUserNameLocal]) {
        
        cell.lblAddedBy.text=[NSString stringWithFormat:@"%@",strEmpNameLocal];
        
    }
    
    cell.lblAddedOn.text=[global ChangeDateToLocalDateMechanicalParts:[NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"createdDate"]]];
    cell.lblNoteDesc.text=[NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"note"]];
    
    [cell.btnViewMoreNoteDesc addTarget:self action:@selector(action_ViewMoreNotesDescription:) forControlEvents:UIControlEventTouchDown];
    
    if ([NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"note"]].length>56) {
        
        cell.btnViewMoreNoteDesc.hidden=NO;
        
    } else {
        
        cell.btnViewMoreNoteDesc.hidden=YES;
        
    }
    
}
-(void)action_ViewMoreNotesDescription:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    NSManagedObject *dictDataNotes=arrOfSubWorkServiceNotes[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"note"]];
    [global AlertMethod:@"Note Description" :strJobDesc];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==105) {
        
        if (!(arrOfSelectedRepairOption.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 105:
                {
                    NSDictionary *dictData=[arrOfSelectedRepairOption objectAtIndex:indexPath.row];
                    [_btnSelectOption setTitle:[dictData valueForKey:@"RepairLaborTitle"] forState:UIControlStateNormal];
                    strRepairOptionIdGlobal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"RepairLaborMasterId"]];
                    
                    // Calculation for Repair Price
                    [self methodCalculationRepairAmt:(int)indexPath.row];
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }
        
    }else if(tableView.tag==106){
        
        NSManagedObject *dictData=[arrOfMechanicalWoEquipment objectAtIndex:indexPath.row];
        [_btn_AddEquipments setTitle:[dictData valueForKey:@"itemName"] forState:UIControlStateNormal];
        objSelectedEquipment=dictData;
        
    }
    else {
        
        if (!(arrDataTblView.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 101:
                {
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnPrioityAddServiceIssues setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                    strPriorityIdGlobal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PriorityId"]];
                    break;
                }
                case 102:
                {
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectRepair setTitle:[dictData valueForKey:@"RepairName"] forState:UIControlStateNormal];
                    strRepairIdGlobal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"RepairMasterId"]];
                    
                    indexRepairGlobal=(int)indexPath.row;
                    
                    //Change For Good Better Best
                    
                    arrOfSelectedRepairOption=[[NSMutableArray alloc] init];
                    
                    NSArray *arrOfOptions=[dictData valueForKey:@"RepairLaborMasterDcs"];
                    
                    [arrOfSelectedRepairOption addObjectsFromArray:arrOfOptions];
                    
                    int k1=0;
                    
                    for (int k=0; k<arrOfSelectedRepairOption.count; k++) {
                        
                        NSDictionary *dictDataOptions=arrOfSelectedRepairOption[k];
                        
                        NSString *strIsDefault=[NSString stringWithFormat:@"%@",[dictDataOptions valueForKey:@"IsDefault"]];
                        
                        if ([strIsDefault isEqualToString:@"true"] || [strIsDefault isEqualToString:@"True"] || [strIsDefault isEqualToString:@"1"]) {
                            
                            [_btnSelectOption setTitle:[dictDataOptions valueForKey:@"RepairLaborTitle"] forState:UIControlStateNormal];
                            strRepairOptionIdGlobal=[NSString stringWithFormat:@"%@",[dictDataOptions valueForKey:@"RepairLaborMasterId"]];
                            
                            k1=k;
                            
                        }
                        
                    }
                    
                    // Calculation for Repair Price
                    [self methodCalculationRepairAmt:k1];
                    
                    break;
                }
                case 103:
                {
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    // [_btnSelectHelper setTitle:[dictData valueForKey:@"FullName"] forState:UIControlStateNormal];
                    strHelperIdGlobal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EmployeeNo"]];
                    
                    if ([strEmployeeNo isEqualToString:strHelperIdGlobal]) {
                        
                        strHelperIdGlobal=@"";
                        [global AlertMethod:Alert :@"Main technician can't be selected as helper"];
                        
                    } else {
                        
                        [_btnSelectHelper setTitle:[dictData valueForKey:@"FullName"] forState:UIControlStateNormal];
                        
                    }
                    
                    break;
                }
                case 104:
                {
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectReason setTitle:[dictData valueForKey:@"ResetReason"] forState:UIControlStateNormal];
                    //                strResertId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ResetId"]];
                    break;
                }
                    
                default:
                    break;
            }
            
        }
        
    }
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isCompletedStatusMechanical) {
        
        return false;
        
    }else{
        
        if (tableView.tag==2) {
            
            return true;
            
        }else
            
            return false;
        
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==2) {
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Are you sure you want to delete"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     NSManagedObject *dictDataHelper=arrOfSubWorkServiceHelper[indexPath.row];
                                     
                                     [self deleteHelperFromDB:[NSString stringWithFormat:@"%@",[dictDataHelper valueForKey:@"employeeNo"]]];
                                     
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1) {
        return arrOfSubWorkServiceIssues.count;
    }else{
        return 1;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (tableView.tag==1) {
        return 50;
    }else{
        return 0;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (tableView.tag==1) {
        return 5;
    }else{
        return 0;
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = tableView.frame;
    
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-140, 0, 130, 50)];
    [addButton setTitle:@"Add Repair" forState:UIControlStateNormal];
    addButton.backgroundColor = [UIColor themeColorBlack];
    addButton.titleLabel.font=[UIFont systemFontOfSize:22];
    
    if (isCompletedStatusMechanical) {
        
        [addButton setHidden:YES];
        
    }
    
    addButton.tag=section;
    [addButton addTarget:self action:@selector(clickAddRepair:) forControlEvents:UIControlEventTouchDown];
    
    
    
    UIButton *addButtonEditIssues = [[UIButton alloc] initWithFrame:CGRectMake(addButton.frame.origin.x-55, 0, 50, 50)];
    [addButtonEditIssues setTitle:@"" forState:UIControlStateNormal];
    [addButtonEditIssues setImage:[UIImage imageNamed:@"ic_edit.png"] forState:UIControlStateNormal];
    addButtonEditIssues.backgroundColor = [UIColor clearColor];
    //addButtonEditIssues.titleLabel.font=[UIFont systemFontOfSize:22];
    
    if (isCompletedStatusMechanical) {
        
        [addButtonEditIssues setHidden:YES];
        
    }
    
    addButtonEditIssues.tag=section;
    [addButtonEditIssues addTarget:self action:@selector(clickEditIssues:) forControlEvents:UIControlEventTouchDown];
    
    UIButton *minusButton;
    
    if (section==indexToShow) {
        
        minusButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 0, 50, 50)];
        [minusButton setTitle:@"-" forState:UIControlStateNormal];
        minusButton.backgroundColor = [UIColor themeColorBlack];
        minusButton.titleLabel.font=[UIFont systemFontOfSize:22];
        
        minusButton.tag=section;
        [minusButton addTarget:self action:@selector(clickMinusSection:) forControlEvents:UIControlEventTouchDown];
        
        //[self methodToOpenSection:(int)section];
        
    } else {
        
        minusButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 0, 50, 50)];
        [minusButton setTitle:@"+" forState:UIControlStateNormal];
        minusButton.backgroundColor = [UIColor themeColorBlack];
        minusButton.titleLabel.font=[UIFont systemFontOfSize:22];
        
        minusButton.tag=section;
        [minusButton addTarget:self action:@selector(clickPlusSection:) forControlEvents:UIControlEventTouchDown];
        
    }
    
    NSManagedObject *dictData=arrOfSubWorkServiceIssues[section];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, frame.size.width-250, 50)];
    title.text = [NSString stringWithFormat:@"%@ :%@",[dictData valueForKey:@"serviceIssue"],[dictData valueForKey:@"equipmentName"]];
    title.backgroundColor=[UIColor clearColor];
    title.font=[UIFont systemFontOfSize:22];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [headerView addSubview:title];
    [headerView addSubview:addButton];
    [headerView addSubview:minusButton];
    [headerView addSubview:addButtonEditIssues];
    
    return headerView;
}

//============================================================================
//============================================================================
#pragma mark - --------------------Service Issues METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_PriorityAddServiceIssues:(id)sender {
    
    [_btnPrioityAddServiceIssues setTitle:@"----Select Priority----" forState:UIControlStateNormal];
    [self hideAllTxtViewsnFields];
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfStatus=[dictDetailsFortblView valueForKey:@"PriorityMasters"];
    
    for (int k=0; k<arrOfStatus.count; k++) {
        
        NSDictionary *dictDataa=arrOfStatus[k];
        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
        if (isActive) {
            
            [arrDataTblView addObject:dictDataa];
            
        } else {
            
            //            if (YesUpdateInactiveCheck) {
            //
            //                long long  longEmployeeId=[[dictDataa valueForKey:@"PriorityId"] longLongValue];
            //                long long  longEmployeeIdToCheck=[strPriorityId longLongValue];
            //                if (longEmployeeId==longEmployeeIdToCheck) {
            //
            //                    [arrDataTblView addObject:dictDataa];
            //
            //                }
            //            }
        }
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=101;
        [self tableLoad:tblData.tag];
    }
    
}

- (IBAction)action_SaveServiceIssues:(id)sender {
    
    if (_txtViewIssuesDesc.text.length==0) {
        
        [global AlertMethod:Alert :@"Please enter issue description"];
        
    } else if(strPriorityIdGlobal.length==0){
        
        [global AlertMethod:Alert :@"Please select priority"];
        
    } else{
        
        [self hideAllTxtViewsnFields];
        
        if (isEditIssues) {
            
            [objIssuesToEditEquipment setValue:_txtViewIssuesDesc.text forKey:@"serviceIssue"];
            [objIssuesToEditEquipment setValue:[global strCurrentDate] forKey:@"modifiedDate"];
            
            if (![_btn_AddEquipments.titleLabel.text isEqualToString:@"----Select Equipment----"]) {
                
                [objIssuesToEditEquipment setValue:[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"itemcode"]] forKey:@"equipmentCode"];
                [objIssuesToEditEquipment setValue:[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"itemName"]] forKey:@"equipmentName"];
                [objIssuesToEditEquipment setValue:[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"serialNumber"]] forKey:@"serialNumber"];
                [objIssuesToEditEquipment setValue:[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"modelNumber"]] forKey:@"modelNumber"];
                [objIssuesToEditEquipment setValue:[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"manufacturer"]] forKey:@"manufacturer"];
                [objIssuesToEditEquipment setValue:[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"itemNo"]] forKey:@"equipmentNo"];
                
                NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"itemCustomName"]];
                
                if ([strItemCustomName isEqualToString:@"(null)"]) {
                    
                    strItemCustomName=@"";
                    
                }
                [objIssuesToEditEquipment setValue:[NSString stringWithFormat:@"%@",strItemCustomName] forKey:@"equipmentCustomName"];
                
            }else{
                
                [objIssuesToEditEquipment setValue:@"" forKey:@"equipmentCode"];
                [objIssuesToEditEquipment setValue:@"" forKey:@"equipmentName"];
                [objIssuesToEditEquipment setValue:@"" forKey:@"serialNumber"];
                [objIssuesToEditEquipment setValue:@"" forKey:@"modelNumber"];
                [objIssuesToEditEquipment setValue:@"" forKey:@"manufacturer"];
                [objIssuesToEditEquipment setValue:@"" forKey:@"equipmentNo"];
                
                NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"itemCustomName"]];
                
                if ([strItemCustomName isEqualToString:@"(null)"]) {
                    
                    strItemCustomName=@"";
                    
                }
                [objIssuesToEditEquipment setValue:@"" forKey:@"equipmentCustomName"];
                
            }
            NSError *error2;
            [context save:&error2];
            
            [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
            
        } else {
            
            [self addServiceIssuesFromMobileToDB];
            
        }
        
        [viewBackGround removeFromSuperview];
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
    }
    
}

- (IBAction)action_CancelServiceIssuesView:(id)sender {
    
    [self hideAllTxtViewsnFields];
    
    [viewBackGround removeFromSuperview];
    
}

//============================================================================
//============================================================================
#pragma mark - --------------------Dynamic Tableview METHODS-----------------
//============================================================================
//============================================================================


-(void)setTableFrame
{
    viewBackGroundOnView.tag=202;
    viewBackGroundOnView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundOnView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGroundOnView];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 500);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGroundOnView];
    [viewBackGroundOnView addSubview:tblData];
    
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 60)];
    
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont systemFontOfSize:25];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(methodDonePriorityTableview) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGroundOnView addSubview:btnDone];
    
    
    UIButton *btnAddNew=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.size.width/2+2+tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 60)];
    
    [btnAddNew setTitle:@"Cancel" forState:UIControlStateNormal];
    btnAddNew.titleLabel.font=[UIFont systemFontOfSize:25];
    [btnAddNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnAddNew.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [btnAddNew addTarget:self action:@selector(methodCancelPriorityTableview) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGroundOnView addSubview:btnAddNew];
    
}

-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
        case 103:
        {
            [self setTableFrame];
            break;
        }
        case 104:
        {
            [self setTableFrame];
            break;
        }
        case 105:
        {
            [self setTableFrame];
            break;
        }
        case 106:
        {
            [self setTableFrame];
            break;
        }
            
        default:
            break;
    }
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}

//============================================================================
//============================================================================
#pragma mark- --------------------Repair METHODS-----------------
//============================================================================
//============================================================================


- (IBAction)action_SelectOption:(id)sender {
    
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    if (arrOfSelectedRepairOption.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=105;
        [self tableLoad:tblData.tag];
    }
    
}


- (IBAction)action_RadioStandard:(id)sender {
    _const_NonStandardRepairAmtView_H.constant=0;
    [_txtRepairAmt setEnabled:NO];
    isStandardRepair=YES;
    _lblRepairStdorNonStd.text=@"Repair";
    //_txtRepairAmt.text=@"";
    //_txtRepairQty.text=@"1";
    _txtRepairAmt.placeholder = @"";
    _txtRepairCostAdjustment.text=@"";
    strPartCostAdjustmentGlobal=@"";
    [_txtRepairName setHidden:YES];
    [_btnSelectRepair setHidden:NO];
    //[_btnSelectOption setTitle:@"----Select Option----" forState:UIControlStateNormal];
    [_btnSelectOption setHidden:NO];
    [_lblSelectOption setHidden:NO];
    [_btnDropDownRepair setHidden:NO];
    
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_txtRepairAmt setHidden:NO];
    
}
- (IBAction)action_RadioNonStandard:(id)sender {
    _const_NonStandardRepairAmtView_H.constant=110;
    [_txtRepairAmt setEnabled:YES];
    isStandardRepair=NO;
    _lblRepairStdorNonStd.text=@"Repair Name";
    //_lblRepairStdorNonStd.text=@"Repair";
    _txtRepairCostAdjustment.text=@"";
    strPartCostAdjustmentGlobal=@"";
    _txtRepairAmt.text=@"";
    _txtRepairQty.text=@"1";
    _lblTotalAmount.text=@"0.00";
    _txtRepairAmt.placeholder = @"Enter Amount";
    [_txtRepairName setHidden:NO];
    [_btnSelectRepair setHidden:YES];
    //[_btnSelectOption setTitle:@"----Select Option----" forState:UIControlStateNormal];
    [_btnSelectOption setHidden:YES];
    [_lblSelectOption setHidden:YES];
    [_btnDropDownRepair setHidden:YES];
    
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    //Condition For Total Repair cost
    
    isTotalRepairCost=YES;
    
    [_btnRadioTotalRepairCost setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioSpecifyRepairPartnLaborCost setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [_txtTotalRepairCostNonStandard setHidden:NO];
    [_txtRepairLaborCostNonStandard setHidden:YES];
    [_txtRepairPartCostNonStandard setHidden:YES];
    [_txtRepairAmt setHidden:YES];
    
}
- (IBAction)action_SelectRepair:(id)sender {
    
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    //@"MasterAllMechanical"
    NSDictionary *dictMaster=[defs valueForKey:@"MasterAllMechanical"];
    NSArray *arrOfStatus=[dictMaster valueForKey:@"RepairMasterExtSerNewDc"];
    
    for (int k=0; k<arrOfStatus.count; k++) {
        
        NSDictionary *dictDataa=arrOfStatus[k];
        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
        if (isActive) {
            
            if ([[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]] isEqualToString:strDepartMentSysName]) {
                
                [arrDataTblView addObject:dictDataa];
                
            }
            
        } else {
            
            //            if (YesUpdateInactiveCheck) {
            //
            //                long long  longEmployeeId=[[dictDataa valueForKey:@"PriorityId"] longLongValue];
            //                long long  longEmployeeIdToCheck=[strPriorityId longLongValue];
            //                if (longEmployeeId==longEmployeeIdToCheck) {
            //
            //                    [arrDataTblView addObject:dictDataa];
            //
            //                }
            //            }
        }
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=102;
        [self tableLoad:tblData.tag];
    }
    
}
- (IBAction)action_SaveRepair:(id)sender {
    
    if (isStandardRepair) {
        // Is Standard repair
        
        NSString *strQty = [NSString stringWithFormat:@"%@",_txtRepairQty.text];
        
        int repairQty = [strQty intValue];
        
        if (strRepairIdGlobal.length==0) {
            
            [global AlertMethod:Alert :@"Please select repair"];
            
        }else if (repairQty<1) {
            
            [global AlertMethod:Alert :@"Please enter repair quantity greater then 0"];
            
        } else {
            
            [self addServiceIssuesRepairFromMobileToDB];
            
            [_btnSelectRepair setTitle:@"----Select Repair----" forState:UIControlStateNormal];
            [_btnSelectOption setTitle:@"----Select Option----" forState:UIControlStateNormal];
            
            strRepairIdGlobal=@"";
            
            [viewBackGround removeFromSuperview];
            [_view_AddRepair removeFromSuperview];
            
            [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        }
        
    } else {
        
        NSString *strQty = [NSString stringWithFormat:@"%@",_txtRepairQty.text];
        
        int repairQty = [strQty intValue];
        
        if (_txtRepairName.text.length==0) {
            
            [global AlertMethod:Alert :@"Please enter repair name"];
            
        }else if (repairQty<1) {
            
            [global AlertMethod:Alert :@"Please enter repair quantity greater then 0"];
            
        } else {
            
            [self addServiceIssuesRepairNonStandardFromMobileToDB];
            
            _txtRepairName.text=@"";
            _txtRepairName.placeholder=@"Enter Repair Name";
            
            [viewBackGround removeFromSuperview];
            [_view_AddRepair removeFromSuperview];
            
            [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        }
        
    }
    
}

- (IBAction)action_CancelRepair:(id)sender {
    
    strRepairIdGlobal=@"";
    [viewBackGround removeFromSuperview];
    [_view_AddRepair removeFromSuperview];
    
}

- (IBAction)action_AddRepair:(id)sender {
    
    _txtRepairAmt.text=@"";
    _txtRepairAmt.placeholder = @"";
    
    [self methodAddRepairView];
    
}

//============================================================================
//============================================================================
#pragma mark- --------------------Helper METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_Selecthelper:(id)sender {
    
    if (isJobStarted) {
        
        [self hideAllTxtViewsnFields];
        
        [self methodTableViewAllocation];
        
        [viewBackGroundOnView removeFromSuperview];
        [tblData removeFromSuperview];
        
        arrDataTblView=[[NSMutableArray alloc]init];
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
        
        arrDataTblView=[[NSMutableArray alloc]init];
        NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
        
        for (int k=0; k<arrOfEmployee.count; k++) {
            
            NSDictionary *dictDataa=arrOfEmployee[k];
            BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
            if (isActive) {
                
                [arrDataTblView addObject:dictDataa];
                
            } else {
                
                //            if (YesUpdateInactiveCheck) {
                //
                //                long long  longEmployeeId=[[dictDataa valueForKey:@"EmployeeId"] longLongValue];
                //                long long  longEmployeeIdToCheck=[strSalesPersonId longLongValue];
                //                if (longEmployeeId==longEmployeeIdToCheck) {
                //
                //                    [arrDataTblView addObject:dictDataa];
                //
                //                }
                //            }
            }
        }
        
        if (arrDataTblView.count==0) {
            [global AlertMethod:Info :NoDataAvailable];
        }else{
            tblData.tag=103;
            [self tableLoad:tblData.tag];
        }
        
    } else {
        
        [global AlertMethod:Alert :alertStartJob];
        
    }
    
}

- (IBAction)action_AddHelper:(id)sender {
    
    if (isJobStarted) {
        
        if (strHelperIdGlobal.length==0) {
            
            [global AlertMethod:Alert :@"Please select helper to add"];
            
        } else {
            
            BOOL isAlreadyThere;
            
            isAlreadyThere=NO;
            
            for (int k=0; k<arrOfSubWorkServiceHelper.count; k++) {
                
                NSManagedObject *objTemp=arrOfSubWorkServiceHelper[k];
                
                NSString *strHelperId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                
                if ([strHelperIdGlobal isEqualToString:strHelperId]) {
                    
                    isAlreadyThere=YES;
                    break;
                    
                }
                
            }
            
            if (isAlreadyThere) {
                
                [global AlertMethod:Alert :@"Helper already added. Please select another helper to add"];
                
            } else {
                
                [_btnSelectHelper setTitle:@"----Select Helper----" forState:UIControlStateNormal];
                
                [self addServiceHelperFromMobileToDB];
                
                strHelperIdGlobal=@"";
                
                isMechanic=NO;
                [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                
                [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
            }
            
        }
        
    } else {
        
        [global AlertMethod:Alert :alertStartJob];
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- --------------------NOTES METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_AddNotes:(id)sender {
    
    if (isJobStarted) {
        
        _txtViewAddNotesDesc.text=@"";
        [_txtViewAddNotesDesc becomeFirstResponder];
        
        [self methodAddNotesView];
        
    } else {
        
        [global AlertMethod:Alert :alertStartJob];
        
    }
    
}
- (IBAction)action_SaveNotes:(id)sender {
    
    if (_txtViewAddNotesDesc.text.length==0) {
        
        [global AlertMethod:Alert :@"Please enter Notes description"];
        
    } else{
        
        [self hideAllTxtViewsnFields];
        
        [self addServiceNotesFromMobileToDB];
        
        [viewBackGround removeFromSuperview];
        [_viewAddNotes removeFromSuperview];
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
    }
    
}

- (IBAction)action_CancelNotes:(id)sender {
    
    [self hideAllTxtViewsnFields];
    [viewBackGround removeFromSuperview];
    [_viewAddNotes removeFromSuperview];
    
}

//============================================================================
//============================================================================
#pragma mark- --------------------Client Approval METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_StartJob:(id)sender {
    
    if ([_btnStartnStopJOB.titleLabel.text isEqualToString:@"Start Job"] || [_btnStartnStopJOB.currentTitle isEqualToString:@"Start Job"]) {
        
        isJobStarted=YES;
        
        if ([strWoStatus isEqualToString:@"OnRoute"]) {
            
            [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute];
            
        }else{
            
            [self addStartActualHoursFromMobileToDB];
            
        }
        
        [self fetchSubWorkOrderActualHrsFromDataBaseForMechanical];
        
        [self timerStarthere];
        
        [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
        
        [_objSubWorkOrderdetails setValue:@"Start" forKey:@"clockStatus"];
        
        NSError *error2;
        [context save:&error2];
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        
    } else {
        
        [self methodAddReasonStopJobView];
        
        // Create dynamic view for employee time sheet slot wise.....
        arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
        arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
        
        arrOfGlobalDynamicEmpSheetFinal=[global createEmpSheetDataInBackground:strWorkOrderId :strSubWorkOrderIdGlobal :arrOfHoursConfig :strEmpID :strEmployeeNoLoggedIn :strEmpName :@"Front" :@""];
        
        arrOfHeaderTitleForSlots=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
        
        [global convertIntoJson:dictDetailsFortblView :arrOfGlobalDynamicEmpSheetFinal :@"array" :@"arrOfGlobalDynamicEmpSheetFinal on stop job"];
        
        [self createDynamicEmpSheet:arrOfGlobalDynamicEmpSheetFinal :arrOfHeaderTitleForSlots];
        
    }
    
}

-(void)callAfterDelay{
    
    [self createDynamicEmpSheet:arrOfGlobalDynamicEmpSheetFinal :arrOfHeaderTitleForSlots];
    
}

//---------------------PTBN--------------
//---------------------
// Same method on all four view so if change in one copy in all other view also
//---------------------

-(void)createDynamicEmpSheet :(NSArray*)arrOfEmpSheet :(NSArray*)arrOfHeaderTitle{
    
    for(UIView *view in scrollViewEmpTimeSheet.subviews)
    {
        [view removeFromSuperview];
    }
    
    arrOfHeaderTitle=arrOfHeaderTitleForSlots;
    //    arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
    //    [arrOfGlobalDynamicEmpSheetFinal addObjectsFromArray:arrOfEmpSheet];
    
    CGFloat scrollViewHeight=0.0;
    CGFloat scrollViewWidth=0.0;
    
    scrollViewHeight=arrOfGlobalDynamicEmpSheetFinal.count*60+60;
    scrollViewWidth=arrOfHeaderTitle.count*100;
    
    //scrollViewEmpTimeSheet=[[UIScrollView alloc]initWithFrame:CGRectMake(scrollViewEmpTimeSheet.frame.origin.x, scrollViewEmpTimeSheet.frame.origin.y, scrollViewWidth, scrollViewHeight)];
    
    UIView *viewEmpSheetOnScroll=[[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollViewEmpTimeSheet.frame.size.width, scrollViewEmpTimeSheet.frame.size.height)];
    
    CGFloat xAxisHeaderTitle=10;
    
    for (int i=0; i<=arrOfHeaderTitleForSlots.count; i++) {
        
        UILabel *lblTitleHeader=[[UILabel alloc]init];
        lblTitleHeader.backgroundColor=[UIColor lightTextColor];
        lblTitleHeader.layer.borderWidth = 1.5f;
        lblTitleHeader.layer.cornerRadius = 0.0f;
        lblTitleHeader.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        if (i==0) {
            
            lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, 0, 200, 50);
            xAxisHeaderTitle=xAxisHeaderTitle+200;
            
            
        }else{
            
            lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, 0, 100, 50);
            xAxisHeaderTitle=xAxisHeaderTitle+100;
            
            
        }
        //lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, yAxisMain+60, 100, 50);
        lblTitleHeader.font=[UIFont systemFontOfSize:22];
        lblTitleHeader.backgroundColor=[UIColor lightTextColorTimeSheet];
        if (i==0) {
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@""];
            
        } else if(i==1){
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Regular"];
            
        }else if(i==arrOfHeaderTitleForSlots.count){
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Holiday"];
            
        }else {
            
            //lblTitleHeader.text=[NSString stringWithFormat:@"%@",arrOfHeaderTitle[i]];
            lblTitleHeader.text=[global slotHeaderTitleFromTimeSlot:arrOfHoursConfig :[NSString stringWithFormat:@"%@",arrOfHeaderTitle[i-1]]];
            
        }
        
        lblTitleHeader.textAlignment=NSTextAlignmentCenter;
        lblTitleHeader.numberOfLines=700;
        [lblTitleHeader setAdjustsFontSizeToFitWidth:YES];
        
        [viewEmpSheetOnScroll addSubview:lblTitleHeader];
        
    }
    
    CGFloat yAxis=50;
    
    for (int k=0; k<arrOfGlobalDynamicEmpSheetFinal.count; k++) {
        
        CGFloat xAxis=210;
        
        NSDictionary *dictDataEmpSheet=arrOfGlobalDynamicEmpSheetFinal[k];
        
        UILabel *lblTitleEmpName=[[UILabel alloc]init];
        lblTitleEmpName.backgroundColor=[UIColor clearColor];
        lblTitleEmpName.layer.borderWidth = 1.5f;
        lblTitleEmpName.layer.cornerRadius = 0.0f;
        lblTitleEmpName.layer.borderColor = [[UIColor grayColor] CGColor];
        lblTitleEmpName.frame=CGRectMake(10, yAxis, 200, 50);
        lblTitleEmpName.font=[UIFont systemFontOfSize:22];
        lblTitleEmpName.text=[NSString stringWithFormat:@"%@",[dictDataEmpSheet valueForKey:@"EmployeeName"]];
        lblTitleEmpName.textAlignment=NSTextAlignmentCenter;
        lblTitleEmpName.numberOfLines=700;
        [lblTitleEmpName setAdjustsFontSizeToFitWidth:YES];
        lblTitleEmpName.backgroundColor=[UIColor lightTextColorTimeSheet];
        [viewEmpSheetOnScroll addSubview:lblTitleEmpName];
        
        
        NSArray *arrEmpData=[dictDataEmpSheet valueForKey:@"EmployeeList"];
        
        for (int k1=0; k1<arrEmpData.count; k1++) {
            
            NSDictionary *dictDataaaa=arrEmpData[k1];
            CGRect txtViewFrame = CGRectMake(xAxis, yAxis,100,50);
            UITextField *txtView= [[UITextField alloc] init];
            //txtView.text = [global getHrAndMinsGlobal:[dictDataaaa valueForKey:@"actualDurationInMin"]];
            txtView.text = [global getHrAndMinsGlobal:[dictDataaaa valueForKey:@"billableDurationInMin"]];
            
            if ([txtView.text isEqualToString:@"00:00"]) {
                
                txtView.text =@"";
                txtView.placeholder=@"00:00";
                
            }
            
            
            txtView.frame =txtViewFrame;
            txtView.layer.borderWidth = 1.5f;
            txtView.layer.cornerRadius = 0.0f;
            txtView.layer.borderColor = [[UIColor grayColor] CGColor];
            [txtView setBackgroundColor:[UIColor clearColor]];
            txtView.font=[UIFont systemFontOfSize:20];
            txtView.delegate=self;
            
            UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
            leftView.backgroundColor = [UIColor clearColor];
            txtView.leftView = leftView;
            txtView.leftViewMode = UITextFieldViewModeAlways;
            txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            
            txtView.tag=k*1000+k1+500;
            
            [viewEmpSheetOnScroll addSubview:txtView];
            
            xAxis=xAxis+100;
            
        }
        
        yAxis=yAxis+50;
        
    }
    
    [scrollViewEmpTimeSheet addSubview:viewEmpSheetOnScroll];
    
    [_viewStopJobReason addSubview:scrollViewEmpTimeSheet];
    
    [scrollViewEmpTimeSheet setContentSize:CGSizeMake(scrollViewWidth,scrollViewHeight)];
    
    scrollViewEmpTimeSheet.scrollEnabled=true;
    scrollViewEmpTimeSheet.showsHorizontalScrollIndicator=true;
    scrollViewEmpTimeSheet.showsVerticalScrollIndicator=true;
    
}

-(void)createDynamicEmpSheetNew :(NSArray*)arrOfEmpSheet :(NSArray*)arrOfHeaderTitle{
    
    for(UIView *view in scrollViewEmpTimeSheet.subviews)
    {
        [view removeFromSuperview];
    }
    
    CGFloat scrollViewHeight=0.0;
    CGFloat scrollViewWidth=0.0;
    
    scrollViewHeight=arrOfGlobalDynamicEmpSheetFinal.count*60+60;
    scrollViewWidth=arrOfHeaderTitleForSlots.count*100;
    
    UIView *viewEmpSheetOnScroll=[global createDynamicEmpSheet:arrOfGlobalDynamicEmpSheetFinal :arrOfHeaderTitleForSlots :scrollViewEmpTimeSheet];
    
    [scrollViewEmpTimeSheet addSubview:viewEmpSheetOnScroll];
    
    [_viewStopJobReason addSubview:scrollViewEmpTimeSheet];
    
    [scrollViewEmpTimeSheet setContentSize:CGSizeMake(scrollViewWidth,scrollViewHeight)];
    
    scrollViewEmpTimeSheet.scrollEnabled=true;
    scrollViewEmpTimeSheet.showsHorizontalScrollIndicator=true;
    scrollViewEmpTimeSheet.showsVerticalScrollIndicator=true;
    
}

-(NSString*)getHrAndMins:(NSString*)strMins
{
    int seconds = (int)[strMins integerValue]*60;
    int minutes = (seconds / 60) % 60;
    int hours = seconds / 3600;
    return [NSString stringWithFormat:@"%02d:%02d",hours,minutes];
    
}

-(void)timerStarthere
{
    if (isCompletedStatusMechanical) {
        
    }else{
        
        secondsTimer=totalsecond;
        
        // [_btnTimeClock setTitle:[NSString stringWithFormat:@"%d",secondsTimer] forState:UIControlStateNormal];
        
        if (!jobMinTimer) {
            
            jobMinTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                           target:self
                                                         selector:@selector(timer)
                                                         userInfo:nil
                                                          repeats:YES];
            
            [[NSRunLoop currentRunLoop] addTimer:jobMinTimer
                                         forMode:NSRunLoopCommonModes];
        }
        
        //    jobMinTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
        //                                                   target:self
        //                                                 selector:@selector(timer)
        //                                                 userInfo:nil
        //                                                  repeats:YES];
        
    }
}
- (void)timer {
    
    secondsTimer++;
    
    NSDictionary *dictTimeClock=[self createTimemapForSeconds:secondsTimer];
    
    NSString *strTimeFormat=[NSString stringWithFormat:@"%02d:%02d:%02d",[[dictTimeClock valueForKey:@"h"] intValue],[[dictTimeClock valueForKey:@"m"] intValue],[[dictTimeClock valueForKey:@"s"] intValue]];
    
    [_btnTimeClock setTitle:strTimeFormat forState:UIControlStateNormal];
    
}

- (void)countUp
{
    secondsTimer += 1;
    
    if (secondsTimer == 60)
    {
        secondsTimer = 0;
        minutesTimer++;
        
        if (minutesTimer == 60)
        {
            minutesTimer = 0;
            hoursTimer++;
        }
    }
    
}

- (IBAction)action_ClientApproval:(id)sender {
    
    [self saveTechComments];
    
    if (isJobStarted) {
        
        //    [_objSubWorkOrderdetails setValue:@"true" forKey:@"isClientApproved"];
        //    NSError *error2;
        //    [context save:&error2];
        
        [jobMinTimer invalidate];
        jobMinTimer=nil;
        [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
        
        //  [self goToSendMail];
        
        [self saveImageToCoreData];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalClientApprovalViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalClientApprovalViewController"];
        objByProductVC.strWorlOrderId=strWorkOrderId;
        objByProductVC.strSubWorkOderId=_strSubWorkOrderId;
        objByProductVC.strWoType=_strWoType;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    } else {
        
        [global AlertMethod:Alert :alertStartJob];
        
    }
}

- (IBAction)action_StartRepair:(id)sender {
    
    if (isCompletedStatusMechanical) {
        
        [self goToStartRepair];
        
    }else{
        
        [self saveTechComments];
        
        if (isJobStarted) {
            
            if (isClientApproved) {
                
                if ([_btnStartnStopJOB.titleLabel.text isEqualToString:@"Start Job"] || [_btnStartnStopJOB.currentTitle isEqualToString:@"Start Job"]) {
                    
                    [global AlertMethod:Alert :alertStartJob];
                    
                }else{
                    
                    [_objSubWorkOrderdetails setValue:@"Running" forKey:@"subWOStatus"];
                    
                    NSError *error2;
                    [context save:&error2];
                    
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setObject:@"Running" forKey:@"WoStatus"];
                    isJobStarted=YES;
                    [defs synchronize];
                    
                    // _strWoType=@"TM";
                    
                    [self goToStartRepair];
                    
                }
                
            } else {
                
                [global AlertMethod:Alert :alertClientApproval];
                
            }
            
        } else {
            
            [global AlertMethod:Alert :alertStartJob];
            
        }
        
    }
    
}

-(void)goToStartRepair{
    
    [self saveImageToCoreData];
    
    [jobMinTimer invalidate];
    jobMinTimer=nil;
    [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
    
    if ([_strWoType isEqualToString:@"TM"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalStartRepairTM
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairTM"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=_objWorkOrderdetails;
        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderdetails;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalStartRepairViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairViewController"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=_objWorkOrderdetails;
        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderdetails;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
}
//============================================================================
//============================================================================
#pragma mark- --------------------Stop Job METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_SelectReason:(id)sender {
    
    [self methodTableViewAllocation];
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfLogType;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictDetailsFortbl=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    if ([dictDetailsFortbl isKindOfClass:[NSString class]]) {
        
        arrOfLogType=nil;
        
    }else{
        
        arrOfLogType=[dictDetailsFortbl valueForKey:@"ResetReasons"];
        
    }
    
    if ([arrOfLogType isKindOfClass:[NSArray class]]) {
        for (int k=0; k<arrOfLogType.count; k++) {
            
            NSDictionary *dictDataa=arrOfLogType[k];
            [arrDataTblView addObject:dictDataa];
            
        }
    }
    //    }else{
    //        [global AlertMethod:Info :NoDataAvailable];
    //    }
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailableReset];
    }else{
        tblData.tag=104;
        [self tableLoad:tblData.tag];
    }
    
    
}

- (IBAction)action_SaveStopReason:(id)sender {
    
    if ([_btnSelectReason.titleLabel.text isEqualToString:@"----Select Reason----"]) {
        
        [global AlertMethod:Alert :@"Please select reason to stop job"];
        
    } else {
        
        [jobMinTimer invalidate];
        
        [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOut];
        [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
        [viewBackGround removeFromSuperview];
        [_viewStopJobReason removeFromSuperview];
        
        [_btnSelectReason setTitle:@"----Select Reason----" forState:UIControlStateNormal];
        
        [_objSubWorkOrderdetails setValue:@"Stop" forKey:@"clockStatus"];
        
        NSError *error2;
        [context save:&error2];
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        
        [global saveEmployeeTimeSheetSlotWise:arrOfGlobalDynamicEmpSheetFinal];
        
        [self goToAppointmentViewOnStopJob];
        
    }
    
}



-(void)goToAppointmentViewOnStopJob
{
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
        AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
        AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}
- (IBAction)action_CancelStopJob:(id)sender {
    
    [viewBackGround removeFromSuperview];
    [_viewStopJobReason removeFromSuperview];
    
}

//============================================================================
//============================================================================
#pragma mark- --------------------Add Service Issues in Core Data METHODS-----------------
//============================================================================
//============================================================================

-(void)addServiceIssuesFromMobileToDB{
    
    // isCollapseAllViews=YES;
    
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderIssueDcs *objSubWorkOrderIssues = [[MechanicalSubWorkOrderIssueDcs alloc]initWithEntity:entitySubWorkOrderIssues insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    objSubWorkOrderIssues.subWorkOrderIssueId=[global getReferenceNumber];
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.serviceIssue=_txtViewIssuesDesc.text;
    objSubWorkOrderIssues.priority=[NSString stringWithFormat:@"%@",strPriorityIdGlobal];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    if (![_btn_AddEquipments.titleLabel.text isEqualToString:@"----Select Equipment----"]) {
        
        objSubWorkOrderIssues.equipmentCode=[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"itemcode"]];
        objSubWorkOrderIssues.equipmentName=[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"itemName"]];
        objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"serialNumber"]];
        objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"modelNumber"]];
        objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"manufacturer"]];
        objSubWorkOrderIssues.equipmentNo=[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"itemNo"]];
        
        NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[objSelectedEquipment valueForKey:@"itemCustomName"]];
        
        if ([strItemCustomName isEqualToString:@"(null)"]) {
            
            strItemCustomName=@"";
            
        }
        
        objSubWorkOrderIssues.equipmentCustomName=[NSString stringWithFormat:@"%@",strItemCustomName];
        
    }else{
        
        objSubWorkOrderIssues.equipmentCode=@"";
        objSubWorkOrderIssues.equipmentName=@"";
        objSubWorkOrderIssues.serialNumber=@"";
        objSubWorkOrderIssues.modelNumber=@"";
        objSubWorkOrderIssues.manufacturer=@"";
        objSubWorkOrderIssues.equipmentCustomName=@"";
        objSubWorkOrderIssues.equipmentNo=@"";
        
    }
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
}

-(void)addServiceHelperFromMobileToDB{
    
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    
    MechanicalSubWOTechHelperDcs *objSubWorkOrderHelper = [[MechanicalSubWOTechHelperDcs alloc]initWithEntity:entityMechanicalSubWorkOrderHelper insertIntoManagedObjectContext:context];
    
    objSubWorkOrderHelper.workorderId=strWorkOrderId;
    objSubWorkOrderHelper.subWorkOrderTechnicianId=[global getReferenceNumber];
    objSubWorkOrderHelper.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderHelper.employeeNo=[NSString stringWithFormat:@"%@",strHelperIdGlobal];
    objSubWorkOrderHelper.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderHelper.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderHelper.createdDate=[global strCurrentDate];
    objSubWorkOrderHelper.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderHelper.modifiedDate=[global strCurrentDate];
    objSubWorkOrderHelper.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    if (isMechanic) {
        objSubWorkOrderHelper.laborType=[NSString stringWithFormat:@"%@",@"L"];
    } else {
        objSubWorkOrderHelper.laborType=[NSString stringWithFormat:@"%@",@"H"];
    }
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderHelperFromDataBaseForMechanical];
    
}

-(void)addServiceNotesFromMobileToDB{
    
    entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderNoteDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderNoteDcs alloc]initWithEntity:entityMechanicalSubWorkOrderNotes insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWorkOrderId;
    objSubWorkOrderNotes.subWorkOrderNoteId=[global getReferenceNumber];
    objSubWorkOrderNotes.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderNotes.note=_txtViewAddNotesDesc.text;
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.createdDate=[global strCurrentDate];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.modifiedDate=[global strCurrentDate];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderNotesFromDataBaseForMechanical];
    
}

-(void)addStartActualHoursFromMobileToDB{
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWorkOrderId;
    
    NSString *strNos=[global getReferenceNumber];
    objSubWorkOrderNotes.subWOActualHourId=strNos;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:strNos forKey:@"subWOActualHourId"];
    [defs synchronize];
    
    objSubWorkOrderNotes.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderNotes.timeIn=[global strCurrentDateFormattedForMechanical];
    objSubWorkOrderNotes.mobileTimeIn=objSubWorkOrderNotes.timeIn;
    objSubWorkOrderNotes.status=@"Inspection";
    objSubWorkOrderNotes.timeOut=@"";
    objSubWorkOrderNotes.mobileTimeOut=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKey;
    objSubWorkOrderNotes.reason=@"";
    objSubWorkOrderNotes.actHrsDescription=@"";
    objSubWorkOrderNotes.subWorkOrderNo=@"";
    objSubWorkOrderNotes.subWOActualHourNo=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKey;
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.createdDate=[global strCurrentDate];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.modifiedDate=[global strCurrentDate];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    // Latitude changes
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objSubWorkOrderNotes.latitude=latitude;
    objSubWorkOrderNotes.longitude=longitude;
    // Latitude changes end
    
    objSubWorkOrderNotes.employeeNo=[global getEmployeeDeatils];
    
    NSError *error2;
    [context save:&error2];
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
        
        strNotificationTypeName=@"MechanicalActualHrsSyncInspectionFR2";
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:strNotificationTypeName object:nil];
        
        SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
        
        [objSync syncMechanicalActualHours:strWorkOrderId :strSubWorkOrderIdGlobal :strNos :strNotificationTypeName];
        
    }
}

-(void)stopLoader
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:strNotificationTypeName
                                                  object:nil];
    
    [DejalBezelActivityView removeView];
    
}

-(void)addServiceIssuesRepairFromMobileToDB{
    
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepair insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    
    NSString *strIssueRepairTdLocal=[global getReferenceNumber];
    objSubWorkOrderIssues.issueRepairId=strIssueRepairTdLocal;
    objSubWorkOrderIssues.repairLaborId=strRepairOptionIdGlobal;
    objSubWorkOrderIssues.subWorkOrderIssueId=strSubWorkOrderIssueIdGlobal;
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.repairMasterId=strRepairIdGlobal;
    objSubWorkOrderIssues.repairName=[NSString stringWithFormat:@"%@",_btnSelectRepair.titleLabel.text];
    objSubWorkOrderIssues.repairDesc=[NSString stringWithFormat:@"%@",_txtViewRepairDescriptions.text];
    objSubWorkOrderIssues.costAdjustment=[NSString stringWithFormat:@"%@",_txtRepairCostAdjustment.text];
    objSubWorkOrderIssues.partCostAdjustment=[NSString stringWithFormat:@"%@",strPartCostAdjustmentGlobal];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    objSubWorkOrderIssues.repairAmt=_txtRepairAmt.text;
    
    if (_txtRepairQty.text.length==0) {
        
        objSubWorkOrderIssues.qty = @"1";
        
    } else {
        
        objSubWorkOrderIssues.qty = _txtRepairQty.text;
        
    }
    
    NSError *error2;
    [context save:&error2];
    
    NSDictionary *dictDataRepairMaster=arrOfSelectedRepairOption[indexRepairOptionGlobal];
    NSArray *arrOfPartsMaster=[dictDataRepairMaster valueForKey:@"RepairPartMasterExtSerDc"];
    
    for (int k=0; k<arrOfPartsMaster.count; k++) {
        
        NSDictionary *dictDataPartss=arrOfPartsMaster[k];
        
        NSDictionary *dictDataPartssMaster=[self fetchPartDetailDictionaryViaPartCode:[NSString stringWithFormat:@"%@",[dictDataPartss valueForKey:@"PartCode"]]];
        
        [self addServiceIssuesRepairPartsFromMobileToDB:dictDataPartss :dictDataPartssMaster :strIssueRepairTdLocal];
        
    }
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"H" :[dictLaborInfoToSave valueForKey:@"totalPriceHelper"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"HelperHours"] :strIssueRepairTdLocal];//HelperHours totalPriceHelper
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"L" :[dictLaborInfoToSave valueForKey:@"totalPriceLabour"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"LaborHours"] :strIssueRepairTdLocal];
    
    
    //Fetch All Data After Saving in DB
    
    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
    
    // [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
}

-(void)addServiceIssuesRepairNonStandardFromMobileToDB{
    
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepair insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    
    NSString *strIssueRepairTdLocal=[global getReferenceNumber];
    objSubWorkOrderIssues.issueRepairId=strIssueRepairTdLocal;
    
    objSubWorkOrderIssues.subWorkOrderIssueId=strSubWorkOrderIssueIdGlobal;
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.repairMasterId=@"";
    objSubWorkOrderIssues.repairLaborId=@"";
    objSubWorkOrderIssues.repairName=[NSString stringWithFormat:@"%@",_txtRepairName.text];
    objSubWorkOrderIssues.repairDesc=[NSString stringWithFormat:@"%@",_txtViewRepairDescriptions.text];
    objSubWorkOrderIssues.costAdjustment=[NSString stringWithFormat:@"%@",_txtRepairCostAdjustment.text];
    objSubWorkOrderIssues.partCostAdjustment=[NSString stringWithFormat:@"%@",strPartCostAdjustmentGlobal];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    objSubWorkOrderIssues.repairAmt=_txtTotalRepairCostNonStandard.text;
    
    if (isTotalRepairCost) {
        
        objSubWorkOrderIssues.repairAmt=_txtTotalRepairCostNonStandard.text;
        objSubWorkOrderIssues.nonStdPartAmt=@"";
        objSubWorkOrderIssues.nonStdLaborAmt=@"";
        objSubWorkOrderIssues.nonStdRepairAmt=_txtTotalRepairCostNonStandard.text;
        
    } else {
        
        float TotalValueRepairAmt=[_txtRepairPartCostNonStandard.text floatValue]+[_txtRepairLaborCostNonStandard.text floatValue];
        
        objSubWorkOrderIssues.repairAmt=[NSString stringWithFormat:@"%f",TotalValueRepairAmt];
        objSubWorkOrderIssues.nonStdPartAmt=_txtRepairPartCostNonStandard.text;
        objSubWorkOrderIssues.nonStdLaborAmt=_txtRepairLaborCostNonStandard.text;
        objSubWorkOrderIssues.nonStdRepairAmt=@"";
        
    }
    
    if (_txtRepairQty.text.length==0) {
        
        objSubWorkOrderIssues.qty = @"1";
        
    } else {
        
        objSubWorkOrderIssues.qty = _txtRepairQty.text;
        
    }
    
    NSError *error2;
    [context save:&error2];
    
    dictLaborInfoToSave = @{@"LaborHours":[NSString stringWithFormat:@"%@",@"00:00"],
                            @"HelperHours":[NSString stringWithFormat:@"%@",@"00:00"],
                            @"totalPriceLabour":[NSString stringWithFormat:@"%@",@"0.0"],
                            @"totalPriceHelper": [NSString stringWithFormat:@"%@",@"0.0"],
                            @"DescRepair": [NSString stringWithFormat:@"%@",@""],
                            };
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"H" :[dictLaborInfoToSave valueForKey:@"totalPriceLabour"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"LaborHours"] :strIssueRepairTdLocal];
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"L" :[dictLaborInfoToSave valueForKey:@"totalPriceHelper"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"HelperHours"] :strIssueRepairTdLocal];
    
    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
    
}
-(void)addServiceIssuesRepairPartsFromMobileToDB :(NSDictionary*)dictPartsData :(NSDictionary*)dictPartsDataFromMasterEquip :(NSString *)strRepairIDD{
    
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    objSubWorkOrderIssues.subWorkOrderIssueId=strSubWorkOrderIssueIdGlobal;
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.issueRepairPartId=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"RepairPartMasterId"]];
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",strRepairIDD];
    
    NSString *srtPartType;
    if (isStandardSubWorkOrder) {
        
        srtPartType=@"Standard";
    } else {
        
        srtPartType=@"Non-Standard";
    }
    
    objSubWorkOrderIssues.partType=srtPartType;
    objSubWorkOrderIssues.partCode=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"PartCode"]];
    objSubWorkOrderIssues.partName=[NSString stringWithFormat:@"%@",[dictPartsDataFromMasterEquip valueForKey:@"Name"]];
    objSubWorkOrderIssues.partDesc=[NSString stringWithFormat:@"%@",[dictPartsDataFromMasterEquip valueForKey:@"Description"]];
    objSubWorkOrderIssues.qty=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"Qty"]];
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"Qty"]];
    objSubWorkOrderIssues.unitPrice=[NSString stringWithFormat:@"%@",[dictPartsDataFromMasterEquip valueForKey:@"LookupPrice"]];
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.serialNumber=@"";
    objSubWorkOrderIssues.modelNumber=@"";
    objSubWorkOrderIssues.manufacturer=@"";
    objSubWorkOrderIssues.installationDate=[NSString stringWithFormat:@"%@",@""];
    objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%f",multiplierGlobal];
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",@"true"];
    
    NSError *error2;
    [context save:&error2];
    
}

-(void)addServiceIssuesRepairLabourFromMobileToDB :(NSString*)strLaborType :(NSString*)strLaborCostt :(NSString*)strRepairDescriptionn :(NSString*)strLaborHourss :(NSString*)strIssueRepairTdLocal{
    
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    
    MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssues = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairLabour insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    if ([strLaborType isEqualToString:@"H"]) {
        
        objSubWorkOrderIssues.issueRepairLaborId=[NSString stringWithFormat:@"%@11",[global getReferenceNumber]];
        
    } else {
        
        objSubWorkOrderIssues.issueRepairLaborId=[NSString stringWithFormat:@"%@22",[global getReferenceNumber]];
        
    }
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",strIssueRepairTdLocal];
    objSubWorkOrderIssues.subWorkOrderIssueId=strSubWorkOrderIssueIdGlobal;
    
    NSString *srtLaborCostType;
    if (isStandardSubWorkOrder) {
        
        srtLaborCostType=@"SC";
        
    } else {
        
        srtLaborCostType=@"AHC";
        
    }
    
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    
    objSubWorkOrderIssues.laborCost=[NSString stringWithFormat:@"%@",strLaborCostt];
    objSubWorkOrderIssues.laborCostType=[NSString stringWithFormat:@"%@",srtLaborCostType];
    objSubWorkOrderIssues.laborDescription=[NSString stringWithFormat:@"%@",strRepairDescriptionn];
    objSubWorkOrderIssues.laborHours=[NSString stringWithFormat:@"%@",strLaborHourss];
    objSubWorkOrderIssues.laborType=[NSString stringWithFormat:@"%@",strLaborType];
    
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    //    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    //
    //    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
}

-(float)fetchPartDetailViaPartCode :(NSString*)strPartCode{
    
    float lookUpPrice=[global fetchPartDetailViaPartCode:strPartCode :arrOfPartsGlobalMasters];
    
    return lookUpPrice;
    
}


-(NSString*)fetchPartCategorySysNameDetailViaPartCode :(NSString*)strPartCode{
    
    NSString *strCategorySysNameLocal;
    
    strCategorySysNameLocal=[global fetchPartCategorySysNameDetailViaPartCode:strPartCode :arrOfPartsGlobalMasters];
    
    return strCategorySysNameLocal;
    
}

-(NSDictionary*)fetchPartDetailDictionaryViaPartCode :(NSString*)strPartCode{
    
    NSDictionary *dictDataPartss;
    
    dictDataPartss=[global fetchPartDetailDictionaryViaPartCode:strPartCode :arrOfPartsGlobalMasters];
    
    return dictDataPartss;
    
}

//============================================================================
//============================================================================
#pragma mark TextField Delegate Method
//============================================================================
//============================================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    return  YES;
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)texth
{
    if (range.location == 0 && [texth isEqualToString:@" "]) {
        return NO;
    }
    else
        return YES;
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    if (textView==_txtViewTechComment) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        [_objWorkOrderdetails setValue:_txtViewTechComment.text forKey:@"technicianComment"];
        
        NSError *error2;
        [context save:&error2];
        
    }
    
}
-(void)replaceBillableDurationInEmpSheet :(int)tag :(NSString*)strDuration{
    
    int row=tag/1000;
    int section=tag%1000;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictTemp=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictTemp2=[[NSMutableDictionary alloc]init];
    
    dictTemp=arrOfGlobalDynamicEmpSheetFinal[row];
    
    NSString *strHelper=[dictTemp valueForKey:@"Helper"];
    BOOL isLaborType;
    if ([strHelper isEqualToString:@"No"]) {
        
        isLaborType=YES;
        
    } else {
        
        isLaborType=NO;
        
    }
    
    arrTemp=[dictTemp valueForKey:@"EmployeeList"];
    
    dictTemp2=arrTemp[section];
    
    NSString *strworkingType=[dictTemp2 valueForKey:@"workingType"];
    BOOL isHolidayyyy;
    if ([strworkingType isEqualToString:@"3"]) {
        isHolidayyyy=YES;
    } else {
        isHolidayyyy=NO;
    }
    
    NSString *strAmount= [global methodToCalculateLaborPriceGlobal:strDuration :isLaborType :arrOfHoursConfig :true :isHolidayyyy :[dictTemp2 valueForKey:@"timeSlot"]];
    
    [dictTemp2 setValue:strDuration forKey:@"billableDurationInMin"];
    [dictTemp2 setValue:strAmount forKey:@"billableAmt"];
    
    [arrTemp replaceObjectAtIndex:section withObject:dictTemp2];
    
    [dictTemp setValue:arrTemp forKey:@"EmployeeList"];
    
    [arrOfGlobalDynamicEmpSheetFinal replaceObjectAtIndex:row withObject:dictTemp];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    if (textField==_txtTotalRepairCostNonStandard) {
        
        NSString *strString;
        BOOL isNuberOnly;
        
        isNuberOnly = YES;
        
        if ([string isEqualToString:@""]) {
            
            
            if ([textField.text length] > 0) {
                strString = [textField.text substringToIndex:[textField.text length] - 1];
            }
            if ([textField.text length] == 1){
                strString= @"";
            }
            
        }else{
            
            strString=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            isNuberOnly =[global isNumberOnlyWithDecimalNew:string :range :10 :(int)textField.text.length :@".0123456789" :strString];
            
        }
        
        if (isNuberOnly) {
            
            [self calculateTotalRepairAmount:_txtRepairQty.text :strString];
            
        }
        
        return isNuberOnly;
        
        
    }
    if (textField==_txtRepairPartCostNonStandard) {
        
        NSString *strString;
        BOOL isNuberOnly;
        
        isNuberOnly = YES;
        
        if ([string isEqualToString:@""]) {
            
            
            if ([textField.text length] > 0) {
                strString = [textField.text substringToIndex:[textField.text length] - 1];
            }
            if ([textField.text length] == 1){
                strString= @"";
            }
            
        }else{
            
            strString=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            isNuberOnly =[global isNumberOnlyWithDecimalNew:string :range :10 :(int)textField.text.length :@".0123456789" :strString];
            
        }
        
        if (isNuberOnly) {
            
            NSString *strTotal = [NSString stringWithFormat:@"%d",[strString intValue]+[_txtRepairLaborCostNonStandard.text intValue]];
            
            [self calculateTotalRepairAmount:_txtRepairQty.text :strTotal];
            
        }
        
        return isNuberOnly;
        
        
    }
    if (textField==_txtRepairLaborCostNonStandard) {
        
        NSString *strString;
        BOOL isNuberOnly;
        
        isNuberOnly = YES;
        
        if ([string isEqualToString:@""]) {
            
            
            if ([textField.text length] > 0) {
                strString = [textField.text substringToIndex:[textField.text length] - 1];
            }
            if ([textField.text length] == 1){
                strString= @"";
            }
            
        }else{
            
            strString=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            isNuberOnly =[global isNumberOnlyWithDecimalNew:string :range :10 :(int)textField.text.length :@".0123456789" :strString];
            
        }
        
        if (isNuberOnly) {
            
            NSString *strTotal = [NSString stringWithFormat:@"%d",[strString intValue]+[_txtRepairPartCostNonStandard.text intValue]];
            
            [self calculateTotalRepairAmount:_txtRepairQty.text :strTotal];
            
        }
        
        return isNuberOnly;
        
        
    }
    if (textField==_txtRepairQty) {
        
        
        if (range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        
        NSString *strString;
        BOOL isNuberOnly;
        
        isNuberOnly = YES;
        
        if ([string isEqualToString:@""]) {
            
            
            if ([textField.text length] > 0) {
                strString = [textField.text substringToIndex:[textField.text length] - 1];
            }
            if ([textField.text length] == 1){
                strString= @"";
            }
            
        }else{
            
            strString=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            isNuberOnly =[global isNumberOnlyWithDecimal:string :range :3 :(int)textField.text.length :@"0123456789" :strString];
            
        }
        
        if (isNuberOnly) {
            
            //[self calculateTotalRepairAmount:strString :_txtRepairAmt.text];
            
            if (isStandardRepair) {
                
                [self calculateTotalRepairAmount:strString :_txtRepairAmt.text];
                
            } else {
                
                if (isTotalRepairCost) {
                    
                    [self calculateTotalRepairAmount:strString :_txtTotalRepairCostNonStandard.text];
                    
                } else {
                    
                    NSString *strTotal = [NSString stringWithFormat:@"%d",[_txtRepairPartCostNonStandard.text intValue]+[_txtRepairLaborCostNonStandard.text intValue]];
                    
                    [self calculateTotalRepairAmount:strString :strTotal];
                    
                }
                
            }
            
        }
        
        return isNuberOnly;
        
        
    }
    
    int tagTextFld=(int)textField.tag;
    
    if (tagTextFld==0) {
        
        if (range.location == 0 && [string isEqualToString:@"0"]) {
            
            return NO;
            
        }else {
            
            NSString *strString;
            BOOL isNuberOnly;
            
            isNuberOnly = YES;
            
            if ([string isEqualToString:@""]) {
                
                
                if ([textField.text length] > 0) {
                    strString = [textField.text substringToIndex:[textField.text length] - 1];
                }
                if ([textField.text length] == 1){
                    strString= @"";
                }
                
            }else{
                
                strString=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                isNuberOnly =[global isNumberOnlyWithDecimal:string :range :3 :(int)textField.text.length :@"0123456789" :strString];
                
            }
            
            if (isNuberOnly) {
                
                NSString *strTagTextFld=textField.accessibilityHint;
                int row= [strTagTextFld intValue]/1000;
                int section =[strTagTextFld intValue]%1000;
                
                NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
                
                NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
                
                NSString *strIssueRepairIdToCheck;
                
                NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                
                for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
                    
                    NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
                    
                    NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
                    
                    if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                        
                        [arrTemp addObject:dictIssuesRepairData];
                        
                    }
                    
                }
                
                NSManagedObject *objTemp=arrTemp[row];
                
                strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
                NSString *strWorkOrderIDd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"workorderId"]];
                
                NSString *strSubWorkOrderIdd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderId"]];
                
                if ((strString.length==0) || [strString isEqualToString:@""]) {
                    
                    strString = @"";
                    
                }
                
                SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
                
                [objSync fetchRepairToUpdateRepairQty:strWorkOrderIDd :strSubWorkOrderIdd :strIssueIdToCheck :strIssueRepairIdToCheck :strString];
                
                NSIndexPath *indexPaths = [NSIndexPath indexPathForRow:row inSection:section];
                
                [_tblViewServiceIssues reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPaths] withRowAnimation:UITableViewRowAnimationNone];
                
                return YES;
                
            } else {
                
                return NO;
                
            }
            
        }
        
    } else {
        
        if (tagTextFld>499) {
            
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            {
                // BasicAlert(@"", @"This field accepts only numeric entries.");
                return NO;
            }
            else
            {
                NSString *text = textField.text;
                NSInteger length = text.length;
                BOOL shouldReplace = YES;
                
                if (![string isEqualToString:@""])
                {
                    switch (length)
                    {
                        case 2:
                            textField.text = [text stringByAppendingString:@":"];
                            break;
                            
                        default:
                            break;
                    }
                    if (length > 4)
                        shouldReplace = NO;
                }
                
                return shouldReplace;
            }
            return YES;
            
        } else {
            
            if (textField.tag==402) {
                
                if(range.length + range.location > textField.text.length)
                {
                    return NO;
                }
                
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                
                if (newLength>10) {
                    
                    return NO;
                    
                }
                
            }
            
            if (textField.tag==402) {
                
                if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
                {
                    if (![string isEqualToString:@"."]) {
                        
                        return NO;
                        
                    }
                }
                
            }
            else{
                NSScanner *scanner = [NSScanner scannerWithString:string];
                BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
                
                if (range.location == 0 && [string isEqualToString:@" "]) {
                    return NO;
                }
                if (textField.tag==403) {
                    
                    return YES;
                    
                }
                BOOL  isReturnTrue;
                
                isReturnTrue=YES;
                
                if (!isNumeric) {
                    
                    if ([string isEqualToString:@""]) {
                        
                        isReturnTrue=YES;
                        
                    }else
                        
                        isReturnTrue=NO;
                    
                }
                
                if (!isReturnTrue) {
                    
                    return NO;
                    
                }
            }
            
            if (textField.tag==402) {
                
                if (!isStandardRepair) {
                    
                    return YES;
                    
                }
                
                if ([string isEqualToString:@""]) {
                    
                    NSString *strAmtRepair=[NSString stringWithFormat:@"%@",strGlobalAmtRepair];
                    
                    float amtRepair=[strAmtRepair floatValue];
                    
                    NSString *strLast;
                    
                    if ([textField.text length] > 0) {
                        strLast = [textField.text substringToIndex:[textField.text length] - 1];
                    }
                    if ([textField.text length] == 1){
                        strLast= @"";
                    }
                    
                    float currentValue=[strLast floatValue];
                    
                    float totalAmtCostAdjust=amtRepair+currentValue;
                    
                    NSString *strtotalAmtCostAdjust=[NSString stringWithFormat:@"%.02f",totalAmtCostAdjust];
                    
                    _txtRepairAmt.text=strtotalAmtCostAdjust;
                    
                    
                } else {
                    
                    
                    
                    NSString *strAmtRepair=[NSString stringWithFormat:@"%@",strGlobalAmtRepair];
                    
                    float amtRepair=[strAmtRepair floatValue];
                    
                    NSString *strString=[NSString stringWithFormat:@"%@%@",textField.text,string];
                    
                    
                    // Logic For Two Digits After Decimals
                    NSArray *sep = [strString componentsSeparatedByString:@"."];
                    if([sep count] >= 2)
                    {
                        NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                        if (!([sepStr length]>2)) {
                            if ([sepStr length]==2 && [string isEqualToString:@"."]) {
                                return NO;
                            }
                        }
                        else{
                            return NO;
                        }
                    }
                    //END Logic For Two Digits After Decimals
                    
                    
                    float currentValue=[strString floatValue];
                    
                    float totalAmtCostAdjust=amtRepair+currentValue;
                    
                    NSString *strtotalAmtCostAdjust=[NSString stringWithFormat:@"%.02f",totalAmtCostAdjust];
                    
                    _txtRepairAmt.text=strtotalAmtCostAdjust;
                    
                    
                }
                
                
                return YES;
            }
            
            else
                return YES;
        }
        
    }
    
    
}


//- (void)textViewDidBeginEditing:(UITextView *)textView {
//
//    if (textView==_txtViewAdditionalInfo) {
//
//        [_scrollVieww setContentOffset:
//         CGPointMake(0, _scrollVieww.frame.size.height-200) animated:YES];
//
//    }
//
//}
//- (void)textViewDidEndEditing:(UITextView *)textView {
//
//    if (textView==_txtViewAdditionalInfo) {
//
//        [_scrollVieww setContentOffset:
//         CGPointMake(0, _scrollVieww.frame.size.height+200) animated:YES];
//
//    }
//
//}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    int tagTextFld=(int)textField.tag;
    
    if (tagTextFld>499) {
        
        NSString *strTextx;
        
        if (textField.text.length>=5) {
            
            strTextx=textField.text;
            
        } else if (textField.text.length==4){
            
            NSString *strTemp=@"0";//  0:00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==3){
            
            NSString *strTemp=@"00";//  :00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==2){
            
            //            NSString *strTemp=@"00:";//  00
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=@":00";//  00
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==1){
            
            //            NSString *strTemp=@"00:0";//  0
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=[NSString stringWithFormat:@"0%@:00",textField.text];
            //NSString *strTemp=@"0:00";
            //strTextx=[textField.text stringByAppendingString:strTemp];
            strTextx=strTemp;
        } else if (textField.text.length==0){
            
            strTextx=@"00:00";
            
        }
        
        NSString *lastStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        //NSString *lastSecondStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        NSString *finalStrText = [NSString stringWithFormat:@"%@",lastStrText];
        int valueEntered = [finalStrText intValue];
        if (valueEntered>59) {
            
            [global AlertMethod:Alert :@"Minutes can't be greater then 59"];
            
        }else{
            
            textField.text=strTextx;
            
            if ([textField.text isEqualToString:@"00:00"]) {
                
                textField.text =@"";
                textField.placeholder=@"00:00";
                
            }
            
            NSArray *arrTime=[strTextx componentsSeparatedByString:@":"];
            
            NSString *strHrs,*strMinutes;
            
            if (arrTime.count==1) {
                
                strHrs=arrTime[0];
                strMinutes=@"";
                
            }
            
            if (arrTime.count==2) {
                
                strHrs=arrTime[0];
                strMinutes=arrTime[1];
                
            }
            
            NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
            
            int totalsecondUpdatedEstTime=(int)secondTimeToIncrease;
            totalsecondUpdatedEstTime=totalsecondUpdatedEstTime/60;
            
            //[self replaceBillableDurationInEmpSheet:tagTextFld-500 :strTextx];
            [self replaceBillableDurationInEmpSheet:tagTextFld-500 :[NSString stringWithFormat:@"%d",totalsecondUpdatedEstTime]];
            
        }
        
    }
}

//============================================================================
//============================================================================
#pragma mark- ----------------Delete Core DB Methods----------------
//============================================================================
//============================================================================

-(void)deleteHelperFromDB :(NSString *)strEmployeeNoToDelete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOTechHelperDcs Detail Data
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && employeeNo = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strEmployeeNoToDelete];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self fetchSubWorkOrderHelperFromDataBaseForMechanical];
    
    [_tblviewHelper reloadData];
    
    [self adjustViewHeights];
    
}


-(void)deleteRepairFromDB :(NSString *)strWoId :(NSString *)strSubWoId :(NSString *)strSubWoIssuesId :(NSString *)strRepairId :(int)intSection :(int)intRow{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOIssueRepairDcs Detail Data
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context]];
    //subWorkOrderIssueId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWoId,strSubWoId,strRepairId,strSubWoIssuesId];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    // [arrOfSubWorkServiceIssuesRepair removeObjectAtIndex:intRow];
    
    NSRange range = NSMakeRange(intSection, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [_tblViewServiceIssues reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
    
    [self deletePartFromDB:strWoId :strSubWoId :strSubWoIssuesId :strRepairId];
    
    [self adjustViewHeightsonCollapsingSections];
    
}

-(void)deletePartFromDB :(NSString *)strWoId :(NSString *)strSubWoId :(NSString *)strSubWoIssuesId :(NSString *)strRepairId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOTechHelperDcs Detail Data
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWoId,strSubWoId,strRepairId,strSubWoIssuesId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self deletelaborFromDb:strWoId :strSubWoId :strSubWoIssuesId :strRepairId];
    
}

-(void)deletelaborFromDb :(NSString *)strWoId :(NSString *)strSubWoId :(NSString *)strSubWoIssuesId :(NSString *)strRepairId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOTechHelperDcs Detail Data
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWoId,strSubWoId,strRepairId,strSubWoIssuesId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}


//============================================================================
//============================================================================
#pragma mark- ----------------Before Image Methods----------------
//============================================================================
//============================================================================

- (IBAction)action_CloseBeforImgView:(id)sender {
    
    [_view_BeforeImage removeFromSuperview];
    
    // [_scrollViewGeneralInfo setFrame:CGRectMake(0, _scrollViewGeneralInfo.frame.origin.y, _scrollViewGeneralInfo.frame.size.width, _scrollViewGeneralInfo.frame.size.height+_viewFinalSavenContinue.frame.size.height+_view_BeforeImage.frame.size.height)];
    
}

- (IBAction)action_BeforeImgView:(id)sender {
    
    //Adding BeforeImageInfoView
    
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewFinalSavenContinue.frame.origin.y-_view_BeforeImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view_BeforeImage.frame.size.height);
    [_view_BeforeImage setFrame:frameFor_view_BeforeImageInfo];
    [self.view addSubview:_view_BeforeImage];
    
    //   [_scrollViewGeneralInfo setFrame:CGRectMake(0, _scrollViewGeneralInfo.frame.origin.y, _scrollViewGeneralInfo.frame.size.width, _scrollViewGeneralInfo.frame.size.height-160)];
    
    // [_scrollViewGeneralInfo setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_OtherInfo.frame.size.height+_view_OtherInfo.frame.origin.y)];
    
    
}
- (IBAction)action_BeforeImages:(id)sender {
    
    /*
     UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
     delegate:self
     cancelButtonTitle:@"Cancel"
     destructiveButtonTitle:nil
     otherButtonTitles:@"Capture New", @"Gallery", nil];
     
     [actionSheet showInView:self.view];
     
     */
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              if (isCompletedStatusMechanical) {
                                  
                                  
                              }else{
                                  
                                  if (arrOfBeforeImageAll.count<10)
                                  {
                                      NSLog(@"The CApture Image.");
                                      
                                      NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                      
                                      BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                                      
                                      if (isfirstTimeAudio) {
                                          
                                          [defs setBool:NO forKey:@"firstCamera"];
                                          [defs synchronize];
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }else{
                                          BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                          
                                          if (isCameraPermissionAvailable) {
                                              
                                              UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                              imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                              imagePickController.delegate=(id)self;
                                              imagePickController.allowsEditing=TRUE;
                                              [self presentViewController:imagePickController animated:YES completion:nil];
                                              
                                              
                                          }else{
                                              
                                              UIAlertController *alert= [UIAlertController
                                                                         alertControllerWithTitle:@"Alert"
                                                                         message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                              
                                              UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction * action)
                                                                    {
                                                                        
                                                                        
                                                                        
                                                                    }];
                                              [alert addAction:yes];
                                              UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                           NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                           [[UIApplication sharedApplication] openURL:url];
                                                                       } else {
                                                                           
                                                                           //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                           //                                     [alert show];
                                                                           
                                                                       }
                                                                       
                                                                   }];
                                              [alert addAction:no];
                                              [self presentViewController:alert animated:YES completion:nil];
                                          }
                                      }
                                  }
                                  else
                                  {
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                      [alert show];
                                  }
                              }
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if (isCompletedStatusMechanical) {
                                 
                                 
                             }else{
                                 
                                 if (arrOfBeforeImageAll.count<10)
                                 {
                                     
                                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                     
                                     BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                                     
                                     if (isfirstTimeAudio) {
                                         
                                         [defs setBool:NO forKey:@"firstGallery"];
                                         [defs synchronize];
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }else{
                                         BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                         
                                         if (isCameraPermissionAvailable) {
                                             
                                             UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                             imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                             imagePickController.delegate=(id)self;
                                             imagePickController.allowsEditing=TRUE;
                                             [self presentViewController:imagePickController animated:YES completion:nil];
                                             
                                             
                                         }else{
                                             
                                             UIAlertController *alert= [UIAlertController
                                                                        alertControllerWithTitle:@"Alert"
                                                                        message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       
                                                                       
                                                                   }];
                                             [alert addAction:yes];
                                             UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                        handler:^(UIAlertAction * action)
                                                                  {
                                                                      
                                                                      if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                          NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                          [[UIApplication sharedApplication] openURL:url];
                                                                      } else {
                                                                          
                                                                          //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                          //                                     [alert show];
                                                                          
                                                                      }
                                                                      
                                                                  }];
                                             [alert addAction:no];
                                             [self presentViewController:alert animated:YES completion:nil];
                                         }
                                     }
                                 }else{
                                     
                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                     [alert show];
                                 }
                             }
                             
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)methodViewWillAppear{
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodStatus];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
        EditImageViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewControlleriPad"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isFromBackServiceDynamci"];
        
        if (isFromBack) {
            
            arrOfBeforeImageAll=nil;
            arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            [defsBack setBool:NO forKey:@"isFromBackServiceDynamci"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseMechanical];
            
        }
        
        
        //    _lblOne.clipsToBounds = YES;
        //    _lblOne.layer.masksToBounds = YES;
        //    _lblOne.layer.cornerRadius = 40.0;
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreview"];
        if (yesFromDeleteImage) {
            
            yesEditedSomething=YES;
            
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreview"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfBeforeImageAll.count; k++) {
                
                NSDictionary *dictdat=arrOfBeforeImageAll[k];
                
                NSString *strImageName;
                
                if ([dictdat isKindOfClass:[NSString class]]) {
                    
                    strImageName=arrOfBeforeImageAll[k];
                    
                } else {
                    
                    strImageName=[dictdat valueForKey:@"woImagePath"];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImages"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                        
                    }
                }
                //            if (yesFoundName) {
                //
                //                [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                //
                //            }
            }
            if (!(arrTempBeforeImage.count==0)) {
                // arrOfBeforeImageAll=nil;
                // arrOfBeforeImageAll=[[NSMutableArray alloc]init];
                [arrOfBeforeImageAll removeObjectsInArray:arrTempBeforeImage];
            }
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImages"];
            [defsnew synchronize];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            yesEditedSomething=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            yesEditedSomething=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            
        }
        
        [self downloadingImagesThumbNailCheck];
        
    }
    
    [_beforeImageCollectionView reloadData];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodStatus];
    
}

-(void)fetchImageDetailFromDataBaseMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetail.count; j++) {
            
            matchesImageDetail=arrAllObjImageDetail[j];
            NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"Before"]) {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,nil];
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrOfBeforeImageAll addObject:dict_ToSendLeadInfo];
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
            }
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        
    }else if (arrOfImagesDetail.count==0){
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionView addObjectsFromArray:arrOfImagess];
        
        [self downloadImages:arrOfImagess];
        
    }
}

-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation1=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation1,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImage:str:k];
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload1: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
        }
    }
    [_beforeImageCollectionView reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    if (indexx==arrOfImagenameCollewctionView.count-1) {
        
        [_beforeImageCollectionView reloadData];
        
    }
    [_beforeImageCollectionView reloadData];
}

//============================================================================
//============================================================================
#pragma mark -----------------------Load Image-------------------------------
//============================================================================
//============================================================================

- (UIImage*)loadImage :(NSString*)name :(int)indexxx {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    return image;
}

-(void)goingToPreview :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"isFromOtherPresentedView"];
        [defs synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ImagePreviewGeneralInfoAppointmentViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.statusOfWorkOrder=strWorkOrderStatuss;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaption;
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescription;
        //objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}

#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    isImageTaken=YES;
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
    
    [arrOfBeforeImageAll addObject:strImageNamess];
    [arrOfImagenameCollewctionView addObject:strImageNamess];
    [_beforeImageCollectionView reloadData];
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    [self resizeImage:chosenImage :strImageNamess];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    //imageCaption
    
    NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
    
    BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
    
    if (yesImageCaption) {
        
        [self alertViewCustom];
        
    } else {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        [self saveImageToCoreData];
        
    }
    
}


-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    
    //NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 105, [UIScreen mainScreen].bounds.size.width-20, 430)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 50)];
    
    lblCaption.text=@"Enter image caption below...";
    lblCaption.font=[UIFont systemFontOfSize:22];
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 100)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:22];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+105, viewAlertt.bounds.size.width-20, 50)];
    
    lbl.text=@"Enter image description below...";
    lbl.font=[UIFont systemFontOfSize:22];
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+55, viewAlertt.bounds.size.width-20, 200)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:22];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+435, viewAlertt.frame.size.width/2-20, 50)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    btnSave.titleLabel.font=[UIFont systemFontOfSize:22];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,50)];
    [btnDone setTitle:@"No Caption" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont systemFontOfSize:22];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        
        [self saveImageToCoreData];
        
        //        CGPoint bottomOffset = CGPointMake(0, _scrollViewGeneralInfo.contentSize.height - _scrollViewGeneralInfo.bounds.size.height);
        //        [_scrollViewGeneralInfo setContentOffset:bottomOffset animated:YES];
        
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}


-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    [self saveImageToCoreData];
    
    //    CGPoint bottomOffset = CGPointMake(0, _scrollViewGeneralInfo.contentSize.height - _scrollViewGeneralInfo.bounds.size.height);
    //    [_scrollViewGeneralInfo setContentOffset:bottomOffset animated:YES];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrOfImagenameCollewctionView.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"CellCollectionService";
    
    GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    //    cell.selected=YES;
    //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
    NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    if (image==nil) {
        
        cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
        
    } else {
        
        cell.imageBefore.image = image;
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    if (image==nil) {
        
        
        
    }else{
        
        NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        [self goingToPreview : strIndex];
        
    }
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
    [self deleteBeforeImagesBeforeSaving];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    for (int k=0; k<arrOfBeforeImageAll.count; k++)
    {
        
        if ([arrOfBeforeImageAll[k] isKindOfClass:[NSString class]]) {
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfBeforeImageAll objectAtIndex:k]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            
            NSError *error1;
            [context save:&error1];
            
        }else{
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            NSDictionary *dictData=[arrOfBeforeImageAll objectAtIndex:k];
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    //........................................................................
}

-(void)deleteBeforeImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"Before"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


- (IBAction)action_RadioTotalRepairCost:(id)sender {
    
    //Condition For Total Repair cost
    
    isTotalRepairCost=YES;
    
    [_btnRadioTotalRepairCost setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioSpecifyRepairPartnLaborCost setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [_txtTotalRepairCostNonStandard setHidden:NO];
    [_txtRepairLaborCostNonStandard setHidden:YES];
    [_txtRepairPartCostNonStandard setHidden:YES];
    [_txtRepairAmt setHidden:YES];
    
    [self calculateTotalRepairAmount:_txtRepairQty.text :_txtTotalRepairCostNonStandard.text];
}
- (IBAction)action_RadioSpecifyRepairPartnLaborCost:(id)sender {
    
    //Condition For Total Repair cost
    
    isTotalRepairCost=NO;
    
    [_btnRadioSpecifyRepairPartnLaborCost setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioTotalRepairCost setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [_txtTotalRepairCostNonStandard setHidden:YES];
    [_txtRepairLaborCostNonStandard setHidden:NO];
    [_txtRepairPartCostNonStandard setHidden:NO];
    [_txtRepairAmt setHidden:YES];
    
    NSString *strTotal = [NSString stringWithFormat:@"%d",[_txtRepairPartCostNonStandard.text intValue]+[_txtRepairLaborCostNonStandard.text intValue]];
    
    [self calculateTotalRepairAmount:_txtRepairQty.text :strTotal];
}


//Changes For Calculation At Runtime for Repair and save in Database

-(void)fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanicalAndCalculateValuesAndUpdateDB :(NSString*)strRepairIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strRepairIdToFetch];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    NSArray *arrOfRepairFetched = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrOfRepairFetched count] == 0)
    {
        
    }
    else
    {
        
        strAmountTotalRepairRunTime=@"";
        
        NSManagedObject *objTemp =arrOfRepairFetched[0];
        
        NSString *strSubWorkOrderIssueIdToFetch=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
        NSString *strIssueRepairIdToFetch=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
        NSString *strRepairMasterIdToFetch=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairMasterId"]];
        
        [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAndCalculateValuesAndUpdateDB:strSubWorkOrderIssueIdToFetch :strIssueRepairIdToFetch];
        
        [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanicalAndCalculateValuesAndUpdateDB:strIssueRepairIdToFetch :strSubWorkOrderIssueIdToFetch];
        
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictMaster=[defs valueForKey:@"MasterAllMechanical"];
        NSArray *arrOfStatus=[dictMaster valueForKey:@"RepairMasterExtSerNewDc"];
        
        NSDictionary *dictDataaRepairMasterFetched=[[NSDictionary alloc]init];
        
        for (int k=0; k<arrOfStatus.count; k++) {
            
            dictDataaRepairMasterFetched=arrOfStatus[k];
            
            NSString *strRepairMasterIdToCheck=[NSString stringWithFormat:@"%@",[dictDataaRepairMasterFetched valueForKey:@"RepairMasterId"]];
            
            if ([strRepairMasterIdToCheck isEqualToString:strRepairMasterIdToFetch]) {
                
                break;
                
            }else{
                
                dictDataaRepairMasterFetched=[[NSDictionary alloc]init];
                
            }
        }
        
        
        //dictDataaRepairMasterFetched
        
        NSString *strCostAdjustmentToUpdate;
        NSString *strPartCostAdjustmentToUpdate;
        
        if (dictDataaRepairMasterFetched.count>0) {
            
            strCostAdjustmentToUpdate=[NSString stringWithFormat:@"%@",[dictDataaRepairMasterFetched valueForKey:@"CostAdjustment"]];
            
            strPartCostAdjustmentToUpdate=[NSString stringWithFormat:@"%@",[dictDataaRepairMasterFetched valueForKey:@"PartCostAdjustment"]];
            
        }else{
            
            strCostAdjustmentToUpdate=@"";
            strPartCostAdjustmentToUpdate=@"";
            
        }
        
        //        NSString *strCostAdjustmentToUpdate=[NSString stringWithFormat:@"%@",[dictDataaRepairMasterFetched valueForKey:@"CostAdjustment"]];
        //
        //        NSString *strPartCostAdjustmentToUpdate=[NSString stringWithFormat:@"%@",[dictDataaRepairMasterFetched valueForKey:@"PartCostAdjustment"]];
        
        [objTemp setValue:strCostAdjustmentToUpdate forKey:@"costAdjustment"];
        [objTemp setValue:strPartCostAdjustmentToUpdate forKey:@"partCostAdjustment"];
        [objTemp setValue:strAmountTotalRepairRunTime forKey:@"repairAmt"];
        
        NSError *error1;
        [context save:&error1];
        
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAndCalculateValuesAndUpdateDB :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueRepairIdTofetch,strSubWorkOrderIssueIdToFetch];//subWorkOrderIssueId
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    NSArray *arrTempParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrTempParts count] == 0)
    {
        
    }
    else
    {
        
        
        for (int k=0; k<arrTempParts.count; k++) {
            
            NSManagedObject *objTemp =arrTempParts[k];
            
            NSString *strPartCode=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"partCode"]];
            
            float valueLookUpPrice=[self fetchPartDetailViaPartCode:strPartCode];
            
            float QTY=[[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]] floatValue];
            
            NSString *strCategorySysNameLocal=[self fetchPartCategorySysNameDetailViaPartCode:strPartCode];
            
            NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",valueLookUpPrice] :strCategorySysNameLocal :[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"partType"]]];
            
            float multiplier=[strMultiplier floatValue];
            
            NSString *strPartTypee=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"partType"]];
            
            // [objTemp setValue:strMultiplier forKey:@"multiplier"];
            
            if ([strPartTypee isEqualToString:@"Non-Standard"]) {
                
                [objTemp setValue:[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"unitPrice"]] forKey:@"unitPrice"];
                [objTemp setValue:[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"multiplier"]] forKey:@"multiplier"];
                
            }else{
                
                [objTemp setValue:[NSString stringWithFormat:@"%f",valueLookUpPrice] forKey:@"unitPrice"];
                [objTemp setValue:strMultiplier forKey:@"multiplier"];
                
            }
            
            NSError *error1;
            [context save:&error1];
            
            valueLookUpPrice=valueLookUpPrice*QTY*multiplier;
            
            float totall=[strAmountTotalRepairRunTime floatValue]+valueLookUpPrice;
            
            strAmountTotalRepairRunTime=[NSString stringWithFormat:@"%f",totall];
            
        }
        
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}


-(void)fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanicalAndCalculateValuesAndUpdateDB :(NSString*)strIssueRepairIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueRepairIdToFetch,strSubWorkOrderIssueIdToFetch];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    NSArray *arrTempLabor = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrTempLabor count] == 0)
    {
        
    }
    else
    {
        
        
        for (int k=0; k<arrTempLabor.count; k++) {
            
            NSDictionary *dictDataAfterHourRateToBeUsed=[[NSDictionary alloc]init];
            
            NSManagedObject *objTemp =arrTempLabor[k];
            
            if ([[objTemp valueForKey:@"isWarranty"] isEqualToString:@"1"] || [[objTemp valueForKey:@"isWarranty"] isEqualToString:@"true"] || [[objTemp valueForKey:@"isWarranty"] isEqualToString:@"True"]) {
                
                if ([[objTemp valueForKey:@"laborType"] isEqualToString:@"H"]) {
                    
                    float strStdPriceHLR=0.0;
                    float hrsHLR=0.0;
                    
                    if (!(arrOfHoursConfig.count==0)) {
                        
                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                        //Helper Price Logic
                        
                        hrsHLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]]];
                        
                        hrsHLR=hrsHLR/3600;
                        
                        strStdPriceHLR=[[dictDataHours valueForKey:@"HelperWrntyStdPrice"] floatValue];
                        
                        if (!isStandardSubWorkOrder) {
                            
                            if (isHoliday) {
                                
                                strStdPriceHLR=[[dictDataHours valueForKey:@"HolidayHelperWrntyPrice"] floatValue];
                                
                            } else {
                                
                                NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                
                                NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                
                                strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                
                                
                                NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                
                                for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                    
                                    NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                    
                                    NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                    NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                    
                                    if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                        
                                        dictDataAfterHourRateToBeUsed=dictDataAHC;
                                        break;
                                        
                                    }
                                    
                                }
                                
                                strStdPriceHLR=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperWrntyPrice"] floatValue];
                                
                            }
                            
                        }
                        
                    }
                    
                    float totalPriceHelper=hrsHLR*strStdPriceHLR;
                    
                    [objTemp setValue:[NSString stringWithFormat:@"%f",totalPriceHelper] forKey:@"laborCost"];
                    
                    NSError *error1;
                    [context save:&error1];
                    
                    float totall=[strAmountTotalRepairRunTime floatValue]+totalPriceHelper;
                    
                    strAmountTotalRepairRunTime=[NSString stringWithFormat:@"%f",totall];
                    
                    
                } else {
                    
                    float hrsLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]]];
                    
                    hrsLR=hrsLR/3600;
                    
                    float strStdPrice=0.0;
                    
                    if (!(arrOfHoursConfig.count==0)) {
                        
                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                        
                        strStdPrice=[[dictDataHours valueForKey:@"WrntyStdPrice"] floatValue];
                        
                        if (!isStandardSubWorkOrder) {
                            
                            // Change for holiday and After hours change
                            
                            if (isHoliday) {
                                
                                strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborWrntyPrice"] floatValue];
                                
                            } else {
                                
                                // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                                
                                if (strAfterHrsDuration.length==0) {
                                    
                                    strStdPrice=0.0;
                                    
                                } else {
                                    
                                    NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                    
                                    NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                    NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                    
                                    strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                    strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                    
                                    
                                    NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                    
                                    for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                        
                                        NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                        
                                        NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                        NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                        
                                        if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                            
                                            dictDataAfterHourRateToBeUsed=dictDataAHC;
                                            break;
                                            
                                        }
                                        
                                    }
                                    
                                    
                                    strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborWrntyPrice"] floatValue];
                                }
                                
                            }
                            
                            
                        }
                        
                    }
                    
                    float totalPriceLabour=hrsLR*strStdPrice;
                    
                    [objTemp setValue:[NSString stringWithFormat:@"%f",totalPriceLabour] forKey:@"laborCost"];
                    
                    NSError *error1;
                    [context save:&error1];
                    
                    float totall=[strAmountTotalRepairRunTime floatValue]+totalPriceLabour;
                    
                    strAmountTotalRepairRunTime=[NSString stringWithFormat:@"%f",totall];
                    
                }
                
            }else{
                
                if ([[objTemp valueForKey:@"laborType"] isEqualToString:@"H"]) {
                    
                    float strStdPriceHLR=0.0;
                    float hrsHLR=0.0;
                    
                    if (!(arrOfHoursConfig.count==0)) {
                        
                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                        //Helper Price Logic
                        
                        hrsHLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]]];
                        
                        hrsHLR=hrsHLR/3600;
                        
                        strStdPriceHLR=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                        
                        if (!isStandardSubWorkOrder) {
                            
                            if (isHoliday) {
                                
                                strStdPriceHLR=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];
                                
                            } else {
                                
                                NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                
                                NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                
                                strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                
                                
                                NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                
                                for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                    
                                    NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                    
                                    NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                    NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                    
                                    if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                        
                                        dictDataAfterHourRateToBeUsed=dictDataAHC;
                                        break;
                                        
                                    }
                                    
                                }
                                
                                strStdPriceHLR=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                                
                            }
                            
                        }
                        
                    }
                    
                    float totalPriceHelper=hrsHLR*strStdPriceHLR;
                    
                    [objTemp setValue:[NSString stringWithFormat:@"%f",totalPriceHelper] forKey:@"laborCost"];
                    
                    NSError *error1;
                    [context save:&error1];
                    
                    float totall=[strAmountTotalRepairRunTime floatValue]+totalPriceHelper;
                    
                    strAmountTotalRepairRunTime=[NSString stringWithFormat:@"%f",totall];
                    
                    
                } else {
                    
                    float hrsLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]]];
                    
                    hrsLR=hrsLR/3600;
                    
                    float strStdPrice=0.0;
                    
                    if (!(arrOfHoursConfig.count==0)) {
                        
                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                        
                        strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                        
                        if (!isStandardSubWorkOrder) {
                            
                            // Change for holiday and After hours change
                            
                            if (isHoliday) {
                                
                                strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                                
                            } else {
                                
                                // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                                
                                if (strAfterHrsDuration.length==0) {
                                    
                                    strStdPrice=0.0;
                                    
                                } else {
                                    
                                    NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                    
                                    NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                    NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                    
                                    strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                    strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                    
                                    
                                    NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                    
                                    for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                        
                                        NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                        
                                        NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                        NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                        
                                        if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                            
                                            dictDataAfterHourRateToBeUsed=dictDataAHC;
                                            break;
                                            
                                        }
                                        
                                    }
                                    
                                    
                                    strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborPrice"] floatValue];
                                }
                                
                            }
                            
                            
                        }
                        
                    }
                    
                    float totalPriceLabour=hrsLR*strStdPrice;
                    
                    [objTemp setValue:[NSString stringWithFormat:@"%f",totalPriceLabour] forKey:@"laborCost"];
                    
                    NSError *error1;
                    [context save:&error1];
                    
                    float totall=[strAmountTotalRepairRunTime floatValue]+totalPriceLabour;
                    
                    strAmountTotalRepairRunTime=[NSString stringWithFormat:@"%f",totall];
                    
                }
            }
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


- (IBAction)action_Quote:(id)sender {
    
    if (isJobStarted) {
        
        BOOL isNet=[global isNetReachable];
        
        if (isNet) {
            
            //            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"createQuoteiPad" bundle:nil];
            //            QuoteViewController *vc = [sb instantiateViewControllerWithIdentifier:@"QuoteViewController"];
            //            vc.objWorkOrderDetail = _objWorkOrderdetails;
            //            vc.objSubWorkOrderDetail = _objSubWorkOrderdetails;
            //            vc.strFromWhere=@"sdf";
            //            [self.navigationController pushViewController:vc animated:NO];
            //            //[self.navigationController presentViewController:vc animated:YES completion:nil];
            
            UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
            QuoteHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"QuoteHistoryViewController"];
            objSignViewController.strWorkOrderID=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"workOrderNo"]];;
            objSignViewController.objWorkOrderDetail = _objWorkOrderdetails;
            objSignViewController.objSubWorkOrderDetail = _objSubWorkOrderdetails;
            objSignViewController.strFromWhere=@"sdf";
            [self presentViewController:objSignViewController animated:YES completion:nil];
            
        } else {
            
            [global AlertMethod:Alert :ErrorInternetMsg];
            
        }
        
    } else {
        
        [global AlertMethod:Alert :alertStartJob];
        
    }
    
}


- (IBAction)action_PurchaseOrder:(id)sender {
    
//    if (isJobStarted) {
        
        BOOL isNet=[global isNetReachable];
        
        if (isNet) {
            
            [self saveImageToCoreData];
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setBool:YES forKey:@"isFromOtherPresentedView"];
            [defs synchronize];
            
            //            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
            //                                                                     bundle: nil];
            //            PurchaseOrderViewController
            //            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"PurchaseOrderViewController"];
            //            objByProductVC.strWorkOrderId=strWorkOrderId;
            //            objByProductVC.strSubWorkOrderId=_strSubWorkOrderId;
            //            objByProductVC.strWoType=_strWoType;
            //            objByProductVC.objWorkOrderDetail=_objWorkOrderdetails;
            //            objByProductVC.objSubWorkOrderDetail=_objSubWorkOrderdetails;
            //            objByProductVC.strFromWhere=@"";
            //            [self.navigationController pushViewController:objByProductVC animated:NO];
            
            UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
            PoHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"PoHistoryViewController"];
            objSignViewController.strWorkOrderNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"workOrderNo"]];
            objSignViewController.strWorkOrderId=strWorkOrderId;
            objSignViewController.strSubWorkOrderId=_strSubWorkOrderId;
            objSignViewController.strWoType=_strWoType;
            objSignViewController.objWorkOrderDetail=_objWorkOrderdetails;
            objSignViewController.objSubWorkOrderDetail=_objSubWorkOrderdetails;
            objSignViewController.strFromWhere=@"";
            [self presentViewController:objSignViewController animated:YES completion:nil];
            
        } else {
            
            [global AlertMethod:Alert :ErrorInternetMsg];
            
        }
        
        
//    } else {
//
//        [global AlertMethod:Alert :alertStartJob];
//
//    }
    
    
}


- (IBAction)action_Equipments:(id)sender {
    
    // [global AlertMethod:Alert :@"Coming Soon..."];
    //
    
    if (isJobStarted) {
        
        [self saveImageToCoreData];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalEquipmentsViewControlleriPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalEquipmentsViewControlleriPad"];
        objByProductVC.strWorkOrderId=strWorkOrderId;
        objByProductVC.strSubWorkOrderId=_strSubWorkOrderId;
        objByProductVC.objWorkOrderDetail=_objWorkOrderdetails;
        objByProductVC.objSubWorkOrderDetail=_objSubWorkOrderdetails;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
        //[self.navigationController pushViewController:objByProductVC animated:NO];
        
    } else {
        
        [global AlertMethod:Alert :alertStartJob];
        
    }
    
    
}

- (IBAction)action_AddEquipment:(id)sender{
    
    [self fetchWoEquipmentFromDB];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Core Data Fetch Methods-----------------
//============================================================================
//============================================================================


-(void)fetchWoEquipmentFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextWoEquipment = [appDelegate managedObjectContext];
    entityWoEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:contextWoEquipment];
    requestWoEquipment = [[NSFetchRequest alloc] init];
    [requestWoEquipment setEntity:entityWoEquipment];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestWoEquipment setPredicate:predicate];
    
    sortDescriptorWoEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWoEquipment = [NSArray arrayWithObject:sortDescriptorWoEquipment];
    
    [requestWoEquipment setSortDescriptors:sortDescriptorsWoEquipment];
    
    self.fetchedResultsControllerWoEquipmentService = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoEquipment managedObjectContext:contextWoEquipment sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoEquipmentService setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoEquipmentService performFetch:&error];
    arrAllObjWoEquipment = [self.fetchedResultsControllerWoEquipmentService fetchedObjects];
    if ([arrAllObjWoEquipment count] == 0)
    {
        
    }
    else
    {
        
        arrOfMechanicalWoEquipment=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjWoEquipment.count; k++){
            
            [arrOfMechanicalWoEquipment addObject:arrAllObjWoEquipment[k]];
            
        }
        
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    //[_btn_AddEquipments setTitle:@"----Select Equipment----" forState:UIControlStateNormal];
    [self hideAllTxtViewsnFields];
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    
    if (arrOfMechanicalWoEquipment.count==0) {
        [global AlertMethod:Info :NoDataAvailableee];
    }else{
        tblData.tag=106;
        [self tableLoad:tblData.tag];
    }
    
}


-(void)fetchWoEquipmentFromDBToFilterScannedEquip :(NSString *)strScannedEquip{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextWoEquipment = [appDelegate managedObjectContext];
    entityWoEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:contextWoEquipment];
    requestWoEquipment = [[NSFetchRequest alloc] init];
    [requestWoEquipment setEntity:entityWoEquipment];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestWoEquipment setPredicate:predicate];
    
    sortDescriptorWoEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWoEquipment = [NSArray arrayWithObject:sortDescriptorWoEquipment];
    
    [requestWoEquipment setSortDescriptors:sortDescriptorsWoEquipment];
    
    self.fetchedResultsControllerWoEquipmentService = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoEquipment managedObjectContext:contextWoEquipment sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoEquipmentService setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoEquipmentService performFetch:&error];
    arrAllObjWoEquipment = [self.fetchedResultsControllerWoEquipmentService fetchedObjects];
    if ([arrAllObjWoEquipment count] == 0)
    {
        
        [global AlertMethod:Alert :NoDataAvailableee];
        
    }
    else
    {
        
        arrOfMechanicalWoEquipment=[[NSMutableArray alloc]init];
        
        BOOL isResult;
        
        isResult=NO;
        
        for (int k=0; k<arrAllObjWoEquipment.count; k++){
            
            NSManagedObject *objTemp=arrAllObjWoEquipment[k];
            
            NSString *strItemNo=[objTemp valueForKey:@"itemNo"];
            
            if ([strItemNo isEqualToString:strScannedEquip]) {
                
                isResult=YES;
                
                NSManagedObject *dictData=[arrAllObjWoEquipment objectAtIndex:k];
                NSString *strItemName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"itemName"]];
                NSString *strItemCode=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"itemcode"]];
                NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"itemCustomName"]];
                
                if ([strItemCustomName isEqualToString:@"(null)"]) {
                    
                    strItemCustomName=@"";
                    
                }
                
                NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                
                if (strItemName.length>0) {
                    
                    [arrTemp addObject:strItemName];
                    
                }
                
                if (strItemCode.length>0) {
                    
                    [arrTemp addObject:strItemCode];
                    
                }
                
                if (strItemCustomName.length>0) {
                    
                    [arrTemp addObject:strItemCustomName];
                    
                }
                
                NSString *strToSet;
                
                if (arrTemp.count==1) {
                    
                    strToSet=[NSString stringWithFormat:@"Equip.Name(#):%@",[dictData valueForKey:@"ItemName"]];
                    
                } else if (arrTemp.count==2){
                    
                    strToSet=[NSString stringWithFormat:@"Equip.Name(#):%@(%@)",[dictData valueForKey:@"ItemName"],[dictData valueForKey:@"Itemcode"]];
                    
                } else if (arrTemp.count==3){
                    
                    strToSet=[NSString stringWithFormat:@"Equip.Name(#):%@(%@)-%@",[dictData valueForKey:@"ItemName"],[dictData valueForKey:@"Itemcode"],[dictData valueForKey:@"ItemCustomName"]];
                    
                }
                
                [_btn_AddEquipments setTitle:strToSet forState:UIControlStateNormal];
                
                objSelectedEquipment=dictData;
                
                break;
            }
            
        }
        
        if (isResult) {
            
        } else {
            
            [global AlertMethod:Alert :NoDataAvailableee];
            
        }
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)methodOpenScannerView{
    
    [self saveImageToCoreData];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"" forKey:@"ScannedResult"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ScannerViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ScannerViewController"];
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}
-(void)methodOpenScannerViewEquip{
    
    [self saveImageToCoreData];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"" forKey:@"ScannedResult"];
    [defs setBool:YES forKey:@"IsScannerViewEquip"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ScannerViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ScannerViewController"];
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}

-(void)goToEquipMentHistory{
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        EquipmentHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EquipmentHistoryViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    }
    
}


-(void)methodToCheckIfValuesAreNull{
    
    NSString *strValuesToCheck=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderNo"]];
    NSString *strValuesToCheck1=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWOStatus"]];
    
    if (([strValuesToCheck caseInsensitiveCompare:@"(null)"] == NSOrderedSame) || ([strValuesToCheck1 caseInsensitiveCompare:@"(null)"] == NSOrderedSame)) {
        
        [global AlertMethod:Alert :@"Something went wrong. Please process again."];
        
        NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
        
        NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
        
        if ([strAppointmentFlow isEqualToString:@"New"])
        {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
            AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }
        else
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
            AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
        }
        
    }
}

- (IBAction)action_ScanEquipBarcode:(id)sender {
    
    [self methodOpenScannerViewEquip];
    
}

-(void)aisehi{
    
    //changes nothing commit k liye
}

- (IBAction)action_ViewMoreNotesDesc:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    NSManagedObject *dictDataNotes=arrOfSubWorkServiceNotes[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"note"]];
    [global AlertMethod:@"Note Description" :strJobDesc];
    
}

- (IBAction)action_EmpTimeSheet:(id)sender {
    
    [self goToEmpTimeSheet];
    
}

- (IBAction)action_InvoiceView:(id)sender {
    
    if (isCompletedStatusMechanical) {
        
        [self stopLoaderAndGoToInvoice];
        
    }else{
        
        [self saveTechComments];
        
        if (isClientApproved) {
            
            if ([_btnStartnStopJOB.titleLabel.text isEqualToString:@"Pause Job"] || [_btnStartnStopJOB.currentTitle isEqualToString:@"Pause Job"]) {
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
                
                [self performSelector:@selector(updatingTimeAfterSometime) withObject:nil afterDelay:0.1];
                
            } else {
                
                [self stopLoaderAndGoToInvoice];
                
            }
            
        } else {
            
            [global AlertMethod:Alert :alertClientApproval];
            
        }
        
    }
    
}

-(void)stopLoaderAndGoToInvoice
{
    
    [self saveImageToCoreData];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:strNotificationTypeName
                                                  object:nil];
    
    [DejalBezelActivityView removeView];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    MechanicalInvoiceViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalInvoiceViewController"];
    objByProductVC.strWorlOrderId=strWorkOrderId;
    objByProductVC.strSubWorkOderId=_strSubWorkOrderId;
    objByProductVC.strWoType=_strWoType;
    objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderdetails;
    objByProductVC.objWorkOrderdetails=_objWorkOrderdetails;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)updatingTimeAfterSometime{
    
    [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutOnComplete];
    
}

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutOnComplete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strID=[defs valueForKey:@"subWOActualHourId"];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strID];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
    }
    else
    {
        [jobMinTimer invalidate];
        
        matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[0];
        
        NSString *strCureentTimeToset=[global strCurrentDateFormattedForMechanical];
        
        [matchesSubWorkOrderActualHrs setValue:strCureentTimeToset forKey:@"timeOut"];
        //[matchesSubWorkOrderActualHrs setValue:@"Running" forKey:@"status"];
        [matchesSubWorkOrderActualHrs setValue:strCureentTimeToset forKey:@"mobileTimeOut"];
        
        [matchesSubWorkOrderActualHrs setValue:ReasonRunning forKey:@"reason"];
        [matchesSubWorkOrderActualHrs setValue:@"" forKey:@"actHrsDescription"];
        
        BOOL isNetReachable=[global isNetReachable];
        
        if (isNetReachable) {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
            
            strNotificationTypeName=@"MechanicalActualHrsSyncStartFR3";
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoaderAndGoToInvoice) name:strNotificationTypeName object:nil];
            
            SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
            
            [objSync syncMechanicalActualHours:strWorkOrderId :strSubWorkOrderIdGlobal :strID :strNotificationTypeName];
            
            //[self stopLoaderAndGoToInvoice];
            
        }else{
            
            [self stopLoaderAndGoToInvoice];
            
        }
        
        NSError *error2;
        [context save:&error2];
        
        [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
        
        // Create dynamic view for employee time sheet slot wise.....
        arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
        arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
        
        arrOfGlobalDynamicEmpSheetFinal=[global createEmpSheetDataInBackground:strWorkOrderId :strSubWorkOrderIdGlobal :arrOfHoursConfig :strEmpID :strEmployeeNoLoggedIn :strEmpName :@"StartRepairBackground" :strCureentTimeToset];
        
        //arrOfHeaderTitleForSlots=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"" forKey:@"subWOActualHourId"];
        [defs synchronize];
        
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}

//changes for employee sheet

-(NSArray*)fetchSubWorkOrderActualHrsFromDataBaseForEmployeeTimeSheet{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strID=[defs valueForKey:@"subWOActualHourId"];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strID];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    NSArray *arrActualHours = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
    
    return arrActualHours;
    
}


//Goto Emp Sheet

-(void)goToEmpTimeSheet{
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    EmpTimeSheetViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EmpTimeSheetViewController"];
    objSignViewController.strWorkOrderId=strWorkOrderId;
    objSignViewController.strSubWorkOrderIdGlobal=strSubWorkOrderIdGlobal;
    objSignViewController.arrOfHoursConfig=arrOfHoursConfig;
    
    if (isCompletedStatusMechanical) {
        
        objSignViewController.strWorkOrderStatus=@"Complete";
        
    } else {
        
        objSignViewController.strWorkOrderStatus=@"InComplete";
        
    }
    
    objSignViewController.strAfterHrsDuration=strAfterHrsDuration;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

-(void)saveTechComments{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    [_objWorkOrderdetails setValue:_txtViewTechComment.text forKey:@"technicianComment"];
   // [_objSubWorkOrderdetails setValue:@"true" forKey:@"isIncludeDetailOnInvoice"];

    NSError *error2;
    [context save:&error2];
    
}

- (IBAction)action_Mechanic:(id)sender {
    
    isMechanic=YES;
    [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
}

- (IBAction)action_Helper:(id)sender {
    
    isMechanic=NO;
    [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
}

-(void)calculateTotalRepairAmount :(NSString*)strQty :(NSString*)strAmount{
    
    _lblTotalAmount.text=[NSString stringWithFormat:@"%.02f",[strAmount floatValue]*[strQty floatValue]];
    
}
@end
