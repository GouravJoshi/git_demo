//
//  EmpTimeSheetViewController.h
//  DPS
//
//  Created by Saavan Patidar on 05/07/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface EmpTimeSheetViewController : UIViewController<UIScrollViewDelegate,NSFetchedResultsControllerDelegate,UITextFieldDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entitySubWOCompleteTimeExtSerDcs,*entitySubWoEmployeeWorkingTimeExtSerDcs,*entityTempSubWoEmployeeWorkingTimeExtSerDcs,*entityMechanicalSubWorkOrderActualHrs;
    
    NSManagedObjectContext *context;
    
    NSFetchRequest *requestSubWOCompleteTimeExtSerDcs,*requestSubWoEmployeeWorkingTimeExtSerDcs,*requestTempSubWoEmployeeWorkingTimeExtSerDcs,*requestSubWorkOrderActualHrs;
    
    NSSortDescriptor *sortDescriptorSubWOCompleteTimeExtSerDcs,*sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorSubWorkOrderActualHrs;
    
    NSArray *sortDescriptorsSubWOCompleteTimeExtSerDcs,*sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorsSubWorkOrderActualHrs;
    
    NSManagedObject *matchesSubWorkOrderIssuesRepairLabour,*matchesSubWOCompleteTimeExtSerDcs,*matchesSubWoEmployeeWorkingTimeExtSerDcs;
    
    NSArray *arrAllObjSubWOCompleteTimeExtSerDcs,*arrAllObjSubWoEmployeeWorkingTimeExtSerDcs,*arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs;
    
    NSEntityDescription *entityMechanicalSubWorkOrderHelper;

}
- (IBAction)action_Back:(id)sender;
- (IBAction)action_SaveContinue:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewEmpTimeSheet;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWOCompleteTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs;
@property (strong, nonatomic) NSArray *arrOfHoursConfig;
@property (strong, nonatomic) NSString *strSubWorkOrderIdGlobal;
@property (strong, nonatomic) NSString *strWorkOrderId;
@property (strong, nonatomic) NSString *strWorkOrderStatus;
@property (strong, nonatomic) NSString *strAfterHrsDuration;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderActualHrs;
@property (strong, nonatomic) IBOutlet UIView *viewBackgroundAddEmpWorkingTime;
@property (weak, nonatomic) IBOutlet UIButton *btnHelper;
@property (weak, nonatomic) IBOutlet UILabel *lblTechName;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectHelper;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewAddEmpWorkingTime;
- (IBAction)action_SaveAddEmpWorkingTime:(id)sender;
- (IBAction)action_CancelAddEmpWorkingTime:(id)sender;
- (IBAction)action_SelectHelper:(id)sender;
- (IBAction)action_AddEmpWorkingTimeView:(id)sender;
- (IBAction)action_HelperOrtech:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblHelperTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnAddEmpWorkingTime;

@end
