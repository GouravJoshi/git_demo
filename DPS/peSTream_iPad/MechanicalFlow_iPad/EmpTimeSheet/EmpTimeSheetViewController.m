//
//  EmpTimeSheetViewController.m
//  DPS
//
//  Created by Saavan Patidar on 05/07/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

// tag From 99999999 are for adding Employee Working Time

#import "EmpTimeSheetViewController.h"
#import "AllImportsViewController.h"
#import "DPS-Swift.h"
@protocol DatePickerProtocol;
@protocol PopUpDelegate;

@interface EmpTimeSheetViewController ()<DatePickerProtocol,PopUpDelegate>
{
    Global *global;
    NSString * strEmpID,*strCompanyKey,*strEmpName,*strEmployeeNoLoggedIn,*strSubWoActualHrIdGlobal,*strHelperIdGlobal;
    NSMutableArray *arrOfTimeSlotsEmployeeWise,*arrOfHeaderTitleForSlots,*arrOfEmployeesAssignedInSubWorkOrder,*arrOfWorkingDate,*arrOfGlobalDynamicEmpSheetFinal,*arrOfGlobalSubWOCompleteTimeExtSerDcs,*arrOfTimeSlotsEmployeeWiseTempNew,*arrOfHeaderTitleForSlotsTempNew,*arrOfEmployeesAssignedInSubWorkOrderTempNew,*arrOfWorkingDateTempNew,*arrOfAddEmpWorkingTimeDuration;
    BOOL isCompletedStatusMechanical, isHelperGlobal;
    NSMutableDictionary *dictSubWorkOrderEmployeeWorkingTime;
    UIButton *btnTitleWorkingDate;
    
}

@end

@implementation EmpTimeSheetViewController
@synthesize scrollViewEmpTimeSheet,strSubWorkOrderIdGlobal,strWorkOrderId,arrOfHoursConfig,strWorkOrderStatus,strAfterHrsDuration;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    global = [[Global alloc] init];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmployeeNoLoggedIn=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];

    if ([strWorkOrderStatus isEqualToString:@"Complete"]) {
        
        isCompletedStatusMechanical=YES;
        
    } else {
        
        isCompletedStatusMechanical=NO;
        
    }
    
    //[self performSelector:@selector(fetchData) withObject:nil afterDelay:7.0];
    
    [self fetchEmployeeTimeSheetDataFromDb:strWorkOrderId :strSubWorkOrderIdGlobal];
    // Do any additional setup after loading the view.
    
    NSArray *arrActualHoursTemp=[global fetchSubWorkOrderActualHrsFromDataBaseForEmployeeTimeSheet :strWorkOrderId :strSubWorkOrderIdGlobal];
    
    /*
    BOOL isTimeOut = false;
    
    if (arrActualHoursTemp.count>0) {
        
        for (int k=0; k<arrActualHoursTemp.count; k++) {
            
            NSManagedObject *objTemp=arrActualHoursTemp[0];
            NSLog(@"actual hours=====%@",objTemp);
            
            NSString *strTimeOut = [NSString stringWithFormat:@"%@",[objTemp valueForKey:@"timeOut"]];
            
            if (strTimeOut.length==0) {
                
                strSubWoActualHrIdGlobal = [NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWOActualHourId"]];
                
                isTimeOut = true;
                
                break;
                
            }
            
        }
        
    }
    
    NSLog(@"strSubWoActualHrIdGlobal = = = %@",strSubWoActualHrIdGlobal);
    */
    
    if (isCompletedStatusMechanical) {
        
        [_btnAddEmpWorkingTime setHidden:YES];
        
    } else {
        
        [_btnAddEmpWorkingTime setHidden:NO];
        
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
//============================================================================
#pragma mark- ----------------Button Action Methods----------------
//============================================================================
//============================================================================

- (IBAction)action_Back:(id)sender {
    
    [self backMethod];
    
}

- (IBAction)action_SaveContinue:(id)sender {
    
    if (!isCompletedStatusMechanical) {
        
        [self saveEmployeeTimeSheetSlotWise:arrOfGlobalDynamicEmpSheetFinal :@"Save"];

        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];

        [self backMethod];

    }else{
        
        BOOL isNetReachable=[global isNetReachable];
        
        if (isNetReachable) {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Submitting please wait..."];
            
            [self saveEmployeeTimeSheetSlotWise:arrOfGlobalDynamicEmpSheetFinal :@"Sync"];
            
        } else {
            
            [global displayAlertController:Alert :ErrorInternetMsg :self];
            
        }
        
    }
    
    //[self backMethod];
    
}


//============================================================================
//============================================================================
#pragma mark- ----------------Methods----------------
//============================================================================
//============================================================================

-(void)fetchData{
    
    [self fetchEmployeeTimeSheetDataFromDb:strWorkOrderId :strSubWorkOrderIdGlobal];
    
}

-(void)backMethod{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)fetchEmployeeTimeSheetDataFromDb :(NSString*)strWorkOrderNew :(NSString*)strSubWorkOrderNew{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    requestSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestSubWoEmployeeWorkingTimeExtSerDcs setEntity:entitySubWoEmployeeWorkingTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderNew,strSubWorkOrderNew];
    
    [requestSubWoEmployeeWorkingTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:YES];
    sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs];
    
    [requestSubWoEmployeeWorkingTimeExtSerDcs setSortDescriptors:sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs];
    
    self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWoEmployeeWorkingTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs performFetch:&error];
    arrAllObjSubWoEmployeeWorkingTimeExtSerDcs = [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs fetchedObjects];
    
    if ([arrAllObjSubWoEmployeeWorkingTimeExtSerDcs count] == 0)
    {
        
        NSMutableArray *arrTemporary=[self fetchTempEmployeeTimeSheetDataFromDb:strWorkOrderId :strSubWorkOrderIdGlobal];
        
        if (arrTemporary.count>0) {
            
            [self logicAfterFetchingFromDb:arrTemporary :@"null"];
            
        }
        
    }
    else
    {
        
        [self logicAfterFetchingFromDb:arrAllObjSubWoEmployeeWorkingTimeExtSerDcs :@"ValuePresent"];
        
        /*
         arrOfTimeSlotsEmployeeWise=[[NSMutableArray alloc]init];
         arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
         arrOfEmployeesAssignedInSubWorkOrder=[[NSMutableArray alloc]init];
         arrOfWorkingDate=[[NSMutableArray alloc]init];
         
         NSMutableArray *arrWorkingDateTemp=[[NSMutableArray alloc]init];
         
         [arrOfHeaderTitleForSlots addObject:@"Standard"];
         
         for (int k=0; k<arrAllObjSubWoEmployeeWorkingTimeExtSerDcs.count; k++) {
         
         NSManagedObject *objTemp=arrAllObjSubWoEmployeeWorkingTimeExtSerDcs[k];
         
         NSArray *keys = [[[objTemp entity] attributesByName] allKeys];
         NSDictionary *dict = [objTemp dictionaryWithValuesForKeys:keys];
         
         NSDictionary *dictObjTemp=dict;
         
         NSMutableDictionary *dictObjMutableTemp=[[NSMutableDictionary alloc]init];
         
         [dictObjMutableTemp addEntriesFromDictionary:dictObjTemp];
         
         [arrOfTimeSlotsEmployeeWise addObject:dictObjMutableTemp];
         
         NSString *strWorkingDate=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingDate"]];
         NSString *strSubWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderActualHourId"]];
         
         if (![arrWorkingDateTemp containsObject:strSubWorkOrderActualHourId]) {
         
         [arrOfWorkingDate addObject:objTemp];
         [arrWorkingDateTemp addObject:strSubWorkOrderActualHourId];
         
         }
         
         NSString *strworkingType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingType"]];
         NSString *strTitleSlotsComing=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"timeSlotTitle"]];
         
         NSString *strTitleToSet;
         if ([strworkingType isEqualToString:@"1"]) {
         strTitleToSet=@"Standard";
         } else if ([strworkingType isEqualToString:@"2"]){
         strTitleToSet=strTitleSlotsComing;
         
         if(strTitleToSet.length==0){
         
         strTitleToSet=@"N/A";
         
         }
         
         if (![arrOfHeaderTitleForSlots containsObject:strTitleToSet]) {
         
         [arrOfHeaderTitleForSlots addObject:strTitleToSet];
         
         }
         
         } else if ([strworkingType isEqualToString:@"3"]){
         strTitleToSet=@"Holiday";
         }
         
         
         NSString *strEmployeesAssignedInSubWorkOrder=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
         
         if (![arrOfEmployeesAssignedInSubWorkOrder containsObject:strEmployeesAssignedInSubWorkOrder]) {
         
         [arrOfEmployeesAssignedInSubWorkOrder addObject:strEmployeesAssignedInSubWorkOrder];
         
         }
         
         }
         
         [arrOfHeaderTitleForSlots addObject:@"Holiday"];
         
         
         arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
         arrOfHeaderTitleForSlots=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
         
         NSMutableArray *arrTemporary=[self fetchTempEmployeeTimeSheetDataFromDb:strWorkOrderId :strSubWorkOrderIdGlobal];
         
         if (arrTemporary.count>0) {
         
         [arrOfTimeSlotsEmployeeWise addObjectsFromArray:arrTemporary];
         
         }
         
         NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
         
         NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
         NSMutableArray *arrOfEmpIdss=[[NSMutableArray alloc]init];
         arrOfEmpIdss=arrOfEmployeesAssignedInSubWorkOrder;
         
         arrOfEmployeesAssignedInSubWorkOrder=[[NSMutableArray alloc]init];
         
         if ([dictEmployeeList isKindOfClass:[NSString class]]) {
         
         
         } else {
         
         if (!(dictEmployeeList.count==0)) {
         NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
         for (int k=0; k<arrOfEmployee.count; k++) {
         NSDictionary *dictDatata=arrOfEmployee[k];
         NSString *strEmpIdsss=[NSString stringWithFormat:@"%@",[dictDatata valueForKey:@"EmployeeNo"]];
         if ([arrOfEmpIdss containsObject:strEmpIdsss]) {
         [arrOfEmployeesAssignedInSubWorkOrder addObject:dictDatata];
         }
         }
         }
         }
         
         NSMutableArray *arrOfMainTempEmployeeTimeSlot=[[NSMutableArray alloc]init];
         
         // Logic for creating final Array list for Employee Time Slot Wise
         
         for (int q=0; q<arrOfEmployeesAssignedInSubWorkOrder.count; q++) {
         
         NSMutableDictionary *dictOfMainObj=[[NSMutableDictionary alloc]init];
         
         NSDictionary *dictObjEmp=arrOfEmployeesAssignedInSubWorkOrder[q];
         
         NSString *strEmployeeNoInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"EmployeeNo"]];
         NSString *strEmployeeNameInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"FullName"]];
         
         [dictOfMainObj setValue:strEmployeeNoInsideLoop forKey:@"EmployeeNo"];
         [dictOfMainObj setValue:strEmployeeNameInsideLoop forKey:@"EmployeeName"];
         
         if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoLoggedIn]) {
         
         [dictOfMainObj setValue:@"No" forKey:@"Helper"];
         
         
         } else {
         
         [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
         
         }
         
         NSMutableArray *arrOfTempEmployeeListFiltered=[[NSMutableArray alloc]init];
         
         for (int q1=0; q1<arrOfWorkingDate.count; q1++) {
         
         NSMutableDictionary *dictOfMainObjDateWise=[[NSMutableDictionary alloc]init];
         
         NSDictionary *dictObjWorkingDate=arrOfWorkingDate[q1];
         
         NSString *strSubWorkOrderActualHourIdInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"subWorkOrderActualHourId"]];
         NSString *strWorkingDateInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"workingDate"]];
         
         //[dictOfMainObj setValue:strSubWorkOrderActualHourIdInsideLoop forKey:@"ActualHrId"];
         [dictOfMainObjDateWise setValue:strWorkingDateInsideLoop forKey:@"WorkingDate"];
         
         NSMutableArray *arrOfTempDateWiseObj=[[NSMutableArray alloc]init];
         
         for (int q2=0; q2<arrOfHeaderTitleForSlots.count; q2++) {
         
         NSString *strHeaderTitleInsideLoop=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[q2]];
         
         NSMutableDictionary *dictTemoObjEmployeeWiseSlots=[[NSMutableDictionary alloc]init];
         
         for (int q3=0; q3<arrOfTimeSlotsEmployeeWise.count; q3++) {
         
         NSDictionary *dictObjTimeSlotsEmployeeWise=arrOfTimeSlotsEmployeeWise[q3];
         
         NSString *strEmployeeNoTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"employeeNo"]];
         NSString *strWorkingDateTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingDate"]];
         NSString *strActualHrIdTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"subWorkOrderActualHourId"]];
         NSString *strWorkingTypeTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingType"]];
         // NSString *strtimeSlotTitleEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlotTitle"]];
         NSString *strtimeSlotTitleEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlot"]];
         
         
         
         if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Standard"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"]) {
         
         [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
         
         }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Holiday"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
         
         [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
         
         }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:strtimeSlotTitleEmployeeWiseInsideLoop] &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"] &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
         
         [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
         
         }
         
         }
         
         // if dictTemoObjEmployeeWiseSlots is null to apna pura data banaunga mai
         
         if ([dictTemoObjEmployeeWiseSlots count] < 1) {
         
         NSString *strDateNew =[global strCurrentDate];
         
         [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"actualAmt"];
         [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"actualDurationInMin"];
         [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"billableAmt"];
         [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"billableDurationInMin"];
         [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"createdBy"];
         [dictTemoObjEmployeeWiseSlots setObject:@"Mobile" forKey:@"createdByDevice"];
         [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"createdDate"];
         //[dictTemoObjEmployeeWiseSlots setObject:strEmployeeNameInsideLoop forKey:@"employeeName"];
         [dictTemoObjEmployeeWiseSlots setObject:strEmployeeNoInsideLoop forKey:@"employeeNo"];
         [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isActive"];
         [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isApprove"];
         [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"modifiedBy"];
         [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"modifiedDate"];
         [dictTemoObjEmployeeWiseSlots setObject:[global getReferenceNumber] forKey:@"subWOEmployeeWorkingTimeId"];
         [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderActualHourIdInsideLoop forKey:@"subWorkOrderActualHourId"];
         [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderIdGlobal forKey:@"subWorkOrderId"];
         [dictTemoObjEmployeeWiseSlots setObject:strWorkingDateInsideLoop forKey:@"workingDate"];
         [dictTemoObjEmployeeWiseSlots setObject:strWorkOrderId forKey:@"workorderId"];
         
         if ([strHeaderTitleInsideLoop isEqualToString:@"Standard"]) {
         
         [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
         [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
         [dictTemoObjEmployeeWiseSlots setObject:@"1" forKey:@"workingType"];
         
         } else if ([strHeaderTitleInsideLoop isEqualToString:@"Holiday"]){
         
         [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
         [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
         [dictTemoObjEmployeeWiseSlots setObject:@"3" forKey:@"workingType"];
         
         } else{
         
         NSString *strSlotsToSet;
         
         if (arrOfHoursConfig.count>0) {
         
         NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
         
         NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
         
         for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
         
         NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
         NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
         
         if ([strHeaderTitleInsideLoop isEqualToString:strTitle]) {
         
         strSlotsToSet=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
         
         break;
         }
         }
         }
         
         if (strSlotsToSet.length==0) {
         
         strSlotsToSet=@"N/A";
         
         }
         
         [dictTemoObjEmployeeWiseSlots setObject:strSlotsToSet forKey:@"timeSlot"];
         [dictTemoObjEmployeeWiseSlots setObject:strHeaderTitleInsideLoop forKey:@"timeSlotTitle"];
         [dictTemoObjEmployeeWiseSlots setObject:@"2" forKey:@"workingType"];
         
         }
         
         }
         
         [arrOfTempDateWiseObj addObject:dictTemoObjEmployeeWiseSlots];
         
         }
         
         
         [dictOfMainObjDateWise setValue:arrOfTempDateWiseObj forKey:@"DateWise"];
         
         [arrOfTempEmployeeListFiltered addObject:dictOfMainObjDateWise];
         
         }
         [dictOfMainObj setValue:arrOfTempEmployeeListFiltered forKey:@"DateWiseData"];
         [arrOfMainTempEmployeeTimeSlot addObject:dictOfMainObj];
         //[arrOfMainTempEmployeeTimeSlot addObject:arrOfTempEmployeeListFiltered];
         
         }
         
         NSError *errorNew = nil;
         NSData *json;
         NSString *jsonString;
         // Dictionary convertable to JSON ?
         if ([NSJSONSerialization isValidJSONObject:arrOfMainTempEmployeeTimeSlot])
         {
         // Serialize the dictionary
         json = [NSJSONSerialization dataWithJSONObject:arrOfMainTempEmployeeTimeSlot options:NSJSONWritingPrettyPrinted error:&errorNew];
         
         // If no errors, let's view the JSON
         if (json != nil && errorNew == nil)
         {
         jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
         NSLog(@"Employee Time Sheet arrOfMainTempEmployeeTimeSlot Json object final to set on view: %@", jsonString);
         }
         }
         
         arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
         [arrOfGlobalDynamicEmpSheetFinal addObjectsFromArray:arrOfMainTempEmployeeTimeSlot];
         
         [self createDynamicEmpSheet:arrOfGlobalDynamicEmpSheetFinal :arrOfHeaderTitleForSlots];
         
         
         */
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}


-(NSMutableArray*)fetchTempEmployeeTimeSheetDataFromDbIfMainTableIsNull :(NSString*)strWorkOrderNew :(NSString*)strSubWorkOrderNew{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTempSubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"TempSubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    requestTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setEntity:entityTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderNew,strSubWorkOrderNew];
    
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:YES];
    sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setSortDescriptors:sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTempSubWoEmployeeWorkingTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs performFetch:&error];
    arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs = [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs fetchedObjects];
    
    if ([arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs count] == 0)
    {
        
        
    }
    else
    {
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs;
}

-(void)logicAfterFetchingFromDb :(NSArray*)arrData :(NSString*)strType{
    
    arrOfTimeSlotsEmployeeWise=[[NSMutableArray alloc]init];
    arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
    arrOfEmployeesAssignedInSubWorkOrder=[[NSMutableArray alloc]init];
    arrOfWorkingDate=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrWorkingDateTemp=[[NSMutableArray alloc]init];
    
    [arrOfHeaderTitleForSlots addObject:@"Standard"];
    
    NSMutableArray *arrTempForWorkingDate=[[NSMutableArray alloc]init];
    
    [arrTempForWorkingDate addObjectsFromArray:arrData];
    
    if (![strType isEqualToString:@"null"]) {
        
        NSMutableArray *arrTemporary=[self fetchTempEmployeeTimeSheetDataFromDb:strWorkOrderId :strSubWorkOrderIdGlobal];
        
        if (arrTemporary.count>0) {
            
            // [arrOfTimeSlotsEmployeeWise addObjectsFromArray:arrTemporary];
            [arrTempForWorkingDate addObjectsFromArray:arrTemporary];
        }
        
    }
    
    for (int k=0; k<arrTempForWorkingDate.count; k++) {
        
        NSManagedObject *objTemp=arrTempForWorkingDate[k];
        
        if ([objTemp isKindOfClass:[NSManagedObject class]]) {
            
            NSArray *keys = [[[objTemp entity] attributesByName] allKeys];
            NSDictionary *dict = [objTemp dictionaryWithValuesForKeys:keys];
            
            NSDictionary *dictObjTemp=dict;
            
            NSMutableDictionary *dictObjMutableTemp=[[NSMutableDictionary alloc]init];
            
            [dictObjMutableTemp addEntriesFromDictionary:dictObjTemp];
            
            [arrOfTimeSlotsEmployeeWise addObject:dictObjMutableTemp];
            
        } else {
            
            [arrOfTimeSlotsEmployeeWise addObject:objTemp];
            
        }
        
        
        NSString *strWorkingDate=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingDate"]];
        NSString *strSubWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderActualHourId"]];
        
        if (![arrWorkingDateTemp containsObject:strSubWorkOrderActualHourId]) {
            
            [arrOfWorkingDate addObject:objTemp];
            [arrWorkingDateTemp addObject:strSubWorkOrderActualHourId];
            
        }
        
        NSString *strworkingType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingType"]];
        NSString *strTitleSlotsComing=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"timeSlotTitle"]];
        
        NSString *strTitleToSet;
        if ([strworkingType isEqualToString:@"1"]) {
            strTitleToSet=@"Standard";
        } else if ([strworkingType isEqualToString:@"2"]){
            strTitleToSet=strTitleSlotsComing;
            
            if(strTitleToSet.length==0){
                
                strTitleToSet=@"N/A";
                
            }
            
            if (![arrOfHeaderTitleForSlots containsObject:strTitleToSet]) {
                
                [arrOfHeaderTitleForSlots addObject:strTitleToSet];
                
            }
            
        } else if ([strworkingType isEqualToString:@"3"]){
            strTitleToSet=@"Holiday";
        }
        
        
        NSString *strEmployeesAssignedInSubWorkOrder=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
        
        if (![arrOfEmployeesAssignedInSubWorkOrder containsObject:strEmployeesAssignedInSubWorkOrder]) {
            
            [arrOfEmployeesAssignedInSubWorkOrder addObject:strEmployeesAssignedInSubWorkOrder];
            
        }
        
    }
    
    [arrOfHeaderTitleForSlots addObject:@"Holiday"];
    
    arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
    arrOfHeaderTitleForSlots=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
    
    /*
     if (![strType isEqualToString:@"null"]) {
     
     NSMutableArray *arrTemporary=[self fetchTempEmployeeTimeSheetDataFromDb:_strWorlOrderId :_strSubWorkOderId];
     
     if (arrTemporary.count>0) {
     
     [arrOfTimeSlotsEmployeeWise addObjectsFromArray:arrTemporary];
     
     }
     
     }
     */
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
    NSMutableArray *arrOfEmpIdss=[[NSMutableArray alloc]init];
    arrOfEmpIdss=arrOfEmployeesAssignedInSubWorkOrder;
    
    arrOfEmployeesAssignedInSubWorkOrder=[[NSMutableArray alloc]init];
    
    if ([dictEmployeeList isKindOfClass:[NSString class]]) {
        
        
    } else {
        
        if (!(dictEmployeeList.count==0)) {
            NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
            for (int k=0; k<arrOfEmployee.count; k++) {
                NSDictionary *dictDatata=arrOfEmployee[k];
                NSString *strEmpIdsss=[NSString stringWithFormat:@"%@",[dictDatata valueForKey:@"EmployeeNo"]];
                if ([arrOfEmpIdss containsObject:strEmpIdsss]) {
                    [arrOfEmployeesAssignedInSubWorkOrder addObject:dictDatata];
                }
            }
        }
    }
    
    
    
    NSMutableArray *arrOfMainTempEmployeeTimeSlot=[[NSMutableArray alloc]init];
    
    // Logic for creating final Array list for Employee Time Slot Wise
    
    for (int q=0; q<arrOfEmployeesAssignedInSubWorkOrder.count; q++) {
        
        NSMutableDictionary *dictOfMainObj=[[NSMutableDictionary alloc]init];
        
        NSDictionary *dictObjEmp=arrOfEmployeesAssignedInSubWorkOrder[q];
        
        NSString *strEmployeeNoInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"EmployeeNo"]];
        NSString *strEmployeeNameInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"FullName"]];
        
        [dictOfMainObj setValue:strEmployeeNoInsideLoop forKey:@"EmployeeNo"];
        [dictOfMainObj setValue:strEmployeeNameInsideLoop forKey:@"EmployeeName"];
        
        // Change for Labor Type
        
        if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoLoggedIn]) {
            
            BOOL isMechanicLocal = [global isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderIdGlobal];
            
            if (isMechanicLocal) {
                
                [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                
            } else {
                
                [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                
            }
            
            //[dictOfMainObj setValue:@"No" forKey:@"Helper"];
            
            
        } else {
            
            BOOL isMechanicLocal = [global isMechanicType:strWorkOrderId :strSubWorkOrderIdGlobal :strEmployeeNoInsideLoop];
            
            if (isMechanicLocal) {
                
                [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                
            } else {
                
                [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                
            }
            
            //[dictOfMainObj setValue:@"Yes" forKey:@"Helper"];

        }
        
        NSMutableArray *arrOfTempEmployeeListFiltered=[[NSMutableArray alloc]init];
        
        for (int q1=0; q1<arrOfWorkingDate.count; q1++) {
            
            NSMutableDictionary *dictOfMainObjDateWise=[[NSMutableDictionary alloc]init];
            
            NSDictionary *dictObjWorkingDate=arrOfWorkingDate[q1];
            
            NSString *strSubWorkOrderActualHourIdInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"subWorkOrderActualHourId"]];
            NSString *strWorkingDateInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"workingDate"]];
            
            //[dictOfMainObj setValue:strSubWorkOrderActualHourIdInsideLoop forKey:@"ActualHrId"];
            [dictOfMainObjDateWise setValue:strWorkingDateInsideLoop forKey:@"WorkingDate"];
            
            NSMutableArray *arrOfTempDateWiseObj=[[NSMutableArray alloc]init];
            
            for (int q2=0; q2<arrOfHeaderTitleForSlots.count; q2++) {
                
                NSString *strHeaderTitleInsideLoop=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[q2]];
                
                NSMutableDictionary *dictTemoObjEmployeeWiseSlots=[[NSMutableDictionary alloc]init];
                
                for (int q3=0; q3<arrOfTimeSlotsEmployeeWise.count; q3++) {
                    
                    NSDictionary *dictObjTimeSlotsEmployeeWise=arrOfTimeSlotsEmployeeWise[q3];
                    
                    NSString *strEmployeeNoTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"employeeNo"]];
                    NSString *strWorkingDateTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingDate"]];
                    NSString *strActualHrIdTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"subWorkOrderActualHourId"]];
                    NSString *strWorkingTypeTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingType"]];
                    NSString *strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlotTitle"]];
                    NSString *strtimeSlotTitleEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlot"]];
                    
                    BOOL isMatchecd = [global isSlotMatched:strtimeSlotTitleEmployeeWiseInsideLoop :strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3 :strHeaderTitleInsideLoop];

                    
                    if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Standard"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"]) {
                        
                        [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];

                        /*
                        if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        } else {
                            
                            NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableDurationInMin"]];
                            
                            NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableDurationInMin"]];
                            
                            float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                            float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                            float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                            float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                            
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                         
                        }
                         */
                        
                    }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Holiday"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                        
                        [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];

                        /*
                        if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        } else {
                            
                            NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableDurationInMin"]];
                            
                            NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableDurationInMin"]];
                            
                            float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                            float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                            float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                            float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                            
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                            
                        }
                         */
                        
                    }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && isMatchecd &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"] &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                        
                        [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];

                        /*
                        if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        } else {
                            
                            NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableDurationInMin"]];
                            
                            NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableDurationInMin"]];
                            
                            float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                            float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                            float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                            float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                            
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                            
                        }
                        */
                        
                    }
                    
                }
                
                // if dictTemoObjEmployeeWiseSlots is null to apna pura data banaunga mai
                
                if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                    
                    NSString *strDateNew =[global strCurrentDate];
                    
                    [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"actualAmt"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"actualDurationInMin"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"billableAmt"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"billableDurationInMin"];
                    [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"createdBy"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"Mobile" forKey:@"createdByDevice"];
                    [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"createdDate"];
                    //[dictTemoObjEmployeeWiseSlots setObject:strEmployeeNameInsideLoop forKey:@"employeeName"];
                    [dictTemoObjEmployeeWiseSlots setObject:strEmployeeNoInsideLoop forKey:@"employeeNo"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"True" forKey:@"isActive"];
                    [dictTemoObjEmployeeWiseSlots setObject:@"True" forKey:@"isApprove"];
                    [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"modifiedBy"];
                    [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"modifiedDate"];
                    [dictTemoObjEmployeeWiseSlots setObject:[global getReferenceNumber] forKey:@"subWOEmployeeWorkingTimeId"];
                    [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderActualHourIdInsideLoop forKey:@"subWorkOrderActualHourId"];
                    [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderIdGlobal forKey:@"subWorkOrderId"];
                    [dictTemoObjEmployeeWiseSlots setObject:strWorkingDateInsideLoop forKey:@"workingDate"];
                    [dictTemoObjEmployeeWiseSlots setObject:strWorkOrderId forKey:@"workorderId"];
                    
                    if ([strHeaderTitleInsideLoop isEqualToString:@"Standard"]) {
                        
                        [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"1" forKey:@"workingType"];
                        
                    } else if ([strHeaderTitleInsideLoop isEqualToString:@"Holiday"]){
                        
                        [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"3" forKey:@"workingType"];
                        
                    } else{
                        
                        NSString *strSlotsToSet,*strTitleSlotToSet;
                        
                        if (arrOfHoursConfig.count>0) {
                            
                            NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
                            
                            NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
                            
                            for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                                
                                NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                                NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                NSString *strTimeSlotsNew =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                
                                BOOL isMatched = [global isSlotMatched:strTitle :strTimeSlotsNew :strHeaderTitleInsideLoop];

                                if (isMatched) {
                                    
                                    strSlotsToSet=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                    strTitleSlotToSet = [NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];

                                    break;
                                }
                            }
                        }
                        
                        if (strSlotsToSet.length==0) {
                            
                            strSlotsToSet=@"N/A";
                            
                        }
                        
                        [dictTemoObjEmployeeWiseSlots setObject:strSlotsToSet forKey:@"timeSlot"];
                        [dictTemoObjEmployeeWiseSlots setObject:strTitleSlotToSet forKey:@"timeSlotTitle"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"2" forKey:@"workingType"];
                        
                    }
                    
                }
                
                [arrOfTempDateWiseObj addObject:dictTemoObjEmployeeWiseSlots];
                
            }
            
            
            [dictOfMainObjDateWise setValue:arrOfTempDateWiseObj forKey:@"DateWise"];
            
            [arrOfTempEmployeeListFiltered addObject:dictOfMainObjDateWise];
            
        }
        [dictOfMainObj setValue:arrOfTempEmployeeListFiltered forKey:@"DateWiseData"];
        [arrOfMainTempEmployeeTimeSlot addObject:dictOfMainObj];
        //[arrOfMainTempEmployeeTimeSlot addObject:arrOfTempEmployeeListFiltered];
        
    }
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:arrOfMainTempEmployeeTimeSlot])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:arrOfMainTempEmployeeTimeSlot options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Employee Time Sheet arrOfMainTempEmployeeTimeSlot Json object final to set on view: %@", jsonString);
        }
    }
    
    arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
    [arrOfGlobalDynamicEmpSheetFinal addObjectsFromArray:arrOfMainTempEmployeeTimeSlot];
    [self createDynamicEmpSheet:arrOfGlobalDynamicEmpSheetFinal :arrOfHeaderTitleForSlots];
    
}

-(NSMutableArray*)fetchTempEmployeeTimeSheetDataFromDb :(NSString*)strWorkOrderNew :(NSString*)strSubWorkOrderNew{
    
    NSMutableArray *arrTempDataall;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTempSubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"TempSubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    requestTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setEntity:entityTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderNew,strSubWorkOrderNew];
    
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:YES];
    sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    [requestTempSubWoEmployeeWorkingTimeExtSerDcs setSortDescriptors:sortDescriptorsTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTempSubWoEmployeeWorkingTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs performFetch:&error];
    arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs = [self.fetchedResultsControllerTempSubWoEmployeeWorkingTimeExtSerDcs fetchedObjects];
    
    if ([arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs count] == 0)
    {
        
        
    }
    else
    {
        arrOfTimeSlotsEmployeeWiseTempNew=[[NSMutableArray alloc]init];
        arrOfHeaderTitleForSlotsTempNew=[[NSMutableArray alloc]init];
        arrOfEmployeesAssignedInSubWorkOrderTempNew=[[NSMutableArray alloc]init];
        arrOfWorkingDateTempNew=[[NSMutableArray alloc]init];
        
        NSMutableArray *arrWorkingDateTemp=[[NSMutableArray alloc]init];
        NSMutableArray *arrWorkingDateTempForMultipleDates=[[NSMutableArray alloc]init];// all dates were not coming uske liye change kiye.
        
        [arrOfHeaderTitleForSlotsTempNew addObject:@"Standard"];
        
        for (int k=0; k<arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs.count; k++) {
            
            NSManagedObject *objTemp=arrAllObjTempSubWoEmployeeWorkingTimeExtSerDcs[k];
            
            NSArray *keys = [[[objTemp entity] attributesByName] allKeys];
            NSDictionary *dict = [objTemp dictionaryWithValuesForKeys:keys];
            
            NSDictionary *dictObjTemp=dict;
            
            NSMutableDictionary *dictObjMutableTemp=[[NSMutableDictionary alloc]init];
            
            [dictObjMutableTemp addEntriesFromDictionary:dictObjTemp];
            
            [arrOfTimeSlotsEmployeeWiseTempNew addObject:dictObjMutableTemp];
            
            NSString *strWorkingDate=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingDate"]];
            NSString *strSubWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderActualHourId"]];
            
            //            if (![arrWorkingDateTemp containsObject:strSubWorkOrderActualHourId] || ![arrWorkingDateTempForMultipleDates containsObject:strWorkingDate]) {
            //
            //                [arrOfWorkingDateTempNew addObject:objTemp];
            //                [arrWorkingDateTemp addObject:strSubWorkOrderActualHourId];
            //                [arrWorkingDateTempForMultipleDates addObject:strWorkingDate];
            //
            //            }
            
            if (![arrWorkingDateTemp containsObject:strWorkingDate]) {
                
                [arrOfWorkingDateTempNew addObject:objTemp];
                [arrWorkingDateTemp addObject:strWorkingDate];
                
            }
            
            NSString *strworkingType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingType"]];
            NSString *strTitleSlotsComing=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"timeSlotTitle"]];
            
            NSString *strTitleToSet;
            if ([strworkingType isEqualToString:@"1"]) {
                strTitleToSet=@"Standard";
            } else if ([strworkingType isEqualToString:@"2"]){
                strTitleToSet=strTitleSlotsComing;
                
                if(strTitleToSet.length==0){
                    
                    strTitleToSet=@"N/A";
                    
                }
                
                if (![arrOfHeaderTitleForSlotsTempNew containsObject:strTitleToSet]) {
                    
                    [arrOfHeaderTitleForSlotsTempNew addObject:strTitleToSet];
                    
                }
                
            } else if ([strworkingType isEqualToString:@"3"]){
                strTitleToSet=@"Holiday";
            }
            
            
            NSString *strEmployeesAssignedInSubWorkOrder=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
            
            if (![arrOfEmployeesAssignedInSubWorkOrderTempNew containsObject:strEmployeesAssignedInSubWorkOrder]) {
                
                [arrOfEmployeesAssignedInSubWorkOrderTempNew addObject:strEmployeesAssignedInSubWorkOrder];
                
            }
            
        }
        
        [arrOfHeaderTitleForSlotsTempNew addObject:@"Holiday"];
        
        arrOfHeaderTitleForSlotsTempNew=[[NSMutableArray alloc]init];
        arrOfHeaderTitleForSlotsTempNew=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        
        NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
        NSMutableArray *arrOfEmpIdss=[[NSMutableArray alloc]init];
        arrOfEmpIdss=arrOfEmployeesAssignedInSubWorkOrderTempNew;
        
        arrOfEmployeesAssignedInSubWorkOrderTempNew=[[NSMutableArray alloc]init];
        
        if ([dictEmployeeList isKindOfClass:[NSString class]]) {
            
            
        } else {
            
            if (!(dictEmployeeList.count==0)) {
                NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
                for (int k=0; k<arrOfEmployee.count; k++) {
                    NSDictionary *dictDatata=arrOfEmployee[k];
                    NSString *strEmpIdsss=[NSString stringWithFormat:@"%@",[dictDatata valueForKey:@"EmployeeNo"]];
                    if ([arrOfEmpIdss containsObject:strEmpIdsss]) {
                        [arrOfEmployeesAssignedInSubWorkOrderTempNew addObject:dictDatata];
                    }
                }
            }
        }
        
        
        
        NSMutableArray *arrOfMainTempEmployeeTimeSlot=[[NSMutableArray alloc]init];
        
        // Logic for creating final Array list for Employee Time Slot Wise
        
        for (int q=0; q<arrOfEmployeesAssignedInSubWorkOrderTempNew.count; q++) {
            
            
            NSMutableDictionary *dictOfMainObj=[[NSMutableDictionary alloc]init];
            
            NSDictionary *dictObjEmp=arrOfEmployeesAssignedInSubWorkOrderTempNew[q];
            
            NSString *strEmployeeNoInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"EmployeeNo"]];
            NSString *strEmployeeNameInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"FullName"]];
            
            [dictOfMainObj setValue:strEmployeeNoInsideLoop forKey:@"EmployeeNo"];
            [dictOfMainObj setValue:strEmployeeNameInsideLoop forKey:@"EmployeeName"];
            
            // Change for Labor Type
            
            if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoLoggedIn]) {
                
                BOOL isMechanicLocal = [global isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderIdGlobal];
                
                if (isMechanicLocal) {
                    
                    [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                    
                } else {
                    
                    [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                    
                }
                
                //[dictOfMainObj setValue:@"No" forKey:@"Helper"];
                
                
            } else {
                
                BOOL isMechanicLocal = [global isMechanicType:strWorkOrderId :strSubWorkOrderIdGlobal :strEmployeeNoInsideLoop];
                
                if (isMechanicLocal) {
                    
                    [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                    
                } else {
                    
                    [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                    
                }
                
                //[dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                
            }
            
            NSMutableArray *arrOfTempEmployeeListFiltered=[[NSMutableArray alloc]init];
            
            for (int q1=0; q1<arrOfWorkingDateTempNew.count; q1++) {
                
                NSMutableDictionary *dictOfMainObjDateWise=[[NSMutableDictionary alloc]init];
                
                NSDictionary *dictObjWorkingDate=arrOfWorkingDateTempNew[q1];
                
                NSString *strSubWorkOrderActualHourIdInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"subWorkOrderActualHourId"]];
                NSString *strWorkingDateInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"workingDate"]];
                
                //[dictOfMainObj setValue:strSubWorkOrderActualHourIdInsideLoop forKey:@"ActualHrId"];
                [dictOfMainObjDateWise setValue:strWorkingDateInsideLoop forKey:@"WorkingDate"];
                
                NSMutableArray *arrOfTempDateWiseObj=[[NSMutableArray alloc]init];
                
                for (int q2=0; q2<arrOfHeaderTitleForSlotsTempNew.count; q2++) {
                    
                    CGFloat actualAmountTemp=0.0,billableAmountTemp=0.0,actualDurationTemp=0.0,billableDurationTemp=0.0;
                    
                    NSString *strHeaderTitleInsideLoop=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlotsTempNew[q2]];
                    
                    NSMutableDictionary *dictTemoObjEmployeeWiseSlots=[[NSMutableDictionary alloc]init];
                    
                    for (int q3=0; q3<arrOfTimeSlotsEmployeeWiseTempNew.count; q3++) {
                        
                        NSDictionary *dictObjTimeSlotsEmployeeWise=arrOfTimeSlotsEmployeeWiseTempNew[q3];
                        
                        NSString *strEmployeeNoTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"employeeNo"]];
                        NSString *strWorkingDateTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingDate"]];
                        NSString *strActualHrIdTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"subWorkOrderActualHourId"]];
                        NSString *strWorkingTypeTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingType"]];
                        NSString *strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlotTitle"]];
                        NSString *strtimeSlotTitleEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlot"]];
                        
                        BOOL isMatchecd = [global isSlotMatched:strtimeSlotTitleEmployeeWiseInsideLoop :strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3 :strHeaderTitleInsideLoop];
                        
                        NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualAmt"]];
                        NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualDurationInMin"]];
                        NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableAmt"]];
                        NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableDurationInMin"]];
                        
                        /*
                         NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualAmt"]];
                         NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualDurationInMin"]];
                         NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableAmt"]];
                         NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableDurationInMin"]];
                         */
                        
                        //                        actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                        //                        actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                        //                        billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                        //                        billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                        
                        strActualHrIdTimeSlotsEmployeeWiseInsideLoop = strSubWorkOrderActualHourIdInsideLoop;
                        
                        if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Standard"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"]) {
                            
                            actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                            actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                            billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                            billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Holiday"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                            
                            actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                            actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                            billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                            billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && isMatchecd &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"] &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                            
                            actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                            actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                            billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                            billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        }else{
                            
                            //                            actualAmountTemp=[strActualAmtInsideLoop floatValue] + actualAmountTemp;
                            //                            actualDurationTemp=[strActualDurationInMinInsideLoop floatValue] + actualDurationTemp;
                            //                            billableAmountTemp=[strBillableAmtInsideLoop floatValue] + billableAmountTemp;
                            //                            billableDurationTemp=[strBillableDurationInMinInsideLoop floatValue] + billableDurationTemp;
                            
                        }
                        
                        if ([dictTemoObjEmployeeWiseSlots count] > 0) {
                            
                            if (dictTemoObjEmployeeWiseSlots.count==4) {
                                
                            } else {
                                
                                [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualAmountTemp] forKey:@"actualAmt"];
                                [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualDurationTemp] forKey:@"actualDurationInMin"];
                                [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableAmountTemp] forKey:@"billableAmt"];
                                [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableDurationTemp] forKey:@"billableDurationInMin"];
                                //[dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%@",strActualHrIdTimeSlotsEmployeeWiseInsideLoop] forKey:@"subWorkOrderActualHourId"];
                                
                            }
                        }
                        
                    }
                    
                    
                    /*
                     // this was commented before don't know why but i m uncommenting it to check
                     // if dictTemoObjEmployeeWiseSlots is null to apna pura data banaunga mai
                     
                     if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                     
                     NSString *strDateNew =[global strCurrentDate];
                     
                     [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"actualAmt"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"actualDurationInMin"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"billableAmt"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"billableDurationInMin"];
                     [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"createdBy"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"Mobile" forKey:@"createdByDevice"];
                     [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"createdDate"];
                     //[dictTemoObjEmployeeWiseSlots setObject:strEmployeeNameInsideLoop forKey:@"employeeName"];
                     [dictTemoObjEmployeeWiseSlots setObject:strEmployeeNoInsideLoop forKey:@"employeeNo"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isActive"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isApprove"];
                     [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"modifiedBy"];
                     [dictTemoObjEmployeeWiseSlots setObject:strDateNew forKey:@"modifiedDate"];
                     [dictTemoObjEmployeeWiseSlots setObject:[global getReferenceNumber] forKey:@"subWOEmployeeWorkingTimeId"];
                     [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderActualHourIdInsideLoop forKey:@"subWorkOrderActualHourId"];
                     [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderIdGlobal forKey:@"subWorkOrderId"];
                     [dictTemoObjEmployeeWiseSlots setObject:strWorkingDateInsideLoop forKey:@"workingDate"];
                     [dictTemoObjEmployeeWiseSlots setObject:strWorkOrderId forKey:@"workorderId"];
                     
                     if ([strHeaderTitleInsideLoop isEqualToString:@"Standard"]) {
                     
                     [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"1" forKey:@"workingType"];
                     
                     } else if ([strHeaderTitleInsideLoop isEqualToString:@"Holiday"]){
                     
                     [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"3" forKey:@"workingType"];
                     
                     } else{
                     
                     NSString *strSlotsToSet;
                     
                     if (arrOfHoursConfig.count>0) {
                     
                     NSString *strSlotsToSet,*strTitleSlotToSet;
                     
                     if (arrOfHoursConfig.count>0) {
                     
                     NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
                     
                     NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
                     
                     for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                     
                     NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                     NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                     NSString *strTimeSlotsNew =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                     
                     BOOL isMatched = [global isSlotMatched:strTitle :strTimeSlotsNew :strHeaderTitleInsideLoop];
                     
                     if (isMatched) {
                     
                     strSlotsToSet=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                     strTitleSlotToSet = [NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                     break;
                     }
                     }
                     }
                     
                     if (strSlotsToSet.length==0) {
                     
                     strSlotsToSet=@"N/A";
                     
                     }
                     
                     [dictTemoObjEmployeeWiseSlots setObject:strSlotsToSet forKey:@"timeSlot"];
                     [dictTemoObjEmployeeWiseSlots setObject:strTitleSlotToSet forKey:@"timeSlotTitle"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"2" forKey:@"workingType"];
                     
                     }
                     
                     if (strSlotsToSet.length==0) {
                     
                     strSlotsToSet=@"N/A";
                     
                     }
                     
                     [dictTemoObjEmployeeWiseSlots setObject:strSlotsToSet forKey:@"timeSlot"];
                     [dictTemoObjEmployeeWiseSlots setObject:strHeaderTitleInsideLoop forKey:@"timeSlotTitle"];
                     [dictTemoObjEmployeeWiseSlots setObject:@"2" forKey:@"workingType"];
                     
                     }
                     
                     }
                     // yaha tak comment tha
                     */
                    
                    if (dictTemoObjEmployeeWiseSlots.count>1) {
                        
                        [dictTemoObjEmployeeWiseSlots setValue:strSubWorkOrderActualHourIdInsideLoop forKey:@"subWorkOrderActualHourId"];
                        
                        [arrOfTempDateWiseObj addObject:dictTemoObjEmployeeWiseSlots];
                        
                    }
                }
                
                [dictOfMainObjDateWise setValue:arrOfTempDateWiseObj forKey:@"DateWise"];
                
                [arrOfTempEmployeeListFiltered addObject:dictOfMainObjDateWise];
                
            }
            [dictOfMainObj setValue:arrOfTempEmployeeListFiltered forKey:@"DateWiseData"];
            [arrOfMainTempEmployeeTimeSlot addObject:dictOfMainObj];
            //[arrOfMainTempEmployeeTimeSlot addObject:arrOfTempEmployeeListFiltered];
            
        }
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:arrOfMainTempEmployeeTimeSlot])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrOfMainTempEmployeeTimeSlot options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Employee Time Sheet Temporary arrOfMainTempEmployeeTimeSlot Json object final to set on view: %@", jsonString);
            }
        }
        
        arrTempDataall=[[NSMutableArray alloc]init];
        
        for (int q=0; q<arrOfMainTempEmployeeTimeSlot.count; q++) {
            
            NSDictionary *dictDataTemp=arrOfMainTempEmployeeTimeSlot[q];
            
            NSArray *arrTempData=[dictDataTemp valueForKey:@"DateWiseData"];
            
            for (int w=0; w<arrTempData.count; w++) {
                
                NSDictionary *dictDataTemp1=arrTempData[w];
                
                NSArray *arrTempData1=[dictDataTemp1 valueForKey:@"DateWise"];
                
                for (int r=0; r<arrTempData1.count; r++) {
                    
                    NSDictionary *dictDataTemp2=arrTempData1[r];
                    
                    [arrTempDataall addObject:dictDataTemp2];
                    
                }
            }
        }
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return arrTempDataall;
}

-(void)createDynamicEmpSheet :(NSArray*)arrOfEmpSheet :(NSArray*)arrOfHeaderTitle{
    
    NSMutableArray *arrHeight=[[NSMutableArray alloc]init];
    
    for(UIView *view in scrollViewEmpTimeSheet.subviews)
    {
        [view removeFromSuperview];
    }
    
    arrOfHeaderTitle=arrOfHeaderTitleForSlots;
    //    arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
    //    [arrOfGlobalDynamicEmpSheetFinal addObjectsFromArray:arrOfEmpSheet];
    
    CGFloat scrollViewHeight=0.0;
    CGFloat scrollViewWidth=0.0;
    
    scrollViewHeight=arrOfGlobalDynamicEmpSheetFinal.count*60+60;
    scrollViewWidth=arrOfHeaderTitle.count*100+250;
    
    //scrollViewEmpTimeSheet=[[UIScrollView alloc]initWithFrame:CGRectMake(scrollViewEmpTimeSheet.frame.origin.x, scrollViewEmpTimeSheet.frame.origin.y, scrollViewWidth, scrollViewHeight)];
    
    UIView *viewEmpSheetOnScroll=[[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollViewWidth, 100)];
    
    CGFloat xAxisHeaderTitle=200;
    CGFloat yAxisMain=0;
    for (int j=0; j<arrOfGlobalDynamicEmpSheetFinal.count; j++) {
        
        xAxisHeaderTitle=10;
        
        [arrHeight addObject:@"1"];
        
        NSDictionary *dictDataEmp=arrOfGlobalDynamicEmpSheetFinal[j];
        
        UILabel *lblTitleEmployeeName=[[UILabel alloc]init];
        lblTitleEmployeeName.backgroundColor=[UIColor clearColor];
        //lblTitleEmployeeName.layer.borderWidth = 1.5f;
        //lblTitleEmployeeName.layer.cornerRadius = 0.0f;
        lblTitleEmployeeName.layer.borderColor = [[UIColor grayColor] CGColor];
        lblTitleEmployeeName.frame=CGRectMake(10, yAxisMain, [UIScreen mainScreen].bounds.size.width-10, 50);
        lblTitleEmployeeName.font=[UIFont boldSystemFontOfSize:22];
        //lblTitleEmployeeName.text=[NSString stringWithFormat:@"%@",[dictDataEmp valueForKey:@"EmployeeName"]];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[dictDataEmp valueForKey:@"EmployeeName"]]];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        lblTitleEmployeeName.attributedText=attributeString;
        
        lblTitleEmployeeName.textAlignment=NSTextAlignmentLeft;
        lblTitleEmployeeName.numberOfLines=700;
        [lblTitleEmployeeName setAdjustsFontSizeToFitWidth:YES];
        [viewEmpSheetOnScroll addSubview:lblTitleEmployeeName];
        
        NSArray *arrDataDateWise=[dictDataEmp valueForKey:@"DateWiseData"];
        
        yAxisMain=yAxisMain;
        
        for (int i=0; i<=arrOfHeaderTitleForSlots.count; i++) {
            
            UILabel *lblTitleHeader=[[UILabel alloc]init];
            lblTitleHeader.backgroundColor=[UIColor lightTextColor];
            lblTitleHeader.layer.borderWidth = 1.5f;
            lblTitleHeader.layer.cornerRadius = 0.0f;
            lblTitleHeader.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            if (i==0) {
                
                lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, yAxisMain+60, 200, 50);
                xAxisHeaderTitle=xAxisHeaderTitle+200;

                
            }else{
                
                lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, yAxisMain+60, 100, 50);
                xAxisHeaderTitle=xAxisHeaderTitle+100;

                
            }
            //lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, yAxisMain+60, 100, 50);
            lblTitleHeader.font=[UIFont systemFontOfSize:22];
            lblTitleHeader.backgroundColor=[UIColor lightTextColorTimeSheet];
            if (i==0) {
                
                lblTitleHeader.text=[NSString stringWithFormat:@"%@",@""];
                
            } else if(i==1){
                
                lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Regular"];
                
            }else if(i==arrOfHeaderTitleForSlots.count){
                
                lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Holiday"];
                
            }else {
                
                //lblTitleHeader.text=[NSString stringWithFormat:@"%@",arrOfHeaderTitle[i]];
                lblTitleHeader.text=[global slotHeaderTitleFromTimeSlot:arrOfHoursConfig :[NSString stringWithFormat:@"%@",arrOfHeaderTitle[i-1]]];

            }
            
            lblTitleHeader.textAlignment=NSTextAlignmentCenter;
            lblTitleHeader.numberOfLines=700;
            [lblTitleHeader setAdjustsFontSizeToFitWidth:YES];
            
            [viewEmpSheetOnScroll addSubview:lblTitleHeader];
            
        }
        
        yAxisMain=yAxisMain;
        
        CGFloat yAxis=yAxisMain+120;
        
        for (int k=0; k<arrDataDateWise.count; k++) {
            
            CGFloat xAxis=210;
            
            scrollViewHeight=yAxis+100;
            
            NSDictionary *dictDataDatee=arrDataDateWise[k];
            
            UILabel *lblTitleWorkingDate=[[UILabel alloc]init];
            lblTitleWorkingDate.backgroundColor=[UIColor clearColor];
            lblTitleWorkingDate.layer.borderWidth = 1.5f;
            lblTitleWorkingDate.layer.cornerRadius = 0.0f;
            lblTitleWorkingDate.layer.borderColor = [[UIColor grayColor] CGColor];
            lblTitleWorkingDate.frame=CGRectMake(10, yAxis, 200, 50);
            lblTitleWorkingDate.font=[UIFont systemFontOfSize:22];
            lblTitleWorkingDate.text=[NSString stringWithFormat:@"%@",[dictDataDatee valueForKey:@"WorkingDate"]];
            lblTitleWorkingDate.text=[global changeDtaeEmpTimeSheet:[NSString stringWithFormat:@"%@",[dictDataDatee valueForKey:@"WorkingDate"]]];
            lblTitleWorkingDate.textAlignment=NSTextAlignmentCenter;
            lblTitleWorkingDate.numberOfLines=700;
            [lblTitleWorkingDate setAdjustsFontSizeToFitWidth:YES];
            lblTitleWorkingDate.backgroundColor=[UIColor lightTextColorTimeSheet];
            [viewEmpSheetOnScroll addSubview:lblTitleWorkingDate];
            
            NSArray *arrEmpData=[dictDataDatee valueForKey:@"DateWise"];
            
            for (int k1=0; k1<arrEmpData.count; k1++) {
                [arrHeight addObject:@"1"];
                NSDictionary *dictDataaaa=arrEmpData[k1];
                CGRect txtViewFrame = CGRectMake(xAxis, yAxis,100,50);
                UITextField *txtView= [[UITextField alloc] init];
                //txtView.text = [global getHrAndMinsGlobal:[dictDataaaa valueForKey:@"actualDurationInMin"]];
                txtView.text = [global getHrAndMinsGlobal:[dictDataaaa valueForKey:@"billableDurationInMin"]];
                
                if ([txtView.text isEqualToString:@"00:00"]) {
                    
                    txtView.text =@"";
                    //txtView.placeholder=@"00:00";
                    
                }
                
                txtView.frame =txtViewFrame;
                txtView.layer.borderWidth = 1.5f;
                txtView.layer.cornerRadius = 0.0f;
                txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                [txtView setBackgroundColor:[UIColor clearColor]];
                txtView.font=[UIFont systemFontOfSize:20];
                txtView.delegate=self;
                
                UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                leftView.backgroundColor = [UIColor clearColor];
                txtView.leftView = leftView;
                txtView.leftViewMode = UITextFieldViewModeAlways;
                txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                
                txtView.tag=k*1000+k1+500;
                
                if (isCompletedStatusMechanical) {
                    
                    txtView.enabled=false;
                    txtView.enabled=true;

                } else {
                    
                    txtView.enabled=true;
                    
                }
                
                NSString *strTags=[NSString stringWithFormat:@"%d,%d,%d",j,k,k1];
                UIColor *color = [UIColor clearColor];
                txtView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:strTags attributes:@{NSForegroundColorAttributeName: color}];
                
                [viewEmpSheetOnScroll addSubview:txtView];
                
                xAxis=xAxis+100;
                
            }
            
            
            yAxis=yAxis+50;
            
        }
        
        yAxisMain=yAxis;
    }
    
    if (scrollViewWidth<[UIScreen mainScreen].bounds.size.width) {
        
        scrollViewWidth=[UIScreen mainScreen].bounds.size.width;
        
    }
    
   // scrollViewEmpTimeSheet=[[UIScrollView alloc]initWithFrame:CGRectMake(0, scrollViewEmpTimeSheet.frame.origin.y, scrollViewWidth, [UIScreen mainScreen].bounds.size.height-194)];
    
    viewEmpSheetOnScroll.frame=CGRectMake(viewEmpSheetOnScroll.frame.origin.x, viewEmpSheetOnScroll.frame.origin.y, scrollViewWidth, scrollViewHeight);
    
    [viewEmpSheetOnScroll setFrame:CGRectMake(viewEmpSheetOnScroll.frame.origin.x, viewEmpSheetOnScroll.frame.origin.y, scrollViewWidth, scrollViewHeight)];
    
    [scrollViewEmpTimeSheet addSubview:viewEmpSheetOnScroll];
    
    [scrollViewEmpTimeSheet setContentSize:CGSizeMake(scrollViewWidth,scrollViewHeight)];
    
    scrollViewEmpTimeSheet.scrollEnabled=true;
    scrollViewEmpTimeSheet.showsHorizontalScrollIndicator=true;
    scrollViewEmpTimeSheet.showsVerticalScrollIndicator=true;
    
    [self.view addSubview:scrollViewEmpTimeSheet];
}

#pragma mark- **************** Textfield Delegate Methods ****************

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
   // [scrollViewEmpTimeSheet setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    int tagTextFld=(int)textField.tag;
    
    
    if (tagTextFld>499) {
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        else
        {
            NSString *text = textField.text;
            NSInteger length = text.length;
            BOOL shouldReplace = YES;
            
            if (![string isEqualToString:@""])
            {
                switch (length)
                {
                    case 2:
                        textField.text = [text stringByAppendingString:@":"];
                        break;
                        
                    default:
                        break;
                }
                if (length > 4)
                    shouldReplace = NO;
            }
            
            return shouldReplace;
        }
        return YES;
        
    }
    else
        return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    int tagTextFld=(int)textField.tag;
    
    if (tagTextFld>499) {
        
        NSString *strTextx;
        
        if (textField.text.length>=5) {
            
            strTextx=textField.text;
            
        } else if (textField.text.length==4){
            
            NSString *strTemp=@"0";//  0:00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==3){
            
            NSString *strTemp=@"00";//  :00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==2){
            
            //            NSString *strTemp=@"00:";//  00
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=@":00";//  00
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==1){
            
            //            NSString *strTemp=@"00:0";//  0
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=[NSString stringWithFormat:@"0%@:00",textField.text];
            //NSString *strTemp=@"0:00";
            //strTextx=[textField.text stringByAppendingString:strTemp];
            strTextx=strTemp;
        } else if (textField.text.length==0){
            
            strTextx=@"00:00";
            
        }
        
        
        NSString *lastStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        //NSString *lastSecondStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        NSString *finalStrText = [NSString stringWithFormat:@"%@",lastStrText];
        int valueEntered = [finalStrText intValue];
        if (valueEntered>59) {
            
            [global AlertMethod:Alert :@"Minutes can't be greater then 59"];
            
        }else{
         
            textField.text=strTextx;
            
            if ([textField.text isEqualToString:@"00:00"]) {
                
                textField.text =@"";
                //textField.placeholder=@"00:00";
                
            }
            
            NSArray *arrTime=[strTextx componentsSeparatedByString:@":"];
            
            NSString *strHrs,*strMinutes;
            
            if (arrTime.count==1) {
                
                strHrs=arrTime[0];
                strMinutes=@"";
                
            }
            
            if (arrTime.count==2) {
                
                strHrs=arrTime[0];
                strMinutes=arrTime[1];
                
            }
            
            NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
            
            int totalsecondUpdatedEstTime=(int)secondTimeToIncrease;
            totalsecondUpdatedEstTime=totalsecondUpdatedEstTime/60;
            
            NSString *strTagTextFld=textField.placeholder;
            
            NSArray *arrOfTagsTxtFld=[strTagTextFld componentsSeparatedByString:@","];
            
            if (tagTextFld>=99999999) {
                
                // this is for Replacing  EMP working time duration in arrOfAddEmpWorkingTimeDuration
                
                [arrOfAddEmpWorkingTimeDuration replaceObjectAtIndex:tagTextFld-99999999 withObject:[NSString stringWithFormat:@"%d",totalsecondUpdatedEstTime]];
                
            }else {
                
                [self replaceBillableDurationInEmpSheet:arrOfTagsTxtFld :[NSString stringWithFormat:@"%d",totalsecondUpdatedEstTime]];
                
            }
            
            //[self replaceBillableDurationInEmpSheet:arrOfTagsTxtFld :[NSString stringWithFormat:@"%d",totalsecondUpdatedEstTime]];
            
        }
        
    }
}

-(void)saveAddEmpWorkingTime :(int)strIndex  :(NSString*)strDuration :(NSString*)strSubWOEmployeeWorkingTimeId{
    
    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    
    SubWoEmployeeWorkingTimeExtSerDcs *objSubWoEmployeeWorkingTimeExtSerDcs = [[SubWoEmployeeWorkingTimeExtSerDcs alloc]initWithEntity:entitySubWoEmployeeWorkingTimeExtSerDcs insertIntoManagedObjectContext:context];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.subWOEmployeeWorkingTimeId=[NSString stringWithFormat:@"%@",[global getReferenceNumber]];
    //objSubWoEmployeeWorkingTimeExtSerDcs.subWOEmployeeWorkingTimeId=[NSString stringWithFormat:@"%@",strSubWOEmployeeWorkingTimeId];

    objSubWoEmployeeWorkingTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",strWorkOrderId];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",strSubWorkOrderIdGlobal];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderActualHourId=[NSString stringWithFormat:@"%@",strSubWoActualHrIdGlobal];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.employeeNo=[NSString stringWithFormat:@"%@",strEmployeeNoLoggedIn];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.employeeName=[NSString stringWithFormat:@"%@",strEmpName];
    objSubWoEmployeeWorkingTimeExtSerDcs.workingDate=[NSString stringWithFormat:@"%@",btnTitleWorkingDate.titleLabel.text];
    
    BOOL isHolidayyyy;
    if ([arrOfHeaderTitleForSlots[strIndex] isEqualToString:@"Holiday"]) {
        isHolidayyyy=YES;
    } else {
        isHolidayyyy=NO;
    }
    
    // strHelperIdGlobal
    
    // Change for Labor Type
    
    BOOL isLaborTypee = [global isMechanicType:strWorkOrderId :strSubWorkOrderIdGlobal :strHelperIdGlobal];
    
//    if (isMechanicLocal) {
//
//        isHelperGlobal = false;
//
//    } else {
//
//        isHelperGlobal = true;
//
//    }
    
    NSString *strAmount= [global methodToCalculateLaborPriceGlobal:strDuration :isLaborTypee :arrOfHoursConfig :true :isHolidayyyy :arrOfHeaderTitleForSlots[strIndex]];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",strDuration];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",strAmount];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",@"0"];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",@"0"];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%@",strDuration];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",strAmount];
    
    
    if ([arrOfHeaderTitleForSlots[strIndex] isEqualToString:@"Standard"]) {
        
        objSubWoEmployeeWorkingTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",@"1"];
        
        objSubWoEmployeeWorkingTimeExtSerDcs.timeSlotTitle=[NSString stringWithFormat:@"%@",@""];
        
        objSubWoEmployeeWorkingTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",@""];
        
    } else if ([arrOfHeaderTitleForSlots[strIndex] isEqualToString:@"Holiday"]){
        
        objSubWoEmployeeWorkingTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",@"3"];

        objSubWoEmployeeWorkingTimeExtSerDcs.timeSlotTitle=[NSString stringWithFormat:@"%@",@""];
        
        objSubWoEmployeeWorkingTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",@""];
        
    } else{
        
        objSubWoEmployeeWorkingTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",@"2"];

        
        NSString *strSlotsToSet,*strTitleSlotToSet;
        
        if (arrOfHoursConfig.count>0) {
            
            NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
            
            NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
            
            for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                
                NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                NSString *strTimeSlotsNew =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                
                if ([strTimeSlotsNew isEqualToString:arrOfHeaderTitleForSlots[strIndex]]) {
                    
                    strSlotsToSet=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                    strTitleSlotToSet = [NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                    
                    break;
                    
                }
                
            }
        }

        
        objSubWoEmployeeWorkingTimeExtSerDcs.timeSlotTitle=[NSString stringWithFormat:@"%@",strTitleSlotToSet];
        
        objSubWoEmployeeWorkingTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[strIndex]];
        
    }
    
    objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",@"True"];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",@"True"];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",@"True"];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",@"True"];
    
    NSString *strDateNew =[global strCurrentDate];

    objSubWoEmployeeWorkingTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",strDateNew];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",strEmpName];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",strDateNew];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",strEmpName];
    
    objSubWoEmployeeWorkingTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    // Change for Labor Type
    
    if (isHelperGlobal) {
        
        objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"True";
        
        objSubWoEmployeeWorkingTimeExtSerDcs.employeeNo=[NSString stringWithFormat:@"%@",strHelperIdGlobal];
        
        objSubWoEmployeeWorkingTimeExtSerDcs.employeeName=[NSString stringWithFormat:@"%@",_btnSelectHelper.titleLabel.text];
        
    } else {
        
        objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"False";
        
    }
    
    
    NSLog(@"strSubWoActualHrIdGlobal being saved is equal toooo = = = %@",objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderActualHourId);
    
    NSLog(@"objSubWoEmployeeWorkingTimeExtSerDcs being saved is equal toooo = = = %@",objSubWoEmployeeWorkingTimeExtSerDcs);

    
    NSError *errorSubWoEmployeeWorkingTimeExtSerDcs;
    [context save:&errorSubWoEmployeeWorkingTimeExtSerDcs];
    
}


-(void)replaceBillableDurationInEmpSheet :(NSArray*)tag :(NSString*)strDuration{
    
    
    NSString *strTagI=tag[0];
    NSString *strTagJ=tag[1];
    NSString *strTagK=tag[2];
    
    int tagI=[strTagI intValue];
    int tagJ=[strTagJ intValue];
    int tagK=[strTagK intValue];
    
    
    //NSMutableArray *arrMainTemp=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictMainTemp=[[NSMutableDictionary alloc]init];
    NSMutableArray *arrDateWiseDataTemp=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictDateWiseDataTemp=[[NSMutableDictionary alloc]init];
    NSMutableArray *arrDateTemp=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictDateTemp=[[NSMutableDictionary alloc]init];
    
    dictMainTemp=arrOfGlobalDynamicEmpSheetFinal[tagI];
    
    NSString *strHelper=[dictMainTemp valueForKey:@"Helper"];
    
    BOOL isLaborType;
    if ([strHelper isEqualToString:@"No"]) {
        
        isLaborType=YES;
        
    } else {
        
        isLaborType=NO;
        
    }
    
    arrDateWiseDataTemp=[dictMainTemp valueForKey:@"DateWiseData"];
    
    dictDateWiseDataTemp=arrDateWiseDataTemp[tagJ];
    
    arrDateTemp=[dictDateWiseDataTemp valueForKey:@"DateWise"];
    
    dictDateTemp=arrDateTemp[tagK];
    
    NSString *strworkingType=[dictDateTemp valueForKey:@"workingType"];
    BOOL isHolidayyyy;
    if ([strworkingType isEqualToString:@"3"]) {
        isHolidayyyy=YES;
    } else {
        isHolidayyyy=NO;
    }
    
    NSString *strAmount= [global methodToCalculateLaborPriceGlobal:strDuration :isLaborType :arrOfHoursConfig :true :isHolidayyyy :[dictDateTemp valueForKey:@"timeSlot"]];
    
    NSString *strAmtBlank = [NSString stringWithFormat:@"%@",[dictDateTemp valueForKey:@"actualAmt"]];
    
    if ([strAmtBlank isEqualToString:@""]) {
        
        [dictDateTemp setValue:@"0" forKey:@"actualDurationInMin"];
        [dictDateTemp setValue:@"0" forKey:@"actualAmt"];
        
    }
    
    [dictDateTemp setValue:strDuration forKey:@"billableDurationInMin"];
    [dictDateTemp setValue:strAmount forKey:@"billableAmt"];
    
    [arrDateTemp replaceObjectAtIndex:tagK withObject:dictDateTemp];
    
    [dictDateWiseDataTemp setValue:arrDateTemp forKey:@"DateWise"];
    
    [arrDateWiseDataTemp replaceObjectAtIndex:tagJ withObject:dictDateWiseDataTemp];
    
    [dictMainTemp setValue:arrDateWiseDataTemp forKey:@"DateWiseData"];
    
    [arrOfGlobalDynamicEmpSheetFinal replaceObjectAtIndex:tagI withObject:dictMainTemp];
    
    //[self getCompletedSubWOCompleteTimeExtSerDcs :@"add"];
    
}


-(void)deleteSubWoEmployeeWorkingTimeExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete SubWoEmployeeWorkingTimeExtSerDcs Data//SubWoEmployeeWorkingTimeExtSerDcs
    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdGlobal];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

// TempSubWoEmployeeWorkingTimeExtSerDcs
-(void)deleteTempSubWoEmployeeWorkingTimeExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete SubWoEmployeeWorkingTimeExtSerDcs Data//SubWoEmployeeWorkingTimeExtSerDcs
    entityTempSubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"TempSubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"TempSubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdGlobal];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)saveEmployeeTimeSheetSlotWise :(NSArray*)arrEmpSlots :(NSString*)strType{
    //SubWoEmployeeWorkingTimeExtSerDcs
    [self deleteSubWoEmployeeWorkingTimeExtSerDcs];
    
    [self deleteTempSubWoEmployeeWorkingTimeExtSerDcs];
    
    for (int k=0; k<arrEmpSlots.count; k++) {
        
        NSDictionary *dictData=arrEmpSlots[k];
        
        NSArray *arrTempp=[dictData valueForKey:@"DateWiseData"];
        
        for (int j=0; j<arrTempp.count; j++) {
            
            NSDictionary *dictDataTemp=arrTempp[j];
            
            NSArray *arrTempNew=[dictDataTemp valueForKey:@"DateWise"];
            
            for (int i=0; i<arrTempNew.count; i++) {
                
                NSDictionary *dictDataaa=arrTempNew[i];
                
                NSString *strBillableAmt=[dictDataaa valueForKey:@"billableAmt"];
                NSString *strActualAmt=[dictDataaa valueForKey:@"actualAmt"];
                NSString *strBillableDuration=[dictDataaa valueForKey:@"billableDurationInMin"];
                NSString *strActualDuration=[dictDataaa valueForKey:@"actualDurationInMin"];
                
                float BillAmount=[strBillableAmt floatValue];
                float ActualAmount=[strActualAmt floatValue];
                float BillDuration=[strBillableDuration floatValue];
                float ActualDuration=[strActualDuration floatValue];
                
                if ((BillAmount>0) || (ActualAmount>0) || (BillDuration>0) || (ActualDuration>0)) {
                    
                    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
                    
                    SubWoEmployeeWorkingTimeExtSerDcs *objSubWoEmployeeWorkingTimeExtSerDcs = [[SubWoEmployeeWorkingTimeExtSerDcs alloc]initWithEntity:entitySubWoEmployeeWorkingTimeExtSerDcs insertIntoManagedObjectContext:context];
                    
                    
                    NSDictionary *dictOfSubWoEmployeeWorkingTimeExtSerDcs =arrTempNew[i];
                    
                    objSubWoEmployeeWorkingTimeExtSerDcs.subWOEmployeeWorkingTimeId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWOEmployeeWorkingTimeId"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workorderId"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWorkOrderId"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWorkOrderActualHourId"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"employeeNo"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.employeeName=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"employeeName"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.workingDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workingDate"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.timeSlotTitle=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"timeSlotTitle"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"timeSlot"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"actualDurationInMin"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"actualAmt"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"billableDurationInMin"]];
                  //  objSubWoEmployeeWorkingTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%d",totalsecondUpdatedEstTime];
                    objSubWoEmployeeWorkingTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"billableAmt"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workingType"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isApprove"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isActive"]];
                    
                    // Change For True And False
                    
                    NSString *strApprove = [NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isApprove"]];
                    
                    if ([strApprove isEqualToString:@"1"] || [strApprove isEqualToString:@"true"] || [strApprove isEqualToString:@"True"]) {
                        
                        objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",@"True"];
                        
                    } else {
                        
                        objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",@"False"];

                    }
                    
                    
                    NSString *strActive = [NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isActive"]];
                    
                    if ([strActive isEqualToString:@"1"] || [strActive isEqualToString:@"true"] || [strActive isEqualToString:@"True"]) {
                        
                        objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",@"True"];
                        
                    } else {
                        
                        objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",@"False"];
                        
                    }
                    
                    
                    objSubWoEmployeeWorkingTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdDate"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdBy"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"modifiedDate"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"modifiedBy"]];
                    objSubWoEmployeeWorkingTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdByDevice"]];
                    
                    // Change for Labor Type
                    
                    if ([[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"Helper"]] isEqualToString:@"Yes"]) {
                        
                        objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"True";
                        
                    } else {
                        
                        objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"False";
                        
                    }
                    
                    
                    NSError *errorSubWoEmployeeWorkingTimeExtSerDcs;
                    [context save:&errorSubWoEmployeeWorkingTimeExtSerDcs];
                    
                }
            }
        }
    }
    
    if ([strType isEqualToString:@"Sync"]) {
        
        [self syncSubWoEmployeeWorkingTimeToServer];
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ----------------Update Time To Server In Case Of Completed WO's----------------
//============================================================================
//============================================================================

-(void)fetchSubWorkOrderActualHrsFromDB{
    
    dictSubWorkOrderEmployeeWorkingTime = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    NSArray *arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderEmployeeWorkingTime setObject:arrEmailDetail forKey:@"SubWorkOrderActualHoursDcs"];
        
    }
    else
    {
        
        NSMutableArray *arrTempActualHrs=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            NSManagedObject *objTempActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempActualHrs entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempActualHrs valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempActualHrs valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSMutableArray *tempKeyArray=[[NSMutableArray alloc]init];
            
            [tempKeyArray addObjectsFromArray:arrLeadDetailKey];
            
            [tempKeyArray addObject:@"EmployeeEmail"];
            
            [arrLeadDetailValue addObject:strFromEmail];
            
            
            //Change For Employee Time Sheet
            
            NSMutableArray *arrTemp=[[NSMutableArray alloc]init];

           arrTemp = [self fetchSubWoEmployeeWorkingTimeExtSerDcs:strSubWorkOrderIdGlobal :[NSString stringWithFormat:@"%@",[objTempActualHrs valueForKey:@"subWOActualHourId"]]];
            
            [tempKeyArray addObject:@"SubWoEmployeeWorkingTimeExtSerDcs"];
            
            [arrLeadDetailValue addObject:arrTemp];
            
            //Change For Employee Time Sheet
            
            
            arrLeadDetailKey=tempKeyArray;
            
            NSDictionary *dictTempActualHrs = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempActualHrs addObject:dictTempActualHrs];
            
        }
        
        [dictSubWorkOrderEmployeeWorkingTime setObject:arrTempActualHrs forKey:@"SubWorkOrderActualHoursDcs"];
        
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}


-(NSMutableArray*)fetchSubWoEmployeeWorkingTimeExtSerDcs :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderActualHoursId{
    
    NSMutableArray *arrOfSubWoEmployeeWorkingTime = [[NSMutableArray alloc] init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    requestSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestSubWoEmployeeWorkingTimeExtSerDcs setEntity:entitySubWoEmployeeWorkingTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderActualHourId = %@",strWorkOrderId,strSubWorkOrderIdToFetch,strSubWorkOrderActualHoursId];

    [requestSubWoEmployeeWorkingTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs];
    
    [requestSubWoEmployeeWorkingTimeExtSerDcs setSortDescriptors:sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs];
    
    self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWoEmployeeWorkingTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs performFetch:&error];
    arrAllObjSubWoEmployeeWorkingTimeExtSerDcs = [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs fetchedObjects];
    
    if ([arrAllObjSubWoEmployeeWorkingTimeExtSerDcs count] == 0)
    {
        
        
    }
    else
    {
        // NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWoEmployeeWorkingTimeExtSerDcs.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjSubWoEmployeeWorkingTimeExtSerDcs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfSubWoEmployeeWorkingTime addObject:dictTempNotes];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return arrOfSubWoEmployeeWorkingTime;
    
}


-(void)syncSubWoEmployeeWorkingTimeToServer{
    
    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
   NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];

    //NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];

    [self fetchSubWorkOrderActualHrsFromDB];
    
    NSArray *arrTempToSend = [dictSubWorkOrderEmployeeWorkingTime valueForKey:@"SubWorkOrderActualHoursDcs"];
 
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:arrTempToSend])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:arrTempToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Sub WO Employee Working Time Json: %@", jsonString);
        }
        
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&companyKey=%@",strServiceUrlMain,UrlSyncSubWoEmployeeWorkingTimeToServer,strSubWorkOrderIdGlobal,strCompanyKey];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"POST"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
    
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    [requestLocal setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [requestLocal setHTTPBody:requestData];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
           
             NSLog(@"Response on Updating Sub Wo Employee Working Time --------%@",ResponseDict);
             
             if ([ResponseDict isKindOfClass:[NSArray class]]) {
                 
                 NSArray *arrTemp = (NSArray*)ResponseDict;
                 
                 if (arrTemp.count>0) {
                     
                     NSDictionary *dictTemp = arrTemp[0];
                     
                     NSString *strStatus =[NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"status"]];
                     
                     if ([strStatus isEqualToString:@"True"] || [strStatus isEqualToString:@"true"] || [strStatus isEqualToString:@"1"]) {
                         
                         [self backMethod];
                         
                     } else {
                         
                         [global AlertMethod:Alert :Sorry];
                         
                     }
                     
                 } else {
                     
                     [global AlertMethod:Alert :Sorry];
                     
                 }
                 
             }else{
                 
                 [global AlertMethod:Alert :Sorry];
                 
             }
             
             [DejalBezelActivityView removeView];
             
             
         }];
    }
    @catch (NSException *exception) {
        
        [global AlertMethod:Alert :Sorry];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }
    
}



- (IBAction)action_SaveAddEmpWorkingTime:(id)sender {

    BOOL isValuePresent = false;
    
    for (int k=0; k<arrOfAddEmpWorkingTimeDuration.count; k++) {
        //00:00
        
        if ([arrOfAddEmpWorkingTimeDuration[k] isEqualToString:@""] || [arrOfAddEmpWorkingTimeDuration[k] isEqualToString:@"00:00"] || [arrOfAddEmpWorkingTimeDuration[k] isEqualToString:@"0"] || [arrOfAddEmpWorkingTimeDuration[k] isEqualToString:@"00"]) {
            
            
        }else{
            
            isValuePresent = YES;
            break;
            
        }
        
    }
    
    if (!isValuePresent) {
        
        [global AlertMethod:Alert :@"Please Enter Hours"];
        
    } else {
        
        if (isHelperGlobal) {
            
            if (strHelperIdGlobal.length==0) {
                
                [global AlertMethod:Alert :@"Please select helper to add"];
                
            } else {
                
                BOOL isAlreadyHelperInDB = [global isAlreadyHelperInDb :strHelperIdGlobal :strWorkOrderId :strSubWorkOrderIdGlobal];
                
                if (!isAlreadyHelperInDB) {
                    
                    [self addServiceHelperFromMobileToDB];
                    
                }
                
                [self saveEmpWorkingTime];
                
            }
            
        }else{
            
            [self saveEmpWorkingTime];
            
        }
        
    }
    
}

-(void)saveEmpWorkingTime{
    
    [self addStartActualHoursFromMobileToDB];
    
    NSString *strID = [global getReferenceNumber];
    
    //strSubWoActualHrIdGlobal = strID;
    
    for (int k=0; k<arrOfAddEmpWorkingTimeDuration.count; k++) {
        //00:00
        
        if ([arrOfAddEmpWorkingTimeDuration[k] isEqualToString:@""] || [arrOfAddEmpWorkingTimeDuration[k] isEqualToString:@"00:00"] || [arrOfAddEmpWorkingTimeDuration[k] isEqualToString:@"0"] || [arrOfAddEmpWorkingTimeDuration[k] isEqualToString:@"00"]) {
            
            
        }else{
            
            [self saveAddEmpWorkingTime : k : arrOfAddEmpWorkingTimeDuration[k] : strID];
            
        }
        
    }
    
    [_viewBackgroundAddEmpWorkingTime removeFromSuperview];
    
    [self fetchEmployeeTimeSheetDataFromDb:strWorkOrderId :strSubWorkOrderIdGlobal];
    
}

- (IBAction)action_CancelAddEmpWorkingTime:(id)sender {
    
    [_viewBackgroundAddEmpWorkingTime removeFromSuperview];
    
}

- (IBAction)action_SelectHelper:(id)sender {
    
    
    NSMutableArray *arrDataTblView=[[NSMutableArray alloc]init];
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
    
    NSArray *arrOfSubWorkServiceHelper = [global fetchSubWorkOrderHelperFromDataBaseForMechanical:strWorkOrderId :strSubWorkOrderIdGlobal];

    if (arrOfSubWorkServiceHelper.count>0) {
        
        for (int k=0; k<arrOfSubWorkServiceHelper.count; k++) {
            
            NSDictionary *dictDataaHelper=arrOfSubWorkServiceHelper[k];
            
            NSString *strEmpNoooo = [NSString stringWithFormat:@"%@",[dictDataaHelper valueForKey:@"employeeNo"]];
            
            for (int i=0; i<arrOfEmployee.count; i++) {
                
                NSDictionary *dictDataa=arrOfEmployee[i];
                
                NSString *strEmpNooooLocal = [NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"EmployeeNo"]];

                if ([strEmpNoooo isEqualToString:strEmpNooooLocal]) {
                    
                    [arrDataTblView addObject:dictDataa];
                    
                }
                
            }
            
        }
        
    }
    
    if (arrDataTblView.count==0) {
        
        [global AlertMethod:Info :NoDataAvailableee];
        
    }else{
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"CRMiPad" bundle:nil];
        SelectionClass  *vc=[storyboard instantiateViewControllerWithIdentifier:@"SelectionClass"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        vc.strTitle = @"Select Helper";
        vc.strTag = 106;
        vc.delegate = self;
        vc.aryList = arrDataTblView;
        [self presentViewController:vc animated:YES completion:nil];
        
    }
    
}

- (IBAction)action_AddEmpWorkingTimeView:(id)sender {
    
    arrOfAddEmpWorkingTimeDuration = [[NSMutableArray alloc] init];
    
    for (int k=0; k<arrOfHeaderTitleForSlots.count; k++) {
        
        [arrOfAddEmpWorkingTimeDuration addObject:@""];
        
    }
    
    
    [_btnSelectHelper.layer setCornerRadius:5.0f];
    [_btnSelectHelper.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectHelper.layer setBorderWidth:0.8f];
    [_btnSelectHelper.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectHelper.layer setShadowOpacity:0.3];
    [_btnSelectHelper.layer setShadowRadius:3.0];
    [_btnSelectHelper.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectHelper setTitle:@"----Select Helper----" forState:UIControlStateNormal];
    
    isHelperGlobal=NO;
    [_btnHelper setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    
    _lblTechName.text = [NSString stringWithFormat:@"Name: %@",strEmpName];
    [_lblTechName setHidden:NO];
    
    [_lblHelperTitle setHidden:YES];
    [_btnSelectHelper setHidden:YES];
    
    strHelperIdGlobal = @"";
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    _viewBackgroundAddEmpWorkingTime.frame = screenRect;
    _viewBackgroundAddEmpWorkingTime.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    [self.view addSubview:_viewBackgroundAddEmpWorkingTime];
    
    [self createDynamicAddEmpWorkingTime];
    
}

- (IBAction)action_HelperOrtech:(id)sender {
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnHelper imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        isHelperGlobal=YES;
        [_btnHelper setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [_lblTechName setHidden:YES];
        [_lblHelperTitle setHidden:NO];
        [_btnSelectHelper setHidden:NO];
        
    }else{
        
        isHelperGlobal=NO;
        [_btnHelper setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [_lblTechName setHidden:NO];
        [_lblHelperTitle setHidden:YES];
        [_btnSelectHelper setHidden:YES];
        
    }
    
}

-(void)createDynamicAddEmpWorkingTime{
    
    for(UIView *view in _scrollViewAddEmpWorkingTime.subviews)
    {
        
        [view removeFromSuperview];
        
    }
    
    CGFloat scrollViewHeight=0.0;
    CGFloat scrollViewWidth=0.0;
    
    scrollViewHeight=150;
    scrollViewWidth=arrOfHeaderTitleForSlots.count*120+250;

    UIView *viewEmpSheetOnScroll=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-40, 150)];
    
    CGFloat xAxisHeaderTitle=0;

    for (int i=0; i<=arrOfHeaderTitleForSlots.count; i++) {
        
        UILabel *lblTitleHeader=[[UILabel alloc]init];
        lblTitleHeader.backgroundColor=[UIColor lightTextColor];
        lblTitleHeader.layer.borderWidth = 1.5f;
        lblTitleHeader.layer.cornerRadius = 0.0f;
        lblTitleHeader.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        if (i==0) {
            
            lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, 0, 200, 50);
            xAxisHeaderTitle=xAxisHeaderTitle+200;
            
            
        }else{
            
            lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, 0, 120, 50);
            xAxisHeaderTitle=xAxisHeaderTitle+120;
            
            
        }
        lblTitleHeader.font=[UIFont systemFontOfSize:22];
        lblTitleHeader.backgroundColor=[UIColor lightTextColorTimeSheet];
        if (i==0) {
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@""];
            
        } else if(i==1){
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Regular"];
            
        }else if(i==arrOfHeaderTitleForSlots.count){
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Holiday"];
            
        }else {
            
            //lblTitleHeader.text=[NSString stringWithFormat:@"%@",arrOfHeaderTitle[i]];
            lblTitleHeader.text=[global slotHeaderTitleFromTimeSlot:arrOfHoursConfig :[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[i-1]]];
            
        }
        
        lblTitleHeader.textAlignment=NSTextAlignmentCenter;
        lblTitleHeader.numberOfLines=700;
        [lblTitleHeader setAdjustsFontSizeToFitWidth:YES];
        
        [viewEmpSheetOnScroll addSubview:lblTitleHeader];
        
        
        if(i==arrOfHeaderTitleForSlots.count){
            
            // We are not creating textfield here for last index
            
        }else{
            
            CGRect txtViewFrame = CGRectMake(xAxisHeaderTitle, 55,120,50);
            UITextField *txtView= [[UITextField alloc] init];
            //txtView.text = [global getHrAndMinsGlobal:[dictDataaaa valueForKey:@"actualDurationInMin"]];
            txtView.text = @"";
            
            if ([txtView.text isEqualToString:@"00:00"]) {
                
                txtView.text =@"";
                //txtView.placeholder=@"00:00";
                
            }
            
            txtView.frame =txtViewFrame;
            txtView.layer.borderWidth = 1.5f;
            txtView.layer.cornerRadius = 0.0f;
            txtView.layer.borderColor = [[UIColor grayColor] CGColor];
            [txtView setBackgroundColor:[UIColor clearColor]];
            txtView.font=[UIFont systemFontOfSize:20];
            txtView.delegate=self;
            
            UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
            leftView.backgroundColor = [UIColor clearColor];
            txtView.leftView = leftView;
            txtView.leftViewMode = UITextFieldViewModeAlways;
            txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            
            txtView.tag=i+99999999;
            
            if (isCompletedStatusMechanical) {
                
                txtView.enabled=false;
                txtView.enabled=true;
                
            } else {
                
                txtView.enabled=true;
                
            }
            
            NSString *strTags=[NSString stringWithFormat:@"%d,%d,%d",0,0,0];
            UIColor *color = [UIColor clearColor];
            txtView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:strTags attributes:@{NSForegroundColorAttributeName: color}];
            
            [viewEmpSheetOnScroll addSubview:txtView];
            
        }
        
    }
    
    btnTitleWorkingDate=[[UIButton alloc]init];
    btnTitleWorkingDate.backgroundColor=[UIColor clearColor];
    btnTitleWorkingDate.layer.borderWidth = 1.5f;
    btnTitleWorkingDate.layer.cornerRadius = 0.0f;
    btnTitleWorkingDate.layer.borderColor = [[UIColor grayColor] CGColor];
    btnTitleWorkingDate.frame=CGRectMake(0, 55, 200, 50);
    btnTitleWorkingDate.titleLabel.font=[UIFont systemFontOfSize:22];
    //btnTitleWorkingDate.titleLabel.textColor = [UIColor blackColor];
    [btnTitleWorkingDate setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    //[btnTitleWorkingDate setTitle:@"Current Date" forState:UIControlStateNormal];
    
    NSString *strCurrentDate = [global strCurrentDateFormatted:@"MM/dd/yyyy" :@"en_US_POSIX"];
    [btnTitleWorkingDate setTitle:strCurrentDate forState:UIControlStateNormal];

    btnTitleWorkingDate.backgroundColor=[UIColor lightTextColorTimeSheet];
    [btnTitleWorkingDate addTarget: self action: @selector(selectDateForAddingEmpWorkingTime:)forControlEvents: UIControlEventTouchDown];

    
    [viewEmpSheetOnScroll addSubview:btnTitleWorkingDate];
    
    
    [_scrollViewAddEmpWorkingTime addSubview:viewEmpSheetOnScroll];
    [_scrollViewAddEmpWorkingTime setContentSize:CGSizeMake(scrollViewWidth,150)];
    _scrollViewAddEmpWorkingTime.scrollEnabled=true;
    _scrollViewAddEmpWorkingTime.showsHorizontalScrollIndicator=true;
    _scrollViewAddEmpWorkingTime.showsVerticalScrollIndicator=true;
    
    [_scrollViewAddEmpWorkingTime addSubview:viewEmpSheetOnScroll];
    
}

-(void)selectDateForAddingEmpWorkingTime :(id)sender{
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"CRMiPad" bundle:nil];
    DateTimePicker  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DateTimePicker"];
    objSignViewController.strTag = 1;
    objSignViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    objSignViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    objSignViewController.strType = @"Date";
    objSignViewController.handleDateSelectionForDatePickerProtocol = self;
    NSString *strBtnDate = btnTitleWorkingDate.titleLabel.text;
    NSDate *dateee = [global changeStringDateToDate:strBtnDate];
    
    if (dateee != nil) {
        
        objSignViewController.dateToSet = dateee;
        
    }
    
    [self presentViewController:objSignViewController animated:YES completion:nil];
    
}


//============================================================================
//============================================================================
#pragma mark- ----------------Date Picker Protocol Method----------------
//============================================================================
//============================================================================

- (void)setDateSelctionForDatePickerProtocolWithStrDate:(NSString * _Nonnull)strDate tag:(NSInteger)tag {
    
    [btnTitleWorkingDate setTitle:strDate forState:UIControlStateNormal];
    
}

//============================================================================
//============================================================================
#pragma mark- ----------------Helper Picker Protocol Method----------------
//============================================================================
//============================================================================

- (void)getDataFromPopupDelegateWithDictData:(NSDictionary * _Nonnull)dictData tag:(NSInteger)tag {
    
   strHelperIdGlobal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EmployeeNo"]];
    
    if ([strEmployeeNoLoggedIn isEqualToString:strHelperIdGlobal]) {
        
        strHelperIdGlobal=@"";
        [global AlertMethod:Alert :@"Main technician can't be selected as helper"];
        
    } else {
        
        [_btnSelectHelper setTitle:[dictData valueForKey:@"FullName"] forState:UIControlStateNormal];
        
    }

}


-(void)addServiceHelperFromMobileToDB{
    
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    
    MechanicalSubWOTechHelperDcs *objSubWorkOrderHelper = [[MechanicalSubWOTechHelperDcs alloc]initWithEntity:entityMechanicalSubWorkOrderHelper insertIntoManagedObjectContext:context];
    
    objSubWorkOrderHelper.workorderId=strWorkOrderId;
    objSubWorkOrderHelper.subWorkOrderTechnicianId=[global getReferenceNumber];
    objSubWorkOrderHelper.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderHelper.employeeNo=[NSString stringWithFormat:@"%@",strHelperIdGlobal];
    objSubWorkOrderHelper.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderHelper.createdBy=[NSString stringWithFormat:@"%@",strEmpName];
    objSubWorkOrderHelper.createdDate=[global strCurrentDate];
    objSubWorkOrderHelper.modifiedBy=[NSString stringWithFormat:@"%@",strEmpName];
    objSubWorkOrderHelper.modifiedDate=[global strCurrentDate];
    objSubWorkOrderHelper.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    objSubWorkOrderHelper.laborType=[NSString stringWithFormat:@"%@",@"H"];

    NSError *error2;
    [context save:&error2];
    
}


-(void)addStartActualHoursFromMobileToDB{
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWorkOrderId;
    
    strSubWoActualHrIdGlobal = [global getReferenceNumber];
    objSubWorkOrderNotes.subWOActualHourId = strSubWoActualHrIdGlobal;
    
//    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
//    [defs setValue:strNos forKey:@"subWOActualHourId"];
//    [defs synchronize];
    
    NSString *timeInOut =[global strCurrentDateFormattedForMechanical];
    
    objSubWorkOrderNotes.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderNotes.timeIn=timeInOut;
    objSubWorkOrderNotes.mobileTimeIn=objSubWorkOrderNotes.timeIn;
    
    NSManagedObject *objSubWo=[global fetchMechanicalSubWorkOrderObj:strWorkOrderId :strSubWorkOrderIdGlobal];
    objSubWorkOrderNotes.status=[objSubWo valueForKey:@"subWOStatus"];  //subWOStatus
    
    objSubWorkOrderNotes.timeOut=timeInOut;
    objSubWorkOrderNotes.mobileTimeOut=timeInOut;
    objSubWorkOrderNotes.companyKey=strCompanyKey;
    objSubWorkOrderNotes.reason=@"";
    objSubWorkOrderNotes.actHrsDescription=@"";
    objSubWorkOrderNotes.subWorkOrderNo=@"";
    objSubWorkOrderNotes.subWOActualHourNo=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKey;
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",strEmpName];
    objSubWorkOrderNotes.createdDate=[global strCurrentDate];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",strEmpName];
    objSubWorkOrderNotes.modifiedDate=[global strCurrentDate];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    // Latitude changes
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objSubWorkOrderNotes.latitude=latitude;
    objSubWorkOrderNotes.longitude=longitude;
    // Latitude changes end
    
    objSubWorkOrderNotes.employeeNo=[global getEmployeeDeatils];
    
    NSError *error2;
    [context save:&error2];

}

@end
