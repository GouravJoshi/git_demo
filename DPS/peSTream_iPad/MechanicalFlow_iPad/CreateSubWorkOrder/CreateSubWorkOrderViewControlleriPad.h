//
//  CreateSubWorkOrderViewControlleriPad.h
//  DPS
//  peSTream
//  Created by Saavan Patidar on 16/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//  peSTream
//  peStream

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"


@interface CreateSubWorkOrderViewControlleriPad : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSEntityDescription *entityMechanicalSubWorkOrder,*entitySubWorkOrderIssues;

}
- (IBAction)action_back:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constantViewAfterHour_H;
@property (strong, nonatomic) IBOutlet UITextView *txtViewJobDescriptions;
@property (strong, nonatomic) IBOutlet UIButton *btn_Technician;
- (IBAction)action_Techician:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_CopyIssues;
- (IBAction)action_CopyIssues:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_ServiceDateTime;
- (IBAction)action_ServiceDateTime:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_JobType;
- (IBAction)action_JobType:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_FR;
- (IBAction)action_FR:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_TM;
- (IBAction)action_TM:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_StandardHr;
- (IBAction)action_StandardHr:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_AfterHr;
- (IBAction)action_AfterHr:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_HolidayHr;
- (IBAction)action_HolidayHr:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_SpecificHr;
- (IBAction)action_SpecificHr:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_SpecificHrList;
- (IBAction)action_SpecificHrList:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtEstimatedTime;
- (IBAction)action_Save:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewSpecificHr;
@property (strong, nonatomic) NSString *strSubWorkOrderId;
@property (strong, nonatomic) NSString *strWorkOrderId;
@property (strong, nonatomic) NSString *strWorkOrderNo;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_SubWorkOrderType_H;
@property (strong, nonatomic) IBOutlet UIButton *btnMechanic;
@property (strong, nonatomic) IBOutlet UIButton *btnHelper;
- (IBAction)action_Mechanic:(id)sender;
- (IBAction)action_Helper:(id)sender;

@end
