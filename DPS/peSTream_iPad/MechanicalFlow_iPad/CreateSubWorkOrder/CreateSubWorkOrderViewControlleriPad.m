//
//  CreateSubWorkOrderViewControlleriPad.m
//  DPS
//
//  Created by Saavan Patidar on 16/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "CreateSubWorkOrderViewControlleriPad.h"

#import "AllImportsViewController.h"

@interface CreateSubWorkOrderViewControlleriPad ()
{
    Global *global;
    NSMutableArray *arrDataTblView,*arrOfHoursConfig;
    BOOL isFR, isStandardHR, isHolidayHR, isMechanic;
    UITableView *tblData;
    NSDictionary *dictDetailsFortblView;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    NSString *strDate,*strEmpID,*strUserName,*strCompanyKey,*strServiceUrlMain,*strEmployeeNo,*strDepartmentSysName,*strWorkOrderAccNo;

}

@end

@implementation CreateSubWorkOrderViewControlleriPad

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    global = [[Global alloc] init];
    _constantViewAfterHour_H.constant=0;
    [_viewSpecificHr setHidden:YES];
    [_btn_SpecificHrList setHidden:YES];
    isFR=YES;
    isStandardHR=YES;
    isHolidayHR=YES;
    isMechanic=YES;
    
    [_btn_FR setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btn_TM setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    _const_SubWorkOrderType_H.constant=0;
    _constantViewAfterHour_H.constant=0;
    
    [_btn_StandardHr setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btn_HolidayHr setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDateAndTime;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate:[NSDate date]];
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [_scrollView addGestureRecognizer:singleTapGestureRecognizer];
    
    //============================================================================
    //============================================================================

    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];//
    strEmployeeNo     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];

    [self methodBorderColor];
    
    [self getHoursConfiFromMaster];
    
    
    // Setting Default time on service date and time
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm"];
    strDate = [dateFormat stringFromDate:[NSDate date]];
    [_btn_ServiceDateTime setTitle:strDate forState:UIControlStateNormal];
    //@"Repair/Remodeling Restoration"
    [_btn_JobType setTitle:@"Repair/Remodeling Restoration" forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)singleTap:(UITapGestureRecognizer *)gesture {
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}
-(void)tapDetectedOnBackGroundView{
    [viewBackGround removeFromSuperview];
}

//================================================
//================================================
#pragma mark - Button Action Methods
//================================================
//================================================

- (IBAction)action_back:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"AddSubWorkOrder"];
    [defs synchronize];

    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)action_Techician:(id)sender {
    
    [self.view endEditing:YES];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
    
    for (int k=0; k<arrOfEmployee.count; k++) {
        
        NSDictionary *dictDataa=arrOfEmployee[k];
        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
        if (isActive) {
            
            [arrDataTblView addObject:dictDataa];
            
        }
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=105;
        [self tableLoad:tblData.tag];
    }

}
- (IBAction)action_CopyIssues:(id)sender {
}
- (IBAction)action_ServiceDateTime:(id)sender {
    
    [self.view endEditing:YES];
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    tblData.tag=109;
    [self addPickerViewDateTo :@""];

}
- (IBAction)action_JobType:(id)sender {
    
    [self.view endEditing:YES];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    [arrDataTblView addObject:@"New Construction"];
    [arrDataTblView addObject:@"Repair/Remodeling Restoration"];
//    [arrDataTblView addObject:@"NewConstruction"];
//    [arrDataTblView addObject:@"Remodeling"];
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=102;
        [self tableLoad:tblData.tag];
    }

}
- (IBAction)action_FR:(id)sender {

    [self.view endEditing:YES];
    _const_SubWorkOrderType_H.constant=50;
    isFR=YES;
    
    if (!isStandardHR) {
        
        [_viewSpecificHr setHidden:NO];
        _constantViewAfterHour_H.constant=70;
        
    }
    [_btn_FR setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btn_TM setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

}
- (IBAction)action_TM:(id)sender {
    
    [self.view endEditing:YES];
    _const_SubWorkOrderType_H.constant=0;
    _constantViewAfterHour_H.constant=0;
    isFR=NO;
    [_btn_TM setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btn_FR setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

}
- (IBAction)action_StandardHr:(id)sender {
    
    [self.view endEditing:YES];
    isStandardHR=YES;
    [_viewSpecificHr setHidden:YES];
    [_btn_StandardHr setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btn_AfterHr setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    _constantViewAfterHour_H.constant=0;

}
- (IBAction)action_AfterHr:(id)sender {
    
    [self.view endEditing:YES];
    isStandardHR=NO;
    [_viewSpecificHr setHidden:NO];
    [_btn_AfterHr setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btn_StandardHr setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    _constantViewAfterHour_H.constant=70;

}
- (IBAction)action_HolidayHr:(id)sender {
    
    [self.view endEditing:YES];
    isHolidayHR=YES;
    [_btn_SpecificHrList setHidden:YES];
    [_btn_HolidayHr setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btn_SpecificHr setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

}
- (IBAction)action_SpecificHr:(id)sender {
    
    [self.view endEditing:YES];
    isHolidayHR=NO;
    [_btn_SpecificHrList setHidden:NO];
    [_btn_SpecificHr setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btn_HolidayHr setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

}
- (IBAction)action_SpecificHrList:(id)sender {
    
    [self.view endEditing:YES];
    if (arrOfHoursConfig.count==0) {
        
        [global AlertMethod:Info :NoDataAvailable];
        
    } else {
        
        arrDataTblView=[[NSMutableArray alloc]init];

        NSDictionary *dictDataHours=arrOfHoursConfig[0];
        
        NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
        
        [arrDataTblView addObjectsFromArray:arrOfAfterHourRateConfigDcs];
        
        if (arrDataTblView.count==0) {
            [global AlertMethod:Info :NoDataAvailable];
        }else{
            tblData.tag=103;
            [self tableLoad:tblData.tag];
        }
    }
}
- (IBAction)action_Save:(id)sender {
    
    [self.view endEditing:YES];
    BOOL isNet=[global isNetReachable];
    
    if (isNet) {
        
        BOOL isSelectedSubWorkOrderRate = [self checkIfSelectedSubWoType];
        
        if (_txtViewJobDescriptions.text.length==0) {
            
            [global AlertMethod:Alert :@"Please provide serivce issue"];
            
        } else if ([_btn_Technician.titleLabel.text isEqualToString:@"---Select---"]) {
            
            [global AlertMethod:Alert :@"Please select technician"];
            
        } else if ([_btn_ServiceDateTime.titleLabel.text isEqualToString:@"---Select---"]) {
            
            [global AlertMethod:Alert :@"Please select date & time"];
            
        } else if ([_btn_JobType.titleLabel.text isEqualToString:@"---Select---"]) {
            
            [global AlertMethod:Alert :@"Please select job type"];
            
        } else if (!isSelectedSubWorkOrderRate) {
            
            [global AlertMethod:Alert :@"Please provide sub work order rate"];
            
        }else if (_txtEstimatedTime.text.length==0) {
            
            [global AlertMethod:Alert :@"Please provide estimated time to complete service"];
            
        } else if (_txtEstimatedTime.text.length<5) {
            
            [global AlertMethod:Alert :@"Please provide estimated time to complete service in hh:mm(00:00) format"];
            
        } else if((!isStandardHR) && (!isHolidayHR)){
            
            if ([_btn_SpecificHrList.titleLabel.text isEqualToString:@"---Select---"]) {
                
                [global AlertMethod:Alert :@"Please select specific hours"];
                
            } else {
                
                [self checkifTimeEnteredIsWrong];
                
            }
            
        } else{
            
            [self checkifTimeEnteredIsWrong];
            
        }
    } else {
        [global AlertMethod:Alert :ErrorInternetMsg];
    }
}


-(void)checkifTimeEnteredIsWrong{
    
    if (_txtEstimatedTime.text.length>=5) {
        
        NSArray *arrTime=[_txtEstimatedTime.text componentsSeparatedByString:@":"];
        
        NSString *strHrs,*strMinutes;
        
        if (arrTime.count==1) {
            
            strHrs=arrTime[0];
            strMinutes=@"";
            
        }
        
        if (arrTime.count==2) {
            
            strHrs=arrTime[0];
            strMinutes=arrTime[1];
            
        }
        
        int strMinutesInt=[strMinutes intValue];
        
        if (strMinutesInt>59) {
            
            [global AlertMethod:Alert :@"Please provide estimated minutes less then 60"];

            
        } else {
            
            [self finalSaveWorkOrder];
            
        }
        
    }else{
        
        [self finalSaveWorkOrder];
        
    }
}

//================================================
//================================================
#pragma mark - Methods
//================================================
//================================================

-(void)getHoursConfiFromMaster{
    
    NSManagedObject *objWorkOrder=[global fetchMechanicalWorkOrderObj:_strWorkOrderId];

    strDepartmentSysName=[NSString stringWithFormat:@"%@",[objWorkOrder valueForKey:@"departmentSysName"]];
    strWorkOrderAccNo=[NSString stringWithFormat:@"%@",[objWorkOrder valueForKey:@"accountNo"]];
    NSString *strWorkOrderAddressId=[NSString stringWithFormat:@"%@",[objWorkOrder valueForKey:@"serviceAddressId"]];
    NSString *strWorkOrderAddressSubType=[NSString stringWithFormat:@"%@",[objWorkOrder valueForKey:@"addressSubType"]];

    arrOfHoursConfig=nil;
    arrOfHoursConfig=[[NSMutableArray alloc]init];
    arrOfHoursConfig=[global getHoursConfiFromMaster:strCompanyKey :strDepartmentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType];
    
}

-(void)finalSaveWorkOrder{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Submitting please wait..."];
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MM/dd/yyyy HH:mm"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strCurrentDate = [formatterDate stringFromDate:[NSDate date]];

    
    NSDateFormatter *formatterDate1 = [[NSDateFormatter alloc] init];
    [formatterDate1 setDateFormat:@"MMddyyyy"];
    [formatterDate1 setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDateNew = [formatterDate1 stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString *strMobileUniqueId = [NSString stringWithFormat:@"iOS%@%@%@",_strWorkOrderId,strDateNew,strTime];

    NSArray *objects1 = [NSArray arrayWithObjects:
                        strUserName,
                        @"Mobile",
                        strCurrentDate,
                        @"",
                        ([_txtViewJobDescriptions.text isEqualToString:@""]) ? @"" : _txtViewJobDescriptions.text,
                        @"",
                        strMobileUniqueId,
                        @"true",
                        @"",
                        @"",
                        strUserName,
                        strCurrentDate,
                        @"High",
                        @"",
                        ([_txtViewJobDescriptions.text isEqualToString:@""]) ? @"" : _txtViewJobDescriptions.text,
                        @"",
                        @"",
                        @"",nil];
    
    NSArray *keys1 = [NSArray arrayWithObjects:
                     @"CreatedBy",
                     @"CreatedByDevice",
                     @"CreatedDate",
                     @"EquipmentCode",
                     @"EquipmentCustomName",
                     @"EquipmentName",
                     @"EquipmentNo",
                     @"IsActive",
                     @"Manufacturer",
                     @"ModelNumber",
                     @"ModifiedBy",
                     @"ModifiedDate",
                     @"Priority",
                     @"SerialNumber",
                     @"ServiceIssue",
                     @"SubWorkOrderId",
                     @"SubWorkOrderIssueId",
                     @"WorkorderId",nil];
    
    NSDictionary *dict_ToSend1 = [[NSDictionary alloc] initWithObjects:objects1 forKeys:keys1];

    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    [arrTemp addObject:dict_ToSend1];
    
    
    
    int hrsTime=[global ChangeTimeMechanicalInvoice:_txtEstimatedTime.text];
    
    hrsTime=hrsTime/60;
    
    
    NSString  *strSubWoType,*strPropertyType,*strHolidayHr,*strIsStandard,*strAfterHrsDuration,*strLaborType;
    
    if (isFR) {
        strSubWoType=@"FR";
    } else {
        strSubWoType=@"TM";
    }
    
    if (isHolidayHR) {
        strHolidayHr=@"true";
    } else {
        strHolidayHr=@"false";
    }
    
    if (isStandardHR) {
        strIsStandard=@"true";
    } else {
        strIsStandard=@"false";
    }

    if (isMechanic) {
        strLaborType=@"L";
    } else {
        strLaborType=@"H";
    }
    
    [arrDataTblView addObject:@"New Construction"];
    [arrDataTblView addObject:@"Repair/Remodeling Restoration"];
    //    [arrDataTblView addObject:@"NewConstruction"];
    //    [arrDataTblView addObject:@"Remodeling"];

    if ([_btn_JobType.titleLabel.text isEqualToString:@"New Construction"]) {
        strPropertyType=@"NewConstruction";
    } else if ([_btn_JobType.titleLabel.text isEqualToString:@"Repair/Remodeling Restoration"]){
        strPropertyType=@"Remodeling";
    } else{
        strPropertyType=@"";
    }
    
    
    if((!isStandardHR) && (!isHolidayHR)){
        
        strAfterHrsDuration=_btn_SpecificHrList.titleLabel.text;
        
    }else{
        
        strAfterHrsDuration=@"";
        
    }
    
    if ([strSubWoType isEqualToString:@"TM"]) {
        
        strIsStandard=@"true";
        
    }
        
    NSMutableArray *objects = [NSMutableArray arrayWithObjects:
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        @"",
                        strCompanyKey,
                        @"false",
                        strUserName,
                        @"Mobile",
                        strCurrentDate,
                        strEmployeeNo,
                        @"true",
                        @"false",
                        @"false",
                        @"false",
                        strHolidayHr,
                        @"false",
                        strIsStandard,
                        ([_txtViewJobDescriptions.text isEqualToString:@""]) ? @"" : _txtViewJobDescriptions.text,
                        strUserName,
                        strCurrentDate,
                        strPropertyType,
                        strDate,
                        @"New",
                        strSubWoType,
                        _txtEstimatedTime.text,
                        [NSString stringWithFormat:@"%d",hrsTime],
                        _strWorkOrderId,
                        strAfterHrsDuration,
                        arrTemp,
                        strLaborType,nil];
    
    NSArray *keys = [NSArray arrayWithObjects:
                     @"AccountPSPId",
                     @"ActualTime",
                     @"ActualTimeInt",
                     @"AmtPaid",
                     @"CalculatedActualTimeInt",
                     @"ClockStatus",
                     @"CompleteSWO_CustSignPath",
                     @"CompleteSWO_TechSignPath",
                     @"CustomerSignaturePath",
                     @"DiagnosticChargeMasterId",
                     @"DiscountAmt",
                     @"Inspectionresults",
                     @"InvoiceAmt",
                     @"InvoicePayType",
                     @"IsCompleted",
                     @"IsCustomerNotPresent",
                     @"LaborPercent",
                     @"MiscChargeAmt",
                     @"OtherDiagnosticChargesAmt",
                     @"OtherTripChargesAmt",
                     @"PSPCharges",
                     @"PSPDiscount",
                     @"PSPMasterId",
                     @"PartPercent",
                     @"PriceNotToExceed",
                     @"ServiceStartEndTime",
                     @"ServiceStartStartTime",
                     @"SubWorkOrderId",
                     @"SubWorkOrderIssueId",
                     @"SubWorkOrderNo",
                     @"TaxAmt",
                     @"TechnicianSignaturePath",
                     @"TotalApprovedAmt",
                     @"TripChargeMasterId",
                     @"CompanyKey",
                     @"CompleteSWO_IsCustomerNotPresent",
                     @"CreatedBy",
                     @"CreatedByDevice",
                     @"CreatedDate",
                     @"EmployeeNo",
                     @"IsActive",
                     @"IsClientApprovalReq",
                     @"IsClientApproved",
                     @"IsCreateWOforInCompleteIssue",
                     @"IsHolidayHrs",
                     @"IsSendDocWithOutSign",
                     @"IsStdPrice",
                     @"JobDesc",
                     @"ModifiedBy",
                     @"ModifiedDate",
                     @"PropertyType",
                     @"ServiceDateTime",
                     @"SubWOStatus",
                     @"SubWOType",
                     @"TotalEstimationTime",
                     @"TotalEstimationTimeInt",
                     @"WorkorderId",
                     @"AfterHrsDuration",
                     @"SubWorkOrderIssueDcs",
                     @"LaborType",nil];
    
    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Sub Work Order Json: %@", jsonString);
        }
        
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];

    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlSubmitSubWorkOrder];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"POST"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    [requestLocal setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [requestLocal setHTTPBody:requestData];

    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             
             if ((ResponseDict==nil) || ResponseDict.count==0) {
                 
                 [global AlertMethod:Alert :Sorry];
                 [DejalBezelActivityView removeView];
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDict =dictData;
                 
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                                  
                 if (strException.length==0) {
                     
                     NSString *strStatus=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"status"]];
                     NSString *strMsgResponse=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"Message"]];

                     if ([strStatus isEqualToString:@"1"] || [strStatus caseInsensitiveCompare:@"true"] == NSOrderedSame) {
                         
                         NSDictionary *dictSubWorKorder=[ResponseDict valueForKey:@"SubWorkOrderExtSerDc"];
                         
                         NSArray *arrSubWorKorderIssues=[dictSubWorKorder valueForKey:@"SubWorkOrderIssueDcs"];
                         
                         [self saveSubWorkOrderInDb :dictSubWorKorder :arrSubWorKorderIssues];
                         
                         [DejalBezelActivityView removeView];
                         
                         [global AlertMethod:Alert :strMsgResponse];
                         
                         [self dismissViewControllerAnimated:YES completion:nil];


                     } else {
                         
                         [DejalBezelActivityView removeView];

                         [global AlertMethod:Alert :strMsgResponse];
                         
                     }
                     
                     /*
                     NSArray *arrResponse=(NSArray*)ResponseDict;
                     
                     for (int k=0; k<arrResponse.count; k++) {
                         
                         NSDictionary *dictData=arrResponse[k];
                         NSString *strKey=[dictData valueForKey:@"Key"];
                         if ([strKey isEqualToString:@"Status"]) {
                             strStatusResponse=[dictData valueForKey:@"Value"];
                         }
                         if ([strKey isEqualToString:@"Msg"]) {
                             strMsgResponse=[dictData valueForKey:@"Value"];
                         }
                         if ([strKey isEqualToString:@"SubWorkOrderNo"]) {
                             strSubWorkOrderNoResponse=[dictData valueForKey:@"Value"];
                         }
                         if ([strKey isEqualToString:@"SubWorkOrderId"]) {
                             strSubWorkOrderIdResponse=[dictData valueForKey:@"Value"];
                         }
                         if ([strKey isEqualToString:@"SubWorkOrderIssueId"]) {
                             strSubWorkOrderIssueId=[dictData valueForKey:@"Value"];
                         }

                     }
                     
                     
                     if ([strStatusResponse caseInsensitiveCompare:@"true"] == NSOrderedSame) {
                         
                         //Save Workorder In DB

                         if (strSubWorkOrderIssueId.length==0) {
                             strSubWorkOrderIssueId=@"";
                         }
                         [self saveSubWorkOrderInDb:strSubWorkOrderNoResponse :strSubWorkOrderIdResponse :dict_ToSend :dict_ToSend1:strSubWorkOrderIssueId];

                         [DejalBezelActivityView removeView];

                         [self dismissViewControllerAnimated:YES completion:nil];
                         
                     } else {
                         
                         [global AlertMethod:Alert :strMsgResponse];
                         
                     }
                     */
                     
                     [DejalBezelActivityView removeView];
                     
                 } else {
                     
                     ResponseDict=nil;
                     [global AlertMethod:Alert :Sorry];
                     [DejalBezelActivityView removeView];
                     
                 }
                 
             }
             
         }];
    }
    @catch (NSException *exception) {
        
        [global AlertMethod:Alert :Sorry];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }

}

-(void)saveSubWorkOrderInDb :(NSDictionary*)dictOfSubWorkOrderInfo :(NSArray*)arrOfSubWorkOrderIssuesInfo{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];

    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    
    MechanicalSubWorkOrder *objSubWorkOrder = [[MechanicalSubWorkOrder alloc]initWithEntity:entityMechanicalSubWorkOrder insertIntoManagedObjectContext:context];
    
    objSubWorkOrder.workorderId=_strWorkOrderId;
    objSubWorkOrder.departmentSysName =[NSString stringWithFormat:@"%@",strDepartmentSysName];
    objSubWorkOrder.companyKey=strCompanyKey;
    objSubWorkOrder.accountNo=strWorkOrderAccNo;
    objSubWorkOrder.workOrderNo=_strWorkOrderNo;
    objSubWorkOrder.actualTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualTime"]];
    objSubWorkOrder.amtPaid=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AmtPaid"]];
    objSubWorkOrder.companyKey=strCompanyKey;
    objSubWorkOrder.completeSWO_CustSignPath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_CustSignPath"]];
    objSubWorkOrder.completeSWO_IsCustomerNotPresent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_IsCustomerNotPresent"]];
    objSubWorkOrder.completeSWO_TechSignPath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_TechSignPath"]];
    objSubWorkOrder.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedBy"]];
    objSubWorkOrder.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedDate"]]];
    objSubWorkOrder.customerSignaturePath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CustomerSignaturePath"]];
    objSubWorkOrder.diagnosticChargeMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"DiagnosticChargeMasterId"]];
    objSubWorkOrder.discountAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"DiscountAmt"]];
    objSubWorkOrder.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"EmployeeNo"]];
    objSubWorkOrder.invoiceAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoiceAmt"]];
    objSubWorkOrder.invoicePayType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoicePayType"]];
    objSubWorkOrder.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsActive"]]];
    objSubWorkOrder.isActive=@"true";
    objSubWorkOrder.isClientApprovalReq=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsClientApprovalReq"]]];
    objSubWorkOrder.isClientApproved=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsClientApproved"]]];
    objSubWorkOrder.isCompleted=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCompleted"]]];
    objSubWorkOrder.isCustomerNotPresent=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCustomerNotPresent"]]];
    objSubWorkOrder.isStdPrice=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsStdPrice"]]];
    objSubWorkOrder.jobDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"JobDesc"]];
    objSubWorkOrder.laborPercent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborPercent"]];
    objSubWorkOrder.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ModifiedBy"]];
    objSubWorkOrder.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ModifiedDate"]]];
    objSubWorkOrder.otherDiagnosticChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"OtherDiagnosticChargesAmt"]];
    objSubWorkOrder.otherTripChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"OtherTripChargesAmt"]];
    objSubWorkOrder.partPercent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPercent"]];
    objSubWorkOrder.priceNotToExceed=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PriceNotToExceed"]];
    objSubWorkOrder.serviceDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceDateTime"]]];
    objSubWorkOrder.serviceStartEndTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceStartEndTime"]];
    objSubWorkOrder.serviceStartStartTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceStartStartTime"]];
    objSubWorkOrder.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
    
    objSubWorkOrder.subWorkOrderIssueId=@"";
    objSubWorkOrder.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNo"]];
    objSubWorkOrder.subWOStatus=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOStatus"]];
    objSubWorkOrder.subWOType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOType"]];
    objSubWorkOrder.taxAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TaxAmt"]];
    objSubWorkOrder.technicianSignaturePath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TechnicianSignaturePath"]];
    objSubWorkOrder.totalApprovedAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalApprovedAmt"]];
    objSubWorkOrder.totalEstimationTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalEstimationTime"]];
    objSubWorkOrder.totalEstimationTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalEstimationTimeInt"]];
    objSubWorkOrder.tripChargeMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TripChargeMasterId"]];
    objSubWorkOrder.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedByDevice"]];
    
    objSubWorkOrder.pSPDiscount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPDiscount"]];
    
    objSubWorkOrder.pSPCharges=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPCharges"]];
    
    objSubWorkOrder.invoiceAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoiceAmt"]];
    
    objSubWorkOrder.accountPSPId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AccountPSPId"]];
    
    objSubWorkOrder.pSPMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPMasterId"]];
    
    objSubWorkOrder.inspectionresults=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"Inspectionresults"]];
    objSubWorkOrder.propertyType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PropertyType"]];
    
    objSubWorkOrder.addressType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AddressType"]];
    objSubWorkOrder.isLaborTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsLaborTaxable"]];
    objSubWorkOrder.isPartTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsPartTaxable"]];
    objSubWorkOrder.partPriceType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPriceType"]];
    objSubWorkOrder.isVendorPayTax=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsVendorPayTax"]];
    objSubWorkOrder.isChargeTaxOnFullAmount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsChargeTaxOnFullAmount"]];
    objSubWorkOrder.afterHrsDuration=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AfterHrsDuration"]];
    objSubWorkOrder.isHolidayHrs=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsHolidayHrs"]];
    objSubWorkOrder.laborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborHrsPrice"]];
    objSubWorkOrder.mileageChargesName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesName"]];
    objSubWorkOrder.totalMileage=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalMileage"]];
    objSubWorkOrder.mileageChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesAmt"]];
    objSubWorkOrder.miscChargeAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MiscChargeAmt"]];
    objSubWorkOrder.actualTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualTimeInt"]];
    
    objSubWorkOrder.isCreateWOforInCompleteIssue=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCreateWOforInCompleteIssue"]];
    objSubWorkOrder.isSendDocWithOutSign=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsSendDocWithOutSign"]];
    objSubWorkOrder.calculatedActualTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CalculatedActualTimeInt"]];
    objSubWorkOrder.actualLaborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualLaborHrsPrice"]];
    objSubWorkOrder.clockStatus=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ClockStatus"]];
    objSubWorkOrder.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
    objSubWorkOrder.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNo"]];
    
    //setting null values to blank
    
    objSubWorkOrder.actualLaborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualLaborHrsPrice"]];
    objSubWorkOrder.addressType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AddressType"]];
    objSubWorkOrder.isChargeTaxOnFullAmount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsChargeTaxOnFullAmount"]];
    objSubWorkOrder.isLaborTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsLaborTaxable"]];
    objSubWorkOrder.isPartTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsPartTaxable"]];
    objSubWorkOrder.isVendorPayTax=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsVendorPayTax"]];
    objSubWorkOrder.laborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborHrsPrice"]];
    objSubWorkOrder.mileageChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesAmt"]];
    objSubWorkOrder.mileageChargesName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesName"]];
    objSubWorkOrder.partPriceType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPriceType"]];
    objSubWorkOrder.totalMileage=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalMileage"]];
    objSubWorkOrder.tripChargesQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TripChargesQty"]];

    if (isMechanic) {
        objSubWorkOrder.laborType=[NSString stringWithFormat:@"%@",@"L"];
    } else {
        objSubWorkOrder.laborType=[NSString stringWithFormat:@"%@",@"H"];
    }
    objSubWorkOrder.isIncludeDetailOnInvoice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsIncludeDetailOnInvoice"]];
    
    NSError *error222;
    [context save:&error222];

    for (int k=0; k<arrOfSubWorkOrderIssuesInfo.count; k++) {
        
        NSDictionary *dictSubworkorderIssues=arrOfSubWorkOrderIssuesInfo[k];
        
        [self saveSUbWorkOrderIssues :dictSubworkorderIssues];
        
    }
    
}

-(void)saveSUbWorkOrderIssues :(NSDictionary*)dictOfSubWorkOrderIssuesInfo{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];//ServiceDynamicForm

    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderIssueDcs *objSubWorkOrderIssues = [[MechanicalSubWorkOrderIssueDcs alloc]initWithEntity:entitySubWorkOrderIssues insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=_strWorkOrderId;
    objSubWorkOrderIssues.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
    objSubWorkOrderIssues.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderId"]];
    objSubWorkOrderIssues.serviceIssue=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ServiceIssue"]];
    objSubWorkOrderIssues.priority=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"Priority"]];
    objSubWorkOrderIssues.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"IsActive"]]];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedBy"]];
    objSubWorkOrderIssues.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedDate"]]];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModifiedBy"]];
    objSubWorkOrderIssues.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModifiedDate"]]];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedByDevice"]];
    
    objSubWorkOrderIssues.equipmentCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentCode"]];
    objSubWorkOrderIssues.equipmentName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentName"]];
    objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SerialNumber"]];
    objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModelNumber"]];
    objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"Manufacturer"]];
    objSubWorkOrderIssues.equipmentCustomName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentCustomName"]];
    objSubWorkOrderIssues.equipmentNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentNo"]];
    
    NSError *error2;
    [context save:&error2];

}
-(void)methodBorderColor{
    
    _txtViewJobDescriptions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewJobDescriptions.layer.borderWidth=1.0;
    _txtViewJobDescriptions.layer.cornerRadius=5.0;

    [_btn_Technician.layer setCornerRadius:5.0f];
    [_btn_Technician.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_Technician.layer setBorderWidth:0.8f];
    [_btn_Technician.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_Technician.layer setShadowOpacity:0.3];
    [_btn_Technician.layer setShadowRadius:3.0];
    [_btn_Technician.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    [_btn_CopyIssues.layer setCornerRadius:5.0f];
    [_btn_CopyIssues.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_CopyIssues.layer setBorderWidth:0.8f];
    [_btn_CopyIssues.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_CopyIssues.layer setShadowOpacity:0.3];
    [_btn_CopyIssues.layer setShadowRadius:3.0];
    [_btn_CopyIssues.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    [_btn_ServiceDateTime.layer setCornerRadius:5.0f];
    [_btn_ServiceDateTime.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_ServiceDateTime.layer setBorderWidth:0.8f];
    [_btn_ServiceDateTime.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_ServiceDateTime.layer setShadowOpacity:0.3];
    [_btn_ServiceDateTime.layer setShadowRadius:3.0];
    [_btn_ServiceDateTime.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    [_btn_JobType.layer setCornerRadius:5.0f];
    [_btn_JobType.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_JobType.layer setBorderWidth:0.8f];
    [_btn_JobType.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_JobType.layer setShadowOpacity:0.3];
    [_btn_JobType.layer setShadowRadius:3.0];
    [_btn_JobType.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    [_btn_SpecificHrList.layer setCornerRadius:5.0f];
    [_btn_SpecificHrList.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_SpecificHrList.layer setBorderWidth:0.8f];
    [_btn_SpecificHrList.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_SpecificHrList.layer setShadowOpacity:0.3];
    [_btn_SpecificHrList.layer setShadowRadius:3.0];
    [_btn_SpecificHrList.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    [_btn_SpecificHrList.layer setCornerRadius:5.0f];
    [_btn_SpecificHrList.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_SpecificHrList.layer setBorderWidth:0.8f];
    [_btn_SpecificHrList.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_SpecificHrList.layer setShadowOpacity:0.3];
    [_btn_SpecificHrList.layer setShadowRadius:3.0];
    [_btn_SpecificHrList.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

}

-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 102:
        {
            [self setTableFrame];
            break;
        }
        case 103:
        {
            [self setTableFrame];
            break;
        }
        case 104:
        {
            [self setTableFrame];
            break;
        }
        case 105:
        {
            [self setTableFrame];
            break;
        }
        default:
            break;
    }
    //[tblData setContentOffset:CGPointZero animated:YES];
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}


-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x+tblData.frame.size.width/2-50, tblData.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x+tblData.frame.size.width/2-50, tblData.frame.origin.y+tblData.frame.size.height+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x+tblData.frame.size.width/2-70, tblData.frame.origin.y+tblData.frame.size.height+10, 70, 30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x+tblData.frame.size.width/2-100, tblData.frame.origin.y+tblData.frame.size.height+10, 200, 50)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.titleLabel.font=[UIFont systemFontOfSize:25];
    
    btnClose.backgroundColor=[UIColor themeColorBlack];;

    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];

    [viewBackGround addSubview:btnClose];
}

//================================================
//================================================
#pragma mark- **************** Textfield Delegate Methods ****************
//================================================
//================================================
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)texth

{
    if (range.location == 0 && [texth isEqualToString:@" "]) {
        return NO;
    }
    else
        return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }else if (textField==_txtEstimatedTime){
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        else
        {
            NSString *text = textField.text;
            NSInteger length = text.length;
            BOOL shouldReplace = YES;
            
            if (![string isEqualToString:@""])
            {
                switch (length)
                {
                    case 2:
                        textField.text = [text stringByAppendingString:@":"];
                        break;
                        
                    default:
                        break;
                }
                if (length > 4)
                    shouldReplace = NO;
            }
            
            return shouldReplace;
        }
        return YES;
        
    }
    else
    {
        return YES;
    }
}


//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrDataTblView count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if (!(arrDataTblView.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 102:
            {
                //NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                break;
            }
            case 103:
            {
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                
                NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"FromTime"]];
                NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ToTime"]];
                
                NSString *strCombnedString=[NSString stringWithFormat:@"%@ - %@",strFromTimeAHRToCompare,strToTimeAHRToCompare];

                cell.textLabel.text=strCombnedString;
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                break;
            }
            case 104:
            {
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                cell.textLabel.text=[dictData valueForKey:@"Name"];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                break;
            }
            case 105:
            {
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                cell.textLabel.text=[dictData valueForKey:@"FullName"];
                NSString *str=[dictData valueForKey:@"FullName"];
                
                long index=10000000;
                
//                for (int k=0; k<arraySelecteValues.count; k++) {
//                    if ([arraySelecteValues containsObject:str]) {
//                        index=indexPath.row;
//                        break;
//                    }
//                }
                
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                break;
            }
                
            default:
                break;
        }
        
    }
    if ([UIScreen mainScreen].bounds.size.height==667) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }
    else
        cell.textLabel.font=[UIFont systemFontOfSize:15];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!(arrDataTblView.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 102:
            {
                //NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                [_btn_JobType setTitle:[arrDataTblView objectAtIndex:indexPath.row] forState:UIControlStateNormal];

                break;
            }
            case 103:
            {
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"FromTime"]];
                NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ToTime"]];
                
                NSString *strCombnedString=[NSString stringWithFormat:@"%@ - %@",strFromTimeAHRToCompare,strToTimeAHRToCompare];
                
                [_btn_SpecificHrList setTitle:strCombnedString forState:UIControlStateNormal];


                break;
            }
            case 104:
            {
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                break;
            }
            case 105:
            {
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                [_btn_Technician setTitle:[dictData valueForKey:@"FullName"] forState:UIControlStateNormal];
                strEmployeeNo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EmployeeNo"]];
                break;
            }
                
            default:
                break;
        }
        
    }
    
        [tblData removeFromSuperview];
        [viewBackGround removeFromSuperview];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addPickerViewDateTo :(NSString*)strDateString
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDateAndTime;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    // [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strDateString.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm"];
        NSDate *dateToSett = [dateFormat dateFromString:strDateString];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    
        pickerDate.datePickerMode = UIDatePickerModeDateAndTime;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4-70, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSInteger i;
    i=tblData.tag;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm"];
    strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    
    if (yesSameDay) {
        
        [self settingDate:i];
        
    } else {
        
        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
        
        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
            
            //[global AlertMethod:@"Alert" :@"Back dates cant be selected"];  // Removed check of past date selection
            [self settingDate:i];
            
        }else{
            
            [self settingDate:i];
            
        }
        
    }
}

-(void)settingDate :(NSInteger)tag{
    
    [_btn_ServiceDateTime setTitle:strDate forState:UIControlStateNormal];
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
}


//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

- (IBAction)action_Mechanic:(id)sender {
    
    isMechanic=YES;
    [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
}

- (IBAction)action_Helper:(id)sender {
    
    isMechanic=NO;
    [_btnHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnMechanic setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
}

-(BOOL)checkIfSelectedSubWoType{
    
    BOOL isSelected = YES;
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"RadioButton-Unselected.png"];
    
    UIImage *img = [_btn_FR imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    
    UIImage *imageToCheckFor1 = [UIImage imageNamed:@"RadioButton-Unselected.png"];
    
    UIImage *img1 = [_btn_TM imageForState:UIControlStateNormal];
    
    NSData *imgData11 = UIImagePNGRepresentation(imageToCheckFor1);
    
    NSData *imgData21 = UIImagePNGRepresentation(img1);
    
    BOOL isCompare1 =  [imgData11 isEqual:imgData21];
    
    if((isCompare==true) && (isCompare1==true))
    {
        
        isSelected = NO;
        
    }
    
    return isSelected;
    
}

@end
