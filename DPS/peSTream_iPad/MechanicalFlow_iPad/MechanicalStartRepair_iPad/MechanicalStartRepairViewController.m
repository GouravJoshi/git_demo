//
//  MechanicalStartRepairViewController.m
//  DPS  peSTream
//  peSTream
//  Created by Saavan Patidar on 31/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream

#import "MechanicalStartRepairViewController.h"
#import "DPS-Swift.h"

@interface MechanicalStartRepairViewController ()
{
    
    Global *global;
    NSString *strWorkOrderId,*strSubWorkOrderIdGlobal,*strPriorityIdGlobal,*strEmpID,*strUserName,*strCompanyKey,*strEmpName,*strHelperIdGlobal,*strRepairIdGlobal,*strDepartMentSysName,*strSubWorkOrderIssueIdGlobal,*strGlobalAmtRepair,*strWoStatus,*strCategoryMasterId,*strGlobalDateToShow,*strMultiplierGlobal,*strIssueIdGlobalSelected,*strRepairIdGlobalSelected,*strWorkOrderStatuss,*strNotificationTypeName,*strWorkOrderAccNo,*strWorkOrderAddressSubType,*strWorkOrderAddressId, *strAfterHrsDuration,*strCompanyName,*strCategorySysNameSelected,*strTxtUnitPrice;
    NSMutableArray *arrOfSubWorkServiceIssues,*arrDataTblView,*arrOfSubWorkServiceHelper,*arrOfSubWorkServiceNotes,*arrOfSubWorkServiceActualHrs,*arrOfHrsTotal,*arrOfSubWorkServiceIssuesRepair,*arrOfSubWorkServiceIssuesRepairParts,*arrOfSubWorkServiceIssuesRepairLabour,*arrOfPriceLookup,*arrOfHoursConfig,*arrOfPartsGlobalMasters,*arrOfSubWorkServiceIssuesRepairPartsAll,*arrOfBeforeImageAll,*arrOfImagenameCollewctionView,*arrOfImageCaption,*arrOfImageDescription;
    UIView *viewBackAlertt,*viewBackGround,*viewBackGroundOnView,*viewForDate,*viewBackGroundUpdateEstTime;
    UITableView *tblData;
    NSDictionary *dictDetailsFortblView,*dictDetailsMastersMechanical,*dictLaborInfoToSave,*dictDataPartsSelected;
    int indexToShow, secondsTimer, minutesTimer, hoursTimer, totalsecond ,indexRepairGlobal, intGlobalSection, indexToEditPart;
    BOOL isCollapseAllViews , isStandardRepair, isStandardSubWorkOrder, isStandard, isWarranty, isEditParts, yesEditedSomething, isImageTaken, isHoliday, isChangeStdPartPrice, isCompletedStatusMechanical, isChargeToCustomer;
    NSTimer *jobMinTimer;
    UIDatePicker *pickerDate;
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    NSArray *arrayUpdateTimeEst;
    NSString *strUpdateEstTime;
    NSMutableArray *arrOfGlobalDynamicEmpSheetFinal,*arrOfHeaderTitleForSlots;
    NSString *strEmployeeNoLoggedIn;
    
    // Vendor Details
    NSInteger itemMasterID;
    NSString *strVendorSysNameSelected;
    
    // changes for collapse expand views.
    NSMutableArray *arrOfSectionsToOpen;
    
}
@property (strong, nonatomic) IBOutlet UIButton *btnWarranty;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectCategory;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectParts;
@property (strong, nonatomic) IBOutlet UITextField *txtQty;
@property (strong, nonatomic) IBOutlet UITextField *txtUnitPrice;
@property (strong, nonatomic) IBOutlet UITextField *txtTotalPrice;
@property (strong, nonatomic) IBOutlet UITextField *txtSerial;
@property (strong, nonatomic) IBOutlet UITextField *txtModel;
@property (strong, nonatomic) IBOutlet UITextField *txtManufacturer;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectdate;
@property (strong, nonatomic) IBOutlet UITextView *txtViewDesc;
@property (strong, nonatomic) IBOutlet UIView *viewAddParts;
@property (strong, nonatomic) IBOutlet UITextField *txtpartName;
@property (strong, nonatomic) IBOutlet UILabel *lblPartSelectOrEnter;
@property (strong, nonatomic) IBOutlet UIButton *btnAddServiceIssue;
@property (strong, nonatomic) IBOutlet UIButton *btnAddNotes;
@property (strong, nonatomic) IBOutlet UIButton *btnAddHelper;
@property (strong, nonatomic) IBOutlet UIImageView *btnArrowDown;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectIssues;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectRepairParts;
@property (strong, nonatomic) IBOutlet UIButton *btnCompleteSubWorkOrder;
@property (strong, nonatomic) IBOutlet UIButton *btnRecommandation;
@property (strong, nonatomic) IBOutlet UIButton *btnAddPartss;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewEmpTimeSheet;

@end

@implementation MechanicalStartRepairViewController
@synthesize scrollViewEmpTimeSheet;

- (void)viewDidLoad {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"" forKey:@"ScannedResult"];
    [defs setBool:NO forKey:@"IsScannerView"];
    [defs synchronize];
    
    isChangeStdPartPrice=NO;
    intGlobalSection=-1;
    isImageTaken=NO;
    indexToEditPart=-1;
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    NSString *strIncludeDetailOnInvoice=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isIncludeDetailOnInvoice"]];
    
    if ([strIncludeDetailOnInvoice isEqualToString:@"1"] || [strIncludeDetailOnInvoice isEqualToString:@"true"] || [strIncludeDetailOnInvoice isEqualToString:@"True"]) {
        
        [_btnIncludeDetailOnInvoice setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    } else {
        
        [_btnIncludeDetailOnInvoice setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    arrayUpdateTimeEst = @[@"15 Mins",@"30 Mins",@"45 Mins",@"60 Mins"];
    
    _buttonUpdateEstTime.layer.cornerRadius = 2.0;
    _buttonUpdateEstTime.layer.borderWidth = 1.0;
    _buttonUpdateEstTime.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL isFromScanner=[defs boolForKey:@"IsScannerView"];
    BOOL isFromOtherPresentedView=[defs boolForKey:@"isFromOtherPresentedView"];
    
    if (isFromScanner) {
        
        NSString *strScannedResult=[defs valueForKey:@"ScannedResult"];
        
        if (strScannedResult.length>0) {
            
            [self setpartValuesAfterScanning:strScannedResult];
            
        }
        
        //[global AlertMethod:@"Scanned-Code" :strScannedResult];
        
        [defs setValue:@"" forKey:@"ScannedResult"];
        [defs setBool:YES forKey:@"IsScannerView"];
        [defs synchronize];
        
        
    }else if (isImageTaken) {
        isImageTaken=NO;
        
    }else if (isFromOtherPresentedView){
        
        [defs setBool:NO forKey:@"isFromOtherPresentedView"];
        [defs synchronize];
        
    }else{
        
        [self allMethodsCalling];
        
        [self methodViewWillAppear];
        
    }
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//============================================================================
//============================================================================
#pragma mark- ----------------Void Methods----------------
//============================================================================
//============================================================================

-(void)allMethodsCalling{
    
    [self methodAllocation];
    
    [self loadValues];
    
    // changes for collapse expand views.
    
    arrOfSectionsToOpen = [[NSMutableArray alloc]init];
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanicalToExpandCollapse];
    
    [self methodBorderColor];
    
    [self addViewOnLoading];
    
    [self LoadProfileImage];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll];
    
    [self fetchSubWorkOrderNotesFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderActualHrsFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    
    [self fetchImageDetailFromDataBaseMechanical];
    
    [self getHoursConfiFromMaster];
    
    //[self getPricLookupFromMaster];
    
    [self getPartsMaster];
    
    [self methodStatus];
    
}

-(void)hideAllTxtViewsnFields{
    
    [_txtViewIssuesDesc resignFirstResponder];
    [_txtViewReason resignFirstResponder];
    [_txtRepairAmt resignFirstResponder];
    [_txtViewAddNotesDesc resignFirstResponder];
    [_txtRepairCostAdjustment resignFirstResponder];
    [_txtViewRepairDescriptions resignFirstResponder];
    
}
-(void)methodDonePriorityTableview{
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
}

-(void)methodCancelPriorityTableview{
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
}


-(void)methodBack{
    
    [jobMinTimer invalidate];
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k1=0; k1<arrstack.count; k1++) {
        if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControlleriPad class]]) {
            index=k1;
            //break;
        }
    }
    MechanicalSubWorkOrderDetailsViewControlleriPad *myController = (MechanicalSubWorkOrderDetailsViewControlleriPad *)[self.navigationController.viewControllers objectAtIndex:index];
    // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    [self.navigationController popToViewController:myController animated:NO];
    
}


-(void)addViewOnLoading{
    
    //Adding _viewSubWorkOrderDetails
    
    CGRect frameFor_view_SubWorkOrderInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,_viewSubWorkOrderDetails.frame.size.height);
    [_viewSubWorkOrderDetails setFrame:frameFor_view_SubWorkOrderInfo];
    [_scrollVieww addSubview:_viewSubWorkOrderDetails];
    
    //Adding _view_ServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, _viewSubWorkOrderDetails.frame.origin.y+_viewSubWorkOrderDetails.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssues.count*60+70+50);
    [_view_ServiceIssues setFrame:frameFor_view_ServiceIssues];
    [_scrollVieww addSubview:_view_ServiceIssues];
    
    _tblViewServiceIssues.tag=1;
    _tblViewServiceIssues.frame=CGRectMake(_tblViewServiceIssues.frame.origin.x, _tblViewServiceIssues.frame.origin.y, _tblViewServiceIssues.frame.size.width, arrOfSubWorkServiceIssues.count*60+70+50);
    
    //Adding _viewHelper
    
    CGRect frameFor_viewHelper=CGRectMake(0, _view_ServiceIssues.frame.origin.y+_view_ServiceIssues.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssuesRepairPartsAll.count*175+50+70);
    [_viewHelper setFrame:frameFor_viewHelper];
    [_scrollVieww addSubview:_viewHelper];
    
    _tblViewPartsAll.tag=2;
    _tblViewPartsAll.frame=CGRectMake(_tblViewPartsAll.frame.origin.x, _tblViewPartsAll.frame.origin.y, _tblViewPartsAll.frame.size.width, arrOfSubWorkServiceIssuesRepairPartsAll.count*175+50+70);
    
    
    //Adding _viewNotes
    
    CGRect frameFor_viewNotes=CGRectMake(0, _viewHelper.frame.origin.y+_viewHelper.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceNotes.count*175+70+50);
    [_viewNotes setFrame:frameFor_viewNotes];
    [_scrollVieww addSubview:_viewNotes];
    
    _tblViewNotes.tag=3;
    _tblViewNotes.frame=CGRectMake(_tblViewNotes.frame.origin.x, _tblViewNotes.frame.origin.y, _tblViewNotes.frame.size.width, arrOfSubWorkServiceNotes.count*175+70+50);
    
    //    //Adding viewClientApproval
    //
    //    CGRect frameFor_viewClientApproval=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_viewClientApproval.frame.size.height);
    //    [_viewClientApproval setFrame:frameFor_viewClientApproval];
    //    //[_scrollVieww addSubview:_viewClientApproval];
    
    //Adding viewAdditionalInfo
    
    CGRect frameFor_viewAdditionalInfo=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height);
    [_view_AdditionalInfo setFrame:frameFor_viewAdditionalInfo];
    [_scrollVieww addSubview:_view_AdditionalInfo];
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height+_view_AdditionalInfo.frame.origin.y)];
    
}

-(void)adjustViewHeights{
    
    //Adding _view_ServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, _viewSubWorkOrderDetails.frame.origin.y+_viewSubWorkOrderDetails.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssues.count*60+70+50);
    [_view_ServiceIssues setFrame:frameFor_view_ServiceIssues];
    //  [_scrollVieww addSubview:_view_ServiceIssues];
    
    _tblViewServiceIssues.frame=CGRectMake(_tblViewServiceIssues.frame.origin.x, _tblViewServiceIssues.frame.origin.y, _tblViewServiceIssues.frame.size.width, arrOfSubWorkServiceIssues.count*60+70+50);
    
    //Adding _viewHelper
    
    CGRect frameFor_viewHelper=CGRectMake(0, _view_ServiceIssues.frame.origin.y+_view_ServiceIssues.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssuesRepairPartsAll.count*175+50+70);
    [_viewHelper setFrame:frameFor_viewHelper];
    // [_scrollVieww addSubview:_viewHelper];
    
    _tblViewPartsAll.frame=CGRectMake(_tblViewPartsAll.frame.origin.x, _tblViewPartsAll.frame.origin.y, _tblViewPartsAll.frame.size.width, arrOfSubWorkServiceIssuesRepairPartsAll.count*175+50+70);
    
    
    //Adding _viewNotes
    
    CGRect frameFor_viewNotes=CGRectMake(0, _viewHelper.frame.origin.y+_viewHelper.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceNotes.count*175+70+50);
    [_viewNotes setFrame:frameFor_viewNotes];
    // [_scrollVieww addSubview:_viewNotes];
    
    _tblViewNotes.frame=CGRectMake(_tblViewNotes.frame.origin.x, _tblViewNotes.frame.origin.y, _tblViewNotes.frame.size.width, arrOfSubWorkServiceNotes.count*175+70+50);
    
    //    //Adding viewClientApproval
    //
    //    CGRect frameFor_viewClientApproval=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_viewClientApproval.frame.size.height);
    //    [_viewClientApproval setFrame:frameFor_viewClientApproval];
    //    // [_scrollVieww addSubview:_viewClientApproval];
    
    //Adding viewAdditionalInfo
    
    CGRect frameFor_viewAdditionalInfo=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height);
    [_view_AdditionalInfo setFrame:frameFor_viewAdditionalInfo];
    //[_scrollVieww addSubview:_view_AdditionalInfo];
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height+_view_AdditionalInfo.frame.origin.y)];
    
    [self adjustViewHeightsonCollapsingSections];
}
-(void)adjustViewHeightsonCollapsingSections{
    
    //Adding _view_ServiceIssues
    
    if (isCollapseAllViews) {
        
        CGRect frameFor_view_ServiceIssues=CGRectMake(0, _viewSubWorkOrderDetails.frame.origin.y+_viewSubWorkOrderDetails.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssues.count*60+70+50);
        [_view_ServiceIssues setFrame:frameFor_view_ServiceIssues];
        //  [_scrollVieww addSubview:_view_ServiceIssues];
        
        _tblViewServiceIssues.frame=CGRectMake(_tblViewServiceIssues.frame.origin.x, _tblViewServiceIssues.frame.origin.y, _tblViewServiceIssues.frame.size.width, arrOfSubWorkServiceIssues.count*60+70+50);
        
    } else {
        
        NSManagedObject *dictIssuesData;
        
        //        if (indexToShow==-1) {
        //
        //            dictIssuesData=arrOfSubWorkServiceIssues[indexToShow];
        //
        //        } else {
        //
        //            if (arrOfSubWorkServiceIssues.count==0) {
        //
        //
        //
        //            } else {
        //
        //                dictIssuesData=arrOfSubWorkServiceIssues[indexToShow];
        //
        //            }
        //        }
        
        int k1=0;
        
        for (int p=0; p<arrOfSectionsToOpen.count; p++) {
            
            int indexx = [arrOfSectionsToOpen[p] intValue];
            
            dictIssuesData=arrOfSubWorkServiceIssues[indexx];
            
            NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
            
            for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
                
                NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
                
                NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
                
                if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                    
                    k1++;
                    
                }
                
            }
            
        }
        
        CGRect frameFor_view_ServiceIssues=CGRectMake(0, _viewSubWorkOrderDetails.frame.origin.y+_viewSubWorkOrderDetails.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssues.count*60+70+380*k1+50);
        [_view_ServiceIssues setFrame:frameFor_view_ServiceIssues];
        //  [_scrollVieww addSubview:_view_ServiceIssues];
        
        _tblViewServiceIssues.frame=CGRectMake(_tblViewServiceIssues.frame.origin.x, _tblViewServiceIssues.frame.origin.y, _tblViewServiceIssues.frame.size.width, arrOfSubWorkServiceIssues.count*60+70+380*k1+50);
        
    }
    
    //Adding _viewHelper
    
    CGRect frameFor_viewHelper=CGRectMake(0, _view_ServiceIssues.frame.origin.y+_view_ServiceIssues.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceIssuesRepairPartsAll.count*175+50+70);
    [_viewHelper setFrame:frameFor_viewHelper];
    // [_scrollVieww addSubview:_viewHelper];
    
    _tblViewPartsAll.frame=CGRectMake(_tblViewPartsAll.frame.origin.x, _tblViewPartsAll.frame.origin.y, _tblViewPartsAll.frame.size.width, arrOfSubWorkServiceIssuesRepairPartsAll.count*175+50+70);
    
    
    //Adding _viewNotes
    
    CGRect frameFor_viewNotes=CGRectMake(0, _viewHelper.frame.origin.y+_viewHelper.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkServiceNotes.count*175+70+50);
    [_viewNotes setFrame:frameFor_viewNotes];
    // [_scrollVieww addSubview:_viewNotes];
    
    _tblViewNotes.frame=CGRectMake(_tblViewNotes.frame.origin.x, _tblViewNotes.frame.origin.y, _tblViewNotes.frame.size.width, arrOfSubWorkServiceNotes.count*175+70+50);
    
    //    //Adding viewClientApproval
    //
    //    CGRect frameFor_viewClientApproval=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_viewClientApproval.frame.size.height);
    //    [_viewClientApproval setFrame:frameFor_viewClientApproval];
    //    // [_scrollVieww addSubview:_viewClientApproval];
    
    //Adding viewAdditionalInfo
    
    CGRect frameFor_viewAdditionalInfo=CGRectMake(0, _viewNotes.frame.origin.y+_viewNotes.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height);
    [_view_AdditionalInfo setFrame:frameFor_viewAdditionalInfo];
    //[_scrollVieww addSubview:_view_AdditionalInfo];
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_AdditionalInfo.frame.size.height+_view_AdditionalInfo.frame.origin.y)];
    
}

-(void)LoadProfileImage{
    
    NSUserDefaults *defsPhoto=[NSUserDefaults standardUserDefaults];
    NSString *strSavedPhoto=[defsPhoto valueForKey:@"EmployeePhoto"];
    NSString *urlString = [NSString stringWithFormat:@"%@",strSavedPhoto];
    NSString *strNewString=[urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    [_imgViewProfile sd_setImageWithURL:[NSURL URLWithString:strNewString] placeholderImage:[UIImage imageNamed:@"profile.png"] options:SDWebImageRefreshCached];
    
}

-(void)methodBorderColor{
    
    _txtViewTechComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewTechComment.layer.borderWidth=1.0;
    _txtViewTechComment.layer.cornerRadius=5.0;
    
    _txtViewAdditionalInfo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAdditionalInfo.layer.borderWidth=1.0;
    _txtViewAdditionalInfo.layer.cornerRadius=5.0;
    
    _txtViewIssuesDesc.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewIssuesDesc.layer.borderWidth=1.0;
    _txtViewIssuesDesc.layer.cornerRadius=5.0;
    
    _txtViewRepairDescriptions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewRepairDescriptions.layer.borderWidth=1.0;
    _txtViewRepairDescriptions.layer.cornerRadius=5.0;
    
    _txtViewAddNotesDesc.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAddNotesDesc.layer.borderWidth=1.0;
    _txtViewAddNotesDesc.layer.cornerRadius=5.0;
    
    _txtViewReason.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewReason.layer.borderWidth=1.0;
    _txtViewReason.layer.cornerRadius=5.0;
    
    [_btnEmail.layer setCornerRadius:5.0f];
    [_btnEmail.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnEmail.layer setBorderWidth:0.8f];
    [_btnEmail.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnEmail.layer setShadowOpacity:0.3];
    [_btnEmail.layer setShadowRadius:3.0];
    [_btnEmail.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectReason.layer setCornerRadius:5.0f];
    [_btnSelectReason.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectReason.layer setBorderWidth:0.8f];
    [_btnSelectReason.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectReason.layer setShadowOpacity:0.3];
    [_btnSelectReason.layer setShadowRadius:3.0];
    [_btnSelectReason.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnPrioityAddServiceIssues.layer setCornerRadius:5.0f];
    [_btnPrioityAddServiceIssues.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnPrioityAddServiceIssues.layer setBorderWidth:0.8f];
    [_btnPrioityAddServiceIssues.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnPrioityAddServiceIssues.layer setShadowOpacity:0.3];
    [_btnPrioityAddServiceIssues.layer setShadowRadius:3.0];
    [_btnPrioityAddServiceIssues.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectRepair.layer setCornerRadius:5.0f];
    [_btnSelectRepair.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectRepair.layer setBorderWidth:0.8f];
    [_btnSelectRepair.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectRepair.layer setShadowOpacity:0.3];
    [_btnSelectRepair.layer setShadowRadius:3.0];
    [_btnSelectRepair.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectHelper.layer setCornerRadius:5.0f];
    [_btnSelectHelper.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectHelper.layer setBorderWidth:0.8f];
    [_btnSelectHelper.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectHelper.layer setShadowOpacity:0.3];
    [_btnSelectHelper.layer setShadowRadius:3.0];
    [_btnSelectHelper.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    
    _txtViewDesc.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewDesc.layer.borderWidth=1.0;
    _txtViewDesc.layer.cornerRadius=5.0;
    
    [_btnSelectParts.layer setCornerRadius:5.0f];
    [_btnSelectParts.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectParts.layer setBorderWidth:0.8f];
    [_btnSelectParts.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectParts.layer setShadowOpacity:0.3];
    [_btnSelectParts.layer setShadowRadius:3.0];
    [_btnSelectParts.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectdate.layer setCornerRadius:5.0f];
    [_btnSelectdate.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectdate.layer setBorderWidth:0.8f];
    [_btnSelectdate.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectdate.layer setShadowOpacity:0.3];
    [_btnSelectdate.layer setShadowRadius:3.0];
    [_btnSelectdate.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectCategory.layer setCornerRadius:5.0f];
    [_btnSelectCategory.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectCategory.layer setBorderWidth:0.8f];
    [_btnSelectCategory.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectCategory.layer setShadowOpacity:0.3];
    [_btnSelectCategory.layer setShadowRadius:3.0];
    [_btnSelectCategory.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectIssues.layer setCornerRadius:5.0f];
    [_btnSelectIssues.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectIssues.layer setBorderWidth:0.8f];
    [_btnSelectIssues.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectIssues.layer setShadowOpacity:0.3];
    [_btnSelectIssues.layer setShadowRadius:3.0];
    [_btnSelectIssues.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSelectRepairParts.layer setCornerRadius:5.0f];
    [_btnSelectRepairParts.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectRepairParts.layer setBorderWidth:0.8f];
    [_btnSelectRepairParts.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectRepairParts.layer setShadowOpacity:0.3];
    [_btnSelectRepairParts.layer setShadowRadius:3.0];
    [_btnSelectRepairParts.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    // Vendor Details
    [_btnVendorName.layer setCornerRadius:5.0f];
    [_btnVendorName.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnVendorName.layer setBorderWidth:0.8f];
    [_btnVendorName.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnVendorName.layer setShadowOpacity:0.3];
    [_btnVendorName.layer setShadowRadius:3.0];
    [_btnVendorName.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}

-(void)loadValues{
    
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strEmployeeNoLoggedIn=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strWoStatus       =[defsLead valueForKey:@"WoStatus"];
    
    dictDetailsFortblView=[defsLead valueForKey:@"TotalLeadCountResponse"];
    
    dictDetailsMastersMechanical=[defsLead valueForKey:@"MasterServiceAutomation"];
    
    strWorkOrderId=[defsLead valueForKey:@"LeadId"];
    
    _strSubWorkOrderId=[defsLead valueForKey:@"SubWorkOrderId"];
    
    _objSubWorkOrderdetails=[global fetchMechanicalSubWorkOrderObj:strWorkOrderId :_strSubWorkOrderId];
    
    _objWorkOrderdetails=[global fetchMechanicalWorkOrderObj:strWorkOrderId];
    
    //_lblCustNamenAccNo.text=[defsLead valueForKey:@"lblName"];
    _lblCustNamenAccNo.text=[NSString stringWithFormat:@"%@, Sub Work Order #: %@",[defsLead valueForKey:@"lblName"],[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderNo"]]];
    
    strWorkOrderStatuss=[_objSubWorkOrderdetails valueForKey:@"subWOStatus"];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodStatus];
    
    _strWoType=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWOType"]];
    
    strSubWorkOrderIdGlobal=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderId"]];
    strDepartMentSysName=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"departmentSysName"]];
    _lblWorkOrderNi.text=[NSString stringWithFormat:@"Workorder #: %@",[_objWorkOrderdetails valueForKey:@"workOrderNo"]];
    _lblAccountNo.text=[NSString stringWithFormat:@"%@ %@",[_objWorkOrderdetails valueForKey:@"accountNo"],[defsLead valueForKey:@"customerNameService"]];
    
    NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"thirdPartyAccountNo"]];
    
    if (strThirdPartyAccountNo.length>0) {
        
        _lblAccountNo.text=[NSString stringWithFormat:@"%@ %@",strThirdPartyAccountNo,[defsLead valueForKey:@"customerNameService"]];;
        
    }else{
        
        strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"accountNo"]];
        _lblAccountNo.text=[NSString stringWithFormat:@"%@ %@",strThirdPartyAccountNo,[defsLead valueForKey:@"customerNameService"]];;
        
    }
    
    _lblSubWorkOrderNo.text=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderId"]];
    _lblStatus.text=[NSString stringWithFormat:@"Status: %@",[_objWorkOrderdetails valueForKey:@"workorderStatus"]];
    _lblStatus.text=[NSString stringWithFormat:@"Status: %@",[_objSubWorkOrderdetails valueForKey:@"subWOStatus"]];
    
    if ([_lblStatus.text isEqualToString:@"Status: Inspection"]) {
        
        _lblStatus.text=[NSString stringWithFormat:@"Status: %@",@"In Progress"];
        
    }
    
    [self methodToCheckIfValuesAreNull];
    
    _lblPhoneNo.text=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"primaryPhone"]];
    
    NSString *strServiceAddress=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"serviceCountry"]];
    
    if ([strServiceAddress caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strServiceAddress=@"United States";
    }
    NSString *strServiceAddress2=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"billingAddress2"]];
    if (strServiceAddress2.length==0) {
        
        _lblAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[_objWorkOrderdetails valueForKey:@"servicesAddress1"],[_objWorkOrderdetails valueForKey:@"serviceCity"],[_objWorkOrderdetails valueForKey:@"serviceState"],strServiceAddress,[_objWorkOrderdetails valueForKey:@"serviceZipcode"]];
        
        
    }else{
        
        _lblAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",[_objWorkOrderdetails valueForKey:@"servicesAddress1"],[_objWorkOrderdetails valueForKey:@"serviceAddress2"],[_objWorkOrderdetails valueForKey:@"serviceCity"],[_objWorkOrderdetails valueForKey:@"serviceState"],strServiceAddress,[_objWorkOrderdetails valueForKey:@"serviceZipcode"]];
        
        
    }
    [_btnEmail setTitle:[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"primaryEmail"]] forState:UIControlStateNormal];
    
    NSString *strTotalEstTimeSubWorkOrder=[NSString stringWithFormat:@"Est. Time: %@",[_objSubWorkOrderdetails valueForKey:@"totalEstimationTime"]];
    
    if ([NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"totalEstimationTime"]].length>5) {
        
        NSRange equalRange = [strTotalEstTimeSubWorkOrder rangeOfString:@":" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strTotalEstTimeSubWorkOrder = [strTotalEstTimeSubWorkOrder substringToIndex:equalRange.location];
        }
    }
    [_btnEstTime setTitle:strTotalEstTimeSubWorkOrder forState:UIControlStateNormal];
    
    strGlobalAmtRepair=@"";
    
    NSString *strIsActive=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isStdPrice"]];
    
    if ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) {
        
        isStandardSubWorkOrder=YES;
        
    } else {
        
        isStandardSubWorkOrder=NO;
        
    }
    
    //Additional Info
    
    _txtViewAdditionalInfo.text=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"inspectionresults"]];
    
    strWorkOrderAccNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"accountNo"]];
    
    strWorkOrderAddressId=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"serviceAddressId"]];
    
    strWorkOrderAddressSubType=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"addressSubType"]];
    
    //isHoliday
    
    NSString *strIsHoliday=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isHolidayHrs"]];
    
    if ([strIsHoliday isEqualToString:@"true"] || [strIsHoliday isEqualToString:@"1"]) {
        
        isHoliday=YES;
        
    } else {
        
        isHoliday=NO;
        
    }
    
    //AfterHrsDuration
    
    strAfterHrsDuration=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"afterHrsDuration"]];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodStatus];
    
    _txtViewTechComment.text=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"technicianComment"]];
    
}
-(void)methodAllocation{
    
    indexToShow=0;
    intGlobalSection=0;
    isCollapseAllViews=NO;
    isStandardRepair=YES;
    global = [[Global alloc] init];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodStatus];
    
    arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
    arrOfSubWorkServiceHelper=[[NSMutableArray alloc]init];
    arrOfSubWorkServiceNotes=[[NSMutableArray alloc]init];
    arrDataTblView=[[NSMutableArray alloc]init];
    arrOfSubWorkServiceActualHrs=[[NSMutableArray alloc]init];
    arrOfHrsTotal=[[NSMutableArray alloc]init];
    arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
    arrOfBeforeImageAll=[[NSMutableArray alloc]init];
    arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    
    [_tblViewServiceIssues reloadData];
    [_tblViewPartsAll reloadData];
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
    // [self adjustViewHeights];
    
}


-(void)methodTableViewAllocation{
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
}


-(void)methodAddServiceIssuesView{
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, 105, [UIScreen mainScreen].bounds.size.width,_viewAddServiceIssues.frame.size.height);
    [_viewAddServiceIssues setFrame:frameFor_view_ServiceIssues];
    [viewBackGround addSubview:_viewAddServiceIssues];
    
}
-(void)methodAddReasonStopJobView{
    
    _txtViewReason.text=@"";
    [_btnSelectReason setTitle:@"----Select Reason----" forState:UIControlStateNormal];
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width,_viewStopJobReason.frame.size.height);
    [_viewStopJobReason setFrame:frameFor_view_ServiceIssues];
    [viewBackGround addSubview:_viewStopJobReason];
    
}

-(void)methodAddRepairView{
    
    [_txtRepairAmt setEnabled:NO];
    _txtRepairAmt.text=@"";
    _txtRepairCostAdjustment.text=@"";
    isStandardRepair=YES;
    [_btnSelectRepair setTitle:@"----Select Repair----" forState:UIControlStateNormal];
    _lblRepairStdorNonStd.text=@"Repair";
    [_txtRepairName setHidden:YES];
    [_btnSelectRepair setHidden:NO];
    [_btnDropDownRepair setHidden:NO];
    
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, 105, [UIScreen mainScreen].bounds.size.width,_view_AddRepair.frame.size.height);
    [_view_AddRepair setFrame:frameFor_view_ServiceIssues];
    [viewBackGround addSubview:_view_AddRepair];
    
}


-(void)methodAddNotesView{
    
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewServiceIssues
    
    CGRect frameFor_view_ServiceIssues=CGRectMake(0, 105, [UIScreen mainScreen].bounds.size.width,_viewAddNotes.frame.size.height);
    [_viewAddNotes setFrame:frameFor_view_ServiceIssues];
    [viewBackGround addSubview:_viewAddNotes];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // [tblData removeFromSuperview];
    // [viewBackGround removeFromSuperview];
    
}

-(void)clickAddRepair:(UIButton*)sender
{
    
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    
    intGlobalSection=button.tag;
    
    NSDictionary *dictDatTemp=arrOfSubWorkServiceIssues[button.tag];
    strSubWorkOrderIssueIdGlobal=[NSString stringWithFormat:@"%@",[dictDatTemp valueForKey:@"subWorkOrderIssueId"]];
    [self methodAddRepairView];
    
}
-(void)clickMinusSection:(UIButton*)sender
{
    
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    indexToShow=-1;
    intGlobalSection=-1;
    [_tblViewServiceIssues reloadData];
    //isCollapseAllViews=YES;
    
    if ([arrOfSectionsToOpen containsObject:[NSString stringWithFormat:@"%ld",(long)button.tag]]) {
        
        [arrOfSectionsToOpen removeObject:[NSString stringWithFormat:@"%ld",(long)button.tag]];
        
    }
    
    [self adjustViewHeightsonCollapsingSections];
    
}
-(void)clickPlusSection:(UIButton*)sender
{
    
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    indexToShow=button.tag;
    intGlobalSection=button.tag;
    NSDictionary *dictDatTemp=arrOfSubWorkServiceIssues[button.tag];
    strSubWorkOrderIssueIdGlobal=[NSString stringWithFormat:@"%@",[dictDatTemp valueForKey:@"subWorkOrderIssueId"]];
    
    [_tblViewServiceIssues reloadData];
    isCollapseAllViews=NO;
    
    if (![arrOfSectionsToOpen containsObject:[NSString stringWithFormat:@"%ld",(long)button.tag]]) {
        
        [arrOfSectionsToOpen addObject:[NSString stringWithFormat:@"%ld",(long)button.tag]];
        
    }
    
    [self adjustViewHeightsonCollapsingSections];
    
}


-(void)methodToOpenSection :(int)intSection{
    
    indexToShow=intSection;
    
    NSDictionary *dictDatTemp=arrOfSubWorkServiceIssues[intSection];
    strSubWorkOrderIssueIdGlobal=[NSString stringWithFormat:@"%@",[dictDatTemp valueForKey:@"subWorkOrderIssueId"]];
    
    isCollapseAllViews=NO;
    
    [self adjustViewHeightsonCollapsingSections];
    
}

-(void)getPricLookupFromMaster :(NSString*)strCategorySysName{
    
    arrOfPriceLookup=nil;
    arrOfPriceLookup=[[NSMutableArray alloc]init];
    arrOfPriceLookup=[global getPricLookupFromMaster:strCompanyKey :strDepartMentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType :strCategorySysName];
    
}

-(void)getPartsMaster{
    
    arrOfPartsGlobalMasters=nil;
    arrOfPartsGlobalMasters=[[NSMutableArray alloc]init];
    arrOfPartsGlobalMasters=[global getPartsMaster:strDepartMentSysName :@""];
    
}
-(void)getHoursConfiFromMaster{
    
    arrOfHoursConfig=nil;
    arrOfHoursConfig=[[NSMutableArray alloc]init];
    arrOfHoursConfig=[global getHoursConfiFromMaster:strCompanyKey :strDepartMentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType];
    
}

-(void)methodCalculationRepairAmt :(int)indexRepair{
    
    indexRepairGlobal=indexRepair;
    
    if (isStandardRepair) {
        
        NSDictionary *dictDataRepairMaster=arrDataTblView[indexRepair];
        
        //Labour Price Logic
        
        _txtRepairCostAdjustment.text=[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"CostAdjustment"]];
        
        float hrsLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"LaborHours"]]];
        
        hrsLR=hrsLR/3600;
        
        float strStdPrice=0.0;
        
        if (!(arrOfHoursConfig.count==0)) {
            
            NSDictionary *dictDataHours=arrOfHoursConfig[0];
            
            strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
            
            if (!isStandardSubWorkOrder) {
                
                
            }
            
        }
        
        float totalPriceLabour=hrsLR*strStdPrice;
        
        //Helper Price Logic
        
        float hrsHLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"HelperHours"]]];
        
        hrsHLR=hrsHLR/3600;
        
        float strStdPriceHLR=0.0;
        
        if (!(arrOfHoursConfig.count==0)) {
            
            NSDictionary *dictDataHours=arrOfHoursConfig[0];
            
            strStdPriceHLR=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
            
            if (!isStandardSubWorkOrder) {
                
                strStdPriceHLR=[[dictDataHours valueForKey:@"HelperAftHrsStdPrice"] floatValue];
                
            }
            
        }
        
        float totalPriceHelper=hrsHLR*strStdPriceHLR;
        
        //Adding Cost Adjustment To Price Repair
        
        float costAdjustMentToAdd=[_txtRepairCostAdjustment.text floatValue];
        
        //Final Price Logic
        
        float finalPriceLRHLR=totalPriceLabour+totalPriceHelper+costAdjustMentToAdd;
        
        //Parts Logic From Master
        
        float totalLookUpPrice=0;
        
        NSArray *ArrOfPartsMaster=[dictDataRepairMaster valueForKey:@"RepairPartMasterDcs"];
        
        for (int k=0; k<ArrOfPartsMaster.count; k++) {
            
            NSDictionary *dictData=ArrOfPartsMaster[k];
            
            NSString *strPartCode=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PartCode"]];
            
            float valueLookUpPrice=[self fetchPartDetailViaPartCode:strPartCode];
            
            float QTY=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Qty"]] floatValue];
            
            float multiplier=1;
            
            valueLookUpPrice=valueLookUpPrice*QTY*multiplier;
            
            totalLookUpPrice=totalLookUpPrice+valueLookUpPrice;
            
        }
        
        //Final Price Logic after part and repair adding
        
        float finalOverAllPrice=totalLookUpPrice+finalPriceLRHLR;
        
        _txtRepairAmt.text=[NSString stringWithFormat:@"%.02f",finalOverAllPrice];
        
        
        dictLaborInfoToSave = @{@"LaborHours":[NSString stringWithFormat:@"%@",[dictDataRepairMaster             valueForKey:@"LaborHours"]],
                                @"HelperHours":[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"HelperHours"]],
                                @"totalPriceLabour":[NSString stringWithFormat:@"%f",totalPriceLabour],
                                @"totalPriceHelper": [NSString stringWithFormat:@"%f",totalPriceHelper],
                                @"DescRepair": [NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"RepairDesc"]],
                                };
        
        
    } else {
        
        
        
    }
    
    strGlobalAmtRepair=_txtRepairAmt.text;
    
}

-(void)methodStatus{
    
    if (isCompletedStatusMechanical) {
        
        [_btnAddNotes setHidden:YES];
        [_btnAddHelper setHidden:YES];
        [_btnAddServiceIssue setHidden:YES];
        [_btnStartnStopJOB setEnabled:NO];
        [_btnSelectHelper setEnabled:NO];
        [_btnRecommandation setHidden:YES];
        [_btnAddPartss setHidden:YES];
        [_btnTimeClock setHidden:YES];
        [_btnStartnStopJOB setHidden:YES];
        [_btnAdditionalInfo setHidden:YES];
        _btnIncludeDetailOnInvoice.enabled=NO;
        [_btnCreateQuote setHidden:YES];
        [_btnPoOrder setHidden:YES];
        [_btnUpdateEstTime setHidden:YES];
        //_btnIncludeDetailOnInvoice.enabled=YES;
        [_txtViewTechComment setEditable:NO];
        
    }else{
        
        [_txtViewTechComment setEditable:YES];
        [_btnUpdateEstTime setHidden:NO];
        _btnIncludeDetailOnInvoice.enabled=YES;
        [_btnCreateQuote setHidden:NO];
        [_btnPoOrder setHidden:NO];
        [_btnAddNotes setHidden:NO];
        [_btnAddHelper setHidden:NO];
        [_btnAddServiceIssue setHidden:NO];
        [_btnStartnStopJOB setEnabled:YES];
        [_btnSelectHelper setEnabled:YES];
        [_btnRecommandation setHidden:NO];
        [_btnAddPartss setHidden:NO];
        [_btnTimeClock setHidden:NO];
        [_btnStartnStopJOB setHidden:NO];
        [_btnAdditionalInfo setHidden:NO];
        _btnIncludeDetailOnInvoice.enabled=YES;
        [_btnCreateQuote setHidden:NO];
        [_btnPoOrder setHidden:NO];
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ----------------Core Data Fetch Methods----------------
//============================================================================
//============================================================================

-(void)fetchSubWorkOrderIssuesFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssues = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssues setEntity:entitySubWorkOrderIssues];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssues setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssues = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssues = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssues];
    
    [requestSubWorkOrderIssues setSortDescriptors:sortDescriptorsSubWorkOrderIssues];
    
    self.fetchedResultsControllerSubWorkOrderIssues = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssues managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssues setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssues performFetch:&error];
    arrAllObjSubWorkOrderIssues = [self.fetchedResultsControllerSubWorkOrderIssues fetchedObjects];
    if ([arrAllObjSubWorkOrderIssues count] == 0)
    {
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        [_tblViewServiceIssues reloadData];
        [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssues.count; k++) {
            
            matchesSubWorkOrderIssues=arrAllObjSubWorkOrderIssues[k];
            
            [arrOfSubWorkServiceIssues addObject:matchesSubWorkOrderIssues];
            
        }
        [_tblViewServiceIssues reloadData];
        [self adjustViewHeights];
        [self adjustViewHeightsonCollapsingSections];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    indexToShow=intGlobalSection;
    
    [self adjustViewHeightsonCollapsingSections];
    
}

-(void)fetchSubWorkOrderHelperFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    arrAllObjSubWorkOrderHelper = [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    if ([arrAllObjSubWorkOrderHelper count] == 0)
    {
        arrOfSubWorkServiceHelper=nil;
        arrOfSubWorkServiceHelper=[[NSMutableArray alloc]init];
        [_tblViewPartsAll reloadData];
        
        [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceHelper=nil;
        arrOfSubWorkServiceHelper=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderHelper.count; k++) {
            
            matchesSubWorkOrderHelper=arrAllObjSubWorkOrderHelper[k];
            
            NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderHelper valueForKey:@"isActive"]];
            
            if ([strIsActive caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsActive isEqualToString:@"1"]) {
                
                [arrOfSubWorkServiceHelper addObject:matchesSubWorkOrderHelper];
                
            }
            
        }
        [_tblViewPartsAll reloadData];
        
        [self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderHelperToDeleteFromDataBaseForMechanical :(NSString *)strEmployeeNoToDelete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && employeeNo = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strEmployeeNoToDelete];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    arrAllObjSubWorkOrderHelper = [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    if ([arrAllObjSubWorkOrderHelper count] == 0)
    {
        [_tblViewPartsAll reloadData];
        
        [self adjustViewHeights];
        
    }
    else
    {
        
        matchesSubWorkOrderHelper=arrAllObjSubWorkOrderHelper[0];
        
        [matchesSubWorkOrderHelper setValue:@"false" forKey:@"isActive"];
        
        //Final Save
        NSError *error;
        [context save:&error];
        
        [self fetchSubWorkOrderHelperFromDataBaseForMechanical];
        
        [_tblViewPartsAll reloadData];
        
        [self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderNotesFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
    requestSubWorkOrderNotes = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderNotes setEntity:entityMechanicalSubWorkOrderNotes];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderNotes setPredicate:predicate];
    
    sortDescriptorSubWorkOrderNotes = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderNotes = [NSArray arrayWithObject:sortDescriptorSubWorkOrderNotes];
    
    [requestSubWorkOrderNotes setSortDescriptors:sortDescriptorsSubWorkOrderNotes];
    
    self.fetchedResultsControllerSubWorkOrderNotes = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderNotes managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderNotes setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderNotes performFetch:&error];
    arrAllObjSubWorkOrderNotes = [self.fetchedResultsControllerSubWorkOrderNotes fetchedObjects];
    if ([arrAllObjSubWorkOrderNotes count] == 0)
    {
        arrOfSubWorkServiceNotes=nil;
        arrOfSubWorkServiceNotes=[[NSMutableArray alloc]init];
        [_tblViewNotes reloadData];
        
        [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceNotes=nil;
        arrOfSubWorkServiceNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderNotes.count; k++) {
            
            matchesSubWorkOrderNotes=arrAllObjSubWorkOrderNotes[k];
            
            [arrOfSubWorkServiceNotes addObject:matchesSubWorkOrderNotes];
            
        }
        [_tblViewNotes reloadData];
        
        [self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"" forKey:@"subWOActualHourId"];
        [defs synchronize];
        
        [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
        arrOfSubWorkServiceActualHrs=nil;
        arrOfSubWorkServiceActualHrs=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceActualHrs=nil;
        arrOfSubWorkServiceActualHrs=[[NSMutableArray alloc]init];
        
        arrOfHrsTotal=nil;
        arrOfHrsTotal=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strTimeInJob=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]];
            
            NSString *strTimeOutJob=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:@"" forKey:@"subWOActualHourId"];
            [defs synchronize];
            
            [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
            
            NSString *strSubWorkOrderStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"status"]];
            if ([strSubWorkOrderStatus isEqualToString:@"OnRoute"]) {
                
                
                
            } else {
                
                if (strTimeOutJob.length==0){
                    
                    NSString *strSubWOActualHourId=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWOActualHourId"]];
                    
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setValue:strSubWOActualHourId forKey:@"subWOActualHourId"];
                    [defs synchronize];
                    
                    [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
                    
                    break;
                }
                
            }
        }
        
        //Calculate Time For Job Duration
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strSubWorkOrderStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"status"]];
            
            if ([strSubWorkOrderStatus isEqualToString:@"OnRoute"]) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                
                BOOL isIncludeTravelTime=[defs boolForKey:@"IncludeTravelTime"];
                
                if (isIncludeTravelTime) {
                    
                    NSDate *timeInDate=[global ChangeDateToLocalDateMechanicalUTC:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]]];
                    
                    NSString *strTimeOUT=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
                    
                    if (strTimeOUT.length==0) {
                        
                        strTimeOUT=[global strCurrentDateFormattedForMechanical];
                        
                    }
                    
                    NSDate *timeOutDate=[global ChangeDateToLocalDateMechanicalTimeOut:[NSString stringWithFormat:@"%@",strTimeOUT]];
                    
                    NSTimeInterval secondsBetween = [timeOutDate timeIntervalSinceDate:timeInDate];
                    
                    int hrs = secondsBetween;
                    
                    [arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",hrs]];
                    
                    [arrOfSubWorkServiceActualHrs addObject:matchesSubWorkOrderActualHrs];
                    
                }
                
            } else {
                
                NSDate *timeInDate=[global ChangeDateToLocalDateMechanicalUTC:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]]];
                
                NSString *strTimeOUT=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
                
                if (strTimeOUT.length==0) {
                    
                    strTimeOUT=[global strCurrentDateFormattedForMechanical];
                    
                }
                
                NSDate *timeOutDate=[global ChangeDateToLocalDateMechanicalTimeOut:[NSString stringWithFormat:@"%@",strTimeOUT]];
                
                NSTimeInterval secondsBetween = [timeOutDate timeIntervalSinceDate:timeInDate];
                
                int hrs = secondsBetween;
                
                [arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",hrs]];
                
                [arrOfSubWorkServiceActualHrs addObject:matchesSubWorkOrderActualHrs];
                
            }
            
        }
        
        NSInteger sum = 0;
        for (NSString *num in arrOfHrsTotal)
        {
            sum += [num intValue];
        }
        
        totalsecond=(int)sum;
        
        NSDictionary *dictTimeClock=[self createTimemapForSeconds:(int)sum];
        
        NSString *strTimeFormat=[NSString stringWithFormat:@"%02d:%02d:%02d",[[dictTimeClock valueForKey:@"h"] intValue],[[dictTimeClock valueForKey:@"m"] intValue],[[dictTimeClock valueForKey:@"s"] intValue]];
        
        [_btnTimeClock setTitle:strTimeFormat forState:UIControlStateNormal];
        
        if (isCompletedStatusMechanical) {
            
        }else{
            
            if ([_btnStartnStopJOB.titleLabel.text isEqualToString:@"Pause Job"] || [_btnStartnStopJOB.currentTitle isEqualToString:@"Pause Job"]) {
                
                [self timerStarthere];
                
            } else {
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
                
                [self performSelector:@selector(addingStartTimeDefault) withObject:nil afterDelay:0.1];
                
            }
        }
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}


-(void)addingStartTimeDefault{
    
    [self addStartActualHoursFromMobileToDB];
    
    [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalAfterDefaultTimeEntry];
    
    [self timerStarthere];
    
    [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
    
    [_objSubWorkOrderdetails setValue:@"Start" forKey:@"clockStatus"];
    
    NSError *error2;
    [context save:&error2];
    
    [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
    
}

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalAfterDefaultTimeEntry{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        
    }
    else
    {
        arrOfSubWorkServiceActualHrs=nil;
        arrOfSubWorkServiceActualHrs=[[NSMutableArray alloc]init];
        
        arrOfHrsTotal=nil;
        arrOfHrsTotal=[[NSMutableArray alloc]init];
        
        //Calculate Time For Job Duration
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strSubWorkOrderStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"status"]];
            
            if ([strSubWorkOrderStatus isEqualToString:@"OnRoute"]) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                
                BOOL isIncludeTravelTime=[defs boolForKey:@"IncludeTravelTime"];
                
                if (isIncludeTravelTime) {
                    
                    NSDate *timeInDate=[global ChangeDateToLocalDateMechanicalUTC:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]]];
                    
                    NSString *strTimeOUT=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
                    
                    if (strTimeOUT.length==0) {
                        
                        strTimeOUT=[global strCurrentDateFormattedForMechanical];
                        
                    }
                    
                    NSDate *timeOutDate=[global ChangeDateToLocalDateMechanicalTimeOut:[NSString stringWithFormat:@"%@",strTimeOUT]];
                    
                    NSTimeInterval secondsBetween = [timeOutDate timeIntervalSinceDate:timeInDate];
                    
                    int hrs = secondsBetween;
                    
                    [arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",hrs]];
                    
                    [arrOfSubWorkServiceActualHrs addObject:matchesSubWorkOrderActualHrs];
                    
                }
                
            } else {
                
                NSDate *timeInDate=[global ChangeDateToLocalDateMechanicalUTC:[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeIn"]]];
                
                NSString *strTimeOUT=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
                
                if (strTimeOUT.length==0) {
                    
                    strTimeOUT=[global strCurrentDateFormattedForMechanical];
                    
                }
                
                NSDate *timeOutDate=[global ChangeDateToLocalDateMechanicalTimeOut:[NSString stringWithFormat:@"%@",strTimeOUT]];
                
                NSTimeInterval secondsBetween = [timeOutDate timeIntervalSinceDate:timeInDate];
                
                int hrs = secondsBetween;
                
                [arrOfHrsTotal addObject:[NSString stringWithFormat:@"%d",hrs]];
                
                [arrOfSubWorkServiceActualHrs addObject:matchesSubWorkOrderActualHrs];
                
            }
            
        }
        
        NSInteger sum = 0;
        for (NSString *num in arrOfHrsTotal)
        {
            sum += [num intValue];
        }
        
        totalsecond=sum;
        
        NSDictionary *dictTimeClock=[self createTimemapForSeconds:sum];
        
        NSString *strTimeFormat=[NSString stringWithFormat:@"%02d:%02d:%02d",[[dictTimeClock valueForKey:@"h"] intValue],[[dictTimeClock valueForKey:@"m"] intValue],[[dictTimeClock valueForKey:@"s"] intValue]];
        
        [_btnTimeClock setTitle:strTimeFormat forState:UIControlStateNormal];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}


-(NSDictionary*)createTimemapForSeconds:(int)seconds{
    int hours = floor(seconds /  (60 * 60) );
    
    float minute_divisor = seconds % (60 * 60);
    int minutes = floor(minute_divisor / 60);
    
    float seconds_divisor = seconds % 60;
    seconds = ceil(seconds_divisor);
    
    NSDictionary * timeMap = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:hours], [NSNumber numberWithInt:minutes], [NSNumber numberWithInt:seconds], nil] forKeys:[NSArray arrayWithObjects:@"h", @"m", @"s", nil]];
    
    return timeMap;
}

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOut{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strID=[defs valueForKey:@"subWOActualHourId"];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strID];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
    }
    else
    {
        
        matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[0];
        
        [matchesSubWorkOrderActualHrs setValue:[global strCurrentDateFormattedForMechanical] forKey:@"timeOut"];
        //[matchesSubWorkOrderActualHrs setValue:@"Running" forKey:@"status"];
        [matchesSubWorkOrderActualHrs setValue:[global strCurrentDateFormattedForMechanical] forKey:@"mobileTimeOut"];
        
        [matchesSubWorkOrderActualHrs setValue:_btnSelectReason.titleLabel.text forKey:@"reason"];
        [matchesSubWorkOrderActualHrs setValue:_txtViewReason.text forKey:@"actHrsDescription"];
        
        NSError *error2;
        [context save:&error2];
        
        [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
        
        BOOL isNetReachable=[global isNetReachable];
        
        if (isNetReachable) {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
            
            strNotificationTypeName=@"MechanicalActualHrsSyncStartFR1";
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:strNotificationTypeName object:nil];
            
            SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
            
            [objSync syncMechanicalActualHours:strWorkOrderId :strSubWorkOrderIdGlobal :strID :strNotificationTypeName];
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:@"" forKey:@"subWOActualHourId"];
            [defs synchronize];
            
        }
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}


-(void)fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        [_tblViewServiceIssues reloadData];
        
        [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepair.count; k++) {
            
            matchesSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[k];
            
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepair valueForKey:@"customerFeedback"]];
            
            if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]) {
                
                [arrOfSubWorkServiceIssuesRepair addObject:matchesSubWorkOrderIssuesRepair];
                
            }
            
        }
        [_tblViewServiceIssues reloadData];
        
        [self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderRepairsViaIssuesID{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    //subWorkOrderIssueId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueIdGlobalSelected];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepair.count; k++) {
            
            matchesSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[k];
            
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepair valueForKey:@"customerFeedback"]];
            
            if ([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]) {
                
                [arrOfSubWorkServiceIssuesRepair addObject:matchesSubWorkOrderIssuesRepair];
                
            }else{
                
            }
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderRepairsViaIssuenRepairId :(NSString*)strIssueIdToFetch :(NSString*)strRepairIdToFetch :(NSString*)strTruenFalse{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    //subWorkOrderIssueId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueIdToFetch,strRepairIdToFetch];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    NSArray *arrTemp = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrTemp count] == 0)
    {
        
        
    }
    else
    {
        
        NSManagedObject *objTemp=arrTemp[0];
        [objTemp setValue:strTruenFalse forKey:@"isCompleted"];
        
        NSError *error2;
        [context save:&error2];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    //issueRepairPartId
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueRepairIdTofetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        // [_tblViewServiceIssues reloadData];
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
            [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
            
        }
        // [_tblViewServiceIssues reloadData];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll {
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    //issueRepairPartId
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairPartsAll=nil;
        arrOfSubWorkServiceIssuesRepairPartsAll=[[NSMutableArray alloc]init];
        [_tblViewPartsAll reloadData];
        [self  adjustViewHeights];
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairPartsAll=nil;
        arrOfSubWorkServiceIssuesRepairPartsAll=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
            NSString *strCustomerFeedback=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"customerFeedback"]];
            
            if ([strCustomerFeedback isEqualToString:@"1"] || [strCustomerFeedback caseInsensitiveCompare:@"true"] == NSOrderedSame) {
                
                [arrOfSubWorkServiceIssuesRepairPartsAll addObject:matchesSubWorkOrderIssuesRepairParts];
                
            }
            
        }
        [_tblViewPartsAll reloadData];
        [self  adjustViewHeights];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :(NSString*)strIssueRepairIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueRepairIdToFetch];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairLabour = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairLabour count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairLabour.count; k++) {
            
            matchesSubWorkOrderIssuesRepairLabour=arrAllObjSubWorkOrderIssuesRepairLabour[k];
            
            [arrOfSubWorkServiceIssuesRepairLabour addObject:matchesSubWorkOrderIssuesRepairLabour];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

//============================================================================
//============================================================================
#pragma mark- ----------------Button Action Methods----------------
//============================================================================
//============================================================================


- (IBAction)action_SaveAdditionalInfo:(id)sender {
    
    if (_txtViewAdditionalInfo.text.length==0) {
        
        [global AlertMethod:Alert :@"Please enter Additional info to add"];
        
    } else {
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateAdditionalInfo:strWorkOrderId :_strSubWorkOrderId :_txtViewAdditionalInfo.text];
        
        [_txtViewAdditionalInfo resignFirstResponder];
    }
    
}


- (IBAction)action_DashBoardView:(id)sender {
    
    //    UIAlertController *alert= [UIAlertController
    //                               alertControllerWithTitle:@"Alert!"
    //                               message:@"Are you sure to move to DashBoard"
    //                               preferredStyle:UIAlertControllerStyleAlert];
    //
    //    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
    //                                                handler:^(UIAlertAction * action)
    //                          {
    //
    //                          }];
    //    [alert addAction:yes];
    //    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
    //                                               handler:^(UIAlertAction * action)
    //                         {
    //
    //                             [jobMinTimer invalidate];
    //                             jobMinTimer=nil;
    //                             [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
    //
    //                             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
    //                                                                                      bundle: nil];
    //                             DashBoardViewiPad
    //                             *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewiPad"];
    //                             [self.navigationController pushViewController:objByProductVC animated:NO];
    //                         }];
    //    [alert addAction:no];
    //    [self presentViewController:alert animated:YES completion:nil];
    
    [self goToEquipMentHistory];
    
}


- (IBAction)action_ServiceHistory:(id)sender {
    
    [self saveImageToCoreData];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isFromOtherPresentedView"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ServiceHistoryMechanical
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistoryMechanical"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

- (IBAction)action_Documents:(id)sender {
    
    [self saveImageToCoreData];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"isFromOtherPresentedView"];
    [defs synchronize];
    
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServiceiPad"
    //                                                             bundle: nil];
    //    ServiceDocumentsViewControlleriPad
    //    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDocumentsViewControlleriPad"];
    //    objByProductVC.strAccountNo=[_objWorkOrderdetails valueForKey:@"accountNo"];
    //    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    MechanicalDocumentsViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalDocumentsViewController"];
    objByProductVC.strAccountNo=[_objWorkOrderdetails valueForKey:@"accountNo"];
    objByProductVC.strWorkOrderId = strWorkOrderId;
    objByProductVC.strWorkOrderNo = [NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"workOrderNo"]];
    objByProductVC.strLeadNo = strWorkOrderId;
    
    if (isCompletedStatusMechanical) {
        
        objByProductVC.strWorkStatus=@"Complete";
        
    } else {
        
        objByProductVC.strWorkStatus=@"InComplete";
        
    }
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
}

- (IBAction)action_KnowledgeBase:(id)sender {
}


- (IBAction)action_Back:(id)sender {
    
    [self saveTechComments];

    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    [defsBack setBool:YES forKey:@"isFromBackServiceDynamci"];
    [defsBack synchronize];
    
    [jobMinTimer invalidate];
    jobMinTimer=nil;
    
    // [self.navigationController popViewControllerAnimated:YES];
    
    //[self goToSubWorkOrderDetailView];
    [self goToGeneralInfoView];
    
}


- (IBAction)action_PrimaryEmail:(id)sender{
    
    NSString *strMessage=_btnEmail.titleLabel.text;
    
    if (strMessage.length==0) {
        
    } else {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@""
                                   message:strMessage
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}

- (IBAction)action_AddServiceIssues:(id)sender {
    
    [_btnPrioityAddServiceIssues setTitle:@"----Select Priority----" forState:UIControlStateNormal];
    strPriorityIdGlobal=@"";
    _txtViewIssuesDesc.text=@"";
    [_txtViewIssuesDesc becomeFirstResponder];
    [self methodAddServiceIssuesView];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                k1++;
                
            }
            
        }
        
        return k1;
        
        
    }else if (tableView.tag==2){
        
        return arrOfSubWorkServiceIssuesRepairPartsAll.count;
        
    }else if (tableView.tag==3){
        
        return arrOfSubWorkServiceNotes.count;
        
    }else if(tableView.tag==101)
    {
        return arrayUpdateTimeEst.count;
    }
    else {
        
        return arrDataTblView.count;
        
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==1) {
        
        //if (indexPath.section==indexToShow) {
        if ([arrOfSectionsToOpen containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.section]]) {
            
            
            return tableView.rowHeight;
            
        } else {
            
            return 0;
            
        }
        
    }else if (tableView.tag==2){
        
        return 175;
        
    }else if (tableView.tag==3){
        
        return tableView.rowHeight;
        
    }
    else {
        
        return 80;
        
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==1) {
        
        MechanicalServiceIssueTableViewCell *cell = (MechanicalServiceIssueTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MechanicalServiceIssueTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
        
    }else if (tableView.tag==2){
        
        AddPartsTableViewCell *cell = (AddPartsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AddPartsTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        // Configure Table View Cell
        [self configureCellParts:cell atIndexPath:indexPath];
        
        return cell;
        
        
    }else if (tableView.tag==3){
        
        MechanicalNotesTableViewCell *cell = (MechanicalNotesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MechanicalNotesTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        // Configure Table View Cell
        [self configureCellNotes:cell atIndexPath:indexPath];
        
        return cell;
        
    }else if (tableView.tag==101)
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.textLabel.text = [arrayUpdateTimeEst objectAtIndex:indexPath.row];
        return cell;
    }
    else{
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrDataTblView.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 101:
                {
                    //_Btn_Priority
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"Name"];
                    break;
                }
                case 102:
                {
                    //Select Category
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"Name"];
                    break;
                }
                case 103:
                {
                    //Select Helper
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"FullName"];
                    break;
                }
                case 104:
                {
                    //Select Reason
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"ResetReason"];
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    break;
                }
                case 105:
                {
                    //Select parts
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"Name"];
                    break;
                }
                case 106:
                {
                    //Select Issues
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"serviceIssue"];
                    break;
                }
                case 107:
                {
                    //Select Repairs
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"repairName"];
                    break;
                }
                    // Vendor Details
                case 108:
                {
                    // VendorName List
                    cell.textLabel.text = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"Name"];
                    break;
                }
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
    }
}

- (void)configureCellParts:(AddPartsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *dictData =arrOfSubWorkServiceIssuesRepairPartsAll[indexPath.row];
    
    NSString *strQTY=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"qty"]];
    NSString *strMultiplier=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"multiplier"]];
    NSString *strUnitPrice=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"unitPrice"]];
    
    float Total=[strQTY floatValue]*[strMultiplier floatValue]*[strUnitPrice floatValue];
    
    cell.lblQty.text=[NSString stringWithFormat:@"%@",strQTY];
    cell.lblDesc.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"partDesc"]];
    cell.lblParts.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"partName"]];
    cell.lblTotalPrice.text=[NSString stringWithFormat:@"%.02f",Total];
    NSString *strActualQTY=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"actualQty"]];
    if ((strActualQTY.length==0) || [strActualQTY isEqualToString:@"(null)"]) {
        cell.txtActualQty.text=@"";
    } else {
        cell.txtActualQty.text=strActualQTY;
    }
    cell.txtActualQty.tag=indexPath.row;
    
    if (isCompletedStatusMechanical) {
        
        [cell.txtActualQty setEnabled:NO];
        
    }else{
        
        [cell.txtActualQty setEnabled:YES];
        
    }
    
    //    if (indexPath.row==indexToEditPart) {
    //
    //        [cell.txtActualQty becomeFirstResponder];
    //
    //    }
    
}

- (void)configureCell:(MechanicalServiceIssueTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSString *strnonStdLaborAmt,*strnonStdPartAmt,*strnonStdRepairAmt,*strRepairQty;
    
    BOOL isNonStandard;
    
    isNonStandard=NO;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }
        
    }
    
    NSManagedObject *objTemp=arrTemp[indexPath.row];
    
    float totalCostAdjustment=0;
    
    NSString *strcostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"costAdjustment"]];
    
    totalCostAdjustment=[strcostAdjustment floatValue];
    
    NSString *strPartCostAdjustment=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"partCostAdjustment"]];
    
    totalCostAdjustment=totalCostAdjustment+[strPartCostAdjustment floatValue];
    
    cell.lblRepairName.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairName"]];
    cell.lblRepairDesc.text=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
    if (cell.lblRepairDesc.text.length==0) {
        
        cell.lblRepairDesc.text=@"N/A";
        
    }
    strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
    
    if ([NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairMasterId"]].length==0) {
        
        isNonStandard=YES;
        
    } else {
        
        isNonStandard=NO;
        
    }
    
    strnonStdLaborAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdLaborAmt"]];
    strnonStdPartAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdPartAmt"]];
    strnonStdRepairAmt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"nonStdRepairAmt"]];
    
    strRepairQty =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
    cell.txtFldQty.delegate=self;
    cell.txtFldQty.accessibilityHint=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
    //cell.txtFldQty.leftView.hidden=YES;
    cell.txtFldQty.tag=0;
    cell.txtFldQty.text=strRepairQty;
    
    if ((strRepairQty.length==0) || [strRepairQty isEqualToString:@""] || [strRepairQty isEqualToString:@"(null)"] || [strRepairQty isEqualToString:@"0"]) {
        
        strRepairQty = @"1";
        cell.txtFldQty.text=@"";
        
    }
    
    //Fetch Parts
    
    [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical:strIssueIdToCheck :strIssueRepairIdToCheck];
    
    NSMutableArray *arrTempParts=[[NSMutableArray alloc]init];
    
    float totalCostPartss=0;
    
    for (int j=0; j<arrOfSubWorkServiceIssuesRepairParts.count; j++) {
        
        NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairParts[j];
        
        NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[objTemp valueForKey:@"partName"],[objTemp valueForKey:@"qty"]];
        //multiplier   qty  unitPrice
        
        NSString *strMulti=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"multiplier"]];
        float multip=[strMulti floatValue];
        
        NSString *strqtyy=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
        float qtyy=[strqtyy floatValue];
        
        NSString *strunitPricee=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"unitPrice"]];
        float unitPricee=[strunitPricee floatValue];
        
        float finalPriceParts=unitPricee*qtyy*multip;
        
        //        totalCostPartss=totalCostPartss+finalPriceParts;
        //
        //        [arrTempParts addObject:strPartsnQty];
        
        NSString *strIsAddedAfterApproval=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isAddedAfterApproval"]];
        
        if ([strIsAddedAfterApproval isEqualToString:@"true"] || [strIsAddedAfterApproval isEqualToString:@"1"]) {
            
        }else{
            
            totalCostPartss=totalCostPartss+finalPriceParts;
            
            [arrTempParts addObject:strPartsnQty];
            
        }
        
    }
    
    cell.lblPart.text=[arrTempParts componentsJoinedByString:@", "];
    
    if (cell.lblPart.text.length==0) {
        
        cell.lblPart.text=@"N/A";
        
    }
    //Fetch LAbor
    
    [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :strIssueRepairIdToCheck];
    
    float totalCostLaborss=0;
    
    NSString *strLaborHrs,*strHelperHrs;
    for (int j=0; j<arrOfSubWorkServiceIssuesRepairLabour.count; j++) {
        
        NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairLabour[j];
        
        NSString *strLaborCostt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborCost"]];
        float laborCostt=[strLaborCostt floatValue];
        
        NSString *strIsWarrantyHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isWarranty"]];
        
        if ([strIsWarrantyHrs isEqualToString:@"true"] || [strIsWarrantyHrs isEqualToString:@"1"]) {
            
        }else{
            
            totalCostLaborss=totalCostLaborss+laborCostt;
            
        }
        
        // totalCostLaborss=totalCostLaborss+laborCostt;
        
        NSString *strLaborType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborType"]];
        
        if ([strLaborType isEqualToString:@"H"]) {
            
            strHelperHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
            
        } else {
            
            strLaborHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
            
        }
        
    }
    
    float overAllAmt=totalCostPartss+totalCostLaborss+totalCostAdjustment;
    
    // Condition change for if laborcost is 0 then no cost adjustment is to be added
    
    if (totalCostLaborss<=0) {
        
        overAllAmt = overAllAmt-[strcostAdjustment floatValue];
        
    }
    
    // For Non standard Amount calculation
    
    if (isNonStandard) {
        
        if ((totalCostPartss==0) && (totalCostLaborss==0)) {
            
            if (strnonStdRepairAmt.length==0) {
                
                overAllAmt=overAllAmt+[strnonStdLaborAmt floatValue]+[strnonStdPartAmt floatValue];
                
            } else {
                
                overAllAmt=overAllAmt+[strnonStdRepairAmt floatValue];
                
            }
            
        }
        
    }
    
    //Change for saving Overall Amount All Time in DB
    
    NSString *strOverAllAmt=[NSString stringWithFormat:@"%f",overAllAmt];
    
    NSString *strWorkOrderIDd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"workorderId"]];
    
    NSString *strSubWorkOrderIdd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderId"]];
    
    SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
    
    [objSync fetchRepairToUpdateRepairAmt:strWorkOrderIDd :strSubWorkOrderIdd :strIssueIdToCheck :strIssueRepairIdToCheck :strOverAllAmt];
    
    cell.lblAmt.text=[NSString stringWithFormat:@"%.02f",overAllAmt];
    
    int repairQty = [strRepairQty intValue];
    
    float amountAfterAddingQty = [strOverAllAmt floatValue]*repairQty;
    
    cell.lblTotalAmount.text=[NSString stringWithFormat:@"%.02f",amountAfterAddingQty];
    
    if (strLaborHrs.length==0) {
        strLaborHrs=@"00:00";
    }
    if (strHelperHrs.length==0) {
        strHelperHrs=@"00:00";
    }
    
    cell.lblLR.text=strLaborHrs;
    cell.lblHLR.text=strHelperHrs;
    
    
    cell.switchStatus.tag=indexPath.row*1000+indexPath.section;
    //    cell.btnAddLabour.tag=indexPath.row*1000+indexPath.section;
    //    cell.btnDeleteRepair.tag=indexPath.row*1000+indexPath.section;
    //
    //    [cell.btnAddParts addTarget:self action:@selector(action_btnAddParts:) forControlEvents:UIControlEventTouchDown];
    //    [cell.btnAddLabour addTarget:self action:@selector(action_btnAddLabour:) forControlEvents:UIControlEventTouchDown];
    //    [cell.btnDeleteRepair addTarget:self action:@selector(action_btnDeleteRepair:) forControlEvents:UIControlEventTouchDown];
    
    [cell.switchStatus addTarget:self action:@selector(actionSwitch:) forControlEvents:UIControlEventValueChanged];
    
    if (isCompletedStatusMechanical) {
        
        [cell.switchStatus setEnabled:NO];
        
    }else{
        
        [cell.switchStatus setEnabled:YES];
        
    }
    
    NSString *strIsCompleted=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isCompleted"]];
    
    if (([strIsCompleted isEqualToString:@"true"] || [strIsCompleted isEqualToString:@"1"])) {
        
        [cell.switchStatus setOn:YES];
        //cell.switchStatus.onTintColor=[UIColor themeColor];
        
    }else{
        
        [cell.switchStatus setOn:NO];
        //cell.switchStatus.onTintColor=[UIColor redColor];
        
    }
    
    cell.btnViewMore.tag=indexPath.row*1000+indexPath.section;;
    [cell.btnViewMore addTarget:self action:@selector(action_ViewMoreJobDescription:) forControlEvents:UIControlEventTouchDown];
    
    if ([NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]].length>49) {
        
        cell.btnViewMore.hidden=NO;
        
    } else {
        
        cell.btnViewMore.hidden=YES;
        
    }
    
}


-(void)action_ViewMoreJobDescription:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }
        
    }
    
    NSManagedObject *objTemp=arrTemp[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"repairDesc"]];
    [global AlertMethod:@"Repair Description" :strJobDesc];
    
}

- (void)actionSwitch:(id)sender {
    
    UISwitch *btn = (UISwitch *)sender;
    
    int row= btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }
        
    }
    
    NSManagedObject *objTemp=arrTemp[row];
    
    if ([sender isOn]) {
        
        [self fetchSubWorkOrderRepairsViaIssuenRepairId:strIssueIdToCheck :[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]] :@"true"];
        
    } else {
        
        [self fetchSubWorkOrderRepairsViaIssuenRepairId:strIssueIdToCheck :[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]] :@"false"];
        
    }
    
    [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
}

-(void)action_btnAddParts:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= btn.tag/1000;
    
    int section =btn.tag%1000;
    
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp1=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp1 addObject:dictIssuesRepairData];
            
        }
        
    }
    
    NSManagedObject *objTemp1=arrTemp1[row];
    
    strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"issueRepairId"]];
    
    NSString *strSubWorkOrderIssuesIdToSend=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"subWorkOrderIssueId"]];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    AddPartsiPadViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"AddPartsiPadViewController"];
    objSignViewController.strWorkOrderId=strWorkOrderId;
    objSignViewController.strSubWorkOrderId=strSubWorkOrderIdGlobal;
    objSignViewController.strIssuePartsIdToFetch=strIssueRepairIdToCheck;
    objSignViewController.objSubWorkOrderdetails=_objSubWorkOrderdetails;
    objSignViewController.strSubWorkOrderIssuesIdToFetch=strSubWorkOrderIssuesIdToSend;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

-(void)action_btnAddLabour:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }
        
    }
    
    NSManagedObject *objTemp=arrTemp[row];
    
    strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
    NSString *strSubWorkOrderIssuesIdToSend=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWorkOrderIssueId"]];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    AddLaboriPadViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"AddLaboriPadViewController"];
    objSignViewController.strWorkOrderId=strWorkOrderId;
    objSignViewController.strSubWorkOrderId=strSubWorkOrderIdGlobal;
    objSignViewController.strIssueRepairIdToFetch=strIssueRepairIdToCheck;
    objSignViewController.objSubWorkOrderdetails=_objSubWorkOrderdetails;
    objSignViewController.strSubWorkOrderIssuesIdToFetch=strSubWorkOrderIssuesIdToSend;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

-(void)action_btnDeleteRepair:(id)sender
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert!"
                                  message:@"Are you sure you want to delete"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             UIButton *btn = (UIButton *)sender;
                             int row= btn.tag/1000;
                             
                             int section =btn.tag%1000;
                             
                             NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
                             
                             NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
                             
                             NSString *strIssueRepairIdToCheck;
                             
                             NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                             
                             for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
                                 
                                 NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
                                 
                                 NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
                                 
                                 if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                                     
                                     [arrTemp addObject:dictIssuesRepairData];
                                     
                                 }
                                 
                             }
                             
                             NSManagedObject *objTemp=arrTemp[row];
                             
                             strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"issueRepairId"]];
                             
                             [self deleteRepairFromDB:strWorkOrderId :strSubWorkOrderIdGlobal :strIssueIdToCheck :strIssueRepairIdToCheck :section :row];
                             
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


- (void)configureCellNotes:(MechanicalNotesTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *dictDataNotes=arrOfSubWorkServiceNotes[indexPath.row];
    
    cell.lblAddedBy.text=[NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"createdBy"]];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    NSString *strEmpNameLocal = [NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"employeeName"]];
    NSString *strLoggedInUserNameLocal = [NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"loggedInUserName"]];
    
    if ([cell.lblAddedBy.text isEqualToString:strLoggedInUserNameLocal]) {
        
        cell.lblAddedBy.text=[NSString stringWithFormat:@"%@",strEmpNameLocal];
        
    }
    
    cell.lblAddedOn.text=[global ChangeDateToLocalDateMechanicalParts:[NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"createdDate"]]];
    cell.lblNoteDesc.text=[NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"note"]];
    
    cell.btnViewMoreNoteDesc.tag=indexPath.row*1000+indexPath.section;;
    [cell.btnViewMoreNoteDesc addTarget:self action:@selector(action_ViewMoreNotesDescription:) forControlEvents:UIControlEventTouchDown];
    
    if ([NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"note"]].length>56) {
        
        cell.btnViewMoreNoteDesc.hidden=NO;
        
    } else {
        
        cell.btnViewMoreNoteDesc.hidden=YES;
        
    }
    
}
-(void)action_ViewMoreNotesDescription:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    NSManagedObject *dictDataNotes=arrOfSubWorkServiceNotes[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[dictDataNotes valueForKey:@"note"]];
    [global AlertMethod:@"Note Description" :strJobDesc];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==101)
    {
        
        [_buttonUpdateEstTime setTitle:[arrayUpdateTimeEst objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        NSArray *arry = [[arrayUpdateTimeEst objectAtIndex:indexPath.row] componentsSeparatedByString:@"Mins"];
        
        strUpdateEstTime = [[NSString stringWithFormat:@"%@",[arry firstObject]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [tblData removeFromSuperview];
        [viewBackGround removeFromSuperview];
        
        
    }
    else
    {
        
        if (!(arrDataTblView.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 101:
                {
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnPrioityAddServiceIssues setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                    strPriorityIdGlobal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PriorityId"]];
                    break;
                }
                case 102:
                {
                    
                    strCategoryMasterId=@"";
                    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
                    [_btnSelectParts setTitle:@"----Select Part----" forState:UIControlStateNormal];
                    [_btnSelectdate setTitle:@"----Select Date----" forState:UIControlStateNormal];
                    _txtViewDesc.text=@"";
                    _txtQty.text=@"";
                    _txtModel.text=@"";
                    _txtSerial.text=@"";
                    _txtUnitPrice.text=@"";
                    strTxtUnitPrice=@"";
                    _txtTotalPrice.text=@"";
                    _txtManufacturer.text=@"";
                    _txtpartName.text=@"";
                    
                    // Vendor Details
                    strVendorSysNameSelected = @"";
                    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
                    _txtFldVendoreQuoteNo.text = @"";
                    _txtFldVendorPartNo.text = @"";
                    
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectCategory setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                    strCategoryMasterId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategoryMasterId"]];
                    strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
                    
                    break;
                }
                case 103:
                {
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectHelper setTitle:[dictData valueForKey:@"FullName"] forState:UIControlStateNormal];
                    strHelperIdGlobal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EmployeeNo"]];
                    break;
                }
                case 104:
                {
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectReason setTitle:[dictData valueForKey:@"ResetReason"] forState:UIControlStateNormal];
                    //                strResertId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ResetId"]];
                    break;
                }
                case 105:
                {
                    isChangeStdPartPrice=NO;
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectParts setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                    [_txtQty setEnabled:YES];
                    
                    _txtViewDesc.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Description"]];
                    
                    float unitPriceFloat=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BestPrice"]] floatValue];
                    
                    _txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
                    strTxtUnitPrice=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
                    dictDataPartsSelected=dictData;
                    
                    NSString *strType;
                    
                    if (isStandard) {
                        
                        strType=@"Standard";
                        
                    } else {
                        
                        strType=@"Non-Standard";
                        
                    }
                    
                    if (strCategorySysNameSelected.length==0) {
                        
                        strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
                        
                    }
                    
                    NSString *strPartCate=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
                    
                    if (strPartCate.length==0) {
                        
                        strCategorySysNameSelected=@"";
                        
                    }else{
                        
                        strCategorySysNameSelected=strPartCate;
                        
                    }
                    
                    
                    NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",unitPriceFloat] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :strType];
                    
                    float multiplier=[strMultiplier floatValue];
                    
                    strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];
                    
                    NSString *strTextQTY=[NSString stringWithFormat:@"%@",_txtQty.text];
                    
                    _txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",multiplier*[strTxtUnitPrice floatValue]];
                    
                    if (strTextQTY.length>0) {
                        
                        float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];
                        
                        _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                        
                    }
                    
                    // Vendor Details
                    strVendorSysNameSelected = @"";
                    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
                    itemMasterID = [[dictData valueForKey:@"ItemMasterId"] integerValue];
                    
                    break;
                }
                case 106:
                {
                    //Issues
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectIssues setTitle:[dictData valueForKey:@"serviceIssue"] forState:UIControlStateNormal];
                    strIssueIdGlobalSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"subWorkOrderIssueId"]];
                    break;
                }
                case 107:
                {
                    //Repairs
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectRepairParts setTitle:[dictData valueForKey:@"repairName"] forState:UIControlStateNormal];
                    strRepairIdGlobalSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"issueRepairId"]];
                    break;
                }// Vendor Details
                case 108:
                {
                    NSString *strVendorNameSelected = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"Name"];
                    strVendorSysNameSelected = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"SysName"];
                    [_btnVendorName setTitle:strVendorNameSelected forState:UIControlStateNormal];
                    
                    /*
                     
                     // Vendor Details
                     strVendorSysNameSelected = @"";
                     [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
                     _txtFldVendoreQuoteNo.text = @"";
                     _txtFldVendorPartNo.text = @"";
                     
                     */
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }
    }
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
}

-(NSString*)logicForFetchingMultiplier :(NSString*)strPriceToLookUp :(NSString*)strCategorySysName :(NSString*)strType{
    
    NSString *strMultiplier;
    
    [self getPricLookupFromMaster:strCategorySysName];

    strMultiplier=[global logicForFetchingMultiplier:strPriceToLookUp :strCategorySysName :strType :arrOfPriceLookup];
    
    
    return strMultiplier;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return false;
    
    //    if ([strWoStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strWoStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame) {
    //
    //        return false;
    //
    //    }else{
    //
    //        if (tableView.tag==2) {
    //
    //            return true;
    //
    //        }else
    //
    //            return false;
    //
    //    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==2) {
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:@"Are you sure you want to delete"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     NSManagedObject *dictDataHelper=arrOfSubWorkServiceHelper[indexPath.row];
                                     
                                     [self deleteHelperFromDB:[NSString stringWithFormat:@"%@",[dictDataHelper valueForKey:@"employeeNo"]]];
                                     
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1) {
        return arrOfSubWorkServiceIssues.count;
    }else{
        return 1;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (tableView.tag==1) {
        return 50;
    }else{
        return 0;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (tableView.tag==1) {
        return 5;
    }else{
        return 0;
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = tableView.frame;
    
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-160, 0, 150, 50)];
    [addButton setTitle:@"Add Repair" forState:UIControlStateNormal];
    addButton.backgroundColor = [UIColor themeColorBlack];
    addButton.titleLabel.font=[UIFont systemFontOfSize:22];
    
    if (isCompletedStatusMechanical) {
        
        [addButton setHidden:YES];
        
    }
    
    addButton.tag=section;
    // [addButton addTarget:self action:@selector(clickAddRepair:) forControlEvents:UIControlEventTouchDown];
    UIButton *minusButton;
    
    //if (section==indexToShow) {
    if ([arrOfSectionsToOpen containsObject:[NSString stringWithFormat:@"%ld",(long)section]]) {
        
        minusButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 0, 50, 50)];
        [minusButton setTitle:@"-" forState:UIControlStateNormal];
        minusButton.backgroundColor = [UIColor themeColorBlack];
        minusButton.titleLabel.font=[UIFont systemFontOfSize:22];
        
        minusButton.tag=section;
        [minusButton addTarget:self action:@selector(clickMinusSection:) forControlEvents:UIControlEventTouchDown];
        
        //[self methodToOpenSection:(int)section];
        
    } else {
        
        minusButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 0, 50, 50)];
        [minusButton setTitle:@"+" forState:UIControlStateNormal];
        minusButton.backgroundColor = [UIColor themeColorBlack];
        minusButton.titleLabel.font=[UIFont systemFontOfSize:22];
        
        minusButton.tag=section;
        [minusButton addTarget:self action:@selector(clickPlusSection:) forControlEvents:UIControlEventTouchDown];
        
    }
    
    NSManagedObject *dictData=arrOfSubWorkServiceIssues[section];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, frame.size.width-170, 50)];
    title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"serviceIssue"]];
    title.text = [NSString stringWithFormat:@"%@ :%@",[dictData valueForKey:@"serviceIssue"],[dictData valueForKey:@"equipmentName"]];
    title.backgroundColor=[UIColor clearColor];
    title.font=[UIFont systemFontOfSize:22];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [headerView addSubview:title];
    //[headerView addSubview:addButton];
    [headerView addSubview:minusButton];
    
    return headerView;
}

//============================================================================
//============================================================================
#pragma mark - --------------------Service Issues METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_PriorityAddServiceIssues:(id)sender {
    
    [_btnPrioityAddServiceIssues setTitle:@"----Select Priority----" forState:UIControlStateNormal];
    [self hideAllTxtViewsnFields];
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfStatus=[dictDetailsFortblView valueForKey:@"PriorityMasters"];
    
    for (int k=0; k<arrOfStatus.count; k++) {
        
        NSDictionary *dictDataa=arrOfStatus[k];
        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
        if (isActive) {
            
            [arrDataTblView addObject:dictDataa];
            
        } else {
            
            //            if (YesUpdateInactiveCheck) {
            //
            //                long long  longEmployeeId=[[dictDataa valueForKey:@"PriorityId"] longLongValue];
            //                long long  longEmployeeIdToCheck=[strPriorityId longLongValue];
            //                if (longEmployeeId==longEmployeeIdToCheck) {
            //
            //                    [arrDataTblView addObject:dictDataa];
            //
            //                }
            //            }
        }
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=101;
        [self tableLoad:tblData.tag];
    }
    
    
}

- (IBAction)action_SaveServiceIssues:(id)sender {
    
    if (_txtViewIssuesDesc.text.length==0) {
        
        [global AlertMethod:Alert :@"Please enter issue description"];
        
    } else if(strPriorityIdGlobal.length==0){
        
        [global AlertMethod:Alert :@"Please select priority"];
        
    } else{
        
        [self hideAllTxtViewsnFields];
        
        [self addServiceIssuesFromMobileToDB];
        
        [viewBackGround removeFromSuperview];
        
    }
    
}

- (IBAction)action_CancelServiceIssuesView:(id)sender {
    
    [self hideAllTxtViewsnFields];
    
    [viewBackGround removeFromSuperview];
    
}

//============================================================================
//============================================================================
#pragma mark - --------------------Dynamic Tableview METHODS-----------------
//============================================================================
//============================================================================


-(void)setTableFrame
{
    viewBackGroundOnView.tag=202;
    viewBackGroundOnView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundOnView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGroundOnView];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 500);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGroundOnView];
    [viewBackGroundOnView addSubview:tblData];
    
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 60)];
    
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont systemFontOfSize:25];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(methodDonePriorityTableview) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGroundOnView addSubview:btnDone];
    
    
    UIButton *btnAddNew=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.size.width/2+2+tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 60)];
    
    [btnAddNew setTitle:@"Cancel" forState:UIControlStateNormal];
    btnAddNew.titleLabel.font=[UIFont systemFontOfSize:25];
    [btnAddNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnAddNew.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [btnAddNew addTarget:self action:@selector(methodCancelPriorityTableview) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGroundOnView addSubview:btnAddNew];
    
}

-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
        case 103:
        {
            [self setTableFrame];
            break;
        }
        case 104:
        {
            [self setTableFrame];
            break;
        }
        case 105:
        {
            [self setTableFrame];
            break;
        }
        case 106:
        {
            [self setTableFrame];
            break;
        }
        case 107:
        {
            [self setTableFrame];
            break;
        }// Vendor Details
        case 108:
        {
            [self setTableFrame];
            break;
        }
            
        default:
            break;
    }
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}

//============================================================================
//============================================================================
#pragma mark- --------------------Repair METHODS-----------------
//============================================================================
//============================================================================


- (IBAction)action_SelectRepair:(id)sender {
    
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    //@"MasterAllMechanical"
    NSDictionary *dictMaster=[defs valueForKey:@"MasterAllMechanical"];
    NSArray *arrOfStatus=[dictMaster valueForKey:@"RepairMasterExtSerDc"];
    
    for (int k=0; k<arrOfStatus.count; k++) {
        
        NSDictionary *dictDataa=arrOfStatus[k];
        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
        if (isActive) {
            
            if ([[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]] isEqualToString:strDepartMentSysName]) {
                
                [arrDataTblView addObject:dictDataa];
                
            }
            
        } else {
            
            //            if (YesUpdateInactiveCheck) {
            //
            //                long long  longEmployeeId=[[dictDataa valueForKey:@"PriorityId"] longLongValue];
            //                long long  longEmployeeIdToCheck=[strPriorityId longLongValue];
            //                if (longEmployeeId==longEmployeeIdToCheck) {
            //
            //                    [arrDataTblView addObject:dictDataa];
            //
            //                }
            //            }
        }
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=102;
        [self tableLoad:tblData.tag];
    }
    
}
- (IBAction)action_SaveRepair:(id)sender {
    
    if (isStandardRepair) {
        // Is Standard repair
        
        if (strRepairIdGlobal.length==0) {
            
            [global AlertMethod:Alert :@"Please select repair"];
            
        } else {
            
            [self addServiceIssuesRepairFromMobileToDB];
            
            [_btnSelectRepair setTitle:@"----Select Repair----" forState:UIControlStateNormal];
            
            strRepairIdGlobal=@"";
            
            [viewBackGround removeFromSuperview];
            [_view_AddRepair removeFromSuperview];
            
        }
        
    } else {
        
        if (_txtRepairName.text.length==0) {
            
            [global AlertMethod:Alert :@"Please enter repair name"];
            
        } else {
            
            [self addServiceIssuesRepairNonStandardFromMobileToDB];
            
            _txtRepairName.text=@"";
            _txtRepairName.placeholder=@"Enter Repair Name";
            
            [viewBackGround removeFromSuperview];
            [_view_AddRepair removeFromSuperview];
            
        }
        
    }
    
}

- (IBAction)action_CancelRepair:(id)sender {
    
    [viewBackGround removeFromSuperview];
    [_view_AddRepair removeFromSuperview];
    
}

- (IBAction)action_AddRepair:(id)sender {
    
    
    [self methodAddRepairView];
    
}

//============================================================================
//============================================================================
#pragma mark- --------------------Helper METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_Selecthelper:(id)sender {
    
    
    [self hideAllTxtViewsnFields];
    
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
    
    for (int k=0; k<arrOfEmployee.count; k++) {
        
        NSDictionary *dictDataa=arrOfEmployee[k];
        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
        if (isActive) {
            
            [arrDataTblView addObject:dictDataa];
            
        } else {
            
            //            if (YesUpdateInactiveCheck) {
            //
            //                long long  longEmployeeId=[[dictDataa valueForKey:@"EmployeeId"] longLongValue];
            //                long long  longEmployeeIdToCheck=[strSalesPersonId longLongValue];
            //                if (longEmployeeId==longEmployeeIdToCheck) {
            //
            //                    [arrDataTblView addObject:dictDataa];
            //
            //                }
            //            }
        }
    }
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=103;
        [self tableLoad:tblData.tag];
    }
    
    
}

- (IBAction)action_AddHelper:(id)sender {
    
    if (strHelperIdGlobal.length==0) {
        
        [global AlertMethod:Alert :@"Please select helper to add"];
        
    } else {
        
        BOOL isAlreadyThere;
        
        isAlreadyThere=NO;
        
        for (int k=0; k<arrOfSubWorkServiceHelper.count; k++) {
            
            NSManagedObject *objTemp=arrOfSubWorkServiceHelper[k];
            
            NSString *strHelperId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
            
            if ([strHelperIdGlobal isEqualToString:strHelperId]) {
                
                isAlreadyThere=YES;
                break;
            }
        }
        
        if (isAlreadyThere) {
            
            [global AlertMethod:Alert :@"Helper already added. Please select another helper to add"];
            
        } else {
            
            [_btnSelectHelper setTitle:@"----Select Helper----" forState:UIControlStateNormal];
            
            [self addServiceHelperFromMobileToDB];
            
            strHelperIdGlobal=@"";
            
        }
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- --------------------NOTES METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_AddNotes:(id)sender {
    
    _txtViewAddNotesDesc.text=@"";
    [_txtViewAddNotesDesc becomeFirstResponder];
    
    [self methodAddNotesView];
    
}
- (IBAction)action_SaveNotes:(id)sender {
    
    if (_txtViewAddNotesDesc.text.length==0) {
        
        [global AlertMethod:Alert :@"Please enter Notes description"];
        
    } else{
        
        [self hideAllTxtViewsnFields];
        
        [self addServiceNotesFromMobileToDB];
        
        [viewBackGround removeFromSuperview];
        [_viewAddNotes removeFromSuperview];
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        
    }
    
}

- (IBAction)action_CancelNotes:(id)sender {
    
    [self hideAllTxtViewsnFields];
    [viewBackGround removeFromSuperview];
    [_viewAddNotes removeFromSuperview];
    
}

//============================================================================
//============================================================================
#pragma mark- --------------------Client Approval METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_StartJob:(id)sender {
    
    if ([_btnStartnStopJOB.titleLabel.text isEqualToString:@"Start Job"] || [_btnStartnStopJOB.currentTitle isEqualToString:@"Start Job"]) {
        
        [self addStartActualHoursFromMobileToDB];
        
        [self fetchSubWorkOrderActualHrsFromDataBaseForMechanical];
        
        [self timerStarthere];
        
        [_btnStartnStopJOB setTitle:@"Pause Job" forState:UIControlStateNormal];
        
        [_objSubWorkOrderdetails setValue:@"Start" forKey:@"clockStatus"];
        
        NSError *error2;
        [context save:&error2];
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        
    } else {
        
        [self methodAddReasonStopJobView];
        
        // Create dynamic view for employee time sheet slot wise.....
        arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
        arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
        
        arrOfGlobalDynamicEmpSheetFinal=[global createEmpSheetDataInBackground:strWorkOrderId :strSubWorkOrderIdGlobal :arrOfHoursConfig :strEmpID :strEmployeeNoLoggedIn :strEmpName :@"Front" :@""];
        
        arrOfHeaderTitleForSlots=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
        
        [self createDynamicEmpSheet:arrOfGlobalDynamicEmpSheetFinal :arrOfHeaderTitleForSlots];
        
    }
    
}


//---------------------PTBN--------------
//---------------------
// Same method on all four view so if change in one copy in all other view also
//---------------------

-(void)createDynamicEmpSheet :(NSArray*)arrOfEmpSheet :(NSArray*)arrOfHeaderTitle{
    
    for(UIView *view in scrollViewEmpTimeSheet.subviews)
    {
        [view removeFromSuperview];
    }
    
    arrOfHeaderTitle=arrOfHeaderTitleForSlots;
    //    arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
    //    [arrOfGlobalDynamicEmpSheetFinal addObjectsFromArray:arrOfEmpSheet];
    
    CGFloat scrollViewHeight=0.0;
    CGFloat scrollViewWidth=0.0;
    
    scrollViewHeight=arrOfGlobalDynamicEmpSheetFinal.count*60+60;
    scrollViewWidth=arrOfHeaderTitle.count*100;
    
    //scrollViewEmpTimeSheet=[[UIScrollView alloc]initWithFrame:CGRectMake(scrollViewEmpTimeSheet.frame.origin.x, scrollViewEmpTimeSheet.frame.origin.y, scrollViewWidth, scrollViewHeight)];
    
    UIView *viewEmpSheetOnScroll=[[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollViewEmpTimeSheet.frame.size.width, scrollViewEmpTimeSheet.frame.size.height)];
    
    CGFloat xAxisHeaderTitle=10;
    
    for (int i=0; i<=arrOfHeaderTitleForSlots.count; i++) {
        
        UILabel *lblTitleHeader=[[UILabel alloc]init];
        lblTitleHeader.backgroundColor=[UIColor lightTextColor];
        lblTitleHeader.layer.borderWidth = 1.5f;
        lblTitleHeader.layer.cornerRadius = 0.0f;
        lblTitleHeader.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        if (i==0) {
            
            lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, 0, 200, 50);
            xAxisHeaderTitle=xAxisHeaderTitle+200;
            
            
        }else{
            
            lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, 0, 100, 50);
            xAxisHeaderTitle=xAxisHeaderTitle+100;
            
            
        }
        //lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, yAxisMain+60, 100, 50);
        lblTitleHeader.font=[UIFont systemFontOfSize:22];
        lblTitleHeader.backgroundColor=[UIColor lightTextColorTimeSheet];
        if (i==0) {
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@""];
            
        } else if(i==1){
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Regular"];
            
        }else if(i==arrOfHeaderTitleForSlots.count){
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Holiday"];
            
        }else {
            
            //lblTitleHeader.text=[NSString stringWithFormat:@"%@",arrOfHeaderTitle[i]];
            lblTitleHeader.text=[global slotHeaderTitleFromTimeSlot:arrOfHoursConfig :[NSString stringWithFormat:@"%@",arrOfHeaderTitle[i-1]]];
            
        }
        
        lblTitleHeader.textAlignment=NSTextAlignmentCenter;
        lblTitleHeader.numberOfLines=700;
        [lblTitleHeader setAdjustsFontSizeToFitWidth:YES];
        
        [viewEmpSheetOnScroll addSubview:lblTitleHeader];
        
    }
    
    CGFloat yAxis=50;
    
    for (int k=0; k<arrOfGlobalDynamicEmpSheetFinal.count; k++) {
        
        CGFloat xAxis=210;
        
        NSDictionary *dictDataEmpSheet=arrOfGlobalDynamicEmpSheetFinal[k];
        
        UILabel *lblTitleEmpName=[[UILabel alloc]init];
        lblTitleEmpName.backgroundColor=[UIColor clearColor];
        lblTitleEmpName.layer.borderWidth = 1.5f;
        lblTitleEmpName.layer.cornerRadius = 0.0f;
        lblTitleEmpName.layer.borderColor = [[UIColor grayColor] CGColor];
        lblTitleEmpName.frame=CGRectMake(10, yAxis, 200, 50);
        lblTitleEmpName.font=[UIFont systemFontOfSize:22];
        lblTitleEmpName.text=[NSString stringWithFormat:@"%@",[dictDataEmpSheet valueForKey:@"EmployeeName"]];
        lblTitleEmpName.textAlignment=NSTextAlignmentCenter;
        lblTitleEmpName.numberOfLines=700;
        [lblTitleEmpName setAdjustsFontSizeToFitWidth:YES];
        lblTitleEmpName.backgroundColor=[UIColor lightTextColorTimeSheet];
        [viewEmpSheetOnScroll addSubview:lblTitleEmpName];
        
        
        NSArray *arrEmpData=[dictDataEmpSheet valueForKey:@"EmployeeList"];
        
        for (int k1=0; k1<arrEmpData.count; k1++) {
            
            NSDictionary *dictDataaaa=arrEmpData[k1];
            CGRect txtViewFrame = CGRectMake(xAxis, yAxis,100,50);
            UITextField *txtView= [[UITextField alloc] init];
            //txtView.text = [global getHrAndMinsGlobal:[dictDataaaa valueForKey:@"actualDurationInMin"]];
            txtView.text = [global getHrAndMinsGlobal:[dictDataaaa valueForKey:@"billableDurationInMin"]];
            
            if ([txtView.text isEqualToString:@"00:00"]) {
                
                txtView.text =@"";
                txtView.placeholder=@"00:00";
                
            }
            
            txtView.frame =txtViewFrame;
            txtView.layer.borderWidth = 1.5f;
            txtView.layer.cornerRadius = 0.0f;
            txtView.layer.borderColor = [[UIColor grayColor] CGColor];
            [txtView setBackgroundColor:[UIColor clearColor]];
            txtView.font=[UIFont systemFontOfSize:20];
            txtView.delegate=self;
            
            UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
            leftView.backgroundColor = [UIColor clearColor];
            txtView.leftView = leftView;
            txtView.leftViewMode = UITextFieldViewModeAlways;
            txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            
            txtView.tag=k*1000+k1+500;
            
            [viewEmpSheetOnScroll addSubview:txtView];
            
            xAxis=xAxis+100;
            
        }
        
        yAxis=yAxis+50;
        
    }
    
    [scrollViewEmpTimeSheet addSubview:viewEmpSheetOnScroll];
    
    [_viewStopJobReason addSubview:scrollViewEmpTimeSheet];
    
    [scrollViewEmpTimeSheet setContentSize:CGSizeMake(scrollViewWidth,scrollViewHeight)];
    
    scrollViewEmpTimeSheet.scrollEnabled=true;
    scrollViewEmpTimeSheet.showsHorizontalScrollIndicator=true;
    scrollViewEmpTimeSheet.showsVerticalScrollIndicator=true;
    
}


-(void)timerStarthere
{
    
    secondsTimer=totalsecond;
    
    // [_btnTimeClock setTitle:[NSString stringWithFormat:@"%d",secondsTimer] forState:UIControlStateNormal];
    
    if (!jobMinTimer) {
        
        jobMinTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                       target:self
                                                     selector:@selector(timer)
                                                     userInfo:nil
                                                      repeats:YES];
        
        [[NSRunLoop currentRunLoop] addTimer:jobMinTimer
                                     forMode:NSRunLoopCommonModes];
    }
    
    //    jobMinTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
    //                                                   target:self
    //                                                 selector:@selector(timer)
    //                                                 userInfo:nil
    //                                                  repeats:YES];
}
- (void)timer {
    
    secondsTimer++;
    
    NSDictionary *dictTimeClock=[self createTimemapForSeconds:secondsTimer];
    
    NSString *strTimeFormat=[NSString stringWithFormat:@"%02d:%02d:%02d",[[dictTimeClock valueForKey:@"h"] intValue],[[dictTimeClock valueForKey:@"m"] intValue],[[dictTimeClock valueForKey:@"s"] intValue]];
    
    [_btnTimeClock setTitle:strTimeFormat forState:UIControlStateNormal];
    
}

- (void)countUp
{
    secondsTimer += 1;
    
    if (secondsTimer == 60)
    {
        secondsTimer = 0;
        minutesTimer++;
        
        if (minutesTimer == 60)
        {
            minutesTimer = 0;
            hoursTimer++;
        }
    }
    
}

- (IBAction)action_ClientApproval:(id)sender {
    
    
    
}

- (IBAction)action_StartRepair:(id)sender {
    
    
    
}

//============================================================================
//============================================================================
#pragma mark- --------------------Stop Job METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_SelectReason:(id)sender {
    
    [self methodTableViewAllocation];
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfLogType;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictDetailsFortbl=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    if ([dictDetailsFortbl isKindOfClass:[NSString class]]) {
        
        arrOfLogType=nil;
        
    }else{
        
        arrOfLogType=[dictDetailsFortbl valueForKey:@"ResetReasons"];
        
    }
    
    if ([arrOfLogType isKindOfClass:[NSArray class]]) {
        for (int k=0; k<arrOfLogType.count; k++) {
            
            NSDictionary *dictDataa=arrOfLogType[k];
            [arrDataTblView addObject:dictDataa];
            
        }
    }
    //    }else{
    //        [global AlertMethod:Info :NoDataAvailable];
    //    }
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailableReset];
    }else{
        tblData.tag=104;
        [self tableLoad:tblData.tag];
    }
    
    
}

- (IBAction)action_SaveStopReason:(id)sender {
    
    if ([_btnSelectReason.titleLabel.text isEqualToString:@"----Select Reason----"]) {
        
        [global AlertMethod:Alert :@"Please select reason to stop job"];
        
    } else {
        
        [jobMinTimer invalidate];
        
        [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOut];
        
        [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
        [viewBackGround removeFromSuperview];
        [_viewStopJobReason removeFromSuperview];
        
        [_btnSelectReason setTitle:@"----Select Reason----" forState:UIControlStateNormal];
        
        [_objSubWorkOrderdetails setValue:@"Stop" forKey:@"clockStatus"];
        
        NSError *error2;
        [context save:&error2];
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        
        [global saveEmployeeTimeSheetSlotWise:arrOfGlobalDynamicEmpSheetFinal];
        
        //[self goToGeneralInfoView];
        [self goToAppointmentViewOnStopJob];
        
    }
    
}

-(void)goToAppointmentViewOnStopJob
{
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
        AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
        AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}

- (IBAction)action_CancelStopJob:(id)sender {
    
    [viewBackGround removeFromSuperview];
    [_viewStopJobReason removeFromSuperview];
    
}

//============================================================================
//============================================================================
#pragma mark- --------------------Add Service Issues in Core Data METHODS-----------------
//============================================================================
//============================================================================

-(void)addServiceIssuesFromMobileToDB{
    
    isCollapseAllViews=YES;
    
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderIssueDcs *objSubWorkOrderIssues = [[MechanicalSubWorkOrderIssueDcs alloc]initWithEntity:entitySubWorkOrderIssues insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    objSubWorkOrderIssues.subWorkOrderIssueId=[global getReferenceNumber];
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.serviceIssue=_txtViewIssuesDesc.text;
    objSubWorkOrderIssues.priority=[NSString stringWithFormat:@"%@",strPriorityIdGlobal];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
}

-(void)addServiceHelperFromMobileToDB{
    
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    
    MechanicalSubWOTechHelperDcs *objSubWorkOrderHelper = [[MechanicalSubWOTechHelperDcs alloc]initWithEntity:entityMechanicalSubWorkOrderHelper insertIntoManagedObjectContext:context];
    
    objSubWorkOrderHelper.workorderId=strWorkOrderId;
    objSubWorkOrderHelper.subWorkOrderTechnicianId=[global getReferenceNumber];
    objSubWorkOrderHelper.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderHelper.employeeNo=[NSString stringWithFormat:@"%@",strHelperIdGlobal];
    objSubWorkOrderHelper.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderHelper.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderHelper.createdDate=[global strCurrentDate];
    objSubWorkOrderHelper.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderHelper.modifiedDate=[global strCurrentDate];
    objSubWorkOrderHelper.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderHelperFromDataBaseForMechanical];
    
}

-(void)addServiceNotesFromMobileToDB{
    
    entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderNoteDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderNoteDcs alloc]initWithEntity:entityMechanicalSubWorkOrderNotes insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWorkOrderId;
    objSubWorkOrderNotes.subWorkOrderNoteId=[global getReferenceNumber];
    objSubWorkOrderNotes.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderNotes.note=_txtViewAddNotesDesc.text;
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.createdDate=[global strCurrentDate];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.modifiedDate=[global strCurrentDate];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderNotesFromDataBaseForMechanical];
    
}

-(void)addStartActualHoursFromMobileToDB{
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWorkOrderId;
    
    NSString *strNos=[global getReferenceNumber];
    objSubWorkOrderNotes.subWOActualHourId=strNos;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:strNos forKey:@"subWOActualHourId"];
    [defs synchronize];
    
    objSubWorkOrderNotes.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderNotes.timeIn=[global strCurrentDateFormattedForMechanical];
    objSubWorkOrderNotes.status=@"Running";
    objSubWorkOrderNotes.timeOut=@"";
    objSubWorkOrderNotes.mobileTimeIn=objSubWorkOrderNotes.timeIn;
    objSubWorkOrderNotes.mobileTimeOut=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKey;
    objSubWorkOrderNotes.reason=@"";
    objSubWorkOrderNotes.actHrsDescription=@"";
    objSubWorkOrderNotes.subWorkOrderNo=@"";
    objSubWorkOrderNotes.subWOActualHourNo=@"";
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.createdDate=[global strCurrentDate];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.modifiedDate=[global strCurrentDate];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    // Latitude changes
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objSubWorkOrderNotes.latitude=latitude;
    objSubWorkOrderNotes.longitude=longitude;
    // Latitude changes end
    
    objSubWorkOrderNotes.employeeNo=[global getEmployeeDeatils];
    
    NSError *error2;
    [context save:&error2];
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        strNotificationTypeName=@"MechanicalActualHrsSyncStartFR2";
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:strNotificationTypeName object:nil];
        
        SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
        
        [objSync syncMechanicalActualHours:strWorkOrderId :strSubWorkOrderIdGlobal :strNos :strNotificationTypeName];
        
    }else{
        
        [self stopLoader];
        
    }
    
}

-(void)stopLoader
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:strNotificationTypeName
                                                  object:nil];
    
    [DejalBezelActivityView removeView];
    
}


-(void)addServiceIssuesRepairFromMobileToDB{
    
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepair insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    
    NSString *strIssueRepairTdLocal=[global getReferenceNumber];
    objSubWorkOrderIssues.issueRepairId=strIssueRepairTdLocal;
    
    objSubWorkOrderIssues.subWorkOrderIssueId=strSubWorkOrderIssueIdGlobal;
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.repairMasterId=strRepairIdGlobal;
    objSubWorkOrderIssues.repairName=[NSString stringWithFormat:@"%@",_btnSelectRepair.titleLabel.text];
    objSubWorkOrderIssues.repairDesc=[NSString stringWithFormat:@"%@",_txtViewRepairDescriptions.text];
    objSubWorkOrderIssues.costAdjustment=[NSString stringWithFormat:@"%@",_txtRepairCostAdjustment.text];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    NSDictionary *dictDataRepairMaster=arrDataTblView[indexRepairGlobal];
    NSArray *arrOfPartsMaster=[dictDataRepairMaster valueForKey:@"RepairPartMasterDcs"];
    
    for (int k=0; k<arrOfPartsMaster.count; k++) {
        
        NSDictionary *dictDataPartss=arrOfPartsMaster[k];
        
        NSDictionary *dictDataPartssMaster=[self fetchPartDetailDictionaryViaPartCode:[NSString stringWithFormat:@"%@",[dictDataPartss valueForKey:@"PartCode"]]];
        
        [self addServiceIssuesRepairPartsFromMobileToDB:dictDataPartss :dictDataPartssMaster :strIssueRepairTdLocal];
        
    }
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"H" :[dictLaborInfoToSave valueForKey:@"totalPriceLabour"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"LaborHours"] :strIssueRepairTdLocal];
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"L" :[dictLaborInfoToSave valueForKey:@"totalPriceHelper"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"HelperHours"] :strIssueRepairTdLocal];
    
    
    //Fetch All Data After Saving in DB
    
    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
    
    // [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
}

-(void)addServiceIssuesRepairNonStandardFromMobileToDB{
    
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepair insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    
    NSString *strIssueRepairTdLocal=[global getReferenceNumber];
    objSubWorkOrderIssues.issueRepairId=strIssueRepairTdLocal;
    
    objSubWorkOrderIssues.subWorkOrderIssueId=strSubWorkOrderIssueIdGlobal;
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.repairMasterId=@"";
    objSubWorkOrderIssues.repairName=[NSString stringWithFormat:@"%@",_txtRepairName.text];
    objSubWorkOrderIssues.repairDesc=[NSString stringWithFormat:@"%@",_txtViewRepairDescriptions.text];
    objSubWorkOrderIssues.costAdjustment=[NSString stringWithFormat:@"%@",_txtRepairCostAdjustment.text];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    dictLaborInfoToSave = @{@"LaborHours":[NSString stringWithFormat:@"%@",@"00:00"],
                            @"HelperHours":[NSString stringWithFormat:@"%@",@"00:00"],
                            @"totalPriceLabour":[NSString stringWithFormat:@"%@",@"0.0"],
                            @"totalPriceHelper": [NSString stringWithFormat:@"%@",@"0.0"],
                            @"DescRepair": [NSString stringWithFormat:@"%@",@"0.0"],
                            };
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"H" :[dictLaborInfoToSave valueForKey:@"totalPriceLabour"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"LaborHours"] :strIssueRepairTdLocal];
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"L" :[dictLaborInfoToSave valueForKey:@"totalPriceHelper"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"HelperHours"] :strIssueRepairTdLocal];
    
    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
    
}
-(void)addServiceIssuesRepairPartsFromMobileToDB :(NSDictionary*)dictPartsData :(NSDictionary*)dictPartsDataFromMasterEquip :(NSString *)strRepairIDD{
    
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    objSubWorkOrderIssues.subWorkOrderIssueId=strSubWorkOrderIssueIdGlobal;
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.issueRepairPartId=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"RepairPartMasterId"]];
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",strRepairIDD];
    
    NSString *srtPartType;
    if (isStandardRepair) {
        
        srtPartType=@"Standard";
    } else {
        
        srtPartType=@"Non-Standard";
    }
    
    objSubWorkOrderIssues.partType=srtPartType;
    objSubWorkOrderIssues.partCode=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"PartCode"]];
    objSubWorkOrderIssues.partName=[NSString stringWithFormat:@"%@",[dictPartsDataFromMasterEquip valueForKey:@"Name"]];
    objSubWorkOrderIssues.partDesc=[NSString stringWithFormat:@"%@",[dictPartsDataFromMasterEquip valueForKey:@"Description"]];
    objSubWorkOrderIssues.qty=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"Qty"]];
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"Qty"]];
    objSubWorkOrderIssues.unitPrice=[NSString stringWithFormat:@"%@",[dictPartsDataFromMasterEquip valueForKey:@"LookupPrice"]];
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.installationDate=[NSString stringWithFormat:@"%@",@""];
    objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",@"1"];
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    if (isChangeStdPartPrice) {
        
        objSubWorkOrderIssues.isChangeStdPartPrice=[NSString stringWithFormat:@"%@",@"true"];
        objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",@"1"];
        
    }else{
        
        objSubWorkOrderIssues.isChangeStdPartPrice=[NSString stringWithFormat:@"%@",@"false"];
        
    }
    
    NSError *error2;
    [context save:&error2];
    
    //    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    //
    //    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
}

-(void)addServiceIssuesRepairLabourFromMobileToDB :(NSString*)strLaborType :(NSString*)strLaborCostt :(NSString*)strRepairDescriptionn :(NSString*)strLaborHourss :(NSString*)strIssueRepairTdLocal{
    
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    
    MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssues = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairLabour insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    objSubWorkOrderIssues.issueRepairLaborId=[global getReferenceNumber];
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",strIssueRepairTdLocal];
    
    NSString *srtLaborCostType;
    if (isStandardSubWorkOrder) {
        
        srtLaborCostType=@"SC";
        
    } else {
        
        srtLaborCostType=@"AHC";
        
    }
    
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    
    objSubWorkOrderIssues.laborCost=[NSString stringWithFormat:@"%@",strLaborCostt];
    objSubWorkOrderIssues.laborCostType=[NSString stringWithFormat:@"%@",srtLaborCostType];
    objSubWorkOrderIssues.laborDescription=[NSString stringWithFormat:@"%@",strRepairDescriptionn];
    objSubWorkOrderIssues.laborHours=[NSString stringWithFormat:@"%@",strLaborHourss];
    objSubWorkOrderIssues.laborType=[NSString stringWithFormat:@"%@",strLaborType];
    
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    //    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    //
    //    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
}

-(float)fetchPartDetailViaPartCode :(NSString*)strPartCode{
    
    float lookUpPrice=[global fetchPartDetailViaPartCode:strPartCode :arrOfPartsGlobalMasters];
    
    return lookUpPrice;
    
}

-(NSDictionary*)fetchPartDetailDictionaryViaPartCode :(NSString*)strPartCode{
    
    NSDictionary *dictDataPartss;
    
    dictDataPartss=[global fetchPartDetailDictionaryViaPartCode:strPartCode :arrOfPartsGlobalMasters];
    
    return dictDataPartss;
    
}

//============================================================================
//============================================================================
#pragma mark TextField Delegate Method
//============================================================================
//============================================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    return  YES;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)texth
{
    if (range.location == 0 && [texth isEqualToString:@" "]) {
        return NO;
    }
    else
        return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    int tagTextFld=(int)textField.tag;
    if ((tagTextFld>499) && !(tagTextFld==1874919202) && !(tagTextFld==1874919203)) {
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        else
        {
            NSString *text = textField.text;
            NSInteger length = text.length;
            BOOL shouldReplace = YES;
            
            if (![string isEqualToString:@""])
            {
                switch (length)
                {
                    case 2:
                        textField.text = [text stringByAppendingString:@":"];
                        break;
                        
                    default:
                        break;
                }
                if (length > 4)
                    shouldReplace = NO;
            }
            
            return shouldReplace;
        }
        return YES;
        
    }
    NSInteger row = textField.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    AddPartsTableViewCell *tappedCell = (AddPartsTableViewCell *)[_tblViewPartsAll cellForRowAtIndexPath:indexPath];
    indexToEditPart=indexPath.row;
    if(textField==tappedCell.txtActualQty)
    {
        
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        if (newLength>4) {
            
            return NO;
            
        }
        
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        
        if (range.location == 0 && [string isEqualToString:@" "]) {
            return NO;
        }
        BOOL  isReturnTrue;
        
        isReturnTrue=YES;
        
        if (!isNumeric) {
            
            if ([string isEqualToString:@""]) {
                
                isReturnTrue=YES;
                
            }else
                
                isReturnTrue=NO;
            
        }
        
        if (!isReturnTrue) {
            
            return NO;
            
        }
        
        if([string isEqualToString:@""])
        {
            NSString *str1 =tappedCell.txtActualQty.text;
            
            NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
            
            NSManagedObject *objTemp = arrOfSubWorkServiceIssuesRepairPartsAll[row];
            
            [objTemp setValue:newString forKey:@"actualQty"];
            
            NSError *error2;
            [context save:&error2];
            
            // [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll];
            
        }
        else
        {
            //[NSString stringWithFormat:@"%@%@",tappedCell.txtServicePrice.text,string];
            
            NSManagedObject *objTemp = arrOfSubWorkServiceIssuesRepairPartsAll[row];
            
            [objTemp setValue:[NSString stringWithFormat:@"%@%@",tappedCell.txtActualQty.text,string] forKey:@"actualQty"];
            
            NSError *error2;
            [context save:&error2];
            
            // [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll];
            
        }
        // [tappedCell.txtActualQty becomeFirstResponder];
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        
    }
    
    if (textField.tag==1874919202) {
        
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        if (newLength>4) {
            
            return NO;
            
        }
        
    }
    if (textField.tag==1874919203) {
        
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        if (newLength>10) {
            
            return NO;
            
        }
        
    }
    
    
    if (isStandard) {
        
        
        if (textField.tag==1874919202) {
            
            NSScanner *scanner = [NSScanner scannerWithString:string];
            BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
            
            if (range.location == 0 && [string isEqualToString:@" "]) {
                return NO;
            }
            BOOL  isReturnTrue;
            
            isReturnTrue=YES;
            
            if (!isNumeric) {
                
                if ([string isEqualToString:@""]) {
                    
                    isReturnTrue=YES;
                    
                }else
                    
                    isReturnTrue=NO;
                
            }
            
            if (!isReturnTrue) {
                
                return NO;
                
            }
            
            
            if ([string isEqualToString:@""]) {
                
                NSString *strTextQTY;
                
                if ([textField.text length] > 0) {
                    
                    strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                    
                }
                
                //   NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Standard"];
                
                float multiplier=[strMultiplier floatValue];
                
                strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];
                
                if (isChangeStdPartPrice) {
                    
                    strMultiplierGlobal=@"1";
                    multiplier=[strMultiplierGlobal floatValue];
                }
                
                float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }else{
                
                NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Standard"];
                
                float multiplier=[strMultiplier floatValue];
                
                strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];
                
                if (isChangeStdPartPrice) {
                    
                    strMultiplierGlobal=@"1";
                    multiplier=[strMultiplierGlobal floatValue];
                }
                
                float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }
        }else if (textField.tag==1874919203) {
            
            isChangeStdPartPrice=YES;
            
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            {
                if (![string isEqualToString:@"."]) {
                    
                    return NO;
                    
                }
            }
            
            if ([string isEqualToString:@""]) {
                
                NSString *strTextQTY;
                
                if ([textField.text length] > 0) {
                    
                    strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                    
                }
                
                //strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                
                strMultiplierGlobal=@"1";
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtQty.text floatValue];
                
                strTxtUnitPrice=strTextQTY;
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }else{
                
                
                NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                // Logic For Two Digits After Decimals
                NSArray *sep = [strTextQTY componentsSeparatedByString:@"."];
                if([sep count] >= 2)
                {
                    NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                    if (!([sepStr length]>2)) {
                        if ([sepStr length]==2 && [string isEqualToString:@"."]) {
                            return NO;
                        }
                    }
                    else{
                        return NO;
                    }
                }
                //END Logic For Two Digits After Decimals
                
                
                // strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                
                strMultiplierGlobal=@"1";
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtQty.text floatValue];
                
                strTxtUnitPrice=strTextQTY;
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }
            
        }
        
        
    } else {
        
        if (textField.tag==1874919203) {
            
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            {
                if (![string isEqualToString:@"."]) {
                    
                    return NO;
                    
                }
            }
            
            if ([string isEqualToString:@""]) {
                
                NSString *strTextQTY;
                
                if ([textField.text length] > 0) {
                    
                    strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                    
                }
                
                strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",strTextQTY] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtQty.text floatValue];
                
                strTxtUnitPrice=strTextQTY;
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }else{
                
                
                NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                // Logic For Two Digits After Decimals
                NSArray *sep = [strTextQTY componentsSeparatedByString:@"."];
                if([sep count] >= 2)
                {
                    NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                    if (!([sepStr length]>2)) {
                        if ([sepStr length]==2 && [string isEqualToString:@"."]) {
                            return NO;
                        }
                    }
                    else{
                        return NO;
                    }
                }
                //END Logic For Two Digits After Decimals
                
                
                strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",strTextQTY] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtQty.text floatValue];
                
                strTxtUnitPrice=strTextQTY;
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }
            
            
            
        } else if (textField.tag==1874919202) {
            
            NSScanner *scanner = [NSScanner scannerWithString:string];
            BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
            
            if (range.location == 0 && [string isEqualToString:@" "]) {
                return NO;
            }
            BOOL  isReturnTrue;
            
            isReturnTrue=YES;
            
            if (!isNumeric) {
                
                if ([string isEqualToString:@""]) {
                    
                    isReturnTrue=YES;
                    
                }else
                    
                    isReturnTrue=NO;
                
            }
            
            if (!isReturnTrue) {
                
                return NO;
                
            }
            
            if ([string isEqualToString:@""]) {
                
                NSString *strTextQTY;
                
                if ([textField.text length] > 0) {
                    
                    strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                    
                }
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[strTxtUnitPrice floatValue];
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }else{
                
                NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[strTxtUnitPrice floatValue];
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }
            
        }
        
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [_viewAddParts setFrame:CGRectMake(_viewAddParts.frame.origin.x, _viewAddParts.frame.origin.y-200, _viewAddParts.frame.size.width, _viewAddParts.frame.size.height)];
    
    
}

//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
//
//    [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll];
//
//}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [_viewAddParts setFrame:CGRectMake(_viewAddParts.frame.origin.x, _viewAddParts.frame.origin.y+200, _viewAddParts.frame.size.width, _viewAddParts.frame.size.height)];
    
    int tagTextFld=(int)textField.tag;
    
    if ((tagTextFld>499) && !(tagTextFld==1874919202) && !(tagTextFld==1874919203)&& !(tagTextFld==1874919205) && !(tagTextFld==1874919206) && !(tagTextFld==1874919207)) {
        
        NSString *strTextx;
        
        if (textField.text.length>=5) {
            
            strTextx=textField.text;
            
        } else if (textField.text.length==4){
            
            NSString *strTemp=@"0";//  0:00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==3){
            
            NSString *strTemp=@"00";//  :00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==2){
            
            //            NSString *strTemp=@"00:";//  00
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=@":00";//  00
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==1){
            
            //            NSString *strTemp=@"00:0";//  0
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=@"0:00";//  0
            strTextx=[textField.text stringByAppendingString:strTemp];
        } else if (textField.text.length==0){
            
            strTextx=@"00:00";
            
        }
        
        textField.text=strTextx;
        
        if ([textField.text isEqualToString:@"00:00"]) {
            
            textField.text =@"";
            textField.placeholder=@"00:00";
            
        }
        
        NSArray *arrTime=[strTextx componentsSeparatedByString:@":"];
        
        NSString *strHrs,*strMinutes;
        
        if (arrTime.count==1) {
            
            strHrs=arrTime[0];
            strMinutes=@"";
            
        }
        
        if (arrTime.count==2) {
            
            strHrs=arrTime[0];
            strMinutes=arrTime[1];
            
        }
        
        NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
        
        int totalsecondUpdatedEstTime=(int)secondTimeToIncrease;
        totalsecondUpdatedEstTime=totalsecondUpdatedEstTime/60;
        
        //[self replaceBillableDurationInEmpSheet:tagTextFld-500 :strTextx];
        [self replaceBillableDurationInEmpSheet:tagTextFld-500 :[NSString stringWithFormat:@"%d",totalsecondUpdatedEstTime]];
        
    }else{
        
        [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll];
        
    }
}

-(void)replaceBillableDurationInEmpSheet :(int)tag :(NSString*)strDuration{
    
    int row=tag/1000;
    int section=tag%1000;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    NSMutableDictionary *dictTemp=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictTemp2=[[NSMutableDictionary alloc]init];
    
    dictTemp=arrOfGlobalDynamicEmpSheetFinal[row];
    
    NSString *strHelper=[dictTemp valueForKey:@"Helper"];
    BOOL isLaborType;
    if ([strHelper isEqualToString:@"No"]) {
        
        isLaborType=YES;
        
    } else {
        
        isLaborType=NO;
        
    }
    
    arrTemp=[dictTemp valueForKey:@"EmployeeList"];
    
    dictTemp2=arrTemp[section];
    
    NSString *strworkingType=[dictTemp2 valueForKey:@"workingType"];
    BOOL isHolidayyyy;
    if ([strworkingType isEqualToString:@"3"]) {
        isHolidayyyy=YES;
    } else {
        isHolidayyyy=NO;
    }
    
    NSString *strAmount= [global methodToCalculateLaborPriceGlobal:strDuration :isLaborType :arrOfHoursConfig :true :isHolidayyyy :[dictTemp2 valueForKey:@"timeSlot"]];
    
    [dictTemp2 setValue:strDuration forKey:@"billableDurationInMin"];
    [dictTemp2 setValue:strAmount forKey:@"billableAmt"];
    
    [arrTemp replaceObjectAtIndex:section withObject:dictTemp2];
    
    [dictTemp setValue:arrTemp forKey:@"EmployeeList"];
    
    [arrOfGlobalDynamicEmpSheetFinal replaceObjectAtIndex:row withObject:dictTemp];
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    [_viewAddParts setFrame:CGRectMake(_viewAddParts.frame.origin.x, _viewAddParts.frame.origin.y-300, _viewAddParts.frame.size.width, _viewAddParts.frame.size.height)];
    
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (textView==_txtViewTechComment) {
        
        [_objWorkOrderdetails setValue:_txtViewTechComment.text forKey:@"technicianComment"];
        
        NSError *error2;
        [context save:&error2];
        
    }
    
    [_viewAddParts setFrame:CGRectMake(_viewAddParts.frame.origin.x, _viewAddParts.frame.origin.y+300, _viewAddParts.frame.size.width, _viewAddParts.frame.size.height)];
    
}

/*
 -(NSString*)logicForFetchingMultiplier :(NSString*)strPriceToLookUp{
 
 NSString *strMultiplier;
 
 for (int k=0; k<arrOfPriceLookup.count; k++) {
 
 NSDictionary *dictDataPrice=arrOfPriceLookup[k];
 
 NSString *strFrom=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"RangeFrom"]];
 NSString *strTo=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"RangeTo"]];
 
 int c=[strPriceToLookUp intValue];
 
 int a=[strFrom intValue];
 int b=[strTo intValue];
 
 if(c >= a && c <= b){
 
 strMultiplier=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"Multiplier"]];
 
 break;
 
 }
 
 }
 if (strMultiplier.length==0) {
 
 strMultiplier=@"1";
 
 }
 
 return strMultiplier;
 }
 
 */

//============================================================================
//============================================================================
#pragma mark- ----------------Delete Core DB Methods----------------
//============================================================================
//============================================================================

-(void)deleteHelperFromDB :(NSString *)strEmployeeNoToDelete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOTechHelperDcs Detail Data
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && employeeNo = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strEmployeeNoToDelete];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self fetchSubWorkOrderHelperFromDataBaseForMechanical];
    
    [_tblViewPartsAll reloadData];
    
    [self adjustViewHeights];
    
}


-(void)deleteRepairFromDB :(NSString *)strWoId :(NSString *)strSubWoId :(NSString *)strSubWoIssuesId :(NSString *)strRepairId :(int)intSection :(int)intRow{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOIssueRepairDcs Detail Data
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context]];
    //subWorkOrderIssueId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWoId,strSubWoId,strRepairId,strSubWoIssuesId];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    // [arrOfSubWorkServiceIssuesRepair removeObjectAtIndex:intRow];
    
    NSRange range = NSMakeRange(intSection, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [_tblViewServiceIssues reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
    
    [self adjustViewHeightsonCollapsingSections];
    
}

-(void)addPartView{
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewAddLabor
    
    CGRect frameFor_ViewAddLabor=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width,_viewAddParts.frame.size.height);
    [_viewAddParts setFrame:frameFor_ViewAddLabor];
    
    [viewBackGround addSubview:_viewAddParts];
    
    [self setdefaultNonStandard];
    
}

-(void)hideTextFields{
    
    [_txtpartName resignFirstResponder];
    [_txtQty resignFirstResponder];
    [_txtUnitPrice resignFirstResponder];
    [_txtTotalPrice resignFirstResponder];
    [_txtSerial resignFirstResponder];
    [_txtpartName resignFirstResponder];
    [_txtModel resignFirstResponder];
    [_txtManufacturer resignFirstResponder];
    [_txtViewDesc resignFirstResponder];
    
}
- (IBAction)action_AddPartss:(id)sender {
    
    strCategoryMasterId=@"";
    strCategorySysNameSelected=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
    [_btnSelectParts setTitle:@"----Select Part----" forState:UIControlStateNormal];
    [_btnSelectdate setTitle:@"----Select Date----" forState:UIControlStateNormal];
    isStandard=YES;
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    isWarranty=NO;
    isEditParts=NO;
    [_btnWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    _txtViewDesc.text=@"";
    _txtQty.text=@"";
    _txtModel.text=@"";
    _txtSerial.text=@"";
    _txtUnitPrice.text=@"";
    strTxtUnitPrice=@"";
    _txtTotalPrice.text=@"";
    _txtManufacturer.text=@"";
    _txtpartName.text=@"";
    _lblPartSelectOrEnter.text=@"Part";
    [_txtpartName setHidden:YES];
    [_btnSelectParts setHidden:NO];
    [_txtUnitPrice setEnabled:NO];
    [_txtUnitPrice setEnabled:YES];
    [_btnSelectRepairParts setTitle:@"----Select Repair----" forState:UIControlStateNormal];
    [_btnSelectIssues setTitle:@"----Select Issue----" forState:UIControlStateNormal];
    
    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
    [self addPartView];
    
    [self setDefaultCategoryIfSingleCategoryExist];
    
}

- (IBAction)action_CLearParts:(id)sender {
    
    [self hideTextFields];
    
    strCategoryMasterId=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
    [_btnSelectParts setTitle:@"----Select Part----" forState:UIControlStateNormal];
    [_btnSelectdate setTitle:@"----Select Date----" forState:UIControlStateNormal];
    isStandard=YES;
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    isWarranty=NO;
    [_btnWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    _txtViewDesc.text=@"";
    _txtQty.text=@"";
    _txtModel.text=@"";
    _txtSerial.text=@"";
    _txtUnitPrice.text=@"";
    strTxtUnitPrice=@"";
    _txtTotalPrice.text=@"";
    _txtManufacturer.text=@"";
    _txtpartName.text=@"";
    _lblPartSelectOrEnter.text=@"Part";
    [_txtpartName setHidden:YES];
    [_btnSelectParts setHidden:NO];
    [_txtUnitPrice setEnabled:NO];
    [_txtUnitPrice setEnabled:YES];
    [_btnSelectRepairParts setTitle:@"----Select Repair----" forState:UIControlStateNormal];
    [_btnSelectIssues setTitle:@"----Select Issue----" forState:UIControlStateNormal];
    
    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
    
}
- (IBAction)action_SaveParts:(id)sender {
    
    [self hideTextFields];
    if (isStandard) {
        
        if ([_btnSelectIssues.titleLabel.text isEqualToString:@"----Select Issue----"]) {
            
            [global AlertMethod:Alert :@"Please select Issue"];
            
        }else if ([_btnSelectRepairParts.titleLabel.text isEqualToString:@"----Select Repair----"]) {
            
            [global AlertMethod:Alert :@"Please select Repair"];
            
        }else if ([_btnSelectParts.titleLabel.text isEqualToString:@"----Select Part----"]) {
            
            [global AlertMethod:Alert :@"Please select part"];
            
        } else if((_txtQty.text.length==0) || (_txtQty.text.intValue==0)) {
            
            [global AlertMethod:Alert :@"Please enter quantity"];
            
        } else{
            
            if (!isEditParts) {
                
                [self addServiceIssuesRepairPartsFromMobileToDB];
                
            } else {
                
                // [self updatePartsStandard];
                
            }
            
            dictDataPartsSelected=nil;
            
            [viewBackGround removeFromSuperview];
            
            [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
            
            [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll];
            
            [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        }
        
        
    } else {
        
        if ([_btnSelectIssues.titleLabel.text isEqualToString:@"----Select Issue----"]) {
            
            [global AlertMethod:Alert :@"Please select Issue"];
            
        }else if ([_btnSelectRepairParts.titleLabel.text isEqualToString:@"----Select Repair----"]) {
            
            [global AlertMethod:Alert :@"Please select Repair"];
            
        }else if (_txtpartName.text.length==0) {
            
            [global AlertMethod:Alert :@"Please enter part name"];
            
        } else if((_txtQty.text.length==0) || (_txtQty.text.intValue==0)) {
            
            [global AlertMethod:Alert :@"Please enter quantity"];
            
        } else{
            
            if (!isEditParts) {
                
                [self addServiceIssuesRepairPartsNonStandardFromMobileToDB];
                
            } else {
                
                // [self updatePartsNonStandard];
                
            }
            
            [viewBackGround removeFromSuperview];
            
            [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
            
            [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll];
            
            [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        }
        
        
    }
    
}
- (IBAction)action_CancelParts:(id)sender {
    [self hideTextFields];
    [viewBackGround removeFromSuperview];
    
}
- (IBAction)action_SelectDate:(id)sender {
    [self hideTextFields];
    [self addPickerViewDateTo];
    
}
- (IBAction)action_SelectPart:(id)sender {
    [self hideTextFields];
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    [self getPartsMasterNew];
    
    if (arrDataTblView.count==0) {
        
        [global AlertMethod:Info :NoDataAvailable];
        
    }else{
        
        tblData.tag=105;
        [self tableLoad:tblData.tag];
        
    }
    
}
- (IBAction)action_SelectCategory:(id)sender {
    
    [self hideTextFields];
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsMastersMechanical valueForKey:@"InventoryCategoryMaster"];
    
    //    for (int k=0; k<arrOfAreaMaster.count; k++) {
    //
    //        NSDictionary *dictDataa=arrOfAreaMaster[k];
    //        [arrDataTblView addObject:dictDataa];
    //
    //    }
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        
        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
            
            [arrDataTblView addObject:dictDataa];
            
        }
    }
    
    if (arrDataTblView.count==0) {
        
        [global AlertMethod:Info :NoDataAvailable];
        
    }else{
        
        tblData.tag=102;
        [self tableLoad:tblData.tag];
        
    }
    
}
- (IBAction)action_RadioStandard:(id)sender {
    [self hideTextFields];
    isStandard=YES;
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    _txtpartName.text=@"";
    _lblPartSelectOrEnter.text=@"Part";
    _txtUnitPrice.text=@"";
    strTxtUnitPrice=@"";
    [_txtpartName setHidden:YES];
    [_btnSelectParts setHidden:NO];
    [_txtUnitPrice setEnabled:NO];
    [_txtUnitPrice setEnabled:YES];
    _txtTotalPrice.text=@"";
    _txtQty.text=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
    [_btnSelectParts setTitle:@"----Select Part----" forState:UIControlStateNormal];
    
    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
    NSString *strInventory = [NSString stringWithFormat:@"Unit Price($)%@",@"*"];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strInventory];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(13,1)];
    _labelUnitPrice.attributedText = string;
    _txtUnitPrice.placeholder=@"Unit Price";

    [self setDefaultCategoryIfSingleCategoryExist];
    
}
- (IBAction)action_RadioNonStandard:(id)sender {
    strCategorySysNameSelected=@"";
    [self hideTextFields];
    isStandard=NO;
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    _txtpartName.text=@"";
    _txtUnitPrice.text=@"";
    strTxtUnitPrice=@"";
    _txtQty.text=@"";
    _lblPartSelectOrEnter.text=@"Part Name";
    [_txtpartName setHidden:NO];
    [_btnSelectParts setHidden:YES];
    [_txtUnitPrice setEnabled:YES];
    // [_txtQty setEnabled:YES];
    _txtTotalPrice.text=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
    
    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
    NSString *strInventory = [NSString stringWithFormat:@"Vendor Price($)%@",@"*"];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strInventory];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(15,1)];
    _labelUnitPrice.attributedText = string;
    _txtUnitPrice.placeholder=@"Vendor Price";

    [self setDefaultCategoryIfSingleCategoryExist];
}

-(void)setdefaultNonStandard{
    
    strCategorySysNameSelected=@"";
    [self hideTextFields];
    isStandard=NO;
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    _txtpartName.text=@"";
    _txtUnitPrice.text=@"";
    strTxtUnitPrice=@"";
    _txtQty.text=@"";
    _lblPartSelectOrEnter.text=@"Part Name";
    [_txtpartName setHidden:NO];
    [_btnSelectParts setHidden:YES];
    [_txtUnitPrice setEnabled:YES];
    // [_txtQty setEnabled:YES];
    _txtTotalPrice.text=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
    
    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
    NSString *strInventory = [NSString stringWithFormat:@"Vendor Price($)%@",@"*"];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strInventory];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(15,1)];
    _labelUnitPrice.attributedText = string;
    _txtUnitPrice.placeholder=@"Vendor Price";

}

- (IBAction)action_Warranty:(id)sender {
    [self hideTextFields];
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnWarranty imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        isWarranty=YES;
        [_btnWarranty setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
        // [self methodToCalculateLaborPrice:_txt_LaborHrs.text];
        
    }else{
        
        isWarranty=NO;
        [_btnWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
        // [self methodToCalculateLaborPrice:_txt_LaborHrs.text];
    }
    
}

- (IBAction)action_ChargeToCustomer:(id)sender{
    
    [self hideTextFields];
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnChargeToCustomer imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        isChargeToCustomer=YES;
        [_btnChargeToCustomer setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        isChargeToCustomer=NO;
        [_btnChargeToCustomer setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
}

- (IBAction)actionSelectRepair:(id)sender {
    
    if ([_btnSelectIssues.titleLabel.text isEqualToString:@"----Select Issue----"]) {
        
        [global AlertMethod:Alert :@"Please select Issue"];
        
    }else {
        
        [self hideTextFields];
        [self methodTableViewAllocation];
        
        [viewBackGroundOnView removeFromSuperview];
        [tblData removeFromSuperview];
        
        arrDataTblView=nil;
        arrDataTblView=[[NSMutableArray alloc]init];
        
        [self fetchSubWorkOrderRepairsViaIssuesID];
        
        //strIssueIdGlobalSelected
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            [arrDataTblView addObject:arrOfSubWorkServiceIssuesRepair[k]];
            
        }
        
        if (arrDataTblView.count==0) {
            
            [global AlertMethod:Info :NoDataAvailableee];
            
        }else{
            
            tblData.tag=107;
            [self tableLoad:tblData.tag];
            
        }
    }
}
- (IBAction)actionSelectIssues:(id)sender {
    
    [self hideTextFields];
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=nil;
    arrDataTblView=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
        
        [arrDataTblView addObject:arrOfSubWorkServiceIssues[k]];
        
    }
    
    if (arrDataTblView.count==0) {
        
        [global AlertMethod:Info :NoDataAvailableee];
        
    }else{
        
        tblData.tag=106;
        [self tableLoad:tblData.tag];
        
    }
    
}

-(void)addServiceIssuesRepairPartsFromMobileToDB{
    
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    objSubWorkOrderIssues.issueRepairPartId=[global getReferenceNumber];
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",strRepairIdGlobalSelected];
    objSubWorkOrderIssues.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",strIssueIdGlobalSelected];
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",@"true"];
    if ([_btnSelectdate.titleLabel.text isEqualToString:@"----Select Date----"]) {
        
        objSubWorkOrderIssues.installationDate=[NSString stringWithFormat:@"%@",@""];
        
    } else {
        
        objSubWorkOrderIssues.installationDate=[NSString stringWithFormat:@"%@",_btnSelectdate.titleLabel.text];
        
    }
    objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"true"];
    
    if (isChargeToCustomer) {
        
        // if is charge to customer mtllb charge krna mtlbb is added after approval false kyunki agr is added after approval hota hai to charge nhi krte hain customer ko
        objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"false"];
        
    } else {
        
        objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"true"];
        
    }
    
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"false"];
    
    NSString *strWarranty;
    if (isWarranty) {
        
        strWarranty=@"true";
        
    }else{
        
        strWarranty=@"false";
    }
    
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",strWarranty];
    objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",_txtManufacturer.text];
    objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",_txtModel.text];
    //objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",@"1"];
    objSubWorkOrderIssues.partCode=[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"SysName"]];
    objSubWorkOrderIssues.partDesc=[NSString stringWithFormat:@"%@",_txtViewDesc.text];
    objSubWorkOrderIssues.partName=[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"Name"]];
    objSubWorkOrderIssues.partType=[NSString stringWithFormat:@"%@",@"Standard"];
    objSubWorkOrderIssues.qty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",_txtSerial.text];
    objSubWorkOrderIssues.unitPrice=[NSString stringWithFormat:@"%@",strTxtUnitPrice];
    
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    if (isChangeStdPartPrice) {
        
        objSubWorkOrderIssues.isChangeStdPartPrice=[NSString stringWithFormat:@"%@",@"true"];
        objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",@"1"];
        
    }else{
        
        objSubWorkOrderIssues.isChangeStdPartPrice=[NSString stringWithFormat:@"%@",@"false"];
        objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",strMultiplierGlobal];

    }
    
    // Saving Vendor Details
    
    if([_btnVendorName.titleLabel.text isEqualToString:@"----Select Vendor----"]){
        
        objSubWorkOrderIssues.vendorName=@"";
        
    }else{
        
        objSubWorkOrderIssues.vendorName=strVendorSysNameSelected;
        
    }
    objSubWorkOrderIssues.vendorPartNo=_txtFldVendorPartNo.text;
    objSubWorkOrderIssues.vendorQuoteNo=_txtFldVendoreQuoteNo.text;
    objSubWorkOrderIssues.partCategorySysName=strCategorySysNameSelected;

    // End // Saving Vendor Details
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll];
    
    
}
-(void)addServiceIssuesRepairPartsNonStandardFromMobileToDB{
    
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    objSubWorkOrderIssues.issueRepairPartId=[global getReferenceNumber];
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",strRepairIdGlobalSelected];
    objSubWorkOrderIssues.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",strIssueIdGlobalSelected];
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",@"true"];
    if ([_btnSelectdate.titleLabel.text isEqualToString:@"----Select Date----"]) {
        
        objSubWorkOrderIssues.installationDate=[NSString stringWithFormat:@"%@",@""];
        
    } else {
        
        objSubWorkOrderIssues.installationDate=[NSString stringWithFormat:@"%@",_btnSelectdate.titleLabel.text];
        
    }
    objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"true"];
    
    if (isChargeToCustomer) {
        
        // if is charge to customer mtllb charge krna mtlbb is added after approval false kyunki agr is added after approval hota hai to charge nhi krte hain customer ko
        objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"false"];
        
    } else {
        
        objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"true"];
        
    }
    
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"false"];
    
    NSString *strWarranty;
    if (isWarranty) {
        
        strWarranty=@"true";
        
    }else{
        
        strWarranty=@"false";
    }
    
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",strWarranty];
    objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",_txtManufacturer.text];
    objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",_txtModel.text];
    objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",strMultiplierGlobal];
    objSubWorkOrderIssues.partCode=@"";
    objSubWorkOrderIssues.partDesc=[NSString stringWithFormat:@"%@",_txtViewDesc.text];
    objSubWorkOrderIssues.partName=[NSString stringWithFormat:@"%@",_txtpartName.text];
    objSubWorkOrderIssues.partType=@"Non-Standard";
    objSubWorkOrderIssues.qty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",_txtSerial.text];
    objSubWorkOrderIssues.unitPrice=[NSString stringWithFormat:@"%@",_txtUnitPrice.text];
    
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    // Saving Vendor Details
    
    if([_btnVendorName.titleLabel.text isEqualToString:@"----Select Vendor----"]){
        
        objSubWorkOrderIssues.vendorName=@"";
        
    }else{
        
        objSubWorkOrderIssues.vendorName=strVendorSysNameSelected;
        
    }
    objSubWorkOrderIssues.vendorPartNo=_txtFldVendorPartNo.text;
    objSubWorkOrderIssues.vendorQuoteNo=_txtFldVendoreQuoteNo.text;
    objSubWorkOrderIssues.partCategorySysName=strCategorySysNameSelected;

    // End // Saving Vendor Details
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalAll];
    
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //  [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strGlobalDateToShow.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strGlobalDateToShow];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGroundOnView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundOnView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGroundOnView];
    
    //============================================================================
    //============================================================================
    
    //   UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    //  singleTap1.numberOfTapsRequired = 1;
    //  [viewBackGround setUserInteractionEnabled:YES];
    //  [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGroundOnView addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    // btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGroundOnView removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    if (yesSameDay) {
        [_btnSelectdate setTitle:strDate forState:UIControlStateNormal];
        strGlobalDateToShow=strDate;
        [viewForDate removeFromSuperview];
        [viewBackGroundOnView removeFromSuperview];
    }
    else
    {
        
        //        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
        //
        //        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending)
        //        {
        //
        //            [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
        //
        //        }else
        //        {
        
        [_btnSelectdate setTitle:strDate forState:UIControlStateNormal];
        strGlobalDateToShow=strDate;
        [viewForDate removeFromSuperview];
        [viewBackGroundOnView removeFromSuperview];
        
        //        }
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

-(void)getPartsMasterNew{
    
    NSArray *arrOfAreaMaster=[dictDetailsMastersMechanical valueForKey:@"InventoryItems"];
    arrDataTblView=nil;
    arrDataTblView=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        
        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        
        NSString *strCategoryMasterIdToCheck=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryId"]];
        
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
            
            if (strCategoryMasterId.length==0) {
                
                [arrDataTblView addObject:dictDataa];
                
                
            } else {
                
                if ([strCategoryMasterId isEqualToString:strCategoryMasterIdToCheck]) {
                    
                    [arrDataTblView addObject:dictDataa];
                    
                } else {
                    
                    
                    
                }
                
            }
            
            
        }
        
    }
    
}

- (IBAction)action_CompleteSubWorkOrder:(id)sender {
    
    if (isCompletedStatusMechanical) {
        
        [self stopLoaderAndGoToInvoice];
        
    }else{
        
        [self saveTechComments];

        if ([self isNotInstalled]) {
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Confirmation...!!!"
                                       message:@"Do you want to continue without installing the repair?"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      
                                      
                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                     [self callingMethodAfterAlertToSaveValue];
                                     
                                 }];
            [alert addAction:no];
            UIAlertAction* cancell = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                      }];
            //[alert addAction:cancell];
            [self presentViewController:alert animated:YES completion:nil];
            
        } else {
            
            [self callingMethodAfterAlertToSaveValue];
            
        }
        
    }
    
}

-(void)callingMethodAfterAlertToSaveValue{
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnIncludeDetailOnInvoice imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        [_objSubWorkOrderdetails setValue:@"false" forKey:@"isIncludeDetailOnInvoice"];
    }else{
        
        [_objSubWorkOrderdetails setValue:@"true" forKey:@"isIncludeDetailOnInvoice"];
    }
    
    //[_objSubWorkOrderdetails setValue:@"true" forKey:@"isIncludeDetailOnInvoice"];
    
    [_objWorkOrderdetails setValue:_txtViewTechComment.text forKey:@"technicianComment"];
    
    NSError *error2;
    [context save:&error2];
    
    if ([_btnStartnStopJOB.titleLabel.text isEqualToString:@"Pause Job"] || [_btnStartnStopJOB.currentTitle isEqualToString:@"Pause Job"]) {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
        
        [self performSelector:@selector(updatingTimeAfterSometime) withObject:nil afterDelay:0.1];
        
    } else {
        
        [self stopLoaderAndGoToInvoice];
        
    }
    
}

-(void)updatingTimeAfterSometime{
    
    
    [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutOnComplete];
    
    
}

- (IBAction)actionBackToRecommandations:(id)sender {
    
    [self saveTechComments];

    [jobMinTimer invalidate];
    jobMinTimer=nil;
    
    //isClientApproved
    [_objSubWorkOrderdetails setValue:@"Inspection" forKey:@"subWOStatus"];
    [_objSubWorkOrderdetails setValue:@"false" forKey:@"isClientApproved"];
    
    NSError *error2;
    [context save:&error2];
    
    [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
    
    [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalBackToRecommandation];
    
}

-(void)goToSubWorkOrderDetailView{
    
    [self saveImageToCoreData];
    
    [jobMinTimer invalidate];
    jobMinTimer=nil;
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    [defss setInteger:-1 forKey:@"sectionToOpen"];
    [defss synchronize];
    
    if ([_strWoType isEqualToString:@"TM"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=_objWorkOrderdetails;
        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderdetails;
        objByProductVC.strWoType=_strWoType;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalSubWorkOrderDetailsViewControlleriPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControlleriPad"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=_objWorkOrderdetails;
        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderdetails;
        objByProductVC.strWoType=_strWoType;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
}

//============================================================================
//============================================================================
#pragma mark- ----------------Before Image Methods----------------
//============================================================================
//============================================================================

- (IBAction)action_CloseBeforImgView:(id)sender {
    
    [_view_BeforeImage removeFromSuperview];
    
    // [_scrollViewGeneralInfo setFrame:CGRectMake(0, _scrollViewGeneralInfo.frame.origin.y, _scrollViewGeneralInfo.frame.size.width, _scrollViewGeneralInfo.frame.size.height+_viewFinalSavenContinue.frame.size.height+_view_BeforeImage.frame.size.height)];
    
}

- (IBAction)action_BeforeImgView:(id)sender {
    
    //Adding BeforeImageInfoView
    
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewFinalSavenContinue.frame.origin.y-_view_BeforeImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view_BeforeImage.frame.size.height);
    [_view_BeforeImage setFrame:frameFor_view_BeforeImageInfo];
    [self.view addSubview:_view_BeforeImage];
    
    //   [_scrollViewGeneralInfo setFrame:CGRectMake(0, _scrollViewGeneralInfo.frame.origin.y, _scrollViewGeneralInfo.frame.size.width, _scrollViewGeneralInfo.frame.size.height-160)];
    
    // [_scrollViewGeneralInfo setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_OtherInfo.frame.size.height+_view_OtherInfo.frame.origin.y)];
    
    
}
- (IBAction)action_BeforeImages:(id)sender {
    
    /*
     UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
     delegate:self
     cancelButtonTitle:@"Cancel"
     destructiveButtonTitle:nil
     otherButtonTitles:@"Capture New", @"Gallery", nil];
     
     [actionSheet showInView:self.view];
     
     */
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              if (isCompletedStatusMechanical) {
                                  
                                  
                              }else{
                                  
                                  if (arrOfBeforeImageAll.count<10)
                                  {
                                      NSLog(@"The CApture Image.");
                                      
                                      NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                      
                                      BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                                      
                                      if (isfirstTimeAudio) {
                                          
                                          [defs setBool:NO forKey:@"firstCamera"];
                                          [defs synchronize];
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }else{
                                          BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                          
                                          if (isCameraPermissionAvailable) {
                                              
                                              UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                              imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                              imagePickController.delegate=(id)self;
                                              imagePickController.allowsEditing=TRUE;
                                              [self presentViewController:imagePickController animated:YES completion:nil];
                                              
                                              
                                          }else{
                                              
                                              UIAlertController *alert= [UIAlertController
                                                                         alertControllerWithTitle:@"Alert"
                                                                         message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                              
                                              UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction * action)
                                                                    {
                                                                        
                                                                        
                                                                        
                                                                    }];
                                              [alert addAction:yes];
                                              UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                           NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                           [[UIApplication sharedApplication] openURL:url];
                                                                       } else {
                                                                           
                                                                           //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                           //                                     [alert show];
                                                                           
                                                                       }
                                                                       
                                                                   }];
                                              [alert addAction:no];
                                              [self presentViewController:alert animated:YES completion:nil];
                                          }
                                      }
                                  }
                                  else
                                  {
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                      [alert show];
                                  }
                              }
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if (isCompletedStatusMechanical) {
                                 
                                 
                             }else{
                                 
                                 if (arrOfBeforeImageAll.count<10)
                                 {
                                     
                                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                     
                                     BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                                     
                                     if (isfirstTimeAudio) {
                                         
                                         [defs setBool:NO forKey:@"firstGallery"];
                                         [defs synchronize];
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }else{
                                         BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                         
                                         if (isCameraPermissionAvailable) {
                                             
                                             UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                             imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                             imagePickController.delegate=(id)self;
                                             imagePickController.allowsEditing=TRUE;
                                             [self presentViewController:imagePickController animated:YES completion:nil];
                                             
                                             
                                         }else{
                                             
                                             UIAlertController *alert= [UIAlertController
                                                                        alertControllerWithTitle:@"Alert"
                                                                        message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       
                                                                       
                                                                   }];
                                             [alert addAction:yes];
                                             UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                        handler:^(UIAlertAction * action)
                                                                  {
                                                                      
                                                                      if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                          NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                          [[UIApplication sharedApplication] openURL:url];
                                                                      } else {
                                                                          
                                                                          //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                          //                                     [alert show];
                                                                          
                                                                      }
                                                                      
                                                                  }];
                                             [alert addAction:no];
                                             [self presentViewController:alert animated:YES completion:nil];
                                         }
                                     }
                                 }else{
                                     
                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                     [alert show];
                                 }
                             }
                             
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)methodViewWillAppear{
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodStatus];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
        EditImageViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewControlleriPad"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isFromBackServiceDynamci"];
        
        if (isFromBack) {
            
            arrOfBeforeImageAll=nil;
            arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            [defsBack setBool:NO forKey:@"isFromBackServiceDynamci"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseMechanical];
            
        }
        
        
        //    _lblOne.clipsToBounds = YES;
        //    _lblOne.layer.masksToBounds = YES;
        //    _lblOne.layer.cornerRadius = 40.0;
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreview"];
        if (yesFromDeleteImage) {
            
            yesEditedSomething=YES;
            
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreview"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfBeforeImageAll.count; k++) {
                
                NSDictionary *dictdat=arrOfBeforeImageAll[k];
                
                NSString *strImageName;
                
                if ([dictdat isKindOfClass:[NSString class]]) {
                    
                    strImageName=arrOfBeforeImageAll[k];
                    
                } else {
                    
                    strImageName=[dictdat valueForKey:@"woImagePath"];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImages"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                        
                    }
                }
                //            if (yesFoundName) {
                //
                //                [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                //
                //            }
            }
            if (!(arrTempBeforeImage.count==0)) {
                // arrOfBeforeImageAll=nil;
                // arrOfBeforeImageAll=[[NSMutableArray alloc]init];
                [arrOfBeforeImageAll removeObjectsInArray:arrTempBeforeImage];
            }
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImages"];
            [defsnew synchronize];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            yesEditedSomething=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            yesEditedSomething=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            
        }
        
        [self downloadingImagesThumbNailCheck];
        
    }
    
    [_beforeImageCollectionView reloadData];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    [self methodStatus];
    
}

-(void)fetchImageDetailFromDataBaseMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetail.count; j++) {
            
            matchesImageDetail=arrAllObjImageDetail[j];
            NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"Before"]) {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,nil];
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrOfBeforeImageAll addObject:dict_ToSendLeadInfo];
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
            }
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        
    }else if (arrOfImagesDetail.count==0){
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionView addObjectsFromArray:arrOfImagess];
        
        [self downloadImages:arrOfImagess];
        
    }
}

-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation1=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation1,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImage:str:k];
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload1: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
        }
    }
    [_beforeImageCollectionView reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    if (indexx==arrOfImagenameCollewctionView.count-1) {
        
        [_beforeImageCollectionView reloadData];
        
    }
    [_beforeImageCollectionView reloadData];
}

//============================================================================
//============================================================================
#pragma mark -----------------------Load Image-------------------------------
//============================================================================
//============================================================================

- (UIImage*)loadImage :(NSString*)name :(int)indexxx {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    return image;
}

-(void)goingToPreview :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        //[defs setBool:YES forKey:@"isFromOtherPresentedView"];
        [defs synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ImagePreviewGeneralInfoAppointmentViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.statusOfWorkOrder=strWorkOrderStatuss;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaption;
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescription;
        //objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}

#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    isImageTaken=YES;
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
    
    [arrOfBeforeImageAll addObject:strImageNamess];
    [arrOfImagenameCollewctionView addObject:strImageNamess];
    [_beforeImageCollectionView reloadData];
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    [self resizeImage:chosenImage :strImageNamess];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    //imageCaption
    
    NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
    
    BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
    
    if (yesImageCaption) {
        
        [self alertViewCustom];
        
    } else {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        [self saveImageToCoreData];
        
    }
    
}


-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    
    //NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

//Change for Image Caption
-(void)alertViewCustom{
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 105, [UIScreen mainScreen].bounds.size.width-20, 430)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 50)];
    
    lblCaption.text=@"Enter image caption below...";
    lblCaption.font=[UIFont systemFontOfSize:22];
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 100)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:22];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+105, viewAlertt.bounds.size.width-20, 50)];
    
    lbl.text=@"Enter image description below...";
    lbl.font=[UIFont systemFontOfSize:22];
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+55, viewAlertt.bounds.size.width-20, 200)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:22];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+435, viewAlertt.frame.size.width/2-20, 50)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    btnSave.titleLabel.font=[UIFont systemFontOfSize:22];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,50)];
    [btnDone setTitle:@"No Caption" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont systemFontOfSize:22];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        
        [self saveImageToCoreData];
        
        //        CGPoint bottomOffset = CGPointMake(0, _scrollViewGeneralInfo.contentSize.height - _scrollViewGeneralInfo.bounds.size.height);
        //        [_scrollViewGeneralInfo setContentOffset:bottomOffset animated:YES];
        
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}


-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    [self saveImageToCoreData];
    
    //    CGPoint bottomOffset = CGPointMake(0, _scrollViewGeneralInfo.contentSize.height - _scrollViewGeneralInfo.bounds.size.height);
    //    [_scrollViewGeneralInfo setContentOffset:bottomOffset animated:YES];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrOfImagenameCollewctionView.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"CellCollectionService";
    
    GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    //    cell.selected=YES;
    //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
    NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    if (image==nil) {
        
        cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
        
    } else {
        
        cell.imageBefore.image = image;
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    if (image==nil) {
        
        
        
    }else{
        
        NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        [self goingToPreview : strIndex];
        
    }
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
    [self deleteBeforeImagesBeforeSaving];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    for (int k=0; k<arrOfBeforeImageAll.count; k++)
    {
        
        if ([arrOfBeforeImageAll[k] isKindOfClass:[NSString class]]) {
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfBeforeImageAll objectAtIndex:k]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            
            NSError *error1;
            [context save:&error1];
            
        }else{
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            NSDictionary *dictData=[arrOfBeforeImageAll objectAtIndex:k];
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    //........................................................................
}

-(void)deleteBeforeImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"Before"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutOnComplete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strID=[defs valueForKey:@"subWOActualHourId"];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strID];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
    }
    else
    {
        [jobMinTimer invalidate];
        
        matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[0];
        
        NSString *strCureentTimeToset=[global strCurrentDateFormattedForMechanical];
        
        [matchesSubWorkOrderActualHrs setValue:strCureentTimeToset forKey:@"timeOut"];
        //[matchesSubWorkOrderActualHrs setValue:@"Running" forKey:@"status"];
        [matchesSubWorkOrderActualHrs setValue:strCureentTimeToset forKey:@"mobileTimeOut"];
        
        [matchesSubWorkOrderActualHrs setValue:ReasonRunning forKey:@"reason"];
        [matchesSubWorkOrderActualHrs setValue:@"" forKey:@"actHrsDescription"];
        
        BOOL isNetReachable=[global isNetReachable];
        
        if (isNetReachable) {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
            
            strNotificationTypeName=@"MechanicalActualHrsSyncStartFR3";
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoaderAndGoToInvoice) name:strNotificationTypeName object:nil];
            
            SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
            
            [objSync syncMechanicalActualHours:strWorkOrderId :strSubWorkOrderIdGlobal :strID :strNotificationTypeName];
            
            //[self stopLoaderAndGoToInvoice];
            
        }else{
            
            [self stopLoaderAndGoToInvoice];
            
        }
        
        NSError *error2;
        [context save:&error2];
        
        [_btnStartnStopJOB setTitle:@"Start Job" forState:UIControlStateNormal];
        
        // Create dynamic view for employee time sheet slot wise.....
        arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
        arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
        
        arrOfGlobalDynamicEmpSheetFinal=[global createEmpSheetDataInBackground:strWorkOrderId :strSubWorkOrderIdGlobal :arrOfHoursConfig :strEmpID :strEmployeeNoLoggedIn :strEmpName :@"StartRepairBackground" :strCureentTimeToset];
        
        //arrOfHeaderTitleForSlots=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"" forKey:@"subWOActualHourId"];
        [defs synchronize];
        
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}

-(void)stopLoaderAndGoToInvoice
{
    
    [self saveImageToCoreData];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:strNotificationTypeName
                                                  object:nil];
    
    [DejalBezelActivityView removeView];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    MechanicalInvoiceViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalInvoiceViewController"];
    objByProductVC.strWorlOrderId=strWorkOrderId;
    objByProductVC.strSubWorkOderId=_strSubWorkOrderId;
    objByProductVC.strWoType=_strWoType;
    objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderdetails;
    objByProductVC.objWorkOrderdetails=_objWorkOrderdetails;
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}


-(void)goToGeneralInfoView{
    
    if ([_strWoType isEqualToString:@"TM"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalGeneralInfoViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalGeneralInfoViewController"];
        //        objByProductVC.strSubWorkOrderId=_strSubWorkOrderId;
        //        objByProductVC.objWorkOrderdetails=objServiceWorkOrderGlobal;
        //        objByProductVC.objSubWorkOrderdetails=matchesMechanicalSubWorkOrder;
        //        objByProductVC.strWoType=strWorkOrderType;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalGeneralInfoViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalGeneralInfoViewController"];
        //        objByProductVC.strSubWorkOrderId=_strSubWorkOrderId;
        //        objByProductVC.objWorkOrderdetails=objServiceWorkOrderGlobal;
        //        objByProductVC.objSubWorkOrderdetails=matchesMechanicalSubWorkOrder;
        //        objByProductVC.strWoType=strWorkOrderType;
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    
}

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalBackToRecommandation{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        [self stopLoader];
        
        //        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        //        [defs setValue:@"" forKey:@"subWOActualHourId"];
        //        [defs synchronize];
        
    }
    else
    {
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strTimeOutJob=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
            
            NSString *strSubWorkOrderStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"status"]];
            
            if ([strSubWorkOrderStatus isEqualToString:@"OnRoute"]) {
                
                
                
            } else {
                
                if (strTimeOutJob.length==0){
                    
                    matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
                    
                    NSString *strCureentTimeToset=[global strCurrentDateFormattedForMechanical];
                    
                    [matchesSubWorkOrderActualHrs setValue:strCureentTimeToset forKey:@"timeOut"];
                    [matchesSubWorkOrderActualHrs setValue:strCureentTimeToset forKey:@"mobileTimeOut"];
                    
                    [matchesSubWorkOrderActualHrs setValue:ReasonInspection forKey:@"reason"];
                    
                    BOOL isNetReachable=[global isNetReachable];
                    
                    if (isNetReachable) {
                        
                        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
                        
                        strNotificationTypeName=@"MechanicalActualHrsSyncStartFR4";
                        
                        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoaderBackToRecommandation) name:strNotificationTypeName object:nil];
                        
                        SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
                        
                        [objSync syncMechanicalActualHours:strWorkOrderId :strSubWorkOrderIdGlobal :[matchesSubWorkOrderActualHrs valueForKey:@"subWOActualHourId"] :strNotificationTypeName];
                        
                    }else{
                        
                        [self stopLoaderBackToRecommandation];
                        
                    }
                    
                    [global fetchWorkOrderFromDataBaseForMechanicalToUpdateClockStatus:strWorkOrderId : strSubWorkOrderIdGlobal :@"Stop"];
                    
                    NSError *error2;
                    [context save:&error2];
                    
                    // Create dynamic view for employee time sheet slot wise.....
                    arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
                    arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
                    
                    arrOfGlobalDynamicEmpSheetFinal=[global createEmpSheetDataInBackground:strWorkOrderId :strSubWorkOrderIdGlobal :arrOfHoursConfig :strEmpID :strEmployeeNoLoggedIn :strEmpName :@"Background" :strCureentTimeToset];
                    
                    //arrOfHeaderTitleForSlots=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
                    
                    break;
                    
                }
            }
        }
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}


-(void)stopLoaderBackToRecommandation
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:strNotificationTypeName
                                                  object:nil];
    
    [DejalBezelActivityView removeView];
    
    [self goToSubWorkOrderDetailView];
    
}

//============================================================================
#pragma mark- ------------Scan Part------------------
//============================================================================

-(void)methodOpenScannerView{
    
    [self saveImageToCoreData];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"" forKey:@"ScannedResult"];
    [defs setBool:YES forKey:@"IsScannerView"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ScannerViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ScannerViewController"];
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}

- (IBAction)action_ScanBarcode:(id)sender {
    
    [self methodOpenScannerView];
    
}


-(void)setpartValuesAfterScanning :(NSString *)strScannedResult{
    
    BOOL isPartPresent;
    
    isPartPresent=NO;
    isChangeStdPartPrice=NO;
    [self getPartsMasterNew];
    NSDictionary *dictData;
    
    for (int k=0; k<arrDataTblView.count; k++) {
        
        dictData=[arrDataTblView objectAtIndex:k];
        
        NSString *strItemNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ItemNumber"]];
        
        if ([strScannedResult caseInsensitiveCompare:strItemNumber] == NSOrderedSame) {
            isPartPresent=YES;
            break;
        }
    }
    
    if (isPartPresent) {
        
        [_btnSelectParts setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        [_txtQty setEnabled:YES];
        
        _txtViewDesc.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Description"]];
        
        float unitPriceFloat=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BestPrice"]] floatValue];
        
        _txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
        strTxtUnitPrice=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
        
        dictDataPartsSelected=dictData;
        
        NSString *strType;
        
        if (isStandard) {
            
            strType=@"Standard";
            
        } else {
            
            strType=@"Non-Standard";
            
        }
        
        if (strCategorySysNameSelected.length==0) {
            
            strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
            
        }
        
        NSString *strPartCate=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
        
        if (strPartCate.length==0) {
            
            strCategorySysNameSelected=@"";
            
        }else{
            
            strCategorySysNameSelected=strPartCate;
            
            NSArray *arrOfAreaMaster=[dictDetailsMastersMechanical valueForKey:@"InventoryCategoryMaster"];
            
            for (int k=0; k<arrOfAreaMaster.count; k++) {
                
                NSDictionary *dictDataa=arrOfAreaMaster[k];
                NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
                
                NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
                NSString *strCategorySysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
                
                if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
                    
                    if ([strCategorySysName isEqualToString:strCategorySysNameSelected]) {
                        
                        [_btnSelectCategory setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                        strCategoryMasterId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategoryMasterId"]];
                        
                    }
                    
                }
            }
            
        }
        
        NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",unitPriceFloat] :strCategorySysNameSelected :strType];
        
        float multiplier=[strMultiplier floatValue];
        
        strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];
        
        NSString *strTextQTY=[NSString stringWithFormat:@"%@",_txtQty.text];
        
        _txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",multiplier*[strTxtUnitPrice floatValue]];
        
        if (strTextQTY.length>0) {
            
            float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];
            
            _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
            
        }else{
            
            _txtQty.text=@"1";
            
            strTextQTY=[NSString stringWithFormat:@"%@",_txtQty.text];
            
            float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];
            
            _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
            
            
        }
        
    } else {
        
        [global AlertMethod:Alert :@"Part not available"];
        
    }
    
}

- (IBAction)action_IncludeDetailOnInvoice:(id)sender {
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnIncludeDetailOnInvoice imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        [_btnIncludeDetailOnInvoice setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
    }else{
        
        [_btnIncludeDetailOnInvoice setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    }
    
}

-(void)goToEquipMentHistory{
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        EquipmentHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EquipmentHistoryViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    }
    
}


-(void)methodToCheckIfValuesAreNull{
    
    NSString *strValuesToCheck=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderNo"]];
    NSString *strValuesToCheck1=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWOStatus"]];
    
    if (([strValuesToCheck caseInsensitiveCompare:@"(null)"] == NSOrderedSame) || ([strValuesToCheck1 caseInsensitiveCompare:@"(null)"] == NSOrderedSame)) {
        
        [global AlertMethod:Alert :@"Something went wrong. Please process again."];
        
        NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
        
        NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
        
        if ([strAppointmentFlow isEqualToString:@"New"])
        {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
            AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }
        else
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
            AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
            [self.navigationController pushViewController:objByProductVC animated:NO];
        }
        
    }
}


- (IBAction)action_CreateQuote:(id)sender {
    
    BOOL isNet=[global isNetReachable];
    
    if (isNet) {
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        QuoteHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"QuoteHistoryViewController"];
        objSignViewController.strWorkOrderID=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"workOrderNo"]];;
        objSignViewController.objWorkOrderDetail = _objWorkOrderdetails;
        objSignViewController.objSubWorkOrderDetail = _objSubWorkOrderdetails;
        objSignViewController.strFromWhere=@"sdf";
        [self presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    }
}
- (IBAction)action_PoOrder:(id)sender {
    
    BOOL isNet=[global isNetReachable];
    
    if (isNet) {
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        PoHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"PoHistoryViewController"];
        objSignViewController.strWorkOrderNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"workOrderNo"]];
        objSignViewController.strWorkOrderId=strWorkOrderId;
        objSignViewController.strSubWorkOrderId=_strSubWorkOrderId;
        objSignViewController.strWoType=_strWoType;
        objSignViewController.objWorkOrderDetail=_objWorkOrderdetails;
        objSignViewController.objSubWorkOrderDetail=_objSubWorkOrderdetails;
        objSignViewController.strFromWhere=@"";
        [self presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    }
}

// akshay
-(NSString*)getHrAndMins:(NSString*)strMins
{
    int seconds = (int)[strMins integerValue]*60;
    int minutes = (seconds / 60) % 60;
    int hours = seconds / 3600;
    return [NSString stringWithFormat:@"%02d:%02d",hours,minutes];
    
}
-(void)callApiToUpdateTimeEstimation:(NSString*)strEstTime
{
    NSString *strWorkOrderNo = [NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"workOrderNo"]];//
    
    NSString *strSubWorkOrderNo = [NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderNo"]];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@?companyKey=%@&userName=%@&userId=%@&workOrderNo=%@&subWorkOrderNo=%@&EstimateTime=%@",strServiceUrlMainServiceAutomation,UrlMechanicalUpdateEstTime,strCompanyKey,strUserName,strEmpID,strWorkOrderNo,strSubWorkOrderNo,strEstTime];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
    
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
             [DejalBezelActivityView removeView];
             NSData* jsonData = [NSData dataWithData:data];
             NSString *responseString1 = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
             
             NSCharacterSet *quoteCharset = [NSCharacterSet characterSetWithCharactersInString:@"\""];
             responseString1 = [responseString1 stringByTrimmingCharactersInSet:quoteCharset];
             
             
             
             if ((responseString1==nil) || responseString1.length==0) {
                 
                 [global AlertMethod:Alert :NoDataAvailableee];
                 
                 
             } else {
                 
                 [_objSubWorkOrderdetails setValue:responseString1 forKey:@"totalEstimationTime"];
                 //TotalEstimationTimeInt
                 
                 NSArray *arrTime=[responseString1 componentsSeparatedByString:@":"];
                 
                 NSString *strHrs,*strMinutes;
                 
                 if (arrTime.count==1) {
                     
                     strHrs=arrTime[0];
                     strMinutes=@"";
                     
                 }
                 
                 if (arrTime.count==2) {
                     
                     strHrs=arrTime[0];
                     strMinutes=arrTime[1];
                     
                 }
                 
                 NSInteger secondTimeToIncrease=[strHrs intValue]*3600+[strMinutes intValue]*60;
                 
                 int totalsecondUpdatedEstTime=secondTimeToIncrease;
                 totalsecondUpdatedEstTime=totalsecondUpdatedEstTime/60;
                 
                 NSString *strUpdatedEstTimeInt=[NSString stringWithFormat:@"%d",totalsecondUpdatedEstTime];
                 
                 [_objSubWorkOrderdetails setValue:strUpdatedEstTimeInt forKey:@"totalEstimationTimeInt"];
                 
                 NSError *error2;
                 [context save:&error2];
                 
                 [_btnEstTime setTitle:[NSString stringWithFormat:@"Est. Time: %@",responseString1] forState:UIControlStateNormal];
                 
                 [viewBackGroundUpdateEstTime removeFromSuperview];
                 
                 
                 NSString *strException;
                 @try {
                     //strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                 
                 if (strException.length==0)
                 {
                     // NSArray *tempArray=(NSArray*)ResponseDict;
                     //                     if(!(tempArray.count>0))
                     //                     {
                     //                         [global AlertMethod:@"Message" :NoDataAvailableee];
                     //                     }
                     //                     else
                     //                     {
                     //
                     //                     }
                     
                 } else {
                     
                     //ResponseDict=nil;
                     [global AlertMethod:Alert :NoDataAvailableee];
                 }
             }
         }];
    }
    @catch (NSException *exception) {
        
        [global AlertMethod:Alert :Sorry];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }
}

#pragma mark - Update Est Time View's actions

- (IBAction)actionOnUpdateEstTime:(id)sender {
    
    strUpdateEstTime=@"";
    // akshay
    tblData.tag = 101;
    [self setTableFrame];
    [tblData reloadData];
}
- (IBAction)actionOnPlusUpdateEstTime:(id)sender {
    
    //    // add two times
    //
    //    NSArray *arrTime1=[_btnEstTime.titleLabel.text componentsSeparatedByString:@":"];
    //
    //    NSString *strHrs1,*strMinutes1;
    //
    //    if (arrTime1.count==1) {
    //
    //        strHrs1=arrTime1[0];
    //        strMinutes1=@"";
    //
    //    }
    //
    //    if (arrTime1.count==3) {
    //
    //        strHrs1=arrTime1[1];
    //        strMinutes1=arrTime1[2];
    //    }
    //
    //    int secondTimeToIncrease1=[strHrs1 intValue]*3600+[strMinutes1 intValue]*60;
    //
    //    NSArray *arry = [_buttonUpdateEstTime.titleLabel.text componentsSeparatedByString:@"Mins"];
    //
    //    strUpdateEstTime = [[NSString stringWithFormat:@"%@",[arry firstObject]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //
    //     int seconds = (int)[strUpdateEstTime integerValue]*60;
    //     int totalSeconds = secondTimeToIncrease1+seconds;
    //
    //    int minutes = (totalSeconds / 60) % 60;
    //    int hours = totalSeconds / 3600;
    //    NSLog(@"%@",[NSString stringWithFormat:@"%02d:%02d",hours,minutes]) ;
    //    NSString *strTotalTime = [NSString stringWithFormat:@"%02d:%02d",hours,minutes];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:Alert :ErrorInternetMsg];
    }
    else
    {
        
        if (strUpdateEstTime.length>0) {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Est. Time..."];
            DELAY(0.1);
            
            [self callApiToUpdateTimeEstimation:[self getHrAndMins:strUpdateEstTime]];
            
        } else {
            
            [global AlertMethod:Alert :@"Please select time to update est.time"];
            
        }
        
    }
    
}
- (IBAction)actionOnMinusUpdateEstTime:(id)sender {
    
    
    NSArray *arrTime1=[_btnEstTime.titleLabel.text componentsSeparatedByString:@":"];
    
    NSString *strHrs1,*strMinutes1;
    
    if (arrTime1.count==1) {
        
        strHrs1=arrTime1[0];
        strMinutes1=@"";
        
    }
    
    if (arrTime1.count==3) {
        
        strHrs1=arrTime1[1];
        strMinutes1=arrTime1[2];
    }
    
    int secondTimeToIncrease1=[strHrs1 intValue]*3600+[strMinutes1 intValue]*60;
    
    int updateEstTimeSec = (int)[strUpdateEstTime integerValue]*60;
    
    if(secondTimeToIncrease1<updateEstTimeSec)
    {
        [global AlertMethod:Alert :@"Update estimation time can not be greater than estimation time."];
        return;
    }
    
    
    
    NSString *time = [NSString stringWithFormat:@"-%@",[self getHrAndMins:strUpdateEstTime]];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:Alert :ErrorInternetMsg];
    }
    else
    {
        
        if (strUpdateEstTime.length>0) {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Est. Time..."];
            DELAY(0.1);
            [self callApiToUpdateTimeEstimation:time];
            
        } else {
            
            [global AlertMethod:Alert :@"Please select time to update est.time"];
            
        }
        
    }
}

-(void)actionOnCancelUpdateEstTime
{
    [viewBackGroundUpdateEstTime removeFromSuperview];
}

- (IBAction)action_UpdateEstTime:(id)sender
{
    strUpdateEstTime=@"";
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        [_buttonUpdateEstTime setTitle:@"" forState:UIControlStateNormal];
        viewBackGroundUpdateEstTime=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        viewBackGroundUpdateEstTime.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
        [self.view addSubview: viewBackGroundUpdateEstTime];
        
        // akshay
        
        _viewUpdateEstTime.center = CGPointMake(viewBackGroundUpdateEstTime.frame.size.width  / 2,
                                                viewBackGroundUpdateEstTime.frame.size.height / 2);
        
        
        [viewBackGroundUpdateEstTime addSubview:_viewUpdateEstTime];
        
        UIButton *btnCancelUpdateEstTime=[[UIButton alloc]initWithFrame:CGRectMake(_viewUpdateEstTime.frame.origin.x+_viewUpdateEstTime.frame.size.width/4, _viewUpdateEstTime.frame.origin.y+_viewUpdateEstTime.frame.size.height+20, _viewUpdateEstTime.frame.size.width/2-2, 60)];
        
        [btnCancelUpdateEstTime setTitle:@"Cancel" forState:UIControlStateNormal];
        btnCancelUpdateEstTime.titleLabel.font=[UIFont systemFontOfSize:25];
        [btnCancelUpdateEstTime setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnCancelUpdateEstTime.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
        
        [btnCancelUpdateEstTime addTarget:self action:@selector(actionOnCancelUpdateEstTime) forControlEvents:UIControlEventTouchDown];
        
        [viewBackGroundUpdateEstTime addSubview:btnCancelUpdateEstTime];
        
    }else{
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    }
    
}

- (IBAction)action_EmpTimeSheet:(id)sender {
    
    [self goToEmpTimeSheet];
    
}

//Goto Emp Sheet

-(void)goToEmpTimeSheet{
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    EmpTimeSheetViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EmpTimeSheetViewController"];
    objSignViewController.strWorkOrderId=strWorkOrderId;
    objSignViewController.strSubWorkOrderIdGlobal=strSubWorkOrderIdGlobal;
    objSignViewController.arrOfHoursConfig=arrOfHoursConfig;
    
    if (isCompletedStatusMechanical) {
        
        objSignViewController.strWorkOrderStatus=@"Complete";
        
    } else {
        
        objSignViewController.strWorkOrderStatus=@"InComplete";
        
    }
    
    objSignViewController.strAfterHrsDuration=strAfterHrsDuration;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

// Vendor Details
- (IBAction)action_VendorName:(id)sender {
    
    [self hideTextFields];
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    NSUserDefaults *defsVendorName=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMechanicalMasters1=[defsVendorName valueForKey:@"MasterAllMechanical"];
    NSArray *arrayItemVendorMapping = [dictMechanicalMasters1 valueForKey:@"ItemVendorMappingExtSerDc"];
    NSArray *arrayVendor=[dictMechanicalMasters1 valueForKey:@"VendorBasicExtSerDc"];
    
    NSMutableArray *arrayTempVendor = [NSMutableArray new];
    
    if (isStandard) {
        
        for(NSDictionary *dict in arrayItemVendorMapping)
        {
            if([[dict valueForKey:@"ItemMasterId"] integerValue]==itemMasterID)
            {
                for(NSDictionary *dictTempVendor in arrayVendor)
                {
                    if ([[dict valueForKey:@"VendorMasterId"] integerValue]==[[dictTempVendor valueForKey:@"VendorMasterId"] integerValue])
                    {
                        [arrayTempVendor addObject:dictTempVendor];
                    }
                }
            }
        }
        
    } else {
        
        arrayTempVendor = [arrayVendor mutableCopy];
        
    }
    
    
    
    if(arrayTempVendor.count>0)
    {
        arrDataTblView = [arrayTempVendor mutableCopy];
        tblData.tag=108;
        [self tableLoad:tblData.tag];
    }
    else
    {
        [global AlertMethod:Info :@"No Data Available.\n1. Please Either select part. OR \n2. Please check either Data is set on Web, if set on Web check your internet connection and Sync Masters from Menu options."];
    }
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------Method Set Default Category-----------------
//============================================================================
//============================================================================

-(void)setDefaultCategoryIfSingleCategoryExist{
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsMastersMechanical valueForKey:@"InventoryCategoryMaster"];
    
    
    //    for (int k=0; k<arrOfAreaMaster.count; k++) {
    //
    //        NSDictionary *dictDataa=arrOfAreaMaster[k];
    //        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
    //
    //        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
    //
    //        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
    //
    //            [arrDataTblView addObject:dictDataa];
    //
    //        }
    //    }
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        if (([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"])) {
            
            [arrDataTblView addObject:dictDataa];
            
        }
    }
    
    if (arrDataTblView.count==1) {
        
        NSDictionary *dictData=[arrDataTblView objectAtIndex:0];
        [_btnSelectCategory setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        strCategoryMasterId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategoryMasterId"]];
        strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        
    }
    
}

-(BOOL)isNotInstalled{
    
    BOOL isNotInstalled;
    isNotInstalled=NO;
    
    
    for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[k];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                
            }
            
        }
        
        for (int k1=0; k1<arrTemp.count; k1++) {
            
            NSManagedObject *objTemp=arrTemp[k1];
            
            NSString *strIsCompleted=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isCompleted"]];
            
            if (([strIsCompleted isEqualToString:@"true"] || [strIsCompleted isEqualToString:@"1"])) {
                
                
            }else{
                
                isNotInstalled = true;
                
                break;
                
            }
            
        }
        
    }
    
    return isNotInstalled;
}


// Changes for collapse expand

-(void)fetchSubWorkOrderIssuesFromDataBaseForMechanicalToExpandCollapse{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssues = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssues setEntity:entitySubWorkOrderIssues];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssues setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssues = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssues = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssues];
    
    [requestSubWorkOrderIssues setSortDescriptors:sortDescriptorsSubWorkOrderIssues];
    
    self.fetchedResultsControllerSubWorkOrderIssues = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssues managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssues setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssues performFetch:&error];
    NSArray *arrTemp = [self.fetchedResultsControllerSubWorkOrderIssues fetchedObjects];
    if ([arrTemp count] == 0)
    {
        arrOfSectionsToOpen=nil;
        arrOfSectionsToOpen=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSectionsToOpen=nil;
        arrOfSectionsToOpen=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrTemp.count; k++) {
            
            [arrOfSectionsToOpen addObject:[NSString stringWithFormat:@"%d",k]];
            
        }
        
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}

-(void)saveTechComments{
    
    [_objWorkOrderdetails setValue:_txtViewTechComment.text forKey:@"technicianComment"];
    
    NSError *error2;
    [context save:&error2];
    
}
@end

