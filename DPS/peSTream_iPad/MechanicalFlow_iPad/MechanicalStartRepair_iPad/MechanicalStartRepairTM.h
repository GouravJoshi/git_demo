//
//  MechanicalStartRepairTM.h
//  DPS  peSTream
//  peSTream
//  Created by Saavan Patidar on 15/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface MechanicalStartRepairTM : UIViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>
{
    NSTimer *jobMinTimer;
    int totalsecond,totalSecondsNotToExceed;
    AppDelegate *appDelegate;
    NSEntityDescription *entitySubWorkOrderIssues,*entityMechanicalSubWorkOrderHelper,*entityMechanicalSubWorkOrderNotes,*entityMechanicalSubWorkOrderActualHrs,*entitySubWorkOrderIssuesRepair,*entitySubWorkOrderIssuesRepairParts,*entitySubWorkOrderIssuesRepairLabour;
    
    
    NSManagedObjectContext *context;
    
    
    NSFetchRequest *requestSubWorkOrderIssues,*requestSubWorkOrderHelper,*requestSubWorkOrderNotes,*requestSubWorkOrderActualHrs,*requestSubWorkOrderIssuesRepair,*requestSubWorkOrderIssuesRepairParts,*requestSubWorkOrderIssuesRepairLabour;
    
    
    NSSortDescriptor *sortDescriptorSubWorkOrderIssues,*sortDescriptorSubWorkOrderHelper,*sortDescriptorSubWorkOrderNotes,*sortDescriptorSubWorkOrderActualHrs,*sortDescriptorSubWorkOrderIssuesRepair,*sortDescriptorSubWorkOrderIssuesRepairParts,*sortDescriptorSubWorkOrderIssuesRepairLabour;
    
    
    NSArray *sortDescriptorsSubWorkOrderIssues,*sortDescriptorsSubWorkOrderHelper,*sortDescriptorsSubWorkOrderNotes,*sortDescriptorsSubWorkOrderActualHrs,*sortDescriptorsSubWorkOrderIssuesRepair,*sortDescriptorsSubWorkOrderIssuesRepairParts,*sortDescriptorsSubWorkOrderIssuesRepairLabour;
    
    
    NSManagedObject *matchesSubWorkOrderIssues,*matchesSubWorkOrderHelper,*matchesSubWorkOrderNotes,*matchesSubWorkOrderActualHrs,*matchesSubWorkOrderIssuesRepair,*matchesSubWorkOrderIssuesRepairParts,*matchesSubWorkOrderIssuesRepairLabour;
    
    
    NSArray *arrAllObjSubWorkOrderIssues,*arrAllObjSubWorkOrderHelper,*arrAllObjSubWorkOrderNotes,*arrAllObjSubWorkOrderActualHrs,*arrAllObjSubWorkOrderIssuesRepair,*arrAllObjSubWorkOrderIssuesRepairParts,*arrAllObjSubWorkOrderIssuesRepairLabour;
    
    NSEntityDescription *entityImageDetail;
    NSFetchRequest *requestImageDetail;
    NSSortDescriptor *sortDescriptorImageDetail;
    NSArray *sortDescriptorsImageDetail;
    NSManagedObject *matchesImageDetail;
    NSArray *arrAllObjImageDetail;

}
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblCustNamenAccNo;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVieww;
@property (strong, nonatomic) IBOutlet UIView *viewSubWorkOrderDetails;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewProfile;
@property (strong, nonatomic) IBOutlet UILabel *lblWorkOrderNi;
@property (strong, nonatomic) IBOutlet UILabel *lblSubWorkOrderNo;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblPhoneNo;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
- (IBAction)action_PrimaryEmail:(id)sender;
- (IBAction)action_AddServiceIssues:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) NSString *strSubWorkOrderId;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderdetails;
@property (strong, nonatomic) NSManagedObject *objWorkOrderdetails;
@property (strong, nonatomic) IBOutlet UILabel *lblAccountNo;
@property (strong, nonatomic) IBOutlet UIView *view_ServiceIssues;
@property (strong, nonatomic) IBOutlet UITableView *tblViewServiceIssues;
@property (strong, nonatomic) IBOutlet UIView *viewAddServiceIssues;
@property (strong, nonatomic) IBOutlet UITextView *txtViewIssuesDesc;
@property (strong, nonatomic) IBOutlet UIButton *btnPrioityAddServiceIssues;
- (IBAction)action_PriorityAddServiceIssues:(id)sender;
- (IBAction)action_SaveServiceIssues:(id)sender;
- (IBAction)action_CancelServiceIssuesView:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *view_AddRepair;
@property (strong, nonatomic) IBOutlet UIButton *btnRadioStandard;
@property (strong, nonatomic) IBOutlet UIButton *btnRadioNonStandard;
- (IBAction)action_RadioStandard:(id)sender;
- (IBAction)action_RadioNonStandard:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectRepair;
- (IBAction)action_SelectRepair:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtRepairAmt;
@property (strong, nonatomic) IBOutlet UITextField *txtRepairCostAdjustment;
@property (strong, nonatomic) IBOutlet UITextView *txtViewRepairDescriptions;
- (IBAction)action_SaveRepair:(id)sender;
- (IBAction)action_CancelRepair:(id)sender;
- (IBAction)action_AddRepair:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewHelper;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectHelper;
- (IBAction)action_Selecthelper:(id)sender;
- (IBAction)action_AddHelper:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblviewHelper;
@property (strong, nonatomic) IBOutlet UIView *viewNotes;
- (IBAction)action_AddNotes:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewNotes;
@property (strong, nonatomic) IBOutlet UIView *viewAddNotes;
@property (strong, nonatomic) IBOutlet UITextView *txtViewAddNotesDesc;
- (IBAction)action_SaveNotes:(id)sender;
- (IBAction)action_CancelNotes:(id)sender;
- (IBAction)action_StartJob:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEstTime;
@property (strong, nonatomic) IBOutlet UIButton *btnStartnStopJOB;
@property (strong, nonatomic) IBOutlet UIButton *btnTimeClock;
@property (strong, nonatomic) IBOutlet UIView *viewClientApproval;
- (IBAction)action_ClientApproval:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnClientApproval;
- (IBAction)action_StartRepair:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnStartRepair;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectReason;
@property (strong, nonatomic) IBOutlet UITextView *txtViewReason;
- (IBAction)action_SelectReason:(id)sender;
- (IBAction)action_SaveStopReason:(id)sender;
- (IBAction)action_CancelStopJob:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewStopJobReason;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssues;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderHelper;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderNotes;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderActualHrs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepair;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairParts;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairLabour;
@property (strong, nonatomic) IBOutlet UILabel *lblRepairStdorNonStd;
@property (strong, nonatomic) IBOutlet UIImageView *btnDropDownRepair;
@property (strong, nonatomic) IBOutlet UITextField *txtRepairName;
@property (strong, nonatomic) NSString *strWoType;
@property (strong, nonatomic) IBOutlet UITableView *tblViewPartsAll;


- (IBAction)action_BeforeImages:(id)sender;
@property (strong, nonatomic) IBOutlet UICollectionView *beforeImageCollectionView;
@property (strong, nonatomic) IBOutlet UIView *view_BeforeImage;
@property (strong, nonatomic) IBOutlet UIView *viewFinalSavenContinue;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;


@property (strong, nonatomic) IBOutlet UITextView *txtViewAdditionalInfo;
@property (strong, nonatomic) IBOutlet UIButton *btnAdditionalInfo;
@property (strong, nonatomic) IBOutlet UIView *view_AdditionalInfo;

- (IBAction)action_ScanBarcode:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnIncludeDetailOnInvoice;
- (IBAction)action_IncludeDetailOnInvoice:(id)sender;
- (IBAction)action_ServiceHistory:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnCreateQuote;
- (IBAction)action_CreateQuote:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnPoOrder;
- (IBAction)action_PoOrder:(id)sender;
// akshay
@property (strong, nonatomic) IBOutlet UIView *viewUpdateEstTime;
@property (weak, nonatomic) IBOutlet UIButton *buttonUpdateEstTime;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlusUpdateEstTime;
@property (weak, nonatomic) IBOutlet UIButton *buttonMinusUpdateEstTime;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdateEstTime;
- (IBAction)action_UpdateEstTime:(id)sender;
- (IBAction)action_EmpTimeSheet:(id)sender;

//Vendor Details
@property (strong, nonatomic) IBOutlet UIButton *btnVendorName;
- (IBAction)action_VendorName:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtFldVendorPartNo;
@property (strong, nonatomic) IBOutlet UITextField *txtFldVendoreQuoteNo;

@property (strong, nonatomic) IBOutlet UILabel *labelUnitPrice;

@property (strong, nonatomic) IBOutlet UITextView *txtViewTechComment;


@end
