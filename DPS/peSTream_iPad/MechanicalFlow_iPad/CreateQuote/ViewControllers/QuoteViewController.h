//
//  QuoteViewController.h
//  DPS
//
//  Created by Akshay Hastekar on 26/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"
@interface QuoteViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textFieldAccount;
@property (weak, nonatomic) IBOutlet UITextField *textFieldWorkOrder;
@property (weak, nonatomic) IBOutlet UIButton *buttonDepartment;
@property (weak, nonatomic) IBOutlet UITextField *textFieldQuoteNumber;
@property (weak, nonatomic) IBOutlet UIButton *buttonService;
@property (weak, nonatomic) IBOutlet UIButton *buttonJobType;
@property (weak, nonatomic) IBOutlet UIButton *buttonServiceAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonLocationContact;
@property (weak, nonatomic) IBOutlet UIButton *buttonMechanic;
@property (weak, nonatomic) IBOutlet UITextView *textFieldDescriptionQuote;
@property (weak, nonatomic) IBOutlet UIButton *buttonAddDetails;
@property (weak, nonatomic) IBOutlet UIButton *buttonCategory;
@property (weak, nonatomic) IBOutlet UIButton *buttonEquipment;
@property (weak, nonatomic) IBOutlet UITextField *textFieldManufacturer;
@property (weak, nonatomic) IBOutlet UIButton *buttonArea;
@property (weak, nonatomic) IBOutlet UITextField *textFieldSerialNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldModalNumber;
@property (weak, nonatomic) IBOutlet UITextField* textFieldInstallationDate;
@property (weak, nonatomic) IBOutlet UITextField *textFieldWarrantyExpirationDate;

@property (weak, nonatomic) IBOutlet UITextView *textViewQuoteDescription;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescriptionAddDetails;
@property (weak, nonatomic) IBOutlet UITextField *textFieldMiles;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSignature;
@property (weak, nonatomic) IBOutlet UIView *viewAddDetails;
@property (weak, nonatomic) IBOutlet UILabel *labelSubTotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *labelDiagnosticCharge;
@property (weak, nonatomic) IBOutlet UILabel *labelTripCharge;
@property (weak, nonatomic) IBOutlet UILabel *labelMileageCharge;
@property (weak, nonatomic) IBOutlet UILabel *labelSubTotal;
@property (weak, nonatomic) IBOutlet UILabel *labelTax;
@property (weak, nonatomic) IBOutlet UILabel *labelTotal;
@property (weak, nonatomic) IBOutlet UITableView *tableViewPart;
@property (weak, nonatomic) IBOutlet UITableView *tableViewLabour;
@property (weak, nonatomic) IBOutlet UIView *viewMainContainer;


@property (weak, nonatomic) IBOutlet UIButton *buttonDiagnosticCharge;
@property (weak, nonatomic) IBOutlet UIButton *buttonTripCharge;
@property (weak, nonatomic) IBOutlet UIButton *buttonMileageCharge;

@property (weak, nonatomic) IBOutlet UITextField *textFieldMiscCharge;
@property (weak, nonatomic) IBOutlet UILabel *labelMiscCharge;


// Constraints outlet
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hghtTableViewPart;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hghtTableViewLabour;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hghtMainContainerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hghtViewAddDetails;

// managed object
@property (strong, nonatomic) NSManagedObject *objWorkOrderDetail;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderDetail;


@property (weak, nonatomic) IBOutlet UILabel *labelSearchHeader;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableViewSearch;
@property (strong, nonatomic) IBOutlet UIView *viewTextFieldSearch;

///// part /////
@property (strong, nonatomic) IBOutlet UIView *viewAddPart;
@property (weak, nonatomic) IBOutlet UIButton *buttonStandard;
@property (weak, nonatomic) IBOutlet UIButton *buttonNonStandard;
@property (weak, nonatomic) IBOutlet UIButton *buttonWarranty;
@property (weak, nonatomic) IBOutlet UIButton *buttonCategory_AddPart;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectPart_AddPart;
@property (weak, nonatomic) IBOutlet UITextField *textFieldQuantity_AddPart;
@property (weak, nonatomic) IBOutlet UITextField *textFieldUnitPrice_AddPart;
@property (weak, nonatomic) IBOutlet UITextField *textFieldTotalPrice_AddPart;
@property (weak, nonatomic) IBOutlet UITextField *textFieldSerialNumber_AddPart;
@property (weak, nonatomic) IBOutlet UITextField *textFieldModelNumber_AddPart;
@property (weak, nonatomic) IBOutlet UITextField *textFieldManufacturer_AddPart;
@property (weak, nonatomic) IBOutlet UIButton *buttonInstallationDate_AddPart;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescription_AddPart;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPartName_AddPart;
@property (weak, nonatomic) IBOutlet UIButton *buttonVendor_AddPart;
@property (weak, nonatomic) IBOutlet UIButton *buttonVendorLocation_AddPart;
@property (weak, nonatomic) IBOutlet UIButton *buttonWarrantyExpireDate_AddPart;
///////////////////////////////
/////// labour /////
@property (strong, nonatomic) IBOutlet UIView *viewAddLabor;
@property (weak, nonatomic) IBOutlet UILabel *labelLaborType;
@property (weak, nonatomic) IBOutlet UIButton *buttonIsWarranty_AddLabor;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAmount_AddLabor;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescription_AddLabor;
//@property (weak, nonatomic) IBOutlet UIButton *buttonSelectTime_AddLabor;
@property (weak, nonatomic) IBOutlet UITextField *textFieldHrs_AddLabor;

@property (weak, nonatomic) IBOutlet UIButton *buttonLabor;
@property (weak, nonatomic) IBOutlet UIButton *buttonHelper;

@property (weak, nonatomic) IBOutlet UITextField *textFieldCustomerPONumber;
- (IBAction)action_QuoteHistory:(id)sender;

@property (strong, nonatomic) NSString *strFromWhere;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewAmount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewSubAmount;

@property (weak, nonatomic) IBOutlet UILabel *labelSubTotalAmountHeader;

@property (weak, nonatomic) IBOutlet UIButton *buttonTechnicalSignature;

@property (weak, nonatomic) IBOutlet UILabel *lblUnitPriceTxtFldTitle;

@property (weak, nonatomic) IBOutlet UITextField *txtFldQty;

@end
