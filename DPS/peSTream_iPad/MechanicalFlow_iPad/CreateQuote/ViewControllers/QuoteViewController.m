//
//  QuoteViewController.m
//  DPS
//
//  Created by Akshay Hastekar on 26/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS_MILES @"0123456789."

#import "QuoteViewController.h"

@interface PartListCreateQuoteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel  *labelPart;
@property (weak, nonatomic) IBOutlet UILabel  *labelQty;
@property (weak, nonatomic) IBOutlet UILabel  *labelTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel  *labelDescription;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeletePart;
@property (weak, nonatomic) IBOutlet UIButton *buttonEditPart;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalPriceHeader;
@property (weak, nonatomic) IBOutlet UIButton *buttonViewMoreDesc;


@end

@implementation PartListCreateQuoteCell

@end
///////////////////////////
@interface LabourListCreateQuoteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel  *labelLabourType;
@property (weak, nonatomic) IBOutlet UILabel  *labourHr;
@property (weak, nonatomic) IBOutlet UILabel  *labelAmount;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeleteLabor;
@property (weak, nonatomic) IBOutlet UIButton *buttonEditLabor;
@property (weak, nonatomic) IBOutlet UILabel  *labelAmountHeader;

@end

@implementation LabourListCreateQuoteCell

@end
/////////////////////////////

@interface QuoteViewController ()

@property (strong, nonatomic) IBOutlet UILabel *lblPartSelectOrEnter;

@end


@implementation QuoteViewController
{
    BOOL isAddDetailsChecked;
    NSDictionary *dictWorkOrderDetails;
    NSDictionary *dictSubWorkOrderDetails;
    NSDictionary *dictSalesLeadMaster;
    NSMutableArray *arrayDepartment;
    NSMutableArray *arrayService;
    NSArray *arrayJobType;
    UITableView *tblData;
    UIButton *buttonBackground;
    NSDictionary *dictLoginDetails;
    NSString *strServiceUrlMainServiceAutomation;
    Global *global;
    NSInteger addressID;
    NSArray *arrayServiceAddress;
    NSArray *arrayLocationContact;
    NSArray *arrayEmployeeList;
    NSDictionary *dictDetailsFortblView;
    NSMutableArray *arrayCategory;
    NSMutableArray *arrayEquipment;
    NSArray *arrayArea;
    NSString *strSearchType;
    NSMutableArray *arraySerialNumber,*arrayModalNumber,*arrayManufacturer;
    NSMutableArray *arrOfSerialNosFiltered;
    NSMutableArray *arrOfModelNosFiltered;
    NSMutableArray *arrOfManufacturerFiltered;
    NSMutableArray *arrayVendor;
    NSMutableArray *arrayVendorLocation;
    NSMutableArray *arrOfPriceLookup;
    NSString *strItemSysName;
    NSString *strCategoryId;
    BOOL isSerialNo,isModelNo,isManufacturer,isInstallationDate,isWarrantyDate,isStandard,isWarranty,isEditParts,isAddPart,isWarranty_AddLabor,isLaborType,isStandardSubWorkOrder,isHoliday,isEditLabor,isAddLabor,isEditPart,isSignatureImage;
    NSString *strSerialNumber,*strModalNumber,*strManufacturer;
    UIDatePicker *pickerDate;
    UIView *viewForDate,*viewForDateTime;
    UIView *viewBackGround,*viewBackGroundTime,*viewBackGround1;
    NSString *strDateEquip,*strInstalledOn,*strWarrantyDate,*strMultiplierGlobal,*strDepartMentSysName,*strCompanyName,*strWorkOrderAccNo,*strWorkOrderAddressSubType,*strWorkOrderAddressId,*strCompanyKey;
    NSMutableArray *arrDataTblView;
    NSArray *arrayItemVendorMapping;
    NSInteger itemMasterID;
    NSDictionary *dictDataPartsSelected;
    NSString *strCategorySysNameSelected;
    NSDictionary *dictVendor,*dictVendorLocation;
    NSMutableArray *arrOfHoursConfig;
    NSString *strAfterHrsDuration,*strDate;
    NSMutableArray *arrayPart;
    NSMutableArray *arrayLabour;
    NSInteger cellTag;
    BOOL isPart;// will be used in alert delegate method to indicate whether it is part cell delete button or labor cell delete button
    NSDictionary *dictCategory;
    NSDictionary *dictMechanicalMasters;
    NSMutableArray *arrayDiagnostic,*arrayTripCharge,*arrayMileageCharge;
    NSArray *arrMechanicalMasters;
    NSString *strDiagnosticChargeID;
    NSString *strTripChargeID;
    NSString *strMileageChargeID;
    NSString *strWoType;
    float chargeAmount,subTotalParts,subTotalLabor;
    NSString *strTax;
    NSString *strWorkOrderId;
    NSString *strJobType;
    NSString *strCRMContactID;
    NSString *strServiceID;
    
    NSString *strDiagnosticChargeName;
    NSString *strDiagnosticChargeAmt;
    
    NSString *strTripChargeName;
    NSString *strTripChargeAmt;
    
    NSString *strMileageChargeAmt;
    
    NSString *strMileageName;
    
    NSString *strEquipmentCode;
    NSString *strEquipmentName;
    NSString *strEquipmentNumber;
    NSString *strEquipmentCategorySysName;
    NSString *strInstallationdate;
    NSString *strWarrantyExpirationDate;
    NSString *strArea;
    NSString *strTechSignaturePath;
    NSString *strServiceAddressID;
    NSString *strServiceAddressName;
    NSString *strPrimaryServiceSysName;
    NSString *strCompanyID;
    NSString *strDepartmentID;
    NSString *strIsChangeStdPartPrice;
    NSDictionary *dictCategory_AddPart;
    BOOL isShowPriceOnQuote;
    NSString *strIsEmployeePresetSignature;
    NSString *strPresetSignature;
    BOOL isFirst;// ye variable location contact method me check lagane ke liye h ki ate se hi call hui h ya service address select karne ke baad
    
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrayDepartment = [NSMutableArray new];
    arrayService    = [NSMutableArray new];
    arrayCategory   = [NSMutableArray new];
    arraySerialNumber = [NSMutableArray new];
    arrayModalNumber = [NSMutableArray new];
    arrayManufacturer = [NSMutableArray new];
    arrOfSerialNosFiltered = [NSMutableArray new];
    arrOfModelNosFiltered = [NSMutableArray new];
    arrOfManufacturerFiltered = [NSMutableArray new];
    arrayEquipment = [NSMutableArray new];
    arrayVendor    = [NSMutableArray new];
    arrayVendorLocation = [NSMutableArray new];
    arrayPart = [NSMutableArray new];
    arrayLabour = [NSMutableArray new];
    arrayDiagnostic = [NSMutableArray new];
    arrayTripCharge = [NSMutableArray new];
    arrayMileageCharge = [NSMutableArray new];
    global = [[Global alloc]init];
    isAddDetailsChecked = NO;
    isWarranty = NO;
    isWarranty_AddLabor = NO;
    isLaborType = YES;
    isHoliday = NO;
    isEditLabor = NO;
    isAddLabor = NO;
    strSearchType = @"";
    strItemSysName = @"";
    strCategoryId = @"";
    strSerialNumber = @"";
    strModalNumber = @"";
    strManufacturer = @"";
    strCRMContactID = @"";
    strServiceID = @"";
    strDiagnosticChargeName = @"";
    strDiagnosticChargeAmt = @"";
    strTripChargeName = @"";
    strTripChargeAmt = @"";
    strMileageName = @"";
    strMileageChargeAmt = @"";
    strJobType = @"";
    strEquipmentCode = @"";
    strEquipmentName = @"";
    strEquipmentNumber = @"";
    strEquipmentCategorySysName = @"";
    strArea = @"";
    strTechSignaturePath = @"";
    strInstallationdate = @"";
    strWarrantyExpirationDate = @"";
    strServiceAddressID = @"";
    strServiceAddressName = @"";
    strIsChangeStdPartPrice = @"false";
    strPresetSignature = @"";
    strIsEmployeePresetSignature = @"false";
    _buttonTechnicalSignature.userInteractionEnabled = YES;
    isFirst = YES;
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    dictLoginDetails = [defsLead objectForKey:@"LoginDetails"];
    strCompanyID = [NSString stringWithFormat:@"%@",[[[[dictLoginDetails valueForKey:@"Company"] valueForKey:@"CompanyConfig"] valueForKey:@"BillingServiceModule"] valueForKey:@"CompanyId"]];
    
    isShowPriceOnQuote = [[[[dictLoginDetails valueForKey:@"Company"] valueForKey:@"CompanyConfig"] valueForKey:@"isShowPriceOnQuote"] boolValue];
    
    subTotalParts = 0.0;
    subTotalLabor = 0.0;
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:@"no" forKey:@"isFromQuote"];
    [userDef setValue:@"" forKey:@"imagePathQuote"];
    [userDef synchronize];
    
    // configure pop up tableView
    tblData=[[UITableView alloc]init];
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.tag = 10;
    tblData.dataSource=self;
    tblData.delegate=self;
    
    // configure textfields and buttons
    
    _textFieldAccount.layer.cornerRadius = 4.0;
    _textFieldAccount.layer.borderWidth = 1.0;
    _textFieldAccount.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textFieldQuoteNumber.layer.cornerRadius = 4.0;
    _textFieldQuoteNumber.layer.borderWidth = 1.0;
    _textFieldQuoteNumber.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textFieldWorkOrder.layer.cornerRadius = 4.0;
    _textFieldWorkOrder.layer.borderWidth = 1.0;
    _textFieldWorkOrder.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonDepartment.layer.cornerRadius = 4.0;
    _buttonDepartment.layer.borderWidth = 1.0;
    _buttonDepartment.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonService.layer.cornerRadius = 4.0;
    _buttonService.layer.borderWidth = 1.0;
    _buttonService.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonJobType.layer.cornerRadius = 4.0;
    _buttonJobType.layer.borderWidth = 1.0;
    _buttonJobType.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonServiceAddress.layer.cornerRadius = 4.0;
    _buttonServiceAddress.layer.borderWidth = 1.0;
    _buttonServiceAddress.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonLocationContact.layer.cornerRadius = 4.0;
    _buttonLocationContact.layer.borderWidth = 1.0;
    _buttonLocationContact.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonMechanic.layer.cornerRadius = 4.0;
    _buttonMechanic.layer.borderWidth = 1.0;
    _buttonMechanic.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonCategory.layer.cornerRadius = 4.0;
    _buttonCategory.layer.borderWidth = 1.0;
    _buttonCategory.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonEquipment.layer.cornerRadius = 4.0;
    _buttonEquipment.layer.borderWidth = 1.0;
    _buttonEquipment.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonArea.layer.cornerRadius = 4.0;
    _buttonArea.layer.borderWidth = 1.0;
    _buttonArea.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textFieldManufacturer.layer.cornerRadius = 4.0;
    _textFieldManufacturer.layer.borderWidth = 1.0;
    _textFieldManufacturer.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textFieldSerialNumber.layer.cornerRadius = 4.0;
    _textFieldSerialNumber.layer.borderWidth = 1.0;
    _textFieldSerialNumber.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textFieldModalNumber.layer.cornerRadius = 4.0;
    _textFieldModalNumber.layer.borderWidth = 1.0;
    _textFieldModalNumber.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textFieldInstallationDate.layer.cornerRadius = 4.0;
    _textFieldInstallationDate.layer.borderWidth = 1.0;
    _textFieldInstallationDate.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textFieldWarrantyExpirationDate.layer.cornerRadius = 4.0;
    _textFieldWarrantyExpirationDate.layer.borderWidth = 1.0;
    _textFieldWarrantyExpirationDate.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textViewQuoteDescription.layer.cornerRadius = 4.0;
    _textViewQuoteDescription.layer.borderWidth = 1.0;
    _textViewQuoteDescription.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textViewDescriptionAddDetails.layer.cornerRadius = 4.0;
    _textViewDescriptionAddDetails.layer.borderWidth = 1.0;
    _textViewDescriptionAddDetails.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textFieldCustomerPONumber.layer.cornerRadius = 4.0;
    _textFieldCustomerPONumber.layer.borderWidth = 1.0;
    _textFieldCustomerPONumber.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    
    // configure add part view elements //
    
    _buttonCategory_AddPart.layer.cornerRadius = 4.0;
    _buttonCategory_AddPart.layer.borderWidth = 1.0;
    _buttonCategory_AddPart.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonSelectPart_AddPart.layer.cornerRadius = 4.0;
    _buttonSelectPart_AddPart.layer.borderWidth = 1.0;
    _buttonSelectPart_AddPart.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonVendor_AddPart.layer.cornerRadius = 4.0;
    _buttonVendor_AddPart.layer.borderWidth = 1.0;
    _buttonVendor_AddPart.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonVendorLocation_AddPart.layer.cornerRadius = 4.0;
    _buttonVendorLocation_AddPart.layer.borderWidth = 1.0;
    _buttonVendorLocation_AddPart.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonInstallationDate_AddPart.layer.cornerRadius = 4.0;
    _buttonInstallationDate_AddPart.layer.borderWidth = 1.0;
    _buttonInstallationDate_AddPart.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonWarrantyExpireDate_AddPart.layer.cornerRadius = 4.0;
    _buttonWarrantyExpireDate_AddPart.layer.borderWidth = 1.0;
    _buttonWarrantyExpireDate_AddPart.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _textViewDescription_AddPart.layer.cornerRadius = 4.0;
    _textViewDescription_AddPart.layer.borderWidth = 1.0;
    _textViewDescription_AddPart.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    //////////////////////////////////////////////////////////////
    // configure add labor view elements //
    _textViewDescription_AddLabor.layer.cornerRadius = 4.0;
    _textViewDescription_AddLabor.layer.borderWidth = 1.0;
    _textViewDescription_AddLabor.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    // configure price view //
    _buttonDiagnosticCharge.layer.cornerRadius = 4.0;
    _buttonDiagnosticCharge.layer.borderWidth = 1.0;
    _buttonDiagnosticCharge.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonTripCharge.layer.cornerRadius = 4.0;
    _buttonTripCharge.layer.borderWidth = 1.0;
    _buttonTripCharge.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _buttonMileageCharge.layer.cornerRadius = 4.0;
    _buttonMileageCharge.layer.borderWidth = 1.0;
    _buttonMileageCharge.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    //    _textFieldMiscCharge.layer.cornerRadius = 4.0;
    //    _textFieldMiscCharge.layer.borderWidth = 1.0;
    //    _textFieldMiscCharge.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self addPaddingInTextFields:_textFieldMiscCharge];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        _hghtTableViewPart.constant = 0*230;
        _hghtTableViewLabour.constant = 0*230;
        [_tableViewPart updateConstraintsIfNeeded];
        [_tableViewLabour updateConstraintsIfNeeded];
        
        _hghtMainContainerView.constant = (_hghtMainContainerView.constant+_hghtTableViewPart.constant+_hghtTableViewLabour.constant)-400;
        
        _hghtMainContainerView.constant = _hghtMainContainerView.constant-_hghtViewAddDetails.constant;
        
        [_viewMainContainer updateConstraintsIfNeeded];
        
        _hghtViewAddDetails.constant = 0.0;
        [_viewAddDetails updateConstraintsIfNeeded];
        
    });
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    
    arrayJobType = @[@"New Construction",@"Repair/Remodeling Restoration"];
    
    [_buttonJobType setTitle:@"Repair/Remodeling Restoration" forState:UIControlStateNormal];
    strJobType = @"Remodeling";
    
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    
    NSDictionary* dictMechanicalMasters1=[defrs valueForKey:@"MasterAllMechanical"];
    arrayVendor=[dictMechanicalMasters1 valueForKey:@"VendorBasicExtSerDc"];
    arrayVendorLocation=[dictMechanicalMasters1 valueForKey:@"VendorLocationExtSerDc"];
    arrayItemVendorMapping = [dictMechanicalMasters1 valueForKey:@"ItemVendorMappingExtSerDc"];
    
    dictDetailsFortblView=[defs2 valueForKey:@"MasterServiceAutomation"];
    
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
    
    arrayEmployeeList = [defs2 objectForKey:@"EmployeeList"];
    if(arrayEmployeeList.count>0)
    {
        for(NSDictionary *dict in arrayEmployeeList)
        {
            if([[NSString stringWithFormat:@"%@",[dictLoginDetails valueForKey:@"EmployeeNumber"]] isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeNo"]]])
            {
                [_buttonMechanic setTitle:[dict valueForKey:@"FullName"] forState:UIControlStateNormal];
                break;
            }
        }
    }
    
    dictLoginDetails = [defs2 objectForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[dictLoginDetails valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    
    arrayArea=[dictDetailsFortblView valueForKey:@"AreaMaster"];
    
    NSArray *arrayTempBranch = [[[dictSalesLeadMaster valueForKey:@"BranchMasters"] firstObject] valueForKey:@"Departments"];
    NSArray *arrayTempService = [dictSalesLeadMaster valueForKey:@"ServiceMasters"];
    
    NSArray *keysWO = [[[_objWorkOrderDetail entity] attributesByName] allKeys];
    dictWorkOrderDetails = [_objWorkOrderDetail dictionaryWithValuesForKeys:keysWO];
    
    NSArray *keysSWO = [[[_objSubWorkOrderDetail entity] attributesByName] allKeys];
    dictSubWorkOrderDetails = [_objSubWorkOrderDetail dictionaryWithValuesForKeys:keysSWO];
    
    strWoType=[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWOType"]];
    
    _textFieldAccount.text = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"accountNo"]];
    _textFieldWorkOrder.text = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"workOrderNo"]];
    
    strDepartMentSysName = [dictWorkOrderDetails valueForKey:@"departmentSysName"];
    
    strWorkOrderId = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"workorderId"]];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        if (([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]))
        {
            [arrayCategory addObject:dictDataa];
        }
    }
    
    ////////// getting serial numbers////////////////
    NSDictionary *dictOfInventoryMaster=[dictDetailsFortblView valueForKey:@"InventoryMasters"];
    
    NSArray *arrOfSerialNos=[dictOfInventoryMaster valueForKey:@"SerialNumbers"];
    
    if ([arrOfSerialNos isKindOfClass:[NSString class]])
    {
        
    }
    else
    {
        for (int k=0; k<arrOfSerialNos.count; k++)
        {
            NSDictionary *dictDataa=arrOfSerialNos[k];
            NSString *strIsDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            
            if ([strIsDepartmentSysName isEqualToString:strDepartMentSysName])
            {
                [arraySerialNumber addObject:dictDataa];
            }
        }
    }
    
    ///////// getting modal numbers///////////////
    
    NSArray *arrOfModalNos=[dictOfInventoryMaster valueForKey:@"ModelNumbers"];
    
    if ([arrOfModalNos isKindOfClass:[NSString class]])
    {
        
    }
    else
    {
        for (int k=0; k<arrOfModalNos.count; k++)
        {
            NSDictionary *dictDataa=arrOfModalNos[k];
            NSString *strIsDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            
            if ([strIsDepartmentSysName isEqualToString:strDepartMentSysName])
            {
                [arrayModalNumber addObject:dictDataa];
            }
        }
        
    }
    
    /////////////////////////////////////////////
    
    /////////// getting manufacturer////////////
    
    NSArray *arrOfManufacturer=[dictOfInventoryMaster valueForKey:@"Manufacturers"];
    
    if ([arrOfManufacturer isKindOfClass:[NSString class]])
    {
        
    }
    else
    {
        for (int k=0; k<arrOfManufacturer.count; k++)
        {
            NSDictionary *dictDataa=arrOfManufacturer[k];
            
            [arrayManufacturer addObject:dictDataa];
        }
    }
    
    ///////////////////////////////////////////
    
    for(NSDictionary *dict in arrayTempBranch)
    {
        if([[dict valueForKey:@"SysName"] isEqualToString:strDepartMentSysName]&&[[dict valueForKey:@"IsActive"] integerValue]==1)
        {
            [arrayDepartment addObject:dict];
        }
    }
    if(arrayDepartment.count>0)
    {
        [_buttonDepartment setTitle:[[arrayDepartment objectAtIndex:0] valueForKey:@"Name"] forState:UIControlStateNormal];
        strDepartmentID = [NSString stringWithFormat:@"%@",[[arrayDepartment objectAtIndex:0] valueForKey:@"DepartmentMasterId"]];
    }
    
    
    for(NSDictionary *dict in arrayTempService)
    {
        if([[dict valueForKey:@"DepartmentSysName"] isEqualToString:strDepartMentSysName]&&[[dict valueForKey:@"IsActive"] integerValue]==1)
        {
            [arrayService addObject:dict];
        }
    }
    _tableViewSearch.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _tableViewSearch.layer.borderWidth=2.0;
    _tableViewSearch.layer.cornerRadius=5.0;
    _tableViewSearch.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait..."];
    [self performSelector:@selector(getServiceAddress) withObject:self afterDelay:1.0 ];
    // [self getServiceAddress];
    
    [self methodLoadValues];
    [self getHoursConfiFromMaster];
    
    [self fetchLocalMasters];
    
    strTax=[global getTaxForMechanicalService:strServiceUrlMainServiceAutomation :strCompanyKey :strWorkOrderId];
    
    if(!(strTax.length>0))
    {
        strTax=[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"tax"]];
    }
    
    // show/hide price values for technician according to isShowPriceOnQuote in company configuration
    //Note:- all the price values will be sent to server, it will be hidden only,all the functionality will work as it is as earlier
    
    if(isShowPriceOnQuote==NO)// means hide price values
    {
        _labelSubTotalAmount.textColor = [UIColor clearColor];
        _labelDiagnosticCharge.textColor = [UIColor clearColor];
        _labelTripCharge.textColor = [UIColor clearColor];
        _labelMileageCharge.textColor = [UIColor clearColor];
        _heightViewAmount.constant  =  _heightViewAmount.constant-_heightViewSubAmount.constant;
        _heightViewSubAmount.constant = 0.0;
        _hghtMainContainerView.constant = _hghtMainContainerView.constant-220.0;
        _labelSubTotalAmountHeader.hidden = YES;
    }
    
    
    //BOOL isPresetSignature = [[dictLoginDetails valueForKeyPath:@"Company.CompanyConfig.IsEmployeePresetSignature_ServiceAuto"] boolValue];
    
    
    BOOL isPresetSignature=[defs2 boolForKey:@"isPreSetSignService"];
    
    if(isPresetSignature)
    {
        NSString *strEmployeeSignature = [dictLoginDetails valueForKey:@"EmployeeSignature"];
        if([strEmployeeSignature isEqualToString:@""])
        {
            strIsEmployeePresetSignature = @"false";
        }
        else
        {
            NSArray *arrayUrl = [strEmployeeSignature componentsSeparatedByString:@"Documents"];
            NSLog(@"%@",arrayUrl);
            
            strPresetSignature = [arrayUrl objectAtIndex:1];
            strPresetSignature = [strPresetSignature substringFromIndex:1];
            _buttonTechnicalSignature.userInteractionEnabled = false;
            
            //  strPresetSignature = [NSString stringWithFormat:@"%@%@",@"/Documents/UploadImages/",strPresetSignature];
            
            NSString* webStringURL = [strEmployeeSignature stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            NSURL *urlImage = [NSURL URLWithString:webStringURL];
            
            [_imageViewSignature sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"profile.png"] options:SDWebImageRefreshCached];
            strIsEmployeePresetSignature = @"true";
            
        }
    }
    else
    {
        strIsEmployeePresetSignature = @"false";
    }
    
    //[self getPricLookupFromMaster];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    
    if([[userDef valueForKey:@"ScannedResult"] length]>0)// from scan view if ScannedResult contains value that means there is scan result and not its time to filter part
    {
        [self setpartValuesAfterScanning:[userDef valueForKey:@"ScannedResult"]];
    }
    
    if([[userDef valueForKey:@"isFromQuote"] isEqualToString:@"yes"])
    {
        NSString *strImageName;
        strImageName=[userDef valueForKey:@"imagePathQuote"];
        if(strImageName.length>0)
        {
            strTechSignaturePath = strImageName;
            UIImage *img = [self loadImage:strImageName];
            _imageViewSignature.image=img;
            isSignatureImage = YES;
        }
        [userDef setValue:@"no" forKey:@"isFromQuote"];
        [userDef synchronize];
        
    }
    else
    {
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        [userDef setValue:@"" forKey:@"imagePath"];
        [userDef synchronize];
        isSignatureImage = NO;
    }
}

#pragma mark - UITableView delegate and datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==101)
    {
        return arrayDepartment.count;
    }
    if(tableView.tag==102)
    {
        return arrayService.count;
    }
    if(tableView.tag==103)
    {
        return arrayJobType.count;
    }
    
    if(tableView.tag==104)
    {
        return arrayServiceAddress.count;
    }
    if(tableView.tag==105)
    {
        return arrayLocationContact.count;
    }
    if(tableView.tag==106)
    {
        return arrayEmployeeList.count;
    }
    if(tableView.tag==107)
    {
        return arrayCategory.count;
    }
    if(tableView.tag==108)
    {
        return arrayEquipment.count;
    }
    if(tableView.tag==109)
    {
        return arrayArea.count;
    }
    if (tableView.tag==1874919423)
    {
        return arrayEquipment.count;
    }
    if (tableView.tag==201)
    {
        return arrOfSerialNosFiltered.count;
    }
    if (tableView.tag==202)
    {
        return arrOfModelNosFiltered.count;
    }
    if (tableView.tag==203)
    {
        return arrOfManufacturerFiltered.count;
    }
    if(tableView.tag==301)
    {// return array count for category
        return arrDataTblView.count;
        
    }
    if(tableView.tag==302)
    {// return array count for parts
        return arrDataTblView.count;
    }
    if(tableView.tag==303)
    {// return array count for vendor
        return arrDataTblView.count;
    }
    if(tableView.tag==304)
    {// return array count for vendor location
        return arrDataTblView.count;
    }
    if(tableView==_tableViewPart)
    {
        return arrayPart.count;
    }
    if(tableView==_tableViewLabour)
    {
        if(arrayLabour.count>0)
        {
            return arrayLabour.count;
        }
        return 0;
    }
    if(tableView.tag==10)
    {
        return arrayDiagnostic.count;
    }
    if(tableView.tag==20)
    {
        return arrayTripCharge.count;
    }
    if(tableView.tag==30)
    {
        return arrayMileageCharge.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PartListCreateQuoteCell *cellPart = [tableView dequeueReusableCellWithIdentifier:@"PartListCreateQuoteCell"];
    LabourListCreateQuoteCell *cellLabour = [tableView dequeueReusableCellWithIdentifier:@"LabourListCreateQuoteCell"];
    
    if(cellPart==nil)
    {
        cellPart = [[PartListCreateQuoteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PartListCreateQuoteCell"];
    }
    if(cellLabour==nil)
    {
        cellLabour = [[LabourListCreateQuoteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LabourListCreateQuoteCell"];
    }
    
    if(tableView==_tableViewPart)
    {
        if([[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"isStandard"] isEqualToString:@"yes"])
        {
            cellPart.labelPart.text = [[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"dictPart"] valueForKey:@"Name"];
        }
        else
        {
            cellPart.labelPart.text = [[arrayPart objectAtIndex:indexPath.row] valueForKey:@"dictPart"];
        }
        
        cellPart.labelQty.text = [[arrayPart objectAtIndex:indexPath.row] valueForKey:@"quantity"];
        cellPart.labelTotalPrice.text = [[arrayPart objectAtIndex:indexPath.row] valueForKey:@"totalPrice"];
        cellPart.labelDescription.text = [[arrayPart objectAtIndex:indexPath.row] valueForKey:@"description"];
        
        cellPart.buttonDeletePart.tag = indexPath.row;
        [cellPart.buttonDeletePart addTarget:self action:@selector(actionOnDeletePart:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cellPart.buttonEditPart.tag = indexPath.row;
        [cellPart.buttonEditPart addTarget:self action:@selector(actionOnEditPart:) forControlEvents:UIControlEventTouchUpInside];
        
        if(isShowPriceOnQuote==NO)
        {
            cellPart.labelTotalPriceHeader.textColor = [UIColor clearColor];
            cellPart.labelTotalPrice.textColor = [UIColor clearColor];
        }
        
        cellPart.buttonViewMoreDesc.tag=indexPath.row*1000+indexPath.section;
        [cellPart.buttonViewMoreDesc addTarget:self action:@selector(action_ViewMorePartDescription:) forControlEvents:UIControlEventTouchDown];
        
        if ([NSString stringWithFormat:@"%@",[[arrayPart objectAtIndex:indexPath.row] valueForKey:@"description"]].length>43) {
            
            cellPart.buttonViewMoreDesc.hidden=NO;
            
        } else {
            
            cellPart.buttonViewMoreDesc.hidden=YES;
            
        }
        
        return cellPart;
    }
    if(tableView==_tableViewLabour)
    {
        if([[[arrayLabour objectAtIndex:indexPath.row] valueForKey:@"isLaberType"] isEqualToString:@"yes"])
        {
            cellLabour.labelLabourType.text = @"Labor";
        }
        else
        {
            cellLabour.labelLabourType.text = @"Helper";
        }
        cellLabour.labourHr.text = [[arrayLabour objectAtIndex:indexPath.row] valueForKey:@"time"];
        
        cellLabour.labelAmount.text = [[arrayLabour objectAtIndex:indexPath.row] valueForKey:@"amount"];
        
        cellLabour.buttonDeleteLabor.tag = indexPath.row;
        [cellLabour.buttonDeleteLabor addTarget:self action:@selector(actionOnDeleteLabor:) forControlEvents:UIControlEventTouchUpInside];
        
        cellLabour.buttonEditLabor.tag = indexPath.row;
        [cellLabour.buttonEditLabor addTarget:self action:@selector(actionOnEditLabor:) forControlEvents:UIControlEventTouchUpInside];
        
        if(isShowPriceOnQuote==NO)
        {
            cellLabour.labelAmountHeader.textColor = [UIColor clearColor];
            cellLabour.labelAmount.textColor = [UIColor clearColor];
        }
        
        return cellLabour;
    }
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if(tableView.tag==101)
    {
        cell.textLabel.text = [[arrayDepartment objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==102)
    {
        cell.textLabel.text = [[arrayService objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==103)
    {
        cell.textLabel.text = [arrayJobType objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==104)
    {
        cell.textLabel.text = [[arrayServiceAddress objectAtIndex:indexPath.row] valueForKey:@"CombinedAddress"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    
    if(tableView.tag==105)
    {
        cell.textLabel.text = [[arrayLocationContact objectAtIndex:indexPath.row] valueForKey:@"ContactName"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    
    if(tableView.tag==106)
    {
        cell.textLabel.text = [[arrayEmployeeList objectAtIndex:indexPath.row] valueForKey:@"FullName"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==107)
    {
        cell.textLabel.text = [[arrayCategory objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==108)
    {
        cell.textLabel.text = [[arrayEquipment objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==109)
    {
        cell.textLabel.text = [[arrayArea objectAtIndex:indexPath.row] valueForKey:@"AreaName"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    
    if(tableView.tag==201)
    {
        cell.textLabel.text = [[arrOfSerialNosFiltered objectAtIndex:indexPath.row] valueForKey:@"SerialNumber"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==202)
    {
        cell.textLabel.text = [[arrOfModelNosFiltered objectAtIndex:indexPath.row] valueForKey:@"ModalNumber"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==203)
    {
        cell.textLabel.text = [[arrOfManufacturerFiltered objectAtIndex:indexPath.row] valueForKey:@"Manufacturer"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==301)
    {
        cell.textLabel.text = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
        
    }
    if(tableView.tag==302)
    {
        cell.textLabel.text = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==303)
    {
        cell.textLabel.text = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==304)
    {
        cell.textLabel.text = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==10)
    {// diagnostic charge
        
        cell.textLabel.text = [[arrayDiagnostic objectAtIndex:indexPath.row] valueForKey:@"ChargeName"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==20)
    {// trip charge
        
        cell.textLabel.text = [[arrayTripCharge objectAtIndex:indexPath.row] valueForKey:@"ChargeName"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    if(tableView.tag==30)
    {// mileage charge
        
        cell.textLabel.text = [[arrayMileageCharge objectAtIndex:indexPath.row] valueForKey:@"ChargeName"];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(tableView.tag==101)
    {
        [_buttonDepartment setTitle:[[arrayDepartment objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
        strDepartmentID = [NSString stringWithFormat:@"%@",[[arrayDepartment objectAtIndex:indexPath.row] valueForKey:@"DepartmentMasterId"]];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==102)
    {
        [_buttonService setTitle:[[arrayService objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
        strServiceID = [NSString stringWithFormat:@"%@",[[arrayService objectAtIndex:indexPath.row] valueForKey:@"ServiceId"]];
        strPrimaryServiceSysName = [[arrayService objectAtIndex:indexPath.row] valueForKey:@"ServiceSysName"];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==103)
    {
        //arrayJobType
        [_buttonJobType setTitle:[arrayJobType objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        strJobType = [arrayJobType objectAtIndex:indexPath.row];
        
        if([strJobType isEqualToString:@"New Construction"])
        {
            strJobType = @"NewConstruction";
        }
        else
        {
            strJobType = @"Remodeling";
        }
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==104)
    {
        [_buttonServiceAddress setTitle:[[arrayServiceAddress objectAtIndex:indexPath.row] valueForKey:@"CombinedAddress"] forState:UIControlStateNormal];
        addressID = [[[arrayServiceAddress objectAtIndex:indexPath.row] valueForKey:@"CustomerAddressId"] integerValue];
        strServiceAddressID = [NSString stringWithFormat:@"%@",[[arrayServiceAddress objectAtIndex:indexPath.row] valueForKey:@"CustomerAddressId"]];//AddressTypeId
        
        strServiceAddressName = [[arrayServiceAddress objectAtIndex:indexPath.row] valueForKey:@"Name"];
        [self getLocationContact];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==105)
    {
        [_buttonLocationContact setTitle:[[arrayLocationContact objectAtIndex:indexPath.row] valueForKey:@"ContactName"] forState:UIControlStateNormal];
        strCRMContactID = [NSString stringWithFormat:@"%@",[[arrayLocationContact objectAtIndex:indexPath.row] valueForKey:@"CrmContactId"]];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==106)
    {
        [_buttonMechanic setTitle:[[arrayEmployeeList objectAtIndex:indexPath.row] valueForKey:@"FullName"] forState:UIControlStateNormal];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==107)
    {
        
        strCategoryId=@"";
        strCategorySysNameSelected=@"";
        [_buttonCategory_AddPart setTitle:@"--Select Category--" forState:UIControlStateNormal];
        [_buttonSelectPart_AddPart setTitle:@"--Select Part--" forState:UIControlStateNormal];
        [_buttonInstallationDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
        [_buttonWarrantyExpireDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
        [_buttonVendor_AddPart setTitle:@"--Select Vendor--" forState:UIControlStateNormal];
        [_buttonVendorLocation_AddPart setTitle:@"--Select Vendor Location--" forState:UIControlStateNormal];
        _textViewDescription_AddPart.text=@"";
        _textFieldQuantity_AddPart.text=@"";
        _textFieldModelNumber_AddPart.text=@"";
        _textFieldSerialNumber_AddPart.text=@"";
        _textFieldUnitPrice_AddPart.text=@"";
        _textFieldTotalPrice_AddPart.text=@"";
        _textFieldManufacturer_AddPart.text=@"";
        _textFieldPartName_AddPart.text=@"";
        
        [_buttonCategory setTitle:[[arrayCategory objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
        strCategoryId = [NSString stringWithFormat:@"%@",[[arrayCategory objectAtIndex:indexPath.row] valueForKey:@"CategoryMasterId"]];
        dictCategory = [arrayCategory objectAtIndex:indexPath.row];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==108)
    {
        [_buttonEquipment setTitle:[[arrayEquipment objectAtIndex:indexPath.row] valueForKey:@"Name"] forState:UIControlStateNormal];
        strItemSysName = [[arrayEquipment objectAtIndex:indexPath.row] valueForKey:@"SysName"];
        
        strEquipmentCode = strItemSysName;
        strEquipmentName = [[arrayEquipment objectAtIndex:indexPath.row] valueForKey:@"Name"];
        strEquipmentNumber = [NSString stringWithFormat:@"%@",[[arrayEquipment objectAtIndex:indexPath.row] valueForKey:@"ItemNumber"]];
        
        strEquipmentCategorySysName = [NSString stringWithFormat:@"%@",[[arrayEquipment objectAtIndex:indexPath.row] valueForKey:@"CategorySysName"]];
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    
    if(tableView.tag==109)
    {
        [_buttonArea setTitle:[[arrayArea objectAtIndex:indexPath.row] valueForKey:@"AreaName"] forState:UIControlStateNormal];
        strArea = [[arrayArea objectAtIndex:indexPath.row] valueForKey:@"AreaName"];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==201)
    {// serial number
        _textFieldSerialNumber.text = [[arrOfSerialNosFiltered objectAtIndex:indexPath.row] valueForKey:@"SerialNumber"];
        
        NSLog(@"serial number");
        [_textFieldSerialNumber resignFirstResponder];
        [_viewTextFieldSearch removeFromSuperview];
    }
    if(tableView.tag==202)
    {// modal number
        NSLog(@"modal number");
        _textFieldModalNumber.text = [[arrOfModelNosFiltered objectAtIndex:indexPath.row] valueForKey:@"ModelNumber"];
        
        [_textFieldModalNumber resignFirstResponder];
        [_viewTextFieldSearch removeFromSuperview];
    }
    if(tableView.tag==203)
    {// manufacturer
        NSLog(@"manufacture");
        _textFieldManufacturer.text = [[arrOfManufacturerFiltered objectAtIndex:indexPath.row] valueForKey:@"Manufacturer"];
        [_textFieldManufacturer resignFirstResponder];
        [_viewTextFieldSearch removeFromSuperview];
    }
    if(tableView.tag==301)
    {// category_add part
        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
        [_buttonCategory_AddPart setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        strCategoryId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategoryMasterId"]];
        strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        dictCategory_AddPart = dictData;
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        
        
    }
    if(tableView.tag==302)
    {// part
        strIsChangeStdPartPrice = @"false";
        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
        [_buttonSelectPart_AddPart setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        [_textFieldQuantity_AddPart setEnabled:YES];
        
        _textViewDescription_AddPart.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Description"]];
        
        float unitPriceFloat=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BestPrice"]] floatValue];
        
        //        _textFieldUnitPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
        dictDataPartsSelected=dictData;
        itemMasterID = [[dictData valueForKey:@"ItemMasterId"] integerValue];
        
        NSString *strType;
        
        if (isStandard)
        {
            strType=@"Standard";
        }
        else
        {
            strType=@"Non-Standard";
        }
        
        if (strCategorySysNameSelected.length==0) {
            
            strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
            
        }
        
        NSString *strPartCate=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
        
        if (strPartCate.length==0) {
            
            strCategorySysNameSelected=@"";
            
        }else{
            
            strCategorySysNameSelected=strPartCate;
            
        }
        
        NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",unitPriceFloat] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :strType];
        
        float multiplier=[strMultiplier floatValue];
        
        strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];
        _textFieldQuantity_AddPart.text = @"1";
        [_textFieldQuantity_AddPart resignFirstResponder];
        NSString *strTextQTY=[NSString stringWithFormat:@"%@",_textFieldQuantity_AddPart.text];
        
        _textFieldUnitPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",multiplier*unitPriceFloat];
        
        if (strTextQTY.length>0)
        {
            float totalPriceStandard=[strTextQTY floatValue]*multiplier*unitPriceFloat;
            _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
        }
        
        NSLog(@"%@",[arrDataTblView objectAtIndex:indexPath.row]);
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==303)
    {// vendor
        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
        dictVendor = dictData;
        [_buttonVendor_AddPart setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==304)
    {// vendor location
        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
        dictVendorLocation = dictData;
        [_buttonVendorLocation_AddPart setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==10)
    {
        NSDictionary *dict=[arrayDiagnostic objectAtIndex:indexPath.row];
        
        [_buttonDiagnosticCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
        
        strDiagnosticChargeName = [NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeName"]];
        
        float amtDC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
        
        strDiagnosticChargeAmt = [NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
        
        _labelDiagnosticCharge.text=[NSString stringWithFormat:@"$%.02f",amtDC];
        
        strDiagnosticChargeID=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
        
        [self taxCalculationNewPlan];
        
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==20)
    {
        NSDictionary *dict=[arrayTripCharge objectAtIndex:indexPath.row];
        [_buttonTripCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
        
        strTripChargeName = [NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeName"]];
        
        float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
        
        strTripChargeAmt = [NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
        
        amtTC=amtTC*[_txtFldQty.text floatValue];
        
        _labelTripCharge.text=[NSString stringWithFormat:@"$%.02f",amtTC];
        
        strTripChargeID=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
        
        [self taxCalculationNewPlan];
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
    }
    if(tableView.tag==30)
    {
        NSDictionary *dict=[arrayMileageCharge objectAtIndex:indexPath.row];
        [_buttonMileageCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
        
        strMileageName = [NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeName"]];
        
        chargeAmount =[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
        
        float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
        
        strMileageChargeAmt = [NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
        
        // strChargeAmountMiles=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
        
        amtTC=amtTC*[_textFieldMiles.text floatValue];
        
        _labelMileageCharge.text=[NSString stringWithFormat:@"%.02f",amtTC];
        
        [self taxCalculationNewPlan];
        
        [tblData removeFromSuperview];
        [buttonBackground removeFromSuperview];
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==_tableViewPart)
    {
        return 230.0;
    }
    else if(tableView==_tableViewLabour)
    {
        if(isShowPriceOnQuote==NO)
        {
            return 230.0-50.0;
        }
        else
        {
            return 230.0;
        }
    }
    return 50.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView==_tableViewPart)
    {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:0.8];
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 100, 40)];
        lbl.text = @"Part";
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.textColor = [UIColor whiteColor];
        
        lbl.font = [UIFont systemFontOfSize:28.0];
        view.clipsToBounds = YES;
        [view addSubview:lbl];
        return view;
    }
    if(tableView==_tableViewLabour)
    {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:0.8];
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 100, 40)];
        lbl.text = @"Labor";
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.textColor = [UIColor whiteColor];
        lbl.font = [UIFont systemFontOfSize:28.0];
        view.clipsToBounds = YES;
        [view addSubview:lbl];
        return view;
    }
    return [UIView new];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == _tableViewPart || tableView==_tableViewLabour)
    {
        return 60.0;
    }
    return 0.0;
}

#pragma mark - UIButton Action

- (IBAction)actionOnAddDetails:(id)sender
{
    if(isAddDetailsChecked==NO)
    {
        if(strServiceID.length>0)
        {
            isAddDetailsChecked = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_buttonAddDetails setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
                
                _hghtViewAddDetails.constant = 850;
                [_viewAddDetails updateConstraintsIfNeeded];
                _hghtMainContainerView.constant = _hghtMainContainerView.constant+_hghtViewAddDetails.constant;
                [_viewMainContainer updateConstraintsIfNeeded];
            });
        }
        else
        {
            [global AlertMethod:Alert :@"Please select service"];
        }
    }
    else
    {
        isAddDetailsChecked = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_buttonAddDetails setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            
            [_viewAddDetails updateConstraintsIfNeeded];
            _hghtMainContainerView.constant = _hghtMainContainerView.constant-   _hghtViewAddDetails.constant;
            _hghtViewAddDetails.constant = 0;
            [_viewMainContainer updateConstraintsIfNeeded];
        });
    }
}

- (IBAction)actionOnDepartment:(id)sender
{
    tblData.tag = 101;
    [self setTableFrame];
    [tblData reloadData];
}
- (IBAction)actionOnService:(id)sender
{
    tblData.tag = 102;
    [self setTableFrame];
    [tblData reloadData];
    
}
- (IBAction)actionOnJobType:(id)sender
{
    tblData.tag = 103;
    [self setTableFrame];
    [tblData reloadData];
}
- (IBAction)actionOnServiceAddress:(id)sender
{
    tblData.tag = 104;
    [self setTableFrame];
    [tblData reloadData];
}
- (IBAction)actionOnLocationContact:(id)sender
{
    if([_buttonServiceAddress.titleLabel.text isEqualToString:@"Select Service Address"])
    {
        [global AlertMethod:@"Message" :@"Please select service address."];
    }
    else
    {
        if(arrayLocationContact.count>0)
        {
            tblData.tag = 105;
            //arrayLocationContact
            [self setTableFrame];
            [tblData reloadData];
        }
        else
        {
            [global AlertMethod:Alert :@"No data available"];
        }
    }
}


- (IBAction)actionOnMechanic:(id)sender
{
    //arrayEmployeeList
    tblData.tag = 106;
    [self setTableFrame];
    [tblData reloadData];
    
}
- (IBAction)actionOnAddPart:(id)sender
{
    if(!(strItemSysName.length>0))
    {
        [global AlertMethod:Alert :@"Please select equipment"];
    }
    else
    {
        [self addKeyboardObserverForPartView];
        isAddPart = YES;
        isAddLabor = NO;
        isEditPart = NO;
        strCategoryId=@"";
        strCategorySysNameSelected=@"";
        _buttonVendor_AddPart.userInteractionEnabled = true;
        
        _buttonVendorLocation_AddPart.userInteractionEnabled = true;
        
        [_buttonCategory_AddPart setTitle:@"--Select Category--" forState:UIControlStateNormal];
        [_buttonSelectPart_AddPart setTitle:@"--Select Part--" forState:UIControlStateNormal];
        [_buttonInstallationDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
        [_buttonWarrantyExpireDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
        [_buttonVendor_AddPart setTitle:@"--Select Vendor--" forState:UIControlStateNormal];
        [_buttonVendorLocation_AddPart setTitle:@"--Select Vendor Location--" forState:UIControlStateNormal];
        
        
        isStandard=NO;
        [_buttonNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isWarranty=NO;
        isEditParts=NO;
        [_buttonWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        _textViewDescription_AddPart.text=@"";
        _textFieldQuantity_AddPart.text=@"";
        _textFieldModelNumber_AddPart.text=@"";
        _textFieldSerialNumber_AddPart.text=@"";
        _textFieldUnitPrice_AddPart.text=@"";
        _textFieldTotalPrice_AddPart.text=@"";
        _textFieldManufacturer_AddPart.text=@"";
        _textFieldPartName_AddPart.text=@"";
        _lblPartSelectOrEnter.text=@"Part";
        
        //        UIFont *fnt = [UIFont fontWithName:@"Helvetica" size:22.0];
        
        //        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Part*"
        //                                                                                             attributes:@{NSFontAttributeName: [fnt fontWithSize:22]}];
        //        [attributedString setAttributes:@{NSFontAttributeName : [fnt fontWithSize:22]
        //                                          , NSBaselineOffsetAttributeName : @10} range:NSMakeRange(4, 1)];
        //
        //        _lblPartSelectOrEnter.attributedText=attributedString;
        
        
        
        //        [_textFieldPartName_AddPart setHidden:YES];
        //        [_buttonSelectPart_AddPart setHidden:NO];
        //        [_textFieldUnitPrice_AddPart setEnabled:NO];
        
        
        [_textFieldPartName_AddPart setHidden:NO];
        [_buttonSelectPart_AddPart setHidden:YES];
        [_textFieldUnitPrice_AddPart setEnabled:YES];
        
        
        [self addPartView];
    }
}
- (IBAction)actionOnAddLabour:(id)sender
{
    if(!(strItemSysName.length>0))
    {
        [global AlertMethod:Alert :@"Please add select equipment"];
    }
    else
    {
        isLaborType=YES;
        isEditLabor=NO;
        isAddLabor = YES;
        _textFieldHrs_AddLabor.placeholder=@"Entre Time(HH:mm)";
        [_textFieldAmount_AddLabor setEnabled:NO];
        _textViewDescription_AddLabor.text=@"";
        _textFieldHrs_AddLabor.text=@"";
        // [_buttonSelectTime_AddLabor setTitle:@"Select Time(HH:MM)" forState:UIControlStateNormal];
        // _textFieldAmount_AddLabor.text=@"0.00";
        isWarranty=NO;
        [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
        [_buttonLabor setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
        
        [self addLaborView];
    }
}

- (IBAction)actionOnDiagnosticCharge:(id)sender
{
    tblData.tag = 10;
    [self setTableFrame];
    [tblData reloadData];
}
- (IBAction)actionOnTripCharge:(id)sender
{
    tblData.tag = 20;
    [self setTableFrame];
    [tblData reloadData];
}
- (IBAction)actionOnMileageCharge:(id)sender
{
    tblData.tag = 30;
    [self setTableFrame];
    [tblData reloadData];
}

- (IBAction)actionOnTechnicalSignature:(id)sender
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:@"yes" forKey:@"isFromQuote"];
    
    [userDef synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
    ServiceSignViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"ServiceSignViewControlleriPad"];
    
    // objSignViewController.strType=@"inspector";
    [self presentViewController:objSignViewController animated:YES completion:nil];
}
- (IBAction)actionOnSubmitRequest:(id)sender
{
    if([_buttonDepartment.titleLabel.text isEqualToString:@"Select Department"])
    {
        [global AlertMethod:Alert :@"Please select department"];
        return;
    }
    else if ([_buttonService.titleLabel.text isEqualToString:@"Select Service"])
    {
        [global AlertMethod:Alert :@"Please select service"];
        return;
    }
    else if ([_buttonJobType.titleLabel.text isEqualToString:@"Select Job Type"])
    {
        [global AlertMethod:Alert :@"Please select job type"];
        return;
    }
    else if ([_buttonServiceAddress.titleLabel.text isEqualToString:@"Select Service Address"])
    {
        [global AlertMethod:Alert :@"Please select service address"];
        return;
    }
    else if ([_buttonLocationContact.titleLabel.text isEqualToString:@"Select Location Contact"])
    {
        [global AlertMethod:Alert :@"Please select location contact"];
        return;
    }
    else if ([_buttonLocationContact.titleLabel.text isEqualToString:@"Select Mechanic"])
    {
        [global AlertMethod:Alert :@"Please select mechanic"];
        return;
    }
    if([strIsEmployeePresetSignature isEqualToString:@"true"])
    {
        strTechSignaturePath = strPresetSignature;
    }
    else if(!(strTechSignaturePath.length>0))
    {
        [global AlertMethod:Alert :@"Technical signature is required"];
        return;
    }
    if(strMileageName.length>0)
    {
        if(!(_textFieldMiles.text.length>0))
        {
            [global AlertMethod:Alert :@"Please enter Miles"];
            return;
        }
        if(!([_textFieldMiles.text floatValue]>0))
        {
            [global AlertMethod:Alert :@"Miles value can not be zero"];
            return;
        }
    }
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:Alert :ErrorInternetMsg];
        return;
    }
    
    if([strIsEmployeePresetSignature isEqualToString:@"true"])
    {
        [self syncQuoteDataOnServer];
    }
    else
    {
        if(strTechSignaturePath.length>0)
        {
            [self uploadSignatureImageOnServer];
        }
        else
        {
            [self syncQuoteDataOnServer];
        }
    }
    
}


- (IBAction)actionOnBack:(id)sender
{
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:@"no" forKey:@"isFromQuote"];
    [userDef synchronize];
    [self goToSubWorkOrderDetailView];
    
}

- (IBAction)actionOnScanBarcode:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"" forKey:@"ScannedResult"];
    [defs setBool:YES forKey:@"IsScannerView"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ScannerViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ScannerViewController"];
    
    [self presentViewController:objByProductVC animated:YES completion:nil];
}


-(void)goToSubWorkOrderDetailView{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    /*
     if ([_strFromWhere isEqualToString:@"Start"]) {
     
     NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
     [defss setInteger:-1 forKey:@"sectionToOpen"];
     [defss synchronize];
     
     if ([strWoType isEqualToString:@"TM"]) {
     
     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
     bundle: nil];
     MechanicalStartRepairTM
     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairTM"];
     objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]];
     objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
     objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
     objByProductVC.strWoType=strWoType;
     
     NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
     //        [defsId setValue:strWorkOrderId forKey:@"SubWorkOrderId"];
     [defsId setValue:[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]]  forKey:@"SubWorkOrderId"];
     [defsId synchronize];
     
     [self.navigationController pushViewController:objByProductVC animated:NO];
     
     
     } else {
     
     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
     bundle: nil];
     MechanicalStartRepairViewController
     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairViewController"];
     objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]];
     objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
     objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
     objByProductVC.strWoType=strWoType;
     
     NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
     [defsId setValue:[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]]  forKey:@"SubWorkOrderId"];
     [defsId synchronize];
     
     [self.navigationController pushViewController:objByProductVC animated:NO];
     
     }
     
     
     } else {
     
     NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
     [defss setInteger:-1 forKey:@"sectionToOpen"];
     [defss synchronize];
     
     if ([strWoType isEqualToString:@"TM"]) {
     
     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
     bundle: nil];
     MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController
     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController"];
     objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]];
     objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
     objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
     objByProductVC.strWoType=strWoType;
     
     NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
     //        [defsId setValue:strWorkOrderId forKey:@"SubWorkOrderId"];
     [defsId setValue:[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]]  forKey:@"SubWorkOrderId"];
     [defsId synchronize];
     
     [self.navigationController pushViewController:objByProductVC animated:NO];
     
     
     } else {
     
     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
     bundle: nil];
     MechanicalSubWorkOrderDetailsViewControlleriPad
     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControlleriPad"];
     objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]];
     objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
     objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
     objByProductVC.strWoType=strWoType;
     
     NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
     [defsId setValue:[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]]  forKey:@"SubWorkOrderId"];
     [defsId synchronize];
     
     [self.navigationController pushViewController:objByProductVC animated:NO];
     
     }
     
     }
     
     */
}

- (IBAction)actionOnCategory:(id)sender
{
    
    if(arrayCategory.count>0)
    {
        tblData.tag = 107;
        [self setTableFrame];
        [tblData reloadData];
    }
    else
    {
        [global AlertMethod:Info :NoDataAvailable];
    }
}
- (IBAction)actionOnEquipment:(id)sender
{
    NSString *strEquipmentValueId;
    NSArray *arrOfAreaMasterEquipmentTypes=[dictDetailsFortblView valueForKey:@"EquipmentTypes"];
    NSString *strEqupMentTypeValue;
    for (int k=0; k<arrOfAreaMasterEquipmentTypes.count; k++)
    {
        NSDictionary *dictDataa=arrOfAreaMasterEquipmentTypes[k];
        
        NSString *strEqupMentTypeName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Text"]];
        
        if ([strEqupMentTypeName isEqualToString:@"Equipment"])
        {
            strEqupMentTypeValue=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Value"]];
        }
    }
    
    if (strEqupMentTypeValue.length==0)
    {
        strEquipmentValueId=@"1";
    }
    else
    {
        strEquipmentValueId=strEqupMentTypeValue;
    }
    
    
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryItems"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++)
    {
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strEquipmentIdToCheck=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemType"]];
        NSString *strCategoryIdToCheck=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryId"]];
        
        if (strCategoryId.length==0)
        {
            strCategoryId=@"";
            strCategoryIdToCheck=@"";
        }
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        if ([strEquipmentIdToCheck isEqualToString:strEquipmentValueId] && [strCategoryIdToCheck isEqualToString:strCategoryId] && ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]))
        {
            [arrayEquipment addObject:dictDataa];
        }
    }
    
    if (arrayEquipment.count==0)
    {
        [global AlertMethod:Info :NoDataAvailableee];
    }
    else
    {
        tblData.tag = 108;
        [self setTableFrame];
        [tblData reloadData];
    }
}
- (IBAction)actionOnArea:(id)sender
{
    if(arrayArea.count>0)
    {
        tblData.tag = 109;
        [self setTableFrame];
        [tblData reloadData];
    }
    else
    {
        [global AlertMethod:Info :NoDataAvailable];
    }
}
-(void)actionOnBackground:(id)sender
{
    [tblData removeFromSuperview];
    [buttonBackground removeFromSuperview];
}
- (IBAction)actionOnSaveTxtFldSearch:(id)sender
{
    if(_tableViewSearch.tag==201)
    {
        if(!(_txtFieldSearch.text.length>0))
        {
            [global AlertMethod:Alert :@"Please enter serial number"];
        }
        else
        {
            strSerialNumber = _txtFieldSearch.text;
            _textFieldSerialNumber.text = strSerialNumber;
            [_textFieldSerialNumber resignFirstResponder];
            [_viewTextFieldSearch removeFromSuperview];
        }
    }
    else if (_tableViewSearch.tag==202)
    {
        if(!(_txtFieldSearch.text.length>0))
        {
            [global AlertMethod:Alert :@"Please enter modal number"];
        }
        else
        {
            strModalNumber = _txtFieldSearch.text;
            _textFieldModalNumber.text = strModalNumber;
            [_textFieldModalNumber resignFirstResponder];
            [_viewTextFieldSearch removeFromSuperview];
        }
    }
    else if (_tableViewSearch.tag==203)
    {
        if(!(_txtFieldSearch.text.length>0))
        {
            [global AlertMethod:Alert :@"Please enter manufacturer"];
        }
        else
        {
            strManufacturer = _txtFieldSearch.text;
            _textFieldManufacturer.text = strManufacturer;
            [_textFieldManufacturer resignFirstResponder];
            [_viewTextFieldSearch removeFromSuperview];
        }
    }
}

- (IBAction)actionOnCancelTxtFldSearch:(id)sender
{
    [_viewTextFieldSearch removeFromSuperview];
}

// UIButton action on Add Part View//
- (IBAction)actionOnStandard_AddPart:(id)sender
{
    [self hideTextFields];
    
    isStandard=YES;
    strIsChangeStdPartPrice = @"false";
    
    [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    [_buttonNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    _textFieldPartName_AddPart.text=@"";
    
    _textFieldQuantity_AddPart.text=@"";
    
    // _lblPartSelectOrEnter.text=@"Part";
    
    _textFieldUnitPrice_AddPart.text=@"";
    
    [_textFieldPartName_AddPart setHidden:YES];
    
    [_buttonSelectPart_AddPart setHidden:NO];
    
    [_textFieldUnitPrice_AddPart setEnabled:YES];
    
    _textFieldTotalPrice_AddPart.text=@"";
    
    [_buttonCategory_AddPart setTitle:@"--Select Category--" forState:UIControlStateNormal];
    
    [_buttonSelectPart_AddPart setTitle:@"--Select Part--" forState:UIControlStateNormal];
    
    _buttonVendor_AddPart.userInteractionEnabled = true;
    
    _buttonVendorLocation_AddPart.userInteractionEnabled = true;
    
    
    strCategoryId=@"";
    strCategorySysNameSelected=@"";
    [_buttonCategory_AddPart setTitle:@"--Select Category--" forState:UIControlStateNormal];
    [_buttonSelectPart_AddPart setTitle:@"--Select Part--" forState:UIControlStateNormal];
    [_buttonInstallationDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
    [_buttonWarrantyExpireDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
    [_buttonVendor_AddPart setTitle:@"--Select Vendor--" forState:UIControlStateNormal];
    [_buttonVendorLocation_AddPart setTitle:@"--Select Vendor Location--" forState:UIControlStateNormal];
    _textViewDescription_AddPart.text=@"";
    _textFieldQuantity_AddPart.text=@"";
    _textFieldModelNumber_AddPart.text=@"";
    _textFieldSerialNumber_AddPart.text=@"";
    _textFieldUnitPrice_AddPart.text=@"";
    _textFieldTotalPrice_AddPart.text=@"";
    _textFieldManufacturer_AddPart.text=@"";
    _textFieldPartName_AddPart.text=@"";
    
    
    NSString *strInventory = [NSString stringWithFormat:@"Unit Price($)%@",@"*"];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strInventory];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(13,1)];
    _lblUnitPriceTxtFldTitle.attributedText = string;
    
    _textFieldUnitPrice_AddPart.placeholder = @"Unit Price";
    
}


- (IBAction)actionOnNonStandard_AddPart:(id)sender
{
    // strCategorySysNameSelected=@"";
    
    [self hideTextFields];
    
    
    isStandard=NO;
    strIsChangeStdPartPrice = @"false";
    
    [_buttonNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    
    _textFieldPartName_AddPart.text=@"";
    
    _textFieldUnitPrice_AddPart.text=@"";
    
    _textFieldQuantity_AddPart.text=@"";
    
    //    _lblPartSelectOrEnter.text=@"Part Name";
    
    [_textFieldPartName_AddPart setHidden:NO];
    
    [_buttonSelectPart_AddPart setHidden:YES];
    
    [_textFieldUnitPrice_AddPart setEnabled:YES];
    
    // [_txtQty setEnabled:YES];
    
    _textFieldTotalPrice_AddPart.text=@"";
    
    _buttonVendor_AddPart.userInteractionEnabled = false;
    
    _buttonVendorLocation_AddPart.userInteractionEnabled = false;
    
    _textViewDescription_AddPart.text=@"";
    
    
    strCategoryId=@"";
    strCategorySysNameSelected=@"";
    [_buttonCategory_AddPart setTitle:@"--Select Category--" forState:UIControlStateNormal];
    [_buttonSelectPart_AddPart setTitle:@"--Select Part--" forState:UIControlStateNormal];
    [_buttonInstallationDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
    [_buttonWarrantyExpireDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
    [_buttonVendor_AddPart setTitle:@"--Select Vendor--" forState:UIControlStateNormal];
    [_buttonVendorLocation_AddPart setTitle:@"--Select Vendor Location--" forState:UIControlStateNormal];
    _textViewDescription_AddPart.text=@"";
    _textFieldQuantity_AddPart.text=@"";
    _textFieldModelNumber_AddPart.text=@"";
    _textFieldSerialNumber_AddPart.text=@"";
    _textFieldUnitPrice_AddPart.text=@"";
    _textFieldTotalPrice_AddPart.text=@"";
    _textFieldManufacturer_AddPart.text=@"";
    _textFieldPartName_AddPart.text=@"";
    
    
    NSString *strInventory = [NSString stringWithFormat:@"Vendor Price($)%@",@"*"];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strInventory];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(15,1)];
    _lblUnitPriceTxtFldTitle.attributedText = string;
    _textFieldUnitPrice_AddPart.placeholder = @"Vendor Price";
}

- (IBAction)actionOnCategory_AddPart:(id)sender
{
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++)
    {
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        
        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName]))
        {
            [arrDataTblView addObject:dictDataa];
        }
    }
    
    if (arrDataTblView.count==0)
    {
        [global AlertMethod:Info :NoDataAvailable];
    }
    else
    {
        tblData.tag=301;
        [self setTableFrame];
        [tblData reloadData];
    }
    
}

- (IBAction)actionOnPart_AddPart:(id)sender
{
    [self getPartsMaster];
    
    if (arrDataTblView.count==0)
    {
        [global AlertMethod:Info :NoDataAvailable];
    }
    else
    {
        tblData.tag=302;
        [self setTableFrame];
        [tblData reloadData];
    }
}

- (IBAction)actionOnInstallationDate_AddPart:(id)sender
{
    isInstallationDate = YES;
    isWarrantyDate = NO;
    [self hideTextFields];
    [self addPickerViewDateTo:@""];
}

- (IBAction)actionOnSavePart:(id)sender
{
    
    if(isEditPart)
    {
        NSMutableDictionary *dict = [[arrayPart objectAtIndex:cellTag] mutableCopy];
        
        if(isStandard)
        {
            if([_buttonSelectPart_AddPart.titleLabel.text isEqualToString:@"--Select Part--"])
            {
                [global AlertMethod:Alert :@"Please select part"];
                return;
            }
        }
        
        else
        {
            if(!(_textFieldPartName_AddPart.text.length>0))
            {
                [global AlertMethod:Alert :@"Please enter part name"];
                return;
            }
        }
        
        if(!(_textFieldQuantity_AddPart.text.length>0))
        {
            [global AlertMethod:Alert :@"Please enter quantity"];
            return;
        }
        else
        {
            NSString *strDescription = @"";
            
            NSString *warranty = @"no";
            
            NSString *strUnitPrice = @"";
            
            NSString *strSerialNo = @"";
            
            NSString *strModalNo = @"";
            
            NSString *strManufacture = @"";
            
            NSString *strInstallationDate = @"";
            
            NSString *strWarranty = @"";
            
            NSString *strIsStd = @"";
            
            
            if(isWarranty)
            {
                warranty = @"yes";
            }
            
            if(_textViewDescription_AddPart.text.length>0)
            {
                strDescription = _textViewDescription_AddPart.text;
            }
            
            if(_textFieldUnitPrice_AddPart.text.length>0)
            {
                //  strUnitPrice = _textFieldUnitPrice_AddPart.text;
                if(isStandard)
                {
                    strUnitPrice = [NSString stringWithFormat:@"%.2f",[_textFieldUnitPrice_AddPart.text floatValue]/[strMultiplierGlobal floatValue]];
                }
                else
                {
                    strUnitPrice = _textFieldUnitPrice_AddPart.text;
                }
            }
            
            if(_textFieldSerialNumber_AddPart.text.length>0)
            {
                strSerialNo = _textFieldSerialNumber_AddPart.text;
            }
            
            if(_textFieldModelNumber_AddPart.text.length>0)
            {
                strModalNo = _textFieldModelNumber_AddPart.text;
            }
            
            if(_textFieldManufacturer_AddPart.text.length>0)
            {
                strManufacture = _textFieldManufacturer_AddPart.text;
            }
            
            if(!([_buttonInstallationDate_AddPart.titleLabel.text isEqualToString:@"--SelectDate--"]))
            {
                strInstallationDate = _buttonInstallationDate_AddPart.titleLabel.text;
            }
            
            
            if(!([_buttonWarrantyExpireDate_AddPart.titleLabel.text isEqualToString:@"--SelectDate--"]))
            {
                strWarranty = _buttonWarrantyExpireDate_AddPart.titleLabel.text;
            }
            
            
            // instalation date must be less than expiration date
            if(strInstallationDate.length>0 && strWarranty.length>0)
            {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"MM/dd/yyyy"];
                NSDate *installationDate = [dateFormat dateFromString:strInstallationDate];
                NSDate *expirationDate = [dateFormat dateFromString:strWarranty];
                
                NSComparisonResult result = [installationDate compare:expirationDate];
                if(result == NSOrderedSame || result == NSOrderedDescending)
                {
                    [global AlertMethod:Alert :@"Warrenty expiration date must be greater than Installation date"];
                    return;
                }
            }
            
            
            if(isStandard)
            {
                strIsStd = @"yes";
            }
            
            else
            {
                strIsStd = @"no";
            }
            
            if(dictVendor==nil)
            {
                dictVendor = [[NSDictionary alloc]init];
            }
            
            if(dictVendorLocation==nil)
            {
                dictVendorLocation = [[NSDictionary alloc]init];
            }
            
            if(dictCategory_AddPart==nil)
            {
                dictCategory_AddPart = [[NSDictionary alloc]init];
            }
            
            if(strMultiplierGlobal==nil)
            {
                strMultiplierGlobal = @"";
            }
            
            if(isStandard)
            {
                
                [dict setValue:dictDataPartsSelected forKey:@"dictPart"];
                
                [dict setValue:_textFieldQuantity_AddPart.text forKey:@"quantity"];
                
                [dict setValue:_textFieldTotalPrice_AddPart.text forKey:@"totalPrice"];
                
                [dict setValue:strDescription forKey:@"description"];
                
                [dict setValue:strIsStd forKey:@"isStandard"];
                
                [dict setValue:warranty forKey:@"isWarranty"];
                
                [dict setValue:strUnitPrice forKey:@"unitPrice"];
                
                [dict setValue:strSerialNo forKey:@"serialNumber"];
                
                [dict setValue:strModalNo forKey:@"modelNumber"];
                
                [dict setValue:strManufacture forKey:@"manufacture"];
                
                [dict setValue:strInstallationDate forKey:@"installationDate"];
                
                [dict setValue:strWarranty forKey:@"warrantyDate"];
                
                [dict setValue:dictVendor forKey:@"dictVendor"];
                
                [dict setValue:dictVendorLocation forKey:@"dictVendorLocation"];
                
                [dict setValue:dictCategory_AddPart forKey:@"dictCategory"];
                
                [dict setValue:strMultiplierGlobal forKey:@"multiplier"];
                
                [dict setValue:strIsChangeStdPartPrice forKey:@"IsChangeStdPartPrice"];
                
                [arrayPart replaceObjectAtIndex:cellTag withObject:dict];
                
            }
            
            else
            {
                
                [dict setValue:_textFieldPartName_AddPart.text forKey:@"dictPart"];
                
                [dict setValue:_textFieldQuantity_AddPart.text forKey:@"quantity"];
                
                [dict setValue:_textFieldTotalPrice_AddPart.text forKey:@"totalPrice"];
                
                [dict setValue:strDescription forKey:@"description"];
                
                [dict setValue:strIsStd forKey:@"isStandard"];
                
                [dict setValue:warranty forKey:@"isWarranty"];
                
                [dict setValue:strUnitPrice forKey:@"unitPrice"];
                
                [dict setValue:strSerialNo forKey:@"serialNumber"];
                
                [dict setValue:strModalNo forKey:@"modelNumber"];
                
                [dict setValue:strManufacture forKey:@"manufacture"];
                
                [dict setValue:strInstallationDate forKey:@"installationDate"];
                
                [dict setValue:strWarranty forKey:@"warrantyDate"];
                
                [dict setValue:dictVendor forKey:@"dictVendor"];
                
                [dict setValue:dictVendorLocation forKey:@"dictVendorLocation"];
                
                [dict setValue:dictCategory_AddPart forKey:@"dictCategory"];
                
                [dict setValue:strMultiplierGlobal forKey:@"multiplier"];
                
                [dict setValue:strIsChangeStdPartPrice forKey:@"IsChangeStdPartPrice"];
                
                [arrayPart replaceObjectAtIndex:cellTag withObject:dict];
                
            }
        }
        
    }
    
    else
    {
        if(isStandard)
        {
            if([_buttonSelectPart_AddPart.titleLabel.text isEqualToString:@"--Select Part--"])
            {
                [global AlertMethod:Alert :@"Please select part"];
                return;
            }
        }
        else
        {
            if(!(_textFieldPartName_AddPart.text.length>0))
            {
                [global AlertMethod:Alert :@"Please enter part name"];
                return;
            }
        }
        
        if(!(_textFieldQuantity_AddPart.text.length>0))
        {
            [global AlertMethod:Alert :@"Please enter quantity"];
            return;
        }
        else
        {
            
            NSString *strDescription = @"";
            
            NSString *warranty = @"no";
            
            NSString *strUnitPrice = @"";
            
            NSString *strSerialNo = @"";
            
            NSString *strModalNo = @"";
            
            NSString *strManufacture = @"";
            
            NSString *strInstallationDate = @"";
            
            NSString *strWarranty = @"";
            
            NSString *strIsStd = @"";
            
            
            if(isWarranty)
            {
                warranty = @"yes";
            }
            
            
            if(_textViewDescription_AddPart.text.length>0)
            {
                strDescription = _textViewDescription_AddPart.text;
            }
            
            
            if(_textFieldUnitPrice_AddPart.text.length>0)
            {
                //strUnitPrice = _textFieldUnitPrice_AddPart.text;
                if(isStandard)
                {
                    strUnitPrice = [NSString stringWithFormat:@"%.2f",[_textFieldUnitPrice_AddPart.text floatValue]/[strMultiplierGlobal floatValue]];
                }
                else
                {
                    strUnitPrice = _textFieldUnitPrice_AddPart.text;
                }
            }
            
            if(_textFieldSerialNumber_AddPart.text.length>0)
            {
                strSerialNo = _textFieldSerialNumber_AddPart.text;
            }
            
            if(_textFieldModelNumber_AddPart.text.length>0)
            {
                strModalNo = _textFieldModelNumber_AddPart.text;
            }
            
            if(_textFieldManufacturer_AddPart.text.length>0)
            {
                strManufacture = _textFieldManufacturer_AddPart.text;
            }
            
            if(!([_buttonInstallationDate_AddPart.titleLabel.text isEqualToString:@"--SelectDate--"]))
            {
                strInstallationDate = _buttonInstallationDate_AddPart.titleLabel.text;
            }
            
            if(!([_buttonWarrantyExpireDate_AddPart.titleLabel.text isEqualToString:@"--SelectDate--"]))
            {
                strWarranty = _buttonWarrantyExpireDate_AddPart.titleLabel.text;
            }
            
            // instalation date must be less than expiration date
            if(strInstallationDate.length>0 && strWarranty.length>0)
            {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"MM/dd/yyyy"];
                NSDate *installationDate = [dateFormat dateFromString:strInstallationDate];
                NSDate *expirationDate = [dateFormat dateFromString:strWarranty];
                
                NSComparisonResult result = [installationDate compare:expirationDate];
                if(result == NSOrderedSame || result == NSOrderedDescending)
                {
                    [global AlertMethod:Alert :@"Warrenty expiration date must be greater than Installation date"];
                    return;
                }
            }
            
            
            if(isStandard)
            {
                strIsStd = @"yes";
            }
            
            else
            {
                strIsStd = @"no";
            }
            
            if(dictVendor==nil)
            {
                dictVendor = [[NSDictionary alloc]init];
            }
            
            if(dictVendorLocation==nil)
            {
                dictVendorLocation = [[NSDictionary alloc]init];
            }
            
            if(dictCategory_AddPart==nil)
            {
                dictCategory_AddPart = [[NSDictionary alloc]init];
            }
            
            if(strMultiplierGlobal==nil)
            {
                strMultiplierGlobal = @"";
            }
            
            if(isStandard)
            {
                
                NSDictionary *dict = @{@"dictPart":dictDataPartsSelected,
                                       
                                       @"quantity":_textFieldQuantity_AddPart.text,
                                       
                                       @"totalPrice":_textFieldTotalPrice_AddPart.text,
                                       
                                       @"description":strDescription,
                                       
                                       @"isStandard":strIsStd,
                                       
                                       @"isWarranty":warranty,
                                       
                                       @"unitPrice":strUnitPrice,
                                       
                                       @"serialNumber":strSerialNo,
                                       
                                       @"modelNumber":strModalNo,
                                       
                                       @"manufacture":strManufacture,
                                       
                                       @"installationDate":strInstallationDate,
                                       
                                       @"warrantyDate":strWarranty,
                                       
                                       @"dictVendor":dictVendor,
                                       
                                       @"dictVendorLocation":dictVendorLocation,
                                       
                                       @"dictCategory":dictCategory_AddPart,
                                       
                                       @"multiplier":strMultiplierGlobal,
                                       
                                       @"IsChangeStdPartPrice":strIsChangeStdPartPrice
                                       
                                       };
                
                [arrayPart addObject:dict];
            }
            else
            {
                NSDictionary *dict = @{@"dictPart":_textFieldPartName_AddPart.text,
                                       
                                       @"quantity":_textFieldQuantity_AddPart.text,
                                       
                                       @"totalPrice":_textFieldTotalPrice_AddPart.text,
                                       
                                       @"description":strDescription,
                                       
                                       @"isStandard":strIsStd,
                                       
                                       @"isWarranty":warranty,
                                       
                                       @"unitPrice":strUnitPrice,
                                       
                                       @"serialNumber":strSerialNo,
                                       
                                       @"modelNumber":strModalNo,
                                       
                                       @"manufacture":strManufacture,
                                       
                                       @"installationDate":strInstallationDate,
                                       
                                       @"warrantyDate":strWarranty,
                                       
                                       @"dictVendor":dictVendor,
                                       
                                       @"dictVendorLocation":dictVendorLocation,
                                       
                                       @"dictCategory":dictCategory_AddPart,
                                       
                                       @"multiplier":strMultiplierGlobal,
                                       
                                       @"IsChangeStdPartPrice":strIsChangeStdPartPrice
                                       };
                
                [arrayPart addObject:dict];
            }
        }
    }
    
    if(arrayPart.count>0)
    {
        [_tableViewPart reloadData];
    }
    
    isAddPart = NO;
    strIsChangeStdPartPrice = @"false";
    
    [self hideTextFields];
    
    [viewBackGround removeFromSuperview];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        _hghtMainContainerView.constant = (_hghtMainContainerView.constant-_hghtTableViewPart.constant);
        
        _hghtTableViewPart.constant = arrayPart.count*230;
        
        //_hghtTableViewLabour.constant = arrayLabour.count*230;
        
        [_tableViewPart updateConstraintsIfNeeded];
        
        [_tableViewLabour updateConstraintsIfNeeded];
        
        _hghtMainContainerView.constant = (_hghtMainContainerView.constant+_hghtTableViewPart.constant);
        
        
        
        // _hghtMainContainerView.constant = _hghtMainContainerView.constant-_hghtViewAddDetails.constant;
        
        
        
        [_viewMainContainer updateConstraintsIfNeeded];
        
        
        
        //_hghtViewAddDetails.constant = 0.0;
        
        // [_viewAddDetails updateConstraintsIfNeeded];
        
    });
    
    [self showPartsAndLaborAmount];
    
}

- (IBAction)actionOnClearPart:(id)sender
{
    isAddPart = YES;
    isAddLabor = NO;
    strCategoryId=@"";
    strCategorySysNameSelected=@"";
    [_buttonCategory_AddPart setTitle:@"--Select Category--" forState:UIControlStateNormal];
    [_buttonSelectPart_AddPart setTitle:@"--Select Part--" forState:UIControlStateNormal];
    [_buttonInstallationDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
    [_buttonWarrantyExpireDate_AddPart setTitle:@"--Select Date--" forState:UIControlStateNormal];
    [_buttonVendor_AddPart setTitle:@"--Select Vendor--" forState:UIControlStateNormal];
    [_buttonVendorLocation_AddPart setTitle:@"--Select Vendor Location--" forState:UIControlStateNormal];
    
    isStandard=YES;
    [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_buttonNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    isWarranty=NO;
    isEditParts=NO;
    [_buttonWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    _textViewDescription_AddPart.text=@"";
    _textFieldQuantity_AddPart.text=@"";
    _textFieldModelNumber_AddPart.text=@"";
    _textFieldSerialNumber_AddPart.text=@"";
    _textFieldUnitPrice_AddPart.text=@"";
    _textFieldTotalPrice_AddPart.text=@"";
    _textFieldManufacturer_AddPart.text=@"";
    _textFieldPartName_AddPart.text=@"";
    _lblPartSelectOrEnter.text=@"Part";
    
    [_textFieldPartName_AddPart setHidden:NO];
    [_buttonSelectPart_AddPart setHidden:YES];
    [_textFieldUnitPrice_AddPart setEnabled:YES];
    
}
- (IBAction)actionOnCancelPart:(id)sender
{
    isAddPart = NO;
    [self hideTextFields];
    [viewBackGround removeFromSuperview];
}
- (IBAction)actionOnVendor_AddPart:(id)sender
{
    if([_buttonSelectPart_AddPart.titleLabel.text isEqualToString:@"--Select Part--"])
    {
        [global AlertMethod:Alert :@"Please select part"];
    }
    else
    {
        NSMutableArray *arrayTempVendor = [NSMutableArray new];
        
        for(NSDictionary *dict in arrayItemVendorMapping)
        {
            if([[dict valueForKey:@"ItemMasterId"] integerValue]==itemMasterID)
            {
                for(NSDictionary *dictTempVendor in arrayVendor)
                {
                    if ([[dict valueForKey:@"VendorMasterId"] integerValue]==[[dictTempVendor valueForKey:@"VendorMasterId"] integerValue])
                    {
                        [arrayTempVendor addObject:dictTempVendor];
                    }
                }
            }
        }
        
        if(arrayTempVendor.count>0)
        {
            arrDataTblView = [arrayTempVendor mutableCopy];
            tblData.tag=303;
            [self setTableFrame];
            [tblData reloadData];
        }
        else
        {
            [global AlertMethod:Info :NoDataAvailable];
        }
    }
}
- (IBAction)actionOnIsWarranty:(id)sender
{
    [self hideTextFields];
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_buttonWarranty imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        isWarranty=YES;
        [_buttonWarranty setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
    }
    else
    {
        isWarranty=NO;
        [_buttonWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    }
    
    
    
}

- (IBAction)actionOnVendorLocation_AddPart:(id)sender
{
    if([_buttonVendor_AddPart.titleLabel.text isEqualToString:@"--Select Vendor--"])
    {
        [global AlertMethod:Alert :@"Please select vendor"];
    }
    else
    {
        NSInteger vendorMasterID = [[dictVendor valueForKey:@"VendorMasterId"] integerValue];
        
        NSMutableArray *arrayTempVendorLoction = [NSMutableArray new];
        for(NSDictionary *dict in arrayVendorLocation)
        {
            if([[dict valueForKey:@"VendorId"] integerValue]==vendorMasterID)
            {
                [arrayTempVendorLoction addObject:dict];
            }
        }
        if(arrayTempVendorLoction.count>0)
        {
            arrDataTblView = [arrayTempVendorLoction mutableCopy];
            tblData.tag=304;
            [self setTableFrame];
            [tblData reloadData];
        }
        else
        {
            [global AlertMethod:Info :NoDataAvailable];
        }
    }
}
- (IBAction)actionOnWarrantyDate_AddPart:(id)sender
{
    isInstallationDate = NO;
    isWarrantyDate = YES;
    [self hideTextFields];
    [self addPickerViewDateTo:@""];
}

-(void)actionOnDeletePart:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    cellTag = btn.tag;
    isPart = YES;
    [[[UIAlertView alloc] initWithTitle:@"Message" message:@"Are you sure you want to delete?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
}

-(void)actionOnEditPart:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    cellTag = btn.tag;
    isPart = YES;
    isEditPart = YES;
    NSDictionary *dict = [arrayPart objectAtIndex:cellTag];
    if([[dict valueForKey:@"dictCategory"] count]>0)
    {
        [_buttonCategory_AddPart setTitle:[[dict valueForKey:@"dictCategory"] valueForKey:@"Name"] forState:UIControlStateNormal];
    }
    else
    {
        [_buttonCategory_AddPart setTitle:@"--Select Category--" forState:UIControlStateNormal];
    }
    if([[dict valueForKey:@"isStandard"] isEqualToString:@"yes"])
    {
        if([[dict valueForKey:@"dictPart"] count]>0)
        {
            [_buttonSelectPart_AddPart setTitle:[[dict valueForKey:@"dictPart"] valueForKey:@"Name"] forState:UIControlStateNormal];
        }
        else
        {
            [_buttonSelectPart_AddPart setTitle:@"--Select Part--" forState:UIControlStateNormal];
        }
    }
    else
    {
        if([[dict valueForKey:@"dictPart"] length]>0)
        {
            
            _textFieldPartName_AddPart.text = [dict valueForKey:@"dictPart"];
            
        }
        else
        {
            _textFieldPartName_AddPart.text = @"";
        }
        
    }
    if([[dict valueForKey:@"description"] length]>0)
    {
        _textViewDescription_AddPart.text = [dict valueForKey:@"description"];
    }
    else
    {
        _textViewDescription_AddPart.text = @"";
    }
    //  [_buttonCategory_AddPart setTitle:[[dict valueForKey:@"dictPart"] valueForKey:@"Name"] forState:UIControlStateNormal];
    _textFieldQuantity_AddPart.text = [dict valueForKey:@"quantity"];
    _textFieldUnitPrice_AddPart.text = [dict valueForKey:@"unitPrice"];
    _textFieldTotalPrice_AddPart.text = [dict valueForKey:@"totalPrice"];
    
    _textFieldSerialNumber_AddPart.text = [dict valueForKey:@"serialNumber"];
    _textFieldModelNumber_AddPart.text =  [dict valueForKey:@"modelNumber"];
    _textFieldManufacturer_AddPart.text = [dict valueForKey:@"manufacture"];
    
    if([[dict valueForKey:@"installationDate"] length]>0)
    {
        [_buttonInstallationDate_AddPart setTitle:[dict valueForKey:@"installationDate"] forState:UIControlStateNormal];
    }
    
    if([[dict valueForKey:@"warrantyDate"] length]>0)
    {
        [_buttonWarrantyExpireDate_AddPart setTitle:[dict valueForKey:@"warrantyDate"] forState:UIControlStateNormal];
    }
    
    if([[dict valueForKey:@"dictVendor"] count]>0)
    {
        [_buttonVendor_AddPart setTitle:[[dict valueForKey:@"dictVendor"] valueForKey:@"Name"] forState:UIControlStateNormal];
    }
    else
    {
        [_buttonVendor_AddPart setTitle:@"--Select Vendor--" forState:UIControlStateNormal];
    }
    
    if([[dict valueForKey:@"dictVendorLocation"] count]>0)
    {
        [_buttonVendorLocation_AddPart setTitle:[[dict valueForKey:@"dictVendorLocation"] valueForKey:@"Name"] forState:UIControlStateNormal];
    }
    else
    {
        [_buttonVendorLocation_AddPart setTitle:@"--Select Vendor Location--" forState:UIControlStateNormal];
    }
    
    if([[dict valueForKey:@"isStandard"] isEqualToString:@"yes"])
    {
        [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    else
    {
        [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_buttonNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    }
    
    
    if([[dict valueForKey:@"isWarranty"] isEqualToString:@"yes"])
    {
        [_buttonWarranty setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
    }
    else
    {
        [_buttonWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    }
    [self hideTextFields];
    [self addPartView];
    
    //    [[[UIAlertView alloc] initWithTitle:@"Message" message:@"Are you sure you want to delete part?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
}

-(void)actionOnDeleteLabor:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    cellTag = btn.tag;
    isPart = NO;
    [[[UIAlertView alloc] initWithTitle:@"Message" message:@"Are you sure you want to delete?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
    
}

-(void)actionOnEditLabor:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    cellTag = btn.tag;
    isPart = NO;
    isEditPart = NO;
    isEditLabor = YES;
    isAddLabor = YES;
    NSDictionary *dict = [arrayLabour objectAtIndex:cellTag];
    
    if([[dict valueForKey:@"isLaberType"] isEqualToString:@"yes"])
    {
        [_buttonHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_buttonLabor setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        isLaborType=YES;
    }
    else
    {
        [_buttonHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonLabor setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        isLaborType=NO;
    }
    
    if([[dict valueForKey:@"strIsWarranty"] isEqualToString:@"yes"])
    {
        [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
    }
    else
    {
        [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    }
    
    if(!([[dict valueForKey:@"time"] isEqualToString:@"Enter Time(HH:MM)"]))
    {
        _textFieldHrs_AddLabor.text = [dict valueForKey:@"time"];
        strDate = [dict valueForKey:@"time"];
    }
    else
    {
        
        _textFieldHrs_AddLabor.placeholder=@"Enter Time(HH:mm)";
        _textFieldHrs_AddLabor.text=@"";
    }
    /*
     if(!([[dict valueForKey:@"time"] isEqualToString:@"Select Time(HH:MM)"]))
     {
     [_buttonSelectTime_AddLabor setTitle:[dict valueForKey:@"time"] forState:UIControlStateNormal];
     strDate = [dict valueForKey:@"time"];
     }
     else
     {
     [_buttonSelectTime_AddLabor setTitle:@"Select Time(HH:MM)" forState:UIControlStateNormal];
     _textFieldHrs_AddLabor.placeholder=@"Select Time(HH:mm)";
     _textFieldHrs_AddLabor.text=@"";
     }*/
    
    if([[dict valueForKey:@"amount"] isEqualToString:@"0.00"])
    {
        _textFieldAmount_AddLabor.text=@"0.00";
    }
    else
    {
        _textFieldAmount_AddLabor.text = [dict valueForKey:@"amount"];
    }
    
    if([[dict valueForKey:@"description"] length]>0)
    {
        _textViewDescription_AddLabor.text = [dict valueForKey:@"description"];
    }
    else
    {
        _textViewDescription_AddLabor.text = @"";
    }
    
    [self addLaborView];
    
}


- (IBAction)actionOnManufacturer:(id)sender {
    [self.view endEditing:YES];
    _labelSearchHeader.text=@"Manufacturer";
    _txtFieldSearch.placeholder=@"Enter Manufacturer";
    _viewTextFieldSearch.frame=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
    [self.view addSubview:_viewTextFieldSearch];
    isSerialNo=NO;
    isModelNo=NO;
    isManufacturer=YES;
    
    arrOfManufacturerFiltered=[[NSMutableArray alloc]init];
    
    [arrOfManufacturerFiltered addObjectsFromArray:arrayManufacturer];
    
    
    //arrDataTblView=[[NSMutableArray alloc]init];
    //[arrDataTblView addObject:@"sdfasdf"];
    _txtFieldSearch.text=_textFieldManufacturer.text;
    tblData.tag=1874919426;
    _tableViewSearch.tag=203;
    [_tableViewSearch reloadData];
    //[textField resignFirstResponder];
}

- (IBAction)actionOnSerialNumber:(id)sender {
    
    [self.view endEditing:YES];
    
    _labelSearchHeader.text=@"Serial #";
    _txtFieldSearch.placeholder=@"Enter Serial #";
    _viewTextFieldSearch.frame=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
    [self.view addSubview:_viewTextFieldSearch];
    isSerialNo=YES;
    isModelNo=NO;
    isManufacturer=NO;
    arrOfSerialNosFiltered=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arraySerialNumber.count; k++)
    {
        NSDictionary *dictDataa=arraySerialNumber[k];
        
        if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]])
        {
            [arrOfSerialNosFiltered addObject:dictDataa];
        }
    }
    
    //        arrDataTblView=[[NSMutableArray alloc]init];
    //        [arrDataTblView addObject:@"sdfasdf"];
    _txtFieldSearch.text=_textFieldSerialNumber.text;
    tblData.tag=1874919424;
    _tableViewSearch.tag=201;
    [_tableViewSearch reloadData];
    //    [textField resignFirstResponder];
    
}
- (IBAction)actionOnModelNumber:(id)sender {
    
    [self.view endEditing:YES];
    
    _labelSearchHeader.text=@"Model #";
    _txtFieldSearch.placeholder=@"Enter Model #";
    _viewTextFieldSearch.frame=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
    [self.view addSubview:_viewTextFieldSearch];
    isSerialNo=NO;
    isModelNo=YES;
    isManufacturer=NO;
    
    arrOfModelNosFiltered=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrayModalNumber.count; k++)
    {
        NSDictionary *dictDataa=arrayModalNumber[k];
        
        if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]])
        {
            [arrOfModelNosFiltered addObject:dictDataa];
        }
    }
    
    
    
    //        arrDataTblView=[[NSMutableArray alloc]init];
    //        [arrDataTblView addObject:@"sdfasdf"];
    _txtFieldSearch.text=_textFieldModalNumber.text;
    tblData.tag=1874919425;
    _tableViewSearch.tag=202;
    [_tableViewSearch reloadData];
    // [textField resignFirstResponder];
    
}
- (IBAction)actionOnInstallationDate:(id)sender {
    
    [self.view endEditing:YES];
    
    isInstallationDate = YES;
    isWarrantyDate = NO;
    [self addingDatePicker:@"" :0];
}
- (IBAction)actionOnWarrantyExpirationDate:(id)sender {
    
    [self.view endEditing:YES];
    
    isInstallationDate = NO;
    isWarrantyDate = YES;
    [self addingDatePicker:@"" :0];
    
}
#pragma mark - UIAlertView delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        if(isPart)
        {
            [arrayPart removeObjectAtIndex:cellTag];
            
            [_tableViewPart reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _hghtMainContainerView.constant = (_hghtMainContainerView.constant-_hghtTableViewPart.constant);
                
                _hghtTableViewPart.constant = arrayPart.count*230;
                // _hghtTableViewLabour.constant = arrayLabour.count*230;
                [_tableViewPart updateConstraintsIfNeeded];
                [_tableViewLabour updateConstraintsIfNeeded];
                _hghtMainContainerView.constant = (_hghtMainContainerView.constant+_hghtTableViewPart.constant);
                
                [_viewMainContainer updateConstraintsIfNeeded];
            });
            [self showPartsAndLaborAmount];
            
        }
        else
        {
            [arrayLabour removeObjectAtIndex:cellTag];
            
            [_tableViewLabour reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _hghtMainContainerView.constant = _hghtMainContainerView.constant-_hghtTableViewLabour.constant;
                // _hghtTableViewPart.constant = arrayPart.count*230;
                _hghtTableViewLabour.constant = arrayLabour.count*230;
                [_tableViewPart updateConstraintsIfNeeded];
                [_tableViewLabour updateConstraintsIfNeeded];
                _hghtMainContainerView.constant = (_hghtMainContainerView.constant+_hghtTableViewLabour.constant);
                
                [_viewMainContainer updateConstraintsIfNeeded];
            });
            [self showPartsAndLaborAmount];
        }
    }
}

#pragma mark - UIButton action add labor

- (IBAction)actionOnIsWarranty_AddLabor:(id)sender
{
    [self hideTextFields];
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    UIImage *img = [_buttonIsWarranty_AddLabor imageForState:UIControlStateNormal];
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    NSData *imgData2 = UIImagePNGRepresentation(img);
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        isWarranty_AddLabor=YES;
        [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
    }
    else
    {
        isWarranty_AddLabor=NO;
        [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)actionOnSelectTime_AddLabor:(id)sender
{
    [self addPickerViewDateTo];
}

- (IBAction)actionOnSaveLabor:(id)sender
{
    //  if([_buttonSelectTime_AddLabor.titleLabel.text isEqualToString:@"Select Time(HH:MM)"])
    if(_textFieldHrs_AddLabor.text.length == 0)
    {
        [global AlertMethod:Alert :@"Please enter time"];
        return;
    }
    else
    {
        if(isEditLabor)
        {
            NSString *strIsWarranty = @"no";
            NSString *strIsLaberType = @"";
            NSString *strDescription = @"";
            if(isLaborType)
            {
                strIsLaberType = @"yes";
            }
            else
            {
                strIsLaberType = @"no";
            }
            if(isWarranty_AddLabor)
            {
                strIsWarranty = @"yes";
            }
            //            if(!isLaborType)
            //            {
            //                strIsLaberType = @"no";
            //            }
            if(_textViewDescription_AddLabor.text.length>0)
            {
                strDescription = _textViewDescription_AddLabor.text;
            }
            
            NSMutableDictionary *dict = [[arrayLabour objectAtIndex:cellTag] mutableCopy];
            [dict setValue:strIsLaberType forKey:@"isLaberType"];
            [dict setValue:strIsWarranty forKey:@"isWarranty"];
            // [dict setValue:_buttonSelectTime_AddLabor.titleLabel.text forKey:@"time"];
            //
            [dict setValue:_textFieldHrs_AddLabor.text forKey:@"time"];
            
            [dict setValue:_textFieldAmount_AddLabor.text forKey:@"amount"];
            [dict setValue:strDescription forKey:@"description"];
            [arrayLabour replaceObjectAtIndex:cellTag withObject:dict];
            
            if(arrayLabour.count==2)
            {
                if([[[arrayLabour objectAtIndex:0] valueForKey:@"isLaberType"] isEqualToString:[[arrayLabour objectAtIndex:1] valueForKey:@"isLaberType"]])
                {
                    NSString *strTime = [[arrayLabour objectAtIndex:0] valueForKey:@"time"];
                    NSArray *components1 = [strTime componentsSeparatedByString:@":"];
                    
                    NSInteger hours1   = [[components1 objectAtIndex:0] integerValue];
                    NSInteger minutes1 = [[components1 objectAtIndex:1] integerValue];
                    
                    NSNumber *time1 = [NSNumber numberWithInteger:(hours1 * 60 * 60) + (minutes1 * 60)];
                    
                    NSArray *components2 = [[[arrayLabour objectAtIndex:1] valueForKey:@"time"] componentsSeparatedByString:@":"];
                    
                    NSInteger hours2   = [[components2 objectAtIndex:0] integerValue];
                    
                    NSInteger minutes2 = [[components2 objectAtIndex:1] integerValue];
                    
                    NSNumber *time2 = [NSNumber numberWithInteger:(hours2 * 60 * 60) + (minutes2 * 60)];
                    
                    NSString *str1 = [time1 stringValue];
                    NSString *str2 = [time2 stringValue];
                    float totalTime = [str1 floatValue]+[str2 floatValue];
                    NSString *str3 = [NSString stringWithFormat:@"%f",totalTime];
                    
                    int hours = [str3 intValue] / 3600;
                    int minutes = ([str3 intValue] / 60) % 60;
                    
                    NSString *strTotalHR = [NSString stringWithFormat:@"%02d:%02d",hours, minutes];
                    
                    NSMutableDictionary *dict1 = [[arrayLabour objectAtIndex:0] mutableCopy];
                    [dict1 setValue:strTotalHR forKey:@"time"];
                    /************************************************/
                    
                    NSNumber *amount1 = [NSNumber numberWithFloat:[[[arrayLabour objectAtIndex:0] valueForKey:@"amount"] floatValue]];
                    NSNumber *amount2 = [NSNumber numberWithFloat:[[[arrayLabour objectAtIndex:1] valueForKey:@"amount"] floatValue]];
                    
                    float totalAmount = [amount1 floatValue]+[amount2 floatValue];
                    
                    [dict1 setValue:[NSString stringWithFormat:@"%.2f",totalAmount] forKey:@"amount"];
                    
                    [arrayLabour replaceObjectAtIndex:0 withObject:dict1];
                    [arrayLabour removeObjectAtIndex:1];
                }
            }
            
        }
        else
        {
            NSString *strIsWarranty = @"no";
            NSString *strIsLaberType = @"yes";
            NSString *strDescription = @"";
            
            if(isWarranty_AddLabor)
            {
                strIsWarranty = @"yes";
            }
            if(!isLaborType)
            {
                strIsLaberType = @"no";
            }
            if(_textViewDescription_AddLabor.text.length>0)
            {
                strDescription = _textViewDescription_AddLabor.text;
            }
            
            if(arrayLabour.count>0)
            {
                BOOL isReplacedObject = NO;
                for(int i=0;i<arrayLabour.count;i++)
                {
                    NSMutableDictionary *dict = [[arrayLabour objectAtIndex:i] mutableCopy];
                    if([[dict valueForKey:@"isLaberType"] isEqualToString:strIsLaberType])
                    {
                        
                        /******************************************************/
                        NSString *strTime = [dict valueForKey:@"time"];
                        NSArray *components1 = [strTime componentsSeparatedByString:@":"];
                        
                        NSInteger hours1   = [[components1 objectAtIndex:0] integerValue];
                        NSInteger minutes1 = [[components1 objectAtIndex:1] integerValue];
                        
                        NSNumber *time1 = [NSNumber numberWithInteger:(hours1 * 60 * 60) + (minutes1 * 60)];
                        
                        
                        // NSArray *components2 = [_buttonSelectTime_AddLabor.titleLabel.text componentsSeparatedByString:@":"];
                        
                        NSArray *components2 = [_textFieldHrs_AddLabor.text componentsSeparatedByString:@":"];
                        
                        NSInteger hours2   = [[components2 objectAtIndex:0] integerValue];
                        
                        NSInteger minutes2 = [[components2 objectAtIndex:1] integerValue];
                        
                        NSNumber *time2 = [NSNumber numberWithInteger:(hours2 * 60 * 60) + (minutes2 * 60)];
                        
                        
                        NSString *str1 = [time1 stringValue];
                        NSString *str2 = [time2 stringValue];
                        float totalTime = [str1 floatValue]+[str2 floatValue];
                        NSString *str3 = [NSString stringWithFormat:@"%f",totalTime];
                        
                        int hours = [str3 intValue] / 3600;
                        int minutes = ([str3 intValue] / 60) % 60;
                        
                        NSString *strTotalHR = [NSString stringWithFormat:@"%02d:%02d",hours, minutes];
                        
                        [dict setValue:strTotalHR forKey:@"time"];
                        /************************************************/
                        
                        NSNumber *amount1 = [NSNumber numberWithFloat:[[dict valueForKey:@"amount"] floatValue]];
                        NSNumber *amount2 = [NSNumber numberWithFloat:[_textFieldAmount_AddLabor.text floatValue]];
                        
                        float totalAmount = [amount1 floatValue]+[amount2 floatValue];
                        
                        [dict setValue:[NSString stringWithFormat:@"%.2f",totalAmount] forKey:@"amount"];
                        
                        [arrayLabour replaceObjectAtIndex:i withObject:dict];
                        isReplacedObject = YES;
                        break;
                    }
                }
                if(isReplacedObject==NO)
                {
                    NSDictionary *dict = @{  @"isLaberType":strIsLaberType,
                                             @"isWarranty":strIsWarranty,
                                             @"time":_textFieldHrs_AddLabor.text,
                                             @"amount":_textFieldAmount_AddLabor.text,
                                             @"description":strDescription
                                             };
                    
                    [arrayLabour addObject:dict];
                }
            }
            else
            {
                NSDictionary *dict = @{  @"isLaberType":strIsLaberType,
                                         @"isWarranty":strIsWarranty,
                                         @"time":_textFieldHrs_AddLabor.text,
                                         @"amount":_textFieldAmount_AddLabor.text,
                                         @"description":strDescription
                                         };
                
                [arrayLabour addObject:dict];
            }
        }
    }
    
    if(arrayLabour.count>0)
    {
        [_tableViewLabour reloadData];
    }
    isAddLabor = NO;
    [self hideTextFields];
    [viewBackGround removeFromSuperview];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        _hghtMainContainerView.constant = (_hghtMainContainerView.constant-_hghtTableViewLabour.constant);
        // _hghtTableViewPart.constant = arrayPart.count*230;
        
        if(isShowPriceOnQuote==NO)
        {
            _hghtTableViewLabour.constant = arrayLabour.count*230-50;
        }
        else
        {
            _hghtTableViewLabour.constant = arrayLabour.count*230;
        }
        [_tableViewPart updateConstraintsIfNeeded];
        [_tableViewLabour updateConstraintsIfNeeded];
        _hghtMainContainerView.constant = (_hghtMainContainerView.constant+_hghtTableViewLabour.constant);
        
        // _hghtMainContainerView.constant = _hghtMainContainerView.constant-_hghtViewAddDetails.constant;
        
        [_viewMainContainer updateConstraintsIfNeeded];
        
        //_hghtViewAddDetails.constant = 0.0;
        // [_viewAddDetails updateConstraintsIfNeeded];
    });
    
    [self showPartsAndLaborAmount];
    
}
- (IBAction)actionOnClear_AddLabor:(id)sender
{
    isLaborType=YES;
    _textFieldAmount_AddLabor.text = @"";
    _textFieldHrs_AddLabor.placeholder=@"Enter Time(HH:mm)";
    [_textFieldAmount_AddLabor setEnabled:NO];
    _textViewDescription_AddLabor.text=@"";
    //_textFieldAmount_AddLabor.text=@"0.00";
    _textFieldHrs_AddLabor.text=@"";
    strDate=@"";
    // [_buttonSelectTime_AddLabor setTitle:@"Select Time(HH:MM)" forState:UIControlStateNormal];
    
    [_buttonLabor setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_buttonHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    isWarranty=NO;
    [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    
}
- (IBAction)actionOnCancel_AddLabor:(id)sender
{
    [self hideTextFields];
    [viewBackGround removeFromSuperview];
}
- (IBAction)actionOnLabor:(id)sender
{
    if(!isLaborType)
    {
        if(!isEditLabor)
        {
            [_buttonLabor setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            [_buttonHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            isLaborType = YES;
            isEditLabor=NO;
            isAddLabor = YES;
            _textFieldHrs_AddLabor.placeholder=@"Enter Time(HH:mm)";
            [_textFieldAmount_AddLabor setEnabled:NO];
            _textViewDescription_AddLabor.text=@"";
            _textFieldHrs_AddLabor.text=@"";
            strDate=@"";
            //            [_buttonSelectTime_AddLabor setTitle:@"Select Time(HH:MM)" forState:UIControlStateNormal];
            //            _textFieldAmount_AddLabor.text=@"0.00";
            isWarranty=NO;
            [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        }
        else
        {
            [_buttonLabor setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            [_buttonHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            isLaborType = YES;
            // isEditLabor=NO;
            //            isAddLabor = YES;
            //            _textFieldHrs_AddLabor.placeholder=@"Enter Time(HH:mm)";
            [_textFieldAmount_AddLabor setEnabled:NO];
            //            _textViewDescription_AddLabor.text=@"";
            //            _textFieldHrs_AddLabor.text=@"";
            //            strDate=@"";
            //            [_buttonSelectTime_AddLabor setTitle:@"Enter Time(HH:MM)" forState:UIControlStateNormal];
            //            _textFieldAmount_AddLabor.text=@"0.00";
            //            isWarranty=NO;
            [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            
            [self methodToCalculateLaborPrice:[NSString stringWithFormat:@"%@",strDate]];
            
            
        }
    }
}

- (IBAction)actionOnHelper:(id)sender
{
    if(isLaborType)
    {
        if(!isEditLabor)
        {
            [_buttonHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            [_buttonLabor setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            isLaborType = NO;
            
            isEditLabor=NO;
            isAddLabor = YES;
            _textFieldHrs_AddLabor.placeholder=@"Enter Time(HH:mm)";
            [_textFieldAmount_AddLabor setEnabled:NO];
            _textViewDescription_AddLabor.text=@"";
            _textFieldHrs_AddLabor.text=@"";
            strDate=@"";
            //            [_buttonSelectTime_AddLabor setTitle:@"Select Time(HH:MM)" forState:UIControlStateNormal];
            //            _textFieldAmount_AddLabor.text=@"0.00";
            
            isWarranty=NO;
            [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        }
        else
        {
            [_buttonHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            [_buttonLabor setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            isLaborType = NO;
            
            // isEditLabor=NO;
            // isAddLabor = YES;
            //// _textFieldHrs_AddLabor.placeholder=@"Enter Time(HH:mm)";
            [_textFieldAmount_AddLabor setEnabled:NO];
            // _textViewDescription_AddLabor.text=@"";
            //  _textFieldHrs_AddLabor.text=@"";
            //  strDate=@"";
            //  [_buttonSelectTime_AddLabor setTitle:@"Enter Time(HH:MM)" forState:UIControlStateNormal];
            //            _textFieldAmount_AddLabor.text=@"0.00";
            
            // isWarranty=NO;
            [_buttonIsWarranty_AddLabor setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
            [self methodToCalculateLaborPrice:[NSString stringWithFormat:@"%@",strDate]];
            
        }
    }
}

-(void)setTableFrame
{
    buttonBackground=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    buttonBackground.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    
    [buttonBackground addTarget:self action:@selector(actionOnBackground:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: buttonBackground];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    
    tblData.layer.borderWidth = 1.0;
    tblData.layer.borderColor = [[UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0] CGColor];
    
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
    }
    
    [self.view addSubview:tblData];
}
-(void)getServiceAddress
{
    NSString *strUrl = [NSString stringWithFormat:@"%@/api/MobileToServiceAuto/GetCustomerServiceAddress?companyKey=%@&accountNumber=%@",strServiceUrlMainServiceAutomation,[dictWorkOrderDetails valueForKey:@"companyKey"],[dictWorkOrderDetails valueForKey:@"accountNo"]];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
    
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
             [DejalBezelActivityView removeView];
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             
             if ((ResponseDict==nil) || ResponseDict.count==0) {
                 
                 [global AlertMethod:Alert :NoDataAvailableee];
                 
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 // NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 
                 
                 NSString *strException;
                 @try {
                     strException=[dict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                 
                 if (strException.length==0)
                 {
                     
                     NSLog(@"%@",response);
                     arrayServiceAddress=[dict valueForKey:@"response"];
                     
                     if(!(arrayServiceAddress.count>0))
                     {
                         [global AlertMethod:@"Message" :NoDataAvailableee];
                     }
                     else
                     {
                         BOOL isAddressPrimaryFound = NO;
                         for(NSDictionary *dictAddress in  arrayServiceAddress){
                             
                             if([[dictAddress valueForKey:@"IsPrimary"] intValue] == 1 || [[dictAddress valueForKey:@"IsPrimary"] boolValue] == 1)
                             {
                                 isAddressPrimaryFound = YES;
                                 [_buttonServiceAddress setTitle:[dictAddress valueForKey:@"CombinedAddress"] forState:UIControlStateNormal];
                                 addressID = [[dictAddress valueForKey:@"CustomerAddressId"] integerValue];
                                 strServiceAddressID = [NSString stringWithFormat:@"%@",[dictAddress valueForKey:@"CustomerAddressId"]];//AddressTypeId
                                 strServiceAddressName = [dictAddress valueForKey:@"Name"];
                                 break;
                             }
                         }
                         
                         if(isAddressPrimaryFound == NO)
                         {
                             [_buttonServiceAddress setTitle:[[arrayServiceAddress objectAtIndex:0] valueForKey:@"CombinedAddress"] forState:UIControlStateNormal];
                             addressID = [[[arrayServiceAddress objectAtIndex:0] valueForKey:@"CustomerAddressId"] integerValue];
                             strServiceAddressID = [NSString stringWithFormat:@"%@",[[arrayServiceAddress objectAtIndex:0] valueForKey:@"CustomerAddressId"]];//AddressTypeId
                             strServiceAddressName = [[arrayServiceAddress objectAtIndex:0] valueForKey:@"Name"];
                         }
                         
                         
                         
                         [self getLocationContact];
                     }
                     
                 } else {
                     
                     ResponseDict=nil;
                     [global AlertMethod:Alert :NoDataAvailableee];
                 }
             }
         }];
    }
    @catch (NSException *exception) {
        
        [global AlertMethod:Alert :Sorry];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }
}
//-(void)getServiceAddress
//{
//
//    NSString *strUrl = [NSString stringWithFormat:@"%@/api/MobileToServiceAuto/GetCustomerServiceAddress?companyKey=%@&accountNumber=%@",strServiceUrlMainServiceAutomation,[dictWorkOrderDetails valueForKey:@"companyKey"],[dictWorkOrderDetails valueForKey:@"accountNo"]];
//
//   // NSURL *url = [NSURL URLWithString:strUrl];
//    //============================================================================
//    //============================================================================
//
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
//
//        [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
//         {
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 if (success)
//                 {
//                     NSLog(@"%@",response);
//
//                 }
//                 else
//                 {
//                     NSString *strTitle = Alert;
//                     NSString *strMsg = Sorry;
//                     [global AlertMethod:strTitle :strMsg];
//                 }
//             });
//         }];
//    });
//
//    //============================================================================
//    //============================================================================
//}

//-(void)getServiceAddress
//{
//    NSString *strUrl = [NSString stringWithFormat:@"%@/api/MobileToServiceAuto/GetCustomerServiceAddress?companyKey=%@&accountNumber=%@",strServiceUrlMainServiceAutomation,[dictWorkOrderDetails valueForKey:@"companyKey"],[dictWorkOrderDetails valueForKey:@"accountNo"]];
//
//    NSURL *url = [NSURL URLWithString:strUrl];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    [DejalBezelActivityView activityViewForView:self.view withLabel:@"please wait..."];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
//        NSURLSessionDataTask *data = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//            [DejalBezelActivityView removeView];
//            NSError *erro = nil;
//
//            if (data!=nil)
//            {
//                id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
//                NSLog(@"%@",json);
//                arrayServiceAddress = (NSArray*)json;
//            }
//            else
//            {
//                [global AlertMethod:Alert :Sorry];
//            }
//
//        }];
//
//        [data resume];
//    });
//}

-(void)getLocationContact
{
    //strServiceUrlMainServiceAutomation
    //    NSString *strUrl = [NSString stringWithFormat:@"http://tsrs.stagingsoftware.com//api/MobileToServiceAuto/GetAddressPocByAddressId?companyKey=%@&addressId=%ld",[dictWorkOrderDetails valueForKey:@"companyKey"],(long)addressID];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@/api/MobileToServiceAuto/GetAddressPocByAddressId?companyKey=%@&addressId=%ld",strServiceUrlMainServiceAutomation,[dictWorkOrderDetails valueForKey:@"companyKey"],(long)addressID];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"please wait..."];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSURLSessionDataTask *data = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [DejalBezelActivityView removeView];
                
            });
            NSError *erro = nil;
            
            if (data!=nil)
            {
                
                id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
                NSLog(@"%@",json);
                arrayLocationContact = (NSMutableArray*)json;
                if(arrayLocationContact.count>0)
                {
                    if(isFirst)
                    {
                        isFirst = NO;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [_buttonLocationContact setTitle:[[arrayLocationContact objectAtIndex:0] valueForKey:@"ContactName"] forState:UIControlStateNormal];
                        });
                        
                        strCRMContactID = [NSString stringWithFormat:@"%@",[[arrayLocationContact objectAtIndex:0] valueForKey:@"CrmContactId"]];
                    }
                }
            }
            else
            {
                [global AlertMethod:Alert :Sorry];
            }
        }];
        
        [data resume];
    });
    
}

#pragma mark - UITextField delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==_textFieldHrs_AddLabor){
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        else
        {
            NSString *text = textField.text;
            NSInteger length = text.length;
            BOOL shouldReplace = YES;
            
            if (![string isEqualToString:@""])
            {
                switch (length)
                {
                    case 2:
                        textField.text = [text stringByAppendingString:@":"];
                        break;
                        
                    default:
                        break;
                }
                if (length > 4)
                    shouldReplace = NO;
            }
            
            return shouldReplace;
        }
        return YES;
    }
    else if(textField==_textFieldMiles)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_MILES] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if([string isEqualToString:filtered]==YES)
        {
            if([string isEqualToString:@""])
            {
                
                NSString *str1 =_textFieldMiles.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
                
                // _textFieldMiles.text = string;
                
                // _textFieldMiles.text = [_textFieldMiles.text substringToIndex:[_textFieldMiles.text length] - 1];
                
                _labelMileageCharge.text = [NSString stringWithFormat:@"$%.02f",[string floatValue]*chargeAmount];
                [self taxCalculationNewPlan];
                
            }
            else
            {
                string = [_textFieldMiles.text stringByAppendingString:string];
                _labelMileageCharge.text = [NSString stringWithFormat:@"$%.02f",[string floatValue]*chargeAmount];
                [self taxCalculationNewPlan];
                
            }
            return YES;
        }
        else
        {
            return false;
        }
    }
    // 26 march 2019
    else if(textField==_txtFldQty)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS_MILES] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if([string isEqualToString:filtered]==YES)
        {
            if([string isEqualToString:@""])
            {
                
                NSString *str1 =_txtFldQty.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
                
                // _textFieldMiles.text = string;
                
                // _textFieldMiles.text = [_textFieldMiles.text substringToIndex:[_textFieldMiles.text length] - 1];
                
                _labelTripCharge.text = [NSString stringWithFormat:@"$%.02f",[string floatValue]*[strTripChargeAmt floatValue]];
                [self taxCalculationNewPlan];
                
            }
            else
            {
                string = [_txtFldQty.text stringByAppendingString:string];
                _labelTripCharge.text = [NSString stringWithFormat:@"$%.02f",[string floatValue]*[strTripChargeAmt floatValue]];
                [self taxCalculationNewPlan];
                
            }
            return YES;
        }
        else
        {
            return false;
        }
    }
    else if (textField==_textFieldMiscCharge)
    {
        BOOL isNuberOnly=[global isNumbernDecimalOnly:string :range :10 :(int)textField.text.length :@".0123456789"];
        
        if (isNuberOnly)
        {
            if([_textFieldMiscCharge.text containsString:@"."])
            {
                NSString *strMiscCharge=[NSString stringWithFormat:@"%@%@",_textFieldMiscCharge.text,string];
                
                NSArray *sep = [strMiscCharge componentsSeparatedByString:@"."];
                if([sep count] >= 2)
                {
                    NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                    if(!([sepStr length]>2))
                    {
                        [self performSelector:@selector(taxCalculationNewPlan) withObject:nil afterDelay:0.5];
                        return true;
                    }
                    return false;
                }
            }
            else
            {
                [self performSelector:@selector(taxCalculationNewPlan) withObject:nil afterDelay:0.5];
                return true;
                
            }
            
            return isNuberOnly;
        }
        
    }
    if (range.location == 0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    if(textField==_txtFieldSearch)
    {
        if (isSerialNo)
        {
            strSearchType = @"SerialNumber";
            arrOfSerialNosFiltered=[[NSMutableArray alloc]init];
            
            NSString *strTextFld=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            if ([string isEqualToString:@""]) {
                
                if ([strTextFld length] > 0) {
                    strTextFld = [strTextFld substringToIndex:[strTextFld length] - 1];
                } else {
                    //no characters to delete... attempting to do so will result in a crash
                }
                
            }
            for (int k=0; k<arraySerialNumber.count; k++) {
                
                NSDictionary *dictDatas=arraySerialNumber[k];
                
                NSString *strSerialNos=[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"SerialNumber"]];
                
                if ([strSerialNos containsString:strTextFld] && [strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"ItemSysName"]]])
                {
                    [arrOfSerialNosFiltered addObject:dictDatas];
                }
            }
            
            if ([strTextFld isEqualToString:@""] || strTextFld.length==0)
            {
                for (int k=0; k<arraySerialNumber.count; k++)
                {
                    NSDictionary *dictDataa=arraySerialNumber[k];
                    
                    if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]])
                    {
                        [arrOfSerialNosFiltered addObject:dictDataa];
                    }
                }
            }
            
            tblData.tag=1874919424;
            _tableViewSearch.tag=201;
            [_tableViewSearch reloadData];
            
        }
        else if(isModelNo)
        {
            strSearchType = @"ModalNumber";
            
            NSString *strTextFld=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            if ([string isEqualToString:@""])
            {
                if ([strTextFld length] > 0)
                {
                    strTextFld = [strTextFld substringToIndex:[strTextFld length] - 1];
                } else {
                    //no characters to delete... attempting to do so will result in a crash
                }
            }
            for (int k=0; k<arrayModalNumber.count; k++)
            {
                NSDictionary *dictDatas=arrayModalNumber[k];
                
                NSString *strSerialNos=[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"ModelNumber"]];
                
                if ([strSerialNos containsString:strTextFld] && [strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"ItemSysName"]]])
                {
                    [arrOfModelNosFiltered addObject:dictDatas];
                }
            }
            
            if ([strTextFld isEqualToString:@""] || strTextFld.length==0)
            {
                for (int k=0; k<arrayModalNumber.count; k++)
                {
                    NSDictionary *dictDataa=arrayModalNumber[k];
                    
                    if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]]) {
                        
                        [arrOfModelNosFiltered addObject:dictDataa];
                        
                    }
                }
            }
            
            tblData.tag=1874919425;
            _tableViewSearch.tag=202;
            [_tableViewSearch reloadData];
            
        }
        else if(isManufacturer)
        {
            strSearchType = @"Manufacturer";
            
            arrOfManufacturerFiltered=[[NSMutableArray alloc]init];
            
            NSString *strTextFld=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            if ([string isEqualToString:@""])
            {
                if ([strTextFld length] > 0) {
                    strTextFld = [strTextFld substringToIndex:[strTextFld length] - 1];
                } else {
                    //no characters to delete... attempting to do so will result in a crash
                }
            }
            for (int k=0; k<arrayManufacturer.count; k++)
            {
                NSDictionary *dictDatas=arrayManufacturer[k];
                
                NSString *strSerialNos=[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"Manufacturer"]];
                
                if ([strSerialNos containsString:strTextFld])
                {
                    [arrOfManufacturerFiltered addObject:dictDatas];
                }
            }
            
            if ([strTextFld isEqualToString:@""] || strTextFld.length==0)
            {
                [arrOfManufacturerFiltered addObjectsFromArray:arrayManufacturer];
            }
            
            tblData.tag=1874919426;
            _tableViewSearch.tag=203;
            [_tableViewSearch reloadData];
            
        }
        return YES;
    }
    
    else
    {
        if (isStandard)
        {
            if (textField.tag==302)
            {
                NSScanner *scanner = [NSScanner scannerWithString:string];
                BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
                
                if (range.location == 0 && [string isEqualToString:@" "])
                {
                    return NO;
                }
                BOOL  isReturnTrue;
                
                isReturnTrue=YES;
                
                if (!isNumeric)
                {
                    if ([string isEqualToString:@""])
                    {
                        isReturnTrue=YES;
                        
                    }
                    else
                        isReturnTrue=NO;
                }
                
                if (!isReturnTrue)
                {
                    return NO;
                }
                
                
                if ([string isEqualToString:@""])
                {
                    NSString *strTextQTY;
                    
                    if ([textField.text length] > 0)
                    {
                        strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                        
                    }
                    
                    //   NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                    
                    NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Standard"];
                    
                    float multiplier=[strMultiplier floatValue];
                    
                    strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];
                    
                    float totalPriceStandard=[strTextQTY floatValue]*multiplier*[[dictDataPartsSelected valueForKey:@"BestPrice"] floatValue];
                    
                    _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                    
                }else{
                    
                    NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                    
                    NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Standard"];
                    
                    float multiplier=[strMultiplier floatValue];
                    
                    strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];
                    
                    float totalPriceStandard=[strTextQTY floatValue]*multiplier*[[dictDataPartsSelected valueForKey:@"BestPrice"] floatValue];
                    
                    _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                }
            }
            else if (textField.tag==303)
            {
                strIsChangeStdPartPrice = @"true";
                NSScanner *scanner = [NSScanner scannerWithString:string];
                BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
                
                if (range.location == 0 && [string isEqualToString:@" "])
                {
                    return NO;
                }
                BOOL  isReturnTrue;
                
                isReturnTrue=YES;
                
                if (!isNumeric)
                {
                    if ([string isEqualToString:@""])
                    {
                        isReturnTrue=YES;
                        
                    }
                    else
                        isReturnTrue=NO;
                }
                
                if (!isReturnTrue)
                {
                    return NO;
                }
                
                
                if ([string isEqualToString:@""])
                {
                    NSString *strTextQTY;
                    
                    if ([textField.text length] > 0)
                    {
                        strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                        
                    }
                    
                    //   NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                    
                    //                    NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Standard"];
                    
                    float multiplier=1.0;
                    
                    strMultiplierGlobal=[NSString stringWithFormat:@"%f",multiplier];
                    
                    float totalPriceStandard=[strTextQTY floatValue]*multiplier*[[dictDataPartsSelected valueForKey:@"BestPrice"] floatValue];
                    
                    _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                    
                }
                else
                {
                    
                    NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                    
                    //                    NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"CategorySysName"]] :@"Standard"];
                    //
                    float multiplier=1.0;
                    
                    strMultiplierGlobal=[NSString stringWithFormat:@"%f",multiplier];
                    
                    float totalPriceStandard=[strTextQTY floatValue]*multiplier*[[dictDataPartsSelected valueForKey:@"BestPrice"] floatValue];
                    
                    _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                }
            }
        }
        else
        {
            if (textField.tag==303)
            {
                strIsChangeStdPartPrice = @"false";
                if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
                {
                    if (![string isEqualToString:@"."])
                    {
                        return NO;
                    }
                }
                
                if ([string isEqualToString:@""]) {
                    
                    NSString *strTextQTY;
                    
                    if ([textField.text length] > 0) {
                        
                        strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                        
                    }
                    
                    strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",strTextQTY] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                    
                    float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_textFieldQuantity_AddPart.text floatValue];
                    
                    _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                    
                }else{
                    
                    
                    NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                    
                    // Logic For Two Digits After Decimals
                    NSArray *sep = [strTextQTY componentsSeparatedByString:@"."];
                    if([sep count] >= 2)
                    {
                        NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                        if (!([sepStr length]>2)) {
                            if ([sepStr length]==2 && [string isEqualToString:@"."]) {
                                return NO;
                            }
                        }
                        else{
                            return NO;
                        }
                    }
                    //END Logic For Two Digits After Decimals
                    
                    
                    // strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                    
                    strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",strTextQTY] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                    
                    float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_textFieldQuantity_AddPart.text floatValue];
                    _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                }
            }
            else if (textField.tag==302)
            {
                NSScanner *scanner = [NSScanner scannerWithString:string];
                BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
                
                if (range.location == 0 && [string isEqualToString:@" "]) {
                    return NO;
                }
                BOOL  isReturnTrue;
                
                isReturnTrue=YES;
                
                if (!isNumeric)
                {
                    if ([string isEqualToString:@""])
                    {
                        isReturnTrue=YES;
                    }
                    else
                        isReturnTrue=NO;
                }
                
                if (!isReturnTrue)
                {
                    return NO;
                }
                
                if ([string isEqualToString:@""])
                {
                    NSString *strTextQTY;
                    
                    if ([textField.text length] > 0)
                    {
                        strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                    }
                    
                    float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_textFieldUnitPrice_AddPart.text floatValue];
                    
                    _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                }
                else
                {
                    NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                    
                    //float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[[dictDataPartsSelected valueForKey:@"BestPrice"] floatValue];
                    float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_textFieldUnitPrice_AddPart.text floatValue];
                    
                    _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                }
            }
        }
    }
    return YES;
}
//-(void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    if (textField==_textFieldSerialNumber)
//    {
//        _labelSearchHeader.text=@"Serial #";
//        _txtFieldSearch.placeholder=@"Enter Serial #";
//        _viewTextFieldSearch.frame=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
//        [self.view addSubview:_viewTextFieldSearch];
//        isSerialNo=YES;
//        isModelNo=NO;
//        isManufacturer=NO;
//        arrOfSerialNosFiltered=[[NSMutableArray alloc]init];
//
//        for (int k=0; k<arraySerialNumber.count; k++)
//        {
//            NSDictionary *dictDataa=arraySerialNumber[k];
//
//            if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]])
//            {
//                [arrOfSerialNosFiltered addObject:dictDataa];
//            }
//        }
//
//        //        arrDataTblView=[[NSMutableArray alloc]init];
//        //        [arrDataTblView addObject:@"sdfasdf"];
//        _txtFieldSearch.text=_textFieldSerialNumber.text;
//        tblData.tag=1874919424;
//        _tableViewSearch.tag=201;
//        [_tableViewSearch reloadData];
//        [textField resignFirstResponder];
//
//    }
//    else if (textField==_textFieldModalNumber)
//    {
//        _labelSearchHeader.text=@"Model #";
//        _txtFieldSearch.placeholder=@"Enter Model #";
//        _viewTextFieldSearch.frame=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
//        [self.view addSubview:_viewTextFieldSearch];
//        isSerialNo=NO;
//        isModelNo=YES;
//        isManufacturer=NO;
//
//        arrOfModelNosFiltered=[[NSMutableArray alloc]init];
//
//        for (int k=0; k<arrayModalNumber.count; k++)
//        {
//            NSDictionary *dictDataa=arrayModalNumber[k];
//
//            if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]])
//            {
//                [arrOfModelNosFiltered addObject:dictDataa];
//            }
//        }
//
//
//
//        //        arrDataTblView=[[NSMutableArray alloc]init];
//        //        [arrDataTblView addObject:@"sdfasdf"];
//        _txtFieldSearch.text=_textFieldModalNumber.text;
//        tblData.tag=1874919425;
//        _tableViewSearch.tag=202;
//        [_tableViewSearch reloadData];
//        [textField resignFirstResponder];
//
//    }
//    else if (textField==_textFieldManufacturer)
//    {
//        _labelSearchHeader.text=@"Manufacturer";
//        _txtFieldSearch.placeholder=@"Enter Manufacturer";
//        _viewTextFieldSearch.frame=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
//        [self.view addSubview:_viewTextFieldSearch];
//        isSerialNo=NO;
//        isModelNo=NO;
//        isManufacturer=YES;
//
//        arrOfManufacturerFiltered=[[NSMutableArray alloc]init];
//
//        [arrOfManufacturerFiltered addObjectsFromArray:arrayManufacturer];
//
//
//        //arrDataTblView=[[NSMutableArray alloc]init];
//        //[arrDataTblView addObject:@"sdfasdf"];
//        _txtFieldSearch.text=_textFieldManufacturer.text;
//        tblData.tag=1874919426;
//        _tableViewSearch.tag=203;
//        [_tableViewSearch reloadData];
//        [textField resignFirstResponder];
//    }
//    else if (textField==_textFieldInstallationDate)
//    {
//        isInstallationDate = YES;
//        isWarrantyDate = NO;
//        [self addingDatePicker:@"" :0];
//    }
//    else if (textField==_textFieldWarrantyExpirationDate)
//    {
//        isInstallationDate = NO;
//        isWarrantyDate = YES;
//        [self addingDatePicker:@"" :0];
//
//    }
//    //    else
//    //    {
//    //
//    //
//    ////        UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
//    ////        leftView.backgroundColor = [UIColor clearColor];
//    ////
//    ////
//    ////        textField.leftView = leftView;
//    ////
//    ////        textField.leftViewMode = UITextFieldViewModeAlways;
//    ////        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//    ////
//    ////        [viewForDate removeFromSuperview];
//    ////        [viewBackGround removeFromSuperview];
//    ////        [tblData removeFromSuperview];
//    //
//    //    }
//}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_textFieldHrs_AddLabor) {
        
        NSString *strTextx;
        
        if (textField.text.length>=5) {
            
            strTextx=textField.text;
            
        } else if (textField.text.length==4){
            
            NSString *strTemp=@"0";//  0:00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==3){
            
            NSString *strTemp=@"00";//  :00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==2){
            
            //            NSString *strTemp=@"00:";//  00
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=@":00";//  00
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==1){
            
            //            NSString *strTemp=@"00:0";//  0
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=[NSString stringWithFormat:@"0%@:00",textField.text];
            //NSString *strTemp=@"0:00";
            //strTextx=[textField.text stringByAppendingString:strTemp];
            strTextx=strTemp;
        } else if (textField.text.length==0){
            
            strTextx=@"00:00";
            
        }
        
        
        NSString *lastStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        //NSString *lastSecondStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        NSString *finalStrText = [NSString stringWithFormat:@"%@",lastStrText];
        int valueEntered = [finalStrText intValue];
        if (valueEntered>59) {
            
            [global AlertMethod:Alert :@"Minutes can't be greater than 59"];
            
            //            textField.text =@"";
            //            textField.placeholder=@"00:00";
            //            _txtFld_LaborTime.text=@"";
            //            [self methodToCalculateLaborPrice:[NSString stringWithFormat:@"%@",@"00:00"]];
            
        }else{
            
            textField.text=strTextx;
            
            if ([textField.text isEqualToString:@"00:00"]) {
                
                textField.text =@"";
                textField.placeholder=@"00:00";
                
            }
            
            strDate = strTextx;
            
            if (strDate.length==0) {
                strDate=@"00:00";
            }
            
            [self methodToCalculateLaborPrice:[NSString stringWithFormat:@"%@",strDate]];
            
        }
        
    }
    //       if(textField==_textFieldMiscCharge)
    //       {
    //           NSString *strTemp = [NSString stringWithFormat:@"$%.02f",[textField.text floatValue]];
    //           textField.text = strTemp;
    //           // [self taxCalculationNewPlan];
    //       }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================

-(void)addingDatePicker :(NSString*)strDateToSet :(int)tag
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    tblData.tag=tag;
    [self addPickerViewDateTo :strDateToSet];
    if(isInstallationDate)
    {
        [_textFieldInstallationDate resignFirstResponder];
    }
    else
    {
        [_textFieldWarrantyExpirationDate resignFirstResponder];
    }
}

-(void)addPickerViewDateTo :(NSString*)strDateString
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    
    if(isInstallationDate)
    {
        //[pickerDate setMaximumDate:[NSDate date]]; commented on 24 April 2019 as discussed with saavan
    }
    else
    {
        [pickerDate setMinimumDate:[NSDate date]];
    }
    
    if (!(strDateString.length==0))
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strDateString];
        
        if (dateToSett==nil)
        {
            
        }
        else
        {
            pickerDate.date =dateToSett;
        }
    }
    
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround1.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround1];
    
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround1 setUserInteractionEnabled:YES];
    [viewBackGround1 addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround1 addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    if(isAddLabor)
    {
        [viewForDateTime removeFromSuperview];
        [viewBackGroundTime removeFromSuperview];
    }
    else if(isAddPart)
    {
        [viewForDate removeFromSuperview];
        [viewBackGround1 removeFromSuperview];
    }
    else
    {
        [viewForDate removeFromSuperview];
        [viewBackGround1 removeFromSuperview];
    }
    
}

-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDateEquip = [dateFormat stringFromDate:pickerDate.date];
    
    if(isAddPart)
    {
        if(isInstallationDate)
        {
            [_buttonInstallationDate_AddPart setTitle:strDateEquip forState:UIControlStateNormal];
        }
        else
        {
            if([[NSCalendar currentCalendar] isDateInToday:pickerDate.date]==YES)
            {
                [global AlertMethod:@"Alert" :@"Please select future date"];
            }
            else
            {
                [_buttonWarrantyExpireDate_AddPart setTitle:strDateEquip forState:UIControlStateNormal];
            }
        }
        [viewForDate removeFromSuperview];
        [viewBackGround1 removeFromSuperview];
    }
    else if(isAddLabor)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        // [dateFormat setDateFormat:@"hh:mm:a"];
        [dateFormat setDateFormat:@"HH:mm"];
        strDate = [dateFormat stringFromDate:pickerDate.date];
        // [_buttonSelectTime_AddLabor setTitle:strDate forState:UIControlStateNormal];
        
        [self methodToCalculateLaborPrice:[NSString stringWithFormat:@"%@",strDate]];
        
        [viewForDateTime removeFromSuperview];
        [viewBackGroundTime removeFromSuperview];
        NSLog(@"Labor time");
    }
    else
    {
        if(isInstallationDate)
        {
            _textFieldInstallationDate.text = strDateEquip;
            [_textFieldInstallationDate resignFirstResponder];
        }
        else
        {
            if([[NSCalendar currentCalendar] isDateInToday:pickerDate.date]==YES)
            {
                [global AlertMethod:@"Alert" :@"Please select future date"];
            }
            else
            {
                _textFieldWarrantyExpirationDate.text = strDateEquip;
                [_textFieldWarrantyExpirationDate resignFirstResponder];
            }
        }
        [viewForDate removeFromSuperview];
        [viewBackGround1 removeFromSuperview];
    }
}


//============================================================================
#pragma mark- Single Tap Methods
//============================================================================
-(void)tapDetectedOnBackGroundView
{
    if(isAddPart)
    {
        [viewForDate removeFromSuperview];
        [tblData removeFromSuperview];
        [viewBackGround removeFromSuperview];
    }
    else if (isAddLabor)
    {
        [viewForDate removeFromSuperview];
        [tblData removeFromSuperview];
        [viewBackGround removeFromSuperview];
    }
    else
    {
        [viewForDate removeFromSuperview];
        [tblData removeFromSuperview];
        [viewBackGround1 removeFromSuperview];
    }
    
}
- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    [viewBackGround removeFromSuperview];
    [viewForDate removeFromSuperview];
    [tblData removeFromSuperview];
}
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [viewForDate removeFromSuperview];
//    [tblData removeFromSuperview];
//    [viewBackGround removeFromSuperview];
//}

-(void)addPartView
{
    //    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    //    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    
    viewBackGround.backgroundColor = [UIColor whiteColor];
    [self.view addSubview: viewBackGround];
    
    CGRect frameFor_ViewAddLabor=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width,_viewAddPart.frame.size.height);
    [_viewAddPart setFrame:frameFor_ViewAddLabor];
    [viewBackGround addSubview:_viewAddPart];
}

-(void)hideTextFields{
    /*
     
     
     @property (weak, nonatomic) IBOutlet UITextView *textViewDescription_AddPart;
     @property (weak, nonatomic) IBOutlet UITextField *textFieldPartName_AddPart;*/
    [_textFieldUnitPrice_AddPart resignFirstResponder];
    [_textFieldQuantity_AddPart resignFirstResponder];
    [_textFieldTotalPrice_AddPart resignFirstResponder];
    [_textFieldSerialNumber_AddPart resignFirstResponder];
    [_textFieldModelNumber_AddPart resignFirstResponder];
    [_textFieldManufacturer_AddPart resignFirstResponder];
    [_textViewDescription_AddPart resignFirstResponder];
    [_textFieldPartName_AddPart resignFirstResponder];
}

-(NSString*)logicForFetchingMultiplier :(NSString*)strPriceToLookUp :(NSString*)strCategorySysName :(NSString*)strType{
    
    NSString *strMultiplier;
    
    [self getPricLookupFromMaster:strCategorySysName];

    strMultiplier=[global logicForFetchingMultiplier:strPriceToLookUp :strCategorySysName :strType :arrOfPriceLookup];
    
    return strMultiplier;
}

-(void)getPricLookupFromMaster :(NSString*)strCategorySysName{
    
    arrOfPriceLookup=nil;
    arrOfPriceLookup=[[NSMutableArray alloc]init];
    arrOfPriceLookup=[global getPricLookupFromMaster:strCompanyKey :strDepartMentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType :strCategorySysName];
    
}

-(void)getPartsMaster
{
    arrDataTblView=nil;
    arrDataTblView=[[NSMutableArray alloc]init];
    arrDataTblView=[global getPartsMaster:strDepartMentSysName :strCategoryId];
}

-(void)methodToCalculateLaborPrice :(NSString *)strTimeEntered{
    
    if (strTimeEntered.length==5) {
        
        NSDictionary *dictDataAfterHourRateToBeUsed=[[NSDictionary alloc]init];
        
        if (!isWarranty_AddLabor) {
            
            if (isLaborType) {
                
                //Logic For Price Calculation
                
                //                float hrsTime=[global ChangeTimeMechanicalLabor:strTimeEntered];
                float hrsTime=[self getSeconds:strTimeEntered];
                
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0))
                {
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        // Change for holiday and After hours change
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                            
                        } else {
                            
                            // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                            
                            if (strAfterHrsDuration.length==0) {
                                
                                strStdPrice=0.0;
                                
                            } else {
                                
                                NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                
                                NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                
                                strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                
                                
                                NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                
                                for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                    
                                    NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                    
                                    NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                    NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                    
                                    if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                        
                                        dictDataAfterHourRateToBeUsed=dictDataAHC;
                                        break;
                                        
                                    }
                                }
                                
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborPrice"] floatValue];
                                
                            }
                        }
                    }
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                _textFieldAmount_AddLabor.text=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
                
            } else {
                
                //Logic For Price Calculation
                
                //                float hrsTime=[global ChangeTimeMechanicalLabor:strTimeEntered];
                float hrsTime=[self getSeconds:strTimeEntered];
                
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];
                            
                        } else {
                            
                            if ((dictDataAfterHourRateToBeUsed==nil) || ([dictDataAfterHourRateToBeUsed isKindOfClass:[NSNull class]])) {
                                
                                
                                
                            } else {
                                
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                                
                            }
                            
                            strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                            
                        }
                        
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                _textFieldAmount_AddLabor.text=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
            }
            
        }else{
            
            // Is Warranty Checked Hai
            
            if (isLaborType) {
                
                //Logic For Price Calculation
                
                //                float hrsTime=[global ChangeTimeMechanicalLabor:strTimeEntered];
                float hrsTime=[self getSeconds:strTimeEntered];
                
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0))
                {
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"WrntyStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        // Change for holiday and After hours change
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborWrntyPrice"] floatValue];
                            
                        } else {
                            
                            // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                            
                            if (strAfterHrsDuration.length==0) {
                                
                                strStdPrice=0.0;
                                
                            } else {
                                
                                NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                
                                NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                
                                strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                
                                
                                NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                
                                for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                    
                                    NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                    
                                    NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                    NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                    
                                    if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                        
                                        dictDataAfterHourRateToBeUsed=dictDataAHC;
                                        break;
                                        
                                    }
                                    
                                }
                                
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborWrntyPrice"] floatValue];
                                
                            }
                        }
                    }
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                _textFieldAmount_AddLabor.text=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
                
            } else {
                
                //Logic For Price Calculation
                
                //                float hrsTime=[global ChangeTimeMechanicalLabor:strTimeEntered];
                float hrsTime=[self getSeconds:strTimeEntered];
                
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HelperWrntyStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayHelperWrntyPrice"] floatValue];
                            
                        } else {
                            
                            strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperWrntyPrice"] floatValue];
                            
                        }
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                _textFieldAmount_AddLabor.text=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
            }
            
            
        }
        
    }else{
        
        _textFieldAmount_AddLabor.text=@"";
        
    }
    
}

-(void)methodLoadValues
{
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
    //    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    //    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    //    strEmpName        =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    //    strWoStatus       =[defsLead valueForKey:@"WoStatus"];
    
    NSString *strIsActive=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"isStdPrice"]];
    
    if ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"])
    {
        isStandardSubWorkOrder=YES;
    }
    else
    {
        isStandardSubWorkOrder=NO;
    }
    
    //_lblAccNoDetails.text=[NSString stringWithFormat:@"%@, Sub Work Order #: %@",[defsLead valueForKey:@"lblName"],[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderNo"]]];
    
    strDepartMentSysName=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"departmentSysName"]];
    
    
    // _objWorkOrderdetails=[global fetchMechanicalWorkOrderObj:_strWorkOrderId];
    
    //
    
    strWorkOrderAccNo=[NSString stringWithFormat:@"%@",[_objWorkOrderDetail valueForKey:@"accountNo"]];
    
    strWorkOrderAddressId=[NSString stringWithFormat:@"%@",[_objWorkOrderDetail valueForKey:@"serviceAddressId"]];
    
    strWorkOrderAddressSubType=[NSString stringWithFormat:@"%@",[_objWorkOrderDetail valueForKey:@"addressSubType"]];
    
    //isHoliday
    
    NSString *strIsHoliday=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"isHolidayHrs"]];
    
    if ([strIsHoliday isEqualToString:@"true"] || [strIsHoliday isEqualToString:@"1"])
    {
        isHoliday=YES;
    }
    else
    {
        isHoliday=NO;
    }
    
    //AfterHrsDuration
    
    strAfterHrsDuration=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"afterHrsDuration"]];
}

-(void)addLaborView
{
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewAddLabor
    
    CGRect frameFor_ViewAddLabor=CGRectMake(0, 105, [UIScreen mainScreen].bounds.size.width,_viewAddLabor.frame.size.height);
    [_viewAddLabor setFrame:frameFor_ViewAddLabor];
    
    [viewBackGround addSubview:_viewAddLabor];
}

-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    
    pickerDate.datePickerMode =UIDatePickerModeTime;
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    [pickerDate setLocale:locale];
    
    //pickerDate.datePickerMode =UIDatePickerModeTime;  //UIDatePickerModeDate;
    [viewForDateTime setHidden:NO];
    
    viewBackGroundTime=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundTime.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGroundTime];
    
    
    viewForDateTime=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGroundTime addSubview: viewForDateTime];
    
    viewForDateTime.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDateTime.layer.cornerRadius=20.0;
    viewForDateTime.clipsToBounds=YES;
    [viewForDateTime.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDateTime.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDateTime.frame.size.width, 50)];
    
    lblTitle.text=@"Select Time";//@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDateTime addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDateTime.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDateTime addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDateTime.frame.size.height-50, viewForDateTime.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDateTime addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDateTime bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDateTime addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDateTime addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDateTime.frame.size.width, viewForDateTime.frame.size.height-100);
    [viewForDateTime addSubview:pickerDate];
    
    if (!(strDate.length==0))
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"HH:mm"];
        NSDate *dateToSett = [dateFormat dateFromString:strDate];
        if (dateToSett==nil)
        {
        }
        else
        {
            pickerDate.date =dateToSett;
        }
    }
}

-(void)getHoursConfiFromMaster
{
    arrOfHoursConfig=nil;
    arrOfHoursConfig=[[NSMutableArray alloc]init];
    arrOfHoursConfig=[global getHoursConfiFromMaster:strCompanyKey :strDepartMentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType];
}

- (UIImage*)loadImage:(NSString *)imageName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      imageName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

-(void)fetchLocalMasters
{
    //strDepartmentSysName=@"RCDept";
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    arrMechanicalMasters=[dictMechanicalMasters valueForKey:@"ChargeMasterExtSerDc"];
    
    NSMutableArray *arrOfTempChargeMasters=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalMasters.count;i++)
    {
        NSDictionary *dictDataa=[arrMechanicalMasters objectAtIndex:i];
        
        NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
        NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
        NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
        
        if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:strWorkOrderAddressId]) {
            
            [arrOfTempChargeMasters addObject:dictDataa];
        }
    }
    
    if (arrOfTempChargeMasters.count==0)
    {
        for (int k=0; k<arrMechanicalMasters.count; k++)
        {
            NSDictionary *dictDataa=arrMechanicalMasters[k];
            //   BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
            //   if (isActive) {
            
            // Changes for fetching Via company dept account address
            
            
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
            NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
            
            
            if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:@""]) {
                
                [arrOfTempChargeMasters addObject:dictDataa];
                
            }
        }
    }
    
    if (arrOfTempChargeMasters.count==0) {
        
        for (int k=0; k<arrMechanicalMasters.count; k++) {
            
            NSDictionary *dictDataa=arrMechanicalMasters[k];
            //   BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
            //   if (isActive) {
            
            // Changes for fetching Via company dept account address
            
            
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
            NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
            //  NSString *strLocalAddressSubType=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AddressType"]];
            
            
            if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:@""] && [strLocalAddressId isEqualToString:@""])
            {
                [arrOfTempChargeMasters addObject:dictDataa];
            }
        }
        
    }
    //  change for
    for(int i=0;i<arrOfTempChargeMasters.count;i++)
    {
        NSDictionary *dict=[arrOfTempChargeMasters objectAtIndex:i];
        if ([[dict valueForKey:@"ChargeType"]isEqualToString:@"Diagnostic"])
        {
            [arrayDiagnostic addObject:dict];
        }
        else if ([[dict valueForKey:@"ChargeType"]isEqualToString:@"Trip"])
        {
            [arrayTripCharge addObject:dict];
        }
        else if ([[dict valueForKey:@"ChargeType"]isEqualToString:@"Mileage"])
        {
            [arrayMileageCharge addObject:dict];
        }
    }
}

-(double)taxCalculationNewPlan
{
    
    double tax=0.0;
    subTotalParts = 0.0;
    subTotalLabor = 0.0;
    
    NSArray *arrOfTaxMaster=[global getSalesTaxFromMaster:strCompanyKey :strDepartMentSysName :[dictSubWorkOrderDetails valueForKey:@"propertyType"]];
    
    if (!(arrOfTaxMaster.count==0)) {
        
        //Check According To Condition to apply tax on what
        
        BOOL isLaborTax=NO,isMaterialTax=NO,isVendorPayTax=NO,isFullTax=NO;
        
        NSString *strPartPriceType;
        
        NSDictionary *dictTaxData=arrOfTaxMaster[0];
        
        NSString *strVendorPayTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"VendorPayTax"]];
        
        if ([strVendorPayTax isEqualToString:@"true"] || [strVendorPayTax isEqualToString:@"True"] || [strVendorPayTax isEqualToString:@"1"]) {
            
            isVendorPayTax=YES;
            
        }
        
        if ([strWorkOrderAddressSubType isEqualToString:@"Commercial"]) {//Commercial Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMPartPriceType"]];
                
            }
            
        } else {//Resedential Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMPartPriceType"]];
                
            }
            
        }
        if (isFullTax)
        {
            float subTotal=0.0;
            
            for(NSDictionary *dict in arrayPart)
            {
                subTotal = subTotal+[[dict valueForKey:@"totalPrice"] floatValue];
            }
            
            for(NSDictionary *dict in arrayLabour)
            {
                subTotal = subTotal+[[dict valueForKey:@"amount"] floatValue];
            }
            
            
            subTotal = subTotal+[self getNumberString:_labelDiagnosticCharge.text]+[self getNumberString:_labelTripCharge.text]+[self getNumberString:_labelMileageCharge.text]+[self getNumberString:_textFieldMiscCharge.text];
            
            // tax calculation//
            
            double tax= 0.0;
            
            double taxValue;
            taxValue=[strTax doubleValue];
            tax=(subTotal*taxValue)/100;
            
            if (tax>0)
            {
                _labelTax.text=[NSString stringWithFormat:@"$%.02f",tax];
            }
            else
            {
                _labelTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
                tax=0.00;
            }
            
            double totalNew;
            totalNew=subTotal+tax;
            float subTotalTemp = [self getNumberString:_labelDiagnosticCharge.text ]+[self getNumberString:_labelTripCharge.text]+[self getNumberString:_labelMileageCharge.text]+[self getNumberString:_textFieldMiscCharge.text]+[self getNumberString:_labelSubTotalAmount.text];
            
            _labelSubTotal.text = [NSString stringWithFormat:@"$%.02f",subTotalTemp];
            _labelTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
            
            
        }
        else
        {
            
            for(NSDictionary *dict in arrayLabour)
            {
                subTotalLabor = subTotalLabor+[[dict valueForKey:@"amount"] floatValue];
            }
            
            if ([strPartPriceType isEqualToString:@"Sales Price"])
            {
                for(NSDictionary *dict in arrayPart)
                {
                    subTotalParts = subTotalParts+([[dict valueForKey:@"quantity"] floatValue]*[[dict valueForKey:@"multiplier"] floatValue]*[[dict valueForKey:@"unitPrice"] floatValue]);
                }
                
            }
            else
            {
                for(NSDictionary *dict in arrayPart)
                {
                    subTotalParts = subTotalParts+([[dict valueForKey:@"quantity"] floatValue]*[[dict valueForKey:@"unitPrice"] floatValue]);
                }
            }
            
            if(isLaborTax)
            {
                tax = (subTotalLabor+[self getNumberString:_labelDiagnosticCharge.text]+[self getNumberString:_labelTripCharge.text]+[self getNumberString:_labelMileageCharge.text]+[self getNumberString:_textFieldMiscCharge.text])*([strTax floatValue])/100.0;
            }
            if(isMaterialTax)
            {
                tax = (subTotalParts*[strTax floatValue])/100.0;
            }
            
            _labelTax.text = [NSString stringWithFormat:@"$%.02f",tax];
            
            float subTotalTemp = [self getNumberString:_labelDiagnosticCharge.text ]+[self getNumberString:_labelTripCharge.text]+[self getNumberString:_labelMileageCharge.text]+[self getNumberString:_textFieldMiscCharge.text]+[self getNumberString:_labelSubTotalAmount.text];
            
            _labelSubTotal.text = [NSString stringWithFormat:@"$%.02f",subTotalTemp];
            
            float totalValue = [self getNumberString:_labelTax.text]+[self getNumberString:_labelSubTotal.text];
            _labelTotal.text = [NSString stringWithFormat:@"$%.02f",totalValue];
        }
    }
    
    return tax;
}

-(double)calculationOverAllNewReturn :(float)subTotal
{
    
    double tax= 0.0;
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    
    if (tax>0)
    {
        _labelTax.text=[NSString stringWithFormat:@"$%.02f",tax];
    }
    else
    {
        _labelTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    double totalNew;
    totalNew=subTotal+tax;
    _labelTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    
    return tax;
}

-(double)getNumberString:(NSString*)str
{
    double value;
    NSString *strDiscount = [str stringByReplacingOccurrencesOfString:@"$" withString:@""];
    strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    value=[strDiscount doubleValue];
    return value;
}
-(void)showPartsAndLaborAmount
{
    float totalAmt = 0.0;
    for(NSDictionary *dict in arrayPart)
    {
        totalAmt = totalAmt+[[dict valueForKey:@"totalPrice"] floatValue];
    }
    for(NSDictionary *dict in arrayLabour)
    {
        totalAmt = totalAmt+[[dict valueForKey:@"amount"] floatValue];
    }
    
    _labelSubTotalAmount.text = [NSString stringWithFormat:@"$%.02f",totalAmt];
    [self taxCalculationNewPlan];
}




-(void)uploadSignatureImageOnServer
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strImageName = [defs valueForKey:@"imagePathQuote"];
    strTechSignaturePath = strImageName;
    UIImage *img = _imageViewSignature.image;
    
    if(isSignatureImage)
    {
        NSData *imageData  = UIImageJPEGRepresentation(img,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",MainUrl,UrlQuoteImageUpload];
        //http://tcrs.stagingsoftware.com/api/File/UploadQuoteSignaturesAsync
        
        //  NSString *urlString = @"http://tcrs.stagingsoftware.com/api/File/UploadQuoteSignaturesAsync";
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSError *error = Nil;
        
        id jsonObject =[NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&error];
        
        
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            [self syncQuoteDataOnServer];
        }
        else
        {
            [self syncQuoteDataOnServer];
        }
        
        //        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        
        
        //        if ([returnString isEqualToString:@"OK"])
        //        {
        //            NSLog(@"Image Sent");
        //            // sync data
        //            [self syncQuoteDataOnServer];
        //        }
        //        else
        //        {
        //            // sync data
        //            [self syncQuoteDataOnServer];
        //        }
    }
}

-(void)syncQuoteDataOnServer
{
    
    NSMutableArray *arrayTempPart = [NSMutableArray new];
    
    NSMutableArray *arrayTempLabor = [NSMutableArray new];
    
    NSMutableArray *arry = [NSMutableArray new];
    
    NSString *strQuoteNumber = @"";
    
    NSString *strCustomerPONumber = @"";
    
    NSString *strQuote = @"";
    
    NSString *strDescEquip = @"";
    
    
    
    if(_textFieldQuoteNumber.text.length>0)
    {
        strQuoteNumber = _textFieldQuoteNumber.text;
    }
    
    if(_textFieldCustomerPONumber.text.length>0)
    {
        strCustomerPONumber = _textFieldCustomerPONumber.text;
    }
    
    if(_textViewQuoteDescription.text.length>0)
    {
        strQuote = _textViewQuoteDescription.text;
    }
    
    if(_textFieldManufacturer.text.length>0)
    {
        strManufacturer = _textFieldManufacturer.text;
    }
    
    if(_textFieldSerialNumber.text.length>0)
    {
        strSerialNumber = _textFieldSerialNumber.text;
    }
    
    if(_textFieldModalNumber.text.length>0)
    {
        strModalNumber = _textFieldModalNumber.text;
    }
    
    if(_textFieldInstallationDate.text.length>0)
    {
        strInstallationdate = _textFieldInstallationDate.text;
    }
    
    if(_textFieldWarrantyExpirationDate.text.length>0)
    {
        strWarrantyExpirationDate = _textFieldWarrantyExpirationDate.text;
    }
    
    // instalation date must be less than expiration date
    if(strInstallationdate.length>0 && strWarrantyExpirationDate.length>0)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *installationDate = [dateFormat dateFromString:strInstallationdate];
        NSDate *expirationDate = [dateFormat dateFromString:strWarrantyExpirationDate];
        
        NSComparisonResult result = [installationDate compare:expirationDate];
        if(result == NSOrderedSame || result == NSOrderedDescending)
        {
            [global AlertMethod:Alert :@"Warrenty expiration date must be greater than Installation date"];
            return;
        }
    }
    
    
    if(_textViewDescriptionAddDetails.text.length>0)
    {
        strDescEquip = _textViewDescriptionAddDetails.text;
    }
    
    for(NSDictionary *dict in arrayPart)
    {
        
        NSString *strInstDate = @"";
        
        NSString *strWarrDate = @"";
        
        NSString *strPartType = @"";
        
        if(![_buttonInstallationDate_AddPart.titleLabel.text isEqualToString:@"--Select Date--"])
        {
            strInstDate = _buttonInstallationDate_AddPart.titleLabel.text;
        }
        
        if(![_buttonWarrantyExpireDate_AddPart.titleLabel.text isEqualToString:@"--Select Date--"])
        {
            strWarrDate = _buttonWarrantyExpireDate_AddPart.titleLabel.text;
        }
        
        
        if([[dict valueForKey:@"isStandard"] isEqualToString:@"yes"])
        {
            strPartType = @"standard";
        }
        
        else
        {
            strPartType = @"Non-Standard";
        }
        
        
        if([[dict valueForKey:@"isStandard"] isEqualToString:@"yes"])
        {
            NSDictionary *dictTempPart = @{
                                           
                                           @"QuoteEquipmentPartId":@"",
                                           
                                           @"QuoteEquipmentId":@"",
                                           
                                           @"VendorId":[NSString stringWithFormat:@"%@",[[dict valueForKey:@"dictVendor"] valueForKey:@"VendorMasterId"]],
                                           
                                           @"VendorLocationId":[NSString stringWithFormat:@"%@",[[dict valueForKey:@"dictVendorLocation"] valueForKey:@"VendorLocationId"]],
                                           
                                           @"PartType":strPartType,
                                           
                                           @"ModelNumber":[dict valueForKey:@"modelNumber"],
                                           
                                           @"Qty":[NSString stringWithFormat:@"%@",[dict valueForKey:@"quantity"]],
                                           
                                           @"SerialNumber":[dict valueForKey:@"serialNumber"],
                                           
                                           @"InstallationDate":strInstDate,
                                           
                                           @"Manufacturer":[dict valueForKey:@"manufacture"],
                                           
                                           @"IsActive":@"true",
                                           
                                           @"PartCode":[[dict valueForKey:@"dictPart"] valueForKey:@"SysName"],
                                           
                                           @"PartName":[[dict valueForKey:@"dictPart"] valueForKey:@"Name"],
                                           
                                           @"Multiplier":[NSString stringWithFormat:@"%@",[dict valueForKey:@"multiplier"]],
                                           
                                           @"ModifiedBy":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"userName"]],
                                           
                                           @"UnitPrice":[dict valueForKey:@"unitPrice"],
                                           
                                           @"ModifiedDate":[global strCurrentDate],
                                           
                                           @"CreatedDate":[global strCurrentDate],
                                           
                                           @"CreatedBy":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"userName"]],
                                           
                                           @"PartDesc":[dict valueForKey:@"description"],
                                           
                                           @"WarrantyExpireDate":strWarrDate,
                                           
                                           @"IsWarranty":[dict valueForKey:@"isWarranty"]
                                           
                                           };
            
            
            
            [arrayTempPart addObject:dictTempPart];
            
        }
        else
        {
            
            NSDictionary *dictTempPart = @{
                                           
                                           @"QuoteEquipmentPartId":@"",
                                           
                                           @"QuoteEquipmentId":@"",
                                           
                                           @"VendorId":@"",
                                           
                                           @"VendorLocationId":@"",
                                           
                                           @"PartType":strPartType,
                                           
                                           @"ModelNumber":[dict valueForKey:@"modelNumber"],
                                           
                                           @"Qty":[NSString stringWithFormat:@"%@",[dict valueForKey:@"quantity"]],
                                           
                                           @"SerialNumber":[dict valueForKey:@"serialNumber"],
                                           
                                           @"InstallationDate":strInstDate,
                                           
                                           @"Manufacturer":[dict valueForKey:@"manufacture"],
                                           
                                           @"IsActive":@"true",
                                           
                                           @"PartCode":@"",
                                           
                                           @"PartName":[dict valueForKey:@"dictPart"],
                                           
                                           @"Multiplier":[NSString stringWithFormat:@"%@",[dict valueForKey:@"multiplier"]],
                                           
                                           @"ModifiedBy":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"userName"]],
                                           
                                           @"UnitPrice":[dict valueForKey:@"unitPrice"],
                                           
                                           @"ModifiedDate":[global strCurrentDate],
                                           
                                           @"CreatedDate":[global strCurrentDate],
                                           
                                           @"CreatedBy":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"userName"]],
                                           
                                           @"PartDesc":[dict valueForKey:@"description"],
                                           
                                           @"WarrantyExpireDate":strWarrDate,
                                           
                                           @"IsWarranty":[dict valueForKey:@"isWarranty"]
                                           
                                           };
            
            
            
            [arrayTempPart addObject:dictTempPart];
            
        }
        
        
        
    }
    
    
    
    for(NSDictionary *dict in arrayLabour)
        
    {
        
        NSString *strLbrType = @"";
        
        NSString *strTempIsWarranty = @"";
        
        
        
        if([[dict valueForKey:@"strIsLaberType"] isEqualToString:@"yes"])
            
        {
            
            strLbrType = @"L";
            
        }
        
        else
            
        {
            
            strLbrType = @"H";
            
        }
        
        
        
        NSString *srtLaborCostType;
        
        if (isStandardSubWorkOrder)
            
        {
            
            if (isWarranty_AddLabor)
                
            {
                
                srtLaborCostType=@"WSP";
                
            }
            
            else
                
            {
                
                srtLaborCostType=@"SC";
                
            }
            
        }
        
        else
            
        {
            
            if (isWarranty_AddLabor)
                
            {
                
                srtLaborCostType=@"WASP";
                
            }
            
            else
                
            {
                
                srtLaborCostType=@"AHC";
                
            }
            
        }
        
        
        
        if(isWarranty_AddLabor)
            
        {
            
            strTempIsWarranty = @"yes";
            
        }
        
        else
            
        {
            
            strTempIsWarranty = @"no";
            
        }
        
        
        
        NSDictionary *dictTempLabor = @{@"QuoteEquipmentLaborId": @"",
                                        
                                        @"QuoteEquipmentId": @"",
                                        
                                        @"LaborType":strLbrType,
                                        
                                        @"LaborCostType":srtLaborCostType,
                                        
                                        @"LaborHours":[dict valueForKey:@"time"],
                                        
                                        @"LaborCost":[dict valueForKey:@"amount"],
                                        
                                        @"Description":[dict valueForKey:@"description"],
                                        
                                        @"isActive":@"true",
                                        
                                        @"CreateDate":[global strCurrentDate],
                                        
                                        @"ModifiedBy":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"userName"]],
                                        
                                        @"QuoteLaborId":@"",
                                        
                                        @"isDefault":@"true",
                                        
                                        @"isWarranty":strTempIsWarranty
                                        
                                        };
        
        [arrayTempLabor addObject:dictTempLabor];
        
    }
    
    
    
    if(isAddDetailsChecked)
    {
        
        if((strItemSysName.length>0))
            
        {
            
            NSDictionary *dictEquip = @{@"EquipmentCode":strEquipmentCode,
                                        
                                        @"EquipmentName":strEquipmentName,
                                        
                                        @"EquipmentNumber":strEquipmentNumber,
                                        
                                        @"EquipmentCategorySysName":strEquipmentCategorySysName,
                                        
                                        @"Manufacturer":strManufacturer,
                                        
                                        @"SerialNumber":strSerialNumber,
                                        
                                        @"ModelNumber":strModalNumber,
                                        
                                        @"InstalledArea":strArea,
                                        
                                        @"InstallationDate":strInstallationdate,
                                        
                                        @"WarrantyExpireDate":strWarrantyExpirationDate,
                                        
                                        @"EquipmentDesc":strDescEquip,
                                        
                                        @"IsActive":@"true",
                                        
                                        @"CreatedDate":[global strCurrentDate],
                                        
                                        @"CreatedBy":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"userName"]],
                                        
                                        @"ModifiedDate":[global strCurrentDate],
                                        
                                        @"ModifiedBy":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"userName"]],
                                        
                                        @"WorkOrderId":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"workorderId"]],
                                        
                                        @"SubWorkOrderId":[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]],
                                        
                                        @"QuoteEquipmentLaborDcs":arrayTempLabor,
                                        
                                        @"QuoteEquipmentPartDcs":arrayTempPart
                                        
                                        };
            
            
            
            [arry addObject:dictEquip];
            
        }
        
    }
    
    
    
    NSDictionary *dict = @{@"QuoteId":@"",
                           
                           @"CompanyId":strCompanyID,
                           
                           @"WorkOrderId":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"workorderId"]],
                           
                           @"WorkOrderNo":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"workOrderNo"]],
                           
                           @"SubWorkOrderId":[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]],
                           
                           @"SubWorkOrderNo":[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderNo"]],
                           
                           @"AccountNo":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"accountNo"]],
                           
                           @"EmployeeNo":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"employeeNo"]],
                           
                           @"DepartmentId":strDepartmentID,
                           
                           @"QuoteNo":strQuoteNumber,
                           
                           @"CustomerPONumber":strCustomerPONumber,
                           
                           // @"ServiceAddressId":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"serviceAddressId"]],
                           
                           @"ServiceAddressId":strServiceAddressID,
                           
                           @"ServiceAddressName":strServiceAddressName,
                           
                           @"ServicePocId":strCRMContactID,
                           
                           @"QuoteDesc":strQuote,
                           
                           @"PrimaryServiceId":strServiceID,
                           
                           @"PrimaryServiceSysName":strPrimaryServiceSysName,
                           
                           @"QuoteStatus":@"1",
                           
                           @"TechnicianSignaturePath":strTechSignaturePath,
                           
                           @"CustomerSignaturePath":@"",
                           
                           @"IsCustomerNotPresent":@"",
                           
                           @"DiagnosticChargeName":strDiagnosticChargeName,
                           
                           @"DiagnosticChargeAmt":strDiagnosticChargeAmt,
                           
                           @"TripChargeName":strTripChargeName,
                           
                           @"TripChargeAmt":strTripChargeAmt,
                           
                           @"MileageChargeName":strMileageName,
                           
                           @"MileageChargeAmt":strMileageChargeAmt,
                           
                           @"TotalMileage":_textFieldMiles.text,
                           
                           @"TotalApprovedAmt":_labelSubTotalAmount.text,
                           
                           @"MiscChargeAmt":_textFieldMiscCharge.text,
                           
                           @"TaxAmt":_labelTax.text,
                           
                           @"IsActive":@"true",
                           
                           @"DeclineReason":@"",
                           
                           @"JobType":strJobType,
                           
                           @"CreatedDate":[global strCurrentDate],
                           
                           @"CreatedBy":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"userName"]],
                           
                           @"ModifiedDate":[global strCurrentDate],
                           
                           @"ModifiedBy":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"userName"]],
                           
                           @"QuoteEquipmentDcs":arry,// use  QuoteEquipmentExtSerDc array object
                           @"IsEmployeePresetSignature":strIsEmployeePresetSignature,
                           @"TripChargesQty": (_txtFldQty.text.length>0)? _txtFldQty.text : @"0"
                           };
    
    
    
    
    
    //    NSDictionary *jsonDict  = @{@"QuoteExtSerDc":dict
    
    //                                };
    
    [self postApiCall:dict];
    
    
    
}


-(void)postApiCall:(NSDictionary *)jsonDict
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",MainUrl,URLCREATEQUOTE,[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"companyKey"]]];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:Nil];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonDatastr %@",str);
    NSURL * serviceUrl = [NSURL URLWithString:strUrl];
    NSLog(@"REquest URL >> %@",serviceUrl);
    NSLog(@"REquest XML >> %@",str);
    
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:serviceUrl];
    
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *__block serviceResponse;
    NSError *__block serviceError;
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&serviceResponse error:&serviceError];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [DejalBezelActivityView removeView];
            
        });
        
        if (responseData)
        {
            [self parsePostApiData:responseData];
        }
        else
        {
            //        AlertViewClass *a = [[AlertViewClass alloc] init];
            //        [a showMessage:@"Cannot connect to internet." title:@"Skillgrok"];
        }
        
    });
}

-(void)parsePostApiData:(NSData *)response
{
    id jsonObject = Nil;
    
    NSString *charlieSendString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSLog(@"ResponseString %@",charlieSendString);
    
    if (response==nil)
    {
        NSLog(@"No internet connection.");
    }
    else
    {
        NSError *error = Nil;
        jsonObject =[NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            NSLog(@"Probably An Array %@",jsonObject);
            
            if([[[jsonObject objectAtIndex:1] valueForKey:@"Value"] boolValue]==1)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSString *strMsg = [[jsonObject objectAtIndex:0] valueForKey:@"Value"];
                    
                    NSArray *arrayMsg = [strMsg componentsSeparatedByString:@"Quote"];
                    
                    NSString *strQuoteNumber = [NSString stringWithFormat:@"%@",[[jsonObject objectAtIndex:2] valueForKey:@"Value"]];
                    
                    NSString *strCompleteMsg = [NSString stringWithFormat:@"Quote# %@ %@",strQuoteNumber,[arrayMsg objectAtIndex:1]];
                    
                    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
                    [userDef setValue:@"" forKey:@"imagePathQuote"];
                    [userDef synchronize];
                    
                    [global AlertMethod:@"Message" :strCompleteMsg];
                    //[self.navigationController popViewControllerAnimated:YES];
                    
                    [self goToSubWorkOrderDetailView];
                    
                });
            }
            else
            {
                [global AlertMethod:@"Message" :[[jsonObject objectAtIndex:0] valueForKey:@"Value"]];
            }
        }
        else
        {
            NSLog(@"Probably A Dictionary");
            NSDictionary *jsonDictionary=(NSDictionary *)jsonObject;
            NSLog(@"jsonDictionary %@",[jsonDictionary description]);
        }
    }
}

-(void)addKeyboardObserverForPartView
{
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}
-(void)removeKeyboardObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
- (void)keyboardDidShow: (NSNotification *) notif
{
    // Do something here
    
    CGRect frameFor_ViewAddLabor=CGRectMake(0, -150, [UIScreen mainScreen].bounds.size.width,_viewAddPart.frame.size.height);
    [_viewAddPart setFrame:frameFor_ViewAddLabor];
}

- (void)keyboardDidHide: (NSNotification *) notif
{
    // Do something here
    CGRect frameFor_ViewAddLabor=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width,_viewAddPart.frame.size.height);
    [_viewAddPart setFrame:frameFor_ViewAddLabor];
}

-(void)addPaddingInTextFields:(UITextField*)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
-(int)getSeconds:(NSString*)strTime
{
    NSArray *components1 = [strTime componentsSeparatedByString:@":"];
    
    NSInteger hours1   = [[components1 objectAtIndex:0] integerValue];
    
    NSInteger minutes1 = [[components1 objectAtIndex:1] integerValue];
    
    NSNumber *time1 = [NSNumber numberWithInteger:(hours1 * 60 * 60) + (minutes1 * 60)];
    
    return [time1 intValue];
}
- (IBAction)action_QuoteHistory:(id)sender {
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    QuoteHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"QuoteHistoryViewController"];
    objSignViewController.strWorkOrderID=strWorkOrderId;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

-(void)test{
    
    //nsloadshfaksld
    
}

- (IBAction)action_ViewMorePartDescription:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[[arrayPart objectAtIndex:row] valueForKey:@"description"]];
    [global AlertMethod:@"Part Description" :strJobDesc];
    
}

#pragma mark - part value after scanning
-(void)setpartValuesAfterScanning :(NSString *)strScannedResult{
    
    BOOL isPartPresent;
    
    isPartPresent=NO;
    // isChangeStdPartPrice=NO;
    
    [self getPartsMaster];
    
    NSDictionary *dictData;
    
    for (int k=0; k<arrDataTblView.count; k++) {
        
        dictData=[arrDataTblView objectAtIndex:k];
        
        NSString *strItemNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ItemNumber"]];
        
        if ([strScannedResult caseInsensitiveCompare:strItemNumber] == NSOrderedSame) {
            isPartPresent=YES;
            break;
        }
    }
    
    if (isPartPresent) {
        
        [_buttonSelectPart_AddPart setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        [_textFieldQuantity_AddPart setEnabled:YES];
        
        _textViewDescription_AddPart.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Description"]];
        
        float unitPriceFloat=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BestPrice"]] floatValue];
        
        _textFieldUnitPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
        // strTxtUnitPrice=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
        
        dictDataPartsSelected=dictData;
        
        NSString *strType;
        
        if (isStandard) {
            
            strType=@"Standard";
            
        } else {
            
            strType=@"Non-Standard";
            
        }
        
        if (strCategorySysNameSelected.length==0) {
            
            strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
            
        }
        
        NSString *strPartCate=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
        
        if (strPartCate.length==0) {
            
            strCategorySysNameSelected=@"";
            
        }else{
            
            strCategorySysNameSelected=strPartCate;
            
            NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
            
            for (int k=0; k<arrOfAreaMaster.count; k++) {
                
                NSDictionary *dictDataa=arrOfAreaMaster[k];
                NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
                
                NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
                NSString *strCategorySysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
                
                if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
                    
                    if ([strCategorySysName isEqualToString:strCategorySysNameSelected]) {
                        
                        [_buttonCategory_AddPart setTitle:[dictDataa valueForKey:@"Name"] forState:UIControlStateNormal];
                        strCategoryId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryMasterId"]];
                        
                    }
                    
                }
            }
            
        }
        
        NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",unitPriceFloat] :strCategorySysNameSelected :strType];
        
        float multiplier=[strMultiplier floatValue];
        
        strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];
        
        NSString *strTextQTY=[NSString stringWithFormat:@"%@",_textFieldQuantity_AddPart.text];
        
        _textFieldUnitPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",multiplier*[_textFieldUnitPrice_AddPart.text floatValue]];
        
        if (strTextQTY.length>0) {
            
            float totalPriceStandard=[strTextQTY floatValue]*multiplier*[_textFieldUnitPrice_AddPart.text floatValue];
            
            _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
            
        }else{
            
            _textFieldQuantity_AddPart.text=@"1";
            
            strTextQTY=[NSString stringWithFormat:@"%@",_textFieldQuantity_AddPart.text];
            
            float totalPriceStandard=[strTextQTY floatValue]*multiplier*[_textFieldUnitPrice_AddPart.text floatValue];
            
            _textFieldTotalPrice_AddPart.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
            
            
        }
        
    } else {
        
        [global AlertMethod:Alert :@"Part not available"];
        
    }
}

@end

