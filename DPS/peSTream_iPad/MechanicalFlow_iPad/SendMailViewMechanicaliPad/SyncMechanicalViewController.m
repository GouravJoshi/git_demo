//
//  SyncMechanicalViewController.m
//  DPS
//  peSTream
//  Created by Saavan Patidar on 28/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "SyncMechanicalViewController.h"
#import "DPS-Swift.h"

@interface SyncMechanicalViewController ()
{
    NSString *strLeadId,*strLeadName,*strServiceUrlMainServiceAutomation,*strGlobalWorkOrderId,*strCompanyKeyy,*strUserName,*strEmployeeNumber,*strEmployeeNo,*strWorkOrderType,*strNotificationTypeNameGlobal;
    NSMutableArray *arrEmail,*arrFinalJson,*arrUpdateLeadId,*arrOfSubWorkOrderId;
    UITableViewCell *cell;
    NSMutableArray *arr,*arr2;
    BOOL chk,temp,flag;
    int val;
    NSMutableArray *arrFinalSend,*arrDuplicateMail;
    
    NSMutableArray  *arrFinalLeadDetail,
    *arrFinalPaymenyInfo,
    *arrFinalImageDetail,
    *arrFinalEmailDetail,
    *arrFinalDocumentsDetail,
    *arrFinalSoldStandard,
    *arrFinalLeadPreference,
    *arrFinalSoldNonStandard;
    
    
    NSMutableDictionary *dictFinal;
    //......
    Global *global;
    
    NSMutableArray *arrImageName,*arrImageSend,*arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal;
    NSMutableArray *arrOfAllImagesToSendToServer,*arrOfAllSignatureImagesToSendToServer,*arrOfAllCheckImageToSend;
    NSString *strModifyDateToSendToServerAll;
    int indexToSendImage,indexToSendImageSign,indexToSendCheckImage;
    BOOL chkClickStatus;
    NSString *strAudioNameGlobal,*strFromEmail,*strDefaultEmployeeEmail;
    NSManagedObject *objServiceWorkOrderGlobal;
    NSDictionary *dictDetailsFortblView;
    //Nilind 24 Feb
    NSMutableArray *arrSendEmailCount;
    //End
    
    NSMutableArray *arrOfSubWorkOrderDb,*arrOfSubWorkOrderIssuesDb,*arrOfSubWorkOrderIssuesRepairDb;
    NSMutableDictionary *dictSubWorkOrderDetails,*dictSubWorkOrderIssuesDetails,*dictSubWorkOrderIssuesRepairDetails;
}

@end

@implementation SyncMechanicalViewController

- (void)viewDidLoad
{
    
    
}

-(void)methodOnLoad{
    
    _tblEmailData.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _tblEmailData.layer.borderWidth=1.0;
    _tblEmailData.layer.cornerRadius=5.0;
    
    [super viewDidLoad];
    
    indexToSendImage=0;
    indexToSendImageSign=0;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"]];
    strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    strDefaultEmployeeEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    strCompanyKeyy     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strEmployeeNumber=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    dictDetailsFortblView=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    strLeadName=[defsLead valueForKey:@"defsLeadName"];
    
    // Email Fetch
    
    // [self deleteEmailIdFromCoreData];
    // [self fetchDefaultEmailFromDb];
    
    //Yaha Call KArna hai mail k liye
    
    NSString *strServiceReportType=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportType"]];
    
    if ([strServiceReportType isEqualToString:@"CompanyEmail"]) {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportEmail"]];
        
    } else {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
        
    }
    
    _lblFromValue.text=strFromEmail;
    
    arrImageName=[[NSMutableArray alloc]init];
    //...........
    global = [[Global alloc] init];
    //...........
    
    chk=NO;temp=YES;flag=YES;
    val=0;
    
    // Mutable Array  Declaration
    
    arrEmail=[[NSMutableArray alloc]init];
    arr = [NSMutableArray array];
    arrFinalSend=[[NSMutableArray alloc]init];
    arrFinalJson=[[NSMutableArray alloc]init];
    
    dictFinal=[[NSMutableDictionary alloc]init];
    arrUpdateLeadId=[[NSMutableArray alloc]init];
    arrImageSend=[[NSMutableArray alloc]init];
    arrOfAllImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllCheckImageToSend=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    //..........................................................
    
    
    
    // [self serviceEmailFetch];
    
    //........................................................
    
    // [_tblEmailData reloadData];
    
    NSUserDefaults *defsIsService=[NSUserDefaults standardUserDefaults];
    [defsIsService setBool:YES forKey:@"isServiceSurvey"];
    [defsIsService setBool:YES forKey:@"isMechanicalSurvey"];
    [defsIsService setBool:NO forKey:@"isNewSalesSurvey"];
    
    [defsIsService synchronize];
    
    [self finalMails];
    
    //    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    //    BOOL isFromSurvey=[defsss boolForKey:@"isFromSurveyToSendEmail"];
    //    if (isFromSurvey) {
    //
    //        UIView *viewTemp=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    //        [viewTemp setBackgroundColor:[UIColor whiteColor]];
    //        [self.view addSubview:viewTemp];
    //
    //        [defsss setBool:NO forKey:@"isFromSurveyToSendEmail"];
    //        [defsss synchronize];
    //
    //        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    //        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    //        if (netStatusWify1== NotReachable)
    //        {
    //            [DejalBezelActivityView removeView];
    //            [self performSelector:@selector(goToAppointmentView) withObject:nil afterDelay:1.0];
    //            // [self goToAppointmentView];
    //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Data saved offline" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    //            [alert show];
    //        }
    //        else
    //        {
    //            [DejalBezelActivityView activityViewForView:viewTemp withLabel:@"Sending To Server..."];
    //            [self performSelector:@selector(FetchFromCoreDataToSendServiceDynamic) withObject:nil afterDelay:1.0];
    //            //[self FetchFromCoreDataToSendServiceDynamic];
    //
    //        }
    //    }
    
    if ([_isCustomerPresent isEqualToString:@"no"]) {
        
        [_btnSurvey setHidden:YES];
        
    } else {
        
        [_btnSurvey setHidden:NO];
        
    }
    
    NSUserDefaults *defsSurvey=[NSUserDefaults standardUserDefaults];
    
    BOOL yesSurvey=[defsSurvey boolForKey:@"YesSurveyService"];
    if (yesSurvey) {
        [_btnSurvey setHidden:NO];
    } else {
        [_btnSurvey setHidden:YES];
    }
    
    //Comment Karna Hai
    // [self fetchWorkOrderDetailFromDB];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionOnAdd:(id)sender
{
    if (_txtEmailId.text.length==0)
        
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        BOOL chkEmail;
        chkEmail=NO;
        for (int i=0; i<arrEmail.count; i++)
        {
            if ([_txtEmailId.text isEqualToString:[arrEmail objectAtIndex:i]])
            {
                chkEmail=YES;
            }
        }
        if (chkEmail==YES)
        {
            chkEmail=NO;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Email Id already exist" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            BOOL emailChk;
            emailChk=[self NSStringIsValidEmail:_txtEmailId.text];
            if (emailChk==NO)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            else
            {
                
                /*chk=YES;
                 [arrEmail addObject:_txtEmailId.text];
                 [arr addObject:[NSString stringWithFormat:@"%lu",(unsigned long)arrEmail.count-1]];
                 [self salesEmailFetchForSave];
                 [_tblEmailData reloadData];
                 [_tblEmailData scrollRectToVisible:CGRectMake(_tblEmailData.contentSize.width - 1,_tblEmailData.contentSize.height - 1, 1, 1) animated:YES];
                 _txtEmailId.text=@"";*/
                chk=YES;
                [arrEmail addObject:_txtEmailId.text];
                [arr addObject:[NSString stringWithFormat:@"%lu",(unsigned long)arrEmail.count-1]];
                [_txtEmailId resignFirstResponder];
                [self salesEmailFetchForSave];
                [_tblEmailData scrollRectToVisible:CGRectMake(_tblEmailData.contentSize.width - 1,_tblEmailData.contentSize.height - 1, 1, 1) animated:YES];
                _txtEmailId.text=@"";
            }
        }
    }
    [self finalMails];
}

- (IBAction)actionOnCancel:(id)sender
{
    [self goToAppointmentView];
}
- (IBAction)actionOnSendEmail:(id)sender
{
    // [self salesEmailFetchForSave];
    //Nilind 24 Feb
    [self emailCountFetch];
    if (arrSendEmailCount.count==0)
    {
        [global AlertMethod:@"Alert!" :@"No email to send"];
    }
    
    //End
    else
    {
        
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            [DejalBezelActivityView removeView];
            [self goToAppointmentView];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Data saved offline" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            
            [self sendingWorkOrderToServer];
            
            //[self FetchFromCoreDataToSendServiceDynamic];
            
        }
    }
}
- (IBAction)actionOnContinueToSetup:(id)sender
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        
        [self goToBeginAuditView];
        
    }
    
    
}
-(void)goToBeginAuditView{
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        if([UIScreen mainScreen].bounds.size.height == 480.0)
        {
            //move to your iphone5 xib
            BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView" bundle:nil];
            [self.navigationController pushViewController:bav animated:YES];
        }
        else{
            //move to your iphone4s xib
            BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView_iphone5" bundle:nil];
            [self.navigationController pushViewController:bav animated:YES];
        }
    }
    else{
        //BeginAuditView_iPad
        BeginAuditViewiPad *bav = [[BeginAuditViewiPad alloc] initWithNibName:@"BeginAuditView_iPad" bundle:nil];
        [self.navigationController pushViewController:bav animated:NO];
    }
    
    /*
     if([UIScreen mainScreen].bounds.size.height == 480.0)
     {
     //move to your iphone5 xib
     BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView" bundle:nil];
     [self.navigationController pushViewController:bav animated:YES];
     }
     else{
     //move to your iphone4s xib
     BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView_iphone5" bundle:nil];
     [self.navigationController pushViewController:bav animated:YES];
     }
     */
    
}

-(void)sendingWorkOrderToServer{
    
    [self fetchWorkOrderDetailFromDB];
    
    NSLog(@"Final Dictionary >>>>>>%@",dictFinal);
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        if (arrOfAllImagesToSendToServer.count==0) {
            
            [self uploadingAllImages:0];
            
        } else {
            
            [self uploadingAllImages:0];
            
        }
    }
    
}
-(void)FetchFromCoreDataToSendServiceDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];//ServiceDynamicForm
    entityServiceDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    requestNewServiceDynamic = [[NSFetchRequest alloc] init];
    [requestNewServiceDynamic setEntity:entityServiceDynamic];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",_strWorkOrderId];
    
    [requestNewServiceDynamic setPredicate:predicate];
    
    sortDescriptorServiceDynamic = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptorsServiceDynamic = [NSArray arrayWithObject:sortDescriptorServiceDynamic];
    
    [requestNewServiceDynamic setSortDescriptors:sortDescriptorsServiceDynamic];
    
    self.fetchedResultsControllerServiceDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewServiceDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceDynamic performFetch:&error];
    arrAllObjServiceDynamic = [self.fetchedResultsControllerServiceDynamic fetchedObjects];
    if ([arrAllObjServiceDynamic count] == 0)
    {
        
        [self sendingWorkOrderToServer];
        
    }
    else
    {
        matchesServiceDynamic=arrAllObjServiceDynamic[0];
        NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
        
        NSDictionary *dictDataService;
        
        if ([arrTemp isKindOfClass:[NSDictionary class]]) {
            
            dictDataService=(NSDictionary*)arrTemp;
            
        } else {
            
            dictDataService=arrTemp[0];
            
        }
        
        [self sendingFinalDynamicJsonToServerService:dictDataService];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)sendingFinalDynamicJsonToServerService :(NSDictionary*)dictFinalDynamicJson{
    // NSDictionary *dictFinalDynamicJson=(NSDictionary*)arrTemp;
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL Service FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlServiceDynamicFormSubmission];
    
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 // [DejalBezelActivityView removeView];
                 if (success)
                 {
                     //                     NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
                     //
                     //                     BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
                     //
                     //                     if (isEquipEnabled) {
                     //
                     //                         [self FetchFromCoreDataToSendEquipmentDynamicForms];
                     //
                     //                     }else{
                     
                     [self sendingWorkOrderToServer];
                     
                     //                     }
                     
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     if ([str containsString:@"Success"]) {
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                     }else{
                         
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}
//New Change For EquipMent List

-(void)FetchFromCoreDataToSendEquipmentDynamicForms{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityServiceDynamic=[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context];
    requestNewServiceDynamic = [[NSFetchRequest alloc] init];
    [requestNewServiceDynamic setEntity:entityServiceDynamic];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    
    [requestNewServiceDynamic setPredicate:predicate];
    
    sortDescriptorServiceDynamic = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsServiceDynamic = [NSArray arrayWithObject:sortDescriptorServiceDynamic];
    
    [requestNewServiceDynamic setSortDescriptors:sortDescriptorsServiceDynamic];
    
    self.fetchedResultsControllerServiceDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewServiceDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceDynamic performFetch:&error];
    arrAllObjServiceDynamic = [self.fetchedResultsControllerServiceDynamic fetchedObjects];
    if ([arrAllObjServiceDynamic count] == 0)
    {
        
        [self methodOnResponse];
        
    }
    else
    {
        
        NSMutableArray *arrOfEquipToSend=[[NSMutableArray alloc] init];
        
        for (int k=0; k<arrAllObjServiceDynamic.count; k++) {
            
            matchesServiceDynamic=arrAllObjServiceDynamic[k];
            NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
            
            NSDictionary *dictDataService;
            
            if ([arrTemp isKindOfClass:[NSDictionary class]]) {
                
                dictDataService=(NSDictionary*)arrTemp;
                
            } else {
                
                dictDataService=arrTemp[0];
                
            }
            
            [arrOfEquipToSend addObject:dictDataService];
            
            
        }
        
        //        matchesServiceDynamic=arrAllObjServiceDynamic[0];
        //        NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
        //
        //        NSDictionary *dictDataService;
        //
        //        if ([arrTemp isKindOfClass:[NSDictionary class]]) {
        //
        //            dictDataService=(NSDictionary*)arrTemp;
        //
        //        } else {
        //
        //            dictDataService=arrTemp[0];
        //
        //        }
        
        if (arrOfEquipToSend.count==0) {
            
            [self methodOnResponse];
            
            
        } else {
            
            [self sendingFinalDynamicJsonToServerEquipmentDynamicForm:arrOfEquipToSend];
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)sendingFinalDynamicJsonToServerEquipmentDynamicForm :(NSArray*)arrFinalDynamicJson{
    // NSDictionary *dictFinalDynamicJson=(NSDictionary*)arrTemp;
    
    
    NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                       arrFinalDynamicJson,nil];
    
    NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                     @"EquipmentInspectionFormsData",nil];
    
    NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL Service FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlEquipmentDynamicFormSubmission];
    
    NSLog(@"Equpment Sending URL=====%@",strUrl);
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 // [DejalBezelActivityView removeView];
                 if (success)
                 {
                     
                     [self methodOnResponse];
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}

-(void)methodOnResponse{
    
    [objServiceWorkOrderGlobal setValue:@"no" forKey:@"zSync"];
    NSError *error;
    [context save:&error];
    
}
// 2nd Sept
#pragma mark -SalesAuto Fetch Core Data

-(void)serviceEmailFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",_strWorkOrderId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    arrEmail=[[NSMutableArray alloc]init];
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrEmail addObject:[matches valueForKey:@"emailId"]];
            [arr addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
        
    }
    
    chk=YES;
    
    [_tblEmailData reloadData];
    
}

#pragma mark- Table Delegate Method

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrAllObj.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*    static NSString *identifier=@"cell";
     
     cell=[tableView dequeueReusableCellWithIdentifier:identifier];
     if (cell==nil)
     {
     cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
     }
     
     NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
     
     cell.textLabel.text=[arrEmail objectAtIndex:indexPath.row ];
     
     long index=100000;
     for (int k=0; k<arr.count; k++)
     {
     if ([arr containsObject:str]) {
     index=indexPath.row;
     break;
     }
     }
     
     if (indexPath.row==index)
     {
     [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
     } else
     {
     [cell setAccessoryType:UITableViewCellAccessoryNone];
     }
     return cell;*/
    static NSString *identifier=@"cell";
    
    cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    matches=[arrAllObj objectAtIndex:indexPath.row];
    
    cell.textLabel.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"emailId"]];
    
    if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isMailSent"]] isEqualToString:@"true"])
    {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else
    {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*    NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
     UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
     if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
     {
     [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
     [arr addObject:[NSString stringWithFormat:@"%lu",(unsigned long)indexPath.row]];
     }
     else
     {
     [cellNew setAccessoryType:UITableViewCellAccessoryNone];
     NSString *str=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
     [arr removeObject:str];
     
     }*/
    [self serviceEmailFetch];
    
    NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
    UITableViewCell *cellNew = [[tableView visibleCells] objectAtIndex:index];
    if ([cellNew accessoryType] == UITableViewCellAccessoryNone)
    {
        [cellNew setAccessoryType:UITableViewCellAccessoryCheckmark];
        chkClickStatus=YES;
    }
    else
    {
        [cellNew setAccessoryType:UITableViewCellAccessoryNone];
        chkClickStatus=NO;
    }
    NSManagedObject *record = [arrAllObj objectAtIndex:indexPath.row];
    [self updateMailStatus:record];
    [global updateSalesModifydate:_strWorkOrderId];
}
-(void)finalMails
{
    //    arrFinalSend=[[NSMutableArray alloc]init];
    //    for (int i=0; i<arr.count; i++)
    //    {
    //        int value;
    //        value=[[arr objectAtIndex:i]intValue];
    //        [arrFinalSend addObject:[arrEmail objectAtIndex:value]];
    //
    //    }
    //    NSLog(@"ArrFinalSend >>%@",arrFinalSend);
}



#pragma mark- fetching for service Automation

-(void)fetchingModifyDate{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityServiceModifyDate=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityServiceModifyDate];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
    }
    else
    {
        matchesServiceModifyDate=arrAllObjServiceModifyDate[0];
        strModifyDateToSendToServerAll=[matchesServiceModifyDate valueForKey:@"modifyDate"];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}

-(void)ServiceEmailFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",_strWorkOrderId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrEmail addObject:[matches valueForKey:@"emailId"]];
            [arr addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
        
    }
    chk=YES;
    [_tblEmailData reloadData];
    
}

#pragma mark- FINAL JSON SENDING

-(void)fialJson
{
    ///api/MobileToSaleAuto/UpdateLeadDetail
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dictFinal])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictFinal options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Final Mechanical  JSON: %@", jsonString);
            }
            
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@?EmployeeEmail=%@",strServiceUrlMainServiceAutomation,UrlUpdateMechanicalWorkOrder,strFromEmail];
        
        //============================================================================
        //============================================================================
        NSDictionary *dictTemp;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"serviceOrder" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     //Sending Dynamic Json
                     
                     //[DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         NSString *strResult=[NSString stringWithFormat:@"%@",[response valueForKey:@"Result"]];
                         
                         if (strResult.length==0) {
                             
                             if ([[response valueForKey:@"WorkorderDetail"] isKindOfClass:[NSString class]] || [[response valueForKey:@"SubWorkOrderExtSerDcs"] isKindOfClass:[NSString class]]) {
                                 
                                 //Error Hai Uploading mai
                                 
                                 NSString *strTitle = Alert;
                                 // NSString *strMsg = Sorry;
                                 //[global AlertMethod:strTitle :strResult];
                                 
                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"MechanicalSync" object:self];
                                 
                             }else{
                                 
                                 //Update SuccessFully
                                 
                                 [self deleteAllFromDB];
                                 
                                 [self saveMechanicalWorkOrderInDb:response];
                                 
                                 [self methodOnResponse];
                                 
                                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                 NSArray *arrOfLeadsTosendToServer=[defs valueForKey:@"sendServiceLeadToServerMechanical"];
                                 
                                 NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                                 
                                 [arrTemp addObjectsFromArray:arrOfLeadsTosendToServer];
                                 
                                 [arrTemp removeObjectAtIndex:0];
                                 
                                 arrOfLeadsTosendToServer=arrTemp;
                                 
                                 [defs setObject:arrOfLeadsTosendToServer forKey:@"sendServiceLeadToServerMechanical"];
                                 [defs synchronize];
                                 
                                 [self sendingMechanicalWorkOrderToServer];
                                 
                             }
                             
                         } else {
                             
                             //Error Hai Uploading mai
                             
                             NSString *strTitle = Alert;
                             // NSString *strMsg = Sorry;
                             [global AlertMethod:strTitle :strResult];
                             
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"MechanicalSync" object:self];
                         }
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
    }
}


#pragma mark- FINAL IMAGE SEND

-(void)uploadingAllImages :(int)indexToSendimages{
    
    if (arrOfAllImagesToSendToServer.count==indexToSendimages) {
        
        [self uploadingAllImagesSignature:0];
        
    } else {
        
        [self uploadImage:arrOfAllImagesToSendToServer[indexToSendimages]];
        
    }
}
-(void)uploadingAllImagesSignature :(int)indexToSendimages{
    
    if (arrOfAllSignatureImagesToSendToServer.count==indexToSendimages) {
        
        [self uploadingAllImagesCheckImages:0];
        
    } else {
        
        [self uploadImageSignature:arrOfAllSignatureImagesToSendToServer[indexToSendimages]];
        
    }
}
-(void)uploadingAllImagesCheckImages :(int)indexToSendimages{
    
    if (arrOfAllCheckImageToSend.count==indexToSendimages) {
        
        if (strAudioNameGlobal.length==0 || [strAudioNameGlobal isEqualToString:@"(null)"]) {
            
            [self fialJson];
            
        } else {
            
            [self uploadAudio:strAudioNameGlobal];
            
        }
        
        
    } else {
        
        [self uploadImageCheckImage:arrOfAllCheckImageToSend[indexToSendimages]];
        
    }
}

//============================================================================
//============================================================================
#pragma mark- Upload Image METHOD
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
            
        }
        
    }
    
    indexToSendImage++;
    
    [self uploadingAllImages:indexToSendImage];
    
}

-(void)uploadImageSignature :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
            
        }
        
    }
    
    indexToSendImageSign++;
    [self uploadingAllImagesSignature:indexToSendImageSign];
}

-(void)uploadImageCheckImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
            
        }
        
    }
    
    indexToSendCheckImage++;
    [self uploadingAllImagesCheckImages:indexToSendCheckImage];
}


//============================================================================
//============================================================================
#pragma mark- Upload Audio METHOD
//============================================================================
//============================================================================


-(void)uploadAudio :(NSString*)strAudioName
{
    if ((strAudioName.length==0) && [strAudioName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSRange equalRange = [strAudioName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strAudioName = [strAudioName substringFromIndex:equalRange.location + equalRange.length];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlAudioUploadAsync];
        
        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strAudioName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Audio Sent");
        }
        
    }
    [self fialJson];
    
}

//============================================================================
//============================================================================
#pragma mark- TEXT FIELD DELEGATE
//============================================================================
//============================================================================
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtEmailId resignFirstResponder];
    return YES;
}


#pragma mark- SAVE EMAIL TO CORE DATA


-(void)salesEmailFetchForSave
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",_strWorkOrderId];
    [requestNew setPredicate:predicate];
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [requestNew setSortDescriptors:sortDescriptors];
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrDuplicateMail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            [arrDuplicateMail addObject:[matches valueForKey:@"emailId"]];
        }
    }
    NSMutableArray *arrFinalMail;
    arrFinalMail=[[NSMutableArray alloc]init];
    arrFinalMail=arrEmail;//arrFinalSend;
    for (int i=0; i<arrFinalMail.count; i++)
    {
        for (int j=0; j<arrDuplicateMail.count; j++)
        {
            if ([[arrFinalMail objectAtIndex:i] isEqualToString:[arrDuplicateMail objectAtIndex:j]])
            {
                [arrFinalMail removeObjectAtIndex:i];
            }
        }
    }
    [self saveEmailToCoreData:arrFinalMail];
    
}

-(void)saveEmailToCoreData:(NSMutableArray *)arrEmailDetail
{
    for (int j=0; j<arrEmailDetail.count; j++)
    {
        //Email Detail Entity
        entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
        objEmailDetail.createdBy=@"0";;
        
        objEmailDetail.createdDate=@"0";
        
        objEmailDetail.emailId=[arrEmailDetail objectAtIndex:j];
        
        objEmailDetail.isCustomerEmail=@"true";
        
        objEmailDetail.isMailSent=@"true";
        
        objEmailDetail.woInvoiceMailId=@"";
        
        objEmailDetail.modifiedBy=@"";
        
        objEmailDetail.modifiedDate=[global modifyDate];
        
        objEmailDetail.subject=@"";
        
        objEmailDetail.workorderId=_strWorkOrderId;
        
        NSError *error1;
        
        [context save:&error1];
        
    }
    [self serviceEmailFetch];
}
//Nilind 17 Oct
-(void)updateMailStatus:(NSManagedObject *)indexValue
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    
    NSError *error1 = nil;
    matches=indexValue;
    if (chkClickStatus==YES)
    {
        [matches setValue:@"true" forKey:@"isMailSent"];
    }
    else
    {
        [matches setValue:@"false" forKey:@"isMailSent"];
    }
    [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
    [context save:&error1];
    [self serviceEmailFetch];
}

//....................................

-(void)fetchTempEmail
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    context = [appDelegate managedObjectContext];
    
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    
    requestNew = [[NSFetchRequest alloc] init];
    
    [requestNew setEntity:entityEmailDetailServiceAuto];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",_strWorkOrderId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
    }else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
        }
    }
}

//....................................
//....................................
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
//------------------------------------------
-(void)goToAppointmentView{
    
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
        AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
        AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
    
}
//Nilind 24 Feb
-(void)emailCountFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",_strWorkOrderId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjNew;
    arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesNew;
    arrSendEmailCount=[[NSMutableArray alloc]init];
    if (arrAllObjNew.count==0)
    {
        
    }else
    {
        for (int i=0; i<arrAllObjNew.count; i++)
        {
            matchesNew=arrAllObjNew[i];
            if([[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"isMailSent"]]isEqualToString:@"true"] )
            {
                // [arrSendEmailCount addObject:@"%@",[NSString stringWithFormat:@"%d",i]];
                [arrSendEmailCount addObject:[NSString  stringWithFormat:@"%d",i] ];
            }
            
        }
        
        
    }
    
}
//End

- (IBAction)action_Back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

///New Change For Email Customs

-(void)deleteEmailIdFromCoreData{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND isDefaultEmail=%@",_strWorkOrderId,@"true"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}
-(void)saveEmailToCoreDataDefaults:(NSMutableArray *)arrEmailDetail :(NSMutableArray *)arrOfDefaultsEmailsInvoiceId
{
    for (int j=0; j<arrEmailDetail.count; j++)
    {
        //Email Detail Entity
        entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
        objEmailDetail.createdBy=@"0";;
        
        objEmailDetail.createdDate=@"0";
        
        objEmailDetail.emailId=[arrEmailDetail objectAtIndex:j];//arrOfDefaultsEmailsInvoiceId
        
        objEmailDetail.isCustomerEmail=@"true";
        
        objEmailDetail.isMailSent=@"false";
        
        objEmailDetail.woInvoiceMailId=[arrOfDefaultsEmailsInvoiceId objectAtIndex:j];
        
        objEmailDetail.modifiedBy=@"";
        
        objEmailDetail.modifiedDate=[global modifyDate];
        
        objEmailDetail.subject=@"";
        
        objEmailDetail.workorderId=_strWorkOrderId;
        
        objEmailDetail.isDefaultEmail=@"true";
        
        NSError *error1;
        
        [context save:&error1];
        
    }
}
-(void)saveEmployeeEmailDefaultsTrue{
    
    //Email Detail Entity
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
    objEmailDetail.createdBy=@"0";;
    
    objEmailDetail.createdDate=@"0";
    
    objEmailDetail.emailId=strDefaultEmployeeEmail;
    
    objEmailDetail.isCustomerEmail=@"true";
    
    objEmailDetail.isMailSent=@"true";
    
    objEmailDetail.woInvoiceMailId=@"";
    
    objEmailDetail.modifiedBy=@"";
    
    objEmailDetail.modifiedDate=[global modifyDate];
    
    objEmailDetail.subject=@"";
    
    objEmailDetail.workorderId=_strWorkOrderId;
    
    objEmailDetail.isDefaultEmail=@"true";
    
    NSError *error1;
    
    [context save:&error1];
    
}

-(void)fetchDefaultEmailFromDb{
    
    NSArray *arrOfDefaultEmailsDB=[dictDetailsFortblView valueForKey:@"DefaultEmails"];
    
    if ([arrOfDefaultEmailsDB isKindOfClass:[NSArray class]]) {
        
        NSMutableArray *arrOfDefaultsEmails=[[NSMutableArray alloc]init];
        NSMutableArray *arrOfDefaultsEmailsInvoiceId=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfDefaultEmailsDB.count; k++) {
            
            NSDictionary *dictData=arrOfDefaultEmailsDB[k];
            
            NSString *strEmailIds=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EmailId"]];
            if ([strEmailIds isEqualToString:@"##EmployeeEmail##"]) {
                
                [self saveEmployeeEmailDefaultsTrue];
                
            } else {
                
                [arrOfDefaultsEmails addObject:strEmailIds];
                [arrOfDefaultsEmailsInvoiceId addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WoInvoiceMailId"]]];
                
            }
        }
        
        [self saveEmailToCoreDataDefaults:arrOfDefaultsEmails :arrOfDefaultsEmailsInvoiceId];
        
    } else {
        
    }
}


//============================================================================
//============================================================================
#pragma mark- ----------------Core Data Fetch Methods----------------
//============================================================================
//============================================================================

-(void)sendingMechanicalWorkOrderToServer{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"]];
    strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    strDefaultEmployeeEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    strCompanyKeyy     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strEmployeeNumber=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    dictDetailsFortblView=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    strLeadName=[defsLead valueForKey:@"defsLeadName"];
    
    // Email Fetch
    
    // [self deleteEmailIdFromCoreData];
    // [self fetchDefaultEmailFromDb];
    
    //Yaha Call KArna hai mail k liye
    
    NSString *strServiceReportType=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportType"]];
    
    if ([strServiceReportType isEqualToString:@"CompanyEmail"]) {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportEmail"]];
        
    } else {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
        
    }
    
    [self methodOnLoad];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSMutableArray *arrOfLeadsTosendToServer=[defs valueForKey:@"sendServiceLeadToServerMechanical"];
    
    if (arrOfLeadsTosendToServer.count==0) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MechanicalSync" object:self];
        
    } else {
        
        _strWorkOrderId=arrOfLeadsTosendToServer[0];
        
      //  [self  sendingWorkOrderToServer];
        
        [self fetchMechanicalDynamicForm];
        
    }
    
}

-(void)fetchWorkOrderDetailFromDB
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",_strWorkOrderId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"WorkorderDetail"];
        
    }else
    {
        matches=arrAllObj[0];
        objServiceWorkOrderGlobal=nil;
        objServiceWorkOrderGlobal=matches;
        
        NSArray *arrLeadDetailKey;
        NSMutableArray *arrLeadDetailValue;
        arrLeadDetailValue=[[NSMutableArray alloc]init];
        arrLeadDetailKey = [[[matches entity] attributesByName] allKeys];
        
        //            NSString *strCustomerSign=[matches valueForKey:@"CustomerSignaturePath"];
        //            NSString *strTechSign=[matches valueForKey:@"TechnicianSignaturePath"];
        //            strAudioNameGlobal=[matches valueForKey:@"audioFilePath"];
        //
        //            if (!(strCustomerSign.length==0)) {
        //                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
        //            }
        //
        //            if (!(strTechSign.length==0)) {
        //                [arrOfAllSignatureImagesToSendToServer addObject:strTechSign];
        //            }
        
        for (int i=0; i<arrLeadDetailKey.count; i++)
        {
            NSString *str;
            str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
            if ([str isKindOfClass:[NSDate class]]) {
                
                
            }else if ([str isKindOfClass:[NSArray class]]) {
                
                
            }
            else{
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
            }
            [arrLeadDetailValue addObject:str];
        }
        
        int indexToDeleteZDate = 0,indexToDeleteDateModified = 0;
        
        for (int k=0; k<arrLeadDetailKey.count; k++) {
            
            if ([arrLeadDetailKey[k] isEqualToString:@"dateModified"]) {
                
                indexToDeleteZDate=k;
                
            }
            if ([arrLeadDetailKey[k] isEqualToString:@"zdateScheduledStart"]) {
                
                indexToDeleteDateModified=k;
                
            }
            
        }
        
        NSMutableArray *tempArr=[[NSMutableArray alloc]init];
        [tempArr addObjectsFromArray:arrLeadDetailKey];
        
        [tempArr removeObjectAtIndex:indexToDeleteZDate];
        [tempArr removeObjectAtIndex:indexToDeleteDateModified-1];
        
        [arrLeadDetailValue removeObjectAtIndex:indexToDeleteZDate];
        [arrLeadDetailValue removeObjectAtIndex:indexToDeleteDateModified-1];
        
        arrLeadDetailKey=tempArr;
        
        NSDictionary *dictLeadDetail;
        dictLeadDetail = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
        
        //IsNPMATermite
        
        NSString *strBoolKey = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKey:@"isNPMATermite"]];
        
        BOOL isTrue = [strBoolKey boolValue];
        
        if (isTrue) {
            
            [dictLeadDetail setValue:@"true" forKey:@"isNPMATermite"];
            
        } else {
            
            [dictLeadDetail setValue:@"false" forKey:@"isNPMATermite"];
            
        }
        
        [dictLeadDetail setValue:@"false" forKey:@"isMailSent"];

        [dictFinal setObject:dictLeadDetail forKey:@"WorkorderDetail"];
        
        [self fetchImageDetailFromDb];
        
        [self fetchEmailDetailDB];
        
        [self fetchSubWorkOrderFromDB];
        
        [self fetchSubWorkOrderMechanicalEquipmentFromDB :_strWorkOrderId];

        // Akshay
        [self fetchAccountDiscountExtSerDcs];
        
    }
    
    /*
     //Temporary hai Comment karna
     
     NSError *errorNew = nil;
     NSData *json;
     NSString *jsonString;
     // Dictionary convertable to JSON ?
     if ([NSJSONSerialization isValidJSONObject:dictFinal])
     {
     // Serialize the dictionary
     json = [NSJSONSerialization dataWithJSONObject:dictFinal options:NSJSONWritingPrettyPrinted error:&errorNew];
     
     // If no errors, let's view the JSON
     if (json != nil && errorNew == nil)
     {
     jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
     NSLog(@"Final Mechanical JSON: %@", jsonString);
     }
     }
     */
    
}

-(void)fetchImageDetailFromDb
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetailServiceAuto= [NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityImageDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",_strWorkOrderId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSString *strImageName=[matches valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            [arrImageDetail addObject:dictTemp];
        }
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
        
    }
}

-(void)fetchEmailDetailDB
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",_strWorkOrderId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}

-(void)fetchSubWorkOrderFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictFinal setObject:arrEmailDetail forKey:@"SubWorkOrderExtSerDcs"];
        
    }
    else
    {
        arrOfSubWorkOrderDb=[[NSMutableArray alloc]init];
        dictSubWorkOrderDetails=[[NSMutableDictionary alloc]init];
        
        for (int j=0; j<arrAllObjMechanicalSubWorkOrder.count; j++) {
            
            NSManagedObject *objTempSubWorkOrder=arrAllObjMechanicalSubWorkOrder[j];
            //subWorkOrderId
            
            //strWorkOrderType
            strWorkOrderType=[NSString stringWithFormat:@"%@",[objTempSubWorkOrder valueForKey:@"SubWOType"]];
            
            NSString *strStatus=[NSString stringWithFormat:@"%@",[objTempSubWorkOrder valueForKey:@"subWOStatus"]];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrder entity] attributesByName] allKeys];
            
            NSString *strCustomerSign;//=[objTempSubWorkOrder valueForKey:@"CustomerSignaturePath"];
            NSString *strTechSign;//=[objTempSubWorkOrder valueForKey:@"TechnicianSignaturePath"];
            
            BOOL isCompletedStatusMechanical=[global isCompletedSatusMechanical:strStatus];

            
            if (isCompletedStatusMechanical) {
                
                strCustomerSign=[objTempSubWorkOrder valueForKey:@"completeSWO_CustSignPath"];
                strTechSign=[objTempSubWorkOrder valueForKey:@"completeSWO_TechSignPath"];
                
            } else {
                
                strCustomerSign=[objTempSubWorkOrder valueForKey:@"CustomerSignaturePath"];
                strTechSign=[objTempSubWorkOrder valueForKey:@"TechnicianSignaturePath"];
                
            }
            
            //strAudioNameGlobal=[objTempSubWorkOrder valueForKey:@"audioFilePath"];
            
            if (!(strCustomerSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
            }
            
            if (!(strTechSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strTechSign];
            }
            
            //            //Completed k case mai y wali image bhejna hai
            //            NSString *strCustomerSignCompleted=[objTempSubWorkOrder valueForKey:@"completeSWO_CustSignPath"];
            //            NSString *strTechSignCompleted=[objTempSubWorkOrder valueForKey:@"completeSWO_TechSignPath"];
            //
            //            if (!(strCustomerSignCompleted.length==0)) {
            //                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerSignCompleted];
            //            }
            //
            //            if (!(strTechSignCompleted.length==0)) {
            //                [arrOfAllSignatureImagesToSendToServer addObject:strTechSignCompleted];
            //            }
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrder valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrder valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            dictSubWorkOrderDetails = [NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [self fetchPaymentInfoFromDB:[objTempSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            [self fetchSubWorkOrderIssuesFromDB:[objTempSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            [arrOfSubWorkOrderDb addObject:dictSubWorkOrderDetails];
        }
        
        [dictFinal setObject:arrOfSubWorkOrderDb forKey:@"SubWorkOrderExtSerDcs"];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)fetchPaymentInfoFromDB :(NSString*)strSubWorkOrderIdToFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfoServiceAuto= [NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfoServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdToFetch];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        NSMutableArray *arrPaymentInfoValue;
        
        arrPaymentInfoValue=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:arrPaymentInfoValue forKey:@"PaymentInfo"];
        
        [dictSubWorkOrderDetails setObject:arrPaymentInfoValue forKey:@"SubWOPaymentDetailDcs"];
        
    }else
    {
        
        NSMutableArray *arrPaymentInfoValue1;
        
        arrPaymentInfoValue1=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObj.count; k++) {
            
            matches=arrAllObj[k];
            
            NSString *strCheckFrontImage=[matches valueForKey:@"checkFrontImagePath"];
            NSString *strCheckBackImage=[matches valueForKey:@"checkBackImagePath"];
            
            if (!(strCheckFrontImage.length==0)) {
                
                [arrOfAllCheckImageToSend addObject:strCheckFrontImage];//salesSignature
                
            }
            if (!(strCheckBackImage.length==0)) {
                
                [arrOfAllCheckImageToSend addObject:strCheckBackImage];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            [arrPaymentInfoValue1 addObject:dictPaymentInfo];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrPaymentInfoValue1 forKey:@"SubWOPaymentDetailDcs"];

        
        /*
        matches=arrAllObj[0];
        
        NSString *strCheckFrontImage=[matches valueForKey:@"CheckFrontImagePath"];
        NSString *strCheckBackImage=[matches valueForKey:@"CheckBackImagePath"];
        
        if (!(strCheckFrontImage.length==0)) {
            
            [arrOfAllCheckImageToSend addObject:strCheckFrontImage];//salesSignature
            
        }
        if (!(strCheckBackImage.length==0)) {
            
            [arrOfAllCheckImageToSend addObject:strCheckBackImage];//salesSignature
            
        }
        
        NSArray *arrPaymentInfoKey;
        NSMutableArray *arrPaymentInfoValue;
        
        arrPaymentInfoValue=[[NSMutableArray alloc]init];
        arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
        
        for (int i=0; i<arrPaymentInfoKey.count; i++)
        {
            NSString *str;
            str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
            if([str isEqual:nil] || str.length==0 )
            {
                str=@"";
            }
            [arrPaymentInfoValue addObject:str];
        }
        
        NSDictionary *dictPaymentInfo;
        dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
        
        NSMutableArray *arrPaymentInfoValue1;
        
        arrPaymentInfoValue1=[[NSMutableArray alloc]init];
        
        [arrPaymentInfoValue1 addObject:dictPaymentInfo];
        
        [dictSubWorkOrderDetails setObject:arrPaymentInfoValue1 forKey:@"SubWOPaymentDetailDcs"];
        
         */
        
        
    }
}

-(void)fetchSubWorkOrderIssuesFromDB :(NSString*)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssues = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssues setEntity:entitySubWorkOrderIssues];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderIssues setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssues = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssues = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssues];
    
    [requestSubWorkOrderIssues setSortDescriptors:sortDescriptorsSubWorkOrderIssues];
    
    self.fetchedResultsControllerSubWorkOrderIssues = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssues managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssues setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssues performFetch:&error];
    arrAllObjSubWorkOrderIssues = [self.fetchedResultsControllerSubWorkOrderIssues fetchedObjects];
    if ([arrAllObjSubWorkOrderIssues count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWorkOrderIssueDcs"];
        
        
    }
    else
    {
        
        arrOfSubWorkOrderIssuesDb=[[NSMutableArray alloc]init];
        dictSubWorkOrderIssuesDetails=[[NSMutableDictionary alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssues.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssues=arrAllObjSubWorkOrderIssues[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssues entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssues valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssues valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            dictSubWorkOrderIssuesDetails = [NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            if ([strWorkOrderType isEqualToString:@"TM"]) {
                
                [dictSubWorkOrderIssuesDetails setObject: [dictSubWorkOrderIssuesDetails objectForKey: @"subWOIssueRepairPartDcs"] forKey: @"SubWOIssueRepairDcs"];
                
                [dictSubWorkOrderIssuesDetails removeObjectForKey: @"subWOIssueRepairPartDcs"];
                
                [self fetchSubWorkOrderIssuesPartsFromDB:strSubWorkOrderIdToFetch :[objTempSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
                
            } else {
                
                [self fetchSubWorkOrderIssuesRepairsFromDB:strSubWorkOrderIdToFetch :[objTempSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
                
            }
            
            //Yaha Par Db Se Fetch Karna Issues k Repairs
            //  [self fetchSubWorkOrderIssuesRepairsFromDB:strSubWorkOrderIdToFetch :[objTempSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
            
            [arrOfSubWorkOrderIssuesDb addObject:dictSubWorkOrderIssuesDetails];
        }
        
        // Yaha Par nootes helper hrs sab set honge
        [dictSubWorkOrderDetails setObject:arrOfSubWorkOrderIssuesDb forKey:@"SubWorkOrderIssueDcs"];
        
        [self fetchSubWorkOrderActualHrsFromDB :strSubWorkOrderIdToFetch];
        [self fetchSubWorkOrderHelperFromDB :strSubWorkOrderIdToFetch];
        [self fetchSubWorkOrderNotesFromDB :strSubWorkOrderIdToFetch];
        [self fetchSubWOCompleteTimeExtSerDc :strSubWorkOrderIdToFetch];

        // Akshay
        
        [self fetchWorkOrderAppliedDiscountExtSerDcs:strSubWorkOrderIdToFetch];

    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderIssuesRepairsFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    //subWorkOrderIssueId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairDcs"];
        
    }
    else
    {
        arrOfSubWorkOrderIssuesRepairDb=[[NSMutableArray alloc]init];
        dictSubWorkOrderIssuesRepairDetails=[[NSMutableDictionary alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepair.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepair entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepair valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepair valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            dictSubWorkOrderIssuesRepairDetails = [NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [self fetchSubWorkOrderIssuesRepairPartsFromDB:strSubWorkOrderIdToFetch :strSubWorkOrderIssueIdToFetch :[objTempSubWorkOrderIssuesRepair valueForKey:@"issueRepairId"]];
            
            [self fetchSubWorkOrderIssuesRepairLabourFromDB:strSubWorkOrderIdToFetch :strSubWorkOrderIssueIdToFetch :[objTempSubWorkOrderIssuesRepair valueForKey:@"issueRepairId"]];
            
            [arrOfSubWorkOrderIssuesRepairDb addObject:dictSubWorkOrderIssuesRepairDetails];
            
        }
        
        [dictSubWorkOrderIssuesDetails setObject:arrOfSubWorkOrderIssuesRepairDb forKey:@"SubWOIssueRepairDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderIssuesRepairPartsFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesRepairDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairPartDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrOfPartsTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepairParts entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempParts = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfPartsTemp addObject:dictTempParts];
            
        }
        
        [dictSubWorkOrderIssuesRepairDetails setObject:arrOfPartsTemp forKey:@"SubWOIssueRepairPartDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesPartsFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairPartDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrOfPartsTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepairParts entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempParts = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfPartsTemp addObject:dictTempParts];
            
        }
        
        [dictSubWorkOrderIssuesDetails setObject:arrOfPartsTemp forKey:@"SubWOIssueRepairPartDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesRepairLabourFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch,strIssueRepairIdTofetch];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairLabour = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairLabour count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesRepairDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairLaborDcs"];
        
    }
    else
    {
        NSMutableArray *arrOfLaborTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairLabour.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepairLabor=arrAllObjSubWorkOrderIssuesRepairLabour[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepairLabor entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepairLabor valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepairLabor valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempLabor = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfLaborTemp addObject:dictTempLabor];
            
        }
        
        [dictSubWorkOrderIssuesRepairDetails setObject:arrOfLaborTemp forKey:@"SubWOIssueRepairLaborDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderActualHrsFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWorkOrderActualHoursDcs"];
        
    }
    else
    {
        
        NSMutableArray *arrTempActualHrs=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            NSManagedObject *objTempActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempActualHrs entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempActualHrs valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempActualHrs valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSMutableArray *tempKeyArray=[[NSMutableArray alloc]init];
            
            [tempKeyArray addObjectsFromArray:arrLeadDetailKey];
            
            [tempKeyArray addObject:@"EmployeeEmail"];
            
            [arrLeadDetailValue addObject:strFromEmail];
            
            //Change For Employee Time Sheet
            
            arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal=[[NSMutableArray alloc]init];

            [self fetchSubWoEmployeeWorkingTimeExtSerDcs:strSubWorkOrderIdToFetch :[NSString stringWithFormat:@"%@",[objTempActualHrs valueForKey:@"subWOActualHourId"]]];
            
            [tempKeyArray addObject:@"SubWoEmployeeWorkingTimeExtSerDcs"];
            
            [arrLeadDetailValue addObject:arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal];
            
            //Change For Employee Time Sheet
            
            arrLeadDetailKey=tempKeyArray;
            
            NSDictionary *dictTempActualHrs = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempActualHrs addObject:dictTempActualHrs];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempActualHrs forKey:@"SubWorkOrderActualHoursDcs"];
        
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}

-(void)fetchSubWorkOrderHelperFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    arrAllObjSubWorkOrderHelper = [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    if ([arrAllObjSubWorkOrderHelper count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWOTechHelperDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempHelper=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderHelper.count; k++) {
            
            NSManagedObject *objTempHelper=arrAllObjSubWorkOrderHelper[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempHelper entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempHelper = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempHelper addObject:dictTempHelper];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempHelper forKey:@"SubWOTechHelperDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderMechanicalEquipmentFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderMechanicalEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:context];
    requestSubWorkOrderMechanicalEquipment = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderMechanicalEquipment setEntity:entitySubWorkOrderMechanicalEquipment];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    
    [requestSubWorkOrderMechanicalEquipment setPredicate:predicate];
    
    sortDescriptorSubWorkOrderMechanicalEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderMechanicalEquipment = [NSArray arrayWithObject:sortDescriptorSubWorkOrderMechanicalEquipment];
    
    [requestSubWorkOrderMechanicalEquipment setSortDescriptors:sortDescriptorsSubWorkOrderMechanicalEquipment];
    
    self.fetchedResultsControllerSubWorkOrderMechanicalEquipment = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderMechanicalEquipment managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderMechanicalEquipment setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderMechanicalEquipment performFetch:&error];
    arrAllObjSubWorkOrderMechanicalEquipment = [self.fetchedResultsControllerSubWorkOrderMechanicalEquipment fetchedObjects];
    if ([arrAllObjSubWorkOrderMechanicalEquipment count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictFinal setObject:arrEmailDetail forKey:@"AccountItemHistoryExtSerDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempHelper=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderMechanicalEquipment.count; k++) {
            
            NSManagedObject *objTempHelper=arrAllObjSubWorkOrderMechanicalEquipment[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempHelper entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                
                if ([str containsString:@"iOS"]) {
                    str=@"";
                }

                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempHelper = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempHelper addObject:dictTempHelper];
            
        }
        
        [dictFinal setObject:arrTempHelper forKey:@"AccountItemHistoryExtSerDcs"];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderNotesFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
    requestSubWorkOrderNotes = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderNotes setEntity:entityMechanicalSubWorkOrderNotes];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderNotes setPredicate:predicate];
    
    sortDescriptorSubWorkOrderNotes = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderNotes = [NSArray arrayWithObject:sortDescriptorSubWorkOrderNotes];
    
    [requestSubWorkOrderNotes setSortDescriptors:sortDescriptorsSubWorkOrderNotes];
    
    self.fetchedResultsControllerSubWorkOrderNotes = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderNotes managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderNotes setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderNotes performFetch:&error];
    arrAllObjSubWorkOrderNotes = [self.fetchedResultsControllerSubWorkOrderNotes fetchedObjects];
    if ([arrAllObjSubWorkOrderNotes count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWorkOrderNoteDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderNotes.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjSubWorkOrderNotes[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempNotes addObject:dictTempNotes];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempNotes forKey:@"SubWorkOrderNoteDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWOCompleteTimeExtSerDc :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWOCompleteTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWOCompleteTimeExtSerDcs" inManagedObjectContext:context];
    requestSubWOCompleteTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestSubWOCompleteTimeExtSerDcs setEntity:entitySubWOCompleteTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch];
    
    [requestSubWOCompleteTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorSubWOCompleteTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWOCompleteTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorSubWOCompleteTimeExtSerDcs];
    
    [requestSubWOCompleteTimeExtSerDcs setSortDescriptors:sortDescriptorsSubWOCompleteTimeExtSerDcs];
    
    self.fetchedResultsControllerSubWOCompleteTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWOCompleteTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWOCompleteTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWOCompleteTimeExtSerDcs performFetch:&error];
    arrAllObjSubWOCompleteTimeExtSerDcs = [self.fetchedResultsControllerSubWOCompleteTimeExtSerDcs fetchedObjects];
    if ([arrAllObjSubWOCompleteTimeExtSerDcs count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWOCompleteTimeExtSerDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWOCompleteTimeExtSerDcs.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjSubWOCompleteTimeExtSerDcs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempNotes addObject:dictTempNotes];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempNotes forKey:@"SubWOCompleteTimeExtSerDcs"];
        
        NSLog(@"Complete TimeSheet being sent to server =====%@",arrTempNotes);

    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWoEmployeeWorkingTimeExtSerDcs :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderActualHoursId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    requestSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestSubWoEmployeeWorkingTimeExtSerDcs setEntity:entitySubWoEmployeeWorkingTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderActualHourId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch,strSubWorkOrderActualHoursId];
    
    [requestSubWoEmployeeWorkingTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs];
    
    [requestSubWoEmployeeWorkingTimeExtSerDcs setSortDescriptors:sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs];
    
    self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWoEmployeeWorkingTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs performFetch:&error];
    arrAllObjSubWoEmployeeWorkingTimeExtSerDcs = [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs fetchedObjects];
    
    arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal=[[NSMutableArray alloc]init];
    
    if ([arrAllObjSubWoEmployeeWorkingTimeExtSerDcs count] == 0)
    {
        
//        NSMutableArray *arrEmailDetail;
//        arrEmailDetail=[[NSMutableArray alloc]init];
//       // [dictOfSubWoEmployeeWorkingTimeExtSerDcs setObject:arrEmailDetail forKey:@"SubWoEmployeeWorkingTimeExtSerDcs"];
//        [arrOfSubWoEmployeeWorkingTimeExtSerDcs addObject:arrEmailDetail];
        
    }
    else
    {
       // NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWoEmployeeWorkingTimeExtSerDcs.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjSubWoEmployeeWorkingTimeExtSerDcs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal addObject:dictTempNotes];
            
        }
        
        //[dictOfSubWoEmployeeWorkingTimeExtSerDcs setObject:arrTempNotes forKey:@"SubWoEmployeeWorkingTimeExtSerDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}



-(NSDictionary *) nestedDictionaryByReplacingNSDATEwithStringDate:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSString class];
    const NSString *blank = @"Saavan Patidar";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNSDATEwithStringDate:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNSDATEwithStringDate:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}



//============================================================================
//============================================================================
#pragma mark - -------------------------Saving Mechanical Work Orders-------------------------
//============================================================================
//============================================================================

-(void)saveMechanicalWorkOrderInDb :(NSDictionary*)dictWorkOrderMechanicalSubWorkOrder{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    
    //============================================================================
    //============================================================================
#pragma mark- --------- Sub---Work-----Order----Save In Db ----------------
    //============================================================================
    //============================================================================
    
    NSString *strAccountNoMechanical;
    
    NSDictionary *dictWorkOrderDetail=[dictWorkOrderMechanicalSubWorkOrder valueForKey:@"WorkOrderExtSerDcs"];
    
    NSDictionary *dictDataWorkOrders=[dictWorkOrderDetail valueForKey:@"WorkorderDetail"];
    
    NSString *strDepartmentType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentType"]];
    
    if ([strDepartmentType isEqualToString:@"Mechanical"]) {
        
        if(![[dictWorkOrderDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
        {
            
            strGlobalWorkOrderId=@"";
            
        }
        else
        {
            
            entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
            WorkOrderDetailsService *objWorkOrderDetailsService = [[WorkOrderDetailsService alloc]initWithEntity:entityWorkOderDetailServiceAuto insertIntoManagedObjectContext:context];
            
            
            objWorkOrderDetailsService.workorderId =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
            objWorkOrderDetailsService.departmentSysName =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentSysName"]];
            objWorkOrderDetailsService.companyKey=strCompanyKeyy;
            strGlobalWorkOrderId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
            
            strAccountNoMechanical=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountNo"]];

            objWorkOrderDetailsService.thirdPartyAccountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ThirdPartyAccountNo"]];

            objWorkOrderDetailsService.workOrderNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkOrderNo"]];
            
            objWorkOrderDetailsService.companyId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyId"]];
            
            objWorkOrderDetailsService.surveyID=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyID"]];
            
            objWorkOrderDetailsService.departmentId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentId"]];
            
            objWorkOrderDetailsService.accountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountNo"]];
            
            objWorkOrderDetailsService.employeeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EmployeeNo"]];
            
            objWorkOrderDetailsService.firstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"FirstName"]];
            
            objWorkOrderDetailsService.middleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"MiddleName"]];
            
            objWorkOrderDetailsService.lastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastName"]];
            
            objWorkOrderDetailsService.companyName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyName"]];
            
            NSString *strEmailPrimaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryEmail"]];
            
            if (strEmailPrimaryServer.length==0) {
                
                objWorkOrderDetailsService.primaryEmail=@"";
                
            } else {
                
                objWorkOrderDetailsService.primaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"PrimaryEmail"]]];
                
            }
            
            
            NSString *strEmailSecondaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryEmail"]];
            
            if (strEmailSecondaryServer.length==0) {
                
                objWorkOrderDetailsService.secondaryEmail=@"";
                
            } else {
                
                objWorkOrderDetailsService.secondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"SecondaryEmail"]]];
                
            }
            
            
            objWorkOrderDetailsService.primaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryPhone"]];
            
            objWorkOrderDetailsService.secondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryPhone"]];
            
            objWorkOrderDetailsService.scheduleOnStartDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnStartDateTime"]]];
            
            objWorkOrderDetailsService.scheduleOnEndDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnEndDateTime"]]];
            
            objWorkOrderDetailsService.scheduleStartDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleStartDateTime"]]];
            
            objWorkOrderDetailsService.scheduleEndDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleEndDateTime"]]];
            
            objWorkOrderDetailsService.serviceDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDateTime"]]];
            
            objWorkOrderDetailsService.totalEstimationTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalEstimationTime"]];
            
            objWorkOrderDetailsService.branchId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BranchId"]];
            
            objWorkOrderDetailsService.workorderStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderStatus"]];
            
            objWorkOrderDetailsService.atributes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Attributes"]];
            
            objWorkOrderDetailsService.specialInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SpecialInstruction"]];
            
            objWorkOrderDetailsService.serviceInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceInstruction"]];
            
            objWorkOrderDetailsService.direction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Direction"]];
            
            objWorkOrderDetailsService.otherInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OtherInstruction"]];
            
            objWorkOrderDetailsService.categoryName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategoryName"]];
            
            objWorkOrderDetailsService.subCategory=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SubCategory"]];
            
            objWorkOrderDetailsService.services=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Services"]];
            
            objWorkOrderDetailsService.currentServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CurrentServices"]];
            
            objWorkOrderDetailsService.lastServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastServices"]];
            
            objWorkOrderDetailsService.technicianComment=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianComment"]];
            
            objWorkOrderDetailsService.officeNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OfficeNotes"]];
            
            objWorkOrderDetailsService.timeIn=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeIn"]];
            
            objWorkOrderDetailsService.timeOut=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOut"]];
            
            objWorkOrderDetailsService.resetId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetId"]];
            
            objWorkOrderDetailsService.resetDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetDescription"]];
            
            objWorkOrderDetailsService.audioFilePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AudioFilePath"]];
            
            objWorkOrderDetailsService.invoiceAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"InvoiceAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoiceAmount"]];
            
            objWorkOrderDetailsService.productionAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"ProductionAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProductionAmount"]];
            
            objWorkOrderDetailsService.previousBalance=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"PreviousBalance"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PreviousBalance"]];
            
            objWorkOrderDetailsService.tax=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"Tax"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Tax"]];
            
            objWorkOrderDetailsService.customerSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CustomerSignaturePath"]];
            
            objWorkOrderDetailsService.technicianSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianSignaturePath"]];
            
            objWorkOrderDetailsService.electronicSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ElectronicSignaturePath"]];
            
            objWorkOrderDetailsService.invoicePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoicePath"]];
            
            objWorkOrderDetailsService.billingAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress1"]];
            
            objWorkOrderDetailsService.billingAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress2"]];
            
            objWorkOrderDetailsService.billingCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCountry"]];
            
            objWorkOrderDetailsService.billingState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingState"]];
            
            objWorkOrderDetailsService.billingCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCity"]];
            
            objWorkOrderDetailsService.billingZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingZipcode"]];
            
            objWorkOrderDetailsService.servicesAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicesAddress1"]];
            
            objWorkOrderDetailsService.serviceAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddress2"]];
            
            objWorkOrderDetailsService.serviceCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
            
            objWorkOrderDetailsService.serviceState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceState"]];
            
            objWorkOrderDetailsService.serviceCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCity"]];
            
            objWorkOrderDetailsService.serviceZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceZipcode"]];
            
            objWorkOrderDetailsService.serviceAddressLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLatitude"]];
            
            objWorkOrderDetailsService.serviceAddressLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLongitude"]];
            
            objWorkOrderDetailsService.isPDFGenerated=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsPDFGenerated"]]];
            
            objWorkOrderDetailsService.isMailSent=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsMailSent"]]];
            
            objWorkOrderDetailsService.isElectronicSignatureAvailable=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsElectronicSignatureAvailable"]]];
            
            objWorkOrderDetailsService.isFail=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsFail"]]];
            
            objWorkOrderDetailsService.operateMedium=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OperateMedium"]];
            
            objWorkOrderDetailsService.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsActive"]]];
            
            objWorkOrderDetailsService.surveyStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyStatus"]];
            
            objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
            
            objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
            
            objWorkOrderDetailsService.modifiedFormatedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]]];
            
            objWorkOrderDetailsService.modifiedBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedBy"]];
            
            objWorkOrderDetailsService.userName=strUserName;
            objWorkOrderDetailsService.onMyWaySentSMSTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTime"]];
            objWorkOrderDetailsService.routeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteNo"]];
            objWorkOrderDetailsService.routeName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteName"]];
            objWorkOrderDetailsService.targets=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Targets"]];
            
            objWorkOrderDetailsService.deviceVersion=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceVersion"]];
            objWorkOrderDetailsService.deviceName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceName"]];
            objWorkOrderDetailsService.versionNumber=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionNumber"]];
            objWorkOrderDetailsService.versionDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionDate"]];
            
            NSDateFormatter* dateFormatterSchedule = [[NSDateFormatter alloc] init];
            [dateFormatterSchedule setTimeZone:[NSTimeZone localTimeZone]];
            [dateFormatterSchedule setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
            NSDate* newTimeSchedule = [dateFormatterSchedule dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ScheduleStartDateTime"]]];
            objWorkOrderDetailsService.zdateScheduledStart=newTimeSchedule;
            
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
            NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ModifiedFormatedDate"]]];
            objWorkOrderDetailsService.dateModified=newTime;
            
            objWorkOrderDetailsService.isCustomerNotPresent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCustomerNotPresent"]];
            
            objWorkOrderDetailsService.totalallowCrew=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalallowCrew"]];
            
            objWorkOrderDetailsService.assignCrewIds=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AssignCrewIds"]];
            
            objWorkOrderDetailsService.routeCrewList=[dictDataWorkOrders valueForKey:@"RouteCrewList"];
            
            objWorkOrderDetailsService.zSync=@"blank";
            
            // objWorkOrderDetailsService.isCustomerNotPresent=[NSString stringWithFormat:@"%@",@"false"];
            
            //Nilind 27 Feb
            
            objWorkOrderDetailsService.arrivalDuration=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ArrivalDuration"]];
            objWorkOrderDetailsService.keyMap=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"KeyMap"]];
            objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
            objWorkOrderDetailsService.problemDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProblemDescription"]];
            objWorkOrderDetailsService.serviceDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDescription"]];
            //End
            
            objWorkOrderDetailsService.primaryServiceSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryServiceSysName"]];
            objWorkOrderDetailsService.categorySysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategorySysName"]];
            
            NSUserDefaults *defsLogin = [NSUserDefaults standardUserDefaults];
            NSString *dbCompanyKey=[defsLogin valueForKey:@"companyKey"];
            // NSString *dbUserName =[defsLogin valueForKey:@"username"];
            
            objWorkOrderDetailsService.dbCompanyKey=dbCompanyKey;
            objWorkOrderDetailsService.dbUserName=strEmployeeNo;
            
            objWorkOrderDetailsService.noChemical=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"NoChemical"]];
            
            objWorkOrderDetailsService.serviceJobDescriptionId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescriptionId"]];
            
            objWorkOrderDetailsService.serviceJobDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescription"]];
            
            objWorkOrderDetailsService.isEmployeePresetSignature=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsEmployeePresetSignature"]];
            
            objWorkOrderDetailsService.departmentType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentType"]];
            
            objWorkOrderDetailsService.tags=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Tags"]];
            
            objWorkOrderDetailsService.serviceGateCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceGateCode"]];
            
            objWorkOrderDetailsService.serviceMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMapCode"]];
            
            objWorkOrderDetailsService.accountDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountDescription"]];
            
            objWorkOrderDetailsService.isClientApprovalReq=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsClientApprovalReq"]];
            
            //DepartmentSysName
            objWorkOrderDetailsService.departmentSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentSysName"]];
            
            objWorkOrderDetailsService.isResendInvoiceMail=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsResendInvoiceMail"]];

            //Nilind 28 Dec 
            objWorkOrderDetailsService.cellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CellNo"]];
            objWorkOrderDetailsService.billingFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingFirstName"]];
            objWorkOrderDetailsService.billingMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMiddleName"]];
            objWorkOrderDetailsService.billingLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingLastName"]];
            objWorkOrderDetailsService.billingCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCellNo"]];
            objWorkOrderDetailsService.billingPrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingPrimaryEmail"]]];
            objWorkOrderDetailsService.billingSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingSecondaryEmail"]]];
            objWorkOrderDetailsService.billingPrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPrimaryPhone"]];
            objWorkOrderDetailsService.billingSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSecondaryPhone"]];
            objWorkOrderDetailsService.billingMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMapCode"]];
            objWorkOrderDetailsService.billingPOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPOCId"]];

            
            objWorkOrderDetailsService.serviceFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceFirstName"]];
            objWorkOrderDetailsService.serviceMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMiddleName"]];
            objWorkOrderDetailsService.serviceLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceLastName"]];
            objWorkOrderDetailsService.serviceCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCellNo"]];
            objWorkOrderDetailsService.servicePrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServicePrimaryEmail"]]];
            objWorkOrderDetailsService.serviceSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServiceSecondaryEmail"]]];
            objWorkOrderDetailsService.servicePrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePrimaryPhone"]];
            objWorkOrderDetailsService.serviceSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSecondaryPhone"]];
            objWorkOrderDetailsService.serviceMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMapCode"]];
            objWorkOrderDetailsService.serviceGateCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceGateCode"]];
            objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
            objWorkOrderDetailsService.serviceAddressImagePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressImagePath"]];
            objWorkOrderDetailsService.servicePOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePOCId"]];

            //Nilind 08 Dec
            
            objWorkOrderDetailsService.timeInLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLatitude"]];
            objWorkOrderDetailsService.timeInLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLongitude"]];
            objWorkOrderDetailsService.timeOutLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLatitude"]];
            objWorkOrderDetailsService.timeOutLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLongitude"]];
            objWorkOrderDetailsService.onMyWaySentSMSTimeLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLatitude"]];
            objWorkOrderDetailsService.onMyWaySentSMSTimeLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLongitude"]];
            
            //End
            
            objWorkOrderDetailsService.addressSubType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AddressSubType"]];
            objWorkOrderDetailsService.serviceAddressId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressId"]];
            objWorkOrderDetailsService.customerPONumber=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CustomerPONumber"]];
            objWorkOrderDetailsService.servicePOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePOCId"]];
            objWorkOrderDetailsService.billingPOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPOCId"]];

            objWorkOrderDetailsService.billingCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCounty"]];
            objWorkOrderDetailsService.serviceCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCounty"]];
            objWorkOrderDetailsService.billingSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSchoolDistrict"]];
            objWorkOrderDetailsService.serviceSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSchoolDistrict"]];

            objWorkOrderDetailsService.earliestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EarliestStartTimeStr"]];
            objWorkOrderDetailsService.latestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LatestStartTimeStr"]];
            objWorkOrderDetailsService.driveTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTimeStr"]];
            
            NSString *strDateee=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleStartDateTime"]];
            
            objWorkOrderDetailsService.fromDate=[NSString stringWithFormat:@"%@",[global strBlockDateTime:strDateee]];
            
            strDateee=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleEndDateTime"]];
            
            objWorkOrderDetailsService.toDate=[NSString stringWithFormat:@"%@",[global strBlockDateTime:strDateee]];
            
            strDateee=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];
            
            objWorkOrderDetailsService.modifyDate=[NSString stringWithFormat:@"%@",[global strBlockDateTime:strDateee]];
            
            NSString *strIsRegularPestFlow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlow"]];
            
            if ([strIsRegularPestFlow isEqualToString:@"1"] || [strIsRegularPestFlow isEqualToString:@"True"] || [strIsRegularPestFlow isEqualToString:@"true"]) {
                
                objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"true"];
                
            } else {
                
                objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"false"];
                
            }
            
            NSString *strIsRegularPestFlowOnBranch = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlow"]];
            
            
            if ([strIsRegularPestFlowOnBranch isEqualToString:@"1"] || [strIsRegularPestFlowOnBranch isEqualToString:@"True"] || [strIsRegularPestFlowOnBranch isEqualToString:@"true"]) {
                
                objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"true"];
                
            } else {
                
                objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"false"];
                
            }
            
            
            objWorkOrderDetailsService.serviceAddressId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressId"]];

            NSString *strIsCollectPayment = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCollectPayment"]];
            
            if ([strIsCollectPayment isEqualToString:@"1"] || [strIsCollectPayment isEqualToString:@"True"] || [strIsCollectPayment isEqualToString:@"true"]) {
                
                objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"true"];
                
            } else {
                
                objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"false"];
                
            }
            
            NSString *strIsWhetherShow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsWhetherShow"]];
            
            if ([strIsWhetherShow isEqualToString:@"1"] || [strIsWhetherShow isEqualToString:@"True"] || [strIsWhetherShow isEqualToString:@"true"]) {
                
                objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"true"];
                
            } else {
                
                objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"false"];
                
            }
            
            objWorkOrderDetailsService.relatedOpportunityNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RelatedOpportunityNo"]];

            objWorkOrderDetailsService.taxPercent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TaxPercent"]];

            objWorkOrderDetailsService.serviceReportFormat=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceReportFormat"]];

            objWorkOrderDetailsService.isBatchReleased=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsBatchReleased"]];

            objWorkOrderDetailsService.accountName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountName"]];
            objWorkOrderDetailsService.driveTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTime"]];
            objWorkOrderDetailsService.isMailSent = @"false";

        }
        
        //============================================================================
        //============================================================================
#pragma mark- ---------********Mechanical Email Id Save *******----------------
        //============================================================================
        //============================================================================
        
        //Email Id Save
        if(![[dictWorkOrderDetail valueForKey:@"EmailDetail"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            
            NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"EmailDetail"];//EmailDetail
            for (int j=0; j<arrEmailDetail.count; j++)
            {
                //Email Detail Entity
                entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
                
                EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
                NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                
                objEmailDetail.companyKey=strCompanyKeyy;
                
                objEmailDetail.userName=strUserName;
                
                objEmailDetail.workorderId=strGlobalWorkOrderId;
                
                objEmailDetail.woInvoiceMailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"WoInvoiceMailId"]];
                
                objEmailDetail.emailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"EmailId"]];
                
                objEmailDetail.subject=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Subject"]];
                
                //  objEmailDetail.isMailSent=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]];
                
                //objEmailDetail.isCustomerEmail=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]];
                
                if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"True"])
                {
                    objEmailDetail.isCustomerEmail=@"true";

                }
                else
                {
                    objEmailDetail.isCustomerEmail=@"false";

                }

                
                
                objEmailDetail.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedDate"]]];
                
                objEmailDetail.createdBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedBy"]];
                
                objEmailDetail.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedDate"]]];
                
                objEmailDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedBy"]];
                
                if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"1"])
                {
                    objEmailDetail.isMailSent=@"true";
                }
                else
                {
                    objEmailDetail.isMailSent=@"false";
                }
                //IsDefaultEmail
                if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"1"])
                {
                    objEmailDetail.isDefaultEmail=@"true";
                }
                else
                {
                    objEmailDetail.isDefaultEmail=@"false";
                }
                
                if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsInvoiceMailSent"]] isEqualToString:@"1"])
                {
                    objEmailDetail.isInvoiceMailSent=@"true";
                }
                else
                {
                    objEmailDetail.isInvoiceMailSent=@"false";
                }
                
                NSError *error1;
                [context save:&error1];
            }
        }
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------******** Mechanical Image Detail Save *******----------------
        //============================================================================
        //============================================================================
        
        //Email Id Save
        if(![[dictWorkOrderDetail valueForKey:@"ImagesDetail"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            
            NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"ImagesDetail"];//EmailDetail
            for (int j=0; j<arrImagesDetail.count; j++)
            {
                //Images Detail Entity
                entityImageDetailServiceAuto=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
                
                ImageDetailsServiceAuto *objImagesDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetailServiceAuto insertIntoManagedObjectContext:context];
                NSMutableDictionary *dictImagesDetail=[[NSMutableDictionary alloc]init];
                dictImagesDetail=[arrImagesDetail objectAtIndex:j];
                
                objImagesDetail.companyKey=strCompanyKeyy;
                
                objImagesDetail.userName=strUserName;
                
                objImagesDetail.workorderId=strGlobalWorkOrderId;
                
                objImagesDetail.woImageId=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageId"]];
                
                objImagesDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImagePath"]];
                
                objImagesDetail.woImageType=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageType"]];
                
                objImagesDetail.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]]];
                
                objImagesDetail.createdBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"CreatedBy"]];
                
                objImagesDetail.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedDate"]]];
                
                objImagesDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                
                objImagesDetail.imageCaption=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageCaption"]];
                
                objImagesDetail.imageDescription=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageDescription"]];
                
                if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"True"])
                {
                    objImagesDetail.isProblemIdentifaction=@"true";
                }
                else
                {
                    objImagesDetail.isProblemIdentifaction=@"false";
                }
                
                objImagesDetail.graphXmlPath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"GraphXmlPath"]];
                
                NSError *error1;
                [context save:&error1];
            }
        }
        
        
        //============================================================================
        //============================================================================
#pragma mark- ------------------ Sub Work Order Save --------------------------
        //============================================================================
        //============================================================================
        
        //        entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
        //
        //        MechanicalSubWorkOrder *objSubWorkOrder = [[MechanicalSubWorkOrder alloc]initWithEntity:entityMechanicalSubWorkOrder insertIntoManagedObjectContext:context];
        
        //SubWorkOrder
        
        NSString *strSubWorkOrderIdTempToSave;

        if(![[dictWorkOrderDetail valueForKey:@"SubWorkOrder"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            NSArray *arrOfSubWorkOrderFromServer=[dictWorkOrderDetail valueForKey:@"SubWorkOrder"];
            
            for (int k=0; k<arrOfSubWorkOrderFromServer.count; k++) {
                
                entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
                
                MechanicalSubWorkOrder *objSubWorkOrder = [[MechanicalSubWorkOrder alloc]initWithEntity:entityMechanicalSubWorkOrder insertIntoManagedObjectContext:context];
                
                NSDictionary *dictOfSubWorkOrderInfo = arrOfSubWorkOrderFromServer[k];
                
                objSubWorkOrder.workorderId=strGlobalWorkOrderId;
                objSubWorkOrder.departmentSysName =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentSysName"]];
                objSubWorkOrder.companyKey=strCompanyKeyy;
                objSubWorkOrder.accountNo=strAccountNoMechanical;
                objSubWorkOrder.actualTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualTime"]];
                objSubWorkOrder.amtPaid=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AmtPaid"]];
                objSubWorkOrder.companyKey=strCompanyKeyy;
                objSubWorkOrder.completeSWO_CustSignPath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_CustSignPath"]];
                objSubWorkOrder.completeSWO_IsCustomerNotPresent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_IsCustomerNotPresent"]];
                objSubWorkOrder.completeSWO_TechSignPath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_TechSignPath"]];
                objSubWorkOrder.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedBy"]];
                objSubWorkOrder.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedDate"]]];
                objSubWorkOrder.customerSignaturePath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CustomerSignaturePath"]];
                objSubWorkOrder.diagnosticChargeMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"DiagnosticChargeMasterId"]];
                objSubWorkOrder.discountAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"DiscountAmt"]];
                objSubWorkOrder.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"EmployeeNo"]];
                objSubWorkOrder.invoiceAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoiceAmt"]];
                objSubWorkOrder.invoicePayType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoicePayType"]];
                objSubWorkOrder.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsActive"]]];
                objSubWorkOrder.isClientApprovalReq=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsClientApprovalReq"]]];
                objSubWorkOrder.isClientApproved=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsClientApproved"]]];
                objSubWorkOrder.isCompleted=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCompleted"]]];
                objSubWorkOrder.isCustomerNotPresent=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCustomerNotPresent"]]];
                objSubWorkOrder.isStdPrice=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsStdPrice"]]];
                objSubWorkOrder.jobDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"JobDesc"]];
                objSubWorkOrder.laborPercent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborPercent"]];
                objSubWorkOrder.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ModifiedBy"]];
                objSubWorkOrder.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ModifiedDate"]]];
                objSubWorkOrder.otherDiagnosticChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"OtherDiagnosticChargesAmt"]];
                objSubWorkOrder.otherTripChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"OtherTripChargesAmt"]];
                objSubWorkOrder.partPercent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPercent"]];
                objSubWorkOrder.priceNotToExceed=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PriceNotToExceed"]];
                objSubWorkOrder.serviceDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceDateTime"]]];
                objSubWorkOrder.serviceStartEndTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceStartEndTime"]];
                objSubWorkOrder.serviceStartStartTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceStartStartTime"]];
                objSubWorkOrder.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                
                strSubWorkOrderIdTempToSave=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];

                objSubWorkOrder.subWorkOrderIssueId=@"";
                objSubWorkOrder.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNo"]];
                objSubWorkOrder.subWOStatus=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOStatus"]];
                objSubWorkOrder.subWOType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOType"]];
                objSubWorkOrder.taxAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TaxAmt"]];
                objSubWorkOrder.technicianSignaturePath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TechnicianSignaturePath"]];
                objSubWorkOrder.totalApprovedAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalApprovedAmt"]];
                objSubWorkOrder.totalEstimationTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalEstimationTime"]];
                objSubWorkOrder.totalEstimationTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalEstimationTimeInt"]];
                objSubWorkOrder.tripChargeMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TripChargeMasterId"]];
                objSubWorkOrder.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedByDevice"]];
                
                objSubWorkOrder.pSPDiscount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPDiscount"]];
                
                objSubWorkOrder.pSPCharges=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPCharges"]];
                
                objSubWorkOrder.invoiceAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoiceAmt"]];
                
                objSubWorkOrder.accountPSPId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AccountPSPId"]];
                
                objSubWorkOrder.pSPMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPMasterId"]];
                
                objSubWorkOrder.inspectionresults=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"Inspectionresults"]];
                objSubWorkOrder.propertyType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PropertyType"]];

                objSubWorkOrder.addressType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AddressType"]];
                objSubWorkOrder.isLaborTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsLaborTaxable"]];
                objSubWorkOrder.isPartTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsPartTaxable"]];
                objSubWorkOrder.partPriceType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPriceType"]];
                objSubWorkOrder.isVendorPayTax=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsVendorPayTax"]];
                objSubWorkOrder.isChargeTaxOnFullAmount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsChargeTaxOnFullAmount"]];
                objSubWorkOrder.afterHrsDuration=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AfterHrsDuration"]];
                objSubWorkOrder.isHolidayHrs=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsHolidayHrs"]];
                objSubWorkOrder.laborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborHrsPrice"]];
                objSubWorkOrder.mileageChargesName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesName"]];
                objSubWorkOrder.totalMileage=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalMileage"]];
                objSubWorkOrder.mileageChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesAmt"]];
                objSubWorkOrder.miscChargeAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MiscChargeAmt"]];
                objSubWorkOrder.actualTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualTimeInt"]];

                objSubWorkOrder.isCreateWOforInCompleteIssue=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCreateWOforInCompleteIssue"]];
                objSubWorkOrder.isSendDocWithOutSign=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsSendDocWithOutSign"]];
                objSubWorkOrder.calculatedActualTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CalculatedActualTimeInt"]];
                objSubWorkOrder.actualLaborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualLaborHrsPrice"]];
                objSubWorkOrder.clockStatus=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ClockStatus"]];

                // Saving WorkorderNo in SubWorkOrder DB
                objSubWorkOrder.workOrderNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkOrderNo"]];
                
                objSubWorkOrder.officeNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OfficeNotes"]];
                
                objSubWorkOrder.laborType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborType"]];
                
                objSubWorkOrder.tripChargesQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TripChargesQty"]];

                //============================================================================
                //============================================================================
#pragma mark- ------------------ Sub Work Order Issues Save --------------------------
                //============================================================================
                //============================================================================
                
                //                entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
                //
                //                MechanicalSubWorkOrderIssueDcs *objSubWorkOrderIssues = [[MechanicalSubWorkOrderIssueDcs alloc]initWithEntity:entitySubWorkOrderIssues insertIntoManagedObjectContext:context];
                
                //SubWorkOrderIssues
                if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderIssueDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    NSArray *arrOfSubWorkOrderIssuesFromServer=[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderIssueDcs"];
                    
                    for (int k1=0; k1<arrOfSubWorkOrderIssuesFromServer.count; k1++) {
                        
                        entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
                        
                        MechanicalSubWorkOrderIssueDcs *objSubWorkOrderIssues = [[MechanicalSubWorkOrderIssueDcs alloc]initWithEntity:entitySubWorkOrderIssues insertIntoManagedObjectContext:context];
                        
                        NSDictionary *dictOfSubWorkOrderIssuesInfo = arrOfSubWorkOrderIssuesFromServer[k1];
                        
                        objSubWorkOrderIssues.workorderId=strGlobalWorkOrderId;
                        objSubWorkOrderIssues.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                        objSubWorkOrderIssues.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                        objSubWorkOrderIssues.serviceIssue=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ServiceIssue"]];
                        objSubWorkOrderIssues.priority=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"Priority"]];
                        objSubWorkOrderIssues.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"IsActive"]]];
                        objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedBy"]];
                        objSubWorkOrderIssues.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedDate"]]];
                        objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModifiedBy"]];
                        objSubWorkOrderIssues.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModifiedDate"]]];
                        objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedByDevice"]];
                        
                        objSubWorkOrderIssues.equipmentCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentCode"]];
                        objSubWorkOrderIssues.equipmentName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentName"]];
                        objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SerialNumber"]];
                        objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModelNumber"]];
                        objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"Manufacturer"]];
                        objSubWorkOrderIssues.equipmentCustomName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentCustomName"]];
                        objSubWorkOrderIssues.equipmentNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentNo"]];


                        NSError *error2;
                        [context save:&error2];
                        
                        NSString *strWOTYPE=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOType"]];
                        
                        if ([strWOTYPE isEqualToString:@"TM"]) {
                            
                            
                            //Yaha Parr Issues Repairs parts Save karna hai
                            
                            //============================================================================
                            //============================================================================
#pragma mark- @@@@@@------------------@@@@@@@@@@@@ Sub Work Order Issues Parts Time And Material Save --------------------------
                            //============================================================================
                            //============================================================================
                            
                            //                            entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                            //
                            //                            MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
                            
                            //SubWorkOrderIssuesRepairsParts
                            if(![[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairPartDcs"] isKindOfClass:[NSArray class]])
                            {
                                
                            }
                            else
                            {
                                NSArray *arrOfSubWorkOrderIssuesRepairsPartsFromServer=[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairPartDcs"];
                                
                                for (int k2=0; k2<arrOfSubWorkOrderIssuesRepairsPartsFromServer.count; k2++) {
                                    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                                    
                                    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
                                    
                                    NSDictionary *dictOfSubWorkOrderIssuesRepairsPartsInfo = arrOfSubWorkOrderIssuesRepairsPartsFromServer[k2];
                                    
                                    objSubWorkOrderIssuesRepairsParts.workorderId=strGlobalWorkOrderId;
                                    objSubWorkOrderIssuesRepairsParts.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                    objSubWorkOrderIssuesRepairsParts.issueRepairPartId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairPartId"]];
                                    objSubWorkOrderIssuesRepairsParts.issueRepairId=@"";//[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairId"]];
                                    objSubWorkOrderIssuesRepairsParts.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                                    objSubWorkOrderIssuesRepairsParts.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsActive"]]];
                                    objSubWorkOrderIssuesRepairsParts.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedBy"]];
                                    objSubWorkOrderIssuesRepairsParts.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedDate"]]];
                                    objSubWorkOrderIssuesRepairsParts.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedBy"]];
                                    objSubWorkOrderIssuesRepairsParts.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedDate"]]];
                                    objSubWorkOrderIssuesRepairsParts.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedByDevice"]];
                                    objSubWorkOrderIssuesRepairsParts.partType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartType"]];
                                    objSubWorkOrderIssuesRepairsParts.partCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCode"]];
                                    objSubWorkOrderIssuesRepairsParts.partName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartName"]];
                                    objSubWorkOrderIssuesRepairsParts.partDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartDesc"]];
                                    objSubWorkOrderIssuesRepairsParts.qty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Qty"]];
                                    objSubWorkOrderIssuesRepairsParts.unitPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"UnitPrice"]];
                                    objSubWorkOrderIssuesRepairsParts.isDefault=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsDefault"]]];
                                    objSubWorkOrderIssuesRepairsParts.isWarranty=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsWarranty"]]];
                                    objSubWorkOrderIssuesRepairsParts.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"SerialNumber"]];
                                    objSubWorkOrderIssuesRepairsParts.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModelNumber"]];
                                    objSubWorkOrderIssuesRepairsParts.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Manufacturer"]];
                                    objSubWorkOrderIssuesRepairsParts.installationDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"InstallationDate"]]];
                                    objSubWorkOrderIssuesRepairsParts.multiplier=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Multiplier"]];
                                    objSubWorkOrderIssuesRepairsParts.isCompleted=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsCompleted"]]];
                                    objSubWorkOrderIssuesRepairsParts.customerFeedback=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CustomerFeedback"]]];
                                    objSubWorkOrderIssuesRepairsParts.isAddedAfterApproval=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsAddedAfterApproval"]]];
                                    objSubWorkOrderIssuesRepairsParts.actualQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ActualQty"]];
                                    objSubWorkOrderIssuesRepairsParts.isChangeStdPartPrice=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsChangeStdPartPrice"]]];

                                    // Vandor detail in parts
                                    objSubWorkOrderIssuesRepairsParts.vendorName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorName"]];
                                    objSubWorkOrderIssuesRepairsParts.vendorPartNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorPartNo"]];
                                    objSubWorkOrderIssuesRepairsParts.vendorQuoteNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorQuoteNo"]];

                                    objSubWorkOrderIssuesRepairsParts.partCategorySysName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCategorySysName"]];

                                    NSError *error5;
                                    [context save:&error5];
                                    
                                }
                                
                            }
                            
                            
                        } else {
                            
                            //Yaha Par SubWork Order Repairs add honge
                            
                            //============================================================================
                            //============================================================================
#pragma mark- @@@@@@@@------------------ Sub Work Order Issues Repairs Save --------------------------
                            //============================================================================
                            //============================================================================
                            
                            //                            entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
                            //
                            //                            MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssuesRepairs = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepair insertIntoManagedObjectContext:context];
                            
                            //SubWorkOrderIssuesRepairs
                            if(![[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairDcs"] isKindOfClass:[NSArray class]])
                            {
                                
                            }
                            else
                            {
                                NSArray *arrOfSubWorkOrderIssuesRepairsFromServer=[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairDcs"];
                                
                                for (int k3=0; k3<arrOfSubWorkOrderIssuesRepairsFromServer.count; k3++) {
                                    
                                    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
                                    
                                    MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssuesRepairs = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepair insertIntoManagedObjectContext:context];
                                    
                                    NSDictionary *dictOfSubWorkOrderIssuesRepairsInfo = arrOfSubWorkOrderIssuesRepairsFromServer[k3];
                                    
                                    objSubWorkOrderIssuesRepairs.workorderId=strGlobalWorkOrderId;
                                    objSubWorkOrderIssuesRepairs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                    objSubWorkOrderIssuesRepairs.issueRepairId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IssueRepairId"]];
                                    objSubWorkOrderIssuesRepairs.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                                    objSubWorkOrderIssuesRepairs.repairMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairMasterId"]];
                                    objSubWorkOrderIssuesRepairs.repairLaborId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairLaborId"]];
                                    objSubWorkOrderIssuesRepairs.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IsActive"]]];
                                    objSubWorkOrderIssuesRepairs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CreatedBy"]];
                                    objSubWorkOrderIssuesRepairs.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CreatedDate"]]];
                                    objSubWorkOrderIssuesRepairs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"ModifiedBy"]];
                                    objSubWorkOrderIssuesRepairs.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"ModifiedDate"]]];
                                    objSubWorkOrderIssuesRepairs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CreatedByDevice"]];
                                    objSubWorkOrderIssuesRepairs.repairName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairName"]];
                                    objSubWorkOrderIssuesRepairs.repairDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairDesc"]];
                                    objSubWorkOrderIssuesRepairs.costAdjustment=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CostAdjustment"]];
                                    objSubWorkOrderIssuesRepairs.isWarranty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IsWarranty"]];
                                    objSubWorkOrderIssuesRepairs.isCompleted=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IsCompleted"]]];
                                    objSubWorkOrderIssuesRepairs.customerFeedback=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CustomerFeedback"]]];
                                    
                                    objSubWorkOrderIssuesRepairs.partCostAdjustment=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"PartCostAdjustment"]];
                                    objSubWorkOrderIssuesRepairs.nonStdRepairAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"NonStdRepairAmt"]];
                                    objSubWorkOrderIssuesRepairs.nonStdPartAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"NonStdPartAmt"]];
                                    objSubWorkOrderIssuesRepairs.nonStdLaborAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"NonStdLaborAmt"]];
                                    objSubWorkOrderIssuesRepairs.qty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"Qty"]];
                                    
                                    
                                    NSError *error4;
                                    [context save:&error4];
                                    
                                    
                                    //Yaha Parr Issues Repairs parts Save karna hai
                                    
                                    //============================================================================
                                    //============================================================================
#pragma mark- @@@@@@------------------@@@@@@@@@@@@ Sub Work Order Issues Repairs Parts Save --------------------------
                                    //============================================================================
                                    //============================================================================
                                    
                                    //                                    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                                    //
                                    //                                    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
                                    
                                    //SubWorkOrderIssuesRepairsParts
                                    if(![[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairPartDcs"] isKindOfClass:[NSArray class]])
                                    {
                                        
                                    }
                                    else
                                    {
                                        NSArray *arrOfSubWorkOrderIssuesRepairsPartsFromServer=[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairPartDcs"];
                                        
                                        for (int k4=0; k4<arrOfSubWorkOrderIssuesRepairsPartsFromServer.count; k4++) {
                                            
                                            entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                                            
                                            MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
                                            
                                            NSDictionary *dictOfSubWorkOrderIssuesRepairsPartsInfo = arrOfSubWorkOrderIssuesRepairsPartsFromServer[k4];
                                            
                                            objSubWorkOrderIssuesRepairsParts.workorderId=strGlobalWorkOrderId;
                                            objSubWorkOrderIssuesRepairsParts.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                            objSubWorkOrderIssuesRepairsParts.issueRepairPartId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairPartId"]];
                                            objSubWorkOrderIssuesRepairsParts.issueRepairId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairId"]];
                                            objSubWorkOrderIssuesRepairsParts.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                                            objSubWorkOrderIssuesRepairsParts.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsActive"]]];
                                            objSubWorkOrderIssuesRepairsParts.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedBy"]];
                                            objSubWorkOrderIssuesRepairsParts.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedDate"]]];
                                            objSubWorkOrderIssuesRepairsParts.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedBy"]];
                                            objSubWorkOrderIssuesRepairsParts.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedDate"]]];
                                            objSubWorkOrderIssuesRepairsParts.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedByDevice"]];
                                            objSubWorkOrderIssuesRepairsParts.partType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartType"]];
                                            objSubWorkOrderIssuesRepairsParts.partCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCode"]];
                                            objSubWorkOrderIssuesRepairsParts.partName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartName"]];
                                            objSubWorkOrderIssuesRepairsParts.partDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartDesc"]];
                                            objSubWorkOrderIssuesRepairsParts.qty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Qty"]];
                                            objSubWorkOrderIssuesRepairsParts.unitPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"UnitPrice"]];
                                            objSubWorkOrderIssuesRepairsParts.isDefault=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsDefault"]]];
                                            objSubWorkOrderIssuesRepairsParts.isWarranty=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsWarranty"]]];
                                            objSubWorkOrderIssuesRepairsParts.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"SerialNumber"]];
                                            objSubWorkOrderIssuesRepairsParts.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModelNumber"]];
                                            objSubWorkOrderIssuesRepairsParts.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Manufacturer"]];
                                            objSubWorkOrderIssuesRepairsParts.installationDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"InstallationDate"]]];
                                            objSubWorkOrderIssuesRepairsParts.multiplier=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Multiplier"]];
                                            objSubWorkOrderIssuesRepairsParts.isCompleted=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsCompleted"]];
                                            objSubWorkOrderIssuesRepairsParts.customerFeedback=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CustomerFeedback"]]];
                                            objSubWorkOrderIssuesRepairsParts.isAddedAfterApproval=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsAddedAfterApproval"]]];
                                            objSubWorkOrderIssuesRepairsParts.actualQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ActualQty"]];
                                            objSubWorkOrderIssuesRepairsParts.isChangeStdPartPrice=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsChangeStdPartPrice"]]];

                                            // Vandor detail in parts
                                            objSubWorkOrderIssuesRepairsParts.vendorName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorName"]];
                                            objSubWorkOrderIssuesRepairsParts.vendorPartNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorPartNo"]];
                                            objSubWorkOrderIssuesRepairsParts.vendorQuoteNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorQuoteNo"]];
                                            objSubWorkOrderIssuesRepairsParts.partCategorySysName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCategorySysName"]];

                                            
                                            NSError *error5;
                                            [context save:&error5];
                                            
                                        }
                                        
                                    }
                                    
                                    
                                    
                                    //Yaha Parr Issues Repairs parts Save karna hai
                                    
                                    
                                    //============================================================================
                                    //============================================================================
#pragma mark- @@@@@@------------------@@@@@@@@@@@@ Sub Work Order Issues Repairs Labours Save --------------------------
                                    //============================================================================
                                    //============================================================================
                                    
                                    //                                    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
                                    //
                                    //                                    MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssuesRepairsLabour = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairLabour insertIntoManagedObjectContext:context];
                                    
                                    //SubWorkOrderIssues
                                    if(![[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairLaborDcs"] isKindOfClass:[NSArray class]])
                                    {
                                        
                                    }
                                    else
                                    {
                                        NSArray *arrOfSubWorkOrderIssuesRepairsPartsFromServer=[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairLaborDcs"];
                                        
                                        for (int k5=0; k5<arrOfSubWorkOrderIssuesRepairsPartsFromServer.count; k5++) {
                                            
                                            entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
                                            
                                            MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssuesRepairsLabour = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairLabour insertIntoManagedObjectContext:context];
                                            
                                            NSDictionary *dictOfSubWorkOrderIssuesRepairsPartsInfo = arrOfSubWorkOrderIssuesRepairsPartsFromServer[k5];
                                            
                                            objSubWorkOrderIssuesRepairsLabour.workorderId=strGlobalWorkOrderId;
                                            objSubWorkOrderIssuesRepairsLabour.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                            objSubWorkOrderIssuesRepairsLabour.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                                            objSubWorkOrderIssuesRepairsLabour.issueRepairLaborId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairLaborId"]];
                                            objSubWorkOrderIssuesRepairsLabour.issueRepairId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairId"]];
                                            objSubWorkOrderIssuesRepairsLabour.laborType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborType"]];
                                            objSubWorkOrderIssuesRepairsLabour.laborCost=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborCost"]];
                                            
                                            objSubWorkOrderIssuesRepairsLabour.laborHours=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborHours"]];
                                            
                                            objSubWorkOrderIssuesRepairsLabour.laborCostType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborCostType"]];
                                            objSubWorkOrderIssuesRepairsLabour.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsActive"]]];
                                            objSubWorkOrderIssuesRepairsLabour.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedBy"]];
                                            objSubWorkOrderIssuesRepairsLabour.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedDate"]]];
                                            objSubWorkOrderIssuesRepairsLabour.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedBy"]];
                                            objSubWorkOrderIssuesRepairsLabour.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedDate"]]];
                                            objSubWorkOrderIssuesRepairsLabour.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedByDevice"]];
                                            objSubWorkOrderIssuesRepairsLabour.laborDescription=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborDescription"]];
                                            objSubWorkOrderIssuesRepairsLabour.isWarranty=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsWarranty"]]];
                                            objSubWorkOrderIssuesRepairsLabour.isDefault=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsDefault"]]];
                                            
                                            NSError *error6;
                                            [context save:&error6];
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                        
                    }
                    
                }
                
                
                //============================================================================
                //============================================================================
#pragma mark- ------------------ Sub Work Order Helper Save --------------------------
                //============================================================================
                //============================================================================
                
                //                entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
                //
                //                MechanicalSubWOTechHelperDcs *objSubWorkOrderHelper = [[MechanicalSubWOTechHelperDcs alloc]initWithEntity:entityMechanicalSubWorkOrderHelper insertIntoManagedObjectContext:context];
                
                //SubWorkOrderHelper
                if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWOTechHelperDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    NSArray *arrOfSubWorkOrderHelperFromServer=[dictOfSubWorkOrderInfo valueForKey:@"SubWOTechHelperDcs"];
                    
                    for (int k6=0; k6<arrOfSubWorkOrderHelperFromServer.count; k6++) {
                        
                        entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
                        
                        MechanicalSubWOTechHelperDcs *objSubWorkOrderHelper = [[MechanicalSubWOTechHelperDcs alloc]initWithEntity:entityMechanicalSubWorkOrderHelper insertIntoManagedObjectContext:context];
                        
                        NSDictionary *dictOfSubWorkOrderHelperInfo = arrOfSubWorkOrderHelperFromServer[k6];
                        
                        objSubWorkOrderHelper.workorderId=strGlobalWorkOrderId;
                        objSubWorkOrderHelper.subWorkOrderTechnicianId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"SubWorkOrderTechnicianId"]];
                        objSubWorkOrderHelper.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"SubWorkOrderId"]];
                        objSubWorkOrderHelper.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"EmployeeNo"]];
                        objSubWorkOrderHelper.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"IsActive"]]];
                        objSubWorkOrderHelper.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"CreatedBy"]];
                        objSubWorkOrderHelper.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"CreatedDate"]]];
                        objSubWorkOrderHelper.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"ModifiedBy"]];
                        objSubWorkOrderHelper.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"ModifiedDate"]]];
                        objSubWorkOrderHelper.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"CreatedByDevice"]];
                        objSubWorkOrderHelper.laborType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"LaborType"]];

                        NSError *error2;
                        [context save:&error2];
                        
                    }
                    
                }
                
                
#pragma mark- ---------******** Sub Work Order WorkOrderAppliedDiscountExtSerDcs Save *******----------------
                // Akshay Start //
                if(![[dictOfSubWorkOrderInfo valueForKey:@"WorkOrderAppliedDiscountExtSerDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    NSArray *arrOfWorkOrderAppliedDiscount=[dictOfSubWorkOrderInfo valueForKey:@"WorkOrderAppliedDiscountExtSerDcs"];
                    
                    for (int k3=0; k3<arrOfWorkOrderAppliedDiscount.count; k3++) {
                        
                        entityWorkOrderAppliedDiscountExtSerDcs=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
                        
                        WorkOrderAppliedDiscountExtSerDcs *objWorkOrderAppliedDiscount = [[WorkOrderAppliedDiscountExtSerDcs alloc]initWithEntity:entityWorkOrderAppliedDiscountExtSerDcs insertIntoManagedObjectContext:context];
                        
                        NSDictionary *dictOfWorkOrderAppliedDiscount = arrOfWorkOrderAppliedDiscount[k3];
                        
                        objWorkOrderAppliedDiscount.companyKey = strCompanyKeyy;
                        objWorkOrderAppliedDiscount.userName = strUserName;
                        objWorkOrderAppliedDiscount.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"SubWorkOrderId"]];
                        
                        objWorkOrderAppliedDiscount.workOrderId=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"WorkOrderId"]];
                        
                        objWorkOrderAppliedDiscount.workOrderAppliedDiscountId=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"WorkOrderAppliedDiscountId"]];
                        
                        objWorkOrderAppliedDiscount.discountSysName=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountSysName"]];
                        
                        objWorkOrderAppliedDiscount.discountType=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountType"]];
                        
                        objWorkOrderAppliedDiscount.isActive=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"IsActive"]];
                        
                        objWorkOrderAppliedDiscount.createdBy=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"CreatedBy"]];
                        
                        objWorkOrderAppliedDiscount.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"CreatedDate"]]];
                        
                        objWorkOrderAppliedDiscount.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"ModifiedBy"]];
                        
                        objWorkOrderAppliedDiscount.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"ModifiedDate"]]];
                        
                        objWorkOrderAppliedDiscount.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"CreatedByDevice"]];
                        
                        objWorkOrderAppliedDiscount.discountCode=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountCode"]];
                        
                        objWorkOrderAppliedDiscount.discountAmount=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountAmount"]];
                        
                        objWorkOrderAppliedDiscount.discountDescription=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountDescription"]];
                        
                        objWorkOrderAppliedDiscount.discountName=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountName"]];
                        
                        objWorkOrderAppliedDiscount.discountPercent=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountPercent"]];
                        
                        objWorkOrderAppliedDiscount.applyOnPartPrice=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"ApplyOnPartPrice"]];
                        
                        objWorkOrderAppliedDiscount.applyOnLaborPrice=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"ApplyOnLaborPrice"]];
                        
                        objWorkOrderAppliedDiscount.appliedDiscountAmt=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"AppliedDiscountAmt"]];
                        
                        objWorkOrderAppliedDiscount.isDiscountPercent=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"IsDiscountPercent"]];
                        
                        
                        NSError *error4;
                        [context save:&error4];
                    }
                }
                // Akshay End //

                
                //============================================================================
                //============================================================================
#pragma mark- ------------------ Sub Work Order Notes Save --------------------------
                //============================================================================
                //============================================================================
                
                //                entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
                //
                //                MechanicalSubWorkOrderNoteDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderNoteDcs alloc]initWithEntity:entityMechanicalSubWorkOrderNotes insertIntoManagedObjectContext:context];
                
                //SubWorkOrderHelper
                if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNoteDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    NSArray *arrOfSubWorkOrderNotesFromServer=[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNoteDcs"];
                    
                    for (int k7=0; k7<arrOfSubWorkOrderNotesFromServer.count; k7++) {
                        
                        entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
                        
                        MechanicalSubWorkOrderNoteDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderNoteDcs alloc]initWithEntity:entityMechanicalSubWorkOrderNotes insertIntoManagedObjectContext:context];
                        
                        NSDictionary *dictOfSubWorkOrderNotesInfo = arrOfSubWorkOrderNotesFromServer[k7];
                        
                        objSubWorkOrderNotes.workorderId=strGlobalWorkOrderId;
                        objSubWorkOrderNotes.subWorkOrderNoteId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"SubWorkOrderNoteId"]];
                        objSubWorkOrderNotes.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"SubWorkOrderId"]];
                        objSubWorkOrderNotes.note=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"Note"]];
                        objSubWorkOrderNotes.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"IsActive"]]];
                        objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"CreatedBy"]];
                        objSubWorkOrderNotes.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"CreatedDate"]]];
                        objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"ModifiedBy"]];
                        objSubWorkOrderNotes.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"ModifiedDate"]]];
                        objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"CreatedByDevice"]];
                        
                        
                        NSError *error2;
                        [context save:&error2];
                        
                    }
                    
                }
                
                NSError *error1;
                [context save:&error1];
                
                
                //============================================================================
                //============================================================================
#pragma mark- ------------------ Sub Work Order Actual Hours Save --------------------------
                //============================================================================
                //============================================================================
                
                //                entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
                //
                //                MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderActualHours = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
                
                //SubWorkOrderHelper
                if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderActualHoursDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    NSArray *arrOfSubWorkOrderActualHoursFromServer=[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderActualHoursDcs"];
                    
                    for (int k8=0; k8<arrOfSubWorkOrderActualHoursFromServer.count; k8++) {
                        
                        entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
                        
                        MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderActualHours = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
                        
                        NSDictionary *dictOfSubWorkOrderActualHoursInfo = arrOfSubWorkOrderActualHoursFromServer[k8];
                        
                        objSubWorkOrderActualHours.workorderId=strGlobalWorkOrderId;
                        objSubWorkOrderActualHours.subWOActualHourId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWOActualHourId"]];
                        objSubWorkOrderActualHours.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWorkOrderId"]];
                        objSubWorkOrderActualHours.subWOActualHourNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWOActualHourNo"]];
                        objSubWorkOrderActualHours.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"IsActive"]]];
                        objSubWorkOrderActualHours.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"CreatedBy"]];
                        objSubWorkOrderActualHours.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"CreatedDate"]]];
                        objSubWorkOrderActualHours.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"ModifiedBy"]];
                        objSubWorkOrderActualHours.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"ModifiedDate"]]];
                        objSubWorkOrderActualHours.createdByDevice=@"Web";//[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"CreatedByDevice"]];
                        objSubWorkOrderActualHours.companyKey=strCompanyKeyy;
                        objSubWorkOrderActualHours.timeOut=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"TimeOut"]]];
                        objSubWorkOrderActualHours.timeIn=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"TimeIn"]]];
                        objSubWorkOrderActualHours.actHrsDescription=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"ActHrsDescription"]];
                        objSubWorkOrderActualHours.reason=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Reason"]];
                        objSubWorkOrderActualHours.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWorkOrderNo"]];
                        objSubWorkOrderActualHours.status=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Status"]];
                        
                        objSubWorkOrderActualHours.mobileTimeIn=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"MobileTimeIn"]]];
                        objSubWorkOrderActualHours.mobileTimeOut=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"MobileTimeOut"]]];
                        objSubWorkOrderActualHours.latitude=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Latitude"]];
                        objSubWorkOrderActualHours.longitude=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Longitude"]];
                        objSubWorkOrderActualHours.employeeNo=[global getEmployeeDeatils];


                        
                        NSError *error2;
                        [context save:&error2];
                        
                        
                        
                        //============================================================================
                        //============================================================================
#pragma mark- --------------------- SubWoEmployeeWorkingTimeExtSerDcs  Save -------------------------
                        //============================================================================
                        //============================================================================
                        
                        //PaymentInfo
                        if(![[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWoEmployeeWorkingTimeExtSerDcs"] isKindOfClass:[NSArray class]])
                        {
                            
                        }
                        else
                        {
                            
                            NSArray *arrOfSubWoEmployeeWorkingTimeExtSerDcs= [dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWoEmployeeWorkingTimeExtSerDcs"];
                            
                            NSLog(@"Complete TimeSheet coming from server =====%@",arrOfSubWoEmployeeWorkingTimeExtSerDcs);

                            for (int k9=0; k9<arrOfSubWoEmployeeWorkingTimeExtSerDcs.count; k9++) {
                                
                                entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
                                
                                SubWoEmployeeWorkingTimeExtSerDcs *objSubWoEmployeeWorkingTimeExtSerDcs = [[SubWoEmployeeWorkingTimeExtSerDcs alloc]initWithEntity:entitySubWoEmployeeWorkingTimeExtSerDcs insertIntoManagedObjectContext:context];
                                

                                NSDictionary *dictOfSubWoEmployeeWorkingTimeExtSerDcs =arrOfSubWoEmployeeWorkingTimeExtSerDcs[k9];
                                
                                objSubWoEmployeeWorkingTimeExtSerDcs.subWOEmployeeWorkingTimeId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"SubWOEmployeeWorkingTimeId"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"SubWorkOrderId"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.workorderId=strGlobalWorkOrderId;
                                objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"SubWorkOrderActualHourId"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"EmployeeNo"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.employeeName=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"EmployeeName"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.workingDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"WorkingDate"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.timeSlotTitle=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"TimeSlotTitle"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"TimeSlot"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"ActualDurationInMin"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"ActualAmt"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"BillableDurationInMin"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"BillableAmt"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"WorkingType"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"IsApprove"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"IsActive"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"CreatedDate"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"CreatedBy"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"ModifiedDate"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"ModifiedBy"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"CreatedByDevice"]];
                                objSubWoEmployeeWorkingTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",_strWorkOrderId];
                                objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"IsHelper"]];

                                NSError *errorSubWoEmployeeWorkingTimeExtSerDcs;
                                [context save:&errorSubWoEmployeeWorkingTimeExtSerDcs];

                            }
                            
                        }

                    }
                    
                }
                
                
                //Yaha Par Payment bhi
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Payment Info Save *******----------------
                //============================================================================
                //============================================================================
                
                entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
                
                MechanicalSubWOPaymentDetailDcs *objPaymentInfoServiceAuto = [[MechanicalSubWOPaymentDetailDcs alloc]initWithEntity:entityPaymentInfoServiceAuto insertIntoManagedObjectContext:context];
                
                //PaymentInfo
                if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWOPaymentDetailDcs"] isKindOfClass:[NSDictionary class]])
                {
                    
                }
                else
                {
                    NSDictionary *dictOfPaymentInfo = [dictOfSubWorkOrderInfo valueForKey:@"SubWOPaymentDetailDcs"];
                    objPaymentInfoServiceAuto.subWoPaymentId=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"SubWoPaymentId"]];
                    objPaymentInfoServiceAuto.recieptPath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"RecieptPath"]];
                    
                    objPaymentInfoServiceAuto.workorderId=strGlobalWorkOrderId;
                    
                    objPaymentInfoServiceAuto.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"SubWorkOrderId"]];;
                    
                    objPaymentInfoServiceAuto.paymentMode=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"PaymentMode"]];
                    
                    objPaymentInfoServiceAuto.paidAmount=[NSString stringWithFormat:@"%.02f", [[dictOfPaymentInfo valueForKey:@"PaidAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"PaidAmount"]];
                    
                    objPaymentInfoServiceAuto.checkNo=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckNo"]];
                    
                    objPaymentInfoServiceAuto.drivingLicenseNo=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"DrivingLicenseNo"]];
                    
                    objPaymentInfoServiceAuto.expirationDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ExpirationDate"]]];
                    
                    objPaymentInfoServiceAuto.checkFrontImagePath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckFrontImagePath"]];
                    
                    objPaymentInfoServiceAuto.checkBackImagePath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckBackImagePath"]];
                    
                    objPaymentInfoServiceAuto.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CreatedDate"]]];
                    
                    objPaymentInfoServiceAuto.createdBy=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CreatedBy"]];
                    
                    objPaymentInfoServiceAuto.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ModifiedDate"]]];
                    
                    objPaymentInfoServiceAuto.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ModifiedBy"]];
                    
                    objPaymentInfoServiceAuto.userName=strUserName;
                    
                }
                
                
                
                //============================================================================
                //============================================================================
#pragma mark- --------------------- SubWOCompleteTimeExtSerDcs  Save -------------------------
                //============================================================================
                //============================================================================
                
                entitySubWOCompleteTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWOCompleteTimeExtSerDcs" inManagedObjectContext:context];
                
                SubWOCompleteTimeExtSerDcs *objSubWOCompleteTimeExtSerDcs = [[SubWOCompleteTimeExtSerDcs alloc]initWithEntity:entitySubWOCompleteTimeExtSerDcs insertIntoManagedObjectContext:context];
                
                //PaymentInfo
                if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWOCompleteTimeExtSerDcs"] isKindOfClass:[NSDictionary class]])
                {
                    
                }
                else
                {
                    NSDictionary *dictOfSubWOCompleteTimeExtSerDcs = [dictOfSubWorkOrderInfo valueForKey:@"SubWOCompleteTimeExtSerDcs"];
                    objSubWOCompleteTimeExtSerDcs.subWOCompleteTimeId=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"SubWOCompleteTimeId"]];
                    objSubWOCompleteTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"SubWorkOrderId"]];
                    objSubWOCompleteTimeExtSerDcs.workorderId=strGlobalWorkOrderId;
                    objSubWOCompleteTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"ActualDurationInMin"]];
                    objSubWOCompleteTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"ActualAmt"]];
                    objSubWOCompleteTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"BillableDurationInMin"]];
                    objSubWOCompleteTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"BillableAmt"]];
                    objSubWOCompleteTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"TimeSlot"]];
                    objSubWOCompleteTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"WorkingType"]];
                    objSubWOCompleteTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"IsActive"]];
                    objSubWOCompleteTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"CreatedDate"]];
                    objSubWOCompleteTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"CreatedBy"]];
                    objSubWOCompleteTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"ModifiedDate"]];
                    objSubWOCompleteTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"ModifiedBy"]];
                    objSubWOCompleteTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"CreatedByDevice"]];
                    objSubWOCompleteTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",_strWorkOrderId];

                }
                
                

            }
            
        }
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------******** Mechanical Equipment(AccountItemHistoryExtSerDcs) Id Save *******----------------
        //============================================================================
        //============================================================================
        
        //Email Id Save
        if(![[dictWorkOrderDetail valueForKey:@"AccountItemHistoryExtSerDcs"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            
            NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"AccountItemHistoryExtSerDcs"];//AccountItemHistoryExtSerDcs
            for (int j=0; j<arrEmailDetail.count; j++)
            {
                //AccountItemHistoryExtSerDcs Detail Entity
                entityMechanicalEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:context];
                
                MechanicalWoEquipment *objEmailDetail = [[MechanicalWoEquipment alloc]initWithEntity:entityMechanicalEquipment insertIntoManagedObjectContext:context];
                NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                
                objEmailDetail.companyKey=strCompanyKeyy;
                
                objEmailDetail.userName=strUserName;
                objEmailDetail.workorderId=strGlobalWorkOrderId;
                // objEmailDetail.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"SubWorkOrderId"]];
                objEmailDetail.subWorkOrderId=strSubWorkOrderIdTempToSave;
                objEmailDetail.workOrderNo=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"WorkOrderNo"]];
                objEmailDetail.accountItemHistoryId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"AccountItemHistoryId"]];
                objEmailDetail.accountId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"AccountId"]];
                objEmailDetail.itemName=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ItemName"]];
                objEmailDetail.itemcode=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Itemcode"]];
                objEmailDetail.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"SubWorkOrderNo"]];
                objEmailDetail.serialNumber=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"SerialNumber"]];
                objEmailDetail.modelNumber=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModelNumber"]];
                objEmailDetail.manufacturer=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Manufacturer"]];
                objEmailDetail.qty=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Qty"]];
                objEmailDetail.installationDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"InstallationDate"]];
                objEmailDetail.warrantyExpireDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"WarrantyExpireDate"]];
                objEmailDetail.accountNo=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"AccountNo"]];
                objEmailDetail.serviceAddressId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ServiceAddressId"]];
                objEmailDetail.barcodeUrl=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"BarcodeUrl"]];
                objEmailDetail.installedArea=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"InstalledArea"]];
                objEmailDetail.equipmentDesc=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"EquipmentDesc"]];
                objEmailDetail.createdByDevice=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedByDevice"]];
                
                objEmailDetail.createdDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedDate"]];
                
                objEmailDetail.createdBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedBy"]];
                
                objEmailDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedDate"]];
                
                objEmailDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedBy"]];
                
                if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsActive"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsActive"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsActive"]] isEqualToString:@"True"])
                {
                    objEmailDetail.isActive=@"true";
                }
                else
                {
                    objEmailDetail.isActive=@"false";
                }
                
                NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ItemCustomName"]];
                
                if ([strItemCustomName isEqualToString:@"(null)"]) {
                    
                    strItemCustomName=@"";
                    
                }
                NSString *stritemNo=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ItemNo"]];
                
                if ([stritemNo isEqualToString:@"(null)"]) {
                    
                    stritemNo=@"";
                    
                }

                //itemNo
                objEmailDetail.itemCustomName=[NSString stringWithFormat:@"%@",strItemCustomName];
                objEmailDetail.itemNo=stritemNo;

                NSError *error222;
                [context save:&error222];
            }
        }

        //============================================================================
        //============================================================================
#pragma mark- ---------******** Mechanical ServiceAddressPOCDetailDcs Save *******----------------
        //============================================================================
        //============================================================================
        
        //Email Id Save
        if(![[dictWorkOrderDetail valueForKey:@"ServiceAddressPOCDetailDcs"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            
            NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"ServiceAddressPOCDetailDcs"];//AccountItemHistoryExtSerDcs
            for (int j=0; j<arrEmailDetail.count; j++)
            {
                //AccountItemHistoryExtSerDcs Detail Entity
                entityMechanicalServiceAddressPOCDetailDcs=[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context];
                
                ServiceAddressPOCDetailDcs *objEmailDetail = [[ServiceAddressPOCDetailDcs alloc]initWithEntity:entityMechanicalServiceAddressPOCDetailDcs insertIntoManagedObjectContext:context];
                NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                
                objEmailDetail.companyKey=strCompanyKeyy;
                objEmailDetail.userName=strUserName;
                objEmailDetail.workorderId=strGlobalWorkOrderId;
                objEmailDetail.pocDetails=arrEmailDetail;
                
                NSError *error2222;
                [context save:&error2222];
            }
        }
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------******** Mechanical BillingAddressPOCDetailDcs Save *******----------------
        //============================================================================
        //============================================================================
        
        //Email Id Save
        if(![[dictWorkOrderDetail valueForKey:@"BillingAddressPOCDetailDcs"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            
            NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"BillingAddressPOCDetailDcs"];//AccountItemHistoryExtSerDcs
            for (int j=0; j<arrEmailDetail.count; j++)
            {
                //AccountItemHistoryExtSerDcs Detail Entity
                entityMechanicalBillingAddressPOCDetailDcs=[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context];
                
                BillingAddressPOCDetailDcs *objEmailDetail = [[BillingAddressPOCDetailDcs alloc]initWithEntity:entityMechanicalBillingAddressPOCDetailDcs insertIntoManagedObjectContext:context];
                NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                
                objEmailDetail.companyKey=strCompanyKeyy;
                objEmailDetail.userName=strUserName;
                objEmailDetail.workorderId=strGlobalWorkOrderId;
                objEmailDetail.pocDetails=arrEmailDetail;
                
                NSError *error22222;
                [context save:&error22222];
            }
        }

        
#pragma mark- ---------******** Mechanical AccountDiscountExtSerDcs Save *******----------------
        // Akshay Start//
        if(![[dictWorkOrderDetail valueForKey:@"AccountDiscountExtSerDcs"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            
            NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"AccountDiscountExtSerDcs"];
            
            for (int j=0; j<arrEmailDetail.count; j++)
            {
                //AccountItemHistoryExtSerDcs Detail Entity
                entityAccountDiscount=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
                
                AccountDiscountExtSerDcs *objAccountDiscount = [[AccountDiscountExtSerDcs alloc]initWithEntity:entityAccountDiscount insertIntoManagedObjectContext:context];
                NSMutableDictionary *dictAccDiscount=[[NSMutableDictionary alloc]init];
                dictAccDiscount=[arrEmailDetail objectAtIndex:j];
                
                objAccountDiscount.companyKey=strCompanyKeyy;
                objAccountDiscount.userName=strUserName;
                objAccountDiscount.workorderId=strGlobalWorkOrderId;
                objAccountDiscount.accountNo = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"AccountNo"]];
                
                objAccountDiscount.discountAmount = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountAmount"]];
                
                objAccountDiscount.discountSetupSysName = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountSetupSysName"]];
                objAccountDiscount.discountSetupName = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountSetupName"]];
                objAccountDiscount.discountCode = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountCode"]];
                
                objAccountDiscount.isDiscountPercent = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"IsDiscountPercent"]];
                objAccountDiscount.discountPercent = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountPercent"]];
                objAccountDiscount.sysName = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"SysName"]];
                
                objAccountDiscount.name = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"Name"]];
                
                objAccountDiscount.isActive = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"IsActive"]];
                
                objAccountDiscount.isDelete = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"IsDeleted"]];
                
                objAccountDiscount.discountDescription = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountDescription"]];
                
                objAccountDiscount.departmentSysName = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DepartmentSysName"]];
                objAccountDiscount.createdByDevice = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"CreatedByDevice"]];
                
                
                
                NSError *error22222;
                [context save:&error22222];
            }
        }
        // Akshay End

        
        //============================================================================
        //============================================================================
#pragma mark- --------- Mechanical WoOtherDocExtSerDcs Save ----------------
        //============================================================================
        //============================================================================
        
        //WoOtherDocExtSerDcs Save
        if(![[dictWorkOrderDetail valueForKey:@"WoOtherDocExtSerDcs"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            
            NSArray *arrWoOtherDocExtSerDcs=[dictWorkOrderDetail valueForKey:@"WoOtherDocExtSerDcs"];//WoOtherDocExtSerDcs
            for (int j=0; j<arrWoOtherDocExtSerDcs.count; j++)
            {
                //WoOtherDocExtSerDcs Detail Entity
                entityMechanicalWoOtherDocExtSerDcs=[NSEntityDescription entityForName:@"WoOtherDocExtSerDcs" inManagedObjectContext:context];
                
                WoOtherDocExtSerDcs *objWoOtherDocExtSerDcs = [[WoOtherDocExtSerDcs alloc]initWithEntity:entityMechanicalWoOtherDocExtSerDcs insertIntoManagedObjectContext:context];
                NSMutableDictionary *dictWoOtherDocExtSerDcs=[[NSMutableDictionary alloc]init];
                dictWoOtherDocExtSerDcs=[arrWoOtherDocExtSerDcs objectAtIndex:j];
                
                objWoOtherDocExtSerDcs.companyKey=strCompanyKeyy;
                objWoOtherDocExtSerDcs.userName=strUserName;
                objWoOtherDocExtSerDcs.workOrderId=strGlobalWorkOrderId;
                objWoOtherDocExtSerDcs.createdByDevice = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"CreatedByDevice"]];
                objWoOtherDocExtSerDcs.wOOtherDocId = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WOOtherDocId"]];
                objWoOtherDocExtSerDcs.workOrderNo = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WorkOrderNo"]];
                objWoOtherDocExtSerDcs.leadNo = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"LeadNo"]];
                objWoOtherDocExtSerDcs.wOOtherDocPath = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WOOtherDocPath"]];
                objWoOtherDocExtSerDcs.wOOtherDocTitle = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WOOtherDocTitle"]];
                objWoOtherDocExtSerDcs.woOtherDocDescription = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WoOtherDocDescription"]];
                objWoOtherDocExtSerDcs.createdDate = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"CreatedDate"]];
                objWoOtherDocExtSerDcs.createdBy = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"CreatedBy"]];
                objWoOtherDocExtSerDcs.modifiedDate = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"ModifiedDate"]];
                objWoOtherDocExtSerDcs.modifiedBy = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"ModifiedBy"]];
                
                NSError *errorObjWoOtherDocExtSerDcs;
                [context save:&errorObjWoOtherDocExtSerDcs];
            }
        }

        
        //Final Save
        NSError *error;
        [context save:&error];
        
        
    } else {
        
        //Kuch ni karna already save ho gya hai
        
    }
}

//============================================================================
//============================================================================
#pragma mark - -------------------------Deleting Mechanical Work Orders-------------------------
//============================================================================
//============================================================================


-(void)deleteWorkOrderDetailsService{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete WorkOrderDetailsService Data
    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


-(void)deleteMechanicalSubWorkOrder{
    
    //  Delete entitySubWorkOrderIssues Data
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}
-(void)deleteMechanicalSubWOIssueRepairDcs{
    
    //  Delete entitySubWorkOrderIssuesRepair Data
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteMechanicalSubWOIssueRepairPartDcs{
    
    //  Delete entitySubWorkOrderIssuesRepairParts Data
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteMechanicalSubWOPaymentDetailDcs{
    
    //  Delete entitySubWorkOrderIssuesRepairParts Data
    entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",_strSubWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteMechanicalSubWorkOrderActualHoursDcs{
    
    //  Delete entitySubWorkOrderIssuesRepairParts Data
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteMechanicalSubWorkOrderIssueDcs{
    
    //  Delete MechanicalSubWorkOrderIssueDcs Data
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteMechanicalSubWorkOrderNoteDcs{
    
    //  Delete MechanicalSubWorkOrderIssueDcs Data
    entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


-(void)deleteMechanicalSubWOTechHelperDcs{
    
    //  Delete MechanicalSubWorkOrderIssueDcs Data
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


-(void)deleteMechannicalSubWOIssueRepairLaborDcs{
    
    //  Delete entitySubWorkOrderIssuesRepairLabour Data
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteEmailDetailServiceAuto{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete entityEmailDetailServiceAuto Data
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteWoOtherDocExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete entityMechanicalWoOtherDocExtSerDcs Data
    entityMechanicalWoOtherDocExtSerDcs=[NSEntityDescription entityForName:@"WoOtherDocExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"WoOtherDocExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteImageDetailsServiceAuto{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete WorkOrderDetailsService Data
    entityImageDetailServiceAuto=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteEquipDetailMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete entityMechanicalEquipment Data
    entityMechanicalEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteServiceAddressPOCDetailDcsMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete entityMechanicalServiceAddressPOCDetailDcs Data
    entityMechanicalServiceAddressPOCDetailDcs=[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


-(void)deleteBillingAddressPOCDetailDcsMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete entityMechanicalBillingAddressPOCDetailDcs Data
    entityMechanicalBillingAddressPOCDetailDcs=[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteSubWoEmployeeWorkingTimeExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete SubWoEmployeeWorkingTimeExtSerDcs Data//SubWoEmployeeWorkingTimeExtSerDcs
    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteSubWOCompleteTimeExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete SubWOCompleteTimeExtSerDcs Data
    entitySubWOCompleteTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWOCompleteTimeExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"SubWOCompleteTimeExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteAllFromDB{
    
    [self deleteWorkOrderDetailsService];
    [self deleteMechanicalSubWorkOrder];
    [self deleteMechanicalSubWOIssueRepairDcs];
    [self deleteMechanicalSubWOIssueRepairPartDcs];
    [self deleteMechanicalSubWOPaymentDetailDcs];
    [self deleteMechanicalSubWorkOrderActualHoursDcs];
    [self deleteMechanicalSubWorkOrderIssueDcs];
    [self deleteMechanicalSubWorkOrderNoteDcs];
    [self deleteMechanicalSubWOTechHelperDcs];
    [self deleteMechannicalSubWOIssueRepairLaborDcs];
    [self deleteEmailDetailServiceAuto];
    [self deleteImageDetailsServiceAuto];
    [self deleteEquipDetailMechanical];
    [self deleteServiceAddressPOCDetailDcsMechanical];
    [self deleteBillingAddressPOCDetailDcsMechanical];
    [self deleteSubWoEmployeeWorkingTimeExtSerDcs];
    [self deleteSubWOCompleteTimeExtSerDcs];
    [self deleteWoOtherDocExtSerDcs];

    //Akshay
    [self deleteWorkOrderAccountDiscountExtSerDcs];
    [self deleteWorkOrderAppliedDiscountExtSerDcs];

}

-(void)syncMechanicalActualHours :(NSString *)strWorkOrderIdToFetch :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWOActualHourId :(NSString *)strNotificationTypeName{
    
    strNotificationTypeNameGlobal=strNotificationTypeName;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"]];
    strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    strDefaultEmployeeEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    strCompanyKeyy     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strEmployeeNumber=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    dictDetailsFortblView=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    strLeadName=[defsLead valueForKey:@"defsLeadName"];
    
    NSString *strServiceReportType=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportType"]];
    
    if ([strServiceReportType isEqualToString:@"CompanyEmail"]) {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportEmail"]];
        
    } else {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
        
    }
    
    [self methodOnLoad];
    
    [self fetchSubWorkOrderActualHrsFromDBToSync:strWorkOrderIdToFetch :strSubWorkOrderIdToFetch :strSubWOActualHourId];
    
    
}

-(void)fetchSubWorkOrderActualHrsFromDBToSync :(NSString *)strWorkOrderIdToFetch :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWOActualHourId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    //subWOActualHourId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@",strWorkOrderIdToFetch,strSubWorkOrderIdToFetch,strSubWOActualHourId];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    NSArray *arrObjTemp = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    
    if ([arrObjTemp count] == 0)
    {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:strNotificationTypeNameGlobal object:self];
        
    }
    else
    {
        
        NSManagedObject *objTempActualHrs=arrObjTemp[0];
        
        NSArray *arrLeadDetailKey;
        NSMutableArray *arrLeadDetailValue;
        arrLeadDetailValue=[[NSMutableArray alloc]init];
        arrLeadDetailKey = [[[objTempActualHrs entity] attributesByName] allKeys];
        
        for (int i=0; i<arrLeadDetailKey.count; i++)
        {
            NSString *str;
            str=([[objTempActualHrs valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempActualHrs valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
            if ([str isKindOfClass:[NSDate class]]) {
                
                
            }else if ([str isKindOfClass:[NSArray class]]) {
                
                
            }
            else{
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
            }
            [arrLeadDetailValue addObject:str];
        }
        
        
        NSMutableArray *tempKeyArray=[[NSMutableArray alloc]init];
        
        [tempKeyArray addObjectsFromArray:arrLeadDetailKey];
        
        [tempKeyArray addObject:@"EmployeeEmail"];
        
        [arrLeadDetailValue addObject:strFromEmail];
        
        arrLeadDetailKey=tempKeyArray;
        
        NSDictionary *dictTempActualHrs = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
        
        [self finalSynActualHrs:strWorkOrderIdToFetch :strSubWorkOrderIdToFetch :strSubWOActualHourId :dictTempActualHrs];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
    }
}


-(void)finalSynActualHrs :(NSString *)strWorkOrderIdToFetch :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWOActualHourId :(NSDictionary*)dictActualHrs
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:strNotificationTypeNameGlobal object:self];
        
    }
    else
    {
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dictActualHrs])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictActualHrs options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Final dictActualHrs  JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        //    NSString *strUrl = [NSString stringWithFormat:@"%@%@?EmployeeEmail=%@",strServiceUrlMainServiceAutomation,UrlUpdateMechanicalActualHours,strFromEmail];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlUpdateMechanicalActualHours];
        
        //============================================================================
        //============================================================================
        NSDictionary *dictTemp;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"ActualHrs" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     //Sending Dynamic Json
                     
                     //[DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         if ([response  isKindOfClass:[NSString class]]) {
                             
                             
                         }else if(response==nil){
                             
                             
                             
                         }
                         else{
                             
                             [self deleteMechanicalSubWorkOrderActualHoursDcsAfterSync:strWorkOrderIdToFetch :strSubWorkOrderIdToFetch :strSubWOActualHourId :response];
                             
                         }
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:strNotificationTypeNameGlobal object:self];
                         
                     }
                     else
                     {
                         [[NSNotificationCenter defaultCenter] postNotificationName:strNotificationTypeNameGlobal object:self];
                         
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
    }
}

-(void)deleteMechanicalSubWorkOrderActualHoursDcsAfterSync :(NSString *)strWorkOrderIdToFetch :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWOActualHourId :(NSDictionary*)dictActualHrs{
    
    //  Delete entitySubWorkOrderIssuesRepairParts Data
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@",strWorkOrderIdToFetch,strSubWorkOrderIdToFetch,strSubWOActualHourId];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self saveActualHrsAfterSync:dictActualHrs :strWorkOrderIdToFetch];
    
}

-(void)saveActualHrsAfterSync :(NSDictionary*)dictActualHrs :(NSString *)strWorkOrderIdToFetch{
    
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderActualHours = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    NSDictionary *dictOfSubWorkOrderActualHoursInfo = dictActualHrs;
    
    objSubWorkOrderActualHours.workorderId=strWorkOrderIdToFetch;
    objSubWorkOrderActualHours.subWOActualHourId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWOActualHourId"]];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWOActualHourId"]] forKey:@"subWOActualHourId"];
    [defs synchronize];
    
    objSubWorkOrderActualHours.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWorkOrderId"]];
    objSubWorkOrderActualHours.subWOActualHourNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWOActualHourNo"]];
    objSubWorkOrderActualHours.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"IsActive"]]];
    objSubWorkOrderActualHours.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"CreatedBy"]];
    objSubWorkOrderActualHours.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"CreatedDate"]]];
    objSubWorkOrderActualHours.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"ModifiedBy"]];
    objSubWorkOrderActualHours.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"ModifiedDate"]]];
    objSubWorkOrderActualHours.createdByDevice=@"Web";//[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"CreatedByDevice"]];
    objSubWorkOrderActualHours.companyKey=strCompanyKeyy;
    objSubWorkOrderActualHours.timeOut=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"TimeOut"]]];
    objSubWorkOrderActualHours.timeIn=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"TimeIn"]]];
    
    objSubWorkOrderActualHours.mobileTimeIn=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"MobileTimeIn"]]];
    objSubWorkOrderActualHours.mobileTimeOut=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"MobileTimeOut"]]];
    

    objSubWorkOrderActualHours.actHrsDescription=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"ActHrsDescription"]];
    objSubWorkOrderActualHours.reason=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Reason"]];
    objSubWorkOrderActualHours.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWorkOrderNo"]];
    objSubWorkOrderActualHours.status=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Status"]];
    objSubWorkOrderActualHours.latitude=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Latitude"]];
    objSubWorkOrderActualHours.longitude=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Longitude"]];
    objSubWorkOrderActualHours.employeeNo=[global getEmployeeDeatils];

    NSError *error2;
    [context save:&error2];
    
}


-(void)fetchSubWorkOrderFromDBForIds{
    
    arrOfSubWorkOrderId=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        
        
        
    }
    else
    {
        
        for (int j=0; j<arrAllObjMechanicalSubWorkOrder.count; j++) {
            
            matchesMechanicalSubWorkOrder=arrAllObjMechanicalSubWorkOrder[j];
            
            NSString *strSubWorkOrderId=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrder valueForKey:@"SubWorkOrderId"]];
            
            [arrOfSubWorkOrderId addObject:strSubWorkOrderId];
            
        }
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)fetchMechanicalDynamicForm{
    
    [self fetchSubWorkOrderFromDBForIds];
    
    NSMutableArray *arrTempDynamic=[[NSMutableArray alloc]init];
    
    for (int k1=0; k1<arrOfSubWorkOrderId.count; k1++) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];//ServiceDynamicForm
        entityServiceDynamic=[NSEntityDescription entityForName:@"MechanicalDynamicForm" inManagedObjectContext:context];
        requestNewServiceDynamic = [[NSFetchRequest alloc] init];
        [requestNewServiceDynamic setEntity:entityServiceDynamic];
        
        // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",arrOfSubWorkOrderId[k1]];
        
        [requestNewServiceDynamic setPredicate:predicate];
        
        sortDescriptorServiceDynamic = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
        sortDescriptorsServiceDynamic = [NSArray arrayWithObject:sortDescriptorServiceDynamic];
        
        [requestNewServiceDynamic setSortDescriptors:sortDescriptorsServiceDynamic];
        
        self.fetchedResultsControllerServiceDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewServiceDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerServiceDynamic setDelegate:self];
        
        // Perform Fetch
        NSError *error = nil;
        [self.fetchedResultsControllerServiceDynamic performFetch:&error];
        arrAllObjServiceDynamic = [self.fetchedResultsControllerServiceDynamic fetchedObjects];
        if ([arrAllObjServiceDynamic count] == 0)
        {
            
            
        }
        else
        {
            
            
            
            for (int k=0; k<arrAllObjServiceDynamic.count; k++) {
                
                matchesServiceDynamic=arrAllObjServiceDynamic[k];
                NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
                
                NSDictionary *dictDataService;
                
                if ([arrTemp isKindOfClass:[NSDictionary class]]) {
                    
                    dictDataService=(NSDictionary*)arrTemp;
                    
                } else {
                    
                    dictDataService=arrTemp[0];
                    
                }
                
                [arrTempDynamic addObject:dictDataService];
                
            }
            
        }
        if (error) {
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        } else {
            
        }
        
        
    }
    
    if (arrTempDynamic.count==0) {
        
        [self sendingWorkOrderToServer];
        
    } else {
        
        [self sendingFinalDynamicJsonToServerMechanical:arrTempDynamic];
        
    }
}

-(void)sendingFinalDynamicJsonToServerMechanical :(NSMutableArray*)dictFinalDynamicJson{
    
    
    //  NSDictionary *dictFinalDynamicJson=(NSDictionary*)arrTemp;
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL Mechanical FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlMechanicalDynamicFormSubmission];
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     [matchesServiceDynamic setValue:@"Yes" forKey:@"isSentToServer"];
                     NSError *error1;
                     [context save:&error1];
                     
                     //                     NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
                     //
                     //                     BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
                     //
                     //                     if (isEquipEnabled) {
                     //
                     //                         [self FetchFromCoreDataToSendServiceDynamicEquipment];
                     //
                     //                     }else{
                     
                     [self sendingWorkOrderToServer];
                     
                     //                     }
                     
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     if ([str containsString:@"Success"]) {
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                     }else{
                         
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                     [DejalBezelActivityView removeView];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}



-(void)fetchRepairToUpdateRepairAmt:(NSString *)strWoId :(NSString *)strSubWoId :(NSString *)strSubWoIssuesId :(NSString *)strRepairId :(NSString *)strRepairAmt{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWoId,strSubWoId,strRepairId,strSubWoIssuesId];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        
    }
    else
    {
        
        matchesSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[0];
        
        [matchesSubWorkOrderIssuesRepair setValue:strRepairAmt forKey:@"repairAmt"];
        
        NSError *error;
        [context save:&error];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

// akshay Start //
-(void)fetchAccountDiscountExtSerDcs
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAccountDiscount=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
    requestAccountDiscount = [[NSFetchRequest alloc] init];
    [requestAccountDiscount setEntity:entityAccountDiscount];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    
    [requestAccountDiscount setPredicate:predicate];
    
    sortDescriptorAccDiscount = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsAccDiscount = [NSArray arrayWithObject:sortDescriptorAccDiscount];
    
    [requestAccountDiscount setSortDescriptors:sortDescriptorsAccDiscount];
    
    self.fetchedResultsControllerAccountDiscount = [[NSFetchedResultsController alloc] initWithFetchRequest:requestAccountDiscount managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerAccountDiscount setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerAccountDiscount performFetch:&error];
    NSArray* arrAccountDiscount = [self.fetchedResultsControllerAccountDiscount fetchedObjects];
    
    NSMutableArray *arrayAccountDiscont = [NSMutableArray new];
    
    if ([arrAccountDiscount count] == 0)
    {
        
    }
    else
    {
        for(NSManagedObject *accDiscount in arrAccountDiscount)
        {
            NSArray *keys = [[[accDiscount entity] attributesByName] allKeys];
            NSDictionary *dict = [accDiscount dictionaryWithValuesForKeys:keys];
            [arrayAccountDiscont addObject:dict];
            
        }
    }
    
    [dictFinal setObject:arrayAccountDiscont forKey:@"AccountDiscountExtSerDcs"];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}
-(void)fetchWorkOrderAppliedDiscountExtSerDcs :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrderAppliedDiscountExtSerDcs=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    requestWorkOrderAppliedDiscountExtSerDcs = [[NSFetchRequest alloc] init];
    [requestWorkOrderAppliedDiscountExtSerDcs setEntity:entityWorkOrderAppliedDiscountExtSerDcs];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && subWorkOrderId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch];
    //   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && subWorkOrderId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdToFetch];
    
    [requestWorkOrderAppliedDiscountExtSerDcs setPredicate:predicate];
    
    //    sortDescriptorWorkOrderAppliedDiscountExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptorWorkOrderAppliedDiscountExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:YES];
    sortDescriptorsWorkOrderAppliedDiscountExtSerDcs = [NSArray arrayWithObject:sortDescriptorWorkOrderAppliedDiscountExtSerDcs];
    
    [requestWorkOrderAppliedDiscountExtSerDcs setSortDescriptors:sortDescriptorsWorkOrderAppliedDiscountExtSerDcs];
    
    self.fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWorkOrderAppliedDiscountExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs performFetch:&error];
    arrAllObjWorkOrderAppliedDiscountExtSerDcs = [self.fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs fetchedObjects];
    if ([arrAllObjWorkOrderAppliedDiscountExtSerDcs count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"WorkOrderAppliedDiscountExtSerDcs"];
        
    }
    else
    {
        NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjWorkOrderAppliedDiscountExtSerDcs.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjWorkOrderAppliedDiscountExtSerDcs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
//            NSMutableDictionary *dictAppliedDiscount = [[NSMutableDictionary alloc]initWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
//
//            if ([[dictAppliedDiscount valueForKey:@"isDiscountPercent"] isEqualToString:@"1"] || [[dictAppliedDiscount valueForKey:@"isDiscountPercent"] isEqualToString:@"True"] || [[dictAppliedDiscount valueForKey:@"isDiscountPercent"] isEqualToString:@"true"]) {
//
//                [dictAppliedDiscount setValue:@"True" forKey:@"isDiscountPercent"];
//
//            } else {
//
//                [dictAppliedDiscount setValue:@"False" forKey:@"isDiscountPercent"];
//
//            }
            
            [arrTempNotes addObject:dictTempNotes];
            //[arrTempNotes addObject:dictAppliedDiscount];

        }
        
        [dictSubWorkOrderDetails setObject:arrTempNotes forKey:@"WorkOrderAppliedDiscountExtSerDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)deleteWorkOrderAccountDiscountExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete WorkOrderDetailsService Data
    entityAccountDiscount=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)deleteWorkOrderAppliedDiscountExtSerDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete SubWOCompleteTimeExtSerDcs Data
    entityWorkOrderAppliedDiscountExtSerDcs=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",_strSubWorkOrderId];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",_strWorkOrderId];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}
// akshay End //


-(void)fetchRepairToUpdateRepairQty:(NSString *)strWoId :(NSString *)strSubWoId :(NSString *)strSubWoIssuesId :(NSString *)strRepairId :(NSString *)strRepairQty{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWoId,strSubWoId,strRepairId,strSubWoIssuesId];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        
    }
    else
    {
        
        matchesSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[0];
        
        [matchesSubWorkOrderIssuesRepair setValue:strRepairQty forKey:@"qty"];
        
        NSError *error;
        [context save:&error];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


@end

