//
//  SendMailMechanicaliPadViewController.h
//  DPS
//  peSTream 
//  Created by Saavan Patidar on 23/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
// 

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface SendMailMechanicaliPadViewController : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

{
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityWorkOderDetailServiceAuto,
    *entityImageDetailServiceAuto,
    *entityEmailDetailServiceAuto,
    *entityPaymentInfoServiceAuto,
    *entityChemicalListDetailServiceAuto,
    *entityModifyDateServiceAuto,
    *entityCompanyDetailServiceAuto,
    *entityWOProductServiceAuto,*entityMechanicalEquipment,
    *entityMechanicalServiceAddressPOCDetailDcs,*entityMechanicalWoOtherDocExtSerDcs,
    *entityMechanicalBillingAddressPOCDetailDcs,*entitySubWOCompleteTimeExtSerDcs,*entitySubWoEmployeeWorkingTimeExtSerDcs,*entityAccountDiscount,*entityWorkOrderAppliedDiscountExtSerDcs;    NSEntityDescription *entityServiceModifyDate;
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
    NSEntityDescription *entityServiceDynamic;
    NSFetchRequest *requestNewServiceDynamic;
    NSSortDescriptor *sortDescriptorServiceDynamic;
    NSArray *sortDescriptorsServiceDynamic;
    NSManagedObject *matchesServiceDynamic;
    NSArray *arrAllObjServiceDynamic;
    
    NSEntityDescription *entitySubWorkOrderIssues,*entityMechanicalSubWorkOrderHelper,*entityMechanicalSubWorkOrderNotes,*entityMechanicalSubWorkOrderActualHrs,*entitySubWorkOrderIssuesRepair,*entitySubWorkOrderIssuesRepairParts,*entitySubWorkOrderIssuesRepairLabour,*entitySubWorkOrderMechanicalEquipment;
    
    
    NSFetchRequest *requestSubWorkOrderIssues,*requestSubWorkOrderHelper,*requestSubWorkOrderNotes,*requestSubWorkOrderActualHrs,*requestSubWorkOrderIssuesRepair,*requestSubWorkOrderIssuesRepairParts,*requestSubWorkOrderIssuesRepairLabour,*requestSubWorkOrderMechanicalEquipment,*requestSubWOCompleteTimeExtSerDcs,*requestSubWoEmployeeWorkingTimeExtSerDcs,*requestAccountDiscount,*requestWorkOrderAppliedDiscountExtSerDcs;
    
    
    NSSortDescriptor *sortDescriptorSubWorkOrderIssues,*sortDescriptorSubWorkOrderHelper,*sortDescriptorSubWorkOrderNotes,*sortDescriptorSubWorkOrderActualHrs,*sortDescriptorSubWorkOrderIssuesRepair,*sortDescriptorSubWorkOrderIssuesRepairParts,*sortDescriptorSubWorkOrderIssuesRepairLabour,*sortDescriptorSubWorkOrderMechanicalEquipment,*sortDescriptorSubWOCompleteTimeExtSerDcs,*sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorAccDiscount,*sortDescriptorWorkOrderAppliedDiscountExtSerDcs;
    
    
    NSArray *sortDescriptorsSubWorkOrderIssues,*sortDescriptorsSubWorkOrderHelper,*sortDescriptorsSubWorkOrderNotes,*sortDescriptorsSubWorkOrderActualHrs,*sortDescriptorsSubWorkOrderIssuesRepair,*sortDescriptorsSubWorkOrderIssuesRepairParts,*sortDescriptorsSubWorkOrderIssuesRepairLabour,*sortDescriptorsSubWorkOrderMechanicalEquipment,*sortDescriptorsSubWOCompleteTimeExtSerDcs,*sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorsAccDiscount,*sortDescriptorsWorkOrderAppliedDiscountExtSerDcs;
    
    
    NSManagedObject *matchesSubWorkOrderIssues,*matchesSubWorkOrderHelper,*matchesSubWorkOrderNotes,*matchesSubWorkOrderActualHrs,*matchesSubWorkOrderIssuesRepair,*matchesSubWorkOrderIssuesRepairParts,*matchesSubWorkOrderIssuesRepairLabour,*matchesSubWorkOrderMechanicalEquipment,*matchesSubWOCompleteTimeExtSerDcs,*matchesSubWoEmployeeWorkingTimeExtSerDcs;
    
    
    NSArray *arrAllObjSubWorkOrderIssues,*arrAllObjSubWorkOrderHelper,*arrAllObjSubWorkOrderNotes,*arrAllObjSubWorkOrderActualHrs,*arrAllObjSubWorkOrderIssuesRepair,*arrAllObjSubWorkOrderIssuesRepairParts,*arrAllObjSubWorkOrderIssuesRepairLabour,*arrAllObjSubWorkOrderMechanicalEquipment,*arrAllObjSubWOCompleteTimeExtSerDcs,*arrAllObjSubWoEmployeeWorkingTimeExtSerDcs,*arrAllObjWorkOrderAppliedDiscountExtSerDcs;

    NSEntityDescription *entityMechanicalSubWorkOrder;
    NSFetchRequest *requestMechanicalSubWorkOrder;
    NSSortDescriptor *sortDescriptorMechanicalSubWorkOrder;
    NSArray *sortDescriptorsMechanicalSubWorkOrder;
    NSManagedObject *matchesMechanicalSubWorkOrder;
    NSArray *arrAllObjMechanicalSubWorkOrder;

}
@property (strong, nonatomic) IBOutlet UIButton *btnSurvey;
@property (weak, nonatomic) IBOutlet UILabel *lblFromValue;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailId;
- (IBAction)actionOnAdd:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblEmailData;
- (IBAction)actionOnSendEmail:(id)sender;
- (IBAction)actionOnContinueToSetup:(id)sender;
@property (weak, nonatomic) NSString *isCustomerPresent;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceAutomation;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesDynamic;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceDynamic;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;
- (IBAction)action_Back:(id)sender;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssues;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderHelper;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderMechanicalEquipment;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderNotes;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderActualHrs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepair;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairParts;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairLabour;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalSubWorkOrder;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWOCompleteTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs;
@property (strong, nonatomic) NSString *strSubWorkOrderId;
@property (strong, nonatomic) NSString *strWorkOrderId;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderdetails;
@property (strong, nonatomic) NSManagedObject *objWorkOrderdetails;
@property (strong, nonatomic) IBOutlet UIButton *btnSendMail;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerAccountDiscount;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs;

@property (strong, nonatomic) NSString *strFromWhere;


@end
