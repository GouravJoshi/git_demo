//
//  MechanicalGeneralInfoViewController.m
//  DPS
//  peSTream
//  Created by Saavan Patidar on 25/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream

#import "MechanicalGeneralInfoViewController.h"
#import "AllImportsViewController.h"
#import "DPS-Swift.h"


@interface MechanicalGeneralInfoViewController ()
{
    Global *global;
    NSString *strWorkOrderStatuss,*strWorkOrderId,*strEmailIdGlobal
    ,*strEmailIdGlobalSecondary,*strEmpID,*strUserName,*strCompanyKey,*strEmpName,*strGlobalSubWorkOrderToSend,*strWoType,*strServiceUrlMainServiceAutomation,*strSubWorkOrderStatuss,*strFromEmail,*strSubWoAcNo,*strEmpNoLoggedIn,*strGlobalServiceJobDescriptionId,*strGlobalServiceJobDescriptionsText,*strWorkOrderIdOnMyWaySentSMS,*strEmployeeEProfileUrl,*strGGQCompanyKey,*strIsGGQIntegration,*strLoggedInUserName,*strWorkOrderNoGlobal;
    NSMutableArray *arrOfBeforeImageAll,*arrOfImagenameCollewctionView,*arrOfImageCaption,*arrOfImageDescription,*arrOfPrimaryEmailNew,*arrOfSecondaryEmailNew,*arrOfPrimaryEmailAlreadySaved,*arrOfSecondaryEmailAlreadySaved,*arrOfTags,*arrDataTblView,*arrOfSubWorkOrder,*arrPickerServiceJobDescriptions;
    BOOL isFromBeforeImage ,yesEditedSomething, isClientApproval, isEditCustomerInfo, isCompletedStatusMechanical;
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt,*viewBackGround;
    UITableView *tblData;
    NSManagedObject *objSubWorkOrderToSend;
    NSString *strServiceAddImage,*strTypeExpandCollapse,*strServicePOCId,*strBillingPOCId;
    int indexSelectedSubWorkOrder;
    BOOL isOnRouteForcefully,isClickedOnViewSubWorkOrder,isOnlyChangeStatus;
    NSString *strNotificationTypeName,*strIndexOnClickForceFullyCloseOtherWos;
}

@property (weak, nonatomic) IBOutlet RKTagsView *tagsView;

@end

@implementation MechanicalGeneralInfoViewController

- (void)viewDidLoad {
    
    isOnRouteForcefully = NO;
    strSubWoAcNo = @"";
    isClickedOnViewSubWorkOrder = NO;
    isOnlyChangeStatus = NO;
    
    // Do any additional setup after loading the view.  @"edit_iPad.png"
    [_btnBillingArrow setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    [_btnServiceArrow setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    [_btnExpandCustomerInfo setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    [_btnExpandWorkOrderInfo setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    [_btnExpandSubWorkOrderInf setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    [_btnExpandServiceAddress setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    [_btnExpanBillingAddress setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
    [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"edit_iPad.png"] forState:UIControlStateNormal];
    
    _constant_CustomerInfo_H.constant=70;
    _constant_WorkOrderInfo_H.constant=70;
    _constant_tblViewSubWorkorder_H.constant=0;
    _constant_ServiceAddres_H.constant=0;
    _constant_BillingAddress_H.constant=0;
    isEditCustomerInfo=NO;
    [self setValuesOnEdit:@"EditNot"];
    
    //Saaavan
    indexSelectedSubWorkOrder=0;
    
    [super viewDidLoad];
    _btnUploadServiceAddImage.tag=123456;
    
    global = [[Global alloc] init];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strWorkOrderId=[defsLead valueForKey:@"LeadId"];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpName        =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strEmpNoLoggedIn =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strLoggedInUserName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"LoggedInUserName"]];
    
    strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    strEmployeeEProfileUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEprofile"]];
    strGGQCompanyKey=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.GGQCompanyKey"]];
    strIsGGQIntegration=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsGGQIntegration"]];
    
    NSString *strServiceReportType=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportType"]];
    
    if ([strServiceReportType isEqualToString:@"CompanyEmail"]) {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceReportEmail"]];
        
    } else {
        
        strFromEmail=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
        
    }
    
    [self methodAllocation];
    
    //[self addViewOnLoading];
    [self fetchSubWorkOrderFromDB];
    
    strTypeExpandCollapse=@"add";
    
    [self performSelector:@selector(addViewOnLandingNew:) withObject:nil afterDelay:0.2];
    
    //[self addViewOnLandingNew:@"add"];
    
    [self methodSettingTagsView];
    
    [self methodBorderColor];
    
    [self fetchWorkOrderFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderFromDB];
    
    [self fetchImageDetailFromDataBaseMechanical];
    
    [self addMultipleTags];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    _lblAccountNamenNo.text=[NSString stringWithFormat:@"%@ %@",[defs valueForKey:@"lblName"],strSubWoAcNo];
    [defs setValue:_lblAccountNamenNo.text forKey:@"lblNameNew"];
    [defs synchronize];
    
    [self setValuesOnEdit:@"EditNot"];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    
    _tblViewSUbWorkOrder.rowHeight=UITableViewAutomaticDimension;
    _tblViewSUbWorkOrder.estimatedRowHeight=200;
    _tblViewSUbWorkOrder.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    BOOL isForAddSubWorkOrder=[defsBack boolForKey:@"AddSubWorkOrder"];
    
    if (isForAddSubWorkOrder) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"AddSubWorkOrder"];
        [defs synchronize];
        
        [self viewDidLoad];
        
    }else if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
        EditImageViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewControlleriPad"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
        //        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        //        DrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
        //        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isFromBackServiceDynamci"];
        
        if (isFromBack) {
            
            arrOfBeforeImageAll=nil;
            arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            [defsBack setBool:NO forKey:@"isFromBackServiceDynamci"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseMechanical];
            
        }
        
        
        //    _lblOne.clipsToBounds = YES;
        //    _lblOne.layer.masksToBounds = YES;
        //    _lblOne.layer.cornerRadius = 40.0;
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreview"];
        if (yesFromDeleteImage) {
            
            yesEditedSomething=YES;
            
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreview"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfBeforeImageAll.count; k++) {
                
                NSDictionary *dictdat=arrOfBeforeImageAll[k];
                
                NSString *strImageName;
                
                if ([dictdat isKindOfClass:[NSString class]]) {
                    
                    strImageName=arrOfBeforeImageAll[k];
                    
                } else {
                    
                    strImageName=[dictdat valueForKey:@"woImagePath"];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImages"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                        
                    }
                }
                //            if (yesFoundName) {
                //
                //                [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                //
                //            }
            }
            if (!(arrTempBeforeImage.count==0)) {
                // arrOfBeforeImageAll=nil;
                // arrOfBeforeImageAll=[[NSMutableArray alloc]init];
                [arrOfBeforeImageAll removeObjectsInArray:arrTempBeforeImage];
            }
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImages"];
            [defsnew synchronize];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            yesEditedSomething=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            yesEditedSomething=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            
        }
        
        [self downloadingImagesThumbNailCheck];
        
    }
    
    [_beforeImageCollectionView reloadData];
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strEmpNoLoggedIn =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    [self fetchServiceJobDescriptionsFromMaster];
    
    if (isCompletedStatusMechanical) {
        
        _btn_ActionCurrentStatus.enabled=NO;
        
    }else{
        
        _btn_ActionCurrentStatus.enabled=YES;
        
    }
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
//============================================================================
#pragma mark- ----------------Void Methods----------------
//============================================================================
//============================================================================

-(void)goToServiceHistory{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ServiceHistoryMechanical
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistoryMechanical"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(void)goToDocumentView{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    MechanicalDocumentsViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalDocumentsViewController"];
    objByProductVC.strAccountNo=[matchesWorkOrder valueForKey:@"accountNo"];
    objByProductVC.strWorkOrderId = strWorkOrderId;
    objByProductVC.strWorkOrderNo = [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"workOrderNo"]];
    objByProductVC.strLeadNo = strWorkOrderId;
    
    if (isCompletedStatusMechanical) {
        
        objByProductVC.strWorkStatus=@"Complete";
        
    } else {
        
        objByProductVC.strWorkStatus=@"InComplete";
        
    }
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    
}

-(void)goToSubWorkOrderDetailView{
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    [defss setInteger:-1 forKey:@"sectionToOpen"];
    [defss synchronize];
    
    if ([strWoType isEqualToString:@"TM"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController"];
        objByProductVC.strSubWorkOrderId=strGlobalSubWorkOrderToSend;
        objByProductVC.objWorkOrderdetails=matchesWorkOrder;
        objByProductVC.objSubWorkOrderdetails=objSubWorkOrderToSend;
        objByProductVC.strWoType=strWoType;
        
        NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
        [defsId setValue:strGlobalSubWorkOrderToSend forKey:@"SubWorkOrderId"];
        [defsId synchronize];
        
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalSubWorkOrderDetailsViewControlleriPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControlleriPad"];
        objByProductVC.strSubWorkOrderId=strGlobalSubWorkOrderToSend;
        objByProductVC.objWorkOrderdetails=matchesWorkOrder;
        objByProductVC.objSubWorkOrderdetails=objSubWorkOrderToSend;
        objByProductVC.strWoType=strWoType;
        
        NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
        [defsId setValue:strGlobalSubWorkOrderToSend forKey:@"SubWorkOrderId"];
        [defsId synchronize];
        
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    
}

-(void)goToStartRepairView{
    
    if ([strWoType isEqualToString:@"TM"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalStartRepairTM
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairTM"];
        objByProductVC.strSubWorkOrderId=strGlobalSubWorkOrderToSend;
        objByProductVC.objWorkOrderdetails=matchesWorkOrder;
        objByProductVC.objSubWorkOrderdetails=objSubWorkOrderToSend;
        
        NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
        [defsId setValue:strGlobalSubWorkOrderToSend forKey:@"SubWorkOrderId"];
        [defsId synchronize];
        
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalStartRepairViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairViewController"];
        objByProductVC.strSubWorkOrderId=strGlobalSubWorkOrderToSend;
        objByProductVC.objWorkOrderdetails=matchesWorkOrder;
        objByProductVC.objSubWorkOrderdetails=objSubWorkOrderToSend;
        
        NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
        [defsId setValue:strGlobalSubWorkOrderToSend forKey:@"SubWorkOrderId"];
        [defsId synchronize];
        
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    
}

-(void)methodAllocation{
    yesEditedSomething=NO;
    arrOfBeforeImageAll=[[NSMutableArray alloc]init];
    arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    arrOfPrimaryEmailNew=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailNew=[[NSMutableArray alloc]init];
    arrOfPrimaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrOfSecondaryEmailAlreadySaved=[[NSMutableArray alloc]init];
    arrOfTags=[[NSMutableArray alloc]init];
    arrOfSubWorkOrder=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    //============================================================================
    //============================================================================
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
}
-(void)methodSettingTagsView{
    
    self.tagsView.textField.placeholder = @"Add tag...";
    self.tagsView.textField.returnKeyType = UIReturnKeyDone;
    self.tagsView.textField.delegate = self;
    self.tagsView.textField.font=[UIFont systemFontOfSize:22];
    
    self.tagsView.editable = true;
    self.tagsView.selectable = true;
    self.tagsView.allowsMultipleSelection = true;
    self.tagsView.scrollsHorizontally = true;
    [self.view layoutIfNeeded];
    
}

-(void)methodBorderColor{
    
    _txtViewTechComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewTechComment.layer.borderWidth=1.0;
    _txtViewTechComment.layer.cornerRadius=5.0;
    
    _textView_ServiceJobDescriptions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _textView_ServiceJobDescriptions.layer.borderWidth=1.0;
    _textView_ServiceJobDescriptions.layer.cornerRadius=5.0;
    
    _txtViewNotes.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewNotes.layer.borderWidth=1.0;
    _txtViewNotes.layer.cornerRadius=5.0;
    
    _tagsView.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _tagsView.layer.borderWidth=1.0;
    _tagsView.layer.cornerRadius=5.0;
    
    [_btnPrimaryEmail.layer setCornerRadius:5.0f];
    [_btnPrimaryEmail.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnPrimaryEmail.layer setBorderWidth:0.8f];
    [_btnPrimaryEmail.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnPrimaryEmail.layer setShadowOpacity:0.3];
    [_btnPrimaryEmail.layer setShadowRadius:3.0];
    [_btnPrimaryEmail.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnSecondaryEmail.layer setCornerRadius:5.0f];
    [_btnSecondaryEmail.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSecondaryEmail.layer setBorderWidth:0.8f];
    [_btnSecondaryEmail.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSecondaryEmail.layer setShadowOpacity:0.3];
    [_btnSecondaryEmail.layer setShadowRadius:3.0];
    [_btnSecondaryEmail.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btn_ServiceAddressPOCDetailDcs.layer setCornerRadius:5.0f];
    [_btn_ServiceAddressPOCDetailDcs.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_ServiceAddressPOCDetailDcs.layer setBorderWidth:0.8f];
    [_btn_ServiceAddressPOCDetailDcs.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_ServiceAddressPOCDetailDcs.layer setShadowOpacity:0.3];
    [_btn_ServiceAddressPOCDetailDcs.layer setShadowRadius:3.0];
    [_btn_ServiceAddressPOCDetailDcs.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btn_BillingAddressPOCDetailDcs.layer setCornerRadius:5.0f];
    [_btn_BillingAddressPOCDetailDcs.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_BillingAddressPOCDetailDcs.layer setBorderWidth:0.8f];
    [_btn_BillingAddressPOCDetailDcs.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_BillingAddressPOCDetailDcs.layer setShadowOpacity:0.3];
    [_btn_BillingAddressPOCDetailDcs.layer setShadowRadius:3.0];
    [_btn_BillingAddressPOCDetailDcs.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}
-(void)goingToPreview :(NSString*)indexxx
{
    _btnUploadServiceAddImage.tag=123456;
    
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ImagePreviewGeneralInfoAppointmentViewiPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.statusOfWorkOrder=strWorkOrderStatuss;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaption;
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescription;
        //objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}

-(void)methodBack{
        
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentVC class]]) {
                index=k1;
                //break;
            }
        }
        AppointmentVC *myController = (AppointmentVC *)[self.navigationController.viewControllers objectAtIndex:index];
        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
        [self.navigationController popToViewController:myController animated:NO];
    }
    else
    {
        int index = 0;
        NSArray *arrstack=self.navigationController.viewControllers;
        for (int k1=0; k1<arrstack.count; k1++) {
            if ([[arrstack objectAtIndex:k1] isKindOfClass:[AppointmentViewiPad class]]) {
                index=k1;
                //break;
            }
        }
        AppointmentViewiPad *myController = (AppointmentViewiPad *)[self.navigationController.viewControllers objectAtIndex:index];
        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
        [self.navigationController popToViewController:myController animated:NO];
    }

    
}

-(void)addMultipleTags{
    
    NSString *text=[arrOfTags componentsJoinedByString:@","];
    
    [self.tagsView removeAllTags];
    
    for (NSString *word in [text componentsSeparatedByString:@","]) {
        if (word.length > 0) {
            [self.tagsView addTag:word];
        }
    }
    
}

-(void)addViewOnLoading{
    
    //Adding CustomerInfoView
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,_constant_CustomerInfo_H.constant);
    [_view_CustomerInfo setFrame:frameFor_view_CustomerInfo];
    [_scrollViewGeneralInfo addSubview:_view_CustomerInfo];
    
    //Adding WorkOrderInfoView
    
    CGRect frameFor_view_WorkOrderInfo=CGRectMake(0, _view_CustomerInfo.frame.origin.y+_view_CustomerInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_WorkOrderInfo.frame.size.height);
    [_view_WorkOrderInfo setFrame:frameFor_view_WorkOrderInfo];
    [_scrollViewGeneralInfo addSubview:_view_WorkOrderInfo];
    
    
    //Adding Sub-WorkOrderInfoView
    
    CGRect frameFor_view_SubWorkOrderInfo=CGRectMake(0, _view_WorkOrderInfo.frame.origin.y+_view_WorkOrderInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkOrder.count*572+70);
    [_view_SubWorkOredr setFrame:frameFor_view_SubWorkOrderInfo];
    [_scrollViewGeneralInfo addSubview:_view_SubWorkOredr];
    
    _tblViewSUbWorkOrder.frame=CGRectMake(_tblViewSUbWorkOrder.frame.origin.x, _tblViewSUbWorkOrder.frame.origin.y, _tblViewSUbWorkOrder.frame.size.width, arrOfSubWorkOrder.count*572+70);
    
    //Adding AddressInfoView
    
    
    //    _constant_BillingHeight.constant=0;
    //    _constant_serviceHeight.constant=0;
    
    CGRect frameFor_view_AddressInfo=CGRectMake(0, _view_SubWorkOredr.frame.origin.y+_view_SubWorkOredr.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_AddressInfo.frame.size.height);
    [_view_AddressInfo setFrame:frameFor_view_AddressInfo];
    [_scrollViewGeneralInfo addSubview:_view_AddressInfo];
    
    
    //Adding OtherInfoView
    
    CGRect frameFor_view_OtherInfo=CGRectMake(0, _view_AddressInfo.frame.origin.y+_view_AddressInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_OtherInfo.frame.size.height);
    [_view_OtherInfo setFrame:frameFor_view_OtherInfo];
    [_scrollViewGeneralInfo addSubview:_view_OtherInfo];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
    
    CGFloat heightServiceJobDesc=0.0;
    
    if (isNoServiceJobDescriptions) {
        
        heightServiceJobDesc=0.0;
        [_viewServiceJobDescriptions setHidden:YES];
        
    }else{
        
        heightServiceJobDesc=_viewServiceJobDescriptions.frame.size.height;
        [_viewServiceJobDescriptions setHidden:NO];
    }
    
    //Adding Service Job Description
    
    CGRect frameFor_view_JobDescription=CGRectMake(0, _view_OtherInfo.frame.origin.y+_view_OtherInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,heightServiceJobDesc);
    [_viewServiceJobDescriptions setFrame:frameFor_view_JobDescription];
    [_scrollViewGeneralInfo addSubview:_viewServiceJobDescriptions];
    
    //Adding BeforeImageInfoView
    
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _view_OtherInfo.frame.origin.y+_view_OtherInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_BeforeImage.frame.size.height);
    [_view_BeforeImage setFrame:frameFor_view_BeforeImageInfo];
    
    CGRect frameFor_view_SaveContinueInfo=CGRectMake(0, _view_BeforeImage.frame.origin.y+_view_BeforeImage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_SavenContinue.frame.size.height);
    [_view_SavenContinue setFrame:frameFor_view_SaveContinueInfo];
    
    [_scrollViewGeneralInfo setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_viewServiceJobDescriptions.frame.size.height+_viewServiceJobDescriptions.frame.origin.y)];
    
}

-(void)tablViewForPrimaryEmailIds{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    [arrDataTblView addObjectsFromArray:arrOfPrimaryEmailNew];
    
    if (arrDataTblView.count==0) {
        //[global AlertMethod:Info :NoDataAvailableReset];
        //[self alertForPrimaryEmailSaving];
        
        tblData.tag=105;
        [self tableLoad:tblData.tag];
        
    }else{
        tblData.tag=105;
        [self tableLoad:tblData.tag];
    }
    
}
-(void)tablViewForSecondaryEmailIds{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    [arrDataTblView addObjectsFromArray:arrOfSecondaryEmailNew];
    
    if (arrDataTblView.count==0) {
        //[global AlertMethod:Info :NoDataAvailableReset];
        //[self alertForSecondaryEmailSaving];
        tblData.tag=106;
        [self tableLoad:tblData.tag];
        
    }else{
        tblData.tag=106;
        [self tableLoad:tblData.tag];
    }
    
}

-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 104:
        {
            [self setTableFrame];
            break;
        }
        case 105:
        {
            [self setTableFrameForEmailIds :@"primary"];
            break;
        }
        case 106:
        {
            [self setTableFrameForEmailIds :@"secondary"];
            break;
        }
        case 107:
        {
            [self setTableFrame];
            break;
        }
        case 108:
        {
            [self setTableFrame];
            break;
        }
        case 109:
        {
            [self setTableFrame];
            break;
        }
        default:
            break;
    }
    //[tblData setContentOffset:CGPointZero animated:YES];
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}

-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [_scrollViewGeneralInfo addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}

-(void)setTableFrameForEmailIds :(NSString*)strType
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [_scrollViewGeneralInfo addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 500);
    
    UILabel *lblMsg=[[UILabel alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y-40, tblData.bounds.size.width, 30)];
    lblMsg.text=@"Swipe to delete & tap to edit";
    lblMsg.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    // [viewBackGround addSubview:lblMsg];
    
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 60)];
    
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont systemFontOfSize:25];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(methodOnDoneEditingEmailIds) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGround addSubview:btnDone];
    
    
    UIButton *btnAddNew=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.size.width/2+2+tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 60)];
    
    [btnAddNew setTitle:@"Add New" forState:UIControlStateNormal];
    btnAddNew.titleLabel.font=[UIFont systemFontOfSize:25];
    [btnAddNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnAddNew.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    if ([strType isEqualToString:@"primary"]) {
        
        [btnAddNew addTarget:self action:@selector(alertForPrimaryEmailSaving) forControlEvents:UIControlEventTouchDown];
        
    } else {
        
        [btnAddNew addTarget:self action:@selector(alertForSecondaryEmailSaving) forControlEvents:UIControlEventTouchDown];
        
    }
    
    [viewBackGround addSubview:btnAddNew];
    
}

-(void)methodOnDoneEditingEmailIds{
    
    //    UIButton *tempButton=[[UIButton alloc]init];
    //    tempButton.frame=_btnPrimaryEmail.frame;
    //
    //    _btnPrimaryEmail=nil;
    //    _btnPrimaryEmail=[[UIButton alloc]init];
    //    _btnPrimaryEmail.frame=tempButton.frame;
    
    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
    
    if (strMessage.length==0) {
        
        strMessage=@"";
        
    }
    
    [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
    
    NSString *strMessageSec=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
    
    if (strMessageSec.length==0) {
        
        strMessageSec=@"";
        
    }
    
    [_btnSecondaryEmail setTitle:strMessageSec forState:UIControlStateNormal];
    _btnPrimaryEmail.titleLabel.text=strMessage;
    _btnSecondaryEmail.titleLabel.text=strMessageSec;
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [tblData removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [txtFieldCaption resignFirstResponder];
    [txtViewImageDescription resignFirstResponder];
    
}

-(void)alertForPrimaryEmailSaving{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Add Email-Id"
                                                                              message: strMessage
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Email-Id Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    //    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    //        textField.placeholder = @"password";
    //        textField.textColor = [UIColor blueColor];
    //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    //        textField.borderStyle = UITextBorderStyleRoundedRect;
    //        textField.secureTextEntry = YES;
    //    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        if (txtHistoricalDays.text.length>0) {
            
            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
            {
                
                [self performSelector:@selector(AlertViewForNonValidPrimaryEmail) withObject:nil afterDelay:0.2];
                
            }else if ([txtHistoricalDays.text containsString:@" "])
            {
                
                [self performSelector:@selector(AlertViewForNonValidPrimaryEmail) withObject:nil afterDelay:0.2];
                
            }else{
                
                if ([arrOfPrimaryEmailNew containsObject:txtHistoricalDays.text]) {
                    
                    [self performSelector:@selector(AlertViewForAlreadyExitPrimaryEmail) withObject:nil afterDelay:0.2];
                    
                } else {
                    
                    // [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
                    
                    [arrOfPrimaryEmailNew addObject:txtHistoricalDays.text];
                    
                    [arrOfPrimaryEmailAlreadySaved addObject:txtHistoricalDays.text];
                    
                    NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
                    
                    [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
                    
                    [self tablViewForPrimaryEmailIds];
                    
                }
            }
            
        } else {
            
            [self performSelector:@selector(AlertViewForEmptyPrimaryEmail) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self tablViewForPrimaryEmailIds];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)AlertViewForEmptyPrimaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter to add Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForPrimaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForAlreadyExitPrimaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Email-Id Already Present. Please provide another one"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForPrimaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForEmptyEmailEdit{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter to edit Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)AlertViewForNonValidPrimaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter valid Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForPrimaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForNonValidEmailOnEdit{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter valid Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForAlreadyEmailPresentOnEdit{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Email-Id Already Present. Please provide another one"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)AlertViewForEmptySecondaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter to add Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForSecondaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForNonValidSecondaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter valid Email-Id"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForSecondaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)AlertViewForAlreadyPresentSecondaryEmail{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Emai-Id Already Saved. Please provide another one"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self alertForSecondaryEmailSaving];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)alertForSecondaryEmailSaving{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Add Email-Id"
                                                                              message: strMessage
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Email-Id Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    //    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    //        textField.placeholder = @"password";
    //        textField.textColor = [UIColor blueColor];
    //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    //        textField.borderStyle = UITextBorderStyleRoundedRect;
    //        textField.secureTextEntry = YES;
    //    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        if (txtHistoricalDays.text.length>0) {
            
            txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
            {
                
                [self performSelector:@selector(AlertViewForNonValidSecondaryEmail) withObject:nil afterDelay:0.2];
                
            }else if ([txtHistoricalDays.text containsString:@" "])
            {
                
                [self performSelector:@selector(AlertViewForNonValidSecondaryEmail) withObject:nil afterDelay:0.2];
                
            }else{
                
                if ([arrOfSecondaryEmailNew containsObject:txtHistoricalDays.text]) {
                    
                    [self performSelector:@selector(AlertViewForAlreadyPresentSecondaryEmail) withObject:nil afterDelay:0.2];
                    
                    
                } else {
                    
                    [arrOfSecondaryEmailNew addObject:txtHistoricalDays.text];
                    
                    [arrOfSecondaryEmailAlreadySaved addObject:txtHistoricalDays.text];
                    
                    NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
                    
                    [_btnSecondaryEmail setTitle:strMessage forState:UIControlStateNormal];
                    
                    [self tablViewForSecondaryEmailIds];
                    
                }
            }
        } else {
            
            [self performSelector:@selector(AlertViewForEmptySecondaryEmail) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self tablViewForSecondaryEmailIds];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrOfImagenameCollewctionView.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"CellCollectionService";
    
    GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    //    cell.selected=YES;
    //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
    NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    if (image==nil) {
        
        cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
        
    } else {
        
        cell.imageBefore.image = image;
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    if (image==nil) {
        
        
        
    }else{
        
        NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        [self goingToPreview : strIndex];
        
    }
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1) {
        
        return arrOfSubWorkOrder.count;
        
    } else if (tableView.tag==107) {
        
        return arrPickerServiceJobDescriptions.count;
        
    } else if (tableView.tag==108) {
        
        return arrAllObjMechanicalServiceAddressPOCDetailDcs.count;
        
    }else if (tableView.tag==109) {
        
        return arrAllObjMechanicalBillingAddressPOCDetailDcs.count;
        
    }else {
        
        return arrDataTblView.count;
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==1) {
        
        return tableView.rowHeight;
        
        
    } else {
        
        return 80;
        
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==1) {
        
        MechanicalGeneralSubWorkOrderTableViewCell *cell = (MechanicalGeneralSubWorkOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MechanicalGeneralSubWorkOrderTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
        
    }else if (tableView.tag==107) {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrPickerServiceJobDescriptions.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 107:
                {
                    NSDictionary *dictData=[arrPickerServiceJobDescriptions objectAtIndex:indexPath.row];
                    
                    NSString *title = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Title"]];
                    
                    cell.textLabel.text=title;
                    
                    if ([strGlobalServiceJobDescriptionId isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]]]) {
                        
                        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                        
                    }else{
                        
                        [cell setAccessoryType:UITableViewCellAccessoryNone];
                        
                    }
                    break;
                }
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
        
    }else if (tableView.tag==108) {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrAllObjMechanicalServiceAddressPOCDetailDcs.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 108:
                {
                    NSDictionary *dictData=[arrAllObjMechanicalServiceAddressPOCDetailDcs objectAtIndex:indexPath.row];
                    
                    NSMutableArray *arrName=[[NSMutableArray alloc]init];
                    
                    NSString *FirstName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
                    
                    if (FirstName.length>0) {
                        
                        [arrName addObject:FirstName];
                        
                    }
                    
                    NSString *MiddleName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
                    
                    if (MiddleName.length>0) {
                        
                        [arrName addObject:MiddleName];
                        
                    }
                    
                    NSString *LastName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
                    
                    if (LastName.length>0) {
                        
                        [arrName addObject:LastName];
                        
                    }
                    
                    cell.textLabel.text=[arrName componentsJoinedByString:@" "];
                    
                    if ([strServicePOCId isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]]]) {
                        
                        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                        
                    }else{
                        
                        [cell setAccessoryType:UITableViewCellAccessoryNone];
                        
                    }
                    break;
                }
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
        
    }else if (tableView.tag==109) {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrAllObjMechanicalBillingAddressPOCDetailDcs.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 109:
                {
                    NSDictionary *dictData=[arrAllObjMechanicalBillingAddressPOCDetailDcs objectAtIndex:indexPath.row];
                    
                    NSMutableArray *arrName=[[NSMutableArray alloc]init];
                    
                    NSString *FirstName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
                    
                    if (FirstName.length>0) {
                        
                        [arrName addObject:FirstName];
                        
                    }
                    
                    NSString *MiddleName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
                    
                    if (MiddleName.length>0) {
                        
                        [arrName addObject:MiddleName];
                        
                    }
                    
                    NSString *LastName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
                    
                    if (LastName.length>0) {
                        
                        [arrName addObject:LastName];
                        
                    }
                    
                    cell.textLabel.text=[arrName componentsJoinedByString:@" "];
                    
                    if ([strBillingPOCId isEqualToString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]]]) {
                        
                        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                        
                    }else{
                        
                        [cell setAccessoryType:UITableViewCellAccessoryNone];
                        
                    }
                    break;
                }
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
        
    }else{
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrDataTblView.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 104:
                {
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"ResetReason"];
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    break;
                }
                case 105:
                {
                    cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    break;
                }
                case 106:
                {
                    cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    break;
                }
                    
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
    }
}

- (void)configureCell:(MechanicalGeneralSubWorkOrderTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *objSubWorkOrderCell=arrOfSubWorkOrder[indexPath.row];
    // objSubWorkOrderToSend=objSubWorkOrderCell;
    cell.lblSubWorkOrderNo.text=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWorkOrderNo"]];
    //strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWorkOrderId"]];
    cell.lblServiceDatenTime.text=[global ChangeDateFormatOnGeneralInf :[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"serviceDateTime"]]];
    cell.lblJobDescriptions.text=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"jobDesc"]];
    cell.lblAssignedTo.text=[global getEmployeeNameViaEMPId:[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"employeeNo"]]];
    //cell.lblEstTime.text=[global ChangeDateFormatOnGeneralInfoTime :[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"totalEstimationTime"]]];
    
    int totalEstTimeMinutes = [[objSubWorkOrderCell valueForKey:@"totalEstimationTimeInt"] intValue];
    
    totalEstTimeMinutes = totalEstTimeMinutes*60;
    
    int seconds = totalEstTimeMinutes % 60;
    int minutes = (totalEstTimeMinutes / 60) % 60;
    int hours = totalEstTimeMinutes / 3600;
    
    cell.lblEstTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    
    
    cell.lblActualTime.text=[global ChangeDateFormatOnGeneralInfoTime :[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"actualTime"]]];
    cell.lblSubWorkOrderType.text=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWOType"]];
    
    if ([[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWOType"]] isEqualToString:@"FR"]) {
        
        cell.lblSubWorkOrderType.text=[NSString stringWithFormat:@"%@",@"Flat Rate"];
        
    } else {
        
        cell.lblSubWorkOrderType.text=[NSString stringWithFormat:@"%@",@"Time and Material"];
        
    }
    
    cell.lblStatus.text=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWOStatus"]];
    
    if ([cell.lblStatus.text isEqualToString:@"Inspection"]) {
        
        cell.lblStatus.text=[NSString stringWithFormat:@"%@",@"In Progress"];
        
    }
    
    cell.action_DeleteSubWorkOrder.tag=indexPath.row;
    cell.action_ViewSubWorkOrder.tag=indexPath.row;
    cell.btnStatus.tag=indexPath.row;
    cell.btnSelectedSubWorkOrder.tag=indexPath.row;
    cell.action_ViewMoreJobDescription.tag=indexPath.row;
    cell.btnCopySubWorkOrder.tag=indexPath.row;
    cell.btnSubWoEmployeeWorkingTime.tag=indexPath.row;
    
    NSString *strWorkOrderStatusTemp=[objSubWorkOrderCell valueForKey:@"subWOStatus"];
    
    BOOL isCompleted = [global isCompletedSatusMechanical:strWorkOrderStatusTemp];
    
    if (isCompleted) {
        
        [cell.btnSubWoEmployeeWorkingTime setHidden:NO];
        
    } else {
        
        [cell.btnSubWoEmployeeWorkingTime setHidden:YES];
        
    }


    [cell.action_DeleteSubWorkOrder addTarget:self action:@selector(action_DeleteSubWorkOrder:) forControlEvents:UIControlEventTouchDown];
    [cell.action_ViewSubWorkOrder addTarget:self action:@selector(action_ViewSubWorkOrder:) forControlEvents:UIControlEventTouchDown];
    [cell.btnStatus addTarget:self action:@selector(action_StatusSubWorkOrder:) forControlEvents:UIControlEventTouchDown];
    [cell.btnSelectedSubWorkOrder addTarget:self action:@selector(action_SelectSubWorkOrder:) forControlEvents:UIControlEventTouchDown];
    [cell.action_ViewMoreJobDescription addTarget:self action:@selector(action_ViewMoreJobDescription:) forControlEvents:UIControlEventTouchDown];
    [cell.btnCopySubWorkOrder addTarget:self action:@selector(action_CopySubWorkOrder:) forControlEvents:UIControlEventTouchDown];
    [cell.btnSubWoEmployeeWorkingTime addTarget:self action:@selector(action_SubWoEmployeeWorkingTime:) forControlEvents:UIControlEventTouchDown];

    if ([NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"jobDesc"]].length>44) {
        
        cell.action_ViewMoreJobDescription.hidden=NO;
        
    } else {
        
        cell.action_ViewMoreJobDescription.hidden=YES;
        
    }
    
    if (indexPath.row==indexSelectedSubWorkOrder) {
        
        [cell.btnSelectedSubWorkOrder setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    } else {
        
        [cell.btnSelectedSubWorkOrder setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWOStatus"]];
    
    if ([strStatusSubWorkOrder isEqualToString:@"Not Started"]) {
        
        strStatusSubWorkOrder=@"New";
        
    }
    
    if ([strStatusSubWorkOrder isEqualToString:@"New"]) {
        
        [cell.btnStatus setEnabled:YES];
        [cell.btnStatus setTitle:@"Recieve" forState:UIControlStateNormal];
        cell.lblStatus.text=[NSString stringWithFormat:@"%@",@"New"];
        
    } else if ([strStatusSubWorkOrder isEqualToString:@"Dispatch"]){
        
        [cell.btnStatus setEnabled:YES];
        [cell.btnStatus setTitle:@"On My Way" forState:UIControlStateNormal];
        cell.lblStatus.text=[NSString stringWithFormat:@"%@",@"Received"];
        
    } else if ([strStatusSubWorkOrder isEqualToString:@"OnRoute"]){
        
        [cell.btnStatus setEnabled:YES];
        [cell.btnStatus setTitle:@"Arrive" forState:UIControlStateNormal];
        cell.lblStatus.text=[NSString stringWithFormat:@"%@",@"On Route"];
        
    }else{
        
        [cell.btnStatus setTitle:strStatusSubWorkOrder forState:UIControlStateNormal];
        [cell.btnStatus setEnabled:NO];
        
        if ([strStatusSubWorkOrder isEqualToString:@"Inspection"]) {
            
            [cell.btnStatus setTitle:@"In Progress" forState:UIControlStateNormal];
            
        }
        
    }
    
    
    if (indexPath.row==indexSelectedSubWorkOrder) {
        
        
        _lblCurrentStatus.text=[NSString stringWithFormat:@"Current Status: %@",[objSubWorkOrderCell valueForKey:@"subWOStatus"]];
        
        if ([_lblCurrentStatus.text isEqualToString:@"Current Status: Inspection"]) {
            
            _lblCurrentStatus.text=[NSString stringWithFormat:@"Current Status: %@",@"In Progress"];
            
        }
        
        _btn_ActionCurrentStatus.enabled=YES;
        if ([strStatusSubWorkOrder isEqualToString:@"New"]) {
            
            [_btn_ActionCurrentStatus setTitle:@"Recieve" forState:UIControlStateNormal];
            _lblCurrentStatus.text=[NSString stringWithFormat:@"Current Status: %@",@"New"];
            
        } else if ([strStatusSubWorkOrder isEqualToString:@"Dispatch"]){
            
            [_btn_ActionCurrentStatus setTitle:@"On My Way" forState:UIControlStateNormal];
            _lblCurrentStatus.text=[NSString stringWithFormat:@"Current Status: %@",@"Received"];
            
        } else if ([strStatusSubWorkOrder isEqualToString:@"OnRoute"]){
            
            [_btn_ActionCurrentStatus setTitle:@"Arrive" forState:UIControlStateNormal];
            _lblCurrentStatus.text=[NSString stringWithFormat:@"Current Status: %@",@"On Route"];
            
        }else{
            _btn_ActionCurrentStatus.enabled=NO;
            
            [_btn_ActionCurrentStatus setTitle:strStatusSubWorkOrder forState:UIControlStateNormal];
            
            if ([strStatusSubWorkOrder isEqualToString:@"Inspection"]) {
                
                [_btn_ActionCurrentStatus setTitle:@"In Progress" forState:UIControlStateNormal];
                
            }
            
        }
        
    }
    
    if (isCompletedStatusMechanical) {
        
        [cell.action_DeleteSubWorkOrder setEnabled:NO];
        [cell.btnStatus setEnabled:NO];
        [cell.btnCopySubWorkOrder setEnabled:NO];
        
        
    }else{
        
        [cell.action_DeleteSubWorkOrder setEnabled:YES];
        [cell.btnStatus setEnabled:YES];
        [cell.btnCopySubWorkOrder setEnabled:YES];
        
    }
    
    //    if ([strStatusSubWorkOrder isEqualToString:@"New"] || [strStatusSubWorkOrder isEqualToString:@"Dispatch"] || [strStatusSubWorkOrder isEqualToString:@"OnRoute"]) {
    //
    //        [cell.action_ViewSubWorkOrder setEnabled:NO];
    //
    //    } else {
    //
    //        [cell.action_ViewSubWorkOrder setEnabled:YES];
    //
    //    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    yesEditedSomething=YES;
    
    if (tableView.tag==107) {
        
        NSDictionary *dictData=[arrPickerServiceJobDescriptions objectAtIndex:indexPath.row];
        NSString *strServiceJobDescription = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescription"]];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [strServiceJobDescription dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
         _textView_ServiceJobDescriptions.attributedText = attributedString;
        
        //_textView_ServiceJobDescriptions.text=strServiceJobDescription;
        
        strGlobalServiceJobDescriptionsText = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescription"]];
        strGlobalServiceJobDescriptionId = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]];
        [_btnServiceJobDescriptionTitle setTitle:[dictData valueForKey:@"Title"] forState:UIControlStateNormal];
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    } else if (tableView.tag==108){
        
        NSDictionary *dictData=[arrAllObjMechanicalServiceAddressPOCDetailDcs objectAtIndex:indexPath.row];
        
        NSMutableArray *arrName=[[NSMutableArray alloc]init];
        
        NSString *FirstName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
        
        if (FirstName.length>0) {
            
            [arrName addObject:FirstName];
            
        }
        
        NSString *MiddleName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
        
        if (MiddleName.length>0) {
            
            [arrName addObject:MiddleName];
            
        }
        
        NSString *LastName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
        
        if (LastName.length>0) {
            
            [arrName addObject:LastName];
            
        }
        
        strServicePOCId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        //[_btn_ServiceAddressPOCDetailDcs setTitle:[arrName componentsJoinedByString:@" "] forState:UIControlStateNormal];
        
        _serviceContactName.text=[arrName componentsJoinedByString:@" "];
        
        _serviceContactPrimaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictData valueForKey:@"PrimaryEmail"]]]];
        
        _serviceContactSecondaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictData valueForKey:@"SecondaryEmail"]]]];
        
        _serviceContactPrimaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PrimaryPhone"]]];
        
        _serviceContactSecondaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SecondaryPhone"]]];
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    } else if (tableView.tag==109){
        
        NSDictionary *dictData=[arrAllObjMechanicalBillingAddressPOCDetailDcs objectAtIndex:indexPath.row];
        
        NSMutableArray *arrName=[[NSMutableArray alloc]init];
        
        NSString *FirstName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
        
        if (FirstName.length>0) {
            
            [arrName addObject:FirstName];
            
        }
        
        NSString *MiddleName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
        
        if (MiddleName.length>0) {
            
            [arrName addObject:MiddleName];
            
        }
        
        NSString *LastName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
        
        if (LastName.length>0) {
            
            [arrName addObject:LastName];
            
        }
        
        strBillingPOCId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        //[_btn_BillingAddressPOCDetailDcs setTitle:[arrName componentsJoinedByString:@" "] forState:UIControlStateNormal];
        
        _billingContactPrimaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictData valueForKey:@"PrimaryEmail"]]]];
        
        _billingContactSecondayEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictData valueForKey:@"SecondaryEmail"]]]];
        
        _billingContactPrimaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PrimaryPhone"]]];
        
        _billingContactSecondaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SecondaryPhone"]]];
        
        _billingContactName.text=[arrName componentsJoinedByString:@" "];
        
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }else {
        
        if (!(arrDataTblView.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 105:
                {
                    
                    
                    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Edit Email-Id"
                                                                                              message: @""
                                                                                       preferredStyle:UIAlertControllerStyleAlert];
                    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                        textField.placeholder = @"Enter Email-Id Here...";
                        textField.tag=7;
                        textField.delegate=self;
                        textField.text=arrOfPrimaryEmailNew[indexPath.row];
                        textField.textColor = [UIColor blackColor];
                        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                        textField.borderStyle = UITextBorderStyleRoundedRect;
                        textField.keyboardType=UIKeyboardTypeDefault;
                    }];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        NSArray * textfields = alertController.textFields;
                        UITextField * txtHistoricalDays = textfields[0];
                        if (txtHistoricalDays.text.length>0) {
                            
                            txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
                                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
                            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
                            
                            if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
                            {
                                
                                [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                                
                            }else if ([txtHistoricalDays.text containsString:@" "])
                            {
                                
                                [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                                
                            }else{
                                
                                if ([arrOfPrimaryEmailNew containsObject:txtHistoricalDays.text]) {
                                    
                                    if ([arrOfPrimaryEmailNew[indexPath.row] isEqualToString:txtHistoricalDays.text]) {
                                        
                                        [arrOfPrimaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                        
                                        arrDataTblView=[[NSMutableArray alloc]init];
                                        [arrDataTblView addObjectsFromArray:arrOfPrimaryEmailNew];
                                        [tblData reloadData];
                                        [self tablViewForPrimaryEmailIds];
                                        
                                    } else {
                                        
                                        //AlertViewForAlreadyEmailPresentOnEdit
                                        [self performSelector:@selector(AlertViewForAlreadyEmailPresentOnEdit) withObject:nil afterDelay:0.2];
                                        
                                    }
                                    
                                } else {
                                    
                                    
                                    [arrOfPrimaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                    
                                    arrDataTblView=[[NSMutableArray alloc]init];
                                    [arrDataTblView addObjectsFromArray:arrOfPrimaryEmailNew];
                                    [tblData reloadData];
                                    [self tablViewForPrimaryEmailIds];
                                    
                                }
                            }
                        } else {
                            
                            [self performSelector:@selector(AlertViewForEmptyEmailEdit) withObject:nil afterDelay:0.2];
                            
                        }
                    }]];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        
                        [self tablViewForPrimaryEmailIds];
                        
                    }]];
                    [self presentViewController:alertController animated:YES completion:nil];
                    break;
                }
                case 106:
                {
                    
                    
                    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Edit Email-Id"
                                                                                              message: @""
                                                                                       preferredStyle:UIAlertControllerStyleAlert];
                    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                        textField.placeholder = @"Enter Email-Id Here...";
                        textField.tag=7;
                        textField.delegate=self;
                        textField.text=arrOfSecondaryEmailNew[indexPath.row];
                        textField.textColor = [UIColor blackColor];
                        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                        textField.borderStyle = UITextBorderStyleRoundedRect;
                        textField.keyboardType=UIKeyboardTypeDefault;
                    }];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        NSArray * textfields = alertController.textFields;
                        UITextField * txtHistoricalDays = textfields[0];
                        if (txtHistoricalDays.text.length>0) {
                            
                            txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
                                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
                            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
                            
                            if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
                            {
                                
                                [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                                
                            }else if ([txtHistoricalDays.text containsString:@" "])
                            {
                                
                                [self performSelector:@selector(AlertViewForNonValidEmailOnEdit) withObject:nil afterDelay:0.2];
                                
                            }else{
                                
                                if ([arrOfSecondaryEmailNew containsObject:txtHistoricalDays.text]) {
                                    
                                    if ([arrOfSecondaryEmailNew[indexPath.row] isEqualToString:txtHistoricalDays.text]) {
                                        
                                        [arrOfSecondaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                        
                                        arrDataTblView=[[NSMutableArray alloc]init];
                                        [arrDataTblView addObjectsFromArray:arrOfSecondaryEmailNew];
                                        [tblData reloadData];
                                        [self tablViewForSecondaryEmailIds];
                                        
                                    } else {
                                        
                                        
                                        //AlertViewForAlreadyEmailPresentOnEdit
                                        [self performSelector:@selector(AlertViewForAlreadyEmailPresentOnEdit) withObject:nil afterDelay:0.2];
                                        
                                    }
                                    
                                    
                                } else {
                                    
                                    [arrOfSecondaryEmailNew replaceObjectAtIndex:indexPath.row withObject:txtHistoricalDays.text];
                                    
                                    arrDataTblView=[[NSMutableArray alloc]init];
                                    [arrDataTblView addObjectsFromArray:arrOfSecondaryEmailNew];
                                    [tblData reloadData];
                                    [self tablViewForSecondaryEmailIds];
                                    
                                }
                            }
                            
                        } else {
                            
                            [self performSelector:@selector(AlertViewForEmptyEmailEdit) withObject:nil afterDelay:0.2];
                            
                        }
                    }]];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        
                        [self tablViewForSecondaryEmailIds];
                        
                    }]];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                    break;
                }
                default:
                    break;
            }
            
        }
        
    }
    [tblData removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isCompletedStatusMechanical) {
        
        return false;
        
    }else{
        
        if (tableView.tag==105) {
            
            return true;
            
        }else if (tableView.tag==106){
            
            return true;
            
        }else
            
            return false;
        
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==105) {
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            yesEditedSomething=YES;
            [arrOfPrimaryEmailNew removeObjectAtIndex:indexPath.row];
            arrDataTblView=[[NSMutableArray alloc]init];
            [arrDataTblView addObjectsFromArray:arrOfPrimaryEmailNew];
            [tblData reloadData];
            
            NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
            
            if (strMessage.length==0) {
                
                strMessage=@"";
                
            }
            
            [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
            
            NSString *strMessageSec=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
            
            if (strMessageSec.length==0) {
                
                strMessageSec=@"";
                
            }
            
            [_btnSecondaryEmail setTitle:strMessageSec forState:UIControlStateNormal];
            _btnPrimaryEmail.titleLabel.text=strMessage;
            _btnSecondaryEmail.titleLabel.text=strMessageSec;
            
        }
    }else if (tableView.tag==106){
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            yesEditedSomething=YES;
            [arrOfSecondaryEmailNew removeObjectAtIndex:indexPath.row];
            arrDataTblView=[[NSMutableArray alloc]init];
            [arrDataTblView addObjectsFromArray:arrOfSecondaryEmailNew];
            [tblData reloadData];
            
            NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
            
            if (strMessage.length==0) {
                
                strMessage=@"";
                
            }
            
            [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
            
            NSString *strMessageSec=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
            
            if (strMessageSec.length==0) {
                
                strMessageSec=@"";
                
            }
            
            [_btnSecondaryEmail setTitle:strMessageSec forState:UIControlStateNormal];
            _btnPrimaryEmail.titleLabel.text=strMessage;
            _btnSecondaryEmail.titleLabel.text=strMessageSec;
            
            
        }
    }
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Cell Button Methods-----------------
//============================================================================
//============================================================================

- (IBAction)action_DashBoardView:(id)sender {
    
    //    UIAlertController *alert= [UIAlertController
    //                               alertControllerWithTitle:@"Alert!"
    //                               message:@"Are you sure to move to DashBoard"
    //                               preferredStyle:UIAlertControllerStyleAlert];
    //
    //    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
    //                                                handler:^(UIAlertAction * action)
    //                          {
    //
    //                          }];
    //    [alert addAction:yes];
    //    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
    //                                               handler:^(UIAlertAction * action)
    //                         {
    //
    //                             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
    //                                                                                      bundle: nil];
    //                             DashBoardViewiPad
    //                             *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardViewiPad"];
    //                             [self.navigationController pushViewController:objByProductVC animated:NO];
    //                         }];
    //    [alert addAction:no];
    //    [self presentViewController:alert animated:YES completion:nil];
    
    [self goToEquipMentHistory];
    
}

-(void)action_DeleteSubWorkOrder:(id)sender
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:Alert
                               message:@"Are you sure to delete Sub-Work Order"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Yes-Delete" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              yesEditedSomething=YES;
                              
                              UIButton *btn = (UIButton *)sender;
                              NSManagedObject *dictDataSubWorkOrder=arrOfSubWorkOrder[btn.tag];
                              
                              [self upDateSubWorkOrderFromDB:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]]];
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)action_StatusSubWorkOrder:(id)sender
{
    
    isClickedOnViewSubWorkOrder = NO;
    isOnlyChangeStatus = YES;

    
    UIButton *btn = (UIButton *)sender;
    NSManagedObject *dictDataSubWorkOrder=arrOfSubWorkOrder[btn.tag];
    
    strIndexOnClickForceFullyCloseOtherWos = [NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];

    NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWOStatus"]];
    
    if ([strStatusSubWorkOrder isEqualToString:@"(null)"] || ([strStatusSubWorkOrder isKindOfClass:[NSNull class]])) {
        
        [self fetchSubWorkOrderFromCoreDataIfNull];
        
        dictDataSubWorkOrder=arrOfSubWorkOrder[btn.tag];
        
        strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWOStatus"]];
        
    }
    
    
    NSString *strEmpNoOfSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"employeeNo"]];
    
    BOOL isEMP=[self IsDifferentEmp:strEmpNoOfSubWorkOrder];
    
    BOOL isStartedOtherSubWorkOrder=[global fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToSeeIfSubWorkorderIsAlreadyStarted:[global getEmployeeDeatils] :[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
    
    //isStartedOtherSubWorkOrder=NO;
    
    if ([strStatusSubWorkOrder isEqualToString:@"New"] || [strStatusSubWorkOrder isEqualToString:@"Not Started"] || [strStatusSubWorkOrder isEqualToString:@"NotStarted"]) {
        
        isStartedOtherSubWorkOrder=NO;
        
    }
    
    if (isEMP) {
        
        [global AlertMethod:Alert :@"Work Order Assigned to other Technician you can not process it."];
        
    } else if (isStartedOtherSubWorkOrder){
        
        //[global AlertMethod:Alert :[self alertMessageOnRunningStatus :[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]]];
        
        [self showAlertIfOtherWorkOrderStarted:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]]];
        
    }else {
        
        if ([strStatusSubWorkOrder isEqualToString:@"Not Started"]) {
            
            strStatusSubWorkOrder=@"New";
            
        }
        
        if ([strStatusSubWorkOrder isEqualToString:@"New"]) {
            
            BOOL isNetReachable=[global isNetReachable];
            
            strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            if (isNetReachable) {
                
                [self metodUpdateSubWorkOrderStatus :@"New"];
                
            } else {
                
                [self addActualHrsIfResponseIsNil];
                
            }
            
            [self upDateSubWorkOrderStatusFromDB:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :@"Dispatch"];
            
        } else if ([strStatusSubWorkOrder isEqualToString:@"Dispatch"]){
            
            BOOL isNetReachable=[global isNetReachable];
            
            strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            if (isNetReachable) {
                
                [self metodUpdateSubWorkOrderStatus :@"Dispatch"];
                
            } else {
                
                [self addActualHrsIfResponseIsNil];
                
            }
            
            [self upDateSubWorkOrderStatusFromDB:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :@"OnRoute"];
            
        }else if ([strStatusSubWorkOrder isEqualToString:@"OnRoute"]){
            
            //BOOL isNetReachable=[global isNetReachable];
            
            strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            // change sub wo status in DB and send To Server
            
            [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :dictDataSubWorkOrder];
            
            //[global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"workorderId"]]];
            
        }
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        
        strTypeExpandCollapse=@"exp";
        [self addViewOnLandingNew:@"exp"];
        
    }
}


-(void)action_ViewMoreJobDescription:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    NSManagedObject *matchesMechanicalSubWorkOrderLocal;
    matchesMechanicalSubWorkOrderLocal=arrAllObjMechanicalSubWorkOrder[btn.tag];
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"jobDesc"]];
    [global AlertMethod:@"Job Description" :strJobDesc];
    
}
-(void)action_SubWoEmployeeWorkingTime:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    NSManagedObject *matchesMechanicalSubWorkOrderLocal;
    matchesMechanicalSubWorkOrderLocal=arrAllObjMechanicalSubWorkOrder[btn.tag];
    NSString *strSubWoIdTemp=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWorkOrderId"]];
    NSString *strDepartMentSysName=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"departmentSysName"]];
    NSString *strWorkOrderAccNo=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"accountNo"]];
    NSString *strWorkOrderAddressId=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceAddressId"]];
    NSString *strWorkOrderAddressSubType=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"addressSubType"]];
    NSString *strAfterHrsDuration=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"afterHrsDuration"]];
    //NSString *strWorkOrderStatusTemp=[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWOStatus"];

    NSMutableArray *arrOfHoursConfig=[[NSMutableArray alloc]init];
    arrOfHoursConfig=[global getHoursConfiFromMaster:strCompanyKey :strDepartMentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    EmpTimeSheetViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EmpTimeSheetViewController"];
    objSignViewController.strWorkOrderId=strWorkOrderId;
    objSignViewController.strSubWorkOrderIdGlobal=strSubWoIdTemp;
    objSignViewController.arrOfHoursConfig=arrOfHoursConfig;
    objSignViewController.strWorkOrderStatus=@"Complete";
    objSignViewController.strAfterHrsDuration=strAfterHrsDuration;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

-(void)action_CopySubWorkOrder:(id)sender
{
    BOOL isNetReachable=[global isNetReachable];
    if (isNetReachable) {
        
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:Alert
                                   message:@"Are you sure to copy sub work order..?"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
        [alert addAction:yes];
        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes-Copy" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                                 UIButton *btn = (UIButton *)sender;
                                 NSManagedObject *matchesMechanicalSubWorkOrderLocal;
                                 matchesMechanicalSubWorkOrderLocal=arrAllObjMechanicalSubWorkOrder[btn.tag];
                                 NSString *strId=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWorkOrderId"]];
                                 
                                 [self copySubWorkOrder:strId];
                                 
                             }];
        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    }
}

-(void)action_SelectSubWorkOrder:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    
    if (btn.tag==indexSelectedSubWorkOrder) {
        
        indexSelectedSubWorkOrder=-1;
        
        _lblCurrentStatus.text=[NSString stringWithFormat:@"Current Status: %@",@"N/A"];
        
        [_btn_ActionCurrentStatus setTitle:@"N/A" forState:UIControlStateNormal];
        
    } else {
        
        indexSelectedSubWorkOrder=(int)btn.tag;
        
        NSManagedObject *matchesMechanicalSubWorkOrderLocal;
        
        matchesMechanicalSubWorkOrderLocal=arrAllObjMechanicalSubWorkOrder[indexSelectedSubWorkOrder];
        
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"isActive"]];
        
        if ([strIsActive caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsActive isEqualToString:@"1"]) {
            
            //[arrOfSubWorkOrder addObject:matchesMechanicalSubWorkOrderLocal];
            
        }
        //subWOType
        //Temporary hai comment krna hai y wala code
        strWoType=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWOType"]];
        
        NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWOStatus"]];
        
        strSubWorkOrderStatuss=strStatusSubWorkOrder;
        if ([strSubWorkOrderStatuss isEqualToString:@"Not Started"]) {
            
            strSubWorkOrderStatuss=@"New";
            
        }
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setObject:strSubWorkOrderStatuss forKey:@"WoStatus"];
        strSubWoAcNo=[NSString stringWithFormat:@"Sub Workorder #: %@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWorkOrderNo"]];
        [defs synchronize];
        
        NSManagedObject *objSubWorkOrderCell=arrOfSubWorkOrder[indexSelectedSubWorkOrder];
        objSubWorkOrderToSend=objSubWorkOrderCell;
        strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWorkOrderId"]];
        
        [self saveSubWorkOrderNoInDefaults];
        
    }
    
    strTypeExpandCollapse=@"exp";
    [self addViewOnLandingNew:@"exp"];
    
    [_tblViewSUbWorkOrder reloadData];
    
}

-(void)saveSubWorkOrderNoInDefaults{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    _lblAccountNamenNo.text=[NSString stringWithFormat:@"%@ %@",[defs valueForKey:@"lblName"],strSubWoAcNo];
    [defs setValue:_lblAccountNamenNo.text forKey:@"lblNameNew"];
    [defs synchronize];
    
}
-(void)action_ViewSubWorkOrder:(id)sender
{
    
    if (arrOfSubWorkOrder.count==0) {
        
        [global AlertMethod:Alert :@"Sub Work Order not available"];
        
    } else {
        
        NSManagedObject *matchesMechanicalSubWorkOrderLocal;
        UIButton *btn = (UIButton *)sender;
        matchesMechanicalSubWorkOrderLocal=arrAllObjMechanicalSubWorkOrder[btn.tag];
        
        strSubWorkOrderStatuss=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWOStatus"]];
        
        if ([strSubWorkOrderStatuss isEqualToString:@"(null)"] || ([strSubWorkOrderStatuss isKindOfClass:[NSNull class]])) {
            
            [self fetchSubWorkOrderFromCoreDataIfNull];
            
            matchesMechanicalSubWorkOrderLocal=arrAllObjMechanicalSubWorkOrder[btn.tag];
            
            strSubWorkOrderStatuss=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWOStatus"]];
            
        }
        
        strIndexOnClickForceFullyCloseOtherWos = [NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWorkOrderId"]];
        isClickedOnViewSubWorkOrder = YES;
        isOnlyChangeStatus = NO;
        
        NSString *strEmpNoOfSubWorkOrder=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"employeeNo"]];
        
        BOOL isEMP=[self IsDifferentEmp:strEmpNoOfSubWorkOrder];
        
        BOOL isStartedOtherSubWorkOrder=[global fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToSeeIfSubWorkorderIsAlreadyStarted:[global getEmployeeDeatils] :[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWorkOrderId"]];
        
        isStartedOtherSubWorkOrder=NO; // comment krna hai esko wapis
        
        if ([strSubWorkOrderStatuss isEqualToString:@"New"] || [strSubWorkOrderStatuss isEqualToString:@"Not Started"] || [strSubWorkOrderStatuss isEqualToString:@"NotStarted"] || [strSubWorkOrderStatuss isEqualToString:@"Completed"] || [strSubWorkOrderStatuss isEqualToString:@"CompletePending"] || [strSubWorkOrderStatuss isEqualToString:@"Complete"]) {
            
            isStartedOtherSubWorkOrder=NO;
            
        }
        
        if (isEMP) {
            
            [global AlertMethod:Alert :@"Work Order Assigned to other Technician you can not process it."];
            
        } else if (isStartedOtherSubWorkOrder){
            
            //[global AlertMethod:Alert :[self alertMessageOnRunningStatus :[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWorkOrderId"]]];
            
            [self showAlertIfOtherWorkOrderStarted:[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWorkOrderId"]]];
            
        }else {
            
            if ([strSubWorkOrderStatuss isEqualToString:@"New"] || [strSubWorkOrderStatuss isEqualToString:@"Dispatch"]) {
                
                [global AlertMethod:Alert :@"Please update sub work order status"];
                
            }else{
                
                [self updateWorkOrderDetail];
                
                if (yesEditedSomething) {
                    
                    [self saveEmailIdPrimaryToCoreData];
                    
                    [self saveImageToCoreData];
                    
                    //  [self updateWorkOrderDetail];
                    
                } else {
                    
                    
                    
                }
                
                yesEditedSomething=YES;
                UIButton *btn = (UIButton *)sender;
                NSManagedObject *dictDataSubWorkOrder=arrOfSubWorkOrder[btn.tag];
                
                NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWOStatus"]];
                
                strSubWorkOrderStatuss=strStatusSubWorkOrder;
                
                if ([strSubWorkOrderStatuss isEqualToString:@"Not Started"]) {
                    
                    strSubWorkOrderStatuss=@"New";
                    
                }
                
                //strWorkOrderStatuss=@"Completed";
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:strSubWorkOrderStatuss forKey:@"WoStatus"];
                [defs synchronize];
                
                // New Change For SubWorkOrder Multiple
                
                NSManagedObject *matchesMechanicalSubWorkOrderLocal;
                
                matchesMechanicalSubWorkOrderLocal=arrAllObjMechanicalSubWorkOrder[btn.tag];
                
                NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"isActive"]];
                
                if ([strIsActive caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsActive isEqualToString:@"1"]) {
                    
                    [arrOfSubWorkOrder addObject:matchesMechanicalSubWorkOrderLocal];
                    
                }
                //subWOType
                //Temporary hai comment krna hai y wala code
                strWoType=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWOType"]];
                
                strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWOStatus"]];
                
                strSubWorkOrderStatuss=strStatusSubWorkOrder;
                if ([strSubWorkOrderStatuss isEqualToString:@"Not Started"]) {
                    
                    strSubWorkOrderStatuss=@"New";
                    
                }
                
                [defs setObject:strSubWorkOrderStatuss forKey:@"WoStatus"];
                strSubWoAcNo=[NSString stringWithFormat:@"Sub Workorder #: %@",[matchesMechanicalSubWorkOrderLocal valueForKey:@"subWorkOrderNo"]];
                [defs synchronize];
                
                [self saveSubWorkOrderNoInDefaults];
                
                NSManagedObject *objSubWorkOrderCell=arrOfSubWorkOrder[btn.tag];
                objSubWorkOrderToSend=objSubWorkOrderCell;
                strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWorkOrderId"]];
                
                indexSelectedSubWorkOrder=(int)btn.tag;
                
                // ENDdd  New Change For SubWorkOrder Multiple
                
                
                if ([strSubWorkOrderStatuss isEqualToString:@"Running"] || [strSubWorkOrderStatuss isEqualToString:@"Approved"]) {
                    
                    [self goToStartRepairView];
                    
                } else {
                    
                    [self goToSubWorkOrderDetailView];
                    
                }
                
            }
            
        }
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark - UITextFieldDelegate
//============================================================================
//============================================================================

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textViewDidBeginEditing:(UITextView *)textField{
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    yesEditedSomething=YES;
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    if (textView==_txtViewTechComment) {
        
        [matchesWorkOrder setValue:_txtViewTechComment.text forKey:@"technicianComment"];
        
        NSError *error2;
        [context save:&error2];
        
    }
    yesEditedSomething=YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    yesEditedSomething=YES;
    
    if ((textField.tag==3) || (textField.tag==4)) {
        
        if ([string isEqualToString:@""]) {
            
            return true;
            
        } else {
            
            BOOL isNuberOnly=[global isNumberOnly:string :range :20 :(int)textField.text.length :@"+0123456789"];
            
            return isNuberOnly;
            
            //            NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"+0123456789"];
            //
            //            NSRange range = [string rangeOfCharacterFromSet:cset];
            //            if (range.location == NSNotFound) {
            //                // no ( or ) in the string
            //                return false;
            //            } else {
            //                // ( or ) are present
            //                return true;
            //            }
            
        }
        
    } else {
        
        return true;
        
    }
}
//============================================================================
//============================================================================
#pragma mark - RKTagsViewDelegate
//============================================================================
//============================================================================

- (UIButton *)tagsView:(RKTagsView *)tagsView buttonForTagAtIndex:(NSInteger)index {
    RKCustomButton *customButton = [RKCustomButton buttonWithType:UIButtonTypeSystem];
    customButton.titleLabel.font = tagsView.font;
    [customButton setTitle:[NSString stringWithFormat:@"%@,", tagsView.tags[index]] forState:UIControlStateNormal];
    [customButton runBubbleAnimation];
    
    arrOfTags=nil;
    arrOfTags=[[NSMutableArray alloc]init];
    
    [arrOfTags addObjectsFromArray:tagsView.tags];
    
    return customButton;
}
- (void)tagsViewDidChange:(RKTagsView *)tagsView{
    
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Button Action METHODS-----------------
//============================================================================
//============================================================================

- (IBAction)action_back:(id)sender {
    
    ////Yaha Par zSyn ko yes karna hai
    [matchesWorkOrder setValue:@"yes" forKey:@"zSync"];
    
    [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
    
    [self saveImageToCoreData];
    
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
        AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
        AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
    
    // [self.navigationController popViewControllerAnimated:YES];
    
    //[self methodBack];
    
}

- (IBAction)action_PrimaryEmail:(id)sender {
    
    if (!isEditCustomerInfo) {
        
        NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
        
        if (strMessage.length==0) {
            
        } else {
            
            [global emailComposer:strMessage :@"" :@"" :self];
            
        }
        
    } else {
        
        
        if (isCompletedStatusMechanical) {
            
            NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
            
            if (strMessage.length==0) {
                
            } else {
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@""
                                           message:strMessage
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                      }];
                [alert addAction:yes];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            
        }else{
            
            [self tablViewForPrimaryEmailIds];
            
        }
    }
}
- (IBAction)action_SecondaryEmail:(id)sender {
    
    if (!isEditCustomerInfo) {
        
        NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
        
        if (strMessage.length==0) {
            
        } else {
            
            [global emailComposer:strMessage :@"" :@"" :self];
            
        }
        
    } else {
        
        if (isCompletedStatusMechanical) {
            
            NSString *strMessage=[arrOfSecondaryEmailNew componentsJoinedByString:@","];
            if (strMessage.length==0) {
                
            } else {
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@""
                                           message:strMessage
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                      }];
                [alert addAction:yes];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            
        }else{
            
            [self tablViewForSecondaryEmailIds];
            
        }
        
    }
    
}
- (IBAction)action_SavenContinue:(id)sender {
    
    if (arrOfSubWorkOrder.count==0) {
        
        [global AlertMethod:Alert :@"Sub Work Order not available"];
        
    } else {
        
        if (indexSelectedSubWorkOrder>=0) {
            
            isClickedOnViewSubWorkOrder = NO;
            isOnlyChangeStatus = NO;

            
            NSString *strEmpNoOfSubWorkOrder=[NSString stringWithFormat:@"%@",[objSubWorkOrderToSend valueForKey:@"employeeNo"]];
            
            if ([strEmpNoOfSubWorkOrder isEqualToString:@"(null)"] || ([strEmpNoOfSubWorkOrder isKindOfClass:[NSNull class]])) {
                
                [self fetchSubWorkOrderFromCoreDataIfNull];
                
                strEmpNoOfSubWorkOrder=[NSString stringWithFormat:@"%@",[objSubWorkOrderToSend valueForKey:@"employeeNo"]];
                
            }
            
            BOOL isEMP=[self IsDifferentEmp:strEmpNoOfSubWorkOrder];
            
            BOOL isStartedOtherSubWorkOrder=[global fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToSeeIfSubWorkorderIsAlreadyStarted:[global getEmployeeDeatils] :[objSubWorkOrderToSend valueForKey:@"subWorkOrderId"]];
            
            //isStartedOtherSubWorkOrder=NO;
            
            if ([strSubWorkOrderStatuss isEqualToString:@"New"] || [strSubWorkOrderStatuss isEqualToString:@"Not Started"] || [strSubWorkOrderStatuss isEqualToString:@"NotStarted"] || [strSubWorkOrderStatuss isEqualToString:@"Completed"] || [strSubWorkOrderStatuss isEqualToString:@"CompletePending"] || [strSubWorkOrderStatuss isEqualToString:@"Complete"]) {
                
                isStartedOtherSubWorkOrder=NO;
                
            }
            if (isEMP) {
                
                [global AlertMethod:Alert :@"Work Order Assigned to other Technician you can not process it."];
                
            } else if (isStartedOtherSubWorkOrder){
                
                //[global AlertMethod:Alert :[self alertMessageOnRunningStatus :[objSubWorkOrderToSend valueForKey:@"subWorkOrderId"]]];
                
                [self showAlertIfOtherWorkOrderStarted:[NSString stringWithFormat:@"%@",[objSubWorkOrderToSend valueForKey:@"subWorkOrderId"]]];
                
            }else {
                
                if ([strSubWorkOrderStatuss isEqualToString:@"New"] || [strSubWorkOrderStatuss isEqualToString:@"Dispatch"]) {
                    
                    [global AlertMethod:Alert :@"Please update sub work order status"];
                    // isOnRouteForcefully
                }else if ([strSubWorkOrderStatuss isEqualToString:@"OnRoute"]){
                    
                    isOnRouteForcefully =YES;
                    
                    //BOOL isNetReachable=[global isNetReachable];
                    
                    strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[objSubWorkOrderToSend valueForKey:@"subWorkOrderId"]];
                    
                    // change sub wo status in DB and send To Server
                    
                    [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute:[NSString stringWithFormat:@"%@",[objSubWorkOrderToSend valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[objSubWorkOrderToSend valueForKey:@"subWorkOrderId"]] :objSubWorkOrderToSend];
                    
                    [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:[NSString stringWithFormat:@"%@",[objSubWorkOrderToSend valueForKey:@"workorderId"]]];
                    
                }else{
                    
                    [self updateWorkOrderDetail];
                    
                    [self saveEmailIdPrimaryToCoreData];
                    
                    if (yesEditedSomething) {
                        
                        //[self saveEmailIdPrimaryToCoreData];
                        
                        [self saveImageToCoreData];
                        
                        //  [self updateWorkOrderDetail];
                        
                    } else {
                        
                        
                        
                    }
                    
                    if ([strSubWorkOrderStatuss isEqualToString:@"Running"] || [strSubWorkOrderStatuss isEqualToString:@"Approved"]) {
                        
                        [self goToStartRepairView];
                        
                        // [self goToSubWorkOrderDetailView];
                        
                    } else {
                        
                        [self goToSubWorkOrderDetailView];
                        
                    }
                    
                }
                
            }
            
        } else {
            
            [global AlertMethod:Alert :@"Please select Sub Work order to process"];
            
        }
        
    }
}
- (IBAction)action_Cancel:(id)sender {
    
    NSUserDefaults *defsAppointemnts = [NSUserDefaults standardUserDefaults];
    
    NSString *strAppointmentFlow = [defsAppointemnts valueForKey:@"AppointmentFlow"];
    
    if ([strAppointmentFlow isEqualToString:@"New"])
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Appointment_iPAD"bundle: nil];
        AppointmentVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"bundle: nil];
        AppointmentViewiPad *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentViewiPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
    
    
}
- (IBAction)action_CloseBeforImgView:(id)sender {
    
    [_view_BeforeImage removeFromSuperview];
    
    // [_scrollViewGeneralInfo setFrame:CGRectMake(0, _scrollViewGeneralInfo.frame.origin.y, _scrollViewGeneralInfo.frame.size.width, _scrollViewGeneralInfo.frame.size.height+_viewFinalSavenContinue.frame.size.height+_view_BeforeImage.frame.size.height)];
    
}

- (IBAction)action_BeforeImgView:(id)sender {
    
    //Adding BeforeImageInfoView
    
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewFinalSavenContinue.frame.origin.y-_view_BeforeImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view_BeforeImage.frame.size.height);
    [_view_BeforeImage setFrame:frameFor_view_BeforeImageInfo];
    [self.view addSubview:_view_BeforeImage];
    
    //   [_scrollViewGeneralInfo setFrame:CGRectMake(0, _scrollViewGeneralInfo.frame.origin.y, _scrollViewGeneralInfo.frame.size.width, _scrollViewGeneralInfo.frame.size.height-160)];
    
    // [_scrollViewGeneralInfo setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_OtherInfo.frame.size.height+_view_OtherInfo.frame.origin.y)];
    
    
}

- (IBAction)action_BeforeImages:(id)sender {
    
    /*
     UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
     delegate:self
     cancelButtonTitle:@"Cancel"
     destructiveButtonTitle:nil
     otherButtonTitles:@"Capture New", @"Gallery", nil];
     
     [actionSheet showInView:self.view];
     
     */
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              if (isCompletedStatusMechanical) {
                                  
                                  
                              }else{
                                  
                                  if (arrOfBeforeImageAll.count<10)
                                  {
                                      NSLog(@"The CApture Image.");
                                      
                                      NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                      
                                      BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                                      
                                      if (isfirstTimeAudio) {
                                          
                                          [defs setBool:NO forKey:@"firstCamera"];
                                          [defs synchronize];
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }else{
                                          BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                          
                                          if (isCameraPermissionAvailable) {
                                              
                                              UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                              imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                              imagePickController.delegate=(id)self;
                                              imagePickController.allowsEditing=TRUE;
                                              [self presentViewController:imagePickController animated:YES completion:nil];
                                              
                                              
                                          }else{
                                              
                                              UIAlertController *alert= [UIAlertController
                                                                         alertControllerWithTitle:@"Alert"
                                                                         message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                              
                                              UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction * action)
                                                                    {
                                                                        
                                                                        
                                                                        
                                                                    }];
                                              [alert addAction:yes];
                                              UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                           NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                           [[UIApplication sharedApplication] openURL:url];
                                                                       } else {
                                                                           
                                                                           //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                           //                                     [alert show];
                                                                           
                                                                       }
                                                                       
                                                                   }];
                                              [alert addAction:no];
                                              [self presentViewController:alert animated:YES completion:nil];
                                          }
                                      }
                                  }
                                  else
                                  {
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                      [alert show];
                                  }
                              }
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if (isCompletedStatusMechanical) {
                                 
                                 
                             }else{
                                 
                                 if (arrOfBeforeImageAll.count<10)
                                 {
                                     
                                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                     
                                     BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                                     
                                     if (isfirstTimeAudio) {
                                         
                                         [defs setBool:NO forKey:@"firstGallery"];
                                         [defs synchronize];
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }else{
                                         BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                         
                                         if (isCameraPermissionAvailable) {
                                             
                                             UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                             imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                             imagePickController.delegate=(id)self;
                                             imagePickController.allowsEditing=TRUE;
                                             [self presentViewController:imagePickController animated:YES completion:nil];
                                             
                                             
                                         }else{
                                             
                                             UIAlertController *alert= [UIAlertController
                                                                        alertControllerWithTitle:@"Alert"
                                                                        message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action)
                                                                   {
                                                                       
                                                                       
                                                                       
                                                                   }];
                                             [alert addAction:yes];
                                             UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                        handler:^(UIAlertAction * action)
                                                                  {
                                                                      
                                                                      if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                          NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                          [[UIApplication sharedApplication] openURL:url];
                                                                      } else {
                                                                          
                                                                          //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                          //                                     [alert show];
                                                                          
                                                                      }
                                                                      
                                                                  }];
                                             [alert addAction:no];
                                             [self presentViewController:alert animated:YES completion:nil];
                                         }
                                     }
                                 }else{
                                     
                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be chosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                     [alert show];
                                 }
                             }
                             
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)action_Documents:(id)sender {
    
    [self goToDocumentView];
    
}

- (IBAction)action_ServiceHistory:(id)sender {
    
    [self goToServiceHistory];
    
}


#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(_btnUploadServiceAddImage.tag==101)
    {
        
        NSLog(@"Yes Edited Something In Db");
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        // NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
        NSString  *strImageNamess = [NSString stringWithFormat:@"CustomerAddressImages\\ServiceAddImg%@%@.jpg",strDate,strTime];
        
        // [arrOfServiceAddImageName addObject:strImageNamess];
        UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
        //_imgViewServiceAddress.image=image;
        [self resizeImage:image :strImageNamess];
        strServiceAddImage=strImageNamess;
        [self saveImageAfterDownloadServiceAdd :image :strImageNamess];
        [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    }
    else
    {
        yesEditedSomething=YES;
        NSLog(@"Yes Edited Something In Db");
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
        
        [arrOfBeforeImageAll addObject:strImageNamess];
        [arrOfImagenameCollewctionView addObject:strImageNamess];
        [_beforeImageCollectionView reloadData];
        
        UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
        [self resizeImage:chosenImage :strImageNamess];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
        //imageCaption
        
        NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
        
        BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
        
        if (yesImageCaption) {
            
            [self alertViewCustom];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
    }
    
}


-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    
    //NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 105, [UIScreen mainScreen].bounds.size.width-20, 430)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 50)];
    
    lblCaption.text=@"Enter image caption below...";
    lblCaption.font=[UIFont systemFontOfSize:22];
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 100)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:22];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+105, viewAlertt.bounds.size.width-20, 50)];
    
    lbl.text=@"Enter image description below...";
    lbl.font=[UIFont systemFontOfSize:22];
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+55, viewAlertt.bounds.size.width-20, 200)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:22];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+435, viewAlertt.frame.size.width/2-20, 50)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    btnSave.titleLabel.font=[UIFont systemFontOfSize:22];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,50)];
    [btnDone setTitle:@"No Caption" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont systemFontOfSize:22];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        //        CGPoint bottomOffset = CGPointMake(0, _scrollViewGeneralInfo.contentSize.height - _scrollViewGeneralInfo.bounds.size.height);
        //        [_scrollViewGeneralInfo setContentOffset:bottomOffset animated:YES];
        
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}


-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    //    CGPoint bottomOffset = CGPointMake(0, _scrollViewGeneralInfo.contentSize.height - _scrollViewGeneralInfo.bounds.size.height);
    //    [_scrollViewGeneralInfo setContentOffset:bottomOffset animated:YES];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)action_ClientApproval:(id)sender {
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnClientApproval imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        [_btnClientApproval setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        isClientApproval=YES;
        
    }else{
        
        [_btnClientApproval setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        isClientApproval=NO;
        
    }
}


-(void)fetchWorkOrderFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [self.fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        strWorkOrderNoGlobal = [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"workOrderNo"]];
        
        NSLog(@"Work Order Detail on Mechanical General infor view====%@",matchesWorkOrder);
        [self showValuesMechanicalGeneralInfo];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)showValuesMechanicalGeneralInfo{
    
    //changes for multiple POC
    [self fetchServiceAddressPOCDetailDcsFromDB];
    [self fetchBillingAddressPOCDetailDcsFromDB];
    
    strServicePOCId=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"servicePOCId"]];
    strBillingPOCId=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingPOCId"]];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    NSString *strFirstName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"firstName"]];
    NSString *strMiddleName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"middleName"]];
    NSString *strLastName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"lastName"]];
    
    NSMutableArray *arrOfName=[[NSMutableArray alloc]init];
    
    if (!(strFirstName.length==0)) {
        [arrOfName addObject:strFirstName];
    }
    if (!(strMiddleName.length==0)) {
        
        [arrOfName addObject:strMiddleName];
        
    }
    if (!(strLastName.length==0)) {
        
        [arrOfName addObject:strLastName];
        
    }
    
    NSString *strFullName=[arrOfName componentsJoinedByString:@" "];
    strWorkOrderStatuss=[matchesWorkOrder valueForKey:@"workorderStatus"];
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    
    [defs setObject:strFullName forKey:@"customerNameService"];
    [defs setObject:strEmpName forKey:@"inspectorNameService"];
    [defs synchronize];
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:[matchesWorkOrder valueForKey:@"timeIn"]];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:[matchesWorkOrder valueForKey:@"timeIn"]];
    }
    
    
    _lblCustName.text=strFullName;
    
    _lblCompanyName.text=[matchesWorkOrder valueForKey:@"companyName"];
    
    _txtViewNotes.text=[matchesWorkOrder valueForKey:@"serviceNotes"];
    
    strEmailIdGlobal=[matchesWorkOrder valueForKey:@"primaryEmail"];
    
    strEmailIdGlobalSecondary=[matchesWorkOrder valueForKey:@"secondaryEmail"];
    
    _txtCustomerPO.text=[matchesWorkOrder valueForKey:@"customerPONumber"];
    
    strGlobalServiceJobDescriptionId=[matchesWorkOrder valueForKey:@"serviceJobDescriptionId"];
    _textView_ServiceJobDescriptions.text=[matchesWorkOrder valueForKey:@"serviceJobDescription"];
    NSString *strServiceJobDescription = [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceJobDescription"]];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [strServiceJobDescription dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
     _textView_ServiceJobDescriptions.attributedText = attributedString;
    
    strGlobalServiceJobDescriptionsText=[matchesWorkOrder valueForKey:@"serviceJobDescription"];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    if ([dictdata isKindOfClass:[NSString class]]) {
        
        //[global AlertMethod:Alert :NoDataAvailableServiceJobDescriptions];
        
    }else{
        
        NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceJobDescriptionTemplate"];
        if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
            
            for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                
                NSDictionary *dictData=[arrOfServiceJobDescriptionTemplate objectAtIndex:k];
                
                if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]] isEqualToString:strGlobalServiceJobDescriptionId]) {
                    
                    [_btnServiceJobDescriptionTitle setTitle:[dictData valueForKey:@"Title"] forState:UIControlStateNormal];
                    
                }
            }
        }
    }
    
    //Multiple Primary Email
    
    NSString *strTemPrimaryEmail=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"primaryEmail"]];
    NSString *strTemSecondaryEmail=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"secondaryEmail"]];
    
    NSArray *arrTempPrimaryEmail,*arrTempsecondaryEmail;
    if (!(strTemPrimaryEmail.length==0) &&!([strTemPrimaryEmail isEqualToString:@"(null)"])) {
        
        [_btnPrimaryEmail setTitle:[matchesWorkOrder valueForKey:@"primaryEmail"] forState:UIControlStateNormal];
        arrTempPrimaryEmail=[strTemPrimaryEmail componentsSeparatedByString:@","];
        
    }else{
        
        [_btnPrimaryEmail setTitle:@"" forState:UIControlStateNormal];
        
    }
    
    if (!(strTemSecondaryEmail.length==0) &&!([strTemSecondaryEmail isEqualToString:@"(null)"])) {
        
        [_btnSecondaryEmail setTitle:[matchesWorkOrder valueForKey:@"secondaryEmail"] forState:UIControlStateNormal];
        arrTempsecondaryEmail=[strTemSecondaryEmail componentsSeparatedByString:@","];
        
    }else{
        
        [_btnSecondaryEmail setTitle:@"" forState:UIControlStateNormal];
        
    }
    
    if (arrTempPrimaryEmail.count>0) {
        
        [arrOfPrimaryEmailNew addObjectsFromArray:arrTempPrimaryEmail];
        [arrOfPrimaryEmailAlreadySaved addObjectsFromArray:arrTempPrimaryEmail];
        
    }
    if (arrTempsecondaryEmail.count>0) {
        
        [arrOfSecondaryEmailNew addObjectsFromArray:arrTempsecondaryEmail];
        [arrOfSecondaryEmailAlreadySaved addObjectsFromArray:arrTempsecondaryEmail];
        
    }
    
    
    if (_btnPrimaryEmail.titleLabel.text.length==0) {
        
        //  [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        //  isNoEmail=YES;
        
    }
    
    _txtPrimaryPhone.text=[matchesWorkOrder valueForKey:@"primaryPhone"];
    
    _txtSecondaryPhone.text=[matchesWorkOrder valueForKey:@"secondaryPhone"];
    
    _lblWorkOrderNo.text=[matchesWorkOrder valueForKey:@"workOrderNo"];
    
    _lblAccountNo.text=[matchesWorkOrder valueForKey:@"accountNo"];
    
    NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"thirdPartyAccountNo"]];
    
    if (strThirdPartyAccountNo.length>0) {
        
        _lblAccountNo.text=strThirdPartyAccountNo;
        
    }else{
        
        strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"accountNo"]];
        _lblAccountNo.text=strThirdPartyAccountNo;
        
    }
    
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];//Order accountNo
    //_lblAccountNamenNo.text=[NSString stringWithFormat:@"A/C #: %@, Order #: %@",[matchesWorkOrder valueForKey:@"accountNo"],[matchesWorkOrder valueForKey:@"workOrderNo"]];
    
    _lblAccountNamenNo.text=[NSString stringWithFormat:@"A/C #: %@, Order #: %@",strThirdPartyAccountNo,[matchesWorkOrder valueForKey:@"workOrderNo"]];
    
    [defsss setValue:_lblAccountNamenNo.text forKey:@"lblName"];
    [defsss setValue:[matchesWorkOrder valueForKey:@"accountNo"] forKey:@"AccNoService"];
    [defsss setValue:[matchesWorkOrder valueForKey:@"departmentId"] forKey:@"DepartmentIdService"];
    [defsss synchronize];
    
    _lblCategory.text=[matchesWorkOrder valueForKey:@"categoryName"];
    
    _lblServices.text=[matchesWorkOrder valueForKey:@"services"];
    
    _lblEmpNo.text=[NSString stringWithFormat:@"%@(%@)",[global getEmployeeNameViaId:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"employeeNo"]]],[matchesWorkOrder valueForKey:@"employeeNo"]];
    
    // _lblEmpName.text=[global getEmployeeNameViaId:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"employeeNo"]]];
    
    //  _lblRouteName.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"routeName"]];
    
    _lblRouteNo.text=[NSString stringWithFormat:@"%@(%@)",[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"routeName"]],[matchesWorkOrder valueForKey:@"routeNo"]];
    
    _lblScheduleStartDatenTime.text=[global ChangeDateToLocalDateOtherServiceAppointmentsUpdated:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"scheduleStartDateTime"]]];
    
    _lblScheduleEndDatenTime.text=[global ChangeDateToLocalDateOtherServiceAppointmentsUpdated:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"scheduleEndDateTime"]]];
    
    NSString *strBillingcountry=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingCountry"]];
    
    if ([strBillingcountry caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strBillingcountry=@"United States";
    }
    
    NSString *strbillingAddress2=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingAddress2"]];
    if (strbillingAddress2.length==0) {
        
        _lblBillingAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@",[matchesWorkOrder valueForKey:@"billingAddress1"],[matchesWorkOrder valueForKey:@"billingCity"],[matchesWorkOrder valueForKey:@"billingState"],[matchesWorkOrder valueForKey:@"billingZipcode"]];
        _lblBillingAddress.attributedText=[global getUnderLineAttributedString:_lblBillingAddress.text];
        
    } else {
        
        _lblBillingAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"billingAddress1"],[matchesWorkOrder valueForKey:@"billingAddress2"],[matchesWorkOrder valueForKey:@"billingCity"],[matchesWorkOrder valueForKey:@"billingState"],[matchesWorkOrder valueForKey:@"billingZipcode"]];
        _lblBillingAddress.attributedText=[global getUnderLineAttributedString:_lblBillingAddress.text];
        
    }
    
    _lblServiceMapCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceMapCode"]];
    _lblServiceGateCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceGateCode"]];
    _lblBillingMapCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceMapCode"]];
    _lblBillingGateCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceGateCode"]];
    _lblAccountDescriptions.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"accountDescription"]];
    
    NSString *strIsClientApproval=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"isClientApprovalReq"]];
    
    if ([strIsClientApproval caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsClientApproval isEqualToString:@"1"]) {
        
        isClientApproval=YES;
        [_btnClientApproval setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        _lblStatusClientApproval.text=@"Required";
        
    }else{
        
        isClientApproval=NO;
        [_btnClientApproval setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        _lblStatusClientApproval.text=@"Not Required";
    }
    
    
    NSString *strTempString1=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"billingAddress1"],[matchesWorkOrder valueForKey:@"billingAddress2"],[matchesWorkOrder valueForKey:@"billingCity"],[matchesWorkOrder valueForKey:@"billingState"],[matchesWorkOrder valueForKey:@"billingZipcode"]];
    
    strTempString1=[strTempString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *strServiceAddress=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceCountry"]];
    
    if ([strServiceAddress caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strServiceAddress=@"United States";
    }
    
    NSString *strServiceAddress2=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingAddress2"]];
    if (strServiceAddress2.length==0) {
        
        _lblServiceAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@",[matchesWorkOrder valueForKey:@"servicesAddress1"],[matchesWorkOrder valueForKey:@"serviceCity"],[matchesWorkOrder valueForKey:@"serviceState"],[matchesWorkOrder valueForKey:@"serviceZipcode"]];
        
        _lblServiceAddress.attributedText=[global getUnderLineAttributedString:_lblServiceAddress.text];
        
    }else{
        
        _lblServiceAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"servicesAddress1"],[matchesWorkOrder valueForKey:@"serviceAddress2"],[matchesWorkOrder valueForKey:@"serviceCity"],[matchesWorkOrder valueForKey:@"serviceState"],[matchesWorkOrder valueForKey:@"serviceZipcode"]];
        
        _lblServiceAddress.attributedText=[global getUnderLineAttributedString:_lblServiceAddress.text];
        
    }
    
    
    NSString *strTempString2=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[matchesWorkOrder valueForKey:@"servicesAddress1"],[matchesWorkOrder valueForKey:@"serviceAddress2"],[matchesWorkOrder valueForKey:@"serviceCity"],[matchesWorkOrder valueForKey:@"serviceState"],[matchesWorkOrder valueForKey:@"serviceZipcode"]];
    
    strTempString2=[strTempString2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    _lblSpecialInstructions.text=[matchesWorkOrder valueForKey:@"specialInstruction"];
    
    NSString *strTempString3=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"specialInstruction"]];
    
    strTempString3=[strTempString3 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    _lblServiceInstructions.text=[matchesWorkOrder valueForKey:@"serviceInstruction"];
    
    _lblDirections.text=[matchesWorkOrder valueForKey:@"direction"];
    
    _lblOtherInstructions.text=[matchesWorkOrder valueForKey:@"otherInstruction"];
    
    _txtViewTechComment.text=[matchesWorkOrder valueForKey:@"technicianComment"];
    
    NSString *strTags=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"tags"]];
    
    NSArray *arrTagTemp=[strTags componentsSeparatedByString:@","];
    
    [arrOfTags addObjectsFromArray:arrTagTemp];
    
    if (isCompletedStatusMechanical) {
        
        self.tagsView.editable = false;
        self.tagsView.selectable = false;
        self.tagsView.allowsMultipleSelection = false;
        _txtSecondaryPhone.enabled=false;
        _txtPrimaryPhone.enabled=false;
        _txtViewNotes.editable=false;
        _txtViewTechComment.editable=false;
        _btnClientApproval.enabled=false;
        _btnUploadServiceAddImage.enabled=false;
        _txtCustomerPO.enabled=NO;
        _textView_ServiceJobDescriptions.editable=NO;
        _btnEditCustomerInfo.hidden=YES;
        _btn_ServiceAddressPOCDetailDcs.enabled=NO;
        _btn_BillingAddressPOCDetailDcs.enabled=NO;
        [_btnAddSubWorkOrder setHidden:YES];
        [_btnEmail setHidden:YES];
        [_btnTextMsg setHidden:YES];
        
    }else
    {
        
        BOOL isCreateSubWorkOder=[defs boolForKey:@"isCreateSubWorkOrder"];
        
        if (isCreateSubWorkOder) {
            [_btnAddSubWorkOrder setHidden:NO];
        } else {
            [_btnAddSubWorkOrder setHidden:YES];
        }
        
        [_btnEmail setHidden:NO];
        [_btnTextMsg setHidden:NO];
        
        NSString *strSurveID= [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"surveyID"]];
        
        if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
            
            [_btnEmail setHidden:NO];
            [_btnTextMsg setHidden:NO];
            
        }else{
            
            [_btnEmail setHidden:YES];
            [_btnTextMsg setHidden:YES];
            
        }
        _btn_ServiceAddressPOCDetailDcs.enabled=YES;
        _btn_BillingAddressPOCDetailDcs.enabled=YES;
        _btnEditCustomerInfo.hidden=NO;
        _textView_ServiceJobDescriptions.editable=YES;
        _txtCustomerPO.enabled=YES;
        self.tagsView.editable = true;
        self.tagsView.selectable = true;
        self.tagsView.allowsMultipleSelection = true;
        _txtSecondaryPhone.enabled=true;
        _txtPrimaryPhone.enabled=true;
        _txtViewNotes.editable=true;
        _txtViewTechComment.editable=true;
        _btnClientApproval.enabled=false;
        _btnUploadServiceAddImage.enabled=true;
        
        
    }
    
    //Nilind POC CHANGE 22 Nov
    
    _billingContactName.text=[NSString stringWithFormat:@"%@ %@ %@",[matchesWorkOrder valueForKey:@"billingFirstName"],[matchesWorkOrder valueForKey:@"billingMiddleName"],[matchesWorkOrder valueForKey:@"billingLastName"]];
    
    _billingContactCellNo.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingCellNo"]]];
    
    _billingContactMapCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingMapCode"]];
    _billingContactPrimaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingPrimaryEmail"]]];
    
    _billingContactSecondayEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingSecondaryEmail"]]];
    
    _billingContactPrimaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingPrimaryPhone"]]];
    
    _billingContactSecondaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingSecondaryPhone"]]];
    
    _serviceContactName.text=[NSString stringWithFormat:@"%@ %@ %@",[matchesWorkOrder valueForKey:@"serviceFirstName"],[matchesWorkOrder valueForKey:@"serviceMiddleName"],[matchesWorkOrder valueForKey:@"serviceLastName"]];
    _serviceContactCellNo.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceCellNo"]]];
    
    _serviceContactMapCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceMapCode"]];
    // _serviceContactGateCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceGateCode"]];
    
    _serviceContactPrimaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"servicePrimaryEmail"]]];
    
    _serviceContactSecondaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceSecondaryEmail"]]];
    
    _serviceContactPrimaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"servicePrimaryPhone"]]];
    
    _serviceContactSecondaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceSecondaryPhone"]]];
    
    // _serviceContactNotes.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceNotes"]];
    
    strServiceAddImage= [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceAddressImagePath"]];
    
    _lblBillingCounty.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingCounty"]];
    _lblBillingSchoolDistrict.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"billingSchoolDistrict"]];
    _lblServiceCounty.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceCounty"]];
    _lblServiceSchoolDistrict.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"serviceSchoolDistrict"]];
    
    _lblDriveTime.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"driveTimeStr"]];
    _lblLatestStartTime.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"latestStartTimeStr"]];
    _lblEarliestStartTime.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"earliestStartTimeStr"]];
    
    
    [self downloadImageServiceAddImage:strServiceAddImage];
    
    if (strServicePOCId.length>0) {
        
        [self fetchServicePocDetails];
        
    }
    
    if (strBillingPOCId.length>0) {
        
        [self fetchBillingPocDetails];
        
    }
    
}


-(void)updateWorkOrderDetail{
    
    if (yesEditedSomething) {
        
        NSLog(@"Yes Edited Something In Db");
        
        ////Yaha Par zSyn ko yes karna hai
        [matchesWorkOrder setValue:@"yes" forKey:@"zSync"];
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        
    }
    
    NSString *strAssignedCrewIds=[arrOfTags componentsJoinedByString:@","];
    
    [matchesWorkOrder setValue:strAssignedCrewIds forKey:@"tags"];
    [matchesWorkOrder setValue:_txtViewTechComment.text forKey:@"technicianComment"];
    [matchesWorkOrder setValue:_btnPrimaryEmail.currentTitle forKey:@"primaryEmail"];
    [matchesWorkOrder setValue:_btnSecondaryEmail.currentTitle forKey:@"secondaryEmail"];
    [matchesWorkOrder setValue:_txtPrimaryPhone.text forKey:@"primaryPhone"];
    [matchesWorkOrder setValue:_txtSecondaryPhone.text forKey:@"secondaryPhone"];
    [matchesWorkOrder setValue:strWorkOrderStatuss forKey:@"workorderStatus"];
    [matchesWorkOrder setValue:strEmpID forKey:@"modifiedBy"];
    [matchesWorkOrder setValue:_txtCustomerPO.text forKey:@"customerPONumber"];
    // [matchesWorkOrder setValue:[global strCurrentDate] forKey:@"dateModified"];
    
    [matchesWorkOrder setValue:strGlobalServiceJobDescriptionId forKey:@"serviceJobDescriptionId"];
    [matchesWorkOrder setValue:_textView_ServiceJobDescriptions.text forKey:@"serviceJobDescription"];
    
    NSString *DeviceVersion = [[UIDevice currentDevice] systemVersion];
    NSString *VersionDatee = VersionDate;
    NSString *VersionNumberr = VersionNumber;
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *DeviceName=[NSString stringWithCString:systemInfo.machine
                                            encoding:NSUTF8StringEncoding];
    
    NSLog(@"DeviceVersion===%@----VersionDatee===%@----VersionNumberr===%@----DeviceName===%@",DeviceVersion,VersionDatee,VersionNumberr,DeviceName);
    
    [matchesWorkOrder setValue:VersionDatee forKey:@"versionDate"];
    [matchesWorkOrder setValue:VersionNumberr forKey:@"versionNumber"];
    [matchesWorkOrder setValue:DeviceVersion forKey:@"deviceVersion"];
    [matchesWorkOrder setValue:DeviceName forKey:@"deviceName"];
    [matchesWorkOrder setValue:strServiceAddImage forKey:@"serviceAddressImagePath"];
    
    //Update POC Details
    
    NSDictionary *dictDataServicePOC,*dictDataBillingPOC;
    
    for (int k=0; k<arrAllObjMechanicalServiceAddressPOCDetailDcs.count; k++) {
        
        NSDictionary *dictData=arrAllObjMechanicalServiceAddressPOCDetailDcs[k];
        
        NSString *strContactID=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        if ([strContactID isEqualToString:strServicePOCId]) {
            
            dictDataServicePOC=dictData;
            break;
            
        }
        
    }
    
    for (int k=0; k<arrAllObjMechanicalBillingAddressPOCDetailDcs.count; k++) {
        
        NSDictionary *dictData=arrAllObjMechanicalBillingAddressPOCDetailDcs[k];
        
        NSString *strContactID=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        if ([strContactID isEqualToString:strBillingPOCId]) {
            
            dictDataBillingPOC=dictData;
            break;
            
        }
        
    }
    
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"FirstName"] forKey:@"serviceFirstName"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"MiddleName"] forKey:@"serviceMiddleName"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"LastName"] forKey:@"serviceLastName"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"PrimaryEmail"] forKey:@"servicePrimaryEmail"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"SecondaryEmail"] forKey:@"serviceSecondaryEmail"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"PrimaryPhone"] forKey:@"servicePrimaryPhone"];
    [matchesWorkOrder setValue:[dictDataServicePOC valueForKey:@"SecondaryPhone"] forKey:@"serviceSecondaryPhone"];
    
    
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"FirstName"] forKey:@"billingFirstName"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"MiddleName"] forKey:@"billingMiddleName"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"LastName"] forKey:@"billingLastName"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"PrimaryEmail"] forKey:@"billingPrimaryEmail"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"SecondaryEmail"] forKey:@"billingSecondaryEmail"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"PrimaryPhone"] forKey:@"billingPrimaryPhone"];
    [matchesWorkOrder setValue:[dictDataBillingPOC valueForKey:@"SecondaryPhone"] forKey:@"billingSecondaryPhone"];
    
    NSError *error;
    [context save:&error];
    
    
    if (!isCompletedStatusMechanical) {
        
        [self saveServiceAndBillingAddressToEmailDB];
        
    }
    
}

-(void)fetchImageDetailFromDataBaseMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetail.count; j++) {
            
            matchesImageDetail=arrAllObjImageDetail[j];
            NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"Before"]) {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,nil];
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrOfBeforeImageAll addObject:dict_ToSendLeadInfo];
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
            }
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        
    }else if (arrOfImagesDetail.count==0){
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionView addObjectsFromArray:arrOfImagess];
        
        [self downloadImages:arrOfImagess];
        
    }
}

-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation1=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation1,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImage:str:k];
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload1: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
        }
    }
    [_beforeImageCollectionView reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    if (indexx==arrOfImagenameCollewctionView.count-1) {
        
        [_beforeImageCollectionView reloadData];
        
    }
    [_beforeImageCollectionView reloadData];
}

//============================================================================
//============================================================================
#pragma mark -----------------------Load Image-------------------------------
//============================================================================
//============================================================================

- (UIImage*)loadImage :(NSString*)name :(int)indexxx {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    return image;
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
    [self deleteBeforeImagesBeforeSaving];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    for (int k=0; k<arrOfBeforeImageAll.count; k++)
    {
        
        if ([arrOfBeforeImageAll[k] isKindOfClass:[NSString class]]) {
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfBeforeImageAll objectAtIndex:k]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            
            NSError *error1;
            [context save:&error1];
            
        }else{
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            NSDictionary *dictData=[arrOfBeforeImageAll objectAtIndex:k];
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"Before";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    //........................................................................
}

-(void)deleteBeforeImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"Before"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)fetchSubWorkOrderFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        
        
        
    }
    else
    {
        
        arrOfSubWorkOrder=nil;
        arrOfSubWorkOrder=[[NSMutableArray alloc]init];
        
        for (int j=0; j<arrAllObjMechanicalSubWorkOrder.count; j++) {
            
            matchesMechanicalSubWorkOrder=arrAllObjMechanicalSubWorkOrder[j];
            
            NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrder valueForKey:@"isActive"]];
            
            if ([strIsActive caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsActive isEqualToString:@"1"]) {
                
                //[arrOfSubWorkOrder addObject:matchesMechanicalSubWorkOrder];
                [arrOfSubWorkOrder addObject:arrAllObjMechanicalSubWorkOrder[j]];
            }
            
            if (j==indexSelectedSubWorkOrder) {
                
                //subWOType
                //Temporary hai comment krna hai y wala code
                strWoType=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrder valueForKey:@"subWOType"]];
                
                NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrder valueForKey:@"subWOStatus"]];
                
                strSubWorkOrderStatuss=strStatusSubWorkOrder;
                if ([strSubWorkOrderStatuss isEqualToString:@"Not Started"]) {
                    
                    strSubWorkOrderStatuss=@"New";
                    
                }
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:strSubWorkOrderStatuss forKey:@"WoStatus"];
                strSubWoAcNo=[NSString stringWithFormat:@"Sub Workorder #: %@",[matchesMechanicalSubWorkOrder valueForKey:@"subWorkOrderNo"]];
                [defs synchronize];
                
                NSManagedObject *objSubWorkOrderCell=arrAllObjMechanicalSubWorkOrder[indexSelectedSubWorkOrder];
                objSubWorkOrderToSend=objSubWorkOrderCell;
                strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWorkOrderId"]];
                
            }
            
            //strWoType=@"TM";
            
        }
        
        [self AdjustSubWorkOrderInfoView];
        
        [_tblViewSUbWorkOrder reloadData];
        
        NSLog(@"Sub---Work Order Detail on Mechanical General infor view====%@",arrOfSubWorkOrder);
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)AdjustSubWorkOrderInfoView{
    
    //Adding Sub-WorkOrderInfoView
    
    CGRect frameFor_view_SubWorkOrderInfo=CGRectMake(0, _view_WorkOrderInfo.frame.origin.y+_view_WorkOrderInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkOrder.count*572+70);
    [_view_SubWorkOredr setFrame:frameFor_view_SubWorkOrderInfo];
    // [_scrollViewGeneralInfo addSubview:_view_SubWorkOredr];
    
    _tblViewSUbWorkOrder.frame=CGRectMake(_tblViewSUbWorkOrder.frame.origin.x, _tblViewSUbWorkOrder.frame.origin.y, _tblViewSUbWorkOrder.frame.size.width, arrOfSubWorkOrder.count*572+70);
    
    //Adding AddressInfoView
    
    CGRect frameFor_view_AddressInfo=CGRectMake(0, _view_SubWorkOredr.frame.origin.y+_view_SubWorkOredr.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_AddressInfo.frame.size.height);
    [_view_AddressInfo setFrame:frameFor_view_AddressInfo];
    // [_scrollViewGeneralInfo addSubview:_view_AddressInfo];
    
    
    //Adding OtherInfoView
    
    CGRect frameFor_view_OtherInfo=CGRectMake(0, _view_AddressInfo.frame.origin.y+_view_AddressInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_OtherInfo.frame.size.height);
    [_view_OtherInfo setFrame:frameFor_view_OtherInfo];
    // [_scrollViewGeneralInfo addSubview:_view_OtherInfo];
    
    /*
     //Adding BeforeImageInfoView
     
     CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _view_OtherInfo.frame.origin.y+_view_OtherInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_BeforeImage.frame.size.height);
     [_view_BeforeImage setFrame:frameFor_view_BeforeImageInfo];
     // [_scrollViewGeneralInfo addSubview:_view_BeforeImage];
     
     
     //Adding SaveContinueInfoView
     
     CGRect frameFor_view_SaveContinueInfo=CGRectMake(0, _view_BeforeImage.frame.origin.y+_view_BeforeImage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_SavenContinue.frame.size.height);
     [_view_SavenContinue setFrame:frameFor_view_SaveContinueInfo];
     // [_scrollViewGeneralInfo addSubview:_view_SavenContinue];
     */
    
    // [_scrollViewGeneralInfo setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_SavenContinue.frame.size.height+_view_SavenContinue.frame.origin.y)];
    
    [_scrollViewGeneralInfo setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_view_OtherInfo.frame.size.height+_view_OtherInfo.frame.origin.y)];
    
}

-(void)upDateSubWorkOrderFromDB :(NSString*)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdToFetch];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        
        
        
    }
    else
    {
        
        matchesMechanicalSubWorkOrder=arrAllObjMechanicalSubWorkOrder[0];
        [matchesMechanicalSubWorkOrder setValue:@"false" forKey:@"isActive"];
        NSError *error1;
        [context save:&error1];
        
        [self fetchSubWorkOrderFromDB];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)upDateSubWorkOrderStatusFromDB :(NSString*)strSubWorkOrderIdToFetch :(NSString*)strStatusSubWorkOrder{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdToFetch];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        
        
        
    }
    else
    {
        
        matchesMechanicalSubWorkOrder=arrAllObjMechanicalSubWorkOrder[0];
        [matchesMechanicalSubWorkOrder setValue:strStatusSubWorkOrder forKey:@"subWOStatus"];
        [matchesMechanicalSubWorkOrder setValue:@"Start" forKey:@"clockStatus"];
        
        NSError *error1;
        [context save:&error1];
        
        [self fetchSubWorkOrderFromDB];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}
-(void)upDateSubWorkOrderDispatchTimeFromDB :(NSString*)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdToFetch];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        
        
        
    }
    else
    {
        
        matchesMechanicalSubWorkOrder=arrAllObjMechanicalSubWorkOrder[0];
        [matchesMechanicalSubWorkOrder setValue:[global strCurrentDateFormattedForMechanical] forKey:@"dispatchTime"];
        NSError *error1;
        [context save:&error1];
        
        [self fetchSubWorkOrderFromDB];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)addStartActualHoursFromMobileToDB :(NSDictionary*)dict{
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWorkOrderId;
    objSubWorkOrderNotes.subWOActualHourId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SubWOActualHourId"]];
    
    objSubWorkOrderNotes.subWorkOrderId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SubWorkOrderId"]];
    objSubWorkOrderNotes.timeIn=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeIn"]];
    objSubWorkOrderNotes.timeOut=@"";
    objSubWorkOrderNotes.mobileTimeIn=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MobileTimeIn"]];
    objSubWorkOrderNotes.mobileTimeOut=@"";
    objSubWorkOrderNotes.reason=@"";
    objSubWorkOrderNotes.actHrsDescription=@"";
    objSubWorkOrderNotes.subWorkOrderNo=@"";
    objSubWorkOrderNotes.subWOActualHourNo=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKey;
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CreatedBy"]];
    objSubWorkOrderNotes.createdDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CreatedDate"]];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ModifiedBy"]];
    objSubWorkOrderNotes.modifiedDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ModifiedDate"]];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CreatedByDevice"]];
    objSubWorkOrderNotes.status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Status"]];
    
    // Latitude changes
    //    CLLocationCoordinate2D coordinate = [global getLocation] ;
    //    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    //    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objSubWorkOrderNotes.latitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Latitude"]];;
    objSubWorkOrderNotes.longitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Longitude"]];;
    // Latitude changes end
    
    objSubWorkOrderNotes.employeeNo=[global getEmployeeDeatils];
    
    
    NSError *error2;
    [context save:&error2];
    
}

-(void)addActualHrsIfResponseIsNil{
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWorkOrderId;
    
    NSString *strNos=[global getReferenceNumber];
    objSubWorkOrderNotes.subWOActualHourId=strNos;
    objSubWorkOrderNotes.subWorkOrderId=strGlobalSubWorkOrderToSend;
    objSubWorkOrderNotes.timeIn=[global strCurrentDateFormattedForMechanical];
    objSubWorkOrderNotes.timeOut=@"";
    objSubWorkOrderNotes.mobileTimeIn=objSubWorkOrderNotes.timeIn;
    objSubWorkOrderNotes.mobileTimeOut=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKey;
    objSubWorkOrderNotes.reason=@"";
    objSubWorkOrderNotes.actHrsDescription=@"";
    objSubWorkOrderNotes.subWorkOrderNo=@"";
    objSubWorkOrderNotes.subWOActualHourNo=@"";
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.createdDate=[global strCurrentDate];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.modifiedDate=[global strCurrentDate];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    objSubWorkOrderNotes.status=@"OnRoute";
    
    // Latitude changes
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objSubWorkOrderNotes.latitude=latitude;
    objSubWorkOrderNotes.longitude=longitude;
    // Latitude changes end
    
    objSubWorkOrderNotes.employeeNo=[global getEmployeeDeatils];
    
    NSError *error2;
    [context save:&error2];
    
}

-(void)metodUpdateSubWorkOrderStatus :(NSString*)strStatusToSent{
    
    NSString *strStatusToSend;
    
    if ([strStatusToSent isEqualToString:@"New"]){
        //New Matlb Dispatch Ka Response
        strStatusToSend=@"Dispatch";
        
    }else if ([strStatusToSent isEqualToString:@"Dispatch"]){
        //Dispatch Matlb OnRoute Ka Response
        strStatusToSend=@"OnRoute";
    }
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&subWorkOrderId=%@&userName=%@&status=%@",strServiceUrlMainServiceAutomation,UrlMechanicalSubWorkOrderStatusUpdate,strCompanyKey,strGlobalSubWorkOrderToSend,strUserName,strStatusToSend];
    
    NSString *strMobileTimeIn=[global strCurrentDateFormattedForMechanical];;
    
    strUrl = [NSString stringWithFormat:@"%@%@%@&subWorkOrderId=%@&userName=%@&status=%@&%@%@",strServiceUrlMainServiceAutomation,UrlMechanicalSubWorkOrderStatusUpdate,strCompanyKey,strGlobalSubWorkOrderToSend,strUserName,strStatusToSend,UrlMechanicalSubWorkOrderTimeIn,strMobileTimeIn];
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    strUrl = [NSString stringWithFormat:@"%@%@%@&subWorkOrderId=%@&userName=%@&status=%@&%@%@&Latitude=%@&Longitude=%@",strServiceUrlMainServiceAutomation,UrlMechanicalSubWorkOrderStatusUpdate,strCompanyKey,strGlobalSubWorkOrderToSend,strUserName,strStatusToSend,UrlMechanicalSubWorkOrderTimeIn,strMobileTimeIn,latitude,longitude];
    
    NSLog(@"Mechanical Work Order Status Update URl-----%@",strUrl);
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Status..."];
    
    //============================================================================
    //============================================================================
    
    NSString *strType;
    if ([strStatusToSent isEqualToString:@"New"]){
        //New Matlb Dispatch Ka Response
        strType=@"MechanicalSubWorkOrderStatusNew";
        
    }else{
        //Dispatch Matlb OnRoute Ka Response
        strType=@"MechanicalSubWorkOrderStatusDispatch";
        
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [DejalBezelActivityView removeView];
                 
                 if (success)
                 {
                     NSLog(@"Response On SubWorkOrder Satus Update===%@",response);
                     if ([strStatusToSent isEqualToString:@"New"]){
                         //New Matlb Dispatch Ka Response
                         
                         
                     }else if ([strStatusToSent isEqualToString:@"Dispatch"]){
                         //Dispatch Matlb OnRoute Ka Response
                         
                         if (response==nil) {
                             
                             [self addActualHrsIfResponseIsNil];
                             
                         } else {
                             
                             [self addStartActualHoursFromMobileToDB :response];
                             
                         }
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    //============================================================================
    //============================================================================
    
}


//============================================================================
#pragma mark- ------------Email Methods------------------
//============================================================================

-(void)deleteEmailIdFromCoreData{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,strEmailIdGlobal];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)deleteEmailIdFromCoreDataNew :(NSString*)strEmailIdToDelete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,strEmailIdToDelete];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

-(void)deleteEmailIdFromCoreDataSecondary{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,strEmailIdGlobalSecondary];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    
}

-(void)saveEmailIdPrimaryToCoreData
{
    
    for (int k=0; k<arrOfPrimaryEmailAlreadySaved.count; k++) {
        
        [self deleteEmailIdFromCoreDataNew:arrOfPrimaryEmailAlreadySaved[k]];
        
    }
    
    for (int k=0; k<arrOfPrimaryEmailNew.count; k++) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        entityEmailDetailService= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        [request setEntity:entityEmailDetailService];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,arrOfPrimaryEmailNew[k]];
        
        [request setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [request setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerServiceEmail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerServiceEmail setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerServiceEmail performFetch:&error1];
        arrAllObj = [self.fetchedResultsControllerServiceEmail fetchedObjects];
        
        if (arrAllObj.count==0)
        {
            [self saveToCoreDataNewEmail :arrOfPrimaryEmailNew[k]];
            // [self saveToCoreDataNewEmailSecondary];
            
        }else
        {
            
            BOOL notMatched,notMatchedSecondaryEmail;
            
            notMatched=YES;
            notMatchedSecondaryEmail=YES;
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                NSString *strEmailToCompare=[matches valueForKey:@"emailId"];
                
                if ([strEmailToCompare isEqualToString:arrOfPrimaryEmailNew[k]]) {
                    
                    notMatched=NO;
                    
                    [matches setValue:arrOfPrimaryEmailNew[k] forKey:@"emailId"];
                    NSError *error1;
                    [context save:&error1];
                    
                }
            }
            
            if (notMatched) {
                
                [self saveToCoreDataNewEmail:arrOfPrimaryEmailNew[k]];
                
            }
        }
    }
    
    [self saveEmailIdSecondaryToCoreData];
    
}

-(void)saveEmailIdSecondaryToCoreData
{
    
    for (int k=0; k<arrOfSecondaryEmailAlreadySaved.count; k++) {
        
        [self deleteEmailIdFromCoreDataNew:arrOfSecondaryEmailAlreadySaved[k]];
        
    }
    
    for (int k=0; k<arrOfSecondaryEmailNew.count; k++) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        entityEmailDetailService= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        [request setEntity:entityEmailDetailService];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@ AND emailId=%@",strWorkOrderId,arrOfSecondaryEmailNew[k]];
        
        [request setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [request setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerServiceEmail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerServiceEmail setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerServiceEmail performFetch:&error1];
        arrAllObj = [self.fetchedResultsControllerServiceEmail fetchedObjects];
        
        if (arrAllObj.count==0)
        {
            [self saveToCoreDataNewEmail :arrOfSecondaryEmailNew[k]];
            // [self saveToCoreDataNewEmailSecondary];
            
        }else
        {
            
            BOOL notMatched,notMatchedSecondaryEmail;
            
            notMatched=YES;
            notMatchedSecondaryEmail=YES;
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                NSString *strEmailToCompare=[matches valueForKey:@"emailId"];
                
                if ([strEmailToCompare isEqualToString:arrOfSecondaryEmailNew[k]]) {
                    
                    notMatched=NO;
                    
                    [matches setValue:arrOfSecondaryEmailNew[k] forKey:@"emailId"];
                    NSError *error1;
                    [context save:&error1];
                    
                }
            }
            
            if (notMatched) {
                
                [self saveToCoreDataNewEmail:arrOfSecondaryEmailNew[k]];
                
            }
        }
    }
}

-(void)saveToCoreDataNewEmail :(NSString*)strEmailTosave{
    
    if (strEmailTosave.length==0) {
        
    } else {
        
        entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        
        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailService insertIntoManagedObjectContext:context];
        
        objEmailDetail.createdBy=@"0";;
        objEmailDetail.createdDate=@"0";
        objEmailDetail.emailId=strEmailTosave;
        objEmailDetail.isCustomerEmail=@"true";
        objEmailDetail.isMailSent=@"true";
        objEmailDetail.companyKey=strCompanyKey;
        objEmailDetail.modifiedBy=@"";
        objEmailDetail.modifiedDate=[global modifyDate];
        objEmailDetail.subject=@"";
        objEmailDetail.userName=strUserName;
        objEmailDetail.workorderId=strWorkOrderId;
        objEmailDetail.woInvoiceMailId=@"";
        NSError *error1;
        [context save:&error1];
        
    }
}

-(void)saveToCoreDataEmailOfEmployee{
    entityEmailDetailService=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    
    EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailService insertIntoManagedObjectContext:context];
    
    objEmailDetail.createdBy=@"0";;
    objEmailDetail.createdDate=@"0";
    objEmailDetail.emailId=strFromEmail;
    objEmailDetail.isCustomerEmail=@"true";
    objEmailDetail.isMailSent=@"true";
    objEmailDetail.companyKey=strCompanyKey;
    objEmailDetail.modifiedBy=@"";
    objEmailDetail.modifiedDate=[global modifyDate];
    objEmailDetail.subject=@"";
    objEmailDetail.userName=strUserName;
    objEmailDetail.workorderId=strWorkOrderId;
    objEmailDetail.woInvoiceMailId=@"";
    NSError *error1;
    [context save:&error1];
}
#pragma mark- ------------ CODE FOR SERVICE ADD IMAGE ----------------



- (IBAction)actionOnUploadServiceAddImage:(id)sender
{
    
    _btnUploadServiceAddImage.tag=101;
    
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              if (isCompletedStatusMechanical) {
                                  
                                  
                              }
                              else
                              {
                                  
                                  NSLog(@"The CApture Image.");
                                  
                                  NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                  
                                  BOOL isfirstTimeAudio=[defs boolForKey:@"firstCamera"];
                                  
                                  if (isfirstTimeAudio) {
                                      
                                      [defs setBool:NO forKey:@"firstCamera"];
                                      [defs synchronize];
                                      
                                      UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                      imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                      imagePickController.delegate=(id)self;
                                      imagePickController.allowsEditing=TRUE;
                                      [self presentViewController:imagePickController animated:YES completion:nil];
                                      
                                  }
                                  else
                                  {
                                      BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                      
                                      if (isCameraPermissionAvailable) {
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                          
                                      }else{
                                          
                                          UIAlertController *alert= [UIAlertController
                                                                     alertControllerWithTitle:@"Alert"
                                                                     message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                          
                                          UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                      handler:^(UIAlertAction * action)
                                                                {
                                                                    
                                                                    
                                                                    
                                                                }];
                                          [alert addAction:yes];
                                          UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                     handler:^(UIAlertAction * action)
                                                               {
                                                                   
                                                                   if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                       NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                       [[UIApplication sharedApplication] openURL:url];
                                                                   } else {
                                                                       
                                                                       //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                       //                                     [alert show];
                                                                       
                                                                   }
                                                                   
                                                               }];
                                          [alert addAction:no];
                                          [self presentViewController:alert animated:YES completion:nil];
                                      }
                                  }
                                  
                              }
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if (isCompletedStatusMechanical) {
                                 
                                 
                             }
                             else
                             {
                                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                 
                                 BOOL isfirstTimeAudio=[defs boolForKey:@"firstGallery"];
                                 
                                 if (isfirstTimeAudio) {
                                     
                                     [defs setBool:NO forKey:@"firstGallery"];
                                     [defs synchronize];
                                     
                                     UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                     imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                     imagePickController.delegate=(id)self;
                                     imagePickController.allowsEditing=TRUE;
                                     [self presentViewController:imagePickController animated:YES completion:nil];
                                     
                                 }else{
                                     BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                     
                                     if (isCameraPermissionAvailable) {
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                         
                                     }else{
                                         
                                         UIAlertController *alert= [UIAlertController
                                                                    alertControllerWithTitle:@"Alert"
                                                                    message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                         
                                         UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                     handler:^(UIAlertAction * action)
                                                               {
                                                                   
                                                                   
                                                                   
                                                               }];
                                         [alert addAction:yes];
                                         UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                    handler:^(UIAlertAction * action)
                                                              {
                                                                  
                                                                  if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                      NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                      [[UIApplication sharedApplication] openURL:url];
                                                                  } else {
                                                                      
                                                                      //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                      //                                     [alert show];
                                                                      
                                                                  }
                                                                  
                                                              }];
                                         [alert addAction:no];
                                         [self presentViewController:alert animated:YES completion:nil];
                                     }
                                 }
                                 
                             }
                             
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)downloadImageServiceAddImage :(NSString*)str
{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSUserDefaults *defsAddImage=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginDataAddImage=[defsAddImage valueForKey:@"LoginDetails"];
    NSString *strServiceAddImageUrl=[dictLoginDataAddImage valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
    NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/%@",strServiceAddImageUrl,str];
    NSLog(@"Service Add Image URL %@",strUrl);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageServiceAdd:str];
    } else {
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownloadServiceAdd: image : result];
                
            });
        });
    }
    
}
-(void)ShowFirstImageServiceAdd :(NSString*)str
{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImageServiceAdd : result];
}
- (void)saveImageAfterDownloadServiceAdd: (UIImage*)image :(NSString*)name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    [self ShowFirstImageServiceAdd : name];
    
}
- (UIImage*)loadImageServiceAdd :(NSString*)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (image==nil) {
        
        _imgViewServiceAddress.image = [UIImage imageNamed:@"NoImage.jpg"];
        
    } else {
        
        _imgViewServiceAddress.image=image;
    }
    return image;
}

-(BOOL)IsDifferentEmp :(NSString*)strEmpNoSub{
    
    BOOL yesDifferent;
    yesDifferent=YES;
    
    if ([strEmpNoSub isEqualToString:strEmpNoLoggedIn]) {
        
        yesDifferent=NO;
        
    } else {
        
        yesDifferent=YES;
        
    }
    
    return yesDifferent;
    
}

#pragma mark- Service Job Description Method

- (IBAction)action_ServiceJobDescriptionTitle:(id)sender {
    
    
    if (arrPickerServiceJobDescriptions.count==0) {
        
        [global AlertMethod:Alert :NoDataAvailableServiceJobDescriptions];
        
    }else{
        
        tblData.tag=107;
        [self tableLoad:107];
        
    }
}

-(void)fetchServiceJobDescriptionsFromMaster{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    if ([dictdata isKindOfClass:[NSString class]]) {
        
        
    }else{
        
        arrPickerServiceJobDescriptions=nil;
        arrPickerServiceJobDescriptions=[[NSMutableArray alloc]init];
        
        NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceJobDescriptionTemplate"];
        if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
            
            NSDictionary *dictTemp=@{@"CompanyId":@"",@"IsActive":@"",@"ServiceJobDescription":@"",@"ServiceJobDescriptionId":@"",@"Title":@"Select"};
            
            [arrPickerServiceJobDescriptions addObject:dictTemp];
            
            for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                
                NSDictionary *dictData=[arrOfServiceJobDescriptionTemplate objectAtIndex:k];
                
                BOOL isActiveeee=[[dictData valueForKey:@"IsActive"] boolValue];
                
                if (isActiveeee) {
                    
                    [arrPickerServiceJobDescriptions addObject:dictData];
                    
                }
            }
            
        }else{
            
            
        }
    }
}
-(void)updateServiceJobDescription{
    
    [matchesWorkOrder setValue:strGlobalServiceJobDescriptionId forKey:@"serviceJobDescriptionId"];
    // [matchesWorkOrder setValue:[self getHTML] forKey:@"serviceJobDescription"];
    [matchesWorkOrder setValue:_textView_ServiceJobDescriptions.text forKey:@"serviceJobDescription"];
    
    NSError *error;
    [context save:&error];
    
}

- (IBAction)action_BillingArrow:(id)sender {
    
    if ([_btnBillingArrow.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnBillingArrow setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        _constant_BillingHeight.constant=820;
        
    } else {
        
        [_btnBillingArrow setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        _constant_BillingHeight.constant=0;
    }
    [self addViewOnLoadingAgain];
}
- (IBAction)action_ServiceArrow:(id)sender {
    
    if ([_btnServiceArrow.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnServiceArrow setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        _constant_serviceHeight.constant=843;
        
    } else {
        
        [_btnServiceArrow setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        _constant_serviceHeight.constant=0;
        
    }
    
    [self addViewOnLoadingAgain];
}

-(void)addViewOnLoadingAgain{
    
    CGRect frameFor_view_AddressInfo=CGRectMake(0, _view_SubWorkOredr.frame.origin.y+_view_SubWorkOredr.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_AddressInfo.frame.size.height+_constant_BillingHeight.constant+_constant_serviceHeight.constant);
    [_view_AddressInfo setFrame:frameFor_view_AddressInfo];
    // [_scrollViewGeneralInfo addSubview:_view_AddressInfo];
    
    //Adding OtherInfoView
    
    CGRect frameFor_view_OtherInfo=CGRectMake(0, _view_AddressInfo.frame.origin.y+_view_AddressInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_OtherInfo.frame.size.height);
    [_view_OtherInfo setFrame:frameFor_view_OtherInfo];
    // [_scrollViewGeneralInfo addSubview:_view_OtherInfo];
    
    //Adding Service Job Description
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
    
    CGFloat heightServiceJobDesc=0.0;
    
    if (isNoServiceJobDescriptions) {
        
        heightServiceJobDesc=0.0;
        [_viewServiceJobDescriptions setHidden:YES];
        
    }else{
        
        heightServiceJobDesc=_viewServiceJobDescriptions.frame.size.height;
        [_viewServiceJobDescriptions setHidden:NO];
    }
    
    CGRect frameFor_view_JobDescription=CGRectMake(0, _view_OtherInfo.frame.origin.y+_view_OtherInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,heightServiceJobDesc);
    [_viewServiceJobDescriptions setFrame:frameFor_view_JobDescription];
    // [_scrollViewGeneralInfo addSubview:_viewServiceJobDescriptions];
    
    [_scrollViewGeneralInfo setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_viewServiceJobDescriptions.frame.size.height+_viewServiceJobDescriptions.frame.origin.y)];
    
}

- (IBAction)action_btnExpandCustomerInfo:(id)sender {
    
    
    if ([_btnExpandCustomerInfo.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnExpandCustomerInfo setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        _constant_CustomerInfo_H.constant=430+70;
        
    } else {
        
        [_btnExpandCustomerInfo setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        _constant_CustomerInfo_H.constant=70;
        
        isEditCustomerInfo=YES;
        [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"edit_iPad.png"] forState:UIControlStateNormal];
        
    }
    
    strTypeExpandCollapse=@"exp";
    [self addViewOnLandingNew:@"exp"];
    
}

-(void)addViewOnLandingNew :(NSString*)strType{
    
    strType=strTypeExpandCollapse;
    
    //Adding CustomerInfoView
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,_constant_CustomerInfo_H.constant);
    [_view_CustomerInfo setFrame:frameFor_view_CustomerInfo];
    
    if ([strType isEqualToString:@"add"]) {
        
        [_scrollViewGeneralInfo addSubview:_view_CustomerInfo];
        
    }
    
    //Adding WorkOrderInfoView
    
    CGRect frameFor_view_WorkOrderInfo=CGRectMake(0, _view_CustomerInfo.frame.origin.y+_constant_CustomerInfo_H.constant+10, [UIScreen mainScreen].bounds.size.width,_constant_WorkOrderInfo_H.constant);
    [_view_WorkOrderInfo setFrame:frameFor_view_WorkOrderInfo];
    
    if ([strType isEqualToString:@"add"]) {
        
        [_scrollViewGeneralInfo addSubview:_view_WorkOrderInfo];
        
    }
    
    //Adding Sub-WorkOrderInfoView
    
    CGRect frameFor_view_SubWorkOrderInfo=CGRectMake(0, _view_WorkOrderInfo.frame.origin.y+_view_WorkOrderInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,arrOfSubWorkOrder.count*572+70);
    
    [_view_SubWorkOredr setFrame:frameFor_view_SubWorkOrderInfo];
    
    _tblViewSUbWorkOrder.frame=CGRectMake(_tblViewSUbWorkOrder.frame.origin.x, _tblViewSUbWorkOrder.frame.origin.y, _tblViewSUbWorkOrder.frame.size.width, arrOfSubWorkOrder.count*572);
    
    
    if ([strType isEqualToString:@"add"]) {
        
        [_scrollViewGeneralInfo addSubview:_view_SubWorkOredr];
        
    }
    
    
    CGRect frameFor_view_ServiceAddressInfo=CGRectMake(0, _view_SubWorkOredr.frame.origin.y+_view_SubWorkOredr.frame.size.height+60, [UIScreen mainScreen].bounds.size.width,_constant_ServiceAddres_H.constant+70);
    [_view_ServiceAddress setFrame:frameFor_view_ServiceAddressInfo];
    
    if ([strType isEqualToString:@"add"]) {
        
        [_scrollViewGeneralInfo addSubview:_view_ServiceAddress];
        
    }
    
    CGRect frameFor_view_AddressInfo=CGRectMake(0, _view_ServiceAddress.frame.origin.y+_view_ServiceAddress.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_constant_BillingAddress_H.constant+70);
    [_view_AddressInfo setFrame:frameFor_view_AddressInfo];
    
    if ([strType isEqualToString:@"add"]) {
        
        [_scrollViewGeneralInfo addSubview:_view_AddressInfo];
        
    }
    
    /*
     CGRect frameFor_view_AddressInfo=CGRectMake(0, _view_SubWorkOredr.frame.origin.y+_view_SubWorkOredr.frame.size.height+60, [UIScreen mainScreen].bounds.size.width,_constant_BillingAddress_H.constant+160);
     [_view_AddressInfo setFrame:frameFor_view_AddressInfo];
     
     if ([strType isEqualToString:@"add"]) {
     
     [_scrollViewGeneralInfo addSubview:_view_AddressInfo];
     
     }
     
     CGRect frameFor_view_ServiceAddressInfo=CGRectMake(0, _view_SubWorkOredr.frame.origin.y+_view_SubWorkOredr.frame.size.height+_constant_BillingAddress_H.constant+220, [UIScreen mainScreen].bounds.size.width,_constant_ServiceAddres_H.constant+70);
     [_view_ServiceAddress setFrame:frameFor_view_ServiceAddressInfo];
     
     if ([strType isEqualToString:@"add"]) {
     
     [_scrollViewGeneralInfo addSubview:_view_ServiceAddress];
     
     }
     */
    
    //Adding OtherInfoView
    
    CGRect frameFor_view_OtherInfo=CGRectMake(0, _view_AddressInfo.frame.origin.y+_view_AddressInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,_view_OtherInfo.frame.size.height);
    [_view_OtherInfo setFrame:frameFor_view_OtherInfo];
    
    if ([strType isEqualToString:@"add"]) {
        
        [_scrollViewGeneralInfo addSubview:_view_OtherInfo];
        
    }
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
    
    CGFloat heightServiceJobDesc=0.0;
    
    if (isNoServiceJobDescriptions) {
        
        heightServiceJobDesc=0.0;
        [_viewServiceJobDescriptions setHidden:YES];
        
    }else{
        
        heightServiceJobDesc=_viewServiceJobDescriptions.frame.size.height;
        [_viewServiceJobDescriptions setHidden:NO];
    }
    
    //Adding Service Job Description
    
    CGRect frameFor_view_JobDescription=CGRectMake(0, _view_OtherInfo.frame.origin.y+_view_OtherInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width,heightServiceJobDesc);
    [_viewServiceJobDescriptions setFrame:frameFor_view_JobDescription];
    
    if ([strType isEqualToString:@"add"]) {
        
        [_scrollViewGeneralInfo addSubview:_viewServiceJobDescriptions];
        
    }
    
    [_scrollViewGeneralInfo setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width,_viewServiceJobDescriptions.frame.size.height+_viewServiceJobDescriptions.frame.origin.y)];
    
}
- (IBAction)action_btnExpandWorkOrderInfo:(id)sender {
    
    if ([_btnExpandWorkOrderInfo.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnExpandWorkOrderInfo setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        _constant_WorkOrderInfo_H.constant=856+70;
        
    } else {
        
        [_btnExpandWorkOrderInfo setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        _constant_WorkOrderInfo_H.constant=70;
        
    }
    strTypeExpandCollapse=@"exp";
    [self addViewOnLandingNew:@"exp"];
    
}


- (IBAction)action_btnExpandSubWorkOrderInf:(id)sender {
    
    if ([_btnExpandSubWorkOrderInf.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnExpandSubWorkOrderInf setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        //_constant_WorkOrderInfo_H.constant=680;
        [self addViewOnLandingNew:@"addWo"];
        
    } else {
        
        [_btnExpandSubWorkOrderInf setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        _constant_tblViewSubWorkorder_H.constant=0;
        
        [self addViewOnLandingNew:@"exp"];
        
    }
    
}


- (IBAction)action_btnExpanBillingAddress:(id)sender {
    
    if ([_btnExpanBillingAddress.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnExpanBillingAddress setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        _constant_BillingAddress_H.constant=820+70;
        
    } else {
        
        [_btnExpanBillingAddress setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        _constant_BillingAddress_H.constant=0;
        
    }
    strTypeExpandCollapse=@"exp";
    [self addViewOnLandingNew:@"exp"];
    
}
- (IBAction)action_btnExpandServiceAddress:(id)sender {
    
    if ([_btnExpandServiceAddress.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [_btnExpandServiceAddress setImage:[UIImage imageNamed:@"show_iPad.png"] forState:UIControlStateNormal];
        _constant_ServiceAddres_H.constant=863+70;
        
    } else {
        
        [_btnExpandServiceAddress setImage:[UIImage imageNamed:@"hide_iPad.png"] forState:UIControlStateNormal];
        _constant_ServiceAddres_H.constant=0;
        
    }
    strTypeExpandCollapse=@"exp";
    [self addViewOnLandingNew:@"exp"];
    
}

- (IBAction)action_BillingContact:(id)sender {
    
    
}

- (IBAction)action_BillingCell:(id)sender {
    
    [global calling:_billingContactCellNo.text];
    
}

- (IBAction)action_BillingPrimaryEmail:(id)sender {
    
    [global emailComposer:_billingContactPrimaryEmail.text :@"" :@"" :self];
    
}

- (IBAction)action_BillingSecondaryEmail:(id)sender {
    
    [global emailComposer:_billingContactSecondayEmail.text :@"" :@"" :self];
    
}

- (IBAction)action_BillingPrimaryPhone:(id)sender {
    
    [global calling:_billingContactPrimaryPhone.text];
}

- (IBAction)action_BillingSecondaryPhone:(id)sender {
    
    [global calling:_billingContactSecondaryPhone.text];
    
}

- (IBAction)action_BillingAddress:(id)sender {
    
    [global redirectOnAppleMap:self :_lblBillingAddress.text];
    
}

- (IBAction)action_ServiceContact:(id)sender {
    
    
    
}

- (IBAction)action_ServiceCell:(id)sender {
    
    [global calling:_serviceContactCellNo.text];
    
}

- (IBAction)action_ServicePrimaryEmail:(id)sender {
    
    [global emailComposer:_serviceContactPrimaryEmail.text :@"" :@"" :self];
    
}

- (IBAction)action_ServiceSecondaryEmail:(id)sender {
    
    [global emailComposer:_serviceContactSecondaryEmail.text :@"" :@"" :self];
    
}

- (IBAction)action_ServicePrimaryPhone:(id)sender {
    
    [global calling:_serviceContactPrimaryPhone.text];
    
}

- (IBAction)action_ServiceSecondaryPhone:(id)sender {
    
    [global calling:_serviceContactSecondaryPhone.text];
    
}

- (IBAction)action_ServiceAddress:(id)sender {
    
    [global redirectOnAppleMap:self :_lblServiceAddress.text];
    
}
- (IBAction)action_btnEditCustomerInfo:(id)sender {
    
    if ([_btnExpandCustomerInfo.currentImage isEqual:[UIImage imageNamed:@"hide_iPad.png"]]) {
        
        [global AlertMethod:Alert :@"Please open customer info view to edit"];
        
    } else {
        
        if ([_btnEditCustomerInfo.currentImage isEqual:[UIImage imageNamed:@"edit_iPad.png"]]) {
            
            [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"cancel-edit_iPad.png"] forState:UIControlStateNormal];
            
            isEditCustomerInfo=YES;
            
            [self setValuesOnEdit:@"Edit"];
            
        } else {
            
            [_btnEditCustomerInfo setImage:[UIImage imageNamed:@"edit_iPad.png"] forState:UIControlStateNormal];
            isEditCustomerInfo=NO;
            [self setValuesOnEdit:@"EditNot"];
            
        }
        
    }
}

- (IBAction)action_PrimaryPhone:(id)sender {
    
    [global calling:_txtPrimaryPhone.text];
    
}

- (IBAction)action_SecondaryPhone:(id)sender {
    
    [global calling:_txtSecondaryPhone.text];
    
}

-(void)setValuesOnEdit :(NSString*)strType{
    
    if ([strType isEqualToString:@"Edit"]) {
        
        self.tagsView.editable = YES;
        self.tagsView.selectable = YES;
        self.tagsView.allowsMultipleSelection = YES;
        
        _btnPrimaryPhone.hidden=YES;
        _btnSecondaryPhone.hidden=YES;
        
        _txtPrimaryPhone.text=_txtPrimaryPhone.text;
        _txtSecondaryPhone.text=_txtSecondaryPhone.text;
        _txtPrimaryPhone.textColor=[UIColor blackColor];
        _txtSecondaryPhone.textColor=[UIColor blackColor];
        
        _btnPrimaryEmail.titleLabel.textColor=[UIColor blackColor];
        _btnSecondaryEmail.titleLabel.textColor=[UIColor blackColor];
        [_btnPrimaryEmail setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btnSecondaryEmail setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btnPrimaryEmail setAttributedTitle:[global removeUnderLineAttributedString:_btnPrimaryEmail] forState:UIControlStateNormal];
        [_btnSecondaryEmail setAttributedTitle:[global removeUnderLineAttributedString:_btnSecondaryEmail] forState:UIControlStateNormal];
        
    } else {
        
        self.tagsView.editable = false;
        self.tagsView.selectable = false;
        self.tagsView.allowsMultipleSelection = false;
        
        _btnPrimaryPhone.hidden=NO;
        _btnSecondaryPhone.hidden=NO;
        
        _txtPrimaryPhone.textColor=[UIColor blueColor];
        _txtSecondaryPhone.textColor=[UIColor blueColor];
        _txtPrimaryPhone.attributedText=[global getUnderLineAttributedString:_txtPrimaryPhone.text];
        _txtSecondaryPhone.attributedText=[global getUnderLineAttributedString:_txtSecondaryPhone.text];
        
        _btnPrimaryEmail.titleLabel.textColor=[UIColor blueColor];
        _btnSecondaryEmail.titleLabel.textColor=[UIColor blueColor];
        [_btnPrimaryEmail setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_btnSecondaryEmail setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [_btnPrimaryEmail setAttributedTitle:[global getUnderLineAttributedString:_btnPrimaryEmail.titleLabel.text] forState:UIControlStateNormal];
        [_btnSecondaryEmail setAttributedTitle:[global getUnderLineAttributedString:_btnSecondaryEmail.titleLabel.text] forState:UIControlStateNormal];
        
    }
}


-(void)setValuesEditableOrNot{
    
    if (isCompletedStatusMechanical) {
        
        self.tagsView.editable = false;
        self.tagsView.selectable = false;
        self.tagsView.allowsMultipleSelection = false;
        _txtSecondaryPhone.enabled=false;
        _txtPrimaryPhone.enabled=false;
        _txtViewNotes.editable=false;
        _txtViewTechComment.editable=false;
        _btnClientApproval.enabled=false;
        _btnUploadServiceAddImage.enabled=false;
        _txtCustomerPO.enabled=NO;
        _textView_ServiceJobDescriptions.editable=NO;
        
        
    }else
    {
        
        _textView_ServiceJobDescriptions.editable=YES;
        _txtCustomerPO.enabled=YES;
        self.tagsView.editable = true;
        self.tagsView.selectable = true;
        self.tagsView.allowsMultipleSelection = true;
        _txtSecondaryPhone.enabled=true;
        _txtPrimaryPhone.enabled=true;
        _txtViewNotes.editable=true;
        _txtViewTechComment.editable=true;
        _btnClientApproval.enabled=false;
        _btnUploadServiceAddImage.enabled=true;
        
    }
}
- (IBAction)action_CurrentStatus:(id)sender {
    
    if (indexSelectedSubWorkOrder==-1) {
        
        
        
    } else {
        
        isClickedOnViewSubWorkOrder = NO;
        isOnlyChangeStatus = YES;

        yesEditedSomething=YES;
        
        NSManagedObject *dictDataSubWorkOrder=arrOfSubWorkOrder[indexSelectedSubWorkOrder];
        
        NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWOStatus"]];
        
        if ([strStatusSubWorkOrder isEqualToString:@"(null)"] || ([strStatusSubWorkOrder isKindOfClass:[NSNull class]])) {
            
            [self fetchSubWorkOrderFromCoreDataIfNull];
            
            dictDataSubWorkOrder=arrOfSubWorkOrder[indexSelectedSubWorkOrder];
            
            strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWOStatus"]];
            
        }
        
        NSString *strEmpNoOfSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"employeeNo"]];
        
        BOOL isEMP=[self IsDifferentEmp:strEmpNoOfSubWorkOrder];
        
        BOOL isStartedOtherSubWorkOrder=[global fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToSeeIfSubWorkorderIsAlreadyStarted:[global getEmployeeDeatils] :[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
        
        //isStartedOtherSubWorkOrder=NO;
        
        if ([strStatusSubWorkOrder isEqualToString:@"New"] || [strStatusSubWorkOrder isEqualToString:@"Not Started"] || [strStatusSubWorkOrder isEqualToString:@"NotStarted"]) {
            
            isStartedOtherSubWorkOrder=NO;
            
        }
        
        if (isEMP) {
            
            [global AlertMethod:Alert :@"Work Order Assigned to other Technician you can not process it."];
            
        } else if (isStartedOtherSubWorkOrder){
            
            //[global AlertMethod:Alert :[self alertMessageOnRunningStatus :[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]]];
            
            [self showAlertIfOtherWorkOrderStarted:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]]];
            
        }else {
            
            
            if ([strStatusSubWorkOrder isEqualToString:@"Not Started"]) {
                
                strStatusSubWorkOrder=@"New";
                
            }
            
            if ([strStatusSubWorkOrder isEqualToString:@"New"]) {
                
                BOOL isNetReachable=[global isNetReachable];
                
                strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
                
                if (isNetReachable) {
                    
                    [self metodUpdateSubWorkOrderStatus :@"New"];
                    
                } else {
                    
                    [self addActualHrsIfResponseIsNil];
                    
                }
                
                [self upDateSubWorkOrderStatusFromDB:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :@"Dispatch"];
                
            } else if ([strStatusSubWorkOrder isEqualToString:@"Dispatch"]){
                
                BOOL isNetReachable=[global isNetReachable];
                
                strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
                
                if (isNetReachable) {
                    
                    [self metodUpdateSubWorkOrderStatus :@"Dispatch"];
                    
                } else {
                    
                    [self addActualHrsIfResponseIsNil];
                    
                }
                
                [self upDateSubWorkOrderStatusFromDB:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :@"OnRoute"];
                
            }else if ([strStatusSubWorkOrder isEqualToString:@"OnRoute"]){
                
                //BOOL isNetReachable=[global isNetReachable];
                
                strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
                
                // change sub wo status in DB and send To Server
                
                [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :dictDataSubWorkOrder];
                
                [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"workorderId"]]];
                
            }
            
            [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
            strTypeExpandCollapse=@"exp";
            [self addViewOnLandingNew:@"exp"];
            
        }
    }
}


-(void)goToEquipMentHistory{
    
    [self saveImageToCoreData];
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        EquipmentHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EquipmentHistoryViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    }
    
}



-(void)fetchServiceAddressPOCDetailDcsFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalServiceAddressPOCDetailDcs=[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context];
    requestMechanicalServiceAddressPOCDetailDcs = [[NSFetchRequest alloc] init];
    [requestMechanicalServiceAddressPOCDetailDcs setEntity:entityMechanicalServiceAddressPOCDetailDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestMechanicalServiceAddressPOCDetailDcs setPredicate:predicate];
    
    sortDescriptorMechanicalServiceAddressPOCDetailDcs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalServiceAddressPOCDetailDcs = [NSArray arrayWithObject:sortDescriptorMechanicalServiceAddressPOCDetailDcs];
    
    [requestMechanicalServiceAddressPOCDetailDcs setSortDescriptors:sortDescriptorsMechanicalServiceAddressPOCDetailDcs];
    
    self.fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalServiceAddressPOCDetailDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs performFetch:&error];
    arrAllObjMechanicalServiceAddressPOCDetailDcs = [self.fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs fetchedObjects];
    if ([arrAllObjMechanicalServiceAddressPOCDetailDcs count] == 0)
    {
        
        
        
    }
    else
    {
        
        NSManagedObject *obj=arrAllObjMechanicalServiceAddressPOCDetailDcs[0];
        
        arrAllObjMechanicalServiceAddressPOCDetailDcs=[obj valueForKey:@"pocDetails"];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)fetchBillingAddressPOCDetailDcsFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalBillingAddressPOCDetailDcs=[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context];
    requestMechanicalBillingAddressPOCDetailDcs = [[NSFetchRequest alloc] init];
    [requestMechanicalBillingAddressPOCDetailDcs setEntity:entityMechanicalBillingAddressPOCDetailDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestMechanicalBillingAddressPOCDetailDcs setPredicate:predicate];
    
    sortDescriptorMechanicalBillingAddressPOCDetailDcs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalBillingAddressPOCDetailDcs = [NSArray arrayWithObject:sortDescriptorMechanicalBillingAddressPOCDetailDcs];
    
    [requestMechanicalBillingAddressPOCDetailDcs setSortDescriptors:sortDescriptorsMechanicalBillingAddressPOCDetailDcs];
    
    self.fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalBillingAddressPOCDetailDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs performFetch:&error];
    arrAllObjMechanicalBillingAddressPOCDetailDcs = [self.fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs fetchedObjects];
    if ([arrAllObjMechanicalBillingAddressPOCDetailDcs count] == 0)
    {
        
        
        
    }
    else
    {
        
        NSManagedObject *obj=arrAllObjMechanicalBillingAddressPOCDetailDcs[0];
        
        arrAllObjMechanicalBillingAddressPOCDetailDcs=[obj valueForKey:@"pocDetails"];
        
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}


- (IBAction)action_ServiceAddressPOCDetailDcs:(id)sender{
    
    if (arrAllObjMechanicalServiceAddressPOCDetailDcs.count==0) {
        
        [global AlertMethod:Alert :NoPOCAvailableee];
        
    }else{
        
        tblData.tag=108;
        [self tableLoad:108];
        
    }
    
}
- (IBAction)action_BillingAddressPOCDetailDcs:(id)sender{
    
    if (arrAllObjMechanicalBillingAddressPOCDetailDcs.count==0) {
        
        [global AlertMethod:Alert :NoPOCAvailableee];
        
    }else{
        
        tblData.tag=109;
        [self tableLoad:109];
        
    }
    
}

-(void)goToAddSubWorkOrder{
    
    BOOL isNet=[global isNetReachable];
    
    if (isNet) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"AddSubWorkOrder"];
        [defs synchronize];
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        CreateSubWorkOrderViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"CreateSubWorkOrderViewControlleriPad"];
        objSignViewController.strWorkOrderId=strWorkOrderId;
        objSignViewController.strWorkOrderNo=strWorkOrderNoGlobal;
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    } else {
        [global AlertMethod:Alert :ErrorInternetMsg];
    }
    
}
- (IBAction)action_AddSubWorkOrder:(id)sender {
    
    [self saveImageToCoreData];
    
    [self goToAddSubWorkOrder];
    
}
- (IBAction)action_textMsg:(id)sender {
    
    [self sendSMSForSales];
    
}
- (IBAction)action_EmailMsg:(id)sender {
    
    [self sendEmailForSales];
    
}

-(void)sendSMSForSales
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm?"
                               message:@"Do you want to send Text"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             NSString *strPhoneNo;
                             strPhoneNo=[matchesWorkOrder valueForKey:@"primaryPhone"];
                             NSArray *recipients = [NSArray arrayWithObjects:strPhoneNo, nil];
                             if (strPhoneNo.length==0) {
                                 
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Number not available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                 [alert show];
                                 
                             } else {
                                 strWorkOrderIdOnMyWaySentSMS=[matchesWorkOrder valueForKey:@"workorderId"];
                                 [self sendNumber:recipients :matchesWorkOrder];
                             }
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}

//============================================================================
//============================================================================
#pragma mark- ----------------Message Composer Methods----------------
//============================================================================
//============================================================================

-(void)sendNumber:(NSArray*)arrOfnumber :(NSManagedObject*)dictofWorkorderDetail{
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *strOld= [def valueForKey:@"CustomMessage"];
    
    if (strOld.length==0)
    {
        strOld=@"Hello ##CustomerName##, This is ##TechName##, I am on my way to your home and should be arriving within next 30 min on your Address ##Address##.";
    }
    
    NSString *strFirstName=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"FirstName"]];
    NSString *strMiddleName=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"middleName"]];
    NSString *strLastName=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"lastName"]];
    
    NSMutableArray *arrOfName=[[NSMutableArray alloc]init];
    
    if (!(strFirstName.length==0)) {
        [arrOfName addObject:strFirstName];
    }
    if (!(strMiddleName.length==0)) {
        
        [arrOfName addObject:strMiddleName];
        
    }
    if (!(strLastName.length==0)) {
        
        [arrOfName addObject:strLastName];
        
    }
    
    NSString *strFullName=[arrOfName componentsJoinedByString:@" "];
    
    NSString *strFinalMsg= [strOld stringByReplacingOccurrencesOfString:@"##CustomerName##" withString:strFullName];
    
    NSString *strTech= [strFinalMsg stringByReplacingOccurrencesOfString:@"##TechName##" withString:strEmpName];
    
    // NSString *strTime = [self calculateTimeBetweenDistane:[NSString stringWithFormat:@"%f,%f",22.7253,75.8655] :[NSString stringWithFormat:@"%f,%f",22.9600,76.0600]];
    
    NSString *strServiceAddress=[NSString stringWithFormat:@"%@ %@",[dictofWorkorderDetail valueForKey:@"servicesAddress1"],[dictofWorkorderDetail valueForKey:@"serviceAddress2"]];
    
    if (strServiceAddress.length==0)
    {
        strServiceAddress=@"N/A";
    }
    
    NSString  *strFinalMsgF= [strTech stringByReplacingOccurrencesOfString:@"##Address##" withString:strServiceAddress];
    
    if (strEmployeeEProfileUrl.length==0) {
        strEmployeeEProfileUrl=@"http://pestream.com/";
    }
    
    NSString *strFinalMsgLast= [strFinalMsgF stringByReplacingOccurrencesOfString:@"##EprofileUrl##" withString:strEmployeeEProfileUrl];
    
    NSString *strMessageFinal= [strFinalMsgLast stringByReplacingOccurrencesOfString:@"##ServiceAddress##" withString:strServiceAddress];
    
    //##ServiceAddress##
    [self sendSMS:arrOfnumber :strMessageFinal];
    
}


- (void)sendSMS:(NSArray *)recipients :(NSString*)message
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = message;
        controller.recipients = recipients;
        controller.messageComposeDelegate = self;
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Info message:@"Your device does not support sending Text Message" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [global AlertMethod:Alert :failSMSSend];
            break;
        }
            
        case MessageComposeResultSent:
        {
            [global AlertMethod:Info :SuccessSMSSend];
            
            [self fetchWorkOrderDetails:strWorkOrderIdOnMyWaySentSMS];
            [self updateSatusOnTextnEmail];
        }
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)fetchWorkOrderDetails :(NSString*)workOrderIdd{
    
    NSEntityDescription *entityWorkOderDetailServiceAuto;
    NSFetchRequest *requestNewService;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewService = [[NSFetchRequest alloc] init];
    [requestNewService setEntity:entityWorkOderDetailServiceAuto];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",workOrderIdd];
    
    [requestNewService setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNewService setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    NSArray *arrObjOnMyWaySentSMS = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    //    NSManagedObject *matchesNew;
    if (arrObjOnMyWaySentSMS.count==0)
    {
        
    }else
    {
        NSManagedObject *objTemp =arrObjOnMyWaySentSMS[0];
        NSDate *currentDate=[NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *stringCurrentDate = [formatter stringFromDate:currentDate];
        [objTemp setValue:stringCurrentDate forKey:@"onMyWaySentSMSTime"];
        
        //Nilind 08 Dec
        CLLocationCoordinate2D coordinate = [global getLocation] ;
        NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
        [objTemp setValue:latitude forKey:@"onMyWaySentSMSTimeLatitude"];
        [objTemp setValue:longitude forKey:@"onMyWaySentSMSTimeLongitude"];
        //End
        NSError *error;
        [context save:&error];
    }
}


-(void)sendEmailForSales
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Confirm?"
                                   message:@"Do you want to send Email"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                                 [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending Email..."];
                                 
                                 NSString *strEmailKey;
                                 strEmailKey=@"remindersms";
                                 NSString *strUrl=[NSString stringWithFormat:@"%@%@&CompanyKey=%@&aId=%@",UrlSalesEmail,strEmailKey,strGGQCompanyKey,[matchesWorkOrder valueForKey:@"surveyID"]];
                                 NSLog(@"Email url >>%@",strUrl);
                                 //http://www.gogetquality.com/survey/GoHandler.ashx?key=remindersms&companyKey=agt&aId=83
                                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                                     
                                     [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"SendEmailServiceAutomation" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                                      {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [DejalBezelActivityView removeView];
                                              if (success)
                                              {
                                                  NSString *returnString =[response valueForKey:@"ReturnMsg"];
                                                  if ([returnString isEqualToString:@"Successful"]) {
                                                      [global AlertMethod:Info :SuccessMailSend];
                                                      [self updateSatusOnTextnEmail];
                                                  } else {
                                                      [global AlertMethod:Info :SuccessMailSend];
                                                      [self updateSatusOnTextnEmail];
                                                  }
                                                  
                                              }
                                              else
                                              {
                                                  NSString *strTitle = Alert;
                                                  NSString *strMsg = Sorry;
                                                  [global AlertMethod:strTitle :strMsg];
                                              }
                                          });
                                      }];
                                 });
                                 
                                 //..................................................................
                                 
                             }];
        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}


-(void)updateSatusOnTextnEmail{
    
    if (indexSelectedSubWorkOrder==-1) {
        
        
        
    } else {
        
        isClickedOnViewSubWorkOrder = NO;
        isOnlyChangeStatus = YES;

        NSManagedObject *dictDataSubWorkOrder=arrOfSubWorkOrder[indexSelectedSubWorkOrder];
        
        NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWOStatus"]];
        
        if ([strStatusSubWorkOrder isEqualToString:@"(null)"] || ([strStatusSubWorkOrder isKindOfClass:[NSNull class]])) {
            
            [self fetchSubWorkOrderFromCoreDataIfNull];
            
            dictDataSubWorkOrder=arrOfSubWorkOrder[indexSelectedSubWorkOrder];
            
            strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWOStatus"]];
            
        }
        
        NSString *strEmpNoOfSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"employeeNo"]];
        
        BOOL isEMP=[self IsDifferentEmp:strEmpNoOfSubWorkOrder];
        
        BOOL isStartedOtherSubWorkOrder=[global fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToSeeIfSubWorkorderIsAlreadyStarted:[global getEmployeeDeatils] :[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
        
        //isStartedOtherSubWorkOrder=NO;
        
        if ([strStatusSubWorkOrder isEqualToString:@"New"] || [strStatusSubWorkOrder isEqualToString:@"Not Started"] || [strStatusSubWorkOrder isEqualToString:@"NotStarted"]) {
            
            isStartedOtherSubWorkOrder=NO;
            
        }
        
        if (isEMP) {
            
            //[global AlertMethod:Alert :@"Work Order Assigned to other Technician you can not process it."];
            
        } else if (isStartedOtherSubWorkOrder){
            
            //[global AlertMethod:Alert :[self alertMessageOnRunningStatus :[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]]];
            
            [self showAlertIfOtherWorkOrderStarted:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]]];
            
        }else {
            
            if ([strStatusSubWorkOrder isEqualToString:@"Not Started"]) {
                
                strStatusSubWorkOrder=@"New";
                
            }
            
            if ([strStatusSubWorkOrder isEqualToString:@"New"]) {
                
                BOOL isNetReachable=[global isNetReachable];
                
                strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
                
                if (isNetReachable) {
                    
                    [self metodUpdateSubWorkOrderStatus :@"New"];
                    
                } else {
                    
                    [self addActualHrsIfResponseIsNil];
                    
                }
                
                [self upDateSubWorkOrderStatusFromDB:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :@"Dispatch"];
                
                [self updateSatusOnTextnEmail];
                
            } else if ([strStatusSubWorkOrder isEqualToString:@"Dispatch"]){
                
                BOOL isNetReachable=[global isNetReachable];
                
                strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
                
                if (isNetReachable) {
                    
                    [self metodUpdateSubWorkOrderStatus :@"Dispatch"];
                    
                } else {
                    
                    [self addActualHrsIfResponseIsNil];
                    
                }
                
                [self upDateSubWorkOrderStatusFromDB:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :@"OnRoute"];
                
            }
            
            [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
            
            strTypeExpandCollapse=@"exp";
            [self addViewOnLandingNew:@"exp"];
            
        }
        
    }
}

-(void)copySubWorkOrder :(NSString*)strIdSubWorkOrder{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Copying please wait..."];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoaderCopyWorkOrder) name:@"MechanicalCopySubWorkOrder" object:nil];
    
    [global copySubWorkOrder:strServiceUrlMainServiceAutomation :strCompanyKey :strWorkOrderId :strIdSubWorkOrder :strLoggedInUserName];
    
}

-(void)stopLoaderCopyWorkOrder{
    
    [DejalBezelActivityView removeView];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    MechanicalGeneralInfoViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalGeneralInfoViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}

-(NSString*)alertMessageOnRunningStatus :(NSString*)strId{
    
    NSUserDefaults *defsRunning =[NSUserDefaults standardUserDefaults];
    NSArray *arrOfRunningSubWorkOrder =[defsRunning objectForKey:@"RunningSubWorkOrder"];
    
    //    NSMutableString *strStringToShow =[[NSMutableString alloc] init];
    //
    //    [strStringToShow appendString:@"Can't process this sub work-order while other sub work-order is in process.\n\n Following sub-work order are running : \n\n"];
    //
    //    for (int k1=0; k1<arrOfRunningSubWorkOrder.count; k1++) {
    //
    //        NSString *str = [NSString stringWithFormat:@" %d. %@\n",k1+1,arrOfRunningSubWorkOrder[k1]];
    //
    //        [strStringToShow appendString:str];
    //
    //
    //    }
    
    NSString *strString = [arrOfRunningSubWorkOrder componentsJoinedByString:@","];
    
    NSString *strMsgToShow = [NSString stringWithFormat:@"Can't process this work-order because other work-orders (%@) is in process.",strString];
    
    //NSString *strSubWorkOrderNoLocal = [defsRunning valueForKey:@"RunningSubWorkOrderNo"];
    
    if ([arrOfRunningSubWorkOrder containsObject:strWorkOrderNoGlobal]) {
        
        NSMutableArray *arrTemp = [global fetchRunningSubWorkOrder:strEmpNoLoggedIn :strWorkOrderId :strId];
        
        if (arrTemp.count>0) {
            
            NSString *strIdsToShowInALert = [arrTemp componentsJoinedByString:@","];
            strMsgToShow = [NSString stringWithFormat:@"Can't process this sub-work-order because other sub-work-order (%@) is in process.",strIdsToShowInALert];
            
        }
        
        //strMsgToShow = [NSString stringWithFormat:@"Can't process this sub-work-order because other sub-work-order (%@) is in process.",strSubWorkOrderNoLocal];
        
    }
    
    return strMsgToShow;
    
}

-(void)showAlertIfOtherWorkOrderStarted :(NSString*)strId{
    
    NSUserDefaults *defsRunning =[NSUserDefaults standardUserDefaults];
    NSArray *arrOfRunningSubWorkOrder =[defsRunning objectForKey:@"RunningSubWorkOrder"];
    
    if (arrOfRunningSubWorkOrder.count>0) {
        
        
        NSString *strString = [arrOfRunningSubWorkOrder componentsJoinedByString:@","];
        
        NSString *strMsgToShow = [NSString stringWithFormat:@"Can't process this work-order because other work-orders (%@) is in process. Do you won't to stop other work-orders",strString];
        
        if ([arrOfRunningSubWorkOrder containsObject:strWorkOrderNoGlobal]) {
            
            NSMutableArray *arrTemp = [global fetchRunningSubWorkOrder:strEmpNoLoggedIn :strWorkOrderId :strId];
            
            if (arrTemp.count>0) {
                
                NSString *strIdsToShowInALert = [arrTemp componentsJoinedByString:@","];
                strMsgToShow = [NSString stringWithFormat:@"Can't process this sub-work-order because other sub-work-order (%@) is in process. Do you won't to stop other sub-work-orders",strIdsToShowInALert];
                
            }
            
        }
        
        BOOL isNetReachable=[global isNetReachable];
        
        if (!isNetReachable) {
            
            strMsgToShow = [strMsgToShow stringByReplacingOccurrencesOfString:@"Do you won't to stop other work-orders" withString:@""];
            strMsgToShow = [strMsgToShow stringByReplacingOccurrencesOfString:@"Do you won't to stop other sub-work-orders" withString:@""];
            
        }
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:strMsgToShow
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Yes-Stop" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  // Stop All Running WO's
                                  [self stopAllRunningSubWorkOrder:arrOfRunningSubWorkOrder];
                                  
                              }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                     
                                 }];
        
        if (isNetReachable) {
            
            [alert addAction:yes];
            
        }
        //[alert addAction:yes];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}

-(void)stopAllRunningSubWorkOrder :(NSArray*)arrOfWos{
    
    for (int k=0; k<arrOfWos.count; k++) {
        
        NSString *strWorkOrderIdFetched = [global fetchWorkOrderFromDataBaseForMechanicalToFetchWorkOrderId:arrOfWos[k]];
        
        BOOL isStarted;
        isStarted=NO;
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        NSEntityDescription *entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
        NSFetchRequest *requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
        [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeNo = %@ && workorderId = %@",strEmpNoLoggedIn,strWorkOrderIdFetched];
        
        [requestSubWorkOrderActualHrs setPredicate:predicate];
        
        NSSortDescriptor *sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"employeeNo" ascending:NO];
        NSArray *sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
        
        [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
        
        self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
        
        // Perform Fetch
        NSError *error = nil;
        [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
        NSArray *arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
        if ([arrAllObjSubWorkOrderActualHrs count] == 0)
        {
            
            
        }
        else
        {
            
            NSMutableArray *arrOfRunningSubWorkOrderId = [[NSMutableArray alloc]init];
            
            NSString *strSubWorkOderIDRunning;
            
            strSubWorkOderIDRunning=@"";
            
            for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
                
                NSManagedObject *matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
                
                NSString *strSubWoStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWOStatus"]];//subWorkOrderId   employeeNo
                NSString *subWorkOrderId=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWorkOrderId"]];//subWorkOrderId   employeeNo
                NSString *strSubWoClockStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"clockStatus"]];//subWorkOrderId   employeeNo
                
                //  || [subWorkOrderId isEqualToString:strGlobalSubWorkOrderToSend]
                
                if ([strSubWoStatus isEqualToString:@"Completed"] || [strSubWoStatus isEqualToString:@"CompletePending"] || [strSubWoStatus isEqualToString:@"New"] || [strSubWoStatus isEqualToString:@"Not Started"] || [strSubWoStatus isEqualToString:@"Dispatch"]) {
                    
                    
                    
                }else{
                    
                    if ([strSubWoClockStatus caseInsensitiveCompare:@"Start"] == NSOrderedSame) {
                        
                        isStarted=YES;
                        
                        strSubWorkOderIDRunning =[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWorkOrderId"]];
                        
                        NSString *strCombinedStringToShow =[NSString stringWithFormat:@"%@",strSubWorkOderIDRunning];
                        
                        if ([arrOfRunningSubWorkOrderId containsObject:strCombinedStringToShow]) {
                            
                            
                            
                        } else {
                            
                            [arrOfRunningSubWorkOrderId addObject:strCombinedStringToShow];
                            
                        }
                        
                        
                        //break;
                        
                    } else {
                        
                        
                    }
                }
            }
            
            if (arrOfRunningSubWorkOrderId.count>0) {
                
                // Fetched All Sub Work Order Which Are Started
                
                for (int i=0; i<arrOfRunningSubWorkOrderId.count; i++) {
                    
                    // Fetching Actual Hour Id
                    
                    NSString *strActualHourId = [global fetchSubWorkOrderActualHrsFromDataBaseForActualHourId:strWorkOrderIdFetched :arrOfRunningSubWorkOrderId[i]];
                    
                    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    context = [appDelegate managedObjectContext];
                    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
                    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
                    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
                    
                    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@",strWorkOrderIdFetched,arrOfRunningSubWorkOrderId[i],strActualHourId];
                    
                    [requestSubWorkOrderActualHrs setPredicate:predicate];
                    
                    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
                    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
                    
                    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
                    
                    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
                    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
                    
                    // Perform Fetch
                    NSError *error = nil;
                    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
                    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
                    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
                    {
                        
                    }
                    else
                    {
                        
                        NSManagedObject * matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[0];
                        
                        [matchesSubWorkOrderActualHrs setValue:[global strCurrentDateFormattedForMechanical] forKey:@"timeOut"];
                        [matchesSubWorkOrderActualHrs setValue:[global strCurrentDateFormattedForMechanical] forKey:@"mobileTimeOut"];
                        
                        [matchesSubWorkOrderActualHrs setValue:@"Due to start other WorkOrder" forKey:@"reason"];
                        [matchesSubWorkOrderActualHrs setValue:@"Due to start other WorkOrder" forKey:@"actHrsDescription"];
                        
                        
                        NSError *error2;
                        [context save:&error2];
                        
                        //[global fetchMechanicalSubWorkOrderToStopJob:strWorkOrderIdFetched :arrOfRunningSubWorkOrderId[i]];
                        
                        BOOL isNetReachable=[global isNetReachable];
                        
                        if (isNetReachable) {
                            
                            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait..."];
                            
                            strNotificationTypeName=@"MechanicalActualHrsSyncGeneralInfoForceFully";
                            
                            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:strNotificationTypeName object:nil];
                            
                            SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
                            
                            [objSync syncMechanicalActualHours:strWorkOrderIdFetched :arrOfRunningSubWorkOrderId[i] :strActualHourId :strNotificationTypeName];
                            
                        }
                        
                        
                    }
                    if (error) {
                        
                        NSLog(@"Unable to execute fetch request.");
                        NSLog(@"%@, %@", error, error.localizedDescription);
                        
                    } else {
                        
                        
                        
                    }
                    
                    [global fetchMechanicalSubWorkOrderToStopJob:strWorkOrderIdFetched :arrOfRunningSubWorkOrderId[i]];
                    
                }
                
                
            }
            
        }
        if (error) {
            
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
            
        } else {
            
            
            
        }
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderIdFetched];
        
    }
    
}

-(void)stopLoader
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:strNotificationTypeName
                                                  object:nil];
    
    [DejalBezelActivityView removeView];
    
    if (!isOnlyChangeStatus) {
        
        //[self goToNextViewAfterForceFullyStopingWos];

    }else{
        
        [self changeStatusOnForceFullyStopWos];
        
    }
    
    //[self goToNextViewAfterForceFullyStopingWos];
    
}

-(void)changeStatusOnForceFullyStopWos{
    
    int indexxx = indexSelectedSubWorkOrder;
    
    NSManagedObject *dictDataSubWorkOrder=arrOfSubWorkOrder[indexxx];
    
    NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWOStatus"]];
    
    if ([strStatusSubWorkOrder isEqualToString:@"(null)"] || ([strStatusSubWorkOrder isKindOfClass:[NSNull class]])) {
        
        [self fetchSubWorkOrderFromCoreDataIfNull];
        
        dictDataSubWorkOrder=arrOfSubWorkOrder[indexxx];
        
        strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWOStatus"]];
        
    }
    
    
    NSString *strEmpNoOfSubWorkOrder=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"employeeNo"]];
    
    BOOL isEMP=[self IsDifferentEmp:strEmpNoOfSubWorkOrder];
    
    BOOL isStartedOtherSubWorkOrder=[global fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToSeeIfSubWorkorderIsAlreadyStarted:[global getEmployeeDeatils] :[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
    
    //isStartedOtherSubWorkOrder=NO;
    
    if ([strStatusSubWorkOrder isEqualToString:@"New"] || [strStatusSubWorkOrder isEqualToString:@"Not Started"] || [strStatusSubWorkOrder isEqualToString:@"NotStarted"]) {
        
        isStartedOtherSubWorkOrder=NO;
        
    }
    
    if (isEMP) {
        
        [global AlertMethod:Alert :@"Work Order Assigned to other Technician you can not process it."];
        
    } else if (isStartedOtherSubWorkOrder){
        
        //[global AlertMethod:Alert :[self alertMessageOnRunningStatus :[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]]];
        
        [self showAlertIfOtherWorkOrderStarted:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]]];
        
    }else {
        
        if ([strStatusSubWorkOrder isEqualToString:@"Not Started"]) {
            
            strStatusSubWorkOrder=@"New";
            
        }
        
        if ([strStatusSubWorkOrder isEqualToString:@"New"]) {
            
            BOOL isNetReachable=[global isNetReachable];
            
            strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            if (isNetReachable) {
                
                [self metodUpdateSubWorkOrderStatus :@"New"];
                
            } else {
                
                [self addActualHrsIfResponseIsNil];
                
            }
            
            [self upDateSubWorkOrderStatusFromDB:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :@"Dispatch"];
            
        } else if ([strStatusSubWorkOrder isEqualToString:@"Dispatch"]){
            
            BOOL isNetReachable=[global isNetReachable];
            
            strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            if (isNetReachable) {
                
                [self metodUpdateSubWorkOrderStatus :@"Dispatch"];
                
            } else {
                
                [self addActualHrsIfResponseIsNil];
                
            }
            
            [self upDateSubWorkOrderStatusFromDB:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :@"OnRoute"];
            
        }else if ([strStatusSubWorkOrder isEqualToString:@"OnRoute"]){
            
            //BOOL isNetReachable=[global isNetReachable];
            
            strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            // change sub wo status in DB and send To Server
            
            [self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"subWorkOrderId"]] :dictDataSubWorkOrder];
            
            //[global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:[NSString stringWithFormat:@"%@",[dictDataSubWorkOrder valueForKey:@"workorderId"]]];
            
        }
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
        
        strTypeExpandCollapse=@"exp";
        [self addViewOnLandingNew:@"exp"];
        
    }
}

-(void)goToNextViewAfterForceFullyStopingWos{
    
    if (isClickedOnViewSubWorkOrder) {
        
        strGlobalSubWorkOrderToSend = strIndexOnClickForceFullyCloseOtherWos;
        
    }
    
    [self updateWorkOrderDetail];
    
    [self saveEmailIdPrimaryToCoreData];
    
    if (yesEditedSomething) {
        
        //[self saveEmailIdPrimaryToCoreData];
        
        [self saveImageToCoreData];
        
        //  [self updateWorkOrderDetail];
        
    } else {
        
        
        
    }
    
    if ([strSubWorkOrderStatuss isEqualToString:@"Running"] || [strSubWorkOrderStatuss isEqualToString:@"Approved"]) {
        
        [self goToStartRepairView];
        
        // [self goToSubWorkOrderDetailView];
        
    } else {
        
        [self goToSubWorkOrderDetailView];
        
    }
    
}

//===================
//===================
#pragma mark- ------- Mechanical Sub Work Order Status On Route--------
//===================
//===================

-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute :(NSString*)strWoId :(NSString*)strSubWoId :(NSManagedObject*)objSubWoTemp{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWoId,strSubWoId];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    NSArray *arrtemp = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrtemp count] == 0)
    {
        
    }
    else
    {
        
        for (int k=0; k<arrtemp.count; k++) {
            
            NSManagedObject *objTemp=arrtemp[k];
            
            NSString *strStatus=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"status"]];
            
            if ([strStatus isEqualToString:@"OnRoute"]) {
                
                //strWoStatus=@"Inspection";
                
                [objTemp setValue:[global strCurrentDateFormattedForMechanical] forKey:@"timeOut"];
                
                NSString *strTimeOut = [global strCurrentDateFormattedForMechanical];
                
                [objTemp setValue:strTimeOut forKey:@"mobileTimeOut"];
                
                [self metodUpdateSubWorkOrderStatusOnRoute:strTimeOut :strWoId :strSubWoId];
                
                [objTemp setValue:ReasonOnRoute forKey:@"reason"];
                
                [objSubWoTemp setValue:@"Inspection" forKey:@"subWOStatus"];
                [objSubWoTemp setValue:@"Start" forKey:@"clockStatus"];
                
                //                _lblStatus.text=[NSString stringWithFormat:@"Status: %@",[_objSubWorkOrderdetails valueForKey:@"subWOStatus"]];
                //
                //                if ([_lblStatus.text isEqualToString:@"Status: Inspection"]) {
                //
                //                    _lblStatus.text=[NSString stringWithFormat:@"Status: %@",@"In Progress"];
                //
                //                }
                
                NSError *error2;
                [context save:&error2];
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:@"Inspection" forKey:@"WoStatus"];
                //isJobStarted=YES;
                [defs synchronize];
                
                break;
                
            }
            
        }
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}

-(void)metodUpdateSubWorkOrderStatusOnRoute :(NSString*)strTimeOut :(NSString*)strWoId :(NSString*)strSubWoId{
    
    //isJobStarted=YES;
    //strWoStatus=@"Inspection";
    NSString *strStatusToSend;
    
    strStatusToSend=@"OnRoute";
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"]];
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&subWorkOrderId=%@&userName=%@&status=%@&%@%@",strServiceUrlMainServiceAutomation,UrlMechanicalSubWorkOrderStatusUpdateInspection,strCompanyKey,strSubWoId,strUserName,strStatusToSend,UrlMechanicalSubWorkOrderTimeOut,strTimeOut];
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    strUrl = [NSString stringWithFormat:@"%@%@%@&subWorkOrderId=%@&userName=%@&status=%@&%@%@&Latitude=%@&Longitude=%@",strServiceUrlMainServiceAutomation,UrlMechanicalSubWorkOrderStatusUpdateInspection,strCompanyKey,strSubWoId,strUserName,strStatusToSend,UrlMechanicalSubWorkOrderTimeOut,strTimeOut,latitude,longitude];
    
    NSLog(@"Mechanical Work Order Status Update URl-----%@",strUrl);
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Status..."];
    
    //============================================================================
    //============================================================================
    
    NSString *strType;
    
    strType=@"MechanicalSubWorkOrderStatusDispatch";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [DejalBezelActivityView removeView];
                 
                 if (success)
                 {
                     
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
                 
                 [self addStartActualHoursFromMobileToDBForOnRoute :strWoId :strSubWoId];
                 
             });
         }];
    });
    //============================================================================
    //============================================================================
    
}

-(void)addStartActualHoursFromMobileToDBForOnRoute :(NSString*)strWoId :(NSString*)strSubWoId{
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWoId;
    
    NSString *strNos=[global getReferenceNumber];
    objSubWorkOrderNotes.subWOActualHourId=strNos;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:strNos forKey:@"subWOActualHourId"];
    [defs synchronize];
    
    objSubWorkOrderNotes.subWorkOrderId=strSubWoId;
    objSubWorkOrderNotes.timeIn=[global strCurrentDateFormattedForMechanical];
    objSubWorkOrderNotes.mobileTimeIn=objSubWorkOrderNotes.timeIn;
    objSubWorkOrderNotes.status=@"Inspection";
    objSubWorkOrderNotes.timeOut=@"";
    objSubWorkOrderNotes.mobileTimeOut=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKey;
    objSubWorkOrderNotes.reason=@"";
    objSubWorkOrderNotes.actHrsDescription=@"";
    objSubWorkOrderNotes.subWorkOrderNo=@"";
    objSubWorkOrderNotes.subWOActualHourNo=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKey;
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.createdDate=[global strCurrentDate];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.modifiedDate=[global strCurrentDate];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    // Latitude changes
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objSubWorkOrderNotes.latitude=latitude;
    objSubWorkOrderNotes.longitude=longitude;
    // Latitude changes end
    
    objSubWorkOrderNotes.employeeNo=[global getEmployeeDeatils];
    
    NSError *error2;
    [context save:&error2];
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
        
        //strNotificationTypeName=@"MechanicalActualHrsSyncInspectionFR2";
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoaderOnRoute) name:@"MechanicalActualHrsSyncInspectionFR2" object:nil];
        
        SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
        
        [objSync syncMechanicalActualHours:strWoId :strSubWoId :strNos :@"MechanicalActualHrsSyncInspectionFR2"];
        
    }else{
        
        if (isOnRouteForcefully) {
            
            isOnRouteForcefully = NO;
            [self callingMethodOnRouteForcefully];
            
        }
        
    }
    
    [self fetchSubWorkOrderFromDB];
    
    
}

-(void)stopLoaderOnRoute
{
    
    if (isOnRouteForcefully) {
        
        isOnRouteForcefully = NO;
        [self callingMethodOnRouteForcefully];
        
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"MechanicalActualHrsSyncInspectionFR2"
                                                  object:nil];
    
    [DejalBezelActivityView removeView];
    
}

-(void)callingMethodOnRouteForcefully{
    
    isOnRouteForcefully = NO;
    
    [self updateWorkOrderDetail];
    
    [self saveEmailIdPrimaryToCoreData];
    
    if (yesEditedSomething) {
        
        //[self saveEmailIdPrimaryToCoreData];
        
        [self saveImageToCoreData];
        
        //  [self updateWorkOrderDetail];
        
    } else {
        
        
    }
    
    if ([strSubWorkOrderStatuss isEqualToString:@"Running"] || [strSubWorkOrderStatuss isEqualToString:@"Approved"]) {
        
        [self goToStartRepairView];
        
        // [self goToSubWorkOrderDetailView];
        
    } else {
        
        [self goToSubWorkOrderDetailView];
        
    }
    
}


-(void)fetchSubWorkOrderFromCoreDataIfNull{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    NSArray *arrNew = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    
    arrAllObjMechanicalSubWorkOrder = arrNew;
    
    if ([arrNew count] == 0)
    {
        
        
        
    }
    else
    {
        
        arrOfSubWorkOrder=nil;
        
        arrOfSubWorkOrder=[[NSMutableArray alloc]init];
        
        for (int j=0; j<arrNew.count; j++) {
            
            NSManagedObject *tempObjNew=arrNew[j];
            
            NSString *strIsActive=[NSString stringWithFormat:@"%@",[tempObjNew valueForKey:@"isActive"]];
            
            if ([strIsActive caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsActive isEqualToString:@"1"]) {
                
                [arrOfSubWorkOrder addObject:tempObjNew];
                
            }
            
            if (j==indexSelectedSubWorkOrder) {
                
                strWoType=[NSString stringWithFormat:@"%@",[tempObjNew valueForKey:@"subWOType"]];
                
                NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[tempObjNew valueForKey:@"subWOStatus"]];
                
                strSubWorkOrderStatuss=strStatusSubWorkOrder;
                if ([strSubWorkOrderStatuss isEqualToString:@"Not Started"]) {
                    
                    strSubWorkOrderStatuss=@"New";
                    
                }
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:strSubWorkOrderStatuss forKey:@"WoStatus"];
                strSubWoAcNo=[NSString stringWithFormat:@"Sub Workorder #: %@",[tempObjNew valueForKey:@"SubWorkOrderNo"]];
                [defs synchronize];
                
                NSManagedObject *objSubWorkOrderCell=arrAllObjMechanicalSubWorkOrder[indexSelectedSubWorkOrder];
                objSubWorkOrderToSend=objSubWorkOrderCell;
                strGlobalSubWorkOrderToSend=[NSString stringWithFormat:@"%@",[objSubWorkOrderCell valueForKey:@"subWorkOrderId"]];
                
            }
            
        }
        
        NSLog(@"Sub---Work Order Detail on Mechanical General infor view Again if null====%@",arrOfSubWorkOrder);
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)fetchServicePocDetails{
    
    
    NSDictionary *dictDataServicePOC;
    
    for (int k=0; k<arrAllObjMechanicalServiceAddressPOCDetailDcs.count; k++) {
        
        NSDictionary *dictData=arrAllObjMechanicalServiceAddressPOCDetailDcs[k];
        
        NSString *strContactID=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        if ([strContactID isEqualToString:strServicePOCId]) {
            
            dictDataServicePOC=dictData;
            break;
            
        }
        
    }
    
    NSString *strFnService = [NSString stringWithFormat:@"%@",[dictDataServicePOC valueForKey:@"FirstName"]];
    NSString *strMnService = [NSString stringWithFormat:@"%@",[dictDataServicePOC valueForKey:@"MiddleName"]];
    NSString *strLnService = [NSString stringWithFormat:@"%@",[dictDataServicePOC valueForKey:@"LastName"]];
    
    NSMutableArray *arrService;
    
    arrService = [[NSMutableArray alloc]init];
    
    if (strFnService.length>0) {
        
        [arrService addObject:strFnService];
        
    }
    if (strMnService.length>0) {
        
        [arrService addObject:strMnService];
        
    }
    if (strLnService.length>0) {
        
        [arrService addObject:strLnService];
        
    }
    
    _serviceContactName.text=[arrService componentsJoinedByString:@" "];
    
    _serviceContactPrimaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataServicePOC valueForKey:@"PrimaryEmail"]]]];
    
    _serviceContactSecondaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataServicePOC valueForKey:@"SecondaryEmail"]]]];
    
    _serviceContactPrimaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictDataServicePOC valueForKey:@"PrimaryPhone"]]];
    
    _serviceContactSecondaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictDataServicePOC valueForKey:@"SecondaryPhone"]]];
    
    
}
-(void)fetchBillingPocDetails{
    
    
    NSDictionary *dictDataBillingPOC;
    
    for (int k=0; k<arrAllObjMechanicalBillingAddressPOCDetailDcs.count; k++) {
        
        NSDictionary *dictData=arrAllObjMechanicalBillingAddressPOCDetailDcs[k];
        
        NSString *strContactID=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ContactId"]];
        
        if ([strContactID isEqualToString:strBillingPOCId]) {
            
            dictDataBillingPOC=dictData;
            break;
            
        }
        
    }
    
    NSString *strFnBilling = [NSString stringWithFormat:@"%@",[dictDataBillingPOC valueForKey:@"FirstName"]];
    NSString *strMnBilling = [NSString stringWithFormat:@"%@",[dictDataBillingPOC valueForKey:@"MiddleName"]];
    NSString *strLnBilling = [NSString stringWithFormat:@"%@",[dictDataBillingPOC valueForKey:@"LastName"]];
    
    NSMutableArray *arrBilling;
    
    arrBilling = [[NSMutableArray alloc]init];
    
    if (strFnBilling.length>0) {
        
        [arrBilling addObject:strFnBilling];
        
    }
    if (strMnBilling.length>0) {
        
        [arrBilling addObject:strMnBilling];
        
    }
    if (strLnBilling.length>0) {
        
        [arrBilling addObject:strLnBilling];
        
    }
    
    _billingContactName.text=[arrBilling componentsJoinedByString:@" "];
    
    
    _billingContactPrimaryEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataBillingPOC valueForKey:@"PrimaryEmail"]]]];
    
    _billingContactSecondayEmail.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataBillingPOC valueForKey:@"SecondaryEmail"]]]];
    
    _billingContactPrimaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictDataBillingPOC valueForKey:@"PrimaryPhone"]]];
    
    _billingContactSecondaryPhone.attributedText=[global getUnderLineAttributedString:[NSString stringWithFormat:@"%@",[dictDataBillingPOC valueForKey:@"SecondaryPhone"]]];
    
}


-(void)saveServiceAndBillingAddressToEmailDB{
    
    if (_serviceContactPrimaryEmail.text.length>0) {
        
        [global fetchIfEmailIdExistInDB:_serviceContactPrimaryEmail.text  :@"Service" :strWorkOrderId];
        
    }
    
    if (_serviceContactSecondaryEmail.text.length>0) {
        
        [global fetchIfEmailIdExistInDB:_serviceContactSecondaryEmail.text  :@"Service" :strWorkOrderId];
        
    }
    
    if (_billingContactPrimaryEmail.text.length>0) {
        
        [global fetchIfEmailIdExistInDB:_billingContactPrimaryEmail.text  :@"Billing" :strWorkOrderId];
        
    }
    
    if (_billingContactSecondayEmail.text.length>0) {
        
        [global fetchIfEmailIdExistInDB:_billingContactSecondayEmail.text  :@"Billing" :strWorkOrderId];
        
    }
    
    //    // For Same Service and Billing Address
    //
    //    if ([_serviceContactPrimaryEmail.text isEqualToString:_billingContactPrimaryEmail.text] || [_serviceContactPrimaryEmail.text isEqualToString:_billingContactSecondayEmail.text]) {
    //
    //        if (_serviceContactPrimaryEmail.text.length>0) {
    //
    //            [global fetchIfEmailIdExistInDB:_serviceContactPrimaryEmail.text  :@"BillingService" :strWorkOrderId];
    //
    //        }
    //
    //    }
    //    if ([_billingContactPrimaryEmail.text isEqualToString:_serviceContactPrimaryEmail.text] || [_billingContactPrimaryEmail.text isEqualToString:_serviceContactSecondaryEmail.text]) {
    //
    //        if (_billingContactPrimaryEmail.text.length>0) {
    //
    //            [global fetchIfEmailIdExistInDB:_billingContactPrimaryEmail.text  :@"BillingService" :strWorkOrderId];
    //
    //        }
    //    }
    //    if  ([_serviceContactSecondaryEmail.text isEqualToString:_billingContactPrimaryEmail.text] || [_serviceContactSecondaryEmail.text isEqualToString:_billingContactSecondayEmail.text]) {
    //
    //        if (_serviceContactSecondaryEmail.text.length>0) {
    //
    //            [global fetchIfEmailIdExistInDB:_serviceContactSecondaryEmail.text  :@"BillingService" :strWorkOrderId];
    //
    //        }
    //    }
    //    if ([_billingContactSecondayEmail.text isEqualToString:_serviceContactPrimaryEmail.text] || [_billingContactSecondayEmail.text isEqualToString:_serviceContactSecondaryEmail.text]) {
    //
    //        if (_billingContactSecondayEmail.text.length>0) {
    //
    //            [global fetchIfEmailIdExistInDB:_billingContactSecondayEmail.text  :@"BillingService" :strWorkOrderId];
    //
    //        }
    //    }
    
    
}

@end
