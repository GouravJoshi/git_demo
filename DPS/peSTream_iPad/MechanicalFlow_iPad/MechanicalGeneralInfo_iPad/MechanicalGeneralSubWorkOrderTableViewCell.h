//
//  MechanicalGeneralSubWorkOrderTableViewCell.h
//  DPS peSTream Quacito.
//  peSTream  
//  Created by Saavan Patidar on 25/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream  

#import <UIKit/UIKit.h>

@interface MechanicalGeneralSubWorkOrderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblSubWorkOrderNo;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceDatenTime;
@property (strong, nonatomic) IBOutlet UILabel *lblJobDescriptions;
@property (strong, nonatomic) IBOutlet UILabel *lblAssignedTo;
@property (strong, nonatomic) IBOutlet UILabel *lblEstTime;
@property (strong, nonatomic) IBOutlet UILabel *lblActualTime;
@property (strong, nonatomic) IBOutlet UILabel *lblSubWorkOrderType;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UIButton *action_ViewSubWorkOrder;
@property (strong, nonatomic) IBOutlet UIButton *action_DeleteSubWorkOrder;
@property (strong, nonatomic) IBOutlet UIButton *btnStatus;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectedSubWorkOrder;
@property (strong, nonatomic) IBOutlet UIButton *action_ViewMoreJobDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnCopySubWorkOrder;
@property (strong, nonatomic) IBOutlet UIButton *btnSubWoEmployeeWorkingTime;

@end
