//
//  ServiceDynamicForm+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 05/09/17.
//
//

#import "ServiceDynamicForm+CoreDataProperties.h"

@implementation ServiceDynamicForm (CoreDataProperties)

+ (NSFetchRequest<ServiceDynamicForm *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ServiceDynamicForm"];
}

@dynamic arrFinalInspection;
@dynamic companyKey;
@dynamic departmentId;
@dynamic empId;
@dynamic isSentToServer;
@dynamic userName;
@dynamic workOrderId;

@end
