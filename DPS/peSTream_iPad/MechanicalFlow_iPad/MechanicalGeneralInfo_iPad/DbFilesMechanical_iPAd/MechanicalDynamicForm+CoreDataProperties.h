//
//  MechanicalDynamicForm+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 05/09/17.
//
//

#import "MechanicalDynamicForm+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalDynamicForm (CoreDataProperties)

+ (NSFetchRequest<MechanicalDynamicForm *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *arrFinalInspection;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *departmentId;
@property (nullable, nonatomic, copy) NSString *empId;
@property (nullable, nonatomic, copy) NSString *isSentToServer;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;

@end

NS_ASSUME_NONNULL_END
