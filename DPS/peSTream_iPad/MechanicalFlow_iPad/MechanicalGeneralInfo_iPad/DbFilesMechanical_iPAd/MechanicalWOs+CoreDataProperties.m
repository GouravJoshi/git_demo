//
//  MechanicalWOs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 17/04/19.
//
//

#import "MechanicalWOs+CoreDataProperties.h"

@implementation MechanicalWOs (CoreDataProperties)

+ (NSFetchRequest<MechanicalWOs *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"MechanicalWOs"];
}

@dynamic userName;
@dynamic companyKey;
@dynamic workorderId;

@end
