//
//  MechanicalDynamicForm+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 05/09/17.
//
//

#import "MechanicalDynamicForm+CoreDataProperties.h"

@implementation MechanicalDynamicForm (CoreDataProperties)

+ (NSFetchRequest<MechanicalDynamicForm *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalDynamicForm"];
}

@dynamic arrFinalInspection;
@dynamic companyKey;
@dynamic departmentId;
@dynamic empId;
@dynamic isSentToServer;
@dynamic userName;
@dynamic subWorkOrderId;

@end
