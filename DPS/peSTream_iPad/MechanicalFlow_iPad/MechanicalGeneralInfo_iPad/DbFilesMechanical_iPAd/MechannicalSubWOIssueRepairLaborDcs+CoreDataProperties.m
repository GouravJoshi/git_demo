//
//  MechannicalSubWOIssueRepairLaborDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechannicalSubWOIssueRepairLaborDcs+CoreDataProperties.h"

@implementation MechannicalSubWOIssueRepairLaborDcs (CoreDataProperties)

+ (NSFetchRequest<MechannicalSubWOIssueRepairLaborDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechannicalSubWOIssueRepairLaborDcs"];
}

@dynamic createdBy;
@dynamic createdDate;
@dynamic isActive;
@dynamic isDefault;
@dynamic issueRepairId;
@dynamic issueRepairLaborId;
@dynamic isWarranty;
@dynamic laborCost;
@dynamic laborCostType;
@dynamic laborDescription;
@dynamic laborHours;
@dynamic laborType;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic workorderId;
@dynamic createdByDevice;
@dynamic subWorkOrderId;
@dynamic subWorkOrderIssueId;

@end
