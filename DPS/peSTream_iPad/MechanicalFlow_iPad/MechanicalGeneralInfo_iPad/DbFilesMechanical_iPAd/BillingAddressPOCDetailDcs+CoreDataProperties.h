//
//  BillingAddressPOCDetailDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 09/05/18.
//
//

#import "BillingAddressPOCDetailDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BillingAddressPOCDetailDcs (CoreDataProperties)

+ (NSFetchRequest<BillingAddressPOCDetailDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *employeeNo;
@property (nullable, nonatomic, retain) NSObject *pocDetails;
@property (nullable, nonatomic, copy) NSString *workorderId;//userName
@property (nullable, nonatomic, copy) NSString *userName;

@end

NS_ASSUME_NONNULL_END
