//
//  SubWOCompleteTimeExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 18/06/18.
//
//

#import "SubWOCompleteTimeExtSerDcs+CoreDataProperties.h"

@implementation SubWOCompleteTimeExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<SubWOCompleteTimeExtSerDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SubWOCompleteTimeExtSerDcs"];
}

@dynamic subWOCompleteTimeId;
@dynamic subWorkOrderId;
@dynamic actualDurationInMin;
@dynamic actualAmt;
@dynamic billableDurationInMin;
@dynamic billableAmt;
@dynamic timeSlot;
@dynamic workingType;
@dynamic isActive;
@dynamic createdDate;
@dynamic createdBy;
@dynamic modifiedDate;
@dynamic modifiedBy;
@dynamic createdByDevice;
@dynamic workorderId;

@end
