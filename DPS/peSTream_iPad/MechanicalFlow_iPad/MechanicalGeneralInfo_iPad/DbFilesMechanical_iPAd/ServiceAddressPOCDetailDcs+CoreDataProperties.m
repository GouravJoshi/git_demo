//
//  ServiceAddressPOCDetailDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 09/05/18.
//
//

#import "ServiceAddressPOCDetailDcs+CoreDataProperties.h"

@implementation ServiceAddressPOCDetailDcs (CoreDataProperties)

+ (NSFetchRequest<ServiceAddressPOCDetailDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ServiceAddressPOCDetailDcs"];
}

@dynamic companyKey;
@dynamic employeeNo;
@dynamic workorderId;
@dynamic pocDetails;
@dynamic userName;

@end
