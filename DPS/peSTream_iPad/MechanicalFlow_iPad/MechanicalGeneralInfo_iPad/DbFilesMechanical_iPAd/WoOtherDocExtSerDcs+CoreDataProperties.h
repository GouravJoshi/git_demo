//
//  WoOtherDocExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 12/09/18.
//
//

#import "WoOtherDocExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoOtherDocExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoOtherDocExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *wOOtherDocId;
@property (nullable, nonatomic, copy) NSString *workOrderId;
@property (nullable, nonatomic, copy) NSString *workOrderNo;
@property (nullable, nonatomic, copy) NSString *leadNo;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *wOOtherDocPath;
@property (nullable, nonatomic, copy) NSString *wOOtherDocTitle;
@property (nullable, nonatomic, copy) NSString *woOtherDocDescription;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *createdByDevice;
@property (nullable, nonatomic, copy) NSString *userName;

@end

NS_ASSUME_NONNULL_END
