//
//  MechanicalSubWOIssueRepairPartDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWOIssueRepairPartDcs+CoreDataProperties.h"

@implementation MechanicalSubWOIssueRepairPartDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWOIssueRepairPartDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalSubWOIssueRepairPartDcs"];
}

@dynamic actualQty;
@dynamic createdBy;
@dynamic createdDate;
@dynamic customerFeedback;
@dynamic installationDate;
@dynamic isActive;
@dynamic isAddedAfterApproval;
@dynamic isCompleted;
@dynamic isDefault;
@dynamic issueRepairId;
@dynamic issueRepairPartId;
@dynamic isWarranty;
@dynamic manufacturer;
@dynamic modelNumber;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic multiplier;
@dynamic partCode;
@dynamic partDesc;
@dynamic partName;
@dynamic partType;
@dynamic qty;
@dynamic serialNumber;
@dynamic subWorkOrderIssueId;
@dynamic unitPrice;
@dynamic workorderId;
@dynamic createdByDevice;
@dynamic subWorkOrderId;
@dynamic isChangeStdPartPrice;
@dynamic vendorName;
@dynamic vendorPartNo;
@dynamic vendorQuoteNo;
@dynamic partCategorySysName;

@end
