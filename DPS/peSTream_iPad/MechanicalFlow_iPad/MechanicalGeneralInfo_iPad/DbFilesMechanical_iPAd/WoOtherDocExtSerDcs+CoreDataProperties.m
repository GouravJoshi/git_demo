//
//  WoOtherDocExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 12/09/18.
//
//

#import "WoOtherDocExtSerDcs+CoreDataProperties.h"

@implementation WoOtherDocExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoOtherDocExtSerDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"WoOtherDocExtSerDcs"];
}

@dynamic wOOtherDocId;
@dynamic workOrderId;
@dynamic workOrderNo;
@dynamic leadNo;
@dynamic companyKey;
@dynamic wOOtherDocPath;
@dynamic wOOtherDocTitle;
@dynamic woOtherDocDescription;
@dynamic createdDate;
@dynamic createdBy;
@dynamic modifiedDate;
@dynamic modifiedBy;
@dynamic createdByDevice;
@dynamic userName;

@end
