//
//  SubWOCompleteTimeExtSerDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 18/06/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubWOCompleteTimeExtSerDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SubWOCompleteTimeExtSerDcs+CoreDataProperties.h"
