//
//  SubWOCompleteTimeExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 18/06/18.
//
//

#import "SubWOCompleteTimeExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SubWOCompleteTimeExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<SubWOCompleteTimeExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *subWOCompleteTimeId;
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *actualDurationInMin;
@property (nullable, nonatomic, copy) NSString *actualAmt;
@property (nullable, nonatomic, copy) NSString *billableDurationInMin;
@property (nullable, nonatomic, copy) NSString *billableAmt;
@property (nullable, nonatomic, copy) NSString *timeSlot;
@property (nullable, nonatomic, copy) NSString *workingType;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *createdByDevice;  //workorderId
@property (nullable, nonatomic, copy) NSString *workorderId;

@end

NS_ASSUME_NONNULL_END
