//
//  MechanicalSubWorkOrderNoteDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWorkOrderNoteDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MechanicalSubWorkOrderNoteDcs+CoreDataProperties.h"
