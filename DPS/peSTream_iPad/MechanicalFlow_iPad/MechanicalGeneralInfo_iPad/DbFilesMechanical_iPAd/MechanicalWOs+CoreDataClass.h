//
//  MechanicalWOs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 17/04/19.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MechanicalWOs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MechanicalWOs+CoreDataProperties.h"
