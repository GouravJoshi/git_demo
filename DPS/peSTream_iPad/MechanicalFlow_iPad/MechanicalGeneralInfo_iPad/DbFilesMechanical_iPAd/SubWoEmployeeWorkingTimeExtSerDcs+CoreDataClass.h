//
//  SubWoEmployeeWorkingTimeExtSerDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 18/06/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubWoEmployeeWorkingTimeExtSerDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SubWoEmployeeWorkingTimeExtSerDcs+CoreDataProperties.h"
