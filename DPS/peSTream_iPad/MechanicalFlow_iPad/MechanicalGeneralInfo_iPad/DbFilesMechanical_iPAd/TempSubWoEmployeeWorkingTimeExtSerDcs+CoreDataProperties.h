//
//  TempSubWoEmployeeWorkingTimeExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 03/07/18.
//
//

#import "TempSubWoEmployeeWorkingTimeExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TempSubWoEmployeeWorkingTimeExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<TempSubWoEmployeeWorkingTimeExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *actualAmt;
@property (nullable, nonatomic, copy) NSString *actualDurationInMin;
@property (nullable, nonatomic, copy) NSString *billableAmt;
@property (nullable, nonatomic, copy) NSString *billableDurationInMin;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdByDevice;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *employeeName;
@property (nullable, nonatomic, copy) NSString *employeeNo;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *isApprove;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *subWOEmployeeWorkingTimeId;
@property (nullable, nonatomic, copy) NSString *subWorkOrderActualHourId;
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *timeSlot;
@property (nullable, nonatomic, copy) NSString *timeSlotTitle;
@property (nullable, nonatomic, copy) NSString *workingDate;
@property (nullable, nonatomic, copy) NSString *workingType;
@property (nullable, nonatomic, copy) NSString *workorderId;//isHelper
@property (nullable, nonatomic, copy) NSString *isHelper;
@end

NS_ASSUME_NONNULL_END
