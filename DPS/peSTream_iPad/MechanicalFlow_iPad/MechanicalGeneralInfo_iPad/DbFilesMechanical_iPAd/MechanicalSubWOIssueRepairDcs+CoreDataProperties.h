//
//  MechanicalSubWOIssueRepairDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWOIssueRepairDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWOIssueRepairDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWOIssueRepairDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *costAdjustment;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *customerFeedback;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *isCompleted;
@property (nullable, nonatomic, copy) NSString *issueRepairId;
@property (nullable, nonatomic, copy) NSString *isWarranty;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *repairDesc;
@property (nullable, nonatomic, copy) NSString *repairMasterId;
@property (nullable, nonatomic, copy) NSString *repairName;
@property (nullable, nonatomic, copy) NSString *subWorkOrderIssueId;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *createdByDevice;//createdByDevice
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;//repairLaborId
@property (nullable, nonatomic, copy) NSString *repairLaborId;//repairAmt
@property (nullable, nonatomic, copy) NSString *repairAmt;

@property (nullable, nonatomic, copy) NSString *partCostAdjustment;
@property (nullable, nonatomic, copy) NSString *nonStdRepairAmt;
@property (nullable, nonatomic, copy) NSString *nonStdPartAmt;
@property (nullable, nonatomic, copy) NSString *nonStdLaborAmt;
@property (nullable, nonatomic, copy) NSString *qty;


@end

NS_ASSUME_NONNULL_END
