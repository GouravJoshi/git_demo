//
//  MechanicalWoEquipment+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 28/02/18.
//
//

#import "MechanicalWoEquipment+CoreDataProperties.h"

@implementation MechanicalWoEquipment (CoreDataProperties)

+ (NSFetchRequest<MechanicalWoEquipment *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalWoEquipment"];
}

@dynamic accountItemHistoryId;
@dynamic accountId;
@dynamic itemName;
@dynamic itemcode;
@dynamic subWorkOrderNo;
@dynamic workOrderNo;
@dynamic serialNumber;
@dynamic modelNumber;
@dynamic manufacturer;
@dynamic qty;
@dynamic installationDate;
@dynamic warrantyExpireDate;
@dynamic isActive;
@dynamic accountNo;
@dynamic serviceAddressId;
@dynamic barcodeUrl;
@dynamic installedArea;
@dynamic equipmentDesc;
@dynamic createdDate;
@dynamic createdBy;
@dynamic modifiedDate;
@dynamic modifiedBy;
@dynamic createdByDevice;
@dynamic companyKey;
@dynamic userName;
@dynamic subWorkOrderId;
@dynamic workorderId;
@dynamic itemCustomName;
@dynamic itemNo;

@end
