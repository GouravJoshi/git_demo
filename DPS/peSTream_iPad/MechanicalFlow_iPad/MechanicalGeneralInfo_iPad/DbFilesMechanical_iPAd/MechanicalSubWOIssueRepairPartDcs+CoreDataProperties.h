//
//  MechanicalSubWOIssueRepairPartDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWOIssueRepairPartDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWOIssueRepairPartDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWOIssueRepairPartDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *actualQty;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *customerFeedback;
@property (nullable, nonatomic, copy) NSString *installationDate;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *isAddedAfterApproval;
@property (nullable, nonatomic, copy) NSString *isCompleted;
@property (nullable, nonatomic, copy) NSString *isDefault;
@property (nullable, nonatomic, copy) NSString *issueRepairId;
@property (nullable, nonatomic, copy) NSString *issueRepairPartId;
@property (nullable, nonatomic, copy) NSString *isWarranty;
@property (nullable, nonatomic, copy) NSString *manufacturer;
@property (nullable, nonatomic, copy) NSString *modelNumber;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *multiplier;
@property (nullable, nonatomic, copy) NSString *partCode;
@property (nullable, nonatomic, copy) NSString *partDesc;
@property (nullable, nonatomic, copy) NSString *partName;
@property (nullable, nonatomic, copy) NSString *partType;
@property (nullable, nonatomic, copy) NSString *qty;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSString *subWorkOrderIssueId;
@property (nullable, nonatomic, copy) NSString *unitPrice;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *createdByDevice;//createdByDevice
@property (nullable, nonatomic, copy) NSString *subWorkOrderId; //isChangeStdPartPrice
@property (nullable, nonatomic, copy) NSString *isChangeStdPartPrice;
@property (nullable, nonatomic, copy) NSString *vendorName;
@property (nullable, nonatomic, copy) NSString *vendorPartNo;
@property (nullable, nonatomic, copy) NSString *vendorQuoteNo;//partCategorySysName
@property (nullable, nonatomic, copy) NSString *partCategorySysName;
@end

NS_ASSUME_NONNULL_END
