//
//  BillingAddressPOCDetailDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 09/05/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface BillingAddressPOCDetailDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BillingAddressPOCDetailDcs+CoreDataProperties.h"
