//
//  MechanicalSubWorkOrder+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWorkOrder+CoreDataProperties.h"

@implementation MechanicalSubWorkOrder (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWorkOrder *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalSubWorkOrder"];
}

@dynamic subWorkOrderId;
@dynamic workorderId;
@dynamic subWorkOrderNo;
@dynamic companyKey;
@dynamic jobDesc;
@dynamic employeeNo;
@dynamic serviceDateTime;
@dynamic totalEstimationTime;
@dynamic actualTime;
@dynamic subWOStatus;
@dynamic subWOType;
@dynamic isStdPrice;
@dynamic serviceStartStartTime;
@dynamic serviceStartEndTime;
@dynamic invoicePayType;
@dynamic invoiceAmt;
@dynamic taxAmt;
@dynamic discountAmt;
@dynamic totalApprovedAmt;
@dynamic tripChargeMasterId;
@dynamic otherTripChargesAmt;
@dynamic amtPaid;
@dynamic isCompleted;
@dynamic technicianSignaturePath;
@dynamic customerSignaturePath;
@dynamic isCustomerNotPresent;
@dynamic isActive;
@dynamic isClientApprovalReq;
@dynamic isClientApproved;
@dynamic subWorkOrderIssueId;
@dynamic diagnosticChargeMasterId;
@dynamic otherDiagnosticChargesAmt;
@dynamic completeSWO_TechSignPath;
@dynamic completeSWO_CustSignPath;
@dynamic completeSWO_IsCustomerNotPresent;
@dynamic priceNotToExceed;
@dynamic laborPercent;
@dynamic partPercent;
@dynamic createdDate;
@dynamic createdBy;
@dynamic modifiedDate;
@dynamic modifiedBy;
@dynamic departmentSysName;
@dynamic clockStatus;
@dynamic dispatchTime;
@dynamic createdByDevice;
@dynamic totalEstimationTimeInt;
@dynamic pSPCharges;
@dynamic pSPDiscount;
@dynamic inspectionresults;
@dynamic accountPSPId;
@dynamic pSPMasterId;
@dynamic accountNo;

@dynamic tripChargeName;
@dynamic diagnosticChargeName;
@dynamic isIncludeDetailOnInvoice;
@dynamic audioFilePath;
@dynamic propertyType;
@dynamic addressType;
@dynamic isLaborTaxable;
@dynamic isPartTaxable;
@dynamic partPriceType;
@dynamic isVendorPayTax;
@dynamic isChargeTaxOnFullAmount;
@dynamic afterHrsDuration;
@dynamic isHolidayHrs;
@dynamic laborHrsPrice;
@dynamic mileageChargesName;
@dynamic totalMileage;
@dynamic mileageChargesAmt;
@dynamic miscChargeAmt;
@dynamic actualTimeInt;
@dynamic isSendDocWithOutSign;
@dynamic isCreateWOforInCompleteIssue;
@dynamic actualLaborHrsPrice;
@dynamic calculatedActualTimeInt;
@dynamic workOrderNo;
@dynamic officeNotes;
@dynamic laborType;
@dynamic tripChargesQty;

@end
