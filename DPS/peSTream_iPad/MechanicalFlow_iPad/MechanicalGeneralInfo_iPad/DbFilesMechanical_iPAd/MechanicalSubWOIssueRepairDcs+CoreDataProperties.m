//
//  MechanicalSubWOIssueRepairDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWOIssueRepairDcs+CoreDataProperties.h"

@implementation MechanicalSubWOIssueRepairDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWOIssueRepairDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalSubWOIssueRepairDcs"];
}

@dynamic costAdjustment;
@dynamic createdBy;
@dynamic createdDate;
@dynamic customerFeedback;
@dynamic isActive;
@dynamic isCompleted;
@dynamic issueRepairId;
@dynamic isWarranty;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic repairDesc;
@dynamic repairMasterId;
@dynamic repairName;
@dynamic subWorkOrderIssueId;
@dynamic workorderId;
@dynamic createdByDevice;
@dynamic subWorkOrderId;
@dynamic repairLaborId;
@dynamic repairAmt;
@dynamic partCostAdjustment;
@dynamic nonStdRepairAmt;
@dynamic nonStdPartAmt;
@dynamic nonStdLaborAmt;
@dynamic qty;

@end
