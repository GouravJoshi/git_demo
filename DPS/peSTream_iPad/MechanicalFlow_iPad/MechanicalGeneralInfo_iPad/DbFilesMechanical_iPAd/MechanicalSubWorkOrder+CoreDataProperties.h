//
//  MechanicalSubWorkOrder+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWorkOrder+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWorkOrder (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWorkOrder *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *workorderId;//workorderId
@property (nullable, nonatomic, copy) NSString *subWorkOrderNo;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *jobDesc;
@property (nullable, nonatomic, copy) NSString *employeeNo;
@property (nullable, nonatomic, copy) NSString *serviceDateTime;
@property (nullable, nonatomic, copy) NSString *totalEstimationTime;
@property (nullable, nonatomic, copy) NSString *actualTime;
@property (nullable, nonatomic, copy) NSString *subWOStatus;
@property (nullable, nonatomic, copy) NSString *subWOType;
@property (nullable, nonatomic, copy) NSString *isStdPrice;
@property (nullable, nonatomic, copy) NSString *serviceStartStartTime;
@property (nullable, nonatomic, copy) NSString *serviceStartEndTime;
@property (nullable, nonatomic, copy) NSString *invoicePayType;
@property (nullable, nonatomic, copy) NSString *invoiceAmt;
@property (nullable, nonatomic, copy) NSString *taxAmt;
@property (nullable, nonatomic, copy) NSString *discountAmt;
@property (nullable, nonatomic, copy) NSString *totalApprovedAmt;
@property (nullable, nonatomic, copy) NSString *tripChargeMasterId;
@property (nullable, nonatomic, copy) NSString *otherTripChargesAmt;
@property (nullable, nonatomic, copy) NSString *amtPaid;
@property (nullable, nonatomic, copy) NSString *isCompleted;
@property (nullable, nonatomic, copy) NSString *technicianSignaturePath;
@property (nullable, nonatomic, copy) NSString *customerSignaturePath;
@property (nullable, nonatomic, copy) NSString *isCustomerNotPresent;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *isClientApprovalReq;
@property (nullable, nonatomic, copy) NSString *isClientApproved;
@property (nullable, nonatomic, copy) NSString *subWorkOrderIssueId;
@property (nullable, nonatomic, copy) NSString *diagnosticChargeMasterId;
@property (nullable, nonatomic, copy) NSString *otherDiagnosticChargesAmt;
@property (nullable, nonatomic, copy) NSString *completeSWO_TechSignPath;
@property (nullable, nonatomic, copy) NSString *completeSWO_CustSignPath;
@property (nullable, nonatomic, copy) NSString *completeSWO_IsCustomerNotPresent;
@property (nullable, nonatomic, copy) NSString *priceNotToExceed;
@property (nullable, nonatomic, copy) NSString *laborPercent;
@property (nullable, nonatomic, copy) NSString *partPercent;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property(nullable,nonatomic,retain)NSString *departmentSysName;
//clockStatus  dispatchTime
@property (nullable, nonatomic, copy) NSString *clockStatus;
@property (nullable, nonatomic, copy) NSString *dispatchTime;
@property (nullable, nonatomic, copy) NSString *createdByDevice;
@property (nullable, nonatomic, copy) NSString *totalEstimationTimeInt;
@property (nullable, nonatomic, copy) NSString *pSPCharges;
@property (nullable, nonatomic, copy) NSString *pSPDiscount;
@property (nullable, nonatomic, copy) NSString *inspectionresults;//pSPMasterId
@property (nullable, nonatomic, copy) NSString *pSPMasterId;
@property (nullable, nonatomic, copy) NSString *accountPSPId;//AccountNo
@property (nullable, nonatomic, copy) NSString *accountNo;

//SubWork Order New Changes
@property (nullable, nonatomic, copy) NSString *tripChargeName;
@property (nullable, nonatomic, copy) NSString *diagnosticChargeName;
@property (nullable, nonatomic, copy) NSString *isIncludeDetailOnInvoice;
@property (nullable, nonatomic, copy) NSString *audioFilePath;
@property (nullable, nonatomic, copy) NSString *propertyType;
@property (nullable, nonatomic, copy) NSString *addressType;
@property (nullable, nonatomic, copy) NSString *isLaborTaxable;
@property (nullable, nonatomic, copy) NSString *isPartTaxable;
@property (nullable, nonatomic, copy) NSString *partPriceType;
@property (nullable, nonatomic, copy) NSString *isVendorPayTax;
@property (nullable, nonatomic, copy) NSString *isChargeTaxOnFullAmount;
@property (nullable, nonatomic, copy) NSString *afterHrsDuration;
@property (nullable, nonatomic, copy) NSString *isHolidayHrs;
@property (nullable, nonatomic, copy) NSString *laborHrsPrice;
@property (nullable, nonatomic, copy) NSString *mileageChargesName;
@property (nullable, nonatomic, copy) NSString *totalMileage;
@property (nullable, nonatomic, copy) NSString *mileageChargesAmt;
@property (nullable, nonatomic, copy) NSString *miscChargeAmt;//actualTimeInt
@property (nullable, nonatomic, copy) NSString *actualTimeInt;

@property (nullable, nonatomic, copy) NSString *isCreateWOforInCompleteIssue;
@property (nullable, nonatomic, copy) NSString *isSendDocWithOutSign;
@property (nullable, nonatomic, copy) NSString *calculatedActualTimeInt;
@property (nullable, nonatomic, copy) NSString *actualLaborHrsPrice;//workOrderNo
@property (nullable, nonatomic, copy) NSString *workOrderNo;//officeNotes
@property (nullable, nonatomic, copy) NSString *officeNotes;
@property (nullable, nonatomic, copy) NSString *laborType;//tripChargesQty
@property (nullable, nonatomic, copy) NSString *tripChargesQty;

@end

NS_ASSUME_NONNULL_END
