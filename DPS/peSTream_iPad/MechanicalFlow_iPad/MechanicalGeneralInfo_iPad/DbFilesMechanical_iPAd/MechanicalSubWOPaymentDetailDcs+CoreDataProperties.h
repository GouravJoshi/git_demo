//
//  MechanicalSubWOPaymentDetailDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 26/06/17.
//
//

#import "MechanicalSubWOPaymentDetailDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWOPaymentDetailDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWOPaymentDetailDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *checkBackImagePath;
@property (nullable, nonatomic, copy) NSString *checkFrontImagePath;
@property (nullable, nonatomic, copy) NSString *checkNo;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *drivingLicenseNo;
@property (nullable, nonatomic, copy) NSString *expirationDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *paidAmount;
@property (nullable, nonatomic, copy) NSString *paymentMode;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *subWoPaymentId;
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *recieptPath;
@end

NS_ASSUME_NONNULL_END
