//
//  WoOtherDocExtSerDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 12/09/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface WoOtherDocExtSerDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "WoOtherDocExtSerDcs+CoreDataProperties.h"
