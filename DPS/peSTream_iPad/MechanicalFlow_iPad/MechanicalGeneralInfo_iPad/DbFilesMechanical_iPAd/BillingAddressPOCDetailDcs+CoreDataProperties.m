//
//  BillingAddressPOCDetailDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 09/05/18.
//
//

#import "BillingAddressPOCDetailDcs+CoreDataProperties.h"

@implementation BillingAddressPOCDetailDcs (CoreDataProperties)

+ (NSFetchRequest<BillingAddressPOCDetailDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"BillingAddressPOCDetailDcs"];
}

@dynamic companyKey;
@dynamic employeeNo;
@dynamic pocDetails;
@dynamic workorderId;
@dynamic userName;

@end
