//
//  MechanicalSubWorkOrderNoteDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWorkOrderNoteDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWorkOrderNoteDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWorkOrderNoteDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *note;
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *subWorkOrderNoteId;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *createdByDevice;//createdByDevice

@end

NS_ASSUME_NONNULL_END
