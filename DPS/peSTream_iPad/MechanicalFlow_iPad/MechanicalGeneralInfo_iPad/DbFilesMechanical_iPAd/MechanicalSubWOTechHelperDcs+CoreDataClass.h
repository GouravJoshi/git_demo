//
//  MechanicalSubWOTechHelperDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWOTechHelperDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MechanicalSubWOTechHelperDcs+CoreDataProperties.h"
