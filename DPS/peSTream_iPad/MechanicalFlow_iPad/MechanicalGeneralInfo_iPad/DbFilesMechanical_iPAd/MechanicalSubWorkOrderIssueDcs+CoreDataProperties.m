//
//  MechanicalSubWorkOrderIssueDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWorkOrderIssueDcs+CoreDataProperties.h"

@implementation MechanicalSubWorkOrderIssueDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWorkOrderIssueDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalSubWorkOrderIssueDcs"];
}

@dynamic createdBy;
@dynamic createdDate;
@dynamic isActive;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic priority;
@dynamic serviceIssue;
@dynamic subWOIssueRepairPartDcs;
@dynamic subWorkOrderId;
@dynamic subWorkOrderIssueId;
@dynamic workorderId;
@dynamic createdByDevice;
@dynamic equipmentCode;
@dynamic equipmentName;
@dynamic serialNumber;
@dynamic modelNumber;
@dynamic manufacturer;
@dynamic equipmentNo;
@dynamic equipmentCustomName;


@end
