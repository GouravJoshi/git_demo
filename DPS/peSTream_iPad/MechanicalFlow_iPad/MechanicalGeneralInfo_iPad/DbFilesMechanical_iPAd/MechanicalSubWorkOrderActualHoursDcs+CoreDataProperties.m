//
//  MechanicalSubWorkOrderActualHoursDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWorkOrderActualHoursDcs+CoreDataProperties.h"

@implementation MechanicalSubWorkOrderActualHoursDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWorkOrderActualHoursDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalSubWorkOrderActualHoursDcs"];
}

@dynamic actHrsDescription;
@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic isActive;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic reason;
@dynamic subWOActualHourId;
@dynamic subWOActualHourNo;
@dynamic subWorkOrderId;
@dynamic subWorkOrderNo;
@dynamic timeIn;
@dynamic timeOut;
@dynamic workorderId;
@dynamic createdByDevice;
@dynamic status;
@dynamic mobileTimeIn;
@dynamic mobileTimeOut;
@dynamic latitude;
@dynamic longitude;
@dynamic employeeNo;

@end
