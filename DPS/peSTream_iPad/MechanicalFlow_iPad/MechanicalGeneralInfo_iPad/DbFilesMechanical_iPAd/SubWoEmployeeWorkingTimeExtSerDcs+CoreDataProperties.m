//
//  SubWoEmployeeWorkingTimeExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 03/07/18.
//
//

#import "SubWoEmployeeWorkingTimeExtSerDcs+CoreDataProperties.h"

@implementation SubWoEmployeeWorkingTimeExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<SubWoEmployeeWorkingTimeExtSerDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SubWoEmployeeWorkingTimeExtSerDcsTemp"];
}

@dynamic actualAmt;
@dynamic actualDurationInMin;
@dynamic billableAmt;
@dynamic billableDurationInMin;
@dynamic createdBy;
@dynamic createdByDevice;
@dynamic createdDate;
@dynamic employeeName;
@dynamic employeeNo;
@dynamic isActive;
@dynamic isApprove;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic subWOEmployeeWorkingTimeId;
@dynamic subWorkOrderActualHourId;
@dynamic subWorkOrderId;
@dynamic timeSlot;
@dynamic timeSlotTitle;
@dynamic workingDate;
@dynamic workingType;
@dynamic workorderId;
@dynamic isHelper;

@end
