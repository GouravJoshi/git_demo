//
//  MechanicalWoEquipment+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 28/02/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MechanicalWoEquipment : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MechanicalWoEquipment+CoreDataProperties.h"
