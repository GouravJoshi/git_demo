//
//  ServiceDynamicForm+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 05/09/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface ServiceDynamicForm : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ServiceDynamicForm+CoreDataProperties.h"
