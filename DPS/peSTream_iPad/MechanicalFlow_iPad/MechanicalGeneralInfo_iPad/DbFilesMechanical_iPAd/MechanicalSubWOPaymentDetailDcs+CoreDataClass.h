//
//  MechanicalSubWOPaymentDetailDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 26/06/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWOPaymentDetailDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MechanicalSubWOPaymentDetailDcs+CoreDataProperties.h"
