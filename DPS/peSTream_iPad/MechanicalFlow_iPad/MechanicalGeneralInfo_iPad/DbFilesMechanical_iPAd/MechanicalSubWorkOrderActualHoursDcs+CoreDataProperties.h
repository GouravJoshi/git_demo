//
//  MechanicalSubWorkOrderActualHoursDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWorkOrderActualHoursDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWorkOrderActualHoursDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWorkOrderActualHoursDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *actHrsDescription;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *reason;
@property (nullable, nonatomic, copy) NSString *subWOActualHourId;
@property (nullable, nonatomic, copy) NSString *subWOActualHourNo;
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *subWorkOrderNo;
@property (nullable, nonatomic, copy) NSString *timeIn;
@property (nullable, nonatomic, copy) NSString *timeOut;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *createdByDevice;//createdByDevice mobileTimeOut
//status
@property (nullable, nonatomic, copy) NSString *mobileTimeIn;
@property (nullable, nonatomic, copy) NSString *mobileTimeOut;

@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSString *latitude;
@property (nullable, nonatomic, copy) NSString *longitude;//employeeNo
@property (nullable, nonatomic, copy) NSString *employeeNo;

@end

NS_ASSUME_NONNULL_END
