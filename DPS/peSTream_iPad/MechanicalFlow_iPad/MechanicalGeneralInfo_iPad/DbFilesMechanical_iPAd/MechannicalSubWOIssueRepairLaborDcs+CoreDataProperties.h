//
//  MechannicalSubWOIssueRepairLaborDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechannicalSubWOIssueRepairLaborDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechannicalSubWOIssueRepairLaborDcs (CoreDataProperties)

+ (NSFetchRequest<MechannicalSubWOIssueRepairLaborDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *isDefault;
@property (nullable, nonatomic, copy) NSString *issueRepairId;
@property (nullable, nonatomic, copy) NSString *issueRepairLaborId;
@property (nullable, nonatomic, copy) NSString *isWarranty;
@property (nullable, nonatomic, copy) NSString *laborCost;
@property (nullable, nonatomic, copy) NSString *laborCostType;
@property (nullable, nonatomic, copy) NSString *laborDescription;
@property (nullable, nonatomic, copy) NSString *laborHours;
@property (nullable, nonatomic, copy) NSString *laborType;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *createdByDevice;//createdByDevice
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *subWorkOrderIssueId;

@end

NS_ASSUME_NONNULL_END
