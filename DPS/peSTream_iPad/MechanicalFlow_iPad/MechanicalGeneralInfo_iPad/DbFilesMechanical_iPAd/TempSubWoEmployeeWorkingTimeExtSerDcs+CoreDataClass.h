//
//  TempSubWoEmployeeWorkingTimeExtSerDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 03/07/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface TempSubWoEmployeeWorkingTimeExtSerDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TempSubWoEmployeeWorkingTimeExtSerDcs+CoreDataProperties.h"
