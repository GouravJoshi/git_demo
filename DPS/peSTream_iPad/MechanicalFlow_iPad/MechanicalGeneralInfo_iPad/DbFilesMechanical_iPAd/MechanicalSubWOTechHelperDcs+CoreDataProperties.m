//
//  MechanicalSubWOTechHelperDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWOTechHelperDcs+CoreDataProperties.h"

@implementation MechanicalSubWOTechHelperDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWOTechHelperDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalSubWOTechHelperDcs"];
}

@dynamic createdBy;
@dynamic createdDate;
@dynamic employeeNo;
@dynamic isActive;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic subWorkOrderId;
@dynamic subWorkOrderTechnicianId;
@dynamic workorderId;
@dynamic createdByDevice;
@dynamic laborType;

@end
