//
//  MechanicalSubWOPaymentDetailDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 26/06/17.
//
//

#import "MechanicalSubWOPaymentDetailDcs+CoreDataProperties.h"

@implementation MechanicalSubWOPaymentDetailDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWOPaymentDetailDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalSubWOPaymentDetailDcs"];
}

@dynamic checkBackImagePath;
@dynamic checkFrontImagePath;
@dynamic checkNo;
@dynamic createdBy;
@dynamic createdDate;
@dynamic drivingLicenseNo;
@dynamic expirationDate;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic paidAmount;
@dynamic paymentMode;
@dynamic userName;
@dynamic subWoPaymentId;
@dynamic subWorkOrderId;
@dynamic workorderId;
@dynamic recieptPath;

@end
