//
//  MechanicalWoEquipment+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 28/02/18.
//
//

#import "MechanicalWoEquipment+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalWoEquipment (CoreDataProperties)

+ (NSFetchRequest<MechanicalWoEquipment *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *accountItemHistoryId;
@property (nullable, nonatomic, copy) NSString *accountId;
@property (nullable, nonatomic, copy) NSString *itemName;
@property (nullable, nonatomic, copy) NSString *itemcode;
@property (nullable, nonatomic, copy) NSString *subWorkOrderNo;
@property (nullable, nonatomic, copy) NSString *workOrderNo;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSString *modelNumber;
@property (nullable, nonatomic, copy) NSString *manufacturer;
@property (nullable, nonatomic, copy) NSString *qty;
@property (nullable, nonatomic, copy) NSString *installationDate;
@property (nullable, nonatomic, copy) NSString *warrantyExpireDate;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *accountNo;
@property (nullable, nonatomic, copy) NSString *serviceAddressId;
@property (nullable, nonatomic, copy) NSString *barcodeUrl;
@property (nullable, nonatomic, copy) NSString *installedArea;
@property (nullable, nonatomic, copy) NSString *equipmentDesc;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *createdByDevice;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *itemCustomName;//itemNo
@property (nullable, nonatomic, copy) NSString *itemNo;

@end

NS_ASSUME_NONNULL_END
