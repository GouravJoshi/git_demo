//
//  MechanicalSubWorkOrderIssueDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWorkOrderIssueDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalSubWorkOrderIssueDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWorkOrderIssueDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *priority;
@property (nullable, nonatomic, copy) NSString *serviceIssue;
@property (nullable, nonatomic, copy) NSString *subWOIssueRepairPartDcs;
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *subWorkOrderIssueId;
@property (nullable, nonatomic, copy) NSString *workorderId;//createdByDevice
@property (nullable, nonatomic, copy) NSString *createdByDevice;//createdByDevice
@property (nullable, nonatomic, copy) NSString *equipmentCode;//createdByDevice
@property (nullable, nonatomic, copy) NSString *equipmentName;//createdByDevice
@property (nullable, nonatomic, copy) NSString *serialNumber;//createdByDevice
@property (nullable, nonatomic, copy) NSString *modelNumber;//createdByDevice
@property (nullable, nonatomic, copy) NSString *manufacturer;//createdByDevice
@property (nullable, nonatomic, copy) NSString *equipmentCustomName;
@property (nullable, nonatomic, copy) NSString *equipmentNo;

@end

NS_ASSUME_NONNULL_END
