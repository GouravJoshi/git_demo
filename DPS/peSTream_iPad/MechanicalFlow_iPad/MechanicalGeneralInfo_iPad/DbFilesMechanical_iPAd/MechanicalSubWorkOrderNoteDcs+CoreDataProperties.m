//
//  MechanicalSubWorkOrderNoteDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 26/05/17.
//
//

#import "MechanicalSubWorkOrderNoteDcs+CoreDataProperties.h"

@implementation MechanicalSubWorkOrderNoteDcs (CoreDataProperties)

+ (NSFetchRequest<MechanicalSubWorkOrderNoteDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalSubWorkOrderNoteDcs"];
}

@dynamic createdBy;
@dynamic createdDate;
@dynamic isActive;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic note;
@dynamic subWorkOrderId;
@dynamic subWorkOrderNoteId;
@dynamic workorderId;
@dynamic createdByDevice;

@end
