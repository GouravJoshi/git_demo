//
//  MechanicalWOs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 17/04/19.
//
//

#import "MechanicalWOs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalWOs (CoreDataProperties)

+ (NSFetchRequest<MechanicalWOs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *workorderId;

@end

NS_ASSUME_NONNULL_END
