//
//  ServiceAddressPOCDetailDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 09/05/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface ServiceAddressPOCDetailDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ServiceAddressPOCDetailDcs+CoreDataProperties.h"
