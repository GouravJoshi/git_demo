//
//  MechanicalGeneralInfoViewController.h
//  DPS peSTream Quactio
//  peSTream peSTream Quacito 2018
//  Created by Saavan Patidar on 25/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream change for commiting

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"
#import "RKTagsView.h"

@interface MechanicalGeneralInfoViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,RKTagsViewDelegate,UITextViewDelegate,NSFetchedResultsControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,MFMessageComposeViewControllerDelegate>
{//
    AppDelegate *appDelegate;
    NSEntityDescription *entityWorkOrder;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    NSEntityDescription *entityImageDetail,*entityModifyDateServiceAuto,*entityEmailDetailService,*entityMechanicalSubWorkOrderActualHrs,*entitySubWorkOrderIssues,*entityMechanicalSubWorkOrderHelper;
    NSFetchRequest *requestImageDetail;
    NSSortDescriptor *sortDescriptorImageDetail;
    NSArray *sortDescriptorsImageDetail;
    NSManagedObject *matchesImageDetail;
    NSArray *arrAllObjImageDetail;
    
    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;
    
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    
    NSEntityDescription *entityMechanicalSubWorkOrder,*entityMechanicalServiceAddressPOCDetailDcs,*entityMechanicalBillingAddressPOCDetailDcs;
    NSFetchRequest *requestMechanicalSubWorkOrder,*requestMechanicalServiceAddressPOCDetailDcs,*requestMechanicalBillingAddressPOCDetailDcs;
    NSSortDescriptor *sortDescriptorMechanicalSubWorkOrder,*sortDescriptorMechanicalServiceAddressPOCDetailDcs,*sortDescriptorMechanicalBillingAddressPOCDetailDcs;
    NSArray *sortDescriptorsMechanicalSubWorkOrder,*sortDescriptorsMechanicalServiceAddressPOCDetailDcs,*sortDescriptorsMechanicalBillingAddressPOCDetailDcs;
    NSManagedObject *matchesMechanicalSubWorkOrder,*matchesMechanicalServiceAddressPOCDetailDcs,*matchesMechanicalBillingAddressPOCDetailDcs;
    NSArray *arrAllObjMechanicalSubWorkOrder,*arrAllObjMechanicalServiceAddressPOCDetailDcs,*arrAllObjMechanicalBillingAddressPOCDetailDcs;
    
    // Chnages for mechanical status
    //NSEntityDescription *entityMechanicalSubWorkOrderActualHrs;
    NSFetchRequest *requestSubWorkOrderActualHrs;
    NSSortDescriptor *sortDescriptorSubWorkOrderActualHrs;
    NSArray *sortDescriptorsSubWorkOrderActualHrs;
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewGeneralInfo;
@property (strong, nonatomic) IBOutlet UILabel *lblAccountNamenNo;
@property (strong, nonatomic) IBOutlet UIView *view_OtherInfo;
@property (strong, nonatomic) IBOutlet UIView *view_AddressInfo;
@property (strong, nonatomic) IBOutlet UIView *view_WorkOrderInfo;
@property (strong, nonatomic) IBOutlet UIView *view_CustomerInfo;
@property (strong, nonatomic) IBOutlet UIView *view_BeforeImage;
@property (strong, nonatomic) IBOutlet UIView *view_SavenContinue;
@property (strong, nonatomic) IBOutlet UIView *view_SubWorkOredr;
@property (strong, nonatomic) IBOutlet UITableView *tblViewSUbWorkOrder;
- (IBAction)action_back:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtViewTechComment;
@property (strong, nonatomic) IBOutlet UITextView *txtViewNotes;
@property (strong, nonatomic) IBOutlet UILabel *lblCustName;
@property (strong, nonatomic) IBOutlet UILabel *lblCompanyName;
- (IBAction)action_PrimaryEmail:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnPrimaryEmail;
- (IBAction)action_SecondaryEmail:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSecondaryEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPrimaryPhone;
@property (strong, nonatomic) IBOutlet UITextField *txtSecondaryPhone;
- (IBAction)action_SavenContinue:(id)sender;
- (IBAction)action_Cancel:(id)sender;
- (IBAction)action_BeforeImages:(id)sender;
@property (strong, nonatomic) IBOutlet UICollectionView *beforeImageCollectionView;
@property (strong, nonatomic) IBOutlet UILabel *lblWorkOrderNo;
@property (strong, nonatomic) IBOutlet UILabel *lblAccountNo;
@property (strong, nonatomic) IBOutlet UILabel *lblCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblServices;
@property (strong, nonatomic) IBOutlet UILabel *lblScheduleStartDatenTime;
@property (strong, nonatomic) IBOutlet UILabel *lblScheduleEndDatenTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEmpNo;
@property (strong, nonatomic) IBOutlet UILabel *lblEmpName;
@property (strong, nonatomic) IBOutlet UILabel *lblRouteNo;
@property (strong, nonatomic) IBOutlet UILabel *lblRouteName;
@property (strong, nonatomic) IBOutlet UILabel *lblBillingAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblBillingMapCode;
@property (strong, nonatomic) IBOutlet UILabel *lblBillingGateCode;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceMapCode;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceGateCode;
@property (strong, nonatomic) IBOutlet UILabel *lblSpecialInstructions;
@property (strong, nonatomic) IBOutlet UILabel *lblDirections;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceInstructions;
@property (strong, nonatomic) IBOutlet UILabel *lblOtherInstructions;
@property (strong, nonatomic) IBOutlet UIButton *btnClientApproval;
@property (strong, nonatomic) IBOutlet UILabel *lblAccountDescriptions;
- (IBAction)action_ClientApproval:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceEmail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalSubWorkOrder;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs;

@property (strong, nonatomic) IBOutlet UIView *viewFinalSavenContinue;
@property (strong, nonatomic) IBOutlet UILabel *lblStatusClientApproval;

//21 Nov Nilind POC CHANGES
//@property (weak, nonatomic) IBOutlet UITextField *txtCellNo;
@property (weak, nonatomic) IBOutlet UILabel *billingContactName;
@property (weak, nonatomic) IBOutlet UILabel *billingContactCellNo;
@property (weak, nonatomic) IBOutlet UILabel *billingContactMapCode;

@property (weak, nonatomic) IBOutlet UILabel *billingContactPrimaryEmail;

@property (weak, nonatomic) IBOutlet UILabel *billingContactSecondayEmail;
@property (weak, nonatomic) IBOutlet UILabel *billingContactPrimaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *billingContactSecondaryPhone;


@property (weak, nonatomic) IBOutlet UILabel *serviceContactName;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactCellNo;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactMapCode;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactPrimaryEmail;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactSecondaryEmail;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactPrimaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactSecondaryPhone;
//@property (weak, nonatomic) IBOutlet UILabel *serviceContactGateCode;
//@property (weak, nonatomic) IBOutlet UILabel *serviceContactNotes;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadServiceAddImage;
- (IBAction)actionOnUploadServiceAddImage:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewServiceAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtCustomerPO;

@property (strong, nonatomic) IBOutlet UIButton *btnServiceJobDescriptionTitle;
- (IBAction)action_ServiceJobDescriptionTitle:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textView_ServiceJobDescriptions;
@property (strong, nonatomic) IBOutlet UIView *viewServiceJobDescriptions;
@property (strong, nonatomic) IBOutlet UIButton *btnBillingArrow;
- (IBAction)action_BillingArrow:(id)sender;
- (IBAction)action_ServiceArrow:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constant_BillingHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constant_serviceHeight;
@property (strong, nonatomic) IBOutlet UIButton *btnServiceArrow;
@property (strong, nonatomic) IBOutlet UIButton *btnExpandCustomerInfo;
- (IBAction)action_btnExpandCustomerInfo:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constant_CustomerInfo_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constant_WorkOrderInfo_H;
@property (strong, nonatomic) IBOutlet UIButton *btnExpandWorkOrderInfo;
- (IBAction)action_btnExpandWorkOrderInfo:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnExpandSubWorkOrderInf;
- (IBAction)action_btnExpandSubWorkOrderInf:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constant_tblViewSubWorkorder_H;
@property (strong, nonatomic) IBOutlet UIButton *btnExpanBillingAddress;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constant_BillingAddress_H;
- (IBAction)action_btnExpanBillingAddress:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnExpandServiceAddress;
- (IBAction)action_btnExpandServiceAddress:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constant_ServiceAddres_H;
@property (strong, nonatomic) IBOutlet UIView *view_ServiceAddress;
- (IBAction)action_BillingContact:(id)sender;
- (IBAction)action_BillingCell:(id)sender;
- (IBAction)action_BillingPrimaryEmail:(id)sender;
- (IBAction)action_BillingSecondaryEmail:(id)sender;
- (IBAction)action_BillingPrimaryPhone:(id)sender;
- (IBAction)action_BillingSecondaryPhone:(id)sender;
- (IBAction)action_BillingAddress:(id)sender;
- (IBAction)action_ServiceContact:(id)sender;
- (IBAction)action_ServiceCell:(id)sender;
- (IBAction)action_ServicePrimaryEmail:(id)sender;
- (IBAction)action_ServiceSecondaryEmail:(id)sender;
- (IBAction)action_ServicePrimaryPhone:(id)sender;
- (IBAction)action_ServiceSecondaryPhone:(id)sender;
- (IBAction)action_ServiceAddress:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEditCustomerInfo;
- (IBAction)action_btnEditCustomerInfo:(id)sender;
- (IBAction)action_PrimaryPhone:(id)sender;
- (IBAction)action_SecondaryPhone:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnPrimaryPhone;
@property (strong, nonatomic) IBOutlet UIButton *btnSecondaryPhone;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentStatus;
@property (strong, nonatomic) IBOutlet UIButton *btn_ActionCurrentStatus;
- (IBAction)action_CurrentStatus:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btn_ServiceAddressPOCDetailDcs;
- (IBAction)action_ServiceAddressPOCDetailDcs:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_BillingAddressPOCDetailDcs;
- (IBAction)action_BillingAddressPOCDetailDcs:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAddSubWorkOrder;
- (IBAction)action_AddSubWorkOrder:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnTextMsg;
- (IBAction)action_textMsg:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
- (IBAction)action_EmailMsg:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceAutomation;
@property (strong, nonatomic) IBOutlet UILabel *lblBillingCounty;
@property (strong, nonatomic) IBOutlet UILabel *lblBillingSchoolDistrict;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceCounty;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceSchoolDistrict;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderActualHrs;

@property (strong, nonatomic) IBOutlet UILabel *lblDriveTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEarliestStartTime;
@property (strong, nonatomic) IBOutlet UILabel *lblLatestStartTime;


@end
