//
//  EquipmentsViewController.m
//  DPS
//  peSTream
//  Created by Saavan Patidar on 16/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "MechanicalEquipmentsViewControlleriPad.h"
#import "AllImportsViewController.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "EquipmentsListTableViewCell.h"
#import "ServiceDetailAppointmentView.h"
#import "InvoiceAppointmentView.h"
#import <SafariServices/SafariServices.h>
#import "AllImportsViewController.h"

@interface MechanicalEquipmentsViewControlleriPad ()
{
    int globalIndexForNxtserviceDate, indexMultipleSelectTosetValue;;
    Global *global;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    UITableView *tblData;
    NSMutableArray *arrDataTblView,*arrOfEquipmentList,*arrOfNextServiceDateLocally,*arrOfAssociatedWithLocally,*filteredArrOfAssoicatedWithToDisplay,*arrResponseInspection,*arrOfIndexes,*arrOfControlssMain,*arraySelecteValues,*arraySelecteValuesToShow,*arrOfSerialNosGlobal,*arrOfModelNosGlobal,*arrOfManufacturerGlobal,*arrOfSerialNosFiltered,*arrOfModelNosFiltered,*arrOfManufacturerFiltered;
    NSDictionary *dictDetailsFortblView,*dictDataInventory;
    NSString *strDateEquip,*strInstalledOn,*strNextServiceDate,*strWarrantyDate,*strCategoryId,*strEquipmentValueId,*strWorkOrderId,*strGlobalWorkOrderStatus,*strNextServiceDateGlobal,*strAccountNoGlobal,*strDepartmentIdGlobal,*strCategorySysName,*strItemSysName,*strAreaSysName,*strAssociatedWith,*strAssociatedWithId,*strEquipmentNumber,*strCurrentDateGlobal,*strItemNumber,*strGlobalWoEquipIdToFetchDynamicData,*strEmpID,*strCompanyKey,*strCompanyId,*strServiceUrlMain,*strUserName,*strGlobalCatSysNameToFetchDynamicForm,*strGlobalDateToShow,*strGlobalDatenTime,*strGlobalDropDownSelectedValue,*strCompanyKeyy,*strDepartmentSysNameWo,*strGlobalCellForRowNxtServiceDate,*strCompanyIdCore,*strGlobalBarcodeURL,*strEquipIdToGenerateBarcode;
    BOOL yesEditedSomething , isMultipleSelect, isWoEquipIdPrsent, isSerialNo, isModelNo, isManufacturer,isScannedResult, isUpdateEquip, isCompletedStatusMechanical;
    
    NSMutableArray *arrOfMechanicalWoEquipment;
    NSManagedObject *objMechanicalEquipFetchedViaId;
    
}

@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (nonatomic, weak) IBOutlet UIButton *toggleScanningButton;
@property (nonatomic, assign) BOOL captureIsFrozen;
@property (nonatomic, assign) BOOL didShowCaptureWarning;
@property (nonatomic, weak) IBOutlet UIButton *toggleTorchButton;

@end

@implementation MechanicalEquipmentsViewControlleriPad

- (void)viewDidLoad {
    
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
        [self.navigationController.navigationBar setHidden:YES];
        
    }else{
        
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
        [self.navigationController.navigationBar setHidden:YES];
        
    }

    global = [[Global alloc] init];
    
    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strGlobalWorkOrderStatus];
    
    isMultipleSelect=NO;
    isWoEquipIdPrsent=NO;
    isScannedResult=NO;
    isSerialNo=NO;
    isModelNo=NO;
    isManufacturer=NO;
    isUpdateEquip=NO;
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    _lbl_LeadnAccNo.text=[defsLead valueForKey:@"lblName"];//AccNoService
    strAccountNoGlobal=[defsLead valueForKey:@"AccNoService"];//DepartmentIdService
    strDepartmentIdGlobal=[defsLead valueForKey:@"DepartmentIdService"];
    strWorkOrderId=[defsLead valueForKey:@"LeadId"];
    strGlobalWorkOrderStatus=_strStatusWo;
    strGlobalWorkOrderStatus=[_objSubWorkOrderDetail valueForKey:@"subWOStatus"];
    strDepartmentSysNameWo=[defsLead valueForKey:@"departmentSysNameWO"];;
    yesEditedSomething=NO;
    indexMultipleSelectTosetValue=-100000;
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    //ServiceAutoCompanyId
    strCompanyId      =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ServiceAutoCompanyId"]];
    strCompanyIdCore =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.CoreServiceModule.CompanyId"]];

    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    strCurrentDateGlobal = [formatterDate stringFromDate:[NSDate date]];
    
    //Alloc Init
    globalIndexForNxtserviceDate=-1;
    arrOfEquipmentList=[[NSMutableArray alloc]init];
    arrOfNextServiceDateLocally=[[NSMutableArray alloc]init];
    filteredArrOfAssoicatedWithToDisplay=[[NSMutableArray alloc]init];
    arrOfAssociatedWithLocally=[[NSMutableArray alloc]init];
    
    arrayViews = [[NSMutableArray alloc] init];
    arrayOfButtonsMain = [[NSMutableArray alloc] init];
    arrOfHeightsViews = [[NSMutableArray alloc] init];
    arrOfYAxisViews = [[NSMutableArray alloc] init];
    arrOfButtonImages = [[NSMutableArray alloc] init];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    dictDetailsFortblView=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    _txtView_Notes.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_Notes.layer.borderWidth=1.0;
    _txtView_Notes.layer.cornerRadius=5.0;
    
    _view_IntsallNewEquipments.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _view_IntsallNewEquipments.layer.borderWidth=2.0;
    _view_IntsallNewEquipments.layer.cornerRadius=5.0;
    
    _view_SavenCancelButtons.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _view_SavenCancelButtons.layer.borderWidth=2.0;
    _view_SavenCancelButtons.layer.cornerRadius=5.0;
    
    _tblViewTxtSearchResults.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _tblViewTxtSearchResults.layer.borderWidth=2.0;
    _tblViewTxtSearchResults.layer.cornerRadius=5.0;
    
    
    //    _txt_ModelNo.layer.borderColor=[[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor];
    //    _txt_ModelNo.layer.borderWidth=2.0;
    //    _txt_ModelNo.layer.cornerRadius=5.0;
    
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //[pickerDate setMinimumDate:[NSDate date]];
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    _tblViewTxtSearchResults.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    _equipmentTblview.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [_scrollVieww addGestureRecognizer:singleTapGestureRecognizer];
    
    //============================================================================
    //============================================================================
    
    [self fetchWoEquipmentFromDB];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self getSerialNumber];
    

    isCompletedStatusMechanical=[global isCompletedSatusMechanical:strGlobalWorkOrderStatus];
    
    if (isCompletedStatusMechanical) {
        [_btnAddEquip setEnabled:NO];
    }else{
        [_btnAddEquip setEnabled:YES];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.scanner stopScanning];
    [super viewWillDisappear:animated];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Core Data Fetch Methods-----------------
//============================================================================
//============================================================================


-(void)fetchWoEquipmentFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextWoEquipment = [appDelegate managedObjectContext];
    entityWoEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:contextWoEquipment];
    requestWoEquipment = [[NSFetchRequest alloc] init];
    [requestWoEquipment setEntity:entityWoEquipment];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestWoEquipment setPredicate:predicate];
    
    sortDescriptorWoEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWoEquipment = [NSArray arrayWithObject:sortDescriptorWoEquipment];
    
    [requestWoEquipment setSortDescriptors:sortDescriptorsWoEquipment];
    
    self.fetchedResultsControllerWoEquipmentService = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoEquipment managedObjectContext:contextWoEquipment sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoEquipmentService setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoEquipmentService performFetch:&error];
    arrAllObjWoEquipment = [self.fetchedResultsControllerWoEquipmentService fetchedObjects];
    if ([arrAllObjWoEquipment count] == 0)
    {
        
    }
    else
    {
        
        arrOfMechanicalWoEquipment=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjWoEquipment.count; k++){
            
            NSManagedObject *objTemp=arrAllObjWoEquipment[k];
            
            NSString *strIsActiveOrNot=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isActive"]];
            
            if ([strIsActiveOrNot isEqualToString:@"true"] || [strIsActiveOrNot isEqualToString:@"True"] || [strIsActiveOrNot isEqualToString:@"1"]) {
                
                [arrOfMechanicalWoEquipment addObject:arrAllObjWoEquipment[k]];
                
            }
        }
        [_equipmentTblview reloadData];
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchWoEquipmentFromDBViaEquipId :(NSString*)strEquipId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextWoEquipment = [appDelegate managedObjectContext];
    entityWoEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:contextWoEquipment];
    requestWoEquipment = [[NSFetchRequest alloc] init];
    [requestWoEquipment setEntity:entityWoEquipment];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND accountItemHistoryId = %@",strWorkOrderId,strEquipId];
    
    [requestWoEquipment setPredicate:predicate];
    
    sortDescriptorWoEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWoEquipment = [NSArray arrayWithObject:sortDescriptorWoEquipment];
    
    [requestWoEquipment setSortDescriptors:sortDescriptorsWoEquipment];
    
    self.fetchedResultsControllerWoEquipmentService = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoEquipment managedObjectContext:contextWoEquipment sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoEquipmentService setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoEquipmentService performFetch:&error];
    arrAllObjWoEquipmentNew = [self.fetchedResultsControllerWoEquipmentService fetchedObjects];
    if ([arrAllObjWoEquipmentNew count] == 0)
    {
        
        
        
    }
    else
    {
        _btn_EquipmentsName.enabled=NO;
        _btn_Category.enabled=NO;
        
        objMechanicalEquipFetchedViaId=arrAllObjWoEquipmentNew[0];
        
        [_btn_EquipmentsName setTitle:[objMechanicalEquipFetchedViaId valueForKey:@"itemName"] forState:UIControlStateNormal];
        strItemSysName=[NSString stringWithFormat:@"%@",[objMechanicalEquipFetchedViaId valueForKey:@"itemcode"]];
        _txt_Manufaturer.text=[NSString stringWithFormat:@"%@",[objMechanicalEquipFetchedViaId valueForKey:@"manufacturer"]];
        _txt_ModelNo.text=[NSString stringWithFormat:@"%@",[objMechanicalEquipFetchedViaId valueForKey:@"modelNumber"]];
        _txtNameEquip.text=[NSString stringWithFormat:@"%@",[objMechanicalEquipFetchedViaId valueForKey:@"itemCustomName"]];

        _txt_SerialNo.text=[NSString stringWithFormat:@"%@",[objMechanicalEquipFetchedViaId valueForKey:@"serialNumber"]];
        [_btn_Area setTitle:[objMechanicalEquipFetchedViaId valueForKey:@"installedArea"] forState:UIControlStateNormal];
        [_btn_WarrantyDate setTitle:[global ChangeDateMechanicalEquipment:[objMechanicalEquipFetchedViaId valueForKey:@"warrantyExpireDate"]] forState:UIControlStateNormal];
        [_btn_InstalledOn setTitle:[global ChangeDateMechanicalEquipment:[objMechanicalEquipFetchedViaId valueForKey:@"installationDate"]] forState:UIControlStateNormal];
        strInstalledOn=[objMechanicalEquipFetchedViaId valueForKey:@"installationDate"];
        strInstalledOn=[global ChangeDateMechanicalEquipment:[objMechanicalEquipFetchedViaId valueForKey:@"installationDate"]];
        _txtView_Notes.text=[NSString stringWithFormat:@"%@",[objMechanicalEquipFetchedViaId valueForKey:@"equipmentDesc"]];
        strAreaSysName=[objMechanicalEquipFetchedViaId valueForKey:@"installedArea"];
        strWarrantyDate=[objMechanicalEquipFetchedViaId valueForKey:@"warrantyExpireDate"];

        [self getCategoryDetail:strItemSysName];
        
        _view_IntsallNewEquipments.frame=CGRectMake(0, 20,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
        [self.view addSubview:_view_IntsallNewEquipments];

    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)getCategoryDetail :(NSString*)strScannedResult{
    
    NSArray *arrDataTblViewLocal=[self getEquipMentss];
    
    NSDictionary *dictData;
    BOOL isPartPresent;
    
    isPartPresent=NO;

    for (int k1=0; k1<arrDataTblViewLocal.count; k1++) {
        
        dictData=[arrDataTblViewLocal objectAtIndex:k1];
        
        NSString *strItemNumberNew=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        if ([strScannedResult caseInsensitiveCompare:strItemNumberNew] == NSOrderedSame) {
            isPartPresent=YES;
            break;
        }
    }

    
    NSString *strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
    
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        NSString *strCategorySysNameNew=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
        
        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartmentSysNameWo isEqualToString:strDepartmentSysName])) {
            
            if ([strCategorySysNameNew isEqualToString:strCategorySysNameSelected]) {
                
                [_btn_Category setTitle:[dictDataa valueForKey:@"Name"] forState:UIControlStateNormal];
                strCategoryId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryMasterId"]];
                strCategorySysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
                
            }
            
        }
    }

}

-(NSMutableArray*)getEquipMentss{
    
    NSMutableArray *arrDataTblViewLocal;
    NSString *strEquipmentValueIdLocal;
    arrDataTblViewLocal=[[NSMutableArray alloc]init];
    
    NSArray *arrOfAreaMasterEquipmentTypes=[dictDetailsFortblView valueForKey:@"EquipmentTypes"];
    NSString *strEqupMentTypeValue;
    for (int k=0; k<arrOfAreaMasterEquipmentTypes.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMasterEquipmentTypes[k];
        
        NSString *strEqupMentTypeName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Text"]];
        
        if ([strEqupMentTypeName isEqualToString:@"Equipment"]) {
            
            strEqupMentTypeValue=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Value"]];
            
        }
        
    }
    
    if (strEqupMentTypeValue.length==0) {
        
        strEquipmentValueIdLocal=@"1";
        
        
    } else {
        
        strEquipmentValueIdLocal=strEqupMentTypeValue;
        
    }
    
    arrDataTblViewLocal=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryItems"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strEquipmentIdToCheck=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemType"]];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        if ([strEquipmentIdToCheck isEqualToString:strEquipmentValueIdLocal] && ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"])) {
            
            [arrDataTblViewLocal addObject:dictDataa];
            
        }
    }
    
    return arrDataTblViewLocal;
    
}


-(void)fetchWoEquipmentFromDBViaEquipIdToGenerateBarcode{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextWoEquipment = [appDelegate managedObjectContext];
    entityWoEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:contextWoEquipment];
    requestWoEquipment = [[NSFetchRequest alloc] init];
    [requestWoEquipment setEntity:entityWoEquipment];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND accountItemHistoryId = %@",strWorkOrderId,strEquipIdToGenerateBarcode];
    
    [requestWoEquipment setPredicate:predicate];
    
    sortDescriptorWoEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWoEquipment = [NSArray arrayWithObject:sortDescriptorWoEquipment];
    
    [requestWoEquipment setSortDescriptors:sortDescriptorsWoEquipment];
    
    self.fetchedResultsControllerWoEquipmentService = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoEquipment managedObjectContext:contextWoEquipment sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoEquipmentService setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoEquipmentService performFetch:&error];
    arrAllObjWoEquipmentNew = [self.fetchedResultsControllerWoEquipmentService fetchedObjects];
    if ([arrAllObjWoEquipmentNew count] == 0)
    {
        
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        
        NSManagedObject *objTemp =arrAllObjWoEquipmentNew[0];
        
        NSArray *arrLeadDetailKey;
        arrLeadDetailKey = [[[objTemp entity] attributesByName] allKeys];
        
        NSMutableArray *arrPaymentInfoValue;
        
        arrPaymentInfoValue=[[NSMutableArray alloc]init];
        for (int i=0; i<arrLeadDetailKey.count; i++)
        {
            NSString *str;
            str=([[objTemp valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTemp valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
            if([str isEqual:nil] || str.length==0 )
            {
                str=@"";
            }
            [arrPaymentInfoValue addObject:str];
        }

        NSDictionary *dictEquipObj = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrLeadDetailKey];
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:dictEquipObj])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictEquipObj options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Final dictEquipObj  JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMain,UrlGenerateEquipBarcode,strCompanyKey];
        
        NSURL *url = [NSURL URLWithString:strUrl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:requestData];

        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        if (ResponseDict.count==0)
        {
            
            [DejalBezelActivityView removeView];
            [global AlertMethod:Alert :@"Unable to generate barcode.Please try again later"];

            
        }else if ([ResponseDict isKindOfClass:[NSDictionary class]]){
            
            NSString *strUrll=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"BarcodeUrl"]];
            NSString *stritemNo=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"ItemNo"]];

            if ([strUrll isEqualToString:@"(null)"] || [strUrll isEqualToString:@"<null>"]) {
                
                [global AlertMethod:Alert :@"Unable to generate barcode.Please try again later"];
                [DejalBezelActivityView removeView];

                
            }else{
                
                [objTemp setValue:strUrll forKey:@"barcodeUrl"];
                [objTemp setValue:stritemNo forKey:@"itemNo"];
                NSError *error2;
                [context save:&error2];
                
                [self fetchWoEquipmentFromDB];
                
                [DejalBezelActivityView removeView];

            }

        }else{
            
            [global AlertMethod:Alert :@"Unable to generate barcode.Please try again later"];
            [DejalBezelActivityView removeView];

        }

    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchWoEquipmentFromDBViaEquipIdToDelete :(NSString*)strEquipIdToDeleteBarcode{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextWoEquipment = [appDelegate managedObjectContext];
    entityWoEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:contextWoEquipment];
    requestWoEquipment = [[NSFetchRequest alloc] init];
    [requestWoEquipment setEntity:entityWoEquipment];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND accountItemHistoryId = %@",strWorkOrderId,strEquipIdToDeleteBarcode];
    
    [requestWoEquipment setPredicate:predicate];
    
    sortDescriptorWoEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWoEquipment = [NSArray arrayWithObject:sortDescriptorWoEquipment];
    
    [requestWoEquipment setSortDescriptors:sortDescriptorsWoEquipment];
    
    self.fetchedResultsControllerWoEquipmentService = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoEquipment managedObjectContext:contextWoEquipment sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoEquipmentService setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoEquipmentService performFetch:&error];
    arrAllObjWoEquipmentNew = [self.fetchedResultsControllerWoEquipmentService fetchedObjects];
    if ([arrAllObjWoEquipmentNew count] == 0)
    {
        
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        
        NSManagedObject *objTemp =arrAllObjWoEquipmentNew[0];
        
        NSString *strAccountItemHistoryId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"accountItemHistoryId"]];
        NSString *strCreatedByDevice=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"createdByDevice"]];

        if ([strAccountItemHistoryId containsString:@"iOS"] && [strCreatedByDevice isEqualToString:@"Mobile"]) {
            
            [self deleteFromCoreDataEquipments:strEquipIdToDeleteBarcode];
            
        } else {
            
            [objTemp setValue:@"false" forKey:@"isActive"];
            [objTemp setValue:@"Mobile" forKey:@"createdByDevice"];
            
            NSError *error2;
            [context save:&error2];
            
        }
        
        [self fetchWoEquipmentFromDB];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}



-(BOOL)fetchWoEquipmentFromDBToCheckIfExist :(NSString*)strEquipIdToDeleteBarcode :(NSString*)strEquipItemcode :(NSString*)strEquipManufacturer :(NSString*)strEquipModelNumber :(NSString*)strEquipSerialNumber :(NSString*)strEquipAccountNo :(NSString*)strEquipServiceAddressId :(NSString*)strTypee :(NSString*)strEuipNameCustom{
    
    BOOL isExist;
    
    isExist=YES;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextWoEquipment = [appDelegate managedObjectContext];
    entityWoEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:contextWoEquipment];
    requestWoEquipment = [[NSFetchRequest alloc] init];
    [requestWoEquipment setEntity:entityWoEquipment];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND itemcode = %@ AND manufacturer = %@ AND modelNumber = %@ AND serialNumber = %@ AND accountNo = %@ AND serviceAddressId = %@ AND (isActive = %@ || isActive = %@)",strWorkOrderId,strEquipItemcode,strEquipManufacturer,strEquipModelNumber,strEquipSerialNumber,strEquipAccountNo,strEquipServiceAddressId,@"true",@"True"];
    
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND itemcode = %@ AND (manufacturer = %@ || manufacturer != %@) AND (modelNumber = %@ || modelNumber != %@) AND (serialNumber = %@ || serialNumber != %@) AND accountNo = %@ AND serviceAddressId = %@",strWorkOrderId,strEquipItemcode,strEquipManufacturer,@"",strEquipModelNumber,@"",strEquipSerialNumber,@"",strEquipAccountNo,strEquipServiceAddressId];

    [requestWoEquipment setPredicate:predicate];
    
    sortDescriptorWoEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWoEquipment = [NSArray arrayWithObject:sortDescriptorWoEquipment];
    
    [requestWoEquipment setSortDescriptors:sortDescriptorsWoEquipment];
    
    self.fetchedResultsControllerWoEquipmentService = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoEquipment managedObjectContext:contextWoEquipment sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoEquipmentService setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoEquipmentService performFetch:&error];
    arrAllObjWoEquipmentNew = [self.fetchedResultsControllerWoEquipmentService fetchedObjects];
    if ([arrAllObjWoEquipmentNew count] == 0)
    {
        
        [DejalBezelActivityView removeView];
        
        isExist=NO;
        
    }
    else
    {
        
        for (int k=0; k<arrAllObjWoEquipmentNew.count; k++) {
            
            NSManagedObject *objTemp =arrAllObjWoEquipmentNew[k];
            NSString *strAccountItemHistoryId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"accountItemHistoryId"]];
            if ([strTypee isEqualToString:@"update"]) {
                
                if ([strAccountItemHistoryId isEqualToString:strEquipIdToDeleteBarcode]) {
                    
                    isExist=NO;
                    break;
                }else{
                    
                    isExist=YES;
                    
                }
                
            }
            
        }
        
        /*
        
        // Logic For blank Serial Model and  Manufacturer
        
        for (int k1=0; k1<arrAllObjWoEquipmentNew.count; k1++) {
            
            NSManagedObject *objTemp =arrAllObjWoEquipmentNew[k1];
            
            NSString *strManufacturerLocal=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"manufacturer"]];
            NSString *strModelNumberLocal=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"modelNumber"]];
            NSString *strSerialNumberLocal=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"serialNumber"]];

            if ((strManufacturerLocal.length==0) && (strEquipManufacturer.length==0)) {
                
                
                isExist=NO;
                break;
            }
            if ((strModelNumberLocal.length==0) && (strEquipModelNumber.length==0)) {
                
                
                isExist=NO;
                break;
            }
            if ((strSerialNumberLocal.length==0) && (strEquipSerialNumber.length==0)) {
                
                
                isExist=NO;
                break;
            }
            
        }
        
        */
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return isExist;
}

-(BOOL)fetchWoEquipmentFromDBToCheckIfSameNameExist :(NSString*)strEuipNameCustom{
    
    BOOL isExist;
    
    isExist=YES;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextWoEquipment = [appDelegate managedObjectContext];
    entityWoEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:contextWoEquipment];
    requestWoEquipment = [[NSFetchRequest alloc] init];
    [requestWoEquipment setEntity:entityWoEquipment];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND itemCustomName = %@ AND (isActive = %@ || isActive = %@)",strWorkOrderId,strEuipNameCustom,@"true",@"True"];
    
    [requestWoEquipment setPredicate:predicate];
    
    sortDescriptorWoEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWoEquipment = [NSArray arrayWithObject:sortDescriptorWoEquipment];
    
    [requestWoEquipment setSortDescriptors:sortDescriptorsWoEquipment];
    
    self.fetchedResultsControllerWoEquipmentService = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoEquipment managedObjectContext:contextWoEquipment sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoEquipmentService setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoEquipmentService performFetch:&error];
    arrAllObjWoEquipmentNew = [self.fetchedResultsControllerWoEquipmentService fetchedObjects];
    if ([arrAllObjWoEquipmentNew count] == 0)
    {
        
        [DejalBezelActivityView removeView];
        
        isExist=NO;
        
    }
    else
    {
        
        for (int k=0; k<arrAllObjWoEquipmentNew.count; k++) {
            
            NSManagedObject *objTemp =arrAllObjWoEquipmentNew[k];
            NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"itemCustomName"]];
            
                if ([strItemCustomName isEqualToString:strEuipNameCustom]) {
                    
                    isExist=YES;
                    break;
                    
                }else{
                    
                    isExist=NO;
                    
                }
        }
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return isExist;
}


-(void)deleteFromCoreDataEquipments :(NSString*)strEquipIdToDelete{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:context];
    
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND accountItemHistoryId = %@",strWorkOrderId,strEquipIdToDelete];
    
    [allData setPredicate:predicate];
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(void)saveMechanicalEquipToDB :(NSString*)strMobileUniqueID{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];

    //AccountItemHistoryExtSerDcs Detail Entity
    entityMechanicalEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:context];
    
    MechanicalWoEquipment *objEmailDetail = [[MechanicalWoEquipment alloc]initWithEntity:entityMechanicalEquipment insertIntoManagedObjectContext:context];
    
    objEmailDetail.companyKey=strCompanyKeyy;
    objEmailDetail.userName=strUserName;
    objEmailDetail.workorderId=_strWorkOrderId;
    objEmailDetail.subWorkOrderId=_strSubWorkOrderId;
    objEmailDetail.workOrderNo=[NSString stringWithFormat:@"%@",[_objWorkOrderDetail valueForKey:@"workOrderNo"]];
    objEmailDetail.accountItemHistoryId=[NSString stringWithFormat:@"%@",strMobileUniqueID];
    objEmailDetail.accountId=[NSString stringWithFormat:@"%@",@"0"];
    objEmailDetail.itemName=[NSString stringWithFormat:@"%@",[_btn_EquipmentsName.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_EquipmentsName.titleLabel.text];
    objEmailDetail.itemcode=[NSString stringWithFormat:@"%@",strItemSysName];
    objEmailDetail.subWorkOrderNo=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderNo"]];
    objEmailDetail.serialNumber=[NSString stringWithFormat:@"%@",_txt_SerialNo.text];
    objEmailDetail.modelNumber=[NSString stringWithFormat:@"%@",_txt_ModelNo.text];
    objEmailDetail.itemCustomName=[NSString stringWithFormat:@"%@",_txtNameEquip.text];

    objEmailDetail.manufacturer=[NSString stringWithFormat:@"%@",_txt_Manufaturer.text];
    objEmailDetail.qty=[NSString stringWithFormat:@"%@",@"1"];
    objEmailDetail.installationDate=strInstalledOn.length==0 ? @"" : strInstalledOn;
    objEmailDetail.warrantyExpireDate=[_btn_WarrantyDate.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_WarrantyDate.titleLabel.text;
    objEmailDetail.accountNo=[NSString stringWithFormat:@"%@",[_objWorkOrderDetail valueForKey:@"accountNo"]];
    objEmailDetail.serviceAddressId=[NSString stringWithFormat:@"%@",[_objWorkOrderDetail valueForKey:@"serviceAddressId"]];
    objEmailDetail.barcodeUrl=[NSString stringWithFormat:@"%@",@""];
    objEmailDetail.installedArea=[NSString stringWithFormat:@"%@",strAreaSysName];
    objEmailDetail.equipmentDesc=_txtView_Notes.text.length==0 ? @"" : _txtView_Notes.text;
    objEmailDetail.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    objEmailDetail.createdDate=[NSString stringWithFormat:@"%@",[global strCurrentDateFormattedForMechanicalNewLocalTime]];
    objEmailDetail.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objEmailDetail.modifiedDate=[NSString stringWithFormat:@"%@",[global strCurrentDateFormattedForMechanicalNewLocalTime]];
    objEmailDetail.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objEmailDetail.isActive=@"true";
    NSError *error222;
    [context save:&error222];
    [_btn_Category setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_Equipment setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_EquipmentsName setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_Area setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_AssociatedWith setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_InstalledOn setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_NextServiceDate setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_WarrantyDate setTitle:@"Select" forState:UIControlStateNormal];
    _txtView_Notes.text=@"";
    strCategorySysName=@"";
    strEquipmentValueId=@"";
    strItemSysName=@"";
    strAreaSysName=@"";
    strAssociatedWith=@"";
    strAssociatedWithId=@"";
    strInstalledOn=@"";
    strItemNumber=@"";
    _txt_SerialNo.text=@"";
    _txt_Manufaturer.text=@"";
    _txt_ModelNo.text=@"";
    _txtNameEquip.text=@"";
    
    //Yaha Par Master wala Dynamic Form Uthana Hai
    isWoEquipIdPrsent=NO;
    strGlobalWoEquipIdToFetchDynamicData=strMobileUniqueID;
    
    [_view_IntsallNewEquipments removeFromSuperview];

}

-(void)updateMechanicalEquipToDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    [objMechanicalEquipFetchedViaId setValue:[NSString stringWithFormat:@"%@",[_btn_EquipmentsName.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_EquipmentsName.titleLabel.text] forKey:@"itemName"];
    [objMechanicalEquipFetchedViaId setValue:[NSString stringWithFormat:@"%@",strItemSysName] forKey:@"itemcode"];
    [objMechanicalEquipFetchedViaId setValue:[NSString stringWithFormat:@"%@",_txt_SerialNo.text] forKey:@"serialNumber"];
    [objMechanicalEquipFetchedViaId setValue:[NSString stringWithFormat:@"%@",_txt_ModelNo.text] forKey:@"modelNumber"];
    [objMechanicalEquipFetchedViaId setValue:[NSString stringWithFormat:@"%@",_txt_Manufaturer.text] forKey:@"manufacturer"];
    [objMechanicalEquipFetchedViaId setValue:strInstalledOn.length==0 ? @"" : strInstalledOn forKey:@"installationDate"];
    [objMechanicalEquipFetchedViaId setValue:[_btn_WarrantyDate.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_WarrantyDate.titleLabel.text forKey:@"warrantyExpireDate"];
    [objMechanicalEquipFetchedViaId setValue:[NSString stringWithFormat:@"%@",strAreaSysName] forKey:@"installedArea"];
    [objMechanicalEquipFetchedViaId setValue:_txtView_Notes.text.length==0 ? @"" : _txtView_Notes.text forKey:@"equipmentDesc"];
    [objMechanicalEquipFetchedViaId setValue:[NSString stringWithFormat:@"%@",[global strCurrentDateFormattedForMechanicalNewLocalTime]] forKey:@"modifiedDate"];
    [objMechanicalEquipFetchedViaId setValue:[NSString stringWithFormat:@"%@",_txtNameEquip.text] forKey:@"itemCustomName"];

    NSError *error222;
    [context save:&error222];
    
    
    [_btn_Category setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_Equipment setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_EquipmentsName setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_Area setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_AssociatedWith setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_InstalledOn setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_NextServiceDate setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_WarrantyDate setTitle:@"Select" forState:UIControlStateNormal];
    _txtView_Notes.text=@"";
    strCategorySysName=@"";
    strEquipmentValueId=@"";
    strItemSysName=@"";
    strAreaSysName=@"";
    strAssociatedWith=@"";
    strAssociatedWithId=@"";
    strInstalledOn=@"";
    strItemNumber=@"";
    _txt_SerialNo.text=@"";
    _txt_Manufaturer.text=@"";
    _txt_ModelNo.text=@"";
    _txtNameEquip.text=@"";
    
    //Yaha Par Master wala Dynamic Form Uthana Hai
    isWoEquipIdPrsent=NO;
    
    [_view_IntsallNewEquipments removeFromSuperview];
    
    [self fetchWoEquipmentFromDB];
}

-(void)addEquipments{
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDateNew = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString *strMobileUniqueId = [NSString stringWithFormat:@"iOS%@%@%@",strWorkOrderId,strDateNew,strTime];
    
    strGlobalCatSysNameToFetchDynamicForm=strCategorySysName;
    NSArray *objValue=[NSArray arrayWithObjects:
                       @"",
                       strWorkOrderId,
                       strAccountNoGlobal,
                       strCategorySysName,
                       strEquipmentValueId,
                       strItemSysName,
                       strAreaSysName,
                       strAssociatedWithId.length==0 ? @"" : strAssociatedWithId,
                       strAssociatedWith.length==0 ? @"" : strAssociatedWith,
                       strInstalledOn.length==0 ? @"" : strInstalledOn,
                       @"true",
                       strDepartmentIdGlobal,
                       _txtView_Notes.text.length==0 ? @"" : _txtView_Notes.text,
                       [_btn_Category.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_Category.titleLabel.text,
                       [_btn_EquipmentsName.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_Category.titleLabel.text,
                       [_btn_Area.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_Category.titleLabel.text,
                       strItemNumber.length==0 ? @"" : strItemNumber,
                       strInstalledOn.length==0 ? @"" : strInstalledOn,
                       [_btn_NextServiceDate.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_NextServiceDate.titleLabel.text,
                       @"",
                       strCurrentDateGlobal,
                       @"",
                       @"",
                       @"false",
                       @"true",
                       [_btn_WarrantyDate.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_WarrantyDate.titleLabel.text,
                       [_btn_WarrantyDate.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_WarrantyDate.titleLabel.text,
                       @"",
                       @"",
                       @"",
                       @"",
                       @"",
                       strDepartmentIdGlobal,
                       [_btn_NextServiceDate.titleLabel.text isEqualToString:@"Select"] ? @"" : _btn_NextServiceDate.titleLabel.text,
                       strMobileUniqueId,
                       @"",
                       _txt_SerialNo.text,
                       _txt_Manufaturer.text,
                       _txt_ModelNo.text,
                       _txtNameEquip.text.length==0 ? @"" : _txtNameEquip.text,
                       @"",nil];
    
    NSArray *objKey=[NSArray arrayWithObjects:
                     @"WOEquipmentId",
                     @"WorkorderId",
                     @"AccountNo",
                     @"CategorySysName",
                     @"EquipmentType",
                     @"ItemSysName",
                     @"AreaSysName",
                     @"AssociatedWith",
                     @"strAssociatedWith",
                     @"InstalledOn",
                     @"IsActive",
                     @"DepartmentName",
                     @"Notes",
                     @"CategoryName",
                     @"ItemName",
                     @"AreaName",
                     @"EquipmentNumber",
                     @"strInstalledOn",
                     @"strNextServiceDate",
                     @"strLastServiceDate",
                     @"strServiceDate",
                     @"strWOEquipmentHistoryId",
                     @"WOEquipmentInspectionFormHtmlStr",
                     @"IsRemoved",
                     @"IsServiceStatus",
                     @"WarrantyDate",
                     @"strWarrantyDate",
                     @"WorkOrderDc",
                     @"WOEquipmentHistoryDcs",
                     @"CreatedDate",
                     @"CreatedBy",
                     @"ModifiedDate",
                     @"DepartmentId",
                     @"NextServiceDate",
                     @"MobileUniqueId",
                     @"ModifiedBy",
                     @"SerialNumber",
                     @"Manufacturer",
                     @"ModelNumber",
                     @"ItemCustomName",
                     @"ItemSysName",nil];
    
    
    //ModelNumber Manufacturer  ModelNumber  ItemSysName
    
    NSDictionary *dict_ToSend=[[NSDictionary alloc] initWithObjects:objValue forKeys:objKey];
    
    [arrOfEquipmentList addObject:dict_ToSend];
    
   // [arrOfNextServiceDateLocally addObject:@"Select Next Service Date"];
    
    _equipmentTblview.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, arrOfMechanicalWoEquipment.count*390);
    
    _view_EquipmentList.frame=CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, arrOfMechanicalWoEquipment.count*390);
    
    _view_IntsallNewEquipments.frame=CGRectMake(0, _view_EquipmentList.frame.origin.y+_view_EquipmentList.frame.size.height+10,[UIScreen mainScreen].bounds.size.width, _view_IntsallNewEquipments.frame.size.height);
    
    _view_SavenCancelButtons.frame=CGRectMake(0, _view_IntsallNewEquipments.frame.origin.y+_view_IntsallNewEquipments.frame.size.height+10,[UIScreen mainScreen].bounds.size.width, _view_SavenCancelButtons.frame.size.height);
    
    [_equipmentTblview reloadData];
    
    [self setScrollHeightonClick];
    
    NSString *strEquipNameCombined=[NSString stringWithFormat:@"%@(%@)",_btn_Category.titleLabel.text,strItemNumber];
    
    NSArray *objValue1=[NSArray arrayWithObjects:
                        @"",
                        strAreaSysName,
                        strEquipNameCombined,
                        strMobileUniqueId,nil];
    NSArray *objKey1=[NSArray arrayWithObjects:
                      @"WoEquipmentId",
                      @"AreaSysName",
                      @"EquipName",
                      @"MobileUniqueId",nil];
    NSDictionary *dict_ToSend1=[[NSDictionary alloc] initWithObjects:objValue1 forKeys:objKey1];
    
    [arrOfAssociatedWithLocally addObject:dict_ToSend1];
    
    [_btn_Category setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_Equipment setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_EquipmentsName setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_Area setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_AssociatedWith setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_InstalledOn setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_NextServiceDate setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_WarrantyDate setTitle:@"Select" forState:UIControlStateNormal];
    _txtView_Notes.text=@"";
    strCategorySysName=@"";
    strEquipmentValueId=@"";
    strItemSysName=@"";
    strAreaSysName=@"";
    strAssociatedWith=@"";
    strAssociatedWithId=@"";
    strInstalledOn=@"";
    strItemNumber=@"";
    _txt_SerialNo.text=@"";
    _txt_Manufaturer.text=@"";
    _txt_ModelNo.text=@"";
    _txtNameEquip.text=@"";
    
    //Yaha Par Master wala Dynamic Form Uthana Hai
    isWoEquipIdPrsent=NO;
    strGlobalWoEquipIdToFetchDynamicData=strMobileUniqueId;
    
    [_view_IntsallNewEquipments removeFromSuperview];
    
   // [self FetchFromCoreDataToShowEquipmentsDynamic];
    
  //  [self saveEquipmentOnSaveButtonAction];
    
}

//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)singleTap:(UITapGestureRecognizer *)gesture {
    [viewBackGround removeFromSuperview];
    [viewForDate removeFromSuperview];
    [tblData removeFromSuperview];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [viewForDate removeFromSuperview];
    [tblData removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
//============================================================================
//============================================================================
#pragma mark- Single Tap Methods
//============================================================================
//============================================================================
-(void)tapDetectedOnBackGroundView{
    [viewBackGround removeFromSuperview];
}

//=====================================================
//=====================================================
#pragma mark - Button Action Install New Equipments:-
//=====================================================
//=====================================================

- (IBAction)action_Back:(id)sender {
    
    //[self dismissViewControllerAnimated:NO completion:nil];
    [self backMethod];
    
}

- (IBAction)action_Category:(id)sender {
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
    
    //    for (int k=0; k<arrOfAreaMaster.count; k++) {
    //
    //        NSDictionary *dictDataa=arrOfAreaMaster[k];
    //        [arrDataTblView addObject:dictDataa];
    //
    //    }
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        if (([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"])) {
            
            [arrDataTblView addObject:dictDataa];
            
        }
    }
    
    if (arrDataTblView.count==0) {
        
        [global AlertMethod:Info :NoDataAvailable];
        
    }else{
        
        tblData.tag=1874919418;
        [self tableLoad:tblData.tag];
        
    }
    
}


- (IBAction)action_Equipments:(id)sender {
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"EquipmentTypes"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        [arrDataTblView addObject:dictDataa];
        
    }
    
    if (arrDataTblView.count==0) {
        
        [global AlertMethod:Info :NoDataAvailable];
        
    }else{
        
        tblData.tag=1874919419;
        [self tableLoad:tblData.tag];
        
    }
    
}


- (IBAction)actin_EquipmentName:(id)sender {
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfAreaMasterEquipmentTypes=[dictDetailsFortblView valueForKey:@"EquipmentTypes"];
    NSString *strEqupMentTypeValue;
    for (int k=0; k<arrOfAreaMasterEquipmentTypes.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMasterEquipmentTypes[k];
        
        NSString *strEqupMentTypeName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Text"]];
        
        if ([strEqupMentTypeName isEqualToString:@"Equipment"]) {
            
            strEqupMentTypeValue=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Value"]];
            
        }
        
    }
    
    if (strEqupMentTypeValue.length==0) {
        
        strEquipmentValueId=@"1";
        
        
    } else {
        
        strEquipmentValueId=strEqupMentTypeValue;
        
    }
    
    //    if (strCategoryId.length==0 || strEquipmentValueId.length==0) {
    //
    //        [global AlertMethod:Info :alertCatnEquipName];
    //
    //    } else {
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryItems"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strEquipmentIdToCheck=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemType"]];
        NSString *strCategoryIdToCheck=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryId"]];
        
        if (strCategoryId.length==0) {
            
            strCategoryId=@"";
            strCategoryIdToCheck=@"";
            
        }
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        if ([strEquipmentIdToCheck isEqualToString:strEquipmentValueId] && [strCategoryIdToCheck isEqualToString:strCategoryId] && ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"])) {
            
            [arrDataTblView addObject:dictDataa];
            
        }
    }

    
        if (arrDataTblView.count==0) {
            
            [global AlertMethod:Info :NoDataAvailableee];
            
        }else{
            
            tblData.tag=1874919420;
            [self tableLoad:tblData.tag];
            
        }
   // }
}

- (IBAction)action_Area:(id)sender {
    
    [_btn_AssociatedWith setTitle:@"Select" forState:UIControlStateNormal];
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    /*
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"AreaMaster"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        [arrDataTblView addObject:dictDataa];
        
    }
    */
    
    [arrDataTblView addObject:@"Upstair"];
    [arrDataTblView addObject:@"Downstair"];
    
    if (arrDataTblView.count==0) {
        
        [global AlertMethod:Info :NoDataAvailable];
        
    }else{
        
        tblData.tag=1874919421;
        [self tableLoad:tblData.tag];
        
    }
}

- (IBAction)action_AssociatedWith:(id)sender {
    
    if (strAreaSysName.length==0) {
        
        [global AlertMethod:Alert :@"Please select Area to associate with"];
        
    } else {
        filteredArrOfAssoicatedWithToDisplay=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfAssociatedWithLocally.count; k++) {
            
            NSDictionary *dictdata=arrOfAssociatedWithLocally[k];
            NSString *strSysName=[NSString stringWithFormat:@"%@",[dictdata valueForKey:@"AreaSysName"]];
            if ([strAreaSysName isEqualToString:strSysName]) {
                
                [filteredArrOfAssoicatedWithToDisplay addObject:dictdata];
                
            }
        }
        arrDataTblView=[[NSMutableArray alloc]init];
        if (filteredArrOfAssoicatedWithToDisplay.count==0) {
            
            [global AlertMethod:Info :@"No equipment available to associate with"];
            
        }else{
            [arrDataTblView addObjectsFromArray:filteredArrOfAssoicatedWithToDisplay];
            
            tblData.tag=1874919422;
            [self tableLoad:tblData.tag];
            
        }
        
    }
    
}
- (IBAction)action_InstalledOn:(id)sender {
    
    [self addingDatePicker:strInstalledOn :101];
    
}
- (IBAction)action_NextServiceDate:(id)sender {
    
    if (strInstalledOn.length==0) {
        
        [global AlertMethod:Alert :@"Please select install date first."];
        
    } else {
        
        [self addingDatePicker:strNextServiceDate :102];
        
    }
    
}
- (IBAction)action_WarrantyDate:(id)sender {
    
    if (strInstalledOn.length==0) {
        
        [global AlertMethod:Alert :@"Please select install date first."];
        
    } else {
        
        [self addingDatePicker:strWarrantyDate :103];
        
    }
    
}
- (IBAction)action_AddNewEquipment:(id)sender {
    
    if (isCompletedStatusMechanical) {

    }else{
        
        NSString *errorMessage=[self validationCheck];
        if (errorMessage) {
            NSString *strTitle = Alert;
            [global AlertMethod:strTitle :errorMessage];
        } else {
            
            yesEditedSomething=YES;
            
            if (isUpdateEquip) {
                //For Updating added equipment
                
                NSString *strstrMobileUniqueId=[NSString stringWithFormat:@"%@",[objMechanicalEquipFetchedViaId valueForKey:@"accountItemHistoryId"]];
                NSString *strstrItemSysName=[NSString stringWithFormat:@"%@",strItemSysName];
                NSString *strstrManufaturer=[NSString stringWithFormat:@"%@",_txt_Manufaturer.text];
                NSString *strstrModelNo=[NSString stringWithFormat:@"%@",_txt_ModelNo.text];
                NSString *strstrNameEquip=[NSString stringWithFormat:@"%@",_txt_ModelNo.text];
                NSString *strstrSerialNo=[NSString stringWithFormat:@"%@",_txt_SerialNo.text];
                NSString *strstrstrAcNo=[NSString stringWithFormat:@"%@",[objMechanicalEquipFetchedViaId valueForKey:@"accountNo"]];
                NSString *strstrstrServiceAddressId=[NSString stringWithFormat:@"%@",[objMechanicalEquipFetchedViaId valueForKey:@"serviceAddressId"]];


                BOOL isAlreadyEquip = [self fetchWoEquipmentFromDBToCheckIfExist:strstrMobileUniqueId :strstrItemSysName :strstrManufaturer :strstrModelNo :strstrSerialNo :strstrstrAcNo :strstrstrServiceAddressId :@"update" :strstrNameEquip];
                
                //BOOL isAlreadyEquip = [self fetchWoEquipmentFromDBToCheckIfExist:strstrMobileUniqueId :strstrItemSysName :([strstrManufaturer isEqualToString:@""]) ? @"" : strstrManufaturer :([strstrModelNo isEqualToString:@""]) ? @"" : strstrModelNo :([strstrSerialNo isEqualToString:@""]) ? @"" : strstrSerialNo :strstrstrAcNo :strstrstrServiceAddressId :@"update" :strstrNameEquip];

                
                BOOL isEquipNameAlreadyExist = [self fetchWoEquipmentFromDBToCheckIfSameNameExist:_txtNameEquip.text];
                
                if (_txtNameEquip.text.length==0) {
                    
                    isEquipNameAlreadyExist=NO;
                    
                }

                if (isEquipNameAlreadyExist) {
                    
                    [global AlertMethod:Alert :@"Equipment name already exist."];
                    
                } else if (isAlreadyEquip) {
                    
                    [global AlertMethod:Alert :@"Equipment already exist."];
                    
                } else {

                    [self updateMechanicalEquipToDB];
                    
                }
                
            } else {
                // For Inserting New Equipments
                
                NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
                [formatterDate setDateFormat:@"MMddyyyy"];
                [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
                NSString *strDateNew = [formatterDate stringFromDate:[NSDate date]];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HHmmss"];
                [formatter setTimeZone:[NSTimeZone localTimeZone]];
                NSString *strTime = [formatter stringFromDate:[NSDate date]];
                NSString *strMobileUniqueId = [NSString stringWithFormat:@"iOS%@%@%@",strWorkOrderId,strDateNew,strTime];

                NSString *strAcNo=[NSString stringWithFormat:@"%@",[_objWorkOrderDetail valueForKey:@"accountNo"]];
                NSString *strServiceAddressId=[NSString stringWithFormat:@"%@",[_objWorkOrderDetail valueForKey:@"serviceAddressId"]];

                
                BOOL isAlreadyEquip = [self fetchWoEquipmentFromDBToCheckIfExist:strMobileUniqueId :strItemSysName :_txt_Manufaturer.text :_txt_ModelNo.text :_txt_SerialNo.text :strAcNo :strServiceAddressId :@"add" :_txtNameEquip.text];

                //BOOL isAlreadyEquip = [self fetchWoEquipmentFromDBToCheckIfExist:strMobileUniqueId :strItemSysName :([_txt_Manufaturer.text isEqualToString:@""]) ? @"*&#$)(-" : _txt_Manufaturer.text :([_txt_ModelNo.text isEqualToString:@""]) ? @"*&#$)(-" : _txt_ModelNo.text :([_txt_SerialNo.text isEqualToString:@""]) ? @"*&#$)(-" : _txt_SerialNo.text :strAcNo :strServiceAddressId :@"add" :_txtNameEquip.text];

                
                BOOL isEquipNameAlreadyExist = [self fetchWoEquipmentFromDBToCheckIfSameNameExist:_txtNameEquip.text];
                
                if (_txtNameEquip.text.length==0) {
                    
                    isEquipNameAlreadyExist=NO;
                    
                }
                
                if (isEquipNameAlreadyExist) {
                    
                    [global AlertMethod:Alert :@"Equipment name already exist."];
                    
                } else if (isAlreadyEquip) {
                    
                    [global AlertMethod:Alert :@"Equipment already exist."];
                    
                } else {
                    
                    [self saveMechanicalEquipToDB:strMobileUniqueId];
                    
                    [self fetchWoEquipmentFromDB];
                    
                }
                
            }
            
        }
        
    }
    
}
- (IBAction)action_SaveFinalData:(id)sender {
    
    [self goToNextViewMethod];
    
    
    /*
    if (isCompletedStatusMechanical) {
        
        [self goToNextViewMethod];
        
    } else {
        
        NSMutableArray *ArrTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfEquipmentList.count; k++) {
            
            NSDictionary *dictData=arrOfEquipmentList[k];
            NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsServiceStatus"]];
            NSString *strIsRemoved=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsRemoved"]];
            
            if(!([strIsRemoved isEqualToString:@"true"] || [strIsRemoved isEqualToString:@"1"])){
                
                if([strIsActive isEqualToString:@"false"] || [strIsActive isEqualToString:@"0"]){
                    
                    [ArrTemp addObject:dictData];
                    
                }
            }
        }
        
        if (ArrTemp.count==0) {
            
            if (matchesWoEquipment==nil) {
                
                [self saveWhenNoEquipmentsPrsentInDb:arrOfEquipmentList];
                
            } else {
                
                [matchesWoEquipment setValue:arrOfEquipmentList forKey:@"arrOfEquipmentList"];
                NSError *error1;
                [contextWoEquipment save:&error1];
                
            }
            
            [self goToNextViewMethod];
            
        } else {
            
            NSString *strMsg=[NSString stringWithFormat:@"You have not service %lu Equipments. Do you still want to continue?",(unsigned long)ArrTemp.count];
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:Alert
                                       message:strMsg
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Yes-Continue" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      if (matchesWoEquipment==nil) {
                                          
                                          [self saveWhenNoEquipmentsPrsentInDb:arrOfEquipmentList];
                                          
                                      } else {
                                          
                                          [matchesWoEquipment setValue:arrOfEquipmentList forKey:@"arrOfEquipmentList"];
                                          NSError *error1;
                                          [contextWoEquipment save:&error1];
                                          
                                      }
                                      
                                      [self goToNextViewMethod];
                                      
                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
     */
}

- (IBAction)action_Cancel:(id)sender {
    
    [self backMethod];
    
}

-(void)saveEquipmentOnSaveButtonAction{
    
    NSMutableArray *ArrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfEquipmentList.count; k++) {
        
        NSDictionary *dictData=arrOfEquipmentList[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsServiceStatus"]];
        NSString *strIsRemoved=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsRemoved"]];
        
        if(!([strIsRemoved isEqualToString:@"true"] || [strIsRemoved isEqualToString:@"1"])){
            
            if([strIsActive isEqualToString:@"false"] || [strIsActive isEqualToString:@"0"]){
                
                [ArrTemp addObject:dictData];
                
            }
        }
    }
    
    if (ArrTemp.count==0) {
        
        if (matchesWoEquipment==nil) {
            
            [self saveWhenNoEquipmentsPrsentInDb:arrOfEquipmentList];
            
        } else {
            
            [matchesWoEquipment setValue:arrOfEquipmentList forKey:@"arrOfEquipmentList"];
            NSError *error1;
            [contextWoEquipment save:&error1];
            
        }
        
    } else {
        
        if (matchesWoEquipment==nil) {
            
            [self saveWhenNoEquipmentsPrsentInDb:arrOfEquipmentList];
            
        } else {
            
            [matchesWoEquipment setValue:arrOfEquipmentList forKey:@"arrOfEquipmentList"];
            NSError *error1;
            [contextWoEquipment save:&error1];
            
        }

    }
}

//============================================================================
//============================================================================
#pragma mark- ---------------------METHODS-----------------
//============================================================================
//============================================================================
-(void)saveWhenNoEquipmentsPrsentInDb :(NSArray*)arrEquips{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //Equipment Detail Entity
    entityWoEquipment=[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context];
    
    WorkOrderEquipmentDetails *objImagesDetail = [[WorkOrderEquipmentDetails alloc]initWithEntity:entityWoEquipment insertIntoManagedObjectContext:context];
    
    objImagesDetail.companyKey=strCompanyKeyy;
    
    objImagesDetail.userName=strUserName;
    
    objImagesDetail.workorderId=strWorkOrderId;
    
    objImagesDetail.arrOfEquipmentList=arrEquips;
    
    NSError *error1;
    [contextWoEquipment save:&error1];
    
}
-(void)changeToNotEditable{
    
    if (isCompletedStatusMechanical) {
        
    } else {
        
    }
    
}

-(void)backMethod{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
//    int index = 0;
//    NSArray *arrstack=self.navigationController.viewControllers;
//    for (int k1=0; k1<arrstack.count; k1++) {
//        if ([[arrstack objectAtIndex:k1] isKindOfClass:[ServiceDetailAppointmentViewiPad class]]) {
//            index=k1;
//            //break;
//        }
//    }
//    ServiceDetailAppointmentViewiPad *myController = (ServiceDetailAppointmentViewiPad *)[self.navigationController.viewControllers objectAtIndex:index];
//    // myController.typeFromBack=_lbl_LeadInfo_Status.text;
//    [self.navigationController popToViewController:myController animated:NO];
    
}

-(void)goToNextViewMethod{
    
    if (yesEditedSomething) {
        
        [self updateModifyDate];
        
    }
    
    strGlobalWorkOrderStatus=@"InComplete";
    
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"ServiceiPad"
//                                                             bundle: nil];
//    InvoiceAppointmentViewiPad
//    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"InvoiceAppointmentViewiPad"];
//
//    objByProductVC.dictJsonDynamicForm=_dictJsonDynamicFormEquip;
//    objByProductVC.workOrderDetail=_workOrderDetailEquip;
//    objByProductVC.paymentInfoDetail=_paymentInfoDetailEquip;
//    objByProductVC.dictOfChemicalList=_dictOfChemicalListEquip;
//    objByProductVC.arrOfChemicalListOther=_arrOfChemicalListOtherEquip;
//    objByProductVC.arrOfChemicalList=_arrOfChemicalListEquip;
//    objByProductVC.arrChemicalList=_arrChemicalListEquip;//[dictChemicalList valueForKey:@"ProductMasterInfo"];
//    objByProductVC.strPaymentModes=_strPaymentModesEquip;
//    objByProductVC.strDepartmentIdd=_strDepartmentIddEquip;
//    objByProductVC.strPaidAmount=_strPaidAmountEquip;
//    objByProductVC.arrAllObjImageDetail=_arrAllObjImageDetailEquip;
//    objByProductVC.arrOfEquipmentLists=arrOfEquipmentList;
//    [self.navigationController pushViewController:objByProductVC animated:NO];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

-(NSString *)validationCheck
{
    NSString *errorMessage;
//    if ([_btn_Area.titleLabel.text isEqualToString:@"Select"]){
//        errorMessage = @"Please select Area";
//    }
    
    //    if ([_btn_Category.titleLabel.text isEqualToString:@"Select"]){
    //        errorMessage = @"Please select Category";
    //    }
    
    //_txtNameEquip.text
    if (_txtNameEquip.text.length==0){
        errorMessage = @"Please enter Equipment Name";
    }
    
    if ([_btn_EquipmentsName.titleLabel.text isEqualToString:@"Select"]){
        errorMessage = @"Please select Equipment";
    }
    
//    if ([_btn_Equipment.titleLabel.text isEqualToString:@"Select"]){
//        errorMessage = @"Please select Equipment";
//    }
//    if ([_btn_Category.titleLabel.text isEqualToString:@"Select"]){
//        errorMessage = @"Please select Category";
//    }
    return errorMessage;
}

-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 600);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    [self setTableFrame];
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
    
}

-(void)addingEquipmentView{
    
    
    if (isCompletedStatusMechanical) {
        
        _equipmentTblview.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, arrOfMechanicalWoEquipment.count*390);
        
        _view_EquipmentList.frame=CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, arrOfMechanicalWoEquipment.count*390);
        [_scrollVieww addSubview:_view_EquipmentList];
        
        //        _view_IntsallNewEquipments.frame=CGRectMake(0, _view_EquipmentList.frame.origin.y+_view_EquipmentList.frame.size.height+10,[UIScreen mainScreen].bounds.size.width, _view_IntsallNewEquipments.frame.size.height);
        //        [_scrollVieww addSubview:_view_IntsallNewEquipments];
        
        _view_SavenCancelButtons.frame=CGRectMake(0, _view_EquipmentList.frame.origin.y+_view_EquipmentList.frame.size.height+10,[UIScreen mainScreen].bounds.size.width, _view_SavenCancelButtons.frame.size.height);
        [_scrollVieww addSubview:_view_SavenCancelButtons];
        
        [_equipmentTblview reloadData];
        
    } else {
        
        if (arrOfEquipmentList.count==0) {
            
            _view_IntsallNewEquipments.frame=CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, _view_IntsallNewEquipments.frame.size.height);
            [_scrollVieww addSubview:_view_IntsallNewEquipments];
            
            _view_SavenCancelButtons.frame=CGRectMake(0, _view_IntsallNewEquipments.frame.origin.y+_view_IntsallNewEquipments.frame.size.height+10,self.view.frame.size.width, _view_SavenCancelButtons.frame.size.height);
            [_scrollVieww addSubview:_view_SavenCancelButtons];
            
        } else {
            
            _equipmentTblview.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, arrOfEquipmentList.count*390);
            
            _view_EquipmentList.frame=CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, arrOfEquipmentList.count*390);
            [_scrollVieww addSubview:_view_EquipmentList];
            
            _view_IntsallNewEquipments.frame=CGRectMake(0, _view_EquipmentList.frame.origin.y+_view_EquipmentList.frame.size.height+10,[UIScreen mainScreen].bounds.size.width, _view_IntsallNewEquipments.frame.size.height);
            [_scrollVieww addSubview:_view_IntsallNewEquipments];
            
            _view_SavenCancelButtons.frame=CGRectMake(0, _view_IntsallNewEquipments.frame.origin.y+_view_IntsallNewEquipments.frame.size.height+10,[UIScreen mainScreen].bounds.size.width, _view_SavenCancelButtons.frame.size.height);
            [_scrollVieww addSubview:_view_SavenCancelButtons];
            
            [_equipmentTblview reloadData];
        }
        
    }
    
    
    [self setScrollHeightonClick];
    
}

-(void)setScrollHeightonClick{
    
    [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_view_SavenCancelButtons.frame.size.height+_view_SavenCancelButtons.frame.origin.y+10)];
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addingDatePicker :(NSString*)strDateToSet :(int)tag{
    
    [_txtView_Notes resignFirstResponder];
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    tblData.tag=tag;
    [self addPickerViewDateTo :strDateToSet];
    
}

-(void)addPickerViewDateTo :(NSString*)strDateString
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //[pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strDateString.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strDateString];
        
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    yesEditedSomething=YES;
    
    NSInteger i;
    i=tblData.tag;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDateEquip = [dateFormat stringFromDate:pickerDate.date];
    
    
    //    NSDate *dateSELECTED = [dateFormat dateFromString:strDateEquip];
    
    //    if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
    //
    //        [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
    //
    //    }else{
    
    
    if (i==101) {
        
        [_btn_NextServiceDate setTitle:@"Select" forState:UIControlStateNormal];
        strNextServiceDate=@"";
        
        [_btn_WarrantyDate setTitle:@"Select" forState:UIControlStateNormal];
        strWarrantyDate=@"";
        
        [self settingDate:i];
        
    } else if (i==102){
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *date2 = [dateFormat dateFromString:strInstalledOn];
        
        if ([pickerDate.date compare:date2] == NSOrderedDescending) {
            
            [self settingDate:i];
            
        } else {
            
            [global AlertMethod:@"Alert" :@"Next Service Date should be greater than Installed Date"];
            
        }
        
    }
    else if (i==103){
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *date2 = [dateFormat dateFromString:strInstalledOn];
        
        if ([pickerDate.date compare:date2] == NSOrderedDescending) {
            
            [self settingDate:i];
            
        } else {
            
            [global AlertMethod:@"Alert" :@"Warranty Date should be greater than Installed Date"];
            
        }
        
        
    } else{
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *date2 = [dateFormat dateFromString:strGlobalCellForRowNxtServiceDate];
        
        if ([strGlobalCellForRowNxtServiceDate isEqualToString:@""]) {
            
            [self settingDate:i];
            
        }else if ([pickerDate.date compare:date2] == NSOrderedDescending) {
            
            [self settingDate:i];
            
        } else {
            
            [global AlertMethod:@"Alert" :@"Next Service Date should be greater than Installed Date"];
            
        }
        
    }
    
    // }
    
    
    //[self settingDate:i];
}
-(void)settingDate :(NSInteger)tag{
    
    switch (tag)
    {
        case 101:
        {
            [_btn_InstalledOn setTitle:strDateEquip forState:UIControlStateNormal];
            strInstalledOn=strDateEquip;
            break;
        }
        case 102:
        {
            [_btn_NextServiceDate setTitle:strDateEquip forState:UIControlStateNormal];
            strNextServiceDate=strDateEquip;
            break;
        }
        case 103:
        {
            [_btn_WarrantyDate setTitle:strDateEquip forState:UIControlStateNormal];
            strWarrantyDate=strDateEquip;
            break;
        }
        case 110:
        {
            if (globalIndexForNxtserviceDate==-1) {
                
            } else {
                [arrOfNextServiceDateLocally replaceObjectAtIndex:globalIndexForNxtserviceDate withObject:strDateEquip];
                // NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:globalIndexForNxtserviceDate inSection:0];
                //  NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
                //  [_equipmentTblview reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                
                [_equipmentTblview reloadData];
                
            }
            strNextServiceDateGlobal=strDateEquip;
            
            break;
        }
        default:
            break;
    }
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1874919423) {
        return arrOfMechanicalWoEquipment.count;
    }
    else if (tableView.tag==1874919424) {
        return arrOfSerialNosFiltered.count;
    }
    else if (tableView.tag==1874919425) {
        return arrOfModelNosFiltered.count;
    }
    else if (tableView.tag==1874919426) {
        return arrOfManufacturerFiltered.count;
    }
    else {
        return [arrDataTblView count];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==1874919423) {
        
        return tableView.rowHeight;
        
    } else {
        
        if ([UIScreen mainScreen].bounds.size.height==667) {
            return 50;
        }else if ([UIScreen mainScreen].bounds.size.height==736) {
            return 50;
        }
        else
            return 80;
        
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==1874919423) {
        
        EquipmentsListTableViewCell *cell = (EquipmentsListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EquipmentsListTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
    } else {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSInteger i;
        i=tblData.tag;
        
        if (i<1874919417) {
            
            if (isMultipleSelect) {
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                
                NSString *str=[dictData valueForKey:@"Value"];
                
                cell.textLabel.text=[dictData valueForKey:@"Name"];
                
                long index=100000;
                
                for (int k=0; k<arraySelecteValues.count; k++) {
                    if ([arraySelecteValues containsObject:str]) {
                        index=indexPath.row;
                        break;
                    }
                }
                
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
            }
            else{
                
                NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                cell.textLabel.text=[dictData valueForKey:@"Name"];
                
                NSString *str=[dictData valueForKey:@"Value"];
                long index=100000;
                if ([strGlobalDropDownSelectedValue isEqualToString:str]) {
                    index=indexPath.row;
                }
                if (indexPath.row==index) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
                
            }
            
            
        } else {
            
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            if (!(arrDataTblView.count==0)) {
                
                switch (i)
                {
                    case 1874919418:
                    {
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        cell.textLabel.text=[dictData valueForKey:@"Name"];
                        break;
                    }
                    case 1874919419:
                    {
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        cell.textLabel.text=[dictData valueForKey:@"Text"];
                        break;
                    }
                    case 1874919420:
                    {
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        cell.textLabel.text=[dictData valueForKey:@"Name"];
                        break;
                    }
                    case 1874919421:
                    {
                        //NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                        break;
                    }
                    case 1874919422:
                    {
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        cell.textLabel.text=[dictData valueForKey:@"EquipName"];
                        break;
                    }
                    case 1874919424:
                    {
                        NSDictionary *dictData=[arrOfSerialNosFiltered objectAtIndex:indexPath.row];
                        cell.textLabel.text=[dictData valueForKey:@"SerialNumber"];
                        break;
                    }
                    case 1874919425:
                    {
                        NSDictionary *dictData=[arrOfModelNosFiltered objectAtIndex:indexPath.row];
                        cell.textLabel.text=[dictData valueForKey:@"ModelNumber"];
                        break;
                    }
                    case 1874919426:
                    {
                        NSDictionary *dictData=[arrOfManufacturerFiltered objectAtIndex:indexPath.row];
                        cell.textLabel.text=[dictData valueForKey:@"Manufacturer"];
                        break;
                    }
                        
                    default:
                        break;
                }
                
            }
            
        }
        if ([UIScreen mainScreen].bounds.size.height==667) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }else if ([UIScreen mainScreen].bounds.size.height==736) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }
        else
            cell.textLabel.font=[UIFont systemFontOfSize:22];
        return cell;
    }
}

- (void)configureCell:(EquipmentsListTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *dictDataEquip=arrOfMechanicalWoEquipment[indexPath.row];
    
    // Fetching Category Name itemcode SysName
    
    NSArray *arrOfInventoryItems=[dictDetailsFortblView valueForKey:@"InventoryItems"];

    NSString *strCategorySysNameLocal,*strCategoryNameLocal;
    for (int k=0; k<arrOfInventoryItems.count; k++) {
        
        NSDictionary *dictDataItems=arrOfInventoryItems[k];
        
        if ([[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"itemcode"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictDataItems valueForKey:@"SysName"]]]) {
            
            strCategorySysNameLocal = [NSString stringWithFormat:@"%@",[dictDataItems valueForKey:@"CategorySysName"]];
            
            break;
        }
        
    }
    
    
    NSArray *arrOfInventoryCategoryMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
    
    for (int k1=0; k1<arrOfInventoryCategoryMaster.count; k1++) {
        
        NSDictionary *dictDataCategoryMaster = arrOfInventoryCategoryMaster[k1];
        
        if ([[NSString stringWithFormat:@"%@",[dictDataCategoryMaster valueForKey:@"SysName"]] isEqualToString:[NSString stringWithFormat:@"%@",strCategorySysNameLocal]]) {
            
            strCategoryNameLocal = [NSString stringWithFormat:@"%@",[dictDataCategoryMaster valueForKey:@"Name"]];
            
            break;
        }
        
    }
    
    if (strCategoryNameLocal.length==0) {
        
        strCategoryNameLocal = @"N/A";
        
    }
    
    cell.lblCategory.text = [NSString stringWithFormat:@"Category: %@",strCategoryNameLocal];

    cell.lblArea.text=[NSString stringWithFormat:@"Area:%@",[dictDataEquip valueForKey:@"installedArea"]];
    
    NSString *strItemName=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"itemName"]];
    NSString *strItemCode=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"itemcode"]];
    NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"itemCustomName"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    if (strItemName.length>0) {
        
        [arrTemp addObject:strItemName];
        
    }
    
    if (strItemCode.length>0) {
        
        [arrTemp addObject:strItemCode];
        
    }

    if (strItemCustomName.length>0) {
        
        [arrTemp addObject:strItemCustomName];
        
    }

    if (arrTemp.count==1) {
        
        cell.lblEquipName.text=[NSString stringWithFormat:@"Equip.Name(#):%@",[dictDataEquip valueForKey:@"itemName"]];

    } else if (arrTemp.count==2){
        
        cell.lblEquipName.text=[NSString stringWithFormat:@"Equip.Name(#):%@(%@)",[dictDataEquip valueForKey:@"itemName"],[dictDataEquip valueForKey:@"itemcode"]];

    } else if (arrTemp.count==3){
        
        cell.lblEquipName.text=[NSString stringWithFormat:@"Equip.Name(#):%@(%@)-%@",[dictDataEquip valueForKey:@"itemName"],[dictDataEquip valueForKey:@"itemcode"],[dictDataEquip valueForKey:@"itemCustomName"]];

    }
    //cell.lblEquipName.text=@"Equip.Name(#):%@(%@)asdhdhdhdhasdhdhdhdhasdhdhdhdhasdhdhdhdh";
    
    NSString *strWrntyDateLocal=[global ChangeDateMechanicalEquipment:[dictDataEquip valueForKey:@"warrantyExpireDate"]];
    NSString *strInstallDateLocal=[global ChangeDateMechanicalEquipment:[dictDataEquip valueForKey:@"installationDate"]];

    if ([strWrntyDateLocal isEqualToString:@"(null)"] || strWrntyDateLocal.length==0) {
        
        strWrntyDateLocal=@"";
        
    }
    if ([strInstallDateLocal isEqualToString:@"(null)"] || strInstallDateLocal.length==0) {
        
        strInstallDateLocal=@"";
        
    }

    cell.lblWarrantyDate.text=[NSString stringWithFormat:@"Warranty Date:%@",strWrntyDateLocal];
    cell.lblInstalledOnDate.text=[NSString stringWithFormat:@"Installed On:%@",strInstallDateLocal];
    cell.lblSerialNo.text=[NSString stringWithFormat:@"Serial #:%@",[dictDataEquip valueForKey:@"serialNumber"]];
    cell.lblModelNo.text=[NSString stringWithFormat:@"Model #:%@",[dictDataEquip valueForKey:@"modelNumber"]];
    cell.lblManufacturer.text=[NSString stringWithFormat:@"Manufacturer:%@",[dictDataEquip valueForKey:@"manufacturer"]];
    
    
    cell.BtnAdd.tag=indexPath.row;
    cell.BtnDelete.tag=indexPath.row;
    cell.BtnBarcode.tag=indexPath.row;
    cell.BtnBarcodePrint.tag=indexPath.row;
    cell.btnViewMoreEquip.tag=indexPath.row;

    
    if (cell.lblEquipName.text.length>40) {
        
        cell.btnViewMoreEquip.hidden=NO;
        
    } else {
        
        cell.btnViewMoreEquip.hidden=YES;
        
    }
    //cell.btnViewMoreEquip.hidden=NO;

    [cell.btnViewMoreEquip addTarget:self action:@selector(action_ViewMoreEquipName:) forControlEvents:UIControlEventTouchDown];

    
    if (isCompletedStatusMechanical) {
        //[_btnAddEquip setEnabled:NO];
    }else{
        [cell.btnCheckBox addTarget:self action:@selector(methodCheckBox:) forControlEvents:UIControlEventTouchDown];
        [cell.btnSaveServiceDate addTarget:self action:@selector(methodSaveServiceDate:) forControlEvents:UIControlEventTouchDown];
        [cell.BtnDelete addTarget:self action:@selector(methodDeleteEquipments:) forControlEvents:UIControlEventTouchDown];
        [cell.btnServiceDateToSave addTarget:self action:@selector(methodToSelectdateService:) forControlEvents:UIControlEventTouchDown];
        [cell.BtnAdd addTarget:self action:@selector(methodEditEquipments:) forControlEvents:UIControlEventTouchDown];

    }
    
    NSString *strUrlBarcode=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"barcodeUrl"]];
    
    if ((strUrlBarcode.length==0) || [strUrlBarcode isEqualToString:@"(null)"]) {
        
        [cell.BtnBarcodePrint setHidden:YES];
        [cell.BtnBarcode setHidden:NO];
        
        [cell.BtnBarcode addTarget:self action:@selector(methodBarcodeGenerate:) forControlEvents:UIControlEventTouchDown];
        [cell.BtnBarcode setTitle:@"Generate Barcode" forState:UIControlStateNormal];
        [cell.imgViewBarcode setHidden:YES];
        
    } else {
        
        [cell.BtnBarcodePrint setHidden:NO];
        [cell.BtnBarcode setHidden:YES];

        [cell.BtnBarcodePrint addTarget:self action:@selector(methodBarcodePrint:) forControlEvents:UIControlEventTouchDown];
        [cell.BtnBarcodePrint setTitle:@"Print Barcode" forState:UIControlStateNormal];
        [cell.imgViewBarcode setHidden:NO];
        
    }

    if (isCompletedStatusMechanical) {
        
        [cell.btnCheckBox setEnabled:NO];
        [cell.btnSaveServiceDate setEnabled:NO];
        [cell.BtnAdd setEnabled:NO];
        [cell.BtnDelete setEnabled:NO];
        [cell.btnServiceDateToSave setEnabled:NO];
        [cell.BtnBarcodePrint setEnabled:YES];
        [cell.BtnBarcode setEnabled:NO];
        
    }else{
        
        [cell.btnCheckBox setEnabled:YES];
        [cell.btnSaveServiceDate setEnabled:YES];
        [cell.BtnAdd setEnabled:YES];
        [cell.BtnDelete setEnabled:YES];
        [cell.btnServiceDateToSave setEnabled:YES];
        [cell.BtnBarcode setEnabled:YES];
        [cell.BtnBarcodePrint setEnabled:YES];
        
    }
    
    [cell.BtnDelete setTitle:@"Delete" forState:UIControlStateNormal];
    [cell.BtnAdd setTitle:@"Edit" forState:UIControlStateNormal];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==1874919423) {
        
    } else {
        
        if (!(arrDataTblView.count==0)) {
            
            
            NSInteger i;
            i=tblData.tag;
            
            if (i<1874919417) {
                
                if (isMultipleSelect) {
                    NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
                    UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
                    if ([cell accessoryType] == UITableViewCellAccessoryNone) {
                        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        [arraySelecteValues addObject:[dictData valueForKey:@"Value"]];
                        [arraySelecteValuesToShow addObject:[dictData valueForKey:@"Name"]];
                    }
                    else {
                        [cell setAccessoryType:UITableViewCellAccessoryNone];
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        [arraySelecteValues removeObject:[dictData valueForKey:@"Value"]];
                        [arraySelecteValuesToShow removeObject:[dictData valueForKey:@"Name"]];
                    }
                    
                    
                    
                    // NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    
                    NSInteger btnIndex;
                    btnIndex=tblData.tag;
                    indexMultipleSelectTosetValue=(int)btnIndex;
                    
                }
                else{
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    
                    NSInteger btnIndex;
                    btnIndex=tblData.tag;
                    
                    UIButton *btnDropdown=arrOfControlssMain[btnIndex];
                    
                    [btnDropdown setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                    
                    [self commonMethodToSaveValue:[dictData valueForKey:@"Name"] :btnIndex];
                    
                }
                
            }else{
                
                switch (i)
                {
                    case 1874919418:
                    {
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        [_btn_Category setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                        strCategoryId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategoryMasterId"]];
                        strCategorySysName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
                        
                        //Setting Current date ON Installed date
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                        [dateFormat setDateFormat:@"MM/dd/yyyy"];
                        strDateEquip = [dateFormat stringFromDate:[NSDate date]];
                        [_btn_InstalledOn setTitle:strDateEquip forState:UIControlStateNormal];
                        strInstalledOn=strDateEquip;
                        isScannedResult=NO;
                        
                        [_btn_EquipmentsName setTitle:@"Select" forState:UIControlStateNormal];
                        strItemSysName=@"";
                        strItemNumber=@"";
                        
                        break;
                    }
                    case 1874919419:
                    {
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        [_btn_Equipment setTitle:[dictData valueForKey:@"Text"] forState:UIControlStateNormal];
                        strEquipmentValueId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                        isScannedResult=NO;
                        break;
                    }
                    case 1874919420:
                    {
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        [_btn_EquipmentsName setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                        strItemSysName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
                        strItemNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ItemNumber"]];
                        isScannedResult=NO;
                        break;
                    }
                    case 1874919421:
                    {
                        //NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        [_btn_Area setTitle:[arrDataTblView objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                        strAreaSysName=[NSString stringWithFormat:@"%@",[arrDataTblView objectAtIndex:indexPath.row]];
                        break;
                    }
                    case 1874919422:
                    {
                        NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                        [_btn_AssociatedWith setTitle:[dictData valueForKey:@"EquipName"] forState:UIControlStateNormal];
                        strAssociatedWithId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WoEquipmentId"]];
                        if (strAssociatedWithId.length==0) {
                            
                            strAssociatedWithId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MobileUniqueId"]];
                            
                        }
                        strAssociatedWith=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EquipName"]];
                        
                        break;
                    }
                    case 1874919424:
                    {
                        NSDictionary *dictData=[arrOfSerialNosFiltered objectAtIndex:indexPath.row];
                        _txtFieldSearch.text=[dictData valueForKey:@"SerialNumber"];
                        break;
                    }
                    case 1874919425:
                    {
                        NSDictionary *dictData=[arrOfModelNosFiltered objectAtIndex:indexPath.row];
                        _txtFieldSearch.text=[dictData valueForKey:@"ModelNumber"];
                        break;
                    }
                    case 1874919426:
                    {
                        NSDictionary *dictData=[arrOfManufacturerFiltered objectAtIndex:indexPath.row];
                        _txtFieldSearch.text=[dictData valueForKey:@"Manufacturer"];
                        break;
                    }
                        
                    default:
                        break;
                }
                
            }
        }
        
        if (isMultipleSelect) {
            
            
        } else {
            
            [viewBackGround removeFromSuperview];
            [tblData removeFromSuperview];
            
        }
    }
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Cell Button Methods-----------------
//============================================================================
//============================================================================

-(void)methodCheckBox:(id)sender
{
    yesEditedSomething=YES;
    
    UIButton *btn = (UIButton *)sender;
    NSDictionary *dictDataEquip=arrOfEquipmentList[btn.tag];
    NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"IsServiceStatus"]];
    if ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) {
        
        [self replaceEquipments:(int)btn.tag:@"false"];
        
    }else{
        
        [self replaceEquipments:(int)btn.tag:@"true"];
        
    }
    
    //    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    //    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    //    [_equipmentTblview reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    
    [_equipmentTblview reloadData];
    
}

-(void)methodSaveServiceDate:(id)sender
{
    yesEditedSomething=YES;
    
    UIButton *btn = (UIButton *)sender;
    [self replaceEquipmentsServiceDate:(int)btn.tag:@"false"];
    [_equipmentTblview reloadData];
    
    //    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    //    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    //    [_equipmentTblview reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    
}
-(void)methodAddDynamicForm:(id)sender
{
    yesEditedSomething=YES;
    UIButton *btn = (UIButton *)sender;
    NSDictionary *dictDataEquip=arrOfEquipmentList[btn.tag];
    
    NSString *strWoEquipIdd=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"WOEquipmentId"]];
    if (strWoEquipIdd.length==0) {
        
        NSString *strMobileEquipIdd=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"MobileUniqueId"]];
        strGlobalWoEquipIdToFetchDynamicData=strMobileEquipIdd;
        isWoEquipIdPrsent=NO;
    } else {
        isWoEquipIdPrsent=YES;
        strGlobalWoEquipIdToFetchDynamicData=strWoEquipIdd;
    }
    strGlobalCatSysNameToFetchDynamicForm=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"CategorySysName"]];
    
    [self fetchEquipmentsDynamicForm];
    
}
-(void)methodToSelectdateService:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    globalIndexForNxtserviceDate=(int)btn.tag;
    
    NSLog(@"Btn Tag being Pressed===%ld",(long)btn.tag);
    
    NSDictionary *dictDataEquip=arrOfEquipmentList[(int)btn.tag];
    strGlobalCellForRowNxtServiceDate =[NSString stringWithFormat:@"%@",[dictDataEquip
                                                                         valueForKey:@"strInstalledOn"]];
    [self addingDatePicker:@"" :110];
}

-(void)methodDeleteEquipments:(id)sender
{
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:Alert
                                   message:@"Are you sure to delete equipment"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Yes-Delete" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  yesEditedSomething=YES;
                                  
                                  UIButton *btn = (UIButton *)sender;
                                  
                                  NSManagedObject *objData=arrOfMechanicalWoEquipment[btn.tag];
                                  
                                  [self fetchWoEquipmentFromDBViaEquipIdToDelete:[objData valueForKey:@"accountItemHistoryId"]];
                                  
                              }];
        [alert addAction:yes];
        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                             }];
        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
        
}

-(void)methodEditEquipments:(id)sender
{
    isUpdateEquip=YES;
    
    yesEditedSomething=YES;
    
    UIButton *btn = (UIButton *)sender;
    
    NSManagedObject *objData=arrOfMechanicalWoEquipment[btn.tag];
    
    [self fetchWoEquipmentFromDBViaEquipId:[objData valueForKey:@"accountItemHistoryId"]];
    
}


-(void)methodBarcodePrint:(id)sender
{
    BOOL isNetReachable=[global isNetReachable];
    
    if (!isNetReachable) {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    } else {
        
        UIButton *btn = (UIButton *)sender;
        
        NSManagedObject *objData=arrOfMechanicalWoEquipment[btn.tag];
        
        NSString *strBarcodeUrl=[NSString stringWithFormat:@"%@",[objData valueForKey:@"barcodeUrl"]];
        
        if ([strBarcodeUrl containsString:@"http"]) {
            
            strBarcodeUrl=[NSString stringWithFormat:@"%@",[objData valueForKey:@"barcodeUrl"]];
            
        } else {
            
            strBarcodeUrl=[NSString stringWithFormat:@"http://tcr.stagingsoftware.com/%@",[objData valueForKey:@"barcodeUrl"]];
            
        }
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Barcode..."];
        
        strGlobalBarcodeURL=strBarcodeUrl;
        
        [self performSelector:@selector(openPrintOption:) withObject:nil afterDelay:0.5];

    }
    
}

-(void)action_ViewMoreEquipName:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;

    NSString *strEquipName;
    
    NSManagedObject *dictDataEquip=arrOfMechanicalWoEquipment[btn.tag];
    NSString *strItemName=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"itemName"]];
    NSString *strItemCode=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"itemcode"]];
    NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[dictDataEquip
                                                                  valueForKey:@"itemCustomName"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    if (strItemName.length>0) {
        
        [arrTemp addObject:strItemName];
        
    }
    
    if (strItemCode.length>0) {
        
        [arrTemp addObject:strItemCode];
        
    }
    if (strItemCustomName.length>0) {
        
        [arrTemp addObject:strItemCustomName];
        
    }
    if (arrTemp.count==1) {
        
        strEquipName=[NSString stringWithFormat:@"Equip.Name(#):%@",[dictDataEquip
                                                                               valueForKey:@"itemName"]];
    } else if (arrTemp.count==2){
        
        strEquipName=[NSString stringWithFormat:@"Equip.Name(#):%@(%@)",[dictDataEquip
                                                                                   valueForKey:@"itemName"],[dictDataEquip valueForKey:@"itemcode"]];
    } else if (arrTemp.count==3){
        
        strEquipName=[NSString stringWithFormat:@"Equip.Name(#):%@(%@)-%@",[dictDataEquip
                                                                                      valueForKey:@"itemName"],[dictDataEquip valueForKey:@"itemcode"],[dictDataEquip
                                                                                                                                                        valueForKey:@"itemCustomName"]];
    }
    
    [global AlertMethod:@"Equip.Name(#)" :strEquipName];
    
}


-(void)openPrintOption :(NSString*)strUrlPdf{
    
    strUrlPdf=strGlobalBarcodeURL;
    
    if (strUrlPdf.length==0) {
        
        [global AlertMethod:Alert :@"No Barcode Available"];
        [DejalBezelActivityView removeView];
        
    } else {
        
        NSString *strNewString=[strUrlPdf stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:strNewString];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        [DejalBezelActivityView removeView];
        if (data==nil) {
            
            [global AlertMethod:Alert :@"No Barcode Available"];
            
        } else {
            
            NSArray *items = @[data];
            
            // build an activity view controller
            UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
            
            // and present it
            [self presentActivityController:controller];
        }
    }
}

- (void)presentActivityController:(UIActivityViewController *)controller {
    // for iPad: make the presentation a Popover
    controller.modalPresentationStyle = UIModalPresentationPopover;
    controller.popoverPresentationController.sourceView = self.view;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionUnknown;
    UINavigationController* nav = [UINavigationController new];
    popController.barButtonItem = nav.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
            NSLog(@"We used activity type%@", activityType);
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}

-(void)methodBarcodeGenerate:(id)sender
{
    BOOL isNetReachable=[global isNetReachable];
    
    if (!isNetReachable) {
        
        [global AlertMethod:Alert :ErrorInternetMsg];
        
    } else {
        
        UIButton *btn = (UIButton *)sender;
        
        NSManagedObject *objData=arrOfMechanicalWoEquipment[btn.tag];
        
        strEquipIdToGenerateBarcode=[NSString stringWithFormat:@"%@",[objData valueForKey:@"accountItemHistoryId"]];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Generating Barcode..."];
        
        [self performSelector:@selector(fetchWoEquipmentFromDBViaEquipIdToGenerateBarcode) withObject:nil afterDelay:0.5];
        
    }
}


-(void)replaceEquipments :(int)indexToReplace :(NSString *)trueOrFalse{
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    NSDictionary *oldDict = (NSDictionary *)[arrOfEquipmentList objectAtIndex:indexToReplace];
    [newDict addEntriesFromDictionary:oldDict];
    [newDict setValue:trueOrFalse forKey:@"IsServiceStatus"];
    [arrOfEquipmentList replaceObjectAtIndex:indexToReplace withObject:newDict];
    
}
-(void)replaceEquipmentsStatusRemoved :(int)indexToReplace :(NSString *)trueOrFalse{
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    NSDictionary *oldDict = (NSDictionary *)[arrOfEquipmentList objectAtIndex:indexToReplace];
    [newDict addEntriesFromDictionary:oldDict];
    [newDict setValue:trueOrFalse forKey:@"IsRemoved"];
    [arrOfEquipmentList replaceObjectAtIndex:indexToReplace withObject:newDict];
    
}
-(void)replaceEquipmentsServiceDate :(int)indexToReplace :(NSString *)trueOrFalse{
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    NSDictionary *oldDict = (NSDictionary *)[arrOfEquipmentList objectAtIndex:indexToReplace];
    [newDict addEntriesFromDictionary:oldDict];
    NSString *strDateNxtService=arrOfNextServiceDateLocally[indexToReplace];
    if ([strDateNxtService isEqualToString:@"Select Next Service Date"]) {
        strDateNxtService=@"";
    }
    [newDict setValue:strDateNxtService forKey:@"strNextServiceDate"];
    //NextServiceDate
    // [newDict setValue:strDateNxtService forKey:@"NextServiceDate"];
    [arrOfEquipmentList replaceObjectAtIndex:indexToReplace withObject:newDict];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------******** Modify Date Save *******----------------
//============================================================================
//============================================================================

-(void)updateModifyDate{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityModifyDateServiceAuto];
    
    //NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
    }
    else
    {
        matchesServiceModifyDate=arrAllObjServiceModifyDate[0];
        
        NSString *strCurrentDateFormatted = [global modifyDateService];
        
        [matchesServiceModifyDate setValue:strCurrentDateFormatted forKey:@"modifyDate"];
        
        [global fetchWorkOrderFromDataBaseForServiceToUpdateModifydate:strWorkOrderId :strCurrentDateFormatted];
        
        //Final Save
        NSError *error;
        [context save:&error];
    }
}

- (IBAction)action_CancelDynamcView:(id)sender {
    if (arrOfEquipmentList.count==1) {
        //  [self addingEquipmentView];
    }
    [_viewBackgroundDynamicForm removeFromSuperview];
}


//============================================================================
//============================================================================
#pragma mark- ---------------------Dynamic Form Methods-----------------
//============================================================================
//============================================================================


-(void)addDynamicBackgroundView{
    
    [ViewFormSections removeFromSuperview];
    ViewFormSections=nil;
    [MainViewForm removeFromSuperview];
    MainViewForm=nil;
    BtnMainView=nil;
    arrayOfButtonsMain=nil;
    arrOfButtonImages=nil;
    arrOfIndexes=nil;
    arrOfControlssMain=nil;
    arrayViews=nil;
    arrOfHeightsViews=nil;
    arrOfYAxisViews=nil;
    
    arrayViews = [[NSMutableArray alloc] init];
    arrayOfButtonsMain = [[NSMutableArray alloc] init];
    arrOfHeightsViews = [[NSMutableArray alloc] init];
    arrOfYAxisViews = [[NSMutableArray alloc] init];
    arrOfButtonImages = [[NSMutableArray alloc] init];
    arrOfIndexes = [[NSMutableArray alloc] init];
    arrOfControlssMain = [[NSMutableArray alloc] init];
    
    _viewBackgroundDynamicForm.frame=CGRectMake(0, 80,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-80);
    [self.view addSubview:_viewBackgroundDynamicForm];
    
}
-(void)fetchEquipmentsDynamicForm
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [self FetchFromCoreDataToShowEquipmentsDynamic];
    }
    else
    {
        
        [self FetchFromCoreDataToShowEquipmentDynamicInCaseOnline];
        
    }
}

-(void)FetchFromCoreDataToShowEquipmentsDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate;
    if (isWoEquipIdPrsent) {
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND woEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    } else {
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND mobileEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    }
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        [self getDynamicFormEquipment];
    }
    else
    {
        matches=arrAllObj[0];
        NSArray *arrTemp = [matches valueForKey:@"arrFinalInspection"];
        
        if ([arrTemp isKindOfClass:[NSDictionary class]]) {
            
            arrResponseInspection =[[NSMutableArray alloc]init];
            NSDictionary *dictdata=(NSDictionary*)arrTemp;
            [arrResponseInspection addObject:dictdata];
            
        } else {
            
            arrResponseInspection =[[NSMutableArray alloc]init];
            [arrResponseInspection addObjectsFromArray:arrTemp];
            
        }
        
        [self dynamicFormCreation];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)FetchFromCoreDataToShowEquipmentDynamicInCaseOnline{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate;
    if (isWoEquipIdPrsent) {
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND woEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    } else {
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND mobileEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    }
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND woEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
        [self getDynamicFormEquipment];
        
    }
    else
    {
        matches=arrAllObj[0];
        
        NSString *strIsSentToServer=[matches valueForKey:@"isSentToServer"];
        strIsSentToServer=@"no";
        if ([strIsSentToServer isEqualToString:@"Yes"]) {
            
            [self getDynamicFormEquipment];
            
        } else {
            
            NSArray *arrTemp = [matches valueForKey:@"arrFinalInspection"];
            arrResponseInspection =[[NSMutableArray alloc]init];
            
            NSDictionary *dictDataService;
            
            if ([arrTemp isKindOfClass:[NSDictionary class]]) {
                
                dictDataService=(NSDictionary*)arrTemp;
                
            } else {
                
                dictDataService=arrTemp[0];
                
            }
            
            [arrResponseInspection addObject:dictDataService];
            [self dynamicFormCreation];
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)getDynamicFormEquipment{
    
    NSUserDefaults *defsDynamic=[NSUserDefaults standardUserDefaults];
    
    NSArray *arrOfMasterDynamicData=[defsDynamic objectForKey:@"MasterEquipmentsDynamicForm"];
    
    NSMutableArray *arrOfDynamicDataInspections=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfMasterDynamicData.count; k++) {
        
        NSDictionary *dictData=arrOfMasterDynamicData[k];
        
        NSString *strBranchSysNameD=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
        
        if ([strBranchSysNameD isEqualToString:strGlobalCatSysNameToFetchDynamicForm]) {
            
            NSString *strDepartmentSysNameD=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
            
            if ([strDepartmentSysNameD isEqualToString:strDepartmentIdGlobal]) {
                
                [arrOfDynamicDataInspections addObject:dictData];
                
            }else if([strDepartmentSysNameD caseInsensitiveCompare:@"General"] == NSOrderedSame){
                
                [arrOfDynamicDataInspections addObject:dictData];
                
            }
        }
    }
    
    if (arrOfDynamicDataInspections.count>0) {
        
        arrResponseInspection =[[NSMutableArray alloc]init];
        arrResponseInspection =[[NSMutableArray alloc]init];
        [arrResponseInspection addObjectsFromArray:arrOfDynamicDataInspections];
        
        for (int k=0; k<arrOfDynamicDataInspections.count; k++) {
            
            [self saveEquipIdInMasterForms:k];
            
        }
        
        [self saveToCoreDataSalesDynamic:arrResponseInspection];
        
    }else{
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Info" message:@"No Inspection data available for this equipment." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

-(void)saveEquipIdInMasterForms :(int)indexReplace{
    
    NSDictionary *dictData=arrResponseInspection[indexReplace];
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSArray *objValuearrResponseInspection=[NSArray arrayWithObjects:
                                            [dictData valueForKey:@"InspectionId"],
                                            strWorkOrderId,
                                            [dictData valueForKey:@"FormId"],
                                            [dictData valueForKey:@"FormName"],
                                            [dictData valueForKey:@"CshtmlFileName"],
                                            [dictData valueForKey:@"CshtmlFilePath"],
                                            [dictData valueForKey:@"HtmlConvertedFileName"],
                                            [dictData valueForKey:@"HtmlConvertedFilePath"],
                                            [dictData valueForKey:@"HtmlFileName"],
                                            [dictData valueForKey:@"HtmlFilePath"],
                                            isWoEquipIdPrsent ? strGlobalWoEquipIdToFetchDynamicData : @"",
                                            isWoEquipIdPrsent ? @"" : strGlobalWoEquipIdToFetchDynamicData,
                                            [dictData valueForKey:@"DepartmentName"],
                                            [dictData valueForKey:@"DepartmentSysName"],
                                            [dictData valueForKey:@"CategoryName"],
                                            [dictData valueForKey:@"CategorySysName"],
                                            arrSections,nil];
    
    NSArray *objKeyarrResponseInspection=[NSArray arrayWithObjects:
                                          @"InspectionId",
                                          @"WorkorderId",
                                          @"FormId",
                                          @"FormName",
                                          @"CshtmlFileName",
                                          @"CshtmlFilePath",
                                          @"HtmlConvertedFileName",
                                          @"HtmlConvertedFilePath",
                                          @"HtmlFileName",
                                          @"HtmlFilePath",
                                          @"WOEquipmentId",
                                          @"MobileUniqueId",
                                          @"DepartmentName",
                                          @"DepartmentSysName",
                                          @"CategoryName",
                                          @"CategorySysName",
                                          @"Sections",nil];
    
    NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
    
    [arrResponseInspection replaceObjectAtIndex:indexReplace withObject:dictOfarrResponseInspection];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Core Data Methods-----------------
//============================================================================
//============================================================================


-(void)saveToCoreDataSalesDynamic :(NSArray*)arrOfSalesDynamicToSave{
    
    //==================================================================================
    //==================================================================================
    [self deleteFromCoreDataEquipmentsDynamic];
    //==================================================================================
    //==================================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context];
    
    EquipmentDynamicForm *objSalesDynamic = [[EquipmentDynamicForm alloc]initWithEntity:entitySalesDynamic insertIntoManagedObjectContext:context];
    objSalesDynamic.arrFinalInspection=arrOfSalesDynamicToSave;
    objSalesDynamic.workorderId=strWorkOrderId;
    objSalesDynamic.companyKey=strCompanyKey;
    objSalesDynamic.userName=strUserName;
    objSalesDynamic.empId=strEmpID;
    
    if (isWoEquipIdPrsent) {
        
        objSalesDynamic.woEquipId=strGlobalWoEquipIdToFetchDynamicData;
        
    } else {
        
        objSalesDynamic.mobileEquipId=strGlobalWoEquipIdToFetchDynamicData;
        
    }
    
    // objSalesDynamic.departmentId=strDepartmentId;
    objSalesDynamic.isSentToServer=@"Yes";
    
    NSError *error1;
    [context save:&error1];
    
    //==================================================================================
    //==================================================================================
    [self FetchFromCoreDataToShowEquipmentsDynamic];
    //==================================================================================
    //==================================================================================
    
}

-(void)deleteFromCoreDataEquipmentsDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context];
    
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate;
    if (isWoEquipIdPrsent) {
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND woEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    } else {
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND mobileEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    }
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND %@ = %@",strWorkOrderId,strUsername,strTypeOfFilter,strGlobalWoEquipIdToFetchDynamicData];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND woEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    [allData setPredicate:predicate];
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


//-(void)dynamicFormCreation{
//
//    [self addDynamicBackgroundView];
//
//}

-(void)dynamicFormCreation{
    
    [self addDynamicBackgroundView];
    
    //    //Temporary HAi Comment KArna Yad SE
    //    arrResponseInspection=[[NSMutableArray alloc]init];
    //    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    //    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"DynamicFormJson.json"];
    //    NSError * error;
    //    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    //    NSArray *arrOfCountry = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    //    [arrResponseInspection addObjectsFromArray:arrOfCountry];
    
    
    CGFloat scrollViewHeight=0.0;
    
    arrOfIndexes =[[NSMutableArray alloc]init];
    arrOfControlssMain =[[NSMutableArray alloc]init];
    int tagToSetForIndexes=0;
    
    for (int i=0; i<arrResponseInspection.count; i++)
    {
        NSDictionary *dictMain=[arrResponseInspection objectAtIndex:i];
        if (!MainViewForm.frame.size.height) {
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, 52)];
        }
        else{
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 52)];
        }
        BtnMainView.tag=i;
        [BtnMainView addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        //Nilind 28 Feb
        [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentSysName"] forState:UIControlStateNormal];
        /*
         NSString *strDept=[dictDeptNameByKey valueForKey:[dictMain valueForKey:@"DepartmentSysName"]];
         if ([strDept isEqual:nil]||[strDept isEqualToString:@""])
         {
         strDept=@"No Department Available";
         }
         
         [BtnMainView setTitle:strDept forState:UIControlStateNormal];*/
        //End
        // [BtnMainView setTitle:@"Title for Button Main View To See Whole Text On Mobile iOS." forState:UIControlStateNormal];
        
        BtnMainView.titleLabel.numberOfLines=20000;
        BtnMainView.titleLabel.font=[UIFont systemFontOfSize:22];
        
        [BtnMainView setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)];
        
        UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 40, 40)];
        imgView.image=[UIImage imageNamed:@"minus_icon.png"];
        
        //scrollViewHeight=scrollViewHeight+BtnMainView.frame.size.height;
        [BtnMainView addSubview:imgView];
        //_scrollViewDynamicForm
        [_scrollViewDynamicForm addSubview:BtnMainView];
        
        [arrayOfButtonsMain addObject:BtnMainView];
        [arrOfButtonImages addObject:imgView];
        
        // Form Creation
        MainViewForm=[[UIView alloc]init];
        
        MainViewForm.backgroundColor=[UIColor clearColor];
        
        MainViewForm.frame=CGRectMake(0, BtnMainView.frame.origin.y+BtnMainView.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 1000);
        
        MainViewForm.tag=i+3000;
        
        [_scrollViewDynamicForm addSubview:MainViewForm];
        
        NSArray *arrSections=[dictMain valueForKey:@"Sections"];
        
        
        int yAxisViewFormSections=0.0;
        int heightVIewFormSections=0.0;
        int heightMainViewFormSections=0.0;
        
        
        for (int j=0; j<arrSections.count; j++)
        {
            NSDictionary *dictSection=[arrSections objectAtIndex:j];
            NSArray *arrControls=[dictSection valueForKey:@"Controls"];
            
            // Section Creation Start
            heightVIewFormSections=0.0;
            ViewFormSections=[[UIView alloc]init];
            ViewFormSections.frame=CGRectMake(0, yAxisViewFormSections, [UIScreen mainScreen].bounds.size.width, 400);
            
            ViewFormSections.backgroundColor=[UIColor whiteColor];
            UILabel *lblTitleSection=[[UILabel alloc]init];
            lblTitleSection.backgroundColor=[UIColor lightGrayColor];
            lblTitleSection.layer.borderWidth = 1.5f;
            lblTitleSection.layer.cornerRadius = 4.0f;
            lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
            
            lblTitleSection.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50);
            lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictSection valueForKey:@"Name"]] ;
            lblTitleSection.textAlignment=NSTextAlignmentCenter;
            lblTitleSection.font=[UIFont systemFontOfSize:18];
            [ViewFormSections addSubview:lblTitleSection];
            
            ViewFormSections.layer.borderWidth = 1.5f;
            ViewFormSections.layer.cornerRadius = 4.0f;
            ViewFormSections.layer.borderColor = [[UIColor grayColor] CGColor];
            
            [MainViewForm addSubview:ViewFormSections];
            
            //CGFloat xPositionOfControls= 0.0;
            CGFloat yPositionOfControls = 0.0;
            CGFloat heightOfControls= 0.0;
            CGFloat widthOfControls= 0.0;
            
            for (int k=0; k<arrControls.count; k++)
            {
                NSDictionary *dictControls=[arrControls objectAtIndex:k];
                UILabel *lblTitleControl=[[UILabel alloc]init];
                lblTitleControl.backgroundColor=[UIColor lightGrayColor];
                lblTitleControl.frame=CGRectMake(0, 50*k+35, [UIScreen mainScreen].bounds.size.width, 50);
                lblTitleControl.text=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Label"]] ;
                
                lblTitleControl.textAlignment=NSTextAlignmentCenter;
                lblTitleControl.font=[UIFont systemFontOfSize:18];
                
                // [ViewFormSections addSubview:lblTitleControl];
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - CheckBox-Combo
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox-combo"]) {
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    NSArray *arrOfIndexChecked;
                    if (!(strIndexOfValue.length==0)) {
                        arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    }
                    
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    if (s.height<50) {
                        s.height=50;
                    }
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:18];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,35,35);
                    
                    NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                    NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                    NSMutableArray *arrOfValuesToCheck=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOptions.count; k++) {
                        
                        NSDictionary *dictData=arrOptions[k];
                        
                        [arrOfValues addObject:[dictData valueForKey:@"Label"]];
                        [arrOfValuesToCheck addObject:[dictData valueForKey:@"Value"]];
                        
                    }
                    int btnCount=(int)[arrOfValues count];
                    
                    int viewHeight = 0;
                    
                    for (int l=0; l<btnCount; l++){
                        
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.frame = btnFrame;
                        NSString *title = [arrOfValues objectAtIndex:l];
                        
                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                        
                        [arrOfIndexes addObject:strTaggg];
                        
                        btn.tag=tagToSetForIndexes;
                        
                        tagToSetForIndexes++;
                        
                        btn.titleLabel.textColor = [UIColor clearColor];
                        btn.titleLabel.font=[UIFont systemFontOfSize:18];
                        [btn setTitle:title forState:UIControlStateNormal];
                        
                        [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                        
                        for (int s=0; s<arrOfIndexChecked.count; s++) {
                            
                            NSString *strValueTocompare=[NSString stringWithFormat:@"%@",arrOfIndexChecked[s]];
                            if ([[arrOfValuesToCheck objectAtIndex:l] isEqualToString:strValueTocompare]) {
                                [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                            }
                            
                        }
                        
                        if (isCompletedStatusMechanical) {
                            [btn setEnabled:NO];
                        }else{
                            [btn setEnabled:YES];
                        }
                        
                        [btn addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        
                        // btnFrame.origin.y+=btnFrame.size.height+15;
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+50)];
                        [view addSubview:btn];
                        
                        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnHidden.frame =CGRectMake(0, btnFrame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 55);
                        btnHidden.tag=tagToSetForIndexes-1;
                        [btnHidden setBackgroundColor:[UIColor clearColor]];
                        [btnHidden addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        [view addSubview:btnHidden];
                        
                        if (isCompletedStatusMechanical) {
                            [btnHidden setEnabled:NO];
                        } else {
                            [btnHidden setEnabled:YES];
                        }
                        
                        [arrOfControlssMain addObject:btn];
                        
                        UILabel *lblItem = [[UILabel alloc] init];
                        
                        CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice]-80, MAXFLOAT)];
                        
                        if (lblItemHeight.height<50) {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, [global globalWidthDevice]-80, 50);
                            
                        } else {
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, [global globalWidthDevice]-80, lblItemHeight.height);
                            
                        }
                        [lblItem setFont:[UIFont systemFontOfSize:18]];
                        //lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =title;
                        [view addSubview:lblItem];
                        
                        btnFrame.origin.y+=lblItem.frame.size.height+5;
                        
                        int tempheight=lblItem.frame.size.height;
                        
                        viewHeight =viewHeight+tempheight;
                        
                    }
                    
                    viewHeight=viewHeight+lbl.frame.size.height+lbl.frame.origin.y+5*btnCount;
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight)];
                    
                    [ViewFormSections addSubview:view];
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - CheckBox
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox"]) {
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    NSArray *arrOfIndexChecked;
                    if (!(strIndexOfValue.length==0)) {
                        arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    }
                    
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    if (s.height<50) {
                        s.height=50;
                    }
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:18];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,35,35);
                    
                    NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                    NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                    NSMutableArray *arrOfValuesToCheck=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOptions.count; k++) {
                        
                        NSDictionary *dictData=arrOptions[k];
                        
                        [arrOfValues addObject:[dictData valueForKey:@"Label"]];
                        [arrOfValuesToCheck addObject:[dictData valueForKey:@"Value"]];
                        
                    }
                    int btnCount=(int)[arrOfValues count];
                    
                    int viewHeight = 0;
                    
                    for (int l=0; l<btnCount; l++){
                        
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.frame = btnFrame;
                        NSString *title = [arrOfValues objectAtIndex:l];
                        
                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                        
                        [arrOfIndexes addObject:strTaggg];
                        
                        btn.tag=tagToSetForIndexes;
                        
                        tagToSetForIndexes++;
                        
                        btn.titleLabel.textColor = [UIColor clearColor];
                        btn.titleLabel.font=[UIFont systemFontOfSize:18];
                        [btn setTitle:title forState:UIControlStateNormal];
                        
                        [btn setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
                        
                        for (int s=0; s<arrOfIndexChecked.count; s++) {
                            
                            NSString *strValueTocompare=[NSString stringWithFormat:@"%@",arrOfIndexChecked[s]];
                            if ([[arrOfValuesToCheck objectAtIndex:l] isEqualToString:strValueTocompare]) {
                                [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                            }
                            
                        }
                        
                        if (isCompletedStatusMechanical) {
                            [btn setEnabled:NO];
                        }else{
                            [btn setEnabled:YES];
                        }
                        
                        [btn addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        
                        // btnFrame.origin.y+=btnFrame.size.height+15;
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+50)];
                        [view addSubview:btn];
                        
                        
                        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnHidden.frame =CGRectMake(0, btnFrame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 55);
                        btnHidden.tag=tagToSetForIndexes-1;
                        [btnHidden setBackgroundColor:[UIColor clearColor]];
                        [btnHidden addTarget:self action:@selector(checkCheckBoxes:) forControlEvents:UIControlEventTouchDown];
                        [view addSubview:btnHidden];
                        
                        if (isCompletedStatusMechanical) {
                            [btnHidden setEnabled:NO];
                        } else {
                            [btnHidden setEnabled:YES];
                        }
                        
                        [arrOfControlssMain addObject:btn];
                        
                        UILabel *lblItem = [[UILabel alloc] init];
                        
                        CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice]-80, MAXFLOAT)];
                        
                        if (lblItemHeight.height<50) {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, [global globalWidthDevice]-80, 50);
                            
                        } else {
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, [global globalWidthDevice]-80, lblItemHeight.height);
                            
                        }
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =title;
                        [view addSubview:lblItem];
                        
                        btnFrame.origin.y+=lblItem.frame.size.height+5;
                        
                        int tempheight=lblItem.frame.size.height;
                        
                        viewHeight =viewHeight+tempheight;
                        
                    }
                    
                    viewHeight=viewHeight+lbl.frame.size.height+lbl.frame.origin.y+5*btnCount;
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight)];
                    
                    [ViewFormSections addSubview:view];
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Radio---Combo
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                //el-radio-combo
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio-combo"] ||[[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"]){
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    if (s.height<50) {
                        s.height=50;
                    }
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:18];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,35,35);
                    
                    NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                    NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                    
                    int indexToCheck=-1;
                    
                    for (int k=0; k<arrOptions.count; k++) {
                        
                        NSDictionary *dictData=arrOptions[k];
                        
                        [arrOfValues addObject:[dictData valueForKey:@"Label"]];
                        
                        if ([[dictData valueForKey:@"Value"] isEqualToString:strIndexOfValue]) {
                            
                            indexToCheck=k;
                            
                        }
                        
                    }
                    int btnCount=(int)[arrOfValues count];
                    
                    int viewHeight = 0;
                    
                    NSMutableArray *arrOfTempRadioBtns=[[NSMutableArray alloc]init];
                    
                    for (int l=0; l<btnCount; l++){
                        
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.frame = btnFrame;
                        NSString *title = [arrOfValues objectAtIndex:l];
                        
                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                        
                        [arrOfIndexes addObject:strTaggg];
                        
                        [btn setTitle:@"RadioBtnnComboooo" forState:UIControlStateNormal];
                        btn.titleLabel.textColor = [UIColor clearColor];
                        btn.titleLabel.font=[UIFont systemFontOfSize:18];
                        //                            if (l==0) {
                        //
                        //                                [btn setTitle:@"RadioBtnnCombooooFirst" forState:UIControlStateNormal];
                        //
                        //                            }
                        //                            if (l==btnCount-1) {
                        //
                        //                                [btn setTitle:@"RadioBtnnCombooooLast" forState:UIControlStateNormal];
                        //
                        //                            }
                        
                        btn.tag=tagToSetForIndexes;
                        
                        tagToSetForIndexes++;
                        
                        
                        
                        //                            if(!(strIndexOfValue.length==0)){
                        //
                        //                               indexToCheck =[strIndexOfValue intValue];
                        //
                        //                            }
                        
                        if (indexToCheck==l) {
                            
                            [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                            
                        }else{
                            
                            [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                            
                        }
                        
                        if (isCompletedStatusMechanical) {
                            [btn setEnabled:NO];
                        }else{
                            [btn setEnabled:YES];
                        }
                        
                        [btn addTarget:self action:@selector(checkRadioButtonCombo:) forControlEvents:UIControlEventTouchDown];
                        
                        // btnFrame.origin.y+=btnFrame.size.height+15;
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 50)];
                        [view addSubview:btn];
                        
                        UIButton *btnHidden = [UIButton buttonWithType:UIButtonTypeCustom];
                        btnHidden.frame =CGRectMake(0, btnFrame.origin.y-10,[UIScreen mainScreen].bounds.size.width, 55);
                        btnHidden.tag=tagToSetForIndexes-1;
                        [btnHidden setBackgroundColor:[UIColor clearColor]];
                        [btnHidden addTarget:self action:@selector(checkRadioButtonCombo:) forControlEvents:UIControlEventTouchDown];
                        [view addSubview:btnHidden];
                        
                        if (isCompletedStatusMechanical) {
                            [btnHidden setEnabled:NO];
                        } else {
                            [btnHidden setEnabled:YES];
                        }
                        
                        [arrOfTempRadioBtns addObject:btn];
                        
                        // [arrOfControlssMain addObject:arrOfTempRadioBtns];
                        
                        //[arrOfControlssMain addObject:btn];
                        
                        UILabel *lblItem = [[UILabel alloc] init];
                        CGSize lblItemHeight = [title sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice]-80, MAXFLOAT)];
                        
                        if (lblItemHeight.height<50) {
                            
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, [global globalWidthDevice]-80, 50);
                            
                        } else {
                            lblItem.frame = CGRectMake(btn.frame.origin.x+45, btn.frame.origin.y+5, [global globalWidthDevice]-80, lblItemHeight.height);
                            
                        }
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:20000];
                        lblItem.text =title;
                        lblItem.font=[UIFont systemFontOfSize:15];
                        [view addSubview:lblItem];
                        // lblItem.backgroundColor=[UIColor grayColor];
                        btnFrame.origin.y+=lblItem.frame.size.height+5;
                        
                        int tempheight=lblItem.frame.size.height;
                        
                        viewHeight =viewHeight+tempheight;
                        
                    }
                    
                    for (int l=0; l<btnCount; l++){
                        
                        [arrOfControlssMain addObject:arrOfTempRadioBtns];
                        
                    }
                    
                    viewHeight=viewHeight+lbl.frame.size.height+lbl.frame.origin.y+5*btnCount;
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight)];
                    
                    [ViewFormSections addSubview:view];
                    
                    
                }
                /*
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 #pragma mark - Radio Button
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 //---------------------------------------------------------------------------------------
                 
                 
                 
                 else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"]){
                 
                 //Add View At which radio buttons and Tiltle Label will genarate
                 if (!yPositionOfControls) {
                 yPositionOfControls=40;
                 }
                 
                 NSString *strValueOfRadio=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                 
                 CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);;
                 UIView *view = [[UIView alloc] init];
                 [view setFrame:buttonFrame];
                 CALayer *bottomBorder = [CALayer layer];
                 [view.layer addSublayer:bottomBorder];
                 view.backgroundColor=[UIColor whiteColor];
                 CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(308, MAXFLOAT)];
                 UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 255, s.height)];
                 lbl.text = [dictControls valueForKey:@"Label"];
                 [lbl setAdjustsFontSizeToFitWidth:YES];
                 [lbl setFont:[UIFont systemFontOfSize:15]];
                 [lbl setNumberOfLines:9];
                 [view addSubview:lbl];
                 
                 CGRect btnFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,30,30);
                 
                 NSArray *arrOptions=[dictControls valueForKey:@"Options"];
                 int indexToSetValue=-1;
                 NSMutableArray *arrOfValues=[[NSMutableArray alloc]init];
                 for (int k=0; k<arrOptions.count; k++) {
                 
                 NSDictionary *dictData=arrOptions[k];
                 
                 [arrOfValues addObject:[dictData valueForKey:@"Value"]];
                 if ([strValueOfRadio isEqualToString:[dictData valueForKey:@"Value"]]) {
                 indexToSetValue=k;
                 }
                 
                 }
                 
                 int btnCount=[arrOfValues count];
                 
                 float viewHeight = btnCount*42+lbl.frame.size.height+20;
                 for (int l=0; l<btnCount; l++)
                 {
                 UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(15, 10, 255, 40)];
                 btn.frame = btnFrame;
                 NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d/%d",i,j,k,l];
                 [arrOfIndexes addObject:strTaggg];
                 btn.tag=tagToSetForIndexes;
                 tagToSetForIndexes++;
                 
                 if (l==0) {
                 
                 [btn setTitle:@"firstRadioBtnn" forState:UIControlStateNormal];
                 btn.titleLabel.textColor = [UIColor clearColor];
                 
                 }
                 
                 [btn addTarget:self action:@selector(checkRadioButtonCombo:) forControlEvents:UIControlEventTouchDown];
                 [view addSubview:btn];
                 
                 UILabel *lblYes = [[UILabel alloc] init];
                 lblYes.frame = CGRectMake(btn.frame.origin.x+50, btn.frame.origin.y+5, 200, 100);
                 lblYes.text = [arrOfValues objectAtIndex:l];
                 NSString *str=lblYes.text;
                 [lblYes setNumberOfLines:2];
                 lblYes.center=CGPointMake(lblYes.center.x, btn.center.y);
                 
                 [btn setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                 if (indexToSetValue==-1) {
                 
                 }else if (indexToSetValue==k){
                 
                 [btn setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                 }
                 
                 [arrOfControlssMain addObject:btn];
                 
                 [lblYes setAdjustsFontSizeToFitWidth:YES];
                 [lblYes setTag:btn.tag+200];
                 [view addSubview:lblYes];
                 [lblYes setFont:[UIFont systemFontOfSize:15]];
                 btnFrame.origin.y+=btnFrame.size.height+12; //it changes Y exix of radio button
                 }
                 
                 [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, viewHeight+20)];
                 
                 heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                 yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                 
                 [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                 
                 [ViewFormSections addSubview:view];
                 
                 }
                 */
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - TextArea i.e TextViewww
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textarea"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],100);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextView *txtView= [[UITextView alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.delegate=self;
                    [view addSubview:txtView];
                    
                    if (isCompletedStatusMechanical) {
                        [txtView setEditable:NO];
                    }else{
                        [txtView setEditable:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Paragraph i.e paragraph
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-paragraph"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],100);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextView *txtView= [[UITextView alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.editable=NO;
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.delegate=self;
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //el-textbox
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - TextBox i.e TextField
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textbox"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],50);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    txtView.font=[UIFont systemFontOfSize:18];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (isCompletedStatusMechanical) {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - EmailId Field
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-email"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],50);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.keyboardType=UIKeyboardTypeEmailAddress;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (isCompletedStatusMechanical) {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Phone Number Field
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-phone"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],50);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.keyboardType=UIKeyboardTypePhonePad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (isCompletedStatusMechanical) {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //el-currency
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Currency
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-currency"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],50);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.keyboardType=UIKeyboardTypePhonePad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (isCompletedStatusMechanical) {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Decimal
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-decimal"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],50);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.keyboardType=UIKeyboardTypeDecimalPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (isCompletedStatusMechanical) {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Number
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-number"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],50);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.keyboardType=UIKeyboardTypeNumberPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (isCompletedStatusMechanical) {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //el-auto-num  el-precent el-url el-upload
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-auto-num
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-auto-num"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],50);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.keyboardType=UIKeyboardTypeNumberPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    if (isCompletedStatusMechanical) {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-precent
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-precent"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],50);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.keyboardType=UIKeyboardTypePhonePad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    if (isCompletedStatusMechanical) {
                        [txtView setEnabled:NO];
                    }else{
                        [txtView setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-url
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-url"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    //                        CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-30,35);
                    //                        float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    //                        UITextField *txtView= [[UITextField alloc] init];
                    //                        txtView.text = [dictControls valueForKey:@"Value"];
                    //                        txtView.frame =txtViewFrame;
                    //                        txtView.keyboardType=UIKeyboardTypeWebSearch;
                    //
                    //                        NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    //
                    //                        [arrOfIndexes addObject:strTaggg];
                    //
                    //                        txtView.tag=tagToSetForIndexes;
                    //                        tagToSetForIndexes++;
                    //
                    //                        //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    //                        // txtView.layer.borderWidth = 0.5f;
                    //                        txtView.layer.borderWidth = 1.5f;
                    //                        txtView.layer.cornerRadius = 4.0f;
                    //                        txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    //                        [txtView setBackgroundColor:[UIColor clearColor]];
                    //                        txtView.delegate=self;
                    //
                    //                        UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    //                        leftView.backgroundColor = [UIColor clearColor];
                    //
                    //
                    //                        txtView.leftView = leftView;
                    //
                    //                        txtView.leftViewMode = UITextFieldViewModeAlways;
                    //                        txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    //
                    //                        [view addSubview:txtView];
                    //
                    //                        [arrOfControlssMain addObject:txtView];
                    
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 50)];
                    
                    float viewHeight = lbl.frame.size.height+btnDropDown.frame.size.height+30;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(oPenURLonWeb:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    
                    btnDropDown.backgroundColor=[UIColor clearColor];
                    [btnDropDown setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                    
                    [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    //[dictControls valueForKey:@"Value"]
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:18];
                    
                    [view addSubview:btnDropDown];
                    
                    UILabel *lblUnderLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 33, btnDropDown.frame.size.width, 1)];
                    
                    lblUnderLine.backgroundColor=[UIColor blueColor];
                    
                    //  [btnDropDown addSubview:lblUnderLine];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - el-upload
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-upload"]){
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        
                        yPositionOfControls=40;
                        
                    }
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    lbl.font=[UIFont systemFontOfSize:18];
                    [view addSubview:lbl];
                    
                    CGRect txtViewFrame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10,[global globalWidthDevice],50);
                    float viewHeight = lbl.frame.size.height+txtViewFrame.size.height+30;
                    UITextField *txtView= [[UITextField alloc] init];
                    txtView.text = [dictControls valueForKey:@"Value"];
                    txtView.frame =txtViewFrame;
                    txtView.font=[UIFont systemFontOfSize:18];
                    txtView.keyboardType=UIKeyboardTypeNumberPad;
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    txtView.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    //txtView.tag = [[dictMain valueForKey:@"id"] intValue];
                    // txtView.layer.borderWidth = 0.5f;
                    txtView.layer.borderWidth = 1.5f;
                    txtView.layer.cornerRadius = 4.0f;
                    txtView.layer.borderColor = [[UIColor grayColor] CGColor];
                    [txtView setBackgroundColor:[UIColor clearColor]];
                    txtView.delegate=self;
                    
                    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
                    leftView.backgroundColor = [UIColor clearColor];
                    
                    
                    txtView.leftView = leftView;
                    
                    txtView.leftViewMode = UITextFieldViewModeAlways;
                    txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    
                    [view addSubview:txtView];
                    
                    [arrOfControlssMain addObject:txtView];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, viewHeight+20)];
                    //   [arrayViews addObject:view];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Date Picker
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-date"]){
                    
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 50)];
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    if (isCompletedStatusMechanical) {
                        [btnDropDown setEnabled:NO];
                    }else{
                        [btnDropDown setEnabled:YES];
                    }
                    
                    [btnDropDown addTarget:self action:@selector(oPenDatePicker:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:18];
                    btnDropDown.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    
                    btnDropDown.backgroundColor=[UIColor clearColor];
                    [btnDropDown setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Date Time Picker
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-date-time"]){
                    
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 50)];
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(oPenDateTimePicker:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:18];
                    btnDropDown.backgroundColor=[UIColor clearColor];
                    [btnDropDown setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    if (isCompletedStatusMechanical) {
                        [btnDropDown setEnabled:NO];
                    }else{
                        [btnDropDown setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - DropDown
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]){
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    lbl.text = [dictControls valueForKey:@"Label"];
                    [lbl setFont:[UIFont systemFontOfSize:18]];
                    [lbl setNumberOfLines:20000];
                    [view addSubview:lbl];
                    
                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, lbl.frame.origin.y+lbl.frame.size.height+10, [global globalWidthDevice], 50)];
                    
                    UIButton *ImgDropDown = [[UIButton alloc] initWithFrame:CGRectMake(btnDropDown.frame.size.width-40,0, 40, 40)];
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:18];
                    ImgDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    if (isCompletedStatusMechanical) {
                        [btnDropDown setEnabled:NO];
                        [ImgDropDown setEnabled:NO];
                    }else{
                        [btnDropDown setEnabled:YES];
                        [ImgDropDown setEnabled:YES];
                    }
                    
                    [btnDropDown addTarget:self action:@selector(oPenDropDown:) forControlEvents:UIControlEventTouchDown];
                    [ImgDropDown addTarget:self action:@selector(oPenDropDown:) forControlEvents:UIControlEventTouchDown];
                    
                    btnDropDown.layer.borderWidth = 1.5f;
                    btnDropDown.layer.cornerRadius = 4.0f;
                    btnDropDown.layer.borderColor = [[UIColor clearColor] CGColor];
                    
                    btnDropDown.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
                    //drop_down1.png
                    [btnDropDown setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                    //15 March
                    
                    NSArray *arrTemp=[dictControls valueForKey:@"Options"];
                    NSString *strTempValueToCheck=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    NSString *strNameToSet;
                    
                    for (int q=0; q<arrTemp.count; q++) {
                        
                        NSDictionary *dictDataTemp=arrTemp[q];
                        
                        NSString *strTemp=[NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"Value"]];
                        
                        if ([strTemp isEqualToString:strTempValueToCheck]) {
                            
                            strNameToSet=[NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"Name"]];;
                            
                        }
                        
                    }
                    [btnDropDown setTitle:strNameToSet forState:UIControlStateNormal];
                    
                    //End 15 March

                   // [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [ImgDropDown setImage:[UIImage imageNamed:@"drop_down1.png"] forState:UIControlStateNormal];
                    [btnDropDown addSubview:ImgDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 40+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
                //el-multi-select
                
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
#pragma mark - Multiple Select
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------
                
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"]){
                    
                    
                    
                    UIView *view = [[UIView alloc] init];
                    if (!yPositionOfControls) {
                        yPositionOfControls=40;
                    }
                    
                    [view setFrame:CGRectMake(0, yPositionOfControls, [UIScreen mainScreen].bounds.size.width, 150)];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    //Added Label For Heading
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:20] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [global globalWidthDevice], s.height)];
                    
                    lbl.text = [dictControls valueForKey:@"Label"];
                    
                    [lbl setFont:[UIFont systemFontOfSize:20]];
                    
                    [lbl setNumberOfLines:20000];
                    
                    lbl.font=[UIFont systemFontOfSize:20];
                    
                    [view addSubview:lbl];
                    
                    //End

                    
                    UIButton *btnDropDown = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(lbl.frame)+5, [global globalWidthDevice], 50)];
                    
                    
                    NSString *strTaggg=[NSString stringWithFormat:@"%d/%d/%d",i,j,k];
                    
                    [arrOfIndexes addObject:strTaggg];
                    
                    btnDropDown.tag=tagToSetForIndexes;
                    tagToSetForIndexes++;
                    
                    [btnDropDown addTarget:self action:@selector(multipleSelect:) forControlEvents:UIControlEventTouchDown];
                    btnDropDown.titleLabel.font=[UIFont systemFontOfSize:18];
                    btnDropDown.backgroundColor=[UIColor lightGrayColor];
                    
                    // [btnDropDown setTitle:[dictControls valueForKey:@"Value"] forState:UIControlStateNormal];
                    
                    NSArray *arrOfOptions=[dictControls valueForKey:@"Options"];
                    NSArray *arrOfValues=[[dictControls valueForKey:@"Value"] componentsSeparatedByString:@","];
                    NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOfValues.count; k++) {
                        
                        for (int l=0; l<arrOfOptions.count; l++) {
                            
                            NSDictionary *dictData=arrOfOptions[l];
                            NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                            
                            if ([strValue isEqualToString:arrOfValues[k]]) {
                                
                                [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                                
                            }
                            
                        }
                        
                    }
                    NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@","];
                    
                    [btnDropDown setTitle:strTextView forState:UIControlStateNormal];
                    
                    [view addSubview:btnDropDown];
                    
                    if (isCompletedStatusMechanical) {
                        [btnDropDown setEnabled:NO];
                    }else{
                        [btnDropDown setEnabled:YES];
                    }
                    
                    [arrOfControlssMain addObject:btnDropDown];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 50+lbl.frame.size.height+20)];
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, 50)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
                
                
            }
            
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
#pragma mark - Final Size scroll and view and all
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------
            
            [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, heightVIewFormSections+arrControls.count*10+50)];
            
            heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrControls.count*10+50;
            
            MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width, heightMainViewFormSections+60);
            
            yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+10;
            
        }
        
        scrollViewHeight=scrollViewHeight+MainViewForm.frame.size.height;
        [arrayViews addObject:MainViewForm];
        
        CGFloat heightTemp=MainViewForm.frame.size.height;
        
        NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
        
        CGFloat YAxisTemp=MainViewForm.frame.origin.y;
        
        NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
        
        
        [arrOfHeightsViews addObject:strTempHeight];
        [arrOfYAxisViews addObject:strTempYAxis];
        
    }
    
    _scrollViewDynamicForm.backgroundColor=[UIColor clearColor];
    
    _scrollViewDynamicForm.frame=CGRectMake(_scrollViewDynamicForm.frame.origin.x, _scrollViewDynamicForm.frame.origin.y, _scrollViewDynamicForm.frame.size.width, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+100);
    
    [_scrollViewDynamicForm setContentSize:CGSizeMake( _scrollViewDynamicForm.frame.size.width,MainViewForm.frame.origin.y+MainViewForm.frame.size.height+100)];
    
    //For Save And Continue
    
    CGRect frameForSavenContinueView=CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+20, self.view.frame.size.width, _viewSavenCancel.frame.size.height);
    frameForSavenContinueView.origin.x=0;
    frameForSavenContinueView.origin.y=MainViewForm.frame.origin.y+MainViewForm.frame.size.height;
    [_viewSavenCancel setFrame:frameForSavenContinueView];
    [_scrollViewDynamicForm addSubview:_viewSavenCancel];
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Radio Button Methods--------------
//============================================================================
//============================================================================
-(void)checkRadioButton:(id)sender{
    
    UIButton *btn=(UIButton*)sender;
    
    int taggg=(int)btn.tag;
    
    UIButton *btnRadiButton=arrOfControlssMain[taggg];
    
    [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    if ([arrOfControlssMain[taggg-1] isKindOfClass:[UIButton class]]) {
        
        UIButton *btnlast=arrOfControlssMain[taggg-1];
        
        if ([btnlast.currentTitle isEqualToString:@"firstRadioBtnn"]) {
            
            [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            
        }else{
            
            UIButton *btnlast=arrOfControlssMain[taggg+1];
            
            [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            
        }
        
    }else{
        
        UIButton *btnlast=arrOfControlssMain[taggg+1];
        
        [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
    //firstRadioBtnn
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexLLL =[arrOfTag[3] intValue];
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
    
    NSString *strValue=[dictOfValues valueForKey:@"Value"];
    
    [self commonMethodToSaveValue:strValue :taggg];
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Radio Button Combooo Methods--------------
//============================================================================
//============================================================================
-(void)checkRadioButtonCombo:(id)sender{
    
    BOOL isValueToSet;
    isValueToSet=NO;
    
    //RadioBtnnComboooo
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=(int)btn.tag;
    
    
    int taggToCheckTemp=0;
    
    NSArray *arrOfRadioBtnsTemp = arrOfControlssMain[taggg];
    
    for (int k=0; k<arrOfRadioBtnsTemp.count; k++) {
        
        UIButton *btnRadiButton=arrOfRadioBtnsTemp[k];
        if (btnRadiButton.tag==taggg) {
            taggToCheckTemp=k;
            
            if ([btnRadiButton.currentImage isEqual:[UIImage imageNamed:@"RadioButton-Selected.png"]]) {
                
                [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                isValueToSet=NO;
            } else {
                
                [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                isValueToSet=YES;
            }
            
            //  [btnRadiButton setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            
        }
    }
    
    int taggToCheck=taggToCheckTemp-1;
    
    for (int s=taggToCheck;s>=0; s--) {
        
        if ([arrOfRadioBtnsTemp[s] isKindOfClass:[UIButton class]]) {
            
            UIButton *btnlast=arrOfRadioBtnsTemp[s];
            
            if ([btnlast.currentTitle isEqualToString:@"RadioBtnnComboooo"]) {
                
                [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                
            }
        }
        else
            break;
    }
    
    taggToCheck=taggToCheckTemp+1;
    
    for (int s=taggToCheck;s<arrOfRadioBtnsTemp.count; s++) {
        
        if ([arrOfRadioBtnsTemp[s] isKindOfClass:[UIButton class]]) {
            
            UIButton *btnlast=arrOfRadioBtnsTemp[s];
            
            if ([btnlast.currentTitle isEqualToString:@"RadioBtnnComboooo"]) {
                
                [btnlast setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                
            }
        }
        else
            break;
    }
    
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexLLL =[arrOfTag[3] intValue];
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
    
    NSString *strValue=[dictOfValues valueForKey:@"Value"];
    
    if (!isValueToSet) {
        strValue=@"";
    }
    
    [self commonMethodToSaveValue:strValue :taggg];
    
    
    //firstRadioBtnn
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Check Box Button Methods--------------
//============================================================================
//============================================================================
-(void)checkCheckBoxes:(id)sender{
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=(int)btn.tag;
    
    UIButton *btnCheckBox=arrOfControlssMain[taggg];
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"uncheck.png"];
    
    UIImage *img = [btnCheckBox imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        [btnCheckBox setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
    }
    else{
        
        [btnCheckBox setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexLLL =[arrOfTag[3] intValue];
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    NSString *strAlreadySavedValue=[dictOfOptions valueForKey:@"Value"];
    
    NSDictionary *dictOfValues=arrDataOptionss[indexLLL];
    
    NSString *strValue=[dictOfValues valueForKey:@"Value"];
    
    NSString *strFinalValue;
    
    if (strAlreadySavedValue.length==0) {
        
        strFinalValue=strValue;
        
    }else{
        
        NSArray *arrOfValue=[strAlreadySavedValue componentsSeparatedByString:@","];
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        [arrTemp addObjectsFromArray:arrOfValue];
        
        
        if ([arrTemp containsObject:strValue]) {
            
            [arrTemp removeObject:strValue];
            
        }
        else{
            
            [arrTemp addObject:strValue];
            
        }
        
        arrOfValue = arrTemp;
        
        strFinalValue=[arrOfValue componentsJoinedByString:@","];
        
    }
    
    [self commonMethodToSaveValue:strFinalValue :taggg];
    
}
//============================================================================
//============================================================================
#pragma mark- ------------URL OPEN ON WEB Methods--------------
//============================================================================
//============================================================================
-(void)oPenURLonWeb:(id)sender{
    
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggggs];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    // [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    // [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSString *strURL=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
    
    if (strURL.length>0) {
        
        [self openLinkNew:strURL];
        
    }else{
        [global AlertMethod:@"Info" :@"No Link Available"];
        // [self openLinkNew:@"http://www.citizencop.org/"];
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Open In Safari Methods--------------
//============================================================================
//============================================================================

- (void)openLinkNew:(NSString *)url {
    NSURL *URL = [NSURL URLWithString:url];
    
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = (id)self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:URL]) {
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Date Picker Methods--------------
//============================================================================
//============================================================================

-(void)oPenDatePicker:(id)sender{
    
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggggs];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    // [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    // [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    strGlobalDateToShow=[dictOfOptions valueForKey:@"Value"];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    [self addPickerViewDateToDynamicForm : taggggs];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addPickerViewDateToDynamicForm:(int)intTagg
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //[pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strGlobalDateToShow.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strGlobalDateToShow];
        
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    btnDone.tag=intTagg;
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDoneNew:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)setDateOnDoneNew:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    if (yesSameDay) {
        
        UIButton *btnDropdown=arrOfControlssMain[taggggs];
        
        [btnDropdown setTitle:strDate forState:UIControlStateNormal];
        strGlobalDateToShow=strDate;
        [self commonMethodToSaveValue:strDate :taggggs];
        
        [viewForDate removeFromSuperview];
        [viewBackGround removeFromSuperview];
        
    }
    else{
        
        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
        
        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
            
            [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
            
        }else{
            
            UIButton *btnDropdown=arrOfControlssMain[taggggs];
            
            [btnDropdown setTitle:strDate forState:UIControlStateNormal];
            strGlobalDateToShow=strDate;
            [self commonMethodToSaveValue:strDate :taggggs];
            
            [viewForDate removeFromSuperview];
            [viewBackGround removeFromSuperview];
            
        }
        
    }
    
}
//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

//============================================================================
//============================================================================
#pragma mark- ------------Time Picker Methods--------------
//============================================================================
//============================================================================

-(void)oPenDateTimePicker:(id)sender{
    
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggggs];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    arrSections=[dictData valueForKey:@"Sections"];
    // [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    arrOfControls=[dictDataControls valueForKey:@"Controls"];
    // [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    strGlobalDatenTime=[dictOfOptions valueForKey:@"Value"];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    [self addDAteTimePickerView : taggggs];
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Time PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addDAteTimePickerView:(int)intTagg
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //[pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strGlobalDatenTime.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:MM:SS"];
        NSDate *dateToSett = [dateFormat dateFromString:strGlobalDatenTime];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDateAndTime;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    btnDone.tag=intTagg;
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateTimeOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)setDateTimeOnDone:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:MM:SS"];
    NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    if (yesSameDay) {
        
        UIButton *btnDropdown=arrOfControlssMain[taggggs];
        
        [btnDropdown setTitle:strDate forState:UIControlStateNormal];
        strGlobalDatenTime=strDate;
        [self commonMethodToSaveValue:strDate :taggggs];
        
        [viewForDate removeFromSuperview];
        [viewBackGround removeFromSuperview];
        
    }
    else{
        
        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
        
        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
            
            [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
            
        }else{
            
            UIButton *btnDropdown=arrOfControlssMain[taggggs];
            
            [btnDropdown setTitle:strDate forState:UIControlStateNormal];
            strGlobalDatenTime=strDate;
            [self commonMethodToSaveValue:strDate :taggggs];
            
            [viewForDate removeFromSuperview];
            [viewBackGround removeFromSuperview];
            
        }
        
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Drop Down Methods--------------
//============================================================================
//============================================================================

-(void)oPenDropDown:(id)sender{
    
    isMultipleSelect=NO;
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSArray *arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSArray *arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    [arrDataTblView addObjectsFromArray:arrDataOptionss];
    
    strGlobalDropDownSelectedValue=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        [self tableLoad:taggg];
        tblData.tag=taggg;
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Drop Down Methods--------------
//============================================================================
//============================================================================

-(void)multipleSelect:(id)sender{
    
    arraySelecteValues=nil;
    arraySelecteValuesToShow=nil;
    arraySelecteValues= [[NSMutableArray alloc] init];
    arraySelecteValuesToShow= [[NSMutableArray alloc] init];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    UIButton *btn=(UIButton*)sender;
    
    
    int taggg=(int)btn.tag;
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSArray *arrSections=[dictData valueForKey:@"Sections"];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSArray *arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    [arrDataTblView addObjectsFromArray:arrDataOptionss];
    
    
    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictOfOptions valueForKey:@"Value"]];
    
    if (!(strIndexOfValue.length==0)) {
        
        NSArray *arrayvalus=[strIndexOfValue componentsSeparatedByString:@","];
        
        for (int i=0; i<[arrayvalus count]; i++)
        {
            [arraySelecteValues addObject:arrayvalus[i]];
            // [arraySelecteValuesToShow addObject:arrayvalus[i]];
        }
        
    }
    
    NSArray *arrOfOptions=[dictOfOptions valueForKey:@"Options"];
    NSArray *arrOfValues=[[dictOfOptions valueForKey:@"Value"] componentsSeparatedByString:@","];
    NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfValues.count; k++) {
        
        for (int l=0; l<arrOfOptions.count; l++) {
            
            NSDictionary *dictData=arrOfOptions[l];
            NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
            
            if ([strValue isEqualToString:arrOfValues[k]]) {
                
                [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                
            }
            
        }
        
    }
    // NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@","];
    
    [arraySelecteValuesToShow addObjectsFromArray:arrOfValuesToSet];
    
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        [self tableLoad:taggg];
        tblData.tag=taggg;
        isMultipleSelect=YES;
    }
    
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Button Done Action Methods--------------
//============================================================================
//============================================================================

-(void)btnDoneAction:(id)sender{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    if (isMultipleSelect) {
        
        isMultipleSelect=NO;
        
        if (!(indexMultipleSelectTosetValue==-100000)) {
            
            UIButton *btnDropdown=arrOfControlssMain[indexMultipleSelectTosetValue];
            
            NSString *joinedComponents = [arraySelecteValues componentsJoinedByString:@","];
            NSString *joinedComponentsToShow = [arraySelecteValuesToShow componentsJoinedByString:@","];
            
            if (!(joinedComponents.length==0)) {
                
                [btnDropdown setTitle:joinedComponentsToShow forState:UIControlStateNormal];
                
                [self commonMethodToSaveValue:joinedComponents :indexMultipleSelectTosetValue];
                
            } else {
                
                [btnDropdown setTitle:@"" forState:UIControlStateNormal];
                [self commonMethodToSaveValue:joinedComponents :indexMultipleSelectTosetValue];
                
            }
            
        }
    }
}
//============================================================================
//============================================================================
#pragma mark- ------------Button Cancel Action Methods--------------
//============================================================================
//============================================================================

-(void)btnCancelAction:(id)sender{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Open Close MainView METHODS-----------------
//============================================================================
//============================================================================
-(void)actionOpenCloseMAinView:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    int tagggg=(int)btn.tag;
    
    UIButton *btnTempp=[arrayOfButtonsMain objectAtIndex:tagggg];
    
    [arrayOfButtonsMain replaceObjectAtIndex:tagggg withObject:btnTempp];
    
    UIView *viewTempp=[arrayViews objectAtIndex:tagggg];
    
    if (viewTempp.frame.size.height==0) {
        
        UIImageView *imgView=arrOfButtonImages[tagggg];
        [imgView setImage:[UIImage imageNamed:@"minus_icon.png"]];
        
        NSString *strTempHeightLast=arrOfHeightsViews[tagggg];
        CGFloat hiTempLast=[strTempHeightLast floatValue];
        
        [viewTempp setFrame:CGRectMake(btnTempp.frame.origin.x, btnTempp.frame.origin.y+btnTempp.frame.size.height+5, btnTempp.frame.size.width, hiTempLast)];
        
        [_scrollViewDynamicForm setContentSize:CGSizeMake( _scrollViewDynamicForm.frame.size.width,_scrollViewDynamicForm.contentSize.height+viewTempp.frame.size.height)];
        
        [viewTempp setHidden:NO];
    } else {
        
        UIImageView *imgView=arrOfButtonImages[tagggg];
        [imgView setImage:[UIImage imageNamed:@"add_white.png"]];
        
        [_scrollViewDynamicForm setContentSize:CGSizeMake( _scrollViewDynamicForm.frame.size.width,_scrollViewDynamicForm.contentSize.height-viewTempp.frame.size.height)];
        
        [viewTempp setFrame:CGRectMake(0, viewTempp.frame.origin.y, 0, 0)];
        [viewTempp setHidden:YES];
    }
    [arrayViews replaceObjectAtIndex:tagggg withObject:viewTempp];
    
    for (int k=tagggg+1; k<arrayViews.count; k++) {
        
        
        UIButton *btnTemppNew=[arrayOfButtonsMain objectAtIndex:k];
        UIView *viewTemppNew=[arrayViews objectAtIndex:k];
        UIView *viewTemppNewLasttt;
        CGFloat hiTempLast;
        if (k==0) {
            viewTemppNewLasttt.frame=CGRectMake(0, 5, 0, 0);
        } else {
            viewTemppNewLasttt=[arrayViews objectAtIndex:k-1];
            NSString *strTempHeightLast=arrOfHeightsViews[k-1];
            hiTempLast =[strTempHeightLast floatValue];
            
        }
        
        [btnTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, viewTemppNewLasttt.frame.origin.y+viewTemppNewLasttt.frame.size.height+5, btnTemppNew.frame.size.width, btnTemppNew.frame.size.height)];
        [btnTemppNew addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        [arrayOfButtonsMain replaceObjectAtIndex:k withObject:btnTemppNew];
        
        [viewTemppNew setFrame:CGRectMake(btnTemppNew.frame.origin.x, btnTemppNew.frame.origin.y+btnTemppNew.frame.size.height+5, viewTemppNew.frame.size.width, viewTemppNew.frame.size.height)];
        if (viewTemppNew.frame.size.height==0) {
            
            UIImageView *imgView=arrOfButtonImages[k];
            [imgView setImage:[UIImage imageNamed:@"add_white.png"]];
            
            [viewTemppNew setHidden:YES];
            
            //   [_scrollViewDynamicForm setContentSize:CGSizeMake( _scrollViewDynamicForm.frame.size.width,_scrollViewDynamicForm.contentSize.height-viewTemppNew.frame.size.height)];
            
        }else{
            UIImageView *imgView=arrOfButtonImages[k];//checked.png
            [imgView setImage:[UIImage imageNamed:@"minus_icon.png"]];
            
            [viewTemppNew setHidden:NO];
            
            //   [_scrollViewDynamicForm setContentSize:CGSizeMake( _scrollViewDynamicForm.frame.size.width,_scrollViewDynamicForm.contentSize.height+viewTemppNew.frame.size.height)];
            
        }
        [arrayViews replaceObjectAtIndex:k withObject:viewTemppNew];
        
    }
    
    
    //For Save And Continue
    
    CGRect frameForSavenContinueView=CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+20, self.view.frame.size.width, _viewSavenCancel.frame.size.height);
    frameForSavenContinueView.origin.x=0;
    frameForSavenContinueView.origin.y=MainViewForm.frame.origin.y+MainViewForm.frame.size.height;
    [_viewSavenCancel setFrame:frameForSavenContinueView];
    //[_scrollViewDynamicForm addSubview:_viewSavenCancel];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(void)textViewDidBeginEditing:(UITextView *)textField{
    
}
-(void)textViewDidEndEditing:(UITextView *)textField
{
    //1874919417
    if (textField.tag==1874919417) {
        
    } else {
        
        int taggg=(int)textField.tag;
        
        [self commonMethodToSaveValue:textField.text :taggg];
        
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT FIELD DELEGATE METHODS-----------------
//============================================================================
//============================================================================
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    if (textField==_txtFieldSearch) {
        
        
        if (isSerialNo) {
            
            arrOfSerialNosFiltered=[[NSMutableArray alloc]init];
            
            NSString *strTextFld=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            if ([string isEqualToString:@""]) {
                
                if ([strTextFld length] > 0) {
                    strTextFld = [strTextFld substringToIndex:[strTextFld length] - 1];
                } else {
                    //no characters to delete... attempting to do so will result in a crash
                }
                
            }
            for (int k=0; k<arrOfSerialNosGlobal.count; k++) {
                
                NSDictionary *dictDatas=arrOfSerialNosGlobal[k];
                
                NSString *strSerialNos=[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"SerialNumber"]];
                
                if ([strSerialNos containsString:strTextFld] && [strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"ItemSysName"]]]) {
                    
                    [arrOfSerialNosFiltered addObject:dictDatas];
                    
                }
                
            }
            
            if ([strTextFld isEqualToString:@""] || strTextFld.length==0) {
                
                for (int k=0; k<arrOfSerialNosGlobal.count; k++) {
                    
                    NSDictionary *dictDataa=arrOfSerialNosGlobal[k];
                    
                    if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]]) {
                        
                        [arrOfSerialNosFiltered addObject:dictDataa];
                        
                    }
                    
                }
                
            }
            
            tblData.tag=1874919424;
            _tblViewTxtSearchResults.tag=1874919424;
            [_tblViewTxtSearchResults reloadData];
            
        }
        else if(isModelNo){
            
            arrOfModelNosFiltered=[[NSMutableArray alloc]init];
            
            NSString *strTextFld=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            if ([string isEqualToString:@""]) {
                
                if ([strTextFld length] > 0) {
                    strTextFld = [strTextFld substringToIndex:[strTextFld length] - 1];
                } else {
                    //no characters to delete... attempting to do so will result in a crash
                }
                
            }
            for (int k=0; k<arrOfModelNosGlobal.count; k++) {
                
                NSDictionary *dictDatas=arrOfModelNosGlobal[k];
                
                NSString *strSerialNos=[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"ModelNumber"]];
                
                if ([strSerialNos containsString:strTextFld] && [strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"ItemSysName"]]]) {
                    
                    [arrOfModelNosFiltered addObject:dictDatas];
                    
                }
                
            }
            
            if ([strTextFld isEqualToString:@""] || strTextFld.length==0) {
                
                for (int k=0; k<arrOfModelNosGlobal.count; k++) {
                    
                    NSDictionary *dictDataa=arrOfModelNosGlobal[k];
                    
                    if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]]) {
                        
                        [arrOfModelNosFiltered addObject:dictDataa];
                        
                    }
                    
                }
                
                
            }
            
            tblData.tag=1874919425;
            _tblViewTxtSearchResults.tag=1874919425;
            [_tblViewTxtSearchResults reloadData];
            
        }
        else if(isManufacturer){
            
            arrOfManufacturerFiltered=[[NSMutableArray alloc]init];
            
            NSString *strTextFld=[NSString stringWithFormat:@"%@%@",textField.text,string];
            
            if ([string isEqualToString:@""]) {
                
                if ([strTextFld length] > 0) {
                    strTextFld = [strTextFld substringToIndex:[strTextFld length] - 1];
                } else {
                    //no characters to delete... attempting to do so will result in a crash
                }
                
            }
            for (int k=0; k<arrOfManufacturerGlobal.count; k++) {
                
                NSDictionary *dictDatas=arrOfManufacturerGlobal[k];
                
                NSString *strSerialNos=[NSString stringWithFormat:@"%@",[dictDatas valueForKey:@"Manufacturer"]];
                
                if ([strSerialNos containsString:strTextFld]) {
                    
                    [arrOfManufacturerFiltered addObject:dictDatas];
                    
                }
                
            }
            
            if ([strTextFld isEqualToString:@""] || strTextFld.length==0) {
                
                [arrOfManufacturerFiltered addObjectsFromArray:arrOfManufacturerGlobal];
                
            }
            
            tblData.tag=1874919426;
            _tblViewTxtSearchResults.tag=1874919426;
            [_tblViewTxtSearchResults reloadData];
            
        }
        return YES;
        
        
    }
    else {
        
        return YES;
        
        
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==_txt_SerialNo) {
        
        _lblSearchHead.text=@"Serial #";
        _txtFieldSearch.placeholder=@"Enter Serial #";
        _viewTextFieldSearch.frame=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
        [self.view addSubview:_viewTextFieldSearch];
        isSerialNo=YES;
        isModelNo=NO;
        isManufacturer=NO;
        
        
        
        if (isScannedResult) {
            
        } else {
            
            arrOfSerialNosFiltered=[[NSMutableArray alloc]init];
            
            //strItemSysName
            for (int k=0; k<arrOfSerialNosGlobal.count; k++) {
                
                NSDictionary *dictDataa=arrOfSerialNosGlobal[k];
                
                if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]]) {
                    
                    [arrOfSerialNosFiltered addObject:dictDataa];
                    
                }
                
            }
            
        }
        
        
        // [arrOfSerialNosFiltered addObjectsFromArray:arrOfSerialNosGlobal];
        
        arrDataTblView=[[NSMutableArray alloc]init];
        [arrDataTblView addObject:@"sdfasdf"];
        _txtFieldSearch.text=_txt_SerialNo.text;
        tblData.tag=1874919424;
        _tblViewTxtSearchResults.tag=1874919424;
        [_tblViewTxtSearchResults reloadData];
        [textField resignFirstResponder];
        
    }
    else if (textField==_txt_ModelNo) {
        
        _lblSearchHead.text=@"Model #";
        _txtFieldSearch.placeholder=@"Enter Model #";
        _viewTextFieldSearch.frame=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
        [self.view addSubview:_viewTextFieldSearch];
        isSerialNo=NO;
        isModelNo=YES;
        isManufacturer=NO;
        
        
        if (isScannedResult) {
            
        } else {
            
            arrOfModelNosFiltered=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfModelNosGlobal.count; k++) {
                
                NSDictionary *dictDataa=arrOfModelNosGlobal[k];
                
                if ([strItemSysName isEqualToString:[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemSysName"]]]) {
                    
                    [arrOfModelNosFiltered addObject:dictDataa];
                    
                }
                
            }
            
        }
        
        arrDataTblView=[[NSMutableArray alloc]init];
        [arrDataTblView addObject:@"sdfasdf"];
        _txtFieldSearch.text=_txt_ModelNo.text;
        tblData.tag=1874919425;
        _tblViewTxtSearchResults.tag=1874919425;
        [_tblViewTxtSearchResults reloadData];
        [textField resignFirstResponder];
        
    }
    else if (textField==_txt_Manufaturer) {
        
        _lblSearchHead.text=@"Manufacturer";
        _txtFieldSearch.placeholder=@"Enter Manufacturer";
        _viewTextFieldSearch.frame=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
        [self.view addSubview:_viewTextFieldSearch];
        isSerialNo=NO;
        isModelNo=NO;
        isManufacturer=YES;
        
        if (isScannedResult) {
            
        } else {
            
            arrOfManufacturerFiltered=[[NSMutableArray alloc]init];
            
            [arrOfManufacturerFiltered addObjectsFromArray:arrOfManufacturerGlobal];
            
        }
        
        arrDataTblView=[[NSMutableArray alloc]init];
        [arrDataTblView addObject:@"sdfasdf"];
        _txtFieldSearch.text=_txt_Manufaturer.text;
        tblData.tag=1874919426;
        _tblViewTxtSearchResults.tag=1874919426;
        [_tblViewTxtSearchResults reloadData];
        [textField resignFirstResponder];
        
    }
    
    else{
        
        UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
        leftView.backgroundColor = [UIColor clearColor];
        
        
        textField.leftView = leftView;
        
        textField.leftViewMode = UITextFieldViewModeAlways;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        [viewForDate removeFromSuperview];
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_txt_SerialNo) {
        
        
        
    }
    else if (textField==_txt_ModelNo) {
        
        
        
    }
    else if (textField==_txt_Manufaturer) {
        
        
        
    }
    else if (textField==_txtFieldSearch) {
        
        
        
    } else if (textField==_txtNameEquip) {
        
        
        
    }
    else {
        
        [textField resignFirstResponder];
        
        int taggg=(int)textField.tag;
        
        [self commonMethodToSaveValue:textField.text :taggg];
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark ---------------- Validation Check Method--------------------------
//============================================================================
//============================================================================

-(NSString *)validationCheck :(NSString*)stringTovalidate :(NSString*)typeOfelement
{
    NSString *errorMessage;
    if (stringTovalidate.length>0) {
        
        if ([typeOfelement isEqualToString:@"el-email"]) {
            
            NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
            NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            
            stringTovalidate =  [stringTovalidate stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if (![emailPredicate evaluateWithObject:stringTovalidate])
            {
                errorMessage = @"Please enter valid email id only";
            }
            if ([stringTovalidate containsString:@" "])
            {
                errorMessage = @"Please enter valid email id only";
            }
        }
        else if ([typeOfelement isEqualToString:@""]){
            
        }
        
    }
    return errorMessage;
}

//============================================================================
//============================================================================
#pragma mark ---------------- Common Method To Save value---------------------
//============================================================================
//============================================================================
-(void)saveLeadIdInMasterForms :(int)indexReplace{
    
    NSDictionary *dictData=arrResponseInspection[indexReplace];
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSArray *objValuearrResponseInspection=[NSArray arrayWithObjects:
                                            [dictData valueForKey:@"InspectionId"],
                                            strWorkOrderId,
                                            [dictData valueForKey:@"FormId"],
                                            [dictData valueForKey:@"FormName"],
                                            [dictData valueForKey:@"CshtmlFileName"],
                                            [dictData valueForKey:@"CshtmlFilePath"],
                                            [dictData valueForKey:@"HtmlConvertedFileName"],
                                            [dictData valueForKey:@"HtmlConvertedFilePath"],
                                            [dictData valueForKey:@"HtmlFileName"],
                                            [dictData valueForKey:@"HtmlFilePath"],
                                            isWoEquipIdPrsent ? strGlobalWoEquipIdToFetchDynamicData : @"",
                                            isWoEquipIdPrsent ? @"" : strGlobalWoEquipIdToFetchDynamicData,
                                            [dictData valueForKey:@"DepartmentName"],
                                            [dictData valueForKey:@"DepartmentSysName"],
                                            [dictData valueForKey:@"CategoryName"],
                                            [dictData valueForKey:@"CategorySysName"],
                                            arrSections,nil];
    
    NSArray *objKeyarrResponseInspection=[NSArray arrayWithObjects:
                                          @"InspectionId",
                                          @"WorkorderId",
                                          @"FormId",
                                          @"FormName",
                                          @"CshtmlFileName",
                                          @"CshtmlFilePath",
                                          @"HtmlConvertedFileName",
                                          @"HtmlConvertedFilePath",
                                          @"HtmlFileName",
                                          @"HtmlFilePath",
                                          @"WOEquipmentId",
                                          @"MobileUniqueId",
                                          @"DepartmentName",
                                          @"DepartmentSysName",
                                          @"CategoryName",
                                          @"CategorySysName",
                                          @"Sections",nil];
    
    NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
    
    [arrResponseInspection replaceObjectAtIndex:indexReplace withObject:dictOfarrResponseInspection];
    
}
-(void)commonMethodToSaveValue :(NSString*)strValueToSet :(int)tagggg{
    
    int taggg=tagggg;
    
    NSString *strTagg=arrOfIndexes[taggg];
    
    NSArray *arrOfTag=[strTagg componentsSeparatedByString:@"/"];
    
    int indexKKK =[arrOfTag[2] intValue];
    int indexJJJ =[arrOfTag[1] intValue];
    int indexIII =[arrOfTag[0] intValue];
    
    
    NSDictionary *dictData=arrResponseInspection[indexIII];
    
    NSMutableArray *arrSections =[[NSMutableArray alloc]init];
    //arrSections=[dictData valueForKey:@"Sections"];
    
    [arrSections addObjectsFromArray:[dictData valueForKey:@"Sections"]];
    
    NSDictionary *dictDataControls=arrSections[indexJJJ];
    
    NSMutableArray *arrOfControls=[[NSMutableArray alloc]init];
    // arrOfControls=[dictDataControls valueForKey:@"Controls"];
    
    [arrOfControls addObjectsFromArray:[dictDataControls valueForKey:@"Controls"]];
    
    NSDictionary *dictOfOptions=arrOfControls[indexKKK];
    
    
    
    //For Validations new On 18 sep sunday at home
    
    NSString *errorMessage;
    
    if ([[dictOfOptions valueForKey:@"Element"] isEqualToString:@"el-email"]) {
        
        strValueToSet =  [strValueToSet stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        errorMessage=[self validationCheck :strValueToSet :@"el-email"];
        
        
    }
    
    //    if (errorMessage) {
    //        NSString *strTitle = Alert;
    //        [global AlertMethod:strTitle :errorMessage];
    //        [DejalBezelActivityView removeView];
    //    } else {
    
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
        [DejalBezelActivityView removeView];
    }
    
    NSArray *arrDataOptionss=[dictOfOptions valueForKey:@"Options"];
    
    
#pragma mark- ---------------------Dictionary of Controls To Be Updated-----------------
    
    //Dictionary of Controls To Be Updated
    
    NSArray *arrOfOptionsToSend=arrDataOptionss;
    
    NSArray *objValueControls=[NSArray arrayWithObjects:
                               [dictOfOptions valueForKey:@"Position"],
                               [dictOfOptions valueForKey:@"Element"],
                               [dictOfOptions valueForKey:@"Label"],
                               strValueToSet,
                               [dictOfOptions valueForKey:@"Id"],
                               [dictOfOptions valueForKey:@"Name"],
                               [dictOfOptions valueForKey:@"PlaceHolder"],
                               [dictOfOptions valueForKey:@"HelpText"],
                               [dictOfOptions valueForKey:@"Type"],
                               [dictOfOptions valueForKey:@"MaxLength"],
                               [dictOfOptions valueForKey:@"MinLength"],
                               [dictOfOptions valueForKey:@"Required"],
                               [dictOfOptions valueForKey:@"ReadOnly"],
                               [dictOfOptions valueForKey:@"Default"],
                               [dictOfOptions valueForKey:@"Format"],
                               [dictOfOptions valueForKey:@"Rows"],
                               [dictOfOptions valueForKey:@"ClassName"],
                               [dictOfOptions valueForKey:@"IsPrimaryControl"],
                               arrOfOptionsToSend,nil];
    
    NSArray *objKeyControls=[NSArray arrayWithObjects:
                             @"Position",
                             @"Element",
                             @"Label",
                             @"Value",
                             @"Id",
                             @"Name",
                             @"PlaceHolder",
                             @"HelpText",
                             @"Type",
                             @"MaxLength",
                             @"MinLength",
                             @"Required",
                             @"ReadOnly",
                             @"Default",
                             @"Format",
                             @"Rows",
                             @"ClassName",
                             @"IsPrimaryControl",
                             @"Options",nil];
    
    NSDictionary *dictOfControlsToUpdate=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
    
    [arrOfControls replaceObjectAtIndex:indexKKK withObject:dictOfControlsToUpdate];
    
    
#pragma mark- ---------------------Dictinary Of Sections To Be Updated-----------------
    
    //Dictinary Of Sections To Be Updated
    
    NSArray *objValueSections=[NSArray arrayWithObjects:
                               [dictDataControls valueForKey:@"Name"],
                               [dictDataControls valueForKey:@"ColumnType"],
                               [dictDataControls valueForKey:@"IsPrimaryControl"],
                               arrOfControls,nil];
    NSArray *objKeySections=[NSArray arrayWithObjects:
                             @"Name",
                             @"ColumnType",
                             @"IsPrimaryControl",
                             @"Controls",nil];
    
    NSDictionary *dictOfSectionToUpdate=[[NSDictionary alloc] initWithObjects:objValueSections forKeys:objKeySections];
    
    [arrSections replaceObjectAtIndex:indexJJJ withObject:dictOfSectionToUpdate];
    
    
#pragma mark- ---------------------Dictionary of arrResponseInspection-----------------
    
    //Dictionary of arrResponseInspection  strWorkOrderId
    
    
    NSArray *objValuearrResponseInspection=[NSArray arrayWithObjects:
                                            [dictData valueForKey:@"InspectionId"],
                                            strWorkOrderId,
                                            [dictData valueForKey:@"FormId"],
                                            [dictData valueForKey:@"FormName"],
                                            [dictData valueForKey:@"CshtmlFileName"],
                                            [dictData valueForKey:@"CshtmlFilePath"],
                                            [dictData valueForKey:@"HtmlConvertedFileName"],
                                            [dictData valueForKey:@"HtmlConvertedFilePath"],
                                            [dictData valueForKey:@"HtmlFileName"],
                                            [dictData valueForKey:@"HtmlFilePath"],
                                            isWoEquipIdPrsent ? strGlobalWoEquipIdToFetchDynamicData : @"",
                                            isWoEquipIdPrsent ? @"" : strGlobalWoEquipIdToFetchDynamicData,
                                            [dictData valueForKey:@"DepartmentName"],
                                            [dictData valueForKey:@"DepartmentSysName"],
                                            [dictData valueForKey:@"CategoryName"],
                                            [dictData valueForKey:@"CategorySysName"],
                                            arrSections,nil];
    
    NSArray *objKeyarrResponseInspection=[NSArray arrayWithObjects:
                                          @"InspectionId",
                                          @"WorkorderId",
                                          @"FormId",
                                          @"FormName",
                                          @"CshtmlFileName",
                                          @"CshtmlFilePath",
                                          @"HtmlConvertedFileName",
                                          @"HtmlConvertedFilePath",
                                          @"HtmlFileName",
                                          @"HtmlFilePath",
                                          @"WOEquipmentId",
                                          @"MobileUniqueId",
                                          @"DepartmentName",
                                          @"DepartmentSysName",
                                          @"CategoryName",
                                          @"CategorySysName",
                                          @"Sections",nil];
    
    NSDictionary *dictOfarrResponseInspection=[[NSDictionary alloc] initWithObjects:objValuearrResponseInspection forKeys:objKeyarrResponseInspection];
    
    [arrResponseInspection replaceObjectAtIndex:indexIII withObject:dictOfarrResponseInspection];
    
    
#pragma mark- ---------------------Converting Finally To Json-----------------
    
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:arrResponseInspection])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:arrResponseInspection options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL FORM JSON: %@", jsonString);
        }
    }
    //  }
}

-(void)sendingFinalDynamicJsonToServer{
    
    NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                       arrResponseInspection,nil];
    
    NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                     @"InspectionFormsData",nil];
    
    NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlSalesInspectionDynamicFormSubmission];
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     if ([str containsString:@"Success"]) {
                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [alert show];
                     }else{
                         
                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [alert show];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
                 
                 [self goToNextScreen];
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}
-(void)goToNextScreen{
    
    
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------Core Data Methods-----------------
//============================================================================
//============================================================================

/*
 -(void)saveToCoreDataSalesDynamicFinal :(NSArray*)arrOfSalesDynamicToSave{
 
 //==================================================================================
 //==================================================================================
 [self deleteFromCoreDataSalesDynamic];
 //==================================================================================
 //==================================================================================
 
 
 appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
 context = [appDelegate managedObjectContext];
 entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
 
 SalesDynamicInspection *objSalesDynamic = [[SalesDynamicInspection alloc]initWithEntity:entitySalesDynamic insertIntoManagedObjectContext:context];
 objSalesDynamic.arrFinalInspection=arrOfSalesDynamicToSave;
 objSalesDynamic.leadId=strWorkOrderId;
 objSalesDynamic.companyKey=strCompanyKey;
 objSalesDynamic.userName=strUserName;
 objSalesDynamic.empId=strEmpID;
 objSalesDynamic.isSentToServer=@"No";
 NSError *error1;
 [context save:&error1];
 
 }
 */
- (IBAction)action_FinalSaveDynamicForm:(id)sender {
    
    [self saveToCoreDataSalesDynamic:arrResponseInspection];
    [_viewBackgroundDynamicForm removeFromSuperview];
    if (arrOfEquipmentList.count==1) {
        // [self addingEquipmentView];
    }
}

-(void)getSerialNumber{
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrOfSerialNosGlobal=[[NSMutableArray alloc]init];
    NSDictionary *dictOfInventoryMaster=[dictDetailsFortblView valueForKey:@"InventoryMasters"];
    
    NSArray *arrOfSerialNos=[dictOfInventoryMaster valueForKey:@"SerialNumbers"];
    
    if ([arrOfSerialNos isKindOfClass:[NSString class]]) {
        
        
        
    }else{
        
        for (int k=0; k<arrOfSerialNos.count; k++) {
            
            NSDictionary *dictDataa=arrOfSerialNos[k];
            NSString *strIsDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            
            if ([strIsDepartmentSysName isEqualToString:strDepartmentSysNameWo]) {
                
                [arrOfSerialNosGlobal addObject:dictDataa];
                
            }
        }
        
    }
    
    
    [self getModelNumber];
}

-(void)getModelNumber{
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrOfModelNosGlobal=[[NSMutableArray alloc]init];
    NSDictionary *dictOfInventoryMaster=[dictDetailsFortblView valueForKey:@"InventoryMasters"];
    
    NSArray *arrOfSerialNos=[dictOfInventoryMaster valueForKey:@"ModelNumbers"];
    
    if ([arrOfSerialNos isKindOfClass:[NSString class]]) {
        
        
        
    } else {
        
        for (int k=0; k<arrOfSerialNos.count; k++) {
            
            NSDictionary *dictDataa=arrOfSerialNos[k];
            NSString *strIsDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            
            if ([strIsDepartmentSysName isEqualToString:strDepartmentSysNameWo]) {
                
                [arrOfModelNosGlobal addObject:dictDataa];
                
            }
        }

    }
    
    
    [self getManufacturer];
}

-(void)getManufacturer{
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrOfManufacturerGlobal=[[NSMutableArray alloc]init];
    NSDictionary *dictOfInventoryMaster=[dictDetailsFortblView valueForKey:@"InventoryMasters"];
    
    NSArray *arrOfSerialNos=[dictOfInventoryMaster valueForKey:@"Manufacturers"];
    
    if ([arrOfSerialNos isKindOfClass:[NSString class]]) {
        
    } else {
        
        for (int k=0; k<arrOfSerialNos.count; k++) {
            
            NSDictionary *dictDataa=arrOfSerialNos[k];
            //        NSString *strIsDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            //
            //        if ([strIsDepartmentSysName isEqualToString:strDepartmentIdGlobal]) {
            //
            //            [arrOfManufacturerGlobal addObject:dictDataa];
            //
            //        }
            [arrOfManufacturerGlobal addObject:dictDataa];
        }

    }
    
    
}

- (IBAction)action_SaveSearchResult:(id)sender {
    
    if (isSerialNo) {
        
        _txt_SerialNo.text=_txtFieldSearch.text;
        
    }
    else if (isModelNo) {
        
        _txt_ModelNo.text=_txtFieldSearch.text;
        
    }
    else if (isManufacturer) {
        
        _txt_Manufaturer.text=_txtFieldSearch.text;
        
    }
    [_viewTextFieldSearch removeFromSuperview];
}

- (IBAction)action_CancelSearchResult:(id)sender {
    
    [_viewTextFieldSearch removeFromSuperview];
    
}

- (IBAction)action_AddEquipments:(id)sender {
    
    _btn_EquipmentsName.enabled=YES;
    _btn_Category.enabled=YES;

    isUpdateEquip=NO;
    [_btn_Category setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_Equipment setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_EquipmentsName setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_Area setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_AssociatedWith setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_InstalledOn setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_NextServiceDate setTitle:@"Select" forState:UIControlStateNormal];
    [_btn_WarrantyDate setTitle:@"Select" forState:UIControlStateNormal];
    _txtView_Notes.text=@"";
    strCategorySysName=@"";
    strEquipmentValueId=@"";
    strItemSysName=@"";
    strAreaSysName=@"";
    strAssociatedWith=@"";
    strAssociatedWithId=@"";
    strInstalledOn=@"";
    strItemNumber=@"";
    _txt_SerialNo.text=@"";
    _txt_Manufaturer.text=@"";
    _txt_ModelNo.text=@"";
    _txtNameEquip.text=@"";
    
    arrOfSerialNosFiltered=[[NSMutableArray alloc]init];
    arrOfModelNosFiltered=[[NSMutableArray alloc]init];
    arrOfManufacturerFiltered=[[NSMutableArray alloc]init];
    
    
    //Setting Current date ON Installed date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDateEquip = [dateFormat stringFromDate:[NSDate date]];
    [_btn_InstalledOn setTitle:strDateEquip forState:UIControlStateNormal];
    strInstalledOn=strDateEquip;
    
    _view_IntsallNewEquipments.frame=CGRectMake(0, 20,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
    [self.view addSubview:_view_IntsallNewEquipments];
    
}

- (IBAction)action_CancelEquipmetAddView:(id)sender {
    
    [_view_IntsallNewEquipments removeFromSuperview];
    
}



#pragma mark - Scanner Methods

- (MTBBarcodeScanner *)scanner {
    if (!_scanner) {
        _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_scannerPreviewView];
    }
    return _scanner;
}

#pragma mark - Scanning

- (void)startScanning {
    
   // self.uniqueCodes = [[NSMutableArray alloc] init];
    
    NSError *error = nil;
    [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
        for (AVMetadataMachineReadableCodeObject *code in codes) {
            
            //            UIAlertView *Alet=[[UIAlertView alloc]initWithTitle:@"Scanned Result" message:code.stringValue delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [Alet show];
            
            [self stopScanning];
            
            [_scannerPreviewView removeFromSuperview];
            
            [self setEquipmentValuesAfterScanning:code.stringValue];
            
        }
    } error:&error];
    
    if (error) {
        NSLog(@"An error occurred: %@", error.localizedDescription);
    }
    
    [self.toggleScanningButton setTitle:@"Stop Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = [UIColor redColor];
}

-(void)afterScannedResult :(NSString*)stringValue{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Item Details..."];
    
    dictDataInventory=[global getInventoryDetailsByItemNo:strServiceUrlMain :strCompanyIdCore :stringValue];
    
    [DejalBezelActivityView removeView];
    
    //            NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    //            NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"TestingJson.json"];
    //            NSError * error;
    //            NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    //            dictDataInventory = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
    [_btn_Category setTitle:[dictDataInventory valueForKey:@"CategoryName"] forState:UIControlStateNormal];
    strCategoryId=[NSString stringWithFormat:@"%@",[dictDataInventory valueForKey:@"CategoryMasterId"]];
    strCategorySysName=[NSString stringWithFormat:@"%@",[dictDataInventory valueForKey:@"CategorySysName"]];
    //Setting Current date ON Installed date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDateEquip = [dateFormat stringFromDate:[NSDate date]];
    [_btn_InstalledOn setTitle:strDateEquip forState:UIControlStateNormal];
    strInstalledOn=strDateEquip;
    [_btn_Equipment setTitle:[dictDataInventory valueForKey:@"ItemTypeText"] forState:UIControlStateNormal];
    strEquipmentValueId=[NSString stringWithFormat:@"%@",[dictDataInventory valueForKey:@"ItemTypeValue"]];
    [_btn_EquipmentsName setTitle:[dictDataInventory valueForKey:@"ItemName"] forState:UIControlStateNormal];
    strItemSysName=[NSString stringWithFormat:@"%@",[dictDataInventory valueForKey:@"ItemSysName"]];
    strItemNumber=[NSString stringWithFormat:@"%@",[dictDataInventory valueForKey:@"ItemNumber"]];
    
    arrOfSerialNosFiltered=[[NSMutableArray alloc]init];
    arrOfModelNosFiltered=[[NSMutableArray alloc]init];
    arrOfManufacturerFiltered=[[NSMutableArray alloc]init];
    
    NSArray *arrTempSerialNo=[dictDataInventory valueForKey:@"SerialNumbers"];
    NSArray *arrTempModelNo=[dictDataInventory valueForKey:@"ModelNumbers"];
    NSArray *arrTempManufacture=[dictDataInventory valueForKey:@"Manufacturers"];
    
    if ([arrTempSerialNo isKindOfClass:[NSArray class]]) {
        
        [arrOfSerialNosFiltered addObjectsFromArray:arrTempSerialNo];
        
    }
    if ([arrTempModelNo isKindOfClass:[NSArray class]]) {
        
        [arrOfModelNosFiltered addObjectsFromArray:arrTempModelNo];
        
    }
    if ([arrTempManufacture isKindOfClass:[NSArray class]]) {
        
        [arrOfManufacturerFiltered addObjectsFromArray:arrTempManufacture];
        
    }
    
    
    isScannedResult=YES;
    
}

- (void)stopScanning {
    [self.scanner stopScanning];
    
    [self.toggleScanningButton setTitle:@"Start Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = self.view.tintColor;
    
    self.captureIsFrozen = NO;
}

#pragma mark - Actions

- (IBAction)toggleScanningTapped:(id)sender {
    if ([self.scanner isScanning] || self.captureIsFrozen) {
        [self stopScanning];
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
    } else {
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
    }
}

- (IBAction)switchCameraTapped:(id)sender {
    [self.scanner flipCamera];
}

- (IBAction)toggleTorchTapped:(id)sender {
    if (self.scanner.torchMode == MTBTorchModeOff) {
        self.scanner.torchMode = MTBTorchModeOn;
        self.toggleTorchButton.titleLabel.text = @"Disable Torch";
    } else {
        self.scanner.torchMode = MTBTorchModeOff;
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
    }
}

#pragma mark - Gesture Handlers

- (void)previewTapped {
    if (![self.scanner isScanning] && !self.captureIsFrozen) {
        return;
    }
    
    if (!self.didShowCaptureWarning) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Capture Frozen" message:@"The capture is now frozen. Tap the preview again to unfreeze." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
        
        self.didShowCaptureWarning = YES;
    }
    
    if (self.captureIsFrozen) {
        [self.scanner unfreezeCapture];
    } else {
        [self.scanner freezeCapture];
    }
    
    self.captureIsFrozen = !self.captureIsFrozen;
}

#pragma mark - Setters

- (void)setUniqueCodes:(NSMutableArray *)uniqueCodes {
   // _uniqueCodes = uniqueCodes;
}

#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Scanning Unavaialble" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)action_OpenScannerView:(id)sender {
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        [_view_IntsallNewEquipments addSubview:_scannerPreviewView];
        
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
        
    } else {
        
        
        [_view_IntsallNewEquipments addSubview:_scannerPreviewView];
        
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];

      //  [global AlertMethod:Alert :@"Please check your internet connection, this functionality requires internet connectivity."];
        
    }
    
}

- (IBAction)action_CancelScanning:(id)sender {
    
    [self stopScanning];
    [_scannerPreviewView removeFromSuperview];
    
}

- (IBAction)action_TorchMode:(id)sender {
    
    if (self.scanner.torchMode == MTBTorchModeOff) {
        self.scanner.torchMode = MTBTorchModeOn;
        self.toggleTorchButton.titleLabel.text = @"Disable Torch";
        [_btnTorch setTitle:@"Disable Torch" forState:UIControlStateNormal];
    } else {
        self.scanner.torchMode = MTBTorchModeOff;
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
        [_btnTorch setTitle:@"Enable Torch" forState:UIControlStateNormal];
    }
    
}


-(void)setEquipmentValuesAfterScanning :(NSString*)strEquipNo{
    
    BOOL isPartPresent;
    
    isPartPresent=NO;
    NSDictionary *dictData;
    
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryItems"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        dictData=[arrOfAreaMaster objectAtIndex:k];
        
        NSString *strEquipNoToCheck=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ItemNumber"]];
        NSString *strEquipmentIdToCheck=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ItemType"]];

        if (([strEquipNoToCheck caseInsensitiveCompare:strEquipNo] == NSOrderedSame) && [strEquipmentIdToCheck isEqualToString:@"1"]) {
            
            isPartPresent=YES;
            break;
        }
        
    }
    
    if (isPartPresent) {
        
        [_btn_EquipmentsName setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        strItemSysName=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        strItemNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ItemNumber"]];
        
        NSString *strDepartMentSysName=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"departmentSysName"]];

        NSString *strPartCate=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
        
        NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
        
        for (int k=0; k<arrOfAreaMaster.count; k++) {
            
            NSDictionary *dictDataa=arrOfAreaMaster[k];
            NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
            
            NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strCategorySysNameTocheck=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
            
            if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
                
                if ([strCategorySysNameTocheck isEqualToString:strPartCate]) {
                    
                    [_btn_Category setTitle:[dictDataa valueForKey:@"Name"] forState:UIControlStateNormal];
                    strCategoryId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryMasterId"]];
                    
                }
                
            }
        }

        //Setting Current date ON Installed date
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        strDateEquip = [dateFormat stringFromDate:[NSDate date]];
        [_btn_InstalledOn setTitle:strDateEquip forState:UIControlStateNormal];
        strInstalledOn=strDateEquip;

        
    } else {
        
        [global AlertMethod:Alert :@"No Equipment available"];
        //to test commit
        
    }
    
}
@end

