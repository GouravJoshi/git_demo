//
//  EquipmentHistoryTableViewCell.h
//  DPS
//  peSTream asdfsadfsdfsd
//  Created by Saavan Patidar on 18/04/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquipmentHistoryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblWorkorderNo;
@property (strong, nonatomic) IBOutlet UILabel *lblServicedate;
@property (strong, nonatomic) IBOutlet UILabel *lblNotes;
@property (strong, nonatomic) IBOutlet UILabel *lblIssues;

@end
