//
//  EquipmentHistoryViewController.h
//  DPS
//  peSTream sadfsdfsdfsdf
//  Created by Saavan Patidar on 18/04/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface EquipmentHistoryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tblViewEquipmentHistory;
- (IBAction)action_back:(id)sender;
- (IBAction)action_ScanEquipment:(id)sender;
- (IBAction)action_EnterEquipNo:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *scannerPreviewView;
- (IBAction)action_OpenScannerView:(id)sender;
- (IBAction)action_CancelScanning:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnTorch;
- (IBAction)action_TorchMode:(id)sender;

@end
