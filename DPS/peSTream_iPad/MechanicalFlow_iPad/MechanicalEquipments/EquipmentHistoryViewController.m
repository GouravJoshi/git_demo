//
//  EquipmentHistoryViewController.m
//  DPS
//  peSTream
//  Created by Saavan Patidar on 18/04/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "EquipmentHistoryViewController.h"
#import "AllImportsViewController.h"

@interface EquipmentHistoryViewController ()
{
    Global *global;
    NSArray *arrEquipmentHistory;
    NSString *strScannedResult;
}

@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (nonatomic, weak) IBOutlet UIButton *toggleScanningButton;
@property (nonatomic, assign) BOOL captureIsFrozen;
@property (nonatomic, assign) BOOL didShowCaptureWarning;
@property (nonatomic, weak) IBOutlet UIButton *toggleTorchButton;

@end

@implementation EquipmentHistoryViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    global = [[Global alloc] init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"" forKey:@"ScannedResult"];
    [defs setBool:NO forKey:@"IsScannerView"];
    [defs synchronize];

    _tblViewEquipmentHistory.rowHeight=UITableViewAutomaticDimension;
    _tblViewEquipmentHistory.estimatedRowHeight=200;
    _tblViewEquipmentHistory.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    [_tblViewEquipmentHistory setHidden:YES];
    [_tblViewEquipmentHistory setSeparatorColor:[UIColor colorWithRed:(95/255.0) green:(178/255.0) blue:(175/255.0) alpha:1.0]];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL isFromScanner=[defs boolForKey:@"IsScannerView"];
    
    if (isFromScanner) {
        
        strScannedResult=[defs valueForKey:@"ScannedResult"];
        
        if (strScannedResult.length>0) {
            
            BOOL isNetAvailable=[global isNetReachable];
            
            if (isNetAvailable) {
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Equipment History..."];
                
                [self performSelector:@selector(getEquipMentHistory) withObject:nil afterDelay:0.2];

            } else {
                
                [global AlertMethod:Alert :ErrorInternetMsg];
                
            }
            
        }
                
        [defs setValue:@"" forKey:@"ScannedResult"];
        [defs setBool:YES forKey:@"IsScannerView"];
        [defs synchronize];
        
        
    }
    
}

//============================================================================
#pragma mark- ------------Scan Equipment------------------
//============================================================================

-(void)methodOpenScannerView{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"" forKey:@"ScannedResult"];
    [defs setBool:YES forKey:@"IsScannerView"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ScannerViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ScannerViewController"];
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}

-(void)getEquipMentHistory{
    
    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
    NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];

    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMain,UrlGetEquipmentHistory,strCompanyKey,UrlEquipmentno,strScannedResult];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             

             if ((ResponseDict==nil) || ResponseDict.count==0) {
                 
                 [_tblViewEquipmentHistory setHidden:YES];
                 [global AlertMethod:Alert :@"No data available"];
                 [DejalBezelActivityView removeView];

             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDict =dictData;
                 
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                 
                 if (strException.length==0) {
                     
                     [_tblViewEquipmentHistory setHidden:NO];
                     
                     arrEquipmentHistory=(NSArray*)ResponseDict;
                     
                     [_tblViewEquipmentHistory reloadData];
                     
                     [DejalBezelActivityView removeView];

                 } else {
                     
                     [_tblViewEquipmentHistory setHidden:YES];
                     ResponseDict=nil;
                     [global AlertMethod:Alert :@"No data available"];
                     [DejalBezelActivityView removeView];

                 }
                 
             }
             
         }];
    }
    @catch (NSException *exception) {
        
        [_tblViewEquipmentHistory setHidden:YES];
        [global AlertMethod:Alert :Sorry];
        [DejalBezelActivityView removeView];
    }
    @finally {
    }
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrEquipmentHistory.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return tableView.rowHeight;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EquipmentHistoryTableViewCell *cell = (EquipmentHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EquipmentHistoryTableViewCell" forIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [self configureCellEquipment:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCellEquipment:(EquipmentHistoryTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    
    //[string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:238.0/255 green:132.0/255.0 blue:50.0/255.0 alpha:1] range:NSMakeRange(0,10)];

    NSDictionary *dictData=arrEquipmentHistory[indexPath.row];
    NSString *strWorkOrderNo=[NSString stringWithFormat:@"Acc #: %@, Work Order #: %@",[dictData valueForKey:@"AccountNo"],[dictData valueForKey:@"WorkOrderNo"]];
    
    NSString *strTextView = strWorkOrderNo;
    NSRange rangeBold = [strTextView rangeOfString:@"Acc #:"];
    NSRange rangeBold1 = [strTextView rangeOfString:@"Work Order #:"];
    UIFont *fontText = [UIFont boldSystemFontOfSize:22];
    NSDictionary *dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
    NSMutableAttributedString *mutAttrTextViewString = [[NSMutableAttributedString alloc] initWithString:strTextView];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold1];
    [cell.lblWorkorderNo setAttributedText:mutAttrTextViewString];
    
    NSString *strServiceDate=[global ChangeDateFormatOnGeneralInfNew :[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceDateTime"]]];
    NSString *strEmpName=[global getEmployeeNameViaEMPId:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"EmployeeNo"]]];
    NSString *strServiceDatenTechName=[NSString stringWithFormat:@"Service Date: %@ Tech Name: %@",strServiceDate,strEmpName];
    
    strTextView = strServiceDatenTechName;
    NSRange rangeBold2 = [strTextView rangeOfString:@"Service Date:"];
    NSRange rangeBold3 = [strTextView rangeOfString:@"Tech Name:"];
    dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
    mutAttrTextViewString = [[NSMutableAttributedString alloc] initWithString:strTextView];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold2];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold3];
    [cell.lblServicedate setAttributedText:mutAttrTextViewString];

    NSString *strNotes=[NSString stringWithFormat:@"Office Notes: %@\n\nAdditional Notes: %@",[NSString stringWithFormat:@"%@",([[dictData valueForKey:@"OfficeNotes"] isEqualToString:@""]) ? @"N/A" : [dictData valueForKey:@"OfficeNotes"]],[NSString stringWithFormat:@"%@",([[dictData valueForKey:@"Inspectionresults"] isEqualToString:@""]) ? @"N/A" : [dictData valueForKey:@"Inspectionresults"]]];

//    strNotes=@"Office Notes: asdfasdfhsakdfhlksadjflkjsadfklsadfasdfasdfsadfsadfsadfasdfsadf \n\nAdditional Notes: asdfasdfsadfasdfasdfygteiqwynfyhcuaishkdjlasdfhjksdhfjksdahfjksadhfkjahsdfjhxzjcnkjvsahkdjfhiuaewhfkjsadhkfjahsdkfjahsdkjfhsadkjfhsdkajfhajksdfhsjkdafsdfasdfads";
    
//    strTextView = strNotes;
//    rangeBold2 = [strTextView rangeOfString:@"Office Notes:"];
//    rangeBold3 = [strTextView rangeOfString:@"Additional Notes:"];
//    dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
//    mutAttrTextViewString = [[NSMutableAttributedString alloc] initWithString:strTextView];
//    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold2];
//    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold3];
//    [cell.lblNotes setAttributedText:mutAttrTextViewString];
    
    NSString *strWoType=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SubWOType"]];
    
    NSMutableString *strIssuesToShow=[[NSMutableString alloc]init];
    
    [strIssuesToShow appendString:[NSString stringWithFormat:@"%@",strNotes]];

    if ([strWoType isEqualToString:@"FR"]) {
        
        NSArray *arrayOfIssues=[dictData valueForKey:@"SubWorkOrderIssueExtSerDcs"];
        
        for (int k=0; k<arrayOfIssues.count; k++) {
            //SubWOIssueRepairDcs  RepairName ServiceIssue
            
            NSDictionary *dictDataIssues=arrayOfIssues[k];
            
            NSString *strIssuesName=[NSString stringWithFormat:@"%@",[dictDataIssues valueForKey:@"ServiceIssue"]];
            
            if (strIssuesToShow.length==0) {
                
                [strIssuesToShow appendString:[NSString stringWithFormat:@"\n\n1. Issue Name: %@",strIssuesName]];
                
            } else {
                
                int no=k+1;
                [strIssuesToShow appendString:[NSString stringWithFormat:@"\n\n %d. Issue Name: %@",no,strIssuesName]];
                
            }
            
            
            NSArray *arrOfRepairs=[dictDataIssues valueForKey:@"SubWOIssueRepairDcs"];
            
            for (int l=0; l<arrOfRepairs.count; l++) {
                
                NSDictionary *dictDataRepairs=arrOfRepairs[l];
                
                NSString *strRepairName=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"RepairName"]];
                NSString *strRepairAmount=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"RepairAmt"]];
                
                NSString *strRepairStatus;
                
                NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"CustomerFeedback"]];
                NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"IsCompleted"]];
                if (([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
                {
                    if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                    {
                    }
                    else
                    {
                        strRepairStatus=@"Approved";
                    }
                    
                }

                if([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                    {
                        strRepairStatus=@"Completed";
                        
                    }
                }

                if (([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
                {
                    
                    strRepairStatus=@"Declined";
                    
                }
                
                NSArray *arrOfRepairsPartss=[dictDataRepairs valueForKey:@"SubWOIssueRepairPartDcs"];
                
                NSMutableString *strRepairPartName=[[NSMutableString alloc]init];
                
                for (int m=0; m<arrOfRepairsPartss.count; m++) {
                    
                    NSDictionary *dictDatarepairPartss=arrOfRepairsPartss[m];
                    
                    NSString *strPartName=[NSString stringWithFormat:@"%@",[dictDatarepairPartss valueForKey:@"PartName"]];
                    
                    if (strRepairPartName.length==0) {
                        
                        [strRepairPartName appendString:[NSString stringWithFormat:@"%@",strPartName]];

                    } else {
                        
                        [strRepairPartName appendString:[NSString stringWithFormat:@", %@",strPartName]];

                    }

                }
                
                if (l==0) {
                    
                    [strIssuesToShow appendString:[NSString stringWithFormat:@"\n\n      1. Repair Name: %@\n\n          Repair Amount($): %@\n\n          Status: %@\n\n          Repair Part Name : %@",strRepairName,strRepairAmount,strRepairStatus,strRepairPartName]];
                    
                } else {
                    
                    int no=l+1;
                    
                    [strIssuesToShow appendString:[NSString stringWithFormat:@"\n\n      %d. Repair Name: %@ \n\n          Repair Amount($): %@\n\n          Status: %@\n\n          Repair Part Name : %@",no,strRepairName,strRepairAmount,strRepairStatus,strRepairPartName]];
                    
                }
            }
            
        }
        
    } else {
        
        NSArray *arrayOfIssues=[dictData valueForKey:@"SubWorkOrderIssueExtSerDcs"];
        
        for (int k=0; k<arrayOfIssues.count; k++) {
            //SubWOIssueRepairDcs  RepairName ServiceIssue
            
            NSDictionary *dictDataIssues=arrayOfIssues[k];
            
            NSString *strIssuesName=[NSString stringWithFormat:@"%@",[dictDataIssues valueForKey:@"ServiceIssue"]];
            
            if (strIssuesToShow.length==0) {
                
                [strIssuesToShow appendString:[NSString stringWithFormat:@"1. Issue Name: %@",strIssuesName]];
                
            } else {
                
                int no=k+1;
                [strIssuesToShow appendString:[NSString stringWithFormat:@"\n\n %d. Issue Name: %@",no,strIssuesName]];
                
            }
            
            
            NSArray *arrOfRepairsParts=[dictDataIssues valueForKey:@"SubWOIssueRepairPartDcs"];
            
            for (int l=0; l<arrOfRepairsParts.count; l++) {
                
                NSDictionary *dictDataRepairs=arrOfRepairsParts[l];
                
                NSString *strPartName=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"PartName"]];
                
                NSString *strQty=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"Qty"]];
                NSString *strUnitPrice=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"UnitPrice"]];
                NSString *strMultiplier=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"Multiplier"]];

                float totalAmount=[strQty floatValue]*[strUnitPrice floatValue]*[strMultiplier floatValue];
                
                NSString *strPartStatus;
                
                NSString *strAprroveCheck=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"CustomerFeedback"]];
                NSString *strCompleted=[NSString stringWithFormat:@"%@",[dictDataRepairs valueForKey:@"IsCompleted"]];
                
                if (([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"]))
                {
                    if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                    {
                        
                        
                    }else{
                        
                        strPartStatus=@"Approved";
                        
                    }
                }               
                
                if([strAprroveCheck isEqualToString:@"1"]||[strAprroveCheck isEqualToString:@"true"]||[strAprroveCheck isEqualToString:@"True"])
                {
                    if ([strCompleted isEqualToString:@"1"]||[strCompleted isEqualToString:@"true"]||[strCompleted isEqualToString:@"True"])
                    {
                        strPartStatus=@"Completed";
                        
                    }
                }
                
                if (([strAprroveCheck isEqualToString:@"0"]||[strAprroveCheck isEqualToString:@"false"]||[strAprroveCheck isEqualToString:@"False"]))
                {
                    
                    strPartStatus=@"Declined";
                    
                }
                if (l==0) {
                    
                    [strIssuesToShow appendString:[NSString stringWithFormat:@"\n\n      1. Part Name: %@\n\n          Part Amount($): %.02f\n\n          Status: %@",strPartName,totalAmount,strPartStatus]];
                    
                } else {
                    
                    int no=l+1;
                    
                    [strIssuesToShow appendString:[NSString stringWithFormat:@"\n\n      %d. Part Name: %@\n\n          Part Amount($): %.02f\n\n          Status: %@",no,strPartName,totalAmount,strPartStatus]];
                    
                }
            }
            
        }
        
    }
    
    strTextView = strIssuesToShow;
    
    dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
    mutAttrTextViewString = [[NSMutableAttributedString alloc] initWithString:strTextView];

    rangeBold2 = [strTextView rangeOfString:@"Issue Name:"];
    
    while (rangeBold2.location != NSNotFound)
    {
        [mutAttrTextViewString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20.0f] range:rangeBold2];
        
        NSRange rangeToSearch;
        rangeToSearch.location = rangeBold2.location + rangeBold2.length;
        rangeToSearch.length = strTextView.length - rangeToSearch.location;
        rangeBold2 = [strTextView rangeOfString:@"Issue Name:" options:0 range:rangeToSearch];
    }

    rangeBold3 = [strTextView rangeOfString:@"Part Name:"];
    
    while (rangeBold3.location != NSNotFound)
    {
        [mutAttrTextViewString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20.0f] range:rangeBold3];
        
        NSRange rangeToSearch;
        rangeToSearch.location = rangeBold3.location + rangeBold3.length;
        rangeToSearch.length = strTextView.length - rangeToSearch.location;
        rangeBold3 = [strTextView rangeOfString:@"Part Name:" options:0 range:rangeToSearch];
    }

    NSRange rangeBold4 = [strTextView rangeOfString:@"Part Amount($):"];
    
    while (rangeBold4.location != NSNotFound)
    {
        [mutAttrTextViewString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20.0f] range:rangeBold4];
        
        NSRange rangeToSearch;
        rangeToSearch.location = rangeBold4.location + rangeBold4.length;
        rangeToSearch.length = strTextView.length - rangeToSearch.location;
        rangeBold4 = [strTextView rangeOfString:@"Part Amount($):" options:0 range:rangeToSearch];
    }

    NSRange rangeBold5 = [strTextView rangeOfString:@"Status:"];
    
    while (rangeBold5.location != NSNotFound)
    {
        [mutAttrTextViewString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20.0f] range:rangeBold5];
        
        NSRange rangeToSearch;
        rangeToSearch.location = rangeBold5.location + rangeBold5.length;
        rangeToSearch.length = strTextView.length - rangeToSearch.location;
        rangeBold5 = [strTextView rangeOfString:@"Status:" options:0 range:rangeToSearch];
    }

    NSRange rangeBold6 = [strTextView rangeOfString:@"Repair Name:"];
    
    while (rangeBold6.location != NSNotFound)
    {
        [mutAttrTextViewString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20.0f] range:rangeBold6];
        
        NSRange rangeToSearch;
        rangeToSearch.location = rangeBold6.location + rangeBold6.length;
        rangeToSearch.length = strTextView.length - rangeToSearch.location;
        rangeBold6 = [strTextView rangeOfString:@"Repair Name:" options:0 range:rangeToSearch];
    }

    NSRange rangeBold7 = [strTextView rangeOfString:@"Repair Amount($):"];
    
    while (rangeBold7.location != NSNotFound)
    {
        [mutAttrTextViewString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20.0f] range:rangeBold7];
        
        NSRange rangeToSearch;
        rangeToSearch.location = rangeBold7.location + rangeBold7.length;
        rangeToSearch.length = strTextView.length - rangeToSearch.location;
        rangeBold7 = [strTextView rangeOfString:@"Repair Amount($):" options:0 range:rangeToSearch];
    }


    NSRange rangeBold8 = [strTextView rangeOfString:@"Office Notes:"];
    NSRange rangeBold9 = [strTextView rangeOfString:@"Additional Notes:"];
    
    //Repair Part Name :
    NSRange rangeBold10 = [strTextView rangeOfString:@"Repair Part Name:"];
    
    while (rangeBold10.location != NSNotFound)
    {
        [mutAttrTextViewString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20.0f] range:rangeBold10];
        
        NSRange rangeToSearch;
        rangeToSearch.location = rangeBold10.location + rangeBold10.length;
        rangeToSearch.length = strTextView.length - rangeToSearch.location;
        rangeBold10 = [strTextView rangeOfString:@"Repair Part Name:" options:0 range:rangeToSearch];
    }

    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold2];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold3];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold4];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold5];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold6];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold7];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold8];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBold9];

    
    [cell.lblIssues setAttributedText:mutAttrTextViewString];
    

}

- (IBAction)action_back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)action_ScanEquipment:(id)sender {
    
    [self methodOpenScannerView];
    
}

- (IBAction)action_EnterEquipNo:(id)sender {
    
    [self alertToEnterEquipmentNo];
    
}

-(void)alertToEnterEquipmentNo{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Enter Equipment #"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Equipment # Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Search" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        if (txtHistoricalDays.text.length>0) {
            
            strScannedResult=txtHistoricalDays.text;
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Equipment History..."];
            
            [self performSelector:@selector(getEquipMentHistory) withObject:nil afterDelay:0.2];
            
        }else {
            
            [self performSelector:@selector(alertToShow) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)alertToShow{
    
    [global AlertMethod:Alert :@"Enter something to search"];
    
}


#pragma mark - Scanner Methods

- (MTBBarcodeScanner *)scanner {
    if (!_scanner) {
        _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_scannerPreviewView];
    }
    return _scanner;
}

#pragma mark - Scanning

- (void)startScanning {
    
    // self.uniqueCodes = [[NSMutableArray alloc] init];
    
    NSError *error = nil;
    [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
        for (AVMetadataMachineReadableCodeObject *code in codes) {
            
            //            UIAlertView *Alet=[[UIAlertView alloc]initWithTitle:@"Scanned Result" message:code.stringValue delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [Alet show];
            
            [self stopScanning];
            
            [_scannerPreviewView removeFromSuperview];
            
            strScannedResult=code.stringValue;
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Equipment History..."];
            
            [self performSelector:@selector(getEquipMentHistory) withObject:nil afterDelay:0.2];

            
        }
    } error:&error];
    
    if (error) {
        NSLog(@"An error occurred: %@", error.localizedDescription);
    }
    
    [self.toggleScanningButton setTitle:@"Stop Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = [UIColor redColor];
}
- (void)stopScanning {
    [self.scanner stopScanning];
    
    [self.toggleScanningButton setTitle:@"Start Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = self.view.tintColor;
    
    self.captureIsFrozen = NO;
}

-(void)callMethod{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Equipment History..."];
    
    [self performSelector:@selector(getEquipMentHistory) withObject:nil afterDelay:0.2];

}
#pragma mark - Actions

- (IBAction)toggleScanningTapped:(id)sender {
    if ([self.scanner isScanning] || self.captureIsFrozen) {
        [self stopScanning];
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
    } else {
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
    }
}

- (IBAction)switchCameraTapped:(id)sender {
    [self.scanner flipCamera];
}

- (IBAction)toggleTorchTapped:(id)sender {
    if (self.scanner.torchMode == MTBTorchModeOff) {
        self.scanner.torchMode = MTBTorchModeOn;
        self.toggleTorchButton.titleLabel.text = @"Disable Torch";
    } else {
        self.scanner.torchMode = MTBTorchModeOff;
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
    }
}

#pragma mark - Gesture Handlers

- (void)previewTapped {
    if (![self.scanner isScanning] && !self.captureIsFrozen) {
        return;
    }
    
    if (!self.didShowCaptureWarning) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Capture Frozen" message:@"The capture is now frozen. Tap the preview again to unfreeze." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
        
        self.didShowCaptureWarning = YES;
    }
    
    if (self.captureIsFrozen) {
        [self.scanner unfreezeCapture];
    } else {
        [self.scanner freezeCapture];
    }
    
    self.captureIsFrozen = !self.captureIsFrozen;
}

#pragma mark - Setters

- (void)setUniqueCodes:(NSMutableArray *)uniqueCodes {
    // _uniqueCodes = uniqueCodes;
}

#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Scanning Unavaialble" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)action_OpenScannerView:(id)sender {
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        CGRect frameFor_view_scannerPreviewView=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height);
        [_scannerPreviewView setFrame:frameFor_view_scannerPreviewView];
        [self.view addSubview:_scannerPreviewView];
        
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
        
    } else {
        
        [global AlertMethod:Alert :@"Please check your internet connection, this functionality requires internet connectivity."];
        
    }
    
}

- (IBAction)action_CancelScanning:(id)sender {
    
    [self stopScanning];
    [_scannerPreviewView removeFromSuperview];
    
}

- (IBAction)action_TorchMode:(id)sender {
    
    if (self.scanner.torchMode == MTBTorchModeOff) {
        self.scanner.torchMode = MTBTorchModeOn;
        self.toggleTorchButton.titleLabel.text = @"Disable Torch";
        [_btnTorch setTitle:@"Disable Torch" forState:UIControlStateNormal];
    } else {
        self.scanner.torchMode = MTBTorchModeOff;
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
        [_btnTorch setTitle:@"Enable Torch" forState:UIControlStateNormal];
    }
    
}

@end
