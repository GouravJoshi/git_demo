//
//  ServiceHistoryMechanical.m
//  DPS
//  peSTream
//  Created by Rakesh Jain on 31/07/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream

#import "ServiceHistoryMechanical.h"
#import "AllImportsViewController.h"

@interface ServiceHistoryMechanical ()
{
    Global *global;
    NSString *strServiceUrlMainServiceAutomation,*strCompanyKeyy,*strAcctNo;
    NSArray *arrData;
    UITableView *tblRecord;
    UIView *viewBackGround;
    NSArray *arrFilter,*arrFinalFilter;
    BOOL isFilter;
    
    
}
@end

@implementation ServiceHistoryMechanical
@synthesize strAccountNo,strFromWhere;
- (void)viewDidLoad {
    [super viewDidLoad];
    global=[[Global alloc]init];
    arrFinalFilter=[[NSMutableArray alloc]init];
    isFilter=NO;
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [viewBackGround addGestureRecognizer:singleTapGestureRecognizer];
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"]];
    strCompanyKeyy     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strAcctNo=[defs valueForKey:@"AccNoService"];
    
    _btnSearchAddress.titleLabel.font=[UIFont systemFontOfSize:14];
    [self methodBorderColor];
    //[self getServiceHistory];
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getServiceHistory];
        [DejalBezelActivityView removeView];
    });
    
    _lblTitle.text=[defsLogindDetail valueForKey:@"lblNameNew"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-  -------------------- GET SERVICE HISTORY -------------------
-(void)getServiceHistory
{
    //  [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
    }
    else
    {
        NSString *strUrl;
        //http://tsrs.stagingsoftware.com/api/MobileToServiceAuto/GetServiceHistoryByAccountNo?Companykey=Automation&AccountNo=10024
        
        if ([strFromWhere isEqualToString:@"NewSalesFlow"]) {
            
            strAcctNo = _strAccNoSalesFlow;
        }
        
        strUrl = [NSString stringWithFormat:@"%@%@%@&AccountNo=%@",strServiceUrlMainServiceAutomation,UrlMechanicalServiceHistory,strCompanyKeyy,strAcctNo],
        
        NSLog(@"GET Service History  URl-----%@",strUrl);
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        NSURL *url = [NSURL URLWithString:strUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        
        if (jsonData==nil)
        {
            NSString *strTitle = Alert;
            NSString *strMsg = NoDataAvailableee;
            [global AlertMethod:strTitle :strMsg];
            [DejalActivityView removeView];
            [DejalBezelActivityView removeView];
            
        }
        else
        {
            NSArray* responseArr = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            
            //temp
            NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
            [dictTempResponse setObject:responseArr forKey:@"response"];
            NSDictionary *dict=[[NSDictionary alloc]init];
            dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
            NSMutableDictionary *dictData=[dict valueForKey:@"response"];
            responseArr =(NSArray*)dictData;
            
            //end
            
            
            if (responseArr.count==0)
            {
                NSString *strTitle = Alert;
                NSString *strMsg = NoDataAvailableee;
                [global AlertMethod:strTitle :strMsg];
                
            }
            else
            {
                NSLog(@"Plan Response %@",responseArr);
                arrData=responseArr;
                [_tblData reloadData];
                NSMutableArray *arrTemp;
                arrTemp=[[NSMutableArray alloc]init];
                
                NSMutableArray *arrKey,*arrVal;
                [arrKey addObject:@""];
                
                
                
                for (int i=0; i<responseArr.count; i++)
                {
                    NSDictionary *dict=[responseArr objectAtIndex:i];
                    
                    NSString *strAdd;
                    strAdd=[NSString stringWithFormat:@"%@,%@,%@,%@,%@",[dict valueForKey:@"ServicesAddress1"],[dict valueForKey:@"ServiceCountry"],[dict valueForKey:@"ServiceState"],[dict valueForKey:@"ServiceCity"],[dict valueForKey:@"ServiceZipcode"]];
                    
                    // [arrTemp addObject:[global strCombinedAddressService:dict]];
                    strAdd=[self strCombinedAddressService:dict];
                    
                    [arrTemp addObject:strAdd];
                    
                }
                
                //Temp Dict
                NSMutableArray *arr;
                arr=[[NSMutableArray alloc]init];
                [arr addObject:@"All"];
                for (int i=1; i<arrTemp.count; i++)
                {
                    [arr addObject:[arrTemp objectAtIndex:i]];
                }
                NSLog(@"%@",arrTemp);
                
                NSArray *arrCheck;
                arrCheck=[[NSSet setWithArray:arr] allObjects];
                
                
                //arrFilter = [[NSSet setWithArray:arr] allObjects];
                
                arrFilter = [arrCheck sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                
                NSMutableArray *arrCheckNew;
                arrCheckNew=[[NSMutableArray alloc]init];
                for (int i=0; i<arrFilter.count; i++)
                {
                    /*if ([[arrFilter objectAtIndex:i] isEqualToString:@"All"])
                     {
                     NSString *c;
                     c=[arrFilter objectAtIndex:i];
                     
                     
                     }*/
                    [arrCheckNew addObject:[arrFilter objectAtIndex:i]];
                }
                for (int i=0; i<arrCheckNew.count; i++)
                {
                    if ([[arrCheckNew objectAtIndex:i] isEqualToString:@"All"])
                    {
                        [arrCheckNew replaceObjectAtIndex:i withObject:[arrCheckNew objectAtIndex:0]];
                        [arrCheckNew replaceObjectAtIndex:0 withObject:@"All"];
                        
                        break;
                    }
                }
                NSLog(@"Final %@",arrCheckNew);
                arrFilter=arrCheckNew;
                
            }
            [DejalActivityView removeView];
            [DejalBezelActivityView removeView];
        }
        
    }
}

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

- (IBAction)actionOnBack:(id)sender
{
    
    if ([strFromWhere isEqualToString:@"PestNewFlow"]) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    } else {
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }
    
}
- (IBAction)actionOnSerarchByAddress:(id)sender
{
    if (arrData.count == 0)
    {
        [global AlertMethod:@"Alert!" :@"No data available"];
    }
    else
    {
        [self tableCreate];
        tblRecord.tag=10;
        [self tableLoad:tblRecord.tag];
    }

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblData)
    {
        if (isFilter==YES)
        {
            return arrFinalFilter.count;
        }
        else
            return arrData.count;
        
    }
    else
    {
        return arrFilter.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(tableView==_tblData)
    {
        ServiceHistoryTableViewCell *cell = (ServiceHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ServiceHistoryTableViewCell" forIndexPath:indexPath];
        
        if (isFilter==YES)
        {
            NSDictionary *dict=[arrFinalFilter objectAtIndex:indexPath.row];
            
            cell.lblValueForName.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeName"]];
            cell.lblValueForName.text=[global strFullName:dict];
            cell.lblValueForOrder.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WorkOrderNo"]];
            NSString *strAdd;
            strAdd=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[dict valueForKey:@"ServicesAddress1"],[dict valueForKey:@"ServiceCountry"],[dict valueForKey:@"ServiceState"],[dict valueForKey:@"ServiceCity"],[dict valueForKey:@"ServiceZipcode"]];
            strAdd=[self strCombinedAddressService:dict];
            
            
            cell.lblValueForAddress.text=[NSString stringWithFormat:@"%@",strAdd];
            cell.lblValueFroEmployeeNo.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeNo"]];
            cell.lblValueForRout.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]];
            if ([cell.lblValueForRout.text isEqualToString:@"<null>"]) {
                
                cell.lblValueForRout.text=@"N/A";
                
            }
            cell.lblValueForScheduleStartDateTime.text=[global ChangeDateToLocalDateOtherTimeAlso :[NSString stringWithFormat:@"%@",[dict valueForKey:@"ScheduleOnStartDateTime"]]];
            [cell.btnPdf setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"InvoicePath"]] forState:UIControlStateNormal];
            [cell.btnPdf setTitle:[NSString stringWithFormat:@"%@",@"Service Report"] forState:UIControlStateNormal];
            cell.btnPdf.tag=indexPath.row;
            [cell.btnPdf addTarget:self
                            action:@selector(buttonClickOnPDF:) forControlEvents:UIControlEventTouchDown];
        }
        else
        {
            NSDictionary *dict=[arrData objectAtIndex:indexPath.row];
            
            cell.lblValueForName.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeName"]];
            cell.lblValueForName.text=[global strFullName:dict];//[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeName"]];
            
            cell.lblValueForOrder.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WorkOrderNo"]];
            NSString *strAdd;
            strAdd=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[dict valueForKey:@"ServicesAddress1"],[dict valueForKey:@"ServiceCountry"],[dict valueForKey:@"ServiceState"],[dict valueForKey:@"ServiceCity"],[dict valueForKey:@"ServiceZipcode"]];
            
            strAdd=[self strCombinedAddressService:dict];
            
            cell.lblValueForAddress.text=[NSString stringWithFormat:@"%@",strAdd];
            cell.lblValueFroEmployeeNo.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeNo"]];
            cell.lblValueForRout.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"RouteNo"]];
            if ([cell.lblValueForRout.text isEqualToString:@"<null>"]) {
                
                cell.lblValueForRout.text=@"N/A";
                
            }
            cell.lblValueForScheduleStartDateTime.text=[global ChangeDateToLocalDateOtherTimeAlso :[NSString stringWithFormat:@"%@",[dict valueForKey:@"ScheduleOnStartDateTime"]]];
            [cell.btnPdf setTitle:[NSString stringWithFormat:@"%@",@"Service Report"] forState:UIControlStateNormal];//
            cell.btnPdf.tag=indexPath.row;
            [cell.btnPdf addTarget:self
                            action:@selector(buttonClickOnPDF:) forControlEvents:UIControlEventTouchDown];
        }
        
        
        return cell;
    }
    else
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (tblRecord.tag==10)
        {
            //NSDictionary *dict=[arrDiagnostic objectAtIndex:indexPath.row];
            //cell.textLabel.text=[dict valueForKey:@"ChargeName"];
            //            if (indexPath.row==0)
            //            {
            //                cell.textLabel.text=@"ALL";
            //
            //            }
            //            else
            cell.textLabel.text=[arrFilter objectAtIndex:indexPath.row];
            
            
        }
        
        if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            
            cell.textLabel.font=[UIFont systemFontOfSize:18];
            
        }
        else{
            
            cell.textLabel.font=[UIFont systemFontOfSize:14];
            
        }
        cell.textLabel.numberOfLines=2;
        
        return cell;
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblData)
    {
        
    }
    else
    {
        /*  if (indexPath.row==0)
         {
         isFilter=NO;
         [_btnSearchAddress setTitle:@"All" forState:UIControlStateNormal];
         
         }
         else
         {*/
        if ([[arrFilter objectAtIndex:indexPath.row] isEqualToString:@"All"])
        {
            isFilter=NO;
            
        }
        else
        {
            isFilter=YES;
            
        }
        [_btnSearchAddress setTitle:[arrFilter objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        NSMutableArray *arrTemp;
        arrTemp=[[NSMutableArray alloc]init];
        for (int i=0; i<arrData.count; i++)
        {
            NSDictionary *dict=[arrData objectAtIndex:i];
            NSString *strAdd;
            strAdd=[NSString stringWithFormat:@"%@,%@,%@,%@,%@",[dict valueForKey:@"ServicesAddress1"],[dict valueForKey:@"ServiceCountry"],[dict valueForKey:@"ServiceState"],[dict valueForKey:@"ServiceCity"],[dict valueForKey:@"ServiceZipcode"]];
            strAdd=[self strCombinedAddressService:dict];
            if ([strAdd isEqualToString:[arrFilter objectAtIndex:indexPath.row]])
            {
                [arrTemp addObject:dict];
            }
            
        }
        arrFinalFilter = [[NSSet setWithArray:arrTemp] allObjects];
        //}
        [viewBackGround removeFromSuperview];
        [tblRecord removeFromSuperview];
        [_tblData reloadData];
        
    }
}
-(void)buttonClickOnPDF:(UIButton*)btn
{
    NSLog(@"button click");
    
    int tagg = btn.tag;
    NSString *strPath;
    
    if (isFilter==YES)
    {
        
        NSDictionary *dict=[arrFinalFilter objectAtIndex:tagg];
        strPath=[NSString stringWithFormat:@"%@",[dict valueForKey:@"InvoicePath"]];
        
    }
    else
    {
        
        NSDictionary *dict=[arrData objectAtIndex:tagg];
        strPath=[NSString stringWithFormat:@"%@",[dict valueForKey:@"InvoicePath"]];
        
    }
    
    if ((strPath.length==0) || ([strPath isEqualToString:@"<null>"]) || ([strPath isEqualToString:@"(null)"])) {
        
        [global AlertMethod:Alert :NoDataAvailableee];
        
    } else {
        
        [self oPenPdfView:[NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,strPath]];
        
    }
    
    
}

#pragma mark- ****** PDF Method ******

-(void)oPenLinkOnWeb :(NSString *)strUrl{
    
    NSURL *URL = [NSURL URLWithString:strUrl];
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:strUrl]) {
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }
}
-(void)oPenPdfView :(NSString *)strUrl{
    
    NSString *urlString = [NSString stringWithFormat:@"%@",strUrl];
    
    NSString *strNewString=[urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    //    NSURL *url = [NSURL URLWithString:strNewString];
    //
    //    NSData *data = [NSData dataWithContentsOfURL:url];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSArray* foo = [strNewString componentsSeparatedByString: @"/"];
    NSString* lastName = [foo objectAtIndex: foo.count-1];
    
    NSString *removed = [lastName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *mainPath = [documentsDirectory stringByAppendingPathComponent: removed];
    
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:mainPath];
    
    if (exists) {
        
        [self oPenPDFfile :mainPath];
        
    } else {
        
        //[self HudView];
        
        strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSURL *url = [NSURL URLWithString:strNewString];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        [data writeToFile:mainPath atomically:YES];
        
        [self oPenPDFfile :mainPath];
    }
    
    //[data writeToFile:mainPath atomically:YES];
    
}

-(void)oPenPDFfile :(NSString*)strPath{
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-20);
    frameFor_view_CustomerInfo.origin.x=0;
    frameFor_view_CustomerInfo.origin.y=20;
    
    [_pdfView setFrame:frameFor_view_CustomerInfo];
    
    [self.view addSubview:_pdfView];
    
    NSURL *targetURL = [NSURL fileURLWithPath:strPath];
    NSURLRequest *request_pdf = [NSURLRequest requestWithURL:targetURL];
    [_webViewww loadRequest:request_pdf];
    
}

- (IBAction)action_BackPdf:(id)sender {
    
    [_pdfView removeFromSuperview];
    
}

#pragma mark- ****** Local Table Create Method ******

-(void)tableCreate
{
    tblRecord=[[UITableView alloc]init];
    tblRecord.dataSource=self;
    tblRecord.delegate=self;
    tblRecord.backgroundColor=[UIColor whiteColor];
    tblRecord.layer.cornerRadius=20.0;
    tblRecord.clipsToBounds=YES;
    [tblRecord.layer setBorderWidth:2.0];
    [tblRecord.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
}
-(void)setTableFrame:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblRecord.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, 500);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblRecord.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    UILabel *lblOk;
    lblOk=[[UILabel alloc]init];
    lblOk.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    lblOk.textColor=[UIColor whiteColor];
    lblOk.text=@"OK";
    lblOk.textAlignment=NSTextAlignmentCenter;
    lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-80, tblRecord.frame.origin.y+tblRecord.frame.size.height+2, 160, 50);
    if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
    {
        lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-60, tblRecord.frame.origin.y+tblRecord.frame.size.height+2, 120, 40);
    }
    lblOk.layer.borderWidth=1.0;
    lblOk.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    lblOk.layer.cornerRadius=5.0;
    [viewBackGround addSubview:lblOk];
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblRecord];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
            
        default:
            break;
    }
    
    tblRecord.dataSource=self;
    tblRecord.delegate=self;
    tblRecord.backgroundColor=[UIColor whiteColor];
    tblRecord.layer.cornerRadius=20.0;
    tblRecord.clipsToBounds=YES;
    [tblRecord.layer setBorderWidth:2.0];
    [tblRecord.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblRecord reloadData];
}
#pragma mark- ************** TAP METHODS **************

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [viewBackGround removeFromSuperview];
    [tblRecord removeFromSuperview];
}
- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    [viewBackGround removeFromSuperview];
    [tblRecord removeFromSuperview];
}
-(void)methodBorderColor
{
    [_btnSearchAddress.layer setCornerRadius:5.0f];
    //[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    [_btnSearchAddress.layer setBorderColor:[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1].CGColor];
    [_btnSearchAddress.layer setBorderWidth:0.8f];
    [_btnSearchAddress.layer setShadowColor:[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1].CGColor];
    [_btnSearchAddress.layer setShadowOpacity:0.3];
    [_btnSearchAddress.layer setShadowRadius:3.0];
    [_btnSearchAddress.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //[_loaderWebView startAnimating];
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Searching..."];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    // [_loaderWebView stopAnimating];
    [DejalActivityView removeView];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"The File you are looking for does not exist.Please try again later."
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [_pdfView removeFromSuperview];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    [DejalActivityView removeView];
}
-(NSString*)strCombinedAddressService :(NSDictionary*)dictData{
    
    NSArray *arrTempKeys = [dictData allKeys];
    
    NSMutableArray *arrOfTempAddress=[[NSMutableArray alloc]init];
    
    if ([arrTempKeys containsObject:@"ServicesAddress1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServicesAddress1"]].length==0)) {
            
            NSString *str=[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServicesAddress1"]];
            str = [str  stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            [arrOfTempAddress addObject:str];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceAddress2"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceAddress2"]].length==0)) {
            
            NSString* str =[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceAddress2"]];
            str = [str  stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceAddress2"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceCityName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCityName"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCityName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceCity"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCity"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCity"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceState"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceState"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceState"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceCountry"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCountry"]].length==0)) {
            
            //[arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountryName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceZipcode"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceZipcode"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceZipcode"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceZip"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceZip"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceZip"]]];
            
        }
        
    }
    
    NSString *strCombinedAddress=@"";
    
    if (arrOfTempAddress.count>0) {
        
        strCombinedAddress=[arrOfTempAddress componentsJoinedByString:@", "];
        //[global getLocationFromAddressString:strCombinedAddress];
        
    }
    
    return strCombinedAddress;
    
}

@end

