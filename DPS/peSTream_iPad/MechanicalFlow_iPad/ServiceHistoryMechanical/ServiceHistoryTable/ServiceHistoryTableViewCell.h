//
//  ServiceHistoryTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 31/07/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblValueForName;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblValueFroEmployeeNo;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForRout;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForScheduleStartDateTime;
@property (weak, nonatomic) IBOutlet UIButton *btnPdf;


@property (strong, nonatomic) IBOutlet UILabel *lblNotes1ForNoteshistory;

@property (strong, nonatomic) IBOutlet UILabel *lblEmployee1ForNotesHistory;
@property (strong, nonatomic) IBOutlet UILabel *lblDate1ForNotesHistory;

@end
