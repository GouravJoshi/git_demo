//
//  ServiceHistoryMechanical.h
//  DPS
//  peSTream
//  Created by Rakesh Jain on 31/07/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  peSTream  

#import <UIKit/UIKit.h>

@interface ServiceHistoryMechanical : UIViewController<UITableViewDataSource,UITableViewDelegate>
- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchAddress;
- (IBAction)actionOnSerarchByAddress:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblData;
@property(strong,nonatomic)NSString *strAccountNo;
@property (strong, nonatomic) IBOutlet UIWebView *webViewww;
@property (strong, nonatomic) IBOutlet UIView *pdfView;
- (IBAction)action_BackPdf:(id)sender;
@property(strong,nonatomic)NSString *strFromWhere,*strAccNoSalesFlow;

@end
