//
//  AddPartsTableViewCell.h
//  DPS
//  peSTream  sadfgsda
//  Created by Saavan Patidar on 05/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  sadfasdfsadfsad

#import <UIKit/UIKit.h>

@interface AddPartsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblParts;
@property (strong, nonatomic) IBOutlet UILabel *lblQty;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;
@property (strong, nonatomic) IBOutlet UITextField *txtActualQty;
@property (strong, nonatomic) IBOutlet UISwitch *switchInstalled;
@property (strong, nonatomic) IBOutlet UIButton *btnViewMore;
@property (strong, nonatomic) IBOutlet UIButton *btnChargeToCustomer;

@end
