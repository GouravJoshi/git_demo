//
//  AddPartsiPadViewController.m
//  DPS
//  peSTream
//  Created by Saavan Patidar on 05/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "AddPartsiPadViewController.h"

@interface AddPartsiPadViewController ()
{
    
    Global *global;
    NSMutableArray *arrOfSubWorkServiceIssuesRepairParts,*arrDataTblView,*arrOfPriceLookup;
    NSString *strEmpID,*strUserName,*strCompanyKey,*strEmpName,*strDepartMentSysName,*strCategoryMasterId,*strGlobalDateToShow,*strMultiplierGlobal,*strWoStatus,*strWorkOrderAccNo,*strWorkOrderAddressSubType,*strWorkOrderAddressId, *strAfterHrsDuration,*strCompanyName,*strCategorySysNameSelected,*strTxtUnitPrice;
    BOOL isStandard, isWarranty, isEditParts, isHoliday, isChangeStdPartPrice, isChargeToCustomer;
    UIView *viewBackGround,*viewBackGroundOnView,*viewForDate;
    UITableView *tblData;
    UIDatePicker *pickerDate;
    NSManagedObject *objPartsEdit;
    NSDictionary *dictDetailsFortblView,*dictDetailsMastersMechanical,*dictDataPartsSelected;
    
    // Vendor Details
    NSInteger itemMasterID;
    NSString *strVendorSysNameSelected;

}
@property (strong, nonatomic) IBOutlet UIButton *btnRadioStandard;
@property (strong, nonatomic) IBOutlet UIButton *btnRadioNonStandard;
@property (strong, nonatomic) IBOutlet UIButton *btnWarranty;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectCategory;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectParts;
@property (strong, nonatomic) IBOutlet UITextField *txtQty;
@property (strong, nonatomic) IBOutlet UITextField *txtUnitPrice;
@property (strong, nonatomic) IBOutlet UITextField *txtTotalPrice;
@property (strong, nonatomic) IBOutlet UITextField *txtSerial;
@property (strong, nonatomic) IBOutlet UITextField *txtModel;
@property (strong, nonatomic) IBOutlet UITextField *txtManufacturer;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectdate;
@property (strong, nonatomic) IBOutlet UITextView *txtViewDesc;
@property (strong, nonatomic) IBOutlet UIView *viewAddParts;
@property (strong, nonatomic) IBOutlet UITextField *txtpartName;
@property (strong, nonatomic) IBOutlet UILabel *lblPartSelectOrEnter;
@property (strong, nonatomic) IBOutlet UIButton *btnAddPart;


@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (nonatomic, weak) IBOutlet UIButton *toggleScanningButton;
@property (nonatomic, assign) BOOL captureIsFrozen;
@property (nonatomic, assign) BOOL didShowCaptureWarning;
@property (nonatomic, weak) IBOutlet UIButton *toggleTorchButton;

@end

@implementation AddPartsiPadViewController

- (void)viewDidLoad {
    
    isChangeStdPartPrice=NO;
    isChargeToCustomer = YES;
    
    [super viewDidLoad];
    
    [self methodAllocation];
    
    [self methodBorderColor];
    
    [self methodLoadValues];
    
    //[self getPricLookupFromMaster];
    
    [self fetchSubWorkOrderIssuesRepairPartFromDataBaseForMechanical];
    
    BOOL isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWoStatus];
    
    if (isCompletedStatusMechanical) {
        
        [_btnAddPart setHidden:YES];
        
    }
    
    _tblViewParts.rowHeight=UITableViewAutomaticDimension;
    _tblViewParts.estimatedRowHeight=200;
    _tblViewParts.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//============================================================================
//============================================================================
#pragma mark- ----------------Void Methods----------------
//============================================================================
//============================================================================

-(void)methodTableViewAllocation{
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
}

-(void)methodLoadValues{
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strWoStatus       =[defsLead valueForKey:@"WoStatus"];

    dictDetailsFortblView=[defsLead valueForKey:@"MasterServiceAutomation"];
    
    dictDetailsMastersMechanical=[defsLead valueForKey:@"MasterServiceAutomation"];
    
    _lblAccNoDetails.text=[NSString stringWithFormat:@"%@, Sub Work Order #: %@",[defsLead valueForKey:@"lblName"],[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderNo"]]];
    
    
    strDepartMentSysName=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"departmentSysName"]];
    
    _objWorkOrderdetails=[global fetchMechanicalWorkOrderObj:_strWorkOrderId];
    
    //
    
    strWorkOrderAccNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"accountNo"]];
    
    strWorkOrderAddressId=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"serviceAddressId"]];
    
    strWorkOrderAddressSubType=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"addressSubType"]];
    
    //isHoliday
    
    NSString *strIsHoliday=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isHolidayHrs"]];
    
    if ([strIsHoliday isEqualToString:@"true"] || [strIsHoliday isEqualToString:@"1"]) {
        
        isHoliday=YES;
        
    } else {
        
        isHoliday=NO;
        
    }
    
    //AfterHrsDuration
    
    strAfterHrsDuration=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"afterHrsDuration"]];

}

-(void)methodBorderColor{
    
    _txtViewDesc.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewDesc.layer.borderWidth=1.0;
    _txtViewDesc.layer.cornerRadius=5.0;
    
    [_btnSelectParts.layer setCornerRadius:5.0f];
    [_btnSelectParts.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectParts.layer setBorderWidth:0.8f];
    [_btnSelectParts.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectParts.layer setShadowOpacity:0.3];
    [_btnSelectParts.layer setShadowRadius:3.0];
    [_btnSelectParts.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    [_btnSelectdate.layer setCornerRadius:5.0f];
    [_btnSelectdate.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectdate.layer setBorderWidth:0.8f];
    [_btnSelectdate.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectdate.layer setShadowOpacity:0.3];
    [_btnSelectdate.layer setShadowRadius:3.0];
    [_btnSelectdate.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    [_btnSelectCategory.layer setCornerRadius:5.0f];
    [_btnSelectCategory.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectCategory.layer setBorderWidth:0.8f];
    [_btnSelectCategory.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectCategory.layer setShadowOpacity:0.3];
    [_btnSelectCategory.layer setShadowRadius:3.0];
    [_btnSelectCategory.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

    // Vendor Details
    [_btnVendorName.layer setCornerRadius:5.0f];
    [_btnVendorName.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnVendorName.layer setBorderWidth:0.8f];
    [_btnVendorName.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnVendorName.layer setShadowOpacity:0.3];
    [_btnVendorName.layer setShadowRadius:3.0];
    [_btnVendorName.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}

-(void)methodAllocation{
    
    isWarranty=NO;
    global = [[Global alloc] init];
    arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
}

-(void)addPartView{
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewAddLabor
    
    CGRect frameFor_ViewAddLabor=CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width,_viewAddParts.frame.size.height);
    [_viewAddParts setFrame:frameFor_ViewAddLabor];
    
    [viewBackGround addSubview:_viewAddParts];
    
    // Setting Default Non Standard Part 
    [self setDefaultNonStandardFirst];
    
}

-(void)getPartsMaster{
    
    arrDataTblView=nil;
    arrDataTblView=[[NSMutableArray alloc]init];
    arrDataTblView=[global getPartsMaster:strDepartMentSysName :@""];
    
}

-(void)getPricLookupFromMaster :(NSString*)strCategorySysName {
    
    arrOfPriceLookup=nil;
    arrOfPriceLookup=[[NSMutableArray alloc]init];
    arrOfPriceLookup=[global getPricLookupFromMaster:strCompanyKey :strDepartMentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType :strCategorySysName];
    
}


-(void)setValuesOnEditingParts{
    
    _btnRadioNonStandard.enabled=NO;
    _btnRadioStandard.enabled=NO;

    NSString *strIsStandard=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"partCode"]];
    if (!(strIsStandard.length==0)) {
        
        isStandard=YES;
        [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_txtpartName setHidden:YES];
        [_btnSelectParts setHidden:NO];
        [_txtUnitPrice setEnabled:NO];
        //_btnRadioNonStandard.enabled=NO;

    } else {
        
        isStandard=NO;
        [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_txtpartName setHidden:NO];
        _txtpartName.text=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"partName"]];
        [_btnSelectParts setHidden:YES];
        [_txtUnitPrice setEnabled:YES];
        //_btnRadioStandard.enabled=NO;

    }
    
    NSString *strPartsWarrantyEdit=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"isWarranty"]];
    if ([strPartsWarrantyEdit isEqualToString:@"true"] || [strPartsWarrantyEdit isEqualToString:@"1"]) {
        
        isWarranty=YES;
        
        [_btnWarranty setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    } else {
        
        isWarranty=NO;
        
        [_btnWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    

    float unitPriceFloat=[[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"unitPrice"]] floatValue];
    
    _txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
    
    if (unitPriceFloat<=0) {
        
        _txtUnitPrice.text=@"";
        
    }
    
    strTxtUnitPrice=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
    
    NSString *strQTY=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"qty"]];
    NSString *strMultiplier=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"multiplier"]];
    NSString *strUnitPrice=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"unitPrice"]];
    
    float Total=[strQTY floatValue]*[strMultiplier floatValue]*[strUnitPrice floatValue];

    _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",Total];
    _txtSerial.text=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"serialNumber"]];
    _txtModel.text=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"modelNumber"]];
    _txtManufacturer.text=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"manufacturer"]];
    _txtViewDesc.text=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"partDesc"]];
    _txtQty.text=strQTY;
    strMultiplierGlobal=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"multiplier"]];
    [_btnSelectParts setTitle:[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"partName"]] forState:UIControlStateNormal];
    [_btnSelectdate setTitle:[global ChangeDateToLocalDateMechanicalParts:[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"installationDate"]]] forState:UIControlStateNormal];
    [_btnSelectCategory setTitle:[NSString stringWithFormat:@"%@",@"----Select Category----"] forState:UIControlStateNormal];

//    NSString *strPartTypee=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"partType"]];
//
//    if ([strPartTypee isEqualToString:@"Non-Standard"]) {
//
//
//
//    }else{
//
//        [self getCategoryDetail:[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"partCode"]]];
//
//    }
    
    // Changes For Showing selected categorySysName
    
    NSString *strPartCategorySysNameInDB=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"partCategorySysName"]];

    if (strPartCategorySysNameInDB.length>0) {
        
        [self showSelectedPartCategory:strPartCategorySysNameInDB];
        
    }
    

    NSString *strIsChangeStdPartPrice=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"isChangeStdPartPrice"]];
    if ([strIsChangeStdPartPrice isEqualToString:@"true"] || [strIsChangeStdPartPrice isEqualToString:@"1"]) {
        
        isChangeStdPartPrice=YES;
        
        strMultiplierGlobal=@"1";
        
        //_txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",[strMultiplierGlobal floatValue]*[strTxtUnitPrice floatValue]];
        
        // chnage for issues of unit price and total price coming same
        _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",[strMultiplierGlobal floatValue]*[strTxtUnitPrice floatValue]*[strQTY floatValue]];


    } else {
        
        isChangeStdPartPrice=NO;
        //_txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",[strMultiplierGlobal floatValue]*[strTxtUnitPrice floatValue]];
        
        
        // chnage for issues of unit price and total price coming same
        _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",[strMultiplierGlobal floatValue]*[strTxtUnitPrice floatValue]*[strQTY floatValue]];

    }
    
    // Showing Vendor Details  SysName partCode

    NSString *strPartCodeTemp=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"partCode"]];

    NSArray *arrOfPartTemp = [global getPartsMaster:strDepartMentSysName :@""];
    
    for (int k1=0; k1<arrOfPartTemp.count; k1++) {
        
        NSDictionary *dictDataTemp=arrOfPartTemp[k1];
        
        NSString *strSysNamePartTemp = [NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"SysName"]];
        
        if ([strPartCodeTemp isEqualToString:strSysNamePartTemp]) {
            
            itemMasterID = [[dictDataTemp valueForKey:@"ItemMasterId"] integerValue];

            
        }
        
    }
    

    NSString *strVendorName;
    strVendorSysNameSelected=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"vendorName"]];
    
    if (strVendorSysNameSelected.length>0) {
        
        NSUserDefaults *defsVendorName=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMechanicalMasters1=[defsVendorName valueForKey:@"MasterAllMechanical"];
        NSArray *arrayVendor=[dictMechanicalMasters1 valueForKey:@"VendorBasicExtSerDc"];
        
        for(NSDictionary *dictTempVendor in arrayVendor)
        {
            if ([[dictTempVendor valueForKey:@"SysName"] isEqualToString:strVendorSysNameSelected])
            {
                strVendorName=[NSString stringWithFormat:@"%@",[dictTempVendor valueForKey:@"Name"]];
            }
        }
        
        [_btnVendorName setTitle:strVendorName forState:UIControlStateNormal];

    } else {
        
        [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
        
    }
    
    
    
    _txtFldVendoreQuoteNo.text=[NSString stringWithFormat:@"%@",([[objPartsEdit valueForKey:@"vendorQuoteNo"] isEqual:nil]) ? @"" : [objPartsEdit valueForKey:@"vendorQuoteNo"]];
    _txtFldVendorPartNo.text=[NSString stringWithFormat:@"%@",([[objPartsEdit valueForKey:@"vendorPartNo"] isEqual:nil]) ? @"" : [objPartsEdit valueForKey:@"vendorPartNo"]];
    
    // End // Showing Vendor Details
    
    // Charge To Customer isAddedAfterApproval
    
    NSString *strChargeToCustomer=[NSString stringWithFormat:@"%@",[objPartsEdit valueForKey:@"isAddedAfterApproval"]];
    if ([strChargeToCustomer isEqualToString:@"false"] || [strChargeToCustomer isEqualToString:@"0"] || [strChargeToCustomer isEqualToString:@"False"]) {
        
        isChargeToCustomer=YES;
        
        [_btnChargeToCustomer setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    } else {
        
        isChargeToCustomer=NO;
        
        [_btnChargeToCustomer setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }

}


-(void)getCategoryDetail :(NSString*)strScannedResult{
    
    NSArray *arrDataTblViewLocal=[self getEquipMentss];
    
    NSDictionary *dictData;
    BOOL isPartPresent;
    
    isPartPresent=NO;
    
    for (int k1=0; k1<arrDataTblViewLocal.count; k1++) {
        
        dictData=[arrDataTblViewLocal objectAtIndex:k1];
        
        NSString *strItemNumberNew=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        if ([strScannedResult caseInsensitiveCompare:strItemNumberNew] == NSOrderedSame) {
            isPartPresent=YES;
            break;
        }
    }
    
    
    NSString *strCategorySysNameSelectedLocal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
    
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        NSString *strCategorySysNameNew=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
        
        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartmentSysName isEqualToString:strDepartmentSysName])) {
            
            if ([strCategorySysNameNew isEqualToString:strCategorySysNameSelectedLocal]) {
                
                [_btnSelectCategory setTitle:[dictDataa valueForKey:@"Name"] forState:UIControlStateNormal];
                strCategoryMasterId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryMasterId"]];
                strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
                
            }
            
        }
    }
    
}


-(NSMutableArray*)getEquipMentss{
    
    NSMutableArray *arrDataTblViewLocal;
    NSString *strEquipmentValueIdLocal;
    arrDataTblViewLocal=[[NSMutableArray alloc]init];
    
    NSArray *arrOfAreaMasterEquipmentTypes=[dictDetailsFortblView valueForKey:@"EquipmentTypes"];
    NSString *strEqupMentTypeValue;
    for (int k=0; k<arrOfAreaMasterEquipmentTypes.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMasterEquipmentTypes[k];
        
        NSString *strEqupMentTypeName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Text"]];
        
        if ([strEqupMentTypeName isEqualToString:@"Part"]) {
            
            strEqupMentTypeValue=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Value"]];
            
        }
        
    }
    
    if (strEqupMentTypeValue.length==0) {
        
        strEquipmentValueIdLocal=@"2";
        
        
    } else {
        
        strEquipmentValueIdLocal=strEqupMentTypeValue;
        
    }
    
    arrDataTblViewLocal=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryItems"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strEquipmentIdToCheck=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ItemType"]];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        if ([strEquipmentIdToCheck isEqualToString:strEquipmentValueIdLocal] && ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"])) {
            
            [arrDataTblViewLocal addObject:dictDataa];
            
        }
        
    }
    
    return arrDataTblViewLocal;
    
}

-(void)hideTextFields{
    
    [_txtpartName resignFirstResponder];
    [_txtQty resignFirstResponder];
    [_txtUnitPrice resignFirstResponder];
    [_txtTotalPrice resignFirstResponder];
    [_txtSerial resignFirstResponder];
    [_txtpartName resignFirstResponder];
    [_txtModel resignFirstResponder];
    [_txtManufacturer resignFirstResponder];
    [_txtViewDesc resignFirstResponder];
    
}
//============================================================================
//============================================================================
#pragma mark- ----------------Button Action Methods----------------
//============================================================================
//============================================================================

- (IBAction)action_Back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)action_Parts:(id)sender {
    
   // [_txtQty setEnabled:NO];
    
    _btnRadioNonStandard.enabled=YES;
    
    _btnRadioStandard.enabled=YES;

    strCategoryMasterId=@"";
    strCategorySysNameSelected=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
    [_btnSelectParts setTitle:@"----Select Part----" forState:UIControlStateNormal];
    [_btnSelectdate setTitle:@"----Select Date----" forState:UIControlStateNormal];
    isStandard=YES;
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    isWarranty=NO;
    isEditParts=NO;
    [_btnWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    _txtViewDesc.text=@"";
    _txtQty.text=@"";
    _txtModel.text=@"";
    _txtSerial.text=@"";
    _txtUnitPrice.text=@"";
    strTxtUnitPrice=@"";
    _txtTotalPrice.text=@"";
    _txtManufacturer.text=@"";
    _txtpartName.text=@"";
    _lblPartSelectOrEnter.text=@"Part";
    
    UIFont *fnt = [UIFont fontWithName:@"Helvetica" size:22.0];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Part*"
                                                                                         attributes:@{NSFontAttributeName: [fnt fontWithSize:22]}];
    [attributedString setAttributes:@{NSFontAttributeName : [fnt fontWithSize:22]
                                      , NSBaselineOffsetAttributeName : @10} range:NSMakeRange(4, 1)];
    
    _lblPartSelectOrEnter.attributedText=attributedString;
    
    [_txtpartName setHidden:YES];
    [_btnSelectParts setHidden:NO];
    [_txtUnitPrice setEnabled:NO];
    [_txtUnitPrice setEnabled:YES];

    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
    [self addPartView];
    
    [self setDefaultCategoryIfSingleCategoryExist];
    
}
- (IBAction)action_CLearParts:(id)sender {
    
    [self hideTextFields];
    
    strCategoryMasterId=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
    [_btnSelectParts setTitle:@"----Select Part----" forState:UIControlStateNormal];
    [_btnSelectdate setTitle:@"----Select Date----" forState:UIControlStateNormal];
    isStandard=YES;
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    isWarranty=NO;
    [_btnWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    _txtViewDesc.text=@"";
    _txtQty.text=@"";
    _txtModel.text=@"";
    _txtSerial.text=@"";
    _txtUnitPrice.text=@"";
    strTxtUnitPrice=@"";
    _txtTotalPrice.text=@"";
    _txtManufacturer.text=@"";
    _txtpartName.text=@"";
    _lblPartSelectOrEnter.text=@"Part";
    [_txtpartName setHidden:YES];
    [_btnSelectParts setHidden:NO];
    [_txtUnitPrice setEnabled:NO];
    [_txtUnitPrice setEnabled:YES];
    
    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
}
- (IBAction)action_SaveParts:(id)sender {
    
    [self hideTextFields];
    if (isStandard) {
        
        if ([_btnSelectParts.titleLabel.text isEqualToString:@"----Select Part----"]) {
            
            [global AlertMethod:Alert :@"Please select part"];
            
        } else if((_txtQty.text.length==0) || (_txtQty.text.intValue==0)) {
            
            [global AlertMethod:Alert :@"Please enter quantity"];
            
        } else{
            
            if (!isEditParts) {
                
                [self addServiceIssuesRepairPartsFromMobileToDB];

            } else {
                
                [self updatePartsStandard];
                
            }
            
            [viewBackGround removeFromSuperview];

            [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:_strWorkOrderId];
            
        }

        
    } else {
        
        if (_txtpartName.text.length==0) {
            
            [global AlertMethod:Alert :@"Please enter part name"];
            
        } else if((_txtQty.text.length==0) || (_txtQty.text.intValue==0)) {
            
            [global AlertMethod:Alert :@"Please enter quantity"];
            
        } else{
            
            if (!isEditParts) {
                
                [self addServiceIssuesRepairPartsNonStandardFromMobileToDB];

            } else {

                [self updatePartsNonStandard];
                
            }
            
            [viewBackGround removeFromSuperview];
            
            [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:_strWorkOrderId];
        }

        
    }
    
}
- (IBAction)action_CancelParts:(id)sender {
    [self hideTextFields];
    [viewBackGround removeFromSuperview];
    
}
- (IBAction)action_SelectDate:(id)sender {
    [self hideTextFields];
    [self addPickerViewDateTo];
    
}
- (IBAction)action_SelectPart:(id)sender {
    [self hideTextFields];
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    [self getPartsMaster];
    
    if (arrDataTblView.count==0) {
        
        [global AlertMethod:Info :NoDataAvailable];
        
    }else{
        
        tblData.tag=102;
        [self tableLoad:tblData.tag];
        
    }
    
}
- (IBAction)action_SelectCategory:(id)sender {
    
    [self hideTextFields];
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];

    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
    
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];

        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
            
            [arrDataTblView addObject:dictDataa];
            
        }
    }
    
    if (arrDataTblView.count==0) {
        
        [global AlertMethod:Info :NoDataAvailable];
        
    }else{
        
        tblData.tag=101;
        [self tableLoad:tblData.tag];
        
    }
    
}
- (IBAction)action_RadioStandard:(id)sender {
    [self hideTextFields];
    isStandard=YES;
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    _txtpartName.text=@"";
    _txtQty.text=@"";
    _lblPartSelectOrEnter.text=@"Part";
    _txtUnitPrice.text=@"";
    strTxtUnitPrice=@"";
    [_txtpartName setHidden:YES];
    [_btnSelectParts setHidden:NO];
    [_txtUnitPrice setEnabled:NO];
    [_txtUnitPrice setEnabled:YES];
    _txtTotalPrice.text=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
    [_btnSelectParts setTitle:@"----Select Part----" forState:UIControlStateNormal];

    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
    // Unit Price($)*
    
    NSString *strInventory = [NSString stringWithFormat:@"Unit Price($)%@",@"*"];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strInventory];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(13,1)];
    _labelUnitPrice.attributedText = string;
    _txtUnitPrice.placeholder=@"Unit Price";

    [self setDefaultCategoryIfSingleCategoryExist];

}
- (IBAction)action_RadioNonStandard:(id)sender {
    
    strCategorySysNameSelected=@"";
    [self hideTextFields];
    isStandard=NO;
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    _txtpartName.text=@"";
    _txtUnitPrice.text=@"";
    _txtViewDesc.text=@"";
    strTxtUnitPrice=@"";
    _txtQty.text=@"";
    _lblPartSelectOrEnter.text=@"Part Name";
    [_txtpartName setHidden:NO];
    [_btnSelectParts setHidden:YES];
    [_txtUnitPrice setEnabled:YES];
   // [_txtQty setEnabled:YES];
    _txtTotalPrice.text=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];

    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
    NSString *strInventory = [NSString stringWithFormat:@"Vendor Price($)%@",@"*"];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strInventory];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(15,1)];
    _labelUnitPrice.attributedText = string;
    _txtUnitPrice.placeholder=@"Vendor Price";

    [self setDefaultCategoryIfSingleCategoryExist];

}

-(void)setDefaultNonStandardFirst{
    
    strCategorySysNameSelected=@"";
    [self hideTextFields];
    isStandard=NO;
    [_btnRadioNonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    _txtpartName.text=@"";
    _txtUnitPrice.text=@"";
    _txtViewDesc.text=@"";
    strTxtUnitPrice=@"";
    _txtQty.text=@"";
    _lblPartSelectOrEnter.text=@"Part Name";
    [_txtpartName setHidden:NO];
    [_btnSelectParts setHidden:YES];
    [_txtUnitPrice setEnabled:YES];
    // [_txtQty setEnabled:YES];
    _txtTotalPrice.text=@"";
    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
    
    // Vendor Details
    strVendorSysNameSelected = @"";
    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
    _txtFldVendoreQuoteNo.text = @"";
    _txtFldVendorPartNo.text = @"";
    
    NSString *strInventory = [NSString stringWithFormat:@"Vendor Price($)%@",@"*"];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strInventory];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(15,1)];
    _labelUnitPrice.attributedText = string;
    _txtUnitPrice.placeholder=@"Vendor Price";
    
    isChargeToCustomer=YES;
    [_btnChargeToCustomer setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];

}

- (IBAction)action_Warranty:(id)sender {
    [self hideTextFields];
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnWarranty imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        isWarranty=YES;
        [_btnWarranty setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
       // [self methodToCalculateLaborPrice:_txt_LaborHrs.text];
        
    }else{
        
        isWarranty=NO;
        [_btnWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
       // [self methodToCalculateLaborPrice:_txt_LaborHrs.text];
    }
    
}

- (IBAction)action_ChargeToCustomer:(id)sender{
    
    [self hideTextFields];
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnChargeToCustomer imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        isChargeToCustomer=YES;
        [_btnChargeToCustomer setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        isChargeToCustomer=NO;
        [_btnChargeToCustomer setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1) {
        
        return arrOfSubWorkServiceIssuesRepairParts.count;
        
    }else if(tableView.tag==102){
        
        return arrDataTblView.count;
        
    }else
        return arrDataTblView.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView.tag==1) {
        
        return tableView.rowHeight;
        
        
    }else if (tableView.tag==102){
        
        return 80;
        
    }else {
        
        return 80;
        
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==1) {
        
        AddPartsTableViewCell *cell = (AddPartsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AddPartsTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
        
    }else{
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        if (!(arrDataTblView.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 101:
                {
                    //btn select Category
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"Name"];
                    break;
                }
                case 102:
                {
                    //Select parts
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"Name"];
                    break;
                }// Vendor Details
                case 103:
                {
                    // VendorName List
                    cell.textLabel.text = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"Name"];
                    break;
                }
                default:
                    break;
            }
            
        }
        
        cell.textLabel.font=[UIFont systemFontOfSize:22];
        cell.textLabel.numberOfLines=2;
        
        return cell;
        
    }
    
}

- (void)configureCell:(AddPartsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *dictData =arrOfSubWorkServiceIssuesRepairParts[indexPath.row];
    
    NSString *strQTY=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"qty"]];
    NSString *strMultiplier=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"multiplier"]];
    NSString *strUnitPrice=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"unitPrice"]];
    
    float Total=[strQTY floatValue]*[strMultiplier floatValue]*[strUnitPrice floatValue];
    
    cell.lblQty.text=[NSString stringWithFormat:@"%@",strQTY];
    cell.lblDesc.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"partDesc"]];
    cell.lblParts.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"partName"]];
    cell.lblTotalPrice.text=[NSString stringWithFormat:@"%.02f",Total];

    NSString *strChargeToCustomer=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"isAddedAfterApproval"]];
    if ([strChargeToCustomer isEqualToString:@"false"] || [strChargeToCustomer isEqualToString:@"0"] || [strChargeToCustomer isEqualToString:@"False"]) {
        
        [cell.btnChargeToCustomer setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    } else {
        
        [cell.btnChargeToCustomer setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];

    }

    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==1) {
        
        
        
    } else {
        
        if (!(arrDataTblView.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 101:
                {
                    
                    strCategoryMasterId=@"";
                    [_btnSelectCategory setTitle:@"----Select Category----" forState:UIControlStateNormal];
                    [_btnSelectParts setTitle:@"----Select Part----" forState:UIControlStateNormal];
                    [_btnSelectdate setTitle:@"----Select Date----" forState:UIControlStateNormal];
                    _txtViewDesc.text=@"";
                    _txtQty.text=@"";
                    _txtModel.text=@"";
                    _txtSerial.text=@"";
                    _txtUnitPrice.text=@"";
                    strTxtUnitPrice=@"";
                    _txtTotalPrice.text=@"";
                    _txtManufacturer.text=@"";
                    _txtpartName.text=@"";
                    
                    // Vendor Details
                    strVendorSysNameSelected = @"";
                    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
                    _txtFldVendoreQuoteNo.text = @"";
                    _txtFldVendorPartNo.text = @"";
                    
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectCategory setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                    strCategoryMasterId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategoryMasterId"]];
                    strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
                    break;
                }
                case 102:
                {
                    
                    isChangeStdPartPrice=NO;
                    
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    [_btnSelectParts setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
                    [_txtQty setEnabled:YES];
                    
                    _txtViewDesc.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Description"]];

                    float unitPriceFloat=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BestPrice"]] floatValue];

                    _txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
                    strTxtUnitPrice=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
                    
                    dictDataPartsSelected=dictData;
                    
                    NSString *strType;
                    
                    if (isStandard) {
                        
                        strType=@"Standard";
                        
                    } else {
                        
                        strType=@"Non-Standard";
                        
                    }
                    
                    if (strCategorySysNameSelected.length==0) {
                        
                        strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
                        
                    }

                    NSString *strPartCate=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
                    
                    if (strPartCate.length==0) {
                        
                        strCategorySysNameSelected=@"";
                        
                    }else{
                        
                        strCategorySysNameSelected=strPartCate;
                        
                    }


                    NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",unitPriceFloat] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :strType];
                    
                    float multiplier=[strMultiplier floatValue];
                    
                    strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];

                    NSString *strTextQTY=[NSString stringWithFormat:@"%@",_txtQty.text];
                    
                    _txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",multiplier*[strTxtUnitPrice floatValue]];

                    if (strTextQTY.length>0) {
                        
                        //float totalPriceStandard=[strTextQTY floatValue]*multiplier*[_txtUnitPrice.text floatValue];
                        float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];

                        _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];

                    }

                    // Vendor Details
                    strVendorSysNameSelected = @"";
                    [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
                    itemMasterID = [[dictData valueForKey:@"ItemMasterId"] integerValue];

                    break;
                }// Vendor Details
                case 103:
                {
                    NSString *strVendorNameSelected = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"Name"];
                    strVendorSysNameSelected = [[arrDataTblView objectAtIndex:indexPath.row] valueForKey:@"SysName"];
                    [_btnVendorName setTitle:strVendorNameSelected forState:UIControlStateNormal];

                    /*
                    
                     // Vendor Details
                     strVendorSysNameSelected = @"";
                     [_btnVendorName setTitle:@"----Select Vendor----" forState:UIControlStateNormal];
                     _txtFldVendoreQuoteNo.text = @"";
                     _txtFldVendorPartNo.text = @"";
                     
                    */
                    
                    break;
                }
                default:
                    break;
            }
            
        }
        
        [viewBackGroundOnView removeFromSuperview];
        [tblData removeFromSuperview];
        
    }
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BOOL isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWoStatus];
    
    if (isCompletedStatusMechanical) {
        
        return false;
        
    }else
        
        return true;
}

-(NSArray*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    UITableViewRowAction *rowActionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"EDIT" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                           {
                                               isEditParts=YES;
                                               
                                               [self addPartView];
                                               
                                               NSManagedObject *dictData =arrOfSubWorkServiceIssuesRepairParts[indexPath.row];
                                               
                                               [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalViaId:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"issueRepairPartId"]]];
                                               
                                               [self setValuesOnEditingParts];
                                               
                                               
                                               
                                           }];
    UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"DELETE" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                             {
                                                 
                                                 UIAlertController * alert=   [UIAlertController
                                                                               alertControllerWithTitle:@"Alert!"
                                                                               message:@"Are you sure you want to delete"
                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                 
                                                 UIAlertAction* ok = [UIAlertAction
                                                                      actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action)
                                                                      {
                                                                          //Code
                                                                          
                                                                          NSManagedObject *dictData =arrOfSubWorkServiceIssuesRepairParts[indexPath.row];
                                                                          
                                                                          [self deletePartFromDB:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"issueRepairPartId"]]];
                                                                          
                                                                 
                                                                          [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:_strWorkOrderId];
                                                                          
                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                          
                                                                      }];
                                                 UIAlertAction* cancel = [UIAlertAction
                                                                          actionWithTitle:@"Cancel"
                                                                          style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action)
                                                                          {
                                                                              //Code
                                                                              
                                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                                              
                                                                          }];
                                                 
                                                 [alert addAction:ok];
                                                 [alert addAction:cancel];
                                                 
                                                 [self presentViewController:alert animated:YES completion:nil];
                                                 
                                                 
                                             }];
    
    rowActionDelete.backgroundColor=[UIColor redColor];
    rowActionEdit.backgroundColor=[UIColor grayColor];
    return @[rowActionDelete,rowActionEdit];
    // return @[rowActionDelete];
}



-(NSString*)logicForFetchingMultiplier :(NSString*)strPriceToLookUp :(NSString*)strCategorySysName :(NSString*)strType{
    
    NSString *strMultiplier;
    
    [self getPricLookupFromMaster:strCategorySysName];
    
    strMultiplier=[global logicForFetchingMultiplier:strPriceToLookUp :strCategorySysName :strType :arrOfPriceLookup];
    
    return strMultiplier;
}

//============================================================================
//============================================================================
#pragma mark - --------------------Dynamic Tableview METHODS-----------------
//============================================================================
//============================================================================


-(void)setTableFrame
{
    viewBackGroundOnView.tag=202;
    viewBackGroundOnView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundOnView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGroundOnView];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 500);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGroundOnView];
    [viewBackGroundOnView addSubview:tblData];
    
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 60)];
    
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    btnDone.titleLabel.font=[UIFont systemFontOfSize:25];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(methodDonePriorityTableview) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGroundOnView addSubview:btnDone];
    
    
    UIButton *btnAddNew=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.size.width/2+2+tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width/2-2, 60)];
    
    [btnAddNew setTitle:@"Cancel" forState:UIControlStateNormal];
    btnAddNew.titleLabel.font=[UIFont systemFontOfSize:25];
    [btnAddNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnAddNew.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [btnAddNew addTarget:self action:@selector(methodCancelPriorityTableview) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGroundOnView addSubview:btnAddNew];
    
}

-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }// Vendor Details
        case 103:
        {
            [self setTableFrame];
            break;
        }
        default:
            break;
    }
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}

-(void)methodDonePriorityTableview{
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
}

-(void)methodCancelPriorityTableview{
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //  [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strGlobalDateToShow.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strGlobalDateToShow];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGroundOnView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundOnView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGroundOnView];
    
    //============================================================================
    //============================================================================
    
 //   UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
  //  singleTap1.numberOfTapsRequired = 1;
  //  [viewBackGround setUserInteractionEnabled:YES];
  //  [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGroundOnView addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
   // btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGroundOnView removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    if (yesSameDay) {
        [_btnSelectdate setTitle:strDate forState:UIControlStateNormal];
        strGlobalDateToShow=strDate;
        [viewForDate removeFromSuperview];
        [viewBackGroundOnView removeFromSuperview];
    }
    else
    {
        
//        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
//
//        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending)
//        {
//
//            [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
//
//        }else
//        {
        
            [_btnSelectdate setTitle:strDate forState:UIControlStateNormal];
            strGlobalDateToShow=strDate;
            [viewForDate removeFromSuperview];
            [viewBackGroundOnView removeFromSuperview];
            
//        }
    }
}

//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

//============================================================================
//============================================================================
#pragma mark- ----------------Core Data Fetch Methods----------------
//============================================================================
//============================================================================

-(void)fetchSubWorkOrderIssuesRepairPartFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",_strWorkOrderId,_strSubWorkOrderId,_strSubWorkOrderIssuesIdToFetch,_strIssuePartsIdToFetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        [_tblViewParts reloadData];
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
//            NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"isActive"]];
//            
//            if ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) {
//                
//                [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
//                
//            }
            
            NSString *strIsAddedAfterApproval=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"isAddedAfterApproval"]];
            
            if ([strIsAddedAfterApproval isEqualToString:@"true"] || [strIsAddedAfterApproval isEqualToString:@"1"]) {

                // Before we were not adding it now we will show this also
                [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];

            }else{
                
                [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];

            }
            
        }
        
        [_tblViewParts reloadData];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}


-(void)fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanicalViaId :(NSString*)strID{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    //issueRepairLaborId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@ && issueRepairPartId = %@",_strWorkOrderId,_strSubWorkOrderId,_strSubWorkOrderIssuesIdToFetch,_strIssuePartsIdToFetch,strID];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    NSArray *arrTempParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrTempParts count] == 0)
    {
        
    }
    else
    {
        
        objPartsEdit=arrTempParts[0];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
        
        
    }
    
}
//============================================================================
//============================================================================
#pragma mark- ----------------Delete Core DB Methods----------------
//============================================================================
//============================================================================

-(void)deletePartFromDB :(NSString *)strID{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOTechHelperDcs Detail Data
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && issueRepairPartId = %@",_strWorkOrderId,_strSubWorkOrderId,_strIssuePartsIdToFetch,strID];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self fetchSubWorkOrderIssuesRepairPartFromDataBaseForMechanical];
    
}

//============================================================================
//============================================================================
#pragma mark- ----------------Update DB Methods----------------
//============================================================================
//============================================================================


-(void)addServiceIssuesRepairPartsFromMobileToDB{
    
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=_strWorkOrderId;
    objSubWorkOrderIssues.issueRepairPartId=[global getReferenceNumber];
    objSubWorkOrderIssues.subWorkOrderId=_strSubWorkOrderId;
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",_strIssuePartsIdToFetch];
    
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.installationDate=[NSString stringWithFormat:@"%@",_btnSelectdate.titleLabel.text];
    objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"false"];
    
    if (isChargeToCustomer) {
        
        // if is charge to customer mtllb charge krna mtlbb is added after approval false kyunki agr is added after approval hota hai to charge nhi krte hain customer ko
        objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"false"];
        
    } else {
        
        objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"true"];
        
    }
    
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"false"];
    
    NSString *strWarranty;
    if (isWarranty) {
        
        strWarranty=@"true";
        
    }else{
        
        strWarranty=@"false";
    }
    
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",strWarranty];
    objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",_txtManufacturer.text];
    objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",_txtModel.text];
    objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",strMultiplierGlobal];
    objSubWorkOrderIssues.partCode=[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"SysName"]];
    objSubWorkOrderIssues.partDesc=[NSString stringWithFormat:@"%@",_txtViewDesc.text];
    objSubWorkOrderIssues.partName=[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"Name"]];
    objSubWorkOrderIssues.partType=[NSString stringWithFormat:@"%@",@"Standard"];
    objSubWorkOrderIssues.qty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",_txtSerial.text];
    objSubWorkOrderIssues.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",_strSubWorkOrderIssuesIdToFetch];
   // objSubWorkOrderIssues.unitPrice=[NSString stringWithFormat:@"%@",_txtUnitPrice.text];
    objSubWorkOrderIssues.unitPrice=[NSString stringWithFormat:@"%@",strTxtUnitPrice];

    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    //New Added By Saavan For Checking Actual Quantity
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",_txtQty.text];

    if (isChangeStdPartPrice) {
        
        objSubWorkOrderIssues.isChangeStdPartPrice=[NSString stringWithFormat:@"%@",@"true"];
        objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",@"1"];
        
    }else{
        
        objSubWorkOrderIssues.isChangeStdPartPrice=[NSString stringWithFormat:@"%@",@"false"];
        
    }
    
    // Saving Vendor Details
    
    if([_btnVendorName.titleLabel.text isEqualToString:@"----Select Vendor----"]){
        
        objSubWorkOrderIssues.vendorName=@"";
        
    }else{
        
        objSubWorkOrderIssues.vendorName=strVendorSysNameSelected;
        
    }
    objSubWorkOrderIssues.vendorPartNo=_txtFldVendorPartNo.text;
    objSubWorkOrderIssues.vendorQuoteNo=_txtFldVendoreQuoteNo.text;
    objSubWorkOrderIssues.partCategorySysName=strCategorySysNameSelected;

    // End // Saving Vendor Details
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesRepairPartFromDataBaseForMechanical];
    
}
-(void)addServiceIssuesRepairPartsNonStandardFromMobileToDB{
    
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=_strWorkOrderId;
    objSubWorkOrderIssues.issueRepairPartId=[global getReferenceNumber];
    objSubWorkOrderIssues.subWorkOrderId=_strSubWorkOrderId;
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",_strIssuePartsIdToFetch];
    
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.installationDate=[NSString stringWithFormat:@"%@",_btnSelectdate.titleLabel.text];
    objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"false"];
    
    if (isChargeToCustomer) {
        
        // if is charge to customer mtllb charge krna mtlbb is added after approval false kyunki agr is added after approval hota hai to charge nhi krte hain customer ko
        objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"false"];
        
    } else {
        
        objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"true"];
        
    }
    
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"false"];
    
    NSString *strWarranty;
    if (isWarranty) {
        
        strWarranty=@"true";
        
    }else{
        
        strWarranty=@"false";
    }
    
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",strWarranty];
    objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",_txtManufacturer.text];
    objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",_txtModel.text];
    objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",strMultiplierGlobal];
    objSubWorkOrderIssues.partCode=@"";
    objSubWorkOrderIssues.partDesc=[NSString stringWithFormat:@"%@",_txtViewDesc.text];
    objSubWorkOrderIssues.partName=[NSString stringWithFormat:@"%@",_txtpartName.text];
    objSubWorkOrderIssues.partType=@"Non-Standard";
    objSubWorkOrderIssues.qty=[NSString stringWithFormat:@"%@",_txtQty.text];
    objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",_txtSerial.text];
    objSubWorkOrderIssues.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",_strSubWorkOrderIssuesIdToFetch];
    objSubWorkOrderIssues.unitPrice=[NSString stringWithFormat:@"%@",_txtUnitPrice.text];
    //objSubWorkOrderIssues.unitPrice=[NSString stringWithFormat:@"%@",strTxtUnitPrice];

    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    //New Added By Saavan For Checking Actual Quantity
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",_txtQty.text];
    
    // Saving Vendor Details
    
    if([_btnVendorName.titleLabel.text isEqualToString:@"----Select Vendor----"]){
        
        objSubWorkOrderIssues.vendorName=@"";
        
    }else{
        
        objSubWorkOrderIssues.vendorName=strVendorSysNameSelected;
        
    }
    objSubWorkOrderIssues.vendorPartNo=_txtFldVendorPartNo.text;
    objSubWorkOrderIssues.vendorQuoteNo=_txtFldVendoreQuoteNo.text;
    objSubWorkOrderIssues.partCategorySysName=strCategorySysNameSelected;

    // End // Saving Vendor Details
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesRepairPartFromDataBaseForMechanical];
    
}

-(void)updatePartsNonStandard{
    
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtQty.text] forKey:@"actualQty"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_btnSelectdate.titleLabel.text] forKey:@"installationDate"];
    
    NSString *strWarranty;
    if (isWarranty) {
        
        strWarranty=@"true";
        
    }else{
        
        strWarranty=@"false";
    }

    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",strWarranty] forKey:@"isWarranty"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtManufacturer.text] forKey:@"manufacturer"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtModel.text] forKey:@"modelNumber"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",strMultiplierGlobal] forKey:@"multiplier"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtViewDesc.text] forKey:@"partDesc"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtpartName.text] forKey:@"partName"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtQty.text] forKey:@"qty"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtSerial.text] forKey:@"serialNumber"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtUnitPrice.text] forKey:@"unitPrice"];
    //[objPartsEdit setValue:[NSString stringWithFormat:@"%@",strTxtUnitPrice] forKey:@"unitPrice"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",strUserName] forKey:@"modifiedBy"];
    [objPartsEdit setValue:[global strCurrentDate] forKey:@"modifiedDate"];
    
    // Saving Vendor Details
    
    if([_btnVendorName.titleLabel.text isEqualToString:@"----Select Vendor----"]){
        
        [objPartsEdit setValue:@"" forKey:@"vendorName"];
        
    }else{
        
        [objPartsEdit setValue:strVendorSysNameSelected forKey:@"vendorName"];
        
    }
    [objPartsEdit setValue:_txtFldVendorPartNo.text forKey:@"vendorPartNo"];
    [objPartsEdit setValue:_txtFldVendoreQuoteNo.text forKey:@"vendorQuoteNo"];
    
    // End // Saving Vendor Details
    
    if (isChargeToCustomer) {
        
        [objPartsEdit setValue:@"false" forKey:@"isAddedAfterApproval"];
        
    } else {
        
        [objPartsEdit setValue:@"true" forKey:@"isAddedAfterApproval"];
        
    }
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesRepairPartFromDataBaseForMechanical];
    
}
-(void)updatePartsStandard{
    
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtQty.text] forKey:@"actualQty"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_btnSelectdate.titleLabel.text] forKey:@"installationDate"];
    
    NSString *strWarranty;
    if (isWarranty) {
        
        strWarranty=@"true";
        
    }else{
        
        strWarranty=@"false";
    }
    
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",strWarranty] forKey:@"isWarranty"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtManufacturer.text] forKey:@"manufacturer"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtModel.text] forKey:@"modelNumber"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",strMultiplierGlobal] forKey:@"multiplier"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtViewDesc.text] forKey:@"partDesc"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtQty.text] forKey:@"qty"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtSerial.text] forKey:@"serialNumber"];
    //[objPartsEdit setValue:[NSString stringWithFormat:@"%@",_txtUnitPrice.text] forKey:@"unitPrice"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",strTxtUnitPrice] forKey:@"unitPrice"];
    [objPartsEdit setValue:[NSString stringWithFormat:@"%@",strUserName] forKey:@"modifiedBy"];
    [objPartsEdit setValue:[global strCurrentDate] forKey:@"modifiedDate"];
    
    if (dictDataPartsSelected==nil) {
        
    } else {
        
        [objPartsEdit setValue:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"SysName"]] forKey:@"partCode"];
        [objPartsEdit setValue:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"ItemType"]] forKey:@"partType"];
        [objPartsEdit setValue:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"Name"]] forKey:@"partName"];

    }

    if (isChangeStdPartPrice) {
        
        [objPartsEdit setValue:[NSString stringWithFormat:@"%@",@"1"] forKey:@"multiplier"];
        [objPartsEdit setValue:[NSString stringWithFormat:@"%@",@"true"] forKey:@"isChangeStdPartPrice"];
        
    }else{
        
        [objPartsEdit setValue:[NSString stringWithFormat:@"%@",@"false"] forKey:@"isChangeStdPartPrice"];
        
    }

    // Saving Vendor Details
    
    if([_btnVendorName.titleLabel.text isEqualToString:@"----Select Vendor----"]){
        
        [objPartsEdit setValue:@"" forKey:@"vendorName"];

    }else{
        
        [objPartsEdit setValue:strVendorSysNameSelected forKey:@"vendorName"];

    }
    [objPartsEdit setValue:_txtFldVendorPartNo.text forKey:@"vendorPartNo"];
    [objPartsEdit setValue:_txtFldVendoreQuoteNo.text forKey:@"vendorQuoteNo"];
    
    // End // Saving Vendor Details
    
    if (isChargeToCustomer) {
        
        [objPartsEdit setValue:@"false" forKey:@"isAddedAfterApproval"];
        
    } else {
        
        [objPartsEdit setValue:@"true" forKey:@"isAddedAfterApproval"];
        
    }
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesRepairPartFromDataBaseForMechanical];
    
}

//============================================================================
//============================================================================
#pragma mark TextField Delegate Method
//============================================================================
//============================================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    return  YES;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)texth
{
    if (range.location == 0 && [texth isEqualToString:@" "]) {
        return NO;
    }
    else
        return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }

    if (textField.tag==202) {
        
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        if (newLength>4) {
            
            return NO;
            
        }
        
    }
    if (textField.tag==203) {
        
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        if (newLength>10) {
            
            return NO;
            
        }
        
    }

    
    if (isStandard) {
        
        
        if (textField.tag==202) {
            
            NSScanner *scanner = [NSScanner scannerWithString:string];
            BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
            
            if (range.location == 0 && [string isEqualToString:@" "]) {
                return NO;
            }
            BOOL  isReturnTrue;
            
            isReturnTrue=YES;
            
            if (!isNumeric) {
                
                if ([string isEqualToString:@""]) {
                    
                    isReturnTrue=YES;
                    
                }else
                    
                    isReturnTrue=NO;
                
            }
            
            if (!isReturnTrue) {
                
                return NO;
                
            }

            
            if ([string isEqualToString:@""]) {
                
                NSString *strTextQTY;
                
                if ([textField.text length] > 0) {
                    
                    strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                    
                }

             //   NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Standard"];
                
                float multiplier=[strMultiplier floatValue];
                
                strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];

                //float totalPriceStandard=[strTextQTY floatValue]*multiplier*[_txtUnitPrice.text floatValue];
                
                if (isChangeStdPartPrice) {
                    
                    strMultiplierGlobal=@"1";
                    multiplier=[strMultiplierGlobal floatValue];
                }
                
                float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];

                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }else{
                
                NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"CategorySysName"]] :@"Standard"];
                
                float multiplier=[strMultiplier floatValue];
                
                strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];

                //float totalPriceStandard=[strTextQTY floatValue]*multiplier*[_txtUnitPrice.text floatValue];
                
                if (isChangeStdPartPrice) {
                    
                    strMultiplierGlobal=@"1";
                    multiplier=[strMultiplierGlobal floatValue];
                }

                float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }
        } else if (textField.tag==203) {
            
            isChangeStdPartPrice=YES;
            
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            {
                if (![string isEqualToString:@"."]) {
                    
                    return NO;
                    
                }
            }
            
            if ([string isEqualToString:@""]) {
                
                NSString *strTextQTY;
                
                if ([textField.text length] > 0) {
                    
                    strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                    
                }
                
                //strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                
                strMultiplierGlobal=@"1";
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtQty.text floatValue];
                
                strTxtUnitPrice=strTextQTY;
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }else{
                
                
                NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                // Logic For Two Digits After Decimals
                NSArray *sep = [strTextQTY componentsSeparatedByString:@"."];
                if([sep count] >= 2)
                {
                    NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                    if (!([sepStr length]>2)) {
                        if ([sepStr length]==2 && [string isEqualToString:@"."]) {
                            return NO;
                        }
                    }
                    else{
                        return NO;
                    }
                }
                //END Logic For Two Digits After Decimals
                
                
               // strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",[dictDataPartsSelected valueForKey:@"BestPrice"]] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                
                strMultiplierGlobal=@"1";
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtQty.text floatValue];
                
                strTxtUnitPrice=strTextQTY;
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }
            
        }
        
    } else {
        
        if (textField.tag==203) {
            
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            {
                if (![string isEqualToString:@"."]) {
                    
                    return NO;
                    
                }
            }
            
            if ([string isEqualToString:@""]) {
                
                NSString *strTextQTY;
                
                if ([textField.text length] > 0) {
                    
                    strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                    
                }
                
                strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",strTextQTY] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtQty.text floatValue];
                
                strTxtUnitPrice=strTextQTY;
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }else{
                
                
                NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                // Logic For Two Digits After Decimals
                NSArray *sep = [strTextQTY componentsSeparatedByString:@"."];
                if([sep count] >= 2)
                {
                    NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                    if (!([sepStr length]>2)) {
                        if ([sepStr length]==2 && [string isEqualToString:@"."]) {
                            return NO;
                        }
                    }
                    else{
                        return NO;
                    }
                }
                //END Logic For Two Digits After Decimals

                
                strMultiplierGlobal=[self logicForFetchingMultiplier:[NSString stringWithFormat:@"%@",strTextQTY] :[NSString stringWithFormat:@"%@",strCategorySysNameSelected] :@"Non-Standard"];
                
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtQty.text floatValue];
                
                strTxtUnitPrice=strTextQTY;
                
                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }


            
        } else if (textField.tag==202) {
            
            NSScanner *scanner = [NSScanner scannerWithString:string];
            BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
            
            if (range.location == 0 && [string isEqualToString:@" "]) {
                return NO;
            }
            BOOL  isReturnTrue;
            
            isReturnTrue=YES;
            
            if (!isNumeric) {
                
                if ([string isEqualToString:@""]) {
                    
                    isReturnTrue=YES;
                    
                }else
                    
                    isReturnTrue=NO;
                
            }
            
            if (!isReturnTrue) {
                
                return NO;
                
            }

            if ([string isEqualToString:@""]) {
                
                NSString *strTextQTY;
                
                if ([textField.text length] > 0) {
                    
                    strTextQTY = [textField.text substringToIndex:[textField.text length] - 1];
                    
                }
                
                //float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtUnitPrice.text floatValue];
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[strTxtUnitPrice floatValue];

                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }else{
                
                NSString *strTextQTY=[NSString stringWithFormat:@"%@%@",textField.text,string];
                
                //float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[_txtUnitPrice.text floatValue];
                float totalPriceStandard=[strTextQTY floatValue]*[strMultiplierGlobal floatValue]*[strTxtUnitPrice floatValue];

                _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
                
            }

        }
        
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    
}


/*
-(NSString*)logicForFetchingMultiplier :(NSString*)strPriceToLookUp{
    
    NSString *strMultiplier;
    
    for (int k=0; k<arrOfPriceLookup.count; k++) {
        
        NSDictionary *dictDataPrice=arrOfPriceLookup[k];
        
        NSString *strFrom=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"RangeFrom"]];
        NSString *strTo=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"RangeTo"]];
        
        int c=[strPriceToLookUp intValue];
        
        int a=[strFrom intValue];
        int b=[strTo intValue];
        
        if(c >= a && c <= b){
            
            strMultiplier=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"Multiplier"]];
            
            break;
            
        }
        
    }
    if (strMultiplier.length==0) {
        
        strMultiplier=@"1";
        
    }
    
    return strMultiplier;
}
*/


//============================================================================
#pragma mark- ------------Scan Part------------------
//============================================================================

-(void)methodOpenScannerView{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"" forKey:@"ScannedResult"];
    [defs setBool:YES forKey:@"IsScannerView"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    ScannerViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ScannerViewController"];
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}

- (IBAction)action_ScanBarcode:(id)sender {
    
    [_viewAddParts addSubview:_scannerPreviewView];
    
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            [self startScanning];
        } else {
            [self displayPermissionMissingAlert];
        }
    }];
    
}


-(void)setpartValuesAfterScanning :(NSString *)strScannedResult{
    
    BOOL isPartPresent;
    
    isPartPresent=NO;
    isChangeStdPartPrice=NO;
    
    [self getPartsMaster];
    
    NSDictionary *dictData;
    
    for (int k=0; k<arrDataTblView.count; k++) {
        
        dictData=[arrDataTblView objectAtIndex:k];
        
        NSString *strItemNumber=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ItemNumber"]];
        
        if ([strScannedResult caseInsensitiveCompare:strItemNumber] == NSOrderedSame) {
            isPartPresent=YES;
            break;
        }
    }
    
    if (isPartPresent) {
        
        [_btnSelectParts setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        [_txtQty setEnabled:YES];
        
        _txtViewDesc.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Description"]];
        
        float unitPriceFloat=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BestPrice"]] floatValue];
        
        _txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
        strTxtUnitPrice=[NSString stringWithFormat:@"%.02f",unitPriceFloat];
        
        dictDataPartsSelected=dictData;
        
        NSString *strType;
        
        if (isStandard) {
            
            strType=@"Standard";
            
        } else {
            
            strType=@"Non-Standard";
            
        }
        
        if (strCategorySysNameSelected.length==0) {
            
            strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
            
        }
        
        NSString *strPartCate=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
        
        if (strPartCate.length==0) {
            
            strCategorySysNameSelected=@"";
            
        }else{
            
            strCategorySysNameSelected=strPartCate;
            
            NSArray *arrOfAreaMaster=[dictDetailsMastersMechanical valueForKey:@"InventoryCategoryMaster"];
            
            for (int k=0; k<arrOfAreaMaster.count; k++) {
                
                NSDictionary *dictDataa=arrOfAreaMaster[k];
                NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
                
                NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
                NSString *strCategorySysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
                
                if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
                    
                    if ([strCategorySysName isEqualToString:strCategorySysNameSelected]) {
                        
                        [_btnSelectCategory setTitle:[dictDataa valueForKey:@"Name"] forState:UIControlStateNormal];
                        strCategoryMasterId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryMasterId"]];
                        
                    }
                    
                }
            }
            
        }
        
        NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",unitPriceFloat] :strCategorySysNameSelected :strType];
        
        float multiplier=[strMultiplier floatValue];
        
        strMultiplierGlobal=[NSString stringWithFormat:@"%@",strMultiplier];
        
        NSString *strTextQTY=[NSString stringWithFormat:@"%@",_txtQty.text];
        
        _txtUnitPrice.text=[NSString stringWithFormat:@"%.02f",multiplier*[strTxtUnitPrice floatValue]];
        
        if (strTextQTY.length>0) {
            
            float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];
            
            _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
            
        }else{
            
            _txtQty.text=@"1";
            
            strTextQTY=[NSString stringWithFormat:@"%@",_txtQty.text];
            
            float totalPriceStandard=[strTextQTY floatValue]*multiplier*[strTxtUnitPrice floatValue];
            
            _txtTotalPrice.text=[NSString stringWithFormat:@"%.02f",totalPriceStandard];
            
            
        }
        
    } else {
        
        [global AlertMethod:Alert :@"Part not available"];
        
    }
    
}


#pragma mark - Scanner Methods

- (MTBBarcodeScanner *)scanner {
    if (!_scanner) {
        _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_scannerPreviewView];
    }
    return _scanner;
}

#pragma mark - Scanning

- (void)startScanning {
    
    // self.uniqueCodes = [[NSMutableArray alloc] init];
    
    NSError *error = nil;
    [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
        for (AVMetadataMachineReadableCodeObject *code in codes) {
            
            //            UIAlertView *Alet=[[UIAlertView alloc]initWithTitle:@"Scanned Result" message:code.stringValue delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [Alet show];
            
            [self stopScanning];
            
            [_scannerPreviewView removeFromSuperview];
            
            [self setpartValuesAfterScanning:code.stringValue];
            
        }
    } error:&error];
    
    if (error) {
        NSLog(@"An error occurred: %@", error.localizedDescription);
    }
    
    [self.toggleScanningButton setTitle:@"Stop Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = [UIColor redColor];
}
- (void)stopScanning {
    [self.scanner stopScanning];
    
    [self.toggleScanningButton setTitle:@"Start Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = self.view.tintColor;
    
    self.captureIsFrozen = NO;
}

#pragma mark - Actions

- (IBAction)toggleScanningTapped:(id)sender {
    if ([self.scanner isScanning] || self.captureIsFrozen) {
        [self stopScanning];
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
    } else {
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
    }
}

- (IBAction)switchCameraTapped:(id)sender {
    [self.scanner flipCamera];
}

- (IBAction)toggleTorchTapped:(id)sender {
    if (self.scanner.torchMode == MTBTorchModeOff) {
        self.scanner.torchMode = MTBTorchModeOn;
        self.toggleTorchButton.titleLabel.text = @"Disable Torch";
    } else {
        self.scanner.torchMode = MTBTorchModeOff;
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
    }
}

#pragma mark - Gesture Handlers

- (void)previewTapped {
    if (![self.scanner isScanning] && !self.captureIsFrozen) {
        return;
    }
    
    if (!self.didShowCaptureWarning) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Capture Frozen" message:@"The capture is now frozen. Tap the preview again to unfreeze." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
        
        self.didShowCaptureWarning = YES;
    }
    
    if (self.captureIsFrozen) {
        [self.scanner unfreezeCapture];
    } else {
        [self.scanner freezeCapture];
    }
    
    self.captureIsFrozen = !self.captureIsFrozen;
}

#pragma mark - Setters

- (void)setUniqueCodes:(NSMutableArray *)uniqueCodes {
    // _uniqueCodes = uniqueCodes;
}

#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Scanning Unavaialble" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)action_OpenScannerView:(id)sender {
    
    BOOL isNetReachable=[global isNetReachable];
    
    if (isNetReachable) {
        
        [_viewAddParts addSubview:_scannerPreviewView];
        
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
        
    } else {
        
        [global AlertMethod:Alert :@"Please check your internet connection, this functionality requires internet connectivity."];
        
    }
    
}

- (IBAction)action_CancelScanning:(id)sender {
    
    [self stopScanning];
    [_scannerPreviewView removeFromSuperview];
    
}

- (IBAction)action_TorchMode:(id)sender {
    
    if (self.scanner.torchMode == MTBTorchModeOff) {
        self.scanner.torchMode = MTBTorchModeOn;
        self.toggleTorchButton.titleLabel.text = @"Disable Torch";
        [_btnTorch setTitle:@"Disable Torch" forState:UIControlStateNormal];
    } else {
        self.scanner.torchMode = MTBTorchModeOff;
        self.toggleTorchButton.titleLabel.text = @"Enable Torch";
        [_btnTorch setTitle:@"Enable Torch" forState:UIControlStateNormal];
    }
    
}

// Vendor Details
- (IBAction)action_VendorName:(id)sender {
    
    [self hideTextFields];
    [self methodTableViewAllocation];
    
    [viewBackGroundOnView removeFromSuperview];
    [tblData removeFromSuperview];
    
    NSUserDefaults *defsVendorName=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMechanicalMasters1=[defsVendorName valueForKey:@"MasterAllMechanical"];
    NSArray *arrayItemVendorMapping = [dictMechanicalMasters1 valueForKey:@"ItemVendorMappingExtSerDc"];
    NSArray *arrayVendor=[dictMechanicalMasters1 valueForKey:@"VendorBasicExtSerDc"];

    NSMutableArray *arrayTempVendor = [NSMutableArray new];
    
    if (isStandard) {
        
        for(NSDictionary *dict in arrayItemVendorMapping)
        {
            if([[dict valueForKey:@"ItemMasterId"] integerValue]==itemMasterID)
            {
                for(NSDictionary *dictTempVendor in arrayVendor)
                {
                    if ([[dict valueForKey:@"VendorMasterId"] integerValue]==[[dictTempVendor valueForKey:@"VendorMasterId"] integerValue])
                    {
                        [arrayTempVendor addObject:dictTempVendor];
                    }
                }
            }
        }
        
    } else {
        
        arrayTempVendor = [arrayVendor mutableCopy];
        
    }
    

    
    if(arrayTempVendor.count>0)
    {
        arrDataTblView = [arrayTempVendor mutableCopy];
        tblData.tag=103;
        [self tableLoad:tblData.tag];
    }
    else
    {
        [global AlertMethod:Info :@"No Data Available.\n1. Please Either select part. OR \n2. Please check either Data is set on Web, if set on Web check your internet connection and Sync Masters from Menu options."];
    }
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    [_viewAddParts setFrame:CGRectMake(_viewAddParts.frame.origin.x, _viewAddParts.frame.origin.y-150, _viewAddParts.frame.size.width, _viewAddParts.frame.size.height)];
    
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [_viewAddParts setFrame:CGRectMake(_viewAddParts.frame.origin.x, _viewAddParts.frame.origin.y+150, _viewAddParts.frame.size.width, _viewAddParts.frame.size.height)];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------Method Set Default Category-----------------
//============================================================================
//============================================================================

-(void)setDefaultCategoryIfSingleCategoryExist{
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
    
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        
        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
            
            [arrDataTblView addObject:dictDataa];
            
        }
    }
    
    if (arrDataTblView.count==1) {
        
        NSDictionary *dictData=[arrDataTblView objectAtIndex:0];
        [_btnSelectCategory setTitle:[dictData valueForKey:@"Name"] forState:UIControlStateNormal];
        strCategoryMasterId=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategoryMasterId"]];
        strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        
    }
    
}


//changes for showing selected category

-(void)showSelectedPartCategory :(NSString*)strpartCategorySysName{
    
    NSString *strCategorySysNameSelectedLocal=[NSString stringWithFormat:@"%@",strpartCategorySysName];
    
    NSArray *arrOfAreaMaster=[dictDetailsFortblView valueForKey:@"InventoryCategoryMaster"];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        NSString *strCategorySysNameNew=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
        
        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartmentSysName isEqualToString:strDepartmentSysName])) {
            
            if ([strCategorySysNameNew isEqualToString:strCategorySysNameSelectedLocal]) {
                
                [_btnSelectCategory setTitle:[dictDataa valueForKey:@"Name"] forState:UIControlStateNormal];
                strCategoryMasterId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryMasterId"]];
                strCategorySysNameSelected=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]];
                
            }
            
        }
    }
    
}
@end
