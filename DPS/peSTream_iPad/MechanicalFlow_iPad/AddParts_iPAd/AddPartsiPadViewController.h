//
//  AddPartsiPadViewController.h
//  DPS
//  peSTream
//  Created by Saavan Patidar on 05/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  asdfasdfsad

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"


@interface AddPartsiPadViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entitySubWorkOrderIssues,*entityMechanicalSubWorkOrderHelper,*entityMechanicalSubWorkOrderNotes,*entityMechanicalSubWorkOrderActualHrs,*entitySubWorkOrderIssuesRepair,*entitySubWorkOrderIssuesRepairParts,*entitySubWorkOrderIssuesRepairLabour;
    
    
    NSManagedObjectContext *context;
    
    
    NSFetchRequest *requestSubWorkOrderIssues,*requestSubWorkOrderHelper,*requestSubWorkOrderNotes,*requestSubWorkOrderActualHrs,*requestSubWorkOrderIssuesRepair,*requestSubWorkOrderIssuesRepairParts,*requestSubWorkOrderIssuesRepairLabour;
    
    
    NSSortDescriptor *sortDescriptorSubWorkOrderIssues,*sortDescriptorSubWorkOrderHelper,*sortDescriptorSubWorkOrderNotes,*sortDescriptorSubWorkOrderActualHrs,*sortDescriptorSubWorkOrderIssuesRepair,*sortDescriptorSubWorkOrderIssuesRepairParts,*sortDescriptorSubWorkOrderIssuesRepairLabour;
    
    
    NSArray *sortDescriptorsSubWorkOrderIssues,*sortDescriptorsSubWorkOrderHelper,*sortDescriptorsSubWorkOrderNotes,*sortDescriptorsSubWorkOrderActualHrs,*sortDescriptorsSubWorkOrderIssuesRepair,*sortDescriptorsSubWorkOrderIssuesRepairParts,*sortDescriptorsSubWorkOrderIssuesRepairLabour;
    
    
    NSManagedObject *matchesSubWorkOrderIssues,*matchesSubWorkOrderHelper,*matchesSubWorkOrderNotes,*matchesSubWorkOrderActualHrs,*matchesSubWorkOrderIssuesRepair,*matchesSubWorkOrderIssuesRepairParts,*matchesSubWorkOrderIssuesRepairLabour;
    
    
    NSArray *arrAllObjSubWorkOrderIssues,*arrAllObjSubWorkOrderHelper,*arrAllObjSubWorkOrderNotes,*arrAllObjSubWorkOrderActualHrs,*arrAllObjSubWorkOrderIssuesRepair,*arrAllObjSubWorkOrderIssuesRepairParts,*arrAllObjSubWorkOrderIssuesRepairLabour;

}
@property (strong, nonatomic) IBOutlet UITableView *tblViewParts;
- (IBAction)action_Back:(id)sender;
- (IBAction)action_Parts:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAccNoDetails;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderdetails;
@property (strong, nonatomic) NSString *strSubWorkOrderId;
@property (strong, nonatomic) NSString *strWorkOrderId;
@property (strong, nonatomic) NSString *strIssuePartsIdToFetch;
@property (strong, nonatomic) NSString *strSubWorkOrderIssuesIdToFetch;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairParts;
@property (strong, nonatomic) NSManagedObject *objWorkOrderdetails;

- (IBAction)action_ScanBarcode:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *scannerPreviewView;
- (IBAction)action_OpenScannerView:(id)sender;
- (IBAction)action_CancelScanning:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnTorch;
- (IBAction)action_TorchMode:(id)sender;

//Vendor Details
@property (strong, nonatomic) IBOutlet UIButton *btnVendorName;
- (IBAction)action_VendorName:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtFldVendorPartNo;
@property (strong, nonatomic) IBOutlet UITextField *txtFldVendoreQuoteNo;

@property (strong, nonatomic) IBOutlet UILabel *labelUnitPrice;

@property (strong, nonatomic) IBOutlet UIButton *btnChargeToCustomer;
- (IBAction)action_ChargeToCustomer:(id)sender;

@end
