//
//  MechanicalClientApprovalTableViewCell.h
//  DPS
//  peSTream
//  Created by Rakesh Jain on 31/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  jhadsfjkhasdkjfh

#import <UIKit/UIKit.h>

@interface MechanicalClientApprovalTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblValueForRepair;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForLR;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForHLR;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForPart;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnOption1;
@property (weak, nonatomic) IBOutlet UILabel *lblOption1;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmountTotal;


@property (weak, nonatomic) IBOutlet UILabel *lblValueForDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnApprove;
@property (weak, nonatomic) IBOutlet UIButton *btnDecline;
@property (weak, nonatomic) IBOutlet UILabel *lblIssueId;

@property (weak, nonatomic) IBOutlet UILabel *lblValueForLR2;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForHLR2;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmount2;
@property (weak, nonatomic) IBOutlet UIButton *btnOption2;
@property (weak, nonatomic) IBOutlet UILabel *lblOption2;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForPart2;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmount2Total;


@property (weak, nonatomic) IBOutlet UIButton *btnOption3;
@property (weak, nonatomic) IBOutlet UILabel *lblOption3;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForLR3;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForHLR3;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmount3;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForPart3;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmount3Total;

@property (strong, nonatomic) IBOutlet UILabel *lblValueForDesc1;
@property (strong, nonatomic) IBOutlet UILabel *lblValueForDesc2;
@property (strong, nonatomic) IBOutlet UILabel *lblValueForDesc3;

@property (strong, nonatomic) IBOutlet UILabel *lblOptionAmt1;
@property (strong, nonatomic) IBOutlet UILabel *lblOptionAmt2;
@property (strong, nonatomic) IBOutlet UILabel *lblOptionDesc2;
@property (strong, nonatomic) IBOutlet UILabel *lblOptionDesc3;
@property (strong, nonatomic) IBOutlet UILabel *lblLineSeprator;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *lblLineSeprator1;
@property (strong, nonatomic) IBOutlet UILabel *lblLineSeprator3;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMoreGood;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMoreBetter;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMoreBest;
@property (weak, nonatomic) IBOutlet UITextField *txtFldQty;

@property (strong, nonatomic) IBOutlet UILabel *lblOptionAmt1Total;
@property (strong, nonatomic) IBOutlet UILabel *lblOptionAmt2Total;

@end
