//
//  PspPlans+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 04/06/19.
//
//

#import "PspPlans+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PspPlans (CoreDataProperties)

+ (NSFetchRequest<PspPlans *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *arrOfPlans;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *empId;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *accountNo;

@end

NS_ASSUME_NONNULL_END
