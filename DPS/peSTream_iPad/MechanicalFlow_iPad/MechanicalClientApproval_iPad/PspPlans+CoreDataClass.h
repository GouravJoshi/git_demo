//
//  PspPlans+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 04/06/19.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface PspPlans : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "PspPlans+CoreDataProperties.h"
