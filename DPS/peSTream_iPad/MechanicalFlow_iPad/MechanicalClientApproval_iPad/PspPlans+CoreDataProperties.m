//
//  PspPlans+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 04/06/19.
//
//

#import "PspPlans+CoreDataProperties.h"

@implementation PspPlans (CoreDataProperties)

+ (NSFetchRequest<PspPlans *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"PspPlans"];
}

@dynamic arrOfPlans;
@dynamic companyKey;
@dynamic empId;
@dynamic userName;
@dynamic accountNo;

@end
