//
//  MechanicalClientApprovalViewController.h
//  DPS
//  peSTream
//  Created by Saavan Patidar on 31/05/17.
//  Copyright © 2017 Saavan. All rights reserved.///
//  peSTream pestReam

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface MechanicalClientApprovalViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,NSFetchedResultsControllerDelegate,UITextFieldDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entitySubWorkOrderIssues,*entityMechanicalSubWorkOrderHelper,*entityMechanicalSubWorkOrderNotes,*entityMechanicalSubWorkOrderActualHrs,*entitySubWorkOrderIssuesRepair,*entitySubWorkOrderIssuesRepairParts,*entitySubWorkOrderIssuesRepairLabour,*entityPaymentInfoForMechanicalServiceAuto,*entityWorkOrder,*entityPaymentInfoServiceAuto,*entityAccountDescription,*entityWorkOrderAppliedDiscounts;
    
    
    NSManagedObjectContext *context;
    
    NSFetchRequest *requestSubWorkOrderIssues,*requestSubWorkOrderHelper,*requestSubWorkOrderNotes,*requestSubWorkOrderActualHrs,*requestSubWorkOrderIssuesRepair,*requestSubWorkOrderIssuesRepairParts,*requestSubWorkOrderIssuesRepairLabour,*requestPaymentInfo,*requestNewWorkOrder,*requestAccDescription,*requestWorkOrderAppliedDiscount;
    
    
    NSSortDescriptor *sortDescriptorSubWorkOrderIssues,*sortDescriptorSubWorkOrderHelper,*sortDescriptorSubWorkOrderNotes,*sortDescriptorSubWorkOrderActualHrs,*sortDescriptorSubWorkOrderIssuesRepair,*sortDescriptorSubWorkOrderIssuesRepairParts,*sortDescriptorSubWorkOrderIssuesRepairLabour,*sortDescriptorPaymentInfo,*sortDescriptorWorkOrder,*sortDescriptorAccDiscount,*sortDescriptorWorkOrderAppliedDiscount;
    
    
    NSArray *sortDescriptorsSubWorkOrderIssues,*sortDescriptorsSubWorkOrderHelper,*sortDescriptorsSubWorkOrderNotes,*sortDescriptorsSubWorkOrderActualHrs,*sortDescriptorsSubWorkOrderIssuesRepair,*sortDescriptorsSubWorkOrderIssuesRepairParts,*sortDescriptorsSubWorkOrderIssuesRepairLabour,*sortDescriptorsPaymentInfo,*sortDescriptorsWorkOrder,*sortDescriptorsAccDiscount,*sortDescriptorsWorkOrderAppliedDiscount;
    
    
    NSManagedObject *matchesSubWorkOrderIssues,*matchesSubWorkOrderHelper,*matchesSubWorkOrderNotes,*matchesSubWorkOrderActualHrs,*matchesSubWorkOrderIssuesRepair,*matchesSubWorkOrderIssuesRepairParts,*matchesSubWorkOrderIssuesRepairLabour;
    
    
    NSArray *arrAllObjSubWorkOrderIssues,*arrAllObjSubWorkOrderHelper,*arrAllObjSubWorkOrderNotes,*arrAllObjSubWorkOrderActualHrs,*arrAllObjSubWorkOrderIssuesRepair,*arrAllObjSubWorkOrderIssuesRepairParts,*arrAllObjSubWorkOrderIssuesRepairLabour;
    
    NSEntityDescription *entityPspPlans;
    NSFetchRequest *requestPspPlans;
    NSSortDescriptor *sortDescriptorPspPlans;
    NSArray *sortDescriptorsPspPlans;
    NSManagedObject *matchesPspPlans;
    NSArray *arrAllObjPspPlans;
    
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerPspPlans;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

//View Sign
@property (strong, nonatomic) IBOutlet UIView *viewForSign;
@property (weak, nonatomic) IBOutlet UIImageView *imgSignCheckBox;
@property (weak, nonatomic) IBOutlet UIView *viewInspectorSign;
@property (weak, nonatomic) IBOutlet UIImageView *imgInspectorSign;
@property (weak, nonatomic) IBOutlet UIButton *btnInspectorSign;
- (IBAction)actionOnCustomerNotPresent:(id)sender;
- (IBAction)actionOnInspectorSign:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewCustomerSign;
@property (weak, nonatomic) IBOutlet UIImageView *imgCustomerSign;
@property (weak, nonatomic) IBOutlet UIButton *btnCustomerSign;
- (IBAction)actionOnCustomerSign:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
- (IBAction)actionOnSubmit:(id)sender;

//ViewPayment Details
@property (strong, nonatomic) IBOutlet UIView *viewForPaymentDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblNoMembershipPlan;

@property (weak, nonatomic) IBOutlet UILabel *lblValueTotalApprovedRepairs;

@property (weak, nonatomic) IBOutlet UITextField *txtOtherDiscounts;
@property (weak, nonatomic) IBOutlet UIButton *btnDiagnosticCharge;

- (IBAction)actionOnDiagnosticCharge:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTipCharge;
- (IBAction)actionOnTipCharge:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForDiagnosticeCharge;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTipCharge;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForSubtotal;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTax;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmountDue;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmountPaid;
@property (weak, nonatomic) IBOutlet UILabel *lblAmountPaid;

@property (weak, nonatomic) IBOutlet UILabel *lblValueForTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForMembershipPlanCharges;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForMembershipPlanSavings;


//View Other PaymentDetails
@property (strong, nonatomic) IBOutlet UIView *viewForOtherPaymentDetail;

//View Service Issue
@property (strong, nonatomic) IBOutlet UIView *viewForServiceIssue;
@property (weak, nonatomic) IBOutlet UITableView *tblViewServiceIssue;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Table_ViewIssue_H;

//View ServiceParts
@property (strong, nonatomic) IBOutlet UIView *viewForServiceParts;

@property (weak, nonatomic) IBOutlet UITableView *tblIssueParts;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableParts_H;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)actionBack:(id)sender;

//View Payment Mode
@property (strong, nonatomic) IBOutlet UIView *viewForPaymentMode;
- (IBAction)actionOnCash:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCash;
- (IBAction)actionOnCheck:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
- (IBAction)actionOnCreditCard:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCreditCard;
- (IBAction)actionOnBill:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBill;
- (IBAction)actionOnAutoChargeCustomer:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAutoChargeCustomer;
- (IBAction)actionOnPaymentPending:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPaymentPending;
- (IBAction)actionOnNoChange:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNoChange;

//View For CheckDetail
@property (strong, nonatomic) IBOutlet UIView *viewForCheckDetail;
@property (weak, nonatomic) IBOutlet UITextField *txtAmountCheckView;
@property (weak, nonatomic) IBOutlet UITextField *txtCheckNo;

@property (weak, nonatomic) IBOutlet UITextField *txtDrivingLicenseNo;
@property (weak, nonatomic) IBOutlet UIButton *btnExpirationDate;
- (IBAction)actionOnExpirationDate:(id)sender;
- (IBAction)actionOnCheckFrontImage:(id)sender;
- (IBAction)actionOnCheckBackImage:(id)sender;

//View For Single Amount
@property (strong, nonatomic) IBOutlet UIView *viewForSingleAmount;
@property (weak, nonatomic) IBOutlet UITextField *txtAmountSingleAmount;

//Nilind 12th June
@property(strong,nonatomic)NSString *strWorlOrderId,*strSubWorkOderId,*strWoType;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssues;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderHelper;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderNotes;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderActualHrs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepair;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairParts;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairLabour;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerPaymentInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
//View For Agreement
@property (strong, nonatomic) IBOutlet UIView *viewForAgreement;
@property (weak, nonatomic) IBOutlet UITextField *txtAgreementCost;
- (IBAction)actionOnSplitPrice:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxSplitPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtLaborPercent;
@property (weak, nonatomic) IBOutlet UITextField *txtPartPercent;
@property (weak, nonatomic) IBOutlet UIView *viewForLaborPartPercent;

//View For New Plan
@property (strong, nonatomic) IBOutlet UIView *viewForNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMembershipPlan;
@property (weak, nonatomic) IBOutlet UIView *viewOld;
@property (weak, nonatomic) IBOutlet UIView *viewNew;

@property (weak, nonatomic) IBOutlet UILabel *lblValueForTotalAmountNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForMembershipChargeNewPaln;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForMembershipSavingNewPlan;
@property (weak, nonatomic) IBOutlet UITextField *txtOtherDiscountNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForDiagnosticChargeNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTripChargeNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForSubtotalNewPlan;
@property (weak, nonatomic) IBOutlet UIButton *btnDiagnosticChargeNewPlan;
@property (weak, nonatomic) IBOutlet UIButton *btnTripChargeNewPlan;
- (IBAction)actionOnTripChargeNewPlan:(id)sender;
- (IBAction)actionOnDiagnosticChargeNewPlan:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblValueForTaxNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForTotalNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmountPaidNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAmountDueNewPlan;
//View For Plan Status
@property (strong, nonatomic) IBOutlet UIView *viewForPlanStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnNoMembership;
- (IBAction)actionOnNoMembership:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNewPlan;
- (IBAction)actionOnNewPlan:(id)sender;
- (IBAction)actionOnSelectPlan:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectPlan;

@property (strong, nonatomic) IBOutlet UIView *viewForAdditionalInfo;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAdditionalInfo;
@property (strong, nonatomic) IBOutlet UIView *view_TermsnConditions;
@property (strong, nonatomic) IBOutlet UITextView *txtViewTermsnConditions;

//Mileage Outlets

@property (weak, nonatomic) IBOutlet UIButton *btnMileageChargeNewPlan;
@property (weak, nonatomic) IBOutlet UIButton *btnMileageCharge;
- (IBAction)actionOnMileageChargeNewPlan:(id)sender;
- (IBAction)actionOnMileageCharge:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblMileageNewPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblMileage;
@property (weak, nonatomic) IBOutlet UITextField *txtMilesNewPlan;
@property (weak, nonatomic) IBOutlet UITextField *txtMiles;
@property (strong, nonatomic) IBOutlet UITextField *txtMiscellaneousCharge;
@property (strong, nonatomic) IBOutlet UITextField *txtMiscellaneousChargeNewPlan;


// Akshay  Start //
@property (strong, nonatomic) IBOutlet UIView *viewCredit;
@property (strong, nonatomic) IBOutlet UIView *viewPartLaborCreditApply;
@property (weak, nonatomic) IBOutlet UIButton *buttonLaborPrice;
@property (weak, nonatomic) IBOutlet UIButton *buttonPartPrice;


@property (weak, nonatomic) IBOutlet UIButton *buttonSelectCredit_Apply;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectCredit_Add;
@property (weak, nonatomic) IBOutlet UITableView *tableViewCredit;
@property (weak, nonatomic) IBOutlet UIView *viewSubTotal;
@property (weak, nonatomic) IBOutlet UILabel *labelSubTotal;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerAccountDiscount;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderAppliedDiscount;

@property (weak, nonatomic) IBOutlet UITableView *tableViewCreditPaymntDetailLeft
;
@property (weak, nonatomic) IBOutlet UITableView *tableViewCreditPaymntDetailRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightPaymentViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightPaymentViewRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTblViewPaymntDetailLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTblViewPaymntDetailRight;

@property (weak, nonatomic) IBOutlet UILabel *labelCreditsPaymentDetailViewLeft;
@property (weak, nonatomic) IBOutlet UILabel *labelCreditsPaymentDetailViewRight;

// Akshay End //

@property (strong, nonatomic) IBOutlet UITextField *txtFld_TripChargeQty;
@property (strong, nonatomic) IBOutlet UITextField *txtFld_TripChargeQtyNewPlan;

@property (weak, nonatomic) IBOutlet UILabel *lblTripChargeBasedOnQty;
@property (weak, nonatomic) IBOutlet UILabel *lblTripChargeBasedOnQtyNewPlan;

@end
