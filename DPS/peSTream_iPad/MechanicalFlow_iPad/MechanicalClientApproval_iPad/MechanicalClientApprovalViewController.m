//
//  MechanicalClientApprovalViewController.m
//  DPS
//  peSTream
//  Created by Saavan Patidar on 31/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//


#import "AllImportsViewController.h"
#import "MechanicalIssuePartsTableViewCell.h"
#import "MechanicalClientApprovalViewController.h"
#import "MechanicalClientApprovalTableViewCell.h"
#import "PaymentInfoServiceAuto.h"
#import "PaymentInfoServiceAuto+CoreDataProperties.h"
#import "PspPlans+CoreDataProperties.h"
#import "PspPlans+CoreDataClass.h"


// Akshay Start //

#import "AccountDiscountExtSerDcs+CoreDataProperties.h"
#import "AccountDiscountExtSerDcs+CoreDataClass.h"
#import "WorkOrderAppliedDiscountExtSerDcs+CoreDataProperties.h"
#import "WorkOrderAppliedDiscountExtSerDcs+CoreDataClass.h"
#import "DPS-Swift.h"


@interface CreditCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelCredit;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIButton *buttonViewMore;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeleteCredit;

@end

@implementation CreditCell

@end


// cell payment detail left side

@interface CreditPaymentDetailLeftCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelCreditName;

@property (weak, nonatomic) IBOutlet UILabel *labelAmount;

@end
@implementation CreditPaymentDetailLeftCell

@end

// cell payment detail Right side

@interface CreditPaymentDetailRightCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelCreditName;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;

@end
@implementation CreditPaymentDetailRightCell

@end

// Akshay End //


@interface MechanicalClientApprovalViewController ()
{
    UITableView *tblData;
    NSMutableArray *arrayArriveIndexpath,*arrDeclineIndexPath,*arrOfRepairMasters,*arrOfHoursConfig,*arrOfPartsGlobalMasters,*arrOfTempPartsToShow,*arrOfIndexSelectedGBB,*arrOfTotalAmountForApprovedRepairs,*arrOfReapairMasterIdInDb,*arrOfPriceLookup,*arrIndexGlobalToShowEquipHeader;
    BOOL chkHide,checkView,isCheckFrontImage,isStandardSubWorkOrder, isImageCapture, isOldPlan, isHoliday, isFirstPlanSelected, isPreSetSignGlobal;
    NSArray *buttons;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    NSString *strDate,*strCompanyKeyy,*strEmployeeId;
    Global *global;
    NSMutableArray *arrOFImagesName,*arrOfCheckBackImage;
    NSMutableArray *arrTempArrive,*arrTempDecline;
    NSString *strSubWorkOrderIdGlobal,*strWorkOrderId,*strGlobalDepartmentId,*strWorkOrderAccNo,*strWorkOrderAddressSubType,*strWorkOrderAddressId, *strAfterHrsDuration,*strCompanyName;
    NSMutableArray *arrOfSubWorkServiceIssues,*arrOfSubWorkServiceIssuesRepair,*arrOfSubWorkServiceIssuesRepairParts,*arrOfSubWorkServiceIssuesRepairLabour;
    NSString *strButtonClick,*strGlobalPaymentMode,*strUserName,*strGlobalWorkOrderId;
    BOOL isPaymentInfo,chkForUpdateWorkOrder;
    NSManagedObject *matchesPaymentInfo;
    NSManagedObject *matchesWorkOrder;
    NSString *strCustomerSign,*strTechnicianSign,*strCustomerNotPresent;
    UIImage *imageTemp;
    NSDictionary *dictMechanicalMasters,*dictDetailsMastersMechanical;
    NSArray *arrMechanicalMasters;
    NSMutableArray *arrDiagnostic,*arrTripCharge,*arrMileageCharge;
    BOOL chkTextEdit, isForSubmitWork, isSplitBill,chkTextEditNewPlan;
    NSString* paidAmount;
    NSMutableArray *arrIssueId,*arrAllParts;
    NSString *strButtonClickParts,*strDiagnosticChargeId,*strTripChargeId,*strMileageName,*strChargeAmountMiles,*strChargeAmountMilesNewPlan,*strDiagnosticChargeName,*strTripChargeName;
    NSManagedObject *matchesFinalSubWorkorder;
    NSString *strAmountPaidInSubWorkOrder;
    NSString *strTax,*strDiscountGlobal,*strDiscountGlobalNewPlan;
    NSString *strDepartmentSysName,*strServiceUrlMainServiceAutomation,*strAccountNo;
    NSMutableArray *arrMechanicalPSPMasters,*arrPSPMasters;
    NSArray *arrPlanStatusResponse;
    NSMutableDictionary *dictAmountTotalRepairs,*dictOfAllAmountSeparately;
    NSString *strPSPDiscount,*strPSPCharge,*strAccountPSPId,*strPspMasterId,*strDiagnosticChargeIdNewPlan,*strTripChargeIdNewPlan;
    NSString *strPSPDiscountPercent1,*strPSPDiscountAmount1,*strPSPCharge1,*strAccountPSPId1,*strPspMasterId1,*strPSPDiscountPercent2,*strPSPDiscountAmount2,*strPSPCharge2,*strAccountPSPId2,*strPspMasterId2;
    BOOL chkPlanClick,isFirstPlan;
    NSMutableArray *arrOfGlobalDynamicEmpSheetFinal,*arrOfHeaderTitleForSlots;
    NSString *strEmployeeNoLoggedIn,*strEmpID,*strEmpName;
    
    
    // Akshay Start //
    NSMutableArray *arrayAccountDiscountCredits;
    NSMutableArray *arrayMasterCredits;
    NSMutableArray *arrayCopyOfMasterCredits;
    NSMutableArray *arrayAppliedDiscounts;
    int viewCreditHeight;
    BOOL isSubTotalViewHeightAdded;
    BOOL isUpdateViewsFrame;
    NSDictionary *dictSelectedDiscount;
    NSDictionary *dictAddDiscount;
    BOOL isLaborPrice;
    int heightPaymntDetailView;
    
    float totalLaborAmountLocal;
    float totalPartAmountLocal;
    
    float totalLaborAmountLocalCopy;
    float totalPartAmountLocalCopy;
    BOOL isPopUpViewFromMaster;// part labor popup view me apply button hai jo ki common hai master credit or account discount ke liye,isiliye ye var banaya identify karne ke liye ki apply button per actuion kaha se ho raha hai
    
    BOOL isLaborPartViewAddedToSuperView;
    // Akshay End //
    
}


@end

@implementation MechanicalClientApprovalViewController
@synthesize strWoType;

- (void)viewDidLoad
{
    
    // Hidding lbltrip charge
    
    [_lblValueForTripChargeNewPlan setHidden:YES];
    [_lblValueForTipCharge setHidden:YES];
    
    // Akshay Start //
    arrayAccountDiscountCredits = [[NSMutableArray alloc]init];
    arrayMasterCredits = [[NSMutableArray alloc]init];
    arrayCopyOfMasterCredits = [[NSMutableArray alloc] init];
    arrayAppliedDiscounts = [[NSMutableArray alloc] init];
    viewCreditHeight = 145;// after minus tableview height and sub total view height from credit view
    isLaborPrice = YES;
    
    // heightPaymntDetailViewLeft = 950-90;//90 is height of tableview which I have added later for credit
    isLaborPartViewAddedToSuperView = NO;
    // Akshay End //
    
    arrOfIndexSelectedGBB=[[NSMutableArray alloc]init];
    arrOfTotalAmountForApprovedRepairs=[[NSMutableArray alloc]init];
    arrOfReapairMasterIdInDb=[[NSMutableArray alloc]init];
    
    isForSubmitWork=NO;
    chkPlanClick=NO;
    isImageCapture=NO;
    isOldPlan=YES;
    isFirstPlan=YES;
    isFirstPlanSelected=YES;
    isPreSetSignGlobal=NO;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"]];
    strCompanyKeyy     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strEmployeeId=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeId"]];
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strEmployeeNoLoggedIn=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    dictDetailsMastersMechanical=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    
    [super viewDidLoad];
    isPaymentInfo=NO;
    chkForUpdateWorkOrder=NO;
    chkTextEdit=NO;
    chkTextEditNewPlan=NO;
    strSubWorkOrderIdGlobal=_strSubWorkOderId;
    strWorkOrderId=_strWorlOrderId;
    strButtonClick=@"abc";
    global=[[Global alloc]init];
    chkHide=NO;checkView=NO;
    strGlobalPaymentMode=@"Cash";
    
    // change to set company based payment mode
    NSUserDefaults *defsPaymentMode=[NSUserDefaults standardUserDefaults];
    strGlobalPaymentMode = [defsPaymentMode valueForKey:@"CompanyPaymentMode"];
    
    _lblValueForTipCharge.text=@"$0.00";
    _lblValueForDiagnosticeCharge.text=@"$0.00";
    _txtOtherDiscounts.text=@"00";
    strDiscountGlobal=_txtOtherDiscounts.text;
    //Array Declaration
    arrayArriveIndexpath=[[NSMutableArray alloc]init];
    arrDeclineIndexPath=[[NSMutableArray alloc]init];
    arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
    
    buttons = @[_btnCash, _btnCheck, _btnCreditCard, _btnBill,_btnAutoChargeCustomer,_btnPaymentPending,_btnNoChange];
    
    arrTempArrive=[[NSMutableArray alloc]init];
    arrTempDecline=[[NSMutableArray alloc]init];
    [arrTempArrive addObject:@"decline"];
    [arrTempArrive addObject:@"decline"];
    [arrTempDecline addObject:@"decline"];
    [arrTempDecline addObject:@"decline"];
    
    // Do any additional setup after loading the view.
    
    arrOFImagesName=[[NSMutableArray alloc] init];
    arrOfCheckBackImage=[[NSMutableArray alloc] init];
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [viewBackGround addGestureRecognizer:singleTapGestureRecognizer];
    
    
    //    BOOL isNetAvailable=[global isNetReachable];
    //
    //    if (isNetAvailable) {
    //
    //        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    //
    //        strTax=[global getTaxForMechanicalService:strServiceUrlMainServiceAutomation :strCompanyKeyy :strWorkOrderId];
    //
    //    }
    
    [self fetchSubWorkOrderFromDataBase];
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    [self fetchWorkOrderFromDataBaseForMechanical];
    [self fetchLocalMasters];
    //[self getPricLookupFromMaster];
    //For Parts
#pragma mark - ***************** FOR PARTS *****************
    [self fetchAllParts];
    [self methodBorderColor];
    //End
#pragma mark - ***************** FOR ISSUE *****************
    if ([strWoType isEqualToString:@"FR"])
    {
        [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
        [self calculateTotal:_txtOtherDiscounts.text];
        [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
    }
#pragma mark - ***************** FOR PARTS *****************
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
        [self calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    //   [self getPlan];
    [self methodBorderColor];
    [self tableCreate];
    [self addView];
    
    _lblTitle.text=[defsLogindDetail valueForKey:@"lblNameNew"];
    
    //New changes for GBB
    
    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    
    [self fetchALLSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical];
    
    [self fetchALlSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical];
    
    [self fetchAllGbbFromMaster];
    
    [self getHoursConfiFromMaster];
    
    [self getPartsMaster];
    
    [self methodFirstTimeViewLoadedToSetValuesSelectedGBB];
    
    [_tblViewServiceIssue reloadData];
    
    NSArray *arrTotal;
    arrTotal=[self getArrayOfTotalRepairs];
    
    double sumTotal=0;
    
    for(int i=0;i<arrTotal.count;i++)
    {
        
        sumTotal=sumTotal+[[arrTotal objectAtIndex:i]doubleValue];
        
    }
    
    _lblValueTotalApprovedRepairs.text=[NSString stringWithFormat:@"$%.02f",sumTotal];
    _lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"$%.2f",sumTotal];
    
    _btnTripChargeNewPlan.titleLabel.numberOfLines=2;
    _btnDiagnosticChargeNewPlan.titleLabel.numberOfLines=2;
    
    _btnTipCharge.titleLabel.numberOfLines=2;
    
    _btnDiagnosticCharge.titleLabel.numberOfLines=2;
    
    // [self getPlan];
    // [self addView];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Please Wait..."];
    
    [self performSelector:@selector(callMethod) withObject:nil afterDelay:7.0];
    
    [self performSelector:@selector(methodToSetAmountFinally) withObject:nil afterDelay:3.0];
    
    [self performSelector:@selector(removeDejalActivity) withObject:nil afterDelay:40.0];
    [self performSelector:@selector(removeDejalActivity) withObject:nil afterDelay:150.0];
    
    NSLog(@"view did load called");
    
    //[self createEmpTimeSheetButton];
    
    //[self setPaymentModeBasedOnConfig];
    
    [self performSelector:@selector(setPaymentModeBasedOnConfig) withObject:nil afterDelay:0.10];
}

-(void)callMethod{
    
    BOOL isNetAvailable=[global isNetReachable];
    
    if (isNetAvailable) {
        
        //  [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Please Wait..."];
        
        NSLog(@"getTaxForMechanicalService called");
        
        [self performSelector:@selector(removeDejalActivity) withObject:nil afterDelay:40.0];
        [self performSelector:@selector(removeDejalActivity) withObject:nil afterDelay:150.0];
        
        strTax=[global getTaxForMechanicalService:strServiceUrlMainServiceAutomation :strCompanyKeyy :strWorkOrderId];
        
        [self getPlan];
        
    }else{
        
        [self getPlan];

        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
        
    }
    
    [self addView];
}

-(void)setPaymentModeBasedOnConfig{
    
    //strGlobalPaymentMode = @"Check";
    
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        [self setButtonImage:_btnCheck];
        checkView=YES;
        [_viewForSingleAmount setHidden:YES];
        [self addView];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Cash"])
    {
        [self setButtonImage:_btnCash];
        checkView=NO;
        [_viewForSingleAmount setHidden:NO];
        [self addView];
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Bill"])
    {
        [self setButtonImage:_btnBill];
        checkView=NO;
        [_viewForSingleAmount setHidden:YES];
        [self addView];
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"AutoChargeCustomer"])
    {
        [self setButtonImage:_btnAutoChargeCustomer];
        checkView=NO;
        [_viewForSingleAmount setHidden:YES];
        
        [self addView];
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"CreditCard"])
    {
        [self setButtonImage:_btnCreditCard];
        checkView=NO;
        [_viewForSingleAmount setHidden:NO];
        
        [self addView];
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"PaymentPending"])
    {
        
        [self setButtonImage:_btnPaymentPending];
        [_viewForSingleAmount setHidden:YES];
        checkView=NO;
        [self addView];
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"])
    {
        [self setButtonImage:_btnNoChange];
        [_viewForSingleAmount setHidden:YES];
        checkView=NO;
        [self addView];
        
    }else{
        
        [self setButtonImage:_btnCash];
        checkView=NO;
        [_viewForSingleAmount setHidden:NO];
        [self addView];
        
    }
}

-(void)fetchLocalMasters
{
    //strDepartmentSysName=@"RCDept";
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    arrMechanicalMasters=[dictMechanicalMasters valueForKey:@"ChargeMasterExtSerDc"];
    arrDiagnostic=[[NSMutableArray alloc]init];
    arrTripCharge=[[NSMutableArray alloc]init];
    arrMileageCharge=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrOfTempChargeMasters=[[NSMutableArray alloc]init];
    
    
    for(int i=0;i<arrMechanicalMasters.count;i++)
    {
        NSDictionary *dictDataa=[arrMechanicalMasters objectAtIndex:i];
        
        NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
        NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
        NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
        
        
        if ([strLocalCompanyKey isEqualToString:strCompanyKeyy] && [strLocalDeptSysName isEqualToString:strDepartmentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:strWorkOrderAddressId]) {
            
            [arrOfTempChargeMasters addObject:dictDataa];
            
        }
    }
    
    if (arrOfTempChargeMasters.count==0) {
        
        for (int k=0; k<arrMechanicalMasters.count; k++) {
            
            NSDictionary *dictDataa=arrMechanicalMasters[k];
            //   BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
            //   if (isActive) {
            
            // Changes for fetching Via company dept account address
            
            
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
            NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
            
            
            if ([strLocalCompanyKey isEqualToString:strCompanyKeyy] && [strLocalDeptSysName isEqualToString:strDepartmentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:@""]) {
                
                [arrOfTempChargeMasters addObject:dictDataa];
                
            }
        }
        
    }
    
    if (arrOfTempChargeMasters.count==0) {
        
        for (int k=0; k<arrMechanicalMasters.count; k++) {
            
            NSDictionary *dictDataa=arrMechanicalMasters[k];
            //   BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
            //   if (isActive) {
            
            // Changes for fetching Via company dept account address
            
            
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
            NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
            //  NSString *strLocalAddressSubType=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AddressType"]];
            
            
            if ([strLocalCompanyKey isEqualToString:strCompanyKeyy] && [strLocalDeptSysName isEqualToString:strDepartmentSysName] && [strLocalAccountNo isEqualToString:@""] && [strLocalAddressId isEqualToString:@""]) {
                
                [arrOfTempChargeMasters addObject:dictDataa];
                
            }
        }
        
    }
    //  change for
    
    NSDictionary *dictDataTemp=[[NSDictionary alloc]initWithObjectsAndKeys:@"--Diagnostic Charge--",@"ChargeName", nil];
    [arrDiagnostic addObject:dictDataTemp];
    
    dictDataTemp=[[NSDictionary alloc]initWithObjectsAndKeys:@"--Trip Charge--",@"ChargeName", nil];
    [arrTripCharge addObject:dictDataTemp];
    
    dictDataTemp=[[NSDictionary alloc]initWithObjectsAndKeys:@"--Mileage Charge--",@"ChargeName", nil];
    [arrMileageCharge addObject:dictDataTemp];
    
    for(int i=0;i<arrOfTempChargeMasters.count;i++)
    {
        NSDictionary *dict=[arrOfTempChargeMasters objectAtIndex:i];
        if ([[dict valueForKey:@"ChargeType"]isEqualToString:@"Diagnostic"])
        {
            [arrDiagnostic addObject:dict];
        }
        else if ([[dict valueForKey:@"ChargeType"]isEqualToString:@"Trip"])
        {
            [arrTripCharge addObject:dict];
        }
        else if ([[dict valueForKey:@"ChargeType"]isEqualToString:@"Mileage"])
        {
            [arrMileageCharge addObject:dict];
        }
    }
    
}


-(void)viewWillAppear:(BOOL)animated
{
    
    _txtViewAdditionalInfo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewAdditionalInfo.layer.borderWidth=1.0;
    _txtViewAdditionalInfo.layer.cornerRadius=5.0;
    
    NSString *strIns,*strCust;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    strIns=[defs valueForKey:@"fromInspectorSignService"];
    strCust=[defs valueForKey:@"fromCustomerSignService"];
    BOOL isForEditImage=[defs boolForKey:@"yesEditImage"];
    
    if ([strIns isEqualToString:@"fromInspectorSignService"])
    {
        // isEditedInSalesAuto=YES;
        NSLog(@"Global mopdify date set to YES");
        NSString *strImageName;
        strImageName=[defs valueForKey:@"imagePath"];
        strTechnicianSign=strImageName;
        UIImage *imageSign;
        imageSign=[self loadImage:strImageName];
        _imgInspectorSign.image=imageSign;
        [defs setValue:@"abc" forKey:@"fromInspectorSignService"];
        [defs setValue:@"abc" forKey:@"imagePath"];
        [defs synchronize];
    }
    
    else if ([strCust isEqualToString:@"fromCustomerSignService"])
    {
        // isEditedInSalesAuto=YES;
        NSLog(@"Global mopdify date set to YES");
        NSString *strImageName;
        strImageName=[defs valueForKey:@"imagePath"];
        strCustomerSign=strImageName;
        UIImage *imageSign;
        imageSign=[self loadImage:strImageName];
        _imgCustomerSign.image=imageSign;
        [defs setValue:@"xyz" forKey:@"fromCustomerSignService"];
        [defs setValue:@"xyz" forKey:@"imagePath"];
        [defs synchronize];
        
    }else if (isImageCapture){
        isImageCapture=NO;
    }else if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
        EditImageViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewControlleriPad"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    }
    else{
        
        [self methodCalculationOverAll];
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // Akshay Start //
    _heightTblViewPaymntDetailRight.constant = 0.0;
    [self getAccDiscountAppliedDiscountAndMasterCredits];
    // Akshay End //
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- ***************** Button Action *****************

- (IBAction)action_DashBoardView:(id)sender {
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert!"
                               message:@"Are you sure to move to DashBoard"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName: UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"CRM_New_iPad" : @"DashBoard" bundle: nil];
     
        DashBoardNew_iPhoneVC
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardNew_iPhoneVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (IBAction)actionOnCustomerNotPresent:(UIButton*)sender
{
    chkForUpdateWorkOrder=YES;
    if (sender.selected==NO)
    {
        strCustomerNotPresent=@"true";
        [_imgSignCheckBox setImage:[UIImage imageNamed:@"check_box_2.png"]];
        //  _cnstrntCustomerSignatureView_H.constant=0;        _cnstrntViewTermsCondition_H.constant=306;
        sender.selected=YES;
        _viewCustomerSign.hidden=YES;
        //Nilind 05 Jan
        // chkCustomerNotPresent=YES;
        //........
    }
    else
    {
        strCustomerNotPresent=@"false";
        
        [_imgSignCheckBox setImage:[UIImage imageNamed:@"check_box_1.png"]];
        //[_imgViewCustomerSignature setImage:[UIImage imageNamed:@"NoImage.jpg"]];
        //_cnstrntCustomerSignatureView_H.constant=131;// Actual 131
        // _cnstrntViewTermsCondition_H.constant=437;// 437-131
        sender.selected=NO;
        _viewCustomerSign.hidden=NO;
        
        //Nilind 05 Jan
        //  chkCustomerNotPresent=NO;
        //........
    }
    
}

- (IBAction)actionOnInspectorSign:(id)sender
{
    chkForUpdateWorkOrder=YES;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromTechService" forKey:@"signatureType"];
    [defs synchronize];
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
    ServiceSignViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"ServiceSignViewControlleriPad"];
    //objSignViewController.strType=@"inspector";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
}
- (IBAction)actionOnCustomerSign:(id)sender
{
    chkForUpdateWorkOrder=YES;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromCustService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
    ServiceSignViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"ServiceSignViewControlleriPad"];
    // objSignViewController.strType=@"customer";
    
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
}

- (IBAction)actionOnCancel:(id)sender
{
    [self goToSubWorkOrderDetailView];
}

- (IBAction)actionOnSubmit:(id)sender
{
    BOOL isMileageAlert=[self mileageAlert];
    
    if (isMileageAlert) {
        
        [global AlertMethod:Alert :@"Please enter miles"];
        
    } else {
        
        if (isSplitBill) {
            
            float valuesPercent=[_txtLaborPercent.text floatValue]+[_txtPartPercent.text floatValue];
            
            if (!(valuesPercent==100)) {
                
                [global AlertMethod:Alert :@"labor and part percent combined must be 100%"];
                
            } else {
                
                isForSubmitWork=NO;
                [self finalSavePaymentInfo];
                [self updateWorkOrderDetail];
                
            }
            
        } else {
            
            isForSubmitWork=NO;
            [self finalSavePaymentInfo];
            [self updateWorkOrderDetail];
            
        }
        
    }
}

- (IBAction)action_StartWork:(id)sender {
    
    BOOL isMileageAlert=[self mileageAlert];
    
    if (isMileageAlert) {
        
        [global AlertMethod:Alert :@"Please enter miles"];
        
    } else {
        
        if (isSplitBill) {
            
            float valuesPercent=[_txtLaborPercent.text floatValue]+[_txtPartPercent.text floatValue];
            
            if (!(valuesPercent==100)) {
                
                [global AlertMethod:Alert :@"labor and part percent combined must be 100%"];
                
            } else {
                
                isForSubmitWork=YES;
                [self finalSavePaymentInfo];
                [self updateWorkOrderDetail];
                
            }
            
        } else {
            
            isForSubmitWork=YES;
            [self finalSavePaymentInfo];
            [self updateWorkOrderDetail];
            
        }
        
    }
    
}

-(BOOL)mileageAlert{
    
    BOOL mileageAlert = NO;
    
    if (isFirstPlan) {
        
        if ([_btnMileageCharge.titleLabel.text isEqualToString:@"--Mileage Charge--"] || [_btnMileageCharge.currentTitle isEqualToString:@"--Mileage Charge--"]) {
            
            mileageAlert = NO;
            
        } else {
            
            if ((_txtMiles.text==0) || (_txtMiles.text.length==0)) {
                
                mileageAlert = YES;
                
            } else {
                
                mileageAlert = NO;
                
            }
            
        }
        
    } else {
        
        if ([_btnMileageChargeNewPlan.titleLabel.text isEqualToString:@"--Mileage Charge--"] || [_btnMileageChargeNewPlan.currentTitle isEqualToString:@"--Mileage Charge--"]) {
            
            mileageAlert = NO;
            
        } else {
            
            if ((_txtMilesNewPlan.text==0) || (_txtMilesNewPlan.text.length==0)) {
                
                mileageAlert = YES;
                
            } else {
                
                mileageAlert = NO;
                
            }
            
        }
        
    }
    
    return mileageAlert;
}

- (IBAction)actionOnDiagnosticCharge:(id)sender
{
    if (arrDiagnostic.count==0) {
        
        [global AlertMethod:Alert :@"No diagnostic charge available"];
        
    } else {
        
        tblData.tag=10;
        [self tableLoad:tblData.tag];
        
    }
}
- (IBAction)actionOnTipCharge:(id)sender
{
    [self.view endEditing:YES];
    if (arrTripCharge.count==0) {
        
        [global AlertMethod:Alert :@"No trip charge available"];
        
    } else {
        
        tblData.tag=20;
        [self tableLoad:tblData.tag];
        
    }
    
}
- (IBAction)actionOnCash:(id)sender
{
    [_viewForSingleAmount setHidden:NO];
    strGlobalPaymentMode=@"Cash";
    [self setButtonImage:_btnCash];
    checkView=NO;
    [self addView];
    
}
- (IBAction)actionOnCheck:(id)sender
{
    [_viewForSingleAmount setHidden:NO];
    
    strGlobalPaymentMode=@"Check";
    [self setButtonImage:_btnCheck];
    checkView=YES;
    [self addView];
    
    
}
- (IBAction)actionOnCreditCard:(id)sender
{
    [_viewForSingleAmount setHidden:NO];
    
    strGlobalPaymentMode=@"CreditCard";
    [self setButtonImage:_btnCreditCard];
    checkView=NO;
    [self addView];
    
}
- (IBAction)actionOnBill:(id)sender
{
    [_viewForSingleAmount setHidden:YES];
    
    strGlobalPaymentMode=@"Bill";
    [self setButtonImage:_btnBill];
    checkView=NO;
    [self addView];
    
}
- (IBAction)actionOnAutoChargeCustomer:(id)sender
{
    [_viewForSingleAmount setHidden:YES];
    
    strGlobalPaymentMode=@"AutoChargeCustomer";
    [self setButtonImage:_btnAutoChargeCustomer];
    checkView=NO;
    [self addView];
    
}
- (IBAction)actionOnPaymentPending:(id)sender
{
    [_viewForSingleAmount setHidden:YES];
    
    strGlobalPaymentMode=@"PaymentPending";
    [self setButtonImage:_btnPaymentPending];
    checkView=NO;
    [self addView];
}
- (IBAction)actionOnNoChange:(id)sender
{
    [_viewForSingleAmount setHidden:YES];
    strGlobalPaymentMode=@"NoCharge";
    [self setButtonImage:_btnNoChange];
    checkView=NO;
    [self addView];
    
}
- (IBAction)actionOnExpirationDate:(id)sender
{
    [self addPickerViewDateTo];
}

- (IBAction)actionBack:(id)sender
{
    
    //[self.navigationController popViewControllerAnimated:YES];
    
    [self goToSubWorkOrderDetailView];
    
}

-(void)goToSubWorkOrderDetailView{
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    [defss setInteger:-1 forKey:@"sectionToOpen"];
    [defss synchronize];
    
    if ([strWoType isEqualToString:@"TM"]) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=matchesWorkOrder;
        objByProductVC.objSubWorkOrderdetails=matchesFinalSubWorkorder;
        objByProductVC.strWoType=strWoType;
        
        NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
        [defsId setValue:strSubWorkOrderIdGlobal forKey:@"SubWorkOrderId"];
        [defsId synchronize];
        
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    } else {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                 bundle: nil];
        MechanicalSubWorkOrderDetailsViewControlleriPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControlleriPad"];
        objByProductVC.strSubWorkOrderId=strSubWorkOrderIdGlobal;
        objByProductVC.objWorkOrderdetails=matchesWorkOrder;
        objByProductVC.objSubWorkOrderdetails=matchesFinalSubWorkorder;
        objByProductVC.strWoType=strWoType;
        
        NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
        [defsId setValue:strSubWorkOrderIdGlobal forKey:@"SubWorkOrderId"];
        [defsId synchronize];
        
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
    
}

- (IBAction)actionOnCheckFrontImage:(id)sender
{
    isCheckFrontImage=YES;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
    
    [actionSheet showInView:self.view];
    
    
}

- (IBAction)actionOnCheckBackImage:(id)sender
{
    isCheckFrontImage=NO;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
    [actionSheet showInView:self.view];
}

#pragma mark- ****************** Tabel Delegate Methods ******************

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//
//   /* UILabel *myLabel = [[UILabel alloc] init];
//    myLabel.frame = CGRectMake(20, 8, 320, 20);
//    myLabel.font = [UIFont boldSystemFontOfSize:18];
//    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];*/
//
//    /*UIView *headerView = [[UIView alloc] init];
//    [headerView addSubview:myLabel];*/
//
//    //return headerView;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==_tableViewCreditPaymntDetailLeft)
    {
        return 60;
    }
    else if(tableView==_tableViewCreditPaymntDetailRight)
    {
        return 60;
    }
    else
    {
        return tableView.rowHeight;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView==_tblViewServiceIssue)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                k1++;
                
            }
            
        }
        if(k1==0)
        {
            return 0;
            
        }
        else
        {
            NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
            if ([arrIndexGlobalToShowEquipHeader containsObject:strSecton]) {
                
                return 100;
                
            }else{
                
                return 50;
                
            }
            
        }
        
    }
    else if(tableView==_tblIssueParts)
    {
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        if (arrOfSubWorkServiceIssuesRepairParts.count==0)
        {
            return 0;
            
        }
        else
        {
            NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
            if ([arrIndexGlobalToShowEquipHeader containsObject:strSecton]) {
                
                return 100;
                
            }else{
                
                return 50;
                
            }
        }
    }
    else
    {
        return 0;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==_tblViewServiceIssue)
    {
        return arrOfSubWorkServiceIssues.count;//
    }
    else if (tableView == _tblIssueParts)
    {
        return arrOfSubWorkServiceIssues.count;;
    }
    else
    {
        return 1;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGRect frame = tableView.frame;
    NSManagedObject *dictData=arrOfSubWorkServiceIssues[section];
    UILabel *title,*titleEquip;
    NSString *strSecton=[NSString stringWithFormat:@"%ld",(long)section];
    
    if(tableView==_tblIssueParts)
    {
        if ([arrIndexGlobalToShowEquipHeader containsObject:strSecton]) {
            
            titleEquip = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblIssueParts.frame.size.width-20, 40)];
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, titleEquip.frame.size.height+titleEquip.frame.origin.y+5, _tblIssueParts.frame.size.width-20, 40)];
            
        }else{
            
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblIssueParts.frame.size.width-20, 40)];
            
        }
    }
    else if (tableView==_tblViewServiceIssue)
    {
        
        if ([arrIndexGlobalToShowEquipHeader containsObject:strSecton]) {
            
            titleEquip = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblViewServiceIssue.frame.size.width-20, 40)];
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, titleEquip.frame.size.height+titleEquip.frame.origin.y+5, _tblViewServiceIssue.frame.size.width-20, 40)];
            
        }else{
            
            title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _tblViewServiceIssue.frame.size.width-20, 40)];
            
        }
        
    }
    
    title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceIssue"]];
    title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceIssue"]];
    title.backgroundColor=[UIColor lightGrayColor];
    title.font=[UIFont boldSystemFontOfSize:22];
    titleEquip.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceIssue"]];
    titleEquip.backgroundColor=[UIColor lightGrayColor];
    titleEquip.font=[UIFont boldSystemFontOfSize:22];
    
    //Change for equip header
    
    if ([arrIndexGlobalToShowEquipHeader containsObject:strSecton]) {
        titleEquip.text = [NSString stringWithFormat:@"Equipment: %@",[dictData valueForKey:@"equipmentName"]];
        title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceIssue"]];
        titleEquip.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
        
    } else {
        
        title.text = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceIssue"]];
        title.backgroundColor=[UIColor lightGrayColor];
        
    }
    
    // End change for equip header
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [headerView addSubview:titleEquip];
    [headerView addSubview:title];
    
    return headerView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblViewServiceIssue)
    {
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        int k1=0;
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++) {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                k1++;
                
            }
            
        }
        
        return k1;
        
    }
    else if (tableView == _tblIssueParts)
    {
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        return arrOfSubWorkServiceIssuesRepairParts.count;
    }
    // Akshay Start //
    else if(tableView == _tableViewCredit)
    {
        return arrayAppliedDiscounts.count;
    }
    else if(tableView == _tableViewCreditPaymntDetailLeft)
    {
        return arrayAppliedDiscounts.count;
    }
    else if(tableView == _tableViewCreditPaymntDetailRight)
    {
        return arrayAppliedDiscounts.count;
    }
    // Akshay End //
    else if (tableView.tag==10)
    {
        return arrDiagnostic.count;
    }
    else if (tableView.tag==20)
    {
        return arrTripCharge.count;
    }
    else if (tableView.tag==30)
    {
        return arrDiagnostic.count;
    }
    else if (tableView.tag==40)
    {
        return arrTripCharge.count;
    }
    else if (tableView.tag==50)
    {
        return arrPSPMasters.count;
    }
    else if (tableView.tag==60)
    {
        return arrMileageCharge.count;
    }
    else if (tableView.tag==70)
    {
        return arrMileageCharge.count;
    }
    // Akshay Start //
    else if(tableView.tag==80)
    {
        return arrayAccountDiscountCredits.count;
    }
    else if(tableView.tag == 90)
    {
        return arrayMasterCredits.count;
    }
    // Akshay End //
    //    else if (tableView == _tableViewCredit)
    //    {
    //        return <#expression#>
    //    }
    else
    {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblViewServiceIssue)
    {
        MechanicalClientApprovalTableViewCell *cell = (MechanicalClientApprovalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MechanicalClientApprovalTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        [cell.btnApprove addTarget:self
                            action:@selector(buttonClickOnArrive:) forControlEvents:UIControlEventTouchDown];
        [cell.btnDecline addTarget:self
                            action:@selector(buttonClickOnDecline:) forControlEvents:UIControlEventTouchDown];
        
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
    }
    if (tableView==_tblIssueParts)
    {
        MechanicalIssuePartsTableViewCell
        *cell = (MechanicalIssuePartsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MechanicalIssuePartsTableViewCell" forIndexPath:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        
        cell.btnApproveForParts.tag=indexPath.row;
        cell.btnDeclineForParts.tag=indexPath.row;
        [cell.btnApproveForParts addTarget:self
                                    action:@selector(buttonClickOnArriveForParts:) forControlEvents:UIControlEventTouchDown];
        [cell.btnDeclineForParts addTarget:self
                                    action:@selector(buttonClickOnDeclineForParts:) forControlEvents:UIControlEventTouchDown];
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        
        [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
        
        
        NSManagedObject *dictIssuesPartData=arrOfSubWorkServiceIssuesRepairParts[indexPath.row];
        NSString *strMulti=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"multiplier"]];
        float multip=[strMulti floatValue];
        
        NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"qty"]];
        float qtyy=[strqtyy floatValue];
        
        NSString *strunitPricee=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"unitPrice"]];
        float unitPricee=[strunitPricee floatValue];
        
        float finalPriceParts=unitPricee*qtyy*multip;
        float totalCostPartss=0;
        totalCostPartss=totalCostPartss+finalPriceParts;
        
        cell.lblAmountValue.text=[NSString stringWithFormat:@"%.02f",totalCostPartss];
        
        
        NSString *strPartsnQty=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"PartName"]];
        cell.lblPartName.text=strPartsnQty;
        if (cell.lblPartName.text.length==0) {
            
            cell.lblPartName.text=@"N/A";
            
        }
        cell.lblQty.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"qty"]];
        if (cell.lblQty.text.length==0) {
            
            cell.lblQty.text=@"N/A";
        }
        cell.lblPartDesc.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]];
        if (cell.lblPartDesc.text.length==0) {
            
            cell.lblPartDesc.text=@"N/A";
        }
        
        cell.lblIssueId.text=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"issueRepairPartId"]];
        
        if ([[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"customerFeedback"]]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"customerFeedback"]]isEqualToString:@""])
        {
            cell.btnApproveForParts.backgroundColor=[UIColor colorWithRed:50.0f/255 green:164.0f/255 blue:159.0f/255 alpha:1];
            cell.btnDeclineForParts.backgroundColor=[UIColor colorWithRed:170.0f/255 green:170.0f/255 blue:170.0f/255 alpha:1];
        }
        else
        {
            cell.btnDeclineForParts.backgroundColor=[UIColor colorWithRed:50.0f/255 green:164.0f/255 blue:159.0f/255 alpha:1];
            cell.btnApproveForParts.backgroundColor=[UIColor colorWithRed:170.0f/255 green:170.0f/255 blue:170.0f/255 alpha:1];
            
        }
        
        
        cell.btnViewMore.tag=indexPath.row*1000+indexPath.section;;
        [cell.btnViewMore addTarget:self action:@selector(action_ViewMoreJobDescription:) forControlEvents:UIControlEventTouchDown];
        
        if ([NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]].length>22) {
            
            cell.btnViewMore.hidden=NO;
            
        } else {
            
            cell.btnViewMore.hidden=YES;
            
        }
        
        return cell;
    }
    // Akshay Start //
    else if (tableView == _tableViewCredit)
    {
        CreditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreditCell"];
        if(cell==nil)
        {
            cell = [[CreditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreditCell"];
        }
        cell.labelCredit.text = [[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountSysName"];
        cell.labelAmount.text = [NSString stringWithFormat:@"$%.2f",[[[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"appliedDiscountAmt"] floatValue]];
        
        cell.labelDescription.text = [[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountDescription"];
        cell.buttonDeleteCredit.tag = indexPath.row;
        [cell.buttonDeleteCredit addTarget:self action:@selector(actionOnDeleteCredit:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.buttonViewMore addTarget:self action:@selector(actionOnViewMore_Cedit:) forControlEvents:UIControlEventTouchDown];
        cell.buttonViewMore.tag = indexPath.row;
        if ([NSString stringWithFormat:@"%@",[[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountDescription"]].length>22) {
            
            cell.buttonViewMore.hidden=NO;
            
        } else {
            
            cell.buttonViewMore.hidden=YES;
            
        }
        
        
        return cell;
        
    }
    else if(tableView== _tableViewCreditPaymntDetailLeft)
    {
        CreditPaymentDetailLeftCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreditPaymentDetailLeftCell"];
        if(cell==nil)
        {
            cell = [[CreditPaymentDetailLeftCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreditPaymentDetailLeftCell"];
        }
        cell.labelCreditName.text = [[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountSysName"];
        // [cell.labelCreditName setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightSemibold]];
        
        cell.labelAmount.text = [NSString stringWithFormat:@"$%.2f",[[[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"appliedDiscountAmt"] floatValue]];
        
        // [cell.labelAmount setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightSemibold]];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10)];
        separatorLineView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:separatorLineView];
        return cell;
        
    }
    else if(tableView== _tableViewCreditPaymntDetailRight)
    {
        CreditPaymentDetailRightCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreditPaymentDetailRightCell"];
        if(cell==nil)
        {
            cell = [[CreditPaymentDetailRightCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreditPaymentDetailRightCell"];
        }
        cell.labelCreditName.text = [[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"discountSysName"];
        cell.labelAmount.text = [NSString stringWithFormat:@"$%.2f",[[[arrayAppliedDiscounts objectAtIndex:indexPath.row] valueForKey:@"appliedDiscountAmt"] floatValue]];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10)];
        separatorLineView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:separatorLineView];
        return cell;
        
    }
    // Akshay End //
    else
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (tblData.tag==10)
        {
            NSDictionary *dict=[arrDiagnostic objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
            
        }
        else if (tblData.tag==20)
        {
            NSDictionary *dict=[arrTripCharge objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        else if (tblData.tag==30)
        {
            NSDictionary *dict=[arrDiagnostic objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        else if (tblData.tag==40)
        {
            NSDictionary *dict=[arrTripCharge objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        else if (tblData.tag==50)
        {
            NSDictionary *dict=[arrPSPMasters objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"Title"];
        }
        else if (tblData.tag==60)
        {
            NSDictionary *dict=[arrMileageCharge objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        else if (tblData.tag==70)
        {
            NSDictionary *dict=[arrMileageCharge objectAtIndex:indexPath.row];
            cell.textLabel.text=[dict valueForKey:@"ChargeName"];
        }
        // Akshay Start //
        else if (tblData.tag==80)
        {
            NSDictionary *dict=[arrayAccountDiscountCredits objectAtIndex:indexPath.row];//
            cell.textLabel.text = [dict valueForKey:@"sysName"];
            
        }
        
        else if (tblData.tag==90)
        {
            NSDictionary *dict=[arrayMasterCredits objectAtIndex:indexPath.row];
            cell.textLabel.text = [dict valueForKey:@"SysName"];
        }
        
        // Akshay End //
        if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
        {
            cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        
        
        return cell;
    }
    
}


-(void)action_ViewMoreJobDescription:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    [self fetchSubWorkOrderIssuesPartsFromDataBase:strIssueIdToCheck];
    NSManagedObject *dictIssuesPartData=arrOfSubWorkServiceIssuesRepairParts[row];
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[dictIssuesPartData valueForKey:@"partDesc"]];
    [global AlertMethod:@"Part Description" :strJobDesc];
    
}

-(void)action_ViewMoreRepairDescriptionGood:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp1=arrTemp[row];
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"RepairDesc"]];
    [global AlertMethod:@"Repair Description" :strJobDesc];
    
}

-(void)action_ViewMoreRepairDescriptionBetter:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSString *strRepairMasterIdAlreadyPresent,*strLblDescNew;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp1=arrTemp[row];
    strRepairMasterIdAlreadyPresent=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairMasterId"]];
    
    for (int k=0; k<arrOfRepairMasters.count; k++) {
        
        NSDictionary *dictDataRepair1=arrOfRepairMasters[k];
        
        NSString *strId=[NSString stringWithFormat:@"%@",[dictDataRepair1 valueForKey:@"RepairMasterId"]];
        
        if ([strRepairMasterIdAlreadyPresent isEqualToString:strId]) {
            
            strLblDescNew=[dictDataRepair1 valueForKey:@"RepairDesc"];
            
            break;
        }
        
    }
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",strLblDescNew];
    [global AlertMethod:@"Repair Description" :strJobDesc];
    
}

-(void)action_ViewMoreRepairDescriptionBest:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int row= (int)btn.tag/1000;
    
    int section =btn.tag%1000;
    
    NSString *strRepairMasterIdAlreadyPresent,*strLblDescNew;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp1=arrTemp[row];
    strRepairMasterIdAlreadyPresent=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairMasterId"]];
    
    for (int k=0; k<arrOfRepairMasters.count; k++) {
        
        NSDictionary *dictDataRepair1=arrOfRepairMasters[k];
        
        NSString *strId=[NSString stringWithFormat:@"%@",[dictDataRepair1 valueForKey:@"RepairMasterId"]];
        
        if ([strRepairMasterIdAlreadyPresent isEqualToString:strId]) {
            
            strLblDescNew=[dictDataRepair1 valueForKey:@"RepairDesc"];
            
            break;
        }
        
    }
    
    NSString *strJobDesc=[NSString stringWithFormat:@"%@",strLblDescNew];
    [global AlertMethod:@"Repair Description" :strJobDesc];
    
}

- (void)configureCell:(MechanicalClientApprovalTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *strnonStdLaborAmt,*strnonStdPartAmt,*strnonStdRepairAmt,*strRepairQty;
    
    NSString *strRepairMasterIdAlreadyPresent,*strRepairLaborMasterIdAlreadyPresent,*cellRepairMasterId,*cellRepairLaborId,*cellRepairMasterId2,*cellRepairLaborId2,*cellRepairMasterId3,*cellRepairLaborId3,*strLblDescNew,*strPartCostAdjustmentGBB;
    
    NSDictionary *dictTempValues3,*dictTempValues,*dictTempValues2;
    
    cell.btnApprove.tag=indexPath.row;//indexPath.row;
    
    cell.btnDecline.tag=indexPath.row;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[indexPath.section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSString *strIssueRepairIdToCheck;
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp1=arrTemp[indexPath.row];
    if ([[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"customerFeedback"]]isEqualToString:@"1"])
    {
        cell.btnApprove.backgroundColor=[UIColor colorWithRed:50.0f/255 green:164.0f/255 blue:159.0f/255 alpha:1];
        cell.btnDecline.backgroundColor=[UIColor colorWithRed:170.0f/255 green:170.0f/255 blue:170.0f/255 alpha:1];
    }
    else
    {
        cell.btnDecline.backgroundColor=[UIColor colorWithRed:50.0f/255 green:164.0f/255 blue:159.0f/255 alpha:1];
        cell.btnApprove.backgroundColor=[UIColor colorWithRed:170.0f/255 green:170.0f/255 blue:170.0f/255 alpha:1];
    }
    
    cell.lblValueForRepair.text=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"RepairName"]];
    cell.lblValueForDesc1.text=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"RepairDesc"]];
    
    
    
    cell.btnViewMoreGood.tag=indexPath.row*1000+indexPath.section;;
    [cell.btnViewMoreGood addTarget:self action:@selector(action_ViewMoreRepairDescriptionGood:) forControlEvents:UIControlEventTouchDown];
    
    if ([NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"RepairDesc"]].length>56) {
        
        cell.btnViewMoreGood.hidden=NO;
        
    } else {
        
        cell.btnViewMoreGood.hidden=YES;
        
    }
    
    cellRepairMasterId=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairMasterId"]];
    cellRepairLaborId=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairLaborId"]];
    
    
    //repairLaborId  repairMasterId
    
    strRepairMasterIdAlreadyPresent=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairMasterId"]];
    strRepairLaborMasterIdAlreadyPresent=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairLaborId"]];
    
    if (cell.lblValueForDescription.text.length==0)
    {
        
        cell.lblValueForDescription.text=@"N/A";
        
    }
    strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"IssueRepairId"]];
    
    //Fetch Parts
    
    //  [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical:strIssueIdToCheck :strIssueRepairIdToCheck];
    
    strnonStdLaborAmt=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"nonStdLaborAmt"]];
    strnonStdPartAmt=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"nonStdPartAmt"]];
    strnonStdRepairAmt=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"nonStdRepairAmt"]];
    
    strRepairQty =[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"qty"]];
    cell.txtFldQty.delegate=self;
    cell.txtFldQty.accessibilityHint=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
    //cell.txtFldQty.leftView.hidden=YES;
    cell.txtFldQty.tag=0;
    cell.txtFldQty.text=strRepairQty;
    
    if ((strRepairQty.length==0) || [strRepairQty isEqualToString:@""] || [strRepairQty isEqualToString:@"(null)"] || [strRepairQty isEqualToString:@"0"]) {
        
        strRepairQty = @"1";
        cell.txtFldQty.text=@"";
        
    }
    
    NSMutableArray *arrTempParts=[[NSMutableArray alloc]init];
    
    float totalCostAdjustment=0;
    
    NSString *strcostAdjustment=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"costAdjustment"]];
    strPartCostAdjustmentGBB=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"PartCostAdjustment"]];
    
    totalCostAdjustment=[strcostAdjustment floatValue];
    
    NSString *strPartCostAdjustment=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"partCostAdjustment"]];
    
    totalCostAdjustment=totalCostAdjustment+[strPartCostAdjustment floatValue];
    
    float totalCostPartss=0;
    float totalCostPricePartss=0;
    
    for (int j=0; j<arrOfSubWorkServiceIssuesRepairParts.count; j++)
    {
        
        NSManagedObject *objTemp2=arrOfSubWorkServiceIssuesRepairParts[j];
        
        
        if ([strIssueIdToCheck isEqualToString:[objTemp2 valueForKey:@"subWorkOrderIssueId"]] && [strIssueRepairIdToCheck isEqualToString:[objTemp2 valueForKey:@"issueRepairId"]]) {
            
            
            NSString *strAddedAfterApproval=[NSString stringWithFormat:@"%@",[objTemp2 valueForKey:@"isAddedAfterApproval"]];
            
            if ([strAddedAfterApproval isEqualToString:@"0"]||[strAddedAfterApproval isEqualToString:@"false"]||[strAddedAfterApproval isEqualToString:@"False"])
            {
                NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[objTemp2 valueForKey:@"PartName"],[objTemp2 valueForKey:@"Qty"]];
                //multiplier   qty  unitPrice
                
                NSString *strMulti=[NSString stringWithFormat:@"%@",[objTemp2 valueForKey:@"multiplier"]];
                float multip=[strMulti floatValue];
                
                NSString *strqtyy=[NSString stringWithFormat:@"%@",[objTemp2 valueForKey:@"qty"]];
                float qtyy=[strqtyy floatValue];
                
                NSString *strunitPricee=[NSString stringWithFormat:@"%@",[objTemp2 valueForKey:@"unitPrice"]];
                float unitPricee=[strunitPricee floatValue];
                
                totalCostPricePartss=totalCostPricePartss+unitPricee*qtyy;
                
                float finalPriceParts=unitPricee*qtyy*multip;
                
                totalCostPartss=totalCostPartss+finalPriceParts;
                
                [arrTempParts addObject:strPartsnQty];
                
            }
            
        }
        
    }
    
    cell.lblValueForPart.text=[arrTempParts componentsJoinedByString:@", "];
    
    if (cell.lblValueForPart.text.length==0) {
        
        cell.lblValueForPart.text=@"N/A";
        
    }
    //Fetch LAbor
    
    // [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :strIssueRepairIdToCheck];
    
    float totalCostLaborss=0;
    
    NSString *strLaborHrs,*strHelperHrs;
    for (int j=0; j<arrOfSubWorkServiceIssuesRepairLabour.count; j++)
    {
        
        NSManagedObject *objTemp3=arrOfSubWorkServiceIssuesRepairLabour[j];
        
        NSString *strWarranty=[NSString stringWithFormat:@"%@",[objTemp3 valueForKey:@"isWarranty"]];
        
        ////Conditions For Parts Filter
        
        if ([strIssueRepairIdToCheck isEqualToString:[objTemp3 valueForKey:@"issueRepairId"]]) {
            
            if ([strWarranty isEqualToString:@"0"]||[strWarranty isEqualToString:@"false"]||[strWarranty isEqualToString:@"False"])
            {
                NSString *strLaborCostt=[NSString stringWithFormat:@"%@",[objTemp3 valueForKey:@"laborCost"]];
                float laborCostt=[strLaborCostt floatValue];
                
                totalCostLaborss=totalCostLaborss+laborCostt;
                
                NSString *strLaborType=[NSString stringWithFormat:@"%@",[objTemp3 valueForKey:@"laborType"]];
                
                if ([strLaborType isEqualToString:@"H"]) {
                    
                    strHelperHrs=[NSString stringWithFormat:@"%@",[objTemp3 valueForKey:@"laborHours"]];
                    
                }
                else
                {
                    
                    strLaborHrs=[NSString stringWithFormat:@"%@",[objTemp3 valueForKey:@"laborHours"]];
                    
                }
            }
            
        }
    }
    
    float overAllAmt=totalCostPartss+totalCostLaborss+totalCostAdjustment;
    
    // Condition change for if laborcost is 0 then no cost adjustment is to be added
    
    if (totalCostLaborss<=0) {
        
        overAllAmt = overAllAmt-[strcostAdjustment floatValue];
        
    }
    
    if (cellRepairLaborId.length==0) {
        
        if ((totalCostPartss==0) && (totalCostLaborss==0)) {
            
            if ([strnonStdRepairAmt isEqualToString:@"0"]) {
                
                strnonStdRepairAmt=@"";
                
            }
            
            if (strnonStdRepairAmt.length==0) {
                
                overAllAmt=overAllAmt+[strnonStdLaborAmt floatValue]+[strnonStdPartAmt floatValue];
                
                dictTempValues = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",[strnonStdLaborAmt floatValue]],@"laborcost",[NSString stringWithFormat:@"%f",[strnonStdPartAmt floatValue]],@"partcost",[NSString stringWithFormat:@"%f",[strnonStdPartAmt floatValue]],@"partcostsales",[NSString stringWithFormat:@"%f",overAllAmt],@"overallcost",[NSString stringWithFormat:@"%@",strcostAdjustment],@"costAdjustment",[NSString stringWithFormat:@"%@",strPartCostAdjustment],@"partCostAdjustment", nil];
                
            } else {
                
                overAllAmt=overAllAmt+[strnonStdRepairAmt floatValue];
                
                dictTempValues = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",totalCostLaborss],@"laborcost",[NSString stringWithFormat:@"%f",totalCostPricePartss],@"partcost",[NSString stringWithFormat:@"%f",[strnonStdPartAmt floatValue]],@"partcostsales",[NSString stringWithFormat:@"%f",overAllAmt],@"overallcost",[NSString stringWithFormat:@"%@",strcostAdjustment],@"costAdjustment",[NSString stringWithFormat:@"%@",strPartCostAdjustment],@"partCostAdjustment", nil];
                
            }
            
        }else{
            
            dictTempValues = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",totalCostLaborss],@"laborcost",[NSString stringWithFormat:@"%f",totalCostPricePartss],@"partcost",[NSString stringWithFormat:@"%f",totalCostPartss],@"partcostsales",[NSString stringWithFormat:@"%f",overAllAmt],@"overallcost",[NSString stringWithFormat:@"%@",strcostAdjustment],@"costAdjustment",[NSString stringWithFormat:@"%@",strPartCostAdjustment],@"partCostAdjustment", nil];
            
        }
        
    }else{
        
        dictTempValues = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",totalCostLaborss],@"laborcost",[NSString stringWithFormat:@"%f",totalCostPricePartss],@"partcost",[NSString stringWithFormat:@"%f",totalCostPartss],@"partcostsales",[NSString stringWithFormat:@"%f",overAllAmt],@"overallcost",[NSString stringWithFormat:@"%@",strcostAdjustment],@"costAdjustment",[NSString stringWithFormat:@"%@",strPartCostAdjustment],@"partCostAdjustment", nil];
        
    }
    
    //  dictTempValues = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",totalCostLaborss],@"laborcost",[NSString stringWithFormat:@"%f",totalCostPricePartss],@"partcost",[NSString stringWithFormat:@"%f",totalCostPartss],@"partcostsales",[NSString stringWithFormat:@"%f",overAllAmt],@"overallcost",[NSString stringWithFormat:@"%@",strcostAdjustment],@"costAdjustment",[NSString stringWithFormat:@"%@",strPartCostAdjustment],@"partCostAdjustment", nil];
    
    
    //    //Change for saving Overall Amount All Time in DB
    //
    //    NSString *strOverAllAmt=[NSString stringWithFormat:@"%f",overAllAmt];
    //
    //    NSString *strWorkOrderIDd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"workorderId"]];
    //
    //    NSString *strSubWorkOrderIdd=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderId"]];
    //
    //    SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
    //
    //    [objSync fetchRepairToUpdateRepairAmt:strWorkOrderIDd :strSubWorkOrderIdd :strIssueIdToCheck :strIssueRepairIdToCheck :strOverAllAmt];
    
    
    cell.lblValueForAmount.text=[NSString stringWithFormat:@"%.02f",overAllAmt];
    
    int repairQty = [strRepairQty intValue];
    
    float amountAfterAddingQty = [cell.lblValueForAmount.text floatValue]*repairQty;
    
    cell.lblValueForAmountTotal.text=[NSString stringWithFormat:@"%.02f",amountAfterAddingQty];
    
    if (strLaborHrs.length==0) {
        strLaborHrs=@"00:00";
    }
    if (strHelperHrs.length==0) {
        strHelperHrs=@"00:00";
    }
    
    cell.lblValueForLR.text=[global ChangeDateFormatOnGeneralInfoTime :strLaborHrs];
    cell.lblValueForHLR.text=[global ChangeDateFormatOnGeneralInfoTime :strHelperHrs];
    // NSManagedObject *record = [arrAllObjSubWorkOrderIssuesRepair objectAtIndex:indexPath.row];
    
    cell.lblIssueId.text=strIssueRepairIdToCheck;
    
    //Change For Other Option then selected
    
    NSMutableArray *arrOfRepairLaborMasterDcs=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfRepairMasters.count; k++) {
        
        NSDictionary *dictDataRepair1=arrOfRepairMasters[k];
        
        NSString *strId=[NSString stringWithFormat:@"%@",[dictDataRepair1 valueForKey:@"RepairMasterId"]];
        
        if ([strRepairMasterIdAlreadyPresent isEqualToString:strId]) {
            
            NSArray *arrTemps=[dictDataRepair1 valueForKey:@"RepairLaborMasterDcs"];
            
            strLblDescNew=[dictDataRepair1 valueForKey:@"RepairDesc"];
            
            //RepairLaborMasterDcs
            [arrOfRepairLaborMasterDcs addObjectsFromArray:arrTemps];
            
            break;
        }
        
    }
    
    cell.btnViewMoreBetter.tag=indexPath.row*1000+indexPath.section;;
    [cell.btnViewMoreBetter addTarget:self action:@selector(action_ViewMoreRepairDescriptionBetter:) forControlEvents:UIControlEventTouchDown];
    
    if (strLblDescNew.length>5) {
        
        cell.btnViewMoreBetter.hidden=NO;
        
    } else {
        
        cell.btnViewMoreBetter.hidden=YES;
        
    }
    
    
    cell.btnViewMoreBest.tag=indexPath.row*1000+indexPath.section;;
    [cell.btnViewMoreBest addTarget:self action:@selector(action_ViewMoreRepairDescriptionBest:) forControlEvents:UIControlEventTouchDown];
    
    if (strLblDescNew.length>5) {
        
        cell.btnViewMoreBest.hidden=NO;
        
    } else {
        
        cell.btnViewMoreBest.hidden=YES;
        
    }
    
    // arrOfRepairLaborMasterDcs  associated with this repair
    
    NSMutableArray *arrOfRepairLaborMasterDcsToShow=[[NSMutableArray alloc]init];
    
    for (int j=0; j<arrOfRepairLaborMasterDcs.count; j++) {
        
        NSDictionary *dictDataRepairLaborMaster=arrOfRepairLaborMasterDcs[j];
        
        if (![strRepairLaborMasterIdAlreadyPresent isEqualToString:[NSString stringWithFormat:@"%@",[dictDataRepairLaborMaster valueForKey:@"RepairLaborMasterId"]]]) {
            
            [arrOfRepairLaborMasterDcsToShow addObject:dictDataRepairLaborMaster];
            
        }else{
            
            cell.lblOption1.text=[NSString stringWithFormat:@"%@",[dictDataRepairLaborMaster valueForKey:@"RepairLaborTitle"]];
            
        }
        
    }
    
    // Here is the logic to show other options GBB
    
    if (cellRepairLaborId.length==0) {
        
        cell.lblOption1.text=[NSString stringWithFormat:@"%@",@"Default"];
        
        // [cell.btnOption1 setHidden:YES];
        [cell.btnOption2 setHidden:YES];
        [cell.btnOption3 setHidden:YES];
        [cell.lblOption2 setHidden:YES];
        [cell.lblOption3 setHidden:YES];
        [cell.lblOptionAmt1 setHidden:YES];
        [cell.lblOptionAmt2 setHidden:YES];
        [cell.lblValueForAmount2 setHidden:YES];
        [cell.lblValueForAmount3 setHidden:YES];
        [cell.lblOptionDesc2 setHidden:YES];
        [cell.lblOptionDesc3 setHidden:YES];
        [cell.lblValueForDesc2 setHidden:YES];
        [cell.lblValueForDesc3 setHidden:YES];
        [cell.lblLineSeprator setHidden:YES];
        [cell.lblOptionAmt1Total setHidden:NO];
        [cell.lblValueForAmount2Total setHidden:NO];
        [cell.lblOptionAmt2Total setHidden:YES];
        [cell.lblValueForAmount3Total setHidden:YES];
        
        NSString *strSTringUrl=[@"http://thrs.stagingsoftware.com//Documents//EmployeeSignatures/2702545479_20170511055323099_download (1).jpg" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [cell.imgView setImageWithURL:[NSURL URLWithString:strSTringUrl] placeholderImage:[UIImage imageNamed:@"NoImage.jpg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
    } else {
        
        [cell.btnOption1 setHidden:NO];
        [cell.btnOption2 setHidden:NO];
        [cell.btnOption3 setHidden:NO];
        
        [cell.lblOption2 setHidden:NO];
        [cell.lblOption3 setHidden:NO];
        [cell.lblOptionAmt1 setHidden:NO];
        [cell.lblOptionAmt2 setHidden:NO];
        [cell.lblValueForAmount2 setHidden:NO];
        [cell.lblValueForAmount3 setHidden:NO];
        [cell.lblOptionDesc2 setHidden:NO];
        [cell.lblOptionDesc3 setHidden:NO];
        [cell.lblValueForDesc2 setHidden:NO];
        [cell.lblValueForDesc3 setHidden:NO];
        [cell.lblLineSeprator setHidden:NO];
        [cell.lblOptionAmt1Total setHidden:NO];
        [cell.lblValueForAmount2Total setHidden:NO];
        [cell.lblOptionAmt2Total setHidden:YES];
        [cell.lblValueForAmount3Total setHidden:YES];
        
        // }
        
        
        if (arrOfRepairLaborMasterDcsToShow.count>=1) {
            
            // Index 2 pe value set
            NSDictionary *objTempNew=arrOfRepairLaborMasterDcsToShow[0];
            
            cellRepairMasterId2=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"RepairMasterId"]];
            cellRepairLaborId2=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"RepairLaborMasterId"]];
            
            arrOfTempPartsToShow=[[NSMutableArray alloc] init];
            
            strHelperHrs=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"HelperHours"]];
            strLaborHrs=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"LaborHours"]];
            if (strLaborHrs.length==0) {
                strLaborHrs=@"00:00";
            }
            if (strHelperHrs.length==0) {
                strHelperHrs=@"00:00";
            }
            
            cell.lblValueForLR2.text=[global ChangeDateFormatOnGeneralInfoTime :strLaborHrs];
            cell.lblValueForHLR2.text=[global ChangeDateFormatOnGeneralInfoTime :strHelperHrs];
            cell.lblOption2.text=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"RepairLaborTitle"]];
            cell.lblValueForDesc2.text=strLblDescNew;//[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"RepirLaborDesc"]];
            
            dictTempValues2=[self calculateLaborAmountFromMasters:objTempNew :strcostAdjustment :strPartCostAdjustmentGBB];
            
            float costOverAllToShow=[[dictTempValues2 valueForKey:@"overallcost"] floatValue];
            
            // Condition change for if laborcost is 0 then no cost adjustment is to be added

            if ([[dictTempValues2 valueForKey:@"laborcost"] floatValue]<=0) {
                
                costOverAllToShow = costOverAllToShow-[[dictTempValues2 valueForKey:@"laborcost"] floatValue];
                
            }
            
            cell.lblValueForAmount2.text=[NSString stringWithFormat:@"%.02f",costOverAllToShow];
            
            int repairQty = [strRepairQty intValue];
            
            float amountAfterAddingQty = [cell.lblValueForAmount2.text floatValue]*repairQty;
            
            cell.lblValueForAmount2Total.text=[NSString stringWithFormat:@"%.02f",amountAfterAddingQty];
            
            cell.lblValueForPart2.text=[arrOfTempPartsToShow componentsJoinedByString:@", "];
            
        } else {
            
            [cell.btnOption2 setHidden:YES];
            [cell.lblOption2 setHidden:YES];
            [cell.lblOptionAmt1 setHidden:YES];
            [cell.lblValueForAmount2 setHidden:YES];
            [cell.lblOptionDesc2 setHidden:YES];
            [cell.lblValueForDesc2 setHidden:YES];
            [cell.lblLineSeprator setHidden:YES];
            cell.btnViewMoreBetter.hidden=YES;
            [cell.lblOptionAmt1Total setHidden:YES];
            [cell.lblValueForAmount2Total setHidden:YES];

        }
        
        
        if (arrOfRepairLaborMasterDcsToShow.count>=2) {
            
            // Index 3 pe value set
            NSDictionary *objTempNew=arrOfRepairLaborMasterDcsToShow[1];
            
            cellRepairMasterId3=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"RepairMasterId"]];
            cellRepairLaborId3=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"RepairLaborMasterId"]];
            
            arrOfTempPartsToShow=[[NSMutableArray alloc] init];
            
            strHelperHrs=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"HelperHours"]];
            strLaborHrs=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"LaborHours"]];
            if (strLaborHrs.length==0) {
                strLaborHrs=@"00:00";
            }
            if (strHelperHrs.length==0) {
                strHelperHrs=@"00:00";
            }
            
            cell.lblValueForLR3.text=[global ChangeDateFormatOnGeneralInfoTime :strLaborHrs];
            cell.lblValueForHLR3.text=[global ChangeDateFormatOnGeneralInfoTime :strHelperHrs];//RepairLaborTitle
            cell.lblOption3.text=[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"RepairLaborTitle"]];
            cell.lblValueForDesc3.text=strLblDescNew;//[NSString stringWithFormat:@"%@",[objTempNew valueForKey:@"RepirLaborDesc"]];
            
            dictTempValues3=[self calculateLaborAmountFromMasters:objTempNew :strcostAdjustment :strPartCostAdjustmentGBB];
            
            float costOverAllToShow3=[[dictTempValues3 valueForKey:@"overallcost"] floatValue];
            
            // Condition change for if laborcost is 0 then no cost adjustment is to be added
            
            if ([[dictTempValues3 valueForKey:@"laborcost"] floatValue]<=0) {
                
                costOverAllToShow3 = costOverAllToShow3-[[dictTempValues3 valueForKey:@"laborcost"] floatValue];
                
            }
            
            cell.lblValueForAmount3.text=[NSString stringWithFormat:@"%.02f",costOverAllToShow3];
            
            int repairQty = [strRepairQty intValue];
            
            float amountAfterAddingQty = [cell.lblValueForAmount3.text floatValue]*repairQty;
            
            cell.lblValueForAmount3Total.text=[NSString stringWithFormat:@"%.02f",amountAfterAddingQty];
            
            
            cell.lblValueForPart3.text=[arrOfTempPartsToShow componentsJoinedByString:@", "];
            
            
        } else {
            
            [cell.btnOption3 setHidden:YES];
            [cell.lblOption3 setHidden:YES];
            [cell.lblOptionAmt2 setHidden:YES];
            [cell.lblValueForAmount3 setHidden:YES];
            [cell.lblOptionDesc3 setHidden:YES];
            [cell.lblValueForDesc3 setHidden:YES];
            [cell.lblLineSeprator3 setHidden:YES];
            cell.btnViewMoreBest.hidden=YES;
            [cell.lblOptionAmt2Total setHidden:YES];
            [cell.lblValueForAmount3Total setHidden:YES];

        }
        
        
        //    NSString *strSTringUrl=[@"http://thrs.stagingsoftware.com//Documents//EmployeeSignatures/2702545479_20170511055323099_download (1).jpg" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //
        //    [cell.imgView setImageWithURL:[NSURL URLWithString:strSTringUrl] placeholderImage:[UIImage imageNamed:@"NoImage.jpg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        
    }
    NSMutableArray *arrTempToCheckSelectedGBB=arrOfIndexSelectedGBB[indexPath.section];
    
    NSString *strIndex=[NSString stringWithFormat:@"%@",arrTempToCheckSelectedGBB[indexPath.row]];
    
    
    if ([cellRepairLaborId isEqualToString:strIndex]) {
        
        strIndex=@"0";
        
    } else if ([cellRepairLaborId2 isEqualToString:strIndex]){
        
        strIndex=@"1";
        
    } else if ([cellRepairLaborId3 isEqualToString:strIndex]){
        
        strIndex=@"2";
        
    }
    
    int indexSelectedGBB=[strIndex intValue];
    
    NSString *strCustomerFeedback=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"customerFeedback"]];
    
    if (indexSelectedGBB==0) {
        
        [cell.btnOption1 setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
        [cell.btnOption2 setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
        [cell.btnOption3 setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
        
        
        //yaha pe check krna hai konsi id and replace krna use vapis
        
        for (int k2=0; k2<arrOfReapairMasterIdInDb.count; k2++) {
            
            NSString *strBothIds=arrOfReapairMasterIdInDb[k2];
            
            NSArray *strings = [strBothIds componentsSeparatedByString:@","];
            
            NSString *strIssuesIdToCheckLocal=strings[2];
            
            NSString *strIssueRepairIdToCheckLocal=strings[3];
            
            if ([strIssuesIdToCheckLocal isEqualToString:strIssueIdToCheck] && [strIssueRepairIdToCheckLocal isEqualToString:strIssueRepairIdToCheck]) {
                
                NSString *strReapairnLaborMasterId=[NSString stringWithFormat:@"%@,%@,%@,%@",cellRepairMasterId,cellRepairLaborId,strIssuesIdToCheckLocal,strIssueRepairIdToCheckLocal];
                
                [arrOfReapairMasterIdInDb replaceObjectAtIndex:k2 withObject:strReapairnLaborMasterId];
                
                break;
            }
            
            
        }
        
        if ([strCustomerFeedback isEqualToString:@"true"] || [strCustomerFeedback isEqualToString:@"1"]) {
            
            NSString *strKey=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
            
            [dictAmountTotalRepairs removeObjectForKey:strKey];
            
            //[dictAmountTotalRepairs setObject:cell.lblValueForAmount.text forKey:strKey];
            [dictAmountTotalRepairs setObject:cell.lblValueForAmountTotal.text forKey:strKey];

            [dictOfAllAmountSeparately removeObjectForKey:strKey];
            
            [dictOfAllAmountSeparately setObject:dictTempValues forKey:strKey];
            
            
        }else{
            
            NSString *strKey=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
            
            [dictAmountTotalRepairs removeObjectForKey:strKey];
            
            [dictOfAllAmountSeparately removeObjectForKey:strKey];
        }
        
    } else if (indexSelectedGBB==1){
        
        [cell.btnOption1 setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
        [cell.btnOption2 setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
        [cell.btnOption3 setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
        
        
        //yaha pe check krna hai konsi id and replace krna use vapis
        
        for (int k2=0; k2<arrOfReapairMasterIdInDb.count; k2++) {
            
            NSString *strBothIds=arrOfReapairMasterIdInDb[k2];
            
            NSArray *strings = [strBothIds componentsSeparatedByString:@","];
            
            NSString *strIssuesIdToCheckLocal=strings[2];
            
            NSString *strIssueRepairIdToCheckLocal=strings[3];
            
            if ([strIssuesIdToCheckLocal isEqualToString:strIssueIdToCheck] && [strIssueRepairIdToCheckLocal isEqualToString:strIssueRepairIdToCheck]) {
                
                NSString *strReapairnLaborMasterId=[NSString stringWithFormat:@"%@,%@,%@,%@",cellRepairMasterId2,cellRepairLaborId2,strIssuesIdToCheckLocal,strIssueRepairIdToCheckLocal];
                
                [arrOfReapairMasterIdInDb replaceObjectAtIndex:k2 withObject:strReapairnLaborMasterId];
                
                break;
            }
            
            
        }
        
        if ([strCustomerFeedback isEqualToString:@"true"] || [strCustomerFeedback isEqualToString:@"1"]) {
            
            NSString *strKey=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
            
            [dictAmountTotalRepairs removeObjectForKey:strKey];
            
            //[dictAmountTotalRepairs setObject:cell.lblValueForAmount2.text forKey:strKey];
            [dictAmountTotalRepairs setObject:cell.lblValueForAmount2Total.text forKey:strKey];

            [dictOfAllAmountSeparately removeObjectForKey:strKey];
            [dictOfAllAmountSeparately setObject:dictTempValues2 forKey:strKey];
            
        }else{
            
            NSString *strKey=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
            
            [dictAmountTotalRepairs removeObjectForKey:strKey];
            [dictOfAllAmountSeparately removeObjectForKey:strKey];
            
        }
        
    } else if (indexSelectedGBB==2){
        
        [cell.btnOption1 setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
        [cell.btnOption2 setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
        [cell.btnOption3 setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
        
        
        //yaha pe check krna hai konsi id and replace krna use vapis
        
        for (int k2=0; k2<arrOfReapairMasterIdInDb.count; k2++) {
            
            NSString *strBothIds=arrOfReapairMasterIdInDb[k2];
            
            NSArray *strings = [strBothIds componentsSeparatedByString:@","];
            
            NSString *strIssuesIdToCheckLocal=strings[2];
            
            NSString *strIssueRepairIdToCheckLocal=strings[3];
            
            if ([strIssuesIdToCheckLocal isEqualToString:strIssueIdToCheck] && [strIssueRepairIdToCheckLocal isEqualToString:strIssueRepairIdToCheck]) {
                
                NSString *strReapairnLaborMasterId=[NSString stringWithFormat:@"%@,%@,%@,%@",cellRepairMasterId3,cellRepairLaborId3,strIssuesIdToCheckLocal,strIssueRepairIdToCheckLocal];
                
                [arrOfReapairMasterIdInDb replaceObjectAtIndex:k2 withObject:strReapairnLaborMasterId];
                
                break;
            }
            
            
        }
        
        if ([strCustomerFeedback isEqualToString:@"true"] || [strCustomerFeedback isEqualToString:@"1"]) {
            
            NSString *strKey=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
            
            [dictAmountTotalRepairs removeObjectForKey:strKey];
            
            //[dictAmountTotalRepairs setObject:cell.lblValueForAmount3.text forKey:strKey];
            [dictAmountTotalRepairs setObject:cell.lblValueForAmount3Total.text forKey:strKey];

            [dictOfAllAmountSeparately removeObjectForKey:strKey];
            [dictOfAllAmountSeparately setObject:dictTempValues3 forKey:strKey];
            
        }else{
            
            NSString *strKey=[NSString stringWithFormat:@"%d",indexPath.row*1000+indexPath.section];
            
            [dictAmountTotalRepairs removeObjectForKey:strKey];
            [dictOfAllAmountSeparately removeObjectForKey:strKey];
            
        }
        
    }
    
    cell.btnOption1.tag=indexPath.row*1000+indexPath.section;
    cell.btnOption2.tag=indexPath.row*1000+indexPath.section;
    cell.btnOption3.tag=indexPath.row*1000+indexPath.section;
    
    [cell.btnOption1 addTarget:self action:@selector(action_btnOption1:) forControlEvents:UIControlEventTouchDown];
    [cell.btnOption2 addTarget:self action:@selector(action_btnOption2:) forControlEvents:UIControlEventTouchDown];
    [cell.btnOption3 addTarget:self action:@selector(action_btnOption3:) forControlEvents:UIControlEventTouchDown];
    
    [self methodTotalRepairCalculateNew];
    [self methodToCalulateFRTotal];
    [self methodCalculateFRTotalNewPlan];
    
}

-(void)methodToCalulateFRTotal{
    
    double sumTotal=0;
    double subTotal,tax,amountDue;
    amountDue=0;
    
    NSArray *arrValues=[dictAmountTotalRepairs allValues];
    
    double sum=0;
    
    for (int k=0; k<arrValues.count; k++) {
        
        sum=sum+[arrValues[k] doubleValue];
        
    }
    sumTotal=sumTotal+sum;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se

    subTotal=sumTotal-[_txtOtherDiscounts.text doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForMembershipPlanCharges.text]-[self getNumberString:_lblValueForMembershipPlanSavings.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txtMiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    // strTax=@"10";
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    tax=[self taxCalculation];
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
        
    }
    
    
    //Nilind 19 July
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    //End
    
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        // _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        
        
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    [self methodToSetAmountFinally];
}

-(void)setAmountInitially{
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        
        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        
    }
    
}

-(void)showAmountInTextFieldToCharge{
    
    if (isOldPlan) {
        
        if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
        {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        }
        else
        {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        }
        
    } else {
        
        if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
        {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        }
        else
        {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    NSLog(@"amount is equal to======%@---%@",_txtAmountSingleAmount.text,_txtAmountCheckView.text);
    
    
}


-(void)isinitalyyAmountSet{
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        
        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        
    }
    
}

-(void)methodCalculateFRTotalNewPlan{
    
    double sumTotalNewPlan=0;
    double subTotalNewPlan,taxNewPlan,amountDueNewPlan;
    amountDueNewPlan=0;
    
    NSArray *arrValues=[dictAmountTotalRepairs allValues];
    
    double sum=0;
    
    for (int k=0; k<arrValues.count; k++) {
        
        sum=sum+[arrValues[k] doubleValue];
        
    }
    sumTotalNewPlan=sumTotalNewPlan+sum;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se

    subTotalNewPlan=sumTotalNewPlan-[_txtOtherDiscountNewPlan.text doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];
    if (subTotalNewPlan>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotalNewPlan];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotalNewPlan=0.00;
    }
    
    
    double taxValueNewPlan;
    // strTax=@"10";
    taxValueNewPlan=[strTax doubleValue];
    taxNewPlan=(subTotalNewPlan*taxValueNewPlan)/100;
    taxNewPlan=[self taxCalculationNewPlan];
    
    if (taxNewPlan>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",taxNewPlan];
        
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        taxNewPlan=0.00;
    }
    
    
    //Nilind 19 July
    
    double totalNewNewPlan;
    totalNewNewPlan=subTotalNewPlan+taxNewPlan;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNewNewPlan];
    
    //End
    
    
    double  finalTotalWithTaxNewPlan;
    finalTotalWithTaxNewPlan=subTotalNewPlan+taxNewPlan;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        // _lblAmountPaid.hidden=NO;
        //_lblValueForAmountPaid.hidden=NO;
        // _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTaxNewPlan];
    }
    else
    {
        // _lblAmountPaid.hidden=NO;
        // _lblValueForAmountPaid.hidden=NO;
        
        amountDueNewPlan=[[NSString stringWithFormat:@"%.02f",finalTotalWithTaxNewPlan]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaidNewPlan;
        
        amountPaidNewPlan=[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountPaidNewPlan];
        
        
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDueNewPlan];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    
    [self methodToSetAmountFinally];
    
}

-(void)action_btnOption1:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    
    int row=(int)btn.tag/1000;
    
    int section=(int)btn.tag%1000;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp1=arrTemp[row];
    
    NSMutableArray *arrTempToCheckSelectedGBB=arrOfIndexSelectedGBB[section];
    
    [arrTempToCheckSelectedGBB replaceObjectAtIndex:row withObject:[objTemp1 valueForKey:@"repairLaborId"]];
    
    [arrOfIndexSelectedGBB replaceObjectAtIndex:section withObject:arrTempToCheckSelectedGBB];
    
    [_tblViewServiceIssue reloadData];
    
    [self methodCalculationOverAll];
}

-(void)methodTotalRepairCalculateNew{
    
    NSArray *arrValues=[dictAmountTotalRepairs allValues];
    
    double sum=0;
    
    for (int k=0; k<arrValues.count; k++) {
        
        sum=sum+[arrValues[k] doubleValue];
        
    }
    
    _lblValueTotalApprovedRepairs.text=[NSString stringWithFormat:@"$%.2f",sum];
    _lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"$%.2f",sum];
    
#pragma mark- NILIND
    //[self getPlan];
    //End
    [self taxCalculation];
    [self taxCalculationNewPlan];
    
    [self methodToCalulateMembershipCharge];
    
}

-(void)methodToCalulateMembershipCharge{
    
    
    if ((strAccountPSPId.length==0) || (strAccountPSPId==0)) {
        
        double memberShipDiscout,memberShipCharge,value;
        memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
        memberShipDiscout=[strPSPDiscountPercent1 doubleValue];
        
        value=memberShipCharge*(memberShipDiscout/100);
        strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
        _lblValueForMembershipPlanSavings.text=[NSString stringWithFormat:@"$%.02f",value];
        
        if (strPSPCharge1.length>0) {
            
            //  _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",strPSPCharge1];
            
        }else{
            
            // _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",@"00.00"];
            
        }
        //        if ([strAccountPSPId1 isEqualToString:@"0"]) {
        //
        //            _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",strPSPCharge1];
        //
        //        }
        
        
    } else {
        
        double memberShipDiscout,memberShipCharge,value;
        memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
        memberShipDiscout=[strPSPDiscountPercent1 doubleValue];
        
        value=memberShipCharge*(memberShipDiscout/100);
        strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
        _lblValueForMembershipPlanSavings.text=[NSString stringWithFormat:@"$%.02f",value];
        _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",@"00.00"];
        strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
        
    }
    
    
    double memberShipDiscout2,memberShipCharge2,value2;
    memberShipCharge2=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
    memberShipDiscout2=[strPSPDiscountPercent2 doubleValue];
    
    value2=memberShipCharge2*(memberShipDiscout2/100);
    strPSPDiscountAmount2=[NSString stringWithFormat:@"%.02f",value2];
    _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value2];
    
    
    _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",strPSPCharge2];
    double tempValueTemporary=[strPSPCharge2 doubleValue];
    _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary];
    
    
}

-(double)taxCalculation{
    
    double tax=0.0;
    
    NSArray *arrOfTaxMaster=[global getSalesTaxFromMaster:strCompanyKeyy :strDepartmentSysName :[matchesFinalSubWorkorder valueForKey:@"propertyType"]];
    
    if (!(arrOfTaxMaster.count==0)) {
        
        //Check According To Condition to apply tax on what
        
        BOOL isLaborTax=NO,isMaterialTax=NO,isVendorPayTax=NO,isFullTax=NO;
        
        NSString *strPartPriceType;
        
        NSDictionary *dictTaxData=arrOfTaxMaster[0];
        
        NSString *strVendorPayTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"VendorPayTax"]];
        
        if ([strVendorPayTax isEqualToString:@"true"] || [strVendorPayTax isEqualToString:@"True"] || [strVendorPayTax isEqualToString:@"1"]) {
            
            isVendorPayTax=YES;
            
        }
        
        if ([strWorkOrderAddressSubType isEqualToString:@"Commercial"]) {//Commercial Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMPartPriceType"]];
                
            }
            
        } else {//Resedential Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMPartPriceType"]];
                
            }
            
        }
        
        
        
        if (isFullTax) {
            
            float subTotal=0.0;
            
            NSArray *arrOfAllDict=[dictOfAllAmountSeparately allValues];
            
            if ([arrOfAllDict isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfAllDict.count; k++) {
                    
                    NSDictionary *dictDataTemp=arrOfAllDict[k];
                    
                    if ([dictDataTemp isKindOfClass:[NSString class]] || (dictDataTemp==nil)) {
                        
                        
                        
                    } else {
                        
                        float overallcost=[[dictDataTemp valueForKey:@"overallcost"] floatValue];
                        
                        subTotal=subTotal+overallcost;
                        
                    }
                    
                }
                
                tax=[self calculationOverAllNewReturn:subTotal];
                
            }
            
            
        } else {
            
            float subTotal=0.0,subTotalLabor=0.0,subTotalMaterial=0.0,costAdj=0.0,costPartAdj=0.0,subTotalMaterialForDiscount=0.0;
            
            NSArray *arrOfAllDict=[dictOfAllAmountSeparately allValues];
            
            if ([arrOfAllDict isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfAllDict.count; k++) {
                    
                    NSDictionary *dictDataTemp=arrOfAllDict[k];
                    
                    if ([dictDataTemp isKindOfClass:[NSString class]] || (dictDataTemp==nil)) {
                        
                        
                        
                    } else {
                        
                        float overallcost=[[dictDataTemp valueForKey:@"overallcost"] floatValue];
                        
                        subTotal=subTotal+overallcost;
                        
                        float overallcostLabor=[[dictDataTemp valueForKey:@"laborcost"] floatValue];
                        
                        subTotalLabor=subTotalLabor+overallcostLabor;
                        
                        float overallcostMaterial=[[dictDataTemp valueForKey:@"partcost"] floatValue];
                        
                        if ([strPartPriceType isEqualToString:@"Sales Price"]) {
                            
                            overallcostMaterial=[[dictDataTemp valueForKey:@"partcostsales"] floatValue];
                            
                        }
                        subTotalMaterial=subTotalMaterial+overallcostMaterial;
                        
                        subTotalMaterialForDiscount=subTotalMaterialForDiscount+[[dictDataTemp valueForKey:@"partcostsales"] floatValue];
                        
                        float overallcostAdj=[[dictDataTemp valueForKey:@"costAdjustment"] floatValue];
                        
                        costAdj=costAdj+overallcostAdj;
                        
                        // Condition change for if laborcost is 0 then no cost adjustment is to be added
                        
                        if (overallcostLabor<=0) {
                            
                            costAdj = costAdj - overallcostAdj;
                            
                        }
                        
                        float overallcostPartAdj=[[dictDataTemp valueForKey:@"partCostAdjustment"] floatValue];
                        
                        costPartAdj=costPartAdj+overallcostPartAdj;
                        
                        
                    }
                    
                }
                
//                // Akshay Start //
//                totalLaborAmountLocal = subTotalLabor;
//                totalPartAmountLocal =  subTotalMaterialForDiscount;
//                totalLaborAmountLocalCopy = subTotalLabor;
//                totalPartAmountLocalCopy = subTotalMaterialForDiscount;
//                // Akshay End //
                
                tax=[self calculationOverAllNewLaborMaterialReturn:subTotal :subTotalLabor :subTotalMaterial :isLaborTax :isMaterialTax :costAdj :costPartAdj];
                
            }
            
        }
        
    }
    
    return tax;
}



-(double)taxCalculationNewPlan{
    
    double tax=0.0;
    
    NSArray *arrOfTaxMaster=[global getSalesTaxFromMaster:strCompanyKeyy :strDepartmentSysName :[matchesFinalSubWorkorder valueForKey:@"propertyType"]];
    
    if (!(arrOfTaxMaster.count==0)) {
        
        //Check According To Condition to apply tax on what
        
        BOOL isLaborTax=NO,isMaterialTax=NO,isVendorPayTax=NO,isFullTax=NO;
        
        NSString *strPartPriceType;
        
        NSDictionary *dictTaxData=arrOfTaxMaster[0];
        
        NSString *strVendorPayTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"VendorPayTax"]];
        
        if ([strVendorPayTax isEqualToString:@"true"] || [strVendorPayTax isEqualToString:@"True"] || [strVendorPayTax isEqualToString:@"1"]) {
            
            isVendorPayTax=YES;
            
        }
        
        if ([strWorkOrderAddressSubType isEqualToString:@"Commercial"]) {//Commercial Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMPartPriceType"]];
                
            }
            
        } else {//Resedential Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMPartPriceType"]];
                
            }
            
        }
        
        
        
        if (isFullTax) {
            
            float subTotal=0.0;
            
            NSArray *arrOfAllDict=[dictOfAllAmountSeparately allValues];
            
            if ([arrOfAllDict isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfAllDict.count; k++) {
                    
                    NSDictionary *dictDataTemp=arrOfAllDict[k];
                    
                    if ([dictDataTemp isKindOfClass:[NSString class]] || (dictDataTemp==nil)) {
                        
                        
                        
                    } else {
                        
                        float overallcost=[[dictDataTemp valueForKey:@"overallcost"] floatValue];
                        
                        subTotal=subTotal+overallcost;
                        
                    }
                    
                }
                
                tax=[self calculationOverAllNewReturnNewPlan:subTotal];
                
            }
            
            
        } else {
            
            float subTotal=0.0,subTotalLabor=0.0,subTotalMaterial=0.0,costAdj=0.0,costPartAdj=0.0,subTotalMaterialForDiscount=0.0;
            
            NSArray *arrOfAllDict=[dictOfAllAmountSeparately allValues];
            
            if ([arrOfAllDict isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfAllDict.count; k++) {
                    
                    NSDictionary *dictDataTemp=arrOfAllDict[k];
                    
                    if ([dictDataTemp isKindOfClass:[NSString class]] || (dictDataTemp==nil)) {
                        
                        
                        
                    } else {
                        
                        float overallcost=[[dictDataTemp valueForKey:@"overallcost"] floatValue];
                        
                        subTotal=subTotal+overallcost;
                        
                        float overallcostLabor=[[dictDataTemp valueForKey:@"laborcost"] floatValue];
                        
                        subTotalLabor=subTotalLabor+overallcostLabor;
                        
                        float overallcostMaterial=[[dictDataTemp valueForKey:@"partcost"] floatValue];
                        
                        if ([strPartPriceType isEqualToString:@"Sales Price"]) {
                            
                            overallcostMaterial=[[dictDataTemp valueForKey:@"partcostsales"] floatValue];
                            
                        }
                        subTotalMaterial=subTotalMaterial+overallcostMaterial;
                        
                        subTotalMaterialForDiscount=subTotalMaterialForDiscount+[[dictDataTemp valueForKey:@"partcostsales"] floatValue];
                        
                        float overallcostAdj=[[dictDataTemp valueForKey:@"costAdjustment"] floatValue];
                        
                        costAdj=costAdj+overallcostAdj;
                        
                        // Condition change for if laborcost is 0 then no cost adjustment is to be added
                        
                        if (overallcostLabor<=0) {
                            
                            costAdj = costAdj - overallcostAdj;
                            
                        }
                        
                        float overallcostPartAdj=[[dictDataTemp valueForKey:@"partCostAdjustment"] floatValue];
                        
                        costPartAdj=costPartAdj+overallcostPartAdj;
                        
                        
                    }
                    
                }
                
//                // Akshay Start //
//                totalLaborAmountLocal = subTotalLabor;
//                totalPartAmountLocal =  subTotalMaterialForDiscount;
//                totalLaborAmountLocalCopy = subTotalLabor;
//                totalPartAmountLocalCopy = subTotalMaterialForDiscount;
//                // Akshay End //
                
                tax=[self calculationOverAllNewLaborMaterialReturnNewPlan:subTotal :subTotalLabor :subTotalMaterial :isLaborTax :isMaterialTax :costAdj :costPartAdj];
                
            }
            
            
            
        }
        
    }
    
    return tax;
}

-(void)calculationOverAllNew :(float)subTotal{
    
    double tax,amountDue;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    strDiscount=strDiscountGlobal;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se

    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForMembershipPlanCharges.text]-[self getNumberString:_lblValueForMembershipPlanSavings.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txtMiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    [self methodToSetAmountFinally];
}

-(void)calculationOverAllNewLaborMaterial :(float)subTotal :(float)subTotalLabor :(float)subTotalMaterial :(BOOL)isLaborTax :(BOOL)isMaterialTax :(float)strCostAdj :(float)strPartCostAdj{
    
    double tax,amountDue,taxMaterial = 0.0,taxLabor = 0.0;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    strDiscount=strDiscountGlobal;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se

    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForMembershipPlanCharges.text]-[self getNumberString:_lblValueForMembershipPlanSavings.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txtMiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    
    
    if (isLaborTax) {
        
        float laborAppliedCredits = 0.0;
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            if([[dict valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnLaborPrice"] boolValue]==true)
            {
                laborAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
        }
        
        //[self calculateTripChargeBasedOnQty];
        
        // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se

        float subTotalLaborTemp=[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForMembershipPlanCharges.text]-[self getNumberString:_lblValueForMembershipPlanSavings.text]+[self getNumberString:_lblMileage.text]+subTotalLabor+strCostAdj+[self getNumberString:_txtMiscellaneousCharge.text]-([strDiscount doubleValue]+laborAppliedCredits);  //-[strDiscount doubleValue]
        
        if (subTotalLaborTemp>0) {
            
            taxLabor=(subTotalLaborTemp*taxValue)/100;
            
        }
        
    }
    
    if (isMaterialTax) {
        
        float subTotalMaterialTemp=subTotalMaterial+strPartCostAdj;
        
        if (subTotalMaterialTemp>0) {
            
            taxMaterial=(subTotalMaterialTemp*taxValue)/100;
            
        }
        
    }
    
    
    tax=taxMaterial+taxLabor;
    
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    [self methodToSetAmountFinally];
}


-(double)calculationOverAllNewReturn :(float)subTotal{
    
    double tax= 0.0,amountDue;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    strDiscount=strDiscountGlobal;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se

    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForMembershipPlanCharges.text]-[self getNumberString:_lblValueForMembershipPlanSavings.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txtMiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    [self methodToSetAmountFinally];
    return tax;
}

-(double)calculationOverAllNewLaborMaterialReturn :(float)subTotal :(float)subTotalLabor :(float)subTotalMaterial :(BOOL)isLaborTax :(BOOL)isMaterialTax :(float)strCostAdj :(float)strPartCostAdj{
    
    double tax = 0.0,amountDue,taxMaterial = 0.0,taxLabor = 0.0;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    strDiscount=strDiscountGlobal;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se

    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForMembershipPlanCharges.text]-[self getNumberString:_lblValueForMembershipPlanSavings.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txtMiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    
    
    if (isLaborTax) {
        
        float laborAppliedCredits = 0.0;
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            if([[dict valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnLaborPrice"] boolValue]==true)
            {
                laborAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
        }
        
        //[self calculateTripChargeBasedOnQty];
        
        // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se

        float subTotalLaborTemp=[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForMembershipPlanCharges.text]-[self getNumberString:_lblValueForMembershipPlanSavings.text]+[self getNumberString:_lblMileage.text]+subTotalLabor+strCostAdj+[self getNumberString:_txtMiscellaneousCharge.text]-([strDiscount doubleValue]+laborAppliedCredits); //-[strDiscount doubleValue]
        
        if (subTotalLaborTemp>0) {
            
            taxLabor=(subTotalLaborTemp*taxValue)/100;
            
        }
        
    }
    
    if (isMaterialTax) {
        
        float subTotalMaterialTemp=subTotalMaterial+strPartCostAdj;
        
        if (subTotalMaterialTemp>0) {
            
            taxMaterial=(subTotalMaterialTemp*taxValue)/100;
            
        }
        
    }
    
    
    tax=taxMaterial+taxLabor;
    
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    [self methodToSetAmountFinally];
    return tax;
}

-(double)calculationOverAllNewReturnNewPlan :(float)subTotal{
    
    double tax= 0.0,amountDue;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    strDiscount=strDiscountGlobalNewPlan;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se

    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];
    if (subTotal>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    if (tax>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    [self methodToSetAmountFinally];
    return tax;
}

-(double)calculationOverAllNewLaborMaterialReturnNewPlan :(float)subTotal :(float)subTotalLabor :(float)subTotalMaterial :(BOOL)isLaborTax :(BOOL)isMaterialTax :(float)strCostAdj :(float)strPartCostAdj{
    
    double tax = 0.0,amountDue,taxMaterial = 0.0,taxLabor = 0.0;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscountNewPlan.text]];
    }
    
    strDiscount=strDiscountGlobalNewPlan;
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se

    subTotal=subTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];
    if (subTotal>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    
    
    if (isLaborTax) {
        
        float laborAppliedCredits = 0.0;
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            if([[dict valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"]||[[dict valueForKey:@"applyOnLaborPrice"] boolValue]==true)
            {
                laborAppliedCredits = laborAppliedCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
        }
        
        //[self calculateTripChargeBasedOnQty];
        
        // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se

        float subTotalLaborTemp=[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+subTotalLabor+strCostAdj+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-([strDiscount doubleValue]+laborAppliedCredits); //-[strDiscount doubleValue]
        
        if (subTotalLaborTemp>0) {
            
            taxLabor=(subTotalLaborTemp*taxValue)/100;
            
        }
        
    }
    
    if (isMaterialTax) {
        
        float subTotalMaterialTemp=subTotalMaterial+strPartCostAdj;
        
        if (subTotalMaterialTemp>0) {
            
            taxMaterial=(subTotalMaterialTemp*taxValue)/100;
            
        }
        
    }
    
    
    tax=taxMaterial+taxLabor;
    
    if (tax>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    [self methodToSetAmountFinally];
    return tax;
}


-(double)taxCalculationForParts :(double)totalPrice :(NSString*)strTypeOfPlan{
    
    double tax=0.0;
    
    NSArray *arrOfTaxMaster=[global getSalesTaxFromMaster:strCompanyKeyy :strDepartmentSysName :[matchesFinalSubWorkorder valueForKey:@"propertyType"]];
    
    if (!(arrOfTaxMaster.count==0)) {
        
        //Check According To Condition to apply tax on what
        
        BOOL isLaborTax=NO,isMaterialTax=NO,isVendorPayTax=NO,isFullTax=NO;
        
        NSString *strPartPriceType;
        
        NSDictionary *dictTaxData=arrOfTaxMaster[0];
        
        NSString *strVendorPayTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"VendorPayTax"]];
        
        if ([strVendorPayTax isEqualToString:@"true"] || [strVendorPayTax isEqualToString:@"True"] || [strVendorPayTax isEqualToString:@"1"]) {
            
            isVendorPayTax=YES;
            
        }
        
        if ([strWorkOrderAddressSubType isEqualToString:@"Commercial"]) {//Commercial Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"CommTMPartPriceType"]];
                
            }
            
        } else {//Resedential Type
            
            if ([strWoType isEqualToString:@"FR"]) //Flat Rate Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResFRPartPriceType"]];
                
            }
            else if ([strWoType isEqualToString:@"TM"]) //Time and Material Type
            {
                
                NSString *strFullTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMFullTax"]];
                
                if ([strFullTax isEqualToString:@"true"] || [strFullTax isEqualToString:@"True"] || [strFullTax isEqualToString:@"1"]) {
                    
                    isFullTax=YES;
                    
                }
                
                NSString *strLaborTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMLaborTax"]];
                
                if ([strLaborTax isEqualToString:@"true"] || [strLaborTax isEqualToString:@"True"] || [strLaborTax isEqualToString:@"1"]) {
                    
                    isLaborTax=YES;
                    
                }
                
                NSString *strMaterialTax=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMMaterialTax"]];
                
                if ([strMaterialTax isEqualToString:@"true"] || [strMaterialTax isEqualToString:@"True"] || [strMaterialTax isEqualToString:@"1"]) {
                    
                    isMaterialTax=YES;
                    
                }
                
                strPartPriceType=[NSString stringWithFormat:@"%@",[dictTaxData valueForKey:@"ResTMPartPriceType"]];
                
            }
            
        }
        
        
        
        if (isFullTax) {
            
            double taxValue;
            taxValue=[strTax doubleValue];
            tax=(totalPrice*taxValue)/100;
            
        } else {
            
            tax=[self calculationTaxForMaterialnLaborForParts :isLaborTax :isMaterialTax :strPartPriceType :strTypeOfPlan];
            
        }
        
    }
    
    return tax;
}

-(double)calculationTaxForMaterialnLaborForParts :(BOOL)isLaborTax :(BOOL)isMaterialTax :(NSString*)strTypeOfprice :(NSString*)strTypeOfPlan{
    
    float totalCostPartss=0.0,totalCostPartsSales=0;
    NSMutableArray *arrTotal,*arrTemp;
    arrTotal=[[NSMutableArray alloc]init];
    arrTemp=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrOfSubWorkServiceIssues.count; i++)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[i];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                
            }
        }
    }
    
    for (int k=0; k<arrTemp.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrTemp[k];
        
        if ([[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"1"]|| [[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@""])
        {
            NSString *strMulti=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"multiplier"]];
            float multip=[strMulti floatValue];
            
            NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"qty"]];
            float qtyy=[strqtyy floatValue];
            
            NSString *strunitPricee=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"unitPrice"]];
            float unitPricee=[strunitPricee floatValue];
            float costPriceParts=unitPricee*qtyy;
            float finalPriceParts=unitPricee*qtyy*multip;
            totalCostPartss=totalCostPartss+costPriceParts;
            totalCostPartsSales=totalCostPartsSales+finalPriceParts;
            
        }
        
    }
    
    double tax = 0.0,amountDue,taxMaterial = 0.0,taxLabor = 0.0;
    amountDue=0;
    
    NSString *strDiscount;
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    strDiscount=strDiscountGlobal;
    
    if ([strTypeOfPlan isEqualToString:@"NewPlan"]) {
        
        if (chkTextEditNewPlan==NO)
        {
            strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscountNewPlan.text]];
        }
        strDiscount=strDiscountGlobalNewPlan;
        
        
    }
    
    double taxValue;
    taxValue=[strTax doubleValue];
    
    if (isMaterialTax) {
        
        float subTotalMaterialTemp=totalCostPartss;
        
        if ([strTypeOfprice isEqualToString:@"Sales Price"]) {
            
            subTotalMaterialTemp=totalCostPartsSales;
            
        }
        
        if (subTotalMaterialTemp>0) {
            
            taxMaterial=(subTotalMaterialTemp*taxValue)/100;
            
        }
        
    }
    
    tax=taxMaterial+taxLabor;
    
    
//    // Akshay Start //
//    totalLaborAmountLocal = 0.0;
//    totalPartAmountLocal =  totalCostPartsSales;
//    totalLaborAmountLocalCopy = 0.0;
//    totalPartAmountLocalCopy = totalCostPartsSales;
//    // Akshay End //
    
    return tax;
    
}

-(void)action_btnOption2:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    
    int row=(int)btn.tag/1000;
    
    int section=(int)btn.tag%1000;
    
    NSString *strRepairMasterIdAlreadyPresent,*strRepairLaborMasterIdAlreadyPresent;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp1=arrTemp[row];
    strRepairMasterIdAlreadyPresent=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairMasterId"]];
    
    strRepairLaborMasterIdAlreadyPresent=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairLaborId"]];
    
    //Change For Other Option then selected
    
    NSMutableArray *arrOfRepairLaborMasterDcs=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfRepairMasters.count; k++) {
        
        NSDictionary *dictDataRepair1=arrOfRepairMasters[k];
        
        NSString *strId=[NSString stringWithFormat:@"%@",[dictDataRepair1 valueForKey:@"RepairMasterId"]];
        
        if ([strRepairMasterIdAlreadyPresent isEqualToString:strId]) {
            
            NSArray *arrTemps=[dictDataRepair1 valueForKey:@"RepairLaborMasterDcs"];
            
            //RepairLaborMasterDcs
            [arrOfRepairLaborMasterDcs addObjectsFromArray:arrTemps];
            
            break;
        }
        
    }
    
    NSMutableArray *arrOfRepairLaborMasterDcsToShow=[[NSMutableArray alloc]init];
    
    for (int j=0; j<arrOfRepairLaborMasterDcs.count; j++) {
        
        NSDictionary *dictDataRepairLaborMaster=arrOfRepairLaborMasterDcs[j];
        
        if (![strRepairLaborMasterIdAlreadyPresent isEqualToString:[NSString stringWithFormat:@"%@",[dictDataRepairLaborMaster valueForKey:@"RepairLaborMasterId"]]]) {
            
            [arrOfRepairLaborMasterDcsToShow addObject:dictDataRepairLaborMaster];
            
        }else{
            
            
        }
        
    }
    
    // Here is the logic to show other options GBB
    
    // Index 2 pe value set
    NSDictionary *objTempNew=arrOfRepairLaborMasterDcsToShow[0];
    
    NSMutableArray *arrTempToCheckSelectedGBB=arrOfIndexSelectedGBB[section];
    
    [arrTempToCheckSelectedGBB replaceObjectAtIndex:row withObject:[objTempNew valueForKey:@"RepairLaborMasterId"]];
    
    [arrOfIndexSelectedGBB replaceObjectAtIndex:section withObject:arrTempToCheckSelectedGBB];
    
    [_tblViewServiceIssue reloadData];
    
    [self methodCalculationOverAll];
}
-(void)action_btnOption3:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    
    int row=(int)btn.tag/1000;
    
    int section=(int)btn.tag%1000;
    
    NSString *strRepairMasterIdAlreadyPresent,*strRepairLaborMasterIdAlreadyPresent;
    
    NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[section];
    
    NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
        
        if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
            
            [arrTemp addObject:dictIssuesRepairData];
            
        }//forKey:@"customerFeedback"
        
    }
    
    NSManagedObject *objTemp1=arrTemp[row];
    strRepairMasterIdAlreadyPresent=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairMasterId"]];
    
    strRepairLaborMasterIdAlreadyPresent=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"repairLaborId"]];
    
    //Change For Other Option then selected
    
    NSMutableArray *arrOfRepairLaborMasterDcs=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfRepairMasters.count; k++) {
        
        NSDictionary *dictDataRepair1=arrOfRepairMasters[k];
        
        NSString *strId=[NSString stringWithFormat:@"%@",[dictDataRepair1 valueForKey:@"RepairMasterId"]];
        
        if ([strRepairMasterIdAlreadyPresent isEqualToString:strId]) {
            
            NSArray *arrTemps=[dictDataRepair1 valueForKey:@"RepairLaborMasterDcs"];
            
            //RepairLaborMasterDcs
            [arrOfRepairLaborMasterDcs addObjectsFromArray:arrTemps];
            
            break;
        }
        
    }
    
    NSMutableArray *arrOfRepairLaborMasterDcsToShow=[[NSMutableArray alloc]init];
    
    for (int j=0; j<arrOfRepairLaborMasterDcs.count; j++) {
        
        NSDictionary *dictDataRepairLaborMaster=arrOfRepairLaborMasterDcs[j];
        
        if (![strRepairLaborMasterIdAlreadyPresent isEqualToString:[NSString stringWithFormat:@"%@",[dictDataRepairLaborMaster valueForKey:@"RepairLaborMasterId"]]]) {
            
            [arrOfRepairLaborMasterDcsToShow addObject:dictDataRepairLaborMaster];
            
        }else{
            
            
        }
        
    }
    
    // Here is the logic to show other options GBB
    
    // Index 2 pe value set
    NSDictionary *objTempNew=arrOfRepairLaborMasterDcsToShow[1];
    
    NSMutableArray *arrTempToCheckSelectedGBB=arrOfIndexSelectedGBB[section];
    
    [arrTempToCheckSelectedGBB replaceObjectAtIndex:row withObject:[objTempNew valueForKey:@"RepairLaborMasterId"]];
    
    [arrOfIndexSelectedGBB replaceObjectAtIndex:section withObject:arrTempToCheckSelectedGBB];
    
    [_tblViewServiceIssue reloadData];
    
    [self methodCalculationOverAll];
}

-(NSDictionary*)calculateLaborAmountFromMasters :(NSDictionary*)dictDataRepairMaster :(NSString*)strCostAdjustment :(NSString*)strPartCostAdjustment{
    
    NSDictionary *dictDataAfterHourRateToBeUsed=[[NSDictionary alloc]init];
    
    float hrsLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"LaborHours"]]];
    
    hrsLR=hrsLR/3600;
    
    float strStdPrice=0.0;
    
    if (!(arrOfHoursConfig.count==0)) {
        
        NSDictionary *dictDataHours=arrOfHoursConfig[0];
        
        strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
        
        if (!isStandardSubWorkOrder) {
            
            // Change for holiday and After hours change
            
            if (isHoliday) {
                
                strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                
            } else {
                
                // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                
                if (strAfterHrsDuration.length==0) {
                    
                    strStdPrice=0.0;
                    
                } else {
                    
                    NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                    
                    NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                    NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                    
                    strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                    strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                    
                    
                    NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                    
                    for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                        
                        NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                        
                        NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                        NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                        
                        if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                            
                            dictDataAfterHourRateToBeUsed=dictDataAHC;
                            break;
                            
                        }
                        
                    }
                    
                    
                    strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborPrice"] floatValue];
                    
                }
                
            }
            
            
        }
        
    }
    
    float totalPriceLabour=hrsLR*strStdPrice;
    
    //Helper Price Logic
    
    float hrsHLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"HelperHours"]]];
    
    hrsHLR=hrsHLR/3600;
    
    float strStdPriceHLR=0.0;
    
    if (!(arrOfHoursConfig.count==0)) {
        
        NSDictionary *dictDataHours=arrOfHoursConfig[0];
        
        strStdPriceHLR=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
        
        if (!isStandardSubWorkOrder) {
            
            if (isHoliday) {
                
                strStdPriceHLR=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];
                
            } else {
                if ((dictDataAfterHourRateToBeUsed==nil) || ([dictDataAfterHourRateToBeUsed isKindOfClass:[NSNull class]])) {
                    
                    
                    
                } else {
                    
                    strStdPriceHLR=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                    
                }
                
            }
            
        }
        
    }
    
    float totalPriceHelper=hrsHLR*strStdPriceHLR;
    
    //Adding Cost Adjustment To Price Repair
    
    float costAdjustMentToAdd=[strCostAdjustment floatValue];
    
    //Adding Part Cost Adjustment To Price Repair
    
    float partCostAdjustMentToAdd=[strPartCostAdjustment floatValue];
    
    
    //Final Price Logic
    
    float finalPriceLRHLR=totalPriceLabour+totalPriceHelper+costAdjustMentToAdd+partCostAdjustMentToAdd;
    
    //Parts Logic From Master
    
    float totalLookUpPrice=0;
    float totalCostPrice=0;
    
    NSArray *ArrOfPartsMaster=[dictDataRepairMaster valueForKey:@"RepairPartMasterExtSerDc"];
    
    for (int k=0; k<ArrOfPartsMaster.count; k++) {
        
        NSDictionary *dictData=ArrOfPartsMaster[k];
        
        NSString *strPartCode=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PartCode"]];
        
        float valueLookUpPrice=[self fetchPartDetailViaPartCode:strPartCode];
        
        float QTY=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Qty"]] floatValue];
        
        NSString *strCategorySysNameLocal=[self fetchPartCategorySysNameDetailViaPartCode:strPartCode];
        
        NSString *strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",valueLookUpPrice] :strCategorySysNameLocal :@"Standard"];
        
        float multiplier=[strMultiplier floatValue];
        
        //  multiplierGlobal=multiplier;
        
        totalCostPrice=totalCostPrice+valueLookUpPrice*QTY;
        
        valueLookUpPrice=valueLookUpPrice*QTY*multiplier;
        
        totalLookUpPrice=totalLookUpPrice+valueLookUpPrice;
        
    }
    
    //Final Price Logic after part and repair adding
    
    float finalOverAllPrice=totalLookUpPrice+finalPriceLRHLR;
    
    float totalLaborPrice=totalPriceLabour+totalPriceHelper;
    
    NSDictionary *dictOfAllValues = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",totalLaborPrice],@"laborcost",[NSString stringWithFormat:@"%f",totalCostPrice],@"partcost",[NSString stringWithFormat:@"%f",totalLookUpPrice],@"partcostsales",[NSString stringWithFormat:@"%f",finalOverAllPrice],@"overallcost",[NSString stringWithFormat:@"%@",strCostAdjustment],@"costAdjustment",[NSString stringWithFormat:@"%@",strPartCostAdjustment],@"partCostAdjustment", nil];
    
    
    return dictOfAllValues;
    
    
}

-(void)getPartsMaster{
    
    arrOfPartsGlobalMasters=nil;
    arrOfPartsGlobalMasters=[[NSMutableArray alloc]init];
    arrOfPartsGlobalMasters=[global getPartsMaster:strDepartmentSysName :@""];
    
}

-(void)getPricLookupFromMaster :(NSString*)strCategorySysName{
    
    arrOfPriceLookup=nil;
    arrOfPriceLookup=[[NSMutableArray alloc]init];
    arrOfPriceLookup=[global getPricLookupFromMaster:strCompanyKeyy :strDepartmentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType :strCategorySysName];
    
}

-(NSString*)logicForFetchingMultiplier :(NSString*)strPriceToLookUp :(NSString*)strCategorySysName :(NSString*)strType{
    
    NSString *strMultiplier;
    
    [self getPricLookupFromMaster:strCategorySysName];

    strMultiplier=[global logicForFetchingMultiplier:strPriceToLookUp :strCategorySysName :strType :arrOfPriceLookup];
    
    return strMultiplier;
}

-(NSString*)fetchPartCategorySysNameDetailViaPartCode :(NSString*)strPartCode{
    
    NSString *strCategorySysNameLocal;
    
    strCategorySysNameLocal=[global fetchPartCategorySysNameDetailViaPartCode:strPartCode :arrOfPartsGlobalMasters];
    
    return strCategorySysNameLocal;
    
}

-(float)fetchPartDetailViaPartCode :(NSString*)strPartCode :(NSString*)strPartQty{
    
    NSString *strLookupPrice;
    
    for (int k=0; k<arrOfPartsGlobalMasters.count; k++) {
        
        NSDictionary *dictData=arrOfPartsGlobalMasters[k];
        
        NSString *strPartCodeComing=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        
        if ([strPartCodeComing isEqualToString:strPartCode]) {
            
            strLookupPrice=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"LookupPrice"]];
            
            NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[dictData valueForKey:@"Name"],strPartQty];
            
            [arrOfTempPartsToShow addObject:[NSString stringWithFormat:@"%@",strPartsnQty]];
            
            break;
            
        }
    }
    
    float lookUpPrice=[strLookupPrice floatValue];
    
    return lookUpPrice;
    
}

//Fetch All GBB from masters

-(void)fetchAllGbbFromMaster{
    
    arrOfRepairMasters=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    //@"MasterAllMechanical"
    NSDictionary *dictMaster=[defs valueForKey:@"MasterAllMechanical"];
    NSArray *arrOfStatus=[dictMaster valueForKey:@"RepairMasterExtSerNewDc"];
    
    for (int k=0; k<arrOfStatus.count; k++) {
        
        NSDictionary *dictDataa=arrOfStatus[k];
        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
        if (isActive) {
            
            if ([[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]] isEqualToString:strDepartmentSysName]) {
                
                [arrOfRepairMasters addObject:dictDataa];
                
            }
            
        }
    }
    
}

-(void)getHoursConfiFromMaster{
    
    arrOfHoursConfig=nil;
    arrOfHoursConfig=[[NSMutableArray alloc]init];
    arrOfHoursConfig=[global getHoursConfiFromMaster:strCompanyKeyy :strDepartmentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView==_tblViewServiceIssue)
    {
    }
    else if (tableView==_tblIssueParts)
    {
    }
    else
    {
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
                
            case 10:
            {
                NSDictionary *dict=[arrDiagnostic objectAtIndex:indexPath.row];
                
                [_btnDiagnosticCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    _lblValueForDiagnosticeCharge.text=[NSString stringWithFormat:@"$%.02f",0.00];
                    
                    strDiagnosticChargeId=@"";
                    
                } else {
                    
                    float amtDC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    _lblValueForDiagnosticeCharge.text=[NSString stringWithFormat:@"$%.02f",amtDC];
                    
                    strDiagnosticChargeId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
                    
                }
                
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
                
#pragma mark - ***************** FOR PARTS *****************
                // [self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateTotal:_txtOtherDiscounts.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self calculateTotalForParts:_txtOtherDiscounts.text];
                }
                
                break;
            }
            case 20:
            {
                NSDictionary *dict=[arrTripCharge objectAtIndex:indexPath.row];
                [_btnTipCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    _lblValueForTipCharge.text=[NSString stringWithFormat:@"$%.02f",0.00];
                    
                    strTripChargeId=@"";
                    
                    
                } else {
                    
                    float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    _lblValueForTipCharge.text=[NSString stringWithFormat:@"$%.02f",amtTC];
                    
                    strTripChargeId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
                    
                    _txtFld_TripChargeQty.text = @"1";
                    
                }
                
                [self calculateTripChargeBasedOnQty];
                
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
#pragma mark - ***************** FOR PARTS *****************
                //[self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateTotal:_txtOtherDiscounts.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self calculateTotalForParts:_txtOtherDiscounts.text];
                }
                
                break;
            }
            case 30:
            {
                NSDictionary *dict=[arrDiagnostic objectAtIndex:indexPath.row];
                
                [_btnDiagnosticChargeNewPlan setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    _lblValueForDiagnosticChargeNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
                    
                    strDiagnosticChargeIdNewPlan=@"";
                    
                } else {
                    
                    float amtDC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    _lblValueForDiagnosticChargeNewPlan.text=[NSString stringWithFormat:@"$%.02f",amtDC];
                    
                    strDiagnosticChargeIdNewPlan=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
                    
                }
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
                
#pragma mark - ***************** FOR PARTS *****************
                // [self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                
                break;
            }
            case 40:
            {
                NSDictionary *dict=[arrTripCharge objectAtIndex:indexPath.row];
                [_btnTripChargeNewPlan setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    _lblValueForTripChargeNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
                    
                    strTripChargeIdNewPlan=@"";
                    
                } else {
                    
                    float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    _lblValueForTripChargeNewPlan.text=[NSString stringWithFormat:@"$%.02f",amtTC];
                    
                    strTripChargeIdNewPlan=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeMasterId"]];
                    
                }
                
                [self calculateTripChargeBasedOnQty];
                
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
#pragma mark - ***************** FOR PARTS *****************
                //[self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                
                
                break;
            }
            case 50:
            {
                NSDictionary *dict=[arrPSPMasters objectAtIndex:indexPath.row];
                [_btnSelectPlan setTitle:[dict valueForKey:@"Title"] forState:UIControlStateNormal];
                _lblNewMembershipPlan.text=[dict valueForKey:@"Title"];
                _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
                double tempValueTemporary=[[dict valueForKey:@"UnitCharges"] doubleValue];
                _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary];
                
                double memberShipDiscout,memberShipCharge,value;
                memberShipCharge=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
                memberShipDiscout=[[dict valueForKey:@"Discount"]doubleValue];
                
                value=memberShipCharge*(memberShipDiscout/100);
                
                _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value];
                
                
                //CHnage For Plans
                
                strPspMasterId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
                strAccountPSPId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
                strPSPCharge2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitCharges"]];
                strPSPDiscountPercent2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
                
                double memberShipDiscout2,memberShipCharge2,value2;
                memberShipCharge2=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
                memberShipDiscout2=[strPSPDiscountPercent2 doubleValue];
                
                value2=memberShipCharge2*(memberShipDiscout2/100);
                strPSPDiscountAmount2=[NSString stringWithFormat:@"%.02f",value2];
                _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value2];
                _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                double tempValueTemporary1=[[dict valueForKey:@"UnitCharges"] doubleValue];
                _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary1];
                
                //End Plans
                
                
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
                
                break;
            }
            case 60:
            {
                NSDictionary *dict=[arrMileageCharge objectAtIndex:indexPath.row];
                [_btnMileageCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    strChargeAmountMiles=[NSString stringWithFormat:@"%@",@"0.00"];
                    
                    _lblMileage.text=[NSString stringWithFormat:@"%.02f",0.00];
                    
                    _txtMiles.text=@"";
                    
                } else {
                    
                    float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    strChargeAmountMiles=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
                    
                    amtTC=amtTC*[_txtMiles.text floatValue];
                    
                    _lblMileage.text=[NSString stringWithFormat:@"%.02f",amtTC];
                    
                }
                
                
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
#pragma mark - ***************** FOR PARTS *****************
                //[self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateTotal:_txtOtherDiscounts.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self calculateTotalForParts:_txtOtherDiscounts.text];
                }
                
                break;
            }
            case 70:
            {
                NSDictionary *dict=[arrMileageCharge objectAtIndex:indexPath.row];
                [_btnMileageChargeNewPlan setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
                
                if (indexPath.row==0) {
                    
                    strChargeAmountMilesNewPlan=[NSString stringWithFormat:@"%@",@"0.00"];
                    
                    _lblMileageNewPlan.text=[NSString stringWithFormat:@"%.02f",0.00];
                    
                    _txtMilesNewPlan.text=@"";
                    
                } else {
                    
                    float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
                    
                    strChargeAmountMilesNewPlan=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
                    
                    amtTC=amtTC*[_txtMilesNewPlan.text floatValue];
                    
                    _lblMileageNewPlan.text=[NSString stringWithFormat:@"%.02f",amtTC];
                    
                }
                
#pragma mark - ***************** FOR ISSUE *****************
                // [self calculateTotal:_txtOtherDiscounts.text];
#pragma mark - ***************** FOR PARTS *****************
                //[self calculateTotalForParts:_txtOtherDiscounts.text];
                if ([strWoType isEqualToString:@"FR"])
                {
                    [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                else if ([strWoType isEqualToString:@"TM"])
                {
                    [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
                }
                
                
                break;
            }
            case 80:
            {
                dictSelectedDiscount = [arrayAccountDiscountCredits objectAtIndex:indexPath.row];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [_buttonSelectCredit_Apply setTitle:[dictSelectedDiscount valueForKey:@"sysName"] forState:UIControlStateNormal];
                    
                });
                
                
                break;
            }
            case 90:
            {
                dictAddDiscount=[arrayMasterCredits objectAtIndex:indexPath.row];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [_buttonSelectCredit_Add setTitle:[dictAddDiscount valueForKey:@"SysName"] forState:UIControlStateNormal];
                    
                });
                
                break;
            }
                
        }
        [tblData removeFromSuperview];
        [viewBackGround removeFromSuperview];
    }
}


-(void)buttonClickOnArrive:(id)sender
{
    chkHide=YES;
    strButtonClick=@"arrive";
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tblViewServiceIssue];
    NSIndexPath *indexPath = [_tblViewServiceIssue indexPathForRowAtPoint:buttonPosition];
    NSLog(@"selected tableview row is %ld",(long)indexPath.row);
    
    MechanicalClientApprovalTableViewCell *tappedCell = (MechanicalClientApprovalTableViewCell *)[_tblViewServiceIssue cellForRowAtIndexPath:indexPath];
    
    NSLog(@"Issue Id =%@  ",tappedCell.lblIssueId.text);
    NSManagedObject *record,*finalRecord;
    for (int i=0; i<arrAllObjSubWorkOrderIssuesRepair.count; i++)
    {
        record = [arrAllObjSubWorkOrderIssuesRepair objectAtIndex:i];
        if([tappedCell.lblIssueId.text isEqualToString:[record valueForKey:@"issueRepairId"]])
        {
            finalRecord=record;
            break;
        }
    }
    
    [self updateStadard:finalRecord];
    // [self calculateTotal:_txtOtherDiscounts.text];
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
        [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
        [self calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    
    [self showAmountInTextFieldToCharge];
}

-(void)buttonClickOnDecline:(id)sender
{
    strButtonClick=@"decline";
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tblViewServiceIssue];
    NSIndexPath *indexPath = [_tblViewServiceIssue indexPathForRowAtPoint:buttonPosition];
    NSLog(@"selected tableview row is %ld",(long)indexPath.row);
    
    MechanicalClientApprovalTableViewCell *tappedCell = (MechanicalClientApprovalTableViewCell *)[_tblViewServiceIssue cellForRowAtIndexPath:indexPath];
    
    NSLog(@"Issue Id =%@  ",tappedCell.lblIssueId.text);
    NSManagedObject *record,*finalRecord;
    for (int i=0; i<arrAllObjSubWorkOrderIssuesRepair.count; i++)
    {
        record = [arrAllObjSubWorkOrderIssuesRepair objectAtIndex:i];
        if([tappedCell.lblIssueId.text isEqualToString:[record valueForKey:@"issueRepairId"]])
        {
            finalRecord=record;
            break;
        }
    }
    
    [self updateStadard:finalRecord];
    
    //[self calculateTotal:_txtOtherDiscounts.text];
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
        [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
        [self calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    
    [self showAmountInTextFieldToCharge];
}

-(void)methodCalculationOverAll{
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
        [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
        [self calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    
}
-(void)updateStadard:(NSManagedObject *)matchesObject
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    NSManagedObject *matchesTemp;
    matchesTemp=matchesObject;
    NSLog(@"Workorder Id=%@\n",[matchesTemp valueForKey:@"workorderId"]);
    NSLog(@"before =%@",[matchesTemp valueForKey:@"customerFeedback"]);
    if ([strButtonClick isEqualToString:@"arrive"])
    {
        [matchesTemp setValue:@"true" forKey:@"customerFeedback"];
    }
    else if ([strButtonClick isEqualToString:@"decline"])
    {
        [matchesTemp setValue:@"false" forKey:@"customerFeedback"];
    }
    NSLog(@"after =%@",[matchesTemp valueForKey:@"customerFeedback"]);
    
    NSError *error2;
    [context save:&error2];
    
    [_tblViewServiceIssue reloadData];
}
//============================================================================
#pragma mark- **************** DATE PICKER METHOD ****************
//============================================================================
-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate:[NSDate date]];
    
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDate = [dateFormat stringFromDate:pickerDate.date];
    [_btnExpirationDate setTitle:strDate forState:UIControlStateNormal];
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)tapDetectedOnBackGroundView{
    [viewBackGround removeFromSuperview];
}
#pragma mark- ************** TAP METHODS **************

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(isLaborPartViewAddedToSuperView == NO)
    {
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
    }
    
  //  [_viewPartLaborCreditApply removeFromSuperview];
    
}
- (void)singleTap:(UITapGestureRecognizer *)gesture
{
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}

#pragma mark- **************** Action Sheet Method ****************

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        {
            if (isCheckFrontImage)
            {
                
                if (arrOFImagesName.count==0)
                {
                    
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                }else
                {
                    [self performSelector:@selector(getImagePreviewFrontImage) withObject:nil afterDelay:1.0];
                }
                
            }
            else
            {
                
                if (arrOfCheckBackImage.count==0) {
                    
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                }else
                {
                    [self performSelector:@selector(getImagePreviewBackImage) withObject:nil afterDelay:1.0];
                }
            }
        }
    }
    else if (buttonIndex==1)
    {
        NSArray *arrTemp;
        if (isCheckFrontImage) {
            
            arrTemp=arrOFImagesName;
            
        }else{
            
            arrTemp=arrOfCheckBackImage;
            
        }
        
        if (arrTemp.count>=1) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        } else {
            NSLog(@"The CApture Image.");
            
            BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
                
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Camera Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if ( ( (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                             //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                             //                                     [alert show];
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
    }
    else if (buttonIndex==2)
    {
        NSArray *arrTemp;
        if (isCheckFrontImage) {
            
            arrTemp=arrOFImagesName;
            
        }else{
            
            arrTemp=arrOfCheckBackImage;
            
        }
        
        if (arrTemp.count>=1) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        } else {
            
            BOOL isCameraPermissionAvailable=[global isGalleryPermission];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
                
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Gallery Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if ( ((&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                             //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                             //                                     [alert show];
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
    }
}
#pragma mark- **************** Download Image  ****************

-(void)downloadingImagess:(NSString *)str
{
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }
    if (result.length==0)
    {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }/*
      NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
      NSString *result;
      NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
      if (equalRange.location != NSNotFound) {
      result = [str substringFromIndex:equalRange.location + equalRange.length];
      }else{
      result=str;
      }
      if (result.length==0) {
      NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
      if (equalRange.location != NSNotFound) {
      result = [str substringFromIndex:equalRange.location + equalRange.length];
      }
      }
      
      */
    NSString *strUrl;// = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
    strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];//str
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists)
    {
        [self stopdejal];
        //[self ShowFirstImage];
    }
    else
    {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        // NSURL *url = [NSURL URLWithString:strNewString];
        strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
        UIImage *image = [UIImage imageWithData:photoData];
        if (result.length==0)
        {
            result=str;
        }
        [self saveImageAfterDownload1:image :result];
    }
    
    // [self stopdejal];
    imageTemp= [self loadImage:result];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
}
-(void)stopdejal{
    [DejalActivityView removeView];
}
-(void)startDejal
{
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Downloading Image..."];
}

#pragma mark- **************** Image Picker Method ****************

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    isImageCapture=YES;
    // isFromImagePicker=YES;
    NSLog(@"Yes Something Edited");
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\CheckImg%@%@.jpg",strDate,strTime];
    
    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    [self resizeImage:image :strImageNamess];
    
    //[self saveImage:image :strImageNamess];
    
    if (isCheckFrontImage)
    {
        
        [arrOFImagesName addObject:strImageNamess];
        
    }
    else
    {
        
        [arrOfCheckBackImage addObject:strImageNamess];
        
    }
    
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}
- (UIImage*)loadImage:(NSString *)imageName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      imageName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

#pragma mark- ****************** Local Methods ******************

#pragma mark- ****** Local Methods For BorderColor *******

-(void)methodBorderColor
{
    _txtOtherDiscountNewPlan.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtOtherDiscountNewPlan.layer.borderWidth=1.0;
    _txtOtherDiscountNewPlan.layer.cornerRadius=5.0;
    
    _txtAgreementCost.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtAgreementCost.layer.borderWidth=1.0;
    _txtAgreementCost.layer.cornerRadius=5.0;
    
    _txtLaborPercent.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtLaborPercent.layer.borderWidth=1.0;
    _txtLaborPercent.layer.cornerRadius=5.0;
    
    _txtPartPercent.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtPartPercent.layer.borderWidth=1.0;
    _txtPartPercent.layer.cornerRadius=5.0;
    
    _txtOtherDiscounts.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtOtherDiscounts.layer.borderWidth=1.0;
    _txtOtherDiscounts.layer.cornerRadius=5.0;
    
    _txtAmountSingleAmount.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtAmountSingleAmount.layer.borderWidth=1.0;
    _txtAmountSingleAmount.layer.cornerRadius=5.0;
    
    _txtCheckNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtCheckNo.layer.borderWidth=1.0;
    _txtCheckNo.layer.cornerRadius=5.0;
    
    _txtAmountCheckView.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtAmountCheckView.layer.borderWidth=1.0;
    _txtAmountCheckView.layer.cornerRadius=5.0;
    
    _txtDrivingLicenseNo.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtDrivingLicenseNo.layer.borderWidth=1.0;
    _txtDrivingLicenseNo.layer.cornerRadius=5.0;
    
    [_btnTipCharge.layer setCornerRadius:5.0f];
    [_btnTipCharge.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnTipCharge.layer setBorderWidth:0.8f];
    [_btnTipCharge.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnTipCharge.layer setShadowOpacity:0.3];
    [_btnTipCharge.layer setShadowRadius:3.0];
    [_btnTipCharge.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnMileageCharge.layer setCornerRadius:5.0f];
    [_btnMileageCharge.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnMileageCharge.layer setBorderWidth:0.8f];
    [_btnMileageCharge.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnMileageCharge.layer setShadowOpacity:0.3];
    [_btnMileageCharge.layer setShadowRadius:3.0];
    [_btnMileageCharge.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnMileageChargeNewPlan.layer setCornerRadius:5.0f];
    [_btnMileageChargeNewPlan.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnMileageChargeNewPlan.layer setBorderWidth:0.8f];
    [_btnMileageChargeNewPlan.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnMileageChargeNewPlan.layer setShadowOpacity:0.3];
    [_btnMileageChargeNewPlan.layer setShadowRadius:3.0];
    [_btnMileageChargeNewPlan.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnDiagnosticCharge.layer setCornerRadius:5.0f];
    [_btnDiagnosticCharge.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnDiagnosticCharge.layer setBorderWidth:0.8f];
    [_btnDiagnosticCharge.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnDiagnosticCharge.layer setShadowOpacity:0.3];
    [_btnDiagnosticCharge.layer setShadowRadius:3.0];
    [_btnDiagnosticCharge.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnDiagnosticChargeNewPlan.layer setCornerRadius:5.0f];
    [_btnDiagnosticChargeNewPlan.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnDiagnosticChargeNewPlan.layer setBorderWidth:0.8f];
    [_btnDiagnosticChargeNewPlan.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnDiagnosticChargeNewPlan.layer setShadowOpacity:0.3];
    [_btnDiagnosticChargeNewPlan.layer setShadowRadius:3.0];
    [_btnDiagnosticChargeNewPlan.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btnTripChargeNewPlan.layer setCornerRadius:5.0f];
    [_btnTripChargeNewPlan.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnTripChargeNewPlan.layer setBorderWidth:0.8f];
    [_btnTripChargeNewPlan.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnTripChargeNewPlan.layer setShadowOpacity:0.3];
    [_btnTripChargeNewPlan.layer setShadowRadius:3.0];
    [_btnTripChargeNewPlan.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    
    [_btnSelectPlan.layer setCornerRadius:5.0f];
    [_btnSelectPlan.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnSelectPlan.layer setBorderWidth:0.8f];
    [_btnSelectPlan.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnSelectPlan.layer setShadowOpacity:0.3];
    [_btnSelectPlan.layer setShadowRadius:3.0];
    [_btnSelectPlan.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}

#pragma mark- ****** Local Methods For Image *******

-(void)setButtonImage:(UIButton*)btn
{
    
    for (int k1=0; k1<buttons.count; k1++) {
        
        UIButton *button=buttons[k1];
        
        if ([button isEqual:btn])
        {//RadioButton-Selected.png
            // [button setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            
            if (k1==0) {
                
                [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
                
            }else if (k1==3){
                
                [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==4){
                
                [_btnAutoChargeCustomer setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==5){
                
                [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==6){
                
                [_btnNoChange setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==2){
                
                [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }else if (k1==1){
                
                [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
            }
        }
        else
        {
            // [button setImage:[UIImage imageNamed:@"RadioButton-UnSelected.png"] forState:UIControlStateNormal];
            if (k1==0) {
                
                [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
                
            }else if (k1==3){
                
                [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==4){
                
                [_btnAutoChargeCustomer setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==5){
                
                [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==6){
                
                [_btnNoChange setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==2){
                
                [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }else if (k1==1){
                
                [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
            }
            
        }
        
        
    }
    
}

-(void)openGalleryy
{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera
{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)getImagePreviewFrontImage
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"forCheckFrontImageDelete"];
    [defs setBool:NO forKey:@"forCheckBackImageDelete"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                             bundle: nil];
    ImagePreviewGeneralInfoAppointmentViewiPad
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
    objByProductVC.arrOfImages=arrOFImagesName;
    objByProductVC.indexOfImage=@"0";
    objByProductVC.statusOfWorkOrder=@"InComplete";
    //objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;//strWorkOrderStatuss;
    //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    // [self.navigationController pushViewController:objByProductVC animated:NO];
}
-(void)getImagePreviewBackImage
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"forCheckBackImageDelete"];
    [defs setBool:NO forKey:@"forCheckFrontImageDelete"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                             bundle: nil];
    ImagePreviewGeneralInfoAppointmentViewiPad
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentViewiPad"];
    objByProductVC.arrOfImages=arrOfCheckBackImage;
    objByProductVC.indexOfImage=@"0";
    objByProductVC.statusOfWorkOrder=@"InComplete";
    //objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;//strWorkOrderStatuss;
    //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    // [self.navigationController pushViewController:objByProductVC animated:NO];
}
#pragma mark- ****** Local Table Create Method ******

-(void)tableCreate
{
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
}
-(void)setTableFrame:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, 500);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    UILabel *lblOk;
    lblOk=[[UILabel alloc]init];
    lblOk.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    lblOk.textColor=[UIColor whiteColor];
    lblOk.text=@"OK";
    lblOk.textAlignment=NSTextAlignmentCenter;
    lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-80, tblData.frame.origin.y+tblData.frame.size.height+2, 160, 50);
    if([UIScreen mainScreen].bounds.size.height==568|| [UIScreen mainScreen].bounds.size.height==480)
    {
        lblOk.frame=CGRectMake(viewBackGround.frame.size.width/2-60, tblData.frame.origin.y+tblData.frame.size.height+2, 120, 40);
    }
    lblOk.layer.borderWidth=1.0;
    lblOk.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    lblOk.layer.cornerRadius=5.0;
    [viewBackGround addSubview:lblOk];
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}
-(void)tableLoad:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
        case 20:
        {
            [self setTableFrame:i];
            break;
        }
        case 30:
        {
            [self setTableFrame:i];
            break;
        }
        case 40:
        {
            [self setTableFrame:i];
            break;
        }
        case 50:
        {
            [self setTableFrame:i];
            break;
        }
        case 60:
        {
            [self setTableFrame:i];
            break;
        }
        case 70:
        {
            [self setTableFrame:i];
            break;
        }
            // Akshay Start //
        case 80:
        {
            [self setTableFrame:i];
            break;
        }
        case 90:
        {
            [self setTableFrame:i];
            break;
        }
            // Akshay End //
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
}
#pragma mark- ****** Local AddView ******

-(void)addView
{
    
    if (isUpdateViewsFrame==NO) {
        
        bool chkForParts;
        
        chkForParts=YES;
        
        
        
        if ([strWoType isEqualToString:@"TM"])
            
        {
            
#pragma mark - ***************** FOR PARTS *****************
            
            _const_TableParts_H.constant=arrAllParts.count*_tblIssueParts.rowHeight+(arrOfSubWorkServiceIssues.count*50)+(arrIndexGlobalToShowEquipHeader.count*50);
            
            _tblIssueParts.frame=CGRectMake(_tblIssueParts.frame.origin.x, _tblIssueParts.frame.origin.y,[UIScreen mainScreen].bounds.size.width, _const_TableParts_H.constant);//4*382+150
            
            _viewForServiceParts.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _tblIssueParts.frame.size.height+100);
            
            [_scrollView addSubview:_viewForServiceParts];
            
            //        _viewForAgreement.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForServiceParts.frame)+5,_viewForAgreement.frame.size.width,_viewForAgreement.frame.size.height);
            //
            //        [_scrollView addSubview:_viewForAgreement];
            //
            //        _viewForPaymentDetail.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForAgreement.frame)+5,_viewForPaymentDetail.frame.size.width,_viewForPaymentDetail.frame.size.height);
            //
            //        [_scrollView addSubview:_viewForPaymentDetail];
            //
            //        _viewForPaymentMode.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentMode.frame.size.height);
            //
            //        [_scrollView addSubview:_viewForPaymentMode];
            
            
            
            _viewForAdditionalInfo.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForServiceParts.frame)+5,_viewForServiceParts.frame.size.width,_viewForAdditionalInfo.frame.size.height);
            [_scrollView addSubview:_viewForAdditionalInfo];
            
            
            _viewForAgreement.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForAdditionalInfo.frame)+5,_viewForServiceParts.frame.size.width,_viewForAgreement.frame.size.height);
            [_scrollView addSubview:_viewForAgreement];
            
            
            //Nilind 25 July
            
            _viewForPlanStatus.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForAgreement.frame)+5,_viewForServiceParts.frame.size.width,_viewForPlanStatus.frame.size.height);
            [_scrollView addSubview:_viewForPlanStatus];
            
            //End
            NSMutableArray *arr;
            arr=[[NSMutableArray alloc]init];
            [arr addObject:@""];
            [arr addObject:@""];
            //arrPlanStatusResponse
            if(arrPlanStatusResponse.count==1)
            {
                /*
                 _viewForNewPlan.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceParts.frame.size.width,_viewForNewPlan.frame.size.height);
                 [_scrollView addSubview:_viewForNewPlan];
                 
                 _viewForPaymentMode.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForNewPlan.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentMode.frame.size.height);
                 [_scrollView addSubview:_viewForPaymentMode];
                 */
                _viewForPaymentDetail.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentDetail.frame.size.height);
                [_scrollView addSubview:_viewForPaymentDetail];
                
                
                // Akshay Start //
                
                _viewCredit.frame=CGRectMake(_viewForPaymentDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceParts.frame.size.width,viewCreditHeight);
                [_scrollView addSubview:_viewCredit];
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewCredit.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentMode.frame.size.height);
                
                // Akshay End //
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentMode.frame.size.height);
                [_scrollView addSubview:_viewForPaymentMode];
                
                [_lblNewMembershipPlan setHidden:YES];
                [_btnNewPlan setHidden:YES];
                [_btnSelectPlan setHidden:YES];
                [_viewNew setHidden:YES];
            }
            else if (arrPlanStatusResponse.count==2)
            {
                /*
                 _viewForPaymentDetail.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentDetail.frame.size.height);
                 [_scrollView addSubview:_viewForPaymentDetail];
                 
                 //Nilind 25 July
                 
                 _viewForNewPlan.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceParts.frame.size.width,_viewForNewPlan.frame.size.height);
                 [_scrollView addSubview:_viewForNewPlan];
                 
                 _viewForPaymentMode.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForNewPlan.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentMode.frame.size.height);
                 [_scrollView addSubview:_viewForPaymentMode];*/
                
                _viewForPaymentDetail.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentDetail.frame.size.height);
                [_scrollView addSubview:_viewForPaymentDetail];
                
                
                // Akshay Start //
                
                _viewCredit.frame=CGRectMake(_viewForPaymentDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceParts.frame.size.width,viewCreditHeight);
                [_scrollView addSubview:_viewCredit];
                
                
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewCredit.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentMode.frame.size.height);
                
                // Akshay End //
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentMode.frame.size.height);
                [_scrollView addSubview:_viewForPaymentMode];
                
                [_lblNewMembershipPlan setHidden:NO];
                [_btnNewPlan setHidden:NO];
                [_btnSelectPlan setHidden:NO];
                [_viewNew setHidden:NO];
                
            }
            else
            {
                _viewForPaymentDetail.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentDetail.frame.size.height);
                [_scrollView addSubview:_viewForPaymentDetail];
                
                
                // Akshay Start //
                _viewCredit.frame=CGRectMake(_viewForPaymentDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceParts.frame.size.width,viewCreditHeight);
                [_scrollView addSubview:_viewCredit];
                
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewCredit.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentMode.frame.size.height);
                
                // Akshay End //
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceParts.frame.size.width,_viewForPaymentMode.frame.size.height);
                [_scrollView addSubview:_viewForPaymentMode];
                
                [_lblNewMembershipPlan setHidden:YES];
                [_btnNewPlan setHidden:YES];
                [_btnSelectPlan setHidden:YES];
                [_viewNew setHidden:YES];
            }
            
            
            if (checkView==YES)
                
            {
                
                
                
                _viewForCheckDetail.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPaymentMode.frame)+5,_viewForServiceParts.frame.size.width,_viewForCheckDetail.frame.size.height);
                
                [_scrollView addSubview:_viewForCheckDetail];
                
                
                
                _viewForSign.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForCheckDetail.frame)+5,_viewForServiceParts.frame.size.width, _viewForSign.frame.size.height);
                
                [_scrollView addSubview:_viewForSign];
                
            }
            
            else
                
            {
                
                [_viewForCheckDetail removeFromSuperview];
                
                _viewForSingleAmount.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPaymentMode.frame)+5,_viewForServiceParts.frame.size.width,_viewForSingleAmount.frame.size.height);
                
                [_scrollView addSubview:_viewForSingleAmount];
                
                
                
                _viewForSign.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForSingleAmount.frame),_viewForServiceParts.frame.size.width, _viewForSign.frame.size.height);
                
                [_scrollView addSubview:_viewForSign];
                
                
                
            }
            
            BOOL isTmforClientApproval;
            
            isTmforClientApproval=NO;
            
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
            NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceReportTermsAndConditions"];
            
            if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                    
                    
                    NSDictionary *dictData=arrOfServiceJobDescriptionTemplate[k];
                    
                    NSLog(@"Terms And Conditons===%@",dictData);
                    
                    if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]] isEqualToString:[NSString stringWithFormat:@"%@",strGlobalDepartmentId]]) {
                        
                        if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsTMforClientApproval"]] isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsTMforClientApproval"]] isEqualToString:@"1"])
                        {
                            
                            isTmforClientApproval=YES;
                            
                        }
                    }
                }
            }
            
            if (isTmforClientApproval) {
                
                _view_TermsnConditions.frame=CGRectMake(_view_TermsnConditions.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _view_TermsnConditions.frame.size.height);
                
                [_scrollView addSubview:_view_TermsnConditions];
                
                [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_view_TermsnConditions.frame)+50)]   ;
                
            } else {
                
                [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_viewForSign.frame)+5+_viewForSign.frame.size.height)];
                
            }
            
        }
        else if ([strWoType isEqualToString:@"FR"])
            
        {
            
#pragma mark - ***************** FOR ISSUE *****************
            
            
            //arrOfSubWorkServiceIssues
            //_const_Table_ViewIssue_H.constant=arrOfSubWorkServiceIssuesRepair.count*(285+25)+(arrOfSubWorkServiceIssuesRepair.count*0);
            
            _const_Table_ViewIssue_H.constant=arrOfSubWorkServiceIssuesRepair.count*_tblViewServiceIssue.rowHeight+(arrOfSubWorkServiceIssues.count*50)+(arrIndexGlobalToShowEquipHeader.count*50);
            
            
            _tblViewServiceIssue.frame=CGRectMake(_tblViewServiceIssue.frame.origin.x, _tblViewServiceIssue.frame.origin.y, _tblViewServiceIssue.frame.size.width,_const_Table_ViewIssue_H.constant);//4*382+150
            
            
            _viewForServiceIssue.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _tblViewServiceIssue.frame.size.height+100);
            
            //_viewForServiceIssue.backgroundColor=[UIColor redColor];
            //_tblViewServiceIssue.backgroundColor=[UIColor grayColor];
            
            [_scrollView addSubview:_viewForServiceIssue];
            
            
            
            //Nilind 25 July
            
            _viewForAdditionalInfo.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForServiceIssue.frame)+5,_viewForServiceIssue.frame.size.width,_viewForAdditionalInfo.frame.size.height);
            [_scrollView addSubview:_viewForAdditionalInfo];
            
            
            _viewForPlanStatus.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForAdditionalInfo.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPlanStatus.frame.size.height);
            [_scrollView addSubview:_viewForPlanStatus];
            
            //End
            
            NSMutableArray *arr;
            arr=[[NSMutableArray alloc]init];
            [arr addObject:@""];
            // [arr addObject:@""];
            //arrPlanStatusResponse
            if(arrPlanStatusResponse.count==1)
            {
                /*
                 _viewForNewPlan.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceIssue.frame.size.width,_viewForNewPlan.frame.size.height);
                 
                 [_scrollView addSubview:_viewForNewPlan];
                 
                 _viewForPaymentMode.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForNewPlan.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentMode.frame.size.height);
                 
                 [_scrollView addSubview:_viewForPaymentMode];
                 */
                _viewForPaymentDetail.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentDetail.frame.size.height);
                [_scrollView addSubview:_viewForPaymentDetail];
                
                
                // Akshay Start //
                
                _viewCredit.frame=CGRectMake(_viewForPaymentDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceIssue.frame.size.width,viewCreditHeight);
                
                [_scrollView addSubview:_viewCredit];
                
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewCredit.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentMode.frame.size.height);
                
                // Akshay End //
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentMode.frame.size.height);
                
                [_scrollView addSubview:_viewForPaymentMode];
                
                [_lblNewMembershipPlan setHidden:YES];
                [_btnNewPlan setHidden:YES];
                [_btnSelectPlan setHidden:YES];
                [_viewNew setHidden:YES];
                
            }
            else if (arrPlanStatusResponse.count==2)
            {
                /*
                 _viewForPaymentDetail.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentDetail.frame.size.height);
                 [_scrollView addSubview:_viewForPaymentDetail];
                 
                 
                 //Nilind 25 July
                 
                 _viewForNewPlan.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceIssue.frame.size.width,_viewForNewPlan.frame.size.height);
                 
                 [_scrollView addSubview:_viewForNewPlan];
                 
                 _viewForPaymentMode.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForNewPlan.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentMode.frame.size.height);
                 
                 [_scrollView addSubview:_viewForPaymentMode];*/
                
                
                _viewForPaymentDetail.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentDetail.frame.size.height);
                [_scrollView addSubview:_viewForPaymentDetail];
                
                
                // Akshay Start //
                
                _viewCredit.frame=CGRectMake(_viewForPaymentDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceIssue.frame.size.width,viewCreditHeight);
                
                [_scrollView addSubview:_viewCredit];
                
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewCredit.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentMode.frame.size.height);
                
                // Akshay End //
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentMode.frame.size.height);
                
                [_scrollView addSubview:_viewForPaymentMode];
                
                [_lblNewMembershipPlan setHidden:NO];
                [_btnNewPlan setHidden:NO];
                [_btnSelectPlan setHidden:NO];
                [_viewNew setHidden:NO];
                
            }
            else
            {
                
                _viewForPaymentDetail.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPlanStatus.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentDetail.frame.size.height);
                [_scrollView addSubview:_viewForPaymentDetail];
                
                
                // Akshay Start //
                
                _viewCredit.frame=CGRectMake(_viewForPaymentDetail.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceIssue.frame.size.width,viewCreditHeight);
                [_scrollView addSubview:_viewCredit];
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewCredit.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentMode.frame.size.height);
                
                // Akshay End //
                
                _viewForPaymentMode.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPaymentDetail.frame)+5,_viewForServiceIssue.frame.size.width,_viewForPaymentMode.frame.size.height);
                
                [_scrollView addSubview:_viewForPaymentMode];
                
                [_lblNewMembershipPlan setHidden:YES];
                [_btnNewPlan setHidden:YES];
                [_btnSelectPlan setHidden:YES];
                [_viewNew setHidden:YES];
                
                
            }
            
            
            if (checkView==YES)
                
            {
                
                
                
                _viewForCheckDetail.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPaymentMode.frame)+5,_viewForServiceIssue.frame.size.width,_viewForCheckDetail.frame.size.height);
                
                [_scrollView addSubview:_viewForCheckDetail];
                
                
                
                _viewForSign.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForCheckDetail.frame)+5,_viewForServiceIssue.frame.size.width, _viewForSign.frame.size.height);
                
                [_scrollView addSubview:_viewForSign];
                
            }
            
            else
                
            {
                
                [_viewForCheckDetail removeFromSuperview];
                
                _viewForSingleAmount.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForPaymentMode.frame)+5,_viewForServiceIssue.frame.size.width,_viewForSingleAmount.frame.size.height);
                
                [_scrollView addSubview:_viewForSingleAmount];
                
                
                
                _viewForSign.frame=CGRectMake(_viewForServiceIssue.frame.origin.x, CGRectGetMaxY(_viewForSingleAmount.frame),_viewForServiceIssue.frame.size.width, _viewForSign.frame.size.height);
                
                [_scrollView addSubview:_viewForSign];
                
                
                
            }
            
            [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_viewForSign.frame)+5+_viewForSign.frame.size.height)] ;
            
        }
        
    }
    else // update views's frame    // Akshay //
    {
        bool chkForParts;
        
        chkForParts=YES;
        
        
        if ([strWoType isEqualToString:@"TM"])
        {
            
            NSMutableArray *arr;
            arr=[[NSMutableArray alloc]init];
            [arr addObject:@""];
            [arr addObject:@""];
            //arrPlanStatusResponse
            if(arrPlanStatusResponse.count==1)
            {
                
                CGRect frameRect = _viewForPaymentDetail.frame;
                frameRect.size.height = heightPaymntDetailView;
                _viewForPaymentDetail.frame = frameRect;
                
                
                CGRect frameRect1 = _viewCredit.frame;
                frameRect1.size.height = viewCreditHeight;
                _viewCredit.frame = frameRect1;
                
                CGRect frameRect2 = _viewCredit.frame;
                frameRect2.origin.y = CGRectGetMaxY(_viewForPaymentDetail.frame)+5;
                _viewCredit.frame = frameRect2;
                
                
                CGRect frameRect3 = _viewForPaymentMode.frame;
                frameRect3.origin.y = CGRectGetMaxY(_viewCredit.frame)+5;
                _viewForPaymentMode.frame = frameRect3;
                
            }
            else if (arrPlanStatusResponse.count==2)
            {
                
                CGRect frameRect = _viewForPaymentDetail.frame;
                frameRect.size.height = heightPaymntDetailView;
                _viewForPaymentDetail.frame = frameRect;
                
                
                CGRect frameRect1 = _viewCredit.frame;
                frameRect1.size.height = viewCreditHeight;
                _viewCredit.frame = frameRect1;
                
                
                CGRect frameRect2 = _viewCredit.frame;
                frameRect2.origin.y = CGRectGetMaxY(_viewForPaymentDetail.frame)+5;
                _viewCredit.frame = frameRect2;
                
                
                CGRect frameRect3 = _viewForPaymentMode.frame;
                frameRect3.origin.y = CGRectGetMaxY(_viewCredit.frame)+5;
                _viewForPaymentMode.frame = frameRect3;
                
                
            }
            else
            {
                
                CGRect frameRect = _viewForPaymentDetail.frame;
                frameRect.size.height = heightPaymntDetailView;
                _viewForPaymentDetail.frame = frameRect;
                
                CGRect frameRect1 = _viewCredit.frame;
                frameRect1.size.height = viewCreditHeight;
                _viewCredit.frame = frameRect1;
                
                CGRect frameRect2 = _viewCredit.frame;
                frameRect2.origin.y = CGRectGetMaxY(_viewForPaymentDetail.frame)+5;
                _viewCredit.frame = frameRect2;
                
                CGRect frameRect3 = _viewForPaymentMode.frame;
                frameRect3.origin.y = CGRectGetMaxY(_viewCredit.frame)+5;
                _viewForPaymentMode.frame = frameRect3;
                
            }
            
            
            if (checkView==YES)
            {
                
                
                CGRect frameRect = _viewForCheckDetail.frame;
                frameRect.origin.y = CGRectGetMaxY(_viewForPaymentMode.frame)+5;
                _viewForCheckDetail.frame = frameRect;
                
                [_scrollView addSubview:_viewForCheckDetail];

                
                CGRect frameRect1 = _viewForSign.frame;
                frameRect1.origin.y =CGRectGetMaxY(_viewForCheckDetail.frame)+5;
                _viewForSign.frame = frameRect1;
                
                
                
            }
            
            else
            {
                
                [_viewForCheckDetail removeFromSuperview];
                
                
                CGRect frameRect1 = _viewForSingleAmount.frame;
                frameRect1.origin.y =CGRectGetMaxY(_viewForPaymentMode.frame)+5;
                _viewForSingleAmount.frame = frameRect1;
                
                //                _viewForSingleAmount.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForPaymentMode.frame)+5,_viewForServiceParts.frame.size.width,_viewForSingleAmount.frame.size.height);
                
                
                CGRect frameRect2 = _viewForSign.frame;
                frameRect2.origin.y =CGRectGetMaxY(_viewForSingleAmount.frame)+5;
                _viewForSign.frame = frameRect2;
                
                
                
                
                
                //                _viewForSign.frame=CGRectMake(_viewForServiceParts.frame.origin.x, CGRectGetMaxY(_viewForSingleAmount.frame),_viewForServiceParts.frame.size.width, _viewForSign.frame.size.height);
                
                
                
                
            }
            
            BOOL isTmforClientApproval;
            
            isTmforClientApproval=NO;
            
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
            NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceReportTermsAndConditions"];
            
            if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                    
                    
                    NSDictionary *dictData=arrOfServiceJobDescriptionTemplate[k];
                    
                    NSLog(@"Terms And Conditons===%@",dictData);
                    
                    if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]] isEqualToString:[NSString stringWithFormat:@"%@",strGlobalDepartmentId]]) {
                        
                        if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsTMforClientApproval"]] isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsTMforClientApproval"]] isEqualToString:@"1"])
                        {
                            
                            isTmforClientApproval=YES;
                            
                        }
                    }
                }
            }
            
            if (isTmforClientApproval) {
                
                
                // dispatch_async(dispatch_get_main_queue(), ^{
                
                CGRect frameRect1 = _view_TermsnConditions.frame;
                frameRect1.origin.y =CGRectGetMaxY(_viewForSign.frame)+5;
                _view_TermsnConditions.frame = frameRect1;
                
                // });
                
                
                //                _view_TermsnConditions.frame=CGRectMake(_view_TermsnConditions.frame.origin.x, CGRectGetMaxY(_viewForSign.frame)+5, [UIScreen mainScreen].bounds.size.width, _view_TermsnConditions.frame.size.height);
                //
                //                [_scrollView addSubview:_view_TermsnConditions];
                
                [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_view_TermsnConditions.frame)+50)]   ;
                
            }
            else
            {
                
                [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_viewForSign.frame)+5+_viewForSign.frame.size.height)];
                
            }
            
        }
        else if ([strWoType isEqualToString:@"FR"])
        {
            
#pragma mark - ***************** FOR ISSUE *****************
            
            
            //End
            
            NSMutableArray *arr;
            arr=[[NSMutableArray alloc]init];
            [arr addObject:@""];
            // [arr addObject:@""];
            //arrPlanStatusResponse
            if(arrPlanStatusResponse.count==1)
            {
                
                CGRect frameRect = _viewForPaymentDetail.frame;
                frameRect.size.height = heightPaymntDetailView;
                _viewForPaymentDetail.frame = frameRect;
                
                CGRect frameRect1 = _viewCredit.frame;
                frameRect1.size.height = viewCreditHeight;
                _viewCredit.frame = frameRect1;
                
                
                CGRect frameRect2 = _viewCredit.frame;
                frameRect2.origin.y = CGRectGetMaxY(_viewForPaymentDetail.frame)+5;
                _viewCredit.frame = frameRect2;
                
                CGRect frameRect3 = _viewForPaymentMode.frame;
                frameRect3.origin.y =  CGRectGetMaxY(_viewCredit.frame)+5;
                _viewForPaymentMode.frame = frameRect3;
                
                
                [_lblNewMembershipPlan setHidden:YES];
                [_btnNewPlan setHidden:YES];
                [_btnSelectPlan setHidden:YES];
                [_viewNew setHidden:YES];
                
            }
            else if (arrPlanStatusResponse.count==2)
            {
                
                CGRect frameRect = _viewForPaymentDetail.frame;
                frameRect.size.height = heightPaymntDetailView;
                _viewForPaymentDetail.frame = frameRect;
                
                CGRect frameRect1 = _viewCredit.frame;
                frameRect1.size.height = viewCreditHeight;
                _viewCredit.frame = frameRect1;
                
                CGRect frameRect2 = _viewCredit.frame;
                frameRect2.origin.y =CGRectGetMaxY(_viewForPaymentDetail.frame)+5;
                _viewCredit.frame = frameRect2;
                
                CGRect frameRect3 = _viewForPaymentMode.frame;
                frameRect3.origin.y =  CGRectGetMaxY(_viewCredit.frame)+5;
                _viewForPaymentMode.frame = frameRect3;
                
                
                [_lblNewMembershipPlan setHidden:NO];
                [_btnNewPlan setHidden:NO];
                [_btnSelectPlan setHidden:NO];
                [_viewNew setHidden:NO];
                
            }
            else
            {
                
                CGRect frameRect = _viewForPaymentDetail.frame;
                frameRect.size.height = heightPaymntDetailView;
                _viewForPaymentDetail.frame = frameRect;
                
                
                CGRect frameRect1 = _viewCredit.frame;
                frameRect1.size.height = viewCreditHeight;
                _viewCredit.frame = frameRect1;
                
                
                CGRect frameRect2 = _viewCredit.frame;
                frameRect2.origin.y =CGRectGetMaxY(_viewForPaymentDetail.frame)+5;
                _viewCredit.frame = frameRect2;
                
                
                CGRect frameRect3 = _viewForPaymentMode.frame;
                frameRect3.origin.y =  CGRectGetMaxY(_viewCredit.frame)+5;
                _viewForPaymentMode.frame = frameRect3;
                
                
                [_lblNewMembershipPlan setHidden:YES];
                [_btnNewPlan setHidden:YES];
                [_btnSelectPlan setHidden:YES];
                [_viewNew setHidden:YES];
            }
            
            
            if (checkView==YES)
            {
                
                CGRect frameRect1 = _viewForCheckDetail.frame;
               // frameRect1.origin.y = CGRectGetMaxY(_viewCredit.frame)+5;
                frameRect1.origin.y = CGRectGetMaxY(_viewForPaymentMode.frame)+5;
                _viewForCheckDetail.frame = frameRect1;
                
                [_scrollView addSubview:_viewForCheckDetail];

                CGRect frameRect2 = _viewForSign.frame;
                frameRect2.origin.y =  CGRectGetMaxY(_viewForCheckDetail.frame)+5;
                _viewForSign.frame = frameRect2;
                
            }
            
            else
            {
                [_viewForCheckDetail removeFromSuperview];
                
                CGRect frameRect1 = _viewForSingleAmount.frame;
                frameRect1.origin.y = CGRectGetMaxY(_viewForPaymentMode.frame)+5;
                _viewForSingleAmount.frame = frameRect1;
                
                
                CGRect frameRect2 = _viewForSign.frame;
                frameRect2.origin.y = CGRectGetMaxY(_viewForSingleAmount.frame);
                _viewForSign.frame = frameRect2;
                
            }
            
            [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_viewForSign.frame)+5+_viewForSign.frame.size.height)] ;
            
        }
    }
    
    _buttonSelectCredit_Apply.layer.cornerRadius = 2.0;
    _buttonSelectCredit_Apply.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _buttonSelectCredit_Apply.layer.borderWidth = 1.0;
    _buttonSelectCredit_Add.layer.borderColor   = [UIColor lightGrayColor].CGColor;
    _buttonSelectCredit_Add.layer.cornerRadius = 2.0;
    _buttonSelectCredit_Add.layer.borderWidth = 1.0;
    
}

#pragma mark- ****** Local Total Caculation ******


#pragma mark- ****** Local Total Caculation ******
-(NSArray *)getArrayOfTotalRepairs
{
    NSString *strIssueRepairIdToCheck;
    NSMutableArray *arrTotal,*arrTemp;
    arrTotal=[[NSMutableArray alloc]init];
    arrTemp=[[NSMutableArray alloc]init];
    
    //Nilind
    
    for (int i=0; i<arrOfSubWorkServiceIssues.count; i++)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[i];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                
            }
        }
    }
    
    //End
    
    
    for (int k=0; k<arrTemp.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrTemp[k];
        
        NSString *strnonStdRepairAmt=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"nonStdRepairAmt"]];
        NSString *strnonStdLaborAmt=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"nonStdLaborAmt"]];
        NSString *strnonStdPartAmt=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"nonStdPartAmt"]];
        
        NSString *strRepairMasterIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"repairMasterId"]];
        
        if ([[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"1"]|| [[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"true"])
        {
            
            strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"issueRepairId"]];
            
            //  [self fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical:[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]] :strIssueRepairIdToCheck];
            //Parts Caculation
            NSMutableArray *arrTempParts=[[NSMutableArray alloc]init];
            
            float totalCostPartss=0;
            
            for (int j=0; j<arrOfSubWorkServiceIssuesRepairParts.count; j++)
            {
                
                
                NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairParts[j];
                NSString *strAddedAfterApproval=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isAddedAfterApproval"]];
                
                if ([strIssueRepairIdToCheck isEqualToString:[objTemp valueForKey:@"issueRepairId"]]) {
                    
                    if ([strAddedAfterApproval isEqualToString:@"0"]||[strAddedAfterApproval isEqualToString:@"false"]||[strAddedAfterApproval isEqualToString:@"False"])
                    {
                        
                        
                        NSString *strPartsnQty=[NSString stringWithFormat:@"%@(Qty-%@)",[objTemp valueForKey:@"partName"],[objTemp valueForKey:@"qty"]];
                        //multiplier   qty  unitPrice
                        
                        NSString *strMulti=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"multiplier"]];
                        float multip=[strMulti floatValue];
                        
                        NSString *strqtyy=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"qty"]];
                        float qtyy=[strqtyy floatValue];
                        
                        NSString *strunitPricee=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"unitPrice"]];
                        float unitPricee=[strunitPricee floatValue];
                        
                        float finalPriceParts=unitPricee*qtyy*multip;
                        
                        totalCostPartss=totalCostPartss+finalPriceParts;
                        
                        [arrTempParts addObject:strPartsnQty];
                        
                    }
                    
                }
                
            }
            
            // [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :strIssueRepairIdToCheck];
            
            float totalCostAdjustment=0;
            
            NSString *strcostAdjustment=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"costAdjustment"]];
            
            totalCostAdjustment=[strcostAdjustment floatValue];
            
            NSString *strPartCostAdjustment=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"partCostAdjustment"]];
            
            totalCostAdjustment=totalCostAdjustment+[strPartCostAdjustment floatValue];
            
            float totalCostLaborss=0;
            
            NSString *strLaborHrs,*strHelperHrs;
            for (int j=0; j<arrOfSubWorkServiceIssuesRepairLabour.count; j++)
            {
                
                NSManagedObject *objTemp=arrOfSubWorkServiceIssuesRepairLabour[j];
                
                if ([strIssueRepairIdToCheck isEqualToString:[objTemp valueForKey:@"issueRepairId"]]) {
                    
                    NSString *strWarranty=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"isWarranty"]];
                    if ([strWarranty isEqualToString:@"0"]||[strWarranty isEqualToString:@"false"]||[strWarranty isEqualToString:@"False"])
                    {
                        NSString *strLaborCostt=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborCost"]];
                        float laborCostt=[strLaborCostt floatValue];
                        
                        totalCostLaborss=totalCostLaborss+laborCostt;
                        
                        NSString *strLaborType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborType"]];
                        
                        if ([strLaborType isEqualToString:@"H"])
                        {
                            
                            strHelperHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                            
                        }
                        else
                        {
                            
                            strLaborHrs=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"laborHours"]];
                            
                        }
                        
                    }
                }
                
            }
            
            float overAllAmt=totalCostPartss+totalCostLaborss+totalCostAdjustment;
            
            // Condition change for if laborcost is 0 then no cost adjustment is to be added
            
            if (totalCostLaborss<=0) {
                
                overAllAmt = overAllAmt-[strcostAdjustment floatValue];
                
            }
            
            if (strRepairMasterIdToCheck.length==0) {
                
                if ((totalCostPartss==0) && (totalCostLaborss==0)) {
                    
                    if ([strnonStdRepairAmt isEqualToString:@"0"]) {
                        
                        strnonStdRepairAmt=@"";
                        
                    }
                    
                    if (strnonStdRepairAmt.length==0) {
                        
                        overAllAmt=overAllAmt+[strnonStdLaborAmt floatValue]+[strnonStdPartAmt floatValue];
                        
                    } else {
                        
                        overAllAmt=overAllAmt+[strnonStdRepairAmt floatValue];
                        
                    }
                    
                }
                
            }
            
            [arrTotal addObject:[NSString stringWithFormat:@"%.02f",overAllAmt]];
            
            totalLaborAmountLocal=totalCostLaborss;
            totalPartAmountLocal=totalCostPartss;
            totalLaborAmountLocalCopy=totalCostLaborss;
            totalPartAmountLocalCopy=totalCostPartss;
            
        }
        
        NSLog(@"ARRAY TOTAL %@",arrTotal);
    }
    return  arrTotal;
    
}

-(void)calculateTotal:(NSString*)strDiscount
{
    NSArray *arrTotal;
    arrTotal=[self getArrayOfTotalRepairs];
    
    double sumTotal=0;
    double subTotal,tax,amountDue;
    amountDue=0;
    
    for(int i=0;i<arrTotal.count;i++)
    {
        sumTotal=sumTotal+[[arrTotal objectAtIndex:i]doubleValue];
    }
    
    //_lblValueTotalApprovedRepairs.text=[NSString stringWithFormat:@"$%.02f",sumTotal];
    
    [self methodTotalRepairCalculateNew];
    
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    
    strDiscount=strDiscountGlobal;
    // Chnage For calculating mileage total
    
    //    float amtTC = [strChargeAmountMiles floatValue]*[_txtMiles.text floatValue];
    //
    //    _lblMileage.text=[NSString stringWithFormat:@"%.02f",amtTC];
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se

    subTotal=sumTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForMembershipPlanCharges.text]-[self getNumberString:_lblValueForMembershipPlanSavings.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txtMiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];;
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    // strTax=@"10";
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    tax=[self taxCalculation];
    
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    
    //Nilind 19 July
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    //End
    
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        // _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaid;
        
        amountPaid=[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%.02f",amountPaid];
        
        
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    [self methodToSetAmountFinally];
}

-(void)methodToSetAmountFinally{
    
    if (isFirstPlanSelected) {
        
        if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0)
        {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        }else{
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        }
        
    } else {
        
        if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
        {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        }else{
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
    }
}

-(void)calculateForRepairsNewPlan:(NSString *)strDiscount
{
#pragma mark- Calculation for Membership Plan
    //Calculation for Membership Plan
    NSArray *arrTotal;
    arrTotal=[self getArrayOfTotalRepairs];
    double sumTotalNewPlan=0;
    double subTotalNewPlan,taxNewPlan,amountDueNewPlan;
    amountDueNewPlan=0;
    for(int i=0;i<arrTotal.count;i++)
    {
        sumTotalNewPlan=sumTotalNewPlan+[[arrTotal objectAtIndex:i]doubleValue];
    }
    //_lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"$%.02f",sumTotalNewPlan];
    
    [self methodTotalRepairCalculateNew];
    
    if (chkTextEditNewPlan==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscountNewPlan.text]];
    }
    strDiscount=strDiscountGlobalNewPlan;
    
    //    float amtTC = [strChargeAmountMilesNewPlan floatValue]*[_txtMilesNewPlan.text floatValue];
    //
    //    _lblMileageNewPlan.text=[NSString stringWithFormat:@"%f",amtTC];
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se

    subTotalNewPlan=sumTotalNewPlan-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];
    if (subTotalNewPlan>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotalNewPlan];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotalNewPlan=0.00;
    }
    
    
    double taxValueNewPlan;
    // strTax=@"10";
    taxValueNewPlan=[strTax doubleValue];
    taxNewPlan=(subTotalNewPlan*taxValueNewPlan)/100;
    taxNewPlan=[self taxCalculationNewPlan];
    if (taxNewPlan>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",taxNewPlan];
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        taxNewPlan=0.00;
    }
    
    
    //Nilind 19 July
    
    double totalNewNewPlan;
    totalNewNewPlan=subTotalNewPlan+taxNewPlan;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNewNewPlan];
    
    //End
    
    
    double  finalTotalWithTaxNewPlan;
    finalTotalWithTaxNewPlan=subTotalNewPlan+taxNewPlan;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        // _lblAmountPaid.hidden=NO;
        //_lblValueForAmountPaid.hidden=NO;
        // _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTaxNewPlan];
    }
    else
    {
        // _lblAmountPaid.hidden=NO;
        // _lblValueForAmountPaid.hidden=NO;
        
        amountDueNewPlan=[[NSString stringWithFormat:@"%.02f",finalTotalWithTaxNewPlan]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        double amountPaidNewPlan;
        
        amountPaidNewPlan=[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountPaidNewPlan];
        
        
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDueNewPlan];
    }
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0)
    {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    }
    else
    {
        
        if (isOldPlan) {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    [self methodToSetAmountFinally];
}

-(double)getNumberString:(NSString*)str
{
    double value;
    NSString *strDiscount = [str stringByReplacingOccurrencesOfString:@"$" withString:@""];
    strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    value=[strDiscount doubleValue];
    return value;
}

-(NSString*)getNumberStringNew:(NSString*)str
{
    NSString *value;
    NSString *strDiscount = [str stringByReplacingOccurrencesOfString:@"$" withString:@""];
    strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    value=[NSString stringWithFormat:@"%@",strDiscount];
    return value;
}

//Nilind 12 June
//============================================================================
//============================================================================
#pragma mark- ----------------Core Data Fetch Methods----------------
//============================================================================
//============================================================================

-(void)fetchSubWorkOrderIssuesFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssues = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssues setEntity:entitySubWorkOrderIssues];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssues setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssues = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssues = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssues];
    
    [requestSubWorkOrderIssues setSortDescriptors:sortDescriptorsSubWorkOrderIssues];
    
    self.fetchedResultsControllerSubWorkOrderIssues = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssues managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssues setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssues performFetch:&error];
    arrAllObjSubWorkOrderIssues = [self.fetchedResultsControllerSubWorkOrderIssues fetchedObjects];
    arrIssueId=[[NSMutableArray alloc]init];
    
    if ([arrAllObjSubWorkOrderIssues count] == 0)
    {
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        //[_tblViewServiceIssue reloadData];
        // [self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssues.count; k++) {
            
            matchesSubWorkOrderIssues=arrAllObjSubWorkOrderIssues[k];
            NSLog(@"%@",[matchesSubWorkOrderIssues valueForKey:@"workorderId"]);
            [arrIssueId addObject:[matchesSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
            [arrOfSubWorkServiceIssues addObject:matchesSubWorkOrderIssues];
            
        }
        
        // change for equip header
        
        NSMutableArray *tempArrOfArrangedIssues=[[NSMutableArray alloc]init];
        
        NSMutableArray *arrOfIndexesToShowEquipHeader=[[NSMutableArray alloc]init];
        
        NSIndexSet* indexes = [arrOfSubWorkServiceIssues indexesOfObjectsPassingTest:^BOOL (NSManagedObject* tempObj, NSUInteger idx, BOOL *stop) {
            return [[tempObj valueForKey:@"equipmentCode"]  isEqualToString: @""];
        }];
        NSArray* recordsForNoEquip = [arrOfSubWorkServiceIssues objectsAtIndexes:indexes];
        
        if (recordsForNoEquip.count>0) {
            
            [tempArrOfArrangedIssues addObjectsFromArray:recordsForNoEquip];
            
            [arrOfSubWorkServiceIssues removeObjectsAtIndexes:indexes];
            
            [arrOfIndexesToShowEquipHeader addObject:@""];
            
        }
        
        while (arrOfSubWorkServiceIssues.count>0) {
            
            NSManagedObject *objNewTemp=arrOfSubWorkServiceIssues[0];
            NSString *strEquipCode=[NSString stringWithFormat:@"%@",[objNewTemp valueForKey:@"equipmentCode"]];
            
            [arrOfIndexesToShowEquipHeader addObject:strEquipCode];
            
            NSIndexSet* indexes = [arrOfSubWorkServiceIssues indexesOfObjectsPassingTest:^BOOL (NSManagedObject* tempObj, NSUInteger idx, BOOL *stop) {
                return [[tempObj valueForKey:@"equipmentCode"]  isEqualToString: strEquipCode];
            }];
            NSArray* recordsForNoEquip = [arrOfSubWorkServiceIssues objectsAtIndexes:indexes];
            
            [tempArrOfArrangedIssues addObjectsFromArray:recordsForNoEquip];
            
            [arrOfSubWorkServiceIssues removeObjectsAtIndexes:indexes];
            
        }
        
        arrOfSubWorkServiceIssues=nil;
        arrOfSubWorkServiceIssues=[[NSMutableArray alloc]init];
        
        arrIssueId=[[NSMutableArray alloc]init];
        arrOfSubWorkServiceIssues=tempArrOfArrangedIssues;
        
        for (int k=0; k<arrOfSubWorkServiceIssues.count; k++) {
            
            NSManagedObject *objTemp=arrOfSubWorkServiceIssues[k];
            [arrIssueId addObject:[objTemp valueForKey:@"subWorkOrderIssueId"]];
            
        }
        
        arrIndexGlobalToShowEquipHeader=[[NSMutableArray alloc]init];
        
        for (int l=0; l<arrOfIndexesToShowEquipHeader.count; l++) {
            
            //NSManagedObject *objTemp=arrOfIndexesToShowEquipHeader[l];
            NSString *strEquipCode=[NSString stringWithFormat:@"%@",arrOfIndexesToShowEquipHeader[l]];
            
            for (int m=0; m<arrOfSubWorkServiceIssues.count; m++ ) {
                
                NSManagedObject *objTemp1=arrOfSubWorkServiceIssues[m];
                NSString *strEquipCode1=[NSString stringWithFormat:@"%@",[objTemp1 valueForKey:@"equipmentCode"]];
                if ([strEquipCode isEqualToString:strEquipCode1]) {
                    
                    NSString *strIndex=[NSString stringWithFormat:@"%d",m];
                    
                    [arrIndexGlobalToShowEquipHeader addObject:strIndex];
                    
                    break;
                }
                
            }
            
            
        }
        
        //End Change for equip header
        
    }
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    //indexToShow=intGlobalSection;
    
    //[self adjustViewHeightsonCollapsingSections];
    
}

-(NSDictionary*)createTimemapForSeconds:(int)seconds{
    int hours = floor(seconds /  (60 * 60) );
    
    float minute_divisor = seconds % (60 * 60);
    int minutes = floor(minute_divisor / 60);
    
    float seconds_divisor = seconds % 60;
    seconds = ceil(seconds_divisor);
    
    NSDictionary * timeMap = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:hours], [NSNumber numberWithInt:minutes], [NSNumber numberWithInt:seconds], nil] forKeys:[NSArray arrayWithObjects:@"h", @"m", @"s", nil]];
    
    return timeMap;
}



-(void)fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        //[_tblViewServiceIssue reloadData];
        
        //[self adjustViewHeights];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepair=nil;
        arrOfSubWorkServiceIssuesRepair=[[NSMutableArray alloc]init];
        arrOfReapairMasterIdInDb=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepair.count; k++) {
            
            matchesSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[k];
            
            [arrOfSubWorkServiceIssuesRepair addObject:matchesSubWorkOrderIssuesRepair];
            
            NSString *strReapairnLaborMasterId=[NSString stringWithFormat:@"%@,%@,%@,%@",[matchesSubWorkOrderIssuesRepair valueForKey:@"repairMasterId"],[matchesSubWorkOrderIssuesRepair valueForKey:@"repairLaborId"],[matchesSubWorkOrderIssuesRepair valueForKey:@"subWorkOrderIssueId"],[matchesSubWorkOrderIssuesRepair valueForKey:@"issueRepairId"]];
            
            [arrOfReapairMasterIdInDb addObject:strReapairnLaborMasterId];
            
        }
        //[_tblViewServiceIssue reloadData];
        [self addView];
        //[self adjustViewHeights];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    //issueRepairPartId
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    NSPredicate *predicate;
    
    if ([strWoType isEqualToString:@"TM"]) {
        
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch];
        
    } else {
        
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
        
    }
    
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"workorderId"]);
            [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical :(NSString*)strIssueRepairIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strIssueRepairIdToFetch];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairLabour = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairLabour count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairLabour.count; k++) {
            
            matchesSubWorkOrderIssuesRepairLabour=arrAllObjSubWorkOrderIssuesRepairLabour[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairLabour valueForKey:@"workorderId"]);
            [arrOfSubWorkServiceIssuesRepairLabour addObject:matchesSubWorkOrderIssuesRepairLabour];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

//============================================================================
//============================================================================
#pragma mark- ---------******** Payment Info Save *******----------------
//============================================================================
//============================================================================

-(void)fetchPaymentInfoFromDataBase
{
    //MechanicalSubWOPaymentDetailDcs
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityPaymentInfoForMechanicalServiceAuto=[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
    requestPaymentInfo = [[NSFetchRequest alloc] init];
    [requestPaymentInfo setEntity:entityPaymentInfoForMechanicalServiceAuto];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestPaymentInfo setPredicate:predicate];
    
    sortDescriptorPaymentInfo = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsPaymentInfo = [NSArray arrayWithObject:sortDescriptorPaymentInfo];
    
    [requestPaymentInfo setSortDescriptors:sortDescriptorsPaymentInfo];
    
    self.fetchedResultsControllerPaymentInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestPaymentInfo managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerPaymentInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerPaymentInfo performFetch:&error];
    NSArray *arrAllObjPaymentInfo;
    
    arrAllObjPaymentInfo = [self.fetchedResultsControllerPaymentInfo fetchedObjects];
    if ([arrAllObjPaymentInfo count] == 0)
    {
        
    }
    else
    {
        
        matchesPaymentInfo=arrAllObjPaymentInfo[0];
        isPaymentInfo=YES;
        
        // NSString* woPaymentId=[matchesPaymentInfo valueForKey:@"woPaymentId"];
        NSString* workorderId=[matchesPaymentInfo valueForKey:@"workorderId"];
        strGlobalWorkOrderId=workorderId;
        NSString* paymentMode=[matchesPaymentInfo valueForKey:@"paymentMode"];
        paidAmount=[matchesPaymentInfo valueForKey:@"paidAmount"];
        NSString* checkNo=[matchesPaymentInfo valueForKey:@"checkNo"];
        NSString* drivingLicenseNo=[matchesPaymentInfo valueForKey:@"drivingLicenseNo"];
        NSString* expirationDate=[matchesPaymentInfo valueForKey:@"expirationDate"];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];//@"yyyy-MM-dd'T'HH:mm:ss"];
        NSDate* newTime = [dateFormatter dateFromString:expirationDate];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSString* finalTime = [dateFormatter stringFromDate:newTime];
        
        if (finalTime.length==0)
        {
            //strGlobalDateToShow=expirationDate;
            [_btnExpirationDate setTitle:@"Choose Expiration Date" forState:UIControlStateNormal];
        }
        else
        {
            expirationDate=finalTime;
            //strGlobalDateToShow=finalTime;
            [_btnExpirationDate setTitle:expirationDate forState:UIControlStateNormal];
        }
        
        NSString* checkFrontImagePath=[matchesPaymentInfo valueForKey:@"checkFrontImagePath"];
        if (checkFrontImagePath.length>0)
        {
            [arrOFImagesName addObject:checkFrontImagePath];
        }
        NSString* checkBackImagePath=[matchesPaymentInfo valueForKey:@"checkBackImagePath"];
        if (checkBackImagePath.length>0) {
            [arrOfCheckBackImage addObject:checkBackImagePath];
        }
        if ([paymentMode isEqualToString:@"Check"])
        {
            _txtAmountCheckView.text=paidAmount;
        }
        else
        {
            _txtAmountSingleAmount.text=paidAmount;
        }
        //Nilind 17 June
        //_lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%@",paidAmount];
        // _lblValueForAmountDue.text=@"";
        //End
        _txtCheckNo.text=checkNo;
        _txtDrivingLicenseNo.text=[matchesPaymentInfo valueForKey:@"drivingLicenseNo"];
        
        strGlobalPaymentMode=paymentMode;
        if ([strGlobalPaymentMode isEqualToString:@"Check"])
        {
            [self setButtonImage:_btnCheck];
            checkView=YES;
            [_viewForSingleAmount setHidden:NO];
            [self addView];
        }
        else if ([strGlobalPaymentMode isEqualToString:@"Cash"])
        {
            [self setButtonImage:_btnCash];
            checkView=NO;
            [_viewForSingleAmount setHidden:NO];
            [self addView];
        }
        else if ([strGlobalPaymentMode isEqualToString:@"Bill"])
        {
            [self setButtonImage:_btnBill];
            checkView=NO;
            [_viewForSingleAmount setHidden:YES];
            [self addView];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"AutoChargeCustomer"])
        {
            [self setButtonImage:_btnAutoChargeCustomer];
            checkView=NO;
            [_viewForSingleAmount setHidden:YES];
            
            [self addView];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"CreditCard"])
        {
            [self setButtonImage:_btnCreditCard];
            checkView=NO;
            [_viewForSingleAmount setHidden:NO];
            
            [self addView];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"PaymentPending"])
        {
            [self setButtonImage:_btnPaymentPending];
            [_viewForSingleAmount setHidden:YES];
            checkView=NO;
            [self addView];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"])
        {
            [self setButtonImage:_btnNoChange];
            [_viewForSingleAmount setHidden:YES];
            checkView=NO;
            [self addView];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}

-(void)finalSavePaymentInfo
{
    BOOL yesZero;
    yesZero=NO;
    
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"123456789"];
        NSRange range = [_txtAmountCheckView.text rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            // no ( or ) in the string
            yesZero=YES;
        } else {
            // ( or ) are present
            yesZero=NO;
        }
        
        if (_txtAmountCheckView.text.length==0)
        {
            
            [global AlertMethod:@"Alert!!" :@"Please enter amount"];
            
        }else if (yesZero){
            
            [global AlertMethod:@"Alert!!" :@"Please enter amount"];
            
        }
        else if (_txtCheckNo.text.length==0)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter Check #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else if (_txtDrivingLicenseNo.text.length==0)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Driving License #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }else if (_btnExpirationDate.titleLabel.text.length==0 ||[_btnExpirationDate.titleLabel.text isEqualToString:@"Choose Expiration Date"] )
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Check Expiration Date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }else if (arrOFImagesName.count==0) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Front Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else if (arrOfCheckBackImage.count==0)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Back Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }else if (strTechnicianSign.length==0) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Sign the Work Order" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"])
        {
            
            [self savePaymentInfoFirstTime:@"Check"];
            [self goToSendMail];
            
        }
        else
        {
            if (strCustomerSign.length==0) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }else{
                
                [self savePaymentInfoFirstTime:@"Check"];
                [self goToSendMail];
                
            }
            
        }
    }
    else if([strGlobalPaymentMode isEqualToString:@"Bill"]||[strGlobalPaymentMode isEqualToString:@"PaymentPending"]||[strGlobalPaymentMode isEqualToString:@"AutoChargeCustomer"]||[strGlobalPaymentMode isEqualToString:@"NoCharge"])
    {
        
        if (strTechnicianSign.length==0) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Sign the Work Order" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"])
        {
            
            [self goToSendMail];
            
        }
        else
        {
            if (strCustomerSign.length==0) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }else{
                
                [self goToSendMail];
                
            }
            
        }
        
    }
    else
    {
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"123456789"];
        NSRange range = [_txtAmountSingleAmount.text rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            // no ( or ) in the string
            yesZero=YES;
        } else {
            // ( or ) are present
            yesZero=NO;
        }
        
        if (_txtAmountSingleAmount.text.length==0)
        {
            [global AlertMethod:@"Alert!!" :@"Please enter amount"];
        }else if (yesZero){
            
            [global AlertMethod:@"Alert!!" :@"Please enter amount"];
            
        }else if (strTechnicianSign.length==0) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Sign the Work Order" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"])
        {
            
            [self savePaymentInfoFirstTime:@"NoCheck"];
            [self goToSendMail];
            
        }
        else
        {
            if (strCustomerSign.length==0) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }else{
                
                [self savePaymentInfoFirstTime:@"NoCheck"];
                [self goToSendMail];
                
            }
            
        }
    }
}
-(void)savePaymentInfoFirstTime :(NSString*)type
{
    
    // [self deleteMechanicalSubWOPaymentDetailDcs];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    
    entityPaymentInfoForMechanicalServiceAuto=[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
    
    MechanicalSubWOPaymentDetailDcs *objPaymentInfoServiceAuto = [[MechanicalSubWOPaymentDetailDcs alloc]initWithEntity:entityPaymentInfoForMechanicalServiceAuto insertIntoManagedObjectContext:context];
    
    objPaymentInfoServiceAuto.subWoPaymentId=[global getReferenceNumber];
    
    objPaymentInfoServiceAuto.workorderId=strWorkOrderId;
    
    NSString *strTypePaymentToSend;
    
    if ([strGlobalPaymentMode isEqualToString:@"CreditCard"]) {
        objPaymentInfoServiceAuto.paidAmount=_txtAmountSingleAmount.text;
        strTypePaymentToSend=@"CreditCard";
        
    } else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"NoCharge";
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Auto ChargeCustomer"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"AutoChargeCustomer";
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"PaymentPending"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"PaymentPending";
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"Bill"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"Bill";
        
    }
    else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"]){
        objPaymentInfoServiceAuto.paidAmount=@"0.00";
        strTypePaymentToSend=@"NoCharge";
        
    }
    else
    {
        //For Cash Payment
        objPaymentInfoServiceAuto.paidAmount=_txtAmountSingleAmount.text;
        strTypePaymentToSend=strGlobalPaymentMode;
        
    }
    
    objPaymentInfoServiceAuto.paymentMode=strTypePaymentToSend;
    
    if ([type isEqualToString:@"Check"])
    {
        
        objPaymentInfoServiceAuto.paidAmount=_txtAmountCheckView.text;
        objPaymentInfoServiceAuto.checkNo=_txtCheckNo.text;
        objPaymentInfoServiceAuto.drivingLicenseNo=_txtDrivingLicenseNo.text;
        if ([_btnExpirationDate.titleLabel.text isEqualToString:@"Choose Expiration Date"])
        {
            objPaymentInfoServiceAuto.expirationDate=@"";
        }
        else
        {
            objPaymentInfoServiceAuto.expirationDate=_btnExpirationDate.titleLabel.text;
        }
        //objPaymentInfoServiceAuto.expirationDate=_btnExpirationDate.titleLabel.text;
        if (!(arrOFImagesName.count==0)) {
            objPaymentInfoServiceAuto.checkFrontImagePath=arrOFImagesName[0];
        }else{
            objPaymentInfoServiceAuto.checkFrontImagePath=@"";
        }
        if (!(arrOfCheckBackImage.count==0)) {
            objPaymentInfoServiceAuto.checkBackImagePath=arrOfCheckBackImage[0];
        }else{
            objPaymentInfoServiceAuto.checkBackImagePath=@"";
        }
    }
    else
    {
        //objPaymentInfoServiceAuto.paidAmount=_txtAmountSingleAmount.text;
        objPaymentInfoServiceAuto.checkNo=@"";
        objPaymentInfoServiceAuto.drivingLicenseNo=@"";
        objPaymentInfoServiceAuto.expirationDate=@"";
        objPaymentInfoServiceAuto.checkFrontImagePath=@"";
        objPaymentInfoServiceAuto.checkBackImagePath=@"";
    }
    
    objPaymentInfoServiceAuto.createdDate=[global strCurrentDate];;
    
    objPaymentInfoServiceAuto.createdBy=[NSString stringWithFormat:@"%@",strEmployeeId];
    
    objPaymentInfoServiceAuto.modifiedDate=[global strCurrentDate];;
    
    objPaymentInfoServiceAuto.modifiedBy=[NSString stringWithFormat:@"%@",strEmployeeId];
    
    objPaymentInfoServiceAuto.userName=strUserName;
    objPaymentInfoServiceAuto.subWorkOrderId=strSubWorkOrderIdGlobal;
    objPaymentInfoServiceAuto.userName=strUserName;
    objPaymentInfoServiceAuto.recieptPath=@"";
    
    NSError *error;
    [context save:&error];
    
}
-(void)goToSendMail
{
    
    /* UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
     SendMailViewControlleriPad *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailViewControlleriPad"];
     objSendMail.strForSendProposal=@"forSendProposal";
     [self.navigationController pushViewController:objSendMail animated:NO];*/
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
    //                                                             bundle: nil];
    //    MechanicalInvoiceViewController
    //    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalInvoiceViewController"];
    //    [self.navigationController pushViewController:objByProductVC animated:NO];
    
    [self fetchSubWorkOrderActualHrsFromDataBaseForMechanical];
    
}
#pragma mark- **************** Workorder Fetch Methods ****************

-(void)fetchWorkOrderFromDataBaseForMechanical
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    NSArray* arrWorkOrder = [self.fetchedResultsControllerWorkOrderDetails fetchedObjects];
    NSManagedObject *matchesWorkOrderNew;
    if ([arrWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrderNew=arrWorkOrder[0];
        if (strTax.length==0)
        {
            strTax=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"tax"]];
            strAccountNo=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"accountNo"]];
            if (strTax.length==0)
            {
                strTax=@"0";
            }
        }
        
        strAccountNo=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"accountNo"]];
        
        //Terms and Conditions
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
        NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceReportTermsAndConditions"];
        
        strGlobalDepartmentId=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"departmentId"]];
        
        NSString *strTremsnConditions;
        if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
            
            for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                
                
                NSDictionary *dictData=arrOfServiceJobDescriptionTemplate[k];
                
                NSLog(@"Terms And Conditons===%@",dictData);
                
                if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]] isEqualToString:[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"departmentId"]]]) {
                    
                    strTremsnConditions=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SR_TermsAndConditions"]];
                    
                }
            }
            
        }else{
            
            strTremsnConditions=@"N/A";
            
        }
        
        
        NSAttributedString *attributedStringTermsnConditions = [[NSAttributedString alloc]
                                                                initWithData: [strTremsnConditions dataUsingEncoding:NSUnicodeStringEncoding]
                                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                                documentAttributes: nil
                                                                error: nil
                                                                ];
        
        _txtViewTermsnConditions.attributedText=attributedStringTermsnConditions;
        
        
        NSAttributedString *attributedStringTermsnConditionsNew = [[NSAttributedString alloc]
                                                                   initWithData: [[self getHTML] dataUsingEncoding:NSUnicodeStringEncoding]
                                                                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                                   documentAttributes: nil
                                                                   error: nil
                                                                   ];
        
        _txtViewTermsnConditions.attributedText=attributedStringTermsnConditionsNew;
        
        _txtViewTermsnConditions.editable=NO;
        
        strWorkOrderAccNo=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"accountNo"]];
        
        strWorkOrderAddressId=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"serviceAddressId"]];
        
        strWorkOrderAddressSubType=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"addressSubType"]];
        
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

- (NSString *)getHTML {
    NSDictionary *exportParams = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
    NSData *htmlData = [_txtViewTermsnConditions.attributedText dataFromRange:NSMakeRange(0, _txtViewTermsnConditions.attributedText.length) documentAttributes:exportParams error:nil];
    return [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
}


-(void)fetchSubWorkOrderFromDataBase
{
    
    matchesFinalSubWorkorder=[global fetchMechanicalSubWorkOrderObj:strWorkOrderId :_strSubWorkOderId];
    
    strWoType=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"subWOType"]];
    
    matchesWorkOrder=matchesFinalSubWorkorder;
    
    strCustomerSign=[matchesWorkOrder valueForKey:@"customerSignaturePath"];
    strTechnicianSign=[matchesWorkOrder valueForKey:@"technicianSignaturePath"];
    strDepartmentSysName=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"departmentSysName"]];
    [self downloadingImagess:strCustomerSign];
    if(CGSizeEqualToSize(imageTemp.size, CGSizeZero))
    {
        imageTemp=[UIImage imageNamed:@"NoImage.jpg"];
    }
    imageTemp=[UIImage imageNamed:@"NoImage.jpg"];
    _imgCustomerSign.image=imageTemp;
    strCustomerSign=@"";
    
    [self downloadingImagess:strTechnicianSign];
    if(CGSizeEqualToSize(imageTemp.size, CGSizeZero))//if([imageSignInspector isEqual:nil])
    {
        imageTemp=[UIImage imageNamed:@"NoImage.jpg"];
    }
    imageTemp=[UIImage imageNamed:@"NoImage.jpg"];
    _imgInspectorSign.image=imageTemp;
    strTechnicianSign=@"";
    
    strCustomerNotPresent=[matchesWorkOrder valueForKey:@"isCustomerNotPresent"];
    
    if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"])
    {
        [_imgSignCheckBox setImage:[UIImage imageNamed:@"check_box_2.png"]];
        _viewCustomerSign.hidden=YES;
    }
    else
    {
        [_imgSignCheckBox setImage:[UIImage imageNamed:@"check_box_1.png"]];
        _viewCustomerSign.hidden=NO;
        
    }
    strAmountPaidInSubWorkOrder=[matchesWorkOrder valueForKey:@"amtPaid"];
    strTripChargeId=[matchesWorkOrder valueForKey:@"tripChargeMasterId"];
    strDiagnosticChargeId=[matchesWorkOrder valueForKey:@"diagnosticChargeMasterId"];
    strMileageName=[matchesWorkOrder valueForKey:@"mileageChargesName"];
    strDiagnosticChargeName=[matchesWorkOrder valueForKey:@"diagnosticChargeName"];
    strTripChargeName=[matchesWorkOrder valueForKey:@"tripChargeName"];
    _txtMiles.text=[matchesWorkOrder valueForKey:@"totalMileage"];
    
    _txtOtherDiscounts.text=[matchesWorkOrder valueForKey:@"discountAmt"];
    strDiscountGlobal=_txtOtherDiscounts.text;
    
    //Nilind 28 July
    
    strPSPCharge=[matchesWorkOrder valueForKey:@"pSPCharges"];
    strPSPDiscount=[matchesWorkOrder valueForKey:@"pSPDiscount"];
    strAccountPSPId=[matchesWorkOrder valueForKey:@"accountPSPId"];
    strPspMasterId=[matchesWorkOrder valueForKey:@"pSPMasterId"];
    strDiagnosticChargeIdNewPlan=[matchesWorkOrder valueForKey:@"diagnosticChargeMasterId"];
    strTripChargeIdNewPlan=[matchesWorkOrder valueForKey:@"tripChargeMasterId"];
    if(strPSPCharge.length==0)
    {
        strPSPCharge=@"00.00";
    }
    if(strPSPDiscount.length==0)
    {
        strPSPDiscount=@"00.00";
    }
    _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",strPSPCharge];
    _lblValueForMembershipPlanSavings.text=[NSString stringWithFormat:@"$%@",strPSPDiscount];
    
    strPSPCharge1=[matchesWorkOrder valueForKey:@"pSPCharges"];
    strPSPDiscount=[matchesWorkOrder valueForKey:@"pSPDiscount"];
    strAccountPSPId1=[matchesWorkOrder valueForKey:@"accountPSPId"];
    strPspMasterId1=[matchesWorkOrder valueForKey:@"pSPMasterId"];
    
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    arrMechanicalPSPMasters=[[NSMutableArray alloc]init];
    arrMechanicalPSPMasters=[dictMechanicalMasters valueForKey:@"PSPMasterExtSerDc"];
    
    //StepOrder
    NSMutableArray *arrTemp;
    arrTemp=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrMechanicalPSPMasters objectAtIndex:i];
        NSString *strType;
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Type"]];
        if([strDepartmentSysName isEqualToString:[dict valueForKey:@"DepartmentSysName"]]&&[strType isEqualToString:@"General"])
        {
            [arrTemp addObject:dict];
            
        }
    }
    for (int i=0; i<arrTemp.count; i++)
    {
        NSDictionary *dict=[arrTemp objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"] ]isEqualToString:strPspMasterId])
        {
            [_btnNoMembership setTitle:[dict valueForKey:@"Title"] forState:UIControlStateNormal];
        }
    }
    
    //End
    
    [self fetchLocalMasters];
    
    for (int i=0; i<arrDiagnostic.count; i++)
    {
        NSDictionary *dict=[arrDiagnostic objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeName"] ]isEqualToString:strDiagnosticChargeName])
        {
            [_btnDiagnosticCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
            
            float amtDC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
            
            _lblValueForDiagnosticeCharge.text=[NSString stringWithFormat:@"$%.02f",amtDC];
        }
    }
    for (int i=0; i<arrTripCharge.count; i++)
    {
        NSDictionary *dict=[arrTripCharge objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeName"] ]isEqualToString:strTripChargeName])
        {
            [_btnTipCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
            
            float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
            
            _lblValueForTipCharge.text=[NSString stringWithFormat:@"$%.02f",amtTC];
        }
    }
    
    _txtFld_TripChargeQty.text=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"tripChargesQty"]];
    _lblTripChargeBasedOnQty.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTipCharge.text]*[_txtFld_TripChargeQty.text intValue]];

    //[self calculateTripChargeBasedOnQty];
    
    //Mileage Value
    for (int i=0; i<arrMileageCharge.count; i++)
    {
        NSDictionary *dict=[arrMileageCharge objectAtIndex:i];
        if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeName"] ]isEqualToString:strMileageName])
        {
            [_btnMileageCharge setTitle:[dict valueForKey:@"ChargeName"] forState:UIControlStateNormal];
            
            float amtTC=[[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]] floatValue];
            
            strChargeAmountMiles=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ChargeAmount"]];
            
            amtTC = amtTC*[_txtMiles.text floatValue];
            
            _lblMileage.text=[NSString stringWithFormat:@"%.02f",amtTC];
        }
    }
    
    //Additional Info////
    
    //_txtViewAdditionalInfo.text=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"inspectionresults"]];
    
    // technicianComment  inspectionresults
    
    NSManagedObject *objWorkOrderdetailsLocalNew=[global fetchMechanicalWorkOrderObj:strWorkOrderId];

    _txtViewAdditionalInfo.text=[NSString stringWithFormat:@"%@",[objWorkOrderdetailsLocalNew valueForKey:@"technicianComment"]];

    // For Price Not to exceed
    
    NSString *strPriceNotToExceed=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"priceNotToExceed"]];
    _txtAgreementCost.text=strPriceNotToExceed;
    
    NSString *strLaborPercent=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"laborPercent"]];
    
    NSString *strPartPercent=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"partPercent"]];
    
    if ((strPriceNotToExceed.length==0) || ([strPriceNotToExceed floatValue]<=0)) {
        
        [_imgChkBoxSplitPrice setImage:[UIImage imageNamed:@"check_box_1.png"]];
        _viewForLaborPartPercent.hidden=YES;
        
        isSplitBill=NO;
        
    } else {
        
        if ((strLaborPercent.length>0) || (strPartPercent.length>0)) {
            
            if (([strPartPercent floatValue]<=0) && ([strLaborPercent floatValue]<=0)) {
                
                [_imgChkBoxSplitPrice setImage:[UIImage imageNamed:@"check_box_1.png"]];
                _viewForLaborPartPercent.hidden=YES;
                
                isSplitBill=NO;
                
            }else{
                
                [_imgChkBoxSplitPrice setImage:[UIImage imageNamed:@"check_box_2.png"]];
                _viewForLaborPartPercent.hidden=NO;
                
                _txtLaborPercent.text=strLaborPercent;
                _txtPartPercent.text=strPartPercent;
                
                isSplitBill=YES;
                
            }
            
        } else {
            
            [_imgChkBoxSplitPrice setImage:[UIImage imageNamed:@"check_box_1.png"]];
            _viewForLaborPartPercent.hidden=YES;
            
            isSplitBill=NO;
            
        }
        
    }
    
    
    NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"isStdPrice"]];
    
    if ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) {
        
        isStandardSubWorkOrder=YES;
        
    } else {
        
        isStandardSubWorkOrder=NO;
        
    }
    
    
    //isHoliday
    
    NSString *strIsHoliday=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"isHolidayHrs"]];
    
    if ([strIsHoliday isEqualToString:@"true"] || [strIsHoliday isEqualToString:@"1"]) {
        
        isHoliday=YES;
        
    } else {
        
        isHoliday=NO;
        
    }
    
    //AfterHrsDuration
    
    strAfterHrsDuration=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"afterHrsDuration"]];
    
    [self checkForPresetSign:matchesFinalSubWorkorder];
    
}


-(void)updateWorkOrderDetail
{
    /*
     if (yesEditedSomething) {
     
     NSLog(@"Yes Edited Something In Db");
     
     ////Yaha Par zSyn ko yes karna hai
     [matchesWorkOrder setValue:@"yes" forKey:@"zSync"];
     
     }
     */
    if (chkForUpdateWorkOrder==YES)
    {
        
        //Sub Workorder
        
        [matchesFinalSubWorkorder setValue:strTechnicianSign forKey:@"technicianSignaturePath"];
        
        if([_imgSignCheckBox.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
        {
            [matchesFinalSubWorkorder setValue:@"" forKey:@"customerSignaturePath"];
        }
        else
        {
            [matchesFinalSubWorkorder setValue:strCustomerSign forKey:@"customerSignaturePath"];
        }
        //[matchesWorkOrder setValue:strCustomerSign forKey:@"customerSignaturePath"];
        [matchesFinalSubWorkorder setValue:strCustomerNotPresent forKey:@"isCustomerNotPresent"];
        // [matchesFinalSubWorkorder setValue:@"true" forKey:@"isClientApproved"];
        // [matchesFinalSubWorkorder setValue:@"Approved" forKey:@"subWOStatus"];
        
        
    }
    //Nilind 03 July
    
    
    NSString *strALreadyPaidAmt;
    if([_btnNewPlan.currentImage isEqual:[UIImage imageNamed:@"redio_button_2.png"]])
    {
        strALreadyPaidAmt=[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForAmountPaidNewPlan.text]];
        
    }
    if([_btnNoMembership.currentImage isEqual:[UIImage imageNamed:@"redio_button_2.png"]])
    {
        strALreadyPaidAmt=[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForAmountPaid.text]];
        
    }
    
    NSString *strAmtBeingPaid;
    
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        
        strAmtBeingPaid=[NSString stringWithFormat:@"%@",_txtAmountCheckView.text];
        
    }else{
        
        if ([strGlobalPaymentMode isEqualToString:@"CreditCard"]) {
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        } else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"Auto ChargeCustomer"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"PaymentPending"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"Bill"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else if ([strGlobalPaymentMode isEqualToString:@"NoCharge"]){
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",@"0.00"];
            
        }
        else
        {
            //For Cash Payment
            
            strAmtBeingPaid=[NSString stringWithFormat:@"%@",_txtAmountSingleAmount.text];
            
        }
        
        
        
    }
    
    double totalAmtPaids=[strALreadyPaidAmt doubleValue]+[strAmtBeingPaid doubleValue];
    
    [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",totalAmtPaids] forKey:@"amtPaid"];
    
    
    if([_btnNewPlan.currentImage isEqual:[UIImage imageNamed:@"redio_button_2.png"]])
    {
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _txtOtherDiscountNewPlan.text]] forKey:@"discountAmt"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForTotalAmountNewPlan.text]] forKey:@"totalApprovedAmt"];
        
        [matchesFinalSubWorkorder setValue:strTripChargeIdNewPlan forKey:@"tripChargeMasterId"];
        [matchesFinalSubWorkorder setValue:strDiagnosticChargeIdNewPlan forKey:@"diagnosticChargeMasterId"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForTripChargeNewPlan.text]] forKey:@"otherTripChargesAmt"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%@",_txtFld_TripChargeQtyNewPlan.text] forKey:@"tripChargesQty"];

        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _txtMiscellaneousChargeNewPlan.text]] forKey:@"miscChargeAmt"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForDiagnosticChargeNewPlan.text]] forKey:@"otherDiagnosticChargesAmt"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString:_lblValueForMembershipChargeNewPaln.text]] forKey:@"pSPCharges"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString:_lblValueForMembershipSavingNewPlan.text]] forKey:@"pSPDiscount"];
        
        
        if ([_btnMileageChargeNewPlan.titleLabel.text isEqualToString:@"--Mileage Charge--"] || [_btnMileageChargeNewPlan.currentTitle isEqualToString:@"--Mileage Charge--"]) {
            
            //-- Trip Charge --
            [matchesFinalSubWorkorder setValue:@"" forKey:@"mileageChargesName"];
            
        }else{
            
            [matchesFinalSubWorkorder setValue:_btnMileageChargeNewPlan.titleLabel.text forKey:@"mileageChargesName"];
            
            float amtTC = [strChargeAmountMilesNewPlan floatValue]*[_txtMilesNewPlan.text floatValue];
            
            NSString *strTotalMileageAmount = [NSString stringWithFormat:@"%.02f",amtTC];
            
            //[matchesFinalSubWorkorder setValue:strChargeAmountMilesNewPlan forKey:@"mileageChargesAmt"];
            [matchesFinalSubWorkorder setValue:strTotalMileageAmount forKey:@"mileageChargesAmt"];
            [matchesFinalSubWorkorder setValue:_txtMilesNewPlan.text forKey:@"totalMileage"];
            
        }
        
        //DiagnosticChargeName TripChargeName
        
        if ([_btnDiagnosticChargeNewPlan.titleLabel.text isEqualToString:@"-- Diagnostic Charge --"]) {
            
            //-- Trip Charge --
            [matchesFinalSubWorkorder setValue:@"" forKey:@"diagnosticChargeName"];
            
        }else{
            
            [matchesFinalSubWorkorder setValue:_btnDiagnosticChargeNewPlan.titleLabel.text forKey:@"diagnosticChargeName"];
            
        }
        if ([_btnTripChargeNewPlan.titleLabel.text isEqualToString:@"-- Trip Charge --"]) {
            
            //-- Trip Charge --
            [matchesFinalSubWorkorder setValue:@"" forKey:@"tripChargeName"];
            
        }else{
            
            [matchesFinalSubWorkorder setValue:_btnTripChargeNewPlan.titleLabel.text forKey:@"tripChargeName"];
            
        }
        
        
    }
    if([_btnNoMembership.currentImage isEqual:[UIImage imageNamed:@"redio_button_2.png"]])
    {
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _txtOtherDiscounts.text]] forKey:@"discountAmt"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueTotalApprovedRepairs.text]] forKey:@"totalApprovedAmt"];
        
        [matchesFinalSubWorkorder setValue:strTripChargeId forKey:@"tripChargeMasterId"];
        [matchesFinalSubWorkorder setValue:strDiagnosticChargeId forKey:@"diagnosticChargeMasterId"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForTipCharge.text]] forKey:@"otherTripChargesAmt"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%@",_txtFld_TripChargeQty.text] forKey:@"tripChargesQty"];

        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _txtMiscellaneousCharge.text]] forKey:@"miscChargeAmt"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForDiagnosticeCharge.text]] forKey:@"otherDiagnosticChargesAmt"];
        
        
        if ([_btnMileageCharge.titleLabel.text isEqualToString:@"--Mileage Charge--"] || [_btnMileageCharge.currentTitle isEqualToString:@"--Mileage Charge--"]) {
            
            //-- Trip Charge --
            [matchesFinalSubWorkorder setValue:@"" forKey:@"mileageChargesName"];
            
        }else{
            
            [matchesFinalSubWorkorder setValue:_btnMileageCharge.titleLabel.text forKey:@"mileageChargesName"];
            
            float amtTC = [strChargeAmountMiles floatValue]*[_txtMiles.text floatValue];
            
            NSString *strTotalMileageAmount = [NSString stringWithFormat:@"%.02f",amtTC];
            
            //[matchesFinalSubWorkorder setValue:strChargeAmountMiles forKey:@"mileageChargesAmt"];
            [matchesFinalSubWorkorder setValue:strTotalMileageAmount forKey:@"mileageChargesAmt"];

            [matchesFinalSubWorkorder setValue:_txtMiles.text forKey:@"totalMileage"];
            
        }
        
        //DiagnosticChargeName TripChargeName
        
        if ([_btnDiagnosticCharge.titleLabel.text isEqualToString:@"-- Diagnostic Charge --"]) {
            
            //-- Trip Charge --
            [matchesFinalSubWorkorder setValue:@"" forKey:@"diagnosticChargeName"];
            
        }else{
            
            [matchesFinalSubWorkorder setValue:_btnDiagnosticCharge.titleLabel.text forKey:@"diagnosticChargeName"];
            
        }
        if ([_btnTipCharge.titleLabel.text isEqualToString:@"-- Trip Charge --"]) {
            
            //-- Trip Charge --
            [matchesFinalSubWorkorder setValue:@"" forKey:@"tripChargeName"];
            
        }else{
            
            [matchesFinalSubWorkorder setValue:_btnTipCharge.titleLabel.text forKey:@"tripChargeName"];
            
        }
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString:_lblValueForMembershipPlanCharges.text]] forKey:@"pSPCharges"];
        
        [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString:_lblValueForMembershipPlanSavings.text]] forKey:@"pSPDiscount"];
        
    }
    
    if (isFirstPlan) {
        
        if ([strAccountPSPId1 isEqualToString:@"0"])
        {
            strAccountPSPId1=@"";
        }
        
        [matchesFinalSubWorkorder setValue:strAccountPSPId1 forKey:@"accountPSPId"];
        if ([strPspMasterId1 isEqualToString:@"0"] || [strPspMasterId1 isEqualToString:@"(null)"])
        {
            strPspMasterId1=@"";
        }
        [matchesFinalSubWorkorder setValue:strPspMasterId1 forKey:@"pSPMasterId"];
        
        
    } else {
        
        if ([strAccountPSPId2 isEqualToString:@"0"] || [strAccountPSPId2 isEqualToString:@"(null)"])
        {
            strAccountPSPId2=@"";
        }
        
        [matchesFinalSubWorkorder setValue:strAccountPSPId2 forKey:@"accountPSPId"];
        if ([strPspMasterId2 isEqualToString:@"0"])
        {
            strPspMasterId2=@"";
        }
        [matchesFinalSubWorkorder setValue:strPspMasterId2 forKey:@"pSPMasterId"];
        
    }
    
    
    
    [matchesFinalSubWorkorder setValue:@"" forKey:@"otherTripChargesAmt"];
    [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForTax.text]] forKey:@"taxAmt"];
    [matchesFinalSubWorkorder setValue:@"true" forKey:@"isClientApproved"];
    
    if ([strCustomerNotPresent isEqualToString:@"true"]||[strCustomerNotPresent isEqualToString:@"1"])
    {
        
        [matchesFinalSubWorkorder setValue:@"true" forKey:@"isCustomerNotPresent"];
        
    }else{
        
        [matchesFinalSubWorkorder setValue:@"false" forKey:@"isCustomerNotPresent"];
        
    }
    
    [matchesFinalSubWorkorder setValue:@"Approved" forKey:@"subWOStatus"];
    [matchesFinalSubWorkorder setValue:strCompanyKeyy forKey:@"companyKey"];
    
    [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForTipCharge.text]] forKey:@"otherTripChargesAmt"];
    
    [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _txtMiscellaneousCharge.text]] forKey:@"miscChargeAmt"];
    
    [matchesFinalSubWorkorder setValue:[NSString stringWithFormat:@"%.02f",[self getNumberString: _lblValueForDiagnosticeCharge.text]] forKey:@"otherDiagnosticChargesAmt"];
    
    //End
    
    if (isSplitBill) {
        
        [matchesFinalSubWorkorder setValue:_txtAgreementCost.text forKey:@"priceNotToExceed"];
        [matchesFinalSubWorkorder setValue:_txtPartPercent.text forKey:@"partPercent"];
        [matchesFinalSubWorkorder setValue:_txtLaborPercent.text forKey:@"laborPercent"];
        
    } else {
        
        [matchesFinalSubWorkorder setValue:_txtAgreementCost.text forKey:@"priceNotToExceed"];
        [matchesFinalSubWorkorder setValue:@"" forKey:@"partPercent"];
        [matchesFinalSubWorkorder setValue:@"" forKey:@"laborPercent"];
        
    }
    
    
    NSError *error;
    [context save:&error];
    
    
    // yaha pe final save krna on submit===methodToCheckSelectedGbbOnFinalSubmit
    
    if ([strWoType isEqualToString:@"TM"])
    {
        
        
        
    }else{
        
        [self methodToCheckSelectedGbbOnFinalSubmit];
        
    }
    
    
    NSManagedObject *objWorkOrderdetailsLocal=[global fetchMechanicalWorkOrderObj:strWorkOrderId];
    
    if (isPreSetSignGlobal) {
        
        [objWorkOrderdetailsLocal setValue:@"true" forKey:@"isEmployeePresetSignature"];
        
    } else {
        
        [objWorkOrderdetailsLocal setValue:@"false" forKey:@"isEmployeePresetSignature"];
        
    }
    
    NSError *error1;
    [context save:&error1];
    
    [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderId];
    
    [self saveTechComments];
    
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)texth
{
    if (range.location == 0 && [texth isEqualToString:@" "]) {
        return NO;
    }
    else
        return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [self saveTechComments];
    
}
#pragma mark- **************** Textfield Delegate Methods ****************

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self calculateTripChargeBasedOnQty];
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    if(textField==_txtFld_TripChargeQty)
    {
        BOOL isNumberOnly =  [global isNumberOnly:string :range :3 :(int)textField.text.length :@"0123456789"];
        
        if (isNumberOnly) {
            
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtFld_TripChargeQty.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtFld_TripChargeQty.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            [self calculateTripChargeBasedOnQtyOnTextFldEdit:string];
            
        }
        
        return isNumberOnly;

    }else if(textField==_txtFld_TripChargeQtyNewPlan)
    {
        BOOL isNumberOnly =  [global isNumberOnly:string :range :3 :(int)textField.text.length :@"0123456789"];
        
        if (isNumberOnly) {
            
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtFld_TripChargeQtyNewPlan.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtFld_TripChargeQtyNewPlan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            [self calculateTripChargeBasedOnQtyOnTextFldEdit:string];
            
        }
        
        return isNumberOnly;
        
    }else if(textField==_txtCheckNo)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 7;
    }
    else if(textField==_txtDrivingLicenseNo)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 15;
    }
    else if(textField==_txtOtherDiscounts)
    {
        NSString *newString = [_txtOtherDiscounts.text stringByReplacingCharactersInRange:range withString:string];
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] >= 2)
        {
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            if (!([sepStr length]>2))
            {
                if ([sepStr length]==2 && [string isEqualToString:@"."])
                {
                    return NO;
                }
                else
                {
                    chkTextEdit=YES;
                    if([string isEqualToString:@""])
                    {
                        NSString *str1 =_txtOtherDiscounts.text;                        NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                        string=[NSString stringWithFormat:@"%@%@",newString,string];
                        
                    }
                    else
                    {
                        NSString *strDiscount = [_txtOtherDiscounts.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                        strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
                    }
                    
                    strDiscountGlobal=string;
                    
#pragma mark - ***************** FOR ISSUE *****************
                    //[self calculateTotal:string];
#pragma mark - ***************** FOR PARTS *****************
                    if ([strWoType isEqualToString:@"FR"])
                    {
                        [self calculateTotal:string];
                    }
                    else if ([strWoType isEqualToString:@"TM"])
                    {
                        [self calculateTotalForParts:string];
                    }
                    
                    
                    return YES;
                    
                }
            }
            else
            {
                return NO;
            }
        }
        else
        {
            chkTextEdit=YES;
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtOtherDiscounts.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtOtherDiscounts.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            strDiscountGlobal=string;
            
#pragma mark - ***************** FOR ISSUE *****************
            
            //[self calculateTotal:string];
#pragma mark - ***************** FOR PARTS *****************
            
            //[self calculateTotalForParts:string];
            if ([strWoType isEqualToString:@"FR"])
            {
                [self calculateTotal:string];
            }
            else if ([strWoType isEqualToString:@"TM"])
            {
                [self calculateTotalForParts:string];
            }
            
            return YES;
        }
        
    }
    else if(textField==_txtOtherDiscountNewPlan)
    {
        NSString *newString = [_txtOtherDiscountNewPlan.text stringByReplacingCharactersInRange:range withString:string];
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] >= 2)
        {
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            if (!([sepStr length]>2))
            {
                if ([sepStr length]==2 && [string isEqualToString:@"."])
                {
                    return NO;
                }
                else
                {
                    chkTextEditNewPlan=YES;
                    if([string isEqualToString:@""])
                    {
                        NSString *str1 =_txtOtherDiscountNewPlan.text;                        NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                        string=[NSString stringWithFormat:@"%@%@",newString,string];
                        
                    }
                    else
                    {
                        NSString *strDiscount = [_txtOtherDiscountNewPlan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                        strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
                    }
                    
                    strDiscountGlobalNewPlan=string;
#pragma mark - ***************** FOR ISSUE *****************
                    //[self calculateTotal:string];
#pragma mark - ***************** FOR PARTS *****************
                    if ([strWoType isEqualToString:@"FR"])
                    {
                        [self calculateForRepairsNewPlan:string];
                    }
                    else if ([strWoType isEqualToString:@"TM"])
                    {
                        [self calculateForPartsNewPlan:string];
                    }
                    
                    
                    return YES;
                    
                }
            }
            else
            {
                return NO;
            }
        }
        else
        {
            chkTextEditNewPlan=YES;
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtOtherDiscountNewPlan.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtOtherDiscountNewPlan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            strDiscountGlobalNewPlan=string;
#pragma mark - ***************** FOR ISSUE *****************
            
            //[self calculateTotal:string];
#pragma mark - ***************** FOR PARTS *****************
            
            //[self calculateTotalForParts:string];
            if ([strWoType isEqualToString:@"FR"])
            {
                [self calculateForRepairsNewPlan:string];
            }
            else if ([strWoType isEqualToString:@"TM"])
            {
                [self calculateForPartsNewPlan:string];
            }
            
            return YES;
        }
        
    }
    else if (textField==_txtAmountCheckView)
    {
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtAmountCheckView.text];
        
        return isNuberOnly;
    }
    else if (textField==_txtAmountSingleAmount)
    {
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtAmountSingleAmount.text];
        
        return isNuberOnly;
    }
    else if (textField==_txtPartPercent){
        
        BOOL isNuberOnly=[global isNumberOnly:string :range :10 :(int)textField.text.length :@".0123456789"];
        
        return isNuberOnly;
        
    }
    else if (textField==_txtLaborPercent){
        
        BOOL isNuberOnly=[global isNumberOnly:string :range :10 :(int)textField.text.length :@".0123456789"];
        
        return isNuberOnly;
        
    }
    else if (textField==_txtMiles){
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtMiles.text];
        
        //  BOOL isNuberOnly=[global isNumbernDecimalOnly:string :range :10 :(int)textField.text.length :@".0123456789"];
        
        if (isNuberOnly) {
            
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtMiles.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtMiles.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            float amtTC = [strChargeAmountMiles floatValue]*[string floatValue];
            
            _lblMileage.text=[NSString stringWithFormat:@"%.02f",amtTC];
            
            if ([strWoType isEqualToString:@"FR"])
            {
                [self calculateTotal:_txtOtherDiscounts.text];
            }
            else if ([strWoType isEqualToString:@"TM"])
            {
                [self calculateTotalForParts:_txtOtherDiscounts.text];
            }
            
        }
        
        return isNuberOnly;
        
    }
    else if (textField==_txtMilesNewPlan){
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtMilesNewPlan.text];
        
        //BOOL isNuberOnly=[global isNumbernDecimalOnly:string :range :10 :(int)textField.text.length :@".0123456789"];
        
        if (isNuberOnly) {
            
            if([string isEqualToString:@""])
            {
                NSString *str1 =_txtMilesNewPlan.text;
                NSString *newString = [str1 stringByReplacingCharactersInRange:NSMakeRange(range.location,1) withString:@""];
                string=[NSString stringWithFormat:@"%@%@",newString,string];
                
            }
            else
            {
                
                NSString *strDiscount = [_txtMilesNewPlan.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                strDiscount = [strDiscount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                string=[NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%@",strDiscount],string];
            }
            
            float amtTC = [strChargeAmountMilesNewPlan floatValue]*[string floatValue];
            
            _lblMileageNewPlan.text=[NSString stringWithFormat:@"%.02f",amtTC];
            
            if ([strWoType isEqualToString:@"FR"])
            {
                [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
            }
            else if ([strWoType isEqualToString:@"TM"])
            {
                [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
            }
            
        }
        
        return isNuberOnly;
        
    }else if (textField==_txtMiscellaneousCharge){
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtMiscellaneousCharge.text];
        
        //BOOL isNuberOnly=[global isNumbernDecimalOnly:string :range :10 :(int)textField.text.length :@".0123456789"];  addingMiscellenousCharge
        
        if (isNuberOnly) {
            
            NSString *newString = [_txtMiscellaneousCharge.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            if([sep count] >= 2)
            {
                NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                if (!([sepStr length]>2))
                {
                    if ([sepStr length]==2 && [string isEqualToString:@"."])
                    {
                        return NO;
                    }
                    else
                    {
                        [self performSelector:@selector(addingMiscellenousCharge) withObject:nil afterDelay:0.5];
                    }
                }else {
                    return NO;
                }
            }
            
        }
        
        return isNuberOnly;
        
    }
    else if (textField==_txtMiscellaneousChargeNewPlan){
        
        BOOL isNuberOnly=[global isNumberOnlyWithDecimal:string :range :10 :(int)textField.text.length :@".0123456789" :_txtMiscellaneousChargeNewPlan.text];
        
        //BOOL isNuberOnly=[global isNumbernDecimalOnly:string :range :10 :(int)textField.text.length :@".0123456789"];  addingMiscellenousChargeNewPlan
        
        if (isNuberOnly) {
            
            NSString *newString = [_txtMiscellaneousChargeNewPlan.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *sep = [newString componentsSeparatedByString:@"."];
            if([sep count] >= 2)
            {
                NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                if (!([sepStr length]>2))
                {
                    if ([sepStr length]==2 && [string isEqualToString:@"."])
                    {
                        return NO;
                    }
                    else
                    {
                        [self performSelector:@selector(addingMiscellenousChargeNewPlan) withObject:nil afterDelay:0.5];
                    }
                }else {
                    return NO;
                }
            }
            
        }
        
        return isNuberOnly;
        
    }
    else
    {
        return YES;
    }
}

-(void)addingMiscellenousCharge{
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
    
}

-(void)addingMiscellenousChargeNewPlan{
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    
}
#pragma mark- ******************** FOR PARTS ***********************

#pragma mark- ------Cell Button Method-------

-(void)buttonClickOnArriveForParts:(id)sender
{
    strButtonClickParts=@"arrive";
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tblIssueParts];
    NSIndexPath *indexPath = [_tblIssueParts indexPathForRowAtPoint:buttonPosition];
    NSLog(@"selected tableview row is %ld",(long)indexPath.row);
    
    MechanicalIssuePartsTableViewCell *tappedCell = (MechanicalIssuePartsTableViewCell *)[_tblIssueParts cellForRowAtIndexPath:indexPath];
    
    NSLog(@"Issue Id =%@  ",tappedCell.lblIssueId.text);
    NSManagedObject *record,*finalRecord;
    for (int i=0; i<arrAllParts.count; i++)
    {
        record = [arrAllParts objectAtIndex:i];
        if([tappedCell.lblIssueId.text isEqualToString:[record valueForKey:@"issueRepairPartId"]])
        {
            finalRecord=record;
            break;
        }
    }
    [self updateForParts:finalRecord];
    // [self calculateTotalForParts:_txtOtherDiscounts.text];
    [self methodCalculationOverAll];
    
    [self showAmountInTextFieldToCharge];
}
-(void)buttonClickOnDeclineForParts:(id)sender
{
    strButtonClickParts=@"decline";
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tblIssueParts];
    NSIndexPath *indexPath = [_tblIssueParts indexPathForRowAtPoint:buttonPosition];
    NSLog(@"selected tableview row is %ld",(long)indexPath.row);
    
    MechanicalIssuePartsTableViewCell *tappedCell = (MechanicalIssuePartsTableViewCell *)[_tblIssueParts cellForRowAtIndexPath:indexPath];
    
    NSLog(@"Issue Id =%@  ",tappedCell.lblIssueId.text);
    NSManagedObject *record,*finalRecord;
    for (int i=0; i<arrAllParts.count; i++)
    {
        record = [arrAllParts objectAtIndex:i];
        if([tappedCell.lblIssueId.text isEqualToString:[record valueForKey:@"issueRepairPartId"]])
        {
            finalRecord=record;
            break;
        }
    }
    
    [self updateForParts:finalRecord];
    //[self calculateTotalForParts:_txtOtherDiscounts.text];
    [self methodCalculationOverAll];
    
    [self showAmountInTextFieldToCharge];
}
-(void)updateForParts:(NSManagedObject *)matchesObject
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    NSManagedObject *matchesTemp;
    matchesTemp=matchesObject;
    NSLog(@"Workorder Id=%@\n",[matchesTemp valueForKey:@"workorderId"]);
    NSLog(@"before =%@",[matchesTemp valueForKey:@"customerFeedback"]);
    if ([strButtonClickParts isEqualToString:@"arrive"])
    {
        [matchesTemp setValue:@"true" forKey:@"customerFeedback"];
    }
    else if ([strButtonClickParts isEqualToString:@"decline"])
    {
        [matchesTemp setValue:@"false" forKey:@"customerFeedback"];
    }
    NSLog(@"after =%@",[matchesTemp valueForKey:@"customerFeedback"]);
    [_tblIssueParts reloadData];
    
    NSError *error;
    [context save:&error];
    
}


-(NSArray *)getArrayOfTotalParts
{
    NSString *strIssueRepairIdToCheck;
    NSMutableArray *arrTotal,*arrTemp;
    arrTotal=[[NSMutableArray alloc]init];
    arrTemp=[[NSMutableArray alloc]init];
    
    //Nilind
    
    for (int i=0; i<arrOfSubWorkServiceIssues.count; i++)
    {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[i];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        
        for (int k=0; k<arrAllParts.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrAllParts[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                
            }
        }
    }
    
    //End
    
    for (int k=0; k<arrTemp.count; k++)
    {
        
        NSManagedObject *dictIssuesRepairData=arrTemp[k];
        
        
        NSManagedObject *objTemp=arrTemp[k];
        
        if ([[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"1"]|| [[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]]isEqualToString:@""])
        {
            NSString *strMulti=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"multiplier"]];
            float multip=[strMulti floatValue];
            
            NSString *strqtyy=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"qty"]];
            float qtyy=[strqtyy floatValue];
            
            NSString *strunitPricee=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"unitPrice"]];
            float unitPricee=[strunitPricee floatValue];
            
            float finalPriceParts=unitPricee*qtyy*multip;
            float totalCostPartss=0;
            totalCostPartss=totalCostPartss+finalPriceParts;
            [arrTotal addObject:[NSString stringWithFormat:@"%.02f",totalCostPartss]];
            
        }
        
        NSLog(@"ARRAY TOTAL %@",arrTotal);
        
    }
    return arrTotal;
    
}
-(void)calculateTotalForParts:(NSString*)strDiscount
{
    NSArray *arrTotal;
    arrTotal=[self getArrayOfTotalParts];
    double sumTotal=0;
    double otherDiscounts,subTotal,tax,amountDue;
    for(int i=0;i<arrTotal.count;i++)
    {
        sumTotal=sumTotal+[[arrTotal objectAtIndex:i]doubleValue];
    }
    
    totalPartAmountLocal = sumTotal;
    totalPartAmountLocalCopy = sumTotal;
    totalLaborAmountLocal=0.0;
    totalLaborAmountLocalCopy=0.0;
    
    _lblValueTotalApprovedRepairs.text=[NSString stringWithFormat:@"$%.02f",sumTotal];
    if (chkTextEdit==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscounts.text]];
    }
    strDiscount=strDiscountGlobal;
    [self methodToCalulateMembershipCharge];
    //    float amtTC = [strChargeAmountMiles floatValue]*[_txtMiles.text floatValue];
    //
    //    _lblMileage.text=[NSString stringWithFormat:@"%.02f",amtTC];
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTipCharge ko replace kiya hai _lblTripChargeBasedOnQty se

    subTotal=sumTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticeCharge.text]+[self getNumberString:_lblTripChargeBasedOnQty.text]+[self getNumberString:_lblValueForMembershipPlanCharges.text]-[self getNumberString:_lblValueForMembershipPlanSavings.text]+[self getNumberString:_lblMileage.text]+[self getNumberString:_txtMiscellaneousCharge.text]-[self getNumberString:_labelCreditsPaymentDetailViewLeft.text];;
    if (subTotal>0) {
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotal.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    tax=[self taxCalculationForParts:subTotal :@"old"];
    if (tax>0) {
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTax.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    
    //Nilind 19 July
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotal.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    //End
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaid.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaid.text=[NSString stringWithFormat:@"$%@",strAmountPaidInSubWorkOrder];
        
        _lblValueForAmountDue.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
    _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    } else {
        
        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        
    }
    [self methodToSetAmountFinally];
}
-(void)calculateForPartsNewPlan:(NSString*)strDiscount
{
    NSArray *arrTotal;
    arrTotal=[self getArrayOfTotalParts];
    double sumTotal=0;
    double otherDiscounts,subTotal,tax,amountDue;
    for(int i=0;i<arrTotal.count;i++)
    {
        sumTotal=sumTotal+[[arrTotal objectAtIndex:i]doubleValue];
    }
    
    totalPartAmountLocal = sumTotal;
    totalPartAmountLocalCopy = sumTotal;
    totalLaborAmountLocal=0.0;
    totalLaborAmountLocalCopy=0.0;
    
    _lblValueForTotalAmountNewPlan.text=[NSString stringWithFormat:@"$%.02f",sumTotal];
    if (chkTextEditNewPlan==NO)
    {
        strDiscount=[NSString stringWithFormat:@"%.02f",[self getNumberString:_txtOtherDiscountNewPlan.text]];
    }
    strDiscount=strDiscountGlobalNewPlan;
    [self methodToCalulateMembershipCharge];
    
    //    float amtTC = [strChargeAmountMilesNewPlan floatValue]*[_txtMilesNewPlan.text floatValue];
    //
    //    _lblMileageNewPlan.text=[NSString stringWithFormat:@"%f",amtTC];
    
    //[self calculateTripChargeBasedOnQty];
    
    // _lblValueForTripChargeNewPlan ko replace kiya hai lblTripChargeBasedOnQtyNewPlan se

    subTotal=sumTotal-[strDiscount doubleValue]+[self getNumberString:_lblValueForDiagnosticChargeNewPlan.text]+[self getNumberString:_lblTripChargeBasedOnQtyNewPlan.text]+[self getNumberString:_lblValueForMembershipChargeNewPaln.text]-[self getNumberString:_lblValueForMembershipSavingNewPlan.text]+[self getNumberString:_lblMileageNewPlan.text]+[self getNumberString:_txtMiscellaneousChargeNewPlan.text]-[self getNumberString:_labelCreditsPaymentDetailViewRight.text];;
    
    if (subTotal>0) {
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",subTotal];
        
    }else{
        
        _lblValueForSubtotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        subTotal=0.00;
    }
    
    
    double taxValue;
    taxValue=[strTax doubleValue];
    tax=(subTotal*taxValue)/100;
    tax=[self taxCalculationForParts:subTotal :@"NewPlan"];
    
    if (tax>0) {
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",tax];
        
    }else{
        
        _lblValueForTaxNewPlan.text=[NSString stringWithFormat:@"$%.02f",0.00];
        tax=0.00;
    }
    
    //Nilind 19 July
    
    double totalNew;
    totalNew=subTotal+tax;
    _lblValueForTotalNewPlan.text=[NSString stringWithFormat:@"$%.02f",totalNew];
    
    //End
    
    double  finalTotalWithTax;
    finalTotalWithTax=subTotal+tax;
    
    if ([strAmountPaidInSubWorkOrder doubleValue]==0)
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",finalTotalWithTax];
    }
    else
    {
        _lblAmountPaid.hidden=NO;
        _lblValueForAmountPaidNewPlan.hidden=NO;
        
        amountDue=[[NSString stringWithFormat:@"%.02f",finalTotalWithTax]doubleValue]-[strAmountPaidInSubWorkOrder doubleValue];
        
        _lblValueForAmountPaidNewPlan.text=[NSString stringWithFormat:@"$%@",strAmountPaidInSubWorkOrder];
        
        _lblValueForAmountDueNewPlan.text=[NSString stringWithFormat:@"$%.02f",amountDue];
    }
    
    _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
    _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    } else {
        
        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
        
    }
    [self methodToSetAmountFinally];
}


//Nilind 19 June
#pragma mark- ------Core Data Fetch Methods For Parts-------

-(void)fetchSubWorkOrderIssuesPartsFromDataBase:(NSString *)strSubWorkOrderIssueIdToFetch
{
    //subWorkOrderIssueId
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++)
        {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"workorderId"]);
            [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
            
        }
        
    }
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else
    {
    }
    [self addView];
    
}
-(void)fetchAllParts
{
    arrAllParts=[[NSMutableArray alloc]init];
    //    for (int i=0; i<arrIssueId.count; i++)
    //    {
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        //  arrOfSubWorkServiceIssuesRepairParts=nil;
        //arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
    }
    else
    {
        //arrOfSubWorkServiceIssuesRepairParts=nil;
        //arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++)
        {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"workorderId"]);
            //[arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
            [arrAllParts addObject:matchesSubWorkOrderIssuesRepairParts];
            
        }
        
    }
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else
    {
    }
    
    //    }
    
}

-(void)deleteMechanicalSubWOPaymentDetailDcs{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete entitySubWorkOrderIssuesRepairParts Data
    entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",_strSubWorkOderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


-(void)fetchSubWorkOrderActualHrsFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        [self stopLoader];
        
        //        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        //        [defs setValue:@"" forKey:@"subWOActualHourId"];
        //        [defs synchronize];
        
    }
    else
    {
        
        BOOL isTimeout;
        isTimeout=NO;
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strTimeOutJob=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"mobileTimeOut"]];
            
            NSString *strSubWorkOrderStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"status"]];
            
            if ([strSubWorkOrderStatus isEqualToString:@"OnRoute"]) {
                
                // isTimeout=YES;
                
            } else {
                
                if (strTimeOutJob.length==0){
                    
                    isTimeout=YES;
                    
                    matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
                    
                    NSString *strCureentTimeToset=[global strCurrentDateFormattedForMechanical];
                    
                    [matchesSubWorkOrderActualHrs setValue:strCureentTimeToset forKey:@"mobileTimeOut"];
                    
                    //reason
                    
                    [matchesSubWorkOrderActualHrs setValue:ReasonClientApproval forKey:@"reason"];
                    
                    
                    BOOL isNetReachable=[global isNetReachable];
                    
                    if (isNetReachable) {
                        
                        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Updating Time..."];
                        
                        NSString *strNotificationTypeName=@"MechanicalActualHrsSyncClient";
                        
                        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:strNotificationTypeName object:nil];
                        
                        SyncMechanicalViewController *objSync=[[SyncMechanicalViewController alloc]init];
                        
                        [objSync syncMechanicalActualHours:strWorkOrderId :strSubWorkOrderIdGlobal :[matchesSubWorkOrderActualHrs valueForKey:@"subWOActualHourId"] :strNotificationTypeName];
                        
                    }else{
                        
                        [self stopLoader];
                        
                    }
                    
                    [global fetchWorkOrderFromDataBaseForMechanicalToUpdateClockStatus:strWorkOrderId : strSubWorkOrderIdGlobal :@"Stop"];
                    
                    NSError *error2;
                    [context save:&error2];
                    
                    
                    // Create dynamic view for employee time sheet slot wise.....
                    arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
                    arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
                    
                    arrOfGlobalDynamicEmpSheetFinal=[global createEmpSheetDataInBackground:strWorkOrderId :strSubWorkOrderIdGlobal :arrOfHoursConfig :strEmpID :strEmployeeNoLoggedIn :strEmpName :@"Background" :strCureentTimeToset];
                    
                    //arrOfHeaderTitleForSlots=[global arrOfHeaderTitleGlobal:arrOfHoursConfig];
                    
                    break;
                    
                }
            }
        }
        
        if (!isTimeout) {
            
            [self stopLoader];
            
        }
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}


-(void)stopLoader
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"MechanicalActualHrsSyncClient"
                                                  object:nil];
    
    
    [DejalBezelActivityView removeView];
    
    if (isForSubmitWork) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"MechanicalInvoice" forKey:@"WhichScreen"];
        [defs synchronize];
        
    } else {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"MechanicalProcessWorkOrder" forKey:@"WhichScreen"];
        [defs synchronize];
        
    }
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    SendMailMechanicaliPadViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailMechanicaliPadViewController"];
    objSendMail.strWorkOrderId=strWorkOrderId;
    objSendMail.strSubWorkOrderId=_strSubWorkOderId;
    objSendMail.strFromWhere=@"S";
    [self.navigationController pushViewController:objSendMail animated:NO];
    
}

- (IBAction)actionOnSplitPrice:(UIButton*)sender
{
    if ([_imgChkBoxSplitPrice.image isEqual:[UIImage imageNamed:@"check_box_2.png"]])
    {
        isSplitBill=NO;
        [_imgChkBoxSplitPrice setImage:[UIImage imageNamed:@"check_box_1.png"]];
        sender.selected=YES;
        _viewForLaborPartPercent.hidden=YES;
    }
    else
    {
        isSplitBill=YES;
        [_imgChkBoxSplitPrice setImage:[UIImage imageNamed:@"check_box_2.png"]];
        sender.selected=NO;
        _viewForLaborPartPercent.hidden=NO;
        
        if (_txtPartPercent.text.length>0) {
            
            
        }else{
            
            _txtPartPercent.text=@"50";
            _txtLaborPercent.text=@"50";
            
        }
        
    }
    
}

- (IBAction)actionOnNoMembership:(id)sender
{
    
    isFirstPlanSelected=YES;
    isFirstPlan=YES;
    _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
    _viewOld.layer.borderWidth=2.0;
    _viewOld.layer.cornerRadius=5.0;
    
    _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewNew.layer.borderWidth=2.0;
    _viewNew.layer.cornerRadius=5.0;
    
    _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
    _viewNew.backgroundColor=[UIColor whiteColor];
    
    chkPlanClick=YES;
    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
    [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
    NSDictionary *dict;
    
    dict=[arrPlanStatusResponse objectAtIndex:0];
    
    //strAccountPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
    strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanCharges.text]];
    strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanSavings.text]];
    
    strPSPCharge1=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForMembershipPlanCharges.text]];
    strPSPDiscountAmount1=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForMembershipPlanSavings.text]];
    
    
    [self methodCalculationOverAll];
    
    if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    } else {
        
        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
        
    }
    
    isOldPlan=YES;
    
    [self showAmountInTextFieldToCharge];
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self methodTotalRepairCalculateNew];
        [self methodToCalulateFRTotal];
        [self methodCalculateFRTotalNewPlan];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
    }
    
    // Akshay Start //
    [self calculateDiscountPrice];
    [_tableViewCreditPaymntDetailLeft reloadData];
    [self callToChangeCalculationsOverAll];
    // Akshay End //
    
}

- (IBAction)actionOnNewPlan:(id)sender
{
    isFirstPlan=NO;
    isFirstPlanSelected=NO;
    _viewNew.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
    _viewNew.layer.borderWidth=2.0;
    _viewNew.layer.cornerRadius=5.0;
    
    _viewOld.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _viewOld.layer.borderWidth=2.0;
    _viewOld.layer.cornerRadius=5.0;
    
    _viewNew.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
    _viewOld.backgroundColor=[UIColor whiteColor];
    
    chkPlanClick=YES;
    
    [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
    NSDictionary *dict;
    dict=[arrPlanStatusResponse objectAtIndex:1];
    // strAccountPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
    strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipChargeNewPaln.text]];
    strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipSavingNewPlan.text]];
    strPSPCharge2=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForMembershipChargeNewPaln.text]];
    strPSPDiscountAmount2=[NSString stringWithFormat:@"%f",[self getNumberString:_lblValueForMembershipSavingNewPlan.text]];
    
    [self methodCalculationOverAll];
    
    if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0) {
        
        _txtAmountCheckView.text=@"";
        _txtAmountSingleAmount.text=@"";
        
    } else {
        
        _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
        _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
        
    }
    
    isOldPlan=NO;
    
    [self showAmountInTextFieldToCharge];
    
    //    [self methodTotalRepairCalculateNew];
    //    [self methodToCalulateFRTotal];
    //    [self methodCalculateFRTotalNewPlan];
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self methodTotalRepairCalculateNew];
        [self methodToCalulateFRTotal];
        [self methodCalculateFRTotalNewPlan];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
    }
    
    // Akshay Start //
    [self calculateDiscountPrice];
    [_tableViewCreditPaymntDetailRight reloadData];
    [self callToChangeCalculationsOverAll];
    // Akshay End //
    
}

-(void)methodToSetAmount{
    
    if (isOldPlan) {
        
        if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    } else {
        
        if ([[self getNumberStringNew:_lblValueForAmountDueNewPlan.text] intValue]<0) {
            
            _txtAmountCheckView.text=@"";
            _txtAmountSingleAmount.text=@"";
            
        } else {
            
            _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
            
        }
        
    }
    
    NSLog(@"amount is equal to======%@---%@",_txtAmountSingleAmount.text,_txtAmountCheckView.text);
    
}
- (IBAction)actionOnSelectPlan:(id)sender
{
    if (arrPSPMasters.count==0) {
        
        [global AlertMethod:Alert :@"No Plan available"];
        
    } else {
        
        tblData.tag=50;
        [self tableLoad:tblData.tag];
        
    }
    
}

- (IBAction)actionOnTripChargeNewPlan:(id)sender
{
    [self.view endEditing:YES];
    
    if (arrTripCharge.count==0) {
        
        [global AlertMethod:Alert :@"No trip charge available"];
        
    } else {
        
        tblData.tag=40;
        [self tableLoad:tblData.tag];
        
    }
}

- (IBAction)actionOnDiagnosticChargeNewPlan:(id)sender
{
    [self.view endEditing:YES];
    if (arrDiagnostic.count==0) {
        
        [global AlertMethod:Alert :@"No diagnostic charge available"];
        
    } else {
        
        tblData.tag=30;
        [self tableLoad:tblData.tag];
        
    }
}

#pragma mark-  -------------------- GET PLAN -------------------

-(void)deletePspPlansFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityPspPlans=[NSEntityDescription entityForName:@"PspPlans" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"PspPlans" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ && companyKey = %@ && empId = %@ && userName = %@",[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"accountNo"]],strCompanyKeyy,strEmpID,strUserName];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

-(NSArray*)fetchPspPlansFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityPspPlans=[NSEntityDescription entityForName:@"PspPlans" inManagedObjectContext:context];
    requestPspPlans= [[NSFetchRequest alloc] init];
    [requestPspPlans setEntity:entityPspPlans];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ && companyKey = %@ && empId = %@ && userName = %@",[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"accountNo"]],strCompanyKeyy,strEmpID,strUserName];
    
    [requestPspPlans setPredicate:predicate];
    
    sortDescriptorPspPlans = [[NSSortDescriptor alloc] initWithKey:@"accountNo" ascending:NO];
    sortDescriptorsPspPlans = [NSArray arrayWithObject:sortDescriptorPspPlans];
    
    [requestPspPlans setSortDescriptors:sortDescriptorsPspPlans];
    
    self.fetchedResultsControllerPspPlans = [[NSFetchedResultsController alloc] initWithFetchRequest:requestPspPlans managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerPspPlans setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerPspPlans performFetch:&error];
    arrAllObjPspPlans = [self.fetchedResultsControllerPspPlans fetchedObjects];
    
    return arrAllObjPspPlans;
    
}

-(void)getPlan
{
    //  [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
        
        NSArray *arrTemp =  [self fetchPspPlansFromDB];
        
        if (arrTemp.count>0) {
            
            NSManagedObject *objTemp = arrTemp[0];
            
            NSArray *arrPlansDB = [objTemp valueForKey:@"arrOfPlans"];
            
            [self setPlansNewMethod:arrPlansDB : @"Offline"];
            
        } else {
            
            [self noInternetThenPSP];
            
        }
        
    }
    else
    {
        NSString *strUrl;
        strUrl = [NSString stringWithFormat:@"%@%@%@&departmentSysName=%@&accountNo=%@",strServiceUrlMainServiceAutomation,UrlGetPlanMechanical,strCompanyKeyy,strDepartmentSysName,strAccountNo],
        
        NSLog(@"GET PLAN  URl-----%@",strUrl);
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        NSURL *url = [NSURL URLWithString:strUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        
        if (jsonData==nil)
        {
            
            NSString *strTitle = Alert;
            NSString *strMsg = Sorry;
            [global AlertMethod:strTitle :strMsg];
            [DejalActivityView removeView];
            [DejalBezelActivityView removeView];
            
        }
        else
        {
            
            NSArray* responseArr = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            [self setPlansNewMethod:responseArr : @"Online"];
            
        }
    }
}

-(void)setPlansNewMethod :(NSArray*)responseArr :(NSString*)strType{
    
    if (responseArr.count==0)
    {
        
        [self noInternetThenPSP];
        
    }
    else
    {
        
        if ([strType isEqualToString:@"Online"]) {
            
            // Deleting already existing plans from DB
            
            [self deletePspPlansFromDB];
            
            // Saving Plans Into DB
            
            entityPspPlans=[NSEntityDescription entityForName:@"PspPlans" inManagedObjectContext:context];
            
            PspPlans *objPspPlans = [[PspPlans alloc]initWithEntity:entityPspPlans insertIntoManagedObjectContext:context];
            
            objPspPlans.companyKey=strCompanyKeyy;
            objPspPlans.accountNo=[NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"accountNo"]];
            objPspPlans.arrOfPlans=responseArr;
            objPspPlans.empId=strEmpID;
            objPspPlans.userName=strUserName;
            
            NSError *error2;
            [context save:&error2];
            
        }
        
        NSDictionary *dict;
        arrPlanStatusResponse=responseArr;
        if(responseArr.count==1)
        {
            dict=[responseArr objectAtIndex:0];
            
            _lblNewMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPTitle"]];
            [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
            
            _lblNoMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPTitle"]];
            [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
            
            NSString *strPurchaseStepOrder;
            strPurchaseStepOrder=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PurchaseStepOrder"]];
            
            strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            //  strAccountPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanCharges.text]];
            strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanSavings.text]];
            
            
            strPspMasterId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            strAccountPSPId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            // strPSPCharge1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPCharges"]];
            strPSPDiscountPercent1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
            
            double memberShipDiscout,memberShipCharge,value;
            memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
            memberShipDiscout=[strPSPDiscountPercent1 doubleValue];
            
            value=memberShipCharge*(memberShipDiscout/100);
            strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
            _lblValueForMembershipPlanSavings.text=[NSString stringWithFormat:@"$%.02f",value];
            
            if (strAccountPSPId1.length==0) {
                
                //   _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                
            }else{
                
                //   _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",@"00.00"];
                
            }
            if ([strAccountPSPId1 isEqualToString:@"0"]) {
                
                //  _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                
            }
            
            [self fetchMemberShipPlans:strPurchaseStepOrder];
            
            if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
                
                _txtAmountCheckView.text=@"";
                _txtAmountSingleAmount.text=@"";
                
            } else {
                
                _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
                _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDueNewPlan.text];
                
            }
            
            isOldPlan=NO;
            
            _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
            _viewOld.layer.borderWidth=2.0;
            _viewOld.layer.cornerRadius=5.0;
            
            _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
            _viewNew.layer.borderWidth=2.0;
            _viewNew.layer.cornerRadius=5.0;
            
            _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
            _viewNew.backgroundColor=[UIColor whiteColor];
            
            
            [_lblNewMembershipPlan setHidden:YES];
            [_btnNewPlan setHidden:YES];
            [_btnSelectPlan setHidden:YES];
            [_viewNew setHidden:YES];
            
        }
        else if(responseArr.count==2)
        {
            
            [_lblNewMembershipPlan setHidden:NO];
            [_btnNewPlan setHidden:NO];
            [_btnSelectPlan setHidden:NO];
            [_viewNew setHidden:NO];
            
            dict=[responseArr objectAtIndex:0];
            
            _lblNoMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPTitle"]];
            [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
            //   strAccountPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            
            strPspMasterId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            strAccountPSPId1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            strPSPCharge1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPCharges"]];
            
            NSString *strPSP = [NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"pSPMasterId"]];
            
            if (strPSP.length>0) {
                
                
                
            }else{
                
//                strPSPCharge1 = @"";
//                _lblValueForMembershipPlanCharges.text = @"$00.00";
                
            }
            
            strPSPDiscountPercent1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
            
            double memberShipDiscout,memberShipCharge,value;
            memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
            memberShipDiscout=[strPSPDiscountPercent1 doubleValue];
            
            value=memberShipCharge*(memberShipDiscout/100);
            strPSPDiscountAmount1=[NSString stringWithFormat:@"%.02f",value];
            _lblValueForMembershipPlanSavings.text=[NSString stringWithFormat:@"$%.02f",value];
            if (strAccountPSPId1.length==0) {
                
                // _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                
            }else{
                
                // _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",@"00.00"];
                
            }
            
            if ([strAccountPSPId1 isEqualToString:@"0"]) {
                
                // _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
                
            }
            
            // For Second IndexPath
            
            dict=[responseArr objectAtIndex:1];
            
            _lblNewMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPTitle"]];
            [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
            
            NSString *strPurchaseStepOrder;
            strPurchaseStepOrder=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PurchaseStepOrder"]];
            
            strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanCharges.text]];
            strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanSavings.text]];
            
            [_btnSelectPlan setTitle:[dict valueForKey:@"PSPTitle"] forState:UIControlStateNormal];
            
            strPspMasterId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
            strAccountPSPId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"AccountPSPId"]];
            strPSPCharge2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPCharges"]];
            
            strPSPDiscountPercent2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
            
            double memberShipDiscout2,memberShipCharge2,value2;
            memberShipCharge2=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
            memberShipDiscout2=[strPSPDiscountPercent2 doubleValue];
            
            value2=memberShipCharge2*(memberShipDiscout2/100);
            strPSPDiscountAmount2=[NSString stringWithFormat:@"%.02f",value2];
            _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value2];
            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"PSPCharges"]];
            double tempValueTemporary=[[dict valueForKey:@"PSPCharges"] doubleValue];
            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary];
            
            if (strPSP.length>0) {
                
                
                
            }else{
                
                //strPSPCharge2 = @"";
                //_lblValueForMembershipChargeNewPaln.text = @"$00.00";
                
            }
            
            [self fetchMemberShipPlans:strPurchaseStepOrder];
            
            _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
            _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
            _viewOld.layer.borderWidth=2.0;
            _viewOld.layer.cornerRadius=5.0;
            
            _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
            _viewNew.backgroundColor=[UIColor whiteColor];
            _viewNew.layer.borderWidth=2.0;
            _viewNew.layer.cornerRadius=5.0;
            
            if ([[self getNumberStringNew:_lblValueForAmountDue.text] intValue]<0) {
                
                _txtAmountCheckView.text=@"";
                _txtAmountSingleAmount.text=@"";
                
            } else {
                
                _txtAmountCheckView.text=[self getNumberStringNew:_lblValueForAmountDue.text];
                _txtAmountSingleAmount.text=[self getNumberStringNew:_lblValueForAmountDue.text];
                
            }
            
            //NSString *strPSP = [NSString stringWithFormat:@"%@",[matchesFinalSubWorkorder valueForKey:@"pSPMasterId"]];
            
            if (strPSP.length>0) {
                
                // Change for showing selection matchesFinalSubWorkorder
                
                if ([[matchesFinalSubWorkorder valueForKey:@"pSPMasterId"] isEqualToString:strPspMasterId2]) {
                    
                    isFirstPlan=NO;
                    isFirstPlanSelected=NO;
                    _viewNew.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
                    _viewNew.layer.borderWidth=2.0;
                    _viewNew.layer.cornerRadius=5.0;
                    
                    _viewOld.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                    _viewOld.layer.borderWidth=2.0;
                    _viewOld.layer.cornerRadius=5.0;
                    
                    _viewNew.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
                    _viewOld.backgroundColor=[UIColor whiteColor];
                    
                    chkPlanClick=YES;
                    
                    [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
                    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
                    
                    strPSPCharge2 = @"";
                    _lblValueForMembershipChargeNewPaln.text = @"$00.00";
                    
                } else if ([[matchesFinalSubWorkorder valueForKey:@"pSPMasterId"] isEqualToString:strPspMasterId1]){
                    
                    isFirstPlanSelected=YES;
                    isFirstPlan=YES;
                    _viewOld.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
                    _viewOld.layer.borderWidth=2.0;
                    _viewOld.layer.cornerRadius=5.0;
                    
                    _viewNew.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                    _viewNew.layer.borderWidth=2.0;
                    _viewNew.layer.cornerRadius=5.0;
                    
                    _viewOld.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
                    _viewNew.backgroundColor=[UIColor whiteColor];
                    
                    chkPlanClick=YES;
                    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
                    [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
                    
                    strPSPCharge1 = @"";
                    _lblValueForMembershipPlanCharges.text = @"$00.00";
                    
                }else{
                    
                    BOOL isFound;
                    
                    isFound = NO;
                    
                    for (int k=0; k<arrPSPMasters.count; k++) {
                        
                        NSDictionary *dictMasters = arrPSPMasters[k];
                        
                        NSString *strPspMasterId = [NSString stringWithFormat:@"%@",[dictMasters valueForKey:@"PSPMasterId"]];
                        
                        if ([strPspMasterId isEqualToString:[matchesFinalSubWorkorder valueForKey:@"pSPMasterId"]]) {
                            
                            dict=dictMasters;
                            
                            NSMutableArray *arrTempValuesMasters = [[NSMutableArray alloc]init];
                            
                            [arrTempValuesMasters addObjectsFromArray:arrPlanStatusResponse];
                            
                            [arrTempValuesMasters replaceObjectAtIndex:1 withObject:dict];
                            
                            arrPlanStatusResponse = arrTempValuesMasters;
                            
                            _lblNewMembershipPlan.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
                            [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
                            
                            strPspMasterId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
                            strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanCharges.text]];
                            strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanSavings.text]];
                            
                            [_btnSelectPlan setTitle:[dict valueForKey:@"Title"] forState:UIControlStateNormal];
                            
                            strPspMasterId2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
                            strAccountPSPId2=[NSString stringWithFormat:@"%@",@""];
                            strPSPCharge2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitCharges"]];
                            strPSPDiscountPercent2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Discount"]];
                            
                            double memberShipDiscout2,memberShipCharge2,value2;
                            memberShipCharge2=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
                            memberShipDiscout2=[strPSPDiscountPercent2 doubleValue];
                            
                            value2=memberShipCharge2*(memberShipDiscout2/100);
                            strPSPDiscountAmount2=[NSString stringWithFormat:@"%.02f",value2];
                            _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value2];
                            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
                            double tempValueTemporary=[[dict valueForKey:@"UnitCharges"] doubleValue];
                            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%.02f",tempValueTemporary];
                            
                            strPSPCharge2 = @"";
                            _lblValueForMembershipChargeNewPaln.text = @"$00.00";
                            
                            isFirstPlan=NO;
                            isFirstPlanSelected=NO;
                            _viewNew.layer.borderColor=[[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1] CGColor];
                            _viewNew.layer.borderWidth=2.0;
                            _viewNew.layer.cornerRadius=5.0;
                            
                            _viewOld.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                            _viewOld.layer.borderWidth=2.0;
                            _viewOld.layer.cornerRadius=5.0;
                            
                            _viewNew.backgroundColor=[UIColor colorWithRed:247.0f/255 green:247.0f/255 blue:247.0f/255 alpha:1];
                            _viewOld.backgroundColor=[UIColor whiteColor];
                            
                            chkPlanClick=YES;
                            
                            [_btnNewPlan setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
                            [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_1.png"] forState:UIControlStateNormal];
                            
                            isFound = YES;
                            
                            break ;
                            
                        }
                        
                        
                    }
                    
                    if (!isFound) {
                        
                        [self noInternetThenPSP];
                        
                    }
                    
                }
                
            }
            
            isOldPlan=YES;
            
        }
        
        [self methodCalculationOverAll];
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
        
    }
    [DejalActivityView removeView];
    [DejalBezelActivityView removeView];
    
    [self calculateDiscountPrice];
    [_tableViewCreditPaymntDetailRight reloadData];
    [_tableViewCreditPaymntDetailLeft reloadData];
    [self callToChangeCalculationsOverAll];
    
}


-(void)removeDejalActivity{
    
    NSLog(@"Called Remove Dejal");
    [DejalActivityView removeView];
    [DejalBezelActivityView removeView];
    
}

-(void)noInternetThenPSP{
    
    [_viewNew setHidden:YES];
    [_btnNewPlan setHidden:YES];
    [_lblNewMembershipPlan setHidden:YES];
    _lblNoMembershipPlan.text=@"Default Plan";
    [_btnNoMembership setEnabled:NO];
    [_btnNoMembership setImage:[UIImage imageNamed:@"redio_button_2.png"] forState:UIControlStateNormal];
    
    strPspMasterId2=@"";
    strAccountPSPId2=@"";
    strPspMasterId1=@"";
    strAccountPSPId1=@"";
    
}



-(void)fetchForOldNoMembership:(NSString*)strId
{
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    
    
    //strPurchaseStepOrder=@"2";
    arrMechanicalPSPMasters=[[NSMutableArray alloc]init];
    arrPSPMasters=[[NSMutableArray alloc]init];
    arrMechanicalPSPMasters=[dictMechanicalMasters valueForKey:@"PSPMasterExtSerDc"];
    
    //StepOrder
    NSMutableArray *arrTemp;
    arrTemp=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrMechanicalPSPMasters objectAtIndex:i];
        NSString *strType;
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Type"]];
        if([strDepartmentSysName isEqualToString:[dict valueForKey:@"DepartmentSysName"]]&&[strType isEqualToString:@"General"])
        {
            [arrTemp addObject:dict];
            
        }
    }
    for(int i=0;i<arrTemp.count;i++)
    {
        NSDictionary *dict=[arrTemp objectAtIndex:i];
        NSString *strPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
        if ([strPSPId isEqualToString:strId])
        {
            _lblValueForMembershipPlanCharges.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
            
            double memberShipDiscout,memberShipCharge,value;
            memberShipCharge=[self getNumberString:_lblValueTotalApprovedRepairs.text];
            memberShipDiscout=[[dict valueForKey:@"Discount"]doubleValue];
            
            value=memberShipCharge*(memberShipDiscout/100);
            
            _lblValueForMembershipPlanSavings.text=[NSString stringWithFormat:@"$%.02f",value];
            break;
        }
    }
    
    
}
-(void)fetchMemberShipPlans :(NSString*)strPurchaseStepOrder{
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    
    
    //strPurchaseStepOrder=@"2";
    arrMechanicalPSPMasters=[[NSMutableArray alloc]init];
    arrPSPMasters=[[NSMutableArray alloc]init];
    arrMechanicalPSPMasters=[dictMechanicalMasters valueForKey:@"PSPMasterExtSerDc"];
    
    //StepOrder
    NSMutableArray *arrTemp;
    arrTemp=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrMechanicalPSPMasters objectAtIndex:i];
        NSString *strType;
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Type"]];
        if([strDepartmentSysName isEqualToString:[dict valueForKey:@"DepartmentSysName"]]&&[strType isEqualToString:@"General"])
        {
            [arrTemp addObject:dict];
            
        }
    }
    
    
    //    for(int i=[strPurchaseStepOrder intValue]-1;i<arrTemp.count;i++)
    //    {
    //        NSDictionary *dict=[arrTemp objectAtIndex:i];
    //        [arrPSPMasters addObject:dict];
    //    }
    
    for(int i=0;i<arrTemp.count;i++)
    {
        NSDictionary *dict=[arrTemp objectAtIndex:i];
        
        NSString *strStepOrder=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StepOrder"]];
        
        int stepOrderInMaster=[strStepOrder intValue];
        
        int strPurchaseStepOrderPresent=[strPurchaseStepOrder intValue];
        
        if (stepOrderInMaster>=strPurchaseStepOrderPresent) {
            
            [arrPSPMasters addObject:dict];
            
        }
    }
    
}
-(void)fetchPSPMasters:(NSString*)strPurchaseStepOrder
{
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    NSLog(@"MasterAllMechanical>>%@",dictMechanicalMasters);
    
    
    //strPurchaseStepOrder=@"2";
    arrMechanicalPSPMasters=[[NSMutableArray alloc]init];
    arrPSPMasters=[[NSMutableArray alloc]init];
    arrMechanicalPSPMasters=[dictMechanicalMasters valueForKey:@"PSPMasterExtSerDc"];
    
    //StepOrder
    NSMutableArray *arrTemp;
    arrTemp=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrMechanicalPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrMechanicalPSPMasters objectAtIndex:i];
        NSString *strType;
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Type"]];
        if([strDepartmentSysName isEqualToString:[dict valueForKey:@"DepartmentSysName"]]&&[strType isEqualToString:@"General"])
        {
            [arrTemp addObject:dict];
            
        }
    }
    
    
    //    for(int i=[strPurchaseStepOrder intValue]-1;i<arrTemp.count;i++)
    //    {
    //        NSDictionary *dict=[arrTemp objectAtIndex:i];
    //        [arrPSPMasters addObject:dict];
    //    }
    
    for(int i=0;i<arrTemp.count;i++)
    {
        NSDictionary *dict=[arrTemp objectAtIndex:i];
        
        NSString *strStepOrder=[NSString stringWithFormat:@"%@",[dict valueForKey:@"StepOrder"]];
        
        int stepOrderInMaster=[strStepOrder intValue];
        
        int strPurchaseStepOrderPresent=[strPurchaseStepOrder intValue];
        
        if (stepOrderInMaster>=strPurchaseStepOrderPresent) {
            
            [arrPSPMasters addObject:dict];
            
        }
    }
    
    
    for(int i=0;i<arrPSPMasters.count;i++)
    {
        NSDictionary *dict=[arrPSPMasters objectAtIndex:i];
        NSString *strPSPId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"PSPMasterId"]];
        
        if ([strPSPId isEqualToString:strPspMasterId])
        {
            _lblValueForMembershipChargeNewPaln.text=[NSString stringWithFormat:@"$%@",[dict valueForKey:@"UnitCharges"]];
            
            double memberShipDiscout,memberShipCharge,value;
            memberShipCharge=[self getNumberString:_lblValueForTotalAmountNewPlan.text];
            memberShipDiscout=[[dict valueForKey:@"Discount"]doubleValue];
            
            value=memberShipCharge*(memberShipDiscout/100);
            
            _lblValueForMembershipSavingNewPlan.text=[NSString stringWithFormat:@"$%.02f",value];
            
            break;
        }
    }
}
-(void)finalSubmit
{
    
    if([_btnNewPlan.currentImage isEqual:[UIImage imageNamed:@"redio_button_2.png"]])
    {
        strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanCharges.text]];
        strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanSavings.text]];
        NSLog(@"New Plan Click Found");
    }
    if([_btnNoMembership.currentImage isEqual:[UIImage imageNamed:@"redio_button_2.png"]])
    {
        strPSPCharge=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanCharges.text]];
        strPSPDiscount=[NSString stringWithFormat:@".02%f",[self getNumberString:_lblValueForMembershipPlanSavings.text]];
        NSLog(@"Old Plan Click Found");
    }
}


-(void)fetchALLSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairLabour = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairLabour count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairLabour.count; k++) {
            
            matchesSubWorkOrderIssuesRepairLabour=arrAllObjSubWorkOrderIssuesRepairLabour[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairLabour valueForKey:@"workorderId"]);
            [arrOfSubWorkServiceIssuesRepairLabour addObject:matchesSubWorkOrderIssuesRepairLabour];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchALlSubWorkOrderIssuesRepairPartsFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    //issueRepairPartId
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    NSPredicate *predicate;
    
    if ([strWoType isEqualToString:@"TM"]) {
        
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
        
    } else {
        
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
        
    }
    
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairParts=nil;
        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            NSLog(@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"workorderId"]);
            [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)methodFirstTimeViewLoadedToSetValuesSelectedGBB{
    
    dictAmountTotalRepairs=[[NSMutableDictionary alloc]init];
    dictOfAllAmountSeparately=[[NSMutableDictionary alloc]init];
    
    for (int k1=0; k1<arrOfSubWorkServiceIssues.count; k1++) {
        
        NSManagedObject *dictIssuesData=arrOfSubWorkServiceIssues[k1];
        
        NSString *strIssueIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesData valueForKey:@"subWorkOrderIssueId"]];
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
        
        NSMutableArray *arrOfRepairGBB=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfSubWorkServiceIssuesRepair.count; k++)
        {
            
            NSManagedObject *dictIssuesRepairData=arrOfSubWorkServiceIssuesRepair[k];
            
            NSString *strIssueRepairIdToCheck=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"subWorkOrderIssueId"]];
            
            NSString *strCustomerFeedback=[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"customerFeedback"]];
            
            if ([strIssueIdToCheck isEqualToString:strIssueRepairIdToCheck]) {
                
                [arrTemp addObject:dictIssuesRepairData];
                [arrOfRepairGBB addObject:[NSString stringWithFormat:@"%@",[dictIssuesRepairData valueForKey:@"repairLaborId"]]];
                
                //  if ([strCustomerFeedback isEqualToString:@"true"] || [strCustomerFeedback isEqualToString:@"1"]) {
                
                NSString *strKey=[NSString stringWithFormat:@"%d",k*1000+k1];
                
                [dictAmountTotalRepairs setObject:@"" forKey:strKey];
                [dictOfAllAmountSeparately setObject:@"" forKey:strKey];
                
                // }
                
            }
            
        }
        
        [arrOfIndexSelectedGBB addObject:arrOfRepairGBB];
        
    }
}

#pragma mark-  -------------------- Delete Repair for Issues -------------------

-(void)deleteRepairFromDB :(NSString *)strWoId :(NSString *)strSubWoId :(NSString *)strSubWoIssuesId :(NSString *)strRepairId :(NSString *)strRepairMasterId :(NSString *)strRepairLaborId :(NSString*)strCustomerFeedback{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOIssueRepairDcs Detail Data
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context]];
    //subWorkOrderIssueId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWoId,strSubWoId,strRepairId,strSubWoIssuesId];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self deletePartFromDB:strWoId :strSubWoId :strSubWoIssuesId :strRepairId :strRepairMasterId :strRepairLaborId :strCustomerFeedback];
    
}

-(void)deletePartFromDB :(NSString *)strWoId :(NSString *)strSubWoId :(NSString *)strSubWoIssuesId :(NSString *)strRepairId :(NSString *)strRepairMasterId :(NSString *)strRepairLaborId :(NSString*)strCustomerFeedback{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOTechHelperDcs Detail Data
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWoId,strSubWoId,strRepairId,strSubWoIssuesId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    [self deletelaborFromDb:strWoId :strSubWoId :strSubWoIssuesId :strRepairId :strRepairMasterId :strRepairLaborId :strCustomerFeedback];
    
}

-(void)deletelaborFromDb :(NSString *)strWoId :(NSString *)strSubWoId :(NSString *)strSubWoIssuesId :(NSString *)strRepairId :(NSString *)strRepairMasterId :(NSString *)strRepairLaborId :(NSString*)strCustomerFeedback{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete MechanicalSubWOTechHelperDcs Detail Data
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && subWorkOrderIssueId = %@",strWoId,strSubWoId,strRepairId,strSubWoIssuesId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    
    [self addServiceIssuesRepairFromMobileToDB:strRepairLaborId :strSubWoIssuesId :strSubWoId :strRepairMasterId :strRepairLaborId :strCustomerFeedback];
    
}

-(void)methodToCheckSelectedGbbOnFinalSubmit{
    
    for (int k=0; k<arrOfReapairMasterIdInDb.count; k++) {
        
        NSString *strBothIds=arrOfReapairMasterIdInDb[k];
        
        NSArray *strings = [strBothIds componentsSeparatedByString:@","];
        
        NSString *strReapairIdSelected=strings[0];
        
        NSString *strReapairLaborIdSelected=strings[1];
        
        NSManagedObject *dictRepairData=arrOfSubWorkServiceIssuesRepair[k];
        
        NSString *strReapirIdPresentInDb=[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"repairMasterId"]];
        
        NSString *strReapirlaborIdPresentInDb=[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"repairLaborId"]];
        
        if ([strReapairIdSelected isEqualToString:strReapirIdPresentInDb] && [strReapairLaborIdSelected isEqualToString:strReapirlaborIdPresentInDb]) {
            
            //Yaha pe abhi filhal kch ni krna bad mai edit krna hai
            
        } else {
            
            // Repair ko delete kr k part n labor ko bhi
            // Wapis save bhi krna hai naya wala
            
            [self deleteRepairFromDB:[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"subWorkOrderId"]] :[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"subWorkOrderIssueId"]] :[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"issueRepairId"]] :strReapairIdSelected :strReapairLaborIdSelected :[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"customerFeedback"]]];
            
            
        }
        
    }
    
}

-(void)addServiceIssuesRepairFromMobileToDB :(NSString*)strRepairOptionIdGlobal :(NSString*)strIssueIdToSet :(NSString*)strSubWoId :(NSString*)strRepairMasterIdToSet :(NSString*)strRepairLaborMasterIdToSet :(NSString*)strCustomerFeedback{
    
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepair insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    
    NSString *strIssueRepairTdLocal=[global getReferenceNumber];
    objSubWorkOrderIssues.issueRepairId=strIssueRepairTdLocal;
    objSubWorkOrderIssues.repairLaborId=strRepairOptionIdGlobal;
    objSubWorkOrderIssues.subWorkOrderIssueId=strIssueIdToSet;
    objSubWorkOrderIssues.subWorkOrderId=strSubWoId;
    objSubWorkOrderIssues.repairMasterId=strRepairMasterIdToSet;
    
    
    //Fetch Master Repairs to save
    
    //arrOfRepairMasters
    
    NSDictionary *dictRepairData;
    NSArray *arrOfGbbFromMasters;
    for (int k=0; k<arrOfRepairMasters.count; k++) {
        
        dictRepairData=arrOfRepairMasters[k];
        
        NSString *strRepairMasterId=[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"RepairMasterId"]];
        
        if ([strRepairMasterId isEqualToString:strRepairMasterIdToSet]) {
            
            arrOfGbbFromMasters=[dictRepairData valueForKey:@"RepairLaborMasterDcs"];
            
            break;
        }
    }
    
    objSubWorkOrderIssues.repairName=[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"RepairName"]];
    objSubWorkOrderIssues.repairDesc=[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"RepairDesc"]];
    objSubWorkOrderIssues.costAdjustment=[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"CostAdjustment"]];
    objSubWorkOrderIssues.partCostAdjustment=[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"PartCostAdjustment"]];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.customerFeedback=[NSString stringWithFormat:@"%@",strCustomerFeedback];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    //objSubWorkOrderIssues.repairAmt=_txtRepairAmt.text;
    NSError *error2;
    [context save:&error2];
    
    NSDictionary *dictDataRepairMaster;
    
    for (int k1=0; k1<arrOfGbbFromMasters.count; k1++) {
        
        NSDictionary *dictReapairGbbOption=arrOfGbbFromMasters[k1];
        
        NSString *strLaborIdToCheck=[NSString stringWithFormat:@"%@",[dictReapairGbbOption valueForKey:@"RepairLaborMasterId"]];
        
        if ([strLaborIdToCheck isEqualToString:strRepairLaborMasterIdToSet]) {
            
            dictDataRepairMaster=dictReapairGbbOption;
            
            break;
            
        }
        
    }
    
    //Labour Price Logic
    // sdfsdf
    float hrsLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"LaborHours"]]];
    
    hrsLR=hrsLR/3600;
    
    float strStdPrice=0.0;
    
    
    NSDictionary *dictDataAfterHourRateToBeUsed=[[NSDictionary alloc]init];
    
    if (!(arrOfHoursConfig.count==0)) {
        
        NSDictionary *dictDataHours=arrOfHoursConfig[0];
        
        strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
        
        if (strAfterHrsDuration.length==0) {
            
            
            
        } else {
            
            NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
            
            NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
            NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
            
            strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
            strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
            
            
            NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
            
            for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                
                NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                
                NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                
                if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                    
                    dictDataAfterHourRateToBeUsed=dictDataAHC;
                    break;
                    
                }
                
            }
            
        }
        
        if (!isStandardSubWorkOrder) {
            
            if (isHoliday) {
                
                strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                
            } else {
                
                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborPrice"] floatValue];
                
            }
            
        }
        
    }
    
    float totalPriceLabour=hrsLR*strStdPrice;
    
    //Helper Price Logic
    
    float hrsHLR=[global ChangeTimeMechanical:[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"HelperHours"]]];
    
    hrsHLR=hrsHLR/3600;
    
    float strStdPriceHLR=0.0;
    
    if (!(arrOfHoursConfig.count==0)) {
        
        NSDictionary *dictDataHours=arrOfHoursConfig[0];
        
        strStdPriceHLR=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
        
        if (!isStandardSubWorkOrder) {
            
            if (isHoliday) {
                
                strStdPriceHLR=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];
                
            } else {
                
                strStdPriceHLR=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                
            }
            
        }
        
    }
    
    float totalPriceHelper=hrsHLR*strStdPriceHLR;
    
    //Adding Cost Adjustment To Price Repair
    
    float costAdjustMentToAdd=[[NSString stringWithFormat:@"%@",[dictRepairData valueForKey:@"CostAdjustment"]] floatValue];
    
    //Final Price Logic
    
    float finalPriceLRHLR=totalPriceLabour+totalPriceHelper+costAdjustMentToAdd;
    
    //Parts Logic From Master
    
    float totalLookUpPrice=0;
    
    NSArray *ArrOfPartsMaster=[dictDataRepairMaster valueForKey:@"RepairPartMasterExtSerDc"];
    NSString *strMultiplier;
    strMultiplier=@"1";
    for (int k=0; k<ArrOfPartsMaster.count; k++) {
        
        NSDictionary *dictData=ArrOfPartsMaster[k];
        
        NSString *strPartCode=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PartCode"]];
        
        float valueLookUpPrice=[self fetchPartDetailViaPartCode:strPartCode];
        
        float QTY=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Qty"]] floatValue];
        
        //  float multiplier=1;
        
        NSString *strCategorySysNameLocal=[self fetchPartCategorySysNameDetailViaPartCode:strPartCode];
        
        strMultiplier = [self logicForFetchingMultiplier:[NSString stringWithFormat:@"%f",valueLookUpPrice] :strCategorySysNameLocal :@"Standard"];
        
        float multiplier=[strMultiplier floatValue];
        
        valueLookUpPrice=valueLookUpPrice*QTY*multiplier;
        
        totalLookUpPrice=totalLookUpPrice+valueLookUpPrice;
        
    }
    
    //Final Price Logic after part and repair adding
    
    float finalOverAllPrice=totalLookUpPrice+finalPriceLRHLR;
    
    objSubWorkOrderIssues.repairAmt=[NSString stringWithFormat:@"%.02f",finalOverAllPrice];
    
    
    NSArray *arrOfPartsMaster=[dictDataRepairMaster valueForKey:@"RepairPartMasterExtSerDc"];
    
    NSDictionary *dictLaborInfoToSave = @{@"LaborHours":[NSString stringWithFormat:@"%@",[dictDataRepairMaster             valueForKey:@"LaborHours"]],
                                          @"HelperHours":[NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"HelperHours"]],
                                          @"totalPriceLabour":[NSString stringWithFormat:@"%f",totalPriceLabour],
                                          @"totalPriceHelper": [NSString stringWithFormat:@"%f",totalPriceHelper],
                                          @"DescRepair": [NSString stringWithFormat:@"%@",[dictDataRepairMaster valueForKey:@"RepairDesc"]],
                                          };
    
    for (int k=0; k<arrOfPartsMaster.count; k++) {
        
        NSDictionary *dictDataPartss=arrOfPartsMaster[k];
        
        NSDictionary *dictDataPartssMaster=[self fetchPartDetailDictionaryViaPartCode:[NSString stringWithFormat:@"%@",[dictDataPartss valueForKey:@"PartCode"]]];
        
        [self addServiceIssuesRepairPartsFromMobileToDB:dictDataPartss :dictDataPartssMaster :strIssueRepairTdLocal :strIssueIdToSet :strMultiplier];
        
    }
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"H" :[dictLaborInfoToSave valueForKey:@"totalPriceHelper"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"HelperHours"] :strIssueRepairTdLocal :strIssueIdToSet];//HelperHours totalPriceHelper
    
    [self addServiceIssuesRepairLabourFromMobileToDB:@"L" :[dictLaborInfoToSave valueForKey:@"totalPriceLabour"] :[dictLaborInfoToSave valueForKey:@"DescRepair"] :[dictLaborInfoToSave valueForKey:@"LaborHours"] :strIssueRepairTdLocal :strIssueIdToSet];
    
    
}

-(void)addServiceIssuesRepairPartsFromMobileToDB :(NSDictionary*)dictPartsData :(NSDictionary*)dictPartsDataFromMasterEquip :(NSString *)strRepairIDD :(NSString *)strSubWoIssuesId :(NSString *)strMultiplierToset{
    
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    
    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssues = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    objSubWorkOrderIssues.subWorkOrderIssueId=strSubWoIssuesId;
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.issueRepairPartId=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"RepairPartMasterId"]];
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",strRepairIDD];
    
    NSString *srtPartType;
    if (isStandardSubWorkOrder) {
        
        srtPartType=@"Standard";
    } else {
        
        srtPartType=@"Non-Standard";
    }
    
    objSubWorkOrderIssues.partType=srtPartType;
    objSubWorkOrderIssues.partCode=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"PartCode"]];
    objSubWorkOrderIssues.partName=[NSString stringWithFormat:@"%@",[dictPartsDataFromMasterEquip valueForKey:@"Name"]];
    objSubWorkOrderIssues.partDesc=[NSString stringWithFormat:@"%@",[dictPartsDataFromMasterEquip valueForKey:@"Description"]];
    objSubWorkOrderIssues.qty=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"Qty"]];
    objSubWorkOrderIssues.actualQty=[NSString stringWithFormat:@"%@",[dictPartsData valueForKey:@"Qty"]];
    objSubWorkOrderIssues.unitPrice=[NSString stringWithFormat:@"%@",[dictPartsDataFromMasterEquip valueForKey:@"LookupPrice"]];
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.serialNumber=@"";
    objSubWorkOrderIssues.modelNumber=@"";
    objSubWorkOrderIssues.manufacturer=@"";
    objSubWorkOrderIssues.installationDate=[NSString stringWithFormat:@"%@",@""];
    objSubWorkOrderIssues.multiplier=[NSString stringWithFormat:@"%@",strMultiplierToset];
    objSubWorkOrderIssues.isCompleted=[NSString stringWithFormat:@"%@",@"false"];
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    objSubWorkOrderIssues.isAddedAfterApproval=[NSString stringWithFormat:@"%@",@"false"];
    NSError *error2;
    [context save:&error2];
    
    //    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    //
    //    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
}

-(void)addServiceIssuesRepairLabourFromMobileToDB :(NSString*)strLaborType :(NSString*)strLaborCostt :(NSString*)strRepairDescriptionn :(NSString*)strLaborHourss :(NSString*)strIssueRepairTdLocal :(NSString *)strSubWoIssuesId{
    
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    
    MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssues = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairLabour insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    if ([strLaborType isEqualToString:@"H"]) {
        
        objSubWorkOrderIssues.issueRepairLaborId=[NSString stringWithFormat:@"%@11",[global getReferenceNumber]];
        
    } else {
        
        objSubWorkOrderIssues.issueRepairLaborId=[NSString stringWithFormat:@"%@22",[global getReferenceNumber]];
        
    }
    objSubWorkOrderIssues.subWorkOrderId=strSubWorkOrderIdGlobal;
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",strIssueRepairTdLocal];
    objSubWorkOrderIssues.subWorkOrderIssueId=strSubWoIssuesId;
    
    NSString *srtLaborCostType;
    if (isStandardSubWorkOrder) {
        
        srtLaborCostType=@"SC";
        
    } else {
        
        srtLaborCostType=@"AHC";
        
    }
    
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",@"false"];
    
    objSubWorkOrderIssues.laborCost=[NSString stringWithFormat:@"%@",strLaborCostt];
    objSubWorkOrderIssues.laborCostType=[NSString stringWithFormat:@"%@",srtLaborCostType];
    objSubWorkOrderIssues.laborDescription=[NSString stringWithFormat:@"%@",strRepairDescriptionn];
    objSubWorkOrderIssues.laborHours=[NSString stringWithFormat:@"%@",strLaborHourss];
    objSubWorkOrderIssues.laborType=[NSString stringWithFormat:@"%@",strLaborType];
    
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    //    [self fetchSubWorkOrderIssuesRepairsFromDataBaseForMechanical];
    //
    //    [self fetchSubWorkOrderIssuesFromDataBaseForMechanical];
    
}

-(float)fetchPartDetailViaPartCode :(NSString*)strPartCode{
    
    NSString *strLookupPrice;
    
    for (int k=0; k<arrOfPartsGlobalMasters.count; k++) {
        
        NSDictionary *dictData=arrOfPartsGlobalMasters[k];
        
        NSString *strPartCodeComing=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        
        if ([strPartCodeComing isEqualToString:strPartCode]) {
            
            strLookupPrice=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"LookupPrice"]];
            
            break;
            
        }
    }
    
    float lookUpPrice=[strLookupPrice floatValue];
    
    return lookUpPrice;
    
}

-(NSDictionary*)fetchPartDetailDictionaryViaPartCode :(NSString*)strPartCode{
    
    NSDictionary *dictDataPartss;
    
    dictDataPartss=[global fetchPartDetailDictionaryViaPartCode:strPartCode :arrOfPartsGlobalMasters];
    
    return dictDataPartss;
    
}

- (IBAction)actionOnMileageChargeNewPlan:(id)sender{
    
    tblData.tag=70;
    [self tableLoad:tblData.tag];
    
    
}
- (IBAction)actionOnMileageCharge:(id)sender{
    
    tblData.tag=60;
    [self tableLoad:tblData.tag];
    
    
}

#pragma mark- Preset Signature Methods

//Preset Signature Changes
-(void)checkForPresetSign :(NSManagedObject*)matchesSubWo{
    
    NSUserDefaults *defsSign=[NSUserDefaults standardUserDefaults];
    
    BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignService"];
    
    NSString *strSignUrl=[defsSign valueForKey:@"ServiceTechSignPath"];
    
    if ((isPreSetSign) && (strSignUrl.length>0)) {
        
        strSignUrl=[strSignUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        NSString *result;
        NSRange equalRange = [strSignUrl rangeOfString:@"Documents" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strSignUrl substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=strSignUrl;
        }
        
        equalRange = [result rangeOfString:@"EmployeeSignatures"];
        if (equalRange.location != NSNotFound) {
            result = [result substringFromIndex:equalRange.location];
        }else{
            result=result;
        }
        
        strTechnicianSign=result;
        
        [self downloadingImagessTechnicianPreSet:strSignUrl];
        
        [_btnInspectorSign setEnabled:NO];
        
        isPreSetSignGlobal=YES;
        
    } else if ((isPreSetSign) && (strSignUrl.length>0)){
        
        isPreSetSignGlobal=YES;
        
        [_btnInspectorSign setEnabled:NO];
        
        strSignUrl=[strSignUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        NSString *result;
        NSRange equalRange = [strSignUrl rangeOfString:@"Documents" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strSignUrl substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=strSignUrl;
        }
        
        equalRange = [result rangeOfString:@"EmployeeSignatures"];
        if (equalRange.location != NSNotFound) {
            result = [result substringFromIndex:equalRange.location];
        }else{
            result=result;
        }
        
        strTechnicianSign=result;
        
        [self downloadingImagessTechnicianIfPreset:strTechnicianSign];
        
    } else if ((!isPreSetSign) || (strSignUrl.length<1)){
        
        isPreSetSignGlobal=NO;
        
        [_btnInspectorSign setEnabled:YES];
        
    } else{
        
        isPreSetSignGlobal=NO;
        [_btnInspectorSign setEnabled:YES];
        
    }
    
}

-(void)downloadingImagessTechnicianPreSet :(NSString*)str{
    
    //    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [strTechnicianSign rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [strTechnicianSign substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=strTechnicianSign;
    }
    if (result.length==0) {
        NSRange equalRange = [strTechnicianSign rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strTechnicianSign substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@",str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :strTechnicianSign];
    } else {
        
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            strNewString=[strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :strTechnicianSign];
                
            });
        });
        
    }
}

-(void)ShowFirstImageTechPreSet :(NSString*)str{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    _imgInspectorSign.image=[self loadImage:result];
    
}

-(void)downloadingImagessTechnicianIfPreset :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation1=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.HrmsServiceModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@//Documents//%@",strServiceUrlMainServiceAutomation1,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :str];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            strNewString=[strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :str];
                
            });
        });
        
    }
}

-(void)ShowFirstImageTech :(NSString*)str{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    _imgInspectorSign.image=[self loadImage:result];
    
}

-(void)downloadingImagessTechnician :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation1=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation1,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :str];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            // NSURL *url = [NSURL URLWithString:strNewString];
            strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :str];
                
            });
        });
        
    }
}


//Goto Emp Sheet

-(void)goToEmpTimeSheet{
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    EmpTimeSheetViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EmpTimeSheetViewController"];
    objSignViewController.strWorkOrderId=strWorkOrderId;
    objSignViewController.strSubWorkOrderIdGlobal=strSubWorkOrderIdGlobal;
    objSignViewController.arrOfHoursConfig=arrOfHoursConfig;
    objSignViewController.strWorkOrderStatus=@"InComplete";
    objSignViewController.strAfterHrsDuration=strAfterHrsDuration;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

- (void)createEmpTimeSheetButton
{
    // create a new button
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake((self.view.bounds.size.width - 150),
                                                                 (self.view.bounds.size.height - 200),
                                                                 100, 100)];
    button.backgroundColor=[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1];
    // 86 181 153
    button.backgroundColor=[UIColor colorWithRed:86/255.0f green:181/255.0f blue:153/255.0f alpha:1];
    button.layer.cornerRadius = button.frame.size.width/2;
    button.clipsToBounds = YES;
    
    
    // add drag listener
    [button addTarget:self action:@selector(wasDragged:withEvent:)
     forControlEvents:UIControlEventTouchDragInside];
    
    // center and size
    button.frame = CGRectMake((self.view.bounds.size.width - 150),
                              (self.view.bounds.size.height - 200),
                              100, 100);
    
    UIButton *buttonInside = [[UIButton alloc]initWithFrame:CGRectMake((button.frame.size.width-50)/2.0,
                                                                       (button.frame.size.height-50)/2.0,
                                                                       50, 50)];
    [buttonInside setImage:[UIImage imageNamed:@"work_time.png"] forState:UIControlStateNormal];
    //buttonInside.layer.cornerRadius = button.frame.size.width/2;
    //buttonInside.clipsToBounds = YES;
    [buttonInside addTarget:self action:@selector(goToEmpTimeSheet) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    [button addSubview:buttonInside];
    
}

- (void)wasDragged:(UIButton *)button withEvent:(UIEvent *)event
{
    // get the touch
    UITouch *touch = [[event touchesForView:button] anyObject];
    
    // get delta
    CGPoint previousLocation = [touch previousLocationInView:button];
    CGPoint location = [touch locationInView:button];
    CGFloat delta_x = location.x - previousLocation.x;
    CGFloat delta_y = location.y - previousLocation.y;
    
    // move button
    button.center = CGPointMake(button.center.x + delta_x,
                                button.center.y + delta_y);
}


// Akshay Start //
- (IBAction)actionOnSelectCredit_Apply:(id)sender {
    
    if (arrayAccountDiscountCredits.count==0) {
        
        [global AlertMethod:Alert :@"No credit available"];
        
    } else {
        
        tblData.tag=80;
        [self tableLoad:tblData.tag];
    }
    
}
- (IBAction)actionOnSelectCredit_Add:(id)sender {
    
    if (arrayMasterCredits.count==0) {
        
        [global AlertMethod:Alert :@"No credit available"];
        
    } else {
        
        tblData.tag=90;
        [self tableLoad:tblData.tag];
    }
}

- (IBAction)actionOnApplyCredit:(id)sender {
    
    
    if([_buttonSelectCredit_Apply.titleLabel.text isEqualToString:@"Select Credit"])
    {
        [global AlertMethod:Alert :@"Please select credit"];
        return;
    }
    isPopUpViewFromMaster = NO;
    [self addPartLaborPopUpView];
    
    
}
- (IBAction)actionOnAddCredit:(id)sender {
    
    if([_buttonSelectCredit_Add.titleLabel.text isEqualToString:@"Select Credit"])
    {
        [global AlertMethod:Alert :@"Please select credit"];
        return;
    }
    isPopUpViewFromMaster = YES;
    
    [self addPartLaborPopUpView];
    
}

-(void)fetchAccountDiscountForMechanical
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAccountDescription=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
    requestAccDescription = [[NSFetchRequest alloc] init];
    [requestAccDescription setEntity:entityAccountDescription];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestAccDescription setPredicate:predicate];
    
    sortDescriptorAccDiscount = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsAccDiscount = [NSArray arrayWithObject:sortDescriptorAccDiscount];
    
    [requestAccDescription setSortDescriptors:sortDescriptorsAccDiscount];
    
    self.fetchedResultsControllerAccountDiscount = [[NSFetchedResultsController alloc] initWithFetchRequest:requestAccDescription managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerAccountDiscount setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerAccountDiscount performFetch:&error];
    NSArray* arrAccountDiscount = [self.fetchedResultsControllerAccountDiscount fetchedObjects];
    
    if ([arrAccountDiscount count] == 0)
    {
        
    }
    else
    {
        for(NSManagedObject *accDiscount in arrAccountDiscount)
        {
            NSArray *keys = [[[accDiscount entity] attributesByName] allKeys];
            NSDictionary *dict = [accDiscount dictionaryWithValuesForKeys:keys];
            [arrayAccountDiscountCredits addObject:dict];
        }
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)fetchWorkOrderAppliedDiscounts
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrderAppliedDiscounts=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    requestWorkOrderAppliedDiscount = [[NSFetchRequest alloc] init];
    [requestWorkOrderAppliedDiscount setEntity:entityWorkOrderAppliedDiscounts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdGlobal];
    
    [requestWorkOrderAppliedDiscount setPredicate:predicate];
    
    sortDescriptorWorkOrderAppliedDiscount = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:YES];
    sortDescriptorsWorkOrderAppliedDiscount = [NSArray arrayWithObject:sortDescriptorWorkOrderAppliedDiscount];
    //
    [requestWorkOrderAppliedDiscount setSortDescriptors:sortDescriptorsWorkOrderAppliedDiscount];
    
    self.fetchedResultsControllerWorkOrderAppliedDiscount = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWorkOrderAppliedDiscount managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderAppliedDiscount setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderAppliedDiscount performFetch:&error];
    NSArray* arrAppliedDiscounts = [self.fetchedResultsControllerWorkOrderAppliedDiscount fetchedObjects];
    
    NSLog(@"%@",arrAppliedDiscounts);
    
    
    if ([arrAppliedDiscounts count] == 0)
    {
        
        _labelCreditsPaymentDetailViewRight.text = @"$00.00";
        _labelCreditsPaymentDetailViewLeft.text = @"$00.00";
        viewCreditHeight = 145;
        heightPaymntDetailView = 1040+80; // (1120-80(tv height))
        _heightPaymentViewLeft.constant = 940+80;
        _heightPaymentViewRight.constant = 940+80;
        _heightTblViewPaymntDetailLeft.constant = 0.0;
        _heightTblViewPaymntDetailRight.constant = 0.0;
        isUpdateViewsFrame = YES;
        [self addView];
        [_tableViewCredit reloadData];
        
        [self calculateDiscountPrice];
        [_tableViewCreditPaymntDetailRight reloadData];
        [_tableViewCreditPaymntDetailLeft reloadData];
        [self callToChangeCalculationsOverAll];
        
        
    }
    else
    {
        for(NSManagedObject *appliedDiscount in arrAppliedDiscounts)
        {
            NSArray *keys = [[[appliedDiscount entity] attributesByName] allKeys];
            NSDictionary *dict = [appliedDiscount dictionaryWithValuesForKeys:keys];
            [arrayAppliedDiscounts addObject:dict];
        }
        
        if(arrayAppliedDiscounts.count>0)
        {
            totalLaborAmountLocal = totalLaborAmountLocalCopy;
            totalPartAmountLocal = totalPartAmountLocalCopy;
            [self calculateDiscountPrice];
        }
        /* _labelSubTotal.text = @"0.00";
         if(arrayAppliedDiscounts.count>0)
         {
         for(NSDictionary *dict in arrayAppliedDiscounts)
         {
         _labelSubTotal.text = [NSString stringWithFormat:@"%.2f",[_labelSubTotal.text floatValue]+[[dict valueForKey:@"discountAmount"] floatValue]];
         }
         viewCreditHeight = 195+(int)arrayAppliedDiscounts.count*250;
         
         heightPaymntDetailView = 970 + ((int)arrayAppliedDiscounts.count*60)+60;// +60 for header view total credits
         _heightPaymentViewLeft.constant = 860 + ((int)arrayAppliedDiscounts.count*60)+60;
         
         _heightTblViewPaymntDetailLeft.constant = ((int)arrayAppliedDiscounts.count*60)+60;
         
         isUpdateViewsFrame = YES;
         [self addView];
         [_tableViewCreditPaymntDetailLeft reloadData];
         [_tableViewCredit reloadData];
         }*/
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)actionOnDeleteCredit:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Are you sure you want to delete credit from account?" delegate:self cancelButtonTitle:@"Delete From Account" otherButtonTitles:@"NO",@"YES", nil];
    
    
    alertView.tag = btn.tag;
    [alertView show];
}

- (IBAction)actionOnLaborPrice:(id)sender {
    
    if(!isLaborPrice)
    {
        isLaborPrice = YES;
        [_buttonLaborPrice setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonPartPrice setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
}
- (IBAction)actionOnPartPrice:(id)sender {
    
    if(isLaborPrice)
    {
        isLaborPrice = NO;
        [_buttonPartPrice setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonLaborPrice setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
}
- (IBAction)actionOnApplyPartLabor:(id)sender {
    
    if(isPopUpViewFromMaster)
    {// this action triggered from master credits
        
        // create discount object from master
        NSDictionary *dictTemp = @{@"companyKey":[NSString stringWithFormat:@"%@",strCompanyKeyy],
                                   @"userName":[NSString stringWithFormat:@"%@",strUserName],
                                   @"workorderId":[NSString stringWithFormat:@"%@",strWorkOrderId],
                                   @"accountNo":[NSString stringWithFormat:@"%@",strAccountNo],
                                   @"discountAmount":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"DiscountAmount"]],
                                   @"discountSetupSysName":@"",// as dicussed leave blank
                                   @"discountSetupName":@"",// as dicussed leave blank
                                   @"discountCode":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"DiscountCode"]],
                                   @"isDiscountPercent":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"IsDiscountPercent"]],
                                   @"discountPercent":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"DiscountPercent"]],
                                   @"sysName":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"SysName"]],
                                   @"name":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"Name"]],
                                   @"isActive":@"true",
                                   @"isDelete":@"false",
                                   @"discountDescription":[NSString stringWithFormat:@"%@",[dictAddDiscount valueForKey:@"Description"]],
                                   @"departmentSysName":[NSString stringWithFormat:@"%@",strDepartmentSysName],
                                   @"createdByDevice":@"Mobile"
                                   };
        
        [self saveAccountDiscount:dictTemp];// save to account discount table
        [arrayAccountDiscountCredits removeAllObjects];
        [self fetchAccountDiscountForMechanical];
        
        // remove added credit(in account discount) from master list
        for(NSDictionary *dict in [arrayMasterCredits copy])
        {
            if([[dict valueForKey:@"SysName"] isEqualToString:[dictAddDiscount valueForKey:@"SysName"]])
            {
                [arrayMasterCredits removeObject:dict];
                [_buttonSelectCredit_Add setTitle:@"Select Credit" forState:UIControlStateNormal];
            }
        }
        
        // create applied discount from account discount
        
        for(NSDictionary *dict in arrayAccountDiscountCredits)
        {
            if([[dict valueForKey:@"sysName"] isEqualToString:[dictAddDiscount valueForKey:@"SysName"]])
            {
                [self methodToApplyDiscount:dict];
                break;
            }
        }
        
        [self callToChangeCalculationsOverAll];
        
    }
    else
    { // this action triggered from account discount
        
        for(NSDictionary *tempDict in arrayAppliedDiscounts)
        {
            if([[dictSelectedDiscount valueForKey:@"sysName"] isEqualToString:[tempDict valueForKey:@"discountSysName"]])
            {
                [global AlertMethod:@"Alert!" :@"This credit aleady applied"];
                return;
            }
        }
        
        [self methodToApplyDiscount:dictSelectedDiscount];
        [self callToChangeCalculationsOverAll];
    }
    
}

- (IBAction)actionOnCancelPartLabor:(id)sender {
    
    [viewBackGround removeFromSuperview];
    [_viewPartLaborCreditApply removeFromSuperview];
    isLaborPartViewAddedToSuperView = NO;
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"YES"])
    {
        NSDictionary *dictDeletedCredit = [arrayAppliedDiscounts objectAtIndex:alertView.tag];
        
        
        // remove deleted applied discount from Account Discount list
        
        /*
         for(NSDictionary *dictDiscount in arrayAccountDiscountCredits)
         {
         if([[dictDeletedCredit valueForKey:@"discountSysName"]isEqualToString:[dictDiscount valueForKey:@"sysName"]])
         {
         [self deleteAccountDiscountFromLocalDB:dictDiscount];
         break;
         }
         }*/
        
        [self deleteAppliedDiscountFromLocalDB:dictDeletedCredit];
        
        [self getAccDiscountAppliedDiscountAndMasterCredits];
        
        [_buttonSelectCredit_Apply setTitle:@"Select Credit" forState:UIControlStateNormal];
        
        if(isLaborPrice)
        {
            totalLaborAmountLocal = totalLaborAmountLocal+ [[dictDeletedCredit valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        else
        {
            totalPartAmountLocal = totalPartAmountLocal+ [[dictDeletedCredit valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        
        [self callToChangeCalculationsOverAll];
    }
    else if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete From Account"])
    {
        NSDictionary *dictDeletedCredit = [arrayAppliedDiscounts objectAtIndex:alertView.tag];
        
        
        // remove deleted applied discount from Account Discount list
        
        for(NSDictionary *dictDiscount in arrayAccountDiscountCredits)
        {
            if([[dictDeletedCredit valueForKey:@"discountSysName"]isEqualToString:[dictDiscount valueForKey:@"sysName"]])
            {
                [self deleteAccountDiscountFromLocalDB:dictDiscount];
                break;
            }
        }
        
        [self deleteAppliedDiscountFromLocalDB:dictDeletedCredit];
        
        [self getAccDiscountAppliedDiscountAndMasterCredits];
        
        [_buttonSelectCredit_Apply setTitle:@"Select Credit" forState:UIControlStateNormal];
        
        if(isLaborPrice)
        {
            totalLaborAmountLocal = totalLaborAmountLocal+ [[dictDeletedCredit valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        else
        {
            totalPartAmountLocal = totalPartAmountLocal+ [[dictDeletedCredit valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        
        [self callToChangeCalculationsOverAll];
    }
    else
    {
        
    }
}
-(void)deleteAccountDiscountFromLocalDB:(NSDictionary*)dictDelete
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Account Discount Data
    entityAccountDescription=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"sysName = %@",[dictDelete valueForKey:@"sysName"]];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)deleteAppliedDiscountFromLocalDB:(NSDictionary*)dictDelete
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Account Discount Data
    entityAccountDescription=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"discountSysName = %@",[dictDelete valueForKey:@"discountSysName"]];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

-(void)saveAppliedDiscount:(NSDictionary*)dictAppliedDicount
{
    
    entityWorkOrderAppliedDiscounts=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    
    WorkOrderAppliedDiscountExtSerDcs *objWorkOrderAppliedDiscount = [[WorkOrderAppliedDiscountExtSerDcs alloc]initWithEntity:entityWorkOrderAppliedDiscounts insertIntoManagedObjectContext:context];
    
    
    objWorkOrderAppliedDiscount.companyKey = [NSString stringWithFormat:@"%@",strCompanyKeyy];
    
    objWorkOrderAppliedDiscount.userName = strUserName;
    
    objWorkOrderAppliedDiscount.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"subWorkOrderId"]];
    
    objWorkOrderAppliedDiscount.workOrderId=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"workOrderId"]];
    
    objWorkOrderAppliedDiscount.workOrderAppliedDiscountId=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"workOrderAppliedDiscountId"]];
    
    objWorkOrderAppliedDiscount.appliedDiscountAmt=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"appliedDiscountAmt"]];
    
    
    //objWorkOrderAppliedDiscount.isDiscountPercent=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"isDiscountPercent"]];
    
    if ([[dictAppliedDicount valueForKey:@"isDiscountPercent"] isEqualToString:@"1"] || [[dictAppliedDicount valueForKey:@"isDiscountPercent"] isEqualToString:@"True"] || [[dictAppliedDicount valueForKey:@"isDiscountPercent"] isEqualToString:@"true"])
    {
        objWorkOrderAppliedDiscount.isDiscountPercent=[NSString stringWithFormat:@"%@",@"true"];
        
    }
    else
    {
        objWorkOrderAppliedDiscount.isDiscountPercent=[NSString stringWithFormat:@"%@",@"false"];
    }
    
    objWorkOrderAppliedDiscount.discountSysName=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountSysName"]];
    
    objWorkOrderAppliedDiscount.discountType=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountType"]];
    
    objWorkOrderAppliedDiscount.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"isActive"]]];
    
    objWorkOrderAppliedDiscount.createdBy=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"createdBy"]];
    
    objWorkOrderAppliedDiscount.createdDate=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"createdDate"]];
    
    
    
    objWorkOrderAppliedDiscount.modifiedBy=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"modifiedBy"]];
    
    // objWorkOrderAppliedDiscount.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"modifiedDate"]]];
    
    objWorkOrderAppliedDiscount.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"modifiedDate"]]];
    
    
    //[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"ModifiedDate"]]]
    
    objWorkOrderAppliedDiscount.createdByDevice=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"createdByDevice"]];
    
    objWorkOrderAppliedDiscount.discountCode=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountCode"]];
    
    objWorkOrderAppliedDiscount.discountAmount=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountAmount"]];
    
    objWorkOrderAppliedDiscount.discountDescription=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountDescription"]];
    
    objWorkOrderAppliedDiscount.discountName=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountName"]];
    
    objWorkOrderAppliedDiscount.discountPercent=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"discountPercent"]];
    
    objWorkOrderAppliedDiscount.applyOnPartPrice=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"applyOnPartPrice"]];
    
    objWorkOrderAppliedDiscount.applyOnLaborPrice=[NSString stringWithFormat:@"%@",[dictAppliedDicount valueForKey:@"applyOnLaborPrice"]];
    
    NSError *error4;
    [context save:&error4];
    
}
-(void)saveAccountDiscount:(NSDictionary*)dictAccountDiscount
{
    entityAccountDescription=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
    
    AccountDiscountExtSerDcs *objAccountDiscount = [[AccountDiscountExtSerDcs alloc]initWithEntity:entityAccountDescription insertIntoManagedObjectContext:context];
    
    
    objAccountDiscount.companyKey=strCompanyKeyy;
    objAccountDiscount.userName=strUserName;
    objAccountDiscount.workorderId=[NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"workorderId"]];
    objAccountDiscount.accountNo = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"accountNo"]];
    
    objAccountDiscount.discountAmount = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountAmount"]];
    
    objAccountDiscount.discountSetupSysName = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountSetupSysName"]];
    objAccountDiscount.discountSetupName = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountSetupName"]];
    objAccountDiscount.discountCode = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountCode"]];
    
    objAccountDiscount.isDiscountPercent = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"isDiscountPercent"]];
    objAccountDiscount.discountPercent = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountPercent"]];
    objAccountDiscount.sysName = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"sysName"]];
    
    objAccountDiscount.name = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"name"]];
    
    objAccountDiscount.isActive = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"isActive"]];
    
    objAccountDiscount.isDelete = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"isDelete"]];
    
    objAccountDiscount.discountDescription = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"discountDescription"]];
    
    objAccountDiscount.departmentSysName = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"departmentSysName"]];
    objAccountDiscount.createdByDevice = [NSString stringWithFormat:@"%@",[dictAccountDiscount valueForKey:@"createdByDevice"]];
    
    
    NSError *error22222;
    [context save:&error22222];
}

-(void)getAccDiscountAppliedDiscountAndMasterCredits
{
    [arrayMasterCredits removeAllObjects];
    [arrayCopyOfMasterCredits removeAllObjects];
    [arrayAccountDiscountCredits removeAllObjects];
    [arrayAppliedDiscounts removeAllObjects];
    
    [self fetchAccountDiscountForMechanical];
    [self fetchWorkOrderAppliedDiscounts];
    
    NSDictionary  *dictSalesLeadMaster;
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    dictSalesLeadMaster=[defs2 valueForKey:@"MasterSalesAutomation"];
    arrayMasterCredits = [[dictSalesLeadMaster valueForKey:@"DiscountSetupMasterCredit"] mutableCopy];
    NSLog(@"%@",arrayMasterCredits);
    
    arrayCopyOfMasterCredits = [arrayMasterCredits mutableCopy];
    
    for(NSDictionary *dictDiscount in arrayAccountDiscountCredits)
    {
        for(NSDictionary *dictMaster in [arrayMasterCredits copy])
        {
            if([[dictDiscount valueForKey:@"sysName"] isEqualToString:[dictMaster valueForKey:@"SysName"]])
            {
                [arrayMasterCredits removeObject:dictMaster];
            }
        }
    }
}
-(void)calculateDiscountPrice
{
    NSMutableDictionary *dictAppliedDiscount = [[NSMutableDictionary alloc] init];
    
    for(int i=0;i<arrayAppliedDiscounts.count;i++)
    {
        dictAppliedDiscount = [[arrayAppliedDiscounts objectAtIndex:i] mutableCopy];
        
        if([[dictAppliedDiscount valueForKey:@"applyOnLaborPrice"] isEqualToString:@"true"] || [[dictAppliedDiscount valueForKey:@"applyOnLaborPrice"]boolValue]==YES)
        { // discount apply on labor price
            
            if([[dictAppliedDiscount valueForKey:@"isDiscountPercent"] boolValue]==YES)
            { // if discount percent
                
                float discountPercentage = [[dictAppliedDiscount valueForKey:@"discountPercent"] floatValue];
                float discountAmountFromPercentage = (totalLaborAmountLocalCopy*discountPercentage)/100.0;
                
                if(totalLaborAmountLocal==0) //if labor price becomes 0
                {
                    discountAmountFromPercentage = 0;
                    /* [dictAppliedDiscount setValue:@"0" forKey:@"appliedDiscountAmt"];
                     [self updateAppliedDiscount:dictAppliedDiscount];*/
                    
                    
                    [self deleteAppliedDiscountFromLocalDB:dictAppliedDiscount];
                    [arrayAppliedDiscounts removeObjectAtIndex:i];
                    i = i-1;
                    [self showZeroDiscountAmountAlert];
                    return;
                }
                else if(totalLaborAmountLocal>0 && totalLaborAmountLocal<discountAmountFromPercentage) // if labor price is more than 0 but less than discount amount
                {
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",totalLaborAmountLocal] forKey:@"appliedDiscountAmt"];
                    totalLaborAmountLocal = 0.0;
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
                else
                {
                    totalLaborAmountLocal = totalLaborAmountLocal-discountAmountFromPercentage;
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",discountAmountFromPercentage] forKey:@"appliedDiscountAmt"];
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
            }
            else // discount in amount
            {
                float discountAmount = [[dictAppliedDiscount valueForKey:@"discountAmount"] floatValue];
                if(totalLaborAmountLocal == 0)
                {
                    /*[dictAppliedDiscount setValue:@"0" forKey:@"appliedDiscountAmt"];
                     [self updateAppliedDiscount:dictAppliedDiscount];*/
                    
                    [self deleteAppliedDiscountFromLocalDB:dictAppliedDiscount];
                    [arrayAppliedDiscounts removeObjectAtIndex:i];
                    i = i-1;
                    [self showZeroDiscountAmountAlert];
                    return;
                }
                else if(totalLaborAmountLocal>0 && totalLaborAmountLocal<discountAmount)
                {
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",totalLaborAmountLocal] forKey:@"appliedDiscountAmt"];
                    totalLaborAmountLocal = 0.0;
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
                else
                {
                    totalLaborAmountLocal = totalLaborAmountLocal-discountAmount;
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",discountAmount] forKey:@"appliedDiscountAmt"];
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
            }
        }
        else if([[dictAppliedDiscount valueForKey:@"applyOnPartPrice"] isEqualToString:@"true"]||[[dictAppliedDiscount valueForKey:@"applyOnPartPrice"] boolValue]==YES)
        {
            if([[dictAppliedDiscount valueForKey:@"isDiscountPercent"] boolValue]==YES)
            { // if discount percent
                
                float discountPercentage = [[dictAppliedDiscount valueForKey:@"discountPercent"] floatValue];
                float discountAmountFromPercentage = (totalPartAmountLocalCopy*discountPercentage)/100.0;
                
                if(totalPartAmountLocal==0) //if part price becomes 0
                {
                    discountAmountFromPercentage = 0;
                    /*  [dictAppliedDiscount setValue:@"0" forKey:@"appliedDiscountAmt"];
                     [self updateAppliedDiscount:dictAppliedDiscount];*/
                    
                    [self deleteAppliedDiscountFromLocalDB:dictAppliedDiscount];
                    [arrayAppliedDiscounts removeObjectAtIndex:i];
                    i = i-1;
                    [self showZeroDiscountAmountAlert];
                    return;
                }
                else if(totalPartAmountLocal>0 && totalPartAmountLocal<discountAmountFromPercentage) // if part price is more than 0 but less than discount amount
                {
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",totalPartAmountLocal] forKey:@"appliedDiscountAmt"];
                    totalPartAmountLocal = 0.0;
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
                else
                {
                    totalPartAmountLocal = totalPartAmountLocal-discountAmountFromPercentage;
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",discountAmountFromPercentage] forKey:@"appliedDiscountAmt"];
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
            }
            else // discount amount
            {
                float discountAmount = [[dictAppliedDiscount valueForKey:@"discountAmount"] floatValue];
                if(totalPartAmountLocal == 0) // if part sale amount is 0
                {
                    
                    /*[dictAppliedDiscount setValue:@"0" forKey:@"appliedDiscountAmt"];
                     [self updateAppliedDiscount:dictAppliedDiscount];*/
                    
                    
                    [self deleteAppliedDiscountFromLocalDB:dictAppliedDiscount];
                    [arrayAppliedDiscounts removeObjectAtIndex:i];
                    i = i-1;
                    [self showZeroDiscountAmountAlert];
                    return;
                }
                else if(totalPartAmountLocal>0 && totalPartAmountLocal<discountAmount)
                { // part sale amount more than 0 but less than discount amount
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",totalPartAmountLocal] forKey:@"appliedDiscountAmt"];
                    totalPartAmountLocal = 0.0;
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
                else
                {// part sale amount is more than 0 and discount price
                    totalPartAmountLocal = totalPartAmountLocal-discountAmount;
                    [dictAppliedDiscount setValue:[NSString stringWithFormat:@"%.2f",discountAmount] forKey:@"appliedDiscountAmt"];
                    [self updateAppliedDiscount:dictAppliedDiscount];
                }
            }
        }
        
        [arrayAppliedDiscounts replaceObjectAtIndex:i withObject:dictAppliedDiscount];
        
    }
    [_tableViewCredit reloadData];
    [self updateView];
}

-(void)updateAppliedDiscount:(NSDictionary*)dict
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrderAppliedDiscounts=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    requestWorkOrderAppliedDiscount = [[NSFetchRequest alloc] init];
    [requestWorkOrderAppliedDiscount setEntity:entityWorkOrderAppliedDiscounts];
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND subWorkOrderId = %@",[dict valueForKey:@"workOrderId"],[dict valueForKey:@"subWorkOrderId"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"discountSysName = %@",[dict valueForKey:@"discountSysName"]];
    
    [requestWorkOrderAppliedDiscount setPredicate:predicate];
    
    sortDescriptorWorkOrderAppliedDiscount = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrderAppliedDiscount = [NSArray arrayWithObject:sortDescriptorWorkOrderAppliedDiscount];
    
    [requestWorkOrderAppliedDiscount setSortDescriptors:sortDescriptorsWorkOrderAppliedDiscount];
    
    self.fetchedResultsControllerWorkOrderAppliedDiscount = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWorkOrderAppliedDiscount managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderAppliedDiscount setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderAppliedDiscount performFetch:&error];
    NSArray* arrAccountDiscount = [self.fetchedResultsControllerWorkOrderAppliedDiscount fetchedObjects];
    
    if ([arrAccountDiscount count] == 0)
    {
        
    }
    else
    {
        for(NSManagedObject *accDiscount in arrAccountDiscount)
        {
            [accDiscount setValue:[dict valueForKey:@"appliedDiscountAmt"] forKey:@"appliedDiscountAmt"];
            NSLog(@"%@",accDiscount);
            
        }
    }
    
    NSError *error4;
    [context save:&error4];
    
}

-(void)updateView // this method updates payment detail view either left or right view and applied credit view(i.e credit view)
{
    _labelSubTotal.text = @"$0.00";
    float totalCredits = 0.0;
    if(arrayAppliedDiscounts.count>0)
    {
        for(NSDictionary *dict in arrayAppliedDiscounts)
        {
            //            _labelSubTotal.text = [NSString stringWithFormat:@"$%.2f",[_labelSubTotal.text floatValue]+[[dict valueForKey:@"appliedDiscountAmt"] floatValue]];
            totalCredits = totalCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
        }
        _labelSubTotal.text = [NSString stringWithFormat:@"$%.2f",totalCredits];
        
        viewCreditHeight = 195+(int)arrayAppliedDiscounts.count*250;
        
        heightPaymntDetailView = 1040 + ((int)arrayAppliedDiscounts.count*60)+80;
        
        if(isFirstPlanSelected)// left view
        {
            
            _heightPaymentViewLeft.constant = 940 + ((int)arrayAppliedDiscounts.count*60) +80;
            
            _heightTblViewPaymntDetailLeft.constant = ((int)arrayAppliedDiscounts.count*60);
            float totalCredits = 0.0;
            
            for(NSDictionary *dict in arrayAppliedDiscounts)
            {
                totalCredits = totalCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
            _labelCreditsPaymentDetailViewLeft.text = [NSString stringWithFormat:@"$%.2f",totalCredits];
            [_tableViewCreditPaymntDetailLeft reloadData];
            [self callToChangeCalculationsOverAll];
            
        }
        else
        {
            
            
            _heightPaymentViewRight.constant = 940 + ((int)arrayAppliedDiscounts.count*60) +80;
            
            _heightTblViewPaymntDetailRight.constant = ((int)arrayAppliedDiscounts.count*60);
            
            float totalCredits = 0.0;
            for(NSDictionary *dict in arrayAppliedDiscounts)
            {
                totalCredits = totalCredits+[[dict valueForKey:@"appliedDiscountAmt"] floatValue];
            }
            _labelCreditsPaymentDetailViewRight.text = [NSString stringWithFormat:@"$%.2f",totalCredits];
            
            [_tableViewCreditPaymntDetailRight reloadData];
            [self callToChangeCalculationsOverAll];
            
            
        }
        
        if(_heightPaymentViewLeft.constant == _heightPaymentViewRight.constant)
        {
            //leave heightPaymntDetailView as it is
        }
        else if(CGRectGetMaxY(_viewOld.frame)>heightPaymntDetailView || CGRectGetMaxY(_viewForNewPlan.frame)>heightPaymntDetailView)
        {
            CGRect frameRect = _viewForPaymentDetail.frame;
            heightPaymntDetailView = frameRect.size.height;
        }
        
        isUpdateViewsFrame = YES;
        [self addView];
        [_tableViewCredit reloadData];
        
        
//        [self calculateDiscountPrice];
//        [_tableViewCreditPaymntDetailRight reloadData];
//        [_tableViewCreditPaymntDetailLeft reloadData];
//        [self callToChangeCalculationsOverAll];
        
    }
    
}

-(void)addPartLaborPopUpView
{
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor = [UIColor blackColor];
    viewBackGround.alpha = 0.5;
    [self.view addSubview: viewBackGround];
    
    
    CGRect frameFor_viewPopUpPurchaseOrder=CGRectMake(40, 20, [UIScreen mainScreen].bounds.size.width-80,_viewPartLaborCreditApply.frame.size.height);
    
    [_viewPartLaborCreditApply setFrame:frameFor_viewPopUpPurchaseOrder];
    
    _viewPartLaborCreditApply.center = CGPointMake(self.view.frame.size.width  / 2,
                                                   self.view.frame.size.height / 2);
    
    isLaborPrice = YES;
    [_buttonLaborPrice setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_buttonPartPrice setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self.view addSubview:_viewPartLaborCreditApply];
    isLaborPartViewAddedToSuperView = YES;
    
}

-(void)methodToApplyDiscount:(NSDictionary*)dict
{
    NSString *strApplyOnLbrPrice,*strApplyOnPartPrice = @"";
    if(isLaborPrice)
    {
        strApplyOnLbrPrice = @"true";
        strApplyOnPartPrice = @"false";
    }
    else
    {
        strApplyOnLbrPrice = @"false";
        strApplyOnPartPrice = @"true";
    }
    NSDictionary *dictTemp = @{@"appliedDiscountAmt":@"0",
                               @"applyOnLaborPrice":strApplyOnLbrPrice,
                               @"applyOnPartPrice":strApplyOnPartPrice,
                               @"companyKey":[NSString stringWithFormat:@"%@",[dict valueForKey:@"companyKey"]],
                               @"createdBy":[NSString stringWithFormat:@"%@",strUserName],
                               @"createdByDevice":@"Mobile",
                               @"createdDate":[NSString stringWithFormat:@"%@",[global strCurrentDate]],
                               @"discountAmount":[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountAmount"]],
                               @"discountCode":[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountCode"]],
                               @"discountDescription":[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]],
                               @"discountName":[NSString stringWithFormat:@"%@",[dict valueForKey:@"name"]],
                               @"discountPercent":[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountPercent"]],
                               @"discountSysName":[NSString stringWithFormat:@"%@",[dict valueForKey:@"sysName"]],
                               @"discountType":@"Credit",
                               @"isActive":@"true",
                               @"isDiscountPercent":[NSString stringWithFormat:@"%@",[dict valueForKey:@"isDiscountPercent"]],
                               @"modifiedBy":[NSString stringWithFormat:@"%@",strUserName],
                               @"modifiedDate":[NSString stringWithFormat:@"%@",[self getCurrentDateAndTime]],
                               @"subWorkOrderId":[NSString stringWithFormat:@"%@",strSubWorkOrderIdGlobal],
                               @"userName":[NSString stringWithFormat:@"%@",[dict valueForKey:@"userName"]],
                               @"workOrderAppliedDiscountId":@"",
                               @"workOrderId":[NSString stringWithFormat:@"%@",strWorkOrderId]
                               
                               };
    
    [self saveAppliedDiscount:dictTemp];
    
    [arrayAppliedDiscounts removeAllObjects];
    [self fetchWorkOrderAppliedDiscounts];
    
    [viewBackGround removeFromSuperview];
    [_viewPartLaborCreditApply removeFromSuperview];
    isLaborPartViewAddedToSuperView = NO;
    [self callToChangeCalculationsOverAll];
}
-(void)actionOnViewMore_Cedit:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    [global AlertMethod:@"Description" :[[arrayAppliedDiscounts objectAtIndex:btn.tag] valueForKey:@"discountDescription"]];
}

-(NSString*)getCurrentDateAndTime
{
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    return [dateFormatter stringFromDate:[NSDate date]];
}
// Akshay End //


// Call on all change action

-(void)callToChangeCalculationsOverAll{
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
        [self calculateForRepairsNewPlan:_txtOtherDiscountNewPlan.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
        [self  calculateForPartsNewPlan:_txtOtherDiscountNewPlan.text];
    }
}

-(void)showZeroDiscountAmountAlert// ye alert tab dikhega jab discount amount zero hoga
{
    [global displayAlertController:Alert :@"Credit can not be applied" :self];
}
// Akshay End //


-(void)saveTechComments{
    
    NSManagedObject *objWorkOrderdetailsLocalNew=[global fetchMechanicalWorkOrderObj:strWorkOrderId];

    [objWorkOrderdetailsLocalNew setValue:_txtViewAdditionalInfo.text forKey:@"technicianComment"];
    
    NSError *error2;
    [context save:&error2];
    
}

-(void)calculateTripChargeBasedOnQty{
    
    _lblTripChargeBasedOnQty.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTipCharge.text]*[_txtFld_TripChargeQty.text intValue]];
    
    _lblTripChargeBasedOnQtyNewPlan.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTripChargeNewPlan.text]*[_txtFld_TripChargeQtyNewPlan.text intValue]];

    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
    
}

-(void)calculateTripChargeBasedOnQtyOnTextFldEdit :(NSString*)strTripQty{
    
    _lblTripChargeBasedOnQty.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTipCharge.text]*[strTripQty intValue]];
    
    _lblTripChargeBasedOnQtyNewPlan.text = [NSString stringWithFormat:@"$%.02f",[self getNumberString:_lblValueForTripChargeNewPlan.text]*[strTripQty intValue]];
    
    if ([strWoType isEqualToString:@"FR"])
    {
        [self calculateTotal:_txtOtherDiscounts.text];
    }
    else if ([strWoType isEqualToString:@"TM"])
    {
        [self calculateTotalForParts:_txtOtherDiscounts.text];
    }
    
}
@end

