//
//  MechanicalPurchaseOrderStandard+CoreDataProperties.m
//  DPS
//
//  Created by Akshay Hastekar on 19/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "MechanicalPurchaseOrderStandard+CoreDataProperties.h"

@implementation MechanicalPurchaseOrderStandard (CoreDataProperties)

+ (NSFetchRequest<MechanicalPurchaseOrderStandard *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalPurchaseOrderStandard"];
}

@dynamic companyId;
@dynamic declineMessage;
@dynamic employeeNumber;
@dynamic isActive;
@dynamic poDescription;
@dynamic poType;
@dynamic promotionalCode;
@dynamic purchaseDate;
@dynamic purchaseOrderId;
@dynamic purchaseOrderNumber;
@dynamic routeNumber;
@dynamic status;
@dynamic subWorkOrderNumber;
@dynamic vendorLocationId;
@dynamic vendorMasterId;
@dynamic workOrderNumber;
@dynamic poNotes;

@dynamic purchaseOrderDetailDcs;

@end
