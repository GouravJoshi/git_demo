//
//  MechanicalPurchaseOrderStandard+CoreDataProperties.h
//  DPS
//
//  Created by Akshay Hastekar on 19/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "MechanicalPurchaseOrderStandard+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MechanicalPurchaseOrderStandard (CoreDataProperties)

+ (NSFetchRequest<MechanicalPurchaseOrderStandard *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyId;
@property (nullable, nonatomic, copy) NSString *declineMessage;
@property (nullable, nonatomic, copy) NSString *employeeNumber;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *poDescription;
@property (nullable, nonatomic, copy) NSString *poType;
@property (nullable, nonatomic, copy) NSString *promotionalCode;
@property (nullable, nonatomic, copy) NSString *purchaseDate;
@property (nullable, nonatomic, copy) NSString *purchaseOrderId;
@property (nullable, nonatomic, copy) NSString *purchaseOrderNumber;
@property (nullable, nonatomic, copy) NSString *routeNumber;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSString *subWorkOrderNumber;
@property (nullable, nonatomic, copy) NSString *vendorLocationId;
@property (nullable, nonatomic, copy) NSString *vendorMasterId;
@property (nullable, nonatomic, copy) NSString *workOrderNumber;
@property (nullable, nonatomic, copy) NSString *poNotes;

@property (nullable, nonatomic, retain) NSObject *purchaseOrderDetailDcs;

@end

NS_ASSUME_NONNULL_END
