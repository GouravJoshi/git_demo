//
//  MechanicalPurchaseOrderStandard+CoreDataClass.h
//  DPS
//
//  Created by Akshay Hastekar on 19/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface MechanicalPurchaseOrderStandard : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MechanicalPurchaseOrderStandard+CoreDataProperties.h"
