//
//  MechanicalPurchaseOrderGeneric+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 14/02/18.
//
//

#import "MechanicalPurchaseOrderGeneric+CoreDataProperties.h"

@implementation MechanicalPurchaseOrderGeneric (CoreDataProperties)

+ (NSFetchRequest<MechanicalPurchaseOrderGeneric *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MechanicalPurchaseOrderGeneric"];
}

@dynamic purchaseOrderId;
@dynamic purchaseOrderNumber;
@dynamic purchaseDate;
@dynamic isActive;
@dynamic companyId;
@dynamic status;
@dynamic vendorMasterId;
@dynamic vendorLocationId;
@dynamic promotionalCode;
@dynamic workOrderNumber;
@dynamic subWorkOrderNumber;
@dynamic employeeNumber;
@dynamic routeNumber;
@dynamic declineMessage;
@dynamic poType;
@dynamic poNotes;
@dynamic poDescription;

@end
