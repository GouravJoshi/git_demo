//
//  MechanicalPurchaseOrderGeneric+CoreDataClass.h
//  
//
//  Created by Akshay Hastekar on 14/02/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MechanicalPurchaseOrderGeneric : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MechanicalPurchaseOrderGeneric+CoreDataProperties.h"
