//
//  PurchaseOrderGroupViewController.m
//  DPS
//
//  Created by Akshay Hastekar on 09/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "PurchaseOrderGroupViewController.h"
#import "Global.h"
#import "AllImportsViewController.h"
#import "Header.h"
@interface PurchaseOrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelItemType;
@property (weak, nonatomic) IBOutlet UILabel *labelDepartment;
@property (weak, nonatomic) IBOutlet UILabel *labelCategory;
@property (weak, nonatomic) IBOutlet UILabel *labelItem;
@property (weak, nonatomic) IBOutlet UILabel *labelQuantity;
@property (weak, nonatomic) IBOutlet UIButton *buttonDelete;


@end

@implementation PurchaseOrderCell



@end

@interface PurchaseOrderGroupViewController ()

@end

@implementation PurchaseOrderGroupViewController
{
    NSMutableArray *arrayPartsList;
    NSString *strHeaderHeight;
    UIButton *buttonBackground;
    UITableView *tblData;
    NSMutableArray *arrayVendorLocation;
    NSInteger dropDownBtnTag;
    Global *global;
    NSString *strServiceUrlMainServiceAutomation;
    NSInteger locationTag;
    NSEntityDescription *entityMechanicalPurchaseOrderStandard;
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSInteger arrayPartListCount;
    NSInteger tagDeleteButtonPart;
    NSIndexPath *indexPathDeleteButtonPart;
    NSString *strEmployeeID;
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrayPartsList = [NSMutableArray new];
    arrayVendorLocation = [NSMutableArray new];
    global = [[Global alloc] init];
    
    tblData=[[UITableView alloc]init];
    arrayPartListCount = 0;
    tblData.frame=CGRectMake(0, 0, 300, 500);
    tblData.tag = 10;
    tblData.dataSource=self;
    tblData.delegate=self;
    
    strHeaderHeight = @"not";
    arrayPartsList = [[_dictPartList valueForKey:@"groupPartList"] mutableCopy];
    NSLog(@"%@",arrayPartsList);
    
    for(int i = 0;i<arrayPartsList.count;i++)
    {
        NSMutableDictionary *dict = [[arrayPartsList objectAtIndex:i] mutableCopy];
        [dict setValue:@"" forKey:@"vendorLocationId"];
        [arrayPartsList replaceObjectAtIndex:i withObject:dict];
    }
    strServiceUrlMainServiceAutomation=[_dictLoginDetails valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
      strEmployeeID = [NSString stringWithFormat:@"%@",[_dictLoginDetails valueForKey:@"EmployeeId"]];
}

- (IBAction)actionOnBack:(id)sender
{
    
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag==10)
    {
        return 1;
    }
    return arrayPartsList.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==10)
    {
        return arrayVendorLocation.count;
    }
    return [[[arrayPartsList objectAtIndex:section] valueForKey:@"categoryGroup"] count];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"PurchaseOrderGroupView" owner:self options:nil];
    ViewVendor *viewVendor = [nibArray objectAtIndex:0];
    UILabel *labelVendor = [viewVendor viewWithTag:1];
    
    KPDropMenu *dropDownView = [viewVendor viewWithTag:2];
    
    //    NSArray *arrLoc = [[arrayPartsList objectAtIndex:section] valueForKey:@"vendorLocations"];
    //
    //    for(NSDictionary *dict in arrLoc)
    //    {
    //        [arrayVendorLocation addObject:[dict valueForKey:@"Name"]];
    //    }
    
    UIButton *buttonDropDown = [viewVendor viewWithTag:2];
    buttonDropDown.layer.borderColor = [[UIColor blackColor] CGColor];
    buttonDropDown.layer.borderWidth = 1.0;
    buttonDropDown.tag = section;
    
    
    if([[[arrayPartsList objectAtIndex:section] valueForKey:@"dropDownLocationTitle"] length]>0)
    {
        [buttonDropDown setTitle:[[arrayPartsList objectAtIndex:section] valueForKey:@"dropDownLocationTitle"] forState:UIControlStateNormal];
    }
    else
    {
        [buttonDropDown setTitle:@"Select Vendor Location" forState:UIControlStateNormal];
    }
    
    buttonDropDown.titleLabel.textColor = [UIColor blackColor];
    
    [buttonDropDown setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [buttonDropDown addTarget:self action:@selector(actionOnDropDown:) forControlEvents:UIControlEventTouchUpInside];
    
    UITextField *txtFieldPC = [viewVendor viewWithTag:3];
    
    txtFieldPC.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    txtFieldPC.delegate = self;
    dropDownView.tag = section;
    txtFieldPC.tag = section;
    
    labelVendor.text = [[[arrayPartsList objectAtIndex:section] valueForKey:@"vendor"] valueForKey:@"Name"];
    labelVendor.textAlignment = NSTextAlignmentCenter;
    labelVendor.layer.borderWidth = 1.0;
    labelVendor.layer.borderColor = [[UIColor blackColor] CGColor];
    labelVendor.layer.cornerRadius = 4.0;
    
    return viewVendor;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView.tag==10)
    {
        return 0;
    }
    return 200;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==10)
    {
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.textLabel.text = [arrayVendorLocation objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:18.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    else
    {
        PurchaseOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PurchaseOrderCell"];
        if(cell==nil)
        {
            cell = [[PurchaseOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PurchaseOrderCell"];
        }
        
        cell.labelItem.text = @"";
        if([[[[[arrayPartsList objectAtIndex:indexPath.section] valueForKey:@"partGroup"] objectAtIndex:indexPath.row] valueForKey:@"ItemType"] integerValue]==1)
        {
            cell.labelItemType.text = @"Part";
        }
        else
        {
            cell.labelItemType.text = @"Equipment";
        }
        
        
        cell.labelItem.text = [[[[arrayPartsList objectAtIndex:indexPath.section] valueForKey:@"partGroup"] objectAtIndex:indexPath.row] valueForKey:@"Name"];
        
        cell.labelDepartment.text = [[[[arrayPartsList objectAtIndex:indexPath.section] valueForKey:@"partGroup"] objectAtIndex:indexPath.row] valueForKey:@"DepartmentSysName"];
        
        cell.labelDepartment.text=[self fetchDepartmentName:[[[[arrayPartsList objectAtIndex:indexPath.section] valueForKey:@"partGroup"] objectAtIndex:indexPath.row] valueForKey:@"DepartmentSysName"]];

        cell.labelCategory.text = [[[[arrayPartsList objectAtIndex:indexPath.section] valueForKey:@"categoryGroup"] objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.labelQuantity.text = [NSString stringWithFormat:@"%@",[[[arrayPartsList objectAtIndex:indexPath.section] valueForKey:@"quantityGroup"] objectAtIndex:indexPath.row]];
        
        cell.buttonDelete.tag = indexPath.row;
        [cell.buttonDelete addTarget:self action:@selector(actionOnDelete:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.view endEditing:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(tableView.tag==10)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSMutableDictionary *dict = [[arrayPartsList objectAtIndex:dropDownBtnTag] mutableCopy];
        
        [dict setValue:[arrayVendorLocation objectAtIndex:indexPath.row] forKey:@"dropDownLocationTitle"];
        //        [dict setValue:[[NSString stringWithFormat:@"%@",[[[arrayPartsList objectAtIndex:dropDownBtnTag] valueForKey:@"vendorLocations"] objectAtIndex:indexPath.row] valueForKey:@"VendorLocationId"]]];
        
        [dict setValue:[NSString stringWithFormat:@"%@",[[[[arrayPartsList objectAtIndex:dropDownBtnTag] valueForKey:@"vendorLocations"] objectAtIndex:indexPath.row] valueForKey:@"VendorLocationId"]] forKey:@"VendorLocationId"];
        
        [arrayPartsList replaceObjectAtIndex:dropDownBtnTag withObject:dict];
        [_tableViewPurchaseOrder reloadData];
        [buttonBackground removeFromSuperview];
        [tblData removeFromSuperview];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}
#pragma mark - UIButton action

-(void)actionOnDelete:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    tagDeleteButtonPart = btn.tag;
    [[[UIAlertView alloc] initWithTitle:@"Message" message:@"Are you sure you want to delete part?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
    
    indexPathDeleteButtonPart = [NSIndexPath indexPathForRow:[btn tag] inSection:[[sender superview] tag]];
    
}

-(void)actionOnDropDown:(id)sender
{
    [arrayVendorLocation removeAllObjects];
    UIButton *btn = (UIButton*)sender;
    dropDownBtnTag = btn.tag;
    NSArray *arrLoc = [[arrayPartsList objectAtIndex:dropDownBtnTag] valueForKey:@"vendorLocations"];
    
    for(NSDictionary *dict in arrLoc)
    {
        [arrayVendorLocation addObject:[dict valueForKey:@"Name"]];
    }
    [self setTableFrame:btn.tag];
    [tblData reloadData];
}

-(void)actionOnBackground:(id)sender
{
    [tblData removeFromSuperview];
    [buttonBackground removeFromSuperview];
}

- (IBAction)actionOnSubmitRequest:(id)sender
{
    
    if(arrayPartsList.count>0)
        
    {
        
        for(NSDictionary *dict in arrayPartsList)
            
        {
            
            if(!([[dict valueForKey:@"VendorLocationId"] length]>0))
                
            {
                
                [global AlertMethod:@"Alert!" :@"Please select vendor location"];
                
                return;
                
            }
            
        }
        
        
        
        NSMutableArray *arrayPurchaseOrderDetailDcs = [NSMutableArray new];
        
        NSMutableArray *arrayJsonData = [NSMutableArray new];
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        
        NSString *strVendorLocationID = @"";
        
        for(NSDictionary *dict in arrayPartsList)
            
        {
            
            strVendorLocationID = [NSString stringWithFormat:@"%@",[dict  valueForKey:@"VendorLocationId"]];
            
            [arrayPurchaseOrderDetailDcs removeAllObjects];
            
            
            
            for(int i =0;i<[[dict valueForKey:@"categoryGroup"] count];i++)
                
            {
                
                NSDictionary *dictTemp = @{@"PurchaseDetailId":@"",
                                           
                                           @"PurchaseOrderId":@"",
                                           
                                           @"ItemTypeId":[NSString stringWithFormat:@"%@",[[[dict valueForKey:@"partGroup"] objectAtIndex:i] valueForKey:@"ItemType"]],
                                           
                                           @"DepartmentId":[NSString stringWithFormat:@"%@",[_dictWorkOrderDetails valueForKey:@"departmentId"]],
                                           
                                           @"CategoryId":[NSString stringWithFormat:@"%@",[[[dict valueForKey:@"categoryGroup"] objectAtIndex:i] valueForKey:@"CategoryMasterId"]],
                                           
                                           @"ItemMasterId":[NSString stringWithFormat:@"%@",[[[dict valueForKey:@"partGroup"] objectAtIndex:i] valueForKey:@"ItemMasterId"]],
                                           
                                           @"Quantity":[NSString stringWithFormat:@"%@",[[dict valueForKey:@"quantityGroup"] objectAtIndex:i]],
                                           
                                           @"PartName":[NSString stringWithFormat:@"%@",[[[dict valueForKey:@"partGroup"] objectAtIndex:i] valueForKey:@"Name"]],
                                           
                                           @"VendorPartNumber":[NSString stringWithFormat:@"%@",[[dict valueForKey:@"vendorItemNumberGroup"] objectAtIndex:i]],
                                           
                                           @"VendorMasterId":[NSString stringWithFormat:@"%@",[[dict valueForKey:@"vendor"] valueForKey:@"VendorMasterId"]],
                                           
                                           @"PartNumber":[NSString stringWithFormat:@"%@",[[[dict valueForKey:@"partGroup"] objectAtIndex:i] valueForKey:@"ItemNumber"]],
                                           
                                           @"VendorName":[[dict valueForKey:@"vendor"] valueForKey:@"Name"],
                                           
                                           @"WorkOrderNo":[NSString stringWithFormat:@"%@",[_dictWorkOrderDetails valueForKey:@"workOrderNo"]],
                                           
                                           @"SubWorkOrderNo":[NSString stringWithFormat:@"%@",[_dictSubworkOrderDetails valueForKey:@"subWorkOrderNo"]],
                                           
                                           @"IsActive":@"true"
                                           
                                           };
                
                
                
                [arrayPurchaseOrderDetailDcs addObject:dictTemp];
                
            }
            
            
            
            NSDictionary *dictSynchData = @{@"PurchaseOrderId":@"",
                                            
                                            @"PurchaseOrderNumber":@"0",
                                            
                                            @"PurchaseDate":[self getDate],
                                            
                                            @"IsActive":@"true",
                                            
                                            @"CompanyId":[NSString stringWithFormat:@"%@",[[[[_dictLoginDetails valueForKey:@"Company"] valueForKey:@"CompanyConfig"] valueForKey:@"BillingServiceModule"] valueForKey:@"CompanyId"]],
                                            
                                            @"Status":@"0",
                                            
                                            @"VendorMasterId":[NSString stringWithFormat:@"%@",[[dict valueForKey:@"vendor"]valueForKey:@"VendorMasterId"]],
                                            
                                            @"VendorLocationId":strVendorLocationID,
                                            
                                            @"PromotionalCode":[dict valueForKey:@"promotionalCode"],
                                            
                                            @"WorkOrderNo":[NSString stringWithFormat:@"%@",[_dictWorkOrderDetails valueForKey:@"workOrderNo"]],
                                            
                                            @"SubWorkOrderNo":[NSString stringWithFormat:@"%@",[_dictSubworkOrderDetails valueForKey:@"subWorkOrderNo"]],
                                            
                                            @"EmployeeNo":[NSString stringWithFormat:@"%@",[_dictLoginDetails valueForKey:@"EmployeeNumber"]],
                                            
                                            @"RouteNo":[NSString stringWithFormat:@"%@",[_dictWorkOrderDetails valueForKey:@"routeNo"]],
                                            
                                            @"DeclineMessage":@"",
                                            
                                            @"POType":@"false",
                                            
                                            @"PONotes":_strPONotes,
                                            
                                            @"PODescription":@"",
                                            
                                            @"PurchaseOrderDetailDcs":[arrayPurchaseOrderDetailDcs mutableCopy],
                                            
                                            @"CreatedBy":strEmployeeID,
                                            
                                            @"ModifiedBy":strEmployeeID
                                            
                                            };
            
            
            
            NSLog(@"%@",dictSynchData);
            
            [arrayJsonData addObject:dictSynchData];
            
            arrayPartListCount++;
            
            
            
            if (netStatusWify1== NotReachable)
                
            {
                
                [self saveStandardPurchaseOrderInLocalDB:dictSynchData];
                
            }
            
        }
        
        
        
        if (netStatusWify1== NotReachable)
            
        {
            
            [global AlertMethod:Alert :ErrorInternetMsg];
            
        }
        
        else
            
        {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];
            
            DELAY(1.0);
            
            [self postApiCall:arrayJsonData];
            
        }
        
    }
    
    else
        
    {
        
        [global AlertMethod:Alert :@"Please add part"];
        
    }
}


- (IBAction)actionOnCancelStandardPurchaseOrder:(id)sender
{
    [self back];
    //[self.navigationController popViewControllerAnimated:YES];
    //    if ([_strWoType isEqualToString:@"TM"]) {
    //
    //        //        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
    //        //                                                                 bundle: nil];
    //        //        MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController
    //        //        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController"];
    //        //        objByProductVC.strSubWorkOrderId=strSubWorkOrderId;
    //        //        objByProductVC.strWoType=_strWoType;
    //        //        objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
    //        //        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
    //        //        [self.navigationController pushViewController:objByProductVC animated:NO];
    //
    //
    //        int index = 0;
    //        NSArray *arrstack=self.navigationController.viewControllers;
    //        for (int k1=0; k1<arrstack.count; k1++) {
    //            if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController class]]) {
    //                index=k1;
    //                //break;
    //            }
    //        }
    //        MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController *myController = (MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController *)[self.navigationController.viewControllers objectAtIndex:index];
    //        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    //        [self.navigationController popToViewController:myController animated:NO];
    //
    //
    //    } else {
    //
    //        //        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
    //        //                                                                 bundle: nil];
    //        //        MechanicalSubWorkOrderDetailsViewControlleriPad
    //        //        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControlleriPad"];
    //        //        objByProductVC.strSubWorkOrderId=strSubWorkOrderId;
    //        //        objByProductVC.strWoType=_strWoType;
    //        //        objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
    //        //        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
    //        //        [self.navigationController pushViewController:objByProductVC animated:NO];
    //
    //
    //        int index = 0;
    //        NSArray *arrstack=self.navigationController.viewControllers;
    //        for (int k1=0; k1<arrstack.count; k1++) {
    //            if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControlleriPad class]]) {
    //                index=k1;
    //                //break;
    //            }
    //        }
    //        MechanicalSubWorkOrderDetailsViewControlleriPad *myController = (MechanicalSubWorkOrderDetailsViewControlleriPad *)[self.navigationController.viewControllers objectAtIndex:index];
    //        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    //        [self.navigationController popToViewController:myController animated:NO];
    //
    //    }
}

#pragma mark - UIAlertView delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        NSMutableArray *arrCategory = [[arrayPartsList objectAtIndex:indexPathDeleteButtonPart.section] valueForKey:@"categoryGroup"];
        NSMutableArray *arrPart = [[arrayPartsList objectAtIndex:indexPathDeleteButtonPart.section] valueForKey:@"partGroup"];
        
        NSMutableArray *arrQuantity = [[arrayPartsList objectAtIndex:indexPathDeleteButtonPart.section] valueForKey:@"quantityGroup"];
        [arrCategory removeObjectAtIndex:tagDeleteButtonPart];
        [arrPart removeObjectAtIndex:tagDeleteButtonPart];
        [arrQuantity removeObjectAtIndex:tagDeleteButtonPart];
        
        
        
        if(!(arrCategory.count>0))
        {// remove whole object from array
            [arrayPartsList removeObjectAtIndex:indexPathDeleteButtonPart.section];
        }
        
        [_tableViewPurchaseOrder reloadData];
    }
}

-(void)back
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    /*
    if ([_strFromWhere isEqualToString:@"Start"]) {
        
        if ([_strWoType isEqualToString:@"TM"]) {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController class]]) {
                    index=k1;
                    //break;
                }
            }
            MechanicalStartRepairTM *myController = (MechanicalStartRepairTM *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
            
        } else {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControlleriPad class]]) {
                    index=k1;
                    //break;
                }
            }
            MechanicalStartRepairViewController *myController = (MechanicalStartRepairViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
        }
        
    } else {
        
        if ([_strWoType isEqualToString:@"TM"]) {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController class]]) {
                    index=k1;
                    //break;
                }
            }
            MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController *myController = (MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
            
        } else {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControlleriPad class]]) {
                    index=k1;
                    //break;
                }
            }
            MechanicalSubWorkOrderDetailsViewControlleriPad *myController = (MechanicalSubWorkOrderDetailsViewControlleriPad *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
        }
        
    }
     
     */
}

#pragma mark - KPDropMenu delegate

-(void)didShow:(KPDropMenu *)dropMenu
{
    NSLog(@"didShow");
}

-(void)didHide:(KPDropMenu *)dropMenu
{
    NSLog(@"didHide");
}

#pragma mark - UITextField delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length==0)
    {
        if([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSMutableDictionary *dict = [[arrayPartsList objectAtIndex:textField.tag] mutableCopy];
    if(textField.text.length>0)
    {
        [dict setValue:textField.text forKey:@"promotionalCode"];
    }
    else
    {
        [dict setValue:@"" forKey:@"promotionalCode"];
    }
    
    [arrayPartsList replaceObjectAtIndex:textField.tag withObject:dict];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)setTableFrame:(NSInteger)btntag
{
    buttonBackground=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    buttonBackground.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    
    [buttonBackground addTarget:self action:@selector(actionOnBackground:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: buttonBackground];
    
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    
    tblData.layer.borderWidth = 1.0;
    tblData.layer.borderColor = [[UIColor colorWithRed:95.0/255.0 green:178.0/255.0 blue:175.0/255.0 alpha:1.0] CGColor];
    
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        tblData.frame=CGRectMake(20, ([UIScreen mainScreen].bounds.size.height*30)/100, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height/2);
    }
    
    [self.view addSubview:tblData];
}

-(void)tableLoad:(NSInteger)btntag
{
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 10:
        {
            [self setTableFrame:i];
            break;
        }
        case 20:
        {
            [self setTableFrame:i];
            break;
        }
        case 30:
        {
            [self setTableFrame:i];
            break;
        }
        case 40:
        {
            [self setTableFrame:i];
            break;
        }
        case 50:
        {
            [self setTableFrame:i];
            break;
        }
            
            
        default:
            break;
    }
    
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    tblData.layer.cornerRadius=20.0;
    tblData.clipsToBounds=YES;
    [tblData.layer setBorderWidth:2.0];
    [tblData.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [tblData reloadData];
}

#pragma mark - get date
-(NSString *)getDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    NSString *string = [formatter stringFromDate:[NSDate date]];
    return string;
}

-(void)postApiCall:(NSMutableArray *)jsonArray
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMainServiceAutomation,UrlMechanicalToSubmitPurchaseOrder,[NSString stringWithFormat:@"%@",[_dictWorkOrderDetails valueForKey:@"companyKey"]]];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonArray options:NSJSONWritingPrettyPrinted error:Nil];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonDatastr %@",str);
    NSURL * serviceUrl = [NSURL URLWithString:strUrl];
    NSLog(@"REquest URL >> %@",serviceUrl);
    NSLog(@"REquest XML >> %@",str);
    
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:serviceUrl];
    
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *__block serviceResponse;
    NSError *__block serviceError;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&serviceResponse error:&serviceError];
      
        [DejalBezelActivityView removeView];
        
        if (responseData)
        {
            [self parsePostApiData:responseData];
        }
        else
        {
            //        AlertViewClass *a = [[AlertViewClass alloc] init];
            //        [a showMessage:@"Cannot connect to internet." title:@"Skillgrok"];
        }
        
    });
}

-(void)parsePostApiData:(NSData *)response
{
    id jsonObject = Nil;
    
    NSString *charlieSendString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSLog(@"ResponseString %@",charlieSendString);
    
    if (response==nil)
    {
        NSLog(@"No internet connection.");
    }
    else
    {
        NSError *error = Nil;
        jsonObject =[NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            NSLog(@"Probably An Array %@",jsonObject);
            if([[[jsonObject objectAtIndex:0] valueForKey:@"Value"] boolValue]==1)
            {
                [self deleteStandardPurchaseOrderFromLocalDB];
                
                NSString *strMsg = [[jsonObject objectAtIndex:1] valueForKey:@"Value"];
                NSArray *arrayMsg = [strMsg componentsSeparatedByString:@"order"];
                NSString *strPONumbers = [NSString stringWithFormat:@"%@",[[jsonObject objectAtIndex:2] valueForKey:@"Value"]];
                
                NSString *strCompleteMsg = [NSString stringWithFormat:@"%@ order# %@ %@",[arrayMsg objectAtIndex:0],strPONumbers,[arrayMsg objectAtIndex:1]];
                
                NSLog(@"%@",strCompleteMsg);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [global AlertMethod:@"Message" :strCompleteMsg];
                    [self popViewController];
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [global AlertMethod:@"Message" :[[jsonObject objectAtIndex:1] valueForKey:@"Value"]];
                });
            }
        }
        else
        {
            NSLog(@"Probably A Dictionary");
            NSDictionary *jsonDictionary=(NSDictionary *)jsonObject;
            NSLog(@"jsonDictionary %@",[jsonDictionary description]);
        }
    }
}

-(void)saveStandardPurchaseOrderInLocalDB:(NSDictionary*)dictData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    context = [appDelegate managedObjectContext];
    
    entityMechanicalPurchaseOrderStandard = [NSEntityDescription entityForName:@"MechanicalPurchaseOrderStandard" inManagedObjectContext:context];
    
    MechanicalPurchaseOrderStandard *objMechanicalPOStandard = [[MechanicalPurchaseOrderStandard alloc] initWithEntity:entityMechanicalPurchaseOrderStandard insertIntoManagedObjectContext:context];
    
    objMechanicalPOStandard.purchaseOrderId = @"";
    
    objMechanicalPOStandard.purchaseOrderNumber = @"";
    
    objMechanicalPOStandard.purchaseDate = [dictData valueForKey:@"PurchaseDate"];
    
    objMechanicalPOStandard.isActive = [dictData valueForKey:@"IsActive"];
    
    objMechanicalPOStandard.companyId = [dictData valueForKey:@"CompanyId"];
    objMechanicalPOStandard.status = @"0";
    objMechanicalPOStandard.vendorMasterId = [dictData valueForKey:@"VendorMasterId"];
    objMechanicalPOStandard.vendorLocationId = [dictData valueForKey:@"VendorLocationId"];
    objMechanicalPOStandard.promotionalCode = [dictData valueForKey:@"PromotionalCode"];
    objMechanicalPOStandard.workOrderNumber = [dictData valueForKey:@"WorkOrderNo"];
    objMechanicalPOStandard.subWorkOrderNumber = [dictData valueForKey:@"SubWorkOrderNo"];
    objMechanicalPOStandard.employeeNumber = [dictData valueForKey:@"EmployeeNo"];
    objMechanicalPOStandard.routeNumber = [dictData valueForKey:@"RouteNo"];
    objMechanicalPOStandard.declineMessage = [dictData valueForKey:@"DeclineMessage"];
    objMechanicalPOStandard.poType = [dictData valueForKey:@"POType"];
    objMechanicalPOStandard.poNotes = [dictData valueForKey:@"PONotes"];
    objMechanicalPOStandard.poDescription = [dictData valueForKey:@"PODescription"];
    objMechanicalPOStandard.purchaseOrderDetailDcs = [dictData valueForKey:@"PurchaseOrderDetailDcs"];
    
    NSError *error2;
    [context save:&error2];
    
    if(arrayPartListCount==arrayPartsList.count)
    {
        [global AlertMethod:@"Message" :@"Purchase order saved successfully"];
        [self back];
    }
}

-(void)deleteStandardPurchaseOrderFromLocalDB
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityMechanicalPurchaseOrderStandard=[NSEntityDescription entityForName:@"MechanicalPurchaseOrderStandard" inManagedObjectContext:context];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalPurchaseOrderStandard" inManagedObjectContext:context]];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && issueRepairPartId = %@",_strWorkOrderId,_strSubWorkOrderId,_strIssuePartsIdToFetch,strID];
    //    [allData setPredicate:predicate];
    
    //    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

-(void)popViewController
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"goToMainView"];
    [defs synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //    for(UIViewController*vc in self.navigationController.viewControllers)
    //    {
    //        if([vc isKindOfClass:[MechanicalSubWorkOrderDetailsViewControlleriPad class]])
    //        {
    //            dispatch_async(dispatch_get_main_queue(), ^{
    //                [self.navigationController popToViewController:vc animated:YES];
    //            });
    //        }
    //    }
    
   // [self goToSubWorkOrderDetailView];
    
}


-(void)goToSubWorkOrderDetailView{
    
    if ([_strFromWhere isEqualToString:@"Start"]) {
        
        NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
        [defss setInteger:-1 forKey:@"sectionToOpen"];
        [defss synchronize];
        
        if ([_strWoType isEqualToString:@"TM"]) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                     bundle: nil];
            MechanicalStartRepairTM
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairTM"];
            objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]];
            objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
            objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
            objByProductVC.strWoType=_strWoType;
            
            NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
            [defsId setValue:[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]]  forKey:@"SubWorkOrderId"];
            [defsId synchronize];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:objByProductVC animated:NO];
            });
            
            
        } else {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                     bundle: nil];
            MechanicalStartRepairViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairViewController"];
            objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]];
            objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
            objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
            objByProductVC.strWoType=_strWoType;
            //workorderId
            NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
            [defsId setValue:[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]]  forKey:@"SubWorkOrderId"];
            [defsId synchronize];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:objByProductVC animated:NO];
            });
            
        }
        
    } else {
        
        NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
        [defss setInteger:-1 forKey:@"sectionToOpen"];
        [defss synchronize];
        
        if ([_strWoType isEqualToString:@"TM"]) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                     bundle: nil];
            MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController"];
            objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]];
            objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
            objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
            objByProductVC.strWoType=_strWoType;
            
            NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
            [defsId setValue:[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]]  forKey:@"SubWorkOrderId"];
            [defsId synchronize];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:objByProductVC animated:NO];
            });
            
            
        } else {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                     bundle: nil];
            MechanicalSubWorkOrderDetailsViewControlleriPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControlleriPad"];
            objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]];
            objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
            objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
            objByProductVC.strWoType=_strWoType;
            //workorderId
            NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
            [defsId setValue:[NSString stringWithFormat:@"%@",[_objSubWorkOrderDetail valueForKey:@"subWorkOrderId"]]  forKey:@"SubWorkOrderId"];
            [defsId synchronize];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:objByProductVC animated:NO];
            });
            
        }
        
    }
}

- (IBAction)action_PoHistory:(id)sender {
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    PoHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"PoHistoryViewController"];
    objSignViewController.strWorkOrderNo=[NSString stringWithFormat:@"%@",[_dictWorkOrderDetails valueForKey:@"workOrderNo"]];
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];

}

-(NSString*)fetchDepartmentName :(NSString*)strSysname
{
    NSString *strName;
    
    @try {
        
        NSDictionary *dictSalesLeadMaster;
        NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
        dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
        NSMutableArray *arrDept;
        NSMutableArray *arrName12,*arrSysName12;
        arrName12=[[NSMutableArray alloc]init];
        arrSysName12=[[NSMutableArray alloc]init];
        arrDept=[[NSMutableArray alloc]init];
        NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
        for (int i=0;i<arrDeptName.count; i++)
        {
            NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
            NSArray *arry=[dictBranch valueForKey:@"Departments"];
            for (int j=0; j<arry.count; j++)
            {
                NSDictionary *dict=[arry objectAtIndex:j];
                
                [arrName12 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [arrSysName12 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                
                
            }
            
        }
        NSDictionary *dictDepartment = [NSDictionary dictionaryWithObjects:arrName12 forKeys:arrSysName12];
        
        strName=[NSString stringWithFormat:@"%@",[dictDepartment valueForKey:strSysname]];
        
    } @catch (NSException *exception) {
        
        //strName=@"N/A";
        
    } @finally {
        
        //strName=@"N/A";
    }
    
    return strName;
    
}

@end
