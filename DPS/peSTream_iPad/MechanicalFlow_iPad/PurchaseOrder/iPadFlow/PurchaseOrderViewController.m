//
//  PurchaseOrderViewController.m
//  DPS
//
//  Created by Akshay Hastekar on 07/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "PurchaseOrderViewController.h"
#import "Global.h"
#import "PurchaseOrderGroupViewController.h"
// part list cell

@interface PartListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelPart;
@property (weak, nonatomic) IBOutlet UILabel *labelVendorPart;
@property (weak, nonatomic) IBOutlet UILabel *labelPartName;
@property (weak, nonatomic) IBOutlet UILabel *labelPartType;
@property (weak, nonatomic) IBOutlet UITextField *textFieldQuanity;
@property (weak, nonatomic) IBOutlet KPDropMenu *viewDropDownVendor;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeletePart;

@end

@implementation PartListCell


@end


@interface PurchaseOrderViewController ()

@end

@implementation PurchaseOrderViewController
{
    NSMutableArray *arrayVendorName;
    NSArray *arrayVendor;
    NSArray *arrayVendorLocation;
    NSMutableArray *arrayVendorLocationName;
    NSMutableArray *arrayCategories;
    NSMutableArray *arrayCategoriesName;
    NSArray *arrayParts;
    NSMutableArray *arrayPartsName;
    BOOL isGenericPO;
    Global *global;
    NSMutableArray *arrayPartList;
    NSString *strCategory;
    NSString *strPart;
    NSString *strQuantity;
    NSString *categoryIndexPath,*partIndexPath;// this will be used to take out object from category and parts array
    NSDictionary *dictVendorGeneric;
    NSDictionary *dictVendorLocationGeneric;
    
    NSDictionary *dictPartAdded;
    NSInteger itemMasterID;
    
    NSArray *arrayItemVendorMapping;
    NSString *strVendorItemNumber;
    AppDelegate *appDelegate;
    NSInteger tagDeleteButtonPart;
    
    NSFetchRequest *requestSubWorkOrderIssues,*requestSubWorkOrderHelper,*requestSubWorkOrderNotes,*requestSubWorkOrderActualHrs,*requestSubWorkOrderIssuesRepair,*requestSubWorkOrderIssuesRepairParts,*requestSubWorkOrderIssuesRepairLabour,*requestNewWorkOrder,*requestMechanicalSubWorkOrder;
    
    NSSortDescriptor *sortDescriptorSubWorkOrderIssues,*sortDescriptorSubWorkOrderHelper,*sortDescriptorSubWorkOrderNotes,*sortDescriptorSubWorkOrderActualHrs,*sortDescriptorSubWorkOrderIssuesRepair,*sortDescriptorSubWorkOrderIssuesRepairParts,*sortDescriptorSubWorkOrderIssuesRepairLabour,*sortDescriptorWorkOrder,*sortDescriptorMechanicalSubWorkOrder;
    
    
    NSEntityDescription *entitySubWorkOrderIssuesRepairParts,*entityMechanicalPurchaseOrderGeneric,*entityMechanicalPurchaseOrderStandard,*entityWorkOrder,*entityMechanicalSubWorkOrder;
    ;
    
    NSManagedObject *matchesSubWorkOrderIssues,*matchesSubWorkOrderHelper,*matchesSubWorkOrderNotes,*matchesSubWorkOrderActualHrs,*matchesSubWorkOrderIssuesRepair,*matchesSubWorkOrderIssuesRepairParts,*matchesSubWorkOrderIssuesRepairLabour;
    
    NSArray *arrAllObjSubWorkOrderIssues,*arrAllObjSubWorkOrderHelper,*arrAllObjSubWorkOrderNotes,*arrAllObjSubWorkOrderActualHrs,*arrAllObjSubWorkOrderIssuesRepair,*arrAllObjSubWorkOrderIssuesRepairParts,*arrAllObjSubWorkOrderIssuesRepairLabour,*arrAllObjMechanicalSubWorkOrder;
    
    NSArray *sortDescriptorsSubWorkOrderIssues,*sortDescriptorsSubWorkOrderHelper,*sortDescriptorsSubWorkOrderNotes,*sortDescriptorsSubWorkOrderActualHrs,*sortDescriptorsSubWorkOrderIssuesRepair,*sortDescriptorsSubWorkOrderIssuesRepairParts,*sortDescriptorsSubWorkOrderIssuesRepairLabour,*sortDescriptorsWorkOrder,*sortDescriptorsMechanicalSubWorkOrder;
    
    NSManagedObjectContext *context;
    
    //     NSString *strSubWorkOrderId;
    //     NSString *strWorkOrderId;
    NSString *strIssuePartsIdToFetch;
    NSString *strSubWorkOrderIssuesIdToFetch;
    NSString *strDepartmentSysName;
    NSDictionary *dictLoginDetails;
    NSArray *arrAllObjWorkOrder;
    NSDictionary *dictWorkOrderDetails;
    NSDictionary *dictSubWorkOrderDetails;
    NSString* strServiceUrlMainServiceAutomation;
    NSMutableArray *arrayFilteredVendorLocation;
    NSString *strEmployeeID;
    
}
@synthesize strWorkOrderId,strSubWorkOrderId;
#pragma mark - View's life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"goToMainView"];
    [defs synchronize];

    arrayVendorName = [NSMutableArray new];
    arrayVendorLocationName = [NSMutableArray new];
    arrayCategoriesName = [NSMutableArray new];
    arrayPartsName = [NSMutableArray new];
    arrayPartList = [NSMutableArray new];
    arrayCategories = [NSMutableArray new];
    arrayFilteredVendorLocation = [NSMutableArray new];
    global = [[Global alloc]init];
    
    categoryIndexPath = @"not";
    partIndexPath = @"not";
    
    _textViewPONotes_Standard.layer.borderWidth = 1.0;
    _textViewPONotes_Standard.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _textViewPONotes_Standard.layer.cornerRadius = 4.0;
    
    NSLog(@"%@",_objWorkOrderDetail);
    NSLog(@"%@",_objSubWorkOrderDetail);
    
    NSArray *keysWO = [[[_objWorkOrderDetail entity] attributesByName] allKeys];
    dictWorkOrderDetails = [_objWorkOrderDetail dictionaryWithValuesForKeys:keysWO];
    
    NSArray *keysSWO = [[[_objSubWorkOrderDetail entity] attributesByName] allKeys];
    dictSubWorkOrderDetails = [_objSubWorkOrderDetail dictionaryWithValuesForKeys:keysSWO];
    
    
    strDepartmentSysName = [dictSubWorkOrderDetails valueForKey:@"departmentSysName"];
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    dictLoginDetails = [defsLead objectForKey:@"LoginDetails"];
      strEmployeeID = [NSString stringWithFormat:@"%@",[dictLoginDetails valueForKey:@"EmployeeId"]];
    strServiceUrlMainServiceAutomation=[dictLoginDetails valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    NSDictionary *dictCategory = [defsLead valueForKey:@"MasterServiceAutomation"];
    NSArray *arrayTempCategory = [dictCategory valueForKey:@"InventoryCategoryMaster"];
    
    
    for (int k=0; k<arrayTempCategory.count; k++)
    {
        NSDictionary *dictDataa=arrayTempCategory[k];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        
        NSString *strDepartMentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        
        if ((([strIsActive isEqualToString:@"1"] || [strIsActive isEqualToString:@"1"]) && [strDepartmentSysName isEqualToString:strDepartMentSysName])) {
            
            [arrayCategories addObject:dictDataa];
        }
    }
    
    
    arrayParts = [global getPartsMaster:strDepartmentSysName :@""];
    
    // ItemType 1= Part else equipment
    if(arrayCategories.count>0)
    {// taking out array of category name to show category list
        for(NSDictionary *dict in arrayCategories)
        {
            [arrayCategoriesName addObject:[dict valueForKey:@"Name"]];
        }
    }
    
    if(arrayParts.count>0)
    {// taking out array of part name to show part list
        for(NSDictionary *dict in arrayParts)
        {
            [arrayPartsName addObject:[dict valueForKey:@"Name"]];
        }
    }
    
    isGenericPO = YES;
    
    _buttonTransparent.hidden = YES;
    _viewPopUp_AddPart.hidden = YES;
    
    _textViewPODescription_Generic.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _textViewPODescription_Generic.layer.cornerRadius = 5.0;
    _textViewPODescription_Generic.layer.borderWidth = 1.0;
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMechanicalMasters=[defrs valueForKey:@"MasterAllMechanical"];
    arrayVendor=[dictMechanicalMasters valueForKey:@"VendorBasicExtSerDc"];
    arrayVendorLocation=[dictMechanicalMasters valueForKey:@"VendorLocationExtSerDc"];
    
    arrayItemVendorMapping = [dictMechanicalMasters valueForKey:@"ItemVendorMappingExtSerDc"];
    
    if(arrayVendor.count>0)
    {
        for(NSDictionary *dict in arrayVendor)
        {// taking out array of vendor name to show vendor list
            
            [arrayVendorName addObject:[dict valueForKey:@"Name"]];
        }
    }
    
    if(arrayVendorLocation.count>0)
    {
        for(NSDictionary *dict in arrayVendorLocation)
        {// taking out array of vendor location name to show vendor location list
            
            [arrayVendorLocationName addObject:[dict valueForKey:@"Name"]];
        }
    }
    
    // for vendor name
    _viewSelectVendor_Generic.items = arrayVendorName;
    _viewSelectVendor_Generic.itemsFont = [UIFont fontWithName:@"Helvetica-Regular" size:19.0];
    _viewSelectVendor_Generic.title = @"Select Vendor";
    _viewSelectVendor_Generic.titleTextAlignment = NSTextAlignmentCenter;
    _viewSelectVendor_Generic.delegate = self;
    
    
    // for vendor location
    //    _viewSelectVendorLocation_Generic.items = arrayVendorLocationName;
    _viewSelectVendorLocation_Generic.itemsFont = [UIFont fontWithName:@"Helvetica-Regular" size:19.0];
    _viewSelectVendorLocation_Generic.title = @"Select Vendor Location";
    _viewSelectVendorLocation_Generic.titleTextAlignment = NSTextAlignmentCenter;
    _viewSelectVendorLocation_Generic.delegate = self;
    
    
    _labelAddress.text = @"";
    
    [self configureDropDownViewForPopUp];
    
    _textFieldQuantity_AddPart.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _textFieldQuantity_AddPart.layer.borderWidth = 1.0;
    _textFieldQuantity_AddPart.layer.cornerRadius = 4.0;
    
    [self showValues];
    
    [self fetchSubWorkOrderIssuesRepairPartFromDataBaseForMechanical];
    
}

-(void)configureDropDownViewForPopUp
{
    // for category list in pop up
    _viewDropDownSelectCategory_AddPart.items = arrayCategoriesName;
    _viewDropDownSelectCategory_AddPart.itemsFont = [UIFont fontWithName:@"Helvetica-Regular" size:19.0];
    _viewDropDownSelectCategory_AddPart.title = @"Select Category";
    _viewDropDownSelectCategory_AddPart.titleTextAlignment = NSTextAlignmentCenter;
    _viewDropDownSelectCategory_AddPart.delegate = self;
    
    
    // for parts list in pop up
    _viewDropDownSelectPart_AddPart.items = arrayPartsName;
    _viewDropDownSelectPart_AddPart.itemsFont = [UIFont fontWithName:@"Helvetica-Regular" size:19.0];
    _viewDropDownSelectPart_AddPart.title = @"Select Part";
    _viewDropDownSelectPart_AddPart.titleTextAlignment = NSTextAlignmentCenter;
    _viewDropDownSelectPart_AddPart.delegate = self;
    
    ///////////////////////////////////////////////////////////////////
    
    _heightStandardView.constant = 300+arrayPartList.count*256;
    [_viewStandard updateConstraintsIfNeeded];
    
    CGSize textSize = [self findHeightForText:_labelAddress.text havingWidth:_labelAddress.frame.size.width andFont:_labelAddress.font];
    _heightTopContainerView.constant = (21*9)+40+2+228+textSize.height;
    [_viewTopContainer updateConstraintsIfNeeded];
    
    _heightMainContentView.constant = _heightMainContentView.constant+textSize.height;
    
    [_viewGeneric setFrame:CGRectMake(10, CGRectGetMaxY(_viewTopContainer.frame)+100, _viewGeneric.frame.size.width, _viewGeneric.frame.size.height)];
    
    [_viewMainContainer addSubview:_viewGeneric];
    
    _heightMainContentView.constant = 200+_viewTopContainer.frame.size.height+_viewGeneric.frame.size.height;
    
    
    [_buttonGeneric setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_viewStandard removeFromSuperview];
    
    [_viewGeneric setFrame:CGRectMake(0, CGRectGetMaxY(_viewTopContainer.frame)+100, _viewGeneric.frame.size.width, _viewGeneric.frame.size.height)];
    
    [_viewMainContainer addSubview:_viewGeneric];
    
    _heightViewContainer.constant = _viewGeneric.frame.size.height;
    [_viewContainer updateConstraintsIfNeeded];
    
    _heightMainContentView.constant = 200+_viewTopContainer.frame.size.height+_viewGeneric.frame.size.height;
    isGenericPO = YES;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL isToGoToMainView=[defs boolForKey:@"goToMainView"];
    
    if (isToGoToMainView) {
        
        [defs setBool:NO forKey:@"goToMainView"];
        [defs synchronize];

        [self dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        
        [super viewWillAppear:animated];

    }
    

    
    
    //    _heightStandardView.constant = 240+arrayPartList.count*256;
    //    [_viewStandard updateConstraintsIfNeeded];
    //
    //    CGSize textSize = [self findHeightForText:_labelAddress.text havingWidth:_labelAddress.frame.size.width andFont:_labelAddress.font];
    //    _heightTopContainerView.constant = (21*9)+40+2+228+textSize.height;
    //    [_viewTopContainer updateConstraintsIfNeeded];
    //
    //    _heightMainContentView.constant = _heightMainContentView.constant+textSize.height;
    //
    //    [_viewGeneric setFrame:CGRectMake(10, CGRectGetMaxY(_viewTopContainer.frame)+100, _viewGeneric.frame.size.width, _viewGeneric.frame.size.height)];
    //
    //    [_viewMainContainer addSubview:_viewGeneric];
    //
    //    _heightMainContentView.constant = 200+_viewTopContainer.frame.size.height+_viewGeneric.frame.size.height;
    //
    //
    //    [_buttonGeneric setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    //    [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    //    [_viewStandard removeFromSuperview];
    //
    //    [_viewGeneric setFrame:CGRectMake(0, CGRectGetMaxY(_viewTopContainer.frame)+100, _viewGeneric.frame.size.width, _viewGeneric.frame.size.height)];
    //
    //    [_viewMainContainer addSubview:_viewGeneric];
    //
    //    _heightViewContainer.constant = _viewGeneric.frame.size.height;
    //    [_viewContainer updateConstraintsIfNeeded];
    //
    //    _heightMainContentView.constant = 200+_viewTopContainer.frame.size.height+_viewGeneric.frame.size.height;
    //    isGenericPO = YES;
}

-(void)showValues
{
    NSString *strCustomerName = @"";
    
    strCustomerName = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"firstName"]];
    if([[dictWorkOrderDetails valueForKey:@"middleName"] length]>0)
    {
        strCustomerName = [NSString stringWithFormat:@"%@ %@",strCustomerName,[dictWorkOrderDetails valueForKey:@"middleName"]];
    }
    if([[dictWorkOrderDetails valueForKey:@"lastName"] length]>0)
    {
        strCustomerName = [NSString stringWithFormat:@"%@ %@",strCustomerName,[dictWorkOrderDetails valueForKey:@"lastName"]];
    }
    
    _labelWorkOrder.text = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"workOrderNo"]];
    _labelSubWorkOrder.text = [NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderNo"]];
    _labelAccount.text = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"accountNo"]];
    _labelCustomer.text = strCustomerName;
    _labelEmailID.text = [dictWorkOrderDetails valueForKey:@"primaryEmail"];
    _labelPhone.text = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"primaryPhone"]];
    
//    if(_labelPhone.text.length>0)
//    {
//        if([_labelPhone.text rangeOfString:@"-"].location==NSNotFound)
//        {
//            //string does not contain "-"
//            
//            NSMutableString *mutableString = [NSMutableString new];
//            [mutableString setString:_labelPhone.text];
//            
//            for (int p = 0; p < [mutableString length]; p++)
//            {
//                if (p == 2||p==5)
//                {
//                    [mutableString insertString:@"-" atIndex:p];
//                }
//            }
//            _labelPhone.text = mutableString;
//            NSLog(@"%@", mutableString);
//            
//        }
//        else
//        {
//            // string contains "-"
//            
//        }
//        
//    }
    
    
    //_labelDate.text = [dictWorkOrderDetails valueForKey:@"scheduleOnStartDateTime"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd/MM/yyyy HH:mm:ss";
    NSDate *timeDate = [formatter dateFromString:[dictWorkOrderDetails valueForKey:@"scheduleOnStartDateTime"]];
    
    formatter.dateFormat = @"dd/MM/yyyy HH:mm";
    
    _labelDate.text = [formatter stringFromDate:timeDate];
    
    if (_labelDate.text.length==0) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd/MM/yyyy HH:mm";
        NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
        _labelDate.text = stringCurrentDate;
        
    }
    
    NSString *strAddress = @"";
    
    if([[dictWorkOrderDetails valueForKey:@"servicesAddress1"] length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"servicesAddress1"]];
    }
    if([[dictWorkOrderDetails valueForKey:@"serviceAddress2"] length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@,%@",strAddress,[dictWorkOrderDetails valueForKey:@"serviceAddress2"]];
    }
    if([[dictWorkOrderDetails valueForKey:@"serviceCity"] length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@,%@",strAddress,[dictWorkOrderDetails valueForKey:@"serviceCity"]];
    }
    if([[dictWorkOrderDetails valueForKey:@"serviceState"] length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@,%@",strAddress,[dictWorkOrderDetails valueForKey:@"serviceState"]];
    }
    if([[dictWorkOrderDetails valueForKey:@"serviceZipcode"] length]>0)
    {
        strAddress = [NSString stringWithFormat:@"%@,%@",strAddress,[dictWorkOrderDetails valueForKey:@"serviceZipcode"]];
    }
    
    _labelAddress.text = strAddress;
}

#pragma mark - UITableView's Delegate and datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayPartList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PartListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PartListCell"];
    
    if(cell==nil)
    {
        cell = [[PartListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PartListCell"];
    }
    
    cell.labelPart.text = [NSString stringWithFormat:@"%@",[[[arrayPartList objectAtIndex:indexPath.row] valueForKey:@"part"] valueForKey:@"ItemNumber"]];
    cell.labelVendorPart.text = [[arrayPartList objectAtIndex:indexPath.row] valueForKey:@"vendorItemNumber"];
    cell.labelPartName.text = [NSString stringWithFormat:@"%@",[[[arrayPartList objectAtIndex:indexPath.row] valueForKey:@"part"] valueForKey:@"Name"]];
    
    if([[[[arrayPartList objectAtIndex:indexPath.row] valueForKey:@"part"] valueForKey:@"ItemType"] intValue]==1)
    {
        cell.labelPartType.text = @"Part";
    }
    else
    {
        cell.labelPartType.text = @"Equipment";
    }
    
    cell.textFieldQuanity.layer.borderWidth = 1.0;
    cell.textFieldQuanity.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    cell.textFieldQuanity.layer.cornerRadius = 4.0;
    cell.textFieldQuanity.text = [NSString stringWithFormat:@"%@",[[arrayPartList objectAtIndex:indexPath.row] valueForKey:@"quantity"]];//quantity
    cell.textFieldQuanity.tag = indexPath.row;
    cell.textFieldQuanity.delegate = self;
    
    // for vendor
    
    NSMutableArray *arrayName = [NSMutableArray new];
    
    for(NSDictionary *dict in [[arrayPartList objectAtIndex:indexPath.row] valueForKey:@"vendor"])
    {
        [arrayName addObject:[dict valueForKey:@"Name"]];
    }
    
    cell.viewDropDownVendor.items = arrayName;
    cell.viewDropDownVendor.itemsFont = [UIFont fontWithName:@"Helvetica-Regular" size:19.0];
    
    if([[[arrayPartList objectAtIndex:indexPath.row] valueForKey:@"vendorName"] length]>0)
    {
        cell.viewDropDownVendor.title = [[arrayPartList objectAtIndex:indexPath.row] valueForKey:@"vendorName"];
    }
    else
    {
        cell.viewDropDownVendor.title = @"Select Vendor";
    }
    
    cell.viewDropDownVendor.titleTextAlignment = NSTextAlignmentCenter;
    cell.viewDropDownVendor.tag = indexPath.row;
    cell.viewDropDownVendor.DirectionDown = NO;
    cell.viewDropDownVendor.delegate = self;
    
    cell.buttonDeletePart.tag = indexPath.row;
    [cell.buttonDeletePart addTarget:self action:@selector(actionOnDeletePart:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

#pragma UIButton Action

// button actions to switch from generic view or standard view
- (IBAction)actionOnGeneric:(id)sender
{
    if(!isGenericPO)
    {
        [_buttonGeneric setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_viewStandard removeFromSuperview];
        
        [_viewGeneric setFrame:CGRectMake(0, CGRectGetMaxY(_viewTopContainer.frame)+100, _viewGeneric.frame.size.width, _viewGeneric.frame.size.height)];
        
        [_viewMainContainer addSubview:_viewGeneric];
        
        _heightViewContainer.constant = _viewGeneric.frame.size.height;
        [_viewContainer updateConstraintsIfNeeded];
        
        _heightMainContentView.constant = 200+_viewTopContainer.frame.size.height+_viewGeneric.frame.size.height;
        isGenericPO = YES;
    }
}

- (IBAction)actionOnStandard:(id)sender {
    
    if(isGenericPO)
    {
        [_buttonStandard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_buttonGeneric setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_viewGeneric removeFromSuperview];
        
        [_viewStandard setFrame:CGRectMake(10, CGRectGetMaxY(_viewTopContainer.frame)+20, _viewStandard.frame.size.width, 300+arrayPartList.count*256)];
        //        [_viewContainer addSubview:_viewStandard];
        
        [_viewMainContainer addSubview:_viewStandard];
        //       _heightViewContainer.constant = _viewStandard.frame.size.height;
        
        [_viewContainer updateConstraintsIfNeeded];
        
        // 200 including navigation bar height,vertical space between two main view and submit request button
        _heightMainContentView.constant = 200+_viewTopContainer.frame.size.height+_viewStandard.frame.size.height;
        
        isGenericPO = NO;
        
        if(arrayPartList.count>0)
        {
            [_tableViewPartList reloadData];
        }
    }
}

// button actions to submit generic PO
- (IBAction)actionOnSubmitRequest_Generic:(id)sender
{
    if(isGenericPO)
    {
        if(!(dictVendorGeneric.count>0))
        {
            [global AlertMethod:@"Message" :@"Please select vendor"];
        }
        else if (!(dictVendorLocationGeneric.count>0))
        {
            [global AlertMethod:@"Message" :@"Please select vendor location"];
        }
        //        else if (!(_textFieldPC_Generic.text.length>0))
        //        {
        //            [global AlertMethod:@"Message" :@"Please enter promotional code"];
        //        }
        //        else if (!(_textViewPODescription_Generic.text.length>0))
        //        {
        //            [global AlertMethod:@"Message" :@"Please enter description"];
        //        }
        else
        {// if promotional code and description will be manadatory then uncomment above commented code
            
            // submit generic purchase order
            
            NSString *strPONotesGeneric = @"";
            
            if(_textViewPODescription_Generic.text.length>0)
            {
                strPONotesGeneric = _textViewPODescription_Generic.text;
            }
            
            Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
            NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
            if (netStatusWify1== NotReachable)
            {
                [global AlertMethod:Alert :ErrorInternetMsg];
//                context = [appDelegate managedObjectContext];
//                
//                entityMechanicalPurchaseOrderGeneric = [NSEntityDescription entityForName:@"MechanicalPurchaseOrderGeneric" inManagedObjectContext:context];
//                
//                MechanicalPurchaseOrderGeneric *objMechanicalPOGeneric = [[MechanicalPurchaseOrderGeneric alloc] initWithEntity:entityMechanicalPurchaseOrderGeneric insertIntoManagedObjectContext:context];
//                
//                objMechanicalPOGeneric.purchaseOrderId = [global getReferenceNumber];
//                objMechanicalPOGeneric.purchaseOrderNumber = @"0";
//                objMechanicalPOGeneric.purchaseDate = [self getDate];
//                objMechanicalPOGeneric.isActive = @"true";
//                objMechanicalPOGeneric.companyId = @"";
//                objMechanicalPOGeneric.status = @"0";
//                objMechanicalPOGeneric.vendorMasterId = [NSString stringWithFormat:@"%@",[dictVendorGeneric valueForKey:@"VendorMasterId"]];
//                objMechanicalPOGeneric.vendorLocationId = [NSString stringWithFormat:@"%@",[dictVendorGeneric valueForKey:@"VendorLocationId"]];
//                
//                if(_textFieldPC_Generic.text.length>0)
//                {
//                    objMechanicalPOGeneric.promotionalCode = _textFieldPC_Generic.text;
//                }
//                else
//                {
//                    objMechanicalPOGeneric.promotionalCode = @"";
//                }
//                
//                objMechanicalPOGeneric.workOrderNumber = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"workOrderNo"]];
//                objMechanicalPOGeneric.subWorkOrderNumber = [NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderNo"]];
//                
//                objMechanicalPOGeneric.employeeNumber = [NSString stringWithFormat:@"%@",[dictLoginDetails valueForKey:@"EmployeeNumber"]];
//                
//                objMechanicalPOGeneric.routeNumber = [NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"routeNo"]];
//                objMechanicalPOGeneric.declineMessage = @"";
//                objMechanicalPOGeneric.poType = @"true";
//                
//                objMechanicalPOGeneric.poNotes = strPONotesGeneric;
//                
//                if(_textViewPODescription_Generic.text.length>0)
//                {
//                    objMechanicalPOGeneric.poDescription = _textViewPODescription_Generic.text;
//                }
//                else
//                {
//                    objMechanicalPOGeneric.poDescription = @"";
//                }
//                
//                NSError *error2;
//                [context save:&error2];
//                [global AlertMethod:@"Message" :@"Purchase order saved successfully"];
//                //[self back];
//                [self goToSubWorkOrderDetailView];
            }
            else
            {
                
                NSString *strPC = @"";
                NSString *strPODescription = @"";
                NSArray *arrayBlank = @[];
                
                if(_textFieldPC_Generic.text.length>0)
                {
                    strPC = _textFieldPC_Generic.text;
                }
                if(_textViewPODescription_Generic.text.length>0)
                {
                    strPODescription = _textViewPODescription_Generic.text;
                }
                
                NSDictionary *dictSynchData = @{@"PurchaseOrderId":@"",
                                                @"PurchaseOrderNumber":@"0",
                                                @"PurchaseDate":[self getDate],
                                                @"IsActive":@"true",
                                                @"CompanyId":[NSString stringWithFormat:@"%@",[[[[dictLoginDetails valueForKey:@"Company"] valueForKey:@"CompanyConfig"] valueForKey:@"BillingServiceModule"] valueForKey:@"CompanyId"]],
                                                @"Status":@"0",
                                                @"VendorMasterId":[NSString stringWithFormat:@"%@",[dictVendorGeneric valueForKey:@"VendorMasterId"]],
                                                @"VendorLocationId":[NSString stringWithFormat:@"%@",[dictVendorLocationGeneric valueForKey:@"VendorLocationId"]],
                                                @"PromotionalCode":strPC,
                                                @"WorkOrderNo":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"workOrderNo"]],
                                                @"SubWorkOrderNo":[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderNo"]],
                                                @"EmployeeNo":[NSString stringWithFormat:@"%@",[dictLoginDetails valueForKey:@"EmployeeNumber"]],
                                                @"RouteNo":[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"routeNo"]],
                                                @"DeclineMessage":@"",
                                                @"POType":@"true",
                                                @"PONotes":strPONotesGeneric,
                                                @"PODescription":strPODescription,
                                                @"PurchaseOrderDetailDcs":arrayBlank,
                                                @"CreatedBy":strEmployeeID,
                                                @"ModifiedBy":strEmployeeID
                                                };
                
                NSMutableArray *arrayJson = [NSMutableArray new];
                [arrayJson addObject:dictSynchData];
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait"];

                DELAY(1.0);
                [self postApiCall:arrayJson];
                
                //                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                //
                //
                //                });
            }
        }
    }
    else
    {
        if(arrayPartList.count>0)
        {
            for(NSDictionary *dict in arrayPartList)
            {
                if(!([[dict valueForKey:@"vendorName"] length]>0))
                {
                    [global AlertMethod:@"Message" :@"Please select vendor"];
                    return;
                }
                else if (!([[dict valueForKey:@"quantity"] length]>0))
                {
                    [global AlertMethod:@"Message" :@"Please enter quantity"];
                    return;
                }
            }
            //[self performSegueWithIdentifier:@"PurchaseOrderGroupViewController" sender:nil];
            
            [self goToNextView];
            
        }
        else
        {
            [global AlertMethod:@"Message" :@"Please add part"];
        }
    }
}

- (IBAction)actionOnAddPart:(id)sender
{
    //    [self configureDropDownViewForPopUp];
    
    _viewDropDownSelectPart_AddPart.title = @"Select Part";
    _viewDropDownSelectCategory_AddPart.title = @"Select Category";
    _textFieldQuantity_AddPart.text = @"";
    _buttonTransparent.hidden = NO;
    _viewPopUp_AddPart.hidden = NO;
}

- (IBAction)actionOnTransparentButton:(id)sender
{
    //    _viewPopUp_AddPart.hidden = YES;
    //    _buttonTransparent.hidden = YES;
}

- (IBAction)actionOnSavePart:(id)sender
{
    if([categoryIndexPath isEqualToString:@"not"])
    {// category not selected yet
        [global AlertMethod:@"Message" :@"Please select category"];
    }
    else if ([partIndexPath isEqualToString:@"not"])
    {// part not selected yet
        [global AlertMethod:@"Message" :@"Please select part"];
        
    }
    else if (!(_textFieldQuantity_AddPart.text.length>0))
    {// quantity not entered yet
        
        [global AlertMethod:@"Message" :@"Please enter quantity"];
    }
    else
    {// save part
        
        //ItemMasterId
        itemMasterID = [[[arrayParts objectAtIndex:[partIndexPath intValue]] valueForKey:@"ItemMasterId"] integerValue];
        if(arrayPartList.count>0)
        {
            for(NSDictionary *dict in arrayPartList)
            {
                if([[[dict valueForKey:@"part"] valueForKey:@"ItemMasterId"] integerValue]==itemMasterID)
                {
                    [global AlertMethod:@"Message" :@"Duplicate part can not be added"];
                    return;
                }
            }
        }
        
        NSMutableArray *arrayTempItemVendorMapping = [NSMutableArray new];// is wale array me wo hi object add karenge jisme itemmasterID upar wale master id se match karegi
        NSString *vendorMasterID = @"";
        
        NSMutableArray *arrayTempVendor = [NSMutableArray new];
        NSString *vendorName = @"";// this vendor name will be shown when paramter "isPreferred" is 1 i.e true
        strVendorItemNumber =@"";
        BOOL isPreferredVendorFound = false;
        
        for(NSDictionary *dict in arrayItemVendorMapping)
        {
            if([[dict valueForKey:@"ItemMasterId"] integerValue]==itemMasterID)
            {
                for(NSDictionary *dictTempVendor in arrayVendor)
                {
                    if ([[dict valueForKey:@"VendorMasterId"] integerValue]==[[dictTempVendor valueForKey:@"VendorMasterId"] integerValue])
                    {
                        [arrayTempVendor addObject:dictTempVendor];
                        
                        if([[dict valueForKey:@"IsPreferred"] integerValue]==1)
                        {
                            if(isPreferredVendorFound==NO)
                            {
                                vendorName = [dictTempVendor valueForKey:@"Name"];
                                vendorMasterID = [NSString stringWithFormat:@"%@",[dictTempVendor valueForKey:@"VendorMasterId"]];
                                strVendorItemNumber = [dict valueForKey:@"VendorItemNumber"];
                                isPreferredVendorFound = YES;// we have to break this if "isPreferred" found anytime
                            }
                            
                        }
                    }
                }
                [arrayTempItemVendorMapping addObject:dict];
            }
        }
        
        dictPartAdded = @{@"category":[arrayCategories objectAtIndex:[categoryIndexPath intValue]],
                          @"part":[arrayParts objectAtIndex:[partIndexPath intValue]],
                          @"quantity":_textFieldQuantity_AddPart.text,
                          @"vendor":arrayTempVendor,
                          @"vendorMapping":arrayTempItemVendorMapping,
                          @"vendorName":vendorName,
                          @"vendorItemNumber":strVendorItemNumber,
                          @"vendorMasterID":vendorMasterID
                          };
        
        [arrayPartList addObject:dictPartAdded];
        _viewPopUp_AddPart.hidden = YES;
        _buttonTransparent.hidden = YES;
        [_textFieldQuantity_AddPart resignFirstResponder];
        categoryIndexPath = @"not";
        partIndexPath = @"not";
        
        [_tableViewPartList reloadData];
        
        [_viewStandard setFrame:CGRectMake(10, CGRectGetMaxY(_viewTopContainer.frame)+20, _viewStandard.frame.size.width, 300+arrayPartList.count*256)];
        [_viewContainer updateConstraintsIfNeeded];
        
        _heightMainContentView.constant = 200+_viewTopContainer.frame.size.height+_viewStandard.frame.size.height;
        
        //        _viewDropDownSelectCategory_AddPart.title = @"Select Part";
        //        _viewDropDownSelectCategory_AddPart.title = @"Select Category";
    }
}

- (IBAction)actionOnCancel_addPart:(id)sender
{
    //    // for category list in pop up
    //    _viewDropDownSelectCategory_AddPart.items = arrayCategoriesName;
    //    _viewDropDownSelectCategory_AddPart.itemsFont = [UIFont fontWithName:@"Helvetica-Regular" size:15.0];
    //    _viewDropDownSelectCategory_AddPart.title = @"Select Category";
    //    _viewDropDownSelectCategory_AddPart.titleTextAlignment = NSTextAlignmentCenter;
    //    _viewDropDownSelectCategory_AddPart.delegate = self;
    //
    //
    //    // for parts list in pop up
    //    _viewDropDownSelectPart_AddPart.items = arrayPartsName;
    //    _viewDropDownSelectPart_AddPart.itemsFont = [UIFont fontWithName:@"Helvetica-Regular" size:15.0];
    //    _viewDropDownSelectPart_AddPart.title = @"Select Part";
    //    _viewDropDownSelectPart_AddPart.titleTextAlignment = NSTextAlignmentCenter;
    //    _viewDropDownSelectPart_AddPart.delegate = self;
    //
    //    _textFieldQuantity_AddPart.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    //    _textFieldQuantity_AddPart.layer.borderWidth = 1.0;
    //    _textFieldQuantity_AddPart.layer.cornerRadius = 4.0;
    
    //    [self configureDropDownViewForPopUp];
    //    _viewDropDownSelectCategory_AddPart.title = @"Select Part";
    //    _viewDropDownSelectCategory_AddPart.title = @"Select Category";
    
    
    _viewPopUp_AddPart.hidden = YES;
    _buttonTransparent.hidden = YES;
}

-(void)actionOnDeletePart:(id)sender
{
    UIButton *btn =(UIButton*)sender;
    tagDeleteButtonPart = btn.tag;
    [[[UIAlertView alloc] initWithTitle:@"Message" message:@"Are you sure you want to delete part?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
}

#pragma mark - UIAlertView delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [arrayPartList removeObjectAtIndex:tagDeleteButtonPart];
        
        [_tableViewPartList reloadData];
        
        [_viewStandard setFrame:CGRectMake(10, CGRectGetMaxY(_viewTopContainer.frame)+20, _viewStandard.frame.size.width, 300+arrayPartList.count*256)];
        [_viewContainer updateConstraintsIfNeeded];
        
        _heightMainContentView.constant = 200+_viewTopContainer.frame.size.height+_viewStandard.frame.size.height;
    }
}

#pragma mark - KPDropMenu Delegate Methods

-(void)didSelectItem : (KPDropMenu *) dropMenu atIndex : (int) atIntedex
{
    if(dropMenu==_viewDropDownSelectCategory_AddPart)
    {
        categoryIndexPath = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu==_viewDropDownSelectPart_AddPart)
    {
        partIndexPath = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if(dropMenu == _viewSelectVendor_Generic)
    {
        NSLog(@"%@", dropMenu.items[atIntedex]);
        
        // working for filtering vendor location according to vendor selected
        NSInteger vendorMasterID = [[[arrayVendor objectAtIndex:atIntedex] valueForKey:@"VendorMasterId"] integerValue];
        
        NSMutableArray *arrayTempVendorLocName = [NSMutableArray new];
        for(NSDictionary *dict in arrayVendorLocation)
        {
            if([[dict valueForKey:@"VendorId"] integerValue]==vendorMasterID)
            {
                [arrayTempVendorLocName addObject:[dict valueForKey:@"Name"]];
                [arrayFilteredVendorLocation addObject:dict];
            }
        }
        _viewSelectVendorLocation_Generic.items = arrayTempVendorLocName;
        _viewSelectVendorLocation_Generic.title = @"Select Vendor Location";
        dictVendorLocationGeneric = nil;
        
        dictVendorGeneric = [arrayVendor objectAtIndex:atIntedex];
    }
    else if (dropMenu == _viewSelectVendorLocation_Generic)
    {
        NSLog(@"%@", dropMenu.items[atIntedex]);
        //        dictVendorLocationGeneric = [arrayVendorLocation objectAtIndex:atIntedex];//arrayFilteredVendorLocation
        
        dictVendorLocationGeneric = [arrayFilteredVendorLocation objectAtIndex:atIntedex];
    }
    else
    {
        // part list cell drop down
        NSLog(@"%@", dropMenu.items[atIntedex]);
        NSLog(@"Selected vendor of cell is %@",[arrayPartList objectAtIndex:dropMenu.tag]);
        NSMutableDictionary *dictTemp = [NSMutableDictionary new];
        
        
        dictTemp = [[arrayPartList objectAtIndex:dropMenu.tag] mutableCopy];
        NSInteger vendorMasterId = [[[[[arrayPartList objectAtIndex:dropMenu.tag] valueForKey:@"vendor"] objectAtIndex:atIntedex] valueForKey:@"VendorMasterId"] integerValue];
        NSString *vendorName = [[[[arrayPartList objectAtIndex:dropMenu.tag] valueForKey:@"vendor"] objectAtIndex:atIntedex] valueForKey:@"Name"];
        NSString *strVendorMasterID = [[[[arrayPartList objectAtIndex:dropMenu.tag] valueForKey:@"vendor"] objectAtIndex:atIntedex] valueForKey:@"VendorMasterId"];
        
        
        //        for(NSDictionary*dct in [[arrayPartList objectAtIndex:dropMenu.tag] valueForKey:@"vendorMapping"])
        //        {
        
        
        
        NSDictionary*dct = [[[arrayPartList objectAtIndex:dropMenu.tag] valueForKey:@"vendorMapping"] objectAtIndex:atIntedex];
        if([[dct valueForKey:@"VendorMasterId"] integerValue]==vendorMasterId)
        {
            [dictTemp setValue:[NSString stringWithFormat:@"%@",[dct valueForKey:@"VendorItemNumber"]] forKey:@"vendorItemNumber"];
            [dictTemp setValue:vendorName forKey:@"vendorName"];
            [dictTemp setValue:strVendorMasterID forKey:@"vendorMasterID"];
            //vendorName
            [arrayPartList replaceObjectAtIndex:dropMenu.tag withObject:dictTemp];
            //break;
        }
        // }
        
        
        
        //        for(NSDictionary*dct in [[arrayPartList objectAtIndex:dropMenu.tag] valueForKey:@"vendorMapping"])
        //        {
        //            if([[dct valueForKey:@"VendorMasterId"] integerValue]==vendorMasterId)
        //            {
        //                [dictTemp setValue:[NSString stringWithFormat:@"%@",[dct valueForKey:@"VendorItemNumber"]] forKey:@"vendorItemNumber"];
        //                [dictTemp setValue:vendorName forKey:@"vendorName"];
        //                [dictTemp setValue:strVendorMasterID forKey:@"vendorMasterID"];
        //                //vendorName
        //                [arrayPartList replaceObjectAtIndex:dropMenu.tag withObject:dictTemp];
        //                //break;
        //            }
        //        }
        
        NSLog(@"Object after replace is %@",[arrayPartList objectAtIndex:dropMenu.tag]);
        [_tableViewPartList reloadData];
    }
}

-(void)didShow:(KPDropMenu *)dropMenu
{
    NSLog(@"didShow");
}

-(void)didHide:(KPDropMenu *)dropMenu
{
    NSLog(@"didHide");
}

#pragma mark - textField Delegate Methods

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length==0)
    {
        if([string isEqualToString:@" "])
        {
            return NO;
            
        }
    }
    
    
    if(textField==_textFieldQuantity_AddPart)
    {
        BOOL isNuberOnly=[global isNumberOnly:string :range :20 :(int)textField.text.length :@"0123456789"];
        
        return isNuberOnly;
    }
    if(arrayPartList.count>0)
    {
        if(string.length>0)
        {
            NSMutableDictionary *dict = [[arrayPartList objectAtIndex:textField.tag] mutableCopy];
            
            if(string.length>0)
            {
                if(textField.text.length>0)
                {
                    string = [NSString stringWithFormat:@"%@%@",textField.text,string];
                    [dict setValue:string forKey:@"quantity"];
                }
                
            }
            else
            {
                [dict setValue:@"" forKey:@"quantity"];
            }
            
            [arrayPartList replaceObjectAtIndex:textField.tag withObject:dict];
            [_tableViewPartList reloadData];
            return YES;
        }
        
        return YES;
        
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_textFieldQuantity_AddPart resignFirstResponder];
    return YES;
}

#pragma mark - textView Delegate Methods
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(textView.text.length==0)
    {
        if([text isEqualToString:@" "])
        {
            return NO;
        }
    }
    return YES;
    
}

- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGSize size = CGSizeZero;
    if (text) {
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // working for grouping of same vendor items
    NSMutableArray *arrayTempPartList = [NSMutableArray new];
    NSMutableArray *arrayTempCategory = [NSMutableArray new];
    NSMutableArray *arrayTempPart = [NSMutableArray new];
    NSMutableArray *arrayTempVendorLocation = [NSMutableArray new];
    NSMutableArray *arrayTempQty = [NSMutableArray new];
    NSMutableArray *arrayTempVendorItemNumber = [NSMutableArray new];
    
    BOOL isSameVendorIDFound = NO;
    NSDictionary *dictVendor;
    NSDictionary *dictGroupPO;
    
    // arrayTempPartList = arrayPartList;
    
    for(int i=0;i<arrayPartList.count;i++)
    {
        isSameVendorIDFound = NO;
        NSInteger vendorMasterID = [[[arrayPartList objectAtIndex:i] valueForKey:@"vendorMasterID"] integerValue];
        
        for(NSDictionary*dict in [[arrayPartList objectAtIndex:i] valueForKey:@"vendor"])// take out one object from vendor
        {
            if([[dict valueForKey:@"VendorMasterId"] integerValue]==vendorMasterID)
            {
                dictVendor = dict;
                break;
            }
        }
        
        if(arrayTempPartList.count>0)
        {
            for(NSDictionary *dict in arrayTempPartList)// here we are checking in arrayTempPartList(this array will be passed to next viewcontroller) that same vendor id exist or not,if exist then we will not add that vendor and his item again in same array
            {
                if([[[dict valueForKey:@"vendor"] valueForKey:@"VendorMasterId"] integerValue]==vendorMasterID)
                {
                    isSameVendorIDFound = YES;
                    break;
                }
            }
            
            if(isSameVendorIDFound==NO)// here vendor id not found in arrayTempPartList, so we will work ahead
            {
                for(NSDictionary *dict in arrayVendorLocation)// taking out locations for vendor(vendorMasterID)
                {
                    if([[dict valueForKey:@"VendorId"] integerValue]==vendorMasterID)
                    {
                        [arrayTempVendorLocation addObject:dict];
                    }
                }
                
                for(int j =0; j<arrayPartList.count;j++)
                {
                    if([[[arrayPartList objectAtIndex:j] valueForKey:@"vendorMasterID"] integerValue]==vendorMasterID)
                    {
                        [arrayTempCategory addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"category"]];
                        [arrayTempPart addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"part"]];
                        [arrayTempQty addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"quantity"]];
                        [arrayTempVendorItemNumber addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"vendorItemNumber"]];
                        
                        
                    }
                }
                
                dictGroupPO = @{@"categoryGroup":[arrayTempCategory mutableCopy],
                                @"partGroup":[arrayTempPart mutableCopy],
                                @"quantityGroup":[arrayTempQty mutableCopy],
                                @"vendorItemNumberGroup":[arrayTempVendorItemNumber mutableCopy],
                                @"vendorLocations":[arrayTempVendorLocation mutableCopy],
                                @"vendor":dictVendor,
                                @"promotionalCode":@""
                                };
                
                [arrayTempPartList addObject:dictGroupPO];
                [arrayTempCategory removeAllObjects];
                [arrayTempPart removeAllObjects];
                [arrayTempQty removeAllObjects];
                [arrayTempVendorLocation removeAllObjects];
                [arrayTempVendorItemNumber removeAllObjects];
            }
        }
        else
        {
            for(NSDictionary *dict in arrayVendorLocation)
            {
                if([[dict valueForKey:@"VendorId"] integerValue]==vendorMasterID)
                {
                    [arrayTempVendorLocation addObject:dict];
                }
            }
            
            for(int j =0; j<arrayPartList.count;j++)
            {
                if([[[arrayPartList objectAtIndex:j] valueForKey:@"vendorMasterID"] integerValue]==vendorMasterID)
                {
                    [arrayTempCategory addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"category"]];
                    [arrayTempPart addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"part"]];
                    [arrayTempQty addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"quantity"]];
                    [arrayTempVendorItemNumber addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"vendorItemNumber"]];
                }
            }
            
            dictGroupPO = @{@"categoryGroup":[arrayTempCategory mutableCopy],
                            @"partGroup":[arrayTempPart mutableCopy],
                            @"quantityGroup":[arrayTempQty mutableCopy],
                            @"vendorItemNumberGroup":[arrayTempVendorItemNumber mutableCopy],
                            @"vendorLocations":[arrayTempVendorLocation mutableCopy],
                            @"vendor":dictVendor,
                            @"promotionalCode":@"",
                            @"dropDownLocationTitle":@""
                            };
            
            [arrayTempPartList addObject:dictGroupPO];
            
            [arrayTempCategory removeAllObjects];
            [arrayTempPart removeAllObjects];
            [arrayTempQty removeAllObjects];
            [arrayTempVendorLocation removeAllObjects];
            [arrayTempVendorItemNumber removeAllObjects];
        }
    }
    
    /*
    PurchaseOrderGroupViewController *vc = [segue destinationViewController];
    vc.dictPartList = @{@"groupPartList":arrayTempPartList};
    vc.dictWorkOrderDetails = dictWorkOrderDetails;
    vc.dictSubworkOrderDetails = dictSubWorkOrderDetails;
    vc.dictLoginDetails = dictLoginDetails;
    vc.objWorkOrderDetail = _objWorkOrderDetail;
    vc.objSubWorkOrderDetail = _objSubWorkOrderDetail;
    vc.strWoType=_strWoType;
    
    if(_textViewPODescription_Generic.text.length>0)
    {
        vc.strPONotes = _textViewPODescription_Generic.text;
    }
    else
    {
        vc.strPONotes = @"";
    }
    */
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    PurchaseOrderGroupViewController
    *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"PurchaseOrderGroupViewController"];
    vc.dictPartList = @{@"groupPartList":arrayTempPartList};
    vc.dictWorkOrderDetails = dictWorkOrderDetails;
    vc.dictSubworkOrderDetails = dictSubWorkOrderDetails;
    vc.dictLoginDetails = dictLoginDetails;
    vc.objWorkOrderDetail = _objWorkOrderDetail;
    vc.objSubWorkOrderDetail = _objSubWorkOrderDetail;
    vc.strWoType=_strWoType;
    
    if(_textViewPODescription_Generic.text.length>0)
    {
        vc.strPONotes = _textViewPODescription_Generic.text;
    }
    else
    {
        vc.strPONotes = @"";
    }
    [self presentViewController:vc animated:YES completion:nil];

    
}

-(void)goToNextView{
    // working for grouping of same vendor items
    NSMutableArray *arrayTempPartList = [NSMutableArray new];
    NSMutableArray *arrayTempCategory = [NSMutableArray new];
    NSMutableArray *arrayTempPart = [NSMutableArray new];
    NSMutableArray *arrayTempVendorLocation = [NSMutableArray new];
    NSMutableArray *arrayTempQty = [NSMutableArray new];
    NSMutableArray *arrayTempVendorItemNumber = [NSMutableArray new];
    
    BOOL isSameVendorIDFound = NO;
    NSDictionary *dictVendor;
    NSDictionary *dictGroupPO;
    
    // arrayTempPartList = arrayPartList;
    
    for(int i=0;i<arrayPartList.count;i++)
    {
        isSameVendorIDFound = NO;
        NSInteger vendorMasterID = [[[arrayPartList objectAtIndex:i] valueForKey:@"vendorMasterID"] integerValue];
        
        for(NSDictionary*dict in [[arrayPartList objectAtIndex:i] valueForKey:@"vendor"])// take out one object from vendor
        {
            if([[dict valueForKey:@"VendorMasterId"] integerValue]==vendorMasterID)
            {
                dictVendor = dict;
                break;
            }
        }
        
        if(arrayTempPartList.count>0)
        {
            for(NSDictionary *dict in arrayTempPartList)// here we are checking in arrayTempPartList(this array will be passed to next viewcontroller) that same vendor id exist or not,if exist then we will not add that vendor and his item again in same array
            {
                if([[[dict valueForKey:@"vendor"] valueForKey:@"VendorMasterId"] integerValue]==vendorMasterID)
                {
                    isSameVendorIDFound = YES;
                    break;
                }
            }
            
            if(isSameVendorIDFound==NO)// here vendor id not found in arrayTempPartList, so we will work ahead
            {
                for(NSDictionary *dict in arrayVendorLocation)// taking out locations for vendor(vendorMasterID)
                {
                    if([[dict valueForKey:@"VendorId"] integerValue]==vendorMasterID)
                    {
                        [arrayTempVendorLocation addObject:dict];
                    }
                }
                
                for(int j =0; j<arrayPartList.count;j++)
                {
                    if([[[arrayPartList objectAtIndex:j] valueForKey:@"vendorMasterID"] integerValue]==vendorMasterID)
                    {
                        [arrayTempCategory addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"category"]];
                        [arrayTempPart addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"part"]];
                        [arrayTempQty addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"quantity"]];
                        [arrayTempVendorItemNumber addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"vendorItemNumber"]];
                        
                        
                    }
                }
                
                dictGroupPO = @{@"categoryGroup":[arrayTempCategory mutableCopy],
                                @"partGroup":[arrayTempPart mutableCopy],
                                @"quantityGroup":[arrayTempQty mutableCopy],
                                @"vendorItemNumberGroup":[arrayTempVendorItemNumber mutableCopy],
                                @"vendorLocations":[arrayTempVendorLocation mutableCopy],
                                @"vendor":dictVendor,
                                @"promotionalCode":@""
                                };
                
                [arrayTempPartList addObject:dictGroupPO];
                [arrayTempCategory removeAllObjects];
                [arrayTempPart removeAllObjects];
                [arrayTempQty removeAllObjects];
                [arrayTempVendorLocation removeAllObjects];
                [arrayTempVendorItemNumber removeAllObjects];
            }
        }
        else
        {
            for(NSDictionary *dict in arrayVendorLocation)
            {
                if([[dict valueForKey:@"VendorId"] integerValue]==vendorMasterID)
                {
                    [arrayTempVendorLocation addObject:dict];
                }
            }
            
            for(int j =0; j<arrayPartList.count;j++)
            {
                if([[[arrayPartList objectAtIndex:j] valueForKey:@"vendorMasterID"] integerValue]==vendorMasterID)
                {
                    [arrayTempCategory addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"category"]];
                    [arrayTempPart addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"part"]];
                    [arrayTempQty addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"quantity"]];
                    [arrayTempVendorItemNumber addObject:[[arrayPartList objectAtIndex:j] valueForKey:@"vendorItemNumber"]];
                }
            }
            
            dictGroupPO = @{@"categoryGroup":[arrayTempCategory mutableCopy],
                            @"partGroup":[arrayTempPart mutableCopy],
                            @"quantityGroup":[arrayTempQty mutableCopy],
                            @"vendorItemNumberGroup":[arrayTempVendorItemNumber mutableCopy],
                            @"vendorLocations":[arrayTempVendorLocation mutableCopy],
                            @"vendor":dictVendor,
                            @"promotionalCode":@"",
                            @"dropDownLocationTitle":@""
                            };
            
            [arrayTempPartList addObject:dictGroupPO];
            
            [arrayTempCategory removeAllObjects];
            [arrayTempPart removeAllObjects];
            [arrayTempQty removeAllObjects];
            [arrayTempVendorLocation removeAllObjects];
            [arrayTempVendorItemNumber removeAllObjects];
        }
    }
    
    /*
     PurchaseOrderGroupViewController *vc = [segue destinationViewController];
     vc.dictPartList = @{@"groupPartList":arrayTempPartList};
     vc.dictWorkOrderDetails = dictWorkOrderDetails;
     vc.dictSubworkOrderDetails = dictSubWorkOrderDetails;
     vc.dictLoginDetails = dictLoginDetails;
     vc.objWorkOrderDetail = _objWorkOrderDetail;
     vc.objSubWorkOrderDetail = _objSubWorkOrderDetail;
     vc.strWoType=_strWoType;
     
     if(_textViewPODescription_Generic.text.length>0)
     {
     vc.strPONotes = _textViewPODescription_Generic.text;
     }
     else
     {
     vc.strPONotes = @"";
     }
     */
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    PurchaseOrderGroupViewController
    *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"PurchaseOrderGroupViewController"];
    vc.dictPartList = @{@"groupPartList":arrayTempPartList};
    vc.dictWorkOrderDetails = dictWorkOrderDetails;
    vc.dictSubworkOrderDetails = dictSubWorkOrderDetails;
    vc.dictLoginDetails = dictLoginDetails;
    vc.objWorkOrderDetail = _objWorkOrderDetail;
    vc.objSubWorkOrderDetail = _objSubWorkOrderDetail;
    vc.strWoType=_strWoType;
    
    if(_textViewPODescription_Generic.text.length>0)
    {
        vc.strPONotes = _textViewPODescription_Generic.text;
    }
    else
    {
        vc.strPONotes = @"";
    }
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(void)fetchWorkOrderFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    context = [appDelegate managedObjectContext];
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    //    strWorkOrderId = @"45216";
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [self.fetchedResultsControllerWorkOrderDetails fetchedObjects];
    
    for(NSManagedObject *obj in arrAllObjWorkOrder)
    {
        NSArray *keys = [[[obj entity] attributesByName] allKeys];
        dictWorkOrderDetails = [obj dictionaryWithValuesForKeys:keys];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else
    {
    }
}

-(void)fetchSubWorkOrderFromDB
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    //    strWorkOrderId = @"45216";
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    
    for(NSManagedObject *obj in arrAllObjMechanicalSubWorkOrder)
    {
        NSArray *keys = [[[obj entity] attributesByName] allKeys];
        dictSubWorkOrderDetails = [obj dictionaryWithValuesForKeys:keys];
    }
    
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else
    {
        
    }
}

-(void)fetchSubWorkOrderIssuesRepairPartFromDataBaseForMechanical
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    //    strWorkOrderId = @"45216";
    //    strSubWorkOrderId =@"21094";
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderId];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    
    // get all parts master
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictDetailsMastersMechanical=[defsLead valueForKey:@"MasterServiceAutomation"];
    
    NSArray *arrOfAreaMaster=[dictDetailsMastersMechanical valueForKey:@"InventoryItems"];
    NSMutableArray *arrOfPartsGlobalMasters=[[NSMutableArray alloc]init];
    NSMutableArray *arrayAlreadyAddedPart = [NSMutableArray new];
    ////
    
    for(NSManagedObject *obj in arrAllObjSubWorkOrderIssuesRepairParts)
    {
        NSArray *keys = [[[obj entity] attributesByName] allKeys];
        NSDictionary *dictObj = [obj dictionaryWithValuesForKeys:keys];
        [arrayAlreadyAddedPart addObject:dictObj];
    }
    
    NSMutableArray *arrayCopy = [arrayAlreadyAddedPart mutableCopy];
    [arrayAlreadyAddedPart removeAllObjects];
    
    for(NSDictionary *dictAddedPart in arrayCopy)
    {
        BOOL isFoundParamter = false;
        
        if(arrayAlreadyAddedPart.count>0)
        {
            for(int i =0;i<arrayAlreadyAddedPart.count;i++)
            {
                if([[[arrayAlreadyAddedPart objectAtIndex:i] valueForKey:@"partCode"] isEqualToString:[dictAddedPart valueForKey:@"partCode"]]&&[[[arrayAlreadyAddedPart objectAtIndex:i] valueForKey:@"manufacturer"] isEqualToString:[dictAddedPart valueForKey:@"manufacturer"]]&&[[[arrayAlreadyAddedPart objectAtIndex:i] valueForKey:@"modelNumber"] isEqualToString:[dictAddedPart valueForKey:@"modelNumber"]]&& [[[arrayAlreadyAddedPart objectAtIndex:i] valueForKey:@"serialNumber"] isEqualToString:[dictAddedPart valueForKey:@"serialNumber"]])
                {
                    NSInteger qty1 = [[[arrayAlreadyAddedPart objectAtIndex:i] valueForKey:@"qty"] integerValue];
                    NSInteger qty2 = [[dictAddedPart valueForKey:@"qty"] integerValue];
                    
                    qty1 = qty1 + qty2;
                    
                    NSMutableDictionary *dic = [[arrayAlreadyAddedPart objectAtIndex:i] mutableCopy];
                    
                    [dic setValue:[NSNumber numberWithInteger:qty1] forKey:@"qty"];
                    [arrayAlreadyAddedPart replaceObjectAtIndex:i withObject:dic];
                    isFoundParamter = YES;
                }
                else
                {
                    if(isFoundParamter==NO)
                    {
                        [arrayAlreadyAddedPart addObject:dictAddedPart];
                        isFoundParamter = YES;
                    }
                }
            }
        }
        else
        {
            [arrayAlreadyAddedPart addObject:dictAddedPart];
        }
    }
    
    for(NSDictionary *dictObj in arrayAlreadyAddedPart)
    {
        for(NSDictionary *dict in arrOfAreaMaster)
        {
            if([[dict valueForKey:@"SysName"] isEqualToString:[dictObj valueForKey:@"partCode"]])
            {
                NSMutableDictionary *dictTemp = [dict mutableCopy];
                [dictTemp setValue:[dictObj valueForKey:@"qty"] forKey:@"quantity"];// yaha per jo already part added hai usme se quantity nikal kar masterpart wale object me add kar diya
                [arrOfPartsGlobalMasters addObject:dictTemp];
            }
        }
    }
    
    
    NSLog(@"%@",arrOfPartsGlobalMasters);
    
    //     for(NSDictionary *dictTemp in arrOfPartsGlobalMasters)
    for(int i = 0;i<arrOfPartsGlobalMasters.count;i++)
    {
        NSDictionary *dictTemp = [arrOfPartsGlobalMasters objectAtIndex:i];
        
        itemMasterID = [[dictTemp valueForKey:@"ItemMasterId"] integerValue];
        
        NSMutableArray *arrayTempItemVendorMapping = [NSMutableArray new];// is wale array me wo hi object add karenge jisme itemmasterID upar wale master id se match karegi
        NSString *vendorMasterID = @"";
        
        NSMutableArray *arrayTempVendor = [NSMutableArray new];
        NSString *vendorName = @"";// this vendor name will be shown when paramter "isPreferred" is 1 i.e true
        strVendorItemNumber =@"";
        BOOL isPreferredVendorFound = false;
        //         BOOL sameItemMasterIdFound = false;
        
        //for(int i = 0;i<arrayItemVendorMapping.count;i++)
        for(NSDictionary *dict in arrayItemVendorMapping)
        {
            //             NSDictionary *dict = [arrayItemVendorMapping objectAtIndex:i];
            if([[dict valueForKey:@"ItemMasterId"] integerValue]==itemMasterID)
            {
                for(NSDictionary *dictTempVendor in arrayVendor)
                {
                    if ([[dict valueForKey:@"VendorMasterId"] integerValue]==[[dictTempVendor valueForKey:@"VendorMasterId"] integerValue])
                    {
                        [arrayTempVendor addObject:dictTempVendor];
                        
                        if([[dict valueForKey:@"IsPreferred"] integerValue]==1)
                        {
                            if(isPreferredVendorFound==NO)
                            {
                                vendorName = [dictTempVendor valueForKey:@"Name"];
                                vendorMasterID = [NSString stringWithFormat:@"%@",[dictTempVendor valueForKey:@"VendorMasterId"]];
                                strVendorItemNumber = [dict valueForKey:@"VendorItemNumber"];
                                isPreferredVendorFound = YES;// we have to break this if "isPreferred" found anytime
                            }
                        }
                    }
                }
                [arrayTempItemVendorMapping addObject:dict];
            }
        }
        
        NSDictionary *dictcategory;
        NSDictionary *dictPart;
        
        //         for(NSDictionary *dictMasterCat in arrayCategories)
        //         {
        for(NSDictionary *dictMasterCat in arrayCategories)
        {
            if([[[arrOfPartsGlobalMasters objectAtIndex:i] valueForKey:@"CategorySysName"] integerValue]==[[dictMasterCat valueForKey:@"SysName"] integerValue])
            {
                dictcategory = dictMasterCat;
            }
        }
        
        for(NSDictionary *dictMasterPart in arrayParts)
        {
            if([[[arrOfPartsGlobalMasters objectAtIndex:i] valueForKey:@"ItemMasterId"] integerValue]==[[dictMasterPart valueForKey:@"ItemMasterId"] integerValue])
            {
                dictPart = dictMasterPart;
            }
        }
        //}
        
        
        // if in alreadyaddedpart same itemmasterid exists multiple times then we will add quantity from each object(which contains same itemmasterid)
        //         if(arrayPartList.count>0)
        //         {
        //              NSDictionary *dictGlobalPart = [arrOfPartsGlobalMasters objectAtIndex:i];
        //
        //                 for(int j = 0;j<arrayPartList.count;j++)
        //                 {
        //                     NSMutableDictionary *dictTemp = [[arrayPartList objectAtIndex:j] mutableCopy];
        //
        //                    if([[[dictTemp valueForKey:@"part"] valueForKey:@"ItemMasterId"] integerValue]==[[dictGlobalPart valueForKey:@"ItemMasterId"] integerValue])
        //                    {
        //                        NSInteger qty1 = [[dictTemp valueForKey:@"quantity"] integerValue];
        //                        NSInteger qty2 = [[dictGlobalPart valueForKey:@"quantity"] integerValue];
        //
        //                        qty1 = qty1+qty2;
        //
        //                        [dictTemp setValue:[NSString stringWithFormat:@"%ld",(long)qty1] forKey:@"quantity"];
        //                        [arrayPartList replaceObjectAtIndex:j withObject:dictTemp];
        //                        sameItemMasterIdFound = true;
        //                    }
        //               }
        //         }
        //
        //         if(sameItemMasterIdFound==false)
        //         {
        dictPartAdded = @{@"category":dictcategory,
                          @"part":dictPart,
                          @"quantity":[NSString stringWithFormat:@"%@",[[arrOfPartsGlobalMasters objectAtIndex:i] valueForKey:@"quantity"]],
                          @"vendor":arrayTempVendor,
                          @"vendorMapping":arrayTempItemVendorMapping,
                          @"vendorName":vendorName,
                          @"vendorItemNumber":strVendorItemNumber,
                          @"vendorMasterID":vendorMasterID
                          };
        
        [arrayPartList addObject:dictPartAdded];
        //         }
    }
    
    
    
    
    //    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    //    {
    //        arrOfSubWorkServiceIssuesRepairParts=nil;
    //        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
    //    }
    //    else
    //    {
    //        arrOfSubWorkServiceIssuesRepairParts=nil;
    //        arrOfSubWorkServiceIssuesRepairParts=[[NSMutableArray alloc]init];
    //
    //        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
    //
    //            matchesSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
    //
    ////            NSString *strIsAddedAfterApproval=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepairParts valueForKey:@"isAddedAfterApproval"]];
    ////
    ////            if ([strIsAddedAfterApproval isEqualToString:@"true"] || [strIsAddedAfterApproval isEqualToString:@"1"])
    ////            {
    ////
    ////            }
    ////            else
    ////            {
    ////                [arrOfSubWorkServiceIssuesRepairParts addObject:matchesSubWorkOrderIssuesRepairParts];
    ////            }
    //        }
    //    }
    //
    //    if (error) {
    //        NSLog(@"Unable to execute fetch request.");
    //        NSLog(@"%@, %@", error, error.localizedDescription);
    //    } else {
    //    }
    
}

-(void)callApiToSynchGenericPurchaseOrder:(NSArray*)arrayJson
{
    ///api/MobileToSaleAuto/UpdateLeadDetail
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [DejalBezelActivityView removeView];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:arrayJson])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrayJson options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Final Mechanical  JSON: %@", jsonString);
            }
        }
        
        NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMainServiceAutomation,UrlMechanicalToSubmitPurchaseOrder,[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"companyKey"]]];
        
        //        NSDictionary *dictTemp;
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please wait..."];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [global getServerResponseForUrlPostMethod:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error) {
                    
                    [DejalBezelActivityView removeView];
                    if(success)
                    {
                        NSLog(@"%@",response);
                    }
                    else
                    {
                        [global AlertMethod:@"Alert!" :@"something went wrong"];
                    }
                }];
            });
        });
    }
}

#pragma mark - get date
-(NSString *)getDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    NSString *string = [formatter stringFromDate:[NSDate date]];
    return string;
}

-(void)postApiCall:(NSMutableArray *)jsonArray
{
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMainServiceAutomation,UrlMechanicalToSubmitPurchaseOrder,[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"companyKey"]]];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonArray options:NSJSONWritingPrettyPrinted error:Nil];
    
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonDatastr %@",str);
    NSURL * serviceUrl = [NSURL URLWithString:strUrl];
    NSLog(@"REquest URL >> %@",serviceUrl);
    NSLog(@"REquest XML >> %@",str);
    
    NSMutableURLRequest * serviceRequest = [NSMutableURLRequest requestWithURL:serviceUrl];
    
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serviceRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [serviceRequest setHTTPMethod:@"POST"];
    [serviceRequest setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *__block serviceResponse;
    NSError *__block serviceError;
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&serviceResponse error:&serviceError];
        
        [DejalBezelActivityView removeView];
      
        
        if (responseData)
        {
            [self parsePostApiData:responseData];
        }
        else
        {
            //        AlertViewClass *a = [[AlertViewClass alloc] init];
            //        [a showMessage:@"Cannot connect to internet." title:@"Skillgrok"];
        }
        
    });
}

-(void)parsePostApiData:(NSData *)response
{
    id jsonObject = Nil;
    
    NSString *charlieSendString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    NSLog(@"ResponseString %@",charlieSendString);
    
    if (response==nil)
    {
        NSLog(@"No internet connection.");
    }
    else
    {
        NSError *error = Nil;
        jsonObject =[NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            NSLog(@"Probably An Array %@",jsonObject);
            
            if([[[jsonObject objectAtIndex:0] valueForKey:@"Value"] boolValue]==1)
            {
                if(isGenericPO)
                {
                    [self deleteGenericPurchaseOrderFromLocalDB];
                }
                NSString *strMsg = [[jsonObject objectAtIndex:1] valueForKey:@"Value"];
                NSArray *arrayMsg = [strMsg componentsSeparatedByString:@"order"];
                NSString *strPONumbers = [NSString stringWithFormat:@"%@",[[jsonObject objectAtIndex:2] valueForKey:@"Value"]];
                
                NSString *strCompleteMsg = [NSString stringWithFormat:@"%@ order# %@ %@",[arrayMsg objectAtIndex:0],strPONumbers,[arrayMsg objectAtIndex:1]];
                NSLog(@"%@",strCompleteMsg);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [global AlertMethod:@"Message" :strCompleteMsg];
                    
                    [self goToSubWorkOrderDetailView];
                    
                });
                //[self back];
                //                _viewSelectVendor_Generic.title = @"Select Vendor";
                //                _viewSelectVendorLocation_Generic.title = @"Select Vendor Location";
            }
            else
            {
                [global AlertMethod:@"Message" :[[jsonObject objectAtIndex:1] valueForKey:@"Value"]];
            }
        }
        else
        {
            NSLog(@"Probably A Dictionary");
            NSDictionary *jsonDictionary=(NSDictionary *)jsonObject;
            NSLog(@"jsonDictionary %@",[jsonDictionary description]);
        }
    }
}

-(void)deleteGenericPurchaseOrderFromLocalDB
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityMechanicalPurchaseOrderGeneric=[NSEntityDescription entityForName:@"MechanicalPurchaseOrderGeneric" inManagedObjectContext:context];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalPurchaseOrderGeneric" inManagedObjectContext:context]];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && issueRepairPartId = %@",_strWorkOrderId,_strSubWorkOrderId,_strIssuePartsIdToFetch,strID];
    //    [allData setPredicate:predicate];
    
    //    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

- (IBAction)actionOnBack:(id)sender
{
    
    [self goToSubWorkOrderDetailView];
    
    //    //[self.navigationController popViewControllerAnimated:YES];
    //    if ([_strWoType isEqualToString:@"TM"]) {
    //
    ////        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
    ////                                                                 bundle: nil];
    ////        MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController
    ////        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController"];
    ////        objByProductVC.strSubWorkOrderId=strSubWorkOrderId;
    ////        objByProductVC.strWoType=_strWoType;
    ////        objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
    ////        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
    ////        [self.navigationController pushViewController:objByProductVC animated:NO];
    //
    //
    //        int index = 0;
    //        NSArray *arrstack=self.navigationController.viewControllers;
    //        for (int k1=0; k1<arrstack.count; k1++) {
    //            if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController class]]) {
    //                index=k1;
    //                //break;
    //            }
    //        }
    //        MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController *myController = (MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController *)[self.navigationController.viewControllers objectAtIndex:index];
    //        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    //        [self.navigationController popToViewController:myController animated:NO];
    //
    //
    //    } else {
    //
    ////        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
    ////                                                                 bundle: nil];
    ////        MechanicalSubWorkOrderDetailsViewControlleriPad
    ////        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControlleriPad"];
    ////        objByProductVC.strSubWorkOrderId=strSubWorkOrderId;
    ////        objByProductVC.strWoType=_strWoType;
    ////        objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
    ////        objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
    ////        [self.navigationController pushViewController:objByProductVC animated:NO];
    //
    //
    //        int index = 0;
    //        NSArray *arrstack=self.navigationController.viewControllers;
    //        for (int k1=0; k1<arrstack.count; k1++) {
    //            if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControlleriPad class]]) {
    //                index=k1;
    //                //break;
    //            }
    //        }
    //        MechanicalSubWorkOrderDetailsViewControlleriPad *myController = (MechanicalSubWorkOrderDetailsViewControlleriPad *)[self.navigationController.viewControllers objectAtIndex:index];
    //        // myController.typeFromBack=_lbl_LeadInfo_Status.text;
    //        [self.navigationController popToViewController:myController animated:NO];
    //
    //    }
    
}

- (IBAction)action_PoHistory:(id)sender {
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
    PoHistoryViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"PoHistoryViewController"];
    objSignViewController.strWorkOrderNo=[NSString stringWithFormat:@"%@",[dictWorkOrderDetails valueForKey:@"workOrderNo"]];
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];

}

-(void)back
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    /*
    
    if ([_strFromWhere isEqualToString:@"Start"]) {
        
        if ([_strWoType isEqualToString:@"TM"]) {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalStartRepairTM class]]) {
                    index=k1;
                    //break;
                }
            }
            MechanicalStartRepairTM *myController = (MechanicalStartRepairTM *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popToViewController:myController animated:NO];
            });
            
            
        } else {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++)
            {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalStartRepairViewController class]]) {
                    index=k1;
                    //break;
                }
            }
            MechanicalStartRepairViewController *myController = (MechanicalStartRepairViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popToViewController:myController animated:NO];
            });
        }

        
    } else {
        
        if ([_strWoType isEqualToString:@"TM"]) {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController class]]) {
                    index=k1;
                    //break;
                }
            }
            MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController *myController = (MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popToViewController:myController animated:NO];
            });
            
            
        } else {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++)
            {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[MechanicalSubWorkOrderDetailsViewControlleriPad class]]) {
                    index=k1;
                    //break;
                }
            }
            MechanicalSubWorkOrderDetailsViewControlleriPad *myController = (MechanicalSubWorkOrderDetailsViewControlleriPad *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popToViewController:myController animated:NO];
            });
        }
        
    }
     
     */
    
}


-(void)goToSubWorkOrderDetailView{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    /*
    if ([_strFromWhere isEqualToString:@"Start"]) {
        
        NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
        [defss setInteger:-1 forKey:@"sectionToOpen"];
        [defss synchronize];
        
        if ([_strWoType isEqualToString:@"TM"]) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                     bundle: nil];
            MechanicalStartRepairTM
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairTM"];
            objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]];
            objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
            objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
            objByProductVC.strWoType=_strWoType;
            
            NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
            [defsId setValue:strSubWorkOrderId forKey:@"SubWorkOrderId"];
            [defsId synchronize];
            
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
            
        } else {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                     bundle: nil];
            MechanicalStartRepairViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalStartRepairViewController"];
            objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]];
            objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
            objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
            objByProductVC.strWoType=_strWoType;
            
            NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
            [defsId setValue:strSubWorkOrderId forKey:@"SubWorkOrderId"];
            [defsId synchronize];
            
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }
        
    } else {
        
        NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
        [defss setInteger:-1 forKey:@"sectionToOpen"];
        [defss synchronize];
        
        if ([_strWoType isEqualToString:@"TM"]) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                     bundle: nil];
            MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController"];
            objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]];
            objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
            objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
            objByProductVC.strWoType=_strWoType;
            
            NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
            [defsId setValue:strSubWorkOrderId forKey:@"SubWorkOrderId"];
            [defsId synchronize];
            
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
            
        } else {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                     bundle: nil];
            MechanicalSubWorkOrderDetailsViewControlleriPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MechanicalSubWorkOrderDetailsViewControlleriPad"];
            objByProductVC.strSubWorkOrderId=[NSString stringWithFormat:@"%@",[dictSubWorkOrderDetails valueForKey:@"subWorkOrderId"]];
            objByProductVC.objWorkOrderdetails=_objWorkOrderDetail;
            objByProductVC.objSubWorkOrderdetails=_objSubWorkOrderDetail;
            objByProductVC.strWoType=_strWoType;
            
            NSUserDefaults *defsId=[NSUserDefaults standardUserDefaults];
            [defsId setValue:strSubWorkOrderId forKey:@"SubWorkOrderId"];
            [defsId synchronize];
            
            [self.navigationController pushViewController:objByProductVC animated:NO];
            
        }
        
    }
     
     */
    
    
}


@end
