//
//  PurchaseOrderGroupViewController.h
//  DPSsdsdafsdf
//
//  Created by Akshay Hastekar on 09/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPDropMenu.h"
#import "ViewVendor.h"
#import "AllImportsViewController.h"

@interface PurchaseOrderGroupViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,KPDropMenuDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableViewPurchaseOrder;

@property (strong, nonatomic) NSDictionary *dictPartList;
@property (strong, nonatomic) NSDictionary *dictWorkOrderDetails;
@property (strong, nonatomic) NSDictionary *dictSubworkOrderDetails;
@property (strong, nonatomic) NSDictionary *dictLoginDetails;
@property (strong, nonatomic) NSString *strPONotes;
@property (strong, nonatomic) NSString *strSubWorkOrderId;
@property (strong, nonatomic) NSString *strWorkOrderId;
@property (strong, nonatomic) NSString *strWoType;
@property (strong, nonatomic) NSManagedObject *objWorkOrderDetail;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderDetail;
- (IBAction)action_PoHistory:(id)sender;
@property (strong, nonatomic) NSString *strFromWhere;


@end
