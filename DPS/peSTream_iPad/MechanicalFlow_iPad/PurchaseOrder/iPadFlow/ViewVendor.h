//
//  ViewVendor.h
//  DPS
//
//  Created by Akshay Hastekar on 10/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPDropMenu.h"
@interface ViewVendor : UIView
@property (weak, nonatomic) IBOutlet UILabel *labelVendor;
@property (weak, nonatomic) IBOutlet KPDropMenu *viewDropDownVendorLocation;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPromotionalCode;

@end
