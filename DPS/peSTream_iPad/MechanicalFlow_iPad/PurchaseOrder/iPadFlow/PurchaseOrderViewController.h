//
//  PurchaseOrderViewController.h
//  DPS
//
//  Created by Akshay Hastekar on 07/02/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPDropMenu.h"
#import "AllImportsViewController.h"

@interface PurchaseOrderViewController : UIViewController<KPDropMenuDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *labelWorkOrder;
@property (weak, nonatomic) IBOutlet UILabel *labelSubWorkOrder;
@property (weak, nonatomic) IBOutlet UILabel *labelAccount;
@property (weak, nonatomic) IBOutlet UILabel *labelCustomer;
@property (weak, nonatomic) IBOutlet UILabel *labelEmailID;
@property (weak, nonatomic) IBOutlet UILabel *labelPhone;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonGeneric;
@property (weak, nonatomic) IBOutlet UIButton *buttonStandard;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPC_Generic;
@property (weak, nonatomic) IBOutlet UITextView *textViewPODescription_Generic;
@property (weak, nonatomic) IBOutlet KPDropMenu *viewSelectVendor_Generic;
@property (weak, nonatomic) IBOutlet KPDropMenu *viewSelectVendorLocation_Generic;
@property (weak, nonatomic) IBOutlet UIView *viewGeneric;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewContainer;

@property (weak, nonatomic) IBOutlet UIView *viewStandard;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTopContainerView;
@property (weak, nonatomic) IBOutlet UIView *viewTopContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightMainContentView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewPartList;
@property (weak, nonatomic) IBOutlet UITextView *textViewPONotes_Standard;

@property (weak, nonatomic) IBOutlet UIButton *buttonTransparent;

////////// pop up view add part
@property (weak, nonatomic) IBOutlet UIView *viewPopUp_AddPart;
@property (weak, nonatomic) IBOutlet KPDropMenu *viewDropDownSelectCategory_AddPart;
@property (weak, nonatomic) IBOutlet KPDropMenu *viewDropDownSelectPart_AddPart;
@property (weak, nonatomic) IBOutlet UITextField *textFieldQuantity_AddPart;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightStandardView;

@property (weak, nonatomic) IBOutlet UIView *viewMainContainer;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairParts;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;//
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalSubWorkOrder;
@property (strong, nonatomic) NSString *strSubWorkOrderId;
@property (strong, nonatomic) NSString *strWorkOrderId;
@property (strong, nonatomic) NSString *strWoType;
@property (strong, nonatomic) NSManagedObject *objWorkOrderDetail;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderDetail;

- (IBAction)actionOnBack:(id)sender;
- (IBAction)action_PoHistory:(id)sender;
@property (strong, nonatomic) NSString *strFromWhere;

@end
