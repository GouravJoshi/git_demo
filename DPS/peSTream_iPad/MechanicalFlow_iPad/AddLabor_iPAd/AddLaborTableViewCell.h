//
//  AddLaborTableViewCell.h
//  DPS 
//  peSTream  peSTream  dsfgdsfgdsfg
//  Created by Saavan Patidar on 03/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  

#import <UIKit/UIKit.h>

@interface AddLaborTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblLaborType;
@property (strong, nonatomic) IBOutlet UILabel *lblLR;
@property (strong, nonatomic) IBOutlet UILabel *lblAmt;

@end
