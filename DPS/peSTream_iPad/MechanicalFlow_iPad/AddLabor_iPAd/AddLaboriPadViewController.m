//
//  AddLaboriPadViewController.m
//  DPS
//  peSTream
//  Created by Saavan Patidar on 03/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "AddLaboriPadViewController.h"

@interface AddLaboriPadViewController ()
{
    Global *global;
    NSString *strEmpID,*strUserName,*strCompanyKey,*strEmpName,*strDepartMentSysName,*strWoStatus,*strDate,*strWorkOrderAccNo,*strWorkOrderAddressSubType,*strWorkOrderAddressId, *strAfterHrsDuration,*strCompanyName;
    UIView *viewBackGround,*viewBackGroundTime;
    NSMutableArray *arrOfSubWorkServiceIssuesRepairLabour,*arrOfHoursConfig;
    BOOL isLaborType, isWarranty, isEditLabor, isStandardSubWorkOrder, isHoliday;
    NSManagedObject *objLaborEdit;
    UIDatePicker *pickerDate;
    UIView *viewForDate,*viewForDateTime;

}
@end

@implementation AddLaboriPadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self methodAllocation];
    
    [self methodBorderColor];
    
    [self methodLoadValues];
    
    [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical];
    
    [self getHoursConfiFromMaster];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//============================================================================
//============================================================================
#pragma mark- ----------------Button Action Methods----------------
//============================================================================
//============================================================================


- (IBAction)action_Back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)action_AddLabor:(id)sender {
    
    //check_box_1.png
    isLaborType=YES;
    isEditLabor=NO;
    _txt_LaborHrs.placeholder=@"Enter Time(HH:mm)";
    [_txt_LaborAmt setEnabled:NO];
    _txtView_LaborDesc.text=@"";
    _txt_LaborHrs.text=@"";
    strDate=@"";
    [_btnSelectTime setTitle:@"Enter Time(HH:MM)" forState:UIControlStateNormal];
    _txtFld_LaborTime.text=@"";
    _txt_LaborAmt.text=@"0.00";
    [_btnRadioLabor setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    isWarranty=NO;
    [_btnIsWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];

    [self addLaborView];
    
}
- (IBAction)action_RadioLabor:(id)sender {
    
    isLaborType=YES;
    _txt_LaborHrs.placeholder=@"Enter Time(HH:mm)";
    [_txt_LaborAmt setEnabled:NO];
    _txtView_LaborDesc.text=@"";
    _txt_LaborAmt.text=@"0.00";
    _txt_LaborHrs.text=@"";
    strDate=@"";
    [_btnSelectTime setTitle:@"Enter Time(HH:MM)" forState:UIControlStateNormal];
    _txtFld_LaborTime.text=@"";
    [_btnRadioLabor setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

}
- (IBAction)action_RadioHelper:(id)sender {
    
    isLaborType=NO;
    _txt_LaborHrs.placeholder=@"Enter Time(HH:mm)";
    [_txt_LaborAmt setEnabled:NO];
    _txtView_LaborDesc.text=@"";
    _txt_LaborAmt.text=@"0.00";
    [_btnRadioHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioLabor setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

}

- (IBAction)action_IsWarranty:(id)sender {
    
    strDate = _txtFld_LaborTime.text;
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"check_box_1.png"];
    
    UIImage *img = [_btnIsWarranty imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        isWarranty=YES;
        [_btnIsWarranty setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
        [self methodToCalculateLaborPrice:strDate];
        
    }else{
        
        isWarranty=NO;
        [_btnIsWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
        [self methodToCalculateLaborPrice:strDate];
    }
    
}

- (IBAction)action_SaveLabor:(id)sender {
    
    [self.view endEditing:YES];
    
    if (_txtFld_LaborTime.text.length>=5) {
        
        NSArray *arrTime=[_txtFld_LaborTime.text componentsSeparatedByString:@":"];
        
        NSString *strHrs,*strMinutes;
        
        if (arrTime.count==1) {
            
            strHrs=arrTime[0];
            strMinutes=@"";
            
        }
        
        if (arrTime.count==2) {
            
            strHrs=arrTime[0];
            strMinutes=arrTime[1];
            
        }
        
        int strMinutesInt=[strMinutes intValue];
        
        if (strMinutesInt>59) {
            
            [global AlertMethod:Alert :@"Please provide labor minutes less then 60"];
            
            
        } else {
            
            if (isEditLabor) {
                
                [self updateLabor];
                
            } else {
                
                [self addServiceIssuesRepairLabourFromMobileToDB];
                
            }
            
            [_viewAddLabor removeFromSuperview];
            [viewBackGround removeFromSuperview];
            
            [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:_strWorkOrderId];

        }
        
    }
    
}

- (IBAction)action_ClearLabor:(id)sender {
    
    isLaborType=YES;
    _txt_LaborHrs.placeholder=@"Enter Time(HH:mm)";
    [_txt_LaborAmt setEnabled:NO];
    _txtView_LaborDesc.text=@"";
    _txt_LaborAmt.text=@"0.00";
    _txt_LaborHrs.text=@"";
    strDate=@"";
    [_btnSelectTime setTitle:@"Enter Time(HH:MM)" forState:UIControlStateNormal];
    _txtFld_LaborTime.text=@"";
    [_btnRadioLabor setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnRadioHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    isWarranty=NO;
    [_btnIsWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];

    
}
- (IBAction)action_CancelLabor:(id)sender {
    
    [_viewAddLabor removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
}


//============================================================================
//============================================================================
#pragma mark- ----------------Void Methods----------------
//============================================================================
//============================================================================

-(void)methodLoadValues{
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpName        =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strWoStatus       =[defsLead valueForKey:@"WoStatus"];
    
    NSString *strIsActive=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isStdPrice"]];
    
    if ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) {
        
        isStandardSubWorkOrder=YES;
        
    } else {
        
        isStandardSubWorkOrder=NO;
        
    }

    _lblAccNoDetails.text=[NSString stringWithFormat:@"%@, Sub Work Order #: %@",[defsLead valueForKey:@"lblName"],[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"subWorkOrderNo"]]];

    strDepartMentSysName=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"departmentSysName"]];

    
    _objWorkOrderdetails=[global fetchMechanicalWorkOrderObj:_strWorkOrderId];
    
    //
    
    strWorkOrderAccNo=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"accountNo"]];
    
    strWorkOrderAddressId=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"serviceAddressId"]];
    
    strWorkOrderAddressSubType=[NSString stringWithFormat:@"%@",[_objWorkOrderdetails valueForKey:@"addressSubType"]];

    //isHoliday
    
    NSString *strIsHoliday=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"isHolidayHrs"]];
    
    if ([strIsHoliday isEqualToString:@"true"] || [strIsHoliday isEqualToString:@"1"]) {
        
        isHoliday=YES;
        
    } else {
        
        isHoliday=NO;
        
    }
    
    //AfterHrsDuration
    
    strAfterHrsDuration=[NSString stringWithFormat:@"%@",[_objSubWorkOrderdetails valueForKey:@"afterHrsDuration"]];

}

-(void)methodBorderColor{
    
    _txtView_LaborDesc.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_LaborDesc.layer.borderWidth=1.0;
    _txtView_LaborDesc.layer.cornerRadius=5.0;
    
}

-(void)methodAllocation{
    
    [_txt_LaborAmt setEnabled:NO];
    isWarranty=NO;
    global = [[Global alloc] init];
    arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
}

-(void)addLaborView{
    
    viewBackGround.tag=201;
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //Adding ViewAddLabor
    
    CGRect frameFor_ViewAddLabor=CGRectMake(0, 105, [UIScreen mainScreen].bounds.size.width,_viewAddLabor.frame.size.height);
    [_viewAddLabor setFrame:frameFor_ViewAddLabor];
    
    [viewBackGround addSubview:_viewAddLabor];

}

-(void)getHoursConfiFromMaster{
    
    arrOfHoursConfig=nil;
    arrOfHoursConfig=[[NSMutableArray alloc]init];
    arrOfHoursConfig=[global getHoursConfiFromMaster:strCompanyKey :strDepartMentSysName :strWorkOrderAccNo :strWorkOrderAddressId :strWorkOrderAddressSubType];
    
}


-(void)methodToCalculateLaborPrice :(NSString *)strTimeEntered{
    
    if (strTimeEntered.length==5) {
        
        NSDictionary *dictDataAfterHourRateToBeUsed=[[NSDictionary alloc]init];
        
        if (!isWarranty) {
            
            if (isLaborType) {
                
                //Logic For Price Calculation
                
                float hrsTime=[global ChangeTimeMechanicalLabor:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        // Change for holiday and After hours change
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                            
                        } else {
                            
                            // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                            
                            if (strAfterHrsDuration.length==0) {
                                
                                strStdPrice=0.0;
                                
                            } else {

                            NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                            
                            NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                            NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                            
                            strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                            strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                            
                            
                            NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                            
                            for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                
                                NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                
                                NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                
                                if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                    
                                    dictDataAfterHourRateToBeUsed=dictDataAHC;
                                    break;
                                    
                                }
                                
                            }
                            
                            strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborPrice"] floatValue];
                            
                            }
                            
                        }
                        
                        
                    }

                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;
                
                _txt_LaborAmt.text=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
                
            } else {
                
                //Logic For Price Calculation
                
                float hrsTime=[global ChangeTimeMechanicalLabor:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {

                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];
                            
                        } else {
                            
                            if ((dictDataAfterHourRateToBeUsed==nil) || ([dictDataAfterHourRateToBeUsed isKindOfClass:[NSNull class]])) {
                                
                                
                                
                            } else {
                                
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                                
                            }
                            
                            strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                            
                        }
                        
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;

                _txt_LaborAmt.text=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
            }
            
        }else{
            
            // Is Warranty Checked Hai
            
            if (isLaborType) {
                
                //Logic For Price Calculation
                
                float hrsTime=[global ChangeTimeMechanicalLabor:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {

                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"WrntyStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        // Change for holiday and After hours change
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborWrntyPrice"] floatValue];
                            
                        } else {
                            
                            // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                            
                            if (strAfterHrsDuration.length==0) {
                                
                                strStdPrice=0.0;
                                
                            } else {

                            NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                            
                            NSString *strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                            NSString *strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                            
                            strFromTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                            strToTimeAHR=[global ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                            
                            
                            NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                            
                            for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                
                                NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                
                                NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                
                                if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                    
                                    dictDataAfterHourRateToBeUsed=dictDataAHC;
                                    break;
                                    
                                }
                                
                            }
                            
                            
                            strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborWrntyPrice"] floatValue];
                                
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;

                _txt_LaborAmt.text=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
                
            } else {
                
                //Logic For Price Calculation
                
                float hrsTime=[global ChangeTimeMechanicalLabor:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {

                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HelperWrntyStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayHelperWrntyPrice"] floatValue];
                            
                        } else {
                            
                            strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperWrntyPrice"] floatValue];
                            
                        }
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[global roundedOffValuess:hrsTime]*strStdPrice;

                _txt_LaborAmt.text=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
            }
            
            
        }
        
    }else{
        
        _txt_LaborAmt.text=@"";
        
    }

}


-(void)setValuesOnEditingLabor{
    
    NSString *strLaborTypeEdit=[NSString stringWithFormat:@"%@",[objLaborEdit valueForKey:@"laborType"]];
    if ([strLaborTypeEdit isEqualToString:@"L"]) {
        
        isLaborType=YES;
        _lblLaborType.text=@"Labor";
        [_btnRadioLabor setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioHelper setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];

    } else {
        
        isLaborType=NO;
        _lblLaborType.text=@"Helper";
        [_btnRadioHelper setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioLabor setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strLaborWarrantyEdit=[NSString stringWithFormat:@"%@",[objLaborEdit valueForKey:@"isWarranty"]];
    if ([strLaborWarrantyEdit isEqualToString:@"true"] || [strLaborWarrantyEdit isEqualToString:@"1"]) {
        
        isWarranty=YES;

        [_btnIsWarranty setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    } else {
        
        isWarranty=NO;
        
        [_btnIsWarranty setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    _txt_LaborHrs.text=[global ChangeTimeToHrnMin:[NSString stringWithFormat:@"%@",[objLaborEdit valueForKey:@"laborHours"]]];
    
    strDate=[global ChangeTimeToHrnMin:[NSString stringWithFormat:@"%@",[objLaborEdit valueForKey:@"laborHours"]]];
    
    [_btnSelectTime setTitle:[global ChangeTimeToHrnMin:[NSString stringWithFormat:@"%@",[objLaborEdit valueForKey:@"laborHours"]]] forState:UIControlStateNormal];
    
    _txtFld_LaborTime.text = [global ChangeTimeToHrnMin:[NSString stringWithFormat:@"%@",[objLaborEdit valueForKey:@"laborHours"]]];
    
    float laborCosttt=[[NSString stringWithFormat:@"%@",[objLaborEdit valueForKey:@"laborCost"]] floatValue];
    
    _txt_LaborAmt.text=[NSString stringWithFormat:@"%.02f",laborCosttt];
    
    NSString *strDescription=[NSString stringWithFormat:@"%@",[objLaborEdit valueForKey:@"laborDescription"]];
    
    if ([strDescription isEqualToString:@"(null)"]) {
        
        _txtView_LaborDesc.text=@"";
        
    } else {
        
        _txtView_LaborDesc.text=[NSString stringWithFormat:@"%@",[objLaborEdit valueForKey:@"laborDescription"]];
        
    }
    
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return arrOfSubWorkServiceIssuesRepairLabour.count;
        
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return tableView.rowHeight;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   AddLaborTableViewCell *cell = (AddLaborTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AddLaborTableViewCell" forIndexPath:indexPath];
   [cell setAccessoryType:UITableViewCellAccessoryNone];
   // Configure Table View Cell
   [self configureCell:cell atIndexPath:indexPath];
   
   return cell;
        
}

- (void)configureCell:(AddLaborTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *dictData =arrOfSubWorkServiceIssuesRepairLabour[indexPath.row];
    
    cell.lblLR.text=[global ChangeTimeToHrnMin:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"laborHours"]]];
    
    float totalAmount=[[NSString stringWithFormat:@"%@",[dictData valueForKey:@"laborCost"]] floatValue];
    
    cell.lblAmt.text=[NSString stringWithFormat:@"%.02f",totalAmount];
    
    NSString *strType=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"laborType"]];
    
    if ([strType isEqualToString:@"L"]) {
        
        cell.lblLaborType.text=@"Labor";

    } else {

        cell.lblLaborType.text=@"Helper";

    }
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BOOL isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWoStatus];
    
    if (isCompletedStatusMechanical) {
        
        return false;
        
    }else
        
        return true;
}

-(NSArray*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    UITableViewRowAction *rowActionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"EDIT" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                           {
                                               isEditLabor=YES;
                                               
                                               [self addLaborView];
                                               
                                               NSManagedObject *dictData =arrOfSubWorkServiceIssuesRepairLabour[indexPath.row];
                                               
                                               [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanicalViaId:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"issueRepairLaborId"]]];
                                               
                                               [self setValuesOnEditingLabor];
                                               
                                           }];
    UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"DELETE" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                             {
                                                 
                                                 UIAlertController * alert=   [UIAlertController
                                                                               alertControllerWithTitle:@"Alert!"
                                                                               message:@"Are you sure you want to delete"
                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                 
                                                 UIAlertAction* ok = [UIAlertAction
                                                                      actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action)
                                                                      {
                                                                          //Code
                                                                          
                                                                          NSManagedObject *dictData =arrOfSubWorkServiceIssuesRepairLabour[indexPath.row];
                                                                          
                                                                          [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanicalViaId:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"issueRepairLaborId"]]];
                                                                          
                                                                          [self updateLaborToDelete];
                                                                          
                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                          
                                                                      }];
                                                 UIAlertAction* cancel = [UIAlertAction
                                                                          actionWithTitle:@"Cancel"
                                                                          style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action)
                                                                          {
                                                                              //Code
                                                                              
                                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                                              
                                                                          }];
                                                 
                                                 [alert addAction:ok];
                                                 [alert addAction:cancel];
                                                 
                                                 [self presentViewController:alert animated:YES completion:nil];

                                                 
                                             }];
    
    rowActionDelete.backgroundColor=[UIColor redColor];
    rowActionEdit.backgroundColor=[UIColor grayColor];
    return @[rowActionEdit];
    // return @[rowActionDelete];
}


//============================================================================
//============================================================================
#pragma mark- ----------------Core Data Fetch Methods----------------
//============================================================================
//============================================================================

-(void)fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",_strWorkOrderId,_strSubWorkOrderId,_strSubWorkOrderIssuesIdToFetch,_strIssueRepairIdToFetch];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairLabour = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairLabour count] == 0)
    {
        
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        [_tblViewLabor reloadData];
        
    }
    else
    {
        arrOfSubWorkServiceIssuesRepairLabour=nil;
        arrOfSubWorkServiceIssuesRepairLabour=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairLabour.count; k++) {
            
            matchesSubWorkOrderIssuesRepairLabour=arrAllObjSubWorkOrderIssuesRepairLabour[k];
            
            NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderIssuesRepairLabour valueForKey:@"isActive"]];
            
            if ([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) {
                
                [arrOfSubWorkServiceIssuesRepairLabour addObject:matchesSubWorkOrderIssuesRepairLabour];
                
            }
        }
        
        [_tblViewLabor reloadData];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}


-(void)fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanicalViaId :(NSString*)strID{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    //issueRepairLaborId
   // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@ && issueRepairLaborId = %@",_strWorkOrderId,_strSubWorkOrderId,_strIssueRepairIdToFetch,strID];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@ && issueRepairLaborId = %@",_strWorkOrderId,_strSubWorkOrderId,_strSubWorkOrderIssuesIdToFetch,_strIssueRepairIdToFetch,strID];

    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    NSArray *arrTempLabor = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrTempLabor count] == 0)
    {
        
    }
    else
    {
        
       // objLaborEdit=[[NSManagedObject alloc]init];
        
        objLaborEdit=arrTempLabor[0];
        
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
        
        
    }
    
}


-(void)addServiceIssuesRepairLabourFromMobileToDB{
    
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    
    MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssues = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairLabour insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=_strWorkOrderId;
    objSubWorkOrderIssues.issueRepairLaborId=[global getReferenceNumber];
    objSubWorkOrderIssues.subWorkOrderId=_strSubWorkOrderId;
    objSubWorkOrderIssues.issueRepairId=[NSString stringWithFormat:@"%@",_strIssueRepairIdToFetch];
    
    NSString *srtLaborCostType;
    if (isStandardSubWorkOrder) {
        if (isWarranty) {
        
            srtLaborCostType=@"WSP";
            
        } else {
            
            srtLaborCostType=@"SC";
            
        }
        
    } else {
        
        if (isWarranty) {
            
            srtLaborCostType=@"WASP";
            
        } else {
            
            srtLaborCostType=@"AHC";
            
        }
        
    }
    
    objSubWorkOrderIssues.isDefault=[NSString stringWithFormat:@"%@",@"true"];
    
    NSString *strWarranty;
    if (isWarranty) {
        
        strWarranty=@"true";
        
    }else{
        
        strWarranty=@"false";
    }
    
    objSubWorkOrderIssues.isWarranty=[NSString stringWithFormat:@"%@",strWarranty];
    
    objSubWorkOrderIssues.laborCost=[NSString stringWithFormat:@"%@",_txt_LaborAmt.text];
    objSubWorkOrderIssues.laborCostType=[NSString stringWithFormat:@"%@",srtLaborCostType];
    objSubWorkOrderIssues.laborDescription=[NSString stringWithFormat:@"%@",_txtView_LaborDesc.text];
    
    strDate = _txtFld_LaborTime.text;
    
    if (strDate.length==0) {
        strDate=@"00:00";
    }
    
    objSubWorkOrderIssues.laborHours=[NSString stringWithFormat:@"%@",strDate];
    
    NSString *strLaborType;
    if (isLaborType) {
        
        strLaborType=@"L";
        
    }else{
        
        strLaborType=@"H";
    }

    
    objSubWorkOrderIssues.laborType=[NSString stringWithFormat:@"%@",strLaborType];
    
    objSubWorkOrderIssues.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.createdDate=[global strCurrentDate];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderIssues.modifiedDate=[global strCurrentDate];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical];
    
}

-(void)updateLabor{
    
    NSString *strWarranty;
    if (isWarranty) {
        
        strWarranty=@"true";
        
    }else{
        
        strWarranty=@"false";
    }
    
    [objLaborEdit setValue:[NSString stringWithFormat:@"%@",strWarranty] forKey:@"isWarranty"];
    [objLaborEdit setValue:[NSString stringWithFormat:@"%@",_txt_LaborAmt.text] forKey:@"laborCost"];

    NSString *strLaborType;
    if (isLaborType) {
        
        strLaborType=@"L";
        
    }else{
        
        strLaborType=@"H";
    }
    ///
    [objLaborEdit setValue:[NSString stringWithFormat:@"%@",strLaborType] forKey:@"laborType"];
    
    strDate = _txtFld_LaborTime.text;
    
    if (strDate.length==0) {
        strDate=@"00:00";
    }

    [objLaborEdit setValue:[NSString stringWithFormat:@"%@",strDate] forKey:@"laborHours"];
    [objLaborEdit setValue:[NSString stringWithFormat:@"%@",_txtView_LaborDesc.text] forKey:@"laborDescription"];
    
    NSError *error2;
    [context save:&error2];

    [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical];
}

-(void)updateLaborToDelete{
    
    [objLaborEdit setValue:[NSString stringWithFormat:@"%@",@"false"] forKey:@"isActive"];
    
    NSError *error2;
    [context save:&error2];
    
    [self fetchSubWorkOrderIssuesRepairLabourFromDataBaseForMechanical];
    
}

//============================================================================
//============================================================================
#pragma mark TextField Delegate Method
//============================================================================
//============================================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    return  YES;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)texth
{
    if (range.location == 0 && [texth isEqualToString:@" "]) {
        return NO;
    }
    else
        return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }

    if (textField==_txtFld_LaborTime){
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        else
        {
            NSString *text = textField.text;
            NSInteger length = text.length;
            BOOL shouldReplace = YES;
            
            if (![string isEqualToString:@""])
            {
                switch (length)
                {
                    case 2:
                        textField.text = [text stringByAppendingString:@":"];
                        break;
                        
                    default:
                        break;
                }
                if (length > 4)
                    shouldReplace = NO;
            }
            
            return shouldReplace;
        }
        return YES;
        
    }else if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    {
        // BasicAlert(@"", @"This field accepts only numeric entries.");
        return NO;
    }
    else
    {
        NSString *text = textField.text;
        NSInteger length = text.length;
        BOOL shouldReplace = YES;
        
        if (![string isEqualToString:@""])
        {
            switch (length)
            {
                case 2:
                    textField.text = [text stringByAppendingString:@":"];
                    break;
                    
                default:
                    break;
            }
            if (length > 4)
                shouldReplace = NO;
        }
        if (textField.text.length==4) {
            
            [self methodToCalculateLaborPrice:[NSString stringWithFormat:@"%@%@",textField.text,string]];
            
        }

        return shouldReplace;
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField==_txtFld_LaborTime) {
        
        NSString *strTextx;
        
        if (textField.text.length>=5) {
            
            strTextx=textField.text;
            
        } else if (textField.text.length==4){
            
            NSString *strTemp=@"0";//  0:00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==3){
            
            NSString *strTemp=@"00";//  :00
            //strTextx=[strTemp stringByAppendingString:textField.text];
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==2){
            
            //            NSString *strTemp=@"00:";//  00
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=@":00";//  00
            strTextx=[textField.text stringByAppendingString:strTemp];
            
        } else if (textField.text.length==1){
            
            //            NSString *strTemp=@"00:0";//  0
            //            strTextx=[strTemp stringByAppendingString:textField.text];
            
            NSString *strTemp=[NSString stringWithFormat:@"0%@:00",textField.text];
            //NSString *strTemp=@"0:00";
            //strTextx=[textField.text stringByAppendingString:strTemp];
            strTextx=strTemp;
        } else if (textField.text.length==0){
            
            strTextx=@"00:00";
            
        }
        
        
        NSString *lastStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        //NSString *lastSecondStrText = [strTextx substringFromIndex: [strTextx length] - 2];
        NSString *finalStrText = [NSString stringWithFormat:@"%@",lastStrText];
        int valueEntered = [finalStrText intValue];
        if (valueEntered>59) {
            
            [global AlertMethod:Alert :@"Minutes can't be greater then 59"];
            
//            textField.text =@"";
//            textField.placeholder=@"00:00";
//            _txtFld_LaborTime.text=@"";
//            [self methodToCalculateLaborPrice:[NSString stringWithFormat:@"%@",@"00:00"]];

        }else{
            
            textField.text=strTextx;
            
            if ([textField.text isEqualToString:@"00:00"]) {
                
                textField.text =@"";
                textField.placeholder=@"00:00";
                
            }
        
            strDate = strTextx;
            
            if (strDate.length==0) {
                strDate=@"00:00";
            }
            
            [self methodToCalculateLaborPrice:[NSString stringWithFormat:@"%@",strDate]];
            
        }
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    
    
        pickerDate.datePickerMode =UIDatePickerModeTime;
        
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
            [pickerDate setLocale:locale];
    
    //pickerDate.datePickerMode =UIDatePickerModeTime;  //UIDatePickerModeDate;
    [viewForDateTime setHidden:NO];
    
    viewBackGroundTime=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGroundTime.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGroundTime];
    
    //============================================================================
    //============================================================================
    
  //  UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
  //  singleTap1.numberOfTapsRequired = 1;
  //  [viewBackGroundTime setUserInteractionEnabled:YES];
  //  [viewBackGroundTime addGestureRecognizer:singleTap1];
    
    
    viewForDateTime=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGroundTime addSubview: viewForDateTime];
    
    viewForDateTime.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDateTime.layer.cornerRadius=20.0;
    viewForDateTime.clipsToBounds=YES;
    [viewForDateTime.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDateTime.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDateTime.frame.size.width, 50)];
    
    lblTitle.text=@"Select Time";//@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDateTime addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDateTime.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDateTime addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDateTime.frame.size.height-50, viewForDateTime.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDateTime addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDateTime bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDateTime addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDateTime addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDateTime.frame.size.width, viewForDateTime.frame.size.height-100);
    [viewForDateTime addSubview:pickerDate];
    
    if (!(strDate.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"HH:mm"];
        NSDate *dateToSett = [dateFormat dateFromString:strDate];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }

}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDateTime removeFromSuperview];
    [viewBackGroundTime removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    // [dateFormat setDateFormat:@"hh:mm:a"];
    [dateFormat setDateFormat:@"HH:mm"];
    strDate = [dateFormat stringFromDate:pickerDate.date];
    [_btnSelectTime setTitle:strDate forState:UIControlStateNormal];
    
    [self methodToCalculateLaborPrice:[NSString stringWithFormat:@"%@",strDate]];
    
    [viewForDateTime removeFromSuperview];
    [viewBackGroundTime removeFromSuperview];
}

- (IBAction)action_SelectTime:(id)sender {
    
    [self addPickerViewDateTo];
    //to test commit
    
}
@end
