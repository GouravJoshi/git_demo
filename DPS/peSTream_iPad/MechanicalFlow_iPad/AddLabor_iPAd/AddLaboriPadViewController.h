//
//  AddLaboriPadViewController.h
//  peSTream peSTream peSTream 
//  peSTream
//  Created by Saavan Patidar on 03/06/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface AddLaboriPadViewController : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    //
    AppDelegate *appDelegate;
    NSEntityDescription *entitySubWorkOrderIssues,*entityMechanicalSubWorkOrderHelper,*entityMechanicalSubWorkOrderNotes,*entityMechanicalSubWorkOrderActualHrs,*entitySubWorkOrderIssuesRepair,*entitySubWorkOrderIssuesRepairParts,*entitySubWorkOrderIssuesRepairLabour;
    
    
    NSManagedObjectContext *context;
    
    
    NSFetchRequest *requestSubWorkOrderIssues,*requestSubWorkOrderHelper,*requestSubWorkOrderNotes,*requestSubWorkOrderActualHrs,*requestSubWorkOrderIssuesRepair,*requestSubWorkOrderIssuesRepairParts,*requestSubWorkOrderIssuesRepairLabour;
    
    
    NSSortDescriptor *sortDescriptorSubWorkOrderIssues,*sortDescriptorSubWorkOrderHelper,*sortDescriptorSubWorkOrderNotes,*sortDescriptorSubWorkOrderActualHrs,*sortDescriptorSubWorkOrderIssuesRepair,*sortDescriptorSubWorkOrderIssuesRepairParts,*sortDescriptorSubWorkOrderIssuesRepairLabour;
    
    
    NSArray *sortDescriptorsSubWorkOrderIssues,*sortDescriptorsSubWorkOrderHelper,*sortDescriptorsSubWorkOrderNotes,*sortDescriptorsSubWorkOrderActualHrs,*sortDescriptorsSubWorkOrderIssuesRepair,*sortDescriptorsSubWorkOrderIssuesRepairParts,*sortDescriptorsSubWorkOrderIssuesRepairLabour;
    
    
    NSManagedObject *matchesSubWorkOrderIssues,*matchesSubWorkOrderHelper,*matchesSubWorkOrderNotes,*matchesSubWorkOrderActualHrs,*matchesSubWorkOrderIssuesRepair,*matchesSubWorkOrderIssuesRepairParts,*matchesSubWorkOrderIssuesRepairLabour;
    
    
    NSArray *arrAllObjSubWorkOrderIssues,*arrAllObjSubWorkOrderHelper,*arrAllObjSubWorkOrderNotes,*arrAllObjSubWorkOrderActualHrs,*arrAllObjSubWorkOrderIssuesRepair,*arrAllObjSubWorkOrderIssuesRepairParts,*arrAllObjSubWorkOrderIssuesRepairLabour;
    
}
- (IBAction)action_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAccNoDetails;
@property (strong, nonatomic) IBOutlet UITableView *tblViewLabor;
- (IBAction)action_AddLabor:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRadioLabor;
@property (strong, nonatomic) IBOutlet UIButton *btnRadioHelper;
- (IBAction)action_RadioLabor:(id)sender;
- (IBAction)action_RadioHelper:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnIsWarranty;
- (IBAction)action_IsWarranty:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_LaborHrs;
@property (strong, nonatomic) IBOutlet UITextField *txt_LaborAmt;
@property (strong, nonatomic) IBOutlet UITextView *txtView_LaborDesc;
- (IBAction)action_SaveLabor:(id)sender;
- (IBAction)action_ClearLabor:(id)sender;
- (IBAction)action_CancelLabor:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewAddLabor;
@property (strong, nonatomic) NSString *strSubWorkOrderId;
@property (strong, nonatomic) NSString *strWorkOrderId;
@property (strong, nonatomic) NSString *strSubWorkOrderIssueId;
@property (strong, nonatomic) NSString *strIssueRepairIdToFetch;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairLabour;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderdetails;
@property (strong, nonatomic) IBOutlet UILabel *lblLaborType;
@property (strong, nonatomic) NSString *strSubWorkOrderIssuesIdToFetch;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectTime;
- (IBAction)action_SelectTime:(id)sender;
@property (strong, nonatomic) NSManagedObject *objWorkOrderdetails;
@property (strong, nonatomic) IBOutlet UITextField *txtFld_LaborTime;

@end
