//
//  AccountDiscountExtSerDcs+CoreDataClass.h
//  
//
//  Created by Akshay Hastekar on 17/07/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccountDiscountExtSerDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "AccountDiscountExtSerDcs+CoreDataProperties.h"
