//
//  WorkOrderAppliedDiscountExtSerDcs+CoreDataClass.h
//  
//
//  Created by Akshay Hastekar on 19/07/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface WorkOrderAppliedDiscountExtSerDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "WorkOrderAppliedDiscountExtSerDcs+CoreDataProperties.h"
