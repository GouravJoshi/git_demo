//
//  AccountDiscountExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 17/07/18.
//
//

#import "AccountDiscountExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AccountDiscountExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<AccountDiscountExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *accountNo;
@property (nullable, nonatomic, copy) NSString *createdByDevice;
@property (nullable, nonatomic, copy) NSString *departmentSysName;
@property (nullable, nonatomic, copy) NSString *discountAmount;
@property (nullable, nonatomic, copy) NSString *discountCode;
@property (nullable, nonatomic, copy) NSString *discountDescription;
@property (nullable, nonatomic, copy) NSString *discountPercent;
@property (nullable, nonatomic, copy) NSString *discountSetupName;
@property (nullable, nonatomic, copy) NSString *discountSetupSysName;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *isDelete;
@property (nullable, nonatomic, copy) NSString *isDiscountPercent;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *sysName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;

@end

NS_ASSUME_NONNULL_END
