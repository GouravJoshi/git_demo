//
//  WorkOrderAppliedDiscountExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 19/07/18.
//
//

#import "WorkOrderAppliedDiscountExtSerDcs+CoreDataProperties.h"

@implementation WorkOrderAppliedDiscountExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WorkOrderAppliedDiscountExtSerDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"WorkOrderAppliedDiscountExtSerDcs"];
}

@dynamic appliedDiscountAmt;
@dynamic applyOnLaborPrice;
@dynamic applyOnPartPrice;
@dynamic createdBy;
@dynamic createdByDevice;
@dynamic createdDate;
@dynamic discountAmount;
@dynamic discountCode;
@dynamic discountDescription;
@dynamic discountName;
@dynamic discountPercent;
@dynamic discountSysName;
@dynamic discountType;
@dynamic isActive;
@dynamic isDiscountPercent;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic subWorkOrderId;
@dynamic workOrderAppliedDiscountId;
@dynamic workOrderId;

@end
