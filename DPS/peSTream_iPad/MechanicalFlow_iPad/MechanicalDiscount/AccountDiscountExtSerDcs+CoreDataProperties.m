//
//  AccountDiscountExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 17/07/18.
//
//

#import "AccountDiscountExtSerDcs+CoreDataProperties.h"

@implementation AccountDiscountExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<AccountDiscountExtSerDcs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AccountDiscountExtSerDcs"];
}

@dynamic accountNo;
@dynamic createdByDevice;
@dynamic departmentSysName;
@dynamic discountAmount;
@dynamic discountCode;
@dynamic discountDescription;
@dynamic discountPercent;
@dynamic discountSetupName;
@dynamic discountSetupSysName;
@dynamic isActive;
@dynamic isDelete;
@dynamic isDiscountPercent;
@dynamic name;
@dynamic sysName;
@dynamic companyKey;
@dynamic userName;
@dynamic workorderId;

@end
