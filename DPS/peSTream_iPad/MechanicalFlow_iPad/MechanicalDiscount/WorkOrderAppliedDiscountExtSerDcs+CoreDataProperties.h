//
//  WorkOrderAppliedDiscountExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 19/07/18.
//
//

#import "WorkOrderAppliedDiscountExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WorkOrderAppliedDiscountExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WorkOrderAppliedDiscountExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *appliedDiscountAmt;
@property (nullable, nonatomic, copy) NSString *applyOnLaborPrice;
@property (nullable, nonatomic, copy) NSString *applyOnPartPrice;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdByDevice;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *discountAmount;
@property (nullable, nonatomic, copy) NSString *discountCode;
@property (nullable, nonatomic, copy) NSString *discountDescription;
@property (nullable, nonatomic, copy) NSString *discountName;
@property (nullable, nonatomic, copy) NSString *discountPercent;
@property (nullable, nonatomic, copy) NSString *discountSysName;
@property (nullable, nonatomic, copy) NSString *discountType;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *isDiscountPercent;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *subWorkOrderId;
@property (nullable, nonatomic, copy) NSString *workOrderAppliedDiscountId;
@property (nullable, nonatomic, copy) NSString *workOrderId;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;


@end

NS_ASSUME_NONNULL_END
