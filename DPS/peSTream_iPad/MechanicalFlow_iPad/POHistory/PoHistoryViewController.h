//
//  PoHistoryViewController.h
//  DPS
//
//  Created by Saavan Patidar on 08/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//  test 

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface PoHistoryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tblViewPoHistory;
- (IBAction)action_back:(id)sender;
@property (strong, nonatomic) NSString *strWorkOrderNo;
@property (strong, nonatomic) IBOutlet UIButton *btnCreatePo;
- (IBAction)action_CreatePO:(id)sender;
// managed object
@property (strong, nonatomic) NSManagedObject *objWorkOrderDetail;
@property (strong, nonatomic) NSManagedObject *objSubWorkOrderDetail;
@property (strong, nonatomic) NSString *strFromWhere;
@property (strong, nonatomic) NSString *strSubWorkOrderId;
@property (strong, nonatomic) NSString *strWorkOrderId;
@property (strong, nonatomic) NSString *strWoType;
- (IBAction)action_Refresh:(id)sender;

@end
