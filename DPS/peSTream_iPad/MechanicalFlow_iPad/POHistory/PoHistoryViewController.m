//
//  PoHistoryViewController.m
//  DPS
//
//  Created by Saavan Patidar on 08/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "PoHistoryViewController.h"

@interface PoHistoryViewController ()
{
    Global *global;
    NSArray *arrPoHistory;
}

@end

@implementation PoHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    global = [[Global alloc] init];
    _tblViewPoHistory.rowHeight=UITableViewAutomaticDimension;
    _tblViewPoHistory.estimatedRowHeight=200;
    _tblViewPoHistory.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    [_tblViewPoHistory setHidden:YES];
    [_tblViewPoHistory setSeparatorColor:[UIColor colorWithRed:(95/255.0) green:(178/255.0) blue:(175/255.0) alpha:1.0]];
    
    if(_strWorkOrderId.length>0)
    {
        
        [_btnCreatePo setHidden:NO];
        
    }else{
        
        [_btnCreatePo setHidden:YES];
        
    }
    NSString *strWorkOrderStatuss=[_objSubWorkOrderDetail valueForKey:@"subWOStatus"];
    
    BOOL isCompletedStatusMechanical=[global isCompletedSatusMechanical:strWorkOrderStatuss];
    
    if (isCompletedStatusMechanical) {
        
        [_btnCreatePo setHidden:YES];
        
    }

}

-(void)viewWillAppear:(BOOL)animated{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching PO History..."];
    
    [self performSelector:@selector(getPoHistory) withObject:nil afterDelay:0.1];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getPoHistory{
    
    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
    NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *strEmpNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    //strEmpNo=@"83";
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMain,UrlGetPOHistory,strCompanyKey,UrlPoEmployeeNo,strEmpNo];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             
             if ((ResponseDict==nil) || ResponseDict.count==0) {
                 
                 [_tblViewPoHistory setHidden:YES];
                 [global AlertMethod:Alert :@"No data available"];
                 [DejalBezelActivityView removeView];
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDict =dictData;
                 
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                 
                 if (strException.length==0) {
                     
                     [_tblViewPoHistory setHidden:NO];
                     
                     arrPoHistory=(NSArray*)ResponseDict;
                     
                     NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                     
                     if (_strWorkOrderNo.length>0) {
                         
                         for (int j=0; j<arrPoHistory.count; j++) {
                             
                             NSDictionary *dictData=arrPoHistory[j];
                             NSString *strNo=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WorkOrderNo"]];
                             
                             if ([_strWorkOrderNo isEqualToString:strNo]) {
                                 
                                 [arrTemp addObject:dictData];
                                 
                             }
                             
                         }
                         
                         arrPoHistory=arrTemp;
                         
                     }
                     
                     [_tblViewPoHistory reloadData];
                     
                     [DejalBezelActivityView removeView];
                     
                 } else {
                     
                     [_tblViewPoHistory setHidden:YES];
                     ResponseDict=nil;
                     [global AlertMethod:Alert :@"No data available"];
                     [DejalBezelActivityView removeView];
                     
                 }
                 
             }
             
         }];
    }
    @catch (NSException *exception) {
        
        [_tblViewPoHistory setHidden:YES];
        [global AlertMethod:Alert :Sorry];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrPoHistory.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return tableView.rowHeight;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PoHistoryTableViewCell *cell = (PoHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PoHistoryTableViewCell" forIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [self configureCellEquipment:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCellEquipment:(PoHistoryTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dictData=arrPoHistory[indexPath.row];
    cell.lblPoNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"PurchaseOrderNumber"]];
    cell.lblRoute.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"RouteName"]];
    
    NSString *strPoType=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"POType"]];
    
    if ([strPoType isEqualToString:@"true"] || [strPoType isEqualToString:@"True"]) {
        strPoType=@"Generic";
    } else {
        strPoType=@"Standard";
    }
    cell.lblPoType.text=[NSString stringWithFormat:@"%@",strPoType];
    
    NSString *strStatus=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Status"]];
    
    if ([strStatus isEqualToString:@"0"]) {
        strStatus=@"Pending";
        
    }else if ([strStatus isEqualToString:@"1"]){
        strStatus=@"Approved";
        
    }else if ([strStatus isEqualToString:@"2"]){
        strStatus=@"Received";
        
    }else if ([strStatus isEqualToString:@"3"]){
        strStatus=@"Decline";
        
    }
    cell.lblStatus.text=[NSString stringWithFormat:@"%@",strStatus];
    cell.lblVendor.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"VendorName"]];
    cell.lblLocation.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"LocationName"]];
    cell.lblTechnician.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TechnicianName"]];
    cell.lblWorkorderNo.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WorkOrderNo"]];
    
    [cell.buttonReceivePO addTarget:self action:@selector(actionOnReceivePO:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.buttonReceivePO.tag = indexPath.row;
    
    if([[dictData valueForKey:@"Status"] intValue]==1||[[dictData valueForKey:@"Status"] boolValue]==YES)
    {
        cell.buttonReceivePO.hidden = YES;
    }
    else
    {
        cell.buttonReceivePO.hidden = NO;
    }
    
    if ([strStatus isEqualToString:@"Approved"]) {
        
        cell.buttonReceivePO.hidden = NO;
        
    } else {
        
        cell.buttonReceivePO.hidden = YES;
        
    }
    
    
}

-(void)actionOnReceivePO:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Searching..."];
    DELAY(1.0);
    [self searchPurchaseOrderByPurchaseOrderNumber:[NSString stringWithFormat:@"%@",[[arrPoHistory objectAtIndex:btn.tag] valueForKey:@"PurchaseOrderNumber"]]];
    
}

- (IBAction)action_back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)searchPurchaseOrderByPurchaseOrderNumber:(NSString*)strPONumber
{
    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
    NSString *strCompanyKeyTemp = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.CoreServiceModule.ServiceUrl"];
    // NSString *strEmpNoTemp=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@/api/MobileAppToCore/GetPurchaseOrderListByPurchaseOrderNumber?companyKey=%@&poNumber=%@",strServiceUrlMain,strCompanyKeyTemp,strPONumber];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
             [DejalBezelActivityView removeView];
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             
             if ((ResponseDict==nil) || ResponseDict.count==0) {
                 
                 [global AlertMethod:Alert :NoDataAvailableee];
                 
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDict =dictData;
                 
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                 
                 if (strException.length==0)
                 {
                     NSArray *tempArray=(NSArray*)ResponseDict;
                     if(!(tempArray.count>0))
                     {
                         [global AlertMethod:@"Message" :NoDataAvailableee];
                     }
                     else
                     {
                         // got to received po list view
                         
                         UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MainiPad" bundle:nil];
                         ReceivedPOViewController  *vc=[storyboard instantiateViewControllerWithIdentifier:@"ReceivedPOViewController"];
                         vc.dictReceivedPO=[tempArray firstObject];
                         [self presentViewController:vc animated:YES completion:nil];
                         // [self.navigationController pushViewController:vc animated:YES];
                     }
                     
                 } else {
                     
                     ResponseDict=nil;
                     [global AlertMethod:Alert :NoDataAvailableee];
                 }
             }
         }];
    }
    @catch (NSException *exception) {
        
        [global AlertMethod:Alert :Sorry];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }
}
- (IBAction)action_CreatePO:(id)sender {
    
       UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                                bundle: nil];
       PurchaseOrderViewController
       *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"PurchaseOrderViewController"];
       objByProductVC.strWorkOrderId=_strWorkOrderId;
       objByProductVC.strSubWorkOrderId=_strSubWorkOrderId;
       objByProductVC.strWoType=_strWoType;
       objByProductVC.objWorkOrderDetail=_objWorkOrderDetail;
       objByProductVC.objSubWorkOrderDetail=_objSubWorkOrderDetail;
       objByProductVC.strFromWhere=@"";
       [self presentViewController:objByProductVC animated:YES completion:nil];

}

-(void)test{
    
}
- (IBAction)action_Refresh:(id)sender {
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching PO History..."];
    
    [self performSelector:@selector(getPoHistory) withObject:nil afterDelay:0.1];
    
}
@end
