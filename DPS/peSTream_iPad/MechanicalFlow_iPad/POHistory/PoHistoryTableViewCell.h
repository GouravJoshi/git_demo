//
//  PoHistoryTableViewCell.h
//  DPS
////  peStream
//  Created by Saavan Patidar on 08/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PoHistoryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblPoNo;
@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblWorkorderNo;
@property (strong, nonatomic) IBOutlet UILabel *lblPoType;
@property (strong, nonatomic) IBOutlet UILabel *lblVendor;
@property (strong, nonatomic) IBOutlet UILabel *lblTechnician;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblRoute;
@property (weak, nonatomic) IBOutlet UIButton *buttonReceivePO;

@end
