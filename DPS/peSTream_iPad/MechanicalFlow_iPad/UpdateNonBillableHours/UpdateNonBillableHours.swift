//
//  UpdateNonBillableHours.swift
//  DPS
//
//  Created by Akshay Hastekar on 01/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//
//  Saavan Patidar 2021

import UIKit

class UpdateNonBillableHours: UIViewController
{

    // MARK: - ------ Outlets ---------
    
    @IBOutlet weak var btnDepartment: UIButton!
    
    @IBOutlet weak var btnTimeSlot: UIButton!
    
    @IBOutlet weak var btnFromTime: UIButton!
    
    @IBOutlet weak var btnToTime: UIButton!
    
    @IBOutlet weak var txtViewDescription: UITextView!
    
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    var arrResponseNonBillable = NSArray()
    var dictNonBillabel = NSDictionary()
    var arrayBranch = NSArray()
    var strEmployeeBranchId = NSString()
    var strTimeSlotSelected = String()
    var strDepartmentSysName = String()
    var strNonBillabelTimeId = String()
    var strEmployeeId = String()
    var strCompanyKeyNew = String()
    var strFromTimeNonBillable = String()
    var strToTimeNonBillable = String()
    var strFromTime = String()
    var strToTime = String()
    var strTimeSlot = String()
    var strTimeSlotTitle = String()
    var strQBPayrollItemName = String()
    var strDateEdited = String()
    
    var arrBranchDepartment = NSArray()
    var arrDepartment = NSMutableArray()
    var arrOfSlots = NSMutableArray()
    
    var dictLoginData = NSDictionary()
    var dictSlotData = NSDictionary()
    
    // MARK: - ------View DidLoad ---------
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
       /* _txtLinearSqFt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
        _txtLinearSqFt.layer.borderWidth=1.0;
        
        _txtLinearSqFt.layer.cornerRadius=5.0;*/
        
//        txtViewDescription.layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor
//        txtViewDescription.layer.borderWidth = 1.0
//        txtViewDescription.layer.cornerRadius = 5.0
        setColorBorder(item: txtViewDescription)
        setColorBorder(item: btnDepartment)
        setColorBorder(item: btnTimeSlot)
        setColorBorder(item: btnFromTime)
        setColorBorder(item: btnToTime)
        setColorBorder(item: btnUpdate)
        setColorBorder(item: btnCancel)
        
        btnDepartment.titleLabel?.textColor = UIColor.black
        btnTimeSlot.titleLabel?.textColor = UIColor.black
        btnFromTime.titleLabel?.textColor = UIColor.black
        btnToTime.titleLabel?.textColor = UIColor.black
        btnUpdate.titleLabel?.textColor = UIColor.black
        btnCancel.titleLabel?.textColor = UIColor.black

        
        btnDepartment.backgroundColor = UIColor.clear
        btnTimeSlot.backgroundColor = UIColor.clear
        btnFromTime.backgroundColor = UIColor.clear
        btnToTime.backgroundColor = UIColor.clear
        btnUpdate.backgroundColor = UIColor.theme()
        btnCancel.backgroundColor = UIColor.themeColorBlack()

        
        
        getDepartments()
        assignValues()
        // Do any additional setup after loading the view.
    }
    
    
      // MARK: - ------ Actions ---------

    
    @IBAction func actionOnBack(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionOnDepartment(_ sender: Any)
    {
        
    
        let arrOfData = arrDepartment
        btnDepartment.tag = 24
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")

    }
    
    @IBAction func actiononTimeSlot(_ sender: Any)
    {
        arrOfSlots = NSMutableArray()
        
        var arrOfHoursConfig = NSMutableArray()
        
        arrOfHoursConfig = Global().getHoursConfi(fromMaster: strCompanyKeyNew, strDepartmentSysName as String, "", "", "Residential")
        
         //New Change 27 Aug 2019
        var strQbPayrollItemForRegular = String()
        
        var strQbPayrollItemForHoliday = String()
        
        strQbPayrollItemForRegular = "Regular"
        
        strQbPayrollItemForHoliday = "Holiday"
        
        if (arrOfHoursConfig.count>0)
        {
            var dictDataHourConfig = NSDictionary()
            dictDataHourConfig = arrOfHoursConfig.object(at: 0) as! NSDictionary
            
            var arrAfterHourRateConfigDcs = NSArray()
            arrAfterHourRateConfigDcs = dictDataHourConfig.value(forKey: "AfterHourRateConfigDcs") as! NSArray
            
            var strRegular = String()
            var strHoliday = String()
            
            strRegular = "\(dictDataHourConfig.value(forKey: "StdQBPayrollItemName") ?? "")"
            strHoliday = "\(dictDataHourConfig.value(forKey: "HolidayQBPayrollItemName") ?? "")"
            
            if (strRegular.count>0 || !(strRegular == ""))
            {
                strQbPayrollItemForRegular = strRegular
            }
            
            if (strHoliday.count>0 || !(strHoliday == ""))
            {
                strQbPayrollItemForHoliday = strHoliday
            }
            
            if ( arrAfterHourRateConfigDcs.count>0 )
            {
               /* var dict = NSDictionary()
                dict = arrOfHoursConfig.object(at: 0) as! NSDictionary
                var strRegular = String()
                var strHoliday = String()
                
                strRegular = "\(dict.value(forKey: "StdQBPayrollItemName") ?? "")"
                strHoliday = "\(dict.value(forKey: "HolidayQBPayrollItemName") ?? "")"
                
                if (strRegular.count>0 || !(strRegular == ""))
                {
                     strQbPayrollItemForRegular = strRegular
                }
                
                if (strHoliday.count>0 || !(strHoliday == ""))
                {
                     strQbPayrollItemForHoliday = strHoliday
                }*/
            }
        }
        
        //End
     
        
        
        let objValue = ["Regular", "Regular", strQbPayrollItemForRegular,"Regular","1"]
        
        let objKey = ["TimeSlot", "TimeSlotTitle", "QBPayrollItemName","Title","IsActive"]
        
        let dict_ToSend = NSDictionary(objects:objValue, forKeys:objKey as [NSCopying]) as Dictionary

        
        let objValue1 = ["Holiday", "Holiday", strQbPayrollItemForHoliday,"Holiday","1"]
        
        let objKey1 = ["TimeSlot", "TimeSlotTitle", "QBPayrollItemName","Title","IsActive"]
        
        let dict_ToSend1 = NSDictionary(objects:objValue1, forKeys:objKey1 as [NSCopying]) as Dictionary

      
        if arrOfHoursConfig.count>0
        {
            let dictDataHourConfig = arrOfHoursConfig.object(at: 0) as! NSDictionary
            arrOfSlots.add(dict_ToSend)
            
            //New
            var arrTemp = NSArray()
            arrTemp = dictDataHourConfig.value(forKey: "AfterHourRateConfigDcs") as! NSArray
            for item in arrTemp
            {
                let dict = (item as AnyObject)as! NSDictionary
                arrOfSlots.add(dict)

            }
            //End
            
            //arrOfSlots.add(dictDataHourConfig.value(forKey: "AfterHourRateConfigDcs") as! NSArray)
            
            arrOfSlots.add(dict_ToSend1)
        }
        else
        {
            arrOfSlots = NSMutableArray()
            arrOfSlots.add(dict_ToSend)
            arrOfSlots.add(dict_ToSend1)
        }
        
        let arrOfData = arrOfSlots
        btnTimeSlot.tag = 25
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func actionOnFromTime(_ sender: Any)
    {
        btnFromTime.tag = 10
         self.gotoDatePickerView(sender: sender as! UIButton, strType: "Time")
    }
    
    @IBAction func actionOnToTime(_ sender: Any)
    {
        btnToTime.tag = 11
        self.gotoDatePickerView(sender: sender as! UIButton, strType: "Time")
    }
    
    @IBAction func actionOnUpdate(_ sender: Any)
    {
        if !isInternetAvailable()
        {
            Global().displayAlertController("Alert !", "\(ErrorInternetMsg)", self)
        }
        else
        {
            
            var strBtnFromTime = String()
            strBtnFromTime = btnFromTime.currentTitle!
            
            var strBtnToTime = String()
            strBtnToTime = btnToTime.currentTitle!
            
            
            if(strDepartmentSysName == "" || strDepartmentSysName.count == 0)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select department", viewcontrol: self)
            }
            else if(btnTimeSlot.currentTitle == "Select")
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select time slot", viewcontrol: self)
            }
            else if(btnFromTime.currentTitle == "Select")
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select from time", viewcontrol: self)
            }
            else if(btnToTime.currentTitle == "Select")
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select to time", viewcontrol: self)
            }
            else if(btnFromTime.currentTitle == btnToTime.currentTitle)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "From time and To time can not be same", viewcontrol: self)
            }
            else if((stringToDateFormattedUTC(dateToConvert: strBtnFromTime, format: "hh:mm a", formatToBeConverted: "hh:mm a")) > (stringToDateFormattedUTC(dateToConvert: strBtnToTime, format: "hh:mm a", formatToBeConverted: "hh:mm a")))
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "From time can not be greater", viewcontrol: self)
            }
            else
            {
                if (isValidTimeToAdd() == true)
                {
                   // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Time slot  available", viewcontrol: self)
                    callApi()
                }
                else
                {
                     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Time range already exist", viewcontrol: self)
                    
                }
                
            }
        }
    }
    
    @IBAction func actionOnCancel(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - ------ API ---------
    
    func callApi() -> Void
    {
        
        var key = NSArray()
        var value = NSArray()
        var strUrl = String()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        var strClockUrl = String()
        strClockUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.HrmsServiceModule.ServiceUrl")!)"
    
       
        if dictSlotData.count > 0
        {
            var strTitleTemp = String()
            strTitleTemp = "\(dictSlotData.value(forKey: "Title")!)" as String
            
            
            if strTitleTemp .isEqual("Regular" as String)
            {
                
            }
            else if strTitleTemp .isEqual("Holiday" as String)
            {
                
            }
            else
            {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm:ss"
                
                var dateFromTimeTemp = Date()
                dateFromTimeTemp = dateFormatter.date(from: "\(dictSlotData.value(forKey: "FromTime")!)")! as Date
               
                var dateToTimeTemp = Date()
                dateToTimeTemp = dateFormatter.date(from: "\(dictSlotData.value(forKey: "ToTime")!)")! as Date

                let dateFormatterNew = DateFormatter()
                dateFormatterNew.dateFormat = "hh:mm a"

                var strFromTimeTemp = String()
                strFromTimeTemp = dateFormatterNew.string(from: dateFromTimeTemp as Date)
                
                var strToTimeTemp = String()
                strToTimeTemp = dateFormatterNew.string(from: dateToTimeTemp as Date)

                strFromTimeTemp = strFromTimeTemp.lowercased()
                strToTimeTemp = strToTimeTemp.lowercased()

                strTitleTemp = strFromTimeTemp + " - " + strToTimeTemp
                
            }
            strTimeSlot = strTitleTemp
            strTimeSlotTitle = dictSlotData.value(forKey: "Title") as! String
            strQBPayrollItemName = dictSlotData.value(forKey: "QBPayrollItemName") as! String
        }
        else
        {
            strTimeSlot = dictNonBillabel.value(forKey: "TimeSlot") as! String
            strTimeSlotTitle = dictNonBillabel.value(forKey: "TimeSlotTitle") as! String
            strQBPayrollItemName = dictNonBillabel.value(forKey: "QBPayrollItemName") as! String
        }
        
        strUrl = strClockUrl + "/api/EmployeeNonBillableTime/AddUpdateEmployeeNonBillableTimeExt"
        
        
        if (strFromTimeNonBillable.count>15)
        {
            strFromTimeNonBillable = stringDate(toAM_PM: strFromTimeNonBillable)!
            
        }
        if (strToTimeNonBillable.count>15)
        {
            strToTimeNonBillable = stringDate(toAM_PM: strToTimeNonBillable)!
        }
        

        key = ["EmployeeNonBillableTimeId",
               "EmployeeId",
               "FromDate",
               "ToDate",
               "Title",
               "Description",
               "IsActive",
               "EmployeeDc",
               "CreatedDate",
               "CreatedBy",
               "ModifiedDate",
               "ModifiedBy",
               "DepartmentSysName",
               "TimeSlot",
               "TimeSlotTitle",
               "QBPayrollItemName"]
        
        value = [strNonBillabelTimeId,
                 strEmployeeId,
                 strFromTimeNonBillable,
                 strToTimeNonBillable,
                 "Nonbillable Reason",
                 txtViewDescription.text,
                 "True",
                 "",
                 Global().strCurrentDate(),
                 strEmployeeId,
                 "",
                 "",
                 strDepartmentSysName,
                 strTimeSlot,
                 strTimeSlotTitle,
                 strQBPayrollItemName]
        
        let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
        
        FTIndicator.showProgress(withMessage: "Updating...", userInteractionEnable: false)

        WebService.postRequestWithHeaders(dictJson: dict_ToSend as NSDictionary, url: strUrl, responseStringComing: "TimeSlotTitle")
        { (dictResponse, success) in
            
            
            print(dictResponse)
           if(success == true)
            {
                DispatchQueue.main.async
                    {
                    
                    let alertController = UIAlertController(title: "Message", message: "Record Updated Successfully.", preferredStyle:UIAlertController.Style.alert)
                    
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                    { action -> Void in
                        
                    
                        self.dismiss(animated: false)
                        
                        nsud.set(true, forKey: "isChangedFilterDate")
                        nsud.synchronize()
                        
                    })
                    self.present(alertController, animated: true, completion: nil)
                         FTIndicator.dismissProgress()
                        
                }
                
            }
            else
            {
                DispatchQueue.main.async
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Something went wrong, please try again later.", viewcontrol: self)
                     FTIndicator.dismissProgress()
                }
            }
 
            
          /* if success == true
            {
                DispatchQueue.main.async
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Record Updated Successfully", viewcontrol: self)
                    
                    self.dismiss(animated: true, completion: nil)
                     FTIndicator.dismissProgress()

                }
               

            }
            else
            {
                DispatchQueue.main.async
                {
                        
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                    
                    self.dismiss(animated: true, completion: nil)
                     FTIndicator.dismissProgress()
                }
            }*/
            
           
            // self.dismiss(animated: true, completion: nil)
        }

       
    }
    
    // MARK: - ------ Other Functions ---------
    
        func setColorBorder(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    func setColorBorderForView(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        //vc.arySelectedSource = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleSelection = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)

        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    func getDepartments() -> Void
    {
        arrDepartment = NSMutableArray()

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        arrayBranch = (nsud.value(forKey: "LeadDetailMaster") as! NSDictionary).value(forKey: "BranchMastersAll") as! NSArray
        
        strEmployeeBranchId = ("\(dictLoginData.value(forKey: "EmployeeBranchId")!)") as NSString
        
        for item in arrayBranch
        {
            let dict = (item as AnyObject)as! NSDictionary
            var strLocalBranchId = NSString ()
            strLocalBranchId = ("\(dict.value(forKey: "BranchMasterId")!)") as NSString
            
            if strLocalBranchId .isEqual(to: strEmployeeBranchId as String)
            {

                arrBranchDepartment = dict.value(forKey: "Departments") as! NSArray
                
                for newItem in arrBranchDepartment
                {
                    let dictNew = (newItem as AnyObject)as! NSDictionary
                    
                    arrDepartment.add(dictNew)
                    
                }
                
            }
            
            
        }
        print("Department Array \(arrDepartment)")
        
    }
    func getTimeSlot() -> Void
    {
        
    }
    
    func assignValues() -> Void
    {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        strCompanyKeyNew = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")! )"
        
        
        strDepartmentSysName = dictNonBillabel.value(forKey: "DepartmentSysName") as! String
        //    strCompanyKeyNew=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];

        
        for item in arrDepartment
        {
            // let dict = (item as AnyObject)as! NSDictionary
            let dict = item as AnyObject as! NSDictionary
            
            if strDepartmentSysName .isEqual("\(dict.value(forKey: "SysName")as! String)")
            {
                btnDepartment.setTitle("\(dict.value(forKey: "Name") as! NSString )", for: .normal)
                
                break
            }
        }
        strNonBillabelTimeId = "\(dictNonBillabel.value(forKey: "EmployeeNonBillableTimeId")!)"
        
        strEmployeeId = "\(dictNonBillabel.value(forKey: "EmployeeId")!)"
        
        btnTimeSlot.setTitle("\(dictNonBillabel.value(forKey: "TimeSlotTitle") as! String )", for: .normal)
        
        if btnTimeSlot.currentTitle == ""
        {
            var strTitle = String()
            strTitle = "\(dictNonBillabel["TimeSlot"]!) "
            btnTimeSlot.setTitle(strTitle , for: .normal)
            
        }
        
        

        var strFromTimeTemp = String()
        strFromTimeTemp = changeDate(toLocalDateClock: "\(dictNonBillabel.value(forKey: "FromDate")!)")!

        btnFromTime.setTitle(stringDate(toAM_PM: strFromTimeTemp), for: .normal)
        
        var strToTimeTemp = String()
        strToTimeTemp = changeDate(toLocalDateClock: "\(dictNonBillabel.value(forKey: "ToDate")!)")!

        btnToTime.setTitle(stringDate(toAM_PM: strToTimeTemp), for: .normal)
        
        strFromTimeNonBillable = strFromTimeTemp
        strToTimeNonBillable = strToTimeTemp
        strFromTime = strFromTimeTemp
        strToTime = strToTimeTemp
        
        txtViewDescription.text = "\(dictNonBillabel.value(forKey: "Description")!)"
        
        var dateTempEdited = Date()
        dateTempEdited = stringToDateFormattedUTC(dateToConvert: strFromTimeTemp, format: "MM/dd/yyyy hh:mm:a", formatToBeConverted: "MM/dd/yyyy")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        strDateEdited = dateFormatter.string(from: dateTempEdited)
    
    }
    func stringDate(toAM_PM strDateTime: String?) -> String?
    {
        var strDateTime = strDateTime
        
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "MM/dd/yyyy hh:mm:a"
        
        let date1: Date? = dateFormat.date(from: strDateTime ?? "")
        
        dateFormat.dateFormat = "hh:mm a"
        
        if let date1 = date1 {
            strDateTime = dateFormat.string(from: date1)
        }
        
        return strDateTime
        
    }
    func stringDateToString(toAM_PM strDateTime: String?) -> String?
    {
        var strDateTime = strDateTime
        
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "HH:mm:ss"
        
        let date1: Date? = dateFormat.date(from: strDateTime ?? "")
        
        dateFormat.dateFormat = "hh:mm a"
        
        if let date1 = date1 {
            strDateTime = dateFormat.string(from: date1)
        }
        
        return strDateTime
        
    }
    
    
    func changeDate(toLocalDateClock strDateToConvert: String?) -> String? {
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone.local
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        let newTime: Date? = dateFormatter.date(from: strDateToConvert ?? "")
        
        dateFormatter.timeZone = NSTimeZone.system
        
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:a"
        
        var finalTime: String? = nil
        if let newTime = newTime {
            finalTime = dateFormatter.string(from: newTime)
        }
        if (finalTime?.count ?? 0) == 0 {
            
            finalTime = ""
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.timeZone = NSTimeZone.local
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            let newTime: Date? = dateFormatter.date(from: strDateToConvert ?? "")
            
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:a"
            
            if let newTime = newTime {
                finalTime = dateFormatter.string(from: newTime)
            }
        }
        
        if (finalTime?.count ?? 0) == 0 {
            
            finalTime = ""
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.timeZone = NSTimeZone.local
            
            dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
            
            let newTime: Date? = dateFormatter.date(from: strDateToConvert ?? "")
            
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:a"
            
            if let newTime = newTime {
                finalTime = dateFormatter.string(from: newTime)
            }
        }
        
        if (finalTime?.count ?? 0) == 0 {
            
            finalTime = ""
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.timeZone = NSTimeZone.local
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            let newTime: Date? = dateFormatter.date(from: strDateToConvert ?? "")
            
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:a"
            
            if let newTime = newTime {
                finalTime = dateFormatter.string(from: newTime)
            }
        }
        if (finalTime?.count ?? 0) == 0 {
            finalTime = ""
        }
        return finalTime
        
    }
    func changeDate(toLocalDateInAM_PM strDateToConvert: String?) -> String? {
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone.local
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        let newTime: Date? = dateFormatter.date(from: strDateToConvert ?? "")
        
        dateFormatter.timeZone = NSTimeZone.system
        
        dateFormatter.dateFormat = "hh:mm a"
        
        var finalTime: String? = nil
        if let newTime = newTime {
            finalTime = dateFormatter.string(from: newTime)
        }
        if (finalTime?.count ?? 0) == 0 {
            
            finalTime = ""
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.timeZone = NSTimeZone.local
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            let newTime: Date? = dateFormatter.date(from: strDateToConvert ?? "")
            
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "hh:mm a"
            
            if let newTime = newTime {
                finalTime = dateFormatter.string(from: newTime)
            }
        }
        
        if (finalTime?.count ?? 0) == 0 {
            
            finalTime = ""
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.timeZone = NSTimeZone.local
            
            dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
            
            let newTime: Date? = dateFormatter.date(from: strDateToConvert ?? "")
            
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "hh:mm a"
            
            if let newTime = newTime {
                finalTime = dateFormatter.string(from: newTime)
            }
        }
        if (finalTime?.count ?? 0) == 0 {
            
            finalTime = ""
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.timeZone = NSTimeZone.local
            
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:a"
            
            let newTime: Date? = dateFormatter.date(from: strDateToConvert ?? "")
            
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "hh:mm a"
            
            if let newTime = newTime {
                finalTime = dateFormatter.string(from: newTime)
            }
        }
        if (finalTime?.count ?? 0) == 0 {
            
            finalTime = ""
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.timeZone = NSTimeZone.local
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            let newTime: Date? = dateFormatter.date(from: strDateToConvert ?? "")
            
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "hh:mm a"
            
            if let newTime = newTime {
                finalTime = dateFormatter.string(from: newTime)
            }
        }
        if (finalTime?.count ?? 0) == 0 {
            finalTime = ""
        }
        return finalTime
        
    }
    func isValidTimeToAdd() -> Bool
    {
        var chkValidToAdd = Bool()
        chkValidToAdd = false
       
        
        var strFromTimeTemp = String()
        strFromTimeTemp = changeDate(toLocalDateClock: "\(dictNonBillabel.value(forKey: "FromDate")!)")!
     
        
        var strToTimeTemp = String()
        strToTimeTemp = changeDate(toLocalDateClock: "\(dictNonBillabel.value(forKey: "ToDate")!)")!
        
        
        if strFromTimeNonBillable.count>15
        {
            strFromTimeTemp = changeDate(toLocalDateInAM_PM: strFromTimeNonBillable)!
        }
        else
        {
            strFromTimeTemp = strFromTimeNonBillable
        }
        if strToTimeNonBillable.count>15
        {
             strToTimeTemp = changeDate(toLocalDateInAM_PM: strToTimeNonBillable)!
        }
        else
        {
            strToTimeTemp = strToTimeNonBillable

        }
    
        print(arrResponseNonBillable)
        
        var strCurrentDate = String()
        strCurrentDate = Global().getCurrentDate()
        strCurrentDate = strDateEdited
        
       // let strFromDateNew = strCurrentDate + " " + stringDate(toAM_PM: strFromTimeTemp)!//strFromTimeNonBillable
       // let strToDateNew = strCurrentDate + " " + stringDate(toAM_PM: strToTimeTemp)!//strToTimeNonBillable
        
        /*let dateFormatTemp = DateFormatter()
        dateFormatTemp.dateFormat = "hh:mm a"
        var date1Temp = Date()
        var date2Temp = Date()
        date1Temp = dateFormatTemp.date(from: strFromTimeTemp )!
        date2Temp = dateFormatTemp.date(from: strToTimeTemp )!
        dateFormatTemp.dateFormat = "hh:mm a"
        strFromTimeTemp = dateFormatTemp.string(from: date1Temp)
        strToTimeTemp = dateFormatTemp.string(from: date2Temp)*/

        
        
        var strFromDateNew = String()
         //strFromDateNew = strCurrentDate + " " + stringDate(toAM_PM: strFromTimeTemp)!
        strFromDateNew = strCurrentDate + " " + strFromTimeTemp

        var strToDateNew = String()
        // strToDateNew = strCurrentDate + " " + stringDate(toAM_PM: strToTimeTemp)!
        strToDateNew = strCurrentDate + " " + strToTimeTemp

        
        
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "MM/dd/yyyy hh:mm:a"
        
        var date1 = Date()
        //date1 = dateFormat.date(from: strFromDateNew )! //as! Date
        
        //stringToDateFormattedUTC
        date1 = stringToDateFormattedUTC(dateToConvert: strFromDateNew, format: "MM/dd/yyyy hh:mm a", formatToBeConverted: "MM/dd/yyyy hh:mm:a")
        
        var date2 = Date()
         //date2 = dateFormat.date(from: strToDateNew)! //as! Date
        date2 = stringToDateFormattedUTC(dateToConvert: strToDateNew, format: "MM/dd/yyyy hh:mm a", formatToBeConverted: "MM/dd/yyyy hh:mm:a")

        
        dateFormat.dateFormat = "MM/dd/yyyy hh:mm:a"
        var str1 = String()
        var str2 = String()
        str1 = dateFormat.string(from: date1)
        str2 = dateFormat.string(from: date2)
        
        var dateSelectedFromTime = Date()
        //dateSelectedFromTime =  dateFormat.date(from: str1)!
        dateSelectedFromTime = stringToDateFormattedUTC(dateToConvert: str1, format: "MM/dd/yyyy hh:mm:a", formatToBeConverted: "MM/dd/yyyy hh:mm:a")
        
        var dateSelectedToTime = Date()
        //dateSelectedToTime =  dateFormat.date(from: str2)!
        dateSelectedToTime = stringToDateFormattedUTC(dateToConvert: str2, format: "MM/dd/yyyy hh:mm:a", formatToBeConverted: "MM/dd/yyyy hh:mm:a")


        for item in arrResponseNonBillable
        {
            let dict = item as AnyObject as! NSDictionary
            
            if (("\(dict.value(forKey: "EmployeeNonBillableTimeId")!)") == strNonBillabelTimeId)
            {
                
                chkValidToAdd = true
                continue
            }
            
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "MM/dd/yyyy hh:mm:a"
            var strFromDate = String()
            strFromDate = self.changeDate(toLocalDateClock: "\(dict.value(forKey: "FromDate")!)")!
            
            var strToDate = String()
            strToDate = self.changeDate(toLocalDateClock: "\(dict.value(forKey: "ToDate")!)")!

            var fromDateInResponse = Date()
             fromDateInResponse = dateFormat.date(from: strFromDate)!
            
            var toDateInResponse = Date()
             toDateInResponse = dateFormat.date(from: strToDate)!
            
            
        
            if (((fromDateInResponse < dateSelectedFromTime) && (dateSelectedFromTime < toDateInResponse)) || ((fromDateInResponse < dateSelectedToTime) && (dateSelectedToTime < toDateInResponse)))
            {
                
                chkValidToAdd = false
                print("Time slot not available")
                break
                
            }
            else
            {
                chkValidToAdd = true
                
            }
        //Orignal 
          /* if (((dateSelectedFromTime < fromDateInResponse ) || (dateSelectedFromTime < toDateInResponse )) && ((fromDateInResponse < dateSelectedToTime ) || (toDateInResponse < dateSelectedToTime )) )
            {
                chkValidToAdd = false
                print("Time slot not available")
                break
            }
            else
            {
                if((fromDateInResponse == dateSelectedFromTime) && (toDateInResponse != dateSelectedToTime))
                {
                    chkValidToAdd = false
                    break
                }
                
                 chkValidToAdd = true
                
                 print("Time slot  available")
            }*/
        }
        
        return chkValidToAdd
        
    }
    func stringToDateFormattedUTC(dateToConvert: String, format: String, formatToBeConverted: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        //dateFormatter.timeZone = NSTimeZone.local//TimeZone(identifier: "UTC")
        let date = dateFormatter.date(from:dateToConvert)!
        return date
        
    }
    
    func DateToStringFormattedUTC(dateToConvert: Date, format: String, formatToBeConverted: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        //dateFormatter.timeZone = NSTimeZone.local//TimeZone(identifier: "UTC")
        //let date = dateFormatter.date(from:dateToConvert)!
        let date = dateFormatter.string(from: dateToConvert)
        return date
        
    }
    /*
     -(BOOL)isValidTimeToAdd
     {

     BOOL chkValidToAdd;
     
     chkValidToAdd = NO;
     
     
     
     for (int i=1; i<arrResponseNonBillable.count; i++)
     
     {
    
     //2019-07-12T15:18:00
     
     NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
     
     [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:a"];

     NSDictionary *dict = [arrResponseNonBillable objectAtIndex:i];
     
     NSString *strFromDate= [self ChangeDateToLocalDateClock:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FromDate"]]];
     
     NSString *strToDate= [self ChangeDateToLocalDateClock:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ToDate"]]];
     
     
     
     NSDate *fromDateInResponse,*toDateInResponse;
     
     fromDateInResponse = [dateFormat dateFromString:strFromDate];
     
     toDateInResponse = [dateFormat dateFromString:strToDate];
     
     
     
     //strFromDate = [dateFormat stringFromDate:fromDateInResponse];
     
     // strToDate = [dateFormat stringFromDate:toDateInResponse];
     
     
     
     if ((([fromDateInResponse compare:dateSelectedFromTime] == NSOrderedAscending)&&([dateSelectedFromTime compare:toDateInResponse] == NSOrderedAscending)) || (([fromDateInResponse compare:dateSelectedToTime] == NSOrderedAscending)&&([dateSelectedToTime compare:toDateInResponse] == NSOrderedAscending)))
     
     {
     
     NSLog(@"Time slot not available");
     
     chkValidToAdd = NO;
     
     break;
     
     
     
     }
     
     else
     
     {
     
     NSLog(@"Time slot available");
     
     
     
     chkValidToAdd = YES;
     
     }
     
     
     
     
     }
     
     
     
     
     
     return chkValidToAdd;
     
     }
 */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - ------ Creating Protocol ---------

protocol updateNonBillableHours : class
{
    
    func refreshViewOnupdateNonBillableHoursSelection(dictData : NSDictionary ,tag : Int)
    
}
// MARK: - ------ Table Delgate Extension ---------
extension UpdateNonBillableHours :updateNonBillableHours
{
    func refreshViewOnupdateNonBillableHoursSelection(dictData: NSDictionary, tag: Int)
    {
         if tag == 24
         {
            
            btnDepartment.setTitle(dictData["Name"] as? String, for: .normal)
            strDepartmentSysName = "\(dictData.value(forKey: "SysName")!)" as String
            
            btnTimeSlot.setTitle("Select" , for: .normal)
           // btnFromTime.setTitle("Select", for: .normal)
            //btnToTime.setTitle("Select", for: .normal)
            
        }
        else if tag == 25
        {
            
            
            btnTimeSlot.setTitle(dictData["Title"] as? String, for: .normal)
            
            if btnTimeSlot.currentTitle == ""
            {
                //var strTitle = String()
                //strTitle = "\(dictData["FromTime"]!)" + " - " + "\(dictData["ToTime"]!) "
                
                var strTitle = String()
                var strFromTime = String()
                var strToTime = String()
                
                strFromTime = stringDateToString(toAM_PM: "\(dictData.value(forKey: "FromTime") ?? "")")!
                
                strToTime = stringDateToString(toAM_PM: "\(dictData.value(forKey: "ToTime") ?? "")")!
                
                strTitle = strFromTime + " - " + strToTime
                
                
                
                btnTimeSlot.setTitle(strTitle , for: .normal)

            }
            
            dictSlotData = dictData
            strTimeSlotSelected = "\(dictData.value(forKey: "Title")!)" as String
        }
        else if tag == 26
        {
            
            btnFromTime.setTitle(dictData["Name"] as? String, for: .normal)
            //strUrgencyId = "\(dictData.value(forKeyPath: "UrgencyId")!)"
            
        }
        else if tag == 27
        {
            
            btnToTime.setTitle(dictData["Name"] as? String, for: .normal)
            //strUrgencyId = "\(dictData.value(forKeyPath: "UrgencyId")!)"
            
        }
    }
    
    
}
// MARK: - -----------------------------------Date Picker Protocol-----------------------------------

/*protocol datePickerView : class{
    
    func setDateOnSelction(strDate : String ,tag : Int)
    
}*/

extension UpdateNonBillableHours: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if tag == 10
        {
            
            btnFromTime.setTitle(strDate, for: .normal)
            strFromTimeNonBillable = strDate
            //StrFollowUpDate = strDate
            
           /* NSDateFormatter *dateFormatNew = [[NSDateFormatter alloc] init];
            
            //[dateFormatNew setDateFormat:@"MM/dd/yyyy hh:mm a"];
            [dateFormatNew setDateFormat:@"hh:mm a"];
            
            NSString *strNewDate=[dateFormatNew stringFromDate:pickerDate.date];
            strFromTimeNonBillable=strNewDate;*/
            
        } else if tag == 11 {
            
            btnToTime.setTitle(strDate, for: .normal)
            strToTimeNonBillable = strDate
        }
    }
    
    
   
    
   /* func setDateOnSelction(strDate: String, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 10 {
            
            btnFromTime.setTitle(strDate, for: .normal)
            //StrFollowUpDate = strDate
            
        } else if tag == 11 {
            
            btnToTime.setTitle(strDate, for: .normal)
            
        }
    }*/
}
