//
//  DateTimePicker.swift
//  DPS
//
//  Created by Saavan Patidar on 13/02/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

// MARK: ------ Protocl Declaration ---------

@objc protocol DatePickerProtocol : class
{
    
    func setDateSelctionForDatePickerProtocol(strDate : String , tag : Int)
    
}

class DateTimePicker: UIViewController {
    @IBOutlet weak var heightPicker: NSLayoutConstraint!

    @objc var strTag = Int()
    @objc var strDateGlobal = String()
    @objc var strType = String()
    @objc var dateToSet = Date()
    
    var chkForMinDate = Bool()
    var chkForMaxDate = Bool()

    var minDate = Date()
    var maxDate = Date()
    var chkCustomDate = Bool()

    @IBOutlet var dateTimePicker: UIDatePicker!
    @IBOutlet weak var viewBackground: CardView!
    
    //weak var handleOnSelectionOfDate: datePickerView?
    
    @objc weak var handleDateSelectionForDatePickerProtocol: DatePickerProtocol?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        viewBackground.isHidden = true
        self.view.backgroundColor = UIColor.clear
        if(DeviceType.IS_IPAD){
                 heightPicker.constant = 350.0
                   }else{
                                heightPicker.constant = 235.0

                   }
        if strType == "Date" {
            
            dateTimePicker.datePickerMode = UIDatePicker.Mode.date
            
        } else if strType == "DateTime" {
            
            dateTimePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
            
        }else {
            
            dateTimePicker.datePickerMode = UIDatePicker.Mode.time
            
        }
        
        if dateToSet.description != "" {
            
            dateTimePicker.date = dateToSet
            
        }
        
        if chkForMinDate == true
        {
            dateTimePicker.minimumDate = Date()
        }
        
        if chkForMaxDate == true
        {
            dateTimePicker.maximumDate = Date()
        }
        
        if chkCustomDate {
            
            dateTimePicker.minimumDate = minDate
            dateTimePicker.maximumDate = maxDate
            
        }
        
        self.setDate()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewBackground.animShow()
        
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        viewBackground.animHide()
        self.dismiss(animated: false) { }
        
    }
    
    @IBAction func action_Done(_ sender: UIButton)
    {
        viewBackground.animHide()
        
        print(strDateGlobal)
        
        // self.handleOnSelectionOfDate?.setDateOnSelction(strDate: strDateGlobal, tag: strTag)
        
        self.handleDateSelectionForDatePickerProtocol?.setDateSelctionForDatePickerProtocol(strDate: strDateGlobal, tag: strTag)
        
        
        self.dismiss(animated: false) {}
        
    }
    
    @IBAction func datePickerValueChanged(_ sender: Any) {
        
        self.setDate()
        
    }
    
    func setDate() {
        
        let dateFormatter = DateFormatter()
        
        if strType == "Date" {
            
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFormatter.dateFormat = "dd/MM/yy"
            strDateGlobal = dateFormatter.string(from: dateTimePicker.date)
            
            if DeviceType.IS_IPAD{
                
                strDateGlobal = dateTimeConvertor(str: strDateGlobal, formet: "dd/MM/yy" , strFormatToBeConverted: "MM/dd/yyyy")
                
            }else{
                
                strDateGlobal = dateTimeConvertor(str: strDateGlobal, formet: "dd/MM/yy" , strFormatToBeConverted: "MM/dd/yyyy")
                
                
            }
            
            
        } else if strType == "DateTime" {
            
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"

            strDateGlobal = dateFormatter.string(from: dateTimePicker.date)
            
            
            if DeviceType.IS_IPAD{
                
                strDateGlobal = dateTimeConvertor(str: strDateGlobal, formet: "MM/dd/yyyy hh:mm a" , strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
                
            }
            else
            {
                
                strDateGlobal = dateTimeConvertor(str: strDateGlobal, formet: "MM/dd/yyyy hh:mm a" , strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
                
                
            }
            
            
        }else {
            
            dateFormatter.dateStyle = DateFormatter.Style.none
            dateFormatter.timeStyle = DateFormatter.Style.short
            
            strDateGlobal = dateFormatter.string(from: dateTimePicker.date)
            
            strDateGlobal = dateTimeConvertor(str: strDateGlobal, formet: "hh:mm a" , strFormatToBeConverted: "hh:mm a")
            
        }
        
    }
    
}

