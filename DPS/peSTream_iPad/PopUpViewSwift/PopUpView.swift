//
//  PopUpView.swift
//  GNGPL
//
//  Created by Saavan Patidar on 12/21/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

// tag 4 for source
//Nilind
// tag 24 for update non billable hours department
// tag 28 for update non billable hours department
// tag 29 for Unit Of Measurement of materials
// tag 30 for Method of material
// tag 31 for Equipment of material
// tag 32 for Rate of material
// tag 33 for Wind Direction of material
// tag 34 for Sky Conditions of material
// tag 35 for Responsibility of Condition
// tag 36 for Priority of Conditions
// tag 37 for Reason of InspectDevice
// tag 38 for Type of New Device
// tag 39 for Concentration of Materials
// tag 41 for GeneraInfoVC: Reset Rerason
// tag 40 for GeneraInfoVC: Billing Contact
// tag 43 for GeneraInfoVC: Service Contact
// tag 44 for AddDisclaimerVC: Select Disclaimer
// tag 45 for ProblemIdentification: Select Tip Warranty
// tag 46 for ProblemIdentification: Select Subsecution code
// tag 47 for ProblemIdentification: Select Recommandation code
// tag 48 for Pest Flow Invoice Service Job Description
// tag 49 for WDO Inspection Sub-Area-Access
// tag 50 for WDO Inspection No-Of-Stories
// tag 51 for WDO Inspection External-Material
// tag 52 for WDO Inspection Roofing Material
// tag 53 for WDO Inspection CPC Tag Posted
// tag 54 for WDO Inspection Past PCO Tag Posted
// tag 55 for WDO Inspection Structure Description
// tag 56 for Add Problem Identification General Notes
// tag 57 for WDO Inspection - Tip Warranty- Follow Up Inspection
// tag 58 for Add General Notes
// tag 59 60 61 62 63 for AddLeadProspect View


// tag 200 AddStanNonStanVC: Select Category
// tag 201 AddStanNonStanVC: Select Service
// tag 202 AddStanNonStanVC: Select Service Frequency For Standard Service
// tag 203 AddStanNonStanVC: Select Department
// tag 204 AddStanNonStanVC: Select Service Frequency For NonStandard Service

// tag 500 CreateFolloup, Release Lead:  SelectEmployee
//tag 501 CreateFolloup, Release Lead:  Select Team

//tag 601 Add Contact Source NAme
//tag 602 Add Contact Owner NAme

//tag 801 Add Contact Source NAme

// Sourabh Teg 350 - 400

// tag 351 cover letter
// tag 352 introduction letter
// tag 350 markting content
// tag 353 terms Of service
import UIKit

protocol CustomTableView : class
{
    
    func getDataOnSelection(dictData : NSDictionary ,tag : Int)
    
}
protocol PaymentInfoDetails : class
{
    
    func getPaymentInfo(dictData : String ,index : Int)
    
}

protocol DataProtocol : class {
    func getCoverLetterData(data : Any)
    func getIntroductionLetterData(data : Any)
    func getMarketingContentData(data :  NSMutableArray)
    func getTermsOfServiceData(data : Any)
    
}

class PopUpView: UIViewController {
    
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var viewFortv: CardView!
    @IBOutlet weak var heighttvForList: NSLayoutConstraint!

    
    @objc var strTitle = String()
    @objc var aryTBL = NSMutableArray()
    @objc var aryTBLTimeRange = [[String: Any]]()
    @objc var aryPayment = [String]()
    @objc var strTag = Int()
    @objc var arySelectedItems = NSMutableArray()
    @objc var strIdToShowSelected = ""
    @objc var aryCoverLetter = NSMutableArray()
    @objc var aryIntroductionLetter = NSMutableArray()
    @objc var arrayMarketingContent = NSMutableArray()
    @objc var arrayTermsAndConditionsMultipleSelected = NSMutableArray()
    @objc var arrayTermsOfService = NSMutableArray()
    var dictArrayTermsOfService = [[String: Any]]()
    
    var dictCoverLetter = NSDictionary()
    
    
    weak var delegateReturnData : DataProtocol?
    
    var tbl : UITableView?
    var selectMarketingConternt = [[String : Any]]()
    var strCoverLetter : String?
    var strIntroductionLetter : String?
    var strTermsOfService : String?
    var strStatus : String?
    var strFrom : String?
    @objc var aryOfNoteCodes = [String]()

    weak var handleSelectionOnEditOpportunity: refreshEditopportunity?
    
    weak var handleSelection: updateNonBillableHours?
    
    weak var handelDataSelectionTable: CustomTableView?
    
    weak var handelPaymentTable: PaymentInfoDetails?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        tvForList.estimatedRowHeight = 50.0
        tvForList.tableFooterView = UIView()
        tvForList.dataSource = self
        tvForList.delegate = self
        self.view.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
        self.viewFortv.isHidden = true
        if(DeviceType.IS_IPAD){
            heighttvForList.constant = 400.0
              }else{
                           heighttvForList.constant = 280.0

              }
              
        if strTag == 352 {
            tbl?.reloadData()
        }
        selectMarketingConternt = []

        
        print(arrayTermsOfService)
        
        
    }
    
    override func viewWillLayoutSubviews() {
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.viewFortv.animShow()
        
    }
    
    @IBAction func actionOnBack(_ sender: UIButton)
    {
        self.viewFortv.animShow()
        if(strTag == 4)
        {
            let dictSource = NSMutableDictionary()
            dictSource.setValue(self.arySelectedItems, forKey: "source")
            self.handleSelectionOnEditOpportunity?.refreshViewOnEditOpportunitySelection(dictData: dictSource, tag: self.strTag)
            
        }
        else if(strTag == 103 || strTag == 310 || strTag == 66 )// Marketing Content configure proposal WDO
        {
            let dictSource = NSMutableDictionary()
            dictSource.setValue(self.arySelectedItems, forKey: "multi")
            
            self.handelDataSelectionTable?.getDataOnSelection(dictData: dictSource, tag: self.strTag)
        }
        
        else if(strTag == 105)// terms & conditions configure proposal WDO
        {
            let dictSource = NSMutableDictionary()
            dictSource.setValue(self.arySelectedItems, forKey: "multi")
            
            self.handelDataSelectionTable?.getDataOnSelection(dictData: dictSource, tag: self.strTag)
        }
        else if(strTag == 404 || strTag == 950 || strTag == 951 || strTag == 509 || strTag == 520) // AddActivityVC- participants
        {
            let dictSource = NSMutableDictionary()
            dictSource.setValue(self.arySelectedItems, forKey: "multi")
            
            self.handelDataSelectionTable?.getDataOnSelection(dictData: dictSource, tag: self.strTag)
        }
        else if(strTag == 60)
        {
            let dictSource = NSMutableDictionary()
            dictSource.setValue(self.arySelectedItems, forKey: "source")
            self.handelDataSelectionTable?.getDataOnSelection(dictData: dictSource, tag: self.strTag)
            
        }
        else if(strTag == 1102)
        {
            let dictSource = NSMutableDictionary()
            dictSource.setValue(self.arySelectedItems, forKey: "source")
            self.handelDataSelectionTable?.getDataOnSelection(dictData: dictSource, tag: self.strTag)
            
        }
        else if (strTag == 350)
        {
            print("123456789")
            print(aryTBL.count)
            print(arySelectedItems.count)
            delegateReturnData?.getMarketingContentData(data: arySelectedItems)
         //   self.viewFortv.animShow()
          //  self.dismiss(animated: false) { }
        }
        else if(strTag == 65)
        {
            self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
        }
       
        
        self.dismiss(animated: false) { }
        
    }
    func stringDateToString(toAM_PM strDateTime: String?) -> String?
    {
        var strDateTime = strDateTime
        
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "HH:mm:ss"
        
        let date1: Date? = dateFormat.date(from: strDateTime ?? "")
        
        dateFormat.dateFormat = "hh:mm a"
        
        if let date1 = date1 {
            strDateTime = dateFormat.string(from: date1)
        }
        
        return strDateTime
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PopUpView : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if strTag == 5001{
            return aryTBLTimeRange.count
        }
        else if strTag == 6001{
            return aryPayment.count
        }
        else if strTag == 77777{
            return aryOfNoteCodes.count
        }
        else if strTag == 3333 || strTag == 3334 || strTag == 3335 {
            return aryTBL.count
        }
        
        else  if strTag == 351 {
             return aryCoverLetter.count
         }
         else  if strTag == 352 {
             return aryIntroductionLetter.count
         }
         else if strTag == 353{
             return arrayTermsOfService.count
         }
   
        else{
            return aryTBL.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvForList.dequeueReusableCell(withIdentifier: "popupCell", for: indexPath as IndexPath) as! popupCell
        
        if(DeviceType.IS_IPAD){
            cell.popupCell_lbl_Title.font = UIFont.systemFont(ofSize: 20)
        }else{
              cell.popupCell_lbl_Title.font = UIFont.systemFont(ofSize: 16)
        }
        if aryTBLTimeRange.count > 0 {
            if (self.strTag == 5001){
                
                let strStartInterval = aryTBLTimeRange[indexPath.row]["StartInterval"] as? String ?? ""
                let strEndInterval =  aryTBLTimeRange[indexPath.row]["EndInterval"] as? String ?? ""
                
                cell.popupCell_lbl_Title.text = strStartInterval + "  -  " + strEndInterval
                
            }
        }
        else if aryPayment.count > 0 {
            if strTag == 6001{
                cell.popupCell_lbl_Title.text = aryPayment[indexPath.row]
            }
        }
        else if aryOfNoteCodes.count > 0 {
            if strTag == 77777{
                if (indexPath.row == 0){
                    
                    cell.popupCell_lbl_Title.text = "----Select----"
                    
                }else{
                    cell.popupCell_lbl_Title.text = aryOfNoteCodes[indexPath.row]
                }
            }
        }
        else if strTag == 351 {
            if ((aryCoverLetter[indexPath.row] as AnyObject).value(forKey: "TemplateName") as? String) == strCoverLetter{
                cell.accessoryType = .checkmark
            }else {
                cell.accessoryType = .none
            }
            cell.popupCell_lbl_Title.text = ((aryCoverLetter[indexPath.row] as AnyObject).value(forKey: "TemplateName") as? String)
        }
        else if strTag == 352 {
            
            if ((aryIntroductionLetter[indexPath.row] as AnyObject).value(forKey: "TemplateName") as? String) == strIntroductionLetter{
                cell.accessoryType = .checkmark
            }else {
                cell.accessoryType = .none
            }
            
            cell.popupCell_lbl_Title.text = ((aryIntroductionLetter[indexPath.row] as AnyObject).value(forKey: "TemplateName") as? String)
        }
        
        else if strTag == 353 {
            
            if ((arrayTermsOfService[indexPath.row] as AnyObject).value(forKey: "Title") as? String) == strTermsOfService{
                cell.accessoryType = .checkmark
            }else {
                cell.accessoryType = .none
            }
            cell.popupCell_lbl_Title.text =  ((arrayTermsOfService[indexPath.row] as AnyObject).value(forKey: "Title") as? String)
            
        }
        else{
            
            let dict = aryTBL.object(at: indexPath.row)as? NSDictionary
            if ( self.strTag == 1 || self.strTag == 2 || self.strTag == 3 || self.strTag == 5 || self.strTag == 6 || self.strTag == 8 || self.strTag == 9 || self.strTag == 18 || self.strTag == 19 || self.strTag == 20  || self.strTag == 22 )
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Name")as! String)"
                
            }
                
            else if (strTag == 4){ // for Source Multi Selection
                
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Name")as! String)"
                
            }
                
            else if ( self.strTag == 7 || self.strTag == 14 || self.strTag == 23 ){
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "FullName")as! String)"
                
            } else if ( self.strTag == 17 || self.strTag == 21 ){
                
                if (indexPath.row == 0){
                    
                    cell.popupCell_lbl_Title.text = "----Select----"
                    
                }else{
                    
                    cell.popupCell_lbl_Title.text = Global().strCombinedAddress((dict as! [AnyHashable : Any]))
                    
                }
                
            }
                //Nilind 02 August
            else if(self.strTag == 24 || self.strTag == 25 || self.strTag == 26 || self.strTag == 27)
            {
                if self.strTag == 25
                {
                    cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Title")as! String)"
                    
                    if  cell.popupCell_lbl_Title.text == ""
                    {
                        var strTitle = String()
                        var strFromTime = String()
                        var strToTime = String()
                        
                        strFromTime = stringDateToString(toAM_PM: "\(dict?.value(forKey: "FromTime") ?? "")")!
                        
                        strToTime = stringDateToString(toAM_PM: "\(dict?.value(forKey: "ToTime") ?? "")")!
                        
                        strTitle = strFromTime + " - " + strToTime
                        // strTitle = "\(dict?.value(forKey: "FromTime") ?? "")" + " - " + "\(dict?.value(forKey: "ToTime") ?? "")"
                        
                        cell.popupCell_lbl_Title.text = strTitle
                    }
                    
                }
                else
                {
                    cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Name")as! String)"
                }
                
                
            }
            else if(self.strTag == 106)// Terms Of Service WDO Configure Proposal
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Title") ?? "")")
                
            }
            else if(self.strTag == 29)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "UnitName")as! String)"
            }
            else if(self.strTag == 30)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "ApplicationMethodName")as! String)"
            }
            else if(self.strTag == 31)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Name")as! String)"
            }
            else if(self.strTag == 32)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "ApplicationRateName")as! String)"
            }
            else if(self.strTag == 33)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Text")as! String)"
            }
            else if(self.strTag == 34)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Text")as! String)"
            }
            else if(self.strTag == 35)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "ResponsibilityName")as! String)"
            }
            else if(self.strTag == 36)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Text")as! String)"
            }
            else if(self.strTag == 37)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "DeviceReplacementReason")as! String)"
            }
            else if(self.strTag == 38)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "DeviceTypeName")as! String)"
            }
            else if(self.strTag == 39)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Name")as! String)"
            }
            else if(self.strTag == 28 || self.strTag == 35)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Name")as! String)"
            }
            else if(self.strTag == 41)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "ResetReason")as! String)"
            }
            else if(self.strTag == 43)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "FirstName")as! String) " + "\(dict?.value(forKey: "LastName")as! String)"
            }
            else if(self.strTag == 40)
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "FirstName")as! String) " + "\(dict?.value(forKey: "LastName")as! String)"
            }
            else if (self.strTag == 42)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Name")as! String)"
                
            }
            else if (self.strTag == 44)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Title")as! String)" // TIPWarrantyType
                
            }
            else if (self.strTag == 45)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "TIPWarrantyType")as! String)" // TIPWarrantyType
                
            }
            else if (self.strTag == 46)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "CodeName")as! String)" // TIPWarrantyType
                
            }
            else if (self.strTag == 47)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "CodeName")as! String)" // TIPWarrantyType
                
            }
            else if (self.strTag == 48)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Title")as! String)" // TIPWarrantyType
                
            }
            else if (self.strTag == 49 || self.strTag == 50 || self.strTag == 51 || self.strTag == 52 || self.strTag == 53 || self.strTag == 54 || self.strTag == 55)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Text")as! String)"
                
            }
            else if (self.strTag == 56)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Title")as! String)" // TIPWarrantyType

            }
            else if (self.strTag == 57)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Title")as! String)" // TIPWarrantyType
                
            }
            else if (self.strTag == 67)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "RecommendationChangeReason")as! String)" // TIPWarrantyType
                
            }
            else if ( self.strTag == 68 ){
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Text")as! String)"
                
            }
            else if (self.strTag == 58)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Title")as! String)" // TIPWarrantyType
                
            }
            else if ((self.strTag == 59) || (self.strTag == 250))
            {
                
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }

            }
            else if (self.strTag == 60)
            { // for Source Multi Selection
                
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                
            }
            else if (self.strTag == 61)
            {
                
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "FullName")!)")

            }
            else if (self.strTag == 62)
            {
                
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")

            }
            else if (self.strTag == 63)
            {
                
                var dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                
                dict = removeNullFromDict(dict: dict.mutableCopy() as! NSMutableDictionary)
                
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = Global().strCombinedAddress(dict as? [AnyHashable : Any]))

            }
            else if(self.strTag == 64)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")

            }
            else if(self.strTag == 101)// Cover Letter WDO Configure Proposal
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "TemplateName")! ?? "")"
            }
            else if(self.strTag == 102)// Introduction Letter WDO Configure Proposal
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "TemplateName")! ?? "")"
            }
            else if(self.strTag == 103)// Marketing Content WDO Configure Proposal
            {
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Title")! ?? "")"
            }
            else if(self.strTag == 104)// Terms Of Service WDO Configure Proposal
            {
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Title")! ?? "")"
            }
            else if(self.strTag == 105)// Terms And Conditions WDO Configure Proposal
            {
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "TermsTitle")! ?? "")"
            }
            else if (self.strTag == 200 || self.strTag == 201 || self.strTag == 203)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Name")as! String)" // FrequencyName
                
            }
            else if (self.strTag == 202 || self.strTag == 204)
            {
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "FrequencyName")as! String)" // FrequencyName
                
            }
            else if(self.strTag == 301 )
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "TaskName")!)")

            }
                
            else if(self.strTag == 306){
                
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "FullName")!)")

            }
                
            else if(self.strTag == 307 || self.strTag == 309)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")

            }
                
            else if(self.strTag == 308)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                
                cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)"
            }
            else if(self.strTag == 400)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                
                cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)"
            }
            else if(self.strTag == 403)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
            }
                
            else if(self.strTag == 404)
            {
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "FullName")!)")

            }
            else if(self.strTag == 310)
            {
                
                if(arySelectedItems == aryTBL){
                    indexPath.row == 0 ? (cell.accessoryType = .none) : (cell.accessoryType = .checkmark)
                }else{
                        if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                            cell.accessoryType = .checkmark
                        }else{
                            cell.accessoryType = .none
                        }
                }
               
                
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")

            }
            else if(self.strTag == 500 )
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "FullName") ?? "")"
            }
            else if(self.strTag == 501 )
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Title") ?? "")"
            }
            else if(self.strTag == 502 || self.strTag == 503)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "AreaName")!)")
            }
            else if(self.strTag == 504)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "ConditionGroupName")!)")
            }
            else if(self.strTag == 505)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "ConditionName")!)")
            }
            else if(self.strTag == 506)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "name")!)")
            }
            else if(self.strTag == 507)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "ResponsibilityName")!)")
                
            
            }
            else if(self.strTag == 508)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Text")!)")
            }
            else if(self.strTag == 509)
            {
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "CorrectiveActionName")!)")
            }
            else if(self.strTag == 510)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "location")!)")
            }
            else if(self.strTag == 511)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "title")!)")
            }
            else if(self.strTag == 512)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "ProductCategoryName")!)")
            }
            else if(self.strTag == 513)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "ProductName")!)")
            }
            else if(self.strTag == 514)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "UnitName")!)")
            }
            else if(self.strTag == 515)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "DilutedPercent")!)")
            }
            else if(self.strTag == 516)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "ProductCategoryName")!)")
            }
            else if(self.strTag == 517)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "ProductName")!)")
            }
            else if(self.strTag == 518)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "UnitName")!)")
            }
            else if(self.strTag == 519)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "DilutedPercent")!)")
            }
            else if(self.strTag == 520)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary

                if(indexPath.row != 0 ){
                    let strID = "\(dict.value(forKey: "ServiceReportTermsAndConditionsId")!)"
                    let aryWOTemp = arySelectedItems.filter { (activity) -> Bool in
                        return "\((activity as! NSDictionary).value(forKey: "ServiceReportTermsAndConditionsId")!)".contains(strID)}
                 
                    if(aryWOTemp.count != 0){
                        cell.accessoryType = .checkmark

                    }else{
                        cell.accessoryType = .none
                    }
                }
               
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "TermsTitle")!)")
            }
                // CRMContact New(Add Contact)
            else if(self.strTag == 601 )
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)"
            }
            else if(self.strTag == 602 )
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
               
                cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "FullName")!)"
                
            }
                // CRMContact New(Add Company)
            else if(self.strTag == 701 )
            {
                cell.popupCell_lbl_Title.text = "\(aryTBL.object(at: indexPath.row))"
            }
            else if(self.strTag == 702 )
            {
                cell.popupCell_lbl_Title.text = "\(aryTBL.object(at: indexPath.row))"
            }
            else if(self.strTag == 703 )
            {
                cell.popupCell_lbl_Title.text = "\(aryTBL.object(at: indexPath.row))"
            }
            
            else if(self.strTag == 801 )
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)"
            }
            else if(self.strTag == 901 )// activity details(logtype)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)"
            }
            else if(self.strTag == 1001 )// task details(tasktype)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                
                if indexPath.row == 0 {
                    
                    cell.accessoryType = .none
                    
                }else{
                   
                    if "\(dict.value(forKey: "TaskTypeId")!)" == strIdToShowSelected {
                        
                        cell.accessoryType = .checkmark

                    } else {

                        cell.accessoryType = .none

                    }
                    
                }
                
            }
                // akshay
            else if(self.strTag == 1100 )// edit lead(Industry)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                
            }
            else if(self.strTag == 1101 )// edit lead(Branch)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                
            }
            else if(self.strTag == 1102 )// edit lead(Source)
            {
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                
            }
                
            else if(self.strTag == 1103 )// edit lead(Service)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                
            }
            else if(self.strTag == 1104 )// edit lead(Team)
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Title")!)")
                
            }
            else if(self.strTag == 1105){// edit lead(Employee)
                
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "FullName")!)")
                
            }
            else if(self.strTag == 90 )// convertToOpportunity
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "title")!)")
            }
            else if(self.strTag == 91 || self.strTag == 92 || self.strTag == 93)// Branch department
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
            }
            // by NAvin ------
            else if(self.strTag == 950)// Near by Search
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                               cell.accessoryType = .checkmark
                           }else{
                               cell.accessoryType = .none
                           }
            }
            else if(self.strTag == 951)// Near by Search
                   {
                       let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                       indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "StatusName")!)")
                    if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                        cell.accessoryType = .checkmark
                    }else{
                        cell.accessoryType = .none
                    }
            }
            else if (self.strTag == 405)
                  {
                      
                      let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                      indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Agenda")!)")

                  }
            else if (self.strTag == 65 || self.strTag == 66 )
                  {
                      let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                      indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                  }
            
            else if (self.strTag == 1000)
                  {
                      
                      let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                     // indexPath.row == 0 ? cell.popupCell_lbl_Title.text = "----Select----" : (cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")!)")
                
                    cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")as! String)"


                  }
            
            else if strTag == 3333 || strTag == 3334 || strTag == 3335
            {
                let dict = aryTBL.object(at: indexPath.row) as! NSDictionary
                
                if (dict.value(forKey: "Name") != nil) {
                    cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Name")as! String)"
                }
                else if (dict.value(forKey: "FrequencyName") != nil) {
                    cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "FrequencyName")as! String)"
                }
                else if (dict.value(forKey: "Title") != nil) {
                    cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "Title")as! String)"
                }
                else {
                    cell.popupCell_lbl_Title.text = "\(dict.value(forKey: "AddLeadProspect")as! String)"
                }
            }
            else if(self.strTag == 350)// Marketing Content WDO Configure Proposal
            {
                if(arySelectedItems.contains(aryTBL.object(at: indexPath.row))){ // True
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                
                cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Title")! ?? "")"
            }
        }
        

        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if aryTBLTimeRange.count > 0{
            if(self.strTag == 5001){
                self.handelDataSelectionTable?.getDataOnSelection(dictData: self.aryTBLTimeRange[indexPath.row] as NSDictionary, tag: self.strTag)
                
                self.dismiss(animated: false) {}
            }
        }
        else if aryPayment.count > 0 {
            if strTag == 6001{
                
                self.handelPaymentTable?.getPaymentInfo(dictData: self.aryPayment[indexPath.row], index: indexPath.row)
                
                self.dismiss(animated: false) {}

            }
            
        }
        else if aryOfNoteCodes.count > 0 {
            if strTag == 77777{
                
                if indexPath.row == 0{
                    self.handelPaymentTable?.getPaymentInfo(dictData: "", index: indexPath.row)
                }
                else{
                    self.handelPaymentTable?.getPaymentInfo(dictData: self.aryOfNoteCodes[indexPath.row], index: indexPath.row)
                }
                
                self.dismiss(animated: false) {}

            }
            
        }else if strTag == 351 { // COVER LETTER

            if ((aryCoverLetter[indexPath.row] as AnyObject).value(forKey: "TemplateName") as? String) == strCoverLetter{
                let emptyDictionary = [String: Any]()
                delegateReturnData?.getCoverLetterData(data: emptyDictionary)
                self.viewFortv.animShow()
                self.dismiss(animated: false) { }
            }else {
                let singleDict = aryCoverLetter[indexPath.row]
                delegateReturnData?.getCoverLetterData(data: singleDict)
                self.viewFortv.animShow()
                self.dismiss(animated: false) { }
            }
            
        }
        
        else if strTag == 352 {
 
            if ((aryIntroductionLetter[indexPath.row] as AnyObject).value(forKey: "TemplateName") as? String) == strIntroductionLetter{
                let emptyDictionary = [String: Any]()
                delegateReturnData?.getIntroductionLetterData(data: emptyDictionary)
                self.viewFortv.animShow()
                self.dismiss(animated: false) { }
            }else {
                let singleDict = aryIntroductionLetter[indexPath.row]
                delegateReturnData?.getIntroductionLetterData(data: singleDict)
                self.viewFortv.animShow()
                self.dismiss(animated: false) { }
            }
            
        }
        
        else if strTag == 353 {
            
            if ((arrayTermsOfService[indexPath.row] as AnyObject).value(forKey: "Title") as? String) == strTermsOfService{
                let emptyDictionary = [String: Any]()
                delegateReturnData?.getTermsOfServiceData(data: emptyDictionary)
                self.viewFortv.animShow()
                self.dismiss(animated: false) { }
            }else {
                let singleDict = arrayTermsOfService[indexPath.row]
                delegateReturnData?.getTermsOfServiceData(data: singleDict)
                self.viewFortv.animShow()
                self.dismiss(animated: false) { }
            }
            
        }else if(self.strTag == 350)//ConfigureProposal_WDO Marketing Content  button
        {
            if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
                self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                
            }else{
                self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
            }
            self.tvForList.reloadData()
            
        }
        
        else{
                    
        let dict = aryTBL.object(at: indexPath.row)as? NSDictionary
        DispatchQueue.main.async {
            if ( self.strTag == 1 || self.strTag == 2 || self.strTag == 3 || self.strTag == 5 || self.strTag == 6 || self.strTag == 7 || self.strTag == 8 || self.strTag == 9 || self.strTag == 14 || self.strTag == 17 || self.strTag == 18  || self.strTag == 19 || self.strTag == 23 || self.strTag == 20 || self.strTag == 21 ||  self.strTag == 22 ||  self.strTag == 28 ||  self.strTag == 29 || self.strTag == 30 || self.strTag == 31 || self.strTag == 32 || self.strTag == 33 || self.strTag == 34 || self.strTag == 35 || self.strTag == 36 || self.strTag == 37 || self.strTag == 38 || self.strTag == 39 || self.strTag == 48){
                
                if ( self.strTag == 17 || self.strTag == 21 )
                {
                    
                    if (indexPath.row == 0){
                        
                        self.handleSelectionOnEditOpportunity?.refreshViewOnEditOpportunitySelection(dictData: NSDictionary(), tag: self.strTag)
                        
                    }else{
                        
                        self.handleSelectionOnEditOpportunity?.refreshViewOnEditOpportunitySelection(dictData: dict!, tag: self.strTag)
                        
                    }
                    
                }
                else
                {
                    
                    self.handleSelectionOnEditOpportunity?.refreshViewOnEditOpportunitySelection(dictData: dict!, tag: self.strTag)
                    
                }
                self.dismiss(animated: false) {}
                
            } else if (self.strTag == 4){ // for Source Multi Selection
                
                if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
                    self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                    
                }else{
                    self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
                }
                self.tvForList.reloadData()
            }
                //Nilind 02 August
            else if(self.strTag == 24 || self.strTag == 25 || self.strTag == 26 || self.strTag == 27)
            {
                
                self.handleSelection?.refreshViewOnupdateNonBillableHoursSelection(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
                
            }
            else if(self.strTag == 43 || self.strTag == 40 || self.strTag == 41 || self.strTag == 42 || self.strTag == 44 || self.strTag == 45 || self.strTag == 46 || self.strTag == 47 || self.strTag == 49 || self.strTag == 50 || self.strTag == 51 || self.strTag == 52 || self.strTag == 52 || self.strTag == 53 || self.strTag == 54 || self.strTag == 55 || self.strTag == 56 || self.strTag == 57 || self.strTag == 58 || self.strTag == 67)
            {
                
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
                
            }
            else if(self.strTag == 59 || self.strTag == 61 || self.strTag == 62 || self.strTag == 63 || self.strTag == 90 || self.strTag == 91 || self.strTag == 92 || self.strTag == 93 || self.strTag == 250 || self.strTag == 65)
            {
                var dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                
                dict = removeNullFromDict(dict: dict.mutableCopy() as! NSMutableDictionary)

                indexPath.row == 0 ? self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag) : self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)

                self.dismiss(animated: false) {}
            }
            else if (self.strTag == 60 || self.strTag == 66){ // for Source Multi Selection
                if(indexPath.row == 0){
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                }else{
                    if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
                        self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                        
                    }else{
                        self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
                    }
                    self.tvForList.reloadData()
                }
               
            }
            else if(self.strTag == 64) // CRM New Flow
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                
                indexPath.row == 0 ? self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag) : self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)

                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 68)
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                
                indexPath.row == 0 ? self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag) : self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)

                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 101)//ConfigureProposal_WDO cover letter button
            {
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 102)//ConfigureProposal_WDO Introduction letter button
            {
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 103)//ConfigureProposal_WDO Marketing Content  button
            {
                if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
                    self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                    
                }else{
                    self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
                }
                self.tvForList.reloadData()
                
            }
            else if(self.strTag == 104)//ConfigureProposal_WDO Terms Of Service  button
            {
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
                
            else if(self.strTag == 105)//ConfigureProposal_WDO Terms & Conditions button
            {
                if (self.strFrom == "NewCommercial" && self.strStatus == "Complete")
                {
                    
                }
                else
                {
                    if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
                        self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                        
                    }else{
                        self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
                    }
                    self.tvForList.reloadData()
                }
                
            }
            else if(self.strTag == 200 || self.strTag == 201 || self.strTag == 202 || self.strTag == 203 || self.strTag == 204)
            {
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
      
            else if(self.strTag == 301 || self.strTag == 306 || self.strTag == 307 || self.strTag == 309 ) // Add task
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                
                indexPath.row == 0 ? self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag) : self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)

                self.dismiss(animated: false) {}
            }
                
            else if(self.strTag == 308) // CRM New Flow
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 400) // CRM New Flow
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
                
            else if(self.strTag == 403 || self.strTag == 405)
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag) : self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)

                self.dismiss(animated: false) {}
            }
            else if (self.strTag == 404 || self.strTag == 950 || self.strTag == 951 ) // CRM New Flow
            {
               
                if(indexPath.row == 0){
                         
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                 
                }else{
                
                    if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
               
                        self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                
                    }else{
                        self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
                    }
                    self.tvForList.reloadData()
                }
                /* let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                        self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                        self.dismiss(animated: false) {}*/
                    
             }
            else if(self.strTag == 310) //
            {
                if(indexPath.row == 0){
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                }
                else{
                   
                    if(indexPath.row == 1){
                        if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
                            self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                        }else{
                            self.arySelectedItems = NSMutableArray()
                            self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
                            let dictSource = NSMutableDictionary()
                            dictSource.setValue(self.arySelectedItems, forKey: "multi")
                            self.handelDataSelectionTable?.getDataOnSelection(dictData: dictSource, tag: self.strTag)
                            self.dismiss(animated: false) {}
                        }
                        
                       
                    }else{
                        self.arySelectedItems.remove(self.aryTBL.object(at: 1))
                        if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
                            self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                        }else{
                            self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
                        }
                    }
                    
                    self.tvForList.reloadData()
                }
             }
            else if(self.strTag == 500) // CRM New Flow
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 501) // CRM New Flow
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 502 || self.strTag == 503 || self.strTag == 503 || self.strTag == 504 || self.strTag == 505 || self.strTag == 506 || self.strTag == 507 || self.strTag == 508 || self.strTag == 510 || self.strTag == 511 || self.strTag == 512 || self.strTag == 513 || self.strTag == 514 || self.strTag == 515 || self.strTag == 516 || self.strTag == 517 || self.strTag == 518 || self.strTag == 519)
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                indexPath.row == 0 ? self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag) : self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 509) //
            {
               
                if(indexPath.row == 0){
                         
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                 
                }else{
                
                    if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
               
                        self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                
                    }else{
                        self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
                    }
                    self.tvForList.reloadData()
                }
              
                    
             }
            else if(self.strTag == 520) //
            {
               
                if(indexPath.row == 0){
                         
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                 
                }else{
                
                    let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                    let strID = "\(dict.value(forKey: "ServiceReportTermsAndConditionsId")!)"
                    let aryWOTemp = self.arySelectedItems.filter { (activity) -> Bool in
                        return "\((activity as! NSDictionary).value(forKey: "ServiceReportTermsAndConditionsId")!)".contains(strID)}
                 
                    if(aryWOTemp.count != 0){
                        
                        self.arySelectedItems.remove(aryWOTemp[0])
                        
                    }else{
                        
                        self.arySelectedItems.add(dict)
                        
                    }
                    
                    self.tvForList.reloadData()
                }
             }
            else if(self.strTag == 601) // CRMContact New Flow(Add Contact)
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 602) // CRMContact New Flow(Add Contact)
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            
                // CRMContact New Flow(Add Company)
            else if(self.strTag == 701)
            {
                let str = "\(self.aryTBL.object(at: indexPath.row))"
                
                let dict = ["data" : str] as NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict , tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 702)
            {
                let str = "\(self.aryTBL.object(at: indexPath.row))"
                
                let dict = ["data" : str] as NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 703)
            {
                let str = "\(self.aryTBL.object(at: indexPath.row))"
                
                let dict = ["data" : str] as NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 801) // CRMContact New Flow(Add Company)
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 901) // ActivityDetails(logtype)
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 1001) // TaskDetails(TaskType)
            {
                                
                if (indexPath.row == 0){
                    
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }
                else{
                    
                    let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }
                
            }
            else if(self.strTag == 1100) // Edit Lead(Industry)
            {
                
                if (indexPath.row == 0){
                    
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }
                else{
                    
                    let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }
            }
            else if(self.strTag == 1101) // Edit Lead(Branch)
            {
                if (indexPath.row == 0){
                    
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }  else{
                    
                    let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }
            }
            else if(self.strTag == 1102) // Edit Lead(Source)
            {
                if (indexPath.row == 0){
                    
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                } else{
                    
                    if(self.arySelectedItems.contains(self.aryTBL.object(at: indexPath.row))){ // True
                        self.arySelectedItems.remove(self.aryTBL.object(at: indexPath.row))
                        
                    }else{
                        self.arySelectedItems.add(self.aryTBL.object(at: indexPath.row))
                    }
                    self.tvForList.reloadData()
                    
                }
                
                
            }
            else if(self.strTag == 1103) // Edit Lead(Service)
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 1104) // Edit Lead(Team)
            {
                if (indexPath.row == 0){
                    
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }                    else{
                    
                    let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }
            }
            else if(self.strTag == 1105) // Edit Lead(Employee)
            {
                if (indexPath.row == 0){
                    
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: NSDictionary(), tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }                    else{
                    
                    let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                    self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                    self.dismiss(animated: false) {}
                    
                }
                
            }
            else if(self.strTag == 1000)
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if(self.strTag == 106)
            {
                self.handelDataSelectionTable?.getDataOnSelection(dictData: indexPath.row == 0 ? NSMutableDictionary() : dict!, tag: self.strTag)
                self.dismiss(animated: false) {}
            }
            else if self.strTag == 3333 || self.strTag == 3334 || self.strTag == 3335
            {
                let dict = self.aryTBL.object(at: indexPath.row) as! NSDictionary
                self.handelDataSelectionTable?.getDataOnSelection(dictData: dict, tag: self.strTag)
                self.dismiss(animated: false) {}
            }

        }
            
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
}
class popupCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var popupCell_lbl_Title: UILabel!
}


extension UIView{
    func animShow(){
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animHide(){
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()
                        
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
}
