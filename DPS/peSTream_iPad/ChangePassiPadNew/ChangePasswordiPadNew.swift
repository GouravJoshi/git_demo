//
//  ChangePasswordiPadNew.swift
//  DPS
//
//  Created by Saavan Patidar on 05/03/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
import Alamofire
class ChangePasswordiPadNew: UIViewController {

    
    // MARK: - -----------------------------------IBoutlets-----------------------------------

    @IBOutlet var txtFld_OldPass: ACFloatingTextField!
    @IBOutlet var txtFld_NewPass: ACFloatingTextField!
    @IBOutlet var txtFld_ConfirmPass: ACFloatingTextField!
    @IBOutlet var btn_ChangePass: UIButton!
    @IBOutlet weak var lblPasswordValidation: UILabel!
    
    var isPasswordValid = true
    var allowed = CharacterSet()
    
    // MARK: - -----------------------------------View Life Cycle-----------------------------------

    override func viewDidLoad() {
        if #available(iOS 13.0, *) {
            txtFld_NewPass?.isSecureTextEntry = true
            txtFld_NewPass!.enablePasswordToggle()
            txtFld_ConfirmPass?.isSecureTextEntry = true
            txtFld_ConfirmPass!.enablePasswordToggle()
            
            txtFld_OldPass?.isSecureTextEntry = true
            txtFld_OldPass!.enablePasswordToggle()
           
        } else {
            
        }
        allowed.insert(charactersIn: "!@#$&*")

        var fontSize = CGFloat()
        
        if DeviceType.IS_IPAD {
            
            fontSize = 16.0
            
        }else{
            
            fontSize = 12.0

        }
        
        let attrStr = NSMutableAttributedString (
            string: "Password must be at least 6 characters, and contain at least one upper case letter, one lower case letter, and one number, and one special character.",
            attributes: [
                .font: UIFont.init(name: "Roboto", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize),
                .foregroundColor: UIColor.gray
            ])
        lblPasswordValidation.attributedText = attrStr
        
        super.viewDidLoad()

        self.borderColor()
        
        // Do any additional setup after loading the view.
    }

    // MARK: - -----------------------------------Button Action-----------------------------------

    @IBAction func action_Back(_ sender: Any) {
        
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func action_ChangePass(_ sender: Any) {
        
        self.view.endEditing(true)
        
        
        if isInternetAvailable() {
            
            let ifError = self.validateTxtFld()
            
            let ifNewPassNotValid = validatePassword(password: txtFld_NewPass.text!)
            
            if ifError {
                
                
                
            } else if !ifNewPassNotValid {
                
                showAlertWithoutAnyAction(strtitle: Info, strMessage: PassWordValid, viewcontrol: self)
                
            }
            else {
                
                // hit service
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strCompanyKey = String()
                var strEmailOrUserName = String()

                if dictLoginData.count == 2 {
                    strEmailOrUserName =  "\(defsLogindDetail.value(forKey: "usernameChangePass") ?? "")"
                    strCompanyKey =  "\(defsLogindDetail.value(forKey: "CompanyKeyChangePass") ?? "")"
                  
                }else{
                    if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
                        
                        strCompanyKey = "\(value)"
                    }
                    
                    
                    if let value = dictLoginData.value(forKeyPath: "Company.Username") {
                        
                        strEmailOrUserName = "\(value)"
                    }
                    
                }
           
                
                
                let dictJson = NSMutableDictionary()
                
                dictJson.setValue(strCompanyKey, forKey: "CompanyKey")
                dictJson.setValue(strEmailOrUserName, forKey: "EmailOrUserName")
                dictJson.setValue("Employee", forKey: "LoginUserType")
                dictJson.setValue(txtFld_OldPass.text!.trimmingCharacters(in: .whitespaces), forKey: "OldPassword")
                dictJson.setValue(txtFld_NewPass.text!.trimmingCharacters(in: .whitespaces), forKey: "NewPassword")
                dictJson.setValue(txtFld_ConfirmPass.text!.trimmingCharacters(in: .whitespaces), forKey: "ConfirmPassword")
                
                self.changePassword(dictJson: dictJson)
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }
        
    }
    
    @IBAction func action_HelpPassword(_ sender: Any) {
        
        showAlertWithoutAnyAction(strtitle: Info, strMessage: PassWordValid, viewcontrol: self)
        
    }
    
    
    // MARK: - -----------------------------------Functions-----------------------------------

    func borderColor() {
        
        btn_ChangePass.layer.cornerRadius = 5.0
        btn_ChangePass.layer.borderColor = UIColor.themeColorBlack()?.cgColor
        btn_ChangePass.layer.borderWidth = 0.8
        btn_ChangePass.layer.shadowColor = UIColor.themeColorBlack()?.cgColor
        btn_ChangePass.layer.shadowOpacity = 0.3
        btn_ChangePass.layer.shadowRadius = 3.0
        btn_ChangePass.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
    }
    
    func validateTxtFld() -> Bool {

        var error = false
    
        if txtFld_OldPass.text?.count == 0 {
            
            error = true
            txtFld_OldPass.errorTextColor = UIColor.red
            txtFld_OldPass.errorLineColor = UIColor.red
            txtFld_OldPass.showError(withText: "Please enter old Password")
            
        }
        if txtFld_NewPass.text?.count == 0 {
            
            error = true
            txtFld_NewPass.errorTextColor = UIColor.red
            txtFld_NewPass.errorLineColor = UIColor.red
            txtFld_NewPass.showError(withText: "Please enter new Password")
            
        }
        if txtFld_ConfirmPass.text?.count == 0 {
            
            error = true
            txtFld_ConfirmPass.errorTextColor = UIColor.red
            txtFld_ConfirmPass.errorLineColor = UIColor.red
            txtFld_ConfirmPass.showError(withText: "Please enter confirm Password")
            
        }
        
        if ((txtFld_NewPass.text?.count)!>0) && ((txtFld_ConfirmPass.text?.count)!>0) {
            
            if !(txtFld_NewPass.text == txtFld_ConfirmPass.text){
                
                error = true
                txtFld_NewPass.errorTextColor = UIColor.red
                txtFld_NewPass.errorLineColor = UIColor.red
                txtFld_ConfirmPass.errorTextColor = UIColor.red
                txtFld_ConfirmPass.errorLineColor = UIColor.red
                txtFld_NewPass.showError(withText: "Please enter new & Confirm Password same")
                txtFld_ConfirmPass.showError(withText: "Please enter new & Confirm Password same")
                
            }else if (txtFld_NewPass.text?.count)!<6 {
                
                error = true
                txtFld_NewPass.errorTextColor = UIColor.red
                txtFld_NewPass.errorLineColor = UIColor.red
                txtFld_NewPass.showError(withText: "Please enter atleast 6 digits password")

            }else if (txtFld_ConfirmPass.text?.count)!<6 {
                
                error = true
                txtFld_ConfirmPass.errorTextColor = UIColor.red
                txtFld_ConfirmPass.errorLineColor = UIColor.red
                txtFld_ConfirmPass.showError(withText: "Please enter atleast 6 digits password")
                
            }
            
        }
        
        return error
        
    }
    
    func changePassword(dictJson : NSMutableDictionary) {
        
       
        changePasswordFunction(dictJson: dictJson)
        
    }
    
    
    
    func changePasswordFunction(dictJson : NSDictionary) {
        
        var strUrl = ""
        
        strUrl = "\(MainUrl)\(UrlChangePassword)"
        
        FTIndicator.showProgress(withMessage: "Changing.....", userInteractionEnable: false)
        
        
            if let url = URL(string: strUrl) {
                var request = URLRequest(url: url)
                request.httpMethod = HTTPMethod.post.rawValue
                
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                

                guard let httpBody = try? JSONSerialization.data(withJSONObject: dictJson, options: []) else {
                    return
                }
                request.httpBody = httpBody
                
                AF.request(request)
                    .responseJSON {
                        response in
                        debugPrint(response)
                        DispatchQueue.main.async{
                            
                            FTIndicator.dismissProgress()
                            
                        }
                        switch(response.result) {
                        case .success(_):
                            if response.value is NSDictionary
                            {

                                let dictAddress = response.value as! NSDictionary

                                let IsSucceeded = "\(dictAddress.value(forKey: "IsSucceeded")!)"
//
                                if IsSucceeded == "1" {


                                    let alertController = UIAlertController(title: Info, message: "Password changed successfully", preferredStyle:UIAlertController.Style.alert)
                                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                                    { action -> Void in

                                        nsud.set(self.txtFld_NewPass.text!.trimmingCharacters(in: .whitespaces), forKey: "password")
                                        nsud.synchronize()
                                        self.navigationController?.popViewController(animated: false)

                                    })
                                    self.present(alertController, animated: true, completion: nil)

                                }else {

                                    let arrOfErrors = (dictAddress.value(forKey: "Errors") as! NSArray)

                                    let strErrorMsgToShow = arrOfErrors.componentsJoined(by: ", ")

                                    if strErrorMsgToShow == "Incorrect password." {

                                        self.txtFld_OldPass.errorTextColor = UIColor.red
                                        self.txtFld_OldPass.errorLineColor = UIColor.red
                                        self.txtFld_OldPass.showError(withText: "Please enter correct old Password")

                                       // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strErrorMsgToShow, viewcontrol: self)

                                    }else{

                                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strErrorMsgToShow, viewcontrol: self)

                                    }

                                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strErrorMsgToShow, viewcontrol: self)

                                }

                            }
                            break
                        case .failure(_):
                            print(response.error ?? 0)
                         
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(alertSomeError) ", viewcontrol: self)

                            break
                        }
                }
            }
    }
    
    
    // MARK: - In-Place Validation Helpers
    func setupAttributeColor(if isValid: Bool) -> [NSAttributedString.Key: Any] {
        if isValid {
            return [NSAttributedString.Key.foregroundColor: UIColor.blue]
        } else {
            isPasswordValid = false
            return [NSAttributedString.Key.foregroundColor: UIColor.gray]
        }
    }

    func findRange(in baseString: String, for substring: String) -> NSRange {
        if let range = baseString.localizedStandardRange(of: substring) {
            let startIndex = baseString.distance(from: baseString.startIndex, to: range.lowerBound)
            let length = substring.count
            return NSMakeRange(startIndex, length)
        } else {
            print("Range does not exist in the base string.")
            return NSMakeRange(0, 0)
        }
    }
    
}


// MARK: - -----------------------------------Text Field Delegates Methods-----------------------------------

extension ChangePasswordiPadNew : UITextFieldDelegate{
    
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        var fontSize = CGFloat()
        
        if DeviceType.IS_IPAD {
            
            fontSize = 16.0
            
        }else{
            
            fontSize = 12.0

        }
        
        let attrStr = NSMutableAttributedString (
            string: "Password must be at least 6 characters, and contain at least one upper case letter, one lower case letter, and one number, and one special character.",
            attributes: [
                .font: UIFont.init(name: "Roboto", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize),
                .foregroundColor: UIColor.gray
            ])
        
        if let txt = txtFld_NewPass.text {
                isPasswordValid = true
                attrStr.addAttributes(setupAttributeColor(if: (txt.count >= 6)),
                                      range: findRange(in: attrStr.string, for: "at least 6 characters"))
                attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: CharacterSet.uppercaseLetters) != nil)),
                                      range: findRange(in: attrStr.string, for: "one upper case letter"))
                attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: CharacterSet.lowercaseLetters) != nil)),
                                      range: findRange(in: attrStr.string, for: "one lower case letter"))
                attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: CharacterSet.decimalDigits) != nil)),
                                      range: findRange(in: attrStr.string, for: "one number"))
                attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: allowed) != nil)),
                                      range: findRange(in: attrStr.string, for: "one special character"))
            } else {
                isPasswordValid = false
            }
        
        lblPasswordValidation.attributedText = attrStr
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtFld_OldPass {
            
            txtFld_OldPass.hideError()
            
        }
        if textField == txtFld_NewPass {
            
            txtFld_NewPass.hideError()
            var fontSize = CGFloat()
            
            if DeviceType.IS_IPAD {
                
                fontSize = 16.0
                
            }else{
                
                fontSize = 12.0

            }
            
            let attrStr = NSMutableAttributedString (
                string: "Password must be at least 6 characters, and contain at least one upper case letter, one lower case letter, and one number, and one special character.",
                attributes: [
                    .font: UIFont.init(name: "Roboto", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize),
                    .foregroundColor: UIColor.gray
                ])
            
            if let txt = txtFld_NewPass.text {
                    isPasswordValid = true
                    attrStr.addAttributes(setupAttributeColor(if: (txt.count >= 6)),
                                          range: findRange(in: attrStr.string, for: "at least 6 characters"))
                    attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: CharacterSet.uppercaseLetters) != nil)),
                                          range: findRange(in: attrStr.string, for: "one upper case letter"))
                    attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: CharacterSet.lowercaseLetters) != nil)),
                                          range: findRange(in: attrStr.string, for: "one lower case letter"))
                    attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: CharacterSet.decimalDigits) != nil)),
                                          range: findRange(in: attrStr.string, for: "one number"))
                    attrStr.addAttributes(setupAttributeColor(if: (txt.rangeOfCharacter(from: allowed) != nil)),
                                          range: findRange(in: attrStr.string, for: "one special character"))
                } else {
                    isPasswordValid = false
                }
            
            lblPasswordValidation.attributedText = attrStr
        }
        if textField == txtFld_ConfirmPass {
            
            txtFld_ConfirmPass.hideError()
            
        }
        if ( textField == txtFld_NewPass || textField == txtFld_ConfirmPass || textField == txtFld_OldPass){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 30)
            
        }else {
            
            return true
            
        }
    }
    
}
