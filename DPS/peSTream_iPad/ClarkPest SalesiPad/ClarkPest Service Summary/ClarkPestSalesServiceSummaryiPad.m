//
//  ClarkPestSalesServiceSummaryiPad.m
//  DPS
//
//  Created by Rakesh Jain on 18/09/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "ClarkPestSalesServiceSummaryiPad.h"
#import "AllImportsViewController.h"
#import "SalesAutomationServiceSummaryiPad.h"
#import "SalesAutoServiceSummaryTableViewCell.h"
#import "SalesAutomationAgreementProposal.h"
#import "SalesNonStandardTableViewCell.h"
#import "Global.h"
#import "GlobalSyncViewController.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#import <CoreLocation/CoreLocation.h>
#import "SelectServiceCouponDiscountTableViewCell.h"
@interface ClarkPestSalesServiceSummaryiPad ()<CLLocationManagerDelegate>

@end

@implementation ClarkPestSalesServiceSummaryiPad
{
    UIButton *btnTemp;
    UILabel *lblTemp,*lblBorder,*lblBorder2;
    NSArray *arrOfLeads;
    UIScrollView *scroll4S;
    NSMutableArray *arrStanInitialPrice,*arrStanDiscount,*arrStanMaintenancePrice,*arrStanFrequencyName,*arrStanServiceName,*arrBillingFreqSysName,*arrStanDiscountPercent;
    NSMutableArray *arrNonStanInitialPrice,*arrNonStanDiscount,*arrNonStansServiceName,*arrNonStanDescription,*arrNonStanDiscountPercent;
    BOOL chkStandard;
    double initialStan,initialNonStan,discountStan,discountNonStan,finalPrinceStan,finalPrinceNonStan,taxAmountStan,taxAmountNonStan,billAmountStan,billAmountNonStan;
    NSString *strLeadId;
    NSString *strTax;
    NSMutableArray *arrDepartmentNonStan;
    NSDictionary *dictServiceName;
    NSMutableDictionary *dictDesc;
    BOOL clickSendProposal;
    int heightStan,heigthNonStan,totalHeightStan,totalHeightNonStan,totalHeightStanBundle,heightStanBundle;
    NSString *strBranchSysName;
    BOOL chkStatus;
    //Nilind 5 Jan
    
    NSMutableArray *arrStanCount,*arrNonStanCount,*arrSoldCount,*arrNonStanSoldCount,*arrUnSoldStanCount,*arrUnSoldNonStanCount;
    NSMutableArray *arrSold,*arrUnSold;
    Global *global;
    NSDictionary *dictTaxStatus,*dictFreqNameFromSysName;
    NSString *strIsServiceAddrTaxExempt,*strServiceAddressSubType;
    NSDictionary *dictCommercialStatus,*dictResidentialStatus;
    NSMutableArray *arrStanFinalInitialPrice,*arrStanFinalMaintPrice;
    //.........
    
    NSDictionary *dictPackageName,*dictQuantityStatus;
    NSMutableArray *arrPackageNameId;
    
    
    
    //Nilind 07 Jun ImageCaption
    
    BOOL chkForDuplicateImageSave,isEditedInSalesAuto;;
    
    NSMutableArray *arrNoImage;
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    NSMutableArray *arrOfImageCaption,*arrOfImageDescription,*arrOfImageCaptionGraph,*arrOfImageDescriptionGraph;
    NSMutableArray *arrImagePath,*arrGraphImage;
    NSString *strLeadStatusGlobal,*strUserName,*strCompanyKey,*strServiceUrlMain;
    NSDictionary *dictBundleNameFromId;
    NSMutableArray *arrBundleId;
    
    //Bundle Code
    NSArray *arrMatchesBundle;
    NSArray *arrBundleRow;
    NSMutableArray *arrInitialPriceBundle,*arrDiscountPerBundle,*arrDiscountBundle,*arrMaintenancePriceBundle,*arrFrequencyNameBundle,*arrServiceNameBundle,*arrStanIsSoldBundle,*arrSysNameBundle,*arrUnitBundle,*arrFinalInitialPriceBundle,*arrFinalMaintPriceBundle,*arrPackageNameIdBundle,*arrBillingFreqSysNameBundle;
    NSMutableArray *arrTempBundelDetailId,*arrTempBundleServiceSysName;
    double totalInitialBundle,totalMaintBundle;
    NSString *strSoldStatusBundle;
    NSString *strBundleIdForTextField;
    NSMutableArray *arrStanInitialPriceAllServices,*arrStanFinalInitialPriceAllServices,*arrStanDiscountAllServices,*arrStanServiceNameAllServices;
    
    
    // Nilind 23 Nov
    
    NSMutableArray *arrImageLattitude,*arrImageLongitude,*arrImageGraphLattitude,*arrImageGraphLongitude;
    
    
    NSDictionary *dictYearlyOccurFromFreqSysName;
    NSMutableArray *arrFreqSysName,*arrFreqSysNameBundle;
    NSMutableArray *arrAdditionalParamterDcs,*arrUnit,*arrAddParamInital,*arrBillingFreqSysNameNonStan,*arrDiscountCoupon;
    NSDictionary *dictDiscountNameFromSysName;
    NSMutableArray *arrSoldServiceStandardIdForCoupon;
    
    NSArray *arrSavedServiceDetail,*arrSavedScopeDetail,*arrSavedTargetDetail,*arrSavedInitialPriceDetail,*arrSavedMaintPriceDetail;
    
}
- (void)viewDidLoad
{
    
    global =[[Global alloc]init];
    isEditedInSalesAuto=NO;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strLeadStatusGlobal=[NSString stringWithFormat:@"%@",[defsLogindDetail valueForKey:@"leadStatusSales"]];
    
    if ([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strLeadStatusGlobal caseInsensitiveCompare:@"Completed"] == NSOrderedSame) {
        
        strLeadStatusGlobal=@"Complete";
        
    }
    
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    
    [self getClockStatus];
    
    
    [self serviceTaxableStatus];
    //NIlind 03 May
    [self getFrequencySysNameFromName];
    //End
    //Nilind 05 Jan
    totalHeightStan=0;
    totalHeightNonStan=0;
    totalHeightStanBundle=0;
    
    _btnNewCancel.hidden=YES;
    _btnCancel3.hidden=YES;
    _btnNewSendProposal.hidden=YES;
    arrStanCount=[[NSMutableArray alloc]init];
    arrNonStanCount=[[NSMutableArray alloc]init];
    arrSoldCount=[[NSMutableArray alloc]init];
    arrNonStanSoldCount=[[NSMutableArray alloc]init];
    arrBillingFreqSysName=[[NSMutableArray alloc]init];
    arrUnSoldStanCount=[[NSMutableArray alloc]init];
    arrUnSoldNonStanCount=[[NSMutableArray alloc]init];
    
    arrSold=[[NSMutableArray alloc]init];
    arrUnSold=[[NSMutableArray alloc]init];
    
    //.....
    
    //Nilind 16 Nov
    
    //Nilind  2 Nov
    [self salesFetch];
    NSUserDefaults *dfsStatus=[NSUserDefaults standardUserDefaults];
    chkStatus=[dfsStatus boolForKey:@"status"];
    if (chkStatus==YES)
    {
        _btnSendProposal.enabled=NO;
        _btnGlobalSync.enabled=NO;
    }
    
    //............
    
    heightStan=0;heigthNonStan=0;
    
    clickSendProposal=NO;
    [self serviceName];
    [self getDescriptionBySysname];
    arrDepartmentNonStan=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[defsLead valueForKey:@"LeadId"];
    
    _lblNameAcount.text=[defsLead valueForKey:@"lblName"];
    _lblNameTitle.text=[defsLead valueForKey:@"nameTitle"];
    _lblNameAcount.font=[UIFont boldSystemFontOfSize:22];
    _lblNameTitle.font=[UIFont boldSystemFontOfSize:22];
    
    _lblNameAcount.textAlignment=NSTextAlignmentCenter;
    
    [self salesFetch];
    [self fetchFromCoreDataStandard];
    [self fetchFromCoreDataNonStandard];
    
    
    //.................
    [super viewDidLoad];
    initialStan=0;
    initialNonStan=0;
    discountStan=0;discountNonStan=0;
    finalPrinceStan=0;finalPrinceNonStan=0;
    taxAmountStan=0;taxAmountNonStan=0;
    billAmountStan=0;billAmountNonStan=0;
    
    chkStandard=YES;
    
    //Change End
    NSLog(@"arrStanServiceName>>>>%@\n",arrStanServiceName);
    NSLog(@"arrStanInitialPrice>>>>%@\n",arrStanInitialPrice);
    NSLog(@"arrStanDiscount>>>>%@\n",arrStanDiscount);
    for (int i=0; i<arrStanInitialPriceAllServices.count; i++)//arrStanDiscount
    {
        if([[dictQuantityStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]]isEqualToString:@"0"])
        {
            initialStan=initialStan+[[arrStanInitialPriceAllServices objectAtIndex:i]floatValue];
        }
        else
        {
            initialStan=initialStan+[[arrStanFinalInitialPriceAllServices objectAtIndex:i]floatValue];
        }
        
        discountStan=discountStan+[[arrStanDiscountAllServices objectAtIndex:i]floatValue];
    }
    // Parameter Price Service Calculation
    for (int i=0; i<arrAddParamInital.count; i++)
    {
        initialStan=initialStan+[[arrAddParamInital objectAtIndex:i]floatValue];
    }
    //End
    
    _lblInitialPriceValue.text=[NSString stringWithFormat:@"%.2f",initialStan];
    _lblDiscountValue.text=[NSString stringWithFormat:@"%.2f",discountStan];
    _lblFinalInitialPriceValue.text=[NSString stringWithFormat:@"%.2f",initialStan-discountStan];
    
    //NIlind 8 Oct
    [self fetchLeadDetail];
    if (strTax.length==0)
    {
        strTax=@"0.0";
    }
    //Nilind 1 May Tax Change
    double caluclate,initialStanTax,discountStanTax;
    initialStanTax=0;discountStanTax=0;
    for (int i=0; i<arrStanServiceNameAllServices.count; i++)//arrStanDiscount
    {
        //arrStanServiceName
        /*  Old Code
         if ([[dictTaxStatus valueForKey:[arrStanServiceName objectAtIndex:i]] isEqualToString:@"1"])
         {
         initialStanTax=initialStanTax+[[arrStanInitialPrice objectAtIndex:i]intValue];
         discountStanTax=discountStanTax+[[arrStanDiscount objectAtIndex:i]intValue];
         }
         */
        //Nilind 23 May new change is tax calculation
        
        if ([strIsServiceAddrTaxExempt isEqualToString:@"false"])
        {
            if([strServiceAddressSubType isEqualToString:@"Commercial"])
            {
                if ([[dictCommercialStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]] isEqualToString:@"1"])
                {
                    if([[dictQuantityStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]]isEqualToString:@"0"])
                    {
                        initialStanTax=initialStanTax+[[arrStanInitialPriceAllServices objectAtIndex:i]floatValue];
                    }
                    else
                    {
                        initialStanTax=initialStanTax+[[arrStanFinalInitialPriceAllServices objectAtIndex:i]floatValue];
                    }
                    discountStanTax=discountStanTax+[[arrStanDiscountAllServices objectAtIndex:i]floatValue];
                }
            }
            else if ([strServiceAddressSubType isEqualToString:@"Residential"])
            {
                if ([[dictResidentialStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]] isEqualToString:@"1"])
                {
                    if([[dictQuantityStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]]isEqualToString:@"0"])
                    {
                        initialStanTax=initialStanTax+[[arrStanInitialPriceAllServices objectAtIndex:i]floatValue];
                    }
                    else
                    {
                        initialStanTax=initialStanTax+[[arrStanFinalInitialPriceAllServices objectAtIndex:i]floatValue];
                    }
                    
                    discountStanTax=discountStanTax+[[arrStanDiscountAllServices objectAtIndex:i]floatValue];
                }
                
            }
        }
        else
        {
            
        }
        
    }
    //Nilind
    double taxableAmount;
    taxableAmount=initialStanTax-discountStanTax;
    caluclate=(initialStanTax-discountStanTax)*[strTax doubleValue]/100;
    
    //End
    
    //caluclate=(initialStan-discountStan)*[strTax doubleValue]/100;
    
    _lblTaxAmountValue.text=[NSString stringWithFormat:@"%.2f",caluclate];
    
    //_lblBillingAmountValue.text=_lblFinalInitialPriceValue.text;
    
    double total;
    total=(initialStan-discountStan)+caluclate;
    _lblBillingAmountValue.text=[NSString stringWithFormat:@"%.2f",total];
    //.....................................
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:[NSString stringWithFormat:@"%.5f",initialStan] forKey:@"initialStan"];
    [defs setValue:[NSString stringWithFormat:@"%.5f",discountStan] forKey:@"discountStan"];
    //Nilind 1 May
    [defs setValue:[NSString stringWithFormat:@"%.5f",caluclate] forKey:@"caluclateTax"];
    [defs setValue:[NSString stringWithFormat:@"%.5f",taxableAmount] forKey:@"taxableAmountStan"];
    [defs synchronize];
    [self nonStanSubtotal];
    // Do any additional setup after loading the view.
    if([UIScreen mainScreen].bounds.size.height==480)
    {
        //_constViewFinal_H.constant=143;
        // _constTableTop.constant=5;
        //// _constTableBottom.constant=5;
        // _constViewFinal_T.constant=5;
        // _constViewFinal_T1.constant=5;
    }
    btnTemp=[[UIButton alloc]init];
    lblTemp=[[UILabel alloc]init];
    btnTemp.frame=CGRectMake(0, 0, 150+50, 50);
    lblTemp.frame=CGRectMake(0, 0, 150+50, 50);
    arrOfLeads=[[NSMutableArray alloc]init];
    arrOfLeads=[[NSMutableArray alloc]initWithObjects:@"General Info",@"Inspection",@"Select Service",@"Service Summary",@"Agreement", nil];
    _scrollViewLeads.delegate=self;
    
    
    _scrollViewLeads.delegate=self;
    
    CGFloat scrollWidth=arrOfLeads.count*(160+40+20);
    
    [_scrollViewLeads setFrame:CGRectMake(0,_viewTop.frame.origin.y+_viewTop.frame.size.height,[UIScreen mainScreen].bounds.size.width,_scrollViewLeads.frame.size.height)];
    [_scrollViewLeads setContentSize:CGSizeMake(scrollWidth,100)];
    
    
    _scrollViewLeads.showsHorizontalScrollIndicator = NO;
    lblBorder=[[UILabel alloc]initWithFrame:CGRectMake(0,_scrollViewLeads.contentSize.height-5, (150+50)*4, 3)];
    
    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        lblBorder.frame=CGRectMake(0,_scrollViewLeads.contentSize.height-3, 100*2+5, 2);
    }
    lblBorder.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-5, _scrollViewLeads.frame.size.width+btnTemp.frame.size.width-35, 3)];
    lblBorder2.backgroundColor=[UIColor lightGrayColor];
    
    if([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568)
    {
        //lblBorder.frame=CGRectMake(0,_scrollViewLeads.contentSize.height-3, 100*5+20, 2);
        lblBorder2=[[UILabel alloc]initWithFrame:CGRectMake(lblBorder.frame.origin.x+lblBorder.frame.size.width,_scrollViewLeads.contentSize.height-3, _scrollViewLeads.frame.size.width, 2)];
    }
    
    lblBorder2.backgroundColor=[UIColor lightGrayColor];
    
    [_scrollViewLeads addSubview:lblBorder2];
    [_scrollViewLeads addSubview:lblBorder];
    _scrollViewLeads.backgroundColor=[UIColor whiteColor];
    [_scrollViewLeads addSubview:btnTemp];
    [_scrollViewLeads addSubview:lblTemp];
    [self addButtons];
    _scrollViewLeads.contentOffset = CGPointMake(250,0);
    if([UIScreen mainScreen].bounds.size.height==480 || [UIScreen mainScreen].bounds.size.height==568 )
    {
        _scrollViewLeads.contentOffset = CGPointMake(230,0);
    }
    
    
    
    //Nilind 05 Jan
    [self fetchFromCoreDataStandardNew];
    [self fetchFromCoreDataNonStandardNew];
    
    if(arrSold.count==0 && arrUnSold.count==0)
    {
        _btnSaveFooter.enabled=NO;
        _btnNewCancel.hidden=NO;
        _btnSaveContinue.hidden=YES;
        _btnSendProposal.hidden=YES;
        _btnCancel.hidden=YES;
        _btnCancel3.hidden=YES;
        _btnCancel.hidden=YES;
        
    }
    else
    {
        _btnSaveFooter.enabled=YES;
        if (arrSold.count>0)
        {
            if (arrUnSold.count==0)
            {
                _btnCancel.hidden=NO;
                _btnSaveContinue.hidden=NO;
                
                _btnNewCancel.hidden=YES;
                _btnSendProposal.hidden=YES;
                _btnCancel3.hidden=YES;
                _btnSendProposal.hidden=YES;
            }
            else
            {
                _btnSaveContinue.hidden=NO;
                _btnSendProposal.hidden=NO;
                _btnCancel.hidden=NO;
                _btnNewCancel.hidden=YES;
                _btnCancel3.hidden=YES;
                _btnNewSendProposal.hidden=YES;
                
            }
        }
        else
        {
            if(arrUnSold.count>0)
            {
                _btnSaveContinue.hidden=YES;
                _btnSendProposal.hidden=YES;
                _btnCancel.hidden=YES;
                _btnNewCancel.hidden=YES;
                _btnCancel3.hidden=NO;
                _btnNewSendProposal.hidden=NO;
            }
            
            
        }
        if (arrSold.count==0 && arrUnSold.count>0)
        {
            _btnSaveFooter.enabled=NO;
        }
    }
    
    //Nilind 11 June
    UILabel *lbl=[[UILabel alloc]init] ;
    //CGRect newFrame = lbl.frame;
    //newFrame.size.height = 10;//]WithFrame:CGRectMake(0, 0, 294, 30)];
    totalHeightStan=0;
    lbl.frame=CGRectMake(0, 0, 10, 10);
    for (int i=0; i<arrStanServiceName.count; i++)
    {
        lbl.text=[dictDesc valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceName objectAtIndex:i]]];
        
        CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lbl.lineBreakMode];
        
        totalHeightStan=totalHeightStan+expectedLabelSize.height;
        
    }
    
    for (int i=0; i<arrNonStanDescription.count; i++)
    {
        lbl.text=[dictDesc valueForKey:[NSString stringWithFormat:@"%@",[arrNonStanDescription objectAtIndex:i]]];
        
        CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lbl.lineBreakMode];
        
        totalHeightNonStan=totalHeightNonStan+expectedLabelSize.height;
    }
    NSLog(@"Before%f", _const_Stan_table_H.constant);
    int heightTemp;
    heightTemp=0;
    for (int i=0; i<arrStanServiceName.count; i++)
    {
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrStanServiceName objectAtIndex:i]]];
        if ([str isEqualToString:@"0"])
        {
            //return 227-65;
            //  return tableView.rowHeight-65+10;
            heightTemp=heightTemp+_tblRecord.rowHeight;
            
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                //return 227-65;
                // return tableView.rowHeight-65+10;
                heightTemp=heightTemp+_tblRecord.rowHeight;
                
                
            }
            else
            {
                heightTemp=heightTemp+_tblRecord.rowHeight+65;
            }
        }
        
    }
    
    _const_Stan_table_H.constant=heightTemp+totalHeightStan-arrStanServiceName.count*30;
    NSLog(@"After%f", _const_Stan_table_H.constant);
    _const_NonStan_table_H.constant=((_const_NonStan_table_H.constant+50)*[arrNonStanInitialPrice count])+(totalHeightNonStan/2.5);
    int heightTempBundle=0;
    for (int i=0; i<arrBundleRow.count; i++)
    {
        [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:i]];
        for (int j=0; j<arrSysNameBundle.count; j++)
        {
            NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:j]]];
            if ([str isEqualToString:@"0"])
            {
                //return 227-65;
                //  return tableView.rowHeight-65+10;
                heightTempBundle=heightTempBundle+_tblBundle.rowHeight;
                
            }
            else
            {
                if (str.length==0 || [str isEqualToString:@"(null)"])
                {
                    //return 227-65;
                    // return tableView.rowHeight-65+10;
                    heightTempBundle=heightTempBundle+_tblBundle.rowHeight;
                    
                    
                }
                else
                {
                    heightTempBundle=heightTempBundle+_tblBundle.rowHeight+65;
                }
            }
            
            //description height calculation service bundle
            
            lbl.text=[dictDesc valueForKey:[NSString stringWithFormat:@"%@",[arrSysNameBundle objectAtIndex:j]]];
            
            CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lbl.lineBreakMode];
            
            totalHeightStanBundle=totalHeightStanBundle+expectedLabelSize.height;
            
            
            
        }
    }
    
    //End
    _constTblBundle_H.constant=heightTempBundle+totalHeightStanBundle;
    
    
    //Nilind 14 sEPT
    chkForDuplicateImageSave=NO;
    arrNoImage=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImagePath=[[NSMutableArray alloc]init];
    
    arrImageLattitude=[[NSMutableArray alloc]init];
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    
    [self fetchImageDetailFromDataBase];
    
    //End
    _tblCouponDiscount.rowHeight=UITableViewAutomaticDimension;
    _tblCouponDiscount.estimatedRowHeight=50;
    
    [self fetchFromCoreDataStandard];
    
    if(arrAllObj.count==0)
    {
        _const_TableDiscount_H.constant=0;
        _const_ViewStandardTotal.constant=300-108;
    }
    else
    {
        [self fetchForAppliedDiscountFromCoreData];
        [self calculateDiscountAndOtherDiscount];
    }
    //End
    [self setTabelBordeColor];
    
    [self setHeight];
    
    // Fetching Service Scope and Target From DB
    
    [self fetchFromCoreDataStandardClarkPest];
    [self fetchScopeFromCoreData];
    [self fetchTargetFromCoreData];
    
    float initialPrice = [self fetchLeadCommercialInitialInfoFromCoreData];
    float maintPrice = [self fetchLeadCommercialMaintInfoFromCoreData];
    _txtFieldProposedInitialPrice.text = [NSString stringWithFormat:@"%.2f",initialPrice];
    _txtFieldProposedMaintPrice.text = [NSString stringWithFormat:@"%.2f",maintPrice];
}
-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    [self fetchImageDetailFromDataBaseForGraph];
    
    //12 June
    if([defsClock boolForKey:@"isFromBackAgreement"]==YES)
    {
        [self salesFetch];
        [self fetchFromCoreDataStandard];
        [self fetchFromCoreDataNonStandard];
        
        for (int i=0; i<arrStanInitialPriceAllServices.count; i++)//arrStanDiscount
        {
            if([[dictQuantityStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]]isEqualToString:@"0"])
            {
                initialStan=initialStan+[[arrStanInitialPriceAllServices objectAtIndex:i]floatValue];
            }
            else
            {
                initialStan=initialStan+[[arrStanFinalInitialPriceAllServices objectAtIndex:i]floatValue];
            }
            
            discountStan=discountStan+[[arrStanDiscountAllServices objectAtIndex:i]floatValue];
        }
        // Parameter Price Service Calculation
        for (int i=0; i<arrAddParamInital.count; i++)
        {
            initialStan=initialStan+[[arrAddParamInital objectAtIndex:i]floatValue];
        }
        //End
        
        _lblInitialPriceValue.text=[NSString stringWithFormat:@"%.2f",initialStan];
        _lblDiscountValue.text=[NSString stringWithFormat:@"%.2f",discountStan];
        _lblFinalInitialPriceValue.text=[NSString stringWithFormat:@"%.2f",initialStan-discountStan];
        
        //NIlind 8 Oct
        [self fetchLeadDetail];
        if (strTax.length==0)
        {
            strTax=@"0.0";
        }
        //Nilind 1 May Tax Change
        double caluclate,initialStanTax,discountStanTax;
        initialStanTax=0;discountStanTax=0;
        for (int i=0; i<arrStanServiceNameAllServices.count; i++)//arrStanDiscount
        {
            //arrStanServiceName
            /*  Old Code
             if ([[dictTaxStatus valueForKey:[arrStanServiceName objectAtIndex:i]] isEqualToString:@"1"])
             {
             initialStanTax=initialStanTax+[[arrStanInitialPrice objectAtIndex:i]intValue];
             discountStanTax=discountStanTax+[[arrStanDiscount objectAtIndex:i]intValue];
             }
             */
            //Nilind 23 May new change is tax calculation
            
            if ([strIsServiceAddrTaxExempt isEqualToString:@"false"])
            {
                if([strServiceAddressSubType isEqualToString:@"Commercial"])
                {
                    if ([[dictCommercialStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]] isEqualToString:@"1"])
                    {
                        if([[dictQuantityStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]]isEqualToString:@"0"])
                        {
                            initialStanTax=initialStanTax+[[arrStanInitialPriceAllServices objectAtIndex:i]floatValue];
                        }
                        else
                        {
                            initialStanTax=initialStanTax+[[arrStanFinalInitialPriceAllServices objectAtIndex:i]floatValue];
                        }
                        discountStanTax=discountStanTax+[[arrStanDiscountAllServices objectAtIndex:i]floatValue];
                    }
                }
                else if ([strServiceAddressSubType isEqualToString:@"Residential"])
                {
                    if ([[dictResidentialStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]] isEqualToString:@"1"])
                    {
                        if([[dictQuantityStatus valueForKey:[arrStanServiceNameAllServices objectAtIndex:i]]isEqualToString:@"0"])
                        {
                            initialStanTax=initialStanTax+[[arrStanInitialPriceAllServices objectAtIndex:i]floatValue];
                        }
                        else
                        {
                            initialStanTax=initialStanTax+[[arrStanFinalInitialPriceAllServices objectAtIndex:i]floatValue];
                        }
                        
                        discountStanTax=discountStanTax+[[arrStanDiscountAllServices objectAtIndex:i]floatValue];
                    }
                    
                }
            }
            else
            {
                
            }
            
        }
        //Nilind
        double taxableAmount;
        taxableAmount=initialStanTax-discountStanTax;
        caluclate=(initialStanTax-discountStanTax)*[strTax doubleValue]/100;
        
        //End
        
        //caluclate=(initialStan-discountStan)*[strTax doubleValue]/100;
        
        _lblTaxAmountValue.text=[NSString stringWithFormat:@"%.2f",caluclate];
        
        //_lblBillingAmountValue.text=_lblFinalInitialPriceValue.text;
        
        double total;
        total=(initialStan-discountStan)+caluclate;
        _lblBillingAmountValue.text=[NSString stringWithFormat:@"%.2f",total];
        //.....................................
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:[NSString stringWithFormat:@"%.5f",initialStan] forKey:@"initialStan"];
        [defs setValue:[NSString stringWithFormat:@"%.5f",discountStan] forKey:@"discountStan"];
        //Nilind 1 May
        [defs setValue:[NSString stringWithFormat:@"%.5f",caluclate] forKey:@"caluclateTax"];
        [defs setValue:[NSString stringWithFormat:@"%.5f",taxableAmount] forKey:@"taxableAmountStan"];
        [defs synchronize];
        [self nonStanSubtotal];
        
        
        [self fetchFromCoreDataStandard];
        
        if(arrAllObj.count==0)
        {
            _const_TableDiscount_H.constant=0;
            _const_ViewStandardTotal.constant=300-108;
        }
        else
        {
            [self fetchForAppliedDiscountFromCoreData];
            [self calculateDiscountAndOtherDiscount];
        }
        
    }
    [self getImageCollectionView];//isFromBackAgreement
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//============================================================================
#pragma mark- DYNAMIC LABEL AND BUTTON METHOD
//============================================================================

-(void)addButtons
{
    for(int i =0;i<arrOfLeads.count;i++)
    {
        
        UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake((i)*100+20, 0, 50, 50)];
        lbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)i+1];
        lbl.textColor = [UIColor whiteColor];
        lbl.layer.masksToBounds = YES;
        lbl.layer.cornerRadius = 25;
        lbl.layer.borderWidth=1.0;
        [lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:14]];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        if (i==0)
        {
            btn.frame = CGRectMake(0, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
            lbl.frame=CGRectMake(btnTemp.frame.origin.x+(btnTemp.frame.size.width)/2-15, 5, 50, 50);
            lbl.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
            lbl.textColor =  [UIColor lightGrayColor];//[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
        else
        {
            
            btn.frame = CGRectMake(btnTemp.frame.origin.x+btnTemp.frame.size.width+10, lbl.frame.origin.y+lbl.frame.size.height+5, 150+50, 50);
            lbl.frame=CGRectMake(btn.frame.origin.x+(btn.frame.size.width)/2 - 15, 5, 50, 50);
            lbl.textColor = [UIColor darkGrayColor];
            lbl.backgroundColor=[UIColor whiteColor];
            lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            
        }
        for(int j=0;j<=3;j++)
        {
            if (i==j)
            {
                
                
                lbl.backgroundColor=[UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
                lbl.textColor = [UIColor whiteColor];
                lbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        }
        
        btnTemp.frame=btn.frame;
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)];
        
        //btn.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
        
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
        btn.titleLabel.numberOfLines=2;
        btn.titleLabel.textAlignment=NSTextAlignmentCenter;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = i;
        NSString *strLeadNameUpperCase=[arrOfLeads[i] uppercaseString];
        [btn setTitle:strLeadNameUpperCase forState:UIControlStateNormal];
        // [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollViewLeads addSubview:btn];
        
        [_scrollViewLeads addSubview:lbl];
    }
}


- (IBAction)actionOnBack:(id)sender
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backStan"];
    [defs setBool:NO forKey:@"backNonStandard"];
    [defs setBool:YES forKey:@"isFromBackServiceSummary"];
    [defs setBool:YES forKey:@"fromServiceSummaryScroll"];
    
    
    [defs synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
//============================================================================
#pragma mark- Tableview Delegate Methods
//============================================================================
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag==1)
    {
        NSLog(@"heightStan>>%d",190+heightStan-50);
        //_const_Stan_table_H.constant=_const_Stan_table_H.constant+heightStan-50;
        totalHeightStan=totalHeightStan+heightStan;
        if([[dictQuantityStatus valueForKey:[arrStanServiceName objectAtIndex:indexPath.row]]isEqualToString:@"0"])
        {
            // return 190+heightStan-50+50-40;
            return tableView.rowHeight+heightStan-50+50-40;
        }
        else
        {
            //return 190+heightStan-50+50-15+15;
            return tableView.rowHeight+heightStan-50+50-15+15;
        }
    }
    else if (tableView.tag==2)
    {
        
        NSString *strBundellll=[arrBundleRow objectAtIndex:indexPath.section];
        if (strBundellll.length==0 || [strBundellll isEqualToString:@""] || [strBundellll isEqual:nil])
        {
            
        }
        else
        {
            [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:indexPath.section]];
        }
        
        NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:indexPath.row]]];
        if ([str isEqualToString:@"0"])
        {
            return tableView.rowHeight+heightStan-40;
            
        }
        else
        {
            if (str.length==0 || [str isEqualToString:@"(null)"])
            {
                return tableView.rowHeight+heightStan-40;
                
            }
            else
            {
                return tableView.rowHeight+heightStan;
            }
        }
        
    }
    else if (tableView.tag==101)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==300)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==301)
    {
        return UITableViewAutomaticDimension;
    }
    else if (tableView.tag==302)
    {
        return UITableViewAutomaticDimension;
    }
    else
    {
        NSLog(@"heigthNonStan>>%d",190+heigthNonStan-50);
        totalHeightNonStan=totalHeightNonStan+heightStan;
        return 250+heigthNonStan-50;
        return 250+heigthNonStan-50;
        //return tableView.rowHeight+heigthNonStan-50;
        
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        
        return [arrStanInitialPrice count];//5;//[arrData count];
    }
    else if (tableView.tag==2)
    {
        int k=0;
        NSString *strBundleId=[arrBundleRow objectAtIndex:section];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
        NSArray *arrTempBundle=[dictMasters valueForKey:@"ServiceBundles"];
        NSMutableArray *arrAllBundles;
        arrAllBundles=[[NSMutableArray alloc]init];
        for (int i=0; i<arrTempBundle.count;i++)
        {
            NSDictionary *dict=[arrTempBundle objectAtIndex:i];
            [arrAllBundles addObject:dict];
            
        }
        for (int i=0; i<arrAllBundles.count; i++)
        {
            NSDictionary *dict=[arrAllBundles objectAtIndex:i];
            if ([strBundleId isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceBundleId"]]])
            {
                NSArray *arrServiceBundle=[dict valueForKey:@"ServiceBundleDetails"];
                k=(int)arrServiceBundle.count;
                break;
            }
        }
        /*if (strBundleId.length==0 || [strBundleId isEqualToString:@""] || [strBundleId isEqual:nil])
         {
         
         }
         else
         {
         [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:section]];
         }*/
        return k;
    }
    else if (tableView.tag==101)
    {
        return arrDiscountCoupon.count;
    }
    else if (tableView.tag==300)
    {
        return arrSavedServiceDetail.count;
    }
    else if (tableView.tag==301)
    {
        return arrSavedScopeDetail.count;
    }
    else if (tableView.tag==302)
    {
        return arrSavedTargetDetail.count;
    }
    else
    {
        return [arrNonStanInitialPrice count];
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //cell1.backgroundColor=[UIColor lightGrayColor];
    if (tableView.tag==1)
    {
        
        static NSString *identifier=@"cellId";
        SalesAutoServiceSummaryTableViewCell *
        cell1=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell1==nil)
        {
            cell1=[[SalesAutoServiceSummaryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        
        //cell1.lblMaintenance.text=@"Maintenance Price";
        
        
        if ([UIScreen mainScreen].bounds.size.height==480|| [UIScreen mainScreen].bounds.size.height==568)
        {
            cell1.lblNewDesc.font=[UIFont systemFontOfSize:12];
            
        }
        NSString *strHtmlDesc=[dictDesc valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceName objectAtIndex:indexPath.row]]];
        
        cell1.lblNewDesc.text=[self convertHTML:strHtmlDesc];
        //[dictDesc valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceName objectAtIndex:indexPath.row]]];
        cell1.lblNewDesc.textAlignment=NSTextAlignmentJustified;
        // CGSize expectedLabelSize = [cell1.lblNewDesc.text sizeWithFont:cell1.lblNewDesc.font constrainedToSize: CGSizeMake(cell1.lblNewDesc.frame.size.width, FLT_MAX) lineBreakMode:cell1.lblNewDesc.lineBreakMode];
        
        CGSize expectedLabelSize=[self getLabelHeight:cell1.lblNewDesc];
        cell1.cnstr_lblNewDesc_H.constant=expectedLabelSize.height;
        heightStan=0;
        heightStan=expectedLabelSize.height;
        cell1.lblNewDesc.numberOfLines=220;
        cell1.lblDiscountValue.text=[NSString stringWithFormat:@"$%.2f",[[arrStanDiscount objectAtIndex:indexPath.row]doubleValue]];
        NSString *strPer=@"%";
        cell1.lblDiscoutnPercent.text=[NSString stringWithFormat:@"%.2f%@",[[arrStanDiscountPercent objectAtIndex:indexPath.row]doubleValue],strPer];
        
        
        if ([[arrStanInitialPrice objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblInitialPriceValue.text=@"TBD";
        }
        else
        {
            cell1.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrStanInitialPrice objectAtIndex:indexPath.row]doubleValue]];
        }
        if ([[arrStanMaintenancePrice objectAtIndex:indexPath.row]isEqualToString:@"TBD"])
        {
            cell1.lblMaintenanceValue.text=@"TBD";
        }
        else
        {
            cell1.lblMaintenanceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrStanMaintenancePrice objectAtIndex:indexPath.row]doubleValue]];
        }
        cell1.lblUnitBasedFinalInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrStanFinalInitialPrice objectAtIndex:indexPath.row]doubleValue]];
        cell1.lblUnitBaseFinalMaintPriceVal.text=[NSString stringWithFormat:@"$%.2f",[[arrStanFinalMaintPrice objectAtIndex:indexPath.row]doubleValue]];
        
        double finalPrice;
        
        if([[dictQuantityStatus valueForKey:[arrStanServiceName objectAtIndex:indexPath.row]]isEqualToString:@"0"]) //NonUnit Based
        {
            finalPrice=[[arrStanInitialPrice objectAtIndex:indexPath.row]doubleValue]-[[arrStanDiscount objectAtIndex:indexPath.row]doubleValue];
        }
        else //Unit Based
        {
            finalPrice=[[arrStanFinalInitialPrice objectAtIndex:indexPath.row]doubleValue]-[[arrStanDiscount objectAtIndex:indexPath.row]doubleValue];
        }
        
        cell1.lblFinalInitiaPriceValue.text=[NSString stringWithFormat:@"$%.2f",finalPrice];
        
        cell1.lblFrequencyValue.text=[dictFreqNameFromSysName valueForKey:[arrStanFrequencyName objectAtIndex:indexPath.row]];
        
        //cell1.lblBillingFrequency.text=[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysName objectAtIndex:indexPath.row]];
        //Billing Frequency Formula Calculation
        
        NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysName objectAtIndex:indexPath.row]]];
        if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
        {
            strBillingFreq=@"N/A";
        }
        
        //Billing Frequency Formula Calculation
        
        //NSArray *arrAdditionalPara=[arrAdditionalParamterDcs objectAtIndex:indexPath.row];
        
        float totalParaInitial=0.0,totalParaMaint=0.0;
        /* if(arrAdditionalPara.count>0)
         {
         for (int i=0; i<arrAdditionalPara.count; i++)
         {
         NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
         totalParaInitial=totalParaInitial+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]floatValue];
         totalParaMaint=totalParaMaint+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]floatValue];
         
         }
         }*/
        totalParaInitial=totalParaInitial+([[arrStanInitialPrice objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
        
        totalParaInitial=totalParaInitial-[[arrStanDiscount objectAtIndex:indexPath.row]floatValue];
        
        
        totalParaMaint=totalParaMaint+([[arrStanMaintenancePrice objectAtIndex:indexPath.row]floatValue])*[[arrUnit objectAtIndex:indexPath.row]floatValue];
        
        NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
        strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFreqSysName objectAtIndex:indexPath.row]]];
        strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysName objectAtIndex:indexPath.row]]];
        float totalBillingFreqCharge=0.0;
        
        if ([[arrFreqSysName objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFreqSysName objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
        {
            
            totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
        }
        else
        {
            totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            //  strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
        }
        cell1.lblBillingFrequency.text=strBillingFreq;
        //End
        //Nilind 02 Jun
        //cell1.lblServiceNameType.text=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceName objectAtIndex:indexPath.row]]];
        
        NSString *strServiceWithPackage,*strPackage,*strServiceName;
        strServiceName=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceName objectAtIndex:indexPath.row]]];
        strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameId objectAtIndex:indexPath.row]]];
        if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
        {
            strPackage=@"";
        }
        strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strServiceName,strPackage];
        //For Bundle
        /*
         NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
         if ([strBundle isEqualToString:@"0"])
         {
         }
         else
         {
         strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
         if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
         {
         strPackage=@"";
         }
         strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,strServiceName];
         
         }
         */
        //End
        
        
        cell1.lblServiceNameType.text=strServiceWithPackage;
        
        //End
        
        NSLog(@"%@\n%@\n%@",cell1.lblInitialPriceValue.text,cell1.lblDiscountValue.text,cell1.lblMaintenanceValue.text);
        
        [cell1.lblFrequencyValue setHidden:NO];
        
        [cell1.lblMaintenanceValue setHidden:NO];
        
        if([UIScreen mainScreen].bounds.size.height==480 || [UIScreen mainScreen].bounds.size.height==568)
        {
            cell1.lblInitialPriceValue.font=[UIFont systemFontOfSize:12];
            cell1.lblDiscountValue.font=[UIFont systemFontOfSize:12];
            cell1.lblMaintenanceValue.font=[UIFont systemFontOfSize:12];
            cell1.lblFinalInitiaPriceValue.font=[UIFont systemFontOfSize:12];
            cell1.lblFrequencyValue.font=[UIFont systemFontOfSize:12];
            cell1.lblServiceNameType.font=[UIFont systemFontOfSize:12];
            cell1.lblNewDesc.font=[UIFont systemFontOfSize:12];
        }
        //Nilind 06 June
        if([[dictQuantityStatus valueForKey:[arrStanServiceName objectAtIndex:indexPath.row]]isEqualToString:@"0"])
        {
            cell1.lblUnitBasedFinalMaintPrice.hidden=YES;
            cell1.lblUnitBaseFinalMaintPriceVal.hidden=YES;
            cell1.lblUnitBasedFinalInitialPrice.hidden=YES;
            cell1.lblUnitBasedFinalInitialPriceValue.hidden=YES;
        }
        else
        {
            cell1.lblUnitBasedFinalMaintPrice.hidden=NO;
            cell1.lblUnitBaseFinalMaintPriceVal.hidden=NO;
            cell1.lblUnitBasedFinalInitialPrice.hidden=NO;
            cell1.lblUnitBasedFinalInitialPriceValue.hidden=NO;
        }
        //End
        return  cell1;
        
    }
    else if (tableView.tag==2) //bundle
    {
        
        
        static NSString *identifier=@"cellId";
        SalesAutoServiceSummaryTableViewCell *
        cell1=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell1==nil)
        {
            cell1=[[SalesAutoServiceSummaryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSString *strBundellll=[arrBundleRow objectAtIndex:indexPath.section];
        if (strBundellll.length==0 || [strBundellll isEqualToString:@""] || [strBundellll isEqual:nil])
        {
            
        }
        else
        {
            [self fetchFromCoreDataStandardForBundle:[arrBundleRow objectAtIndex:indexPath.section]];
        }
        
        //cell1.lblMaintenance.text=@"Maintenance Price";
        
        
        if ([UIScreen mainScreen].bounds.size.height==480|| [UIScreen mainScreen].bounds.size.height==568)
        {
            cell1.lblNewDesc.font=[UIFont systemFontOfSize:12];
            
        }
        NSString *strHtmlDesc=[dictDesc valueForKey:[NSString stringWithFormat:@"%@",[arrSysNameBundle objectAtIndex:indexPath.row]]];
        
        cell1.lblNewDesc.text=[self convertHTML:strHtmlDesc];
        
        
        //  cell1.lblNewDesc.text=[dictDesc valueForKey:[NSString stringWithFormat:@"%@",[arrSysNameBundle objectAtIndex:indexPath.row]]];
        
        cell1.lblNewDesc.textAlignment=NSTextAlignmentJustified;
        // CGSize expectedLabelSize = [cell1.lblNewDesc.text sizeWithFont:cell1.lblNewDesc.font constrainedToSize: CGSizeMake(cell1.lblNewDesc.frame.size.width, FLT_MAX) lineBreakMode:cell1.lblNewDesc.lineBreakMode];
        
        CGSize expectedLabelSize=[self getLabelHeight:cell1.lblNewDesc];
        
        
        cell1.cnstr_lblNewDesc_H.constant=expectedLabelSize.height;
        heightStan=0;
        heightStan=expectedLabelSize.height;
        cell1.lblNewDesc.numberOfLines=220;
        cell1.lblDiscountValue.text=[NSString stringWithFormat:@"$%.2f",[[arrDiscountBundle objectAtIndex:indexPath.row]doubleValue]];
        
        NSString *strPer=@"%";
        cell1.lblDiscoutnPercent.text=[NSString stringWithFormat:@"%.2f%@",[[arrDiscountPerBundle objectAtIndex:indexPath.row]doubleValue],strPer];
        if ([[arrInitialPriceBundle objectAtIndex:indexPath.row] isEqualToString:@"TBD"])
        {
            cell1.lblInitialPriceValue.text=@"TBD";
        }
        else
        {
            cell1.lblInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrInitialPriceBundle objectAtIndex:indexPath.row]doubleValue]];
        }
        if ([[arrMaintenancePriceBundle objectAtIndex:indexPath.row]isEqualToString:@"TBD"])
        {
            cell1.lblMaintenanceValue.text=@"TBD";
        }
        else
        {
            cell1.lblMaintenanceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrMaintenancePriceBundle objectAtIndex:indexPath.row]doubleValue]];
        }
        cell1.lblUnitBasedFinalInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalInitialPriceBundle objectAtIndex:indexPath.row]doubleValue]];
        cell1.lblUnitBaseFinalMaintPriceVal.text=[NSString stringWithFormat:@"$%.2f",[[arrFinalMaintPriceBundle objectAtIndex:indexPath.row]doubleValue]];
        
        //cell1.lblBillingFrequency.text=[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]];
        NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
        if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
        {
            strBillingFreq=@"N/A";
        }
        
        
        //Billing Freq Calculation
        
        float totalParaInitial=0.0,totalParaMaint=0.0;
        
        totalParaInitial=totalParaInitial+([[arrInitialPriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
        totalParaInitial=totalParaInitial-[[arrDiscountBundle objectAtIndex:indexPath.row]floatValue];
        
        totalParaMaint=totalParaMaint+([[arrMaintenancePriceBundle objectAtIndex:indexPath.row]floatValue])*[[arrUnitBundle objectAtIndex:indexPath.row]floatValue];
        
        NSString *strServiceFreqYearOccurence,*strBillingFreqYearOccurence;
        strServiceFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrFreqSysNameBundle objectAtIndex:indexPath.row]]];
        strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameBundle objectAtIndex:indexPath.row]]];
        float totalBillingFreqCharge=0.0;
        
        if ([[arrFreqSysNameBundle objectAtIndex:indexPath.row]isEqualToString:@"OneTime"]||[[arrFreqSysNameBundle objectAtIndex:indexPath.row]isEqualToString:@"One Time"])
        {
            
            totalBillingFreqCharge=(totalParaInitial * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            //  strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
            
        }
        else
        {
            totalBillingFreqCharge=(totalParaMaint * [strServiceFreqYearOccurence floatValue])/([strBillingFreqYearOccurence floatValue]);
            
            //  strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
            strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
            
        }
        cell1.lblBillingFrequency.text=strBillingFreq;
        //End
        
        double finalPrice;
        
        if([[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:indexPath.row]]isEqualToString:@"0"]) //NonUnit Based
        {
            finalPrice=[[arrInitialPriceBundle objectAtIndex:indexPath.row]doubleValue]-[[arrDiscountBundle objectAtIndex:indexPath.row]doubleValue];
        }
        else //Unit Based
        {
            finalPrice=[[arrFinalInitialPriceBundle objectAtIndex:indexPath.row]doubleValue]-[[arrDiscountBundle objectAtIndex:indexPath.row]doubleValue];
        }
        
        cell1.lblFinalInitiaPriceValue.text=[NSString stringWithFormat:@"$%.2f",finalPrice];
        
        cell1.lblFrequencyValue.text=[dictFreqNameFromSysName valueForKey:[arrFrequencyNameBundle objectAtIndex:indexPath.row]];
        
        //Nilind 02 Jun
        //cell1.lblServiceNameType.text=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrStanServiceName objectAtIndex:indexPath.row]]];
        
        NSString *strServiceWithPackage,*strPackage,*strServiceName;
        
        strServiceName=[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[arrSysNameBundle objectAtIndex:indexPath.row]]];
        /*strPackage=[dictPackageName valueForKey:[NSString stringWithFormat:@"%@",[arrPackageNameId objectAtIndex:indexPath.row]]];
         if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
         {
         strPackage=@"";
         }
         strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strServiceName,strPackage];
         */
        
        //For Bundle
        
        NSString *strBundle=[arrBundleId objectAtIndex:indexPath.row];
        if ([strBundle isEqualToString:@"0"])
        {
        }
        else
        {
            strPackage=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleId objectAtIndex:indexPath.row]]];
            if ([strPackage isEqualToString:@""]||[strPackage isEqual:nil] || strPackage.length==0)
            {
                strPackage=@"";
            }
            strServiceWithPackage=[NSString stringWithFormat:@"%@: %@",strPackage,strServiceName];
            
        }
        
        //End
        
        
        cell1.lblServiceNameType.text=strServiceWithPackage;
        
        //End
        
        NSLog(@"%@\n%@\n%@",cell1.lblInitialPriceValue.text,cell1.lblDiscountValue.text,cell1.lblMaintenanceValue.text);
        
        [cell1.lblFrequencyValue setHidden:NO];
        
        [cell1.lblMaintenanceValue setHidden:NO];
        
        if([UIScreen mainScreen].bounds.size.height==480 || [UIScreen mainScreen].bounds.size.height==568)
        {
            cell1.lblInitialPriceValue.font=[UIFont systemFontOfSize:12];
            cell1.lblDiscountValue.font=[UIFont systemFontOfSize:12];
            cell1.lblMaintenanceValue.font=[UIFont systemFontOfSize:12];
            cell1.lblFinalInitiaPriceValue.font=[UIFont systemFontOfSize:12];
            cell1.lblFrequencyValue.font=[UIFont systemFontOfSize:12];
            cell1.lblServiceNameType.font=[UIFont systemFontOfSize:12];
            cell1.lblNewDesc.font=[UIFont systemFontOfSize:12];
        }
        //Nilind 06 June
        if([[dictQuantityStatus valueForKey:[arrSysNameBundle objectAtIndex:indexPath.row]]isEqualToString:@"0"])
        {
            cell1.lblUnitBasedFinalMaintPrice.hidden=YES;
            cell1.lblUnitBaseFinalMaintPriceVal.hidden=YES;
            cell1.lblUnitBasedFinalInitialPrice.hidden=YES;
            cell1.lblUnitBasedFinalInitialPriceValue.hidden=YES;
        }
        else
        {
            cell1.lblUnitBasedFinalMaintPrice.hidden=NO;
            cell1.lblUnitBaseFinalMaintPriceVal.hidden=NO;
            cell1.lblUnitBasedFinalInitialPrice.hidden=NO;
            cell1.lblUnitBasedFinalInitialPriceValue.hidden=NO;
        }
        //End
        return  cell1;
        
    }
    else if(tableView.tag==101)
    {
        static NSString *identifier=@"SelectServiceCouponDiscountTableViewCell";
        SelectServiceCouponDiscountTableViewCell *cellCoupon=[_tblCouponDiscount dequeueReusableCellWithIdentifier:identifier];
        if (cellCoupon==nil)
        {
            cellCoupon=[[SelectServiceCouponDiscountTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSManagedObject *dict=[arrDiscountCoupon objectAtIndex:indexPath.row];
        
        cellCoupon.lblCouponName.text=[NSString stringWithFormat:@"%@",[dictDiscountNameFromSysName valueForKey:[dict valueForKey:@"discountSysName"]]];
        
        //cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountAmount"]];
        cellCoupon.lblAmountValue.text=[NSString stringWithFormat:@"$%.2f",[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]]floatValue]];
        
        cellCoupon.lbCouponDescription.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]];
        return cellCoupon;
        
    }
    else if (tableView.tag==300)
    {
        static NSString *identifier=@"ClarkPestSelectService";
        ClarkPestSelectServiceTableViewCell *cellService=[_tblService dequeueReusableCellWithIdentifier:identifier];
        if (cellService==nil)
        {
            cellService=[[ClarkPestSelectServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSManagedObject *matchedServiceDetail=[arrSavedServiceDetail objectAtIndex:indexPath.row];
        cellService.lblServiceName.text=[matchedServiceDetail valueForKey:@"serviceSysName"];
        NSString *strDesc=[matchedServiceDetail valueForKey:@"serviceDescription"];
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil])
        {
            cellService.lblDescription.text=@"N/A";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            cellService.lblDescription.text=[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        
        return cellService;
        
    }
    else if (tableView.tag==301)
    {
        static NSString *identifier=@"ClarkPestSelectServiceScope";
        ClarkPestSelectServiceTableViewCell *cellScope=[_tblScope dequeueReusableCellWithIdentifier:identifier];
        if (cellScope==nil)
        {
            cellScope=[[ClarkPestSelectServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellScope.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSManagedObject *matchedScopeDetail=[arrSavedScopeDetail objectAtIndex:indexPath.row];
        cellScope.lblServiceName.text=[matchedScopeDetail valueForKey:@"scopeSysName"];
        //cellScope.lblDescription.text=@"N/A";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        NSString *strDesc=[matchedScopeDetail valueForKey:@"scopeDescription"];
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil])
        {
            cellScope.lblDescription.text=@"N/A";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            cellScope.lblDescription.text=[matchedScopeDetail valueForKey:@"scopeDescription"];
        }
        
        return cellScope;
        
    }
    else if (tableView.tag==302)
    {
        static NSString *identifier=@"ClarkPestSelectServiceTarget";
        ClarkPestSelectServiceTableViewCell *cellTarget=[_tblTarget dequeueReusableCellWithIdentifier:identifier];
        if (cellTarget==nil)
        {
            cellTarget=[[ClarkPestSelectServiceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cellTarget.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSManagedObject *matchedTargetDetail=[arrSavedTargetDetail objectAtIndex:indexPath.row];
        cellTarget.lblServiceName.text=[matchedTargetDetail valueForKey:@"targetSysName"];
        //dgdf dfg dfgd fgcellTarget.lblDescription.text=@"N/A";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        NSString *strDesc=[matchedTargetDetail valueForKey:@"targetDescription"];
        if(strDesc.length==0 || [strDesc isEqualToString:@""] || [strDesc isEqual:nil])
        {
            cellTarget.lblDescription.text=@"N/A";//[matchedServiceDetail valueForKey:@"serviceDescription"];
        }
        else
        {
            cellTarget.lblDescription.text=[matchedTargetDetail valueForKey:@"targetDescription"];
        }
        return cellTarget;
        
    }
    else //tabletag 3
    {
        
        static NSString *identifier=@"cellIdNonStan";
        SalesNonStandardTableViewCell *
        cellNonStan=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cellNonStan==nil)
        {
            cellNonStan=[[SalesNonStandardTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        
        //For NonStandard Service
        
        cellNonStan.lblNonStanServiceNameType.text=[arrNonStansServiceName objectAtIndex:indexPath.row];
        
        
        //Nilind
        
        if ([UIScreen mainScreen].bounds.size.height==480|| [UIScreen mainScreen].bounds.size.height==568)
        {
            cellNonStan.lblNonStanNewDesc.font=[UIFont systemFontOfSize:12];
            
        }
        cellNonStan.lblNonStanNewDesc.text=[arrNonStanDescription objectAtIndex:indexPath.row];
        cellNonStan.lblNonStanNewDesc.textAlignment=NSTextAlignmentJustified;
        CGSize expectedLabelSize = [cellNonStan.lblNonStanNewDesc.text sizeWithFont:cellNonStan.lblNonStanNewDesc.font constrainedToSize: CGSizeMake(cellNonStan.lblNonStanNewDesc.frame.size.width, FLT_MAX) lineBreakMode:cellNonStan.lblNonStanNewDesc.lineBreakMode];
        //CGSize expectedLabelSize = [cell1.lblNewDesc.text sizeWithFont:cell1.lblNewDesc.font constrainedToSize: CGSizeMake(cell1.lblNewDesc.frame.size.width, FLT_MAX) lineBreakMode:cell1.lblNewDesc.lineBreakMode];
        
        
        cellNonStan.const_lblDescNonStan_H.constant=expectedLabelSize.height+10;
        
        heightStan=0;
        heigthNonStan=0;
        heigthNonStan=expectedLabelSize.height;
        cellNonStan.lblNonStanNewDesc.numberOfLines=220;
        
        
        cellNonStan.lblNonStanInitialPriceValue.text=[NSString stringWithFormat:@"$%.2f",[[arrNonStanInitialPrice objectAtIndex:indexPath.row]doubleValue]];
        
        cellNonStan.lblNonStanDiscountValue.text=[NSString stringWithFormat:@"$%.2f",[[arrNonStanDiscount objectAtIndex:indexPath.row]doubleValue]];
        NSString *strPer=@"%";
        cellNonStan.lblDiscountPercent.text=[NSString stringWithFormat:@"%.2f%@",[[arrNonStanDiscountPercent objectAtIndex:indexPath.row]doubleValue],strPer];
        
        double finalPrice;
        finalPrice=[[arrNonStanInitialPrice objectAtIndex:indexPath.row]doubleValue]-[[arrNonStanDiscount objectAtIndex:indexPath.row]doubleValue];
        cellNonStan.lblNonStanFinalInitiaPriceValue.text=[NSString stringWithFormat:@"$%.2f",finalPrice];
        cellNonStan.lblNonStanFrequencyValue.text=@"One Time";
        
        
        NSLog(@"%@\n%@", cellNonStan.lblNonStanInitialPriceValue.text, cellNonStan.lblNonStanDiscountValue.text);
        
        //Nilind
        
        //Billing Freq Calculation
        NSString *strBillingFreq=[NSString stringWithFormat:@"%@",[dictFreqNameFromSysName valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
        if ([strBillingFreq isEqualToString:@""]||[strBillingFreq isEqual:nil]||[strBillingFreq isEqualToString:@"(null)"]||strBillingFreq.length==0  )
        {
            strBillingFreq=@"N/A";
        }
        NSString *strBillingFreqYearOccurence;
        strBillingFreqYearOccurence=[NSString stringWithFormat:@"%@",[dictYearlyOccurFromFreqSysName valueForKey:[arrBillingFreqSysNameNonStan objectAtIndex:indexPath.row]]];
        if([strBillingFreqYearOccurence isEqualToString:@"(null)"])
        {
            strBillingFreqYearOccurence=@"0";
        }
        float totalBillingFreqCharge=0.0;
        @try {
            totalBillingFreqCharge=([[arrNonStanInitialPrice objectAtIndex:indexPath.row]floatValue]-[[arrNonStanDiscount objectAtIndex:indexPath.row]floatValue]) /[strBillingFreqYearOccurence floatValue];
            // totalBillingFreqCharge=totalBillingFreqCharge-[[arrNonStanDiscount objectAtIndex:indexPath.row]floatValue];
            
        } @catch (NSException *exception) {
            totalBillingFreqCharge=0.0;
        } @finally {
            
        }
        // strBillingFreq=[NSString stringWithFormat:@"%@ ( $%.2f )",strBillingFreq,totalBillingFreqCharge];
        strBillingFreq=[NSString stringWithFormat:@"$%.2f/%@",totalBillingFreqCharge,strBillingFreq];
        
        cellNonStan.lblBillingFreqValue.text=strBillingFreq;
        
        
        cellNonStan.lblNonStanDepartmentName.text=[arrDepartmentNonStan objectAtIndex:indexPath.row];
        
        
        if ([UIScreen mainScreen].bounds.size.height==480|| [UIScreen mainScreen].bounds.size.height==568)
        {
            cellNonStan.lblNonStanServiceNameType.font=[UIFont systemFontOfSize:12];
            //cell1.lblServiceDescriptionValue.font=[UIFont systemFontOfSize:12];
            cellNonStan.lblNonStanInitialPriceValue.font=[UIFont systemFontOfSize:12];
            cellNonStan.lblNonStanDiscountValue.font=[UIFont systemFontOfSize:12];
            cellNonStan.lblNonStanFrequencyValue.font=[UIFont systemFontOfSize:12];
            cellNonStan.lblNonStanDepartmentName.font=[UIFont systemFontOfSize:12];
            
            cellNonStan.lblNonStanNewDesc.font=[UIFont systemFontOfSize:12];
            
        }
        // cell1.const_Desc_H.constant=50;
        //....................................
        return  cellNonStan;
        
    }
}
- (IBAction)actionOnStandardService:(id)sender
{
    // [_btnNonStandard setBackgroundColor:[UIColor //lightGrayColor]];
    // [_btnStandard setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    
    initialStan=0;
    initialNonStan=0;
    discountStan=0;discountNonStan=0;
    finalPrinceStan=0;finalPrinceNonStan=0;
    taxAmountStan=0;taxAmountNonStan=0;
    billAmountStan=0;billAmountNonStan=0;
    chkStandard=YES;
    for (int i=0; i<arrStanDiscount.count; i++)
    {
        
        initialStan=initialStan+[[arrStanInitialPrice objectAtIndex:i]intValue];
        discountStan=discountStan+[[arrStanDiscount objectAtIndex:i]intValue];
        
    }
    _lblInitialPriceValue.text=[NSString stringWithFormat:@"%.2f",initialStan];
    _lblDiscountValue.text=[NSString stringWithFormat:@"%.2f",discountStan];
    _lblFinalInitialPriceValue.text=[NSString stringWithFormat:@"%.2f",initialStan-discountStan];
    _lblBillingAmountValue.text=  _lblFinalInitialPriceValue.text;
    //NIlind 8 Oct
    
    if (strTax.length==0)
    {
        strTax=@"0";
    }
    double caluclate;
    caluclate=(initialStan-discountStan)*[strTax doubleValue]/100;
    
    _lblTaxAmountValue.text=[NSString stringWithFormat:@"%.2f",caluclate];
    
    //_lblBillingAmountValue.text=_lblFinalInitialPriceValue.text;
    
    double total;
    total=(initialStan-discountStan)+caluclate;
    _lblBillingAmountValue.text=[NSString stringWithFormat:@"%.2f",total];
    //.....................................
    
    [self fetchFromCoreDataStandard];
    
    [_tblRecord reloadData];
    [_tblNonStandard reloadData];
}

- (void)nonStanSubtotal
{
    initialStan=0;
    initialNonStan=0;
    discountStan=0;discountNonStan=0;
    finalPrinceStan=0;finalPrinceNonStan=0;
    taxAmountStan=0;taxAmountNonStan=0;
    billAmountStan=0;billAmountNonStan=0;
    chkStandard=NO;
    for (int i=0; i<arrNonStanInitialPrice.count; i++)
    {
        initialNonStan=initialNonStan+[[arrNonStanInitialPrice objectAtIndex:i]floatValue];
        discountNonStan=discountNonStan+[[arrNonStanDiscount objectAtIndex:i]floatValue];
        
    }
    _lblInitialPriceNonStan.text=[NSString stringWithFormat:@"%.2f",initialNonStan];
    _lblDiscountNonStan.text=[NSString stringWithFormat:@"%.2f",discountNonStan];
    _llblFinalPriceNonStan.text=[NSString stringWithFormat:@"%.2f",initialNonStan-discountNonStan];
    _lblBillingAmountNonStan.text=  _llblFinalPriceNonStan.text;
    
    //NIlind 8 Oct
#pragma mark- Nilind 25 Oct
    
    if ([strIsServiceAddrTaxExempt isEqualToString:@"false"])
    {
    }
    else
    {
        strTax=@"0";
    }
    if (strTax.length==0)
    {
        strTax=@"0";
    }
    double caluclate,taxabaleAmountNonStan;
    caluclate=(initialNonStan-discountNonStan)*[strTax doubleValue]/100;
    taxabaleAmountNonStan=initialNonStan-discountNonStan;
    _lblTaxAmountNonStan.text=[NSString stringWithFormat:@"%.2f",caluclate];
    
    //_lblBillingAmountValue.text=_lblFinalInitialPriceValue.text;
    
    double total;
    total=(initialNonStan-discountNonStan)+caluclate;
    _lblBillingAmountNonStan.text=[NSString stringWithFormat:@"%.2f",total];
    //.....................................
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:[NSString stringWithFormat:@"%.5f",initialNonStan] forKey:@"initialNonStan"];
    [defs setValue:[NSString stringWithFormat:@"%.5f",discountNonStan] forKey:@"discountNonStan"];
    [defs setValue:[NSString stringWithFormat:@"%.5f",caluclate]  forKey:@"taxNonStandard"];
    [defs setValue:[NSString stringWithFormat:@"%.5f",taxabaleAmountNonStan]  forKey:@"taxabaleAmountNonStan"];
    [defs synchronize];
    
    [self fetchFromCoreDataNonStandard];
    [_tblRecord reloadData];
    [_tblNonStandard reloadData];
}

- (IBAction)actionOnSave:(id)sender
{
    
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame)
    {
        
    }
    else
    {
        [self saveImageToCoreData];
        
    }
    clickSendProposal=NO;
    [self updateLeadIdDetail];
    [global updateSalesZSYNC:strLeadId :@"yes"];
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesClarkPestiPad" bundle:nil];
    ClarkPestSalesAgreementProposaliPad *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"ClarkPestSalesAgreementProposaliPad"];
    objSalesAutomationAgreementProposal.strFromSummary=@"FromSummary";
    [self.navigationController pushViewController:objSalesAutomationAgreementProposal animated:YES];
    
}

- (IBAction)actionOnCancel:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backStan"];
    [defs setBool:NO forKey:@"backNonStandard"];
    [defs setBool:YES forKey:@"isFromBackServiceSummary"];
    [defs setBool:YES forKey:@"fromServiceSummaryScroll"];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- 31 Aug Nilind
#pragma mark- FETCH CORE DATA
-(void)fetchFromCoreDataStandard
{
    /*
     
     //[self deleteFromCoreDataSalesInfo];
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
     requestNew = [[NSFetchRequest alloc] init];
     [requestNew setEntity:entitySoldServiceStandardDetail];
     NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
     
     [requestNew setPredicate:predicate];
     
     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
     sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
     
     [requestNew setSortDescriptors:sortDescriptors];
     
     self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
     [self.fetchedResultsControllerSalesInfo setDelegate:self];
     
     // Perform Fetch
     NSError *error1 = nil;
     [self.fetchedResultsControllerSalesInfo performFetch:&error1];
     //arrAllObj=[[NSArray alloc]init];
     arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
     
     arrStanInitialPrice=[[NSMutableArray alloc]init];
     arrStanDiscount=[[NSMutableArray alloc]init];
     arrStanMaintenancePrice=[[NSMutableArray alloc]init];
     arrStanFrequencyName=[[NSMutableArray alloc]init];
     arrStanServiceName=[[NSMutableArray alloc]init];
     arrStanCount=[[NSMutableArray alloc]init];
     arrSoldCount=[[NSMutableArray alloc]init];
     arrStanFinalInitialPrice=[[NSMutableArray alloc]init];
     arrStanFinalMaintPrice=[[NSMutableArray alloc]init];
     arrPackageNameId=[[NSMutableArray alloc]init];
     arrBillingFreqSysName=[[NSMutableArray alloc]init];
     arrStanDiscountPercent=[[NSMutableArray alloc]init];
     arrStanInitialPriceAllServices=[[NSMutableArray alloc]init];
     arrStanFinalInitialPriceAllServices=[[NSMutableArray alloc]init];
     arrStanDiscountAllServices=[[NSMutableArray alloc]init];
     arrStanServiceNameAllServices=[[NSMutableArray alloc]init];
     //arrBundleId=[[NSMutableArray alloc]init];
     //arrBundleId=[[NSMutableArray alloc]init];
     NSMutableArray *arrBundleNew;
     arrBundleNew=[[NSMutableArray alloc]init];
     
     arrTempBundelDetailId=[[NSMutableArray alloc]init];
     arrTempBundleServiceSysName=[[NSMutableArray alloc]init];
     arrBundleRow=[[NSMutableArray alloc]init];
     arrAdditionalParamterDcs=[[NSMutableArray alloc]init];
     arrUnit=[[NSMutableArray alloc]init];
     arrAddParamInital=[[NSMutableArray alloc]init];
     arrFreqSysName=[[NSMutableArray alloc]init];
     
     arrSoldServiceStandardIdForCoupon=[[NSMutableArray alloc]init];
     
     if (arrAllObj.count==0)
     {
     
     }else
     {
     for (int k=0; k<arrAllObj.count; k++)
     {
     matches=arrAllObj[k];
     NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
     if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:@""])
     {
     [arrAdditionalParamterDcs addObject:[matches valueForKey:@"additionalParameterPriceDcs"]];
     [arrUnit addObject:[matches valueForKey:@"unit"]];
     
     NSArray *arrAdditionalPara=[matches valueForKey:@"additionalParameterPriceDcs"];
     float totalParaInitial=0.0,totalParaMaint=0.0;
     if(arrAdditionalPara.count>0)
     {
     for (int i=0; i<arrAdditionalPara.count; i++)
     {
     NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
     totalParaInitial=totalParaInitial+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]floatValue];
     totalParaMaint=totalParaMaint+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalMaintUnitPrice"]]floatValue];
     
     }
     totalParaInitial=totalParaInitial+[[matches valueForKey:@"initialPrice"]floatValue];
     totalParaMaint=totalParaMaint+[[matches valueForKey:@"maintenancePrice"]floatValue];
     [arrStanInitialPrice addObject:[NSString stringWithFormat:@"%.2f",totalParaInitial]];
     
     [arrStanMaintenancePrice addObject:[NSString stringWithFormat:@"%.2f",totalParaMaint]];
     }
     else
     {
     [arrStanInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
     
     [arrStanMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
     }
     
     
     //[arrStanInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
     //[arrStanMaintenancePrice addObject:[matches valueForKey:@"maintenancePrice"]];
     
     
     [arrStanFinalInitialPrice addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
     
     [arrStanDiscount addObject:[matches valueForKey:@"discount"]];
     
     [arrStanFinalMaintPrice addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
     
     [arrStanFrequencyName addObject:[matches valueForKey:@"serviceFrequency"]];
     [arrFreqSysName addObject:[matches valueForKey:@"frequencySysName"]];
     
     [arrStanServiceName addObject:[matches valueForKey:@"serviceSysName"]];
     [arrBillingFreqSysName addObject:[matches valueForKey:@"billingFrequencySysName"]];
     [arrStanCount addObject:[NSString stringWithFormat:@"%d",k]];
     
     //Nilind 06 Jan
     if([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isSold"]]isEqualToString:@"true"])
     {
     [arrSoldCount addObject:[NSString stringWithFormat:@"%d",k]];
     }
     //..............
     //Nilind 2 Jun
     [arrPackageNameId addObject:[matches valueForKey:@"packageId"]];
     [arrStanDiscountPercent addObject:[matches valueForKey:@"discountPercentage"]];
     //[arrBundleId addObject:[matches valueForKey:@"bundleId"]];
     
     //End
     }
     else
     {
     [arrBundleNew addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]];
     
     if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"BundleDetailId"]] isEqualToString:@"0"]||[[NSString stringWithFormat:@"%@",[matches valueForKey:@"BundleDetailId"]] isEqual:nil])
     {
     
     }
     else
     {
     //[arrTempBundleId addObject:[matches valueForKey:@"bundleId"]];
     [arrTempBundelDetailId addObject:[matches valueForKey:@"bundleDetailId"]];
     [arrTempBundleServiceSysName addObject:[matches valueForKey:@"serviceSysName"]];
     
     
     }
     
     }
     // Paramter Serivce Initial
     NSArray *arrAdditionalPara=[matches valueForKey:@"additionalParameterPriceDcs"];
     for(int i=0;i<arrAdditionalPara.count;i++)
     {
     NSDictionary *dict=[arrAdditionalPara objectAtIndex:i];
     [arrAddParamInital addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FinalInitialUnitPrice"]]];
     }
     //End
     
     //get all services
     [arrStanInitialPriceAllServices addObject:[matches valueForKey:@"initialPrice"]];
     [arrStanFinalInitialPriceAllServices addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
     [arrStanDiscountAllServices addObject:[matches valueForKey:@"discount"]];
     [arrStanServiceNameAllServices addObject:[matches valueForKey:@"serviceSysName"]];
     
     [arrSoldServiceStandardIdForCoupon addObject:[matches valueForKey:@"soldServiceStandardId"]];
     
     }
     arrBundleRow = [[NSSet setWithArray:arrBundleNew] allObjects];
     // [self fetchFromCoreDataStandard];
     [_tblBundle reloadData];
     [_tblRecord reloadData];
     [_tblNonStandard reloadData];
     }
     
     */
}

-(void)fetchFromCoreDataNonStandard
{
    
    /*
     //[self deleteFromCoreDataSalesInfo];
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
     requestNew = [[NSFetchRequest alloc] init];
     [requestNew setEntity:entitySoldServiceNonStandardDetail];
     NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
     
     [requestNew setPredicate:predicate];
     
     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
     sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
     
     [requestNew setSortDescriptors:sortDescriptors];
     
     self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
     [self.fetchedResultsControllerSalesInfo setDelegate:self];
     
     // Perform Fetch
     NSError *error1 = nil;
     [self.fetchedResultsControllerSalesInfo performFetch:&error1];
     //arrAllObj=[[NSArray alloc]init];
     arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
     
     arrNonStanInitialPrice=[[NSMutableArray alloc]init];
     arrNonStanDiscount=[[NSMutableArray alloc]init];
     arrNonStansServiceName=[[NSMutableArray alloc]init];
     arrNonStanDescription=[[NSMutableArray alloc]init];
     arrDepartmentNonStan=[[NSMutableArray alloc]init];
     arrNonStanDiscountPercent=[[NSMutableArray alloc]init];
     //Nilind 10 Oct
     arrBillingFreqSysNameNonStan=[[NSMutableArray alloc]init];
     
     NSDictionary *dictForDepartment;
     NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
     NSDictionary *dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
     
     //NSArray *arrDept=[dictSalesLeadMaster valueForKey:@"DepartmentMasters"];
     //Nilind 14 Nov
     
     NSMutableArray *arrDept;
     NSMutableArray *arrName,*arrSysName;
     arrName=[[NSMutableArray alloc]init];
     arrSysName=[[NSMutableArray alloc]init];
     arrDept=[[NSMutableArray alloc]init];
     NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
     for (int i=0;i<arrDeptName.count; i++)
     {
     NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
     NSArray *arry=[dictBranch valueForKey:@"Departments"];
     for (int j=0; j<arry.count; j++)
     {
     NSDictionary *dict=[arry objectAtIndex:j];
     if ([strBranchSysName isEqualToString:[dictBranch valueForKey:@"SysName"]])
     {
     //[arrDept addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
     [arrName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
     [arrSysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
     }
     
     }
     
     }
     dictForDepartment = [NSDictionary dictionaryWithObjects:arrName forKeys:arrSysName];
     //............................
     
     arrNonStanCount=[[NSMutableArray alloc]init];
     if (arrAllObj.count==0)
     {
     
     }else
     {
     for (int k=0; k<arrAllObj.count; k++)
     {
     matches=arrAllObj[k];
     NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
     [arrNonStanInitialPrice addObject:[matches valueForKey:@"initialPrice"]];
     [arrNonStanDiscount addObject:[matches valueForKey:@"discount"]];
     [arrNonStansServiceName addObject:[matches valueForKey:@"serviceName"]];
     [arrNonStanDescription addObject:[matches valueForKey:@"serviceDescription"]];
     // [arrDepartmentNonStan addObject:[matches valueForKey:@"departmentSysname"]];
     if ([[matches valueForKey:@"departmentSysname"] isEqualToString:@""])
     {
     [arrDepartmentNonStan addObject:@"N/A"];
     }
     else
     {
     [arrDepartmentNonStan addObject:[dictForDepartment valueForKey:[matches valueForKey:@"departmentSysname"]]];
     }
     [arrNonStanCount addObject:[NSString stringWithFormat:@"%d",k]];
     [arrNonStanDiscountPercent addObject:[matches valueForKey:@"discountPercentage"]];
     [arrBillingFreqSysNameNonStan addObject:[matches valueForKey:@"billingFrequencySysName"]];
     
     
     }
     [_tblRecord reloadData];
     [_tblNonStandard reloadData];
     
     }
     
     */
    
}
//Nilind 8 Oct
#pragma mark- FETCHING ALL RECORDS CORE DATA
-(void)fetchLeadDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            strTax=[matches valueForKey:@"tax"];
            strIsServiceAddrTaxExempt=[matches valueForKey:@"isServiceAddrTaxExempt"];
            strServiceAddressSubType=[matches valueForKey:@"serviceAddressSubType"];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
        }
    }
}


- (IBAction)actinoOnSendProposal:(id)sender
{
    clickSendProposal=YES;
    [self updateLeadIdDetail];
    [global updateSalesZSYNC:strLeadId :@"yes"];
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
    SalesAutomationAgreementProposaliPad *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"SalesAutomationAgreementProposaliPad"];
    objSalesAutomationAgreementProposal.strSummarySendPropsal=@"strSummarySendPropsal";
    [self.navigationController pushViewController:objSalesAutomationAgreementProposal animated:YES];
}

//Nilind  3 Nov
-(void)getDescriptionBySysname
{
    NSDictionary* dictMasters;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    NSArray *arrCtgry=[dictMasters valueForKey:@"Categories"];
    NSMutableArray *arrSysName,*arrDesc;
    arrSysName=[[NSMutableArray alloc]init];
    arrDesc=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrCtgry.count; i++)
    {
        NSDictionary *dict=[arrCtgry objectAtIndex:i];
        
        NSArray *arrServices=[dict valueForKey:@"Services"];
        for (int j=0; j<arrServices.count; j++)
        {
            NSDictionary *dict=[arrServices objectAtIndex:j];
            [arrSysName addObject:[dict valueForKey:@"SysName"]];
            [arrDesc addObject:[dict valueForKey:@"Description"]];
            
        }
    }
    dictDesc=[[NSMutableDictionary alloc]init];
    dictDesc=[NSMutableDictionary dictionaryWithObjects:arrDesc forKeys:arrSysName];
    // NSLog(@"dictDescdictDesc>>>>%@",dictDesc);
}
-(void)serviceName
{
    
    NSMutableArray *name,*sysName,*arrPackageId,*arrPackageName,*qtyVal;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    arrPackageId=[[NSMutableArray alloc]init];
    arrPackageName=[[NSMutableArray alloc]init];
    qtyVal=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                [name addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]]];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [qtyVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsUnitBasedService"]]];
                NSArray *arrPackage=[dict valueForKey:@"ServicePackageDcs"];
                
                for (int k=0; k<arrPackage.count; k++)
                {
                    NSDictionary *dictPackage=[arrPackage objectAtIndex:k];
                    [arrPackageId addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"ServicePackageId"]]];
                    [arrPackageName addObject:[NSString stringWithFormat:@"%@",[dictPackage valueForKey:@"PackageName"]]];
                }
                
                
            }
        }
    }
    dictQuantityStatus = [NSDictionary dictionaryWithObjects:qtyVal forKeys:sysName];
    
    dictServiceName = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
    dictPackageName =[NSDictionary dictionaryWithObjects:arrPackageName forKeys:arrPackageId];
    NSLog(@"PaymentInfo%@",dictServiceName);
    //For Bundle
    NSArray *arrBundleData=[dictMasters valueForKey:@"ServiceBundles"];
    NSMutableArray *nameBundle,*keyBundleId;
    nameBundle=[[NSMutableArray alloc]init];
    keyBundleId=[[NSMutableArray alloc]init];
    for (int i=0; i<arrBundleData.count; i++)
    {
        NSDictionary *dict=[arrBundleData objectAtIndex:i];
        {
            [nameBundle addObject:[dict valueForKey:@"BundleName"]];
            [keyBundleId addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ServiceBundleId"]]];
        }
    }
    dictBundleNameFromId =[NSDictionary dictionaryWithObjects:nameBundle forKeys:keyBundleId];
    //For Discount
    NSArray *arrForDiscount;
    NSMutableArray *valNameDiscount,*keySysNameDiscount;
    valNameDiscount=[[NSMutableArray alloc]init];
    keySysNameDiscount=[[NSMutableArray alloc]init];
    arrForDiscount=[dictMasters valueForKey:@"DiscountSetupMasterCoupon"];
    for (int i=0; i<arrForDiscount.count; i++)
    {
        NSDictionary *dict=[arrForDiscount objectAtIndex:i];
        {
            [valNameDiscount addObject:[dict valueForKey:@"Name"]];
            [keySysNameDiscount addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
        }
    }
    dictDiscountNameFromSysName =[NSDictionary dictionaryWithObjects:valNameDiscount forKeys:keySysNameDiscount];
    
    
}
//Nilind 1 May
-(void)serviceTaxableStatus
{
    
    NSMutableArray *name,*sysName,*commercialVal,*residentialVal;
    name=[[NSMutableArray alloc]init];
    sysName=[[NSMutableArray alloc]init];
    commercialVal=[[NSMutableArray alloc]init];
    residentialVal=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    //dictServiceName=[[NSMutableDictionary alloc]init];
    NSArray *arrCatergory=[dictMasters valueForKey:@"Categories"];
    for (int i=0; i<arrCatergory.count; i++)
    {
        NSDictionary *dict=[arrCatergory objectAtIndex:i];
        {
            NSArray *arrServices=[dict valueForKey:@"Services"];
            for (int j=0; j<arrServices.count; j++)
            {
                NSDictionary *dict=[arrServices objectAtIndex:j];
                NSString *str;
                str=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsTaxable"]];
                NSLog(@"str>>>>%@",str);
                [name addObject:str];
                [sysName addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
                [commercialVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsCommercialTaxable"]]];
                [residentialVal addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsResidentialTaxable"]]];
                
            }
        }
    }
    
    dictTaxStatus = [NSDictionary dictionaryWithObjects:name forKeys:sysName];
    dictCommercialStatus = [NSDictionary dictionaryWithObjects:commercialVal forKeys:sysName];
    dictResidentialStatus = [NSDictionary dictionaryWithObjects:residentialVal forKeys:sysName];
    NSLog(@"Service Tax Status Dictionary %@/n Commercial %@/n Residential %@",dictTaxStatus,dictCommercialStatus,dictResidentialStatus);
    
}


#pragma mark- 7 Nov
-(void)updateLeadIdDetail
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        matches=arrAllObj[0];
        
        if (clickSendProposal==YES)
        {
            [matches setValue:@"true" forKey:@"isProposalFromMobile"];
        }
        else
        {
            [matches setValue:@"false" forKey:@"isProposalFromMobile"];
        }
        
        
        [context save:&error1];
        
    }
    
}
#pragma mark - SALES FETCH
-(void)salesFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            strBranchSysName=[NSString stringWithFormat:@"%@",[matches valueForKey:@"branchSysName"]];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        }
    }
}
//Nilind 06 Jan

-(void)fetchFromCoreDataStandardNew
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNew setPredicate:predicate];
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrStanCount=[[NSMutableArray alloc]init];
    arrSoldCount=[[NSMutableArray alloc]init];
    arrUnSoldStanCount=[[NSMutableArray alloc]init];
    
    arrSold=[[NSMutableArray alloc]init];
    arrUnSold=[[NSMutableArray alloc]init];
    NSManagedObject *matchesNew;
    
    if (arrAllObjNew.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjNew.count; k++)
        {
            matchesNew=arrAllObjNew[k];
            NSLog(@"Lead IDDDD====%@",[matchesNew valueForKey:@"leadId"]);
            
            [arrStanCount addObject:[NSString stringWithFormat:@"%d",k]];
            
            if([[NSString stringWithFormat:@"%@",[matchesNew valueForKey:@"isSold"]]isEqualToString:@"true"])
            {
                [arrSoldCount addObject:[NSString stringWithFormat:@"%d",k]];
                [arrSold addObject:[NSString stringWithFormat:@"%d",k]];
            }
            else
            {
                [arrUnSoldStanCount addObject:[NSString stringWithFormat:@"%d",k]];
                [arrUnSold addObject:[NSString stringWithFormat:@"%d",k]];
            }
            
        }
    }
}

-(void)fetchFromCoreDataNonStandardNew
{
    
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    
    NSArray* arrAllObjNew1 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSManagedObject *matchesNew1;
    arrNonStanCount=[[NSMutableArray alloc]init];
    arrNonStanSoldCount=[[NSMutableArray alloc]init];
    arrUnSoldNonStanCount=[[NSMutableArray alloc]init];
    if (arrAllObjNew1.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjNew1.count; k++)
        {
            matchesNew1=arrAllObjNew1[k];
            NSLog(@"Lead IDDDD====%@",[matchesNew1 valueForKey:@"leadId"]);
            [arrNonStanCount addObject:[NSString stringWithFormat:@"%d",k]];
            
            if([[NSString stringWithFormat:@"%@",[matchesNew1 valueForKey:@"isSold"]]isEqualToString:@"true"])
            {
                [arrNonStanSoldCount addObject:[NSString stringWithFormat:@"%d",k]];
                [arrSold addObject:[NSString stringWithFormat:@"%d",k]];
            }
            else
            {
                [arrUnSoldNonStanCount addObject:[NSString stringWithFormat:@"%d",k]];
                [arrUnSold addObject:[NSString stringWithFormat:@"%d",k]];
                
            }
            
        }
    }
}


- (IBAction)actionOnNewCancel:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backStan"];
    [defs setBool:NO forKey:@"backNonStandard"];
    [defs setBool:YES forKey:@"isFromBackServiceSummary"];
    [defs setBool:YES forKey:@"fromServiceSummaryScroll"];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)actionOnNewSendProposal:(id)sender
{
    clickSendProposal=YES;
    [self updateLeadIdDetail];
    [global updateSalesZSYNC:strLeadId :@"yes"];
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
    SalesAutomationAgreementProposaliPad *objSalesAutomationAgreementProposal=[storyBoard instantiateViewControllerWithIdentifier:@"SalesAutomationAgreementProposaliPad"];
    objSalesAutomationAgreementProposal.strSummarySendPropsal=@"strSummarySendPropsal";
    [self.navigationController pushViewController:objSalesAutomationAgreementProposal animated:YES];
}
- (IBAction)actionOnCancel3:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)actionOnGlobalSync:(id)sender
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"Alert!":@"No Internet connection available"];
    }
    else
    {
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoader) name:@"startLoader" object:nil];
        [self performSelector:@selector(methodSync) withObject:nil afterDelay:0.5];
        
    }
    
}

-(void)methodSync
{
    GlobalSyncViewController *objGlobalSyncViewController=[[GlobalSyncViewController alloc]init];
    NSString *str=@"general";
    
    [objGlobalSyncViewController syncCall:str];
}
-(void)stopLoader
{
    [DejalBezelActivityView removeView];
}
//Nilind 03 May
-(void)getFrequencySysNameFromName
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    //  NSLog(@"Response on Sales Auto getLeadDeatilMaster = = = =  = %@",dictMasters);
    
    NSMutableArray *arrName1,*arrSysName1,*arrYearlyOccurence;
    arrName1=[[NSMutableArray alloc]init];
    arrSysName1=[[NSMutableArray alloc]init];
    arrYearlyOccurence=[[NSMutableArray alloc]init];
    
    NSArray *arrFreq=[dictMasters valueForKey:@"Frequencies"];
    for (int i=0; i<arrFreq.count; i++)
    {
        NSDictionary *dict=[arrFreq objectAtIndex:i];
        [arrName1 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FrequencyName"]]];
        [arrSysName1 addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]];
        [arrYearlyOccurence addObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"YearlyOccurrence"]]];
        
    }
    
    dictFreqNameFromSysName=[NSDictionary dictionaryWithObjects:arrName1 forKeys:arrSysName1];
    dictYearlyOccurFromFreqSysName=[NSDictionary dictionaryWithObjects:arrYearlyOccurence forKeys:arrSysName1];
    
    //NSLog(@"dictFreqNameFromSysName>>%@",dictFreqNameFromSysName);
    
    
}

//End
//Nilind 14 Sept
#pragma mark - ---------Nilind 14 Sept change FOOTER IMAGE--------------------

- (IBAction)actionAddAfterImages:(id)sender
{
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        
        /*UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
         delegate:self
         cancelButtonTitle:@"Cancel"
         destructiveButtonTitle:nil
         otherButtonTitles:@"Capture New", @"Gallery", nil];
         
         [actionSheet showInView:self.view];*/
        [self newAlertMethodAddImage];
        
        
    }
}

- (IBAction)actionOnCancelAfterImage:(id)sender
{
    [_viewForAfterImage removeFromSuperview];
    
}

- (IBAction)actionOnAfterImgView:(id)sender
{
    
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewForFinalSave.frame.origin.y-_viewForAfterImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewForAfterImage.frame.size.height);
    [_viewForAfterImage setFrame:frameFor_view_BeforeImageInfo];
    [self.view addSubview:_viewForAfterImage];
}


#pragma mark- ---------- FETCH IMAGE ----------
-(void)fetchImageDetailFromDataBase
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" : leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                /*
                 [arrOfImagenameCollewctionView addObject:leadImagePath];
                 
                 //Nilind 07 June
                 
                 NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                 
                 if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                 
                 [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                 
                 }
                 else
                 {
                 
                 
                 [arrOfImageCaptionGraph addObject:strImageCaption];
                 
                 }
                 
                 
                 NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                 
                 if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                 
                 [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                 
                 } else {
                 
                 [arrOfImageDescriptionGraph addObject:strImageDescription];
                 
                 }
                 
                 
                 */
                //End
                
            }
            else
            {
                
                [arrNoImage addObject:dict_ToSendLeadInfo];
                
                //Nilind 07 June
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaption addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaption addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescription addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescription addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageLongitude addObject:strLong];
                    
                }
                //End
                
            }
            
            
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    }
    else
    {
    }
}


-(void)openGalleryy{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera
{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==7)
    {
        
        
        NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        
        NSArray *arrOfImagesDetail=arrNoImage;
        
        arrOfBeforeImages=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dictData=arrOfImagesDetail[k];
                
                [arrOfBeforeImages addObject:dictData];
                
            }else{
                
                [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
                
            }
            
        }
        
        arrOfImagesDetail=arrOfBeforeImages;
        
        if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
            [global AlertMethod:Info :NoBeforeImg];
        }else if (arrOfImagesDetail.count==0){
            [global AlertMethod:Info :NoBeforeImg];
        }
        else {
            NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
            for (int k=0; k<arrOfImagesDetail.count; k++) {
                if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dict=arrOfImagesDetail[k];
                    [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
                }else{
                    
                    [arrOfImagess addObject:arrOfImagesDetail[k]];
                    
                }
            }
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMainiPad"
                                                                     bundle: nil];
            ImagePreviewSalesAutoiPad
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAutoiPad"];
            objByProductVC.arrOfImages=arrOfImagess;
            
            [self.navigationController pushViewController:objByProductVC animated:YES];
            
        }
    }
    else if (buttonIndex == 0)
    {
        NSLog(@"The CApture Image.");
        
        
        if (arrNoImage.count<10)
        {
            NSLog(@"The CApture Image.");
            
            BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Camera Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         } else {
                                             
                                         }
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        //............
        
    }
    else if (buttonIndex == 1)
    {
        NSLog(@"The Gallery.");
        if (arrNoImage.count<10)
        {
            BOOL isCameraPermissionAvailable=[global isGalleryPermission];
            
            if (isCameraPermissionAvailable) {
                
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    
                    [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                    
                }else{
                    
                    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePickController.delegate=(id)self;
                    imagePickController.allowsEditing=TRUE;
                    [self presentViewController:imagePickController animated:YES completion:nil];
                    
                }
            }else{
                
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Gallery Permission not allowed.Please go to settings and allow"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          
                                          
                                          
                                      }];
                [alert addAction:yes];
                UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                             NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                             [[UIApplication sharedApplication] openURL:url];
                                         }
                                         else
                                         {
                                             
                                             
                                             
                                         }
                                         
                                     }];
                [alert addAction:no];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            chkForDuplicateImageSave=YES;
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    
}

//============================================================================

#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==_collectionViewAfterImage)
    {
        return arrNoImage.count;
    }
    else
    {
        return arrGraphImage.count;
    }
    
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==_collectionViewAfterImage)
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    else
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *str = [arrNoImage objectAtIndex:indexPath.row];
    if (collectionView==_collectionViewAfterImage)
    {
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreview : strIndex];
            
        }
    }
    else
    {
        // NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *str;// = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSDictionary *dictdat=[arrGraphImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrGraphImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewGraph : strIndex];
            
        }
        
    }
    
    
}
//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        
        
        
    }else if (arrOfImagesDetail.count==0){
        
        
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        if (arrOfImagess.count==1)
        {
        }
        else if (arrOfImagess.count==2)
        {
            
        }
        else
        {
        }
        //_const_ImgView_H.constant=100;  temp comment
        //_const_SaveContinue_B.constant=62;
        
        // 21 April 2020
        //[self downloadImages:arrOfImagess];
        
    }
}


-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            [self ShowFirstImage:str:k];
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    //NIlind 14 Sept
    
    [_collectionViewAfterImage reloadData];
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    [self ShowFirstImage : name : indexx];
    
}

- (UIImage*)loadImage :(NSString*)name :(int)indexxx
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (indexxx==0)
    {
        
        
    } else if(indexxx==1)
    {
        
        
        
    }else{
        
        
    }
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    return image;
}
-(void)goingToPreview :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMainiPad"
                                                                 bundle: nil];
        ImagePreviewSalesAutoiPad
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewSalesAutoiPad"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaption;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescription;
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"backfromDynamicView"];
    [defs synchronize];
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
    [arrNoImage addObject:strImageNamess];
    //UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    // [self saveImage:chosenImage :strImageNamess];
    
    [self resizeImage:chosenImage :strImageNamess];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    //Lat long code
       
       CLLocationCoordinate2D coordinate = [global getLocation] ;
       NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
       NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
       [arrImageLattitude addObject:latitude];
       [arrImageLongitude addObject:longitude];
    
    //imageCaption
    
    NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
    
    BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
    
    if (yesImageCaption) {
        
        [self alertViewCustom];
        
    } else {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
    }
   
    
}

//Change for Image Caption
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender
{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
        [_scrollViewService setContentOffset:bottomOffset animated:YES];
        
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}
-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
    [_scrollViewService setContentOffset:bottomOffset animated:YES];
    
}

-(void)alertToEnterImageCaption{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Caption Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Image Description Here...";
        textField.tag=8;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        UITextField * txtImageDescriptions = textfields[1];
        if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
            
            if (txtHistoricalDays.text.length>0) {
                
                [arrOfImageCaption addObject:txtHistoricalDays.text];
                
            } else {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtImageDescriptions.text.length>0) {
                
                [arrOfImageDescription addObject:txtImageDescriptions.text];
                
            } else {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            }
            
            CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
            [_scrollViewService setContentOffset:bottomOffset animated:YES];
            
            
        } else {
            
            [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollViewService.contentSize.height - _scrollViewService.bounds.size.height);
        [_scrollViewService setContentOffset:bottomOffset animated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
//.................................................................................
-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    //CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    CGRect rect = CGRectMake(0.0, 0.0, image.size.width/2, image.size.height/2);
    
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}
#pragma mark- Save & Remove Image  Methods
//============================================================================
//============================================================================

- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    [arrImagePath addObject:imageName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
#pragma mark- Note
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL yesfromback=[defs boolForKey:@"backfromDynamicView"];
    if (yesfromback) {
        
        [defs setBool:NO forKey:@"backfromDynamicView"];
        [defs synchronize];
        
    }
    else
    {
        
        [self deleteImageFromCoreDataSalesInfo];
        
        for (int k=0; k<arrNoImage.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            if ([arrNoImage[k] isKindOfClass:[NSString class]])
            {
                
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[arrNoImage objectAtIndex:k]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                
                NSError *error1;
                [context save:&error1];
            }
            else
            {
                NSDictionary *dictData=[arrNoImage objectAtIndex:k];
                objImageDetail.createdBy=@"";
                objImageDetail.createdDate=@"";
                objImageDetail.descriptionImageDetail=@"";
                objImageDetail.leadImageId=@"";
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=@"Before";
                objImageDetail.modifiedBy=@"";
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                
                
                NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
                if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                    
                    objImageDetail.leadImageCaption=@"";
                    
                } else {
                    
                    objImageDetail.leadImageCaption=strImageCaptionToSet;
                    
                }
                
                NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
                if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                    
                    objImageDetail.descriptionImageDetail=@"";
                    
                } else {
                    
                    objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                    
                }
                
                //Latlong code
                
                NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
                objImageDetail.latitude=strlat;
                NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
                objImageDetail.longitude=strlong;
                NSError *error1;
                [context save:&error1];
                
            }
            
        }
        for (int k=0; k<arrGraphImage.count; k++)
        {
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
            
            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            NSDictionary *dict=[arrGraphImage objectAtIndex:k];
            if ([arrGraphImage[k] isKindOfClass:[NSDictionary class]])
            {
                
                objImageDetail.createdBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdBy"]];
                objImageDetail.createdDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"createdDate"]];
                objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
                objImageDetail.leadImageId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageId"]];
                objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImagePath"]];
                objImageDetail.leadImageType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageType"]];
                objImageDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"modifiedBy"]];
                objImageDetail.modifiedDate=[global modifyDate];
                objImageDetail.leadId=strLeadId;
                objImageDetail.userName=strUserName;
                objImageDetail.companyKey=strCompanyKey;
                objImageDetail.leadImageCaption=[NSString stringWithFormat:@"%@",[dict valueForKey:@"leadImageCaption"]];
                objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dict valueForKey:@"descriptionImageDetail"]];
                
                //Latlong code
                
                //NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
                objImageDetail.latitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"latitude"]];
                // NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
                objImageDetail.longitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"longitude"]];
                
                /*
                 NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionGraph objectAtIndex:k]];
                 if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                 
                 objImageDetail.leadImageCaption=@"";
                 
                 } else {
                 
                 objImageDetail.leadImageCaption=strImageCaptionToSet;
                 
                 }
                 
                 NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionGraph objectAtIndex:k]];
                 if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                 
                 objImageDetail.descriptionImageDetail=@"";
                 
                 } else {
                 
                 objImageDetail.descriptionImageDetail=strImageDescriptionToSet;
                 
                 }
                 */
                NSError *error1;
                [context save:&error1];
            }
        }
        
        
        
    }
    //........................................................................
    // [self fetchForUpdate];
    
    //[self fetchImageFromCoreDataStandard];
    
}
-(void)deleteImageFromCoreDataSalesInfo
{
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
    [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    [allDataImage setPredicate:predicateImage];
    
    [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error2 = nil;
    NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
    //error handling goes here
    for (NSManagedObject * data in DataImage) {
        [context deleteObject:data];
    }
    NSError *saveError2 = nil;
    [context save:&saveError2];
    
}
-(void)getImageCollectionView
{
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        EditImageViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    }
    else
    {
        
        //Nilind 07 June Image Caption
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        BOOL isFromBack=[defsBack boolForKey:@"isFromBackAgreement"];
        
        if (isFromBack)
        {
            
            //arrOfBeforeImageAll=nil;
            //arrOfBeforeImageAll=[[NSMutableArray alloc]init];
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            arrNoImage=[[NSMutableArray alloc]init];
            
            
            arrImageLattitude=nil;
            arrImageLattitude=[[NSMutableArray alloc]init];
            arrImageLongitude=nil;
            arrImageLongitude=[[NSMutableArray alloc]init];
            
            arrImageGraphLattitude=nil;
            arrImageGraphLattitude=[[NSMutableArray alloc]init];
            arrImageGraphLongitude=nil;
            arrImageGraphLongitude=[[NSMutableArray alloc]init];
            
            
            [defsBack setBool:NO forKey:@"isFromBackAgreement"];
            [defsBack synchronize];
            [self fetchImageDetailFromDataBaseForGraph];
            
            [self fetchImageDetailFromDataBase];
            
        }
        
        
        
        //End
        
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreviewSales"];
        if (yesFromDeleteImage)
        {
            isEditedInSalesAuto=YES;
            NSLog(@"Database edited");
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreviewSales"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrNoImage.count; k++)
            {
                
                NSDictionary *dictdat=arrNoImage[k];
                NSString *strImageName;
                if([dictdat isKindOfClass:[NSDictionary class]])
                {
                    strImageName=[dictdat valueForKey:@"leadImagePath"];
                }
                else
                {
                    strImageName=arrNoImage[k];
                    
                }
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImagesSales"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrNoImage[k]];
                        
                    }
                }
                
            }
            if (!(arrTempBeforeImage.count==0))
            {
                
                [arrNoImage removeObjectsInArray:arrTempBeforeImage];
            }
            else if((arrTempBeforeImage.count==0))
            {
                arrNoImage=nil;
                arrNoImage=[[NSMutableArray alloc]init];
            }
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImagesSales"];
            [defsnew synchronize];
            
        }
        else
        {
            
        }
        //Nilind 07 June Image Caption
        
        
        //Change in image Captions
        BOOL yesEditedImageCaptionGraph=[defsSS boolForKey:@"yesEditedImageCaptionGraph"];
        if (yesEditedImageCaptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
            [defsnew synchronize];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescriptionGraph=[defsSS boolForKey:@"yesEditedImageDescriptionGraph"];
        if (yesEditedImageDescriptionGraph) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
            [defsnew synchronize];
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            isEditedInSalesAuto=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            
        }
        
        //Change For Image Description  yesEditedImageDescription
        
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            isEditedInSalesAuto=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            
        }
        
        //End
        
        
        [self downloadingImagesThumbNailCheck];
        //Nilind 10 Jan
        
    }
    //...........
    [_collectionViewAfterImage reloadData];
    [_collectionViewGraphImage reloadData];
    
}
#pragma mark -------------- ADDING GRAPH IMAGE ----------------

- (IBAction)actionOnAddGraphImage:(id)sender
{
    if([strLeadStatusGlobal caseInsensitiveCompare:@"Complete"] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"firstGraphImage"];
        [defs setBool:NO forKey:@"servicegraph"];
        
        [defs synchronize];
        
        //        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
        //        EditGraphViewControlleriPad  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewControlleriPad"];
        //        objSignViewController.strLeadId=strLeadId;
        //        objSignViewController.strCompanyKey=strCompanyKey;
        //        objSignViewController.strUserName=strUserName;
        //        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"MechanicaliPad" bundle:nil];
        GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
        objSignViewController.strLeadId=strLeadId;
        objSignViewController.strCompanyKey=strCompanyKey;
        objSignViewController.strUserName=strUserName;
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    }
}

- (IBAction)actionOnCancelBeforeImage:(id)sender
{
    [_viewForGraphImage removeFromSuperview];
}

- (IBAction)actionOnGraphImageFooter:(id)sender
{
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewForFinalSave.frame.origin.y-_viewForGraphImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewForGraphImage.frame.size.height);
    [_viewForGraphImage setFrame:frameFor_view_BeforeImageInfo];
    [_collectionViewGraphImage reloadData];
    [self.view addSubview:_viewForGraphImage];
}
-(void)goingToPreviewGraph :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrGraphImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++)
    {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
        {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }
        else
        {
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else
    {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++)
        {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"leadImagePath"]];
            }
            else
            {
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        GraphImagePreviewViewControlleriPad
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImagePreviewViewControlleriPad"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.strLeadId=strLeadId;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaptionGraph;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescriptionGraph;
        objImagePreviewSalesAuto.statusOfWorkOrder=strLeadStatusGlobal;
        
        
        
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}
-(void)fetchImageDetailFromDataBaseForGraph
{
    
    arrGraphImage=nil;
    
    arrGraphImage=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObj.count; j++) {
            
            matches=arrAllObj[j];
            
            
            
            NSString *companyKey=[matches valueForKey:@"companyKey"];
            NSString *userName=[matches valueForKey:@"userName"];
            NSString *leadId=[matches valueForKey:@"leadId"];
            NSString *leadImageType=[matches valueForKey:@"leadImageType"];
            NSString *leadImagePath=[matches valueForKey:@"leadImagePath"];
            NSString *leadImageId=[matches valueForKey:@"leadImageId"];
            NSString *descriptionImageDetail=[matches valueForKey:@"descriptionImageDetail"];
            NSString *createdDate=[matches valueForKey:@"createdDate"];
            NSString *createdBy=[matches valueForKey:@"createdBy"];
            NSString *modifiedDate=[matches valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matches valueForKey:@"modifiedBy"];
            NSString *leadImageCaption=[matches valueForKey:@"leadImageCaption"];
            NSString *lattitude=[matches valueForKey:@"latitude"];
            NSString *longitude=[matches valueForKey:@"longitude"];
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       leadId.length==0 ? @"" : leadId,
                                       leadImageType.length==0 ? @"" : leadImageType,
                                       leadImagePath.length==0 ? @"" : leadImagePath,
                                       leadImageId.length==0 ? @"" : leadImageId,
                                       createdDate.length==0 ? @"" : createdDate,
                                       descriptionImageDetail.length==0 ? @"" : descriptionImageDetail,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       leadImageCaption.length==0 ? @"" :leadImageCaption,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"leadId",
                                     @"leadImageType",
                                     @"leadImagePath",
                                     @"leadImageId",
                                     @"createdDate",
                                     @"descriptionImageDetail",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"leadImageCaption",
                                     @"latitude",
                                     @"longitude",nil];
            
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            if ([leadImageType isEqualToString:@"Graph"])
            {
                
                //[arrOfImagenameCollewctionView addObject:leadImagePath];
                [arrGraphImage addObject:dict_ToSendLeadInfo];
                
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadImageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionGraph addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matches valueForKey:@"descriptionImageDetail"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionGraph addObject:strImageDescription];
                    
                }
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matches valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matches valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLongitude addObject:strLong];
                    
                }
                
                
            }
            
        }
    }
    if (error1) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error1, error1.localizedDescription);
    } else {
    }
    
    [self downloadImagesGraphs:arrGraphImage];
    
}
-(void)downloadImagesGraphs :(NSArray*)arrOfImagesDownload
{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        // NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
        NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"leadImagePath"];
        }
        else
        {
            str=[arrOfImagesDownload objectAtIndex:k];
        }
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainService
        NSString *strUrl;// = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        // strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        
        strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        strUrl = [NSString stringWithFormat:@"%@/Documents/%@",strServiceUrlMainServiceAutomation,str];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
            
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
    }
    
    [_collectionViewGraphImage reloadData];
    
}
#pragma mark- ----------Textview Delegate Method-----------
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    if([text isEqualToString:@"\n"])
    {
        [txtFieldCaption resignFirstResponder];
        [txtViewImageDescription resignFirstResponder];
        
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}
#pragma mark- BUNDLE CHANGE

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        NSString *sectionName;
        sectionName=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        return sectionName;
    }
    else
    {
        return @"";
    }
    
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        NSString *sectionName;
        sectionName=[NSString stringWithFormat:@"%@ Footer",[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]]];
        if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
        {
            sectionName=@"";
        }
        return sectionName;
    }
    else
    {
        return @"";
    }
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==2)
    {
        return arrBundleRow.count;
    }
    else
    {
        return 1;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        return 35.0f;
    }
    else
    {
        return 0.0f;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView.tag==2)
    {
        return 30.0f;
    }
    else
    {
        return 0.0f;
    }
}
-(void)fetchFromCoreDataStandardForBundle:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    
    
    
    arrInitialPriceBundle=[[NSMutableArray alloc]init];
    arrDiscountPerBundle=[[NSMutableArray alloc]init];
    
    arrDiscountBundle=[[NSMutableArray alloc]init];
    arrMaintenancePriceBundle=[[NSMutableArray alloc]init];
    arrFrequencyNameBundle=[[NSMutableArray alloc]init];
    arrServiceNameBundle=[[NSMutableArray alloc]init];
    arrStanIsSoldBundle=[[NSMutableArray alloc]init];
    arrSysNameBundle=[[NSMutableArray alloc]init];
    
    arrUnitBundle=[[NSMutableArray alloc]init];
    arrFinalInitialPriceBundle=[[NSMutableArray alloc]init];
    arrFinalMaintPriceBundle=[[NSMutableArray alloc]init];
    
    arrPackageNameIdBundle=[[NSMutableArray alloc]init];
    arrBillingFreqSysNameBundle=[[NSMutableArray alloc]init];
    arrBundleId=[[NSMutableArray alloc]init];
    arrFreqSysNameBundle=[[NSMutableArray alloc]init];
    
    // arrBundleRow=[[NSMutableArray alloc]init];
    NSMutableArray *arrBundleNew;
    arrBundleNew=[[NSMutableArray alloc]init];
    
    
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                [arrFrequencyNameBundle addObject:[matches valueForKey:@"serviceFrequency"]];
                [arrFreqSysNameBundle addObject:[matches valueForKey:@"frequencySysName"]];
                
                if ([[matches valueForKey:@"serviceSysName"]isEqualToString:@""])
                {
                    [arrServiceNameBundle addObject:@""];
                }
                else
                {
                    [arrServiceNameBundle addObject:[dictServiceName valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                }
                
                [arrStanIsSoldBundle addObject: [matches valueForKey:@"isSold"]];
                
                [arrSysNameBundle addObject:[matches valueForKey:@"serviceSysName"]];
                
                //Nilind 6 oct
                
                if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"isTBD"]]isEqualToString:@"true"])
                {
                    [arrInitialPriceBundle addObject:@"TBD"];
                    [arrDiscountPerBundle addObject:@"TBD"];
                    [arrDiscountBundle addObject:@"TBD"];
                    [arrMaintenancePriceBundle addObject:@"TBD"];
                    [arrFinalInitialPriceBundle addObject:@"TBD"];
                    [arrFinalMaintPriceBundle addObject:@"TBD"];
                }
                else
                {
                    [arrInitialPriceBundle addObject:[matches valueForKey:@"initialPrice"]];
                    [arrDiscountBundle addObject:[matches valueForKey:@"discount"]];
                    [arrMaintenancePriceBundle addObject:[matches valueForKey:@"maintenancePrice"]];
                    [arrFinalInitialPriceBundle addObject:[matches valueForKey:@"finalUnitBasedInitialPrice"]];
                    [arrFinalMaintPriceBundle addObject:[matches valueForKey:@"finalUnitBasedMaintPrice"]];
                    [arrDiscountPerBundle addObject:[matches valueForKey:@"discountPercentage"]];
                    
                }
                [arrUnitBundle addObject:[matches valueForKey:@"unit"]];
                
                [arrPackageNameIdBundle addObject:[matches valueForKey:@"packageId"]];
                [arrBillingFreqSysNameBundle addObject:[matches valueForKey:@"billingFrequencySysName"]];
                [arrBundleId addObject:[matches valueForKey:@"bundleId"]];
            }
        }
    }
    // [self heightManage];
}
/*-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
 {
 
 }*/
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 35)];
    headerView.backgroundColor = [UIColor colorWithRed:95.0/255 green:178.0/255 blue:175.0/255 alpha:1];
    headerView.backgroundColor = [UIColor colorWithRed:58.0/255 green:63.0/255 blue:76.0/255 alpha:1];
    //headerView.backgroundColor = [UIColor colorWithRed:211.0/255 green:175.0/255 blue:72.0/255 alpha:1];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = section + 1000;
    button.frame = CGRectMake(_tblBundle.frame.origin.x+4, 2, 30, 30);
    
    [self fetchFromCoreDataStandardForSectionCheckBoxBundle:[arrBundleRow objectAtIndex:section]];
    if ([strSoldStatusBundle isEqualToString:@"true"])
    {
        [button setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [button setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    [button addTarget:self action:@selector(checkBundleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    // [headerView addSubview:button];
    [button setTitle:[arrBundleRow objectAtIndex:section] forState:UIControlStateNormal];
    // button.titleLabel.textColor=[UIColor clearColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 250, 35)];
    
    
    NSString *sectionName;
    sectionName=[dictBundleNameFromId valueForKey:[NSString stringWithFormat:@"%@",[arrBundleRow objectAtIndex:section]]];
    if(sectionName.length ==0 || [sectionName isEqual:nil]|| [sectionName isEqualToString:@""])
    {
        sectionName=@"";
    }
    headerLabel.text =sectionName;
    headerLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabel.textColor = [UIColor whiteColor];
    //headerLabel.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabel];
    
    UIButton *buttonDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonDelete.tag = section + 1000;
    buttonDelete.frame = CGRectMake(CGRectGetMaxX(headerView.frame)-70, 0, 35, 35);
    //[buttonDelete setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
    //buttonDelete.backgroundColor=[UIColor redColor];
    [buttonDelete addTarget:self action:@selector(deleteBundleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    // [headerView addSubview:buttonDelete];
    [buttonDelete setTitle:[arrBundleRow objectAtIndex:section]  forState:UIControlStateNormal];
    buttonDelete.titleLabel.textColor=[UIColor clearColor];
    [buttonDelete setImage:[UIImage imageNamed:@"ic_action_delete.png"] forState:UIControlStateNormal];
    [buttonDelete setBackgroundImage:[UIImage imageNamed:@"ic_action_delete.png"] forState:UIControlStateNormal];
    return headerView;
}
- (IBAction)deleteBundleButtonPressed:(UIButton *)sender
{
    // NSInteger section = sender.tag - 1000;
    //UIButton *selectedButton = (UIButton *) sender;
    
    //[self deleteBundleButtonPressed:sender.titleLabel.text];
    NSLog(@"Section Bundle Id %@",sender.titleLabel.text);
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert!"
                               message:@"Are you sure want to delete Bundle"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              [self fetchFromCoreDataStandard];
                              
                              [self deleteBundleFromCoreDataSalesInfo:[NSString stringWithFormat:@"%@",sender.titleLabel.text]];
                              [self fetchFromCoreDataStandard];
                              [_tblBundle reloadData];
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             
                             
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)checkBundleButtonPressed:(UIButton *)sender
{
    NSInteger section = sender.tag - 1000;
    
    UIButton *selectedButton = (UIButton *) sender;
    NSLog(@"Section Bundle Id %@",selectedButton.titleLabel.text);
    if([selectedButton.currentImage isEqual:[UIImage imageNamed:@"check_box_2.png"]])
        //if(selectedButton.selected==YES)
    {
        [selectedButton setSelected:NO];
        [selectedButton setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [self updateBundle:selectedButton.titleLabel.text :@"false"];
        
        
    }
    else
    {
        [selectedButton setSelected:YES];
        [selectedButton setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [self updateBundle:selectedButton.titleLabel.text :@"true"];
    }
    
}
-(void)updateBundle: (NSString*)strButtonBundleId : (NSString*)status
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && bundleId=%@",strLeadId,strButtonBundleId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            //if([[matches valueForKey:@"bundleId"]isEqualToString:strButtonBundleId])
            //{
            [matches setValue:status forKey:@"isSold"];
            [matches setValue:[global modifyDate] forKey:@"modifiedDate"];
            // [context save:&error1];
            //}
            
        }
        [context save:&error1];
    }
    
    [self fetchFromCoreDataStandard];
    
}
-(void)deleteBundleFromCoreDataSalesInfo:(NSString*)strBundleIdd
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    for (int i=0; i<Data.count; i++)
    {
        NSManagedObject *dataMatches=[Data objectAtIndex:i];
        if ([[dataMatches valueForKey:@"bundleId"]isEqualToString:strBundleIdd])
        {
            [context deleteObject:dataMatches];
            NSError *saveError = nil;
            [context save:&saveError];
            
        }
    }
    
    /*for (NSManagedObject * data in Data)
     {
     [context deleteObject:data];
     }*/
    
    //[self fetchFromCoreDataStandard];
    //[self heightManage];
    //[self fetchFromCoreDataStandard];
    
    // [_tblBundle reloadData];
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblBundle.frame.size.width, 30)];
    headerView.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(_tblBundle.frame.origin.x+5, 0, _tblBundle.frame.size.width/2, 30)];
    [self fetchFromCoreDataStandardForSectionCheckBoxBundle:[arrBundleRow objectAtIndex:section]];
    headerLabel.text =[NSString stringWithFormat:@"Total Initial Cost = $%.2f",totalInitialBundle];
    headerLabel.font=[UIFont boldSystemFontOfSize:12];
    headerLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabel];
    
    UILabel *headerLabelMaint = [[UILabel alloc] initWithFrame:CGRectMake(headerLabel.frame.origin.x+headerLabel.frame.size.width+120, 0,_tblBundle.frame.size.width/2, 30)];
    headerLabelMaint.text =[NSString stringWithFormat:@"Total Maint Cost = $%.2f",totalMaintBundle];
    CGSize expectedLabelSize = [ headerLabelMaint.text sizeWithFont:headerLabelMaint.font constrainedToSize:CGSizeMake(headerLabelMaint.frame.size.width, FLT_MAX) lineBreakMode:headerLabelMaint.lineBreakMode];
    headerLabelMaint.frame=CGRectMake(_tblBundle.frame.size.width -expectedLabelSize.width-10, 0, expectedLabelSize.width, 30);
    
    headerLabelMaint.font=[UIFont boldSystemFontOfSize:14];
    
    headerLabelMaint.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    headerLabelMaint.textColor = [UIColor blackColor];
    headerLabelMaint.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:headerLabelMaint];
    
    return headerView;
}

- (CGSize)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *contextTemp = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:contextTemp].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size;
}

-(void)fetchFromCoreDataStandardForSectionCheckBoxBundle:(NSString *)strServiceBundleId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    strSoldStatusBundle=@"false";
    NSMutableArray *arrTotalInitialCostBundle,*arrTotalMaintCostBundle;
    arrTotalInitialCostBundle=[[NSMutableArray alloc]init];
    arrTotalMaintCostBundle=[[NSMutableArray alloc]init];
    totalInitialBundle=0;totalMaintBundle=0;
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            //....................
            if ([[NSString stringWithFormat:@"%@",[matches valueForKey:@"bundleId"]]isEqualToString:strServiceBundleId])
            {
                strSoldStatusBundle=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isSold"]];
                
                
                
                NSString *str=[NSString stringWithFormat:@"%@",[dictQuantityStatus valueForKey:[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSysName"]]]];
                if ([str isEqualToString:@"0"])
                {
                    totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                    totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                    
                }
                else
                {
                    if (str.length==0 || [str isEqualToString:@"(null)"])
                    {
                        totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                        totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                        
                    }
                    else
                    {
                        totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedInitialPrice"]]doubleValue];
                        
                        totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"finalUnitBasedMaintPrice"]]doubleValue];
                    }
                }
                
                //totalInitialBundle=totalInitialBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"initialPrice"]]doubleValue];
                //totalMaintBundle=totalMaintBundle+[[NSString stringWithFormat:@"%@",[matches valueForKey:@"maintenancePrice"]]doubleValue];
                //break;
            }
        }
    }
}
- (IBAction)actionOnNotesHistory:(id)sender
{
    //Nilind
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceNotesHistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
    objByProductVC.strTypeOfService=@"sales";
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
    
    //End
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPad"
                                                                 bundle: nil];
        ClockInOutViewControlleriPad
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewControlleriPad"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}

-(void)fetchForAppliedDiscountFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesDiscount;
    arrDiscountCoupon=[[NSMutableArray alloc]init];
    
    if (arrAllObjSales.count==0)
    {
    }
    else
    {
        NSArray *uniqueArray = [[NSSet setWithArray:arrSoldServiceStandardIdForCoupon] allObjects];
        
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matchesDiscount=arrAllObjSales[k];
            
            if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
            {
                for (int l=0; l<uniqueArray.count; l++)
                {
                    if ([[uniqueArray objectAtIndex:l]isEqualToString:[matchesDiscount valueForKey:@"soldServiceId"]])
                    {
                        [arrDiscountCoupon addObject:matchesDiscount];
                    }
                }
                //[arrDiscountCoupon addObject:matchesDiscount];
            }
            
        }
        
        
        /*
         for (int k=0; k<arrAllObjSales.count; k++)
         {
         matchesDiscount=arrAllObjSales[k];
         
         for (int j=0; j<arrSoldServiceStandardIdForCoupon.count; j++)
         {
         if([[arrSoldServiceStandardIdForCoupon objectAtIndex:j] isEqualToString:[matchesDiscount valueForKey:@"soldServiceId"]]&&[[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
         {
         [arrDiscountCoupon addObject:matchesDiscount];
         }
         }
         //if([[matchesDiscount valueForKey:@"discountType"] isEqualToString:@"Coupon"])
         // {
         // [arrDiscountCoupon addObject:matchesDiscount];
         // }
         }
         */
        
        
        
    }
    [_tblCouponDiscount reloadData];
    [self heightForDiscountCoupon];
}

-(void)calculateDiscountAndOtherDiscount
{
    float totalCouponDiscount=0;
    for (int i=0; i<arrDiscountCoupon.count; i++)
    {
        NSDictionary *dict=[arrDiscountCoupon objectAtIndex:i];
        totalCouponDiscount=totalCouponDiscount+[[NSString stringWithFormat:@"%@",[dict valueForKey:@"appliedInitialDiscount"]]floatValue];
    }
    _lblcouponDiscount.text=[NSString stringWithFormat:@"%.2f",totalCouponDiscount];
    
    float discount, couponDiscount, otherDiscount,totalDiscount;
    discount=[_lblDiscountValue.text floatValue];
    couponDiscount=[_lblcouponDiscount.text floatValue];
    
    if (discount>couponDiscount)
    {
        otherDiscount=discount-couponDiscount;
    }
    else if (couponDiscount>discount)
    {
        otherDiscount=couponDiscount-discount;
    }
    else
    {
        otherDiscount=0;
    }
    totalDiscount=totalCouponDiscount+otherDiscount;
    _lblOtherDiscount.text=[NSString stringWithFormat:@"%.2f",otherDiscount];
    _lblDiscountValue.text=[NSString stringWithFormat:@"%.2f",totalDiscount];
}
-(void)heightForDiscountCoupon
{
    _const_TableDiscount_H.constant=108;
    _const_ViewStandardTotal.constant=300;
    UILabel *lbl;
    lbl=[[UILabel alloc]init];
    lbl.frame=CGRectMake(0, 0, 594, 21);
    NSMutableArray* arrTempCoupon=[[NSMutableArray alloc]init];
    float totalHeightStan=0;
    [arrTempCoupon addObjectsFromArray:arrDiscountCoupon];
    
    for (int i=0; i<arrTempCoupon.count; i++)
    {
        
        NSManagedObject *dict=[arrTempCoupon objectAtIndex:i];
        
        lbl.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"discountDescription"]];
        
        CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font constrainedToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, FLT_MAX) lineBreakMode:lbl.lineBreakMode];
        
        totalHeightStan=totalHeightStan+expectedLabelSize.height;
        
    }
    [_tblCouponDiscount reloadData];
    _const_TableDiscount_H.constant=totalHeightStan+[arrDiscountCoupon count]*80;
    _const_ViewStandardTotal.constant=_const_TableDiscount_H.constant+300-108;
}
- (IBAction)actionOnChemicalSensitivityList:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMainiPad"
                                                                 bundle: nil];
        ChemicalSensitivityList
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChemicalSensitivityListiPad"];
        objByProductVC.strFrom=@"sales";
        objByProductVC.strId=strLeadId;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
    //End
    
}
-(void)newAlertMethodAddImage
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Make your selection"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Capture New" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              NSLog(@"The CApture Image.");
                              
                              
                              if (arrNoImage.count<10)
                              {
                                  NSLog(@"The CApture Image.");
                                  
                                  BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                                  
                                  if (isCameraPermissionAvailable) {
                                      
                                      if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                                      {
                                          
                                          [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                                          
                                      }else{
                                          
                                          UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                          imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                                          imagePickController.delegate=(id)self;
                                          imagePickController.allowsEditing=TRUE;
                                          [self presentViewController:imagePickController animated:YES completion:nil];
                                          
                                      }
                                  }else{
                                      
                                      UIAlertController *alert= [UIAlertController
                                                                 alertControllerWithTitle:@"Alert"
                                                                 message:@"Camera Permission not allowed.Please go to settings and allow"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                      
                                      UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action)
                                                            {
                                                                
                                                                
                                                                
                                                            }];
                                      [alert addAction:yes];
                                      UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                   NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                   [[UIApplication sharedApplication] openURL:url];
                                                               } else {
                                                                   
                                                               }
                                                           }];
                                      [alert addAction:no];
                                      [self presentViewController:alert animated:YES completion:nil];
                                  }
                                  
                              }
                              else
                              {
                                  chkForDuplicateImageSave=YES;
                                  UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                  [alert show];
                              }
                              //............
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             NSLog(@"The Gallery.");
                             if (arrNoImage.count<10)
                             {
                                 BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                                 
                                 if (isCameraPermissionAvailable) {
                                     
                                     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                                     {
                                         
                                         [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                                         
                                     }else{
                                         
                                         UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                                         imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                                         imagePickController.delegate=(id)self;
                                         imagePickController.allowsEditing=TRUE;
                                         [self presentViewController:imagePickController animated:YES completion:nil];
                                         
                                     }
                                 }else{
                                     
                                     UIAlertController *alert= [UIAlertController
                                                                alertControllerWithTitle:@"Alert"
                                                                message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                                preferredStyle:UIAlertControllerStyleAlert];
                                     
                                     UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * action)
                                                           {
                                                               
                                                               
                                                               
                                                           }];
                                     [alert addAction:yes];
                                     UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                handler:^(UIAlertAction * action)
                                                          {
                                                              
                                                              if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                  NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                  [[UIApplication sharedApplication] openURL:url];
                                                              }
                                                              else
                                                              {
                                                                  
                                                                  
                                                                  
                                                              }
                                                              
                                                          }];
                                     [alert addAction:no];
                                     [self presentViewController:alert animated:YES completion:nil];
                                 }
                             }
                             else
                             {
                                 chkForDuplicateImageSave=YES;
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                 [alert show];
                             }
                         }];
    [alert addAction:no];
    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
    [alert addAction:dismiss];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}
-(void)setHeight
{
    _const_TblService_H.constant=80*arrSavedServiceDetail.count;
    _const_TblScope_H.constant=80*arrSavedScopeDetail.count;
    _const_TblTarget_H.constant=80*arrSavedTargetDetail.count;
    
}
-(void)setTabelBordeColor
{
    _tblService.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblService.layer.borderWidth=1.0;
    
    _tblScope.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblScope.layer.borderWidth=1.0;
    
    _tblTarget.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _tblTarget.layer.borderWidth=1.0;
    //_viewServiceDetail.layer.cornerRadius=5.0;
}



//============================================================================
#pragma mark- Fetch From DB Methods (Service/Scope/Target)
//============================================================================

-(void)fetchFromCoreDataStandardClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    arrSavedServiceDetail=nil;
    
    if (arry.count==0)
    {
        
    }
    else
    {
        arrSavedServiceDetail=arry;
    }
    
    [_tblService reloadData];
    [self setHeight];
}


-(void)fetchScopeFromCoreData
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialScopeExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    arrSavedScopeDetail=nil;
    
    if (arry.count==0)
    {
        
    }
    else
    {
        arrSavedScopeDetail=arry;
    }
    
    [_tblScope reloadData];
    
    [self setHeight];
}

-(void)fetchTargetFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTargetExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    arrSavedTargetDetail=nil;
    if (arry.count==0)
    {
        
    }
    else
    {
        arrSavedTargetDetail=arry;
    }
    [_tblTarget reloadData];
    [self setHeight];
}

#pragma mark - fetchInitialPrice
-(float)fetchLeadCommercialInitialInfoFromCoreData
{
    NSMutableArray *arrayTemp = [NSMutableArray new];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialInitialInfoExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    for(NSManagedObject *initialInfo in arry)
    {
        NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
        NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
        [arrayTemp addObject:dict];
    }
    
    float totalInitialPrice = 0.0;
    if (arrayTemp.count>0)
    {
        arrSavedInitialPriceDetail=(NSMutableArray*)arrayTemp;
        
        for(NSDictionary *dict in arrSavedInitialPriceDetail)
        {
            totalInitialPrice = totalInitialPrice + [[dict valueForKey:@"initialPrice"] floatValue];
        }
    }
    return totalInitialPrice;
}

-(float)fetchLeadCommercialMaintInfoFromCoreData
{
    NSMutableArray *arrayTemp = [NSMutableArray new];
    //[self deleteFromCoreDataSalesInfo];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    NSArray *arry = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    for(NSManagedObject *initialInfo in arry)
    {
        NSArray *keys = [[[initialInfo entity] attributesByName] allKeys];
        NSDictionary *dict = [initialInfo dictionaryWithValuesForKeys:keys];
        [arrayTemp addObject:dict];
    }
    
    float totalMaintPrice = 0.0;
    
    if (arrayTemp.count>0)
    {
        arrSavedMaintPriceDetail=arrayTemp;
        
        for(NSDictionary *dict in arrSavedMaintPriceDetail)
        {
            totalMaintPrice = totalMaintPrice + [[dict valueForKey:@"maintenancePrice"] floatValue];
        }
    }
    return totalMaintPrice;
}

-(void)fetchLead
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSString *str;
    if (arrAllObj12.count==0)
    {
    }
    else
    {
        NSManagedObject *matches12=arrAllObj12[0];
        str=[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"statusSysName"]];
        
        _txtFieldProposedServiceDate.text = [global ChangeDateMechanicalEquipment:[NSString stringWithFormat:@"%@",[matches12 valueForKey:@"initialServiceDate"]]];//
        [_btnRecurringMonth setTitle:[matches12 valueForKey:@"recurringServiceMonth"] forState:UIControlStateNormal];
    }
}
-(void)endEditing
{
    [self.view endEditing:YES];
}


@end

