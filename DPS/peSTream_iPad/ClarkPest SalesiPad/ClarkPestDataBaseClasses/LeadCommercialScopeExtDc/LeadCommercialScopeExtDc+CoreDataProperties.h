//
//  LeadCommercialScopeExtDc+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 20/09/18.
//
//

#import "LeadCommercialScopeExtDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialScopeExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialScopeExtDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadCommercialScopeId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *scopeSysName;
@property (nullable, nonatomic, copy) NSString *scopeDescription;
@property (nullable, nonatomic, copy) NSString *isChangedScopeDesc;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *title;
@end

NS_ASSUME_NONNULL_END
