//
//  LeadCommercialScopeExtDc+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 20/09/18.
//
//

#import "LeadCommercialScopeExtDc+CoreDataProperties.h"

@implementation LeadCommercialScopeExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialScopeExtDc *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LeadCommercialScopeExtDc"];
}

@dynamic leadCommercialScopeId;
@dynamic leadId;
@dynamic scopeSysName;
@dynamic scopeDescription;
@dynamic isChangedScopeDesc;
@dynamic userName;
@dynamic companyKey;
@dynamic title;
@end
