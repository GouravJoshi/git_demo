//
//  LeadCommercialMaintInfoExtDc+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 20/09/18.
//
//

#import "LeadCommercialMaintInfoExtDc+CoreDataProperties.h"

@implementation LeadCommercialMaintInfoExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialMaintInfoExtDc *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LeadCommercialMaintInfoExtDc"];
}

@dynamic leadCommMaintInfoId;
@dynamic leadId;
@dynamic maintenanceDescription;
@dynamic maintenancePrice;
@dynamic frequencySysName;
@dynamic companyKey;
@dynamic userName;

@end
