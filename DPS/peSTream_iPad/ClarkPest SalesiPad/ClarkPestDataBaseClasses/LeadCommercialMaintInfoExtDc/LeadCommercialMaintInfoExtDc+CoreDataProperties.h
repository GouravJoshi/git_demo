//
//  LeadCommercialMaintInfoExtDc+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 20/09/18.
//
//

#import "LeadCommercialMaintInfoExtDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialMaintInfoExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialMaintInfoExtDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadCommMaintInfoId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *maintenanceDescription;
@property (nullable, nonatomic, copy) NSString *maintenancePrice;
@property (nullable, nonatomic, copy) NSString *frequencySysName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;

@end

NS_ASSUME_NONNULL_END
