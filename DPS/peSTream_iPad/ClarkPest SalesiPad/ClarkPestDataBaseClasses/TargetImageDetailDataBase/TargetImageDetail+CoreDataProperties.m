//
//  TargetImageDetail+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 25/11/19.
//
//

#import "TargetImageDetail+CoreDataProperties.h"

@implementation TargetImageDetail (CoreDataProperties)

+ (NSFetchRequest<TargetImageDetail *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"TargetImageDetail"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic descriptionImageDetail;
@dynamic latitude;
@dynamic leadId;
@dynamic leadImageCaption;
@dynamic leadImagePath;
@dynamic leadImageType;
@dynamic longitude;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic targetImageDetailId;
@dynamic targetSysName;
@dynamic userName;
@dynamic leadCommercialTargetId;
@dynamic mobileTargetId;
@dynamic mobileTargetImageId;
@dynamic isDelete;
@dynamic isSync;
@dynamic isNewGraph;

@end
