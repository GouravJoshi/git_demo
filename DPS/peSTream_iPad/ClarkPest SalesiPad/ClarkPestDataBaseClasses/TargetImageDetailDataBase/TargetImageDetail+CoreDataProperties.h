//
//  TargetImageDetail+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 25/11/19.
//
//

#import "TargetImageDetail+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TargetImageDetail (CoreDataProperties)

+ (NSFetchRequest<TargetImageDetail *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *descriptionImageDetail;
@property (nullable, nonatomic, copy) NSString *latitude;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *leadImageCaption;
@property (nullable, nonatomic, copy) NSString *leadImagePath;
@property (nullable, nonatomic, copy) NSString *leadImageType;
@property (nullable, nonatomic, copy) NSString *longitude;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *targetImageDetailId;
@property (nullable, nonatomic, copy) NSString *targetSysName;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *leadCommercialTargetId;
@property (nullable, nonatomic, copy) NSString *mobileTargetId;
@property (nullable, nonatomic, copy) NSString *mobileTargetImageId;
@property (nullable, nonatomic, copy) NSString *isDelete;
@property (nullable, nonatomic, copy) NSString *isSync;
@property (nullable, nonatomic, copy) NSString *isNewGraph;

@end

NS_ASSUME_NONNULL_END
