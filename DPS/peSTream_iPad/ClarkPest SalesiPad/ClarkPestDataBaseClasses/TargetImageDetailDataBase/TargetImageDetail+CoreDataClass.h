//
//  TargetImageDetail+CoreDataClass.h
//  
//
//  Created by Akshay Hastekar on 25/11/19.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface TargetImageDetail : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TargetImageDetail+CoreDataProperties.h"
