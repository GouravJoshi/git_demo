//
//  LeadCommercialTermsExtDc+CoreDataProperties.m
//  
//
//  Created by Rakesh Jain on 24/09/18.
//
//

#import "LeadCommercialTermsExtDc+CoreDataProperties.h"

@implementation LeadCommercialTermsExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialTermsExtDc *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LeadCommercialTermsExtDc"];
}

@dynamic leadCommercialTermsId;
@dynamic leadId;
@dynamic branchSysName;
@dynamic termsnConditions;
@dynamic userName;
@dynamic companyKey;
@dynamic termsId;
@end
