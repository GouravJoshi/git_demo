//
//  LeadCommercialTermsExtDc+CoreDataClass.h
//  
//
//  Created by Rakesh Jain on 24/09/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialTermsExtDc : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "LeadCommercialTermsExtDc+CoreDataProperties.h"
