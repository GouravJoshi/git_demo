//
//  LeadCommercialTermsExtDc+CoreDataProperties.h
//  
//
//  Created by Rakesh Jain on 24/09/18.
//
//

#import "LeadCommercialTermsExtDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialTermsExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialTermsExtDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadCommercialTermsId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *branchSysName;
@property (nullable, nonatomic, copy) NSString *termsnConditions;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *termsId;
@end

NS_ASSUME_NONNULL_END
