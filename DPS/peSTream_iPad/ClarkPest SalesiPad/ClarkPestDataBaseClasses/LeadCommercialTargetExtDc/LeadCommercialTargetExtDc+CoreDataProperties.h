//
//  LeadCommercialTargetExtDc+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 20/09/18.
//
//

#import "LeadCommercialTargetExtDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialTargetExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialTargetExtDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadCommercialTargetId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *targetSysName;
@property (nullable, nonatomic, copy) NSString *targetDescription;
@property (nullable, nonatomic, copy) NSString *isChangedTargetDesc;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *mobileTargetId;
@property (nullable, nonatomic, copy) NSString *wdoProblemIdentificationId;

@end

NS_ASSUME_NONNULL_END
