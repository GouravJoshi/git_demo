//
//  LeadCommercialTargetExtDc+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 20/09/18.
//
//

#import "LeadCommercialTargetExtDc+CoreDataProperties.h"

@implementation LeadCommercialTargetExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialTargetExtDc *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LeadCommercialTargetExtDc"];
}

@dynamic leadCommercialTargetId;
@dynamic leadId;
@dynamic targetSysName;
@dynamic targetDescription;
@dynamic isChangedTargetDesc;
@dynamic userName;
@dynamic companyKey;
@dynamic name;
@dynamic mobileTargetId;
@dynamic wdoProblemIdentificationId;
@end
