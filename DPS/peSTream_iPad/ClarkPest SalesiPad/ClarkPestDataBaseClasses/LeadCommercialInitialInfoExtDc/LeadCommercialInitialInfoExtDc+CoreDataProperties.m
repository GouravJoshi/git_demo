//
//  LeadCommercialInitialInfoExtDc+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 20/09/18.
//
//

#import "LeadCommercialInitialInfoExtDc+CoreDataProperties.h"

@implementation LeadCommercialInitialInfoExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialInitialInfoExtDc *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LeadCommercialInitialInfoExtDc"];
}

@dynamic leadCommInitialInfoId;
@dynamic leadId;
@dynamic initialDescription;
@dynamic userName;
@dynamic companyKey;
@dynamic initialPrice;
@end
