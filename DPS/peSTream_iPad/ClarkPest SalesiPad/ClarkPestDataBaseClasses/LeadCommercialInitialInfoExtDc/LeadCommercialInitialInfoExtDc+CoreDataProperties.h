//
//  LeadCommercialInitialInfoExtDc+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 20/09/18.
//
//

#import "LeadCommercialInitialInfoExtDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialInitialInfoExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialInitialInfoExtDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadCommInitialInfoId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *initialDescription;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *initialPrice;
@end

NS_ASSUME_NONNULL_END
