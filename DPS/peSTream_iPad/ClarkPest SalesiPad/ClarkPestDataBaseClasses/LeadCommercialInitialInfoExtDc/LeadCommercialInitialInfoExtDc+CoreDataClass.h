//
//  LeadCommercialInitialInfoExtDc+CoreDataClass.h
//  
//
//  Created by Akshay Hastekar on 20/09/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialInitialInfoExtDc : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "LeadCommercialInitialInfoExtDc+CoreDataProperties.h"
