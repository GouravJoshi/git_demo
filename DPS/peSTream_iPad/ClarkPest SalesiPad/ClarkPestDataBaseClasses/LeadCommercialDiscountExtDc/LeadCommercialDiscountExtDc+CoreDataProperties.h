//
//  LeadCommercialDiscountExtDc+CoreDataProperties.h
//
//
//  Created by Rakesh Jain on 24/09/18.
//
//

#import "LeadCommercialDiscountExtDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialDiscountExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialDiscountExtDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadCommercialDiscountId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *discountSysName;
@property (nullable, nonatomic, copy) NSString *discountType;
@property (nullable, nonatomic, copy) NSString *appliedInitialDiscount;
@property (nullable, nonatomic, copy) NSString *appliedMaintDiscount;
@property (nullable, nonatomic, copy) NSString *applicableForInitial;
@property (nullable, nonatomic, copy) NSString *applicableForMaintenance;
@property (nullable, nonatomic, copy) NSString *discountDescription;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *isDiscountPercent;
@property (nullable, nonatomic, copy) NSString *discountPercent;
@property (nullable, nonatomic, copy) NSString *accountNo;
@property (nullable, nonatomic, copy) NSString *discountCode;
@property (nullable, nonatomic, copy) NSString *name;

@end

NS_ASSUME_NONNULL_END

