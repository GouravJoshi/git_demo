//
//  LeadCommercialDiscountExtDc+CoreDataProperties.m
//
//
//  Created by Rakesh Jain on 24/09/18.
//
//

#import "LeadCommercialDiscountExtDc+CoreDataProperties.h"

@implementation LeadCommercialDiscountExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialDiscountExtDc *> *)fetchRequest {
    return [[NSFetchRequest alloc] initWithEntityName:@"LeadCommercialDiscountExtDc"];
}

@dynamic leadCommercialDiscountId;
@dynamic leadId;
@dynamic discountSysName;
@dynamic discountType;
@dynamic appliedInitialDiscount;
@dynamic appliedMaintDiscount;
@dynamic applicableForInitial;
@dynamic applicableForMaintenance;
@dynamic discountDescription;
@dynamic userName;
@dynamic companyKey;
@dynamic isDiscountPercent;
@dynamic discountPercent;
@dynamic accountNo;
@dynamic discountCode;
@dynamic name;
@end

