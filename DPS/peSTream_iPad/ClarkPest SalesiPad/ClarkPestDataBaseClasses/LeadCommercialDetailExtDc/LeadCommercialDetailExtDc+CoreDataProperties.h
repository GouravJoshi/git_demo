//
//  LeadCommercialDetailExtDc+CoreDataProperties.h
//  
//
//  Created by Akshay Hastekar on 03/10/18.
//
//

#import "LeadCommercialDetailExtDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialDetailExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialDetailExtDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *leadCommercialDetailId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *coverLetterSysName;
@property (nullable, nonatomic, copy) NSString *introSysName;
@property (nullable, nonatomic, copy) NSString *introContent;
@property (nullable, nonatomic, copy) NSString *termsOfServiceSysName;
@property (nullable, nonatomic, copy) NSString *termsOfServiceContent;
@property (nullable, nonatomic, copy) NSString *isAgreementValidFor;
@property (nullable, nonatomic, copy) NSString *validFor;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *isInitialTaxApplicable;
@property (nullable, nonatomic, copy) NSString *isMaintTaxApplicable;
@end

NS_ASSUME_NONNULL_END
