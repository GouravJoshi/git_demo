//
//  LeadCommercialDetailExtDc+CoreDataClass.h
//  
//
//  Created by Akshay Hastekar on 03/10/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeadCommercialDetailExtDc : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "LeadCommercialDetailExtDc+CoreDataProperties.h"
