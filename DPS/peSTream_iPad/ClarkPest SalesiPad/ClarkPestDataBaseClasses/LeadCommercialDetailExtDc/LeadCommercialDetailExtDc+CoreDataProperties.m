//
//  LeadCommercialDetailExtDc+CoreDataProperties.m
//  
//
//  Created by Akshay Hastekar on 03/10/18.
//
//

#import "LeadCommercialDetailExtDc+CoreDataProperties.h"

@implementation LeadCommercialDetailExtDc (CoreDataProperties)

+ (NSFetchRequest<LeadCommercialDetailExtDc *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LeadCommercialDetailExtDc"];
}

@dynamic leadCommercialDetailId;
@dynamic leadId;
@dynamic coverLetterSysName;
@dynamic introSysName;
@dynamic introContent;
@dynamic termsOfServiceSysName;
@dynamic termsOfServiceContent;
@dynamic isAgreementValidFor;
@dynamic validFor;
@dynamic userName;
@dynamic companyKey;
@dynamic isInitialTaxApplicable;
@dynamic isMaintTaxApplicable;
@end
