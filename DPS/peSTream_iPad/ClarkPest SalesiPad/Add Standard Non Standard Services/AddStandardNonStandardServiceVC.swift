//
//  AddStandardNonStandardServiceVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 26/09/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  Saavan 2020

import UIKit

enum EditType {
    case termsNonStan, descNonStan, descStan, none
}
class AddStandardNonStandardServiceVC: UIViewController
{
    
    // MARK: ----------------------- Outlets -----------------------
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnStandard: UIButton!
    
    @IBOutlet weak var btnNonStandard: UIButton!
    
    // MARK: --------- Outlets  View Standard ---------
    
    @IBOutlet weak var viewStandardService: UIView!
    
    @IBOutlet weak var const_ViewStandardService_H: NSLayoutConstraint!
    
    
    @IBOutlet weak var btnCategoryStandard: UIButton!
    
    
    
    @IBOutlet weak var btnServiceStandard: UIButton!
    
    
    @IBOutlet weak var btnFrequencyStandard: UIButton!
    
    @IBOutlet weak var txtViewServiceDescStandService: UITextView!
    
    @IBOutlet weak var txtViewInternalNotesStandService: UITextView!
    
    @IBOutlet weak var txtFldInitialPriceStanService: ACFloatingTextField!
    
    @IBOutlet weak var txtFldMaintPriceStandService: ACFloatingTextField!
    
    @IBOutlet weak var btnAddStandard: UIButton!
    
    @IBOutlet weak var btnCancelStandard: UIButton!
    
    // MARK: --------- Outlets  View Non Standard ---------
    
    @IBOutlet weak var viewNonStandardSerivce: UIView!
    
    @IBOutlet weak var const_ViewNonStandardService_H: NSLayoutConstraint!
    
    @IBOutlet weak var btnDepartmentNonStandard: UIButton!
    
    @IBOutlet weak var btnFreqNonStandard: UIButton!
    
    @IBOutlet weak var txtFldServiceNameNonStan: ACFloatingTextField!
    
    @IBOutlet weak var txtViewServiceDescNonStandService: UITextView!
    
    @IBOutlet weak var txtViewInternalNotesNonStandService: UITextView!
    
    @IBOutlet weak var txtFldInitialPriceNonStanService: ACFloatingTextField!
    
    @IBOutlet weak var txtFldMaintPriceNonStandService: ACFloatingTextField!
    
    @IBOutlet weak var btnAddNonStandard: UIButton!
    
    @IBOutlet weak var btnCancelNonStandard: UIButton!
    
    @IBOutlet weak var txtViewTermsConditionNonStan: UITextView!
    
    @IBOutlet weak var btnEditTermsCondition: UIButton!
    
    @IBOutlet weak var btnEditNonStanServiceDesctiption: UIButton!
    
    // MARK: ----------------------- Varibales -----------------------
    
    
    // MARK: - --------- Global Variables ---------
    
    @objc var strWoId = NSString ()
    @objc var strBranchSysName = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var strEmpName = String()
    var dictLoginData = NSDictionary()
    
    // MARK: - ---------Dictionary ---------
    
    var dictCategory = NSDictionary()
    var dictService = NSDictionary()
    var dictFrequency = NSDictionary()
    
    var dictDepartment = NSDictionary()
    var dictFrequencyNonStan = NSDictionary()
    var dictServiceNameFromSysname = NSDictionary()
    var dictServiceTermsFromSysname = NSDictionary()
    
    // MARK: - ---------Array ---------
    
    var arrCategory = NSArray()
    var arrService = NSArray()
    var arrFrequency = NSArray()
    
    var arrDepartment = NSMutableArray()
    // MARK: - ---------String ---------
    
    var strCategorySysName = String()
    var strCategoryName = String()
    var strServiceName = String()
    var strServiceId = String()
    
    var strServiceSysName = String()
    var strFreqencySysName = String()
    var strFreqencyName = String()
    
    var strDeptSysName = String()
    var strFreqencySysNameNonStan = String()
    var strFreqencyNameNonStan = String()
    var strHtmlTermsConditionNonStan = String()
    var strHtmlDescriptionNonStan = String()
    var strSoldServiceStandardId = String()
    var strSoldServiceNonStandardId = String()

    //MARK: - ---------Bool ---------
    
    var isChangeServiceDesc = Bool()
    var isEditDescNonStan = Bool()
    var isEditTermsNonStan = Bool()

    var editType = EditType.none//termsNonStan
    
    @objc var strForEdit = NSString ()
    @objc var matchesServiceEdit = NSManagedObject ()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        clearDefault()
        strCategorySysName = ""
        strServiceSysName = ""
        strFreqencySysName = ""
        strServiceId = ""
        strDeptSysName = ""
        strFreqencyNameNonStan = ""
        strFreqencySysNameNonStan = ""
        isChangeServiceDesc = false
        isEditDescNonStan = false
        isEditTermsNonStan = false
        strHtmlDescriptionNonStan = ""
        strHtmlTermsConditionNonStan = ""
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        
        if DeviceType.IS_IPAD
        {
            const_ViewStandardService_H.constant = 800
            
        }
        else
        {
            const_ViewStandardService_H.constant = 560
            
        }
        const_ViewNonStandardService_H.constant = 0
        btnStandard.backgroundColor = UIColor.theme()
        btnNonStandard.backgroundColor = UIColor.lightGray
        viewNonStandardSerivce.isHidden = true
        viewStandardService.isHidden = false
        
        setColorBorderForView(item: txtViewServiceDescStandService)
        setColorBorderForView(item: txtViewInternalNotesStandService)
        setColorBorderForView(item: txtViewServiceDescNonStandService)
        setColorBorderForView(item: txtViewInternalNotesNonStandService)
        setColorBorderForView(item: txtViewTermsConditionNonStan)
        
        buttonRound(sender: btnAddStandard)
        buttonRound(sender: btnCancelStandard)
        buttonRound(sender: btnAddNonStandard)
        buttonRound(sender: btnCancelNonStandard)
        
        //getServiceTerms()
        arrCategory = global.getCategoryDeptWiseGlobal()! as NSArray
        arrFrequency = getFrequency()
        getDepartmentNonStan()
        txtViewTermsConditionNonStan.isEditable = false
        txtViewServiceDescNonStandService.isEditable = false
        
        editServiceDetail()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if (nsud.value(forKey: "EditedHtmlContents") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "EditedHtmlContents")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "EditedHtmlContents")
                nsud.synchronize()
                
                //htmlContents
                
              //  txtViewTermsConditionNonStan.attributedText = htmlAttributedString(strHtmlString: "\(nsud.value(forKey: "htmlContents")!)")
            }
            
        }
      
        if (nsud.value(forKey: "EditedHtmlContents") != nil)
        {
            let data = Data("\(nsud.value(forKey: "htmlContents")!)".utf8)
            if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            {
                //attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 22), range: NSRange(location: 0, length: attributedString.length))
                if editType == .termsNonStan
                {
                    
                    txtViewTermsConditionNonStan.attributedText = attributedString
                    strHtmlTermsConditionNonStan = "\(nsud.value(forKey: "htmlContents") ?? "")"
                    
                    nsud.setValue("", forKey: "htmlContents")
                    
                    nsud.synchronize()
                    
                }
                else if editType == .descNonStan
                {
                    txtViewServiceDescNonStandService.attributedText = attributedString
                    strHtmlDescriptionNonStan = "\(nsud.value(forKey: "htmlContents") ?? "")"
                    
                    nsud.setValue("", forKey: "htmlContents")

                   
                    nsud.synchronize()

                }
                
            }
            
            
        }
        
        
    }
    
    // MARK: ----------------------- Actions -----------------------
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        endEditing()
        back()
        
    }
    
    @IBAction func actionOnStandardService(_ sender: Any)
    {
        endEditing()
        if DeviceType.IS_IPAD
        {
            const_ViewStandardService_H.constant = 800
            
        }
        else
        {
            const_ViewStandardService_H.constant = 560
            
        }
        const_ViewNonStandardService_H.constant = 0
        
        btnStandard.backgroundColor = UIColor.theme()
        btnNonStandard.backgroundColor = UIColor.lightGray
        viewNonStandardSerivce.isHidden = true
        viewStandardService.isHidden = false
        
    }
    
    @IBAction func actionOnNonStadardService(_ sender: Any)
    {
        showNonStandardView()
    }
    func showNonStandardView()
    {
        endEditing()
        
        const_ViewStandardService_H.constant = 0
        
        if DeviceType.IS_IPAD
        {
            const_ViewNonStandardService_H.constant = 1000
            
        }
        else
        {
            const_ViewNonStandardService_H.constant = 700
            
        }
        
        btnStandard.backgroundColor = UIColor.lightGray
        btnNonStandard.backgroundColor = UIColor.theme()
        
        viewNonStandardSerivce.isHidden = false
        viewStandardService.isHidden = true
    }
    
    // MARK: ------- Actions Standard Service -------
    
    
    @IBAction func actionOnServiceCategoryStandService(_ sender: Any)
    {
        endEditing()
        var arrOfData = NSMutableArray()
        arrOfData = NSMutableArray(array: arrCategory)
        
        
        if arrOfData.count > 0
        {
            btnCategoryStandard.tag = 200
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData ) , strTitle: "")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No category found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnServiceStandService(_ sender: Any)
    {
        endEditing()
        if strCategorySysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select category first", viewcontrol: self)
            
        }
        else
        {
            var arrOfData = NSMutableArray()
            
            arrService = getServiceNameFromCategory(strCategoryName: strCategorySysName)
            
            arrOfData = NSMutableArray(array: arrService)
            
            
            if arrOfData.count > 0
            {
                btnServiceStandard.tag = 201
                gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData ) , strTitle: "")
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No service found", viewcontrol: self)
            }
        }
        
        
    }
    
    @IBAction func actionOnFrequencyStandService(_ sender: Any)
    {
        endEditing()
        if strCategorySysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select category first", viewcontrol: self)
            
        }
        else if  strServiceSysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select service first", viewcontrol: self)
            
        }
        else
        {
            var arrOfData = NSMutableArray()
            
            arrOfData = NSMutableArray(array: arrFrequency)
            
            
            if arrOfData.count > 0
            {
                btnFrequencyStandard.tag = 202
                //gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData ) , strTitle: "")
                gotoPopViewWithArray(sender: sender as! UIButton, aryData: arrOfData , strTitle: "")
                
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No frequency found", viewcontrol: self)
            }
        }
        
        
    }
    
    @IBAction func actionOnAddStandardService(_ sender: Any)
    {
        endEditing()
        // || btnCategoryStandard.titleLabel?.text == "--Select Category--"
        // || btnServiceStandard.titleLabel?.text == "--Select Category--"
        // || btnFrequencyStandard.titleLabel?.text == "--Select Category--"
        if strCategorySysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select category", viewcontrol: self)
            
        }
        else if  strServiceSysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select service", viewcontrol: self)
            
        }
        else if  strFreqencySysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select frequency", viewcontrol: self)
            
        }
        else if  txtFldInitialPriceStanService.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter initial price", viewcontrol: self)
            
        }
        else if  txtFldMaintPriceStandService.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter maintenance price", viewcontrol: self)
        }
        else if  chkserviceExistOrNot() == true
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Service already exist", viewcontrol: self)
        }
        else
        {
            if strForEdit == "EditStandard"
            {
                
                let arrayData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialDiscountExtDc", predicate: NSPredicate(format: "leadId == %@ ", strWoId))
                
                if arrayData.count > 0
                {
                    let alert = UIAlertController(title: Alert, message: "Edit in price will delete all the coupon, credit and discount associated with service, Do you want to proceed ?", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
                        
                        self.updateServiceDetail()
                        self.deleteCouponCredit()
                        Global().updateSalesModifydate(self.strWoId as String)
                        self.back()

                    }))
                    alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                        
                    }))
                    
                    alert.popoverPresentationController?.sourceView = self.view
                    self.present(alert, animated: true, completion: {
                    })
                }
                else
                {
                    self.updateServiceDetail()
                    Global().updateSalesModifydate(self.strWoId as String)
                    self.back()
                }
    
            }
            else
            {
                saveStandardServiceToCoreData()
                Global().updateSalesModifydate(strWoId as String)
                back()
            }
           
        }
        
        /*
         if  ((txtFldInitialPriceStanService.text! as NSString).doubleValue < (txtFldMaintPriceStandService.text! as NSString).doubleValue)
         
         {
         showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Initial price must be greated than Maintenance price", viewcontrol: self)
         }
         */
    }
    
    @IBAction func actionOnCancelStandardService(_ sender: Any)
    {
        endEditing()
        back()
    }
    
    
    // MARK: ------- Actions NonStandard Service -------
    
    @IBAction func actionOnDepartmentNonStandService(_ sender: Any)
    {
        endEditing()
        var arrOfData = NSMutableArray()
        arrOfData = NSMutableArray(array: arrDepartment)
        
        
        if arrOfData.count > 0
        {
            btnDepartmentNonStandard.tag = 203
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData ) , strTitle: "")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No category found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnFrequencyNonStandService(_ sender: Any)
    {
        endEditing()
        if strDeptSysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select department first", viewcontrol: self)
            
        }
        else
        {
            var arrOfData = NSMutableArray()
            
            arrOfData = NSMutableArray(array: arrFrequency)
            
            
            if arrOfData.count > 0
            {
                btnFreqNonStandard.tag = 204
                gotoPopViewWithArray(sender: sender as! UIButton, aryData: arrOfData , strTitle: "")
                
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No frequency found", viewcontrol: self)
            }
        }
        
        
    }
    
    
    @IBAction func actionOnAddNonStandardService(_ sender: Any)
    {
        endEditing()
        // || btnDepartmentNonStandard.titleLabel?.text == "--Select Department--"
        // || btnFreqNonStandard.titleLabel?.text == "--Select Frequency--"
        
        if strDeptSysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select department", viewcontrol: self)
            
        }
        else if  strFreqencySysNameNonStan == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select frequency", viewcontrol: self)
            
        }
        else if  txtFldServiceNameNonStan.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter service name", viewcontrol: self)
            
        }
        else if  txtFldInitialPriceNonStanService.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter initial price", viewcontrol: self)
            
        }
        else if  txtFldMaintPriceNonStandService.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter maintenance price", viewcontrol: self)
        }
        else
        {
            if strForEdit == "EditNonStandard"
            {
                updateServiceDetail()
            }
            else
            {
                saveNonStandardServiceToCoreData()
            }
            Global().updateSalesModifydate(strWoId as String)
            back()
        }
        
    }
    
    @IBAction func actionOnCancelNonStandardService(_ sender: Any)
    {
        endEditing()
        back()
    }
    
    @IBAction func actionOnEditTermsCondition(_ sender: Any)
    {
        editType = EditType.termsNonStan
        let strTerms = "\(nsud.value(forKey: "htmlContents") ?? "")"
        htmlEditorView(htmlString: strHtmlTermsConditionNonStan )
    }
    
    @IBAction func actionOnEditNonStanServiceDesctiption(_ sender: Any)
    {
        editType = EditType.descNonStan
        let strTerms = "\(nsud.value(forKey: "htmlContents") ?? "")"
        htmlEditorView(htmlString: strHtmlDescriptionNonStan )
    }
    
    //MARK: ----------------------- Funcitons -----------------------
    
    
    func back()
    {
        clearDefault()
        navigationController?.popViewController(animated: false)
    }
    func clearDefault()
    {
        nsud.set("", forKey: "htmlContents")
        nsud.synchronize()
    }
    
    func setColorBorderForView(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func getServiceNameFromCategory(strCategoryName: String) -> NSArray
    {
        for item in arrCategory
        {
            let dict = item as! NSDictionary
            if "\(dict.value(forKey: "SysName")!)" == strCategorySysName
            {
                arrService = dict.value(forKey: "Services") as! NSArray
                break
            }
        }
        let arrSorted = NSMutableArray()
        
        for item in arrService
        {
            let dict = item as! NSDictionary
            
            //if (("\(dict.value(forKey: "IsActive") ?? "")" == "1" || "\(dict.value(forKey: "IsActive") ?? "")" == "true" || "\(dict.value(forKey: "IsActive") ?? "")" == "True") && ("\(dict.value(forKey: "IsDoortodoor") ?? "")" == "0" || "\(dict.value(forKey: "IsDoortodoor") ?? "")" == "false" || "\(dict.value(forKey: "IsDoortodoor") ?? "")" == "False"))
            //if "\(dict.value(forKey: "IsDoortodoor") ?? "")" == "0" || "\(dict.value(forKey: "IsDoortodoor") ?? "")" == "false" || "\(dict.value(forKey: "IsDoortodoor") ?? "")" == "False"

            //{
                arrSorted.add(dict)
            //}
        }
     
       // print(arrSorted)
        
        arrService = arrSorted

        return arrService
    }
    func getFrequency() -> NSArray
    {

            if(nsud.value(forKey: "MasterSalesAutomation") != nil){
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if(dictMaster.value(forKey: "Frequencies") is NSArray){
                    arrFrequency = NSArray()
                    arrFrequency = dictMaster.value(forKey: "Frequencies") as! NSArray
                
                    //Frequency as per type
                    
                    let arrTemp = NSMutableArray()
                    
                    if (arrFrequency.count  > 0)
                    {
                        for item in arrFrequency
                        {
                            let dict = item as! NSDictionary
                            let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                            if strType == "Service" || strType == "Both" || strType == ""
                            {
                                arrTemp.add(dict)
                            }
                        }
                    }
                    arrFrequency = arrTemp as NSArray
                }
            }
        return arrFrequency
    }
    
    func getDepartmentNonStan()
    {
        arrDepartment = NSMutableArray()
        
        /*
         guard let dictSalesLeadMaster = nsud.value(forKey: "LeadDetailMaster") as? NSDictionary else {
           // Gets to here
           return
         }
         guard let arrDept = dictSalesLeadMaster.value(forKey: "BranchMasters") as? NSArray else {
           // Gets to here
           return
         }
         */
        
        let  dictSalesLeadMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
        
        let arrDept = dictSalesLeadMaster.value(forKey: "BranchMasters") as! NSArray
        
        for itemBranch in arrDept
        {
            let dictBranch = itemBranch as! NSDictionary
            
            let arrTemp = dictBranch.value(forKey: "Departments") as! NSArray
            
            for itemDept in arrTemp
            {
                let dictDept = itemDept as! NSDictionary
                
                if ("\(dictBranch.value(forKey: "SysName")!)") == strBranchSysName as String
                {
                    arrDepartment.add(dictDept)
                }
            }
            
        }
    }
    
    func getServiceTerms()
    {
        //let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        guard let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as? NSDictionary else {
          // Gets to here
          return
        }
        
        //let arrCatergoryTemp = dictMaster.value(forKey: "Categories") as! NSArray
        guard let arrCatergoryTemp = dictMaster.value(forKey: "Categories") as? NSArray else {
          // Gets to here
          return
        }
        
        
        
        let arrServiceSysName = NSMutableArray()
        let arrServiceTerms = NSMutableArray()
        let arrServiceName = NSMutableArray()
        
        if arrCatergoryTemp.count > 0
        {
            for item in arrCatergoryTemp
            {
                let dict = item as! NSDictionary
                
                let arr = dict.value(forKey: "Services") as! NSArray
                
                if arr.count > 0
                {
                    for itemNew in arr
                    {
                        let dictNew = itemNew as! NSDictionary
                        
                        let strServiceName = "\(dictNew.value(forKey: "Name") ?? "")"
                        let strServiceSysName = "\(dictNew.value(forKey: "SysName") ?? "")"
                        let strServiceTerms = "\(dictNew.value(forKey: "TermsConditions") ?? "")"
                        
                        arrServiceName.add(strServiceName)
                        arrServiceSysName.add(strServiceSysName)
                        arrServiceTerms.add(strServiceTerms)

                    }
                }
                
                
            }
        }
        

        
        dictServiceNameFromSysname = NSDictionary(objects:arrServiceName as! [Any], forKeys:arrServiceSysName as! [NSCopying]) as Dictionary as NSDictionary

        dictServiceTermsFromSysname = NSDictionary(objects:arrServiceTerms as! [Any], forKeys:arrServiceSysName as! [NSCopying]) as Dictionary as NSDictionary
        
        print("\(dictServiceNameFromSysname) \(dictServiceTermsFromSysname) ")


    }
    
    func chkserviceExistOrNot() -> Bool
    {
        var chkExist = Bool()
        
        chkExist = false
        
        let arryOfWorkOrderData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ AND serviceSysName == %@", strWoId,strServiceSysName))
        
        if arryOfWorkOrderData.count > 0
        {
            
            chkExist = true
            
        }
        else
        {
            chkExist = false
            
        }
        
        if strForEdit == "EditStandard"
        {
            chkExist = false
        }
        
        return chkExist
    }
    
    func saveStandardServiceToCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        let arrAdditionalPara = NSMutableArray()
        
        var strIsChange = String()
        var strIntialPrice = String()
        var strMaintPrice = String()
        var strDescription = String()
        var strInternalNotes = String()
        
        strIntialPrice = txtFldInitialPriceStanService.text ?? ""
        strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strIntialPrice == ""
        {
            strIntialPrice = "0"
        }
        strMaintPrice = txtFldMaintPriceStandService.text ?? ""
        strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strMaintPrice == ""
        {
            strMaintPrice = "0"
        }
        
        
        
        strDescription = txtViewServiceDescStandService.text ?? ""
        strInternalNotes = txtViewInternalNotesStandService.text ?? ""
        
        if strDescription == "\(dictService.value(forKey: "Description") ?? "")".html2String
        {
            isChangeServiceDesc = false
        }
        else
        {
            isChangeServiceDesc = true
        }
        
        
        if isChangeServiceDesc
        {
            strIsChange = "true"
        }
        else
        {
            strIsChange = "false"
            
        }
        
        
        arrOfKeys = ["additionalParameterPriceDcs",
                     "billingFrequencyPrice",
                     "billingFrequencySysName",
                     "bundleDetailId",
                     "bundleId",
                     "companyKey",
                     "createdBy",
                     "createdDate",
                     "discount",
                     "discountPercentage",
                     "finalUnitBasedInitialPrice",
                     "finalUnitBasedMaintPrice",
                     "frequencySysName",
                     "initialPrice",
                     "isChangeServiceDesc",
                     "isCommercialTaxable",
                     "isResidentialTaxable",
                     "isSold",
                     "isTBD",
                     "leadId",
                     "maintenancePrice",
                     "modifiedBy",
                     "modifiedDate",
                     "modifiedInitialPrice",
                     "modifiedMaintenancePrice",
                     "packageId",
                     "serviceDescription",
                     "serviceFrequency",
                     "serviceId",
                     "servicePackageName",
                     "serviceSysName",
                     "serviceTermsConditions",
                     "soldServiceStandardId",
                     "totalInitialPrice",
                     "totalMaintPrice",
                     "unit",
                     "userName",
                     "internalNotes",
                     "categorySysName"]
        
        arrOfValues = [arrAdditionalPara,
                       "0",
                       "Monthly",
                       "0",
                       "0",
                       strCompanyKey,
                       strUserName,
                       global.strCurrentDate(),
                       "0",
                       "0",
                       strIntialPrice,
                       strMaintPrice,
                       strFreqencySysName,
                       strIntialPrice,
                       strIsChange,
                       "false",
                       "false",
                       "false",
                       "false",
                       strWoId,
                       strMaintPrice,
                       strUserName,
                       global.modifyDate(),
                       "",
                       "",
                       "0",
                       strDescription,
                       strFreqencyName,
                       strServiceId,
                       "",
                       strServiceSysName,
                       "\(dictService.value(forKey: "TermsConditions") ?? "")", //serviceTermsConditions
                       global.getReferenceNumber(),
                       strIntialPrice,
                       strMaintPrice,
                       "1",
                       strUserName,
                       strInternalNotes,
                       strCategorySysName]
        
        saveDataInDB(strEntity: "SoldServiceStandardDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    func saveNonStandardServiceToCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strIntialPrice = String()
        var strMaintPrice = String()
        var strDescription = String()
        var strInternalNotes = String()
        var strTermsCondition = String()
        
        
        strIntialPrice = txtFldInitialPriceNonStanService.text ?? ""
        strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strIntialPrice == ""
        {
            strIntialPrice = "0"
        }
        strMaintPrice = txtFldMaintPriceNonStandService.text ?? ""
        strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strMaintPrice == ""
        {
            strMaintPrice = "0"
        }
        
        strDescription = strHtmlDescriptionNonStan//txtViewServiceDescNonStandService.text ?? ""
        strInternalNotes = txtViewInternalNotesNonStandService.text ?? ""
        
        strTermsCondition = strHtmlTermsConditionNonStan//txtViewTermsConditionNonStan.text ?? ""
        
        
        arrOfKeys = ["billingFrequencyPrice",
                     "billingFrequencySysName",
                     "companyKey",
                     "createdBy",
                     "createdDate",
                     "departmentSysname",
                     "discount",
                     "discountPercentage",
                     "frequencySysName",
                     "initialPrice",
                     "isSold",
                     "leadId",
                     "maintenancePrice",
                     "modifiedBy",
                     "modifiedDate",
                     "modifiedInitialPrice",
                     "modifiedMaintenancePrice",
                     "nonStdServiceTermsConditions",
                     "serviceDescription",
                     "serviceFrequency",
                     "serviceName",
                     "soldServiceNonStandardId",
                     "userName",
                     "internalNotes",
                     "isTaxable"]
        
        arrOfValues = ["",
                       "",
                       strCompanyKey,
                       strUserName,
                       global.strCurrentDate(),
                       strDeptSysName,
                       "0",
                       "0",
                       strFreqencySysNameNonStan,
                       strIntialPrice,
                       "false",
                       strWoId,
                       strMaintPrice,
                       strUserName,
                       global.modifyDate(),
                       strIntialPrice,
                       strMaintPrice,
                       strTermsCondition,
                       strDescription,
                       strFreqencyNameNonStan,
                       txtFldServiceNameNonStan.text ?? "",
                       global.getReferenceNumber(),
                       strUserName,
                       strInternalNotes,
                       "false"]
        
        saveDataInDB(strEntity: "SoldServiceNonStandardDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    func endEditing()
    {
        self.view.endEditing(true)
    }
    
    func htmlEditorView(htmlString : String)
    {
        
        let storyboard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil) // "WDOiPad"
        
        let controller = storyboard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        
        controller.strHtml = htmlString
        controller.strfrom = "sales"
        self.present(controller, animated: false, completion: nil)
        
    }
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
    func editServiceDetail()
    {
        strSoldServiceStandardId = ""
        strSoldServiceNonStandardId = ""
        

        if strForEdit == "EditStandard"
        {
            
            btnAddStandard.setTitle("Update", for: .normal)

            btnNonStandard.isEnabled = false
            btnCategoryStandard.isEnabled = false
            btnServiceStandard.isEnabled = false
                        
            print("For Standard \(matchesServiceEdit)")
            
            strSoldServiceStandardId = "\(matchesServiceEdit.value(forKey: "soldServiceStandardId") ?? "")"
            
            strCategorySysName = "\(matchesServiceEdit.value(forKey: "categorySysName") ?? "")"
            
            dictCategory = NPMA_Proposal_iPadVC().getCategoryObjectFromSysName(strSysName: strCategorySysName)
            
            
            strCategoryName = "\(dictCategory.value(forKey: "Name") ?? "")"
            
            btnCategoryStandard.setTitle("\(dictCategory.value(forKey: "Name") ?? "")", for: .normal)
            
            strServiceSysName = "\(matchesServiceEdit.value(forKey: "serviceSysName") ?? "")"
            
            dictService = NPMA_Proposal_iPadVC().getServiceObjectFromSysName(strSysName: strServiceSysName)
            
            
            strServiceId = "\(dictService.value(forKey: "ServiceMasterId") ?? "")"
            
            strServiceName = "\(dictService.value(forKey: "Name") ?? "")"
            
            btnServiceStandard.setTitle("\(dictService.value(forKey: "Name") ?? "")", for: .normal)
            
            
            strFreqencySysName = "\(matchesServiceEdit.value(forKey: "frequencySysName") ?? "")"
            
            strFreqencyName = "\(matchesServiceEdit.value(forKey: "serviceFrequency") ?? "")"
            
            btnFrequencyStandard.setTitle("\(matchesServiceEdit.value(forKey: "serviceFrequency") ?? "")", for: .normal)
            
            
            txtFldInitialPriceStanService.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "initialPrice") ?? "")")
            
            txtFldMaintPriceStandService.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "maintenancePrice") ?? "")")
            
            txtViewServiceDescStandService.text = "\(matchesServiceEdit.value(forKey: "serviceDescription") ?? "")".html2String

            txtViewInternalNotesStandService.text = "\(matchesServiceEdit.value(forKey: "internalNotes") ?? "")".html2String
            
            for item in arrCategory
            {
                let dict = item as! NSDictionary
                
                if dict.value(forKey: "Services") is NSArray
                {
                    let arrServiceTemp = dict.value(forKey: "Services") as! NSArray
                    
                    for itemService in arrServiceTemp
                    {
                        let dictServiceNew = itemService as! NSDictionary
                        
                        if "\(dictServiceNew.value(forKey: "SysName" ) ?? "")" == "\(matchesServiceEdit.value(forKey: "serviceSysName") ?? "")"
                        {
                            strCategoryName = "\(dict.value(forKey: "Name") ?? "")"
                            strCategorySysName = "\(dict.value(forKey: "SysName") ?? "")"
                            break
                        }
                    }
                }
            }
            btnCategoryStandard.setTitle("\(strCategoryName)", for: .normal)
  
        }
        else if strForEdit == "EditNonStandard"
        {
            btnAddNonStandard.setTitle("Update", for: .normal)

            showNonStandardView()
            
            btnStandard.isEnabled = false
            
            print("For Non Standard \(matchesServiceEdit)")
            
            strSoldServiceNonStandardId = "\(matchesServiceEdit.value(forKey: "soldServiceNonStandardId") ?? "")"

            strDeptSysName = "\(matchesServiceEdit.value(forKey: "departmentSysname") ?? "")"
            
            for item in arrDepartment
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "SysName") ?? "")" == strDeptSysName
                {
                    btnDepartmentNonStandard.setTitle("\(dict.value(forKey: "Name") ?? "")", for: .normal)
                    
                    break;
                }
            }
            
            
            strFreqencySysNameNonStan = "\(matchesServiceEdit.value(forKey: "frequencySysName") ?? "")"
            
            strFreqencyNameNonStan = "\(matchesServiceEdit.value(forKey: "serviceFrequency") ?? "")"
            
            btnFreqNonStandard.setTitle("\(matchesServiceEdit.value(forKey: "serviceFrequency") ?? "")", for: .normal)
            
            txtFldServiceNameNonStan.text = "\(matchesServiceEdit.value(forKey: "serviceName") ?? "")"
            
            
            txtFldInitialPriceNonStanService.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "initialPrice") ?? "")")
            
            txtFldMaintPriceNonStandService.text = stringToFloat(strValue: "\(matchesServiceEdit.value(forKey: "maintenancePrice") ?? "")")
            
            
             
            strHtmlDescriptionNonStan = "\(matchesServiceEdit.value(forKey: "serviceDescription") ?? "")"
            
            if strHtmlDescriptionNonStan.count == 0
            {
                txtViewServiceDescNonStandService.text = ""
            }
            else
            {
                let data = Data("\(strHtmlDescriptionNonStan)".utf8)
                
                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                {
                    txtViewServiceDescNonStandService.attributedText = attributedString
                }

            }
            
            
            txtViewInternalNotesNonStandService.text = "\(matchesServiceEdit.value(forKey: "internalNotes") ?? "")"

            strHtmlTermsConditionNonStan = "\(matchesServiceEdit.value(forKey: "nonStdServiceTermsConditions") ?? "")"
            
            
            if strHtmlTermsConditionNonStan.count == 0
            {
                txtViewTermsConditionNonStan.text = ""
            }
            else
            {
                let data = Data("\(strHtmlTermsConditionNonStan)".utf8)
                
                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                {
                    txtViewTermsConditionNonStan.attributedText = attributedString
                }

            }
        }
    }
    func updateServiceDetail()
    {
        if strForEdit == "EditStandard"
        {
            
            var strIntialPrice = String()
            var strMaintPrice = String()
            var strServiceDescription = String()
            
            strIntialPrice = txtFldInitialPriceStanService.text ?? ""
            strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
            if strIntialPrice == ""
            {
                strIntialPrice = "0"
            }
            strIntialPrice = stringToFloat(strValue: strIntialPrice)
            
            strMaintPrice = txtFldMaintPriceStandService.text ?? ""
            strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
            if strMaintPrice == ""
            {
                strMaintPrice = "0"
            }
            strMaintPrice = stringToFloat(strValue: strMaintPrice)

            strServiceDescription = txtViewServiceDescStandService.text ?? ""
            
            if strServiceDescription == "\(dictService.value(forKey: "Description") ?? "")"
            {
                isChangeServiceDesc = false
            }
            else
            {
                isChangeServiceDesc = true
            }

            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            arrOfKeys = [
                "finalUnitBasedInitialPrice",
                "finalUnitBasedMaintPrice",
                "frequencySysName",
                "initialPrice",
                "maintenancePrice",
                "serviceDescription",
                "serviceFrequency",
                "totalInitialPrice",
                "totalMaintPrice",
                "internalNotes",
                "categorySysName",
                "isChangeServiceDesc",
                "discount",
                "discountPercentage"]
            
            
            arrOfValues = [
                strIntialPrice,
                strMaintPrice,
                strFreqencySysName,
                strIntialPrice,
                strMaintPrice,
                "\(txtViewServiceDescStandService.text ?? "")",
                strFreqencyName,
                strIntialPrice,
                strMaintPrice,
                "\(txtViewInternalNotesStandService.text ?? "")",
                strCategorySysName,
                isChangeServiceDesc ? "true" : "false",
                "0.0",
                "0.0"]
            
            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strWoId,strSoldServiceStandardId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            
            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
        }
        else if strForEdit == "EditNonStandard"
        {
            var strIntialPrice = String()
            var strMaintPrice = String()
            strIntialPrice = txtFldInitialPriceNonStanService.text ?? ""
            strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
            if strIntialPrice == ""
            {
                strIntialPrice = "0"
            }
            strMaintPrice = txtFldMaintPriceNonStandService.text ?? ""
            strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
            if strMaintPrice == ""
            {
                strMaintPrice = "0"
            }
            
            strIntialPrice = stringToFloat(strValue: strIntialPrice)
            strMaintPrice = stringToFloat(strValue: strMaintPrice)
            
            
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            
            
            arrOfKeys = [
                "departmentSysname",
                "frequencySysName",
                "initialPrice",
                "maintenancePrice",
                "modifiedInitialPrice",
                "modifiedMaintenancePrice",
                "nonStdServiceTermsConditions",
                "serviceDescription",
                "serviceFrequency",
                "serviceName",
                "internalNotes",
                "discount",
                "discountPercentage"
            ]
            
            arrOfValues = [
                strDeptSysName,
                strFreqencySysNameNonStan,
                strIntialPrice,
                strMaintPrice,
                strIntialPrice,
                strMaintPrice,
                strHtmlTermsConditionNonStan,
                strHtmlDescriptionNonStan,
                strFreqencyNameNonStan,
                txtFldServiceNameNonStan.text ?? "",
                txtViewInternalNotesNonStandService.text ?? "",
                "0.0",
                "0.0"
            ]

            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@",strWoId, strSoldServiceNonStandardId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)


            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
        }
    }
    func deleteCouponCredit()
    {
         let arrayData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialDiscountExtDc", predicate: NSPredicate(format: "leadId == %@ ", strWoId))
         for item in arrayData
         {
            let data = item as! NSManagedObject
            deleteDataFromDB(obj: data)
         }
    }
    
}

// MARK: --------------------- Tableview Delegate --------------

extension AddStandardNonStandardServiceVC : CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int)
    {
        if tag == 200
        {
            dictCategory = dictData
            strCategorySysName = "\(dictCategory.value(forKey: "SysName") ?? "")"
            strCategoryName = "\(dictCategory.value(forKey: "Name") ?? "")"
            
            btnCategoryStandard.setTitle("\(dictCategory.value(forKey: "Name") ?? "")", for: .normal)
            
            strServiceSysName = ""
            strFreqencySysName = ""
            btnServiceStandard.setTitle("--Select Service--", for: .normal)
            btnFrequencyStandard.setTitle("--Select Frequency--", for: .normal)
            
            
        }
        else if tag == 201
        {
            dictService = dictData
            strServiceSysName = "\(dictService.value(forKey: "SysName") ?? "")"
            
            strServiceId = "\(dictService.value(forKey: "ServiceMasterId") ?? "")"
            
            strServiceName = "\(dictService.value(forKey: "Name") ?? "")"
            btnServiceStandard.setTitle("\(dictService.value(forKey: "Name") ?? "")", for: .normal)
            
            txtViewServiceDescStandService.text = "\(dictService.value(forKey: "Description") ?? "")".html2String
        }
        else if tag == 202
        {
            dictFrequency = dictData
            strFreqencySysName = "\(dictFrequency.value(forKey: "SysName") ?? "")"
            strFreqencyName = "\(dictFrequency.value(forKey: "FrequencyName") ?? "")"
            btnFrequencyStandard.setTitle("\(dictFrequency.value(forKey: "FrequencyName") ?? "")", for: .normal)
        }
        else if tag == 203
        {
            dictDepartment = dictData
            strDeptSysName = "\(dictDepartment.value(forKey: "SysName") ?? "")"
            btnDepartmentNonStandard.setTitle("\(dictDepartment.value(forKey: "Name") ?? "")", for: .normal)
            
            strFreqencySysNameNonStan = ""
            btnFreqNonStandard.setTitle("--Select Frequency--", for: .normal)
            
        }
        else if tag == 204
        {
            dictFrequencyNonStan = dictData
            strFreqencySysNameNonStan = "\(dictFrequencyNonStan.value(forKey: "SysName") ?? "")"
            strFreqencyNameNonStan = "\(dictFrequencyNonStan.value(forKey: "FrequencyName") ?? "")"
            btnFreqNonStandard.setTitle("\(dictFrequencyNonStan.value(forKey: "FrequencyName") ?? "")", for: .normal)
        }
    }
    
    
    
}
// MARK: --------------------- Text Field Delegate --------------

extension AddStandardNonStandardServiceVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //yesEditedSomething = true
        
        if ( textField == txtFldInitialPriceStanService || textField == txtFldMaintPriceStandService  || textField == txtFldInitialPriceNonStanService  || textField == txtFldMaintPriceNonStandService  )
        {
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
            
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
            
        }
        else
        {
            
            return true
            
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
   
    
}
extension AddStandardNonStandardServiceVC : UITextViewDelegate
{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        /*if textView == txtViewServiceDescNonStandService
        {
            editType = EditType.descNonStan
            let strTerms = "\(nsud.value(forKey: "htmlContents") ?? "")"
            htmlEditorView(htmlString: strHtmlDescriptionNonStan )
        }
        if textView == txtViewTermsConditionNonStan
        {
            editType = EditType.termsNonStan
            let strTerms = "\(nsud.value(forKey: "htmlContents") ?? "")"
            htmlEditorView(htmlString: strHtmlTermsConditionNonStan )
        }*/
        
        return true
    }
}


