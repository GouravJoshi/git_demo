//
//  ClarkPestSalesAgreementProposaliPad.h
//  DPS changes undo changes
//
//  Created by Rakesh Jain on 18/09/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

//
//  SalesAutomationAgreementProposal.h
//  DPS CHANGES
//  Created by Rakesh Jain on 19/07/16.
//  Copyright © 2016 Saavan. All rights reserved. change
//

//
//  SalesAutomationAgreementProposal.h
//  DPS
//
//  Created by Rakesh Jain on 19/07/16.
//  Copyright © 2016 Saavan. All rights reserved. changes 
//change done
#import <WebKit/WebKit.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
// Clark Pest Data Base

#import "LeadCommercialScopeExtDc+CoreDataClass.h"
#import "LeadCommercialScopeExtDc+CoreDataProperties.h"

#import "LeadCommercialTargetExtDc+CoreDataClass.h"
#import "LeadCommercialTargetExtDc+CoreDataProperties.h"

#import "LeadCommercialDiscountExtDc+CoreDataClass.h"
#import "LeadCommercialDiscountExtDc+CoreDataProperties.h"

#import "LeadCommercialInitialInfoExtDc+CoreDataClass.h"
#import "LeadCommercialInitialInfoExtDc+CoreDataProperties.h"

#import "LeadCommercialMaintInfoExtDc+CoreDataClass.h"
#import "LeadCommercialMaintInfoExtDc+CoreDataProperties.h"

#import "LeadCommercialTermsExtDc+CoreDataClass.h"
#import "LeadCommercialTermsExtDc+CoreDataProperties.h"

#import "LeadCommercialDetailExtDc+CoreDataClass.h"
#import "LeadCommercialDetailExtDc+CoreDataProperties.h"


@interface ClarkPestSalesAgreementProposaliPad : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate,UITextViewDelegate,UITextFieldDelegate,AVAudioPlayerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UIWebViewDelegate>
{
    AVAudioPlayer *LandingSong;
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,*entityLeadAppliedDiscounts,*entityElectronicAuthorizedForm;
    
    
    AppDelegate *appDelegateAgreementList;
    NSManagedObjectContext *contextAgreementList;
    NSFetchRequest *requestNewAgreementList;
    NSSortDescriptor *sortDescriptorAgreementList;
    NSArray *sortDescriptorsAgreementList;
    NSManagedObject *matchesAgreementList;
    NSArray *arrAllObjAgreementList;
    NSEntityDescription
    *entityLeadAgreementChecklistSetups;
    
    //Clark Pest
    NSEntityDescription *entityLeadCommercialScopeExtDc,*entityLeadCommercialTargetExtDc,*entityLeadCommercialMaintInfoExtDc,*entityLeadCommercialInitialInfoExtDc,*entityLeadCommercialDiscountExtDc,*entityLeadCommercialDetailExtDc,*entityLeadCommercialTermsExtDc,*entityLeadMarketingContentExtDc;
    NSFetchRequest *requestClarkPest;
    NSManagedObject *matchesClarkPest;
    NSArray *arrAllObjClarkPest;
    NSSortDescriptor *sortDescriptorClarkPest;
    NSArray *sortDescriptorsClarkPest;
    AppDelegate *appDelegateClarkPest;
    NSManagedObjectContext *contextClarkPest;
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo,*fetchedResultsControllerSalesInfoAgreementList;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewLeads;
@property (strong, nonatomic) IBOutlet UIView *viewTop;
@property(strong,nonatomic)NSString *strFromSummary;

///***** PERSONAL INFO VIEW ******//
@property (weak, nonatomic) IBOutlet UIView *viewPersonalInfo;
- (IBAction)actionOnBack:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblAccountName;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerName;
@property (weak, nonatomic) IBOutlet UILabel *lblDateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceAddressValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingAddressValue;
@property (weak, nonatomic) IBOutlet UILabel *lblPrimaryPhoneValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondarPhoneValue;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailIdValue;
@property (weak, nonatomic) IBOutlet UILabel *lblInspectorValue;
///***** MAINTAINENANCE SERVICE VIEW ******//
@property (weak, nonatomic) IBOutlet UIView *viewMaintainanceService;
@property (weak, nonatomic) IBOutlet UITableView *tblMaintenanceService;
@property (weak, nonatomic) IBOutlet UILabel *lblInitialPricevValueMaintenanceService;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenancePriceMaintenanceService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntTblMaintenance_T;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntTblMaintenance_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntTblMaintenance_B;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSubtotalMainten_B;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrViewSubTotalMaintain_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnsrtViewMaintainService_H;

///***** NONSTANDARD SERVICE VIEW ******//

@property (weak, nonatomic) IBOutlet UIView *viewNonStandardService;
@property (weak, nonatomic) IBOutlet UITableView *tblNonStandardService;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalAmountValueNonStandardService;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntNonStandardTbl_H;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntNonStandardTbl_T;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntViewNonStandard_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrtntViewNonSubtotal;


///***** ADDITIONAL NOTES VIEW ******//
@property (weak, nonatomic) IBOutlet UIView *viewAdditionalNotes;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAdditionalNotes;

///***** PRICE INFORMATION VIEW ******//
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalAmountValuePriceInformation;
@property (weak, nonatomic) IBOutlet UILabel *lblCouponDiscountValuePriceInformation;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPriceValuePriceInformation;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxAmountValuePriceInformation;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingAmountPriceInformation;
@property (weak, nonatomic) IBOutlet UITextField *txtPaidAmountPriceInforamtion;

- (IBAction)actionOnCrash:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnCash;
@property (weak, nonatomic) IBOutlet UILabel *Cash;

- (IBAction)actionOnCheck:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UILabel *Check;

- (IBAction)actionOnCreditCard:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnCredit;

- (IBAction)actionOnAutoChangeCustomer:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnAutoChangeCustomer;

- (IBAction)actionOnCollectAtTimeOfScheduling:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtChequeValue;
@property (weak, nonatomic) IBOutlet UITextField *txtLicenseValue;
@property (weak, nonatomic) IBOutlet UITextField *txtExpirationDate;

@property (weak, nonatomic) IBOutlet UIButton *btnCollectTimeOfScheduling;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntViewPriceInfo_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntViewCreditCard_H;

@property (weak, nonatomic) IBOutlet UIView *viewCreditCard;

@property (weak, nonatomic) IBOutlet UITableView *tblMaintenance;

#pragma mark- VIEW TERMS & CONSITIONS 1 Sept
@property (weak, nonatomic) IBOutlet UIView *viewTermsConditions;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntViewTermsCondition_H;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckBoxCustomerNotPresent;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewInspectorSign;
- (IBAction)actionOnCustomerNotPresent:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *actionOnCustomerNotPresent;
@property (weak, nonatomic) IBOutlet UIButton *actionOnCustomerSignature;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCustomerSignature;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrntCustomerSignatureView_H;
- (IBAction)actionOnSaveContinue:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewCustomerSignature;
@property (weak, nonatomic) IBOutlet UIView *viewInspectorSignature;
// New Change

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constPersonalInfoView_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constAdditionNots_H;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewAgreement;
- (IBAction)actionOnServiceAgrrment:(id)sender;
- (IBAction)actionOnServiceProposal:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewPriceInformation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constScrollview_H;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCash;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCheck;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCreditCard;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAutoChangeCustomer;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCollectAtTimeOfScheduling;

// SIGNATURE CODE

- (IBAction)actionOnInspectorSign:(id)sender;
- (IBAction)actionOnCustomerSignature:(id)sender;
- (IBAction)actionOnExpirayDate:(id)sender;
- (IBAction)actionOnTermsConditions:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewTerms;

@property (strong, nonatomic) IBOutlet UIView *viewTerms;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollTerms;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)scrollBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTerms;
- (IBAction)btnTerms:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnTerms;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constScrollTop_H;
//InspectionDetail
@property (strong, nonatomic) IBOutlet UIView *viewInspection;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constViewInspection_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ScrollAgreement_H;
@property (weak, nonatomic) IBOutlet UIButton *btnServiceAgreement;
@property (weak, nonatomic) IBOutlet UIButton *btnServiceProposal;
@property (weak, nonatomic) IBOutlet UILabel *lblNameAcount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_AddNotes_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_PriceTerms_H;
@property (weak,nonatomic)NSString *strSummarySendPropsal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ServiceAgreement_W;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Proposal_Leading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ServiceProposal_W;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceProposal;
@property (weak, nonatomic) IBOutlet UILabel *lblInspection;
@property (weak, nonatomic) IBOutlet UILabel *lblInspectionLine;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveContinue;
- (IBAction)actionOnDynamicFormButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnInspector;
@property (weak, nonatomic) IBOutlet UIButton *btnCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblTxtPaidAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnInitialSetup;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)actionOnInitialSetup:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRecordAudio;
- (IBAction)action_RecordAudio:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnPlayAudio;
- (IBAction)action_PlayAudio:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnIAgree;

@property (strong, nonatomic) IBOutlet UIButton *btnGlobalSync;
- (IBAction)actionOnIAgree:(id)sender;
- (IBAction)actionOnGlobalSync:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;
- (IBAction)actionOnCheckFrontImage:(id)sender;

- (IBAction)actionOnCheckBackImage:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnExpirationDate;


//View For Final Save

@property (weak, nonatomic) IBOutlet UIView *viewForFinalSave;
- (IBAction)actionOnAudioBottom:(id)sender;

//View For After Image
@property (strong, nonatomic) IBOutlet UIView *viewForAfterImage;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewAfterImage;
- (IBAction)actionAddAfterImages:(id)sender;
- (IBAction)actionOnCancelAfterImage:(id)sender;
- (IBAction)actionOnAfterImgView:(id)sender;

//View For Graph Image
@property (strong, nonatomic) IBOutlet UIView *viewForGraphImage;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewGraphImage;
- (IBAction)actionOnAddGraphImage:(id)sender;

- (IBAction)actionOnCancelBeforeImage:(id)sender;
- (IBAction)actionOnGraphImageFooter:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPriceNonStandard;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountPriceStandard;
#pragma mark- New Bundle Change
@property (weak, nonatomic) IBOutlet UITableView *tblBundle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTblBundle_H;
- (IBAction)actionOnNotesHistory:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;

//Agreement CheckBox
@property (weak, nonatomic) IBOutlet UITableView *tblAgreement;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableAgreement_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewAgreement_H;
@property (weak, nonatomic) IBOutlet UIView *viewAgreementCheck;

@property (weak, nonatomic) IBOutlet UIButton *btnMarkAsLost;
- (IBAction)actionOnBtnMarkAsLost:(id)sender;

//View Preferred Months
@property (weak, nonatomic) IBOutlet UIView *viewPreferredMonth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewPreferredMonth_H;


@property (weak, nonatomic) IBOutlet UIButton *btnJan;
@property (weak, nonatomic) IBOutlet UIButton *btnFeb;
@property (weak, nonatomic) IBOutlet UIButton *btnMar;
@property (weak, nonatomic) IBOutlet UIButton *btnApr;
@property (weak, nonatomic) IBOutlet UIButton *btnMay;
@property (weak, nonatomic) IBOutlet UIButton *btnJun;
@property (weak, nonatomic) IBOutlet UIButton *btnJul;
@property (weak, nonatomic) IBOutlet UIButton *btnAug;
@property (weak, nonatomic) IBOutlet UIButton *btnSept;
@property (weak, nonatomic) IBOutlet UIButton *btnOct;
@property (weak, nonatomic) IBOutlet UIButton *btnNov;
@property (weak, nonatomic) IBOutlet UIButton *btnDec;

- (IBAction)actionOnJanuary:(UIButton *)sender;
- (IBAction)actionOnFebruary:(id)sender;
- (IBAction)actionOnMarch:(id)sender;
- (IBAction)actionOnApril:(id)sender;
- (IBAction)actionOnMay:(id)sender;
- (IBAction)actionOnJune:(id)sender;
- (IBAction)actionOnJuly:(id)sender;
- (IBAction)actionOnAugust:(id)sender;
- (IBAction)actionOnSeptember:(id)sender;
- (IBAction)actionOnOctober:(id)sender;
- (IBAction)actionOnNovember:(id)sender;
- (IBAction)actionOnDecember:(id)sender;
- (IBAction)actionBackToSchedule:(id)sender;

//View Top New Outlets and Actions
@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryPhone;
- (IBAction)actionOnPrimaryPhone:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondaryPhone;
- (IBAction)actionOnSecondaryPhone:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEmailId;
- (IBAction)actionOnEmailId:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPrintAgreement;
- (IBAction)actionOnPrintAgreement:(id)sender;
- (IBAction)actionOnServiceAddress:(id)sender;
- (IBAction)actionOnBillingAddress:(id)sender;

//VIEW CREDIT COUPON
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewCouponCredit_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblCoupon_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblCredit_H;

@property (weak, nonatomic) IBOutlet UIView *viewForCouponCredit;
@property (weak, nonatomic) IBOutlet UITextField *txtEnterCouponNo;
@property (weak, nonatomic) IBOutlet UIButton *btnApplyCoupon;
- (IBAction)actionOnApplyCoupon:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblCoupon;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalCoupon;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCredit;
- (IBAction)actionOnSelectCredit:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCredit;
- (IBAction)actionOnAddCredit:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblCredit;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalCredit;



//View New Initial and Maintenace Price

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewNewInitialMaintPrice_H;
@property (weak, nonatomic) IBOutlet UIView *viewNewPriceInfo;

@property (weak, nonatomic) IBOutlet UIView *viewNewInitialMaintPrice;
@property (weak, nonatomic) IBOutlet UITableView *tblNewInitialPriceCouponDiscount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblNewInitialPriceCouponDiscount_H;

@property (weak, nonatomic) IBOutlet UITableView *tblNewInitialPriceCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblNewInitialPriceCredit_H;


@property (weak, nonatomic) IBOutlet UITableView *tblNewMaintPriceCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblNewMaintPriceCredit_H;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceSubtotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceCouponDiscount;

@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceCredit;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceSubtotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceCredit;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceOtherDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceTaxAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewInitialPriceBillingAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceTaxAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblNewMaintPriceTotalDueAmount;

@property (weak, nonatomic) IBOutlet UIButton *buttonElectronicAuthorizedForm;
- (IBAction)actionOnElectroinicAuthorizedForm:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForAppliedCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewAppliedCredit_H;
@property (weak, nonatomic) IBOutlet UITableView *tblAppliedCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblAppliedCredit_H;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_lblCouponDisc_InitialPrice_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_lblCredit_InitialPrice_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_lblCredit_MaintPrice_H;
- (IBAction)actionOnChemicalSensitivityList:(id)sender;

//New Change
//
//View Cover Letter
- (IBAction)actionOnBtnCoverLetter:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewCoverLetter;
@property (weak, nonatomic) IBOutlet UIButton *btnCoverLetter;

- (IBAction)actionOnBtnIntroductionLetter:(id)sender;

//View Introduction letter

@property (weak, nonatomic) IBOutlet UIButton *btnIntroductionLetter;

@property (weak, nonatomic) IBOutlet UITextView *txtViewIntroductionLetter;
@property (weak, nonatomic) IBOutlet UIView *viewIntroductionLetter;


//Serivce Scope Target View
//Clark Pest Sales Changes

@property (weak, nonatomic) IBOutlet UIView *viewServiceDetail;

@property (weak, nonatomic) IBOutlet UITableView *tblService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblService_H;

@property (weak, nonatomic) IBOutlet UITableView *tblScope;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblScope_H;

@property (weak, nonatomic) IBOutlet UITableView *tblTarget;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblTarget_H;






//Agreement Valid For
- (IBAction)actionOnBtnTermsOfServices:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsOfServices;

@property (weak, nonatomic) IBOutlet UITextView *txtViewTermsServices;
@property (weak, nonatomic) IBOutlet UIButton *btnAgreementValidFor;
- (IBAction)actionOnBtnAgreementValidFor:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtDays;
@property (weak, nonatomic) IBOutlet UILabel *lblDays;

@property (weak, nonatomic) IBOutlet UIButton *btnTermsConditions;
- (IBAction)actionOnBtnTermsAndConditions:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *btnTaxApplicableInitialPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnTaxApplicableMaintPrice;

@property (weak, nonatomic) IBOutlet UIView *viewTermsOfService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewTermsOfService_H;


@property (weak, nonatomic) IBOutlet UIImageView *imgViewInvoice;
@property (weak, nonatomic) IBOutlet UIButton *btnInvoice;
- (IBAction)actionOnInvoice:(id)sender;

// View Sales Marketing Content
@property (weak, nonatomic) IBOutlet UIView *viewForSalesMarketingContent;
@property (weak, nonatomic) IBOutlet UIButton *btnMarketingContent;
- (IBAction)actionOnMarketingContent:(id)sender;


// Preview Image
@property (weak, nonatomic) IBOutlet UIView *viewForImagePreview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewForImagePreview_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_CollectionViewImgPreview_H;

@property (weak, nonatomic) IBOutlet UIButton *btnShowImagePreview;
- (IBAction)actionShowImagePreview:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewImgPreview;


//Preview Graph
@property (weak, nonatomic) IBOutlet UIView *viewForGraphPreview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewForGraphPreview_H;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewGraphPreview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_CollectionViewGraphPreview_H;
@property (weak, nonatomic) IBOutlet UIButton *btnShowGraphPreview;
- (IBAction)actionOnGraphPreview:(id)sender;

#pragma mark- Cover Letter Height

@property (weak, nonatomic) IBOutlet UITextView *txtViewCoverLetterForAgreement;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TxtViewCoverLetterAgreement;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewCoverLetter_H;

#pragma mark- Introduction Letter Height

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewIntroductionLetter_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TxtViewIntrocutionLetter_H;
//@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (weak, nonatomic) IBOutlet UIWebView *webViewCoverLetter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_WebViewCoverLetter_H;


@property (weak, nonatomic) IBOutlet UIWebView *webViewIntroductionLetter;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_WebViewintroductionLetter_H;


//Marketing COntent
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_SalesMarketingContent_H;

@property (weak, nonatomic) IBOutlet UIButton *btnCellNo;
- (IBAction)actionOnCellNo:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblCellNo;
@property (weak, nonatomic) IBOutlet UIView *viewElectronicAuthForm;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TargetLabel_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Target_Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Target_Bottom;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ScopeLabel_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Scope_Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Scope_Bottom;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ServiceLabel_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Service_Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Service_Bottom;

@property (weak, nonatomic) IBOutlet UITextView *txtViewAgreementCheckList;
@property (weak, nonatomic) IBOutlet UITextView *txtViewPreferredServiceMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailable;

//Non Standard Service
@property (weak, nonatomic) IBOutlet UITableView *tblNonStandardClarkPest;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblNonStan_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Lbl_NonStandard_H;
@property (weak, nonatomic) IBOutlet UIButton *btnShowHideCoverLetter;
- (IBAction)actionOnShowHideCoverLetter:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnShowHideIntroLetter;
- (IBAction)actionOnShowHideIntroLetter:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnShowHideSalesMarektingContent;
- (IBAction)actionOnShowHideSalesMarketingContent:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblOtherDiscountInitialPriceClarkPest;
@property (weak, nonatomic) IBOutlet UILabel *lblTipDiscountInitialPriceClarkPest;

@property (weak, nonatomic) IBOutlet UIButton *btnAllowCustomerToMakeSelection;

- (IBAction)actionOnAllowCustomerToMakeSelection:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_BtnAllowCustomer_H;

@property (weak, nonatomic) IBOutlet UILabel *lblInspectionFee;

@property (weak, nonatomic) IBOutlet UIView *viewInternalNotes;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewInternalNotes_H;
@property (weak, nonatomic) IBOutlet UITextView *txtViewInternalNotes;
@property (weak, nonatomic) IBOutlet UIButton *btnAddImageFooter;

@property (weak, nonatomic) IBOutlet UILabel *lblTaxAmountInitialPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblTaxAmountMaintPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnRefreshInspectorSign;

- (IBAction)actionOnRefreshInspectorSignature:(id)sender;

@end


