//
//  ClarkPestTableViewCellNonStandard.h
//  DPS
//
//  Created by Akshay Hastekar on 27/09/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ClarkPestTableViewCellNonStandard : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblServiceName;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToAgreement;
@property (weak, nonatomic) IBOutlet UIButton *btnIsChanged;

@property (weak, nonatomic) IBOutlet UILabel *lblInitialPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenancePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblFrequency;
@end

NS_ASSUME_NONNULL_END
