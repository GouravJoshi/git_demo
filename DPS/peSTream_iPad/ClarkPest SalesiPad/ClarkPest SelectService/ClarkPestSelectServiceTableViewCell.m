//
//  ClarkPestSelectServiceTableViewCell.m
//  DPS
//
//  Created by Rakesh Jain on 19/09/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "ClarkPestSelectServiceTableViewCell.h"

@implementation ClarkPestSelectServiceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
