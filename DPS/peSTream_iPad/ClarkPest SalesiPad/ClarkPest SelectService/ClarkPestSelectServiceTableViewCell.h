//
//  ClarkPestSelectServiceTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 19/09/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClarkPestSelectServiceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceName;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToAgreement;
@property (weak, nonatomic) IBOutlet UIButton *btnIsChanged;

@property (weak, nonatomic) IBOutlet UILabel *lblInitialPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenancePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblFrequency;
@property (weak, nonatomic) IBOutlet UIButton *btnAddTargetImages;
@property (weak, nonatomic) IBOutlet UIView *viewTargetImages;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewTargetImages_H;

@end

