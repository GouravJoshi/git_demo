//
//  ClarkPestSalesSelectServiceiPad.h
//  DPS changes for clark pest
//
//  Created by Rakesh Jain on 18/09/18.
//  Copyright © 2018 Saavan. All rights reserved.
//
//  SalesAutomationSelectService.h
//  DPS CHANGES changes Git
//
//  Created by Rakesh Jain on 19/07/16.
//  Copyright © 2016 Saavan. All rights reserved. changes
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
// Clark Pest Data Base

#import "LeadCommercialScopeExtDc+CoreDataClass.h"
#import "LeadCommercialScopeExtDc+CoreDataProperties.h"

#import "LeadCommercialTargetExtDc+CoreDataClass.h"
#import "LeadCommercialTargetExtDc+CoreDataProperties.h"

#import "LeadCommercialDiscountExtDc+CoreDataClass.h"
#import "LeadCommercialDiscountExtDc+CoreDataProperties.h"

#import "LeadCommercialInitialInfoExtDc+CoreDataClass.h"
#import "LeadCommercialInitialInfoExtDc+CoreDataProperties.h"

#import "LeadCommercialMaintInfoExtDc+CoreDataClass.h"
#import "LeadCommercialMaintInfoExtDc+CoreDataProperties.h"

#import "LeadCommercialTermsExtDc+CoreDataClass.h"
#import "LeadCommercialTermsExtDc+CoreDataProperties.h"

#import "LeadCommercialDetailExtDc+CoreDataClass.h"
#import "LeadCommercialDetailExtDc+CoreDataProperties.h"

@interface ClarkPestSalesSelectServiceiPad : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIActionSheetDelegate,UITextViewDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entitySoldServiceStandardDetail,*entityLeadAppliedDiscounts;
    
    
    //Clark Pest
    NSEntityDescription *entityLeadCommercialScopeExtDc,*entityLeadCommercialTargetExtDc,*entityLeadCommercialMaintInfoExtDc,*entityLeadCommercialInitialInfoExtDc,*entityLeadCommercialDiscountExtDc,*entityLeadCommercialDetailExtDc,*entityLeadCommercialTermsExtDc,*entityLeadMarketingContentExtDc;
    NSFetchRequest *requestClarkPest;
    NSManagedObject *matchesClarkPest;
    NSArray *arrAllObjClarkPest;
    NSSortDescriptor *sortDescriptorClarkPest;
    NSArray *sortDescriptorsClarkPest;
    AppDelegate *appDelegateClarkPest;
    NSManagedObjectContext *contextClarkPest;
    
    
    
    
}
@property (strong, nonatomic) IBOutlet UIButton *btnGlobalSync;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTaslList,*fetchedResultsControllerSalesInfo,*fetchedResultsControllerSalesInfoClarkPest;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewLeads;
@property (strong, nonatomic) IBOutlet UIView *viewTop;
- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblRecord;
@property (weak, nonatomic) IBOutlet UITableView *tblNonStandardService;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTable_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cons_View_H;
@property (weak, nonatomic) IBOutlet UIView *viewForNonStandardService;
@property (weak, nonatomic) IBOutlet UIView *viewForStandardService;
@property (weak, nonatomic) IBOutlet UIButton *btnStandardService;
@property (weak, nonatomic) IBOutlet UIButton *btnNonStandardService;

- (IBAction)actionOnStandardService:(id)sender;

- (IBAction)actionOnNonStandardService:(id)sender;
- (IBAction)actionOnAddNonStandardService:(id)sender;
- (IBAction)actionOnCategory1:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory1;
- (IBAction)actionCategory2:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory2;
- (IBAction)actionOnCategory3:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory3;
- (IBAction)actionOnSaveContinue:(id)sender;
- (IBAction)actionOnCancel:(id)sender;
#pragma mark- VIEWSTANDARD

@property (weak, nonatomic) IBOutlet UITextField *txtInitialPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscount;

@property (weak, nonatomic) IBOutlet UITextField *txtMaintenance;

@property (weak, nonatomic) IBOutlet UILabel *lblMaintenanceServiceStandard;
@property (weak, nonatomic) IBOutlet UILabel *lblBundleServiceStandard;

#pragma mark- VIEW NONSTANDARD
@property (weak, nonatomic) IBOutlet UITextField *txtServiceName;
@property (weak, nonatomic) IBOutlet UITextField *txtServiceDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtInitialPriceNonStandard;
@property (weak, nonatomic) IBOutlet UITextField *txtDiscountNonStandard;
@property (weak, nonatomic) IBOutlet UILabel *lblNameAcount;
- (IBAction)actionOnGlobalSync:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;
- (IBAction)actionOnChkBoxDiscount:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxDiscount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_Discount_H;
- (IBAction)actionOnDiscountCheckBox:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableStan_H;
- (IBAction)actionOnCheckBoxNonStan:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgChkBoxDiscountNonStan;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constViewDiscountNonStan_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableNonStan_H;

//View For Final Save

@property (weak, nonatomic) IBOutlet UIView *viewForFinalSave;

//View For After Image
@property (strong, nonatomic) IBOutlet UIView *viewForAfterImage;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewAfterImage;
- (IBAction)actionAddAfterImages:(id)sender;
- (IBAction)actionOnCancelAfterImage:(id)sender;
- (IBAction)actionOnAfterImgView:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewService;

//View For Graph Image
@property (strong, nonatomic) IBOutlet UIView *viewForGraphImage;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewGraphImage;
- (IBAction)actionOnAddGraphImage:(id)sender;

- (IBAction)actionOnCancelBeforeImage:(id)sender;
- (IBAction)actionOnGraphImageFooter:(id)sender;
#pragma mark- New Bundle Change
@property (weak, nonatomic) IBOutlet UITableView *tblBundle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTblBundle_H;
- (IBAction)actionOnNotesHistory:(id)sender;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;
@property (weak, nonatomic) IBOutlet UIButton *btnMarkAsLost;
- (IBAction)actionOnBtnMarkAsLost:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewFooter;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;
@property (weak, nonatomic) IBOutlet UITextField *txtFooter;




@property (weak, nonatomic) IBOutlet UITableView *tblCouponDiscount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TableDiscount_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewDiscountCoupon_H;
- (IBAction)actionOnApplyDiscount:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnApplyDiscount;
@property (weak, nonatomic) IBOutlet UITextField *txtApplyDiscount;
- (IBAction)actionOnChemicalSensitivityList:(id)sender;

//Clark Pest Sales Changes
@property (weak, nonatomic) IBOutlet UIView *viewServiceDetail;
@property (weak, nonatomic) IBOutlet UITableView *tblService;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblService_H;

@property (weak, nonatomic) IBOutlet UITableView *tblScope;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblScope_H;

@property (weak, nonatomic) IBOutlet UITableView *tblTarget;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblTarget_H;

@property (weak, nonatomic) IBOutlet UIButton *btnProposedServiceFreq;
- (IBAction)actionOnProposedServiceFreq:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnProposedServiceMonth;
- (IBAction)actionOnProposedServiceMonth:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldProposedInitialPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldProposedMaintPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldProposedServiceDate;


// Pop Up

@property (strong, nonatomic) IBOutlet UIView *viewPopUp;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescription_PopUp;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectOptions_PopUp;
- (IBAction)actionOnBtnSelection:(id)sender;
- (IBAction)actionOnSavePopUp:(id)sender;
- (IBAction)actionOnCancelPopUp:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ViewPopUp_Category_H;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectionCategory;
- (IBAction)actionOnSelectionCategory:(id)sender;

// working for commercial flow Akshay
@property (weak, nonatomic) IBOutlet UITableView *tableViewInitialPrice;
@property (weak, nonatomic) IBOutlet UITableView *tableViewMaintPrice;
@property (weak, nonatomic) IBOutlet UITextView *textViewInitialDescription;
@property (weak, nonatomic) IBOutlet UITextView *textViewMaintDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTblViewInitialPrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTblViewMaintPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtotalInitialPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtotalMaintPrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightSubtotalInitialPrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightSubtotalMaintPrice;
@property (weak, nonatomic) IBOutlet UIButton *buttonProposedInitialServiceDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewDropDownPopUp;
@property (weak, nonatomic) IBOutlet UIView *viewInitialMaintDetail;

//Non Standard Service
@property (weak, nonatomic) IBOutlet UITableView *tblNonStandardClarkPest;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblNonStan_H;


//View Cover Letter
- (IBAction)actionOnBtnCoverLetter:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewCoverLetter;
@property (weak, nonatomic) IBOutlet UIButton *btnCoverLetter;


//View Introduction letter

- (IBAction)actionOnBtnIntroductionLetter:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnIntroductionLetter;

@property (weak, nonatomic) IBOutlet UITextView *txtViewIntroductionLetter;
@property (weak, nonatomic) IBOutlet UIView *viewIntroductionLetter;

// View Sales Marketing Content
@property (weak, nonatomic) IBOutlet UIView *viewForSalesMarketingContent;
@property (weak, nonatomic) IBOutlet UIButton *btnMarketingContent;
- (IBAction)actionOnMarketingContent:(id)sender;


// View Terms of Services

- (IBAction)actionOnBtnTermsOfServices:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsOfServices;
@property (weak, nonatomic) IBOutlet UIView *viewTermsOfService;
@property (weak, nonatomic) IBOutlet UITextView *txtViewTermsServices;

@property (weak, nonatomic) IBOutlet UIButton *btnAddImageFooter;

@property (weak, nonatomic) IBOutlet UIButton *btnAddService;
- (IBAction)actionOnAddService:(id)sender;

@end

