//
//  SyncMasters.swift
//  DPS
//
//  Created by Saavan Patidar on 18/03/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class SyncMasters: UIViewController {
    
    //==============================================================================================
    // MARK: - ----------------------------Global Variables----------------------------
    //==============================================================================================
    
    var blurEffectView = UIVisualEffectView()
    let dispatchGroup = DispatchGroup()
    var strCompanyKey = String()
    var strCrmMainUrl = String()
    var strSalesMainUrl = String()
    var strServiceMainUrl = String()
    var strEmpNo = String()
    
    //==============================================================================================
    // MARK: - ----------------------------IB Outlets----------------------------
    //==============================================================================================
    
    @IBOutlet var imgViewLogo: UIImageView!
    
    
    
    //==============================================================================================
    // MARK: - ----------------------------Life Cycle----------------------------
    //==============================================================================================
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.blurEffects()
        
        self.callOnViewLoading()
        
    }
    
    //==============================================================================================
    // MARK: - ----------------------------Functions----------------------------
    //==============================================================================================
    
    func callOnViewLoading() {
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        strCompanyKey = String()
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            
            strCompanyKey = "\(value)"
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") {
            
            strCrmMainUrl = "\(value)"
            
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") {
            
            strSalesMainUrl = "\(value)"
            
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
            
            strServiceMainUrl = "\(value)"
            
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeNumber") {
            
            strEmpNo = "\(value)"
            
        }
        
        self.callApiMasters()
        
    }
    
    func blurEffects() {
        
        let blurEffect = UIBlurEffect(style: .light)
        
        if (blurEffectView.frame.size.width==0) {
            
            blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.imgViewLogo.frame
            
            self.imgViewLogo.insertSubview(blurEffectView, at: 0)
            
        }
        
    }
    
    func callApiMasters() {
        
        FTIndicator.showProgress(withMessage: "Syncing Masters Please Wait.....", userInteractionEnable: false)
        
        dispatchGroup.enter()
        self.getEmpList()
        
        dispatchGroup.enter()
        self.getCrmMasters()
        
        dispatchGroup.enter()
        self.getSalesMasters()
        
        dispatchGroup.enter()
        self.getSalesDyanamicFormMasters()
        
        dispatchGroup.enter()
        self.getServiceMasters()
        
        dispatchGroup.enter()
        self.getServiceDynamicFormMasters()
        
        dispatchGroup.enter()
        self.getEquipmentDynamicFormMaster()
        
        dispatchGroup.enter()
        self.getChemicalProducts()
        
        dispatchGroup.enter()
        self.getMechanicalMaster()
        
        dispatchGroup.notify(queue: .main) {
            
            FTIndicator.dismissProgress()
            print("Masters Sync Finished")
            self.dismiss(animated: false, completion: nil)
            
        }
        
    }
    
    
    //==============================================================================================
    // MARK: - ----------------------------CRM Masters----------------------------
    //==============================================================================================
    
    func getEmpList() {
        
        let strUrl = "\(strCrmMainUrl)\(UrlGetEmployeeList)\(strCompanyKey)"
        
        WebService.getRequestWithHeaders(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Message" , strUserDefaultKey : "SwiftEmployeeList") { (Response, Status) in
            
            self.dispatchGroup.leave()
            
            
        }
        
    }
    
    func getCrmMasters() {
        
        let strUrl = "\(strCrmMainUrl)\(UrlGetLeadDetailMaster)\(strCompanyKey)"
        
        WebService.getRequestWithHeaders(dictJson: NSDictionary(), url: strUrl , responseStringComing: "SwiftCrmMasters" , strUserDefaultKey : "array") { (Response, Status) in
            
            self.dispatchGroup.leave()
            
        }
        
    }
    
    
    //==============================================================================================
    // MARK: - ----------------------------Sales Masters----------------------------
    //==============================================================================================
    
    func getSalesMasters() {
        
        let strUrl = "\(strSalesMainUrl)\(UrlGetMasterSalesAutomation)\(strCompanyKey)"
        
        WebService.getRequestWithHeaders(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Message" , strUserDefaultKey : "SwiftSalesMasters") { (Response, Status) in
            
            self.dispatchGroup.leave()
            
            
        }
        
    }
    
    func getSalesDyanamicFormMasters() {
        
        let strUrl = "\(strSalesMainUrl)\(UrlSalesDynamicFormMasters)\(strCompanyKey)"
        
        WebService.getRequestWithHeaders(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Message" , strUserDefaultKey : "SwiftSalesDyanamicFormMasters") { (Response, Status) in
            
            self.dispatchGroup.leave()
            
        }
        
    }
    
    
    //==============================================================================================
    // MARK: - ----------------------------Service Masters----------------------------
    //==============================================================================================
    
    func getServiceMasters() {
        
        let strUrl = "\(strSalesMainUrl)\(UrlGetMasterServiceAutomation)\(strCompanyKey)"
        
        WebService.getRequestWithHeaders(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Message" , strUserDefaultKey : "SwiftServiceMasters") { (Response, Status) in
            
            self.dispatchGroup.leave()
            
        }
        
    }
    
    func getServiceDynamicFormMasters() {
        
        let strUrl = "\(strSalesMainUrl)\(UrlServiceDynamicFormMasters)\(strCompanyKey)"
        
        WebService.getRequestWithHeaders(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Message" , strUserDefaultKey : "SwiftServiceDynamicFormMasters") { (Response, Status) in
            
            self.dispatchGroup.leave()
            
        }
        
    }
    
    
    func getChemicalProducts() {
        
        let strUrl = "\(strSalesMainUrl)\(UrlGetMasterProductChemicalsServiceAutomation)\(strCompanyKey)"
        
        WebService.getRequestWithHeaders(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Message" , strUserDefaultKey : "SwiftChemicalProductsMasters") { (Response, Status) in
            
            self.dispatchGroup.leave()
            
        }
        
    }
    
    
    func getEquipmentDynamicFormMaster() {
        
        let strUrl = "\(strSalesMainUrl)\(UrlGetMasterEquipmentDynamicForm)\(strCompanyKey)"
        
        WebService.getRequestWithHeaders(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Message" , strUserDefaultKey : "SwiftEquipmentDynamicFormMaster") { (Response, Status) in
            
            self.dispatchGroup.leave()
            
        }
        
    }
    
    
    //==============================================================================================
    // MARK: - ----------------------------Mechanical Masters----------------------------
    //==============================================================================================
    
    func getMechanicalMaster() {
        
        let branchSysNameKey = "&branchSysName="
        
        let branchSysName = Global().strEmpBranchSysName()
        
        let strUrl = "\(strSalesMainUrl)\(UrlMechanicalGetAllMastersNew)\(strCompanyKey)\(UrlGetTotalWorkOrdersServiceAutomationEmployeeNo)\(strEmpNo)\(branchSysNameKey)\(branchSysName ?? "")"
        
        WebService.getRequestWithHeaders(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Message" , strUserDefaultKey : "SwiftMechanicalMaster") { (Response, Status) in
            
            self.dispatchGroup.leave()
            
        }
        
    }
    
}
