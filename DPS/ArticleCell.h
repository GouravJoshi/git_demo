//
//  SimpleTableCell.h
//  SimpleTable
//
//  Created by Simon Ng on 28/4/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCell : UITableViewCell
{

    IBOutlet UIButton *favButton;
    IBOutlet UILabel *dateLabel;
  
    
}

@property (nonatomic, weak) IBOutlet UILabel *Lblnumber
;
@property (nonatomic, weak) IBOutlet UILabel *lblPriority;

@property(nonatomic,weak)IBOutlet     IBOutlet UITextField *txtPrio;
@property (nonatomic, weak) IBOutlet UIButton *btnRemove;
@property (nonatomic, weak)IBOutlet UITextField *txtInCell;
@property (nonatomic, weak) IBOutlet UIButton *btnImgeFindings;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView *activityTbl;
@end
