//
//  ImagePreviewTermiteViewController.h
//  DPS
//
//  Created by Saavan Patidar on 07/12/17.
//  Copyright © 2017 Saavan. All rights reserved.
////

#import <UIKit/UIKit.h>
#import "GeneralInfoAppointmentView.h"

@interface ImagePreviewTermiteViewController : UIViewController
{
    IBOutlet UIImageView *imgView;
    IBOutlet UILabel *lblCount;
    
    NSMutableArray *arrayImages;
    
    int activeIndex;
}
- (IBAction)action_DeleteImage:(id)sender;
- (IBAction)action_Back:(id)sender;
@property(nonatomic,strong) NSMutableArray *arrOfImages;
@property(nonatomic,strong) NSDictionary *dictOfWorkOrdersImagePreview;
@property (strong, nonatomic) IBOutlet UILabel *imageName;
@property (strong, nonatomic) NSMutableArray *arrayOfImages;
@property (strong, nonatomic) NSString *imagePreviewName;
@property (strong, nonatomic) NSString *indexOfImage;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property(nonatomic,strong) NSString *statusOfWorkOrder;
@property (weak, nonatomic) IBOutlet UITextView *textViewImageCaption;
@property (weak, nonatomic) IBOutlet UIButton *btnEditCaption;
- (IBAction)action_EditCaption:(id)sender;
@property (strong, nonatomic) NSMutableArray *arrOfImageCaptionsSaved;
@property (strong, nonatomic) NSMutableArray *arrOfImageDescriptionSaved;
- (IBAction)action_EditImage:(id)sender;
- (IBAction)action_EditDescription:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textViewImageDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnEditDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnEditImage;
@property (strong, nonatomic) NSString *strForView;
@property (strong, nonatomic) NSString *strFromInvoice;


@end
