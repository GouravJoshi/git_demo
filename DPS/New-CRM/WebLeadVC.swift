//
//  WebLeadVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 20/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class WebLeadVC: UIViewController
{
    // MARK: -  ----------------------------------- Outlet  -----------------------------------
 
    weak var delegateTaskAssociate: AddTaskAssociateProtocol?
    weak var delegateActivityAssociate: AddActivityAssociateProtocol?
    var selectionTag = Int()
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var scrollViewLeads: UIScrollView!

    @IBOutlet weak var containerView: UIView!
    //@IBOutlet weak var tblViewLeads: UITableView!
    
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    @IBOutlet weak var headerView: TopView!

    
    //New Change
    @IBOutlet weak var btnWebLead: UIButton!
    @IBOutlet weak var btnOpportunity: UIButton!
    @IBOutlet weak var viewTopButton: UIView!
    @IBOutlet weak var btnLeadNew: UIButton!
    @IBOutlet weak var const_BtnBack_W: NSLayoutConstraint!
    @IBOutlet weak var const_Footer_H: NSLayoutConstraint!
    @IBOutlet weak var const_HomeButton_L: NSLayoutConstraint!
    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    var strTypeOfAssociations = String()

    let global = Global()
    var loader = UIAlertController()
    // MARK: -  ----------------------------------- Variable Declaration -----------------------------------
    
    // MARK: -  --- String ---
    var strFromDate = String()
    var strToDate = String()
    var strCurrentTab = String()
     var leadCount = String()
    // MARK: -  --- Array ---
    
//    var arrOfLeads = NSArray()
//    var arrAllWebLeads = NSMutableArray()
//    var arrAllWebLeadsData = NSMutableArray()

    
    var arrOfStatus = NSMutableArray()
    var arrOfStatusName = NSMutableArray()
   // var arrAllWebLeadsCount = NSMutableArray()
    
    // MARK: -  --- NSDisctionary ---
    
    var dictLoginData = NSDictionary()
    
    // MARK: -  --- Bool ---
    
    var yesFiltered = Bool()
    var chkFromfilter = Bool()
    
    // MARK: -  --- Int ---
    
    var swipeCount = Int()
    var currentSwipeCount = Int()
    
    var strSearchText = ""
    var arrOfLeadNow = LeadNowModel()

    @IBOutlet weak var lbltaskCount: UILabel!
    @IBOutlet weak var lblScheduleCount: UILabel!

    private lazy var LeadNowMainVC: LeadNowMainVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "RuchikaLeadNow", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "LeadNowMainVC") as! LeadNowMainVC

        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()

    //MARK: - View Will Apperar
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            nsud.set(nil, forKey: "webLeadFilterData")

            if(DeviceType.IS_IPAD){
    //            lbltaskCount.text = "0"
    //            lblScheduleCount.text = "0"
    //            lbltaskCount.layer.cornerRadius = 18.0
    //            lbltaskCount.backgroundColor = UIColor.red
    //            lblScheduleCount.layer.cornerRadius = 18.0
    //            lblScheduleCount.backgroundColor = UIColor.red
    //            lbltaskCount.layer.masksToBounds = true
    //            lblScheduleCount.layer.masksToBounds = true
            }
            if #available(iOS 13.0, *) {
                      searchBar.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
                  } else {
                      // Fallback on earlier versions
                  }
    //              tblViewLeads.tableFooterView = UIView()
    //              tblViewLeads.estimatedRowHeight = 55.0
      
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
            swipeLeft.direction = .left
         //   self.tblViewLeads!.addGestureRecognizer(swipeLeft)
            
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
            swipeRight.direction = .right
            
         //   self.tblViewLeads!.addGestureRecognizer(swipeRight)
            swipeCount = 0
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
            
            strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
            
            strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
            
            
            strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            
            strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            
            strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            yesFiltered = false
         //   arrOfLeads = ["New", "Complete", "Incomplete", "Scheduled", "Defered"]
            

            
            getStartDateEndDate()
           // getStatus()

            defaultSwipeCount()
            
            addButton()
         
            
            let isForAssociation = nsud.bool(forKey: "fromWebLeadAssociation")
            //isForAssociation = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setTopMenuOption(tagForAssociation: isForAssociation)
            })
            if isForAssociation == true
            {
                const_BtnBack_W.constant = DeviceType.IS_IPAD ? 84 : 40
                btnLeadNew.isHidden = false
                btnWebLead.isHidden = true
                btnOpportunity.isHidden = true
                const_Footer_H.constant = 0
                const_HomeButton_L.constant = DeviceType.IS_IPAD ? 0 : 5
              
            }
            else
            {
                const_BtnBack_W.constant = 0
                btnLeadNew.isHidden = true
                btnWebLead.isHidden = false
                btnOpportunity.isHidden = false
                const_Footer_H.constant = DeviceType.IS_IPAD ? 115 : 85
                const_HomeButton_L.constant = DeviceType.IS_IPAD ? 0 : 15
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                        self.setFooterMenuOption()
                    })
            }
        
            setUpView()
           // createBadgeView()
            nsud.set(true, forKey: "fromWebLeadDetailTableAction")

        }
    
        
    }
    
    //MARK: - Custom Functions
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)

        // Add Child View as Subview
        containerView.addSubview(viewController.view)

        // Configure Child View
         viewController.view.frame = containerView.bounds
         viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)

        // Remove Child View From Superview
        viewController.view.removeFromSuperview()

        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
    private func updateView() {
        remove(asChildViewController: LeadNowMainVC)
        add(asChildViewController: LeadNowMainVC)
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        let isForAssociation = nsud.bool(forKey: "fromWebLeadAssociation")
        if isForAssociation == false
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setFooterMenuOption()
            })
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setTopMenuOption(tagForAssociation: isForAssociation)
        })
      }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if !Check_Login_Session_expired() {
            if nsud.bool(forKey: "fromWebLeadDetail") == true
            {
                nsud.set(false, forKey: "fromWebLeadDetail")
                nsud.synchronize()
               /* if self.arrOfStatus.count > 0
                {
                    let strStatus = "\(self.arrOfStatus.object(at: 0))"
                    self.fetchWebLeadFromCoreData(strStatus: "\(strStatus)")
                }
                else
                {
                    self.fetchWebLeadFromCoreData(strStatus: "New")
                }*/
                
                if nsud.bool(forKey: "fromWebLeadDetailTableAction")
                {
                   /* if self.arrOfStatus.count > 0
                    {
                        let strStatus = "\(self.arrOfStatus.object(at: 0))"
                        self.fetchWebLeadFromCoreData(strStatus: "\(strStatus)")
                    }
                    else
                    {
                        self.fetchWebLeadFromCoreData(strStatus: "New")
                     }*/
                    swipeCount = 0
                    defaultSwipeCount()
                   // getStatus()
                    addButton()
                   // getAllWebLeads()
                    addButton()
                    nsud.set(false, forKey: "fromWebLeadDetailTableAction")
                    nsud.synchronize()
                }
                else
                {
                    nsud.setValue("", forKey: "cureentTab")
                    nsud.setValue("", forKey: "cureentSwipeCount")
                    nsud.synchronize()
                   // tblViewLeads.reloadData()
                }

            }
            else
            {
                if nsud.bool(forKey: "fromWebLeadFilter") == true
                {
                    chkFromfilter = true
                    swipeCount = 0
                    defaultSwipeCount() //temp comment
                    //defaultSwipeCountFilter() //temp comment
                    //getStatus()
                   // addBorder()
                  //  getAllWebLeads()
                     addButton()
                   
                }
                else
                {
                    chkFromfilter = false
                    swipeCount = 0
                    defaultSwipeCount()
                  //  getStatus()
                    addButton()
                    getStartDateEndDate()
                 //   getAllWebLeads()

                }
            }
        
            let isForAssociation = nsud.bool(forKey: "fromWebLeadAssociation")
            //isForAssociation = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setTopMenuOption(tagForAssociation: isForAssociation)
            })
            getStartDateEndDate()
    //
            if nsud.bool(forKey: "onlyPropertyAndUrgencyApplied") == true{
                print("Not to call API")
                if(nsud.value(forKey: "webLeadFilterData") != nil) {
                    let dictFilterData = nsud.value(forKey: "webLeadFilterData") as! NSDictionary
                    if(dictFilterData.count != 0){
                        let arySelectedPropertyType = (dictFilterData.value(forKey: "selectedPropertyTypeFromFilter") as! [[String: Any]])
                        let arySelectedUrgencyType = (dictFilterData.value(forKey: "selectedUrgencyTypeFromFilter") as! [[String: Any]])
                                            
                        NotificationCenter.default.post(name: Notification.Name("MoveToLeadMainVC"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "PropertyType": arySelectedPropertyType, "UrgencyType": arySelectedUrgencyType ])

                    }
                }
                else{
                    NotificationCenter.default.post(name: Notification.Name("MoveToLeadMainVC"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "PropertyType": [], "UrgencyType": []])
                }
                
                nsud.set(false, forKey: "onlyPropertyAndUrgencyApplied")
            }
            else{
                updateView()
                callWebServiceToGetLeadData()
                nsud.set(false, forKey: "onlyPropertyAndUrgencyApplied")
            }
            
            nsud.set(false, forKey: "forEditWebLead")
            nsud.set(false, forKey: "fromWebLeadFilter")
            nsud.set(false, forKey: "fromAddUpdateTask")
            nsud.set(false, forKey: "fromAddUpdateActivity")
            nsud.set(false, forKey: "fromWebLeadDetailTableAction")

            nsud.synchronize()
            
            /* let storyboardIpad = UIStoryboard.init(name: "CRM_NewiPhone", bundle: nil)
             let child = storyboardIpad.instantiateViewController(withIdentifier: "LeadVC") as! LeadVC
             child.view.frame = CGRect(x: 0, y: 100, width: (self.view.frame.width), height: (self.view.frame.height))
             self.addChild(child)
             self.view.addSubview(child.view)
             child.didMove(toParent: self)*/
            
           if strTypeOfAssociations == "AddTaskCRM_Associations"
           {
               nsud.set(strTypeOfAssociations, forKey: "strTypeOfAssociationsFromWebLead")
           }
            else if strTypeOfAssociations == "AddActivityCRM_Associations"
            {
                nsud.set(strTypeOfAssociations, forKey: "strTypeOfAssociationsFromWebLead")
            }

        }
       
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        searchBar.text = ""
        searchBar.text = nil
        searchBar.endEditing(true)
    }
    
    
    // MARK: ---------------------------Footer Functions ---------------------------
    func setFooterMenuOption() {
        
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        
        nsud.setValue("Sales", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
            self.view.endEditing(true)
            self.goToDasboard()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
            self.view.endEditing(true)
            self.goToAppointment()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.goToContactList()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
           
        }
        
       
    }

    // MARK: ---------------------------Header Functions ---------------------------
    func setTopMenuOption(tagForAssociation : Bool) {
        for view in self.view.subviews {
            if(view is HeaderView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue(tagForAssociation == true ? "LeadAssosi" : "Lead", forKey: "DPS_TopMenuOptionTitle")
        nsud.synchronize()
        var headerViewtop = HeaderView()
         headerViewtop = HeaderView.init(frame: CGRect(x: 0, y: 0, width: self.headerView.frame.size.width, height:self.headerView.frame.size.height))
        headerViewtop.searchBarTask.delegate = self
        
        if #available(iOS 13.0, *) {
            headerViewtop.searchBarTask.searchTextField.text = self.strSearchText
        } else {
            headerViewtop.searchBarTask.text = self.strSearchText
        }
        
        self.headerView.addSubview(headerViewtop)
        headerViewtop.onClickBackButton = {() -> Void in
            nsud.set(false, forKey: "fromWebLeadAssociation")
            nsud.synchronize()
            self.navigationController?.popViewController(animated: false)
        }
        headerViewtop.onClickFilterButton = {() -> Void in
            self.goToFilter()
        }
        headerViewtop.onClickAddButton = {() -> Void in
            self.goToAddLead()
        }
        headerViewtop.onClickVoiceRecognizationButton = { () -> Void in
            
            self.view.endEditing(true)
            if self.arrOfLeadNow.arrOfLeads.count != 0 {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.delegate = self
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: {})
            }
        }
        headerViewtop.onClickTopMenuButton = { [self](str) -> Void in
            print(str)
            switch str {
            case "Map":
                self.goToNearBy()
                break
            case "Lead":
               updateView()
                break
            case "Opportunity":
                self.goToOpportunityVC()
                break
            case "Tasks":
                self.GotoTaskViewController()
                break
            case "Activity":
                self.GotoActivityViewController()
                break
            case "View Schedule":
                self.GotoViewScheduleViewController()
                break
            case "Signed Agreements":
                self.goToSignedViewAgreement()
                break
            default:
                break
            }
            
        }
        
    }
    
    
    func GotoViewScheduleViewController() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ScheduleD2D" : "ScheduleD2D", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoTaskViewController() {
        
         let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone

        testController.strFromVC = "DashBoardView"//"WebLeadVC"
        self.navigationController?.pushViewController(testController, animated: false)
    }
    
    func GotoActivityViewController()  {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
        //vc.strFromVC = strFromVC
        //nsud.setValue(nil, forKey: "dictFilterActivity")
        //nsud.synchronize()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    //MARK:
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
           if gesture.direction == UISwipeGestureRecognizer.Direction.right {
               swipeCount = swipeCount - 1
               
               if (swipeCount < 0)
               {
                   swipeCount = 0
               }
               print("Swipe count \(swipeCount)")
                addButton()
               
               let strStatus = "\(arrOfStatus.object(at: swipeCount))"
              // fetchWebLeadFromCoreData(strStatus: strStatus)
               
               if swipeCount == 0
               {
                   scrollViewLeads.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                   
               }
               else
               {
                   scrollViewLeads.setContentOffset(CGPoint(x: (swipeCount + 1)*100 - 200, y: 0), animated: true)
               }
           }
           else if gesture.direction == UISwipeGestureRecognizer.Direction.left {
               swipeCount = swipeCount + 1
               
               if (swipeCount >= arrOfStatus.count)
               {
                   swipeCount = arrOfStatus.count - 1
               }
               print("Swipe count \(swipeCount)")
               addButton()
               
               let strStatus = "\(arrOfStatus.object(at: swipeCount))"
               //fetchWebLeadFromCoreData(strStatus: strStatus)
               
               if swipeCount == 0
               {
                   scrollViewLeads.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                   
               }
               else
               {
                   scrollViewLeads.setContentOffset(CGPoint(x: (swipeCount + 1)*100 - 200, y: 0), animated: true)
               }
               
           }
           else if gesture.direction == UISwipeGestureRecognizer.Direction.up {
               print("Swipe Up")
           }
           else if gesture.direction == UISwipeGestureRecognizer.Direction.down {
               print("Swipe Down")
           }
       }
    // MARK: -  ------------------------------ Action ------------------------------
    
    @IBAction func actionOnBack(_ sender: Any)
    {
       /* if nsud.bool(forKey: "fromCompanyVC")
        {
            nsud.set(false, forKey: "fromCompanyVC")
            nsud.synchronize()
            self.navigationController?.popViewController(animated: false)
            
        }
        else
        {
            for controller in self.navigationController!.viewControllers as NSArray
            {
                if controller is DashBoardView
                {
                    self.navigationController!.popToViewController(controller as! UIViewController, animated: false)
                    break
                }
            }
        }*/
        nsud.set(false, forKey: "fromWebLeadAssociation")
        nsud.synchronize()
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func actionOnLead(_ sender: Any)
    {
        
    }
    
    @IBAction func actionOnOpportunity(_ sender: Any)
    {
        goToOpportunityVC()
    }
    
    @IBAction func actionOnSegmentControl(_ sender: Any)
    {
        if segmentController.selectedSegmentIndex == 0
        {
            
        }
        else if segmentController.selectedSegmentIndex == 1
        {
            goToOpportunityVC()
        }
    }
    
    @IBAction func actionOnFilter(_ sender: Any)
    {
        goToFilter()
        
        
    }
    
    @IBAction func actionOnSearch(_ sender: Any)
    {
     
    }
    
    
    @IBAction func actionOnAdd(_ sender: Any)
    {
        goToAddLead()
    }
    
    @IBAction func actionOnTaskActivity(_ sender: UIButton)
    {
        
        self.GotoTaskViewController()

        
    }
    
    @IBAction func actionOnNearBy(_ sender: Any)
    {
        goToNearBy()
    }
    
    @IBAction func actionOnContactList(_ sender: Any)
    {
        goToContactList()
    }
    
    @IBAction func actionOnMore(_ sender: UIButton)
    {
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
           alert.view.tintColor = UIColor.black
        
        let Near = (UIAlertAction(title: "Near By", style: .default , handler:{ (UIAlertAction)in
            
            self.goToNearBy()
            
        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
        Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Near)
        let Signed = (UIAlertAction(title: "Signed View Agreements", style: .default , handler:{ (UIAlertAction)in
            self.goToSignedViewAgreement()
        }))
        
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
        Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Signed)
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    
    @IBAction func actionOnDashboard(_ sender: Any)
    {
        nsud.set(nil, forKey: "webLeadFilterData")
        nsud.synchronize()
        goToDasboard()
    }
    
    @IBAction func actionOnAppointment(_ sender: Any)
    {
        goToAppointment()
    }
    
    // MARK: -  ------------------------------ API Calling  ------------------------------
    func getPostData() -> [String: Any]{
        
        
        let param = [
            "statusIds": [],
            "CompanyKey": strCompanyKey,
            "EmployeeId": strEmpID,
            "startDate": strFromDate,
            "endDate": strToDate] as [String: Any]
        
        //
        //                let param = [
        //                    "statusIds": [],
        //                    "CompanyKey": "production",
        //                    "EmployeeId": "3",
        //                    "startDate": "2020-11-10",
        //                    "endDate": "2021-11-30",
        //                ] as [String: Any]
        
        
        return param
    }
    
    
    func callWebServiceToGetLeadData()
    {
        let loaderLocal = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loaderLocal, animated: true, completion: nil)
        
        var strURL = strServiceUrlMain +  URL.LeadNowFlow.getleadNowData
        
        strURL = strURL.trimmingCharacters(in: .whitespaces)
        
        getFilterDataWebLead()
        
        WebService.postDataWithHeadersToServer(dictJson: getPostData() as NSDictionary, url: strURL, strType: "dict"){ (response, status) in
            
            loaderLocal.dismiss(animated: false) {
                if(status == true)
                {
                    LeadNowParser.parseLeadNowAPIJSON(response: response, completionHandler: { (objLeadNow) in
                        self.arrOfLeadNow = objLeadNow
                        
                        if(nsud.value(forKey: "webLeadFilterData") != nil) {
                            let dictFilterData = nsud.value(forKey: "webLeadFilterData") as! NSDictionary
                            if(dictFilterData.count != 0){
                                let arySelectedPropertyType = (dictFilterData.value(forKey: "selectedPropertyTypeFromFilter") as! [[String: Any]])
                                let arySelectedUrgencyType = (dictFilterData.value(forKey: "selectedUrgencyTypeFromFilter") as! [[String: Any]])
                                
                                
                                NotificationCenter.default.post(name: Notification.Name("MoveToLeadMainVC"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "PropertyType": arySelectedPropertyType, "UrgencyType": arySelectedUrgencyType ])

                            }
                        }
                        else{
                            NotificationCenter.default.post(name: Notification.Name("MoveToLeadMainVC"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "PropertyType": [], "UrgencyType": []])
                        }
                        self.updateView()
                    })
                }
                else{
                    NotificationCenter.default.post(name: Notification.Name("MoveToLeadMainVC"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "PropertyType": [], "UrgencyType": []])
                }
            }
        }
    }
    
//    func getAllWebLeads()
//    {
//
//        let startGlobal = CFAbsoluteTimeGetCurrent()
//
//        self.arrAllWebLeads = NSMutableArray()
//
//        if !isInternetAvailable()
//        {
//            if self.arrOfStatus.count > 0
//            {
//                let strStatus = "\(self.arrOfStatus.object(at: swipeCount))"
//                self.fetchWebLeadFromCoreData(strStatus: "\(strStatus)")
//            }
//            else
//            {
//                self.fetchWebLeadFromCoreData(strStatus: "New")
//
//            }
//        }
//        else
//        {
////            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
////            self.present(loader, animated: false, completion: nil)
//             //self.callApiToGetWebLead()
//
//        }
//
//    }
    
//    func callApiToGetWebLead()
//    {
//       // strFromDate = "12/12/2018"
//       // strToDate = "12/12/2019"
//        deleteAllRecordsFromDB(strEntity: "TotalWebLeads", predicate: NSPredicate(format: "userName=%@", strUserName))
//
//        let startGlobal = CFAbsoluteTimeGetCurrent()
//
//        self.arrAllWebLeads = NSMutableArray()
//        getFilterDataWebLead()
//
//        var strUrl = "\(strServiceUrlMain)\(UrlGetAllWebLead)\(strCompanyKey)&EmployeeNo=\(strEmpNumber)&startDate=\(strFromDate)&endDate=\(strToDate)"  as String
//
//        strUrl = strUrl.trimmingCharacters(in: .whitespaces)
//
//        //FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
//
//        WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
//
//
//            self.loader.dismiss(animated: false)
//            {
//                DispatchQueue.main.async{
//
//                    //FTIndicator.dismissProgress()
//                    //self.refresher.endRefreshing()
//
//                }
//                if(Status)
//                {
//
//                    //let dictAddress = Response["data"] as! NSDictionary
//
//
//                    //let arrData = Response["data"] as! NSArray
//
//                    //let arrData = Response["data"] as! NSArray
//
//                    if  Response["data"] is NSArray
//                                        {
//                                            let arrData = Response["data"] as! NSArray
//                       /* if arrData.count > 0
//                        {
//
//                        }
//                        else
//                        {
//                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
//
//                        }*/
//
//                        let arrAllData = NSMutableArray()
//
//                        for item in arrData
//                        {
//                            let  dict = item as! NSDictionary
//
//                            let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))
//
//                            arrAllData.add(dictNew as Any)
//
//
//                        }
//                        //print(arrAllData)
//
//                        //self.arrAllWebLeads = arrAllData
//
//                        let str = getJson(from: arrAllData)
//                        //print("\(str ?? "")" as Any)
//
//                        self.saveWebLeadToCoreData(arrAllData: arrAllData)
//
//                       /* if self.arrOfStatus.count > 0
//                        {
//                            let strStatus = "\(self.arrOfStatus.object(at: self.swipeCount))"
//                            self.fetchWebLeadFromCoreData(strStatus: "\(strStatus)")
//                        }
//                        else
//                        {
//                            self.fetchWebLeadFromCoreData(strStatus: "New")
//
//                        }*/
//
//
//                        let diffGlobal = CFAbsoluteTimeGetCurrent() - startGlobal
//                        print("Took \(diffGlobal) seconds to get Data OverAll To Render From SQL Server. Count of Lead :-- \(arrAllData.count)")
//
//                        //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(arrAllData)", viewcontrol: self)
//
//                        // Nilind
//                        self.addButton()
//                        if self.arrOfStatus.count > 0
//                        {
//                            let strStatus = "\(self.arrOfStatus.object(at: self.swipeCount))"
//                            self.fetchWebLeadFromCoreData(strStatus: "\(strStatus)")
//                        }
//                        else
//                        {
//                            self.fetchWebLeadFromCoreData(strStatus: "New")
//
//                        }
//
//
//                    }
//                    else
//                    {
//
//                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
//
//                    }
//
//                }
//                else
//                {
//
//                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
//
//                }
//            }
//
//        }
//
//    }
    
//
//    func getAllLeadsOld()
//    {
//
//        if !isInternetAvailable()
//
//        {
//            Global().displayAlertController(alertMessage, "\(ErrorInternetMsg)", self)
//        }
//        else
//        {
//
//
//            // /api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoForPastDateExt/?companyKey=titan&employeeNo=E4307&&fromDate=05/24/2019&toDate=05/25/2019
//
//            //var strUrl =  ""
//
//            //let strUrlMain = ""
//
//            let strUrl = "https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/GetAllLeadsForEmployeeAsync?lastModifiedDate=&startDate=11%2F11%2F2017&endDate=12%2F12%2F2019"
//
//            FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
//
//            WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
//
//                DispatchQueue.main.async{
//
//                    FTIndicator.dismissProgress()
//                    //self.refresher.endRefreshing()
//
//                }
//                if(Status){
//
//                    //let dictAddress = Response["data"] as! NSDictionary
//
//                    let arrData = Response["data"] as! NSArray
//
//                    //let arrData = Response["data"] as! NSArray
//
//                    if arrData.isKind(of: NSArray.self)
//                    {
//                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(arrData)", viewcontrol: self)
//
//                    }else{
//
//                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
//
//                    }
//
//                }
//                else
//                {
//
//                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
//
//                }
//
//            }
//
//        }
//
//    }
    // MARK: -  ------------------------------ Function  ------------------------------
    
//    func createBadgeView() {
//        if(DeviceType.IS_IPAD){
//            lbltaskCount.isHidden = true
//            lblScheduleCount.isHidden = true
//        }
//        if(nsud.value(forKey: "DashBoardPerformance") != nil){
//
//            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
//
//                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
//
//                if(dictData.count != 0){
//
//                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
//                    if strScheduleCount == "0" {
//                        strScheduleCount = ""
//                    }
//                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
//                    if strTaskCount == "0" {
//                        strTaskCount = ""
//                    }
//
//                    if strScheduleCount.count > 0 {
//                        if(DeviceType.IS_IPAD){
//                            lblScheduleCount.text = strScheduleCount
//                            lblScheduleCount.isHidden = false
//                        }else{
//                            let badgeSchedule = SPBadge()
//                            badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
//                            badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
//                            badgeSchedule.badge = strScheduleCount
//                            btnScheduleFooter.addSubview(badgeSchedule)
//                        }
//                    }
//
//                    if strTaskCount.count > 0 {
//                        if(DeviceType.IS_IPAD){
//                            lbltaskCount.text = strTaskCount
//                            lbltaskCount.isHidden = false
//                        }else{
//                            let badgeTasks = SPBadge()
//                            badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
//                            badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
//                            badgeTasks.badge = strTaskCount
//                            btnTasksFooter.addSubview(badgeTasks)
//                        }
//                    }
//                }
//            }
//
//        }
//
//    }
    
//    func getStatus()
//    {
//        arrOfStatus = NSMutableArray()
//        arrOfStatusName = NSMutableArray()
//
//        if nsud.value(forKey: "WebLeadStatusMasters") is NSArray
//        {
//            var arrStatusTemp = NSArray()
//            arrStatusTemp = nsud.value(forKey: "WebLeadStatusMasters") as! NSArray
//            for item in arrStatusTemp
//            {
//                let dict = item as! NSDictionary
//
//                if ("\(dict.value(forKey: "SysName") ?? "")").caseInsensitiveCompare("void") == .orderedSame
//                {
//
//                }
//                else
//                {
//                    arrOfStatus.add("\(dict.value(forKey: "SysName")!)")
//                    arrOfStatusName.add("\(dict.value(forKey: "Name")!)")
//
//                }
//
//            }
//        }
//
//    }
//
    
    
    func goToOpportunityVC()
    {
        /*
         
         let storyboardIpad = UIStoryboard.init(name: "CRM_NewiPhone", bundle: nil)
         let testController = storyboardIpad.instantiateViewController(withIdentifier: "LeadVC") as? LeadVC
         self.navigationController?.pushViewController(testController!, animated: false)
         */
        
        /* let storyboardIpad = UIStoryboard.init(name: "CRM_NewiPhone", bundle: nil)
         let testController = storyboardIpad.instantiateViewController(withIdentifier: "OpportunityVC") as? OpportunityVC*/
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
        
        /*  UIView.animate(withDuration: 0.75, animations: {() -> Void in
         UIView.setAnimationCurve(.easeInOut)
         self.navigationController?.pushViewController(testController!, animated: false)
         UIView.setAnimationTransition(.flipFromLeft, for: (self.navigationController?.view)!, cache: false)
         })*/
        self.navigationController?.pushViewController(controller, animated: false)
    }

    
    
    func addButton()
    {
        if swipeCount == 1 && arrOfStatusName.count == 1
        {
            swipeCount = 0
        }
        if "\(nsud.value(forKey: "cureentTab") ?? "")".count > 0
        {
            let strCount = "\(nsud.value(forKey: "cureentSwipeCount") ?? "")"
            swipeCount = Int(strCount)!
        }
        var width = CGFloat()
        for btn  in scrollViewLeads.subviews
        {
            btn.removeFromSuperview()
        }
        var xAxis = 0
        for i in 0 ..< arrOfStatus.count
        {
            let btn = UIButton()
            let lbl1 = UILabel()
            let lbl2 = UILabel()
            lbl1.textAlignment = NSTextAlignment.center
            lbl2.textAlignment = NSTextAlignment.center
            lbl1.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
            lbl2.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 16 : 12)
            lbl1.textColor = UIColor.white
            lbl2.textColor = UIColor.white
            
            btn.frame = CGRect(x: xAxis, y: 1, width: DeviceType.IS_IPAD ? 170 : 120, height: Int(scrollViewLeads.frame.height) - 2)
            lbl1.frame = CGRect(x: xAxis, y: 4, width: Int(btn.frame.width), height: Int(scrollViewLeads.frame.height/2) - 8)
            lbl2.frame =  CGRect(x: xAxis, y: Int(lbl1.frame.maxY) + 2, width: Int(btn.frame.width), height: Int(scrollViewLeads.frame.height/2) - 4)
           
            xAxis = xAxis + Int(btn.frame.width + 5)
            
            lbl1.text = ""
            lbl2.text = ""
           
            if arrOfStatus.count > 0
            {
                lbl1.text = "\(arrOfStatusName.object(at: i))".uppercased()
                if lbl1.text?.caseInsensitiveCompare("GRABBED") == .orderedSame
                {
                    lbl1.text = "MY LEADS" //MY LEADS
                }
            }
            
           /* let strStatus = "\(self.arrOfStatus.object(at: i))"
            self.fetchWebLeadFromCoreData(strStatus: "\(strStatus)")
            lbl2.text = String(format: "\(arrAllWebLeads.count)")*/
            
            let strStatus = "\(self.arrOfStatus.object(at: i))"
         //   self.fetchWebLeadFromCoreDataForButton(strStatus: "\(strStatus)")
         //   lbl2.text = String(format: "\(arrAllWebLeadsCount.count)")

            
     
            scrollViewLeads.addSubview(lbl1)
            scrollViewLeads.addSubview(lbl2)
           
            
            if i == swipeCount
            {
                btn.backgroundColor = UIColor(red: 208.0/255, green: 191.0/255, blue: 35.0/255, alpha: 1)
               // let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
               // let b = ButtonWithShadow()
            }
            else
            {
                btn.backgroundColor = UIColor.clear
            }
            btn.addTarget(self, action: #selector(action_ButtonClicked), for: .touchUpInside)
            btn.setTitle("\(i)", for: .normal)
            btn.setTitleColor(UIColor.clear, for: .normal)
            width = width + btn.frame.width
            scrollViewLeads.addSubview(btn)
            scrollViewLeads.bringSubviewToFront(lbl1)
            scrollViewLeads.bringSubviewToFront(lbl2)
        }
       
        
    }
    
    @objc func action_ButtonClicked(sender:UIButton)
    {
        let value = Int("\(sender.titleLabel?.text ?? "")")
        print("Button Clicked \(value ?? 0)")
        swipeCount = value ?? 0
        if swipeCount == 0
        {
            scrollViewLeads.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        else
        {
            scrollViewLeads.setContentOffset(CGPoint(x: sender.frame.maxX - sender.frame.size.width*2, y: 0), animated: true)
        }
        print("Swipe count \(swipeCount)")
         self.addButton()
        let strStatus = "\(arrOfStatus.object(at: swipeCount))"
     //   fetchWebLeadFromCoreData(strStatus: strStatus)
        //addBorder()
    }
   
    func defaultSwipeCount()
    {
        
        if arrOfStatusName.count > 0
        {
            
            for k in 0 ..< arrOfStatusName.count {
             
                let strMyLeadStatusName = "\(arrOfStatusName[k])"
                
                if strMyLeadStatusName.caseInsensitiveCompare("GRABBED") == .orderedSame
                {
                    swipeCount = k
                    break
                }
                
            }
            
        }
        
        
    
    }
    func defaultSwipeCountFilter()
    {
        
        if arrOfStatusName.count > 1
        {
            
            for k in 0 ..< arrOfStatusName.count {
             
                let strMyLeadStatusName = "\(arrOfStatusName[k])"
                
                if strMyLeadStatusName.caseInsensitiveCompare("GRABBED") == .orderedSame
                {
                    swipeCount = k
                    break
                }
                
            }
            
        }
        
    }

    func swipeLeftTemp()
    {
        swipeCount = swipeCount + 1
        
        if (swipeCount >= arrOfStatus.count)
        {
            swipeCount = arrOfStatus.count - 1
        }
        print("Swipe count \(swipeCount)")
         self.addButton()
        
        let strStatus = "\(arrOfStatus.object(at: swipeCount))"
      //  fetchWebLeadFromCoreData(strStatus: strStatus)
        
    }
  
    
    func goToFilter()
    {
        let mainStoryboard = UIStoryboard(
                   name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
                   bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "FilterLeadVC_CRMNew_iPhone") as! FilterLeadVC_CRMNew_iPhone
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToAddLead()
    {
        /*let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "AddCustomerView") as! AddCustomerView
        vc.strFromWhere = "WebLeadVC"
        self.navigationController?.pushViewController(vc, animated: false)*/
       let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToAddTask()
    {
          let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
        
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToNearBy()
    {
   /*if(DeviceType.IS_IPAD){
                              let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                              let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
                              self.navigationController?.pushViewController(vc!, animated: false)
                          }else{
                              let storyboard = UIStoryboard(name: "Main", bundle: nil)
                              let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
                              self.navigationController?.pushViewController(vc!, animated: false)
                          }*/
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)

    }
    func goToContactList()
    {
       
        
       let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)

    }
    func getStartDateEndDate()
    {
        strFromDate = ""
        strToDate = ""
        let strNoOfDays = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.DisplayWebLeadDays") ?? "")"
        let strDisplayWebLeadFor = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.DisplayWebLeadFor") ?? "")"
        
        
        if strDisplayWebLeadFor == "currentday"
        {
            strFromDate = Global().getCurrentDate()
            strToDate = Global().getCurrentDate()
        }
        else if strDisplayWebLeadFor == "currentmonth"
        {
            
            let objDelegate = StasticsClass()
            objDelegate.delegate = self as StasticsClassDelegate
            objDelegate.getStart_EndDate("This Month")
            
        }
        else if strNoOfDays == "" || strNoOfDays == "0"
        {
            strFromDate = Global().getCurrentDate()
            strToDate = Global().getCurrentDate()
        }
        else
        {
            strFromDate = Global().getStartDate(Global().getCurrentDate(), Int(strNoOfDays)!)
            strToDate = Global().getEndDate(Global().getCurrentDate(), Int(strNoOfDays)!)
            
        }
        
        
    }
    func goToDasboard()
    {
     
       let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func goToAppointment()
    {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
//        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboardIpad.instantiateViewController(withIdentifier: "AppointmentView") as! AppointmentView
//        self.navigationController?.pushViewController(vc, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
        
    }
    func goToSignedViewAgreement()
    {
        //nsud.set(true, forKey: "fromCompanyVC")
       // nsud.synchronize()
        let storyboardIpad = UIStoryboard.init(name: "CRM", bundle: nil)
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as! SignedAgreementiPhoneVC
        //self.navigationController?.pushViewController(vc, animated: false)
        self.navigationController?.present(vc, animated: false, completion: nil)
        
    }
//    func moveToTop()
//    {
//        if arrAllWebLeads.count > 0
//        {
//            /*var path = NSIndexPath()
//            path = NSIndexPath(row: arrAllWebLeads.count-1 , section: 0)
//            tblViewLeads.scrollToRow(at: path as IndexPath, at: UITableView.ScrollPosition.top, animated: false)*/
//            tblViewLeads.setContentOffset(CGPoint.zero, animated:true)
//
//        }
//    }
    
    func setUpView()
    {
        
        //lblNoDataFound.isHidden = true
        viewTopButton.backgroundColor = UIColor.white
        
        btnWebLead.layer.cornerRadius = btnWebLead.frame.size.height/2
        btnWebLead.layer.borderWidth = 0
        
        viewTopButton.layer.cornerRadius = viewTopButton.frame.size.height/2
        viewTopButton.layer.borderWidth = 0
        
        btnOpportunity.layer.cornerRadius = btnOpportunity.frame.size.height/2
        btnOpportunity.layer.borderWidth = 0
        
        btnLeadNew.layer.cornerRadius = btnLeadNew.frame.size.height/2
        btnLeadNew.layer.borderWidth = 0
        
        viewTopButton.bringSubviewToFront(btnWebLead)
        
        if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .none//.roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15//txfSearchField.frame.size.height/2
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
            
        }
        searchBar.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        //
        searchBar.layer.borderWidth = 1
        searchBar.layer.opacity = 1.0
        
        //        searchBarCompany.layer.backgroundColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        
       // searchBar.layer.cornerRadius = searchBar.frame.size.height/2
        //searchBar.layer.borderWidth = 0
        
        
        
    }
    func getFilterDataWebLead()
    {
        //nsud.set(dictFilterData, forKey: "webLeadFilterData")

        var dictData = NSDictionary()
        
        if nsud.value(forKey: "webLeadFilterData") is NSDictionary
        {
            dictData = nsud.value(forKey: "webLeadFilterData") as! NSDictionary
            
            var strUserType = String()
            var strDayType = String()
            
            if (dictData.count > 0)
            {
                strUserType = "\(dictData.value(forKeyPath: "userTypeFromFilter") ?? "")"
                if (dictData.value(forKeyPath: "selectedUserFromFilter")) is NSArray
                {
                    let arr1 = dictData.value(forKeyPath: "selectedUserFromFilter") as! NSArray
                    
                    let arrUser = arr1.mutableCopy() as! NSMutableArray//dictData.value(forKeyPath: "selectedUserFromFilter") as! NSMutableArray
                    
                    if arrUser.count > 0
                    {
                        strUserType = "\(arrUser.object(at: 0))"
                    }
                    else
                    {
                        strUserType = ""
                    }
                }
                else
                {
                    strUserType = ""
                }
                
                if dictData.value(forKeyPath: "selectedDayFromFilter") is NSArray
                {
                    let arr1 = dictData.value(forKeyPath: "selectedDayFromFilter") as! NSArray
                    
                    let arrDayType = arr1.mutableCopy() as! NSMutableArray//dictData.value(forKeyPath: "selectedDayFromFilter") as! NSMutableArray
                    
                    if arrDayType.count > 0
                    {
                        strDayType = "\(arrDayType.object(at: 0))"
                    }
                    else
                    {
                        strDayType = ""
                    }
                }
                else
                {
                    strDayType = ""
                }
                
                if strDayType.count > 0
                {
                    let objDelegate = StasticsClass()
                    objDelegate.delegate = self as StasticsClassDelegate
                    objDelegate.getStart_EndDate("\(strDayType)")
                }
                else
                {
                    strFromDate = "\(dictData.value(forKeyPath: "fromDateFromFilter") ?? "")"
                    strToDate = "\(dictData.value(forKeyPath: "toDateFromFilter") ?? "")"
                }
                if strFromDate.count == 0  || strToDate.count == 0
                {
                    strFromDate = Global().getCurrentDate()
                    strToDate = Global().getCurrentDate()
                }
                
                if dictData.value(forKey: "statusFromFilter") is NSArray
                {
                    arrOfStatus = NSMutableArray()
                    arrOfStatusName = NSMutableArray()
                    
                    let arr1 = dictData.value(forKey: "statusFromFilter") as! NSArray
                    arrOfStatus = arr1.mutableCopy() as! NSMutableArray
                    
                    let arr2 = dictData.value(forKey: "statusFromFilter") as! NSArray
                    arrOfStatusName = arr2.mutableCopy() as! NSMutableArray
                    
                    //arrOfStatus = dictData.value(forKey: "statusFromFilter") as! NSMutableArray
                    //arrOfStatusName = dictData.value(forKey: "statusFromFilter") as! NSMutableArray
                    //addBorder()
                }
                else
                {
                    getStartDateEndDate()
                    //getStatus()
                    self.addButton()
                }
                
                
            }
        }
    }
    
    

    // MARK: -  ------------------------------ Core Data Function  ------------------------------
    
//    func saveWebLeadToCoreData(arrAllData: NSMutableArray)
//    {
//        deleteAllRecordsFromDB(strEntity: "TotalWebLeads", predicate: NSPredicate(format: "userName=%@", strUserName))
//
//        for item in arrAllData
//        {
//            let dict = item as! NSDictionary
//
//            let arrOfKeys = NSMutableArray()
//            let arrOfValues = NSMutableArray()
//
//            arrOfKeys.add("address1")
//            arrOfKeys.add("address2")
//            arrOfKeys.add("assignedToId")
//            arrOfKeys.add("assignedToName")
//            arrOfKeys.add("assignedType")
//            arrOfKeys.add("branchesSysNames")
//            arrOfKeys.add("cellPhone1")
//            arrOfKeys.add("cityName")
//            arrOfKeys.add("companyKey")
//            arrOfKeys.add("companyName")
//            arrOfKeys.add("countryId")
//            arrOfKeys.add("countyName")
//            arrOfKeys.add("createdBy")
//            arrOfKeys.add("createdDate")
//            arrOfKeys.add("crmContactId")
//            arrOfKeys.add("firstName")
//            arrOfKeys.add("flowType")
//            arrOfKeys.add("industryId")
//            arrOfKeys.add("lastName")
//            arrOfKeys.add("latitude")
//            arrOfKeys.add("leadId")
//            arrOfKeys.add("leadName")
//            arrOfKeys.add("leadNumber")
//            arrOfKeys.add("longitude")
//            arrOfKeys.add("middleName")
//            arrOfKeys.add("modifiedBy")
//            arrOfKeys.add("modifiedDate")
//            arrOfKeys.add("primaryEmail")
//            arrOfKeys.add("primaryPhone")
//            arrOfKeys.add("primaryPhoneExt")
//            arrOfKeys.add("schoolDistrict")
//            arrOfKeys.add("secondaryEmail")
//            arrOfKeys.add("secondaryPhone")
//            arrOfKeys.add("secondaryPhoneExt")
//            arrOfKeys.add("stateId")
//            arrOfKeys.add("statusDate")
//            arrOfKeys.add("statusName")
//            arrOfKeys.add("webLeadSourceId")
//            arrOfKeys.add("zipcode")
//            arrOfKeys.add("userName")
//            arrOfKeys.add("crmCompany")
//
//            arrOfKeys.add("addressPropertyName")
//            arrOfKeys.add("addressPropertyTypeId")
//
//            arrOfKeys.add("urgencyName")
//            arrOfKeys.add("urgencyId")
//
//
//            arrOfValues.add("\(dict.value(forKey: "Address1") ?? "")")//address1
//            arrOfValues.add("\(dict.value(forKey: "Address2") ?? "")")//address2
//            arrOfValues.add("\(dict.value(forKey: "AssignedToId") ?? "")")//assignedToId
//            arrOfValues.add("\(dict.value(forKey: "AssignedToName") ?? "")")//assignedToName
//            arrOfValues.add("\(dict.value(forKey: "AssignedType") ?? "")")//assignedType
//
//
//            if (dict.value(forKey: "BranchesSysNames") ?? "") is NSArray
//            {
//                let arrBranch = (dict.value(forKey: "BranchesSysNames") ?? "") as! NSArray
//                //arrOfValues.add("\(dict.value(forKey: "BranchesSysNames") ?? "")")//branchesSysNames
//                arrOfValues.add(arrBranch)//branchesSysNames
//            }
//            else
//            {
//                arrOfValues.add("")
//            }
//
//            arrOfValues.add("\(dict.value(forKey: "CellPhone1") ?? "")")//cellPhone1
//            arrOfValues.add("\(dict.value(forKey: "CityName") ?? "")")//cityName
//            arrOfValues.add("\(dict.value(forKey: "CompanyKey") ?? "")")//companyKey
//            arrOfValues.add("\(dict.value(forKey: "CompanyName") ?? "")")//companyName
//            arrOfValues.add("\(dict.value(forKey: "CountryId") ?? "")")//countryId
//            arrOfValues.add("\(dict.value(forKey: "CountyName") ?? "")")//countyName
//            arrOfValues.add("\(dict.value(forKey: "CreatedBy") ?? "")")//createdBy
//            arrOfValues.add("\(dict.value(forKey: "CreatedDate") ?? "")")//createdDate
//
//
//            arrOfValues.add("\(dict.value(forKey: "CrmContactId") ?? "")")//crmContactId
//            arrOfValues.add("\(dict.value(forKey: "FirstName") ?? "")")//firstName
//            arrOfValues.add("\(dict.value(forKey: "FlowType") ?? "")")//flowType
//            arrOfValues.add("\(dict.value(forKey: "IndustryId") ?? "")")//industryId
//            arrOfValues.add("\(dict.value(forKey: "LastName") ?? "")")//lastName
//            arrOfValues.add("\(dict.value(forKey: "Latitude") ?? "")")//latitude
//            arrOfValues.add("\(dict.value(forKey: "LeadId") ?? "")")//leadId
//            arrOfValues.add("\(dict.value(forKey: "LeadName") ?? "")")//leadName
//
//            arrOfValues.add("\(dict.value(forKey: "LeadNumber") ?? "")")//leadNumber
//            arrOfValues.add("\(dict.value(forKey: "Longitude") ?? "")")//longitude
//            arrOfValues.add("\(dict.value(forKey: "MiddleName") ?? "")")//middleName
//
//
//            arrOfValues.add("\(dict.value(forKey: "ModifiedBy") ?? "")")//modifiedBy
//            arrOfValues.add("\(dict.value(forKey: "ModifiedDate") ?? "")")//modifiedDate
//            arrOfValues.add("\(dict.value(forKey: "PrimaryEmail") ?? "")")//primaryEmail
//            arrOfValues.add("\(dict.value(forKey: "PrimaryPhone") ?? "")")//primaryPhone
//            arrOfValues.add("\(dict.value(forKey: "PrimaryPhoneExt") ?? "")")//primaryPhoneExt
//            arrOfValues.add("\(dict.value(forKey: "SchoolDistrict") ?? "")")//schoolDistrict
//            arrOfValues.add("\(dict.value(forKey: "SecondaryEmail") ?? "")")//secondaryEmail
//            arrOfValues.add("\(dict.value(forKey: "SecondaryPhone") ?? "")")//secondaryPhone
//            arrOfValues.add("\(dict.value(forKey: "SecondaryPhoneExt") ?? "")")//secondaryPhoneExt
//            arrOfValues.add("\(dict.value(forKey: "StateId") ?? "")")//stateId
//            arrOfValues.add("\(dict.value(forKey: "StatusDate") ?? "")")//statusDate
//            arrOfValues.add("\(dict.value(forKey: "StatusName") ?? "")")//statusName
//
//            // arrOfValues.add("\(dict.value(forKey: "WebLeadSourceId") ?? "")")//webLeadSourceId
//
//            if (dict.value(forKey: "WebLeadSourceId") ?? "") is NSArray
//            {
//                let arrWebLeadSourceId = (dict.value(forKey: "WebLeadSourceId") ?? "") as! NSArray
//                arrOfValues.add(arrWebLeadSourceId)//WebLeadSourceId
//            }
//            else
//            {
//                arrOfValues.add("")
//            }
//
//
//            arrOfValues.add("\(dict.value(forKey: "Zipcode") ?? "")")//zipcode
//
//            arrOfValues.add("\(strUserName)")//zipcode
//
//
//            if (dict.value(forKey: "CrmCompany") ?? "") is NSDictionary
//            {
//                let dictCrmCompany = (dict.value(forKey: "CrmCompany") ?? "") as! NSDictionary
//                arrOfValues.add(dictCrmCompany)//branchesSysNames
//            }
//            else
//            {
//                arrOfValues.add("")
//            }
//
//
//            arrOfValues.add("\(dict.value(forKey: "AddressPropertyName") ?? "")")
//            arrOfValues.add("\(dict.value(forKey: "AddressPropertyTypeId") ?? "")")
//
//            arrOfValues.add("\(dict.value(forKey: "UrgencyName") ?? "")")
//            arrOfValues.add("\(dict.value(forKey: "UrgencyId") ?? "")")
//
//            saveDataInDB(strEntity: "TotalWebLeads", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
//
//
//            //Update Modify Date In Work Order DB
//            //updateServicePestModifyDate(strWoId: self.strWoId as String)
//        }
//
//
//    }
//    func fetchWebLeadFromCoreData(strStatus: String)
//    {
//
//        var strStatusNew = String()
//        strStatusNew = strStatus
//
//        if "\(nsud.value(forKey: "cureentTab") ?? "")".count > 0
//        {
//            strStatusNew = "\(nsud.value(forKey: "cureentTab")!)"
//
//            nsud.setValue("", forKey: "cureentTab")
//            nsud.setValue("", forKey: "cureentSwipeCount")
//            nsud.synchronize()
//
//        }
//        //ServiceAddressPOCDetailDcs
//        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalWebLeads", predicate: NSPredicate(format: "userName == %@ && statusName == %@", strUserName, strStatusNew))
//
//        //-----For Propertype filter ---
//        if(nsud.value(forKey: "webLeadFilterData") != nil) {
//            let dictFilterData = nsud.value(forKey: "webLeadFilterData") as! NSDictionary
//            if(dictFilterData.count != 0){
//                let arySelectedPropertyType = (dictFilterData.value(forKey: "selectedPropertyTypeFromFilter") as! NSArray)
//                let arySelectedUrgencyType = (dictFilterData.value(forKey: "selectedUrgencyTypeFromFilter") as! NSArray)
//
//
//
//
//                if(arySelectedPropertyType.count == 0 && arySelectedUrgencyType.count == 0){
//                    arrAllWebLeads = arryOfData.mutableCopy() as! NSMutableArray
//                    arrAllWebLeadsData = arryOfData.mutableCopy() as! NSMutableArray
//                }else{
//                    let arryFilterType = NSMutableArray()
//
//                    if(arySelectedPropertyType.count != 0){
//                        for item in arySelectedPropertyType {
//                            let id = "\((item as AnyObject).value(forKey: "AddressPropertyTypeId")!)"
//                            let aryTemp = arryOfData.filter { (task) -> Bool in
//                                return "\((task as! NSManagedObject).value(forKey: "addressPropertyTypeId")!)".lowercased().contains(id) }
//                            if aryTemp.count != 0 {
//                                arryFilterType.add(aryTemp[0])
//                            }
//                        }
//
//                    }
//
//                     if(arySelectedUrgencyType.count != 0){
//                        for item in arySelectedUrgencyType {
//                            let id = "\((item as AnyObject).value(forKey: "UrgencyId")!)"
//                            let aryTemp = arryOfData.filter { (task) -> Bool in
//                                return "\((task as! NSManagedObject).value(forKey: "urgencyId")!)".lowercased().contains(id) }
//                            if aryTemp.count != 0 {
//                                arryFilterType.add(aryTemp[0])
//                            }
//                        }
//
//                    }
//
//                                        arrAllWebLeads = arryFilterType.mutableCopy() as! NSMutableArray
//                                        arrAllWebLeadsData = arryFilterType.mutableCopy() as! NSMutableArray
//                }
//            }else{
//                arrAllWebLeads = arryOfData.mutableCopy() as! NSMutableArray
//                arrAllWebLeadsData = arryOfData.mutableCopy() as! NSMutableArray
//            }
//        }else{
//            arrAllWebLeads = arryOfData.mutableCopy() as! NSMutableArray
//            arrAllWebLeadsData = arryOfData.mutableCopy() as! NSMutableArray
//        }
//
//
//        tblViewLeads.reloadData()
//        moveToTop()
//        /*var totalLeadAmount = Float()
//         totalLeadAmount = 0.0
//
//         for item in arrAllOpportunityLeads
//         {
//         let dict = item as! NSManagedObject
//         var value = Float()
//         value = 0.0
//         let myString = "\(dict.value(forKey: "opportunityValue") ?? "")"
//         let myFloat = (myString as NSString).floatValue
//         value = value + myFloat
//         totalLeadAmount = totalLeadAmount + value
//         }*/
//
//       // lblLeadDetail.text = String(format: " $ %.02f | \(arrAllWebLeads.count) Leads")
//
//        leadCount = String(format: "\(arrAllWebLeads.count) Leads")
//
//
//        /*if arryOfData.count > 0
//         {
//
//         let  objServiceAddressContactDetails = arryOfData[0] as! NSManagedObject
//
//         }
//         else
//         {
//
//         }*/
//        //---Navin-
//        //Search bar clear when click on other tab ---
//        self.view.endEditing(true)
//
//        self.searchAutocomplete(searchText: "")
//        let isForAssociation = nsud.bool(forKey: "fromWebLeadAssociation")
//        self.strSearchText = ""
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
//            self.setTopMenuOption(tagForAssociation: isForAssociation)
//        })
//        //----End
//    }
//    func fetchWebLeadFromCoreDataNew(strStatus: String)
//    {
//
//        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalWebLeads", predicate: NSPredicate(format: "userName == %@ && statusName == %@", strUserName, strStatus))
//
//        arrAllWebLeads = arryOfData.mutableCopy() as! NSMutableArray
//
//        arrAllWebLeadsData = arryOfData.mutableCopy() as! NSMutableArray
//
//
//        tblViewLeads.reloadData()
//        moveToTop()
//
//
//        leadCount = String(format: "\(arrAllWebLeads.count) Leads")
//
//
//
//
//    }
//    func fetchWebLeadFromCoreDataForButton(strStatus: String)
//    {
//
//        //ServiceAddressPOCDetailDcs
//        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalWebLeads", predicate: NSPredicate(format: "userName == %@ && statusName == %@", strUserName, strStatus))
//
//        arrAllWebLeadsCount = arryOfData.mutableCopy() as! NSMutableArray
//
//
//    }
//    func fetchWebLeadFromCoreDataDateWise(strStatus: String)
//    {
//        //ServiceAddressPOCDetailDcs
//        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalWebLeads", predicate: NSPredicate(format: "userName == %@ && statusName == %@", strUserName, strStatus))
//        let arrFinalSorted = NSMutableArray()
//
//        for item in arryOfData
//        {
//            let dict = item as! NSManagedObject
//
//            let strCreatedDate = changeStringDateToGivenFormat(strDate: "\(dict.value(forKey: "createdDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
//
//            let dateFormatter = DateFormatter()
//            dateFormatter.timeZone = NSTimeZone.local
//            dateFormatter.dateFormat = "MM/dd/yyyy"
//            var dateCreated = Date()
//            var datefromDate = Date()
//            var dateEndDate = Date()
//            dateCreated = dateFormatter.date(from: strCreatedDate) ?? Date()
//            datefromDate = dateFormatter.date(from: strFromDate) ?? Date()
//            dateEndDate = dateFormatter.date(from: strToDate) ?? Date()
//
//            if ((datefromDate <= dateCreated) && ( dateCreated <= dateEndDate))
//            {
//                arrFinalSorted.add(dict)
//            }
//
//        }
//
//        arrAllWebLeads = arrFinalSorted.mutableCopy() as! NSMutableArray
//        arrAllWebLeadsData = arrFinalSorted.mutableCopy() as! NSMutableArray
//
//
//        tblViewLeads.reloadData()
//        moveToTop()
//
//
//
//
//    }
    
}
// MARK: --------------------- Tableview Delegate --------------

//extension WebLeadVC : UITableViewDelegate, UITableViewDataSource
//{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        return arrAllWebLeads.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//
//        let cell = tblViewLeads.dequeueReusableCell(withIdentifier: "LeadVCCell", for: indexPath as IndexPath) as! LeadVCCell
//
//        let matches = arrAllWebLeads.object(at: indexPath.row) as! NSManagedObject
//
//
//
//        cell.lblNewOppNo.text = "\(matches.value(forKey: "leadName") ?? "")"
//
//
//        var strName = String()
//
//        let strFirstName = "\(matches.value(forKey: "firstName") ?? "")"
//        let strMiddleName = "\(matches.value(forKey: "middleName") ?? "")"
//        let strLastName = "\(matches.value(forKey: "lastName") ?? "")"
//
//        strName = ""
//        if strFirstName.count > 0
//        {
//            strName = strFirstName
//        }
//        if strMiddleName.count > 0
//        {
//            strName = "\(strName) " + "\(strMiddleName)"
//        }
//        if strLastName.count > 0
//        {
//            strName = "\(strName) " + "\(strLastName)"
//        }
//
//        let rangeTime =  getTotalTimeAfterCreated(strdate: "\(matches.value(forKey: "CreatedDate") ?? "")")
//        var strCompanyName = ""
//
//        if matches.value(forKey: "crmCompany") is NSDictionary {
//
//            let dictDataLocal = matches.value(forKey: "crmCompany") as! NSDictionary
//            strCompanyName = "\(dictDataLocal.value(forKey: "Name") ?? "")"
//
//        }
//
//        var strFinal = ""
//        if(rangeTime.count != 0){
//            strFinal =  rangeTime
//        }
//        if(strCompanyName.count != 0){
//            strFinal =  strFinal + "  \u{2022}  \(strCompanyName)"
//        }
//        if(strName.count != 0){
//            strFinal =  strFinal + "  \u{2022}  \(strName)"
//        }
//        cell.lblNewCustomerName.text = strFinal
//        cell.lblNewTechName.text = "\(matches.value(forKey: "assignedToName") ?? "")"
//
//        let arrayTemp = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND status == %@ AND refId == %@ AND refType == %@",strUserName, "Open", "\(matches.value(forKey: "leadId") ?? "")", "WebLead"))
//
//        if arrayTemp.count > 0
//        {
//            cell.lblNewStatusBar.backgroundColor = UIColor.red
//
//            cell.lblNewStatusBar.backgroundColor = UIColor(red: 207.0/255.0, green: 68.0/255.0, blue: 47.0/255.0, alpha: 1)
//           /* let arrayTaskCount = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND refId == %@ AND refType == %@",strUserName,"\(matches.value(forKey: "leadId") ?? "")", "WebLead"))
//
//            if arrayTaskCount.count == 0
//            {
//                cell.lblNewStatusBar.backgroundColor =  UIColor.green
//            }*/
//
//        }
//        else
//        {
//            cell.lblNewStatusBar.backgroundColor =  UIColor.green
//
//            cell.lblNewStatusBar.backgroundColor = UIColor(red: 62.0/255.0, green: 147.0/255.0, blue: 84.0/255.0, alpha: 1)
//
//        }
//        cell.selectionStyle = UITableViewCell.SelectionStyle.none
//
//
//        //let dict = aryTBL.object(at: indexPath.row)as? NSDictionary
//        return cell
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return UITableView.automaticDimension
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//
//        tableView.deselectRow(at: indexPath, animated: false)
//
//        let isForAssociation = nsud.bool(forKey: "fromWebLeadAssociation")
//
//        if isForAssociation {
//
//            nsud.set(false, forKey: "fromWebLeadAssociation")
//            nsud.synchronize()
//
//            let dictData = getMutableDictionaryFromNSManagedObject(obj: arrAllWebLeads.object(at: indexPath.row) as! NSManagedObject) as NSDictionary
//
//            if strTypeOfAssociations == "Activity" {
//
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedActivity_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
//
//            } else if strTypeOfAssociations == "Task"{
//
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedTask_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
//
//            } else if strTypeOfAssociations == "Company"{
//
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedCompany_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
//
//            } else if strTypeOfAssociations == "Contact"{
//
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedContact_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
//
//            } else if strTypeOfAssociations == "Account"{
//
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAccount_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
//
//            }
//                //-------Association Task Activity------
//            else if(strTypeOfAssociations == "AddActivityCRM_Associations"){
//                self.delegateActivityAssociate?.getDataAddActivityAssociateProtocol(notification: dictData, tag: selectionTag)
//            }
//            else if(strTypeOfAssociations == "AddTaskCRM_Associations"){
//                self.delegateTaskAssociate?.getDataAddTaskAssociateProtocol(notification: dictData, tag: selectionTag)
//            }
//
//            //---------------------------//
//
//            self.navigationController?.popViewController(animated: false)
//
//        } else {
//
//            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadDetailsVC_CRMNew_iPhone") as! WebLeadDetailsVC_CRMNew_iPhone
//
//             //vc.dictLeadData = arrAllWebLeads.object(at: indexPath.row) as! NSManagedObject
//
//
//             vc.dictLeadData = getMutableDictionaryFromNSManagedObject(obj: arrAllWebLeads.object(at: indexPath.row) as! NSManagedObject) as NSDictionary
//
//             vc.strReftype = "weblead"
//
//             let matches = arrAllWebLeads.object(at: indexPath.row) as! NSManagedObject
//             vc.strRefIdNew = "\(matches.value(forKey: "leadId") ?? "")"
//
//             self.navigationController?.pushViewController(vc, animated: false)
//
//            nsud.setValue(arrOfStatus.object(at: swipeCount), forKey: "cureentTab")
//            nsud.setValue(arrOfStatus.index(of: "\(arrOfStatus.object(at: swipeCount))"), forKey: "cureentSwipeCount")
//            nsud.synchronize()
//        }
//
//    }
//
//    /*func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        return UIView()
//    }*/
//}
// MARK: --------------------- Searchbar Delegate --------------

extension  WebLeadVC : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(searchText: txtAfterUpdate as String)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(searchText: "")
        searchBar.text = ""
     
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
        self.searchAutocomplete(searchText: (searchBar.text!)) // ""
    }
    func searchAutocomplete(searchText: String) -> Void
    {

        if !(searchText.count == 0)
        {
            
            let dictFilterData = nsud.value(forKey: "webLeadFilterData") as? NSDictionary
            if(dictFilterData?.count != 0){
                let arySelectedPropertyType = (dictFilterData?.value(forKey: "selectedPropertyTypeFromFilter") as? [[String: Any]] ?? [])
                let arySelectedUrgencyType = (dictFilterData?.value(forKey: "selectedUrgencyTypeFromFilter") as? [[String: Any]] ?? [])
                 
                NotificationCenter.default.post(name: Notification.Name("MoveToLeadMainVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "PropertyType": arySelectedPropertyType, "UrgencyType": arySelectedUrgencyType, "searchText": searchText])
                
                
            }
            
            else{
                NotificationCenter.default.post(name: Notification.Name("MoveToLeadMainVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "PropertyType": [], "UrgencyType": [], "searchText": searchText])
            }
            
        }
        else
        {
            
            let dictFilterData = nsud.value(forKey: "webLeadFilterData") as? NSDictionary
            if(dictFilterData?.count != 0){
                let arySelectedPropertyType = (dictFilterData?.value(forKey: "selectedPropertyTypeFromFilter") as? [[String: Any]] ?? [])
                let arySelectedUrgencyType = (dictFilterData?.value(forKey: "selectedUrgencyTypeFromFilter") as? [[String: Any]] ?? [])
                 
                NotificationCenter.default.post(name: Notification.Name("MoveToLeadMainVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "PropertyType": arySelectedPropertyType, "UrgencyType": arySelectedUrgencyType, "searchText": ""])
                
                
            }
            
            else{
                NotificationCenter.default.post(name: Notification.Name("MoveToLeadMainVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "PropertyType": [], "UrgencyType": [], "searchText": ""])
            }
            searchBar.text = ""
        }

    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
       
        if searchText.isEmpty
        {
            self.view.endEditing(true)
            self.searchAutocomplete(searchText: "")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
              searchBar.resignFirstResponder()
            }
        }
    }
}


// MARK: --------------------- Filter Delegate --------------

extension WebLeadVC : FilterWebLead_iPhone_Protocol
{
    func getDataWebLead_iPhone_Protocol(dictData: NSDictionary, tag: Int) {
        
    }
    
  

}
extension WebLeadVC : StasticsClassDelegate
{
    
    func getData(_ dict: [AnyHashable : Any]!)
    {
        let dictNew = dict! as NSDictionary
        
        strFromDate = "\(dictNew.value(forKey: "StartDate") ?? "")"
        strToDate = "\(dictNew.value(forKey: "EndDate") ?? "")"
    }
    
}
// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension WebLeadVC : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        strSearchText = str
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
           
            let isForAssociation = nsud.bool(forKey: "fromWebLeadAssociation")
                self.setTopMenuOption(tagForAssociation: isForAssociation)
            self.searchAutocomplete(searchText: str)
        }
    }
    
    
}
