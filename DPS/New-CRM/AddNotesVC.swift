//
//  AddNotesVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 09/12/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class AddNotesVC: UIViewController
{
    
    // MARK: - ----------------------------------- Outlets -----------------------------------
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblCount: UILabel!

    @IBOutlet weak var btnAddNotes: UIButton!
    
    @IBOutlet weak var txtViewNotes: UITextView!
    
    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    var dictLoginData = NSDictionary()
    var loader = UIAlertController()
    var isFromOpp = Bool()
    @objc var isFromWDO = Bool()
    // MARK: -  --- String ---
    
    var strLeadType = String()
    var strRefType = String()
    var strRefId = String()
    
    @objc var tempstrRefType = NSString ()
    @objc var tempstrRefId = NSString ()
    
    
    // MARK: -  --- NSManagedObject ---
    
    var dictLeadData = NSManagedObject()
    
    var dictActivityData = NSDictionary()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if tempstrRefId != "" && tempstrRefType != ""{
            self.strRefType = self.tempstrRefType as String
            self.strRefId = self.tempstrRefId as String
        }
        txtViewNotes.text = ""
        txtViewNotes.delegate = self
        setBorderColor(item: txtViewNotes)
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
        print("\(dictLeadData)")
        // Do any additional setup after loading the view.
        
        if strLeadType == "Activity"
        {
            lblHeader.text = "Comments"
            btnAddNotes.setTitle("Save", for: .normal   )
            txtViewNotes.placeholder = "Enter comments..."
        }
        else
        {
            lblHeader.text = "Notes"
            btnAddNotes.setTitle("Add Notes", for: .normal   )
            txtViewNotes.placeholder = "Enter notes..."
            
        }
    }
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        if tempstrRefType != "" && tempstrRefId != ""{
            self.dismiss(animated: false, completion: nil)
        }
        else{
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    
    
    @IBAction func actionOnAddNotes(_ sender: Any)
    {
        
        self.view.endEditing(true)
        
        if strLeadType == "Activity"
        {
            if((txtViewNotes.text.trimmingCharacters(in: NSCharacterSet.whitespaces)).count == 0)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter comment", viewcontrol: self)
            }
            else
            {
                addComment()
            }
            
        }
        else
        {
            if((txtViewNotes.text.trimmingCharacters(in: NSCharacterSet.whitespaces)).count == 0)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter note", viewcontrol: self)
            }
            else
            {
                if isFromWDO == true{
                    AddLeadNote()
                }
                else if isFromOpp == true{
                    addNotes()
                }
                else{
                    if strRefType.lowercased() == "lead".lowercased(){
                        AddLeadNote()
                    }
                    else{
                        addNotes()
                    }
                }
            }
        }
    }
    
    //Nilind
    
    // MARK: -  ------------------------------ API Calling  ------------------------------
    
    /*  later dded by prateek sir in sales (ios chnges by ruchika)
     https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/AddUpdateNoteRefTypeRefNo
     {
     "Note":"Test note added ruchika 5:04",
     "EmployeeId":"143035",
     "CompanyKey":"Production",
     "ExpiryDate":"",
     "RefType":"lead",
     "LeadNoteId":"",
     "RefId":"",
     "RefNo":"23420",
     "NoteCodeMasterId":""
     }*/
    func AddLeadNote()
    {
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            var strUrl =  "\(strServiceUrlMain)\(UrlAddNotesSalesNew)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            var key = NSArray()
            var value = NSArray()
            
            key = ["Note",
                   "EmployeeId",
                   "CompanyKey",
                   "ExpiryDate",
                   "RefType",
                   "LeadNoteId",
                   "RefId",
                   "RefNo",
                   "NoteCodeMasterId"]
            
            value = [ txtViewNotes.text ?? "",
                      "\(strEmpID)",
                      strCompanyKey,
                      "",
                      strRefType,
                      "",
                      "",
                      strRefId,
                      ""]
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            
            let str = getJson(from: dict_ToSend)
            print("Json \(str ?? "")" as Any)
            
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postRequestWithHeadersResponseString(dictJson: dict_ToSend as NSDictionary, url: strUrl , responseStringComing: "Timeline") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {
                    
                    if(Status){
                        
                        let strResponse = Response["data"] as! String
                        
                        if strResponse.count > 0
                        {
                            
                            UserDefaults.standard.set(true, forKey: "isNotesAdded")
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedNotes_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TimeLine_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            
                            if self.tempstrRefType != ""{
                                if self.isFromWDO == true{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedNotes_Notification_For_WDO"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                    
                                    self.dismiss(animated: false, completion: nil)
                                }
                                else{
                                    self.dismiss(animated: false, completion: nil)
                                }
                            }
                            self.navigationController?.popViewController(animated: false)
                            
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        
                    }
                }
                
            }
            
            
        }
        
    }
    
    func addNotes()
    {
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            var strUrl =  "\(strServiceUrlMain)\(UrlAddNotesNew)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            var key = NSArray()
            var value = NSArray()
            
            key = ["LeadNoteId",
                   "RefId",
                   "RefType",
                   "Note",
                   "NoteCodeMasterId",
                   "ExpiryDate",
                   "EmployeeId",
                   "CompanyKey"
            ]
            
            value = ["",
                     strRefId,
                     strRefType,
                     txtViewNotes.text ?? "",
                     "",
                     "",
                     "\(strEmpID)",
                     strCompanyKey
            ]
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            
            let str = getJson(from: dict_ToSend)
            print("Json \(str ?? "")" as Any)
            
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postRequestWithHeadersResponseString(dictJson: dict_ToSend as NSDictionary, url: strUrl , responseStringComing: "Timeline") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {
                    
                    if(Status){
                        
                        let strResponse = Response["data"] as! String
                        
                        if strResponse.count > 0
                        {
                            
                            UserDefaults.standard.set(true, forKey: "isNotesAdded")
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedNotes_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TimeLine_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            
                            if self.tempstrRefType != ""{
                                self.dismiss(animated: false, completion: nil)
                            }
                            self.navigationController?.popViewController(animated: false)
                            
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        
                    }
                }
                
            }
            
            
        }
        
    }
    
    func addComment()
    {
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            var strUrl =  "\(strServiceUrlMain)\(UrlPostComment)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            var key = NSArray()
            var value = NSArray()
            var dictLocal = NSMutableDictionary()
            
            key = ["ActivityId",
                   "Comment",
                   "CreatedBy",
                   "CreatedDate",
                   "EmployeeId"
            ]
            
            value = [strRefId,
                     txtViewNotes.text,
                     strEmpID,
                     Global().strCurrentDate(),
                     strEmpID
            ]
            
            
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            
            dictLocal = (dict_ToSend as NSDictionary).mutableCopy() as! NSMutableDictionary
            
            let str = getJson(from: dict_ToSend)
            print("Json \(str ?? "")" as Any)
            
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postRequestWithHeadersResponseString(dictJson: dict_ToSend as NSDictionary, url: strUrl , responseStringComing: "Timeline") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {
                    
                    if(Status){
                        
                        let strResponse = Response["data"] as! String
                        
                        if strResponse.count > 0
                        {
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CommentAdded_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                            
                            self.navigationController?.popViewController(animated: false)
                            
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                }
            }
        }
    }
    
    // MARK: -  ------------------------------ Core Data Functions  ------------------------------
    
    func saveWebLeadToCoreData (strLeadType: String)
    {
        if strLeadType == "WebLead"
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("createdByName")
            arrOfKeys.add("leadNoteId")
            arrOfKeys.add("leadNumber")
            arrOfKeys.add("note")
            arrOfKeys.add("opportunityNumber")
            arrOfKeys.add("userName")
            
            arrOfValues.add("\(Global().getCurrentDate() ?? "")")
            arrOfValues.add("\(strCompanyKey)")
            arrOfValues.add("\(strUserName)")
            arrOfValues.add("")
            
            arrOfValues.add("\(dictLeadData.value(forKey: "LeadNumber") ?? "")")
            
            arrOfValues.add(txtViewNotes.text ?? "")
            arrOfValues.add("")
            arrOfValues.add("\(strUserName)")
            
            saveDataInDB(strEntity: "TotalNotesCRM", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        else
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("createdByName")
            arrOfKeys.add("leadNoteId")
            arrOfKeys.add("leadNumber")
            arrOfKeys.add("note")
            arrOfKeys.add("opportunityNumber")
            arrOfKeys.add("userName")
            
            arrOfValues.add("\(Global().getCurrentDate() ?? "")")
            arrOfValues.add("\(strCompanyKey)")
            arrOfValues.add("\(strUserName)")
            arrOfValues.add("")
            arrOfValues.add("")
            arrOfValues.add(txtViewNotes.text ?? "")
            arrOfValues.add("\(dictLeadData.value(forKey: "OpportunityNumber") ?? "")")
            arrOfValues.add("\(strUserName)")
            
            saveDataInDB(strEntity: "TotalNotesCRM", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        
        
    }
    
}
extension AddNotesVC: UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        else{
            
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
                if newText.count > 1000{
                    print("jyada hai")
                }
                else{
                    lblCount.text =  "\(newText.count)/1000"
                }
                return newText.count < 1000
        }
    }
}
