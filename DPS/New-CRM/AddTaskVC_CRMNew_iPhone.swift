//
//  AddTaskVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 03/12/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class AddTaskVC_CRMNew_iPhone: UIViewController {
    
    // Outlets
    
    
    @IBOutlet weak var constHghtSearchByAcc: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnAccNo: UIButton!
    
    @IBOutlet weak var btnOppNo: UIButton!
    @IBOutlet weak var txtfldSearchByAccount: ACFloatingTextField!
    @IBOutlet weak var btnTaskTemplate: UIButton!
    @IBOutlet weak var txtfldTaskName: ACFloatingTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var btnDueDate: UIButton!
    @IBOutlet weak var btnDueTime: UIButton!
    @IBOutlet weak var btnReminderDate: UIButton!
    @IBOutlet weak var btnReminderTime: UIButton!
    @IBOutlet weak var btnAssignedTo: UIButton!
    @IBOutlet weak var btnUrgency: UIButton!
    @IBOutlet weak var btnLogAsActivity: UIButton!
    @IBOutlet weak var btnActivityLogType: UIButton!
    @IBOutlet weak var txtfldActivity: ACFloatingTextField!
    @IBOutlet weak var txtfldTags: ACFloatingTextField!
    
    @IBOutlet weak var viewContainerLogActivity: UIView!
    
    @IBOutlet weak var btnSave: ButtonWithShadow!
    
    
    @IBOutlet weak var hghtConstViewContainerLogAsActivity: NSLayoutConstraint!
    
    var aryPopUp = NSMutableArray()
    var arrOfSubActivty = NSMutableArray()
    var dictLoginData = NSDictionary()
    var dictDetailsFortblView = NSDictionary()
    var dictSelectedTaskTemplate = NSDictionary()
    var dictSelectedAssignedTo = NSDictionary()
    var dictTask = NSDictionary()
    var aryEmployeeList = NSArray()
    
    var strServiceUrlMain = ""
    var strEmpID = ""
    var strCreatedBy = ""
    var strUserName = ""
    var strCompanyKey = ""
    var strCompanyId = ""
    var isLogAsActivity = false
    var strFromVC = ""
    var strleadIdToUpdate = ""
    var strLogTypeIdd = ""
    var isSelectedLeadByAccountNo = false
    var strLeadIdByAccountNo = ""
    var strAccountIdByAccountNo = ""
    var strSalesPersonId = ""
    var strSalesPersonName = ""
    var strPriorityId = ""
    var strGlobalStatusToSend = "Open"
    var strTypeOfTaskToFetch = ""
    var strFollowUpTaskId = ""
    var isUpdateTask = Bool()
    var strRefTypee = ""
    var strRefIdd = ""
    var strLeadTaskId = ""
    var strLeadNumbr = ""
    @objc var strLeadNo = ""
    @objc var strAccNo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hghtConstViewContainerLogAsActivity.constant = 0.0
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        dictDetailsFortblView = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
        
        strEmpID  = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        
        strCreatedBy  = "\(dictLoginData.value(forKey: "CreatedBy")!)"
        
        strUserName  = "\(dictLoginData.value(forKeyPath: "Company.Username")!)"
        
        strCompanyKey     = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        
        strCompanyId      = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
        
        aryEmployeeList = nsud.value(forKey: "EmployeeList") as! NSArray
        
        lblTitle.text = "Add Task"
        btnSave.setTitle("Save", for: .normal)
        
       
        
        if(strFromVC == "WebLeadVC")
        {
            constHghtSearchByAcc.constant = 0.0
            strLeadTaskId = ""
            strRefTypee = "WebLead"
            strRefIdd = strleadIdToUpdate
            
            btnAccNo.setTitle("Account #: \(strAccNo)", for: .normal)
            
            btnOppNo.setTitle("Lead #: \(strLeadNo)", for: .normal)
        }
        if(strFromVC == "DashBoardView")
        {
            strLeadTaskId = ""
            strRefTypee = "Employee"
            strRefIdd = strEmpID
        }
        
        if(strFromVC == "OpportunityVC")
        {
            constHghtSearchByAcc.constant = 0.0
            strRefTypee = "Lead"
            strRefIdd = strleadIdToUpdate
            
            btnAccNo.setTitle("Account #: \(strAccNo)", for: .normal)
            
            btnOppNo.setTitle("Opportunity #: \(strLeadNo)", for: .normal)
        }
        
        if(isUpdateTask == true)
        {
            constHghtSearchByAcc.constant = 0.0
            
            isUpdateTask = true
            
            if(strFromVC == "OpportunityVC")
            {
                btnAccNo.setTitle("Account #: \(dictTask.value(forKey: "accountNo")!)", for: .normal)
                
                btnOppNo.setTitle("Opportunity #: \(dictTask.value(forKey: "leadNo")!)", for: .normal)
                
            }else if(strFromVC == "WebLeadVC")
            {
                
                btnAccNo.setTitle("Account #: \(dictTask.value(forKey: "accountNo")!)", for: .normal)
                
                btnOppNo.setTitle("Lead #: \(dictTask.value(forKey: "webLeadNo")!)", for: .normal)
                
            }else{
                
                if "\(dictTask.value(forKey: "leadNo")!)".count > 0 {
                    
                    btnAccNo.setTitle("Account #: \(dictTask.value(forKey: "accountNo")!)", for: .normal)
                    
                    btnOppNo.setTitle("Opportunity #: \(dictTask.value(forKey: "leadNo")!)", for: .normal)
                    
                }
                if "\(dictTask.value(forKey: "webLeadNo")!)".count > 0 {
                    
                    btnAccNo.setTitle("Account #: \(dictTask.value(forKey: "accountNo")!)", for: .normal)
                    
                    btnOppNo.setTitle("Lead #: \(dictTask.value(forKey: "webLeadNo")!)", for: .normal)

                }
                
            }
            
            strLeadTaskId = "\(dictTask.value(forKey: "leadTaskId")!)"
            
            lblTitle.text = "Update Task"
            
            txtviewDescription.text = "\(dictTask.value(forKey: "taskDescription")!)"
            
            txtfldTaskName.text = "\(dictTask.value(forKey: "taskName")!)"
            
            txtfldTags.text = "\(dictTask.value(forKey: "tags")!)"
            
            strPriorityId = "\(dictTask.value(forKey: "priority")!)"
            
            strGlobalStatusToSend = "\(dictTask.value(forKey: "status")!)"
            let ary = getPriorityMaster()
            
            if(ary.count > 0)
            {
                for item in ary
                {
                    if("\((item as! NSDictionary).value(forKey: "PriorityId")!)" == strPriorityId)
                    {
                        
                        btnUrgency.setTitle("\((item as! NSDictionary).value(forKey: "Name")!)", for: .normal)
                    }
                }
            }
            else
            {
                btnUrgency.setTitle("", for: .normal)
            }
            
            if(dictTask.value(forKey: "dueDate") is Date)
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                
                let strdate = dateFormatter.string(from: dictTask.value(forKey: "dueDate")! as! Date)
                
                btnDueDate.setTitle(strdate, for: .normal)
            }
            else
            {
                btnDueDate.setTitle(Global().convertDate("\(dictTask.value(forKey: "dueDate")!)"), for: .normal)
            }
            
            let strDueTime = "\(dictTask.value(forKey: "dueTime")!)"
            
            if(strDueTime == "00:00")
            {
                btnDueTime.setTitle("", for: .normal)
            }
            else
            {
                btnDueTime.setTitle(strDueTime, for: .normal)
            }
            
            let strReminderDate = Global().convertDate("\(dictTask.value(forKey: "reminderDate")!)")
            
            btnReminderDate.setTitle(strReminderDate, for: .normal)
            
            let strReminderTime = Global().convertTime("\(dictTask.value(forKey: "reminderDate")!)")
            
            if(strReminderTime == "00:00")
            {
                btnReminderTime.setTitle("", for: .normal)
            }
            else
            {
                btnReminderTime.setTitle(strReminderTime, for: .normal)
            }
            
            strSalesPersonId = "\(dictTask.value(forKey: "assignedTo")!)"
            
            for item in aryEmployeeList
            {
                if(strSalesPersonId == "\((item as! NSDictionary).value(forKey: "EmployeeId")!)")
                {
                    strSalesPersonName = "\((item as! NSDictionary).value(forKey: "FullName")!)"
                    
                    dictSelectedAssignedTo = item as! NSDictionary
                    break
                }
            }
            
            btnAssignedTo.setTitle("\(strSalesPersonName)", for: .normal)
            
            /* if((dictTask.value(forKeyPath: "childActivities") as! NSArray).count > 0)
             {
             let childActivity = (dictTask.value(forKey: "childActivities") as! NSArray).firstObject as! NSDictionary
             hghtConstViewContainerLogAsActivity.constant = 160.0
             btnLogAsActivity.setImage(UIImage(named: "check_box_2.png"), for: .normal)
             isLogAsActivity = true
             
             txtfldActivity.text = "\(childActivity.value(forKey: "Agenda")!)"
             
             
             let aryTemp = dictDetailsFortblView.value(forKey: "ActivityLogTypeMasters") as! NSArray
             
             if(aryTemp.count > 0)
             {
             for item in aryTemp
             {
             let dict = item as! NSDictionary
             if("\(dict.value(forKey: "LogTypeId")!)" == "\(childActivity.value(forKey: "LogTypeId")!)")
             {
             btnActivityLogType.setTitle("\(dict.value(forKey: "Name")!)", for: .normal)
             
             strLogTypeIdd = "\(dict.value(forKey: "LogTypeId")!)"
             break
             }
             }
             }
             }*/
            
            btnSave.setTitle("Update", for: .normal)
        }
    }
    
    fileprivate func configureUI()
    {
        makeCornerRadius(value: 2.0, view: txtviewDescription, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnTaskTemplate, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnDueDate, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnDueTime, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnReminderDate, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnReminderTime, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnAssignedTo, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnUrgency, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnActivityLogType, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        makeCornerRadius(value: 20, view: btnSave, borderWidth: 0.0, borderColor: UIColor.lightGray)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    // MARK: UIButton action
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
        //nsud.set(true, forKey: "fromAddTask")
        //nsud.synchronize()
        
    }
    
    @IBAction func actionOnAccNo(_ sender: UIButton) {
    }
    
    @IBAction func actionOnOppNo(_ sender: UIButton) {
    }
    
    
    @IBAction func actionOnSearch(_ sender: UIButton)
    {
        
        self.view.endEditing(true)
        if(isInternetAvailable() == true)
        {
            if(txtfldSearchByAccount.text?.count == 0)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Account Number to search", viewcontrol: self)
            }
            else
            {
                if(isInternetAvailable() == false)
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                }
                else{
                    callAPIToFetchLeadsByAccountNumber()
                }
                
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        
        
        /* BOOL isNetReachable=[global isNetReachable];
         
         if (isNetReachable) {
         
         if (_txtFld_AccountNoToSearchLead.text.length==0) {
         
         [global displayAlertController:Alert :@"Please enter account no to search" :self];
         
         } else {
         
         [_txtFld_AccountNoToSearchLead resignFirstResponder];
         
         [DejalBezelActivityView activityViewForView:self.view withLabel:@"Searching..."];
         
         [self performSelector:@selector(callAPIFetchLeadsByAccountNo) withObject:nil afterDelay:0.2];
         
         }
         
         }else {
         
         [global AlertMethod:Alert :ErrorInternetMsg];
         
         }*/
    }
    
    
    @IBAction func actionOnTaskTemplate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        aryPopUp.removeAllObjects()
        /*{
         
         [self AllTxtFieldDown];
         
         [viewForDate removeFromSuperview];
         [viewBackGround removeFromSuperview];
         [tblData removeFromSuperview];
         
         arrDataTblView=[[NSMutableArray alloc]init];
         NSArray *arrOfLogType=[dictDetailsFortblView valueForKey:@"TaskTemplateMasters"];
         
         [arrDataTblView addObject:@"1"]; // setting select on zero index
         
         for (int k=0; k<arrOfLogType.count; k++) {
         
         NSDictionary *dictDataa=arrOfLogType[k];
         BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
         
         if (isActive) {
         
         [arrDataTblView addObject:dictDataa];
         
         }
         }
         
         
         if (arrDataTblView.count==0) {
         [global AlertMethod:Info :NoDataAvailable];
         }else{
         tblData.tag=106;
         [self tableLoad:tblData.tag];
         }
         
         }*/
        
        let aryTemp = (dictDetailsFortblView.value(forKey: "TaskTemplateMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryPopUp.add(dict)
                }
            }
            
            if(aryPopUp.count > 0)
            {
                sender.tag = 301
                
                openTableViewPopUp(tag: 301, ary: aryPopUp , aryselectedItem: NSMutableArray())
            }
            else
            {
                // show alert no data available
            }
        }
        else
        {
            // show alert no data available
        }
        
        
    }
    
    
    @IBAction func actionOnDueDate(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 302
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    
    @IBAction func actionOnDueTime(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 303
        gotoDatePickerView(sender: sender, strType: "Time")
    }
    
    
    @IBAction func actionOnReminderDate(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 304
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    
    
    @IBAction func actionOnReminderTime(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = 305
        gotoDatePickerView(sender: sender, strType: "Time")
    }
    
    @IBAction func actionOnAssignedTo(_ sender: UIButton) {
        self.view.endEditing(true)
        aryPopUp.removeAllObjects()
        
        let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
        
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryPopUp.add(dict)
                }
            }
            
            if(aryPopUp.count > 0)
            {
                sender.tag = 306
                
                openTableViewPopUp(tag: 306, ary: aryPopUp , aryselectedItem: NSMutableArray())
            }
            else
            {
                // show alert no data available
            }
            
        }
            
        else{
            // show alert no data available
        }
        
        
        /*{//strPriorityId=[NSString stringWithFormat:@"%@",[dictDataPriority valueForKey:@"PriorityId"]];
         
         [self tbleAllocMethod];
         
         [self AllTxtFieldDown];
         
         [viewForDate removeFromSuperview];
         [viewBackGround removeFromSuperview];
         [tblData removeFromSuperview];
         
         NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
         NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
         
         arrDataTblView=[[NSMutableArray alloc]init];
         NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
         
         for (int k=0; k<arrOfEmployee.count; k++) {
         
         NSDictionary *dictDataa=arrOfEmployee[k];
         BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
         if (isActive) {
         
         [arrDataTblView addObject:dictDataa];
         
         } else {
         
         if (YesUpdateInactiveCheck) {
         
         long long  longEmployeeId=[[dictDataa valueForKey:@"EmployeeId"] longLongValue];
         long long  longEmployeeIdToCheck=[strSalesPersonId longLongValue];
         if (longEmployeeId==longEmployeeIdToCheck) {
         
         [arrDataTblView addObject:dictDataa];
         
         }
         }
         }
         }
         
         if (arrDataTblView.count==0) {
         [global AlertMethod:Info :NoDataAvailable];
         }else{
         tblData.tag=101;
         [self tableLoad:tblData.tag];
         }
         }*/
        
        
        sender.tag = 306
        openTableViewPopUp(tag: 306, ary: aryPopUp , aryselectedItem: NSMutableArray())
    }
    
    @IBAction func actionOnUrgency(_ sender: UIButton) {
        
        self.view.endEditing(true)
        aryPopUp.removeAllObjects()
        let aryTemp = dictDetailsFortblView.value(forKey: "PriorityMasters") as! NSArray
        
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryPopUp.add(dict)
                }
            }
            
            if(aryPopUp.count > 0)
            {
                sender.tag = 307
                
                openTableViewPopUp(tag: 307, ary: aryPopUp , aryselectedItem: NSMutableArray())
            }
            else
            {
                // show alert no data available
            }
            
        }
        /*{
         
         [self tbleAllocMethod];
         
         [self AllTxtFieldDown];
         
         [viewForDate removeFromSuperview];
         [viewBackGround removeFromSuperview];
         [tblData removeFromSuperview];
         
         arrDataTblView=[[NSMutableArray alloc]init];
         NSArray *arrOfStatus=[dictDetailsFortblView valueForKey:@"PriorityMasters"];
         
         for (int k=0; k<arrOfStatus.count; k++) {
         
         NSDictionary *dictDataa=arrOfStatus[k];
         BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
         if (isActive) {
         
         [arrDataTblView addObject:dictDataa];
         
         } else {
         
         if (YesUpdateInactiveCheck) {
         
         long long  longEmployeeId=[[dictDataa valueForKey:@"PriorityId"] longLongValue];
         long long  longEmployeeIdToCheck=[strPriorityId longLongValue];
         if (longEmployeeId==longEmployeeIdToCheck) {
         
         [arrDataTblView addObject:dictDataa];
         
         }
         }
         }
         }
         
         if (arrDataTblView.count==0) {
         [global AlertMethod:Info :NoDataAvailable];
         }else{
         tblData.tag=102;
         [self tableLoad:tblData.tag];
         }
         }*/
        
        sender.tag = 307
        openTableViewPopUp(tag: 307, ary: aryPopUp , aryselectedItem: NSMutableArray())
    }
    
    @IBAction func actionOnLogAsActivity(_ sender: UIButton) {
        self.view.endEditing(true)
        if(btnLogAsActivity.currentImage == UIImage(named: "check_box_2.png"))
        {
            btnLogAsActivity.setImage(UIImage(named: "uncheck.png"), for: .normal)
            isLogAsActivity = false
            hghtConstViewContainerLogAsActivity.constant = 0.0
            
            btnActivityLogType.setTitle("--Select--", for: .normal)
            strLogTypeIdd = ""
            txtfldActivity.text = ""
            self.view.endEditing(true)
            
        }
        else{
            btnLogAsActivity.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            isLogAsActivity = true
            hghtConstViewContainerLogAsActivity.constant = 160.0
        }
    }
    
    @IBAction func actionOnActivityLogType(_ sender: UIButton) {
        self.view.endEditing(true)
        aryPopUp.removeAllObjects()
        let aryTemp = dictDetailsFortblView.value(forKey: "ActivityLogTypeMasters") as! NSArray
        
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryPopUp.add(dict)
                }
            }
            
            if(aryPopUp.count > 0)
            {
                sender.tag = 308
                
                openTableViewPopUp(tag: 308, ary: aryPopUp , aryselectedItem: NSMutableArray())
            }
            else
            {
                // show alert no data available
            }
            
        }
        
        /*{
         
         [self AllTxtFieldDown];
         
         [viewForDate removeFromSuperview];
         [viewBackGround removeFromSuperview];
         [tblData removeFromSuperview];
         
         // [self setValueForParticipants];
         
         arrDataTblView=[[NSMutableArray alloc]init];
         NSArray *arrOfLogType=[dictDetailsFortblView valueForKey:@"ActivityLogTypeMasters"];
         
         for (int k=0; k<arrOfLogType.count; k++) {
         
         NSDictionary *dictDataa=arrOfLogType[k];
         BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
         if (isActive) {
         
         [arrDataTblView addObject:dictDataa];
         
         } else {
         
         if (YesUpdateInactiveCheck) {
         
         long long  longEmployeeId=[[dictDataa valueForKey:@"LogTypeId"] longLongValue];
         long long  longEmployeeIdToCheck=[strLogTypeIdd longLongValue];
         if (longEmployeeId==longEmployeeIdToCheck) {
         
         [arrDataTblView addObject:dictDataa];
         
         }
         }
         }
         }
         
         
         if (arrDataTblView.count==0) {
         [global AlertMethod:Info :NoDataAvailable];
         }else{
         tblData.tag=104;
         [self tableLoad:tblData.tag];
         }
         }*/
        
        sender.tag = 308
        openTableViewPopUp(tag: 308, ary: aryPopUp , aryselectedItem: NSMutableArray())
    }
    
    @IBAction func actionOnSave(_ sender: ButtonWithShadow)
    {
        nsud.set(true, forKey: "fromAddTask")
        nsud.synchronize()
        
        self.view.endEditing(true)
        
        let errMsg = validation()
        
        if(errMsg.count > 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: errMsg, viewcontrol: self)
        }
        else
        {
            if(isInternetAvailable() == true)
            {
                if (isUpdateTask) {
                    
                    FTIndicator.showProgress(withMessage: "Updating Task...", userInteractionEnable: false)
                    
                }else{
                    
                    // saveTaskToLocalDBInOldTable()
                    
                    FTIndicator.showProgress(withMessage: "Adding Task...", userInteractionEnable: false)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    
                    self.callAPIToAddTask()
                }
                
            }
            else
            {
                if(isUpdateTask == true)
                {
                    // updateTaskInLocalDBInOldTable()
                    updateTaskInLocalDBInNewTable(item: dictTask)
                    
                }
                else{
                    
                    //saveTaskToLocalDBInOldTable()
                    saveTaskToLocalDBInNewTable(item: NSDictionary())
                }
            }
        }
    }
    
    
    
    // MARK: Functions
    
    func callAPIToFetchLeadsByAccountNumber()
    {
        let url = "\(strServiceUrlMain)" + "\(UrlGetLeadListByAccountNo)" + "\(strCompanyKey)" + "\(UrlGetLeadListByAccountNoName)" + "\(txtfldSearchByAccount.text!)"
        
        FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
        
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: url, responseStringComing: "AddTaskVC_CRMNew_iPhone") { (response, status) in
            
            FTIndicator.dismissProgress()
            
            print(response)
            
            if(status == true)
            {
                if((response.value(forKey: "Leads") as! NSArray).count == 0){
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                }
                else
                {
                    self.strAccountIdByAccountNo = "\(response.value(forKey: "AccountId")!)"
                    
                    let arytemp = (response.value(forKey: "Leads") as!  NSArray).mutableCopy() as! NSMutableArray
                    
                    self.openTableViewPopUp(tag: 400, ary: arytemp, aryselectedItem: NSMutableArray())
                }
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
        
    }
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = true

        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    fileprivate func validation()-> String
    {
        var errorMessage = ""
        
        if(txtfldTaskName.text?.count == 0)
        {
            errorMessage = "Please enter Task name";
        }
            
        else if(btnDueDate.titleLabel?.text == nil)
        {
            errorMessage = "Please enter Task Due Date.";
        }
            
        else if(btnDueTime.titleLabel?.text == nil)
        {
            errorMessage = "Please enter Task Due Time.";
        }
            
        else if(btnReminderDate.titleLabel?.text == nil)
        {
            errorMessage = "Please enter Reminder Date.";
        }
            
        else if(btnReminderTime.titleLabel?.text == nil)
        {
            errorMessage = "Please enter Reminder Time.";
        }
            
        else  if(btnReminderDate.titleLabel?.text != "" && btnReminderTime.titleLabel?.text != "")
        {
            let dateFormat = DateFormatter.init()
            dateFormat.dateFormat = "MM/dd/yyyyhh:mm a"
            
            let reminderFulDateTime = (btnReminderDate.titleLabel?.text ?? "")! + (btnReminderTime.titleLabel?.text ?? "")!
            
            let dueFullDateTime = (btnDueDate.titleLabel?.text ?? "")! + (btnDueTime.titleLabel?.text ?? "")!
            
            let reminderDate = dateFormat.date(from: reminderFulDateTime)
            
            let dueDate = dateFormat.date(from: dueFullDateTime)
            
            switch reminderDate!.compare(dueDate!) {
                
            case .orderedDescending:
                errorMessage = "Reminder Date & Time should be less than Due Date & Time"
                break
                
            case .orderedSame:
                errorMessage = "Reminder Date & Time should be less than Due Date & Time"
                break
            default:break
            }
        }
        if(btnUrgency.titleLabel?.text == "--Select--")
        {
            errorMessage = "Please select Urgency";
        }
            
        else if(btnAssignedTo.titleLabel?.text == "--Select--")
        {
            errorMessage = "Please select Assign to";
        }
        if(isLogAsActivity == true)
        {
            if(txtfldActivity.text?.count == 0)
            {
                errorMessage = alertActivityAgenda
            }
            else if(btnActivityLogType.titleLabel?.text == "--Select--")
            {
                errorMessage = alertActivityLogType
            }
        }
        
        return errorMessage
    }
    
    fileprivate func callAPIToAddTask()
    {
        
        if(isSelectedLeadByAccountNo == true)
        {
            if(strLeadIdByAccountNo.count > 0)
            {
                strLeadTaskId = ""
                strRefTypee = "Lead"
                strRefIdd = strLeadIdByAccountNo;
            }
            else if (strAccountIdByAccountNo.count > 0){
                
                strLeadTaskId = ""
                strRefTypee = "Account"
                strRefIdd = strAccountIdByAccountNo
                
            } else {
                
                strLeadTaskId = ""
                strRefTypee = "Employee"
                strRefIdd = strEmpID;
                
            }
        }
        
        if (strLogTypeIdd.count > 0) {
            
            var object1 = NSArray()
            var keys1 = NSArray()
            object1 = ["\(txtfldActivity.text ?? "")", strLogTypeIdd]
            keys1 = ["Agenda","LogTypeId"]
            
            let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
            
            arrOfSubActivty.add(dict_ToSend1)
            
        }
        
        let strFullDateTime = (btnDueDate.titleLabel?.text)! + " " + (getTimeWithouAMPM(strTime: (btnDueTime.titleLabel?.text)!))
        
        let strFullDateTimeReminder =  (btnReminderDate.titleLabel?.text!)! + " " + (getTimeWithouAMPM(strTime: (btnReminderTime.titleLabel?.text)!))
        
        var keys = NSArray()
        var values = NSArray()
        keys = ["Id",
                "LeadTaskId",
                "RefId",
                "RefType",
                "TaskName",
                "DueDate",
                "ReminderDate",
                "Description",                               "AssignedTo",                                 "Priority",                                  "Status",                                   "CreatedBy",                                    "Tags",                                     "ChildActivities"]
        
        values = ["1",strLeadTaskId,                     strRefIdd,strRefTypee,
                  txtfldTaskName.text!,
                  strFullDateTime,
                  strFullDateTimeReminder,
                  txtviewDescription.text.count == 0 ? "" :txtviewDescription.text,
                  strSalesPersonId,
                  strPriorityId,
                  strGlobalStatusToSend,
                  strCreatedBy,
                  txtfldTags.text?.count == 0 ? "" : txtfldTags.text!,
                  arrOfSubActivty]
        
        let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        
        let strURL = strServiceUrlMain + UrlAddUpdateTaskGlobal
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "AddTask"
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            DispatchQueue.main.async {
                
                FTIndicator.dismissProgress()
            }
            
            if(success)
            {
                nsud.set(true, forKey: "fromAddUpdateTask")
                nsud.synchronize()
                
                if(self.isUpdateTask)
                {
                    self.updateTaskInLocalDBInNewTable(item: (response as NSDictionary?)!)
                }
                else
                {
                    
                    /*  let arrayTemp = getDataFromLocal(strEntity: "TaskList", predicate: NSPredicate(format: "userName == %@ ",self.strUserName))
                     
                     if(arrayTemp.count > 0)
                     {
                     print(arrayTemp)
                     }*/
                    
                    self.saveTaskToLocalDBInNewTable(item: (response as NSDictionary?)!)
                    
                    let alertCOntroller = UIAlertController(title: "Message", message: "Task Added Successfully", preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                        
                        self.navigationController?.popViewController(animated: false)
                    })
                    
                    alertCOntroller.addAction(alertAction)
                    self.present(alertCOntroller, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    
    /* fileprivate func saveTaskToLocalDBInOldTable()
     {
     let arrOfKeys = NSMutableArray()
     let arrOfValues = NSMutableArray()
     
     var strLeadTaskId = "", strRefTypee = "",strRefIdd = ""
     
     if(strFromVC == "WebLeadVC")
     {
     strLeadTaskId = ""
     strRefTypee = "WebLead"
     strRefIdd = strleadIdToUpdate
     }
     else  if(strFromVC == "DashBoardView")
     {
     strLeadTaskId = ""
     strRefTypee = "Employee"
     strRefIdd = strEmpID
     }
     
     else  if(strFromVC == "OpportunityVC")
     {
     strRefTypee = "Lead"
     }
     
     if (strLogTypeIdd.count > 0) {
     
     var object1 = NSArray()
     var keys1 = NSArray()
     object1 = ["\(txtfldActivity.text ?? "")", strLogTypeIdd]
     keys1 = ["Agenda","LogTypeId"]
     
     let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
     
     arrOfSubActivty.add(dict_ToSend1)
     }
     
     let strFullDateTime = (btnDueDate.titleLabel?.text)! + " " + (getTimeWithouAMPM(strTime: (btnDueTime.titleLabel?.text)!))
     
     
     var strFullDateTimeReminder =  (btnReminderDate.titleLabel?.text!)! + " " +
     (getTimeWithouAMPM(strTime: (btnReminderTime.titleLabel?.text)!))
     
     if (btnReminderDate.titleLabel?.text == ""){
     strFullDateTimeReminder = ""
     }
     
     arrOfKeys.add("Id")
     arrOfKeys.add("LeadTaskId")
     arrOfKeys.add("RefId")
     arrOfKeys.add("RefType")
     arrOfKeys.add("TaskName")
     arrOfKeys.add("DueDate")
     arrOfKeys.add("ReminderDate")
     arrOfKeys.add("Description")
     arrOfKeys.add("AssignedTo")
     arrOfKeys.add("Priority")
     arrOfKeys.add("Status")
     arrOfKeys.add("CreatedBy")
     arrOfKeys.add("Tags")
     arrOfKeys.add("ChildActivities")
     
     // values
     
     arrOfValues.add("1")
     arrOfValues.add(strLeadTaskId)
     arrOfValues.add(strRefIdd)
     arrOfValues.add(strRefTypee)
     arrOfValues.add(txtfldTaskName.text!)
     arrOfValues.add(strFullDateTime)
     arrOfValues.add(strFullDateTimeReminder)
     arrOfValues.add("\(txtviewDescription.text.count == 0 ? "" :txtviewDescription.text!)")
     arrOfValues.add(strSalesPersonId)
     arrOfValues.add(strPriorityId)
     arrOfValues.add(strGlobalStatusToSend)
     arrOfValues.add(strCreatedBy)
     arrOfValues.add(txtfldTags.text?.count == 0 ? "" : txtfldTags.text!)
     arrOfValues.add(arrOfSubActivty)
     
     
     let dict_ToSend = NSDictionary.init(objects: arrOfValues as! [Any], forKeys: arrOfKeys as! [NSCopying])
     
     let arrOfTaskListToSave = NSMutableArray()
     
     arrOfTaskListToSave.add(dict_ToSend)
     
     let dictLogin = nsud.value(forKey: "LoginDetails") as! NSDictionary
     let strHrmsCompanyId = "\(dictLogin.value(forKeyPath: "Company.HrmsCompanyId")!)"
     let strEmployeeNumber = "\(dictLogin.value(forKeyPath: "EmployeeNumber")!)"
     let strEmployeeId = "\(dictLogin.value(forKeyPath: "EmployeeId")!)"
     let strCreatedByyy = "\(dictLogin.value(forKeyPath: "CreatedBy")!)"
     let strEmployeeName = "\(dictLogin.value(forKeyPath: "EmployeeName")!)"
     let strCoreCompanyId = "\(dictLogin.value(forKeyPath: "Company.CoreCompanyId")!)"
     let strSalesProcessCompanyId = "\(dictLogin.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
     let strCompanyKeyyy = "\(dictLogin.value(forKeyPath: "Company.CompanyKey")!)"
     
     
     let arrOfSubKeys = NSMutableArray()
     let arrOfSubValues = NSMutableArray()
     
     arrOfSubKeys.add("HrmsCompanyId")
     arrOfSubKeys.add("EmployeeNumber")
     arrOfSubKeys.add("EmployeeId")
     arrOfSubKeys.add("CreatedBy")
     arrOfSubKeys.add("EmployeeName")
     arrOfSubKeys.add("CoreCompanyId")
     arrOfSubKeys.add("SalesProcessCompanyId")
     arrOfSubKeys.add("CompanyKey")
     arrOfSubKeys.add("strServiceUrlMain")
     
     arrOfSubValues.add(strHrmsCompanyId)
     arrOfSubValues.add(strEmployeeNumber)
     arrOfSubValues.add(strEmployeeId)
     arrOfSubValues.add(strCreatedByyy)
     arrOfSubValues.add(strEmployeeName)
     arrOfSubValues.add(strCoreCompanyId)
     arrOfSubValues.add(strSalesProcessCompanyId)
     arrOfSubValues.add(strCompanyKeyyy)
     arrOfSubValues.add(strServiceUrlMain)
     
     let dictDataAllValues = NSDictionary.init(objects: arrOfSubValues as! [Any], forKeys: arrOfSubKeys as! [NSCopying])
     
     
     let arrOfTableKeys = NSMutableArray()
     let arrOfTableValues = NSMutableArray()
     
     arrOfTableKeys.add("arrOfTask")
     arrOfTableKeys.add("userName")
     arrOfTableKeys.add("companyKey")
     arrOfTableKeys.add("typeOfTask")
     
     arrOfTableKeys.add("dictData")
     
     arrOfTableValues.add(arrOfTaskListToSave)
     arrOfTableValues.add(strUserName)
     arrOfTableValues.add(strCompanyKey)
     arrOfTableValues.add(strTypeOfTaskToFetch)
     arrOfTableValues.add(dictDataAllValues)
     
     saveDataInDB(strEntity: "TaskList", arrayOfKey: arrOfTableKeys, arrayOfValue: arrOfTableValues)
     
     }*/
    
    fileprivate func saveTaskToLocalDBInNewTable(item:NSDictionary)
    {
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        let dictData = removeNullFromDict(dict: (item).mutableCopy() as! NSMutableDictionary)
        
        arrOfKeys.add("userName")
        arrOfKeys.add("companyKey")
        
        arrOfKeys.add("addressLine1")
        arrOfKeys.add("addressLine2")
        arrOfKeys.add("assignedByStr")
        arrOfKeys.add("assignedTo")
        arrOfKeys.add("assignedToStr")
        arrOfKeys.add("cellPhone1")
        arrOfKeys.add("cellPhone2")
        arrOfKeys.add("childActivities")
        arrOfKeys.add("cityName")
        arrOfKeys.add("clientCreatedDate")
        arrOfKeys.add("clientModifiedDate")
        arrOfKeys.add("companyName")
        arrOfKeys.add("countryId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("dueDate")
        arrOfKeys.add("dueTime")
        arrOfKeys.add("firstName")
        arrOfKeys.add("followUpFromTaskId")
        arrOfKeys.add("lastName")
        
        arrOfKeys.add("leadTaskId")
        arrOfKeys.add("middleName")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("primaryEmail")
        arrOfKeys.add("primaryPhone")
        arrOfKeys.add("primaryPhoneExt")
        arrOfKeys.add("priority")
        arrOfKeys.add("priorityStr")
        arrOfKeys.add("refId")
        arrOfKeys.add("refType")
        arrOfKeys.add("reminderDate")
        arrOfKeys.add("secondaryEmail")
        arrOfKeys.add("secondaryPhone")
        arrOfKeys.add("secondaryPhoneExt")
        arrOfKeys.add("stateId")
        arrOfKeys.add("status")
        arrOfKeys.add("tags")
        arrOfKeys.add("taskDescription")
        arrOfKeys.add("taskName")
        arrOfKeys.add("webLeadNo")
        arrOfKeys.add("zipcode")
        arrOfKeys.add("uniqueId")
        arrOfKeys.add("compareDate")
      
        arrOfKeys.add("accountNo")
        arrOfKeys.add("leadNo")
      
        if(dictData.count > 0)
        {
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add("\(dictData.value(forKey: "AddressLine1")!)")
            arrOfValues.add("\(dictData.value(forKey: "AddressLine2")!)")
            arrOfValues.add("\(dictData.value(forKey: "AssignedByStr")!)")
            arrOfValues.add("\(dictData.value(forKey: "AssignedTo")!)")
            arrOfValues.add("\(dictData.value(forKey: "AssignedToStr")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone1")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone2")!)")
            
            
            if((dictData.value(forKey: "ChildActivities") as! NSArray).count > 0)
            {
                let aryChild = NSMutableArray()
                let aryTemp = (dictData.value(forKey: "ChildActivities") as! NSArray).mutableCopy() as! NSMutableArray
                
                for itemTemp in aryTemp
                {
                    aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
                }
                arrOfValues.add(aryChild)
            }
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            arrOfValues.add("\(dictData.value(forKey: "City")!)")
            
            if("\(dictData.value(forKey: "ClientCreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientCreatedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ClientCreatedDate")!)")
                
            }
            else
            {
                arrOfValues.add("")
                
            }
            
            if("\(dictData.value(forKey: "ClientModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientModifiedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ClientModifiedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "CompanyName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CountryId")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CreatedBy")!)")
            
            if("\(dictData.value(forKey: "CreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "CreatedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "CreatedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "DueDate")!)" != "<null>" && "\(dictData.value(forKey: "DueDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "DueDate")!)")
                arrOfValues.add(Global().convertTime("\(dictData.value(forKey: "DueDate")!)"))
                
            }
            else
            {
                arrOfValues.add("")
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "FirstName")!)")
            arrOfValues.add("\(dictData.value(forKey: "FollowUpFromTaskId")!)")
            arrOfValues.add("\(dictData.value(forKey: "LastName")!)")
           
            arrOfValues.add("\(dictData.value(forKey: "LeadTaskId")!)")
            arrOfValues.add("\(dictData.value(forKey: "MiddleName")!)")
            arrOfValues.add("\(dictData.value(forKey: "ModifiedBy")!)")
            
            if("\(dictData.value(forKey: "ModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ModifiedDate")!)" != "")
            {
                
                arrOfValues.add("\(dictData.value(forKey: "ModifiedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "PrimaryEmail")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhone")!)")
            arrOfValues.add("") //PrimaryPhoneExt
            arrOfValues.add("\(dictData.value(forKey: "Priority")!)")
            arrOfValues.add("\(dictData.value(forKey: "PriorityStr")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "RefId")!)")
            arrOfValues.add("\(dictData.value(forKey: "RefType")!)")
            
            if("\(dictData.value(forKey: "ReminderDate")!)" != "<null>" && "\(dictData.value(forKey: "ReminderDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ReminderDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "SecondaryEmail")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhone")!)")
            arrOfValues.add("") //SecondaryPhoneExt
            arrOfValues.add("\(dictData.value(forKey: "StateId")!)")
            arrOfValues.add("\(dictData.value(forKey: "Status")!)")
            arrOfValues.add("\(dictData.value(forKey: "Tags")!)")
            arrOfValues.add("\(dictData.value(forKey: "Description")!)")
            arrOfValues.add("\(dictData.value(forKey: "TaskName")!)")
            arrOfValues.add("\(dictData.value(forKey: "WebLeadNumber")!)")
            arrOfValues.add("\(dictData.value(forKey: "ZipCode")!)")
            arrOfValues.add(getUniqueValueForId())
            
           // arrOfValues.add(Global().getCompareDate())
            
            let dateString = Global().convertDate("\(dictData.value(forKey: "DueDate")!)")
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "MM/dd/yyyy"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            let compareDate = Global().getOnlyDate(dateFormatter.date(from: dateString!))
            
            arrOfValues.add(compareDate!)
            
            if(isSelectedLeadByAccountNo == true)
            {
                if(strLeadIdByAccountNo.count > 0)
                {
                 arrOfValues.add(txtfldSearchByAccount.text!)//account no
                    arrOfValues.add(strLeadNumbr)//lead no
                    
                }
                else if (strAccountIdByAccountNo.count > 0){

                    arrOfValues.add(txtfldSearchByAccount.text!)
                    arrOfValues.add("")
                    
                } else {

                    arrOfValues.add("\(dictData.value(forKey: "AccountNo")!)")
                    arrOfValues.add("\(dictData.value(forKey: "LeadNumber")!)")
                }
            }
            else
            {
                arrOfValues.add("\(dictData.value(forKey: "AccountNo")!)")
                arrOfValues.add("\(dictData.value(forKey: "LeadNumber")!)")
            }
            
            saveDataInDB(strEntity: "TaskListCRMNew", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
        else
        {
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add("")
            arrOfValues.add("")
            arrOfValues.add("")
            arrOfValues.add("")
            arrOfValues.add("\(strSalesPersonId)")
            arrOfValues.add("")
            arrOfValues.add("")
            arrOfValues.add("")
            
            if (strLogTypeIdd.count > 0) {
                
                arrOfSubActivty.removeAllObjects()
                var keys1 = NSArray()
                var object1 = NSArray()
                
                keys1 = ["Agenda","LogTypeId","ActivityId","ParentTaskId","CreatedDate"]
                
                object1 = ["\(txtfldActivity.text ?? "")", strLogTypeIdd, "", "",Global().strCurrentDateFormatted("MM/dd/yyyy HH:mm", "")]
                
                let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
                
                arrOfSubActivty.add(dict_ToSend1)
            }
            
            if(arrOfSubActivty.count > 0)
            {
                arrOfValues.add(arrOfSubActivty)
            }
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            arrOfValues.add("")
            arrOfValues.add(Global().getModifiedDate()) // created date
            arrOfValues.add(Global().getModifiedDate()) // modified date
            
            arrOfValues.add("") // company name
            arrOfValues.add("") // country id
            
            arrOfValues.add("\(strCreatedBy)")
            
            arrOfValues.add("")// created date
            
            let strFullDateTime = (btnDueDate.titleLabel?.text)! + " " + (getTimeWithouAMPM(strTime: (btnDueTime.titleLabel?.text)!))
            
            arrOfValues.add(strFullDateTime)
            arrOfValues.add((btnDueTime.titleLabel?.text)!)
            
            arrOfValues.add("") // first name
            arrOfValues.add("") //FollowUpFromTaskId
            arrOfValues.add("") // last name
            arrOfValues.add("") // LeadNo
            arrOfValues.add("") // LeadTaskId
            arrOfValues.add("") // MiddleName
            arrOfValues.add("") // ModifiedBy
            arrOfValues.add("") // ModifiedDate
            
            arrOfValues.add("") // PrimaryEmail
            arrOfValues.add("") // PrimaryPhone
            arrOfValues.add("") // PrimaryPhoneExt
            arrOfValues.add(strPriorityId)
            arrOfValues.add(btnUrgency.titleLabel!.text!)
            
            arrOfValues.add(strRefIdd)
            arrOfValues.add(strRefTypee)
            
            let strFullDateTimeReminder =  (btnReminderDate.titleLabel?.text!)! + " " + (getTimeWithouAMPM(strTime: (btnReminderTime.titleLabel?.text)!))
            
            arrOfValues.add(strFullDateTimeReminder)
            
            arrOfValues.add("") //SecondaryEmail
            arrOfValues.add("") // SecondaryPhone
            arrOfValues.add("") // SecondaryPhoneExt
            arrOfValues.add("") //StateId
            arrOfValues.add(strGlobalStatusToSend)//Status
            arrOfValues.add(txtfldTags.text ?? "")//Tags
            if(txtviewDescription.text.count > 0)
            {
                arrOfValues.add(txtviewDescription.text)
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add(txtfldTaskName.text!)
            arrOfValues.add("")//WebLeadNo
            arrOfValues.add("")//Zipcode
            arrOfValues.add(getUniqueValueForId())
           
         //    arrOfValues.add(Global().getCompareDate())
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            let date = dateFormatter.date(from: (btnDueDate.titleLabel?.text!)!)
            
            let duedate = Global().getOnlyDate(date)
           
            arrOfValues.add(duedate!)// compare date
            
            saveDataInDB(strEntity: "TaskListCRMNew", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        let alertCOntroller = UIAlertController(title: "Message", message: "Task Added Successfully", preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            var isControllerFound = false
            for item in (self.navigationController?.viewControllers)!
            {
                if(item is TaskVC_CRMNew_iPhone)
                {
                    isControllerFound = true
                    self.navigationController?.popToViewController(item, animated: false)
                    break
                }
                
            }
            if(isControllerFound == false)
            {
                self.navigationController?.popViewController(animated: false)
            }
            
        })
        alertCOntroller.addAction(alertAction)
        self.present(alertCOntroller, animated: true, completion: nil)
    }
    
    fileprivate func updateTaskInLocalDBInNewTable(item:NSDictionary)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        let dictData = removeNullFromDict(dict: (item).mutableCopy() as! NSMutableDictionary)
        
        let arrayAllObject = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "leadTaskId == %@", "\(dictTask.value(forKey: "leadTaskId")!)"))
        
        if (arrayAllObject.count==0)
        {
            saveTaskToLocalDBInNewTable(item: dictData as NSDictionary)
        }
        else
        {
            
            arrOfKeys.add("compareDate")
            arrOfKeys.add("userName")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("assignedTo")
            arrOfKeys.add("childActivities")
            arrOfKeys.add("dueDate")
            arrOfKeys.add("dueTime")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("priority")
            arrOfKeys.add("priorityStr")
            arrOfKeys.add("refId")
            arrOfKeys.add("refType")
            arrOfKeys.add("reminderDate")
            arrOfKeys.add("tags")
            arrOfKeys.add("taskDescription")
            arrOfKeys.add("taskName")
            
            // values
            
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "MM/dd/yyyy"
            dateFormater.timeZone = TimeZone(abbreviation: "UTC")
            let date = dateFormater.date(from: ((btnDueDate.titleLabel?.text)!))
            
            arrOfValues.add(date!)
            
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add("\(strSalesPersonId)")
            
            if (strLogTypeIdd.count > 0) {
                
                arrOfSubActivty.removeAllObjects()
                var keys1 = NSArray()
                var object1 = NSArray()
                
                keys1 = ["Agenda","LogTypeId","ActivityId","ParentTaskId","CreatedDate"]
                
                object1 = ["\(txtfldActivity.text ?? "")", strLogTypeIdd, "", "", Global().strCurrentDateFormatted("MM/dd/yyyy", "")]
                
                let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
                
                arrOfSubActivty.add(dict_ToSend1)
            }
            
            if(arrOfSubActivty.count > 0)
            {
                arrOfValues.add(arrOfSubActivty)
            }
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            let strFullDateTime = (btnDueDate.titleLabel?.text)! + " " + (getTimeWithouAMPM(strTime: (btnDueTime.titleLabel?.text)!))
            
            arrOfValues.add(strFullDateTime)
            arrOfValues.add((btnDueTime.titleLabel?.text)!)
            
            arrOfValues.add(strEmpID) //ModifiedBy
            arrOfValues.add(Global().modifyDate()) //ModifiedDate
            
            arrOfValues.add(strPriorityId)
            arrOfValues.add(btnUrgency.titleLabel!.text!)
            
            arrOfValues.add(strRefIdd)
            arrOfValues.add(strRefTypee)
            
            let strFullDateTimeReminder =  (btnReminderDate.titleLabel?.text!)! + " " + (getTimeWithouAMPM(strTime: (btnReminderTime.titleLabel?.text)!))
            arrOfValues.add(strFullDateTimeReminder)
            
            
            arrOfValues.add(txtfldTags.text ?? "")
            
            if(txtviewDescription.text.count > 0)
            {
                arrOfValues.add(txtviewDescription.text)
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add(txtfldTaskName.text!)
            
            var isSuccess = false
            
            if("\(dictTask.value(forKey: "leadTaskId")!)".count > 0)
            {
                isSuccess =  getDataFromDbToUpdate(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "leadTaskId == %@", "\(dictTask.value(forKey: "leadTaskId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            else
            {
                isSuccess =  getDataFromDbToUpdate(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "uniqueId == %@", "\(dictTask.value(forKey: "uniqueId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            
            
            if(isSuccess)
            {
                let alertCOntroller = UIAlertController(title: "Message", message: "Task Updated Successfully", preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    
                    var isControllerFound = false
                    for item in (self.navigationController?.viewControllers)!
                    {
                        if(item is TaskVC_CRMNew_iPhone)
                        {
                            isControllerFound = true
                            self.navigationController?.popToViewController(item, animated: false)
                            break
                        }
                        
                    }
                    if(isControllerFound == false)
                    {
                        self.navigationController?.popViewController(animated: false)
                    }
                })
                
                alertCOntroller.addAction(alertAction)
                self.present(alertCOntroller, animated: true, completion: nil)
                
            }
        }
    }
    
    /*  fileprivate func updateTaskInLocalDBInOldTable()
     {
     let arrOfKeys = NSMutableArray()
     let arrOfValues = NSMutableArray()
     
     let arrayAllObject = getDataFromLocal(strEntity: "TaskList", predicate: NSPredicate(format: "typeOfTask == %@", "\(strTypeOfTaskToFetch)"))
     
     var arrOfTaskList = (arrayAllObject.value(forKey: "arrOfTask") as! NSArray).mutableCopy() as! NSMutableArray
     
     print(arrayAllObject)
     if(arrayAllObject.count == 0)
     {
     saveTaskToLocalDBInOldTable()
     }
     else
     {
     var strLeadTaskId = "", strRefTypee = "",strRefIdd = ""
     
     if(strFromVC == "WebLeadVC")
     {
     strLeadTaskId = ""
     strRefTypee = "WebLead"
     strRefIdd = strleadIdToUpdate
     }
     else  if(strFromVC == "DashBoardView")
     {
     strLeadTaskId = ""
     strRefTypee = "Employee"
     strRefIdd = strEmpID
     }
     
     else  if(strFromVC == "OpportunityVC")
     {
     strRefTypee = "Lead"
     }
     
     if (strLogTypeIdd.count > 0) {
     
     var object1 = NSArray()
     var keys1 = NSArray()
     object1 = ["\(txtfldActivity.text ?? "")", strLogTypeIdd]
     keys1 = ["Agenda","LogTypeId"]
     
     let dict_ToSend1 = NSDictionary.init(object: object1, forKey: keys1)
     
     arrOfSubActivty.add(dict_ToSend1)
     }
     
     let strFullDateTime = (btnDueDate.titleLabel?.text)! + " " + (getTimeWithouAMPM(strTime: (btnDueTime.titleLabel?.text)!))
     
     
     var strFullDateTimeReminder =  (btnReminderDate.titleLabel?.text!)! + " " +
     (getTimeWithouAMPM(strTime: (btnReminderTime.titleLabel?.text)!))
     
     if (btnReminderDate.titleLabel?.text == ""){
     strFullDateTimeReminder = ""
     }
     
     arrOfKeys.add("Id")
     arrOfKeys.add("LeadTaskId")
     arrOfKeys.add("RefId")
     arrOfKeys.add("RefType")
     arrOfKeys.add("TaskName")
     arrOfKeys.add("DueDate")
     arrOfKeys.add("ReminderDate")
     arrOfKeys.add("Description")
     arrOfKeys.add("AssignedTo")
     arrOfKeys.add("Priority")
     arrOfKeys.add("Status")
     arrOfKeys.add("CreatedBy")
     arrOfKeys.add("Tags")
     arrOfKeys.add("ChildActivities")
     
     // values
     
     arrOfValues.add("1")
     arrOfValues.add(strLeadTaskId)
     arrOfValues.add(strRefIdd)
     arrOfValues.add(strRefTypee)
     arrOfValues.add(txtfldTaskName.text!)
     arrOfValues.add(strFullDateTime)
     arrOfValues.add(strFullDateTimeReminder)
     arrOfValues.add("\(txtviewDescription.text.count == 0 ? "" :txtviewDescription.text!)")
     arrOfValues.add(strSalesPersonId)
     arrOfValues.add(strPriorityId)
     arrOfValues.add(strGlobalStatusToSend)
     arrOfValues.add(strCreatedBy)
     arrOfValues.add(txtfldTags.text?.count == 0 ? "" : txtfldTags.text!)
     arrOfValues.add(arrOfSubActivty)
     
     
     let dict_ToSend = NSDictionary.init(objects: arrOfValues as! [Any], forKeys: arrOfKeys as! [NSCopying])
     
     let arrOfTaskListToSave = NSMutableArray()
     
     arrOfTaskListToSave.add(dict_ToSend)
     
     let dictLogin = nsud.value(forKey: "LoginDetails") as! NSDictionary
     let strHrmsCompanyId = "\(dictLogin.value(forKeyPath: "Company.HrmsCompanyId")!)"
     let strEmployeeNumber = "\(dictLogin.value(forKeyPath: "EmployeeNumber")!)"
     let strEmployeeId = "\(dictLogin.value(forKeyPath: "EmployeeId")!)"
     let strCreatedByyy = "\(dictLogin.value(forKeyPath: "CreatedBy")!)"
     let strEmployeeName = "\(dictLogin.value(forKeyPath: "EmployeeName")!)"
     let strCoreCompanyId = "\(dictLogin.value(forKeyPath: "Company.CoreCompanyId")!)"
     let strSalesProcessCompanyId = "\(dictLogin.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
     let strCompanyKeyyy = "\(dictLogin.value(forKeyPath: "Company.CompanyKey")!)"
     
     
     let arrOfSubKeys = NSMutableArray()
     let arrOfSubValues = NSMutableArray()
     
     arrOfSubKeys.add("HrmsCompanyId")
     arrOfSubKeys.add("EmployeeNumber")
     arrOfSubKeys.add("EmployeeId")
     arrOfSubKeys.add("CreatedBy")
     arrOfSubKeys.add("EmployeeName")
     arrOfSubKeys.add("CoreCompanyId")
     arrOfSubKeys.add("SalesProcessCompanyId")
     arrOfSubKeys.add("CompanyKey")
     arrOfSubKeys.add("strServiceUrlMain")
     
     arrOfSubValues.add(strHrmsCompanyId)
     arrOfSubValues.add(strEmployeeNumber)
     arrOfSubValues.add(strEmployeeId)
     arrOfSubValues.add(strCreatedByyy)
     arrOfSubValues.add(strEmployeeName)
     arrOfSubValues.add(strCoreCompanyId)
     arrOfSubValues.add(strSalesProcessCompanyId)
     arrOfSubValues.add(strCompanyKeyyy)
     arrOfSubValues.add(strServiceUrlMain)
     
     let dictDataAllValues = NSDictionary.init(objects: arrOfSubValues as! [Any], forKeys: arrOfSubKeys as! [NSCopying])
     
     
     let arrOfTableKeys = NSMutableArray()
     let arrOfTableValues = NSMutableArray()
     
     arrOfTableKeys.add("arrOfTask")
     arrOfTableKeys.add("userName")
     arrOfTableKeys.add("companyKey")
     arrOfTableKeys.add("typeOfTask")
     
     arrOfTableKeys.add("dictData")
     
     arrOfTableValues.add(arrOfTaskListToSave)
     arrOfTableValues.add(strUserName)
     arrOfTableValues.add(strCompanyKey)
     arrOfTableValues.add(strTypeOfTaskToFetch)
     arrOfTableValues.add(dictDataAllValues)
     
     let isSuccess =  getDataFromDbToUpdate(strEntity: "TaskList", predicate: NSPredicate(format: "typeOfTask == %@", strTypeOfTaskToFetch), arrayOfKey: arrOfTableKeys, arrayOfValue: arrOfTableValues)
     
     
     if(isSuccess)
     {
     print("successfully updated")
     }
     }
     }*/
    
    fileprivate func getTimeWithouAMPM(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    /*fileprivate func updatePaymentInfo()
     {
     // Update Payment Info
     
     
     let arrayAllObject = getDataFromLocal(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strLeadId))
     
     if (arrayAllObject.count==0)
     {
     savePaymentInfo()
     }
     else
     {
     let arrOfKeys = NSMutableArray()
     let arrOfValues = NSMutableArray()
     
     arrOfKeys.add("companyKey")
     arrOfKeys.add("userName")
     arrOfKeys.add("modifiedBy")
     arrOfKeys.add("modifiedDate")
     arrOfKeys.add("specialInstructions")
     
     
     arrOfValues.add(strCompanyKey)
     arrOfValues.add(strUserName)
     arrOfValues.add(strEmpID)
     arrOfValues.add(global.modifyDate())
     arrOfValues.add(txtviewAdditionalNotes.text)
     
     
     let isSuccess =  getDataFromDbToUpdate(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
     
     if isSuccess
     {
     
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
     }
     }
     }*/
    
    
}

extension AddTaskVC_CRMNew_iPhone: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 301)
        {
            btnTaskTemplate.setTitle("\(dictData.value(forKey: "TaskName")!)", for: .normal)
            dictSelectedTaskTemplate = dictData
            txtfldTaskName.text = "\(dictData.value(forKey: "TaskName")!)"
            txtviewDescription.text = "\(dictData.value(forKey: "Description")!)"
            
        }
        if(tag == 306)
        {
            btnAssignedTo.setTitle("\(dictData.value(forKey: "FullName")!)", for: .normal)
            dictSelectedAssignedTo = dictData
            strSalesPersonId = "\(dictData.value(forKey: "EmployeeId")!)"
        }
        if(tag == 307)
        {
            btnUrgency.setTitle("\(dictData.value(forKey: "Name")!)", for: .normal)
            strPriorityId = "\(dictData.value(forKey: "PriorityId")!)"
        }
        if(tag == 308)
        {
            btnActivityLogType.setTitle("\(dictData.value(forKey: "Name")!)", for: .normal)
            strLogTypeIdd = "\(dictData.value(forKey: "LogTypeId")!)"
        }
        if(tag == 400)
        {
            strLeadIdByAccountNo = "\(dictData.value(forKey: "LeadId")!)"
            strLeadNumbr = "\(dictData.value(forKey: "LeadNumber")!)"
            isSelectedLeadByAccountNo = true
            
            if(strFromVC == "OpportunityVC")
            {
                btnAccNo.setTitle("Account #:\(txtfldSearchByAccount.text!)", for: .normal)
                btnOppNo.setTitle("Opportunity #: \(dictData.value(forKey: "LeadNumber")!)", for: .normal)
                
            }
            
            if(strFromVC == "WebLeadVC")
            {
                btnAccNo.setTitle("Account #:\(txtfldSearchByAccount.text!)", for: .normal)
                
                btnOppNo.setTitle("Lead #: \(dictData.value(forKey: "LeadNumber")!)", for: .normal)
                
            }
            else
            {
                btnAccNo.setTitle("Account #:\(txtfldSearchByAccount.text!)", for: .normal)
                
                btnOppNo.setTitle("Opportunity #: \(dictData.value(forKey: "LeadNumber")!)", for: .normal)
                
            }
        }
    }
    
}
extension AddTaskVC_CRMNew_iPhone: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        
        if(tag == 302)
        {
            btnDueDate.setTitle(strDate, for: .normal)
        }
        if(tag == 303)
        {
            btnDueTime.setTitle(strDate, for: .normal)
        }
        if(tag == 304)
        {
            btnReminderDate.setTitle(strDate, for: .normal)
        }
        if(tag == 305)
        {
            btnReminderTime.setTitle(strDate, for: .normal)
        }
    }
}

extension AddTaskVC_CRMNew_iPhone: UITextFieldDelegate, UITextViewDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtfldSearchByAccount)
        {
            if(string == " ")
            {
                return false
            }
        }
        if(textField == txtfldTaskName )
        {
            if(string == " " && txtfldTaskName.text?.count == 0)
            {
                return false
            }
            return true
        }
        
        if(textField == txtfldTags)
        {
            if(string == " " && txtfldTags.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldActivity)
        {
            if(string == " " && txtfldActivity.text?.count == 0)
            {
                return false
            }
            return true
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
