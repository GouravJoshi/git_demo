//
//  ScheduleNowVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Saavan Patidar on 08/04/20.
//  Copyright © 2020 Saavan. All rights reserved.
//ScheduleNowVC_CRMNew_iPhone_CRMNew_iPhone

import UIKit

class ScheduleNowVC_CRMNew_iPhone: UIViewController {
    
    // MARK: - ----------------------------------- Outlets Variables -----------------------------------
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblAccNo: UILabel!
    @IBOutlet weak var lblPrimaryService: UILabel!
    @IBOutlet weak var txtFldAddress: ACFloatingTextField!
    @IBOutlet weak var btnGoForAddress: UIButton!
    @IBOutlet weak var btnAssignedTo: UIButton!
    
    
    @IBOutlet weak var txtFldAssignTo: ACFloatingTextField!
    @IBOutlet weak var txtFldDate: ACFloatingTextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    
   
    @IBOutlet weak var tf_Date: ACFloatingTextField!
    @IBOutlet weak var tf_Time: ACFloatingTextField!
    @IBOutlet weak var tf_Range: ACFloatingTextField!
    @IBOutlet weak var height_tf_Range: NSLayoutConstraint!
    @IBOutlet weak var btn_Specific: UIButton!
    @IBOutlet weak var btn_TimeRange: UIButton!
    @IBOutlet weak var btnThirdParty: UIButton!

    // MARK: - ----------- Google Address Code ----------------
    
    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    private var placeType: PlaceType = .all
    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var radius: Double = 0.0
    private var strictBounds: Bool = false
    var txtAddressMaxY = CGFloat()
    var imgPoweredBy = UIImageView()
    var strAccountId = ""
    var arrAddress = NSArray()
    var strStateId = ""
    var strCityName = ""
    var strAddressLine1 = ""
    var strZipCode = ""
    var loader = UIAlertController()
    var aryTimeRange = NSArray() ,arySelectedRange = NSMutableArray()

    
    private var places = [Place]() {
        didSet { tableView.reloadData() }
    }
    
    
    
    // MARK: - ----------------------------------- Global Variables -----------------------------------

    var arrEmployeeList = NSMutableArray()
    var dictLoginData = NSDictionary()
    var dictLeadData = NSDictionary()
    var strAccounName = ""
    var strPrimaryServiceId = ""
    var strAssignedToId = ""
    var strCustomerAddressId = ""

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.txtFldAddress.isUserInteractionEnabled = true

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        
        strAccounName = "\(dictLeadData.value(forKey: "AccountContactName") ?? "")"
        strPrimaryServiceId = "\(dictLeadData.value(forKey: "ServiceId") ?? "")"
        strAccountId = "\(dictLeadData.value(forKey: "AccountId") ?? "")"
        //strAccounName = "\(dictLeadData.value(forKey: "AccountContactName") ?? "")"

        if strAccounName.count > 0 {
            
            lblAccNo.text = "Account: \(strAccounName)"

        } else {
            
            lblAccNo.text = ""

        }
        
        let strServiceName = fetchServiceNameViaId(strId: strPrimaryServiceId)
        
        if strServiceName.count > 0 {
            
            lblPrimaryService.text = "Primary service: \(strServiceName)"

        } else {
            
            lblPrimaryService.text = ""

        }
        
        loadGoogleAddress()
        
        getEmployeeList()
        
        strAssignedToId = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
        txtFldAssignTo.text = Global().getEmployeeName(viaEmployeeID: "\(dictLoginData.value(forKeyPath: "EmployeeId")!)")
        
        //if "\(dictLeadData.value(forKey: "OpportunityStage") ?? "")" == "Scheduled" {
            
            if dictLeadData.value(forKey: "ServiceAddress") is NSDictionary {
                
                let dictDataAddressoLocal = dictLeadData.value(forKey: "ServiceAddress") as! NSDictionary
                
                strCustomerAddressId = "\(dictDataAddressoLocal.value(forKey: "CustomerAddressId") ?? "")"
                
                if strCustomerAddressId.count > 0 {
                    
                    self.txtFldAddress.isUserInteractionEnabled = false
                                        
                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(loader, animated: false, completion: nil)

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        
                        self.callApiToGetAddressViaAccountId(strType: "Selected")

                    })
                    
                }
                
                txtFldAddress.text = Global().strCombinedAddress(dictDataAddressoLocal as? [AnyHashable : Any])
                
                if txtFldAddress.text == "N/A" {
                    
                    txtFldAddress.text = ""
                    
                }
                
                strAssignedToId = "\(dictLeadData.value(forKey: "FieldSalesPersonId") ?? "")"
                txtFldAssignTo.text = "\(dictLeadData.value(forKey: "FieldSalesPerson") ?? "")"
                
                
//                var strDate = Global().latestStringDate(toStringFormatted: "\(dictLeadData.value(forKey: "ScheduleDate") ?? "")", "MM/dd/yyyy", "UTC")
//
//
//                var strTime = Global().changeDate(toLocalActivityTime: "\(dictLeadData.value(forKey: "ScheduleTime") ?? "")")
//
//                if strDate == nil
//                {
//                    strDate = ""
//                }
//                if strTime == nil
//                {
//                    strTime = ""
//                }
//
//                let date = Date()
//                let formatter = DateFormatter()
//                formatter.locale = Locale(identifier: "EST")
//                if (strDate!.count == 0){
//                    formatter.dateFormat = "MM/dd/yyyy"
//                    strDate! = formatter.string(from: date)
//                }
//                if (strTime!.count == 0){
//                    formatter.dateFormat = "hh:mm a"
//                    strTime! = formatter.string(from: date)
//                }
//                if(strDate!.count != 0 && strTime!.count != 0){
//                    txtFldDate.text = strDate! + " " + strTime!
//                }
                
              

            }
            
        
        
        //for Time Renge-----
        if(nsud.value(forKey: "MasterSalesTimeRange") != nil){
            if(nsud.value(forKey: "MasterSalesTimeRange") is NSArray){
                self.aryTimeRange = nsud.value(forKey: "MasterSalesTimeRange") as! NSArray
            }
        }
        height_tf_Range.constant = 0.0
        btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
        
        var rangeofTimeId = "" , scheduleDate = "" ,  scheduleTime = "" , scheduleTimeType = ""
        if(dictLeadData.value(forKey: "ScheduleDate")  != nil){
            scheduleDate = "\(dictLeadData.value(forKey: "ScheduleDate") ?? "")"
        }
        if(dictLeadData.value(forKey: "ScheduleTime")  != nil){
            scheduleTime = "\(dictLeadData.value(forKey: "ScheduleTime") ?? "")"

        }
        if(dictLeadData.value(forKey: "RangeofTimeId")  != nil){
            rangeofTimeId = "\(dictLeadData.value(forKey: "RangeofTimeId") ?? "")"

        }
        if(dictLeadData.value(forKey: "ScheduleTimeType")  != nil){
            scheduleTimeType = "\(dictLeadData.value(forKey: "ScheduleTimeType") ?? "")"
        }
        
        
        //-------------Scheduled TimeRangeId
        if rangeofTimeId != "" && rangeofTimeId != "0"{
            height_tf_Range.constant = DeviceType.IS_IPAD ? 50.0 : 44.0
            btn_Specific.setImage(UIImage(named: "redio_1"), for: .normal)
            btn_TimeRange.setImage(UIImage(named: "redio_2"), for: .normal)
            
            if scheduleDate.count > 0
            {
                tf_Date.text = changeStringDateToGivenFormat(strDate: scheduleDate, strRequiredFormat: "MM/dd/yyyy")
            }
      
            if scheduleTime.count > 0
            {
                tf_Time.text = changeStringDateToGivenFormat(strDate: scheduleTime, strRequiredFormat: "hh:mm a")
            }
            
            let aryTimeRange = self.aryTimeRange.filter { (task) -> Bool in
                return ("\((task as! NSDictionary).value(forKey: "RangeofTimeId")!)".lowercased().contains(rangeofTimeId))
            } as NSArray
            
            if(aryTimeRange.count != 0){
                let dictData = aryTimeRange.object(at: 0) as! NSDictionary
                let StartInterval = "\(dictData.value(forKey: "StartInterval")!)"
                let EndInterval = "\(dictData.value(forKey: "EndInterval")!)"
                let strrenge = StartInterval + " - " + EndInterval
                tf_Range.text = strrenge
                
                if tf_Time.text == ""
                {
                    tf_Time.text = StartInterval
                }

                self.arySelectedRange = NSMutableArray()
                self.arySelectedRange.add(dictData)
                if(rangeofTimeId == ""){
                    rangeofTimeId = "0"
                }
                tf_Range.tag = Int("\(rangeofTimeId)")!

            }
 
        }else{
            
            height_tf_Range.constant = 0.0
            btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
            btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
            
            if scheduleDate.count > 0
            {
                tf_Date.text = changeStringDateToGivenFormat(strDate: scheduleDate, strRequiredFormat: "MM/dd/yyyy")
            }
           
            
            if scheduleTime.count > 0
            {
                tf_Time.text = changeStringDateToGivenFormat(strDate: scheduleTime, strRequiredFormat: "hh:mm a") //"\(dataGeneralInfo.value(forKey: "scheduleTime") ?? "")"//

            }
        }
        
        
        //}
     
        
       
        if strAssignedToId.count == 0 {
            
            strAssignedToId = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            txtFldAssignTo.text = Global().getEmployeeName(viaEmployeeID: "\(dictLoginData.value(forKeyPath: "EmployeeId")!)")
            
        }
        
        //Schedule Third party Button
        if isScheduleThirdParty() {
            btnThirdParty.isHidden = false
        }else{
            btnThirdParty.isHidden = true
        }
        btnThirdParty.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
        if dictLeadData.value(forKey: "CreateBlankScheduleInPP") != nil {
          let createBlankScheduleInPP = "\(dictLeadData.value(forKey: "CreateBlankScheduleInPP") ?? "")"
            if(createBlankScheduleInPP.lowercased() == "true" || createBlankScheduleInPP == "1"){
                btnThirdParty.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        if nsud.bool(forKey: "fromScheduledOpportunity") == true
        {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedCompany_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
            
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    // MARK: - ----------------------------------- Actions -----------------------------------
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func actionOnSave(_ sender: Any) {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if(validationOnScheduleView()){
         
            let countryId = "1"
            
            /*if strCustomerAddressId.count > 0 {
                
                strAddressLine1 = ""
                strCityName = ""
                strZipCode = ""
                strStateId = ""
                strAddressLine1 = ""
                countryId = ""
                
            }*/

//            var str_date = "" , str_time = ""
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MM/dd/yyyy"
//            let date = dateFormatter.date(from: txtFldDate.text!)!
//            dateFormatter.dateFormat = "MM/dd/yyyy"
//            str_date = dateFormatter.string(from: date)
//            dateFormatter.dateFormat = "hh:mm a"
//            str_time = dateFormatter.string(from: date)
            
            var keys = NSArray()
            var values = NSArray()
            
            let rangeOfTimeId = tf_Range.tag == 0 ? "" : "\(tf_Range.tag)"
            let scheduleTimeType = tf_Range.tag == 0 ? "Specific" : "Range"
            keys = ["Address1",
                    "CityName",
                    "Zipcode",
                    "StateId",
                    "CountryId",
                    "FieldSalesPerson",
                    "ScheduleDate",
                    "ScheduleTime",
                    "LeadId",
                    "CustomerAddressId", "RangeOfTimeId" , "ScheduleTimeType" , "CreateBlankScheduleInPP" ]
            
            values = [strAddressLine1,
                      strCityName,
                      strZipCode,
                      strStateId,
                      countryId,
                      strAssignedToId,
                      tf_Date.text ?? "",
                      getTimeWithouAMPM(strTime: tf_Time.text ?? ""),
                      "\(dictLeadData.value(forKey: "OpportunityId") ?? "")",
                      strCustomerAddressId , rangeOfTimeId ,scheduleTimeType , btnThirdParty.currentImage == UIImage(named: "check_box_2New.png") ? "true" : "false"]
            
            let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
            
            // Call Api To Schedule Now
            
            if (isInternetAvailable()){
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    self.callApiToScheduleNow(dictOfDataa: dictToSend)

                })

            }else{
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                
            }
                        
        }
        
    }
    
    @IBAction func actionOnCancelAddress(_ sender: Any) {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.view.endEditing(true)
        txtFldAddress.text = ""
        strCustomerAddressId = ""
        self.txtFldAddress.isUserInteractionEnabled = true

    }
    @IBAction func actionOnGoForAddress(_ sender: Any) {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if strAccountId.count > 0 {
            
            if self.arrAddress.count > 0 {
                
                //sender.tag = 63
                self.openTableViewPopUp(tag: 63, ary: (self.arrAddress as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
                
            }else{
             
                if (isInternetAvailable()){
                    
                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(loader, animated: false, completion: nil)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        
                        self.callApiToGetAddressViaAccountId(strType: "sadfasdf")

                    })

                }else{
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                    
                }
                
            }
            
        }else{
         
            showAlertWithoutAnyAction(strtitle: Info, strMessage: "Please select account to get address", viewcontrol: self)
            
        }
        
    }
    
    @IBAction func actionOnAssignTo(_ sender: Any)
    {
    
           self.imgPoweredBy.removeFromSuperview()
           self.tableView.removeFromSuperview()
    
           let isFieldPersonAssign = nsud.bool(forKey: "isFieldPersonAssign")

           if isFieldPersonAssign {
               
               if arrEmployeeList.count > 0
               {
                   self.view.endEditing(true)

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {

                        self.view.endEditing(true)

                    })
                   
                   btnAssignedTo.tag = 500
                   gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrEmployeeList ) , strTitle: "")
               }
               else
               {
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
               }
               
           } else {
               
               showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Not authorized to change", viewcontrol: self)
               
           }
           
       }
    
    
    @IBAction func action_OnDate(_ sender: UIButton) {
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Date.text?.count)! > 0 ? tf_Date.text! : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.date = fromDate
        datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select Schedule Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Date.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
      
    }
    @IBAction func action_OnTime(_ sender: UIButton) {
        self.view.endEditing(true)
   
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((tf_Time.text?.count)! > 0 ? tf_Time.text! : result))!
        
        let datePicker = UIDatePicker()
       // datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.time
        datePicker.date = fromDate
      //  datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)

        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: fromDate, Picker: datePicker, view: self.view ,strTitle : "Select Schedule Time")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            tf_Time.text = formatter.string(from: datePicker.date)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func action_OnSelectRange(_ sender: Any) {
        self.view.endEditing(true)
        if(aryTimeRange.count != 0){
            goToNewSalesSelectionVC(arrItem: aryTimeRange.mutableCopy()as! NSMutableArray, arrSelectedItem: self.arySelectedRange, tag: 1, titleHeader: "Time Range", isMultiSelection: false, ShowNameKey: "StartInterval,EndInterval")
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
        }
    }

    @IBAction func action_OnSpecific(_ sender: Any) {
        height_tf_Range.constant = 0.0
        btn_Specific.setImage(UIImage(named: "redio_2"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_1"), for: .normal)
        arySelectedRange = NSMutableArray()
        tf_Range.text = ""
        tf_Range.tag = 0
    }
    @IBAction func action_OnTimeRange(_ sender: Any) {
        height_tf_Range.constant = DeviceType.IS_IPAD ? 50.0 : 44.0
        btn_Specific.setImage(UIImage(named: "redio_1"), for: .normal)
        btn_TimeRange.setImage(UIImage(named: "redio_2"), for: .normal)

    }
    

    
    
    
    
    @IBAction func actionOnDate(_ sender: Any)
    {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.view.endEditing(true)

         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {

             self.view.endEditing(true)

         })
        
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        let fromDate = dateFormatter.date(from:((txtFldDate.text!.count) > 0 ? txtFldDate.text! : result))!
        
        
        gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime", tag: 10, dateToSet: fromDate)
    }
    
    @IBAction func actionOnTime(_ sender: Any)
    {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.view.endEditing(true)

         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {

             self.view.endEditing(true)

         })
      //  gotoDatePickerView(sender: sender as! UIButton, strType: "Time", tag: 11)
    }
       @IBAction func action_AddAddress(_ sender: UIButton) {
           
         let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
         let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
         vc.tag = 1
         vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
         vc.modalTransitionStyle = .coverVertical
         vc.delegate = self
         self.present(vc, animated: false, completion: {})
         
     }
    
    @IBAction func action_OnThirdParty(_ sender: UIButton) {
        self.view.endEditing(true)
        if sender.currentImage == UIImage(named: "check_box_1New.png") {
            btnThirdParty.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
        }else{
            btnThirdParty.setImage(UIImage(named: "check_box_1New.png"), for: .normal)

        }
    }
    
    // MARK: -----------------------------------Validation----------------------------------------

    func validationOnScheduleView() -> Bool {

        if(txtFldAddress.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address.", viewcontrol: self)
            return false
            
        }
        if(txtFldAssignTo.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select assign to.", viewcontrol: self)
            return false
            
        }
        else if(tf_Date.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select schedule date.", viewcontrol: self)
            return false
            
        }
        else if(tf_Time.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select schedule time.", viewcontrol: self)
            return false
            
        }
        else if (btn_TimeRange.currentImage == UIImage(named: "redio_2") && tf_Range.text == "") {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select time range to schedule .", viewcontrol: self)
            return false
            }
//        else if(txtFldTime.text == "")
//        {
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select schedule time.", viewcontrol: self)
//            return false
//
//        }
        
        if (txtFldAddress.text!.count > 0) {

            let dictOfEnteredAddress = formatEnteredAddress(value: txtFldAddress.text!)
            
            if dictOfEnteredAddress.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                
                return false
                
            } else {
                
                
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    
                    strStateId = ""

                }
                
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address 1.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strCityName.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid city.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strStateId.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid state name.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count != 5 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                
            }
                        
        }
        
        /*if ((txtFldAddress.text!.count > 0) && (strCustomerAddressId.count <= 0)) {
            
                // Fetch Address
                
            let arrOfAddress = self.getAddress(from: txtFldAddress.text!)
            
            let tempArrayOfKeys = NSMutableArray()
            let tempDictData = NSMutableDictionary()

            for k in 0 ..< arrOfAddress.count {
             
                      if arrOfAddress[k] is NSDictionary {
                
                         let tempDict = arrOfAddress[k] as! NSDictionary
                    
                         tempArrayOfKeys.addObjects(from: tempDict.allKeys)
                         tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])

                      }
                
            }
            
            if tempArrayOfKeys.contains("City") {
                
                strCityName = "\(tempDictData.value(forKey: "City")!)"

            }
            if tempArrayOfKeys.contains("ZIP") {
                
                strZipCode = "\(tempDictData.value(forKey: "ZIP")!)"

            }
            if tempArrayOfKeys.contains("State") {
                
                let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(tempDictData.value(forKey: "State")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    //btnState.setTitle("\(dictStateDataTemp.value(forKey: "Name")!)", for: .normal)
                    
                }else{
                    
                    strStateId = ""
                    //btnState.setTitle("\(tempDictData.value(forKey: "State")!)", for: .normal)

                }

            }
            if tempArrayOfKeys.contains("Street") {
                
                strAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"

            }
            
            if strAddressLine1.count == 0 || strCityName.count == 0 || strZipCode.count == 0 || strStateId.count == 0 {
                
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter proper address (address, city, zipcode, state).", viewcontrol: self)
                
                return false
                
            }
            
        }*/
        
        return true
        
    }
    
    
    // MARK: -----------------------------APi Calling Functions---------------------------

    func callApiToScheduleNow(dictOfDataa : NSDictionary) {
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictOfDataa) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictOfDataa, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlScheduleNowByLeadId
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "resendMailService"
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            self.loader.dismiss(animated: false) {

                if(success)
                {
                    
                    let strResponse = "\((response as NSDictionary?)!.value(forKey: "ReturnMsg")!)"
                    
                    if strResponse == "true" {
                        
                        let alertCOntroller = UIAlertController(title: Info, message: "Scheduled Successfully.", preferredStyle: .alert)
                        
                        
                        let alertAppointment = UIAlertAction(title: "Go to Appointments", style: .default, handler: { (action) in
                                    
                            nsud.set(true, forKey: "fromScheduledOpportunity")
                            nsud.synchronize()
                            self.goToAppointment()
                            
                        })
                        
                        let alertCancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                                       
                            nsud.set(true, forKey: "fromScheduledOpportunity")
                            nsud.synchronize()
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedCompany_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])

                            self.navigationController?.popViewController(animated: false)
                            
                        })
                        
                        alertCOntroller.addAction(alertCancelAction)
                        alertCOntroller.addAction(alertAppointment)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    }
                    else {
                        
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }
                    
                }else {
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                    
                }
            }
            
        }
        
    }
    
    func callApiToGetAddressViaAccountId(strType : String) {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetServiceAddressesByAccountId + strAccountId
                    
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {

                if(Status)
                {
                    
                    let arrKeys = Response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        self.arrAddress = Response.value(forKey: "data") as! NSArray
                        
                        if self.arrAddress.count > 0 {
                            
                            if strType == "Selected" {
                                
                                for k in 0 ..< self.arrAddress.count {
                                    
                                    let dictData = self.arrAddress[k] as! NSDictionary
                                    
                                    if "\(dictData.value(forKey: "CustomerAddressId") ?? "")" == self.strCustomerAddressId {
                                        
                                        self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                                        self.strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                                        self.txtFldAddress.isUserInteractionEnabled = false
                                        
                                        if self.txtFldAddress.text == "N/A" {
                                            
                                            self.txtFldAddress.text = ""
                                            
                                        }
                                        
                                        break
                                        
                                    }
                                    
                                }
                                
                            }else{
                             
                                //sender.tag = 63
                                self.openTableViewPopUp(tag: 63, ary: (self.arrAddress as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
                                
                            }
                            
                        }else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }

                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
        }
        
    }
    // MARK: - ----------------------------------- Local Functions -----------------------------------
    func getTimeWithouAMPM(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    func fetchServiceNameViaId(strId : String) -> String {
        
        var serviceName = ""
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        for k in 0 ..< arrOfData.count {
            
            if arrOfData[k] is NSDictionary {
                
                let dictOfData = arrOfData[k] as! NSDictionary
                
                if "\(dictOfData.value(forKey: "ServiceId")!)" == strId {
                    
                    serviceName = "\(dictOfData.value(forKey: "Name")!)"
                    break
                }
                
            }
            
        }
        
        return serviceName
        
    }
    
    func getAddress(from dataString: String) ->  NSMutableArray {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
        let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
        
        let resultsArray = NSMutableArray()
        // put matches into array of Strings
        for match in matches {
            if match.resultType == .address,
                let components = match.addressComponents {
                resultsArray.add(components)
            } else {
                print("no components found")
            }
        }
        return resultsArray
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String, tag: Int , dateToSet:Date)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.dateToSet = dateToSet
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func getEmployeeList()
    {
        arrEmployeeList = NSMutableArray()
       if nsud.value(forKey: "EmployeeList") is NSArray
       {
        var arrEmployee = NSArray()
        arrEmployee = nsud.value(forKey: "EmployeeList") as! NSArray
        
        for item in arrEmployee
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "IsActive") ?? "")" == "1"
            {
                arrEmployeeList.add(dict)
            }
            
        }
        
        }

    }
    func loadGoogleAddress()
    {
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        txtAddressMaxY = (txtFldAddress.frame.origin.y) - 35
        imgPoweredBy.image = UIImage(named: "powered-by-google-on-white")
        imgPoweredBy.contentMode = .center
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)

            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    
    func goToNewSalesSelectionVC(arrItem : NSMutableArray,arrSelectedItem : NSMutableArray, tag : Int, titleHeader : String , isMultiSelection : Bool, ShowNameKey : String) {
        if(arrItem.count != 0){
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SelectionVC") as! SalesNew_SelectionVC
            vc.aryItems = arrItem
            vc.arySelectedItems = arrSelectedItem
            vc.isMultiSelection = isMultiSelection
            vc.strTitle = titleHeader
            vc.strTag = tag
            vc.strShowNameKey = ShowNameKey
            vc.delegate = self
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func goToAppointment()
    {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
              if(DeviceType.IS_IPAD){
                
                let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                
                if strAppointmentFlow == "New" {
                    
                    let mainStoryboard = UIStoryboard(
                        name: "Appointment_iPAD",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                } else {
                    
                    let mainStoryboard = UIStoryboard(
                        name: "MainiPad",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                }
                     
            }else{
                
                let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                
                if strAppointmentFlow == "New" {
                    
                    let mainStoryboard = UIStoryboard(
                        name: "Appointment",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                    self.navigationController?.pushViewController(objByProductVC!, animated: false)
                    
                } else {
                    
                         let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
                         self.navigationController?.pushViewController(controller, animated: false)
                    
                }
                     }
        
    }

    
}


// MARK: - ----------------------------------- Extensions -----------------------------------

// MARK: --------------------- DatePicker Delegate --------------

extension ScheduleNowVC_CRMNew_iPhone: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if tag == 10
        {
            
            txtFldDate.text = strDate
        }
        else if tag == 11
        {
            
          //  txtFldTime.text = strDate
        }
    }
    
    
}
// MARK: --------------------- Tableview Delegate --------------

extension ScheduleNowVC_CRMNew_iPhone : CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int)
    {
        if tag == 500
        {
            //btnSelectEmployee.setTitle("\(dictData.value(forKey: "FullName") ?? "")", for: .normal)
            txtFldAssignTo.text = "\(dictData.value(forKey: "FullName") ?? "")"
            strAssignedToId = "\(dictData.value(forKey: "EmployeeId") ?? "")"
            
        }
        else if(tag == 63)
        {
            if(dictData.count == 0){
                
               txtFldAddress.text = ""
               strCustomerAddressId = ""
               self.txtFldAddress.isUserInteractionEnabled = true
                
            }else{
                
                txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                self.txtFldAddress.isUserInteractionEnabled = false
                
                if txtFldAddress.text == "N/A" {
                    
                    txtFldAddress.text = ""
                    
                }

            }
            // Address
        }
       
        
    }
    
}
// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------

extension ScheduleNowVC_CRMNew_iPhone : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if(textField == txtFldAddress){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()

                    self.tableView.removeFromSuperview()
                    return true
                    
                    //   print("Backspace was pressed")
                }
            }
            var txtAfterUpdate:NSString = txtFldAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.imgPoweredBy.removeFromSuperview()

                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtFldAddress){
            let txtAfterUpdate:NSString = txtFldAddress.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
            
            self.getPlaces(with: parameters) {
                self.places = $0
            }
            return true
        }
        return true
    }
    
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
    
}
extension ScheduleNowVC_CRMNew_iPhone {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                }
                else
                {
                   /* for item in self.scrollView.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)*/
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    
                   // self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    self.tableView.frame = CGRect(x: 0, y: self.txtFldAddress.frame.maxY, width: self.txtFldAssignTo.frame.width + 20, height: DeviceType.IS_IPHONE_6 ? 200 : 250)

                    
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.view.addSubview(self.tableView)
                    self.view.addSubview(self.imgPoweredBy)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension ScheduleNowVC_CRMNew_iPhone : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            self.txtFldAddress.text = value.formattedAddress
            self.txtFldAddress.text = addressFormattedByGoogle(value: value)
            self.strCustomerAddressId = ""

            self.places = [];
        }
        self.imgPoweredBy.removeFromSuperview()

        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
// MARK: -
// MARK: -ReminderCell
/*class addressCell: UITableViewCell {
    @IBOutlet weak var lbl_Address: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}*/
// MARK: -
// MARK: - -----------------------Add Address Delgates

extension ScheduleNowVC_CRMNew_iPhone : AddNewAddressiPhoneDelegate{

    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {
        
        print(dictData)
        txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
        strCustomerAddressId = ""
        
        /*if self.txtFldAddress.text?.count != 0 {
            
            let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                              
            let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                
                self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                self.strCustomerAddressId = ""
                
            })
            
            let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                
                
                
            })
            
            alertCOntroller.addAction(alertAction)
            alertCOntroller.addAction(alertActionCancel)

            self.present(alertCOntroller, animated: true, completion: nil)
            
        }else{
            
            self.txtFldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
            self.strCustomerAddressId = ""
            
        }*/

    }

}
// MARK: -
// MARK: - SalesNew_SelectionProtocol


extension ScheduleNowVC_CRMNew_iPhone: SalesNew_SelectionProtocol
{
    func didSelect(aryData: NSMutableArray, tag: Int) {
        
     }
    
    func didSelect(dictData: NSDictionary, tag: Int) {
        print(dictData)
        if(dictData.count != 0){
            self.arySelectedRange = NSMutableArray()
            self.arySelectedRange.add(dictData)
            let StartInterval = "\(dictData.value(forKey: "StartInterval")!)"
            let EndInterval = "\(dictData.value(forKey: "EndInterval")!)"
            let strrenge = StartInterval + " - " + EndInterval
            tf_Range.text = strrenge
            tf_Time.text = StartInterval
            
            tf_Range.tag = Int("\(dictData.value(forKey: "RangeofTimeId")!)")!
            
        }else{
            tf_Range.text = ""
            tf_Range.tag = 0
        }
    }
}
