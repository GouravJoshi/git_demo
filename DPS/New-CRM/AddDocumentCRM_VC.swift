//
//  AddDocumentCRM_VC.swift
//  DPS
//  Saavan Patidar
//  Created by Navin Patidar on 3/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import AVKit


class AddDocumentCRM_VC: UIViewController, UIDocumentPickerDelegate {
    
    // MARK: - ----IB-Outlets
    
    @IBOutlet weak var txtTitle: ACFloatingTextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnActions: UIButton!
    
    // MARK: - ----Variable
    
    var strWoId = String()
    var strFrom = String()
    var imagePicker = UIImagePickerController()
    var strMobileServiceDocumentId = String ()
    var strServiceDocumentId = String ()
    var strAddressAreaId = String ()
    var strAddressAreaName = String ()
    var strAssociationType = String ()
    var objWorkorderDetail = NSManagedObject()
    var strMobileConditionDocumentId = String ()
    var strConditionDocumentId = String ()
    var strConditionId = String ()
    var strGlobalName = String ()
    var strGlobalDocumentType = String ()
    var globalData  = Data ()
    var timer = Timer()
    var videoURL = NSURL()
    
    
    var strRefId = ""
    var strRefType = ""

   
    // MARK: - ----Views Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblName.text = ""
        btnActions.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //For Audio
        if (nsud.value(forKey: "yesAudioDocument") != nil)
        {
            
            let isAudio = nsud.bool(forKey: "yesAudioDocument")
            
            if isAudio
            {
                //deleteData()
                nsud.set(false, forKey: "yesAudioDocument")
                nsud.synchronize()
                self.strGlobalDocumentType = "audio"
                strGlobalName = nsud.value(forKey: "AudioName") as! String
                globalData = GetDataFromDocumentDirectory(strFileName: strGlobalName)
                lblName.text = strGlobalName
                btnActions.isHidden = false
                btnActions.setImage(UIImage(named: "audio"), for: .normal)
                
            }
            
        }
        
    }
    
    // MARK: - ----Button Action
    
    @IBAction func action_Back(_ sender: Any) {
        
        deleteData()
        
        self.back()
        
    }
    
    @IBAction func action_Browse(_ sender: ButtonWithShadow) {
        
        self.selectDocument(sender: sender)
        
    }
    
    
    @IBAction func action_Save(_ sender: UIButton) {
        
        if (isInternetAvailable()){
            
            self.saveDocuments()

        }else{
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
            
        }
        
    }
    @IBAction func keyDown(_ sender: Any) {
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func action_Play(_ sender: Any) {
        
        if strGlobalDocumentType == "image" {
            
            let uiImage: UIImage = UIImage(data: globalData)!
            
            let  imageView = UIImageView(image: uiImage)
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
                    testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
            
        }else if strGlobalDocumentType == "pdf" {
            
            savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
            
            goToPdfView(strPdfName: strGlobalName)
            
        }else if strGlobalDocumentType == "video" {
            
            
            let player = AVPlayer(url: videoURL as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
            
        }
            
            
            
        else{
            
            playAudioo(strAudioName: strGlobalName)
            
        }
        
    }
    
    
    // MARK: - ----Functionssssss
    
    func deleteData() {
        
        if strGlobalDocumentType == "audio"{
            
            if strGlobalName.count > 0 {
                
                removeImageFromDirectory(itemName: strGlobalName)
                
            }
            
        }
        
        if strGlobalDocumentType == "pdf"{
            
            if strGlobalName.count > 0 {
                
                removeImageFromDirectory(itemName: strGlobalName)
                
            }
            
        }
        
        if strGlobalDocumentType == "image"{
            
            if strGlobalName.count > 0 {
                
                removeImageFromDirectory(itemName: strGlobalName)
                
            }
            
        }
        
    }
    
    func back()  {
        
        self.dismiss(animated: false)
        
    }
    
    func playAudioo(strAudioName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToPdfView(strPdfName : String) {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func saveDocuments() {
        
        if (txtTitle.text?.count)! < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_EnterTitle, viewcontrol: self)
            
        } else if (strGlobalName.count) < 1 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_SelectDoc, viewcontrol: self)
            
        } else {
       
            
            var strMediTyepe = ""
            
            // Saving Data in Document Directory
            
            if strGlobalDocumentType == "image" {
                
                strMediTyepe = "Image"
                
              //  let uiImage: UIImage = UIImage(data: globalData)!
                
              //  let imageResized = Global().resizeImageGloballl(uiImage)
                
              //  saveImageDocumentDirectory(strFileName: strGlobalName, image: imageResized!)
                
            }else if strGlobalDocumentType == "pdf" {
                
                strMediTyepe = "Document"

              //  savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
                
            }
            else if strGlobalDocumentType == "video" {
                
                strMediTyepe = "Video"

              //  savepdfDocumentDirectory(strFileName: strGlobalName, dataa: globalData)
                
            }
            else{
            
                strMediTyepe = "Audio"

                var  strAudioName = ""
                if(nsud.value(forKey: "AudioName") != nil){
                    strAudioName = "\(nsud.value(forKey: "AudioName")!)"
                }
                let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                let localUrl = documentDirectory!.appendingPathComponent(strAudioName)

                if FileManager.default.fileExists(atPath: localUrl.path){
                    if let cert = NSData(contentsOfFile: localUrl.path) {
                        globalData = cert as Data
                        strGlobalName = strAudioName
                    }
                }
            }
          
       
            let dictSendDocument =
                ["RefId": strRefId,
                "RefType": strRefType,
                "MediaType": strMediTyepe,
                "Title": "\(txtTitle.text!)",
                "CompanyKey": Global().getCompanyKey(),
                "WebLeadDocumentId": "0",
                "Createdby": Global().getEmployeeId()]
            
                     let encoder = JSONEncoder()
                     if let jsonData = try? encoder.encode(dictSendDocument) {
                         if let jsonString = String(data: jsonData, encoding: .utf8) {
                             print(jsonString)
                            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddDocumentsByRefIdAndRefType
                            
                            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                            
                                    WebService.getRequestWithHeadersWithMultipleImage(JsonData: jsonString,JsonDataKey: "WebLeadDocDc", data: globalData, strDataName : strGlobalName, url: strURL) { (responce, status) in
                                        FTIndicator.dismissProgress()
                                             if(status)
                                             {
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedDocuments_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])

                                                if(responce.value(forKey: "data")is NSDictionary)
                                                {
                                                    let alert = UIAlertController(title: "document added successfully.", message: "", preferredStyle: UIAlertController.Style.alert)
                                                    
                                                    self.deleteData()
                                                    
                                                      // add the actions (buttons)
                                                      alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                                          self.dismiss(animated: false)
                                                      }))
                                                      self.present(alert, animated: true, completion: nil)
                                                }
                                                else
                                                {
                                                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "String", viewcontrol: self)
                                                }
                                             }else{
                                               showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Error", viewcontrol: self)
                     }
                  }
               }
            }
        }
    }
    
    func selectDocument(sender : ButtonWithShadow)  {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
           alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            self.strGlobalDocumentType = "image"
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            self.strGlobalDocumentType = "image"
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: "Voice Note", style: .default , handler:{ (UIAlertAction)in
            
            self.strGlobalDocumentType = "audio"
            
            self.goToRecordView()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Video", style: .default , handler:{ (UIAlertAction)in
            
            self.strGlobalDocumentType = "video"
            self.imagePicker = UIImagePickerController()
            self.imagePicker.sourceType = .camera
            self.imagePicker.cameraDevice = .rear
            self.imagePicker.videoMaximumDuration = 30
            self.imagePicker.delegate = self
            //self.imagePicker.videoQuality = .typeLow
            let mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)
            let videoMediaTypesOnly = (mediaTypes as NSArray?)?.filtered(using: NSPredicate(format: "(SELF contains %@)", "movie"))
            let movieOutputPossible: Bool = videoMediaTypesOnly != nil
            if movieOutputPossible {
                if let videoMediaTypesOnly = videoMediaTypesOnly as? [String] {
                    self.imagePicker.mediaTypes = videoMediaTypesOnly
                }
                self.timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.pickerDismiss), userInfo: nil, repeats: false)
                self.present(self.imagePicker, animated: true)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "More...", style: .default , handler:{ (UIAlertAction)in
         let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypeImage),String(kUTTypePDF)], in: UIDocumentPickerMode.import)
                                    documentPicker.delegate = self
                                  documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                                  documentPicker.popoverPresentationController?.sourceView = self.view

                                  self.present(documentPicker, animated: true, completion: nil)

            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        if let popoverController = alert.popoverPresentationController {
                      popoverController.sourceView = sender as UIView
                      popoverController.sourceRect = sender.bounds
                      popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
                  }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    @objc func pickerDismiss()
    {
        showAlertWithoutAnyAction(strtitle: "Max Limit Exceeded", strMessage: "Your video recorded succesfully!", viewcontrol: self)
        imagePicker.stopVideoCapture()
    }
    func goToRecordView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioView") as? RecordAudioView
        testController?.strFromWhere = "AddDocumentNewCRM"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    // MARK: - -----------------------------------UIDocumentPickerViewController Delegate Method-----------------------------------
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        
        print("import result : \(myURL)")
        
        do {
            
            let isMoreThenTwoMB = Global().isMoreThenTwoMB(myURL, 2)
            
            if isMoreThenTwoMB {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_DataLimit, viewcontrol: self)
                
            } else {
                
                deleteData()
                
                let myUrlString = "\(myURL)"
                
                lblName.text = Global().strDocName(fromPath: myUrlString)
                
                if myUrlString.contains(".pdf") || myUrlString.contains(".PDF") {
                    
                    let data = try Data(contentsOf: myURL)
                    
                    globalData = data
                    self.strGlobalDocumentType = "pdf"
                    
                    if strFrom == "Condition" {
                        
                        strGlobalName = "pdfC" + "\(getUniqueValueForId())" + ".pdf"
                        
                    }else{
                        
                        strGlobalName = "pdfW" + "\(getUniqueValueForId())" + ".pdf"
                        
                    }
                    
                    btnActions.isHidden = false
                    
                    btnActions.setImage(UIImage(named: "exam"), for: .normal)
                    
                }else{
                    
                    // image
                    let data = try Data(contentsOf: myURL)
                    let uiImage: UIImage = UIImage(data: data)!
                    
                    globalData = data
                    self.strGlobalDocumentType = "image"
                    
                    if strFrom == "Condition" {
                        
                        strGlobalName = "imgC" + "\(getUniqueValueForId())" + ".png"
                        
                    }else{
                        
                        strGlobalName = "imgW" + "\(getUniqueValueForId())" + ".png"
                        
                    }
                    
                    btnActions.isHidden = false
                    
                    btnActions.setImage(uiImage, for: .normal)
                    
                }
                
            }
            
            
        } catch _ as NSError  {
            
        }catch {
            
        }
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
         print("view was cancelled")
         //dismiss(animated: true, completion: nil)
     }
    
    func documentMenu(_ documentMenu: UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
    }
    
    
    /*public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        
        print("import result : \(myURL)")
        
        do {
            
            let isMoreThenTwoMB = Global().isMoreThenTwoMB(myURL, 2)
            
            if isMoreThenTwoMB {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Alert_DataLimit, viewcontrol: self)
                
            } else {
                
                deleteData()
                
                let myUrlString = "\(myURL)"
                
                lblName.text = Global().strDocName(fromPath: myUrlString)
                
                if myUrlString.contains(".pdf") || myUrlString.contains(".PDF") {
                    
                    let data = try Data(contentsOf: myURL)
                    
                    globalData = data
                    self.strGlobalDocumentType = "pdf"
                    
                    if strFrom == "Condition" {
                        
                        strGlobalName = "pdfC" + "\(getUniqueValueForId())" + ".pdf"
                        
                    }else{
                        
                        strGlobalName = "pdfW" + "\(getUniqueValueForId())" + ".pdf"
                        
                    }
                    
                    btnActions.isHidden = false
                    
                    btnActions.setImage(UIImage(named: "exam"), for: .normal)
                    
                }else{
                    
                    // image
                    let data = try Data(contentsOf: myURL)
                    let uiImage: UIImage = UIImage(data: data)!
                    
                    globalData = data
                    self.strGlobalDocumentType = "image"
                    
                    if strFrom == "Condition" {
                        
                        strGlobalName = "imgC" + "\(getUniqueValueForId())" + ".png"
                        
                    }else{
                        
                        strGlobalName = "imgW" + "\(getUniqueValueForId())" + ".png"
                        
                    }
                    
                    btnActions.isHidden = false
                    
                    btnActions.setImage(uiImage, for: .normal)
                    
                }
                
            }
            
            
        } catch _ as NSError  {
            
        }catch {
            
        }
        
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        //dismiss(animated: true, completion: nil)
    }*/
    
}

// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension AddDocumentCRM_VC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        deleteData()
        if(self.strGlobalDocumentType == "video"){ // For Video
            // video
            btnActions.isHidden = false
            
            btnActions.setImage(UIImage(named: "video"), for: .normal)
            
            self.timer.invalidate()
            let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String
            if (mediaType == "public.movie") {
                videoURL = (info[UIImagePickerController.InfoKey.mediaURL] as? NSURL)!
                print("videoURL---------\(videoURL)")
                self.strGlobalDocumentType = "video"
                if let data = try? Data(contentsOf: videoURL as URL)
                {
                    self.globalData = data
                    btnActions.isHidden = false
                    if strFrom == "Condition" {
                        strGlobalName = "videoC" + "\(getUniqueValueForId())" + ".mp4"
                    }else{
                        strGlobalName = "videoW" + "\(getUniqueValueForId())" + ".mp4"
                    }
                    lblName.text = strGlobalName
                }else{
                    
                }
                
            }
        }else{
            let imageData = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.jpegData(compressionQuality:1.0)
            
            //let data = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.pngData() as NSData?
            
            btnActions.isHidden = false
            
            btnActions.setImage(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, for: .normal)
            
            globalData = imageData!
            
            self.strGlobalDocumentType = "image"
            
            if strFrom == "Condition" {
                
                strGlobalName = "imgC" + "\(getUniqueValueForId())" + ".png"
                
            }else{
                
                strGlobalName = "imgW" + "\(getUniqueValueForId())" + ".png"
                
            }
            
            lblName.text = strGlobalName
            
        }
    }
}
