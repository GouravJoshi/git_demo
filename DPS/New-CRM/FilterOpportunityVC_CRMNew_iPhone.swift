//
//  FilterOpportunityVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 02/12/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
/*protocol FilterDataOpportunity : class
{
    
    func getFilterDataOnSelection(dictData : NSMutableDictionary ,strType : String)
    
}*/
class FilterOpportunityVC_CRMNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    // Outlets
    @IBOutlet weak var tblviewFilterOpportunity: UITableView!
    
    //Protocol
   
    
    // variables
   // fileprivate  let aryUserName = ["Akshay Hastekar", "EveryOne"] as NSArray
    fileprivate  var aryUserName = NSArray()

    
    fileprivate  let aryDays  = ["Today", "Yesterday","This week", "Last week"] as NSArray
  //  fileprivate  let aryStage = ["New", "Open","Default","Complete","Incomplete"] as NSArray
    
    fileprivate  var aryStage = NSArray()

    
    fileprivate  var strFromDate = ""
    fileprivate  var strToDate = ""
    fileprivate  var isFromDate = false
    fileprivate  var aryIndexPath = NSMutableArray()
    
    fileprivate  var aryIndexForStatus = NSMutableArray()
    fileprivate  var arySelectedStatus = NSMutableArray()
    fileprivate  var arySelectedUser = NSMutableArray()
    fileprivate  var arySelectedDay = NSMutableArray()
    
    
    let dictFilterData = NSMutableDictionary()
    
    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    var dictLoginData = NSDictionary()
    
    var arrOfStatus = NSMutableArray()
    var arrOfStatusName = NSMutableArray()
    
    fileprivate  var aryUrgencyType = NSMutableArray() , arySelectedUrgencyType = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
       /* let dictTemp = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        let arrStageMaster = dictTemp.value(forKey: "LeadStageMasters") as! NSArray
        print(arrStageMaster)*/
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
        
        aryUserName = ["\(strUserName)", "EveryOne"] as NSArray
        // Do any additional setup after loading the view.
        
        getStatus()
        aryStage = arrOfStatus.mutableCopy() as! NSArray
        self.aryUrgencyType = NSMutableArray()
        self.aryUrgencyType = self.getUrgencyMaster()
        //arySelectedStatus = arrOfStatus.mutableCopy() as! NSMutableArray
        
        if nsud.value(forKey: "opportunityFilterData") is NSDictionary
        {
            var dict = NSDictionary()
            dict = nsud.value(forKey: "opportunityFilterData") as! NSDictionary
            
            var dictNew = NSMutableDictionary()
            dictNew = dict.mutableCopy() as! NSMutableDictionary
            
            strFromDate = "\(dictNew.value(forKey: "fromDateFromFilter") ?? "")"
            strToDate = "\(dictNew.value(forKey: "toDateFromFilter") ?? "")"
            
            if dictNew.value(forKey: "statusFromFilter") is NSArray
            {
                let arr1  = dictNew.value(forKey: "statusFromFilter") as! NSArray
                arySelectedStatus = arr1.mutableCopy() as! NSMutableArray
            }
            
            if dictNew.value(forKey: "selectedUserFromFilter") is NSArray
            {
                let arr2  = dictNew.value(forKey: "selectedUserFromFilter") as! NSArray
                arySelectedUser = arr2.mutableCopy() as! NSMutableArray
            }
            
            if dictNew.value(forKey: "selectedDayFromFilter") is NSArray
            {
                let arr3  = dictNew.value(forKey: "selectedDayFromFilter") as! NSArray
                arySelectedDay = arr3.mutableCopy() as! NSMutableArray
            }
            
            if dictNew.value(forKey: "selectedUrgencyTypeFromOppFilter") is NSArray
            {
                let arr3  = dictNew.value(forKey: "selectedUrgencyTypeFromOppFilter") as! NSArray
                arySelectedUrgencyType = arr3.mutableCopy() as! NSMutableArray
            }
        }
    }
    
    // MARK: UITableview delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: DeviceType.IS_IPAD ? 55 : 40))
        viewheader.backgroundColor = UIColor.groupTableViewBackground
        
        let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: DeviceType.IS_IPAD ? 55 : 40))
        //--------------
      let btnCheckAll = UIButton(frame: CGRect(x: self.tblviewFilterOpportunity.frame.width - (DeviceType.IS_IPAD ? 65 : 50), y: 0, width: DeviceType.IS_IPAD ? 55 : 40, height: DeviceType.IS_IPAD ? 55 : 40))
               let lblAll = UILabel(frame: CGRect(x: self.tblviewFilterOpportunity.frame.width - 90, y: 0, width: DeviceType.IS_IPAD ? 55 : 40, height: DeviceType.IS_IPAD ? 55 : 40))
               
        lblAll.text = "All"
        btnCheckAll.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
        btnCheckAll.tag = section
        btnCheckAll.addTarget(self, action: #selector(actionSelectAllData), for: .touchUpInside)
        //---------
        if(DeviceType.IS_IPAD){
                  lblAll.font = UIFont.systemFont(ofSize: 21)
                  lbl.font = UIFont.systemFont(ofSize: 21)
                  
              }else{
                  lblAll.font = UIFont.systemFont(ofSize: 18)
                  lbl.font = UIFont.systemFont(ofSize: 18)
              }
        if(section == 0)
        {
            lbl.text = "User"
        }
        else if(section == 1)
        {
            lbl.text = "Days"
        }
        else if(section == 2)
        {
            lbl.text = "Date"
        }
        else if(section == 3)
        {
            lbl.text = "Status"
            viewheader.addSubview(btnCheckAll)
            viewheader.addSubview(lblAll)
            if(self.arySelectedStatus.count == self.aryStage.count){
                btnCheckAll.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            }
        }
        else if(section == 4)
        {
            lbl.text = "Urgency Type"
            viewheader.addSubview(btnCheckAll)
            viewheader.addSubview(lblAll)

            if(self.aryUrgencyType.count == self.arySelectedUrgencyType.count){
                btnCheckAll.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            }
        }
        viewheader.addSubview(lbl)
        return viewheader
    }
    @objc func actionSelectAllData (sender : UIButton){
        if(sender.tag == 3) //Stage
        {
            //Uncheck
            if(sender.currentImage == UIImage(named: "check_box_1New.png")){
                 self.arySelectedStatus  = NSMutableArray()
                for item in self.aryStage {
                    self.arySelectedStatus.add("\(item)")
                }
            }
                //Check
            else{
                self.arySelectedStatus  = NSMutableArray()
            }
        }
        
        else if(sender.tag == 4){
            //Uncheck
            if(sender.currentImage == UIImage(named: "check_box_1New.png")){
                self.arySelectedUrgencyType  = NSMutableArray()
                self.arySelectedUrgencyType = self.aryUrgencyType.mutableCopy()as! NSMutableArray
            }
                //Check
            else{
                self.arySelectedUrgencyType  = NSMutableArray()
            }
        }
        self.tblviewFilterOpportunity.reloadData()
    }
   
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 55 : 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0)
        {
            return aryUserName.count
        }
        if(section == 1)
        {
            return aryDays.count
        }
        if(section == 2)
        {
            return 1
        }
        return aryStage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTaskCell") as! FilterTaskCell
        
        if(indexPath.section == 0)
        {
            cell.lblFilterName.text =  "\(aryUserName[indexPath.row])"
            
            if arySelectedUser .contains((aryUserName[indexPath.row]))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        else  if(indexPath.section == 1)
        {
            cell.lblFilterName.text =  "\(aryDays[indexPath.row])"
            
            if arySelectedDay .contains((aryDays[indexPath.row]))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        else  if(indexPath.section == 2)
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "FilterTaskDateCell") as! FilterTaskDateCell
            
            cell1.btnFromDate.addTarget(self, action: #selector(actionOnFromDate), for: .touchUpInside)
            
            cell1.btnClearDate.addTarget(self, action: #selector(actionOnClearDate), for: .touchUpInside)

            
            cell1.btnToDate.addTarget(self, action: #selector(actionOnToDate), for: .touchUpInside)
            
           // cell1.btnFromDate.setTitle(strFromDate, for: .normal)
           // cell1.btnToDate.setTitle(strToDate, for: .normal)
            
           /* cell1.btnFromDate.layer.borderWidth = 1.0
            cell1.btnToDate.layer.borderWidth = 1.0
            cell1.btnFromDate.layer.borderColor = UIColor.lightGray.cgColor
            cell1.btnToDate.layer.borderColor = UIColor.lightGray.cgColor
            cell1.btnFromDate.layer.cornerRadius = 2.0
            cell1.btnToDate.layer.cornerRadius = 2.0*/
            
            cell1.txtFromDate.text = strFromDate
            cell1.txtToDate.text = strToDate
            cell1.txtFromDate.inputView = UIView()
            cell1.txtFromDate.delegate = self
            
            cell1.txtToDate.inputView = UIView()
            cell1.txtToDate.delegate = self
            
            return cell1
        }
        else  if(indexPath.section == 3)
        {
            cell.lblFilterName.text =  "\(aryStage[indexPath.row])"
            
            if arySelectedStatus .contains((aryStage[indexPath.row]))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
            
            
        }
        else if (indexPath.section == 4){
            
            let dict = aryUrgencyType[indexPath.row] as! NSDictionary
            cell.lblFilterName.text =  "\(dict.value(forKey: "Name")!)"
            
            if arySelectedUrgencyType .contains((aryUrgencyType[indexPath.row]))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
            
            
        }
        else
        {
            cell.lblFilterName.text =  "\(aryStage[indexPath.row])"
        }
        
       /* if(aryIndexPath.contains(indexPath))
        {
            for item in aryIndexPath
            {
                if(indexPath == (item as! NSIndexPath) as IndexPath)
                {
                    if(indexPath.section == (item as! NSIndexPath).section && indexPath.row == (item as! NSIndexPath).row)
                    {
                        cell.accessoryType = .checkmark
                        
                    }
                    else{
                        cell.accessoryType = .none
                    }
                }
            }
        }
        else
        {
            cell.accessoryType = .none
        }*/
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if(indexPath.section == 0)
        {
            let str = aryUserName.object(at: indexPath.row)
            
            if arySelectedUser.contains(str)
            {
                arySelectedUser.remove(str)
            }
            else
            {
                arySelectedUser.removeAllObjects()
                arySelectedUser.add(str)
            }
            tblviewFilterOpportunity.reloadData()
        }
        else if(indexPath.section == 1)
        {
            if strFromDate.count > 2 || strToDate.count > 2
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please choose either days or date", viewcontrol: self)
                
            }
            else
            {
                let str = aryDays.object(at: indexPath.row)
                
                if arySelectedDay.contains(str)
                {
                    arySelectedDay.remove(str)
                }
                else
                {
                    arySelectedDay.removeAllObjects()
                    arySelectedDay.add(str)
                }
                tblviewFilterOpportunity.reloadData()
                
            }
            
        }
        else if(indexPath.section == 2)
        {
             tblviewFilterOpportunity.reloadData()
        }
        else if(indexPath.section == 3)
        {
            /*if(aryIndexForStatus.contains(indexPath))
            {
                aryIndexForStatus.remove(indexPath)
                arySelectedStatus.remove(aryStage.object(at: indexPath.row))
            }
            else
            {
                aryIndexForStatus.add(indexPath)
                arySelectedStatus.add(aryStage.object(at: indexPath.row))
            }*/
            
            
            if arySelectedStatus .contains((aryStage[indexPath.row]))
            {
               // aryIndexForStatus.remove(indexPath)
                arySelectedStatus.remove(aryStage.object(at: indexPath.row))
            }
            else
            {
               // aryIndexForStatus.add(indexPath)
                arySelectedStatus.add(aryStage.object(at: indexPath.row))
            }
            tblviewFilterOpportunity.reloadData()
        }
        
        else if(indexPath.section == 4)
        {
           
            if arySelectedUrgencyType .contains((aryUrgencyType[indexPath.row]))
            {
                arySelectedUrgencyType.remove(aryUrgencyType.object(at: indexPath.row))
            }
            else
            {
                arySelectedUrgencyType.add(aryUrgencyType.object(at: indexPath.row))
            }
            tblviewFilterOpportunity.reloadData()
        }
        else
        {
            /*if(aryIndexPath.contains(indexPath))
            {
                aryIndexPath.remove(indexPath)
            }
            else
            {
                aryIndexPath.add(indexPath)
            }*/
            tblviewFilterOpportunity.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    // MARK: UIButton action
    @objc func actionOnClearDate(sender: UIButton!)
    {
        strFromDate = ""
        strToDate = ""
        tblviewFilterOpportunity.reloadData()
    }
    
    
    
    @objc func actionOnFromDate(sender: UIButton!)
    {
        if arySelectedDay.count > 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please choose either days or date", viewcontrol: self)
            
        }
        else
        {
            isFromDate = true
            gotoDatePickerView(sender: sender, strType: "Date")
        }
    }
    @objc func actionOnToDate(sender: UIButton!)
    {
        if arySelectedDay.count > 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please choose either days or date", viewcontrol: self)
            
        }
        else
        {
            isFromDate = false
            gotoDatePickerView(sender: sender, strType: "Date")
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton)
    {
        nsud.set(true, forKey: "fromOpportunityDetail")
        nsud.synchronize()
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnClearAll(_ sender: UIButton) {
           let alert = UIAlertController(title: alertMessage, message: alertFilterClear, preferredStyle: UIAlertController.Style.alert)
           alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                   nsud.set(NSDictionary(), forKey: "opportunityFilterData")
                  nsud.set(false, forKey: "fromOpportunityFilter")
               nsud.set(NSMutableArray(), forKey: "selectedUrgencyTypeFromOppFilter")
                  nsud.synchronize()
                       
               
               self.navigationController?.popViewController(animated: false)
           }))
           alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
           self.present(alert, animated: true, completion: nil)
           
       }
    @IBAction func actionOnApply(_ sender: UIButton)
    {
        if arySelectedStatus.count == 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select at least one status", viewcontrol: self)
        }
        else
        {
            
            //Date Comparision
            if(self.strFromDate.count > 0 && self.strToDate.count > 0)
            {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                let fromDate = dateFormatter.date(from: self.strFromDate)
                let toDate = dateFormatter.date(from: self.strToDate)
                
                if(fromDate! > toDate!){
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To Date should be greater than From Date.", viewcontrol: self)
                    return
                }
            }
            //End
            
            
            print("\(arrOfStatus)")
            print("Before \(arySelectedStatus)")

            var arrTemp =  NSMutableArray()
            arrTemp = arySelectedStatus.mutableCopy() as! NSMutableArray
            arySelectedStatus = NSMutableArray()
            for item in arrOfStatus
            {
                let str = item as! String
                if arrTemp .contains(str)
                {
                    arySelectedStatus.add(str)
                }
            }
            print("After \(arySelectedStatus)")
            
            
            
            dictFilterData.setObject("\(strFromDate)", forKey: "fromDateFromFilter" as NSCopying)
            dictFilterData.setObject("\(strToDate)", forKey: "toDateFromFilter" as NSCopying)
            
            dictFilterData.setObject(arySelectedStatus , forKey: "statusFromFilter" as NSCopying)
            
            dictFilterData.setObject(arySelectedUser , forKey: "selectedUserFromFilter" as NSCopying)
            
            dictFilterData.setObject(arySelectedDay , forKey: "selectedDayFromFilter" as NSCopying)
            
            dictFilterData.setObject(arySelectedUrgencyType , forKey: "selectedUrgencyTypeFromOppFilter" as NSCopying)

            //self.handleFilterData?.getFilterDataOnSelection(dictData: dictFilterData, strType: "forLead")
            
            nsud.set(dictFilterData, forKey: "opportunityFilterData")
            nsud.set(true, forKey: "fromOpportunityFilter")
            nsud.synchronize()
            self.navigationController?.popViewController(animated: false)

        }
    }
    
    // MARK: Functions
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    func getStatus()
    {
        arrOfStatus = NSMutableArray()
        arrOfStatusName = NSMutableArray()
        
        if nsud.value(forKey: "LeadStatusMasters") is NSArray
        {
            var arrStatusTemp = NSArray()
            arrStatusTemp = nsud.value(forKey: "LeadStatusMasters") as! NSArray //@"LeadStatusMasters" //WebLeadStatusMasters
            for item in arrStatusTemp
            {
                let dict = item as! NSDictionary
                
                if ("\(dict.value(forKey: "SysName") ?? "")").caseInsensitiveCompare("void") == .orderedSame
                {
                    
                }
                else
                {
                    arrOfStatus.add("\(dict.value(forKey: "SysName")!)")
                    arrOfStatusName.add("\(dict.value(forKey: "StatusName")!)")
                    
                }
                
            }
        }
        
    }
    func getUrgencyMaster() -> NSMutableArray {
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary {
            let dictLeadDetailMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if dictLeadDetailMaster.value(forKey: "UrgencyMasters") is NSArray {
                let aryTempUrgencyMasters = dictLeadDetailMaster.value(forKey: "UrgencyMasters") as! NSArray
                let aryTemp = aryTempUrgencyMasters.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("1") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("true")
                    
                }
                return (aryTemp as NSArray).mutableCopy() as! NSMutableArray
            }
            return NSMutableArray()

            
        }
        return NSMutableArray()
    }
    
}

extension FilterOpportunityVC_CRMNew_iPhone: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if isFromDate == true {
            
            strFromDate = strDate
            
        }
        else
        {
            strToDate = strDate
        }
        
        tblviewFilterOpportunity.reloadData()
    }
}
extension FilterOpportunityVC_CRMNew_iPhone: UITextFieldDelegate
{
   /* func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 100
        {
            return false
        }
        else if textField.tag == 101
        {
            return false
        }
        else
        {
            return true
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 100
        {
           
        }
        if textField.tag == 101
        {
           
        }
        return true
    }
 */
}
