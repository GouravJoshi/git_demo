//
//  ActivityDetailsVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 27/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class ActivityDetailsVC_CRMNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    // outlets
    
    @IBOutlet weak var lblActivityName: UILabel!
    @IBOutlet weak var lblParticipants: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewContainerBtnLogType: UIView!
    @IBOutlet weak var btnLogType: UIButton!
    @IBOutlet weak var imgviewLogType: UIImageView!
    @IBOutlet weak var tblviewActivityDetails: UITableView!
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    
    // variables
    fileprivate  var dictLoginData = NSDictionary()
    fileprivate  var strServiceUrlMain = ""
    fileprivate  var aryLogType = NSMutableArray()
    fileprivate  var dictActivityDetails = NSMutableDictionary()
    fileprivate  var aryAssociations = NSMutableArray()
    var strFromVC = "DashBoardView"
    var dictActivity = NSMutableDictionary()
    var activityID = ""
    var aryDetails = NSMutableArray()
    var rowCount = 0
    var loader = UIAlertController()
    @IBOutlet weak var lbltaskCount: UILabel!
        @IBOutlet weak var lblScheduleCount: UILabel!
    // MARK: view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                         lblScheduleCount.text = "0"
                             lbltaskCount.layer.cornerRadius = 18.0
                                   lbltaskCount.backgroundColor = UIColor.red
                                   lblScheduleCount.layer.cornerRadius = 18.0
                                   lblScheduleCount.backgroundColor = UIColor.red
                                   lbltaskCount.layer.masksToBounds = true
                                   lblScheduleCount.layer.masksToBounds = true

                         }
            setUpView()
            getLogTypeFromMaster()
            
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
            
            //createBadgeView()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setFooterMenuOption()
            })
        }
        
      
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setFooterMenuOption()
        })
      }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !Check_Login_Session_expired() {
            if isInternetAvailable() {
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    self.callAPIToGetActivityDetails()
                    
                }
            }
        }
    
    }
    
    // MARK: Functions
    func setFooterMenuOption() {
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Sales", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
           self.GotoDashboardViewController()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
          self.GotoScheduleViewController()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.GotoAccountViewController()
        }
        
       
    }
    func GotoDashboardViewController() {
        
        var status = Bool()

        if (self.navigationController != nil) {

            for vc in  self.navigationController!.viewControllers {

                if vc is DashBoardNew_iPhoneVC {

                    status = true

                    self.navigationController?.popToViewController(vc, animated: false)

                }

            }

        }

        if !(status){

              let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC

            self.navigationController?.pushViewController(controller, animated: false)

        }

    }
    func GotoScheduleViewController() {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        //let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
        
        //self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
    }
    func GotoAccountViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone

       self.navigationController?.pushViewController(controller, animated: false)
    }

    // MARK: UITableView delegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryAssociations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellActivityAssociates") as! LeadVCCell
        
        let dict = aryAssociations[indexPath.row] as! NSDictionary
        
        cell.lblHeader.text = "\(dict.value(forKey: "Title")!)"
        
        if let name = dict.value(forKey: "Name"){
            
            if("\(name)".count > 0 && "\(name)" != "<null>"){
                
                cell.lblTitle.text = "\(name)"
            }
            else{
                cell.lblTitle.text = " "
            }
        }
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(actionOnDissociate), for: .touchUpInside)
        
        /* let dictData = aryDetails.object(at: indexPath.row) as! NSDictionary
         
         var strDateNew = "", strCreatedDateNew = ""
         
         if("\(dictData.value(forKey: "fromDate")!)".count > 0)
         {
         strDateNew = Global().convertDate("\(dictData.value(forKey: "fromDate")!)")
         }
         
         if("\(dictData.value(forKey: "clientCreatedDate")!)".count > 0)
         {
         strCreatedDateNew = Global().convertDate("\(dictData.value(forKey: "clientCreatedDate")!)")
         }
         else if("\(dictData.value(forKey: "createdDate")!)".count > 0)
         {
         
         strCreatedDateNew = Global().convertDate("\(dictData.value(forKey: "createdDate")!)")
         }
         
         cell.lblActivtyDate.text = strDateNew
         cell.lblCreatedDate.text = strCreatedDateNew
         if("\(dictData.value(forKey: "activityTime")!)".count > 0)//activityTime
         {
         let strTime = Global().convertTime("\(dictData.value(forKey: "activityTime")!)")
         
         cell.lblTime.text = strTime
         }
         
         
         cell.lblParticipants.text = " "
         if("\(dictData.value(forKey: "participants")!)".count > 0)
         {
         let  ary = "\(dictData.value(forKey: "participants")!)".components(separatedBy: ",")
         var name = ""
         for item in ary
         {
         name = name + "," + getParticipantNameFromId(id: item)
         }
         if(name.count > 0)
         {
         name.removeFirst()
         }
         
         cell.lblParticipants.text = name
         cell.lblParticipants.textAlignment = .left
         }
         */
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let dict = aryAssociations[indexPath.row] as! NSDictionary
        
        if("\(dict.value(forKey: "Name")!)".count > 0 && "\(dict.value(forKey: "Name")!)" != "<null>"){
            return UITableView.automaticDimension
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
      
        let dict = self.aryAssociations[indexPath.row] as! NSDictionary
      
        if("\(dict.value(forKey: "Title")!)" == "Contact"){
            goToContactDetails(strContactId: "\(dict.value(forKey: "Id")!)" , dictContact: dict)
        }
        else if("\(dict.value(forKey: "Title")!)" == "Company"){
           
            goToCompanyDetails(strCompanyId: "\(dict.value(forKey: "Id")!)")
        }
        else if("\(dict.value(forKey: "Title")!)" == "Lead"){//weblead
       
            goToWebLeadDetails(strWebLeadId: "\(dict.value(forKey: "Id")!)")
        }
        else if("\(dict.value(forKey: "Title")!)" == "Opportunity"){//weblead
            
            goToOpportunityDetails(strOpportunityId: "\(dict.value(forKey: "Id")!)")
        }
        
    }
    
    // MARK: Web Service
    
    fileprivate func callAPIToGetActivityDetails()
    {
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetActivityById?ActivityId=\(activityID)"
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ActivityDetailsVC_CRMNew_iPhone") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status == true)
                         {
                             if (response.value(forKey: "data") is NSDictionary)
                             {
                                 var dict = response.value(forKey: "data") as! NSDictionary
                                 
                                 dict = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))! as NSDictionary
                                 
                                 self.dictActivityDetails = dict.mutableCopy() as! NSMutableDictionary
                                 
                                 print(self.dictActivityDetails)
                                 self.aryAssociations.removeAllObjects()
                                 self.showActivityDetails()
                             }
                             else
                             {
                                 showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                             }
                         }
                         else
                         {
                             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                             // something went wrong
                         }
            }
            
         
        }
    }
    
    fileprivate func callAPIToDissociateEntity(strEntityName:String){
        
        var entityType = ""
        if(strEntityName == "Contact"){
            entityType = "CrmContact"
        }
        else if(strEntityName == "Company"){
            entityType = "CrmCompany"
        }
        else if(strEntityName == "Lead"){
            entityType = "WebLead"
        }
        else if(strEntityName == "Opportunity"){
            entityType = "Lead"
        }
        else{
            entityType = "Account"
        }
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/DissociateActivityWithEntityType?ActivityId=\(activityID)&EntityType=\(entityType)"
      
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ActivityDetailsVC_CRMNew_iPhone") { (response, status) in
            
            self.loader.dismiss(animated: false) {
                if(status == true)
                          {
                              if (response.value(forKey: "data") is Bool)
                              {
                                
                                UserDefaults.standard.set(true, forKey: "RefreshTasks_ContactDetails")

                                UserDefaults.standard.set(true, forKey: "RefreshTasks_CompanyDetails")
                                
                                  if(response.value(forKey: "data") as! Bool == true){
                                      
                                      self.callAPIToGetActivityDetails()
                                      
                                      /*  for (index, item) in self.aryAssociations.enumerated(){
                                       
                                       let dict = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                       if("\(dict.value(forKey: "Title")!)" == strEntityName){
                                       
                                       dict.setValue("", forKey: "Name")
                                       self.aryAssociations.replaceObject(at: index, with: dict)
                                       self.tblviewActivityDetails.reloadData()
                                       break
                                       }
                                       }*/
                                  }
                                  else{
                                      showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                  }
                              }
                              else
                              {
                                  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                              }
                          }
                          else
                          {
                              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                              // something went wrong
                          }
            }
            
          
        }
    }
    fileprivate func callAPIToUpdateActivityLogType(dict:NSDictionary){
        
        let strLogTypeId = "\(dict.value(forKey: "LogTypeId")!)"
        
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/UpdateActivityLogType?ActivityId=\(activityID)&LogTypeId=\(strLogTypeId)"
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ActivityDetailsVC_CRMNew_iPhone") { (response, status) in
            
            self.loader.dismiss(animated: false) {
                    if(status == true)
                        {
                            if (response.value(forKey: "data") is Bool)
                            {
                                if(response.value(forKey: "data") as! Bool == true){
                                    
                                    UserDefaults.standard.set(true, forKey: "RefreshTasks_ContactDetails")

                                    UserDefaults.standard.set(true, forKey: "RefreshTasks_CompanyDetails")
                                    
                                    UserDefaults.standard.set(true, forKey: "isActivityAddedUpdated")

                                    self.dictActivityDetails.setValue(strLogTypeId, forKey: "LogTypeId")
                                    
                                    print(self.dictActivityDetails)
                                    
                                    self.btnLogType.setTitle("\(dict.value(forKey: "Name")!)", for: .normal)
                                    
                                    showAlertWithoutAnyAction(strtitle:"Message" , strMessage: "Activity Type updated successfully.", viewcontrol: self)
                                    
                                }
                                    
                                else{
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                }
                            }
                            else
                            {
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            }
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            // something went wrong
                        }
            }
            
        
        }
        
    }
    
    // MARK: Functions
    
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
                        lbltaskCount.isHidden = true
                        lblScheduleCount.isHidden = true
             }
                
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }

                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                                                   lblScheduleCount.text = strScheduleCount
                                                   lblScheduleCount.isHidden = false
                                               }else{
                        let badgeSchedule = SPBadge()
                        badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                        badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeSchedule.badge = strScheduleCount
                        btnScheduleFooter.addSubview(badgeSchedule)
                        }
                    }
                    

                    if strTaskCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                                                 lbltaskCount.text = strTaskCount
                                                 lbltaskCount.isHidden = false
                                                 
                                             }else{
                        let badgeTasks = SPBadge()
                        badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                        badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeTasks.badge = strTaskCount
                        btnTasksFooter.addSubview(badgeTasks)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    fileprivate func getLogTypeFromMaster()
    {
        
        let dict = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        
        if(dict.count > 0)
        {
            let aryTemp = (dict.value(forKey: "ActivityLogTypeMasters") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in aryTemp
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    aryLogType.add((item as! NSDictionary))
                }
            }
            
            print(aryLogType)
        }
    }
    
    fileprivate func getParticipantNameFromId(id:String)-> String
    {
        var name = id
        let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
        
        
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if("\(dict.value(forKey: "EmployeeNo")!)"  == id)
                {
                    name = "\(dict.value(forKey: "FullName")!)"
                }
            }
        }
        return name
    }
    
    fileprivate func goToAddActivity()
    {
        let vc = storyboardNewCRM.instantiateViewController(withIdentifier: "AddActivityVC_CRMNew_iPhone") as! AddActivityVC_CRMNew_iPhone
        
        //        vc.dictActivity = dictActivity
        vc.isForUpdate = true
        vc.strFromVC = strFromVC
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    fileprivate func setUpView(){
        
        viewContainerBtnLogType.layer.cornerRadius = viewContainerBtnLogType.frame.size.height/2.0
        viewContainerBtnLogType.layer.borderWidth = 0.5
        viewContainerBtnLogType.layer.borderColor = UIColor.lightGray.cgColor
        viewContainerBtnLogType.layer.masksToBounds = true
        
    }
    
    fileprivate func showActivityDetails(){
        
        var dictAssociation = NSMutableDictionary()
        
        // adding CRM Contact
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "CrmContactName")!)", forKey: "Name")
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "CrmContactId")!)", forKey: "Id")
        dictAssociation.setValue("Contact", forKey: "Title")
        
        aryAssociations.add(dictAssociation)
        
        dictAssociation = NSMutableDictionary()
        
        // adding CRM Company
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "CrmCompanyName")!)", forKey: "Name")
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "CrmCompanyId")!)", forKey: "Id")
        dictAssociation.setValue("Company", forKey: "Title")
        aryAssociations.add(dictAssociation)
        
        dictAssociation = NSMutableDictionary()
        
        // adding Lead
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "WebLeadName")!)", forKey: "Name")
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "WebLeadId")!)", forKey: "Id")
        dictAssociation.setValue("Lead", forKey: "Title")
        aryAssociations.add(dictAssociation)
        
        dictAssociation = NSMutableDictionary()
        
        // adding Opportunity
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "LeadName")!)", forKey: "Name")
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "LeadId")!)", forKey: "Id")
        dictAssociation.setValue("Opportunity", forKey: "Title")
        aryAssociations.add(dictAssociation)
        
        dictAssociation = NSMutableDictionary()
        
        // adding Account
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "AccountDisplayName")!)", forKey: "Name")
        dictAssociation.setValue("\(self.dictActivityDetails.value(forKey: "AccountId")!)", forKey: "Id")
        dictAssociation.setValue("Account", forKey: "Title")
        aryAssociations.add(dictAssociation)
        
        // showing activity details
        lblActivityName.text = "\(dictActivityDetails.value(forKey: "Agenda")!)"
        
        lblParticipants.text = "Akshay Hastekar"
        
        var date = ""
        var time = ""
        
        if("\(self.dictActivityDetails.value(forKey: "FromDate")!)".count > 0 && "\(self.dictActivityDetails.value(forKey: "FromDate")!)" != "<null>"){
            
            date = Global().convertDate("\(self.dictActivityDetails.value(forKey: "FromDate")!)")
            
        }
        if("\(self.dictActivityDetails.value(forKey: "ActivityTime")!)".count > 0 && "\(self.dictActivityDetails.value(forKey: "ActivityTime")!)" != "<null>"){
            time = Global().convertTime("\(self.dictActivityDetails.value(forKey: "ActivityTime")!)")
           
        }else{
            let date = Date()
                           let formatter = DateFormatter()
                           formatter.dateFormat = "hh:mm a"
                           formatter.locale = Locale(identifier: "EST")
                           time = formatter.string(from: date)
        }
        if(date.count > 0 && time.count > 0){
            
            lblDate.text = date + " " + time
        }
        
        for item in aryLogType{
            
            if("\(self.dictActivityDetails.value(forKey: "LogTypeId")!)" == "\((item as!NSDictionary).value(forKey: "LogTypeId")!)"){
                
                btnLogType.setTitle("\((item as!NSDictionary).value(forKey: "Name")!)", for: .normal)
                break
            }
        }
        
        let participants = NSMutableArray()
        
        if let ary = self.dictActivityDetails.value(forKey: "ActivityParticipants"){
            
            if(ary is NSArray){
                
                if((ary as! NSArray).count > 0){
                    
                    for item in (ary as! NSArray){
                        participants.add("\((item as! NSDictionary).value(forKey: "EmployeeFullName")!)")
                    }
                }
            }
        }
        
        if(participants.count > 0){
            
            lblParticipants.text = participants.componentsJoined(by: ",")
        }
        
        tblviewActivityDetails.reloadData()
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func editActivity() {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(1, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "AccountId")!)", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "LeadId")!)", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "WebLeadId")!)", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "CrmContactId")!)", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "CrmCompanyId")!)", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "AccountDisplayName")!)", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "LeadName")!)", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "WebLeadName")!)", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "CrmContactName")!)", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("\(self.dictActivityDetails.value(forKey: "CrmCompanyName")!)", forKeyPath: "Pre_CrmCompanyName")
        
        dictTaskDetailsData.addEntries(from: dictActivityDetails as! [AnyHashable : Any])
        
  let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
        controller.dictActivityDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func goToCompanyDetails(strCompanyId:String){
        
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
        
        controller.strCRMCompanyIdGlobal = strCompanyId
        
        self.navigationController?.pushViewController(controller, animated: false)
    }
    fileprivate func goToContactDetails(strContactId:String , dictContact:NSDictionary){
        
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as! ContactDetailsVC_CRMContactNew_iPhone
        controller.strCRMContactIdGlobal = strContactId
        controller.dictContactDetailFromList = dictContact
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    fileprivate func goToWebLeadDetails(strWebLeadId:String)
    {
         let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadDetailsVC_CRMNew_iPhone") as! WebLeadDetailsVC_CRMNew_iPhone
        
        vc.strReftype = enumRefTypeWebLead
        
        vc.strRefIdNew = strWebLeadId
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    fileprivate func goToOpportunityDetails(strOpportunityId:String){
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityDetailsVC_CRMNew_iPhone") as! OpportunityDetailsVC_CRMNew_iPhone
        
        vc.strRefIdNew = strOpportunityId
        vc.strReftype = enumRefTypeOpportunity
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    // MARK: UIButton action
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnAddActivity(_ sender: UIButton) {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        
  let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
        controller.dictActivityDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    @IBAction func actionOnEdit(_ sender: UIButton)
    {
        if let isSystem = dictActivityDetails.value(forKey: "IsSystem")
        {
            if(isSystem as! Bool == true)
            {
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: "System generated activity, not authorised to update. \n you can only view this", viewcontrol: self)
            }
            else
            {
                // goto add/update activity
                editActivity()
            }
            
        }
        else
        {
            // goto add/update activity
            editActivity()
        }
    }
    
    @IBAction func actionOnDashboard(_ sender: UIButton) {
        self.GotoDashboardViewController()
    }
    
    @IBAction func actionOnLeads(_ sender: UIButton) {
        
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func actionOnAppointment(_ sender: UIButton) {
        self.GotoScheduleViewController()
    }
    
    @IBAction func actionOnContact(_ sender: UIButton) {
        self.GotoAccountViewController()
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
           alert.view.tintColor = UIColor.black
        let Near = (UIAlertAction(title: enumNearBy, style: .default , handler:{ (UIAlertAction)in
            /*
        if(DeviceType.IS_IPAD){
                                let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
                                self.navigationController?.pushViewController(vc!, animated: false)
                            }else{
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
                                self.navigationController?.pushViewController(vc!, animated: false)
                            }*/
            let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
            self.navigationController?.pushViewController(vc!, animated: false)

        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
                         Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                         alert.addAction(Near)
        
         let Signed = (UIAlertAction(title: enumSignedAgreement, style: .default , handler:{ (UIAlertAction)in
            
          if(DeviceType.IS_IPAD){
                               let mainStoryboard = UIStoryboard(
                                   name: "CRMiPad",
                                   bundle: nil)
                               let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
                               self.navigationController?.present(objByProductVC!, animated: true)
                           }else{
                               let mainStoryboard = UIStoryboard(
                                   name: "CRM",
                                   bundle: nil)
                               let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
                               self.navigationController?.present(objByProductVC!, animated: true)
                           }
            
        }))
        
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
                                     Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                                     alert.addAction(Signed)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        if let popoverController = alert.popoverPresentationController {
                            popoverController.sourceView = sender as UIView
                            popoverController.sourceRect = sender.bounds
                            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                        }
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    @IBAction func actionOnLogType(_ sender: UIButton) {
        
        if let isSystem = dictActivityDetails.value(forKey: "IsSystem")
        {
            if(isSystem as! Bool == true)
            {
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: "System generated activity, not authorised to update. \n you can only view this", viewcontrol: self)
            }
            else
            {
                if(aryLogType.count > 0){
                    
                    openTableViewPopUp(tag: 901, ary: aryLogType, aryselectedItem: NSMutableArray())
                    
                }
            }
            
        }else{
            
            if(aryLogType.count > 0){
                
                openTableViewPopUp(tag: 901, ary: aryLogType, aryselectedItem: NSMutableArray())
                
            }
        }
        
    }
    
    func apiDeAssociate(sender: UIButton!) {
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            let alert = UIAlertController(title: "Message", message: "Are you sure you want to dissociate?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default , handler:{ (UIAlertAction)in
                
                let dict = self.aryAssociations[sender.tag] as! NSDictionary
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    self.callAPIToDissociateEntity(strEntityName: "\(dict.value(forKey: "Title")!)")
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .default , handler:{ (UIAlertAction)in
                
                self.dismiss(animated: false, completion: nil)
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @objc func actionOnDissociate(sender: UIButton!)
    {
        
        if let isSystem = dictActivityDetails.value(forKey: "IsSystem")
        {
            if(isSystem as! Bool == true)
            {
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: "System generated activity, not authorised to update. \n you can only view this", viewcontrol: self)
            }
            else
            {
                apiDeAssociate(sender: sender)
                
            }
            
        }else{
            
            apiDeAssociate(sender: sender)

        }
        
    }
}

extension ActivityDetailsVC_CRMNew_iPhone: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        self.dismiss(animated: false, completion: nil)
        if(isInternetAvailable() == true)
        {
            let alertController = UIAlertController(title: "Message", message: "Do you want to update activity type?", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    self.callAPIToUpdateActivityLogType(dict: dictData)
                }
            }
            
            let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
}
