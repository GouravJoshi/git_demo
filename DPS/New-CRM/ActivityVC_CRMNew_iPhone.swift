//
//  ActivityVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 26/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  Saavan Patidar 

import UIKit

fileprivate enum FilterType : String{
    
    case  all,upcoming,overdue
}
fileprivate enum SortBy : String{
    
    case  date,logType,aToz,zToa
}

class ActivityVC_CRMNew_iPhone: UIViewController,UISearchBarDelegate, UITableViewDelegate,UITableViewDataSource {
    
    // outlets
    
    @IBOutlet weak var searchBarActivity: UISearchBar!
    @IBOutlet weak var tblviewActivity: UITableView!
    @IBOutlet weak var viewBtnContainer: UIView!
    @IBOutlet weak var btnTask: UIButton!
    @IBOutlet weak var btnActivity: UIButton!
    @IBOutlet weak var segmentControlActivity: UISegmentedControl!
    @IBOutlet weak var segmentControlActivityHeight: NSLayoutConstraint!
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    @IBOutlet weak var headerView: TopView!
    
    // variables
    var dictLoginData = NSDictionary()
    var aryActivities = [NSMutableDictionary]()
    var aryActivitiesCopy = NSMutableArray()
    var strRefID = ""
    var strRefType = ""
    var strServiceUrlMain = ""
    var strEmpID = ""
    @objc var strFromVC = "DashBoardView"
    @objc var strleadId = ""
    var strFromDate = ""
    var strToDate = ""
    
    var dictGroupedActivity = Dictionary<AnyHashable, [NSMutableDictionary]>()
    var aryAllKeys = Array<Any>()
    var filterAry = [NSMutableDictionary]()
    var filterAryCopy = [NSMutableDictionary]()
    var aryLogTypeMaster = NSMutableArray()
    
    fileprivate  var filterType = FilterType.all
    fileprivate  var sortBy = SortBy.date
    
    
    var loader = UIAlertController()
    
    
    
    @IBOutlet weak var lbltaskCount: UILabel!
    @IBOutlet weak var lblScheduleCount: UILabel!
    var strSearchText = ""

    
    // view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                lblScheduleCount.text = "0"
                lbltaskCount.layer.cornerRadius = 18.0
                lbltaskCount.backgroundColor = UIColor.red
                lblScheduleCount.layer.cornerRadius = 18.0
                lblScheduleCount.backgroundColor = UIColor.red
                lbltaskCount.layer.masksToBounds = true
                lblScheduleCount.layer.masksToBounds = true
                
            }
            if #available(iOS 13.0, *) {
                searchBarActivity.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
            } else {
                // Fallback on earlier versions
            }
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            segmentControlActivity.isHidden = true
            tblviewActivity.tableFooterView = UIView()
            tblviewActivity.estimatedRowHeight = 70.0
            segmentControlActivityHeight.constant = 0.0
            
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
            
            strEmpID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
            
            setUpViews()
            getLogTypeFromMaster()
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "ActivityAdded_Notification"),
                                                   object: nil,
                                                   queue: nil,
                                                   using:catchNotification)
            
            UserDefaults.standard.set(true, forKey: "isActivityAddedUpdated")
            
            //createBadgeView()
        }
      

    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setTopMenuOption()
            self.setFooterMenuOption()
        })
      }
    override func viewWillAppear(_ animated: Bool) {
        if !Check_Login_Session_expired() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setTopMenuOption()
                self.setFooterMenuOption()
            })
            
            super.viewWillAppear(animated)
            
            nsud.set(false, forKey: "fromAddUpdateTask")
            nsud.set(false, forKey: "fromAddUpdateActivity")
            nsud.synchronize()
            
            super.viewWillAppear(animated)
            
            if let isActivityAddedUpdated = nsud.value(forKey: "isActivityAddedUpdated"){
                if(isActivityAddedUpdated is Bool){
                    
                    if((isActivityAddedUpdated as! Bool) == true){
                        
                        if (isInternetAvailable()){
                            
                            UserDefaults.standard.set(false, forKey: "isActivityAddedUpdated")
                            
                            //  FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                                
                                self.callAPIToGetActivityList()
                                
                            })
                            
                        }else{
                            
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                            
                        }
                    }
                }
            }        }
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchBarActivity.text = nil
    }
    func setFooterMenuOption() {
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Sales", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
            self.GotoDashboardViewController()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
            self.GotoScheduleViewController()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.GotoAccountViewController()
        }
        
    }
    
    func setTopMenuOption() {
        for view in self.view.subviews {
            if(view is HeaderView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Activity", forKey: "DPS_TopMenuOptionTitle")
        nsud.synchronize()
        var headerViewtop = HeaderView()
        
        headerViewtop = HeaderView.init(frame: CGRect(x: 0, y: 0, width: self.headerView.frame.size.width, height:self.headerView.frame.size.height))
        headerViewtop.searchBarTask.delegate = self
        if #available(iOS 13.0, *) {
            headerViewtop.searchBarTask.searchTextField.text = self.strSearchText
        } else {
            headerViewtop.searchBarTask.text = self.strSearchText
        }
        self.headerView.addSubview(headerViewtop)
        
        headerViewtop.onClickFilterButton = {() -> Void in
            self.GotoFilerViewController()
        }
        headerViewtop.onClickAddButton = {() -> Void in
            self.GotoAddActivityViewController()
        }
        headerViewtop.onClickVoiceRecognizationButton = { () -> Void in
            
            self.view.endEditing(true)
            if self.aryAllKeys.count != 0 {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.delegate = self
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: {})
            }
        }
        headerViewtop.onClickTopMenuButton = {(str) -> Void in
            print(str)
            switch str {
            case "Map":
                self.GotoMapViewController()
                break
            case "Lead":
                self.GotoLeadViewController()
                break
            case "Opportunity":
                self.GotoOpportunityViewController()
                break
            case "Tasks":
                self.GotoTaskViewController()
                break
            case "Activity":
                
                break
            case "View Schedule":
                self.GotoViewScheduleViewController()
                break
            case "Signed Agreements":
                self.GotoSignAgreementViewController()
                break
            default:
                break
            }
            
        }
        
    }
    
    
    func GotoViewScheduleViewController() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ScheduleD2D" : "ScheduleD2D", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoDashboardViewController() {
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoScheduleViewController() {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        //let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
        
        //self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "Main",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
        }
        
    }
    func GotoAccountViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoFilerViewController()  {
        let mainStoryboard = UIStoryboard(
            name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
            bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "FilterActivityVC_CRMNew_iPhone") as! FilterActivityVC_CRMNew_iPhone
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func GotoAddActivityViewController()  {
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
        controller.dictActivityDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoMapViewController() {
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    func GotoLeadViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoOpportunityViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoTaskViewController() {
        var isVCFound = false
        
        var controllerVC = UIViewController()
        
        for controller in self.navigationController!.viewControllers as Array { if controller.isKind(of: TaskVC_CRMNew_iPhone.self) {
            
            isVCFound = true
            controllerVC = controller
            
        }
        
        }
        
        if isVCFound == true {
            
            self.navigationController?.popToViewController(controllerVC, animated: false)
            
        }
        
        if(isVCFound == false)
        {
            let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
            
            testController.strFromVC = self.strFromVC
            self.navigationController?.pushViewController(testController, animated: false)
        }
    }
    func GotoActivityViewController() {
        
    }
    func GotoSignAgreementViewController() {
        if(DeviceType.IS_IPAD){
            let mainStoryboard = UIStoryboard(
                name: "CRMiPad",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }else{
            let mainStoryboard = UIStoryboard(
                name: "CRM",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }
    }
    //MARK:------End Other viewController----
    // MARK: Web Service Calling
    //    old api
    /*  @objc fileprivate func callAPIToGetActivityList()
     {
     if(isInternetAvailable() == false)
     {
     self.refreshControl.endRefreshing()
     tblviewActivity.reloadData()
     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
     }
     else
     {
     
     strRefType = "Employee"
     strRefID = strEmpID
     
     if(strFromVC == "WebLeadVC")
     {
     strRefType = "WebLead"
     strRefID = strleadId
     }
     if(strFromVC == "DashBoardView")
     {
     strRefType = "Employee"
     strRefID = strEmpID
     }
     
     if(strFromVC == "OpportunityVC")
     {
     strRefType = "Lead"
     strRefID = strleadId
     }
     
     
     if(strRefID.count == 0)
     {
     strRefType = "Employee"
     strRefID = strEmpID
     }
     
     let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetActivitiesByRefIdAndRefTypeAsyncV2_SubObjs?refid=\(strRefID)&reftype=\(strRefType)"
     
     self.view.isUserInteractionEnabled = false
     DispatchQueue.main.async {
     
     FTIndicator.showProgress(withMessage: "Fetching Activity...", userInteractionEnable: false)
     }
     
     WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
     
     self.view.isUserInteractionEnabled = true
     self.refreshControl.endRefreshing()
     if(status == true)
     {
     if((response.value(forKey: "data") as! NSArray).count > 0)
     {
     self.aryActivities.removeAllObjects()
     
     self.saveActivityListToLocalDb(data: response.value(forKey: "data") as! NSArray)
     
     }
     else{
     
     if(self.aryAllKeys.count == 0)
     {
     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
     }
     
     
     }
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
     
     }
     }
     }
     }*/
    
    
    // new wali api
    @objc fileprivate func callAPIToGetActivityList()
    {
        
        let startGlobal = CFAbsoluteTimeGetCurrent()
        
        strRefType = "Employee"
        strRefID = strEmpID
        
        if(strFromVC == "WebLeadVC")
        {
            strRefType = "WebLead"
            strRefID = strleadId
        }
        if(strFromVC == "DashBoardView")
        {
            strRefType = "Employee"
            strRefID = strEmpID
        }
        
        if(strFromVC == "OpportunityVC")
        {
            strRefType = "Lead"
            strRefID = strleadId
            // strEmpID
        }
        
        if(strRefID.count == 0)
        {
            strRefID = strEmpID
        }
        var dicJson = [String : Any]()
        var aryLogTypeId = NSArray()
        // default json dictionary
        dicJson = ["RefId":strRefID,
                   "RefType":strRefType,
                   "IsDefault":"true",
                   "SkipDefaultDate":"false",
                   "FromDate":"",
                   "ToDate":"",
                   "EmployeeId":"",
                   "TakeRecords":"",
                   "LogTypeIds":aryLogTypeId
        ]
        
        
        if let dictLoc = nsud.value(forKey: "dictFilterActivity"){
            
            if(dictLoc is NSDictionary){
                
                if((dictLoc as! NSDictionary).count > 0){
                    
                    let dictForSort = dictLoc as! NSDictionary
                    
                    if let logTypeId = dictForSort.value(forKey: "logTypeId")
                    {
                        if (logTypeId is NSArray)
                        {
                            if(logTypeId as! NSArray).count > 0{
                                
                                aryLogTypeId = logTypeId as! NSArray
                            }
                        }
                    }
                    
                    dicJson = ["RefId":strRefID,
                               "RefType":strRefType,
                               "IsDefault":"false",
                               "SkipDefaultDate":"false",
                               "FromDate":"\(dictForSort.value(forKey: "fromDate")!)",
                               "ToDate":"\(dictForSort.value(forKey: "toDate")!)",
                               "EmployeeId":"\(dictForSort.value(forKey: "user")!)",
                               "TakeRecords":"",
                               "LogTypeIds":aryLogTypeId
                    ]
                    
                }
            }
        }
        
        
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetActivitiesByRefIdAndRefTypeAsyncV3"
        
        //FTIndicator.showProgress(withMessage: "Fetching Activity...", userInteractionEnable: false)
        self.present(loader, animated: false, completion: nil)
        WebService.postRequestWithHeaders(dictJson: dicJson as NSDictionary, url: strURL, responseStringComing: "ActivityVC_CRMNew_iPhone") { (response, status) in
            
            self.loader.dismiss(animated: false) {
                
                if(status == true)
                {
                    if let dictData = response.value(forKey: "data"){
                        
                        print(dictData)
                        
                        self.aryAllKeys.removeAll()
                        self.dictGroupedActivity.removeAll()
                        self.aryActivitiesCopy.removeAllObjects()
                        
                        if((dictData as! NSDictionary).count > 0){
                            
                            if ((dictData as! NSDictionary).value(forKey: "Activities")) is NSArray
                            {
                                if(((dictData as! NSDictionary).value(forKey: "Activities") as! NSArray).count > 0){
                                    
                                    let arrayTemp = (dictData as! NSDictionary).value(forKey: "Activities") as! NSArray
                                    
                                    for item in arrayTemp
                                    {
                                        let dictTemp = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                        
                                        if("\(dictTemp.value(forKey: "FromDate")!)".count > 0 && "\(dictTemp.value(forKey: "FromDate")!)" != "<null>" && "\(dictTemp.value(forKey: "FromDate")!)" != " ")
                                        {
                                            let dateString = Global().convertDate("\(dictTemp.value(forKey: "FromDate")!)")
                                            
                                            let dateFormatter = DateFormatter()
                                            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                                            dateFormatter.dateFormat = "MM/dd/yyyy"
                                            
                                            // let date = Global().getOnlyDate(dateFormatter.date(from: dateString!))
                                            
                                            dictTemp.setValue(dateFormatter.date(from: dateString!), forKey: "FromDate")
                                            self.aryActivitiesCopy.add(dictTemp)
                                        }
                                    }
                                    
                                    self.applyFilter()
                                    
                                    let diffGlobal = CFAbsoluteTimeGetCurrent() - startGlobal
                                    print("Took \(diffGlobal) seconds to get Data OverAll To Render From SQL Server. Count of Activity :-- \(arrayTemp.count)")
                                    
                                }
                                else
                                {
                                    self.tblviewActivity.reloadData()
                                }
                            }
                            else
                            {
                                self.tblviewActivity.reloadData()
                            }
                        }
                        else
                        {
                            self.tblviewActivity.reloadData()
                        }
                    }
                    
                    else{
                        
                        
                        if(self.aryAllKeys.count == 0)
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                        }
                    }
                }
                else
                {
                    
                    
                    self.tblviewActivity.reloadData()
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            
        }
    }
    
    // MARK: Core Date Methods
    
    /*  fileprivate func fetchActivityListFromLocalDB()
     {
     
     aryActivities.removeAllObjects()
     aryActivitiesCopy.removeAllObjects()
     
     var arrayTemp = NSArray()
     
     if(strFromVC == "WebLeadVC" || strFromVC == "OpportunityVC")
     {
     arrayTemp = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND refType == %@",strUserName,strTypeOfActivityToFetch))
     }
     else
     {
     arrayTemp = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@",strUserName))
     
     }
     
     if(arrayTemp.count > 0)
     {
     for item in arrayTemp
     {
     aryActivities.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
     
     }
     
     // print(arrayTemp)
     aryActivitiesCopy = aryActivities.mutableCopy() as! NSMutableArray
     
     var aryForSort = NSMutableArray()
     aryForSort =  aryActivities.mutableCopy() as! NSArray as! NSMutableArray
     
     if let status = nsud.value(forKey: "fromActivityFilter")
     {
     print(status as! Bool)
     let dictForSort = (nsud.value(forKey: "dictFilterActivity") as! NSDictionary).mutableCopy()as! NSMutableDictionary
     
     if("\(dictForSort.value(forKey: "user")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "user")!)" == "Everyone")
     {// do nothing
     
     }
     else{// filter according to participants
     
     let tempAry = aryForSort.mutableCopy() as! NSMutableArray
     aryForSort.removeAllObjects()
     
     for item in tempAry
     {
     if("\((item as! NSDictionary).value(forKey: "participants")!)".count > 0)
     {
     let aryPerticipants = "\((item as! NSDictionary).value(forKey: "participants")!)".components(separatedBy: ",")
     
     for innerItem in aryPerticipants
     {
     if("\(innerItem)" == strEmpNumber)
     {
     
     aryForSort.add((item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
     break
     }
     }
     }
     else
     {
     aryForSort.add((item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
     }
     
     }
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     "\((dict as! NSDictionary).value(forKey: "participants")!)" == "\(strEmpNumber)"
     } as! [NSMutableDictionary]
     
     // print(filterAry)
     
     
     if("\(dictForSort.value(forKey: "fromDate")!)".count > 0 && "\(dictForSort.value(forKey: "toDate")!)".count > 0)
     {
     let strFromDate = "\(dictForSort.value(forKey: "fromDate")!)"
     
     let strToDate = "\(dictForSort.value(forKey: "toDate")!)"
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     
     let date1 = dateFormatter.date(from: strFromDate)
     let date2 = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date >= fromDate!) && toDate! >= ((dict as! NSDictionary).value(forKey: "compareDate") as! Date)
     } as! [NSMutableDictionary]
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     /*   if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     
     else if("\(dictForSort.value(forKey: "sortby")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "sortby")!)" == "A to Z")
     {
     let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "agenda", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
     
     filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     
     
     dictGroupedActivity = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
     
     (($0["agenda"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
     
     
     }
     else if("\(dictForSort.value(forKey: "sortby")!)" == "Z to A")
     {
     let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "agenda", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
     
     filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     
     dictGroupedActivity = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
     
     (($0["agenda"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
     
     }
     
     tblviewActivity.reloadData()
     
     /*if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     
     else if let logTypeId = dictForSort.value(forKey: "logTypeId")
     {
     
     if(logTypeId is NSArray)
     {
     let arytemp = NSMutableArray()
     for itemLogTypeId in logTypeId as! NSArray
     {
     
     arytemp.add(aryForSort.filter { (dict) -> Bool in
     
     "\((dict as! NSDictionary).value(forKey: "logTypeId")!)" == "\(itemLogTypeId)"
     } as! [NSMutableDictionary])
     }
     
     filterAry.removeAll()
     for item in arytemp
     {
     for innerItem in item as! [NSMutableDictionary]
     {
     filterAry.append(innerItem)
     }
     }
     
     filterAryCopy = filterAry
     
     dictGroupedActivity = Dictionary(grouping: filterAryCopy) { $0["logTypeId"] as! String }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! String) ) > (($1 as! String) ) })
     
     tblviewActivity.reloadData()
     
     /* if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     }
     
     else if("\(dictForSort.value(forKey: "days")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "days")!)" == "Today")
     {
     let today = Global().getCurrentDateAkshay()
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date == today!)
     } as! [NSMutableDictionary]
     
     dictGroupedActivity = Dictionary(grouping: filterAry) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     tblviewActivity.reloadData()
     
     /*if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     if("\(dictForSort.value(forKey: "days")!)" == "Tomorrow")
     {
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     
     //  let tmrwDate = Global().getOnlyDate(dateFormatter.date(from: getTomorrowDate()))
     
     let tmrwDate = Global().getOnlyDate(getTomorrowDate())
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date == tmrwDate!)
     } as! [NSMutableDictionary]
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     /*  if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     if("\(dictForSort.value(forKey: "days")!)" == "This week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("This Week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     
     let date1 = dateFormatter.date(from: strFromDate)
     let date2 = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date > fromDate!) && toDate! > ((dict as! NSDictionary).value(forKey: "compareDate") as! Date)
     } as! [NSMutableDictionary]
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     tblviewActivity.reloadData()
     
     /*   if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }
     */
     }
     }
     if("\(dictForSort.value(forKey: "days")!)" == "Next week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("Next Week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     let date1 = dateFormatter.date(from: strFromDate)
     let date2  = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date > fromDate!) && toDate! > ((dict as! NSDictionary).value(forKey: "compareDate") as! Date)
     } as! [NSMutableDictionary]
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     /*  if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     
     
     }
     }
     }
     
     else{
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     
     /*  if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     }
     }
     
     else if("\(dictForSort.value(forKey: "fromDate")!)".count > 0 && "\(dictForSort.value(forKey: "toDate")!)".count > 0)
     {
     let strFromDate = "\(dictForSort.value(forKey: "fromDate")!)"
     
     let strToDate = "\(dictForSort.value(forKey: "toDate")!)"
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     
     let date1 = dateFormatter.date(from: strFromDate)
     let date2 = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date >= fromDate!) && toDate! >= ((dict as! NSDictionary).value(forKey: "compareDate") as! Date)
     } as! [NSMutableDictionary]
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     /*     if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     
     }
     
     else if("\(dictForSort.value(forKey: "sortby")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "sortby")!)" == "A to Z")
     {
     let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "agenda", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
     
     filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     
     dictGroupedActivity = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
     
     (($0["agenda"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
     
     tblviewActivity.reloadData()
     
     /*   if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     else if("\(dictForSort.value(forKey: "sortby")!)" == "Z to A")
     {
     let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "agenda", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
     
     filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     
     dictGroupedActivity = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
     
     (($0["agenda"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
     
     tblviewActivity.reloadData()
     /*   if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     
     
     }
     }
     else if let logTypeId = dictForSort.value(forKey: "logTypeId")
     {
     if(logTypeId is NSArray)
     {
     let arytemp = NSMutableArray()
     for itemLogTypeId in logTypeId as! NSArray
     {
     arytemp.add(aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "logTypeId") as! String == "\(itemLogTypeId)")
     } as! [NSMutableDictionary])
     
     //("\((dict as! NSDictionary).value(forKey: "logTypeId")!)" == "\(itemLogTypeId)")
     //} as! [NSMutableDictionary])
     }
     
     filterAry.removeAll()
     
     for item in arytemp
     {
     for innerItem in item as! [NSMutableDictionary]
     {
     filterAry.append(innerItem)
     }
     }
     
     filterAryCopy = filterAry
     
     dictGroupedActivity = Dictionary(grouping: filterAryCopy) { $0["logTypeId"] as! String }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! String) ) > (($1 as! String) ) })
     
     tblviewActivity.reloadData()
     
     /*   if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     }
     else if("\(dictForSort.value(forKey: "days")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "days")!)" == "Today")
     {
     let today = Global().getCurrentDateAkshay()
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date == today!)
     } as! [NSMutableDictionary]
     
     dictGroupedActivity = Dictionary(grouping: filterAry) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     tblviewActivity.reloadData()
     /*  if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     if("\(dictForSort.value(forKey: "days")!)" == "Tomorrow")
     {
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     
     //  let tmrwDate = Global().getOnlyDate(dateFormatter.date(from: getTomorrowDate()))
     
     let tmrwDate = Global().getOnlyDate(getTomorrowDate())
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date == tmrwDate!)
     } as! [NSMutableDictionary]
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     /* if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     if("\(dictForSort.value(forKey: "days")!)" == "This week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("This Week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     
     let date1 = dateFormatter.date(from: strFromDate)
     let date2 = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date > fromDate!) && toDate! > ((dict as! NSDictionary).value(forKey: "compareDate") as! Date)
     } as! [NSMutableDictionary]
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     
     /* if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     }
     if("\(dictForSort.value(forKey: "days")!)" == "Next week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("Next Week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     let date1 = dateFormatter.date(from: strFromDate)
     let date2  = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date > fromDate!) && toDate! > ((dict as! NSDictionary).value(forKey: "compareDate") as! Date)
     } as! [NSMutableDictionary]
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     tblviewActivity.reloadData()
     
     /*  if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     }
     }
     
     }
     
     else
     {
     let sortedArray = aryForSort.sorted(by: {
     (($0 as! NSMutableDictionary)["compareDate"] as? Date)! > (($1 as! NSMutableDictionary)["compareDate"] as? Date)!
     })
     
     filterAry = sortedArray as! [NSMutableDictionary]
     var arr = [NSMutableDictionary]()
     
     arr = sortedArray as! [NSMutableDictionary]
     
     dictGroupedActivity = Dictionary(grouping: arr) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     
     /* if(aryAllKeys.count > 0)
     {
     tblviewActivity.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     
     }
     
     
     
     }
     else
     {
     // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
     }
     }
     
     fileprivate func filterAndReloadSearchedData(aryTemp : [NSMutableDictionary])
     {
     if let status = nsud.value(forKey: "fromActivityFilter")
     {
     print(status as! Bool)
     let dictForSort = (nsud.value(forKey: "dictFilterActivity") as! NSDictionary).mutableCopy()as! NSMutableDictionary
     
     if("\(dictForSort.value(forKey: "user")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "user")!)" == "Everyone")
     {// do nothing
     
     }
     else{// filter according to assign to == employeeId
     
     if("\(dictForSort.value(forKey: "fromDate")!)".count > 0 && "\(dictForSort.value(forKey: "toDate")!)".count > 0)
     {
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     }
     else if("\(dictForSort.value(forKey: "sortby")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "sortby")!)" == "A to Z")
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp ) {
     
     (($0["Agenda"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
     
     
     }
     else if("\(dictForSort.value(forKey: "sortby")!)" == "Z to A")
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp ) {
     
     (($0["Agenda"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
     
     }
     
     tblviewActivity.reloadData()
     }
     else if let logTypeId = dictForSort.value(forKey: "logTypeId")
     {
     
     if(logTypeId is NSArray)
     {
     let arytemp1 = NSMutableArray()
     for itemLogTypeId in logTypeId as! NSArray
     {
     
     arytemp1.add(aryTemp.filter { (dict) -> Bool in
     
     "\((dict as NSDictionary).value(forKey: "LogTypeId")!)" == "\(itemLogTypeId)"
     } )
     }
     
     filterAryCopy.removeAll()
     for item in arytemp1
     {
     for innerItem in item as! [NSMutableDictionary]
     {
     filterAryCopy.append(innerItem)
     }
     }
     
     dictGroupedActivity = Dictionary(grouping: filterAryCopy) { $0["LogTypeId"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     }
     }
     
     else if("\(dictForSort.value(forKey: "days")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "days")!)" == "Today")
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     tblviewActivity.reloadData()
     
     }
     if("\(dictForSort.value(forKey: "days")!)" == "Tomorrow")
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     }
     if("\(dictForSort.value(forKey: "days")!)" == "This week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("This week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     
     }
     }
     if("\(dictForSort.value(forKey: "days")!)" == "Next week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("Next Week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     
     }
     }
     }
     else
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     tblviewActivity.reloadData()
     
     }
     }
     }
     
     else if("\(dictForSort.value(forKey: "fromDate")!)".count > 0 && "\(dictForSort.value(forKey: "toDate")!)".count > 0)
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     }
     
     else if("\(dictForSort.value(forKey: "sortby")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "sortby")!)" == "A to Z")
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp ) {
     
     (($0["Agenda"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
     
     
     }
     else if("\(dictForSort.value(forKey: "sortby")!)" == "Z to A")
     {
     dictGroupedActivity = Dictionary(grouping: aryTemp ) {
     
     (($0["Agenda"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
     
     }
     }
     else if let logTypeId = dictForSort.value(forKey: "logTypeId")
     {
     if(logTypeId is NSArray)
     {
     let arytemp1 = NSMutableArray()
     for itemLogTypeId in logTypeId as! NSArray
     {
     arytemp1.add(aryTemp.filter { (dict) -> Bool in
     
     "\((dict as NSDictionary).value(forKey: "LogTypeId")!)" == "\(itemLogTypeId)"
     } )
     }
     
     filterAryCopy.removeAll()
     for item in arytemp1
     {
     for innerItem in item as! [NSMutableDictionary]
     {
     filterAryCopy.append(innerItem)
     }
     }
     
     dictGroupedActivity = Dictionary(grouping: filterAryCopy) { $0["LogTypeId"] as! String }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     // aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     
     tblviewActivity.reloadData()
     }
     }
     else if("\(dictForSort.value(forKey: "days")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "days")!)" == "Today")
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     }
     if("\(dictForSort.value(forKey: "days")!)" == "Tomorrow")
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     }
     if("\(dictForSort.value(forKey: "days")!)" == "This week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("This Week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     }
     }
     if("\(dictForSort.value(forKey: "days")!)" == "Next week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("Next Week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     }
     }
     }
     }
     else
     {
     dictGroupedActivity = Dictionary(grouping: aryTemp) { $0["ModifiedDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedActivity.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     }
     
     tblviewActivity.reloadData()
     }
     */
    // MARK: UITableView's delegate and datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return aryAllKeys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let key = aryAllKeys[section]
        
        let valueForKey = dictGroupedActivity[key as! AnyHashable]
        return (valueForKey?.count)!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: DeviceType.IS_IPAD ? 50 : 40))
        let lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: DeviceType.IS_IPAD ? 50 : 40))
        lblTitle.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 18)
        
        lblTitle.textAlignment = .center
        lblTitle.backgroundColor = hexStringToUIColor(hex: "EFEFF4")
        viewHeader.addSubview(lblTitle)
        
        let key = aryAllKeys[section]
        
        if((key as? Date) != nil)
        {
            let strDate = changeDateToString(date: key as! Date)
            
            lblTitle.text = strDate
        }
        if((key as? Character) != nil)
        {
            lblTitle.text = "\(key)".uppercased()
        }
        if((key as? String) != nil)
        {
            switch sortBy {
            case .logType:
                lblTitle.text = getLogTypeNameFromLogTypeId(logTypeID: "\(key)")
                
            default:
                print("")
            }
        }
        return viewHeader
    }
    
    /*func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
     return UIView()
     }*/
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1))
        viewFooter.backgroundColor = UIColor.lightGray
        return viewFooter
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 50 : 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellActivity") as! LeadVCCell
        
        let key = aryAllKeys[indexPath.section]
        let valueForKey = dictGroupedActivity[key as! AnyHashable]
        let dictData = valueForKey![indexPath.row]
        
        // showing activity name
        cell.lblOpportunityName.text = "\(dictData.value(forKey: "Agenda")!)"
        
        // showing activity log type
        for item in aryLogTypeMaster
        {
            if("\(dictData.value(forKey: "LogTypeId")!)" == "\((item as! NSDictionary).value(forKey: "LogTypeId")!)" )
            {
                cell.lblLogType.text = "Activity Type:" + " " + "\((item as! NSDictionary).value(forKey: "Name")!)" + "  \u{2022}  "
                break
            }
        }
        
        // showing activity date
        if let fromDate = dictData.value(forKey: "FromDate"){
            
            if("\(fromDate)".count > 0) && ("\(fromDate)" != "<null>"){
                
                
                
                let strDate  = "Date:" + " " +  Global().convertDate("\(dictData.value(forKey: "FromDate")!)") + "  \u{2022}  "
                
                cell.lblLogType.text = cell.lblLogType.text! + strDate
                
            }
        }
        
        cell.btnComment.tag = (indexPath.section * 1000) + indexPath.row
        cell.btnComment.addTarget(self, action: #selector(actionOnComment), for: .touchUpInside)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: false)
        
        let key = aryAllKeys[indexPath.section] as! AnyHashable
        let valueForKey = dictGroupedActivity[key]
        
        let dictData = valueForKey![indexPath.row]
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityDetailsVC_CRMNew_iPhone") as! ActivityDetailsVC_CRMNew_iPhone
        
        
        vc.activityID = "\(dictData.value(forKey: "ActivityId")!)"
        vc.strFromVC = strFromVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    // MARK: UIButton action
    
    @objc func actionOnComment(sender: UIButton!)
    {
        let section = sender.tag / 1000
        let row = sender.tag % 1000
        
        let key = aryAllKeys[section] as! AnyHashable
        
        let valueForKey = dictGroupedActivity[key]
        
        let dictData = valueForKey![row]
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "CommentList_iPhoneVC") as! CommentList_iPhoneVC
        
        controller.iD_PreviousView = "\(dictData.value(forKey: "ActivityId")!)"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func actionOnTask(_ sender: UIButton) {
        
        /*for controller in self.navigationController!.viewControllers as Array { if controller.isKind(of: TaskVC_CRMNew_iPhone.self) {
         
         self.navigationController!.popToViewController(controller, animated: true)
         
         break
         
         }
         
         }*/
        
        var isVCFound = false
        
        var controllerVC = UIViewController()
        
        for controller in self.navigationController!.viewControllers as Array { if controller.isKind(of: TaskVC_CRMNew_iPhone.self) {
            
            isVCFound = true
            controllerVC = controller
            
        }
        
        }
        
        if isVCFound == true {
            
            self.navigationController?.popToViewController(controllerVC, animated: false)
            
        }
        
        if(isVCFound == false)
        {
            let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
            
            testController.strFromVC = strFromVC
            self.navigationController?.pushViewController(testController, animated: false)
        }
        
    }
    
    @IBAction func actionOnActivity(_ sender: UIButton) {
        // kuch nh karna yaha, baad me ye action delete kar dena
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        GotoFilerViewController()
    }
    
    @IBAction func actionOnSearch(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3) {
            // self.hghtConstSearchBar.constant = 56.0
        }
    }
    
    @IBAction func actionOnAddActivity(_ sender: UIButton) {
        self.GotoAddActivityViewController()
        
    }
    
    @IBAction func actionOnDashboard(_ sender: UIButton) {
        
        self.GotoDashboardViewController()
        
    }
    
    @IBAction func actionOnLeadOpportunity(_ sender: UIButton) {
        
        self.GotoLeadViewController()
        
    }
    
    @IBAction func actionOnAppointment(_ sender: UIButton) {
        
        self.GotoScheduleViewController()
    }
    
    
    
    @IBAction func actionOnNearBy(_ sender: UIButton)
    {
        /*
         let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
         
         let vc = storyboardIpad.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as! NearByMeViewControlleriPhone
         
         self.navigationController?.pushViewController(vc, animated: false)*/
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    @IBAction func actionOnContactList(_ sender: UIButton)
    {
        self.GotoAccountViewController()
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        
        let Near = (UIAlertAction(title: enumNearBy, style: .default , handler:{ (UIAlertAction)in
            /*
             if(DeviceType.IS_IPAD){
             let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
             self.navigationController?.pushViewController(vc!, animated: false)
             }else{
             let storyboard = UIStoryboard(name: "Main", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
             self.navigationController?.pushViewController(vc!, animated: false)
             }*/
            self.GotoMapViewController()
        }))
        
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
        Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Near)
        
        
        
        let Signed = (UIAlertAction(title: enumSignedAgreement, style: .default , handler:{ (UIAlertAction)in
            
            self.GotoSignAgreementViewController()
            
        }))
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
        Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Signed)
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    @IBAction func actionOnSegmentControl(_ sender: UISegmentedControl) {
        
        if(sender.selectedSegmentIndex == 0){
            
            searchBarActivity.text = nil
            filterType = FilterType.all
            applyFilter()
            return
        }
        if(sender.selectedSegmentIndex == 1){
            
            searchBarActivity.text = nil
            filterType = FilterType.upcoming
            applyFilter()
            return
        }
        if(sender.selectedSegmentIndex == 2){
            
            searchBarActivity.text = nil
            filterType = FilterType.overdue
            applyFilter()
            return
        }
        if(sender.selectedSegmentIndex == 3){
            
            switch filterType {
            case .all:
                segmentControlActivity.selectedSegmentIndex = 0
                
            case .upcoming:
                segmentControlActivity.selectedSegmentIndex = 1
                
            case .overdue:
                segmentControlActivity.selectedSegmentIndex = 2
            }
            
            let alertController = UIAlertController(title: "Sort By", message: "", preferredStyle: .actionSheet)
            alertController.view.tintColor = UIColor.black
            let dateAction = UIAlertAction(title: "Date", style: .default) { (action) in
                
                self.searchBarActivity.text = nil
                self.sortBy = SortBy.date
                self.applySorting(aryFiltered: self.aryActivities)
            }
            
            let logtypeAction = UIAlertAction(title: "Activity Type", style: .default) { (action) in
                
                self.searchBarActivity.text = nil
                self.sortBy = SortBy.logType
                self.applySorting(aryFiltered: self.aryActivities)
            }
            
            let azAction = UIAlertAction(title: "A-Z", style: .default) { (action) in
                
                self.searchBarActivity.text = nil
                self.sortBy = SortBy.aToz
                self.applySorting(aryFiltered: self.aryActivities)
            }
            let zaAction = UIAlertAction(title: "Z-A", style: .default) { (action) in
                
                self.searchBarActivity.text = nil
                self.sortBy = SortBy.zToa
                self.applySorting(aryFiltered: self.aryActivities)
            }
            let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel) { (action) in
                
                self.dismiss(animated: false, completion: nil)
            }
            
            alertController.addAction(dateAction)
            alertController.addAction(logtypeAction)
            alertController.addAction(azAction)
            alertController.addAction(zaAction)
            alertController.addAction(dismissAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: Functions
    
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
            lbltaskCount.isHidden = true
            lblScheduleCount.isHidden = true
        }
        
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }
                    
                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                            lblScheduleCount.text = strScheduleCount
                            lblScheduleCount.isHidden = false
                        }else{
                            let badgeSchedule = SPBadge()
                            badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                            badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeSchedule.badge = strScheduleCount
                            btnScheduleFooter.addSubview(badgeSchedule)
                        }
                        
                        
                    }
                    
                    
                    if strTaskCount.count > 0 {
                        
                        if(DeviceType.IS_IPAD){
                            lbltaskCount.text = strTaskCount
                            lbltaskCount.isHidden = false
                            
                        }else{
                            let badgeTasks = SPBadge()
                            badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                            badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeTasks.badge = strTaskCount
                            btnTasksFooter.addSubview(badgeTasks)
                        }
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    func catchNotification(notification:Notification) {
        
        UserDefaults.standard.set(true, forKey: "isActivityAddedUpdated")
        
        /*if (isInternetAvailable()){
         
         FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
         
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
         
         self.callAPIToGetActivityList()
         
         })
         
         }else{
         
         showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
         
         }*/
        
    }
    fileprivate func setUpViews(){
        
        viewBtnContainer.layer.cornerRadius = viewBtnContainer.frame.size.height/2.0
        viewBtnContainer.layer.masksToBounds = true
        
        btnTask.layer.cornerRadius = btnTask.frame.size.height/2.0
        btnTask.layer.masksToBounds = true
        btnActivity.layer.cornerRadius = btnActivity.frame.size.height/2.0
        btnActivity.layer.masksToBounds = true
        
        if let txfSearchField = searchBarActivity.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .none
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
        }
        searchBarActivity.layer.borderColor = hexStringToUIColor(hex: appThemeColor).cgColor
        searchBarActivity.layer.borderWidth = 1
        searchBarActivity.layer.opacity = 1.0
    }
    
    func changeDateToString(date: Date)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    func getTomorrowDate()-> Date
    {
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        return tomorrow!
    }
    
    fileprivate func getLogTypeFromMaster()
    {
        
        let dict = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        
        if(dict.count > 0)
        {
            let aryTemp = (dict.value(forKey: "ActivityLogTypeMasters") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in aryTemp
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    aryLogTypeMaster.add((item as! NSDictionary))
                }
            }
            
            print(aryLogTypeMaster)
        }
    }
    
    fileprivate func getLogTypeNameFromLogTypeId(logTypeID: String)-> String{
        
        var name = logTypeID
        
        for item in aryLogTypeMaster
        {
            let dict = item as! NSDictionary
            if("\(dict.value(forKey: "LogTypeId")!)" == logTypeID)
            {
                name = "\(dict.value(forKey: "Name")!)"
                break
            }
        }
        
        return name
        
    }
    
    fileprivate func applyFilter(){
        
        switch filterType {
        
        case .all:
            
            aryActivities = aryActivitiesCopy as! [NSMutableDictionary]
            applySorting(aryFiltered: aryActivities)
            return
            
        case .upcoming:
            
            aryActivities = aryActivitiesCopy as! [NSMutableDictionary]
            let aryLocal = aryActivities
            aryActivities = aryLocal.filter { (dict) -> Bool in
                
                return  ((dict as NSDictionary).value(forKey: "FromDate") as! Date >= getTodaysDate())
            }
            
            applySorting(aryFiltered: aryActivities)
            return
            
            
        case .overdue:
            
            aryActivities = aryActivitiesCopy as! [NSMutableDictionary]
            let aryLocal = aryActivities
            
            aryActivities = aryLocal.filter { (dict) -> Bool in
                
                return  ( getTodaysDate() > (dict as NSDictionary).value(forKey: "FromDate") as! Date)
            }
            
            applySorting(aryFiltered: aryActivities)
            return
        }
        
        /*aryActivitiesCopy = arrayForFilter.mutableCopy() as! NSMutableArray
         
         var aryForSort = NSMutableArray()
         aryForSort = arrayForFilter.mutableCopy() as! NSMutableArray
         
         if let status = nsud.value(forKey: "fromActivityFilter")
         {
         print(status as! Bool)
         let dictForSort = (nsud.value(forKey: "dictFilterActivity") as! NSDictionary).mutableCopy()as! NSMutableDictionary
         
         if("\(dictForSort.value(forKey: "sortby")!)".count > 0)
         {
         if("\(dictForSort.value(forKey: "sortby")!)" == "A to Z")
         {
         let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "Agenda", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
         
         filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]
         
         filterAryCopy = filterAry
         
         dictGroupedActivity = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
         
         (($0["Agenda"] as! String).lowercased()).first
         }
         
         aryAllKeys = Array(dictGroupedActivity.keys)
         let aryAllKeysCopy = aryAllKeys
         
         aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
         
         tblviewActivity.reloadData()
         }
         else if("\(dictForSort.value(forKey: "sortby")!)" == "Z to A")
         {
         let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "Agenda", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
         
         filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]
         
         filterAryCopy = filterAry
         
         dictGroupedActivity = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
         
         (($0["Agenda"] as! String).lowercased()).first
         }
         
         aryAllKeys = Array(dictGroupedActivity.keys)
         let aryAllKeysCopy = aryAllKeys
         
         aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
         
         tblviewActivity.reloadData()
         }
         }
         else if let logTypeId = dictForSort.value(forKey: "LogTypeId")
         {
         if(logTypeId is NSArray)
         {
         let arytemp = NSMutableArray()
         for itemLogTypeId in logTypeId as! NSArray
         {
         arytemp.add(aryForSort.filter { (dict) -> Bool in
         
         return  ((dict as! NSDictionary).value(forKey: "LogTypeId") as! String == "\(itemLogTypeId)")
         } as! [NSMutableDictionary])
         }
         
         filterAry.removeAll()
         
         for item in arytemp
         {
         for innerItem in item as! [NSMutableDictionary]
         {
         filterAry.append(innerItem)
         }
         }
         
         filterAryCopy = filterAry
         
         dictGroupedActivity = Dictionary(grouping: filterAryCopy) { $0["LogTypeId"] as! String }
         
         aryAllKeys = Array(dictGroupedActivity.keys)
         
         let aryAllKeysCopy = aryAllKeys
         
         aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! String) ) > (($1 as! String) ) })
         
         tblviewActivity.reloadData()
         
         }
         }
         }
         else
         {
         let sortedArray = aryForSort.sorted(by: {
         (($0 as! NSMutableDictionary)["ModifiedDate"] as? Date)! > (($1 as! NSMutableDictionary)["ModifiedDate"] as? Date)!
         })
         
         filterAry = sortedArray as! [NSMutableDictionary]
         var arr = [NSMutableDictionary]()
         
         arr = sortedArray as! [NSMutableDictionary]
         
         dictGroupedActivity = Dictionary(grouping: arr) { $0["ModifiedDate"] as! Date }
         
         aryAllKeys = Array(dictGroupedActivity.keys)
         
         let aryAllKeysCopy = aryAllKeys
         
         aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
         
         tblviewActivity.reloadData()
         }*/
    }
    
    fileprivate func applySorting(aryFiltered:[NSMutableDictionary]){
        
        switch sortBy {
        case .date:
            
            let arrLoc = aryFiltered.filter { (dict) -> Bool in
                
                return  "\(dict.value(forKey: "FromDate")!)" != "<null>" &&  "\(dict.value(forKey: "FromDate")!)" != " " && "\(dict.value(forKey: "FromDate")!)".count > 0
            }
            
            let sortedArray = arrLoc.sorted(by: {
                (($0 as NSMutableDictionary)["FromDate"] as? Date)! >= (($1 as NSMutableDictionary)["FromDate"] as? Date)!
            })
            
            dictGroupedActivity = Dictionary(grouping: sortedArray) { $0["FromDate"] as! Date }
            
            aryAllKeys = Array(dictGroupedActivity.keys)
            
            let aryAllKeysCopy = aryAllKeys
            
            aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
            
            tblviewActivity.reloadData()
            return
            
            
        case .logType:
            
            let arr = aryFiltered.filter { (dict) -> Bool in
                
                return  "\((dict as NSDictionary).value(forKey: "LogTypeId")!)" != "<null>" &&  "\((dict as NSDictionary).value(forKey: "LogTypeId")!)" != " " && "\((dict as NSDictionary).value(forKey: "LogTypeId")!)".count > 0
            }
            
            dictGroupedActivity = Dictionary(grouping: arr) { "\($0["LogTypeId"]!)" }
            
            aryAllKeys = Array(dictGroupedActivity.keys)
            
            tblviewActivity.reloadData()
            
            return
            
        case .aToz:
            
            let arr = aryFiltered.filter { (dict) -> Bool in
                
                return  "\((dict as NSDictionary).value(forKey: "Agenda")!)" != "<null>" &&  "\((dict as NSDictionary).value(forKey: "Agenda")!)" != " " && "\((dict as NSDictionary).value(forKey: "Agenda")!)".count > 0
            }
            
            dictGroupedActivity = Dictionary(grouping: arr ) {
                
                (($0["Agenda"] as! String).lowercased()).first
            }
            
            aryAllKeys = Array(dictGroupedActivity.keys)
            let aryAllKeysCopy = aryAllKeys
            
            aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
            tblviewActivity.reloadData()
            
            return
        case .zToa:
            
            let arr = aryFiltered.filter { (dict) -> Bool in
                
                return  "\((dict as NSDictionary).value(forKey: "Agenda")!)" != "<null>" &&  "\((dict as NSDictionary).value(forKey: "Agenda")!)" != " " && "\((dict as NSDictionary).value(forKey: "Agenda")!)".count > 0
            }
            
            dictGroupedActivity = Dictionary(grouping: arr ) {
                
                (($0["Agenda"] as! String).lowercased()).first
            }
            
            aryAllKeys = Array(dictGroupedActivity.keys)
            let aryAllKeysCopy = aryAllKeys
            
            aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
            tblviewActivity.reloadData()
            
            return
        }
    }
    
    // MARK: UISearchBar delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchAutocomplete(searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.view.endEditing(true)
        if(searchBar.text?.count == 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter text to be searched.", viewcontrol: self)
        }

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBarActivity.text = ""
        applySorting(aryFiltered: aryActivities)
        self.view.endEditing(true)
      
    }
    func searchAutocomplete(searchText: String) -> Void{
        if searchText.isEmpty {
            
            applySorting(aryFiltered: aryActivities)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.view.endEditing(true)
            }
        }
        
        else
        {
            let aryTemp = aryActivities.filter { (activity) -> Bool in
                
                return "\((activity as NSDictionary).value(forKey: "Agenda")!)".lowercased().contains(searchText.lowercased()) || "\((activity as NSDictionary).value(forKey: "ActivityId")!)".lowercased().contains(searchText.lowercased())}
            applySorting(aryFiltered: aryTemp)

//            if(aryTemp.count > 0)
//            {
//                applySorting(aryFiltered: aryTemp)
//            }
//            else
//            {
//                applySorting(aryFiltered: aryActivities)
//            }
        }
    }
}
extension ActivityVC_CRMNew_iPhone : StasticsClassDelegate
{
    func getData(_ dict: [AnyHashable : Any]!)
    {
        let dictNew = dict! as NSDictionary
        
        strFromDate = "\(dictNew.value(forKey: "StartDate") ?? "")"
        strToDate = "\(dictNew.value(forKey: "EndDate") ?? "")"
    }
}


// MARK: -
// MARK: - FilterActivity_iPhone_Protocol
extension ActivityVC_CRMNew_iPhone: FilterActivity_iPhone_Protocol
{
    func getDataFilterActivity_iPhone_Protocol(dictData: NSDictionary, tag: Int) {
        if(tag == 0){ // Filter
            if (isInternetAvailable()){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    nsud.setValue(dictData, forKey: "dictFilterActivity")
                    nsud.synchronize()
                    self.callAPIToGetActivityList()
                })
            }else{
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
            }
            
        }
    }
}

// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension ActivityVC_CRMNew_iPhone : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        strSearchText = str
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
           self.setTopMenuOption()
            self.searchAutocomplete(searchText: str)
        }
    }
    
    
}
