//
//  FilterLeadVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 29/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

protocol FilterWebLead_iPhone_Protocol : class
{
    func getDataWebLead_iPhone_Protocol(dictData : NSDictionary ,tag : Int)
}

class FilterLeadVC_CRMNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    //Protocol
    weak var delegate: FilterWebLead_iPhone_Protocol?
    
    //Outlets
    @IBOutlet weak var tblviewFilterLead: UITableView!
    
    
    // variables
   // fileprivate  let aryUserName = ["Akshay Hastekar", "EveryOne"] as NSArray
    
    fileprivate  var aryUserName = NSArray()

    
    fileprivate  let aryDays  = ["Yesterday","Today", "This week", "Last week"] as NSArray
    
   // fileprivate  let aryStatus = ["New", "Open","Default","Complete","Incomplete"] as NSArray
    
    fileprivate  var aryStatus = NSArray()

    
    fileprivate  var strFromDate = ""
    fileprivate  var strToDate = ""
    fileprivate  var isFromDate = false
    fileprivate  var aryIndexPath = NSMutableArray()
    
    fileprivate  var aryIndexForStatus = NSMutableArray()
    fileprivate  var arySelectedStatus = NSMutableArray()
    fileprivate  var arySelectedUser = NSMutableArray()
    fileprivate  var arySelectedDay = NSMutableArray()

    fileprivate  var aryPropertyType = NSMutableArray() , arySelectedPropertyType = NSMutableArray()
    fileprivate  var aryUrgencyType = NSMutableArray() , arySelectedUrgencyType = NSMutableArray()

    let dictFilterData = NSMutableDictionary()
    
    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    var dictLoginData = NSDictionary()
    var  isOnlyClearDate = false
    var arrOfStatus = NSMutableArray()
    var arrOfStatusName = NSMutableArray()
    
    var selectionDrop = 0
    
    // MARK: view's life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
        
        aryUserName = ["\(strUserName)", "EveryOne"] as NSArray

        getStatus()
        aryStatus = arrOfStatus.mutableCopy() as! NSArray
        //arySelectedStatus = arrOfStatus.mutableCopy() as! NSMutableArray

        
        //Nilind 20 March 2020
        if nsud.value(forKey: "webLeadFilterData") is NSDictionary
        {
            var dict = NSDictionary()
            dict = nsud.value(forKey: "webLeadFilterData") as! NSDictionary
            
            var dictNew = NSMutableDictionary()
            dictNew = dict.mutableCopy() as! NSMutableDictionary
            
            strFromDate = "\(dictNew.value(forKey: "fromDateFromFilter") ?? "")"
            strToDate = "\(dictNew.value(forKey: "toDateFromFilter") ?? "")"
            
            if dictNew.value(forKey: "statusFromFilter") is NSArray
            {
                let arr1  = dictNew.value(forKey: "statusFromFilter") as! NSArray
                arySelectedStatus = arr1.mutableCopy() as! NSMutableArray
            }
            
            if dictNew.value(forKey: "selectedUserFromFilter") is NSArray
            {
                let arr2  = dictNew.value(forKey: "selectedUserFromFilter") as! NSArray
                arySelectedUser = arr2.mutableCopy() as! NSMutableArray
            }
            
            if dictNew.value(forKey: "selectedDayFromFilter") is NSArray
            {
                let arr3  = dictNew.value(forKey: "selectedDayFromFilter") as! NSArray
                arySelectedDay = arr3.mutableCopy() as! NSMutableArray
            }
            if dictNew.value(forKey: "selectedPropertyTypeFromFilter") is NSArray
            {
                let arr3  = dictNew.value(forKey: "selectedPropertyTypeFromFilter") as! NSArray
                arySelectedPropertyType = arr3.mutableCopy() as! NSMutableArray
            }
            if dictNew.value(forKey: "selectedUrgencyTypeFromFilter") is NSArray
            {
                let arr3  = dictNew.value(forKey: "selectedUrgencyTypeFromFilter") as! NSArray
                arySelectedUrgencyType = arr3.mutableCopy() as! NSMutableArray
            }
        }
        
        self.aryPropertyType = NSMutableArray()
        self.aryPropertyType = self.getAddressTypeFromMasterSalesAutomation()
        self.aryUrgencyType = NSMutableArray()
        self.aryUrgencyType = self.getUrgencyMaster()
        
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: UITableview delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: DeviceType.IS_IPAD ? 55 : 40))
          viewheader.backgroundColor = UIColor.groupTableViewBackground
          
          let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: DeviceType.IS_IPAD ? 55 : 40))
       
        let btnDrop = UIButton(frame: CGRect(x: self.tblviewFilterLead.frame.width - (DeviceType.IS_IPAD ? 65 : 50), y: 0, width: DeviceType.IS_IPAD ? 55 : 40, height: DeviceType.IS_IPAD ? 55 : 40))

        //--------------
        let btnCheckAll = UIButton(frame: CGRect(x: self.tblviewFilterLead.frame.width - (DeviceType.IS_IPAD ? 130 : 100), y: 0, width: DeviceType.IS_IPAD ? 55 : 40, height: DeviceType.IS_IPAD ? 55 : 40))

        let lblAll = UILabel(frame: CGRect(x: self.tblviewFilterLead.frame.width - (DeviceType.IS_IPAD ? 185 : 140) , y: 0, width: DeviceType.IS_IPAD ? 55 : 40, height: DeviceType.IS_IPAD ? 55 : 40))
        
        lblAll.text = "All"
        btnCheckAll.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
        btnCheckAll.tag = section
        btnCheckAll.addTarget(self, action: #selector(actionSelectAllData), for: .touchUpInside)
        
        btnDrop.setImage(UIImage(named: section == selectionDrop ? "drop_down_up_ipad" : "arrow_2"), for: .normal)
        btnDrop.tag = section
        btnDrop.addTarget(self, action: #selector(actionDrop), for: .touchUpInside)

        //---------
        //---------
        if(DeviceType.IS_IPAD){
            lblAll.font = UIFont.systemFont(ofSize: 21)
            lbl.font = UIFont.systemFont(ofSize: 21)
            
        }else{
            lblAll.font = UIFont.systemFont(ofSize: 18)
            lbl.font = UIFont.systemFont(ofSize: 18)
        }
//        if(section == 0)
//        {
//            lbl.text = "User"
//        }
         if(section == 0)
        {
            lbl.text = "Days"
        }
        else if(section == 1)
        {
            lbl.text = "Date"
        }
//        else if(section == 3)
//        {
//            lbl.text = "Status"
//            viewheader.addSubview(btnCheckAll)
//            viewheader.addSubview(lblAll)
//            viewheader.addSubview(btnDrop)
//
//            if(self.aryStatus.count == self.arySelectedStatus.count){
//                btnCheckAll.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
//            }
//        }
        else if(section == 2)
        {
            lbl.text = "Property Type"
            viewheader.addSubview(btnCheckAll)
            viewheader.addSubview(lblAll)
            viewheader.addSubview(btnDrop)

            if(self.aryPropertyType.count == self.arySelectedPropertyType.count){
                btnCheckAll.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            }
        }
        else if(section == 3)
        {
            lbl.text = "Urgency Type"
            viewheader.addSubview(btnCheckAll)
            viewheader.addSubview(lblAll)
            viewheader.addSubview(btnDrop)

            if(self.aryUrgencyType.count == self.arySelectedUrgencyType.count){
                btnCheckAll.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            }
        }
        viewheader.addSubview(lbl)
        return viewheader
    }
    @objc func actionDrop (sender : UIButton){
        if selectionDrop == sender.tag {
            selectionDrop = 0
        }else{
            selectionDrop = sender.tag
        }
        self.tblviewFilterLead.reloadData()
    }
    @objc func actionSelectAllData (sender : UIButton){
//        if(sender.tag == 3) //status
//        {
//            //Uncheck
//            if(sender.currentImage == UIImage(named: "check_box_1New.png")){
//                self.arySelectedStatus  = NSMutableArray()
//                self.arySelectedStatus = self.aryStatus.mutableCopy()as! NSMutableArray
//            }
//                //Check
//            else{
//                self.arySelectedStatus  = NSMutableArray()
//            }
//        }
         if(sender.tag == 2){
            //Uncheck
            if(sender.currentImage == UIImage(named: "check_box_1New.png")){
                self.arySelectedPropertyType  = NSMutableArray()
                self.arySelectedPropertyType = self.aryPropertyType.mutableCopy()as! NSMutableArray
            }
                //Check
            else{
                self.arySelectedPropertyType  = NSMutableArray()
            }
        }
        else if(sender.tag == 3){
            //Uncheck
            if(sender.currentImage == UIImage(named: "check_box_1New.png")){
                self.arySelectedUrgencyType  = NSMutableArray()
                self.arySelectedUrgencyType = self.aryUrgencyType.mutableCopy()as! NSMutableArray
            }
                //Check
            else{
                self.arySelectedUrgencyType  = NSMutableArray()
            }
        }
        self.tblviewFilterLead.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 55 : 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if(section == 0)
//        {
//            return aryUserName.count
//        }
         if(section == 0)
        {
            return aryDays.count
        }
        else if(section == 1)
        {
            return 1
        }
//        else if(section == 3)
//        {
//            return  selectionDrop == section ? aryStatus.count : 0
//        }
        else if(section == 2)
        {
            return  selectionDrop == section ? aryPropertyType.count : 0
        }
        else if(section == 3)
        {
            return  selectionDrop == section ? aryUrgencyType.count : 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTaskCell") as! FilterTaskCell
        
//        if(indexPath.section == 0)
//        {
//            cell.lblFilterName.text =  "\(aryUserName[indexPath.row])"
//
//            if arySelectedUser .contains((aryUserName[indexPath.row]))
//            {
//                cell.accessoryType = .checkmark
//            }
//            else
//            {
//                cell.accessoryType = .none
//            }
//        }
          if(indexPath.section == 0)
        {
            cell.lblFilterName.text =  "\(aryDays[indexPath.row])"
            
            if arySelectedDay .contains((aryDays[indexPath.row]))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        else  if(indexPath.section == 1)
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "FilterTaskDateCell") as! FilterTaskDateCell
            
            cell1.btnFromDate.addTarget(self, action: #selector(actionOnFromDate), for: .touchUpInside)
            
            cell1.btnToDate.addTarget(self, action: #selector(actionOnToDate), for: .touchUpInside)
            
            cell1.btnClearDate.addTarget(self, action: #selector(actionOnClearDate), for: .touchUpInside)

            cell1.txtFromDate.text = strFromDate
            cell1.txtToDate.text = strToDate
            
            cell1.txtFromDate.tag = 100
            cell1.txtToDate.tag = 101
            
            cell1.txtFromDate.addTarget(self, action: #selector(actionOnFromDate), for: .touchDown)
            cell1.txtToDate.addTarget(self, action: #selector(actionOnToDate), for: .touchDown)

            cell1.txtFromDate.inputView = UIView()
            cell1.txtFromDate.delegate = self
            
            cell1.txtToDate.inputView = UIView()
            cell1.txtToDate.delegate = self
            //_txtScheduleStartDate.inputView = [[UIView alloc] init];
               //   _txtScheduleStartDate.delegate = self;

            
            
            return cell1
        }
//        else  if(indexPath.section == 3)
//        {
//            cell.lblFilterName.text =  "\(aryStatus[indexPath.row])"
//
//            if arySelectedStatus .contains((aryStatus[indexPath.row]))
//            {
//                 cell.accessoryType = .checkmark
//            }
//            else
//            {
//                 cell.accessoryType = .none
//            }
//
//
//        }
        else if(indexPath.section == 2)
        {
            let dict = aryPropertyType[indexPath.row] as! NSDictionary
            cell.lblFilterName.text =  "\(dict.value(forKey: "Name")!)"
            
            if arySelectedPropertyType .contains((aryPropertyType[indexPath.row]))
            {
                 cell.accessoryType = .checkmark
            }
            else
            {
                 cell.accessoryType = .none
            }
        }
        else{
            let dict = aryUrgencyType[indexPath.row] as! NSDictionary
            cell.lblFilterName.text =  "\(dict.value(forKey: "Name")!)"
            
            if arySelectedUrgencyType .contains((aryUrgencyType[indexPath.row]))
            {
                 cell.accessoryType = .checkmark
            }
            else
            {
                 cell.accessoryType = .none
            }

        }

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
//        if(indexPath.section == 0)
//        {
//          /*  if arySelectedUser.count > 0
//            {
//                arySelectedUser.removeAllObjects()
//            }
//            arySelectedUser.add(aryUserName.object(at: indexPath.row))*/
//
//
//            let str = aryUserName.object(at: indexPath.row)
//
//            if arySelectedUser.contains(str)
//            {
//                arySelectedUser.remove(str)
//            }
//            else
//            {
//                arySelectedUser.removeAllObjects()
//                arySelectedUser.add(str)
//            }
//            tblviewFilterLead.reloadData()
//        }
         if(indexPath.section == 0)
        {
            if strFromDate.count > 2 || strToDate.count > 2
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please choose either days or date", viewcontrol: self)
                
            }
            else
            {
                let str = aryDays.object(at: indexPath.row)
                
                if arySelectedDay.contains(str)
                {
                    arySelectedDay.remove(str)
                }
                else
                {
                    arySelectedDay.removeAllObjects()
                    arySelectedDay.add(str)
                }
                tblviewFilterLead.reloadData()
               
            }
            
        }
        else if(indexPath.section == 1)
        {
            tblviewFilterLead.reloadData()
        }
//        else if(indexPath.section == 3)
//        {
//            /*if(aryIndexForStatus.contains(indexPath))// if arySelectedStatus .contains((aryStatus[indexPath.row]))
//            {
//                aryIndexForStatus.remove(indexPath)
//                arySelectedStatus.remove(aryStatus.object(at: indexPath.row))
//            }
//            else
//            {
//                aryIndexForStatus.add(indexPath)
//                arySelectedStatus.add(aryStatus.object(at: indexPath.row))
//            }*/
//
//            if arySelectedStatus .contains((aryStatus[indexPath.row]))
//            {
//               // aryIndexForStatus.remove(indexPath)
//                arySelectedStatus.remove(aryStatus.object(at: indexPath.row))
//            }
//            else
//            {
//               // aryIndexForStatus.add(indexPath)
//                arySelectedStatus.add(aryStatus.object(at: indexPath.row))
//            }
//
//
//            tblviewFilterLead.reloadData()
//        }
        else if(indexPath.section == 2)
        {
           
            if arySelectedPropertyType .contains((aryPropertyType[indexPath.row]))
            {
                arySelectedPropertyType.remove(aryPropertyType.object(at: indexPath.row))
            }
            else
            {
                arySelectedPropertyType.add(aryPropertyType.object(at: indexPath.row))
            }
            tblviewFilterLead.reloadData()
        }
        
        else if(indexPath.section == 3)
        {
           
            if arySelectedUrgencyType .contains((aryUrgencyType[indexPath.row]))
            {
                arySelectedUrgencyType.remove(aryUrgencyType.object(at: indexPath.row))
            }
            else
            {
                arySelectedUrgencyType.add(aryUrgencyType.object(at: indexPath.row))
            }
            tblviewFilterLead.reloadData()
        }
        else
        {
            tblviewFilterLead.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    // MARK: UIButton action
    @objc func actionOnClearDate(sender: UIButton!)
    {
        strFromDate = ""
        strToDate = ""
        isOnlyClearDate = true
        tblviewFilterLead.reloadData()
    }
    
    
    @objc func actionOnFromDate(sender: UIButton!) //UIButton
    {
        print("text from date")
        if arySelectedDay.count > 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please choose either days or date", viewcontrol: self)

        }
        else
        {
            isFromDate = true
            gotoDatePickerView(sender: sender, strType: "Date")
        }
     
    }
    @objc func actionOnToDate(sender: UIButton!) //UIButton
    {
        print("text to date")
        if arySelectedDay.count > 0
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please choose either days or date", viewcontrol: self)

        }
        else
        {
            isFromDate = false
            gotoDatePickerView(sender: sender, strType: "Date")
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        nsud.set(true, forKey: "fromWebLeadDetail")
        nsud.set(true, forKey: "onlyPropertyAndUrgencyApplied")
        nsud.synchronize()
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionOnClearAll(_ sender: UIButton) {
        let alert = UIAlertController(title: alertMessage, message: alertFilterClear, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
          // self.delegate?.getDataWebLead_iPhone_Protocol(dictData: NSDictionary(), tag: 0)
            nsud.set(false, forKey: "fromWebLeadFilter")
            nsud.set(NSDictionary(), forKey: "webLeadFilterData")
            nsud.set(nil, forKey: "webLeadFilterData")
            nsud.set(false, forKey: "onlyPropertyAndUrgencyApplied")
            nsud.synchronize()
                    
            
            self.navigationController?.popViewController(animated: false)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func actionOnApply(_ sender: UIButton)
    {
//        if arySelectedStatus.count == 0
//        {
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select at least one status", viewcontrol: self)
//        }
//        else
        //        {
        
        //Date Comparision
        if arySelectedDay.count > 0 || arySelectedUrgencyType.count > 0 || arySelectedPropertyType.count > 0 || strFromDate != "" || strToDate != ""{
            if(self.strFromDate.count > 0 && self.strToDate.count > 0)
            {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                let fromDate = dateFormatter.date(from: self.strFromDate)
                let toDate = dateFormatter.date(from: self.strToDate)
                
                if(fromDate! > toDate!){
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To Date should be greater than From Date.", viewcontrol: self)
                    return
                }
            }
            //End
            
            
//            print("\(aryStatus)")
//            print("\(arySelectedStatus)")
//            print("\(arySelectedPropertyType)")
//            print("\(arySelectedUrgencyType)")
            
            
            dictFilterData.setObject("\(strFromDate)", forKey: "fromDateFromFilter" as NSCopying)
            dictFilterData.setObject("\(strToDate)", forKey: "toDateFromFilter" as NSCopying)
            
            dictFilterData.setObject(arySelectedStatus , forKey: "statusFromFilter" as NSCopying)
            
            // dictFilterData.setObject(arySelectedUser , forKey: "selectedUserFromFilter" as NSCopying)
            
            dictFilterData.setObject(arySelectedDay , forKey: "selectedDayFromFilter" as NSCopying)
            dictFilterData.setObject(arySelectedPropertyType , forKey: "selectedPropertyTypeFromFilter" as NSCopying)
            
            dictFilterData.setObject(arySelectedUrgencyType , forKey: "selectedUrgencyTypeFromFilter" as NSCopying)
            
            nsud.set(true, forKey: "fromWebLeadFilter")
            if  strFromDate == "" && strToDate == "" && arySelectedDay.count == 0 {
                if arySelectedUrgencyType.count > 0  || arySelectedPropertyType.count > 0{
                    if isOnlyClearDate == true{
                        isOnlyClearDate = false
                        nsud.set(false, forKey: "onlyPropertyAndUrgencyApplied")
                    }
                    else{
                        nsud.set(true, forKey: "onlyPropertyAndUrgencyApplied")
                    }
                }
                else{
                    nsud.set(false, forKey: "onlyPropertyAndUrgencyApplied")
                }
            }
            else{
                nsud.set(false, forKey: "onlyPropertyAndUrgencyApplied")
            }
            nsud.set(dictFilterData, forKey: "webLeadFilterData")
            nsud.synchronize()
            
            self.navigationController?.popViewController(animated: false)
            
        }
        else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select at least one category", viewcontrol: self)
        }
  
    }
    
    // MARK: ------Functions------
    func getUrgencyMaster() -> NSMutableArray {
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary {
            let dictLeadDetailMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if dictLeadDetailMaster.value(forKey: "UrgencyMasters") is NSArray {
                let aryTempUrgencyMasters = dictLeadDetailMaster.value(forKey: "UrgencyMasters") as! NSArray
                let aryTemp = aryTempUrgencyMasters.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("1") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("true")
                    
                }
                return (aryTemp as NSArray).mutableCopy() as! NSMutableArray
            }
            return NSMutableArray()

            
        }
        return NSMutableArray()
    }
    
    
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    func getStatus()
    {
        arrOfStatus = NSMutableArray()
        arrOfStatusName = NSMutableArray()
        
        if nsud.value(forKey: "WebLeadStatusMasters") is NSArray
        {
            var arrStatusTemp = NSArray()
            arrStatusTemp = nsud.value(forKey: "WebLeadStatusMasters") as! NSArray
            for item in arrStatusTemp
            {
                let dict = item as! NSDictionary
                
                if ("\(dict.value(forKey: "SysName") ?? "")").caseInsensitiveCompare("void") == .orderedSame
                {
                    
                }
                else
                {
                    arrOfStatus.add("\(dict.value(forKey: "SysName")!)")
                    arrOfStatusName.add("\(dict.value(forKey: "Name")!)")
                    
                }
                
            }
        }
        
    }
    func getAddressTypeFromMasterSalesAutomation() -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    
}

extension FilterLeadVC_CRMNew_iPhone: DatePickerProtocol
{
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if isFromDate == true
        {
            
            strFromDate = strDate
            
            
        }
        else
        {
            strToDate = strDate
        }
        
        tblviewFilterLead.reloadData()
    }
}
extension FilterLeadVC_CRMNew_iPhone: UITextFieldDelegate
{
   /* func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 100
        {
            return false
        }
        else if textField.tag == 101
        {
            return false
        }
        else
        {
            return true
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 100
        {
           
        }
        if textField.tag == 101
        {
           
        }
        return true
    }
 */
}
