//
//  TaskVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 22/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
fileprivate enum FilterType : String{
    
    case  all,upcoming,overdue
}
fileprivate enum SortBy : String{
    
    case  date,priority,taskType,status,aToz,zToa
}
class TaskVC_CRMNew_iPhone: UIViewController,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource {
    
    // outlets
    
    @IBOutlet weak var viewBtnContainer: UIView!
    @IBOutlet weak var btnTask: UIButton!
    @IBOutlet weak var btnActivity: UIButton!
    @IBOutlet weak var searchBarTask: UISearchBar!
    @IBOutlet weak var tblviewTask: UITableView!
    
    @IBOutlet weak var segmentControlTask: UISegmentedControl!
    @IBOutlet weak var hghtConstSearchBar: NSLayoutConstraint!
    @IBOutlet weak var segmentControlActivityHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    
    @IBOutlet weak var headerView: TopView!
    
    // variables
    fileprivate var aryIndexPath = NSMutableArray()
    fileprivate var dictLoginData = NSDictionary()
    fileprivate var aryTasks = [NSMutableDictionary]()
    fileprivate var aryTasksCopy = NSMutableArray()
    fileprivate var strRefID = ""
    fileprivate var strRefType = ""
    fileprivate var strServiceUrlMain = ""
    fileprivate var strEmpID = ""
    @objc var strleadId = ""
    fileprivate var strFromDate = ""
    fileprivate var strToDate = ""
    @objc var strFromVC = "DashBoardView"
    var dictGroupedTask = Dictionary<AnyHashable, [NSMutableDictionary]>()
    var aryAllKeys = Array<Any>()
    var filterAry = [NSMutableDictionary]()
    var filterAryCopy = [NSMutableDictionary]()
    fileprivate let refreshControl = UIRefreshControl()
    fileprivate  var filterType = FilterType.all
    fileprivate  var sortBy = SortBy.date
    fileprivate  var aryTaskTypeMaster = NSMutableArray()
    
    var loader = UIAlertController()
    
    
    
    
    @IBOutlet weak var lbltaskCount: UILabel!
    @IBOutlet weak var lblScheduleCount: UILabel!
    
    var strSearchText = ""

    // MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                lblScheduleCount.text = "0"
                lbltaskCount.layer.cornerRadius = 18.0
                lbltaskCount.backgroundColor = UIColor.red
                lblScheduleCount.layer.cornerRadius = 18.0
                lblScheduleCount.backgroundColor = UIColor.red
                lbltaskCount.layer.masksToBounds = true
                lblScheduleCount.layer.masksToBounds = true
                
            }
            
            if #available(iOS 13.0, *) {
                searchBarTask.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
            } else {
                // Fallback on earlier versions
            }
            
            
            
            tblviewTask.tableFooterView = UIView()
            tblviewTask.estimatedRowHeight = 55.0
            segmentControlTask.isHidden = true
            segmentControlActivityHeight.constant = 0.0
            setUpViews()
            getTaskTypeFromMaster()
            
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
            
            strEmpID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
            
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "TaskAdded_Notification"),
                                                   object: nil,
                                                   queue: nil,
                                                   using:catchNotification)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "TaskFollowUpCreated_Notification"),
                                                   object: nil,
                                                   queue: nil,
                                                   using:catchNotification)
            
            UserDefaults.standard.set(true, forKey: "isTaskAddedUpdated")
            
            //createBadgeView()
        }
        
       
        
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setTopMenuOption()
           self.setFooterMenuOption()
        })
        
      }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !Check_Login_Session_expired() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setTopMenuOption()
                self.setFooterMenuOption()
            })
            
            
            if let isTaskAddedUpdated = nsud.value(forKey: "isTaskAddedUpdated"){
                if(isTaskAddedUpdated is Bool){
                    if((isTaskAddedUpdated as! Bool) == true){
                        if (isInternetAvailable()){
                            UserDefaults.standard.set(false, forKey: "isTaskAddedUpdated")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                                self.callAPIToGetTaskList()
                            })
                        }else{
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                        }
                    }
                }
            }
            
            self.applyFilter()
            
            //----------------------//
        }
       
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchBarTask.text = nil
    }
    
    // MARK: Web Service Calling
    
    // old api
    /* @objc fileprivate func callAPIToGetTaskList()
     {
     if(isInternetAvailable() == false)
     {
     self.refreshControl.endRefreshing()
     tblviewTask.reloadData()
     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
     
     }
     else
     {
     strRefType = "Employee"
     strRefID = strEmpID
     
     if(strFromVC == "WebLeadVC")
     {
     strRefType = "WebLead"
     strRefID = strleadId
     }
     if(strFromVC == "DashBoardView")
     {
     strRefType = "Employee"
     strRefID = strEmpID
     }
     
     if(strFromVC == "OpportunityVC")
     {
     strRefType = "Lead"
     strRefID = strleadId
     // strEmpID
     }
     
     if(strRefID.count == 0)
     {
     strRefID = strEmpID
     }
     
     let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV2_SubObjs?refid=\(strRefID)&reftype=\(strRefType)"
     
     FTIndicator.showProgress(withMessage: "Fetching Tasks...", userInteractionEnable: false)
     
     WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
     
     FTIndicator.dismissProgress()
     
     self.refreshControl.endRefreshing()
     
     if(status == true)
     {
     if((response.value(forKey: "data") as! NSArray).count > 0)
     {
     self.aryTasks.removeAllObjects()
     
     self.saveTaskListToLocalDb(data: response.value(forKey: "data") as! NSArray)
     
     }
     else{
     
     FTIndicator.dismissProgress()
     
     if(self.aryAllKeys.count == 0)
     {
     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
     }
     
     
     }
     }
     else
     {
     FTIndicator.dismissProgress()
     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
     
     }
     }
     }
     }*/
    // MARK: ---------------------------Footer Functions ---------------------------
    func setFooterMenuOption() {
        
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        
        nsud.setValue("Sales", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
            self.GotoDashboardViewController()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
            self.GotoScheduleViewController()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.GotoAccountViewController()
        }
        
        
    }
    func GotoDashboardViewController() {
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoScheduleViewController() {
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        //        let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
        //
        //        self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                let mainStoryboard = UIStoryboard(
                    name: "Main",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
            }
        }
    }
    func GotoAccountViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
    }
    // MARK: ---------------------------Header Functions ---------------------------
    func setTopMenuOption() {
        for view in self.view.subviews {
            if(view is HeaderView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Tasks", forKey: "DPS_TopMenuOptionTitle")
        nsud.synchronize()
        var headerViewtop = HeaderView()
        headerViewtop = HeaderView.init(frame: CGRect(x: 0, y: 0, width: self.headerView.frame.size.width, height:self.headerView.frame.size.height))
        headerViewtop.searchBarTask.delegate = self
        if #available(iOS 13.0, *) {
            headerViewtop.searchBarTask.searchTextField.text = self.strSearchText
        } else {
            headerViewtop.searchBarTask.text = self.strSearchText
        }
        self.headerView.addSubview(headerViewtop)
        
        headerViewtop.onClickFilterButton = {() -> Void in
            self.GotoFilterViewController()
        }
        headerViewtop.onClickAddButton = {() -> Void in
            self.GotoAddNewTaskViewController()
        }
        headerViewtop.onClickVoiceRecognizationButton = { () -> Void in
            
            self.view.endEditing(true)
            if self.aryAllKeys.count != 0 {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.delegate = self
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: {})
            }
        }
        headerViewtop.onClickTopMenuButton = {(str) -> Void in
            print(str)
            switch str {
            case "Map":
                self.GotoMapViewController()
                break
            case "Lead":
                self.GotoLeadViewController()
                break
            case "Opportunity":
                self.GotoOpportunityViewController()
                break
            case "Tasks":
                break
            case "Activity":
                self.GotoActivityViewController()
                break
            case "View Schedule":
                self.GotoViewScheduleViewController()
                break
            case "Signed Agreements":
                self.GotoSignAgreementViewController()
                break
            default:
                break
            }
            
        }
    }
    
    func GotoViewScheduleViewController() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ScheduleD2D" : "ScheduleD2D", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoFilterViewController() {
        let mainStoryboard = UIStoryboard(
            name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
            bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "FilterTaskVC_CRMNew_iPhone") as! FilterTaskVC_CRMNew_iPhone
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func GotoAddNewTaskViewController() {
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        
        
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func GotoMapViewController() {
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    func GotoLeadViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoOpportunityViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoTaskViewController() {
        
    }
    func GotoActivityViewController() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
        vc.strFromVC = strFromVC
        //nsud.setValue(nil, forKey: "dictFilterActivity")
        //nsud.synchronize()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func GotoSignAgreementViewController() {
        if(DeviceType.IS_IPAD){
            let mainStoryboard = UIStoryboard(
                name: "CRMiPad",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }else{
            let mainStoryboard = UIStoryboard(
                name: "CRM",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }
    }
    
    //MARK:------
    
    // new api
    @objc fileprivate func callAPIToGetTaskList()
    {
        
        let startGlobal = CFAbsoluteTimeGetCurrent()
        
        strRefType = "Employee"
        strRefID = strEmpID
        
        if(strFromVC == "WebLeadVC")
        {
            strRefType = "WebLead"
            strRefID = strleadId
        }
        if(strFromVC == "DashBoardView")
        {
            strRefType = "Employee"
            strRefID = strEmpID
        }
        
        if(strFromVC == "OpportunityVC")
        {
            strRefType = "Lead"
            strRefID = strleadId
            // strEmpID
        }
        
        if(strRefID.count == 0)
        {
            strRefID = strEmpID
        }
        
        var dicJson = [String : Any]()
        var aryPriority = NSArray()
        var aryTaskTypeId = NSArray()
        
        
        dicJson = ["RefId":strRefID,
                   "RefType":strRefType,
                   "IsDefault":"true",
                   "SkipDefaultDate":"false",
                   "Status":"",
                   "FromDate":"",
                   "ToDate":"",
                   "EmployeeId":"",
                   "TakeRecords":"",
                   "TaskTypeId":aryTaskTypeId,
                   "PriorityIds":aryPriority]
        
        
        if let dict = nsud.value(forKey: "dictFilterTask"){
            
            if(dict is NSDictionary){
                
                if((dict as! NSDictionary).count > 0){
                    let dictForSort = dict as! NSDictionary
                    
                    var strTaskStatusIds = ""
                    
                    if let taskStatus = dictForSort.value(forKey: "taskstatus")
                    {
                        if(taskStatus as! NSArray).count > 0{
                            
                            strTaskStatusIds = "\((taskStatus as! NSArray).firstObject!)"
                            
                            if (taskStatus as! NSArray).count > 1 {
                                
                                strTaskStatusIds = ""
                                
                            }
                            
                        }else{
                            
                            strTaskStatusIds = "Open"
                            
                        }
                    }
                    if let taskType = dictForSort.value(forKey: "tasktypeid")
                    {
                        if(taskType as! NSArray).count > 0{
                            
                            aryTaskTypeId = taskType as! NSArray
                        }
                    }
                    if let priority = dictForSort.value(forKey: "priority")
                    {
                        if(priority is NSArray){
                            
                            if(priority as! NSArray).count > 0{
                                
                                aryPriority = priority as! NSArray
                            }
                        }
                    }
                    
                    dicJson = ["RefId":strRefID,
                               "RefType":strRefType,
                               "IsDefault":"false",
                               "SkipDefaultDate":"false",
                               "Status":strTaskStatusIds,
                               "FromDate":"\(dictForSort.value(forKey: "fromDate")!)",
                               "ToDate":"\(dictForSort.value(forKey: "toDate")!)",
                               "EmployeeId":"\(dictForSort.value(forKey: "user")!)",
                               "TakeRecords":"",
                               "TaskTypeId":aryTaskTypeId,
                               "PriorityIds":aryPriority]
                    
                }
            }
        }
        
        print(dicJson)
        
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3"
        
        loader = loader_Show(controller: self, strMessage: "Fetching Tasks...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.postRequestWithHeaders(dictJson: dicJson as NSDictionary, url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status == true)
                {
                    if let dictData = response.value(forKey: "data"){
                        print(dictData)
                        self.aryAllKeys.removeAll()
                        self.dictGroupedTask.removeAll()
                        self.aryTasksCopy.removeAllObjects()
                        if((dictData as! NSDictionary).count > 0)
                        {
                            if ((dictData as! NSDictionary).value(forKey: "Tasks")) is NSArray
                            {
                                if(((dictData as! NSDictionary).value(forKey: "Tasks") as! NSArray).count > 0)
                                {
                                    let arrayTemp = (dictData as! NSDictionary).value(forKey: "Tasks") as! NSArray
                                    self.aryTasksCopy = arrayTemp.mutableCopy() as! NSMutableArray
                                    for (index, item) in self.aryTasksCopy.enumerated()
                                    {
                                        let dictTemp = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                        let dateString = Global().convertDate("\(dictTemp.value(forKey: "DueDate")!)")
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                                        dateFormatter.dateFormat = "MM/dd/yyyy"
                                        
                                        
                                        //  let date = Global().getOnlyDate(dateFormatter.date(from: dateString!))
                                        
                                        dictTemp.setValue(dateFormatter.date(from: dateString!), forKey: "DueDate")
                                        self.aryTasksCopy.replaceObject(at: index, with: dictTemp)
                                    }
                                    
                                    self.applyFilter()
                                    let diffGlobal = CFAbsoluteTimeGetCurrent() - startGlobal
                                    print("Took \(diffGlobal) seconds to get Data OverAll To Render From SQL Server. Count of Task :-- \(arrayTemp.count)")
                                    
                                }
                                else{
                                    self.tblviewTask.reloadData()
                                }
                            }

                            else{
                                self.tblviewTask.reloadData()
                            }
                        }
                        else{
                            self.tblviewTask.reloadData()
                        }
                    }
                    
                    else{
                        
                        
                        if(self.aryAllKeys.count == 0)
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                        }
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            
            
        }
    }
    
    fileprivate func callAPIToUpdateTaskMarkAsDone(dictData:NSMutableDictionary, index:Int, key:AnyHashable)
    {
        var taskStatus = ""
        
        if("\(dictData.value(forKey: "Status")!)" == "Open")
        {
            taskStatus = "Done"
        }
        else
        {
            taskStatus = "Open"
        }
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/ChangeTaskStatus?TaskId=\(dictData.value(forKey: "LeadTaskId")!)&Status=\(taskStatus)"
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        //  FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status == true)
                {
                    if("\(response.value(forKey: "data")!)" == "true")
                    {
                        let valueForKey = (self.dictGroupedTask[key]! as NSArray).mutableCopy() as! NSMutableArray
                        
                        let dictData = (valueForKey[index] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        dictData.setValue(taskStatus, forKey: "Status")
                        
                        for(index,item) in self.aryTasksCopy.enumerated(){
                            
                            if("\((item as!NSDictionary).value(forKey: "LeadTaskId")!)" == "\(dictData.value(forKey: "LeadTaskId")!)")
                            
                            {
                                self.aryTasksCopy.replaceObject(at: index, with: dictData)
                                self.applyFilter()
                                break
                            }
                            
                        }
                        
                        //  valueForKey.replaceObject(at: index, with: dictData)
                        
                        // self.dictGroupedTask.updateValue(valueForKey as! [NSMutableDictionary], forKey: key)
                        
                        
                        if("\(dictData.value(forKey: "Status")!)" == "Done")
                        {
                            /*let alertController = UIAlertController(title: "Message", message: "Do you want to create follow up?", preferredStyle: .alert)
                             
                             let okAction = UIAlertAction(title: "Yes - Create", style: UIAlertAction.Style.default)
                             {
                             UIAlertAction in
                             // api call nh karna hai, add task per redirect karna hai
                             
                             self.callAPIToGetTaskDetailById(strId: "\((dictData.value(forKey: "LeadTaskId")!))")
                             
                             
                             }
                             let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                             {
                             UIAlertAction in
                             NSLog("Cancel Pressed")
                             }
                             // Add the actions
                             alertController.addAction(okAction)
                             alertController.addAction(cancelAction)
                             
                             // Present the controller
                             self.present(alertController, animated: true, completion: nil)*/
                            
                            UserDefaults.standard.set(true, forKey: "isTaskAddedUpdated")
                            
                            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "LogAsActivityVC") as! LogAsActivityVC
                            
                            controller.dictLeadTaskData = NSDictionary()
                            controller.leadTaskId = "\((dictData.value(forKey: "LeadTaskId")!))"
                            controller.dictOfAssociations = NSMutableDictionary()
                            self.navigationController?.pushViewController(controller, animated: false)
                            
                        }
                        
                        self.tblviewTask.reloadData()
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    // something went wrong
                }
            }
        }
    }
    
    func callApiForFollowUpOld(dictData: NSMutableDictionary)
    {
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            var strUrl =  "\(strServiceUrlMain)\(UrlAddUpdateTaskGlobal)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            var key = NSArray()
            var value = NSArray()
            
            key = ["Id",
                   "LeadTaskId",
                   "RefId",
                   "RefType",
                   "TaskName",
                   "DueDate",
                   "ReminderDate",
                   "Description",
                   "AssignedTo",
                   "Priority",
                   "Status",
                   "CreatedBy",
                   "Tags",
                   "ChildActivities",
                   "FollowUpFromTaskId"]
            
            let arrChildActivity = NSMutableArray()
            
            value = ["1",
                     "",
                     "\(dictData.value(forKey: "RefId") ?? "")",//RefId
                     "\(dictData.value(forKey: "RefType") ?? "")",//RefType
                     "\(dictData.value(forKey: "TaskName") ?? "")",
                     "\(dictData.value(forKey: "DueDate") ?? "")",
                     "\(dictData.value(forKey: "ReminderDate") ?? "")",
                     "\(dictData.value(forKey: "TaskDescription") ?? "")",
                     "\(dictData.value(forKey: "AssignedTo") ?? "")",
                     "\(dictData.value(forKey: "Priority") ?? "")",
                     "\(dictData.value(forKey: "Status") ?? "")",
                     "\(dictData.value(forKey: "CreatedBy") ?? "")",
                     "\(dictData.value(forKey: "Tags") ?? "")",
                     arrChildActivity,
                     (dictData.value(forKey: "LeadTaskId") ?? "")]
            
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            
            let str = getJson(from: dict_ToSend)
            print("Json \(str ?? "")" as Any)
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postRequestWithHeadersResponseString(dictJson: dict_ToSend as NSDictionary, url: strUrl , responseStringComing: "Timeline") { (Response, Status) in
                self.loader.dismiss(animated: false) {
                    if(Status)
                    {
                        
                        
                        let strResponse = Response["data"] as! String
                        
                        if strResponse.count > 0
                        {
                            if strResponse == "true" || strResponse == "1"
                            {
                                
                                let alertController = UIAlertController(title: "Alert", message: "FollowUp created successfully.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default)
                                {
                                    UIAlertAction in
                                    
                                    self.dismiss(animated: false, completion: nil)
                                    
                                    NSLog("OK Pressed")
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                            else
                            {
                                // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                
                            }
                            
                        }
                        else
                        {
                            
                            // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                    }
                }
                
                
            }
            
            
        }
        
    }
    func callApiForFollowUp(dictData: NSMutableDictionary)
    {
        if !isInternetAvailable()
        {
            //showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            
            var strUrl =  "\(strServiceUrlMain)\(UrlAddUpdateTaskGlobal)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            var key = NSArray()
            var value = NSArray()
            
            key = ["Id",
                   "LeadTaskId",
                   "RefId",
                   "RefType",
                   "TaskName",
                   "DueDate",
                   "ReminderDate",
                   "Description",
                   "AssignedTo",
                   "Priority",
                   "Status",
                   "CreatedBy",
                   "Tags",
                   "ChildActivities",
                   "FollowUpFromTaskId"]
            
            let arrChildActivity = NSMutableArray()
            value = ["1",
                     "",
                     "\(dictData.value(forKey: "refId") ?? "")",//RefId
                     "\(dictData.value(forKey: "refType") ?? "")",//RefType
                     "\(dictData.value(forKey: "taskName") ?? "")",
                     "\(dictData.value(forKey: "dueDate") ?? "")",
                     "\(dictData.value(forKey: "reminderDate") ?? "")",
                     "\(dictData.value(forKey: "taskDescription") ?? "")",
                     "\(dictData.value(forKey: "assignedTo") ?? "")",
                     "\(dictData.value(forKey: "priority") ?? "")",
                     "\(dictData.value(forKey: "status") ?? "")",
                     "\(dictData.value(forKey: "createdBy") ?? "")",
                     "\(dictData.value(forKey: "tags") ?? "")",
                     arrChildActivity,
                     (dictData.value(forKey: "leadTaskId") ?? "")]
            
            let Url = String(format: strUrl)
            
            guard
                let serviceUrl = URL(string: Url) else { return }
            
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            let parameterDictionary = dict_ToSend
            
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            if (nsud.value(forKey: "LoginDetails") != nil)
            {
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
                
            }
            
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                
                self.loader.dismiss(animated: false) {
                    if let response = response {
                        print(response)
                    }
                    if let data = data
                    {
                        do
                                      {
                                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                                        
                                        
                                        let dict = NSMutableDictionary()
                                        
                                        dict.setValue(json, forKey: "response")
                                        print(dict)
                                        
                                        print(Global().nestedDictionaryByReplacingNulls(withNil: dict as? [AnyHashable : Any]))
                                        
                                        let dictResponse = Global().nestedDictionaryByReplacingNulls(withNil:dict as NSDictionary as? [AnyHashable : Any])
                                        
                                        print(dictResponse ?? 0)
                                        
                                        DispatchQueue.main.async
                                        {
                                            let alertController = UIAlertController(title: "Alert", message: "Follow up created successfully", preferredStyle: .alert)
                                            // Create the actions
                                            
                                            let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default)
                                            {
                                                UIAlertAction in
                                                
                                                self.callAPIToGetTaskList()
                                                
                                            }
                                            
                                            alertController.addAction(okAction)
                                            
                                            // Present the controller
                                            self.present(alertController, animated: true, completion: nil)
                                        }
                                      }
                        catch
                        {}
                    }
                    
                }
                
                
            }.resume()
        }
    }
    
    fileprivate func callAPIToGetTaskDetailById(strId : String){
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        
        var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetTaskById + strId
        
        strURL = strURL.trimmingCharacters(in: .whitespaces)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "CRMConactNew_ContactDetails_About") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status)
                {
                    if((response.value(forKey: "data") as! NSDictionary).count > 0)
                    {
                        var dictTaskDetailData = (response.value(forKey: "data") as! NSDictionary)
                        
                        dictTaskDetailData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictTaskDetailData as! [AnyHashable : Any]))! as NSDictionary
                        
                        // goto create follow up
                        
                        self.gotoCreateFollowUp(dictData: dictTaskDetailData)
                        
                    }
                    else{
                        
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                    
                }
            }
            
            
        }
    }
    
    // MARK: Functions --
    
    /* fileprivate func saveTaskListToLocalDb(data:NSArray)
     {
     //deleteAllRecordsFromDB(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "refType =="))
     //  deleteAllRecordsFromDB(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "refType == %@",strTypeOfTaskToFetch))
     
     //temp
     
     let context = getContext()
     let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\("TaskListCRMNew")")
     let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
     
     do {
     try context.execute(deleteRequest)
     try context.save()
     
     } catch {
     print ("There was an error")
     }
     //End
     
     for item in data
     {
     let arrOfKeys = NSMutableArray()
     let arrOfValues = NSMutableArray()
     
     let dictData = removeNullFromDict(dict: (item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
     
     arrOfKeys.add("userName")
     arrOfKeys.add("companyKey")
     arrOfKeys.add("accountNo")
     arrOfKeys.add("addressLine1")
     arrOfKeys.add("addressLine2")
     arrOfKeys.add("assignedByStr")
     arrOfKeys.add("assignedTo")
     arrOfKeys.add("assignedToStr")
     arrOfKeys.add("cellPhone1")
     arrOfKeys.add("cellPhone2")
     arrOfKeys.add("childActivities")
     arrOfKeys.add("cityName")
     arrOfKeys.add("clientCreatedDate")
     arrOfKeys.add("clientModifiedDate")
     arrOfKeys.add("companyName")
     arrOfKeys.add("countryId")
     arrOfKeys.add("createdBy")
     arrOfKeys.add("createdDate")
     arrOfKeys.add("dueDate")
     arrOfKeys.add("dueTime")
     arrOfKeys.add("firstName")
     arrOfKeys.add("followUpFromTaskId")
     arrOfKeys.add("lastName")
     arrOfKeys.add("leadNo")
     arrOfKeys.add("leadTaskId")
     arrOfKeys.add("middleName")
     arrOfKeys.add("modifiedBy")
     arrOfKeys.add("modifiedDate")
     arrOfKeys.add("primaryEmail")
     arrOfKeys.add("primaryPhone")
     arrOfKeys.add("primaryPhoneExt")
     arrOfKeys.add("priority")
     arrOfKeys.add("priorityStr")
     arrOfKeys.add("refId")
     arrOfKeys.add("refType")
     arrOfKeys.add("reminderDate")
     arrOfKeys.add("secondaryEmail")
     arrOfKeys.add("secondaryPhone")
     arrOfKeys.add("secondaryPhoneExt")
     arrOfKeys.add("stateId")
     arrOfKeys.add("status")
     arrOfKeys.add("tags")
     arrOfKeys.add("taskDescription")
     arrOfKeys.add("taskName")
     arrOfKeys.add("webLeadNo")
     arrOfKeys.add("zipcode")
     
     arrOfKeys.add("compareDate")
     
     arrOfValues.add(strUserName)
     arrOfValues.add(strCompanyKey)
     arrOfValues.add("\(dictData.value(forKey: "AccountNo")!)")
     arrOfValues.add("\(dictData.value(forKey: "AddressLine1")!)")
     arrOfValues.add("\(dictData.value(forKey: "AddressLine2")!)")
     arrOfValues.add("\(dictData.value(forKey: "AssignedByStr")!)")
     arrOfValues.add("\(dictData.value(forKey: "AssignedTo")!)")
     arrOfValues.add("\(dictData.value(forKey: "AssignedToStr")!)")
     arrOfValues.add("\(dictData.value(forKey: "CellPhone1")!)")
     arrOfValues.add("\(dictData.value(forKey: "CellPhone2")!)")
     
     if (dictData.value(forKey: "ChildActivities")) is NSArray
     {
     if((dictData.value(forKey: "ChildActivities") as! NSArray).count > 0)
     {
     let aryChild = NSMutableArray()
     let aryTemp = (dictData.value(forKey: "ChildActivities") as! NSArray).mutableCopy() as! NSMutableArray
     
     for itemTemp in aryTemp
     {
     aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
     }
     arrOfValues.add(aryChild)
     }
     else
     {
     arrOfValues.add(NSMutableArray())
     }
     }
     else
     {
     arrOfValues.add(NSMutableArray())
     }
     
     arrOfValues.add("\(dictData.value(forKey: "CityName")!)")
     
     if("\(dictData.value(forKey: "ClientCreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientCreatedDate")!)" != "")
     {
     //                arrOfValues.add(Global().convertDate("\(dictData.value(forKey: "ClientCreatedDate")!)"))
     
     arrOfValues.add("\(dictData.value(forKey: "ClientCreatedDate")!)")
     
     }
     else
     {
     arrOfValues.add("")
     }
     
     if("\(dictData.value(forKey: "ClientModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientModifiedDate")!)" != "")
     {
     arrOfValues.add("\(dictData.value(forKey: "ClientModifiedDate")!)")
     }
     else
     {
     arrOfValues.add("")
     }
     
     arrOfValues.add("\(dictData.value(forKey: "CompanyName")!)")
     
     arrOfValues.add("\(dictData.value(forKey: "CountryId")!)")
     
     arrOfValues.add("\(dictData.value(forKey: "CreatedBy")!)")
     
     if("\(dictData.value(forKey: "CreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "CreatedDate")!)" != "")
     {
     
     arrOfValues.add("\(dictData.value(forKey: "CreatedDate")!)")
     }
     else
     {
     arrOfValues.add("")
     }
     
     if("\(dictData.value(forKey: "DueDate")!)" != "<null>" && "\(dictData.value(forKey: "DueDate")!)" != "")
     {
     
     print("\(dictData.value(forKey: "DueDate")!)")
     
     arrOfValues.add(Global().convertDate("\(dictData.value(forKey: "DueDate")!)"))
     // arrOfValues.add("\(dictData.value(forKey: "DueDate")!)")
     arrOfValues.add(Global().convertTime("\(dictData.value(forKey: "DueDate")!)"))
     }
     else
     {
     arrOfValues.add("")
     arrOfValues.add("")
     }
     
     arrOfValues.add("\(dictData.value(forKey: "FirstName")!)")
     arrOfValues.add("\(dictData.value(forKey: "FollowUpFromTaskId")!)")
     arrOfValues.add("\(dictData.value(forKey: "LastName")!)")
     arrOfValues.add("\(dictData.value(forKey: "LeadNo")!)")
     arrOfValues.add("\(dictData.value(forKey: "LeadTaskId")!)")
     arrOfValues.add("\(dictData.value(forKey: "MiddleName")!)")
     arrOfValues.add("\(dictData.value(forKey: "ModifiedBy")!)")
     
     if("\(dictData.value(forKey: "ModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ModifiedDate")!)" != "")
     {
     // arrOfValues.add(Global().convertDate("\(dictData.value(forKey: "ModifiedDate")!)"))
     
     arrOfValues.add("\(dictData.value(forKey: "ModifiedDate")!)")
     }
     else
     {
     arrOfValues.add("")
     }
     
     arrOfValues.add("\(dictData.value(forKey: "PrimaryEmail")!)")
     arrOfValues.add("\(dictData.value(forKey: "PrimaryPhone")!)")
     arrOfValues.add("\(dictData.value(forKey: "PrimaryPhoneExt")!)")
     arrOfValues.add("\(dictData.value(forKey: "Priority")!)")
     arrOfValues.add("\(dictData.value(forKey: "PriorityStr")!)")
     arrOfValues.add("\(dictData.value(forKey: "RefId")!)")
     arrOfValues.add("\(dictData.value(forKey: "RefType")!)")
     
     if("\(dictData.value(forKey: "ReminderDate")!)" != "<null>" && "\(dictData.value(forKey: "ReminderDate")!)" != "")
     {
     arrOfValues.add("\(dictData.value(forKey: "ReminderDate")!)")
     }
     else
     {
     arrOfValues.add("")
     }
     
     arrOfValues.add("\(dictData.value(forKey: "SecondaryEmail")!)")
     arrOfValues.add("\(dictData.value(forKey: "SecondaryPhone")!)")
     arrOfValues.add("\(dictData.value(forKey: "SecondaryPhoneExt")!)")
     arrOfValues.add("\(dictData.value(forKey: "StateId")!)")
     arrOfValues.add("\(dictData.value(forKey: "Status")!)")
     arrOfValues.add("\(dictData.value(forKey: "Tags")!)")
     arrOfValues.add("\(dictData.value(forKey: "TaskDescription")!)")
     arrOfValues.add("\(dictData.value(forKey: "TaskName")!)")
     arrOfValues.add("\(dictData.value(forKey: "WebLeadNo")!)")
     arrOfValues.add("\(dictData.value(forKey: "Zipcode")!)")
     
     let dateString = Global().convertDate("\(dictData.value(forKey: "DueDate")!)")
     
     let dateFormatter = DateFormatter()
     
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     
     let compareDate = Global().getOnlyDate(dateFormatter.date(from: dateString!))
     
     arrOfValues.add(compareDate!)
     
     saveDataInDB(strEntity: "TaskListCRMNew", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
     }
     
     fetchTaskListFromLocalDB()
     }
     */
    /* fileprivate func fetchTaskListFromLocalDB()
     {
     var arrayTemp = NSMutableArray()
     
     if(strFromVC == "WebLeadVC" || strFromVC == "OpportunityVC")
     {
     arrayTemp = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND refType == %@",strUserName, strTypeOfTaskToFetch)).mutableCopy() as! NSMutableArray
     }
     else
     {
     arrayTemp = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@",strUserName)).mutableCopy() as! NSMutableArray
     
     }
     // arrayTemp = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND refType == %@",strUserName, strTypeOfTaskToFetch)).mutableCopy() as! NSMutableArray
     
     // arrayTemp = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@",strUserName)).mutableCopy() as! NSMutableArray
     
     if(arrayTemp.count > 0)
     {
     aryTasks.removeAllObjects()
     
     for item in arrayTemp
     {
     aryTasks.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
     
     }
     
     aryTasksCopy = aryTasks.mutableCopy() as! NSMutableArray
     
     print(aryTasksCopy)
     
     let aryForSort = aryTasks.mutableCopy() as! NSMutableArray
     
     /*   for (index, item) in aryForSort.enumerated()
     {
     let dictTemp = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
     
     let dateString = Global().convertDate("\(dictTemp.value(forKey: "dueDate")!)")
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     
     let date = Global().getOnlyDate(dateFormatter.date(from: dateString!))
     
     dictTemp.setValue(date, forKey: "dueDate")
     
     aryForSort.replaceObject(at: index, with: dictTemp)
     
     }*/
     
     if let status = nsud.value(forKey: "fromTaskFilter")
     {
     print(status as! Bool)
     let dictForSort = (nsud.value(forKey: "dictFilterTask") as! NSDictionary).mutableCopy()as! NSMutableDictionary
     
     if("\(dictForSort.value(forKey: "user")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "user")!)" == "Everyone")
     {// do nothing
     
     }
     else{// filter according to assignTo == employeeId
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     "\((dict as! NSDictionary).value(forKey: "assignedTo")!)" == "\(strEmpID)"
     } as! [NSMutableDictionary]
     filterAryCopy = filterAry
     
     if("\(dictForSort.value(forKey: "fromDate")!)".count > 0 && "\(dictForSort.value(forKey: "toDate")!)".count > 0)
     {
     let strFromDate = "\(dictForSort.value(forKey: "fromDate")!)"
     
     let strToDate = "\(dictForSort.value(forKey: "toDate")!)"
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     
     let date1 = dateFormatter.date(from: strFromDate)
     let date2 = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     
     filterAry = filterAryCopy.filter { (dict) -> Bool in
     
     return  ((dict as NSDictionary).value(forKey: "compareDate") as! Date >= fromDate!) && toDate! >= ((dict as NSDictionary).value(forKey: "compareDate") as! Date)
     }
     
     filterAryCopy = filterAry
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedTask = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     
     if(aryAllKeys.count > 0)
     {
     tblviewTask.reloadData()
     }
     else
     {
     tblviewTask.reloadData()
     //  showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }
     
     }
     else if("\(dictForSort.value(forKey: "sortby")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "sortby")!)" == "A to Z")
     {
     /*let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "taskName", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
     
     filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]*/
     
     filterAry = filterAryCopy.sorted { ($0["taskName"] as! String) .localizedCaseInsensitiveCompare($1["taskName"] as! String) == ComparisonResult.orderedAscending }
     
     filterAryCopy = filterAry
     
     dictGroupedTask = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
     
     (($0["taskName"] as! String).lowercased()).first
     }
     
     
     aryAllKeys = Array(dictGroupedTask.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
     
     tblviewTask.reloadData()
     
     /*   if(aryAllKeys.count > 0)
     {
     tblviewTask.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     
     }
     else if("\(dictForSort.value(forKey: "sortby")!)" == "Z to A")
     {
     /* let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "taskName", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
     
     filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]*/
     
     filterAry = filterAryCopy.sorted { ($0["taskName"] as! String) .localizedCaseInsensitiveCompare($1["taskName"] as! String) == ComparisonResult.orderedDescending }
     
     filterAryCopy = filterAry
     
     
     dictGroupedTask = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
     
     (($0["taskName"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
     
     tblviewTask.reloadData()
     
     /*  if(aryAllKeys.count > 0)
     {
     tblviewTask.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     }
     
     
     else if let priority = dictForSort.value(forKey: "priority")
     {
     if(priority is NSArray)
     {
     let arytemp = NSMutableArray()
     for itemPriorityId in priority as! NSArray
     {
     
     arytemp.add(aryForSort.filter { (dict) -> Bool in
     
     "\((dict as! NSDictionary).value(forKey: "priority")!)" == "\(itemPriorityId)"
     } as! [NSMutableDictionary])
     
     }
     
     //  filterAry = arytemp as! [NSMutableDictionary]
     
     filterAry.removeAll()
     for item in arytemp
     {
     
     for innerItem in item as! [NSMutableDictionary]
     {
     filterAry.append(innerItem)
     
     }
     }
     
     filterAryCopy = filterAry
     print(filterAry)
     
     /*   let sortedArray = filterAry.sorted(by: {
     (($0 )["dueDate"] as? Date)! > (($1 )["dueDate"] as? Date)!
     })*/
     
     dictGroupedTask = Dictionary(grouping: filterAryCopy) { $0["priority"] as! String }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     // aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     
     tblviewTask.reloadData()
     /*    if(aryAllKeys.count > 0)
     {
     tblviewTask.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     }
     
     else
     {
     print(filterAry)
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedTask = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     
     tblviewTask.reloadData()
     
     /*  if(aryAllKeys.count > 0)
     {
     tblviewTask.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     
     }
     }
     
     else if("\(dictForSort.value(forKey: "fromDate")!)".count > 0 && "\(dictForSort.value(forKey: "toDate")!)".count > 0)
     {
     let strFromDate = "\(dictForSort.value(forKey: "fromDate")!)"
     
     let strToDate = "\(dictForSort.value(forKey: "toDate")!)"
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     
     let date1 = dateFormatter.date(from: strFromDate)
     let date2 = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date >= fromDate!) && toDate! >= ((dict as! NSDictionary).value(forKey: "compareDate") as! Date)
     } as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedTask = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     tblviewTask.reloadData()
     }
     
     else if("\(dictForSort.value(forKey: "sortby")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "sortby")!)" == "A to Z")
     {
     let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "taskName", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
     
     filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     
     
     dictGroupedTask = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
     
     (($0["taskName"] as! String).lowercased()).first
     }
     
     
     aryAllKeys = Array(dictGroupedTask.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
     tblviewTask.reloadData()
     
     }
     else if("\(dictForSort.value(forKey: "sortby")!)" == "Z to A")
     {
     let descriptor: NSSortDescriptor =  NSSortDescriptor(key: "taskName", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
     
     filterAry = aryForSort.sortedArray(using: [descriptor]) as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     
     
     dictGroupedTask = Dictionary(grouping: aryForSort as! [NSMutableDictionary]) {
     
     (($0["taskName"] as! String).lowercased()).first
     }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
     tblviewTask.reloadData()
     
     }
     }
     else if let priority = dictForSort.value(forKey: "priority")
     {
     if(priority is NSArray)
     {
     let arytemp = NSMutableArray()
     for itemPriorityId in priority as! NSArray
     {
     
     arytemp.add(aryForSort.filter { (dict) -> Bool in
     
     "\((dict as! NSDictionary).value(forKey: "priority")!)" == "\(itemPriorityId)"
     } as! [NSMutableDictionary])
     
     }
     
     //  filterAry = arytemp as! [NSMutableDictionary]
     
     filterAry.removeAll()
     for item in arytemp
     {
     
     for innerItem in item as! [NSMutableDictionary]
     {
     filterAry.append(innerItem)
     
     }
     
     
     }
     
     filterAryCopy = filterAry
     print(filterAry)
     
     /*   let sortedArray = filterAry.sorted(by: {
     (($0 )["dueDate"] as? Date)! > (($1 )["dueDate"] as? Date)!
     })*/
     
     dictGroupedTask = Dictionary(grouping: filterAryCopy) { $0["priority"] as! String }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     
     
     
     // aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     
     tblviewTask.reloadData()
     }
     }
     else if("\(dictForSort.value(forKey: "overdue")!)".count > 0)
     {
     if("\(dictForSort.value(forKey: "overdue")!)" == "Today")
     {
     let today = Global().getCurrentDateAkshay()
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date == today!)
     } as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     dictGroupedTask = Dictionary(grouping: filterAry) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     tblviewTask.reloadData()
     
     }
     if("\(dictForSort.value(forKey: "overdue")!)" == "Tomorrow")
     {
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     
     //  let tmrwDate = Global().getOnlyDate(dateFormatter.date(from: getTomorrowDate()))
     
     let tmrwDate = Global().getOnlyDate(getTomorrowDate())
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date == tmrwDate!)
     } as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedTask = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     tblviewTask.reloadData()
     }
     if("\(dictForSort.value(forKey: "overdue")!)" == "This week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("This Week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     
     let date1 = dateFormatter.date(from: strFromDate)
     let date2 = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date > fromDate!) && toDate! > ((dict as! NSDictionary).value(forKey: "compareDate") as! Date)
     } as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedTask = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     tblviewTask.reloadData()
     
     }
     }
     if("\(dictForSort.value(forKey: "overdue")!)" == "Next week")
     {
     let objDelegate = StasticsClass()
     objDelegate.delegate = self as StasticsClassDelegate
     objDelegate.getStart_EndDate("Next Week")
     
     if(strFromDate.count > 0 && strToDate.count > 0)
     {
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "MM/dd/yyyy"
     dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
     let date1 = dateFormatter.date(from: strFromDate)
     let date2  = dateFormatter.date(from: strToDate)
     
     let fromDate = Global().getOnlyDate(date1)
     
     let toDate = Global().getOnlyDate(date2)
     
     filterAry = aryForSort.filter { (dict) -> Bool in
     
     return  ((dict as! NSDictionary).value(forKey: "compareDate") as! Date > fromDate!) && toDate! > ((dict as! NSDictionary).value(forKey: "compareDate") as! Date)
     } as! [NSMutableDictionary]
     
     filterAryCopy = filterAry
     let sortedArray = filterAry.sorted(by: {
     (($0 )["compareDate"] as? Date)! > (($1 )["compareDate"] as? Date)!
     })
     
     dictGroupedTask = Dictionary(grouping: sortedArray) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
     tblviewTask.reloadData()
     
     }
     }
     }
     }
     
     else
     {
     
     let sortedArray = aryForSort.sorted(by: {
     (($0 as! NSMutableDictionary)["compareDate"] as? Date)! >= (($1 as! NSMutableDictionary)["compareDate"] as? Date)!
     })
     
     
     filterAry = sortedArray as! [NSMutableDictionary]
     
     var arr = [NSMutableDictionary]()
     
     arr = sortedArray as! [NSMutableDictionary]
     
     dictGroupedTask = Dictionary(grouping: arr) { $0["compareDate"] as! Date }
     
     aryAllKeys = Array(dictGroupedTask.keys)
     
     let aryAllKeysCopy = aryAllKeys
     
     aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
     }
     
     tblviewTask.reloadData()
     /*  if(aryAllKeys.count > 0)
     {
     tblviewTask.reloadData()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
     }*/
     }
     else
     {
     // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
     }
     }
     */
    /*  fileprivate func updateTaskToLocalDB(dict:NSMutableDictionary)
     {
     
     let arrOfKeys = NSMutableArray()
     let arrOfValues = NSMutableArray()
     
     arrOfKeys.add("companyKey")
     arrOfKeys.add("userName")
     arrOfKeys.add("modifiedBy")
     arrOfKeys.add("modifiedDate")
     arrOfKeys.add("status")
     
     arrOfValues.add(strCompanyKey)
     arrOfValues.add(strUserName)
     arrOfValues.add(strEmpID)
     arrOfValues.add(Global().modifyDate())
     
     if("\(dict.value(forKey: "status")!)" == "Open")
     {
     arrOfValues.add("Done")
     }
     else
     {
     arrOfValues.add("Open")
     }
     
     let isSuccess =  getDataFromDbToUpdate(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "leadTaskId == %@", "\(dict.value(forKey: "leadTaskId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
     
     if isSuccess
     {
     fetchTaskListFromLocalDB()
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
     }
     }*/
    
    func createBadgeView() {
        
        if(DeviceType.IS_IPAD){
            lbltaskCount.isHidden = true
            lblScheduleCount.isHidden = true
        }
        
        
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }
                    
                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                            lblScheduleCount.text = strScheduleCount
                            lblScheduleCount.isHidden = false
                        }
                        else{
                            let badgeSchedule = SPBadge()
                            badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                            badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeSchedule.badge = strScheduleCount
                            btnScheduleFooter.addSubview(badgeSchedule)
                        }
                    }
                    
                    if strTaskCount.count > 0 {
                        
                        if(DeviceType.IS_IPAD){
                            lbltaskCount.text = strTaskCount
                            lbltaskCount.isHidden = false
                            
                        }else{
                            let badgeTasks = SPBadge()
                            badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                            badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeTasks.badge = strTaskCount
                            btnTasksFooter.addSubview(badgeTasks)
                        }
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func catchNotification(notification:Notification) {
        
        UserDefaults.standard.set(true, forKey: "isTaskAddedUpdated")
        
        /*if (isInternetAvailable()){
         
         FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
         
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
         
         self.callAPIToGetTaskList()
         })
         
         }else{
         
         showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
         }*/
        
    }
    
    func gotoCreateFollowUp(dictData : NSDictionary) {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(2, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountId")!)", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadId")!)", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadId")!)", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactId")!)", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyId")!)", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountDisplayName")!)", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadName")!)", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadName")!)", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactName")!)", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyName")!)", forKeyPath: "Pre_CrmCompanyName")
        
        dictTaskDetailsData.addEntries(from: dictData as! [AnyHashable : Any])
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func filterAndReloadSearchedData(aryTemp : [NSMutableDictionary])
    {
        if let status = nsud.value(forKey: "fromTaskFilter")
        {
            print(status as! Bool)
            let dictForSort = (nsud.value(forKey: "dictFilterTask") as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
            if("\(dictForSort.value(forKey: "user")!)".count > 0)
            {
                if("\(dictForSort.value(forKey: "user")!)" == "Everyone")
                {// do nothing
                    
                }
                else{// filter according to assignTo == employeeId
                    
                    
                    if("\(dictForSort.value(forKey: "fromDate")!)".count > 0 && "\(dictForSort.value(forKey: "toDate")!)".count > 0)
                    {
                        
                        dictGroupedTask = Dictionary(grouping: aryTemp) { $0["compareDate"] as! Date }
                        
                        aryAllKeys = Array(dictGroupedTask.keys)
                        
                        let aryAllKeysCopy = aryAllKeys
                        
                        aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
                        
                    }
                    
                    else if("\(dictForSort.value(forKey: "sortby")!)".count > 0)
                    {
                        if("\(dictForSort.value(forKey: "sortby")!)" == "A to Z")
                        {
                            dictGroupedTask = Dictionary(grouping: aryTemp ) {
                                
                                (($0["taskName"] as! String).lowercased()).first
                            }
                            
                            
                            aryAllKeys = Array(dictGroupedTask.keys)
                            let aryAllKeysCopy = aryAllKeys
                            
                            aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
                            
                            tblviewTask.reloadData()
                            
                        }
                        else if("\(dictForSort.value(forKey: "sortby")!)" == "Z to A")
                        {
                            
                            dictGroupedTask = Dictionary(grouping: aryTemp ) {
                                
                                (($0["taskName"] as! String).lowercased()).first
                            }
                            
                            aryAllKeys = Array(dictGroupedTask.keys)
                            let aryAllKeysCopy = aryAllKeys
                            
                            aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
                            
                            tblviewTask.reloadData()
                        }
                    }
                    else if let priority = dictForSort.value(forKey: "priority")
                    {
                        if(priority is NSArray)
                        {
                            let arytemp1 = NSMutableArray()
                            for itemPriorityId in priority as! NSArray
                            {
                                
                                arytemp1.add(aryTemp.filter { (dict) -> Bool in
                                    
                                    "\((dict as NSDictionary).value(forKey: "priority")!)" == "\(itemPriorityId)"
                                } )
                            }
                            
                            filterAryCopy.removeAll()
                            for item in arytemp1
                            {
                                for innerItem in item as! [NSMutableDictionary]
                                {
                                    filterAryCopy.append(innerItem)
                                    
                                }
                                
                            }
                            
                            /*    for item in arytemp1
                             {
                             for innerItem in item as! [NSMutableDictionary]
                             {
                             filterAryCopy.append(innerItem)
                             }
                             }*/
                            
                            dictGroupedTask = Dictionary(grouping: filterAryCopy) { $0["priority"] as! String }
                            
                            aryAllKeys = Array(dictGroupedTask.keys)
                            
                            tblviewTask.reloadData()
                        }
                    }
                    else
                    {
                        
                        dictGroupedTask = Dictionary(grouping: aryTemp) { $0["compareDate"] as! Date }
                        
                        aryAllKeys = Array(dictGroupedTask.keys)
                        
                        let aryAllKeysCopy = aryAllKeys
                        
                        aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
                    }
                    
                }
            }
            
            else if("\(dictForSort.value(forKey: "fromDate")!)".count > 0 && "\(dictForSort.value(forKey: "toDate")!)".count > 0)
            {
                
                dictGroupedTask = Dictionary(grouping: aryTemp) { $0["compareDate"] as! Date }
                
                aryAllKeys = Array(dictGroupedTask.keys)
                
                let aryAllKeysCopy = aryAllKeys
                
                aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
            }
            
            else if("\(dictForSort.value(forKey: "sortby")!)".count > 0)
            {
                if("\(dictForSort.value(forKey: "sortby")!)" == "A to Z")
                {
                    
                    dictGroupedTask = Dictionary(grouping: aryTemp ) {
                        
                        (($0["taskName"] as! String).lowercased()).first
                    }
                    
                    
                    aryAllKeys = Array(dictGroupedTask.keys)
                    let aryAllKeysCopy = aryAllKeys
                    
                    aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
                    
                    
                }
                else if("\(dictForSort.value(forKey: "sortby")!)" == "Z to A")
                {
                    
                    
                    dictGroupedTask = Dictionary(grouping: aryTemp ) {
                        
                        (($0["taskName"] as! String).lowercased()).first
                    }
                    
                    aryAllKeys = Array(dictGroupedTask.keys)
                    let aryAllKeysCopy = aryAllKeys
                    
                    aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
                    
                    
                }
            }
            else if let priority = dictForSort.value(forKey: "priority")
            {
                if(priority is NSArray)
                {
                    let arytemp1 = NSMutableArray()
                    
                    for itemPriorityId in priority as! NSArray
                    {
                        
                        arytemp1.add(aryTemp.filter { (dict) -> Bool in
                            
                            "\((dict as NSDictionary).value(forKey: "priority")!)" == "\(itemPriorityId)"
                        } )
                        
                    }
                    
                    filterAryCopy.removeAll()
                    
                    for item in arytemp1
                    {
                        for innerItem in item as! [NSMutableDictionary]
                        {
                            filterAryCopy.append(innerItem)
                            
                        }
                    }
                    
                    dictGroupedTask = Dictionary(grouping: filterAryCopy) { $0["priority"] as! String }
                    
                    aryAllKeys = Array(dictGroupedTask.keys)
                    
                    tblviewTask.reloadData()
                }
            }
            else if("\(dictForSort.value(forKey: "overdue")!)".count > 0)
            {
                if("\(dictForSort.value(forKey: "overdue")!)" == "Today")
                {
                    dictGroupedTask = Dictionary(grouping: aryTemp) { $0["compareDate"] as! Date }
                    
                    aryAllKeys = Array(dictGroupedTask.keys)
                    
                }
                if("\(dictForSort.value(forKey: "overdue")!)" == "Tomorrow")
                {
                    dictGroupedTask = Dictionary(grouping: aryTemp) { $0["compareDate"] as! Date }
                    
                    aryAllKeys = Array(dictGroupedTask.keys)
                    
                    let aryAllKeysCopy = aryAllKeys
                    
                    aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
                }
                if("\(dictForSort.value(forKey: "overdue")!)" == "This week")
                {
                    let objDelegate = StasticsClass()
                    objDelegate.delegate = self as StasticsClassDelegate
                    objDelegate.getStart_EndDate("This Week")
                    
                    if(strFromDate.count > 0 && strToDate.count > 0)
                    {
                        
                        dictGroupedTask = Dictionary(grouping: aryTemp) { $0["compareDate"] as! Date }
                        
                        aryAllKeys = Array(dictGroupedTask.keys)
                        
                        let aryAllKeysCopy = aryAllKeys
                        
                        aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
                        
                    }
                }
                if("\(dictForSort.value(forKey: "overdue")!)" == "Next week")
                {
                    let objDelegate = StasticsClass()
                    objDelegate.delegate = self as StasticsClassDelegate
                    objDelegate.getStart_EndDate("Next Week")
                    
                    if(strFromDate.count > 0 && strToDate.count > 0)
                    {
                        
                        dictGroupedTask = Dictionary(grouping: aryTemp) { $0["compareDate"] as! Date }
                        
                        aryAllKeys = Array(dictGroupedTask.keys)
                        
                        let aryAllKeysCopy = aryAllKeys
                        
                        aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) < (($1 as! Date) ) })
                        
                    }
                }
            }
        }
        else
        {
            dictGroupedTask = Dictionary(grouping: aryTemp) { $0["compareDate"] as! Date }
            
            aryAllKeys = Array(dictGroupedTask.keys)
            
            let aryAllKeysCopy = aryAllKeys
            
            aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
        }
        
        tblviewTask.reloadData()
    }
    
    fileprivate func getPriorityNameFromPriorityId(priorityId: String)-> String
    {
        var name = priorityId
        let dictDetailsFortblView = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        
        let aryTemp = dictDetailsFortblView.value(forKey: "PriorityMasters") as! NSArray
        
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if("\(dict.value(forKey: "PriorityId")!)" == priorityId)
                {
                    name = "\(dict.value(forKey: "Name")!)"
                }
            }
        }
        
        return name
        
    }
    fileprivate func getTaskTypeNameFromTaskTypeId(taskTypeID: String)-> String{
        
        var name = taskTypeID
        
        for item in aryTaskTypeMaster
        {
            let dict = item as! NSDictionary
            if("\(dict.value(forKey: "TaskTypeId")!)" == taskTypeID)
            {
                name = "\(dict.value(forKey: "Name")!)"
                break
            }
        }
        
        return name
    }
    
    fileprivate func getTaskTypeFromMaster(){
        
        if let dict = nsud.value(forKey: "TotalLeadCountResponse")
        {
            if(dict is NSDictionary){
                
                if let aryTaskTypeTemp = (dict as! NSDictionary).value(forKey: "TaskTypeMasters") {
                    
                    if(aryTaskTypeTemp is NSArray)
                    {
                        if((aryTaskTypeTemp as! NSArray).count > 0)
                        {
                            for item in aryTaskTypeTemp as! NSArray
                            {
                                let dict = item as! NSDictionary
                                if(dict.value(forKey: "IsActive") as! Bool == true)
                                {
                                    aryTaskTypeMaster.add(dict)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func setUpViews(){
        
        viewBtnContainer.layer.cornerRadius = viewBtnContainer.frame.size.height/2.0
        viewBtnContainer.layer.masksToBounds = true
        
        btnTask.layer.cornerRadius = btnTask.frame.size.height/2.0
        btnTask.layer.masksToBounds = true
        btnActivity.layer.cornerRadius = btnActivity.frame.size.height/2.0
        btnActivity.layer.masksToBounds = true
        
        if let txfSearchField = searchBarTask.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .none
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
        }
        searchBarTask.layer.borderColor = hexStringToUIColor(hex: appThemeColor).cgColor
        searchBarTask.layer.borderWidth = 1
        searchBarTask.layer.opacity = 1.0
    }
    
    func applyFilter(){
        
        searchBarTask.text = nil
        
        aryTasks = aryTasksCopy as! [NSMutableDictionary]
        sortBy = SortBy.date
        self.sortByCondition()
        //        switch filterType {
        //
        //        case .all:
        //
        //            aryTasks = aryTasksCopy as! [NSMutableDictionary]
        //            applySorting(aryFiltered: aryTasks)
        //            return
        //
        //        case .upcoming:
        //
        //            aryTasks = aryTasksCopy as! [NSMutableDictionary]
        //            let aryLocal = aryTasks
        //            aryTasks = aryLocal.filter { (dict) -> Bool in
        //
        //                return  ((dict as NSDictionary).value(forKey: "DueDate") as! Date >= getTodaysDate())
        //            }
        //
        //            applySorting(aryFiltered: aryTasks)
        //            return
        //
        //
        //        case .overdue:
        //
        //            aryTasks = aryTasksCopy as! [NSMutableDictionary]
        //            let aryLocal = aryTasks
        //
        //            aryTasks = aryLocal.filter { (dict) -> Bool in
        //
        //                return  ( getTodaysDate() > (dict as NSDictionary).value(forKey: "DueDate") as! Date)
        //            }
        //
        //            applySorting(aryFiltered: aryTasks)
        //
        //            return
        //        }
        //  sortByCondition()
        
    }
    func sortByCondition()  {
        //----Check For sort data---------//
        
        if(nsud.value(forKey: "TaskFilterSort") != nil){
            
            //                      switch filterType {
            //                      case .all:
            //                          segmentControlTask.selectedSegmentIndex = 0
            //
            //                      case .upcoming:
            //                          segmentControlTask.selectedSegmentIndex = 1
            //
            //                      case .overdue:
            //                          segmentControlTask.selectedSegmentIndex = 2
            //                      }
            
            let idSort = "\((nsud.value(forKey: "TaskFilterSort")as! NSDictionary).value(forKey: "id")!)"
            //-Date
            if(idSort == "1"){
                self.searchBarTask.text = nil
                self.sortBy = SortBy.date
            }
            //-Priority
            else if(idSort == "2"){
                self.searchBarTask.text = nil
                self.sortBy = SortBy.priority
            }
            //-Task Type
            else if(idSort == "3"){
                self.searchBarTask.text = nil
                self.sortBy = SortBy.taskType
            }
            //-Status
            else if(idSort == "4"){
                self.searchBarTask.text = nil
                self.sortBy = SortBy.status
            }
            //-A-Z
            else if(idSort == "5"){
                
                self.searchBarTask.text = nil
                self.sortBy = SortBy.aToz
            }
            //-Z-A
            else if(idSort == "6"){
                
                self.searchBarTask.text = nil
                self.sortBy = SortBy.zToa
            }
            
        }
        self.applySorting(aryFiltered: self.aryTasks)
        
    }
    fileprivate func applySorting(aryFiltered:[NSMutableDictionary]){
        
        
        switch sortBy {
        case .date:
            
            let sortedArray = aryFiltered.sorted(by: {
                (($0 as NSDictionary)["DueDate"] as? Date)! >= (($1 as NSDictionary)["DueDate"] as? Date)!
            })
            
            filterAry = sortedArray
            
            var arr = [NSMutableDictionary]()
            
            arr = sortedArray
            
            dictGroupedTask = Dictionary(grouping: arr) { $0["DueDate"] as! Date }
            
            aryAllKeys = Array(dictGroupedTask.keys)
            
            let aryAllKeysCopy = aryAllKeys
            
            aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
            
            tblviewTask.reloadData()
            return
            
        case .priority:
            
            let arr = aryFiltered.filter { (dict) -> Bool in
                
                return "\((dict as NSDictionary).value(forKey: "PriorityStr")!)" != "<null>" &&  "\((dict as NSDictionary).value(forKey: "PriorityStr")!)" != " " && "\((dict as NSDictionary).value(forKey: "PriorityStr")!)".count > 0
                
            }
            
            dictGroupedTask = Dictionary(grouping: arr) { $0["PriorityStr"] as! String }
            
            aryAllKeys = Array(dictGroupedTask.keys)
            
            tblviewTask.reloadData()
            
            return
        case .taskType:
            
            let arr = aryFiltered.filter { (dict) -> Bool in
                
                return  "\((dict as NSDictionary).value(forKey: "TaskTypeId")!)" != "<null>" &&  "\((dict as NSDictionary).value(forKey: "TaskTypeId")!)" != " " && "\((dict as NSDictionary).value(forKey: "TaskTypeId")!)".count > 0
            }
            
            dictGroupedTask = Dictionary(grouping: arr) { "\($0["TaskTypeId"]!)" }
            
            aryAllKeys = Array(dictGroupedTask.keys)
            
            tblviewTask.reloadData()
            
            return
        case .status:
            
            let arr = aryFiltered.filter { (dict) -> Bool in
                
                return "\((dict as NSDictionary).value(forKey: "Status")!)" != "<null>" &&  "\((dict as NSDictionary).value(forKey: "Status")!)" != " " && "\((dict as NSDictionary).value(forKey: "Status")!)".count > 0
                
            }
            
            dictGroupedTask = Dictionary(grouping: arr) { $0["Status"] as! String }
            
            aryAllKeys = Array(dictGroupedTask.keys)
            
            tblviewTask.reloadData()
            
            return
        case .aToz:
            
            let arr = aryFiltered.filter { (dict) -> Bool in
                
                return  "\((dict as NSDictionary).value(forKey: "TaskName")!)" != "<null>" &&  "\((dict as NSDictionary).value(forKey: "TaskName")!)" != " " && "\((dict as NSDictionary).value(forKey: "TaskName")!)".count > 0
            }
            
            dictGroupedTask = Dictionary(grouping: arr ) {
                
                (($0["TaskName"] as! String).lowercased()).first
            }
            
            aryAllKeys = Array(dictGroupedTask.keys)
            let aryAllKeysCopy = aryAllKeys
            
            aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) < (($1 as! Character) ) })
            tblviewTask.reloadData()
            
            return
        case .zToa:
            
            let arr = aryFiltered.filter { (dict) -> Bool in
                
                return  "\((dict as NSDictionary).value(forKey: "TaskName")!)" != "<null>" &&  "\((dict as NSDictionary).value(forKey: "TaskName")!)" != " " && "\((dict as NSDictionary).value(forKey: "TaskName")!)".count > 0
            }
            
            dictGroupedTask = Dictionary(grouping: arr ) {
                
                (($0["TaskName"] as! String).lowercased()).first
            }
            
            aryAllKeys = Array(dictGroupedTask.keys)
            let aryAllKeysCopy = aryAllKeys
            
            aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Character) ) > (($1 as! Character) ) })
            tblviewTask.reloadData()
            return
        }
    }
    
    // MARK: UITableView's delegate and datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return aryAllKeys.count
        //dictGroupedTask.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let key = aryAllKeys[section]
        
        //(Array(dictGroupedTask.keys))[section]
        let valueForKey = dictGroupedTask[key as! AnyHashable]
        return (valueForKey?.count)!
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: DeviceType.IS_IPAD ? 50 : 40))
        let lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: DeviceType.IS_IPAD ? 50 : 40))
        
        lblTitle.textAlignment = .center
        lblTitle.backgroundColor = hexStringToUIColor(hex: "EFEFF4")
        lblTitle.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 18)
        viewHeader.addSubview(lblTitle)
        
        let key = aryAllKeys[section]
        
        if((key as? Date) != nil)
        {
            if( (key as? Date)!.compare(getYesterdaysDate()) == .orderedSame){
                
                lblTitle.text = "Yesterday"
                
            }
            else if( (key as? Date)!.compare(getTodaysDate()) == .orderedSame){
                
                lblTitle.text = "Today"
            }
            else if( (key as? Date)!.compare(getTomorrowsDate()) == .orderedSame){
                
                lblTitle.text = "Tomorrow"
            }
            else{
                let strDate = changeDateToString(date: key as! Date)
                
                lblTitle.text = strDate
            }
        }
        if((key as? Character) != nil)
        {
            lblTitle.text = "\(key)".uppercased()
        }
        if((key as? String) != nil)
        {
            switch sortBy {
            case .priority:
                lblTitle.text = "\(key)"
                
            case .taskType:
                lblTitle.text = getTaskTypeNameFromTaskTypeId(taskTypeID: "\(key)" )
                
            case .status:
                lblTitle.text = "\(key)"
                
            default:
                print("")
            }
            
            // lblTitle.text = getPriorityNameFromPriorityId(priorityId: "\(key)")
        }
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1))
        viewFooter.backgroundColor = UIColor.lightGray
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 50 : 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTaskNew") as! LeadVCCell
        
        let key = aryAllKeys[indexPath.section]
        
        let valueForKey = dictGroupedTask[key as! AnyHashable]
        
        let dictData = valueForKey![indexPath.row]
        print("Task Dict Print")
        print(dictData)
        
        if let taskName = dictData.value(forKey: "TaskName"){
            if("\(taskName)".count > 0 && "\(taskName)" != "<null>"){
                cell.lblTaskName.text = "\(taskName)"
            }
            else{
                cell.lblTaskName.text = ""
            }
        }
        else{
            cell.lblTaskName.text = ""
        }
        
        cell.lblCompanyName.text = ""
        var strDueDate = "", str_company_Name = "" , str_contact_Name = ""
        
        if let str_strDueDate = dictData.value(forKey: "DueDate"){
            if("\(str_strDueDate)".count > 0 && "\(str_strDueDate)" != "<null>" && "\(str_strDueDate)" != " "){
                var date = ""
                var time = ""
                date = Global().convertDate("\(str_strDueDate)")
                time = Global().convertTime("\(str_strDueDate)")
                strDueDate = "\(date) \(time)"
                cell.lblCompanyName.text = strDueDate
            }
        }
        
        if let contact_Name = dictData.value(forKey: "CrmContactName"){
            if("\(contact_Name)".count > 0 && "\(contact_Name)" != "<null>" && "\(contact_Name)" != " "){
                str_contact_Name = "\(contact_Name)"
                cell.lblCompanyName.text = cell.lblCompanyName.text! + "  \u{2022}  " + str_contact_Name
            }
        }
        
        if let company_Name = dictData.value(forKey: "CrmCompanyName"){
            if("\(company_Name)".count > 0 && "\(company_Name)" != "<null>" && "\(company_Name)" != " "){
                str_company_Name = "\(company_Name)"
                cell.lblCompanyName.text = cell.lblCompanyName.text! + "  \u{2022}  " + str_company_Name
            }
        }
        
        if cell.lblCompanyName.text!.suffix(5) == "  \u{2022}  " {
            cell.lblCompanyName.text = String((cell.lblCompanyName.text?.dropLast(5))!)
        }
        
        
        //        if(str_contact_Name.count != 0 && str_company_Name.count != 0){
        //                cell.lblCompanyName.text = str_company_Name + "  |  " + str_contact_Name
        //        }
        
        //        if let assignTo = dictData.value(forKey: "AssignedToStr"){
        //
        //            if("\(assignTo)".count > 0 && "\(assignTo)" != "<null>"){
        //
        //                cell.lblAssignTo.text = "\(assignTo)"
        //            }
        //            else{
        //                cell.lblAssignTo.text = " "
        //            }
        //        }
        //        else{
        //            cell.lblAssignTo.text = " "
        //        }
        
        if let taskTypeId = dictData.value(forKey: "TaskTypeId"){
            
            print(dictData)
            
            if("\(taskTypeId)".count > 0 && "\(taskTypeId)" != "<null>"){
                
                print("taskTypeId Print")
                print(taskTypeId)
                let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
                print("strImageName Print")
                print(strImageName)
                cell.imgviewgTaskType.image = UIImage(named: strImageName)
                cell.imgviewgTaskType.isHidden = false
                
            }
            else{
                
                cell.imgviewgTaskType.isHidden = false
                
            }
        }
        
        cell.btnTaskCheckMark.tag = (indexPath.section * 1000) + indexPath.row
        cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
        
        /* let dateString = Global().convertDate("\(dictData.value(forKey: "DueDate")!)")
         
         let dateFormatter = DateFormatter()
         
         dateFormatter.dateFormat = "MM/dd/yyyy"
         dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
         
         let dateDue = Global().getOnlyDate(dateFormatter.date(from: dateString!)!)
         */
        
        let dateDue = dictData.value(forKey: "DueDate") as! Date
        
        let timeInterval = floor(Date().timeIntervalSinceReferenceDate / 86400) * 86400
        let currentdate = Date(timeIntervalSinceReferenceDate: timeInterval)
        
        if("\(dictData.value(forKey: "Status")!)" == "Done" || "\(dictData.value(forKey: "Status")!)" == "done")
        {
            cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
            cell.viewContainer.backgroundColor = hexStringToUIColor(hex: "C2EAB6")
            // cell.viewVertical.backgroundColor = hexStringToUIColor(hex: "499536")
            // green color
        }
        
        else if(dateDue.compare(currentdate) == .orderedAscending && ("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open"))
        {
            cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
            cell.viewContainer.backgroundColor = hexStringToUIColor(hex: "FADBDC")
            //cell.viewVertical.backgroundColor = hexStringToUIColor(hex: "EE5E60")
        }
        
        else if(dateDue.compare(currentdate) == .orderedDescending || dateDue.compare(currentdate) == .orderedSame && ("\(dictData.value(forKey: "Status")!)" != "Done" || "\(dictData.value(forKey: "Status")!)" != "done"))
        {
            cell.viewContainer.backgroundColor = UIColor.white
            //  cell.viewVertical.backgroundColor = UIColor.white
        }
        else
        {
            cell.viewContainer.backgroundColor = UIColor.white
            //  cell.viewVertical.backgroundColor = UIColor.white
        }
        /*   if(getPriorityNameFromPriorityId(priorityId:"\(dictData.value(forKey: "Priority")!)") == "High")
         {
         cell.viewVertical.backgroundColor = UIColor(red: 207.0/255.0, green: 68.0/255.0, blue: 47.0/255.0, alpha: 1)
         }
         else  if(getPriorityNameFromPriorityId(priorityId: "\(dictData.value(forKey: "Priority")!)") == "Medium")
         {
         cell.viewVertical.backgroundColor = UIColor(red: 254.0/255.0, green: 147.0/255.0, blue: 84.0/255.0, alpha: 1)
         }
         else  if(getPriorityNameFromPriorityId(priorityId: "\(dictData.value(forKey: "Priority")!)") == "Low")
         {
         cell.viewVertical.backgroundColor = UIColor(red: 62.0/255.0, green: 147.0/255.0, blue: 84.0/255.0, alpha: 1)
         }
         else{
         cell.viewVertical.backgroundColor = UIColor.clear
         }*/
        if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
        {
            cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
        }
        else
        {
            cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
        }
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        cell.alpha = 0.4
    //        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    //        UIView.animate(withDuration: 1.0) {
    //            cell.alpha = 1
    //            cell.transform = .identity
    //        }
    //    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: false)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
        
        let key = aryAllKeys[indexPath.section] as! AnyHashable
        let valueForKey = dictGroupedTask[key]
        
        let dictData = valueForKey![indexPath.row]
        
        vc.taskId = "\(dictData.value(forKey: "LeadTaskId")!)"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    // MARK: UIButton action
    
    @objc func actionOnTaskCheckMark(sender: UIButton!)
    {
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            let section = sender.tag / 1000
            let row = sender.tag % 1000
            
            let key = aryAllKeys[section] as! AnyHashable
            
            let valueForKey = dictGroupedTask[key]
            
            let dictData = valueForKey![row]
            
            callAPIToUpdateTaskMarkAsDone(dictData: dictData,index: row,key:key )
            
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnActivity(_ sender: UIButton) {
        self.GotoActivityViewController()
        
        
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        self.GotoFilterViewController()
    }
    
    @IBAction func actionOnSearch(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3) {
            //            self.hghtConstSearchBar.constant = 56.0
        }
    }
    
    
    @IBAction func actionOnDashboard(_ sender: UIButton) {
        
        self.GotoDashboardViewController()
    }
    @IBAction func actionOnAddTask(_ sender: UIButton) {
        
        self.GotoAddNewTaskViewController()
        
    }
    
    
    
    @IBAction func actionOnLeadOpportunity(_ sender: UIButton) {
        GotoLeadViewController()
    }
    
    @IBAction func actionOnAppointment(_ sender: UIButton) {
        self.GotoScheduleViewController()
    }
    
    @IBAction func actionOnNearBy(_ sender: UIButton)
    {
        /*
         let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
         
         let vc = storyboardIpad.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as! NearByMeViewControlleriPhone
         
         self.navigationController?.pushViewController(vc, animated: false)*/
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    @IBAction func actionOnContactList(_ sender: UIButton)
    {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        
        let Near = (UIAlertAction(title: enumNearBy, style: .default , handler:{ (UIAlertAction)in
            /*
             if(DeviceType.IS_IPAD){
             let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
             self.navigationController?.pushViewController(vc!, animated: false)
             }else{
             let storyboard = UIStoryboard(name: "Main", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
             self.navigationController?.pushViewController(vc!, animated: false)
             }*/
            self.GotoMapViewController()
            
            
        }))
        
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
        Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Near)
        
        
        
        let Signed = (UIAlertAction(title: enumSignedAgreement, style: .default , handler:{ (UIAlertAction)in
            
            self.GotoSignAgreementViewController()
        }))
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
        Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Signed)
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    @IBAction func actionOnSegmentControl(_ sender: UISegmentedControl) {
        
        if(sender.selectedSegmentIndex == 0){
            
            searchBarTask.text = nil
            filterType = FilterType.all
            applyFilter()
            return
        }
        if(sender.selectedSegmentIndex == 1){
            
            searchBarTask.text = nil
            filterType = FilterType.upcoming
            applyFilter()
            return
        }
        if(sender.selectedSegmentIndex == 2){
            
            searchBarTask.text = nil
            filterType = FilterType.overdue
            applyFilter()
            return
        }
        if(sender.selectedSegmentIndex == 3){
            
            switch filterType {
            case .all:
                segmentControlTask.selectedSegmentIndex = 0
                
            case .upcoming:
                segmentControlTask.selectedSegmentIndex = 1
                
            case .overdue:
                segmentControlTask.selectedSegmentIndex = 2
            }
            
            let alertController = UIAlertController(title: "Sort By", message: "", preferredStyle: .actionSheet)
            alertController.view.tintColor = UIColor.black
            let dateAction = UIAlertAction(title: "Date", style: .default) { (action) in
                
                self.searchBarTask.text = nil
                self.sortBy = SortBy.date
                self.applySorting(aryFiltered: self.aryTasks)
            }
            
            let priorityAction = UIAlertAction(title: "Priority", style: .default) { (action) in
                
                self.searchBarTask.text = nil
                self.sortBy = SortBy.priority
                self.applySorting(aryFiltered: self.aryTasks)
            }
            let tasktypeAction = UIAlertAction(title: "Task Type", style: .default) { (action) in
                
                self.searchBarTask.text = nil
                self.sortBy = SortBy.taskType
                self.applySorting(aryFiltered: self.aryTasks)
            }
            let statusAction = UIAlertAction(title: "Status", style: .default) { (action) in
                
                self.searchBarTask.text = nil
                self.sortBy = SortBy.status
                self.applySorting(aryFiltered: self.aryTasks)
            }
            let azAction = UIAlertAction(title: "A-Z", style: .default) { (action) in
                
                self.searchBarTask.text = nil
                self.sortBy = SortBy.aToz
                self.applySorting(aryFiltered: self.aryTasks)
            }
            let zaAction = UIAlertAction(title: "Z-A", style: .default) { (action) in
                
                self.searchBarTask.text = nil
                self.sortBy = SortBy.zToa
                self.applySorting(aryFiltered: self.aryTasks)
            }
            let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel) { (action) in
                
                self.dismiss(animated: false, completion: nil)
            }
            
            alertController.addAction(dateAction)
            alertController.addAction(priorityAction)
            alertController.addAction(tasktypeAction)
            alertController.addAction(statusAction)
            alertController.addAction(azAction)
            alertController.addAction(zaAction)
            alertController.addAction(dismissAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    // MARK: --------------UISearchBar delegate--------
    // MARK: ----------
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchAutocomplete(searchText: searchText)

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if(searchBar.text?.count == 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter text to be searched.", viewcontrol: self)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBarTask.text = ""
        applySorting(aryFiltered: aryTasks)
        self.view.endEditing(true)
    }
    
    func searchAutocomplete(searchText: String) -> Void{
        if searchText.isEmpty {
            
            applySorting(aryFiltered: aryTasks)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.view.endEditing(true)
            }
        }
        
        else{
            
            let aryTemp = aryTasks.filter { (task) -> Bool in
                
                return "\((task as NSDictionary).value(forKey: "TaskName")!)".lowercased().contains(searchText.lowercased()) || "\((task as NSDictionary).value(forKey: "LeadTaskId")!)".lowercased().contains(searchText.lowercased()) }
            applySorting(aryFiltered: aryTemp)

//            if(aryTemp.count > 0)
//            {
//                applySorting(aryFiltered: aryTemp)
//            }
//            else
//            {
//                applySorting(aryFiltered: aryTasks)
//            }
        }
    }
    // ------------END-----

    func changeDateToString(date: Date)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    func getTomorrowDate()-> Date
    {
        // let now = Calendar.current.dateComponents(in: .current, from: Date())
        
        //   let tomorrow = DateComponents(year: now.year, month: now.month, day: now.day! + 1)
        
        //let dateTomorrow = Calendar.current.date(from: tomorrow)!
        
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        
        /* let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "MM/dd/yyyy"
         let strDate = dateFormatter.string(from: tomorrow!)*/
        
        return tomorrow!
    }
}


extension TaskVC_CRMNew_iPhone : StasticsClassDelegate
{
    
    func getData(_ dict: [AnyHashable : Any]!)
    {
        let dictNew = dict! as NSDictionary
        
        strFromDate = "\(dictNew.value(forKey: "StartDate") ?? "")"
        strToDate = "\(dictNew.value(forKey: "EndDate") ?? "")"
    }
    
}
// MARK: -
// MARK: - FilterActivity_iPhone_Protocol
extension TaskVC_CRMNew_iPhone: FilterTask_iPhone_Protocol
{
    func getDataTask_iPhone_Protocol(dictData: NSDictionary, tag: Int) {
        if(tag == 0){ // Filter
            if (isInternetAvailable()){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    nsud.setValue(dictData, forKey: "dictFilterTask")
                    nsud.synchronize()
                    self.callAPIToGetTaskList()
                })
            }else{
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
            }
        }
        
    }
}
// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension TaskVC_CRMNew_iPhone : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        strSearchText = str
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
           self.setTopMenuOption()
            self.searchAutocomplete(searchText: str)
        }
    }
    
    
}
