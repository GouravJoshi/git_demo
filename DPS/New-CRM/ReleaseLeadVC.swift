//
//  ReleaseLeadVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 19/12/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class ReleaseLeadVC: UIViewController {
    // MARK: -  ----------------------------------- Outlet  -----------------------------------
    
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var const_ViewDateTime_H: NSLayoutConstraint!
    
    @IBOutlet weak var btnInvidual: UIButton!
    
    @IBOutlet weak var btnTeam: UIButton!
    
    @IBOutlet weak var btnOpen: UIButton!
    
    @IBOutlet weak var btnSelectEmployee: UIButton!
    
    @IBOutlet weak var btnSelectTeam: UIButton!
    
    @IBOutlet weak var btnCheckBox: UIButton!
    
    @IBOutlet weak var btnDate: UIButton!
    
    @IBOutlet weak var btnTime: UIButton!
    
    @IBOutlet weak var txtDate: ACFloatingTextField!
    
  @IBOutlet weak var txtTime: ACFloatingTextField!
    
    @IBOutlet weak var txtFldSelectEmployee: ACFloatingTextField!
    
    @IBOutlet weak var txtFldSelectTeam: ACFloatingTextField!
    
    
    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    
    let global = Global()
    var loader = UIAlertController()
    var dictLoginData = NSDictionary()
    
    // MARK: -  ----------------------------------- Variable Declaration -----------------------------------
    
    @IBOutlet weak var lblHeading: UILabel!
    
    // MARK: -  --- String ---
    // MARK: -  --- Array ---
    
    var arrEmployeeList = NSMutableArray()
    var arrTeamList = NSMutableArray()
    
    // MARK: -  --- NSDisctionary ---
    var dictEmployee = NSDictionary()
    var dictTeam = NSDictionary()
    //var dictLeadData = NSManagedObject()
    var dictLeadData = NSDictionary()
    
    // MARK: -  --- Bool ---
    
    var isCreateFollowUpChecked = Bool()
    var isIndividualSelected = Bool()
    var isTeamSelected = Bool()
    var isOpenSelected = Bool()
    
    // MARK: -  --- Int ---
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpIntialView()
        
        isCreateFollowUpChecked = false
        isIndividualSelected = true
        isTeamSelected = false
        isOpenSelected = false
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
        
        
        getEmployeeList()
        getTeamList()
        
    }
    // MARK: - ----------------------------------- Actions -----------------------------------
    
    @IBAction func actionOnSelectEmployee(_ sender: Any)
    {
        if arrEmployeeList.count > 0
        {
            //btnSelectEmployee.tag = 500
            //gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrEmployeeList ) , strTitle: "")
            
            var arrOfData = NSMutableArray()
            arrOfData = arrEmployeeList.mutableCopy() as! NSMutableArray
            
            arrOfData = addSelectInArray(strName: "FullName", array: arrOfData)
            
            openTableViewPopUp(tag: 500, ary: returnFilteredArray(array: arrOfData) , aryselectedItem: NSMutableArray())
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnSelectTeam(_ sender: Any)
    {
        if arrTeamList.count > 0
        {
            //btnSelectTeam.tag = 501
            //gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrTeamList ) , strTitle: "")
            var arrOfData = NSMutableArray()
            arrOfData = arrTeamList.mutableCopy() as! NSMutableArray
            
            arrOfData = addSelectInArray(strName: "Title", array: arrOfData)
            
            openTableViewPopUp(tag: 501, ary: returnFilteredArray(array: arrOfData) , aryselectedItem: NSMutableArray())
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnIndividual(_ sender: Any)
    {
        btnInvidual.setBackgroundImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnTeam.setBackgroundImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnOpen.setBackgroundImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        btnSelectEmployee.isEnabled = true
        btnSelectTeam.isEnabled = false
        txtFldSelectEmployee.isEnabled = true
        txtFldSelectTeam.isEnabled = false
        
        isIndividualSelected = true
        isTeamSelected = false
        isOpenSelected = false

        
    }
    
    @IBAction func actionOnTeam(_ sender: Any)
    {
        btnInvidual.setBackgroundImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnTeam.setBackgroundImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnOpen.setBackgroundImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        btnSelectEmployee.isEnabled = false
        btnSelectTeam.isEnabled = true
        txtFldSelectEmployee.isEnabled = false
        txtFldSelectTeam.isEnabled = true
        
        isIndividualSelected = false
        isTeamSelected = true
        isOpenSelected = false
    }
    
    @IBAction func actionOnOpen(_ sender: Any)
    {
        btnInvidual.setBackgroundImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnTeam.setBackgroundImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnOpen.setBackgroundImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        
        btnSelectEmployee.isEnabled = false
        btnSelectTeam.isEnabled = false
        txtFldSelectEmployee.isEnabled = false
        txtFldSelectTeam.isEnabled = false
        
        isIndividualSelected = false
        isTeamSelected = false
        isOpenSelected = true
    }
    

    @IBAction func actionOnBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)

    }
    
    @IBAction func actionOnCheckBox(_ sender: Any)
    {
        if btnCheckBox.currentBackgroundImage == UIImage(named: "check_box_1New.png")
        {
            btnCheckBox.setBackgroundImage(UIImage(named: "check_box_2New.png"), for: .normal)
            const_ViewDateTime_H.constant = 110
            
            isCreateFollowUpChecked = true
        }
        else
        {
            btnCheckBox.setBackgroundImage(UIImage(named: "check_box_1New.png"), for: .normal)
            
            const_ViewDateTime_H.constant = 0
            
            isCreateFollowUpChecked = false
        }
    }
    
    @IBAction func actionOnDate(_ sender: Any)
    {
        
                 let date = Date()
           let formatter = DateFormatter()
           formatter.dateFormat = "MM/dd/yyyy hh:mm a"
           formatter.locale = Locale(identifier: "EST")
           let result = formatter.string(from: date)
           
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
           dateFormatter.locale = Locale(identifier: "en_US_POSIX")
         
           let fromDate = dateFormatter.date(from:((txtDate.text!.count) > 0 ? txtDate.text! : result))!
        
        gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime", tag: 10, dateToSet: fromDate )
    }
    
    @IBAction func actionOnTime(_ sender: Any)
    {
        //gotoDatePickerView(sender: sender as! UIButton, strType: "Time", tag: 11)

    }
    
    @IBAction func actionOnSave(_ sender: Any)
    {
        callApi()
    }
    
    @IBAction func actionOnCancel(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    
    
    // MARK: - ----------------------------------- Other Functions -----------------------------------
    func setUpIntialView()
    {
        btnCheckBox.setBackgroundImage(UIImage(named: "check_box_1New.png"), for: .normal)
        btnInvidual.setBackgroundImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnTeam.setBackgroundImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnOpen.setBackgroundImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        if btnCheckBox.currentBackgroundImage == UIImage(named: "check_box_1New.png")
        {
            const_ViewDateTime_H.constant = 0
        }
        
        //btnDate.setTitle(changeStringDateToGivenFormat(strDate: Global().getCurrentDate(), strRequiredFormat: "MM/dd/yyyy"), for: .normal)
    
        //btnTime.setTitle(changeStringDateToGivenFormat(strDate: Global().getCurrentDate(), strRequiredFormat: "hh:mm a"), for: .normal)
        
        txtDate.text = changeStringDateToGivenFormat(strDate: Global().getCurrentDate(), strRequiredFormat: "MM/dd/yyyy hh:mm a")
       // txtTime.text = changeStringDateToGivenFormat(strDate: Global().getCurrentDate(), strRequiredFormat: "hh:mm a")
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "hh:mm a"
//        let dateString = dateFormatter.string(from:Date())
//        txtTime.text = dateString
        
        btnSelectTeam.isEnabled = true
        btnSelectTeam.isEnabled = false
        txtFldSelectEmployee.isEnabled = true
        txtFldSelectTeam.isEnabled = false
    }
    func gotoDatePickerView(sender: UIButton, strType:String, tag: Int ,dateToSet: Date)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.dateToSet = dateToSet
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func getEmployeeList()
    {
        arrEmployeeList = NSMutableArray()
        if nsud.value(forKey: "EmployeeList") is NSArray
        {
            var arrEmployee = NSArray()
            arrEmployee = nsud.value(forKey: "EmployeeList") as! NSArray
            
            for item in arrEmployee
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "IsActive") ?? "")" == "1"
                {
                    arrEmployeeList.add(dict)
                }
                
            }
            
        }
        
    }
    func getTeamList()
    {
        arrTeamList = NSMutableArray()
        
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary
        {
            
            let dictLead = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            var arrEmployee = NSArray()
            
            if dictLead.value(forKey: "TeamMasters") is NSArray
            {
                arrEmployee = dictLead.value(forKey: "TeamMasters") as! NSArray
                for item in arrEmployee
                {
                    let dict = item as! NSDictionary
                    
                    if "\(dict.value(forKey: "IsActive") ?? "")" == "1"
                    {
                        arrTeamList.add(dict)
                    }
                    
                }
            }
            
        }
        
    }
    
    // MARK: - ----------------------------------- API Calling -----------------------------------
    
    func callApi()
    {
        if !isInternetAvailable()
        {
            showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            var strUrl =  "\(strServiceUrlMain)\(URLWebLeadReleaseOrFollowUp)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            var key = NSArray()
            var value = NSArray()
            
            
            var strAssignToType = String()
            var strAssignTo = String()
            var strDateTime = String()
            var strIsCheckMarkOpened = String()
            
            strAssignToType = ""
            strAssignTo = ""
            strDateTime = ""
            strIsCheckMarkOpened = ""
            
            if isIndividualSelected //Individual
            {
                if dictEmployee.count > 0
                {
                    strAssignToType = "Individual"
                    strAssignTo = "\(dictEmployee.value(forKey: "EmployeeId") ?? "")"
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select employee", viewcontrol: self)
                    return
                }
            }
            else if isTeamSelected //Team
            {
                if dictTeam.count > 0
                {
                    strAssignToType = "Team"
                    strAssignTo = "\(dictTeam.value(forKey: "TeamId") ?? "")"
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select team", viewcontrol: self)
                    return
                }
            }
            else
            {
                strAssignToType = ""
                strAssignTo = ""
            }
            if(isCreateFollowUpChecked)
            {
                strIsCheckMarkOpened = "true"
                //strDateTime = "\(btnDate.titleLabel?.text ?? "") \((btnTime.titleLabel?.text ?? ""))"
               // strDateTime = "\(txtDate.text ?? "") \((txtTime.text ?? ""))"
                 strDateTime = "\(txtDate.text ?? "")"

            }
            else
            {
                strIsCheckMarkOpened = "false"
            }
                
            
            
            key = ["WebLeadId",
                   "EmployeeId",
                   "AssignTo",
                   "AssignToType",
                   "CreateFollowUp",
                   "DueDate",
                   "ReleaseLead"]
            
            value = ["\(dictLeadData.value(forKey: "LeadId") ?? "")", //WebLeadId
                "\(strEmpID)", //EmployeeId
                "\(strAssignTo)", //AssignTo
                "\(strAssignToType)", //AssignToType
                "\(strIsCheckMarkOpened)", //CreateFollowUp
                "\(strDateTime)", //DueDate
                "true"] //ReleaseLead
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            
            let str = getJson(from: dict_ToSend)
            print("Json \(str ?? "")" as Any)
            
            
           // FTIndicator.showProgress(withMessage: "Submitting...", userInteractionEnable: false)
            
           loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
           self.present(loader, animated: false, completion: nil)
            WebService.postRequestWithHeadersResponseString(dictJson: dict_ToSend as NSDictionary, url: strUrl , responseStringComing: "Timeline") { (Response, Status) in
                
                self.loader.dismiss(animated: false)
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                    if(Status)
                    {
                        
                        let strResponse = Response["data"] as! String
                        
                        if strResponse.count > 0
                        {
                            if strResponse == "true" || strResponse == "1"
                            {
                                /*showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Lead released successfully.", viewcontrol: self)
                                
                                self.dismiss(animated: false, completion: nil)*/
                                
                                let alertController = UIAlertController(title: "Alert", message: "Lead released successfully.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default)
                                {
                                    UIAlertAction in
                                    
                                    self.dismiss(animated: false, completion: nil)
                                    
                                    NSLog("OK Pressed")
                                    
                                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedCompany_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedTask_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                    
                                    
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                                
                                
                            }
                            else
                            {
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                
                            }
                            
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        DispatchQueue.main.async{
                            
                            FTIndicator.dismissProgress()
                            
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async{
                            
                            FTIndicator.dismissProgress()
                            
                        }
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        
                    }
                }
                

                
            }
            
            
        }
        
    }
    
}

// MARK: --------------------- DatePicker Delegate --------------

extension ReleaseLeadVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if tag == 10
        {
            //btnDate.setTitle(strDate, for: .normal)
            txtDate.text = strDate
          
        }
        else if tag == 11
        {
            //btnTime.setTitle(strDate, for: .normal)
           // txtTime.text = strDate
            
        }
    }

}
// MARK: --------------------- Tableview Delegate --------------

extension ReleaseLeadVC : CustomTableView
{
    
    
    func getDataOnSelection(dictData: NSDictionary, tag: Int)
    {
        self.view.endEditing(true)
        
        if tag == 500
        {
            if dictData["FullName"] as? String  == strSelectString
            {
                txtFldSelectEmployee.text = ""
                dictEmployee = NSDictionary()
            }
            else
            {
                //btnSelectEmployee.setTitle("\(dictData.value(forKey: "FullName") ?? "")", for: .normal)
                txtFldSelectEmployee.text = "\(dictData.value(forKey: "FullName") ?? "")"
                dictEmployee = dictData
            }
           
            
        }
        else if tag == 501
        {
            if dictData["Title"] as? String  == strSelectString
            {
                txtFldSelectTeam.text = ""
                dictTeam = NSDictionary()
            }
            else
            {
                //btnSelectTeam.setTitle("\(dictData.value(forKey: "Title") ?? "")", for: .normal)
                txtFldSelectTeam.text = "\(dictData.value(forKey: "Title") ?? "")"
                dictTeam = dictData
            }

        }
        
    }
    
    
    
}
