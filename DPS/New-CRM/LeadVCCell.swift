//
//  LeadVCCell.swift
//  DPS
//
//  Created by Akshay Hastekar on 20/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class LeadVCCell: UITableViewCell
{
    //Lead
    @IBOutlet weak var lblNewAccNo: UILabel!
    
    @IBOutlet weak var lblNewOppNo: UILabel!
    
    @IBOutlet weak var lblNewStatus: UILabel!
    
    @IBOutlet weak var lblNewCompanyName: UILabel!
    
    @IBOutlet weak var lblNewCustomerName: UILabel!
    
    @IBOutlet weak var lblNewPercentage: UILabel!
    
    @IBOutlet weak var lblNewAmount: UILabel!
    
    
    //Opportunity
    
    @IBOutlet weak var lblNewStatusBar: UILabel!
    @IBOutlet weak var lblNewTechName: UILabel!
    
    /*
     @property (weak, nonatomic) IBOutlet UILabel *lblNewAccNo;
     @property (weak, nonatomic) IBOutlet UILabel *lblNewOppNo;
     @property (weak, nonatomic) IBOutlet UILabel *lblNewStatus;
     @property (weak, nonatomic) IBOutlet UILabel *lblNewCompanyName;
     @property (weak, nonatomic) IBOutlet UILabel *lblNewCustomerName;
     @property (weak, nonatomic) IBOutlet UILabel *lblNewPercentage;
     @property (weak, nonatomic) IBOutlet UILabel *lblNewAmount;
     */
    
    // Task
    
    @IBOutlet weak var lblOpportunityName: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var btnTaskCheckMark: UIButton!
    @IBOutlet weak var viewVertical: UIView!
    @IBOutlet weak var viewContainer: CardView!
    
    @IBOutlet weak var constWidthCompanyName: NSLayoutConstraint!
    @IBOutlet weak var viewVerticalSeparator: UIView!
    // Activity
    
    @IBOutlet weak var lblComment: UILabel!
    
    @IBOutlet weak var btnComment: UIButton!
    
    // Lead Detail
    // task
    
    @IBOutlet weak var lblTaskName: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblDueTime: UILabel!
    @IBOutlet weak var imgViewTaskType: UIImageView!
    @IBOutlet weak var btnImgTask: UIButton!
    @IBOutlet weak var imgTaskType: UIImageView!
    
    
    // activity
    
    @IBOutlet weak var lblActivityName: UILabel!
    @IBOutlet weak var lblLogType: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    
    //* details
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPrimaryPhone: UILabel!
    @IBOutlet weak var lblSecondaryPhone: UILabel!
    @IBOutlet weak var lblCell: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblBranch: UILabel!
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnConvertToOpportunity: ButtonWithShadow!
    @IBOutlet weak var btnCreateFollowUp: ButtonWithShadow!
    
    @IBOutlet weak var btnRelease: ButtonWithShadow!
    @IBOutlet weak var btnGrab: ButtonWithShadow!
    @IBOutlet weak var btnMarkAsDead: ButtonWithShadow!
    
    // Notes
    
    @IBOutlet weak var lblNoteName: UILabel!
    
    @IBOutlet weak var lblNoteCreatedBy: UILabel!
    @IBOutlet weak var lblNoteCreatedDate: UILabel!
    
    @IBOutlet weak var btnDownload: UIButton!
    
    //7Feb 2020
    //CRMContactNew_ Contact Details_Lead
    
    @IBOutlet weak var lblLeadNumber: UILabel!
    
    @IBOutlet weak var lblAssignTo: UILabel!
    
    @IBOutlet weak var lblCustomerName: UILabel!
    
    
     //CRMContactNew_ Contact Details_Opportunity
    
    @IBOutlet weak var lblAccountNumber: UILabel!
    
    @IBOutlet weak var lblOpportunityNumber: UILabel!
    
    @IBOutlet weak var lblOpportunityStage: UILabel!
    
    @IBOutlet weak var lblConfidenceLevel: UILabel!
    
    @IBOutlet weak var lblProposedAmount: UILabel!
    
    @IBOutlet weak var lblAccContactName: UILabel!
    
    //CRMContactNew_ Contact Details_About
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrimaryEmail: UILabel!
    @IBOutlet weak var lblSecondaryEmail: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblReportTo: UILabel!
    
    @IBOutlet weak var btnPrimaryPhone: UIButton!
    @IBOutlet weak var btnSecondaryPhone: UIButton!
    @IBOutlet weak var btnCellPhone: UIButton!
    @IBOutlet weak var btnPrimaryEmail: UIButton!
    @IBOutlet weak var btnSecondaryEmail: UIButton!
    @IBOutlet weak var btnWebsite: UIButton!
    
    //CRMContactNew_ Contact Details_Contact
    
    @IBOutlet weak var lblContactNumber: UILabel!
    
    //CRMContactNew_ Documents Detail
       
    @IBOutlet weak var lblDocumentTitle: UILabel!
    @IBOutlet weak var imgViewDocument: UIImageView!
    
    //WebLead Detail
    //Detail
    @IBOutlet weak var viewDetailPrimaryPhone: UIView!
    @IBOutlet weak var viewDetailSecondaryPhone: UIView!
    @IBOutlet weak var viewDetailCellPhone: UIView!
    @IBOutlet weak var viewDetailPrimaryEmail: UIView!
    @IBOutlet weak var viewDetailSecondaryEmail: UIView!
    @IBOutlet weak var viewDetailBranch: UIView!
    @IBOutlet weak var viewDetailSource: UIView!
    @IBOutlet weak var viewDetailStatus: UIView!
    @IBOutlet weak var viewDetailLeadNo: UIView!
    @IBOutlet weak var viewDetailFlowType: UIView!
    @IBOutlet weak var viewDetailCreatedBy: UIView!
    @IBOutlet weak var viewDetailCreatedDate: UIView!

    @IBOutlet weak var lblDetailLeadNo: UILabel!
    
    @IBOutlet weak var lblDetailFlowType: UILabel!
    @IBOutlet weak var lblDetailCreatedBy: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    
    @IBOutlet weak var btnDetailMsgPrimaryPhone: UIButton!
    @IBOutlet weak var btnDetailCallPrimaryPhone: UIButton!
    
    @IBOutlet weak var btnDetailMsgSecondaryPhone: UIButton!
    @IBOutlet weak var btnDetailCallSecondaryPhone: UIButton!
    
    @IBOutlet weak var btnDetailMsgCell: UIButton!
    @IBOutlet weak var btnDetailCallCell: UIButton!
    
    @IBOutlet weak var btnDetailPrimaryEmail: UIButton!
    @IBOutlet weak var btnDetailSecondaryEmail: UIButton!
    
    // 11 March 2020
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var lblDetailTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblEmailContent: UILabel!
    @IBOutlet weak var lblEmailFrom: UILabel!
    @IBOutlet weak var constWidthCallBtnContainer: NSLayoutConstraint!
    
    // 12 March 2020
    @IBOutlet weak var lblLeadName: UILabel!
    
    //    16 March 2020
    @IBOutlet weak var txtviewEmailContent: UITextView!
    @IBOutlet weak var constHghtTxtviewEmailContent: NSLayoutConstraint!
    @IBOutlet weak var imgviewgTaskType: UIImageView!

    // 20 March 2020
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    // 18 March 2020  Contact Detail Company Cell
    
    @IBOutlet weak var btnDeAssociateCompany: UIButton!
    
    //Company Detail Contact Cell
    @IBOutlet weak var btnDeAssociateContact: UIButton!

    @IBOutlet weak var btnActivityComment: UIButton!
    
    
     //Webl Lead Detail New
    
    @IBOutlet weak var lblLeadNoNew: UILabel!
    @IBOutlet weak var lblAccNoNew: UILabel!
    @IBOutlet weak var lblServiceNameNew: UILabel!
    @IBOutlet weak var lblBranchNameNew: UILabel!
    @IBOutlet weak var lblSourceNameNew: UILabel!
    @IBOutlet weak var lblTypeNew: UILabel!
    @IBOutlet weak var lblCreatedByNew: UILabel!
    @IBOutlet weak var lblCreatedDateTimeNew: UILabel!
    
    
    // New Opportunity Detail
    
    @IBOutlet weak var viewSeperator: UIView!
    @IBOutlet weak var viewOpportunityDetail: UIView!
    @IBOutlet weak var lblOpportunityNo: UILabel!
    @IBOutlet weak var lblOppAccNo: UILabel!
    @IBOutlet weak var lblOppFieldSalesPerson: UILabel!
    @IBOutlet weak var lblOppCreatedDateTime: UILabel!
    @IBOutlet weak var lblOppCreatedBy: UILabel!
    @IBOutlet weak var lblOppScheduleDateTime: UILabel!
    @IBOutlet weak var lblOppServices: UILabel!
    @IBOutlet weak var lblOppValue: UILabel!
    @IBOutlet weak var lblOppStatus: UILabel!
    @IBOutlet weak var lblOppReasonTitle: UILabel!
    @IBOutlet weak var lblOppReason: UILabel!
    @IBOutlet weak var lblOppSource: UILabel!
    //TableContact New Weblead Detail
    
    @IBOutlet weak var lblContactNameNew: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
