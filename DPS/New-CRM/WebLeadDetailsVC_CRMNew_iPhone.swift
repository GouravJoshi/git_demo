//
//  WebLeadDetailsVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 29/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import AVKit
import CallKit

enum LeadDetailType
{
    case Task, Notes, Details, GeneralNew, DocumentsNew, DetailsNew, Timeline
    
}
class WebLeadDetailsVC_CRMNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate
{
    
    
    
    // MARK: - ----------------------------------- Outlets -----------------------------------
    
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnEditCompany: UIButton!
    @IBOutlet weak var btnTask: UIButton!
    @IBOutlet weak var btnNotes: UIButton!
    @IBOutlet weak var tblviewLeadDetails: UITableView!
    @IBOutlet weak var viewBtnsContainer: UIView!
    @IBOutlet weak var btnGeneralNew: UIButton!
    @IBOutlet weak var btnDocumentsNew: UIButton!
    @IBOutlet weak var const_LblBar_L: NSLayoutConstraint!
    @IBOutlet weak var btnEditWebLead: UIButton!
    @IBOutlet weak var btnTimeline: UIButton!
    @IBOutlet weak var btnCompanyName: UIButton!
    @IBOutlet weak var btnUserName: UIButton!
    @IBOutlet weak var lblBranchName: UILabel!
    @IBOutlet weak var lblSourceName: UILabel!
    @IBOutlet weak var lblLeadNo: UILabel!
    @IBOutlet weak var lblFlowType: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!

    @IBOutlet weak var btnFilterTimeLine: UIButton!
    @IBOutlet weak var const_btnFilterTimeLine_W: NSLayoutConstraint!
    @IBOutlet weak var btnLeadStatusNew: UIButton!
    @IBOutlet weak var btnDetailsNew: UIButton!
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    
    // MARK: - ------- Outlets ViewMatching Account -----
    
    @IBOutlet var view_MatchingAccounts: UIView!
    @IBOutlet weak var tblViewMatchingAccounts: UITableView!
    
    @IBOutlet weak var btnMatchingAccountCreateOpportunity: UIButton!
    
    
    @IBOutlet weak var tblContactNew: UITableView!
    @IBOutlet weak var const_TblContactNew_H: NSLayoutConstraint!
    
    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    var dictLoginData = NSDictionary()
    var strURL = String()
    var loader = UIAlertController()
    // variables
    var detailType = LeadDetailType.Timeline // GeneralNew
    var callObserver = CXCallObserver()
    
    
    // MARK: -  --- String ---
    
    var strRefId = String()
   @objc var strRefIdNew = String()
    var strLeadID = String()
    @objc var strReftype = String()
    var strTypeOfTaskToFetch = String()
    
    // MARK: -  --- Array ---
    
    var arrOfMatchingAccounts = NSArray()
    
    var aryIndexPath = NSMutableArray()
    var arrAllNotes = NSMutableArray()
    var aryTasks = NSMutableArray()
    var aryActivities = NSMutableArray()
    var arrAllDocuments = NSMutableArray()
    var aryTasksNew = NSMutableArray()
    var arrayCompanies = NSMutableArray()
    
    var aryLogType = NSMutableArray()
    var arrOfContacts = NSMutableArray()
    // MARK: -  --- Disctionary ---
    @objc var dictLeadData = NSDictionary()//NSManagedObject()
    @objc var dictLeadDataTemp = NSDictionary()
    
    //var dictLeadDataNew = NSDictionary()
    var dictLeadDataNew = NSDictionary() , dictAccountDetail = NSMutableDictionary()
    var dictServiceNameFromId = NSDictionary()
    var selectedMatchingAccounts = Int()
    
    // MARK: - --- Bool ----
    
    var isCreateNewLeads = Bool()
    
    // 16 March 2020
    
    var dictTimeLine = NSDictionary()
    var arrayTimeline = NSMutableArray()
    var filterTimelineAry = [NSMutableDictionary]()
    var dictGroupedTimeline = Dictionary<AnyHashable, [NSMutableDictionary]>()
    var aryAllKeys = Array<Any>()
    var chkCalledApiSynchro = Bool()

    let dispatchGroup = DispatchGroup()
    @IBOutlet weak var lbltaskCount: UILabel!
         @IBOutlet weak var lblScheduleCount: UILabel!
    
    override func viewDidLoad()
        
    {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            callObserver.setDelegate(self, queue: nil)
            
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                         lblScheduleCount.text = "0"
                           lbltaskCount.layer.cornerRadius = 18.0
                                 lbltaskCount.backgroundColor = UIColor.red
                                 lblScheduleCount.layer.cornerRadius = 18.0
                                 lblScheduleCount.backgroundColor = UIColor.red
                                 lbltaskCount.layer.masksToBounds = true
                                 lblScheduleCount.layer.masksToBounds = true

                       }
            
            buttonRound(sender: btnLeadStatusNew)
            btnLeadStatusNew.layer.cornerRadius = btnLeadStatusNew.frame.size.height/2
            

            
            
            lblHeader.text = ""
            tblViewMatchingAccounts.tableFooterView = UIView()
            tblViewMatchingAccounts.estimatedRowHeight = 50.0
            
            tblviewLeadDetails.tableFooterView = UIView()
            tblviewLeadDetails.estimatedRowHeight = 50.0
            
            const_btnFilterTimeLine_W.constant = 0 + 40
            // btnFilterTimeLine.isHidden = true
            //selectedMatchingAccounts = -1
            isCreateNewLeads = false
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
            
            strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
            
            strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
            
            
            strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            
            strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            
            strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                
                strURL = "\(value)"
                
            }
            strReftype = "WebLead" //"WebLead"//
            strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "leadId") ?? "")"
            strTypeOfTaskToFetch = "WebLead"
            
            if !isInternetAvailable()
            {
                showAccountAndNotesAlert(isOffline: true)
            }
            else
            {
                getNotesAlert()
            }
            //strTypeOfTaskToFetch = "Employee"
            //setDefaultValuesForTimeLineFilter()
            //setDefaultValuesForContactFilter()
            //setDefaultValuesForCompanyFilter()
            serviceNameFromId()
            getLogTypeFromMaster()
            callAllAPISynchronously()
            //setInitialValues()

            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AssociatedCompany_Notification"),
            object: nil,
            queue: nil,
            using:catchNotification)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedNotes_Notification"),
            object: nil,
            queue: nil,
            using:catchNotification1)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AssociatedContact_Notification"),
            object: nil,
            queue: nil,
            using:catchNotification2)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedActivity_Notification"),
            object: nil,
            queue: nil,
            using:catchNotificationActivityAdded)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedTask_Notification"),
            object: nil,
            queue: nil,
            using:catchNotificationTaskAdded)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedDocuments_Notification"),
            object: nil,
            queue: nil,
            using:catchNotificationDocumentsAdded)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "EditLead_Notification"),
            object: nil,
            queue: nil,
            using:catchNotification)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "TimeLine_Notification"),
            object: nil,
            queue: nil,
            using:catchNotification_TimeLine)
            
            
            UserDefaults.standard.set(false, forKey: "isTaskAddedUpdated")
            UserDefaults.standard.set(false, forKey: "isActivityAddedUpdated")
            UserDefaults.standard.set(false, forKey: "isNotesAdded")
            
            //createBadgeView()
          
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                    self.setFooterMenuOption()
                })

        }
       
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setFooterMenuOption()
        })
      }
    func saveWebLeadToCoreData()
    {
        let arrOfKeysNew = NSMutableArray()
        let arrOfValuesNew = NSMutableArray()
        
        arrOfKeysNew.add("address1")
        arrOfKeysNew.add("address2")
        arrOfKeysNew.add("assignedToId")
        arrOfKeysNew.add("assignedToName")
        arrOfKeysNew.add("assignedType")
        arrOfKeysNew.add("branchesSysNames")
        arrOfKeysNew.add("cellPhone1")
        arrOfKeysNew.add("cityName")
        arrOfKeysNew.add("companyKey")
        arrOfKeysNew.add("companyName")
        arrOfKeysNew.add("countryId")
        arrOfKeysNew.add("countyName")
        arrOfKeysNew.add("createdBy")
        arrOfKeysNew.add("createdDate")
        arrOfKeysNew.add("crmContactId")
        arrOfKeysNew.add("firstName")
        arrOfKeysNew.add("flowType")
        arrOfKeysNew.add("industryId")
        arrOfKeysNew.add("lastName")
        arrOfKeysNew.add("latitude")
        arrOfKeysNew.add("leadId")
        arrOfKeysNew.add("leadName")
        arrOfKeysNew.add("leadNumber")
        arrOfKeysNew.add("longitude")
        arrOfKeysNew.add("middleName")
        arrOfKeysNew.add("modifiedBy")
        arrOfKeysNew.add("modifiedDate")
        arrOfKeysNew.add("primaryEmail")
        arrOfKeysNew.add("primaryPhone")
        arrOfKeysNew.add("primaryPhoneExt")
        arrOfKeysNew.add("schoolDistrict")
        arrOfKeysNew.add("secondaryEmail")
        arrOfKeysNew.add("secondaryPhone")
        arrOfKeysNew.add("secondaryPhoneExt")
        arrOfKeysNew.add("stateId")
        arrOfKeysNew.add("statusDate")
        arrOfKeysNew.add("statusName")
        arrOfKeysNew.add("webLeadSourceId")
        arrOfKeysNew.add("zipcode")
        arrOfKeysNew.add("userName")
        arrOfKeysNew.add("crmCompany")
        
        
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "address1") ?? "")")//address1
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "address2") ?? "")")//address2
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "assignedToId") ?? "")")//assignedToId
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "assignedToName") ?? "")")//assignedToName
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "assignedType") ?? "")")//assignedType
        
        
        if (dictLeadDataTemp.value(forKey: "branchesSysNames") ?? "") is NSArray
        {
            let arrBranch = (dictLeadDataTemp.value(forKey: "branchesSysNames") ?? "") as! NSArray
            //arrOfValues.add("\(dict.value(forKey: "BranchesSysNames") ?? "")")//branchesSysNames
            arrOfValuesNew.add(arrBranch)//branchesSysNames
        }
        else
        {
            arrOfValuesNew.add("")
        }
        
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "cellPhone1") ?? "")")//cellPhone1
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "cityName") ?? "")")//cityName
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "companyKey") ?? "")")//companyKey
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "companyName") ?? "")")//companyName
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "countryId") ?? "")")//countryId
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "countyName") ?? "")")//countyName
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "createdBy") ?? "")")//createdBy
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "createdDate") ?? "")")//createdDate
        
        
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "crmContactId") ?? "")")//crmContactId
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "firstName") ?? "")")//firstName
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "flowType") ?? "")")//flowType
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "industryId") ?? "")")//industryId
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "lastName") ?? "")")//lastName
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "latitude") ?? "")")//latitude
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "leadId") ?? "")")//leadId
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "leadName") ?? "")")//leadName
        
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "leadNumber") ?? "")")//leadNumber
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "longitude") ?? "")")//longitude
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "middleName") ?? "")")//middleName
        
        
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "modifiedBy") ?? "")")//modifiedBy
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "modifiedDate") ?? "")")//modifiedDate
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "primaryEmail") ?? "")")//primaryEmail
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "primaryPhone") ?? "")")//primaryPhone
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "primaryPhoneExt") ?? "")")//primaryPhoneExt
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "schoolDistrict") ?? "")")//schoolDistrict
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "secondaryEmail") ?? "")")//secondaryEmail
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "secondaryPhone") ?? "")")//secondaryPhone
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "SecondaryPhoneExt") ?? "")")//secondaryPhoneExt
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "stateId") ?? "")")//stateId
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "statusDate") ?? "")")//statusDate
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "statusName") ?? "")")//statusName
        
        // arrOfValues.add("\(dict.value(forKey: "WebLeadSourceId") ?? "")")//webLeadSourceId
        
        if (dictLeadDataTemp.value(forKey: "webLeadSourceId") ?? "") is NSArray
        {
            let arrWebLeadSourceId = (dictLeadDataTemp.value(forKey: "webLeadSourceId") ?? "") as! NSArray
            arrOfValuesNew.add(arrWebLeadSourceId)//WebLeadSourceId
        }
        else
        {
            arrOfValuesNew.add("")
        }
        
        
        arrOfValuesNew.add("\(dictLeadDataTemp.value(forKey: "zipcode") ?? "")")//zipcode
        
        arrOfValuesNew.add("\(strUserName)")//zipcode
        
        
        if (dictLeadDataTemp.value(forKey: "crmCompany") ?? "") is NSDictionary
        {
            let dictCrmCompany = (dictLeadDataTemp.value(forKey: "crmCompany") ?? "") as! NSDictionary
            arrOfValuesNew.add(dictCrmCompany)//branchesSysNames
        }
        else
        {
            arrOfValuesNew.add("")
        }
        
        saveDataInDB(strEntity: "TotalWebLeads", arrayOfKey: arrOfKeysNew , arrayOfValue: arrOfValuesNew )
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if !Check_Login_Session_expired() {
            viewBtnsContainer.layer.borderColor = hexStringToUIColor(hex: "D0B50E").cgColor
            viewBtnsContainer.layer.borderWidth = 1.0
            viewBtnsContainer.layer.cornerRadius = 5.0
            
            if "\(nsud.value(forKey: "fromAddNotes") ?? "")" == "fromAddNotes"
            {
                //fetchAllNotesFromCoreData()
            }
            
            if nsud.bool(forKey:"fromAddTask") == true
            {
                //fetchTaskListFromLocalDB()
                //fetchActivityListFromLocalDB()
                nsud.set(false, forKey: "fromAddTask")
                nsud.synchronize()
            }
            if nsud.bool(forKey:"fromAddActivity") == true
            {
                //fetchTaskListFromLocalDB()
                //fetchActivityListFromLocalDB()
                nsud.set(false, forKey: "fromAddActivity")
                nsud.synchronize()
            }
            
            if nsud.bool(forKey: "forEditWebLead") == true
            {
                nsud.set(false, forKey: "forEditWebLead")
                nsud.synchronize()
                //self.navigationController?.popViewController(animated: false)
                
                
                //Nilind
                nsud.set(true, forKey: "fromWebLeadDetailTableAction")
                nsud.synchronize()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
                {

                    self.getBasicInfoWebLead()
                    //self.setInitialValues()

                }
               
            }
            if nsud.bool(forKey: "fromAddUpdateTask") == true
            {
                nsud.set(false, forKey: "fromAddUpdateTask")
                nsud.synchronize()
                
                setInitialValues()
                getLogTypeFromMaster()
                
                if chkCalledApiSynchro == true
                {
                    
                }
                else
                {
                    self.callAllAPISynchronously()
                }
            }
            if nsud.bool(forKey: "fromAddUpdateActivity") == true
            {
                nsud.set(false, forKey: "fromAddUpdateActivity")
                nsud.synchronize()
                
                setInitialValues()
                getLogTypeFromMaster()
                
                if chkCalledApiSynchro == true
                {
                    
                }
                else
                {
                    self.callAllAPISynchronously()
                }
            }
            //nsud.set(true, forKey: "fromWebLeadFilter")
            
            if nsud.bool(forKey: "RefreshAssociations_ContactDetails") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "RefreshAssociations_ContactDetails")

                    if chkCalledApiSynchro == true
                    {
                        
                    }
                    else
                    {
                        self.callAllAPISynchronously()
                    }

                }
                
            }else if nsud.bool(forKey: "RefreshAssociations_CompanyDetails") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "RefreshAssociations_CompanyDetails")

                    if chkCalledApiSynchro == true
                    {
                        
                    }
                    else
                    {
                        self.callAllAPISynchronously()
                    }

                }
                
            }else if nsud.bool(forKey: "RefreshAssociations_CompanyDetails") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "RefreshAssociations_CompanyDetails")

                    if chkCalledApiSynchro == true
                    {
                        
                    }
                    else
                    {
                        self.callAllAPISynchronously()
                    }

                }
                
            }else if nsud.bool(forKey: "isTaskAddedUpdated") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "isTaskAddedUpdated")

                    self.callAPToGetTimeLine()//isActivityAddedUpdated

                }
                
            }else if nsud.bool(forKey: "isActivityAddedUpdated") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "isActivityAddedUpdated")

                    self.callAPToGetTimeLine()//isActivityAddedUpdated

                }
                
            }else if nsud.bool(forKey: "isNotesAdded") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "isNotesAdded")

                    self.callAPToGetTimeLine()//isActivityAddedUpdated

                }
                
            }
            self.navigationController?.setNavigationBarHidden(true, animated: false)

        }
        
      
        
    }
    
    // MARK: ---------------------------Footer Functions ---------------------------
    func setFooterMenuOption() {
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Sales", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
            self.view.endEditing(true)
            self.goToDasboard()
           

        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
            self.view.endEditing(true)
            self.goToAppointment()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.goToContactList()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
           
        }
       
    }

    // MARK: Web Service
    
    fileprivate func callAPToGetTimeLine()
    {
        if nsud.value(forKey: "DPS_FilterTimeLine") == nil {
            
            setDefaultValuesForTimeLineFilter()
            
        }
        
        if(isInternetAvailable() == false)
        {
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
            }
            tblviewLeadDetails.reloadData()
           
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            self.dispatchGroup.enter()
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetTimeline
            
            var dict = [String:Any]()
            
            let dictFilterData = (nsud.value(forKey: "DPS_FilterTimeLine")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
            var arySelectedInteraction = NSMutableArray()
            
            if dictFilterData.value(forKey: "SelectedInteraction") is NSArray {
                
                arySelectedInteraction = (dictFilterData.value(forKey: "SelectedInteraction")as! NSArray).mutableCopy()as! NSMutableArray
                
            }else {
                
                
            }
            strRefId = strRefIdNew
            
            dict = ["fromDate":dictFilterData.value(forKey: "FromDate") ?? "",
                    "toDate":dictFilterData.value(forKey: "Todate") ?? "",
                    "refId":strRefId,
                    "refType":enumRefTypeWebLead,
                    "employeeid":dictFilterData.value(forKey: "SelectedUserName") ?? "",
                    "Entities":arySelectedInteraction
                
            ]
            
            WebService.postRequestWithHeaders(dictJson: dict as NSDictionary, url: strURL, responseStringComing: "TimeLine") { (response, status) in
                
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
                self.dispatchGroup.leave()
                if(status == true){
                    
                   // self.dictTimeLine = response.value(forKey: "data") as! NSDictionary
                    
                    self.dictTimeLine = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (response.value(forKey: "data") as! NSDictionary as! [AnyHashable : Any]))! as NSDictionary

                    let aryTemp = NSMutableArray()
                    if self.dictTimeLine.value(forKey: "Activities") is NSArray{
                    if((self.dictTimeLine.value(forKey: "Activities") as! NSArray).count > 0)
                    {
                        for item in self.dictTimeLine.value(forKey: "Activities") as! NSArray{
                            
                            aryTemp.add(item)
                        }
                    }
                    }
                    if self.dictTimeLine.value(forKey: "Emails") is NSArray{
                    if((self.dictTimeLine.value(forKey: "Emails") as! NSArray).count > 0)
                    {
                        for item in self.dictTimeLine.value(forKey: "Emails") as! NSArray{
                            aryTemp.add(item)
                        }
                    }
                    }
                    if self.dictTimeLine.value(forKey: "Notes") is NSArray{
                    if((self.dictTimeLine.value(forKey: "Notes") as! NSArray).count > 0)
                    {
                        for item in self.dictTimeLine.value(forKey: "Notes") as! NSArray{
                            aryTemp.add(item)
                        }
                    }
                    }
                    if self.dictTimeLine.value(forKey: "Tasks") is NSArray{
                    if((self.dictTimeLine.value(forKey: "Tasks") as! NSArray).count > 0)
                    {
                        for item in self.dictTimeLine.value(forKey: "Tasks") as! NSArray{
                            aryTemp.add(item)
                        }
                    }
                    }
                    self.arrayTimeline.removeAllObjects()
                    for item in aryTemp{
                        
                        let innerItem = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        let dateClientCreated = Global().convertDate("\(innerItem.value(forKey: "ClientCreatedDate")!)")
                        
                        let dateFormatter = DateFormatter()
                        
                        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                        dateFormatter.dateFormat = "MM/dd/yyyy"
                        
                        let date1 = dateFormatter.date(from: dateClientCreated!)
                        print(date1!)
                        
                        let date = Global().getOnlyDate(date1)
                        print(date!)
                        
                        innerItem.setValue(date, forKey: "ClientCreatedDate")
                        
                        self.arrayTimeline.add(innerItem)
                    }
                    
                    if(self.arrayTimeline.count > 0){
                        
                        let sortedArray = self.arrayTimeline.sorted(by: {
                            (($0 as! NSMutableDictionary)["ClientCreatedDate"] as? Date)! >= (($1 as! NSMutableDictionary)["ClientCreatedDate"] as? Date)!
                        })
                        
                        self.filterTimelineAry = sortedArray as! [NSMutableDictionary]
                        
                        var arr = [NSMutableDictionary]()
                        
                        arr = sortedArray as! [NSMutableDictionary]
                        
                        self.dictGroupedTimeline = Dictionary(grouping: arr) { $0["ClientCreatedDate"] as! Date }
                        
                        self.aryAllKeys = Array(self.dictGroupedTimeline.keys)
                        
                        let aryAllKeysCopy = self.aryAllKeys
                        
                        self.aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
                        
                    }
                    
                    self.tblviewLeadDetails.reloadData()
                }
                else
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                    }
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }
        }
    }
    fileprivate func callAPToGetTimeLineFilter()
    {
        if nsud.value(forKey: "DPS_FilterTimeLine") == nil {
            
            setDefaultValuesForTimeLineFilter()
            
        }
        
        if(isInternetAvailable() == false)
        {

            tblviewLeadDetails.reloadData()
           
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
          
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetTimeline
            
            var dict = [String:Any]()
            
            let dictFilterData = (nsud.value(forKey: "DPS_FilterTimeLine")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
            var arySelectedInteraction = NSMutableArray()
            
            if dictFilterData.value(forKey: "SelectedInteraction") is NSArray {
                
                arySelectedInteraction = (dictFilterData.value(forKey: "SelectedInteraction")as! NSArray).mutableCopy()as! NSMutableArray
                
            }else {
                
                
            }
            strRefId = strRefIdNew
            
            dict = ["fromDate":dictFilterData.value(forKey: "FromDate") ?? "",
                    "toDate":dictFilterData.value(forKey: "Todate") ?? "",
                    "refId":strRefId,
                    "refType":enumRefTypeWebLead,
                    "employeeid":dictFilterData.value(forKey: "SelectedUserName") ?? "",
                    "Entities":arySelectedInteraction
                
            ]
            
            WebService.postRequestWithHeaders(dictJson: dict as NSDictionary, url: strURL, responseStringComing: "TimeLine") { (response, status) in
                
                
                if(status == true){
                    
                    
                    self.dictTimeLine = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (response.value(forKey: "data") as! NSDictionary as! [AnyHashable : Any]))! as NSDictionary

                    let aryTemp = NSMutableArray()
                    
                    if((self.dictTimeLine.value(forKey: "Activities") as! NSArray).count > 0)
                    {
                        for item in self.dictTimeLine.value(forKey: "Activities") as! NSArray{
                            
                            aryTemp.add(item)
                        }
                    }
                    
                    if((self.dictTimeLine.value(forKey: "Emails") as! NSArray).count > 0)
                    {
                        for item in self.dictTimeLine.value(forKey: "Emails") as! NSArray{
                            aryTemp.add(item)
                        }
                    }
                    
                    if((self.dictTimeLine.value(forKey: "Notes") as! NSArray).count > 0)
                    {
                        for item in self.dictTimeLine.value(forKey: "Notes") as! NSArray{
                            aryTemp.add(item)
                        }
                    }
                    
                    if((self.dictTimeLine.value(forKey: "Tasks") as! NSArray).count > 0)
                    {
                        for item in self.dictTimeLine.value(forKey: "Tasks") as! NSArray{
                            aryTemp.add(item)
                        }
                    }
                    
                    self.arrayTimeline.removeAllObjects()
                    for item in aryTemp{
                        
                        let innerItem = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        let dateClientCreated = Global().convertDate("\(innerItem.value(forKey: "ClientCreatedDate")!)")
                        
                        let dateFormatter = DateFormatter()
                        
                        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                        dateFormatter.dateFormat = "MM/dd/yyyy"
                        
                        let date1 = dateFormatter.date(from: dateClientCreated!)
                        print(date1!)
                        
                        let date = Global().getOnlyDate(date1)
                        print(date!)
                        
                        innerItem.setValue(date, forKey: "ClientCreatedDate")
                        
                        self.arrayTimeline.add(innerItem)
                    }
                    
                    if(self.arrayTimeline.count > 0){
                        
                        let sortedArray = self.arrayTimeline.sorted(by: {
                            (($0 as! NSMutableDictionary)["ClientCreatedDate"] as? Date)! >= (($1 as! NSMutableDictionary)["ClientCreatedDate"] as? Date)!
                        })
                        
                        self.filterTimelineAry = sortedArray as! [NSMutableDictionary]
                        
                        var arr = [NSMutableDictionary]()
                        
                        arr = sortedArray as! [NSMutableDictionary]
                        
                        self.dictGroupedTimeline = Dictionary(grouping: arr) { $0["ClientCreatedDate"] as! Date }
                        
                        self.aryAllKeys = Array(self.dictGroupedTimeline.keys)
                        
                        let aryAllKeysCopy = self.aryAllKeys
                        
                        self.aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
                        
                    }
                    
                    self.tblviewLeadDetails.reloadData()
                }
                else
                {

                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }
        }
    }
    
    // MARK: -  ------------------------------ UITableview delegate and data source  ------------------------------
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == tblViewMatchingAccounts
        {
            return 2
        }
        else if tableView == tblContactNew
        {
            return 1
        }
        else
        {
            switch detailType
            {
              
            case.GeneralNew:
                
                 return 5
                
            case.DocumentsNew:
                
                 return 1
                
             case.DetailsNew:
                
                 return 1
           
            case .Task:
                return 2
                
            case .Notes:
                return 1
            
            case .Timeline:
                return (aryAllKeys.count) + 2
                
            default:
                return 1
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblViewMatchingAccounts
        {
            if(section == 0)
            {
                return arrOfMatchingAccounts.count
            }
            else
            {
                return 0
            }
        }
        else if tableView == tblContactNew
        {
            
            return arrOfContacts.count
            
        }
        else
        {
            switch detailType
            {
                
            case .GeneralNew:
                if(section == 0) //Status
                {
                    return 0
                }
                else if(section == 1) //Company
                {
                    if dictLeadData.value(forKey: "CrmCompany") is NSDictionary
                    {
                        return 1
                    }
                    else
                    {
                        return 0
                    }
                }
                else if(section == 2) //Contact
                {
                    return arrOfContacts.count
                }
                else if(section == 3) // Open Tasks
                {
                    return aryTasksNew.count
                }
                else //Notes
                {
                    return 0//arrAllNotes.count
                }
            case.DocumentsNew:
            
                  return arrAllDocuments.count
                
            case.DetailsNew:
                
                     return 1
            
            case .Task:
                if(section == 0)
                {
                    if aryTasks.count >= 3
                    {
                        return 3
                    }
                    else
                    {
                        return aryTasks.count
                    }
                }
                else
                {
                    if aryActivities.count >= 3
                    {
                        return 3
                    }
                    else
                    {
                        return aryActivities.count
                    }
                }
                
                // return aryActivities.count
                
            case .Notes:
                return arrAllNotes.count
            
            case .Timeline:
               
                if section == 0 // Open Task
                {
                    return aryTasksNew.count
                }
                else if section == 1 //History
                {
                    return 0
                }
                else
                {
                    let key = aryAllKeys[section - 2]
                    let valueForKey = dictGroupedTimeline[key as! AnyHashable]
                    return (valueForKey?.count)!
                }
                
                
            default:
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableView == tblViewMatchingAccounts
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MatchingAccountsCell") as! tblCell
            
            if indexPath.row == selectedMatchingAccounts
            {
                cell.btnCheckBox.setImage(UIImage(named: "check_box_2.png"), for: .normal)
                
            }
            else
            {
                cell.btnCheckBox.setImage(UIImage(named: "check_box_1.png"), for: .normal)
                
            }
            
           
            cell.btnCheckBox.tag = indexPath.row
            
            cell.btnMatchingAccountPrimaryContact.tag = indexPath.row
            cell.btnMatchingAccountPrimaryPhone.tag = indexPath.row
            cell.btnMatchingAccountSecondaryPhone.tag = indexPath.row
            cell.btnMatchingAccountCellNo.tag = indexPath.row
            cell.btnMatchingAccountEmail.tag = indexPath.row
            cell.btnMatchingAccountAddress.tag = indexPath.row
            
            cell.btnMatchingAccountMessagePrimaryPhone.tag = indexPath.row
            cell.btnMatchingAccountMessageSecondaryPhone.tag = indexPath.row
            cell.btnMatchingAccountMessageCellNo.tag = indexPath.row

            
            //cell.btnMatchingAccountPrimaryContact.addTarget(self, action: #selector(action_PrimaryPhone_MatchingAccount), for: .touchUpInside)
            
            cell.btnMatchingAccountPrimaryPhone.addTarget(self, action: #selector(action_PrimaryPhone_MatchingAccount), for: .touchUpInside)
            cell.btnMatchingAccountMessagePrimaryPhone.addTarget(self, action: #selector(action_Message_PrimaryPhone_MatchingAccount), for: .touchUpInside)


            cell.btnMatchingAccountSecondaryPhone.addTarget(self, action: #selector(action_SecondaryPhone_MatchingAccount), for: .touchUpInside)
            cell.btnMatchingAccountMessageSecondaryPhone.addTarget(self, action: #selector(action_Message_SecondaryPhone_MatchingAccount), for: .touchUpInside)

            
            
            cell.btnMatchingAccountCellNo.addTarget(self, action: #selector(action_Cell_MatchingAccount), for: .touchUpInside)
            cell.btnMatchingAccountMessageCellNo.addTarget(self, action: #selector(action_Message_CellPhone_MatchingAccount), for: .touchUpInside)

            
            cell.btnMatchingAccountEmail.addTarget(self, action: #selector(action_PrimaryEmail_MatchingAccount), for: .touchUpInside)
            cell.btnMatchingAccountAddress.addTarget(self, action: #selector(action_Address_MatchingAccount), for: .touchUpInside)
            
            
            
           
            cell.btnCheckBox.addTarget(self, action: #selector(action_CheckBox_MatchingAccount), for: .touchUpInside)
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let dictData = arrOfMatchingAccounts[indexPath.row] as! NSDictionary
            
            //let dictData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (arrOfMatchingAccounts[indexPath.row] as! [AnyHashable : Any])) as NSDictionary

            
            cell.txtFldMatchingAccountOpportunityName.text = "\(dictLeadData.value(forKey: "LeadName") ?? "")"
            
            cell.lblMatchingAccountNo.text = "\(dictData.value(forKey: "AccountNumber") ?? "")"
            
            if cell.txtFldMatchingAccountOpportunityName.text == ""
            {
                cell.viewMatchingAccountOppName.isHidden = true
            }
            else
            {
                cell.viewMatchingAccountOppName.isHidden = true
            }
            
            cell.txtFldMatchingAccountThirdPartyAccNo.text = "\(dictData.value(forKey: "ThirdPartyAccountNumber") ?? "")"
            if cell.txtFldMatchingAccountThirdPartyAccNo.text == ""
            {
                cell.viewMatchingAccountThirdPartyAccNo.isHidden = true
            }
            else
            {
                cell.viewMatchingAccountThirdPartyAccNo.isHidden = false
            }
            
            
           
            cell.txtFldMatchingAccountCompanyName.text = "\(dictData.value(forKey: "CompanyName") ?? "")"
            
            if cell.txtFldMatchingAccountCompanyName.text == ""
            {
                cell.viewMatchingAccountCompanyName.isHidden = true
            }
            else
            {
                cell.viewMatchingAccountCompanyName.isHidden = false
            }
            
            
            cell.txtFldMatchingAccountPrimaryContact.text = Global().strFullName(dictData as? [AnyHashable : Any])
            if cell.txtFldMatchingAccountPrimaryContact.text == ""
            {
                cell.viewMatchingAccountPrimaryContact.isHidden = true
            }
            else
            {
                cell.viewMatchingAccountPrimaryContact.isHidden = false
            }
            

            
            cell.txtFldMatchingAccountPrimaryPhone.text = "\(dictData.value(forKey: "PrimaryPhone") ?? "")"
            if cell.txtFldMatchingAccountPrimaryPhone.text == ""
            {
                cell.viewMatchingAccountPrimaryPhone.isHidden = true
            }
            else
            {
                cell.viewMatchingAccountPrimaryPhone.isHidden = false
            }
            
            
            
            cell.txtFldMatchingAccountSecondaryPhone.text = "\(dictData.value(forKey: "SecondaryPhone") ?? "")"
            
            if cell.txtFldMatchingAccountSecondaryPhone.text == ""
            {
                cell.viewMatchingAccountSecondaryPhone.isHidden = true
            }
            else
            {
                cell.viewMatchingAccountSecondaryPhone.isHidden = false
            }
            
            
            cell.txtFldMatchingAccountCellNo.text = "\(dictData.value(forKey: "CellPhone") ?? "")"
            if  cell.txtFldMatchingAccountCellNo.text == ""
            {
                cell.viewMatchingAccountCell.isHidden = true
            }
            else
            {
                cell.viewMatchingAccountCell.isHidden = false
            }
            
            
            
            cell.txtFldMatchingAccountEmail.text = "\(dictData.value(forKey: "PrimaryEmail") ?? "")"
            if cell.txtFldMatchingAccountEmail.text == ""
            {
                cell.viewMatchingAccountEmail.isHidden = true
            }
            else
            {
                cell.viewMatchingAccountEmail.isHidden = false
            }
            
            
            
            var strCombinedAddress = String()
            strCombinedAddress = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
            
            if strCombinedAddress.count > 0
            {
            }
            else
            {
                strCombinedAddress = ""
            }
            cell.txtFldMatchingAccountAddress.text = strCombinedAddress
            
            
            if cell.txtFldMatchingAccountAddress.text == ""
            {
                cell.viewMatchingAddress.isHidden = true
            }
            else
            {
                cell.viewMatchingAddress.isHidden = false
            }
             
            /*cell.lblMatchingAccountPrimaryContact.text = Global().strFullName(dictData as? [AnyHashable : Any])
            
            cell.btnEmail.setTitle("\(dictData.value(forKey: "PrimaryEmail") ?? "")", for: .normal)
            
            cell.btncellNo.setTitle("\(dictData.value(forKey: "CellPhone") ?? "")", for: .normal)
            cell.btnMatchingAccountsSecondaryPhone.setTitle("\(dictData.value(forKey: "SecondaryPhone") ?? "")", for: .normal)
            
            cell.btnMatchingAccountsPrimaryPhone.setTitle("\(dictData.value(forKey: "PrimaryPhone") ?? "")", for: .normal)
            
            cell.btnMatchingAccountsCell.setTitle("\(dictData.value(forKey: "CellPhone") ?? "")", for: .normal)
            
            
            cell.btnAddress.setTitle("\(strCombinedAddress)", for: .normal)
            
            cell.lblMatchingAccountNo.text = "\(dictData.value(forKey: "AccountNumber") ?? "")"
            
            
            cell.lblMatchingAccountThirdpartyAccNO.text = "\(dictData.value(forKey: "ThirdPartyAccountNumber") ?? "")"
            
            cell.lblCompanynameMatchingAccounts.text = "\(dictData.value(forKey: "CompanyName") ?? "")"*/
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            
            return cell
            
        }
        else if tableView == tblContactNew
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellContact_LeadDetails") as! LeadVCCell
            
             let dictOfContacts = arrOfContacts.object(at: indexPath.row) as! NSDictionary
            //cell.lblNewCustomerName.text = "\(dictOfContacts.value(forKey: "") ?? "")"
            //cell.lblNewCompanyName.text = ""
            
            if((Global().strFullName(dictOfContacts as AnyHashable as? [AnyHashable : Any])!).count > 0)
            {
                
                cell.lblContactNameNew.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any]))
                
                if "\(dictOfContacts.value(forKey: "JobTitle") ?? "")".count > 0 {
                    
                    cell.lblContactNameNew.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any])) + ", \(dictOfContacts.value(forKey: "JobTitle") ?? "")"
                    
                }
                
            }else{
                
                cell.lblContactNameNew.text = ""
                
            }
           // cell.btnDeAssociateContact.tag = indexPath.row
           // cell.btnDeAssociateContact.addTarget(self, action: #selector(actionOnDeAssociateContact), for: .touchUpInside)
            
//            if "\(dictOfContacts.value(forKey: "IsPrimary") ?? "")" == "1" || "\(dictOfContacts.value(forKey: "IsPrimary") ?? "")" == "True"
//            {
//                cell.btnDeAssociateContact.isHidden = true
//            }
//            else
//            {
//                cell.btnDeAssociateContact.isHidden = false
//            }
            
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }
        else
        {
            switch detailType
            {
                
                case .GeneralNew:
               
                    if(indexPath.section == 0) // Status
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTask_LeadDetails") as! LeadVCCell
                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        return cell
                    }
                    else if(indexPath.section == 1) //Company
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellCompany_LeadDetails") as! LeadVCCell
                        
                        if dictLeadData.value(forKey: "CrmCompany") is NSDictionary
                        {
                            
                            cell.lblCompanyName.text = "\(dictLeadData.value(forKeyPath: "CrmCompany.Name") ?? "")"
                        }
                        cell.btnDeAssociateCompany.tag = indexPath.row
                        cell.btnDeAssociateCompany.addTarget(self, action: #selector(actionOnDeAssociateCompany), for: .touchUpInside)
                        cell.btnDeAssociateCompany.isHidden = false
                
                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        return cell
                    }
                    else if(indexPath.section == 2) //Contact
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellContact_LeadDetails") as! LeadVCCell
                        
                         let dictOfContacts = arrOfContacts.object(at: indexPath.row) as! NSDictionary
                        //cell.lblNewCustomerName.text = "\(dictOfContacts.value(forKey: "") ?? "")"
                        cell.lblNewCompanyName.text = ""
                        
                        if((Global().strFullName(dictOfContacts as AnyHashable as? [AnyHashable : Any])!).count > 0)
                        {
                            
                            cell.lblNewCustomerName.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any]))
                            
                            if "\(dictOfContacts.value(forKey: "JobTitle") ?? "")".count > 0 {
                                
                                cell.lblNewCustomerName.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any])) + ", \(dictOfContacts.value(forKey: "JobTitle") ?? "")"
                                
                            }
                            
                        }else{
                            
                            cell.lblNewCustomerName.text = ""
                            
                        }
                        cell.btnDeAssociateContact.tag = indexPath.row
                        cell.btnDeAssociateContact.addTarget(self, action: #selector(actionOnDeAssociateContact), for: .touchUpInside)
                        
                        if "\(dictOfContacts.value(forKey: "IsPrimary") ?? "")" == "1" || "\(dictOfContacts.value(forKey: "IsPrimary") ?? "")" == "True"
                        {
                            cell.btnDeAssociateContact.isHidden = true
                        }
                        else
                        {
                            cell.btnDeAssociateContact.isHidden = false
                        }
                        
                        
                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        return cell
                    }
                    else if(indexPath.section == 3) //Task
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTask_LeadDetails") as! LeadVCCell
                        
                        
                        let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                        
                        cell.btnTaskCheckMark.tag = indexPath.row
                        cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
                        
                        if(aryIndexPath.contains(indexPath.row))
                        {
                            cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                        }
                        else
                        {
                            cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                        }
                        
                        cell.lblTaskName.text = "\(dictData.value(forKey: "TaskName") ?? "")"
                        cell.lblDueDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")//
                        cell.lblDueTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "hh:mm a")
                        
                        if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                        {
                            cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                            
                        }
                        else
                        {
                            cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                            
                        }
                        let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
                       // cell.imgViewTaskType.image = UIImage(named: strImageName)
                        cell.btnImgTask.setImage(UIImage(named: strImageName), for: .normal)
                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        if("\(dictData.value(forKey: "TaskTypeId") ?? "")".count > 0 && "\(dictData.value(forKey: "TaskTypeId") ?? "")" != "<null>")
                        {
                            cell.btnImgTask.isHidden = false
                        }
                        else{
                            cell.btnImgTask.isHidden = true

                        }
                        
                        return cell
                        
                    }
                    else //Notes
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellNotes_LeadDetails") as! LeadVCCell
                        
                        let dict = arrAllNotes.object(at: indexPath.row) as! NSDictionary
                        
                        cell.lblNoteName.text = "\(dict.value(forKey: "Note") ?? "")"
                        cell.lblNoteCreatedBy.text = "\(dict.value(forKey: "CreatedByName") ?? "N/A")"
                        cell.lblNoteCreatedDate.text = changeStringDateToGivenFormat(strDate: "\(dict.value(forKey: "ClientCreatedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        return cell
                    }
                
            case .DocumentsNew:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellDocumentDetail") as! LeadVCCell
                let dict = arrAllDocuments.object(at: indexPath.row) as! NSDictionary
                
                cell.lblDocumentTitle.text = "\(dict.value(forKey: "Title") ?? "")"
                let strDocPathLocal = "\(dict.value(forKey: "Path") ?? "")"

                  
                  
                  if "\(dict.value(forKey: "MediaTypeSysName") ?? "")" == "Image"
                  {
                     if let imgURL = dict.value(forKey: "Path")
                     {
                          if("\(imgURL)" == "<null>" || "\(imgURL)".count == 0 || "\(imgURL)" == " ")
                          {
                             cell.imgViewDocument.image = UIImage(named: "no_image.jpg")
                             cell.imgViewDocument.isHidden = true
                             //cell.lblImageName.isHidden = false
                            // cell.lblImageName.text = firstCharactersFromString(type: "CompanyName", first: name, second: "")

                          }
                         else
                          {
                             
                             cell.imgViewDocument.isHidden = false

                             // Remove Logic for image URL
                             
                             var imgUrlNew = imgURL as! String
                             
                             imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\", with: "/")
                             imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\", with: "/")
                             imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\", with: "/")
                             imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\", with: "/")
                             imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\", with: "/")
                             imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
                              imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
                             
                             imgUrlNew = imgUrlNew.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                           
                             cell.imgViewDocument.setImageWith(URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                         }
                     }else{
                         
                         cell.imgViewDocument.isHidden = true
                        // cell.lblImageName.isHidden = false
                        // let name = dictData.value(forKey: "Name") as! String
                        // cell.lblImageName.text = firstCharactersFromString(type: "CompanyName", first: name, second: "")

                     }
                  }
                  else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Document") == .orderedSame
                  {
                      let image: UIImage = UIImage(named: "exam")!
                      
                      cell.imgViewDocument.image = image
                      
                      /*let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                      
                      let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                      
                      if isImageExists!  {
                          
                          // kch nhi krna
                          
                      }else {
                          
                        strURL = strURL + "\(strDocPathLocal)"
                        
                        strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                        strURL = replaceBackSlasheFromUrl(strUrl: strURL)
                        
                        strURL = strURL.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
                          
                          DispatchQueue.global(qos: .background).async {
                              
                              let url = URL(string:self.strURL)
                              let data = try? Data(contentsOf: url!)
                              
                              if data != nil && (data?.count)! > 0 {
                                  
                                  DispatchQueue.main.async {
                                      
                                      savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                                      
                                  }}
                              
                          }
                          
                      }*/
                      
                  }
                  else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Audio") == .orderedSame
                  {
                      
                      let image: UIImage = UIImage(named: "audio")!
                      
                      cell.imgViewDocument.image = image
                      
                     /* let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                      
                      let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                      
                      if isImageExists!  {
                          
                          // kch nhi krna
                          
                      }else {
                          
                          strURL = strURL + "\(strDocPathLocal)"
                          
                          strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                        
                          strURL = replaceBackSlasheFromUrl(strUrl: strURL)
                          
                          strURL = strURL.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
                          
                          DispatchQueue.global(qos: .background).async {
                              
                              let url = URL(string:self.strURL)
                              let data = try? Data(contentsOf: url!)
                              
                              if data != nil && (data?.count)! > 0 {
                                  
                                  DispatchQueue.main.async {
                                      
                                      savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                                      
                                  }}
                              
                          }
                          
                      }*/
                      
                  }
                  else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Video") == .orderedSame
                  {
                      
                      let image: UIImage = UIImage(named: "video")!
                      
                      cell.imgViewDocument.image = image
                      
                  }
                  
                  
                  cell.selectionStyle = UITableViewCell.SelectionStyle.none
                  return cell
                
            case .DetailsNew:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellDetails_LeadDetails") as! LeadVCCell
                
                
                cell.btnConvertToOpportunity.tag = indexPath.row
                cell.btnCreateFollowUp.tag = indexPath.row
                cell.btnRelease.tag = indexPath.row
                cell.btnGrab.tag = indexPath.row
                cell.btnMarkAsDead.tag = indexPath.row
                
                cell.btnConvertToOpportunity.addTarget(self, action: #selector(actionOnConvertToOpportunity), for: .touchUpInside)
                
                cell.btnCreateFollowUp.addTarget(self, action: #selector(actionOnCreateFollowUp), for: .touchUpInside)
                
                cell.btnRelease.addTarget(self, action: #selector(actionOnRelease), for: .touchUpInside)
                
                cell.btnGrab.addTarget(self, action: #selector(actionOnGrab), for: .touchUpInside)
                
                cell.btnMarkAsDead.addTarget(self, action: #selector(actionOnMarkAsDead), for: .touchUpInside)
                
                makeCornerRadius(value: 3.0, view: cell.btnConvertToOpportunity, borderWidth: 0, borderColor: UIColor.lightGray)
                makeCornerRadius(value: 3.0, view: cell.btnCreateFollowUp, borderWidth: 0, borderColor: UIColor.lightGray)
                makeCornerRadius(value: 3.0, view: cell.btnRelease, borderWidth: 0, borderColor: UIColor.lightGray)
                makeCornerRadius(value: 3.0, view: cell.btnGrab, borderWidth: 0, borderColor: UIColor.lightGray)
                makeCornerRadius(value: 3.0, view: cell.btnMarkAsDead, borderWidth: 0, borderColor: UIColor.lightGray)
                
                //cell.lblEmail.text = "\(dictLeadData.value(forKey: "primaryEmail") ?? "")"
                cell.lblPrimaryEmail.text = "\(dictLeadData.value(forKey: "primaryEmail") ?? "")"
                cell.lblPrimaryPhone.text = "\(dictLeadData.value(forKey: "primaryPhone") ?? "")"
                cell.lblSecondaryPhone.text = "\(dictLeadData.value(forKey: "secondaryPhone") ?? "")"
                cell.lblCell.text = "\(dictLeadData.value(forKey: "cellPhone1") ?? "")"
                
                var strAddress = ""
                
                if "\(dictLeadData.value(forKey: "address1") ?? "")".count > 0
                {
                    strAddress = "\(dictLeadData.value(forKey: "address1") ?? ""), "
                }
                if "\(dictLeadData.value(forKey: "address2") ?? "")".count > 0
                {
                    strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "address2") ?? "")"), "
                }
                if "\(dictLeadData.value(forKey: "cityName") ?? "")".count > 0
                {
                    strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "cityName") ?? "")"), "
                }
                
                if "\(dictLeadData.value(forKey: "stateId") ?? "")".count > 0
                {
                    
                    let stateName = Global().strStatName(fromID: "\((dictLeadData.value(forKey: "stateId") ?? ""))")
                    strAddress = "\(strAddress)\(stateName ?? ""), "
                    
                }
                if "\(dictLeadData.value(forKey: "zipcode") ?? "")".count > 0
                {
                    strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "zipcode") ?? "")")"
                }
                
                if (strAddress.count == 0 || strAddress == "")
                {
                    strAddress = "    "
                }
                cell.lblAddress.text = "\(strAddress)"
                
                
                if dictLeadData.value(forKey: "branchesSysNames") is NSArray
                {
                    let arrBranch = dictLeadData.value(forKey: "branchesSysNames")  as! NSArray
                    
                    let arrBranchName = NSMutableArray()
                    for item in arrBranch
                    {
                        let name = item as! String
                        arrBranchName.add(getBranchNameFromSysName(strSysName: name))
                    }
                    
                  //  let strBranch = arrBranch.componentsJoined(by: ", ")
                    let strBranch = arrBranchName.componentsJoined(by: ", ")

                    
                    cell.lblBranch.text = "\(strBranch)"
                    cell.lblBranchNameNew.text = "\(strBranch)"
                }
                else
                {
                    cell.lblBranch.text = ""
                    cell.lblBranchNameNew.text = ""
                }
                
                if dictLeadData.value(forKey: "webLeadSourceId") is NSArray
                {
                    let arrWebLeadSourceId = dictLeadData.value(forKey: "webLeadSourceId")  as! NSArray
                    
                    let arrWebLeadSourceName = NSMutableArray()
                    
                    for item in arrWebLeadSourceId
                    {
                        let str = item as Any
                        
                        arrWebLeadSourceName.add(sourceNameFromId(sourceId: "\(str)"))
                    }
                    
                    let strWebLeadSourceName = arrWebLeadSourceName.componentsJoined(by: ", ")
                    
                    
                    cell.lblSource.text = "\(strWebLeadSourceName)"
                    cell.lblSourceNameNew.text = "\(strWebLeadSourceName)"
                }
                else
                {
                    cell.lblSource.text = " "
                    cell.lblSourceNameNew.text  = " "
                }
                
                //cell.lblSource.text = "" //"\(dictLeadData.value(forKey: "") ?? "")"
                
                
                cell.lblStatus.text = "\(dictLeadData.value(forKey: "statusName") ?? "")"
                
                cell.lblDetailLeadNo.text = "\(dictLeadData.value(forKey: "leadNumber") ?? "")"
                cell.lblDetailFlowType.text = "\(dictLeadData.value(forKey: "flowType") ?? "")"
                
                cell.lblDetailCreatedBy.text = getEmployeeNameFromId(strEmpId: "\(dictLeadData.value(forKey: "createdBy") ?? "")")//"\(dictLeadData.value(forKey: "createdBy") ?? "")"
                //cell.lblCreatedDate.text = changeStringDateToGivenFormat(strDate: "\(dictLeadData.value(forKey: "createdDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
                
                
                // If Status New
                
                
                cell.btnCreateFollowUp.isHidden = false
                cell.btnRelease.isHidden = false
                cell.btnGrab.isHidden = false
                cell.btnMarkAsDead.isHidden = false
                cell.btnConvertToOpportunity.isHidden = false
                
                
                if "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("New") == .orderedSame
                {
                    cell.btnCreateFollowUp.isHidden = true
                    cell.btnRelease.isHidden = true
                }
                else if  "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("Grabbed") == .orderedSame
                {
                    cell.btnGrab.isHidden = true
                }
                else if  "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("FollowUp") == .orderedSame
                {
                    cell.btnGrab.isHidden = true
                }
                else if  "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("Dead") == .orderedSame
                {
                    cell.btnCreateFollowUp.isHidden = true
                    cell.btnRelease.isHidden = true
                    cell.btnMarkAsDead.isHidden = true
                }
                else if  "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("Converted") == .orderedSame
                {
                    //cell.btntas.isHidden = true
                    //cell.btnActivity.isHidden = true
                    //cell.btnNotes.isHidden = true
                    //cell.btnOpportunity.isHidden = true
                    cell.btnGrab.isHidden = true
                    cell.btnCreateFollowUp.isHidden = true
                    cell.btnRelease.isHidden = true
                    cell.btnConvertToOpportunity.isHidden = true
                    cell.btnMarkAsDead.isHidden = true
                    
                    // cell.btnEditLead.isHidden = true
                }
                
                //New
                
                cell.btnDetailMsgPrimaryPhone.tag = indexPath.row
                cell.btnDetailCallPrimaryPhone.tag = indexPath.row
                cell.btnDetailMsgSecondaryPhone.tag = indexPath.row
                cell.btnDetailCallSecondaryPhone.tag = indexPath.row
                cell.btnDetailPrimaryEmail.tag = indexPath.row
                cell.btnDetailSecondaryEmail.tag = indexPath.row

                cell.btnDetailMsgPrimaryPhone.addTarget(self, action: #selector(actionOnMsgPrimaryPhone), for: .touchUpInside)
                cell.btnDetailCallPrimaryPhone.addTarget(self, action: #selector(actionOnCallPrimaryPhone), for: .touchUpInside)
                
                cell.btnDetailMsgSecondaryPhone.addTarget(self, action: #selector(actionOnMsgSecondaryPhone), for: .touchUpInside)
                cell.btnDetailCallSecondaryPhone.addTarget(self, action: #selector(actionOnCallSecondaryPhone), for: .touchUpInside)
                
                cell.btnDetailMsgCell.addTarget(self, action: #selector(actionOnMsgCell), for: .touchUpInside)
                cell.btnDetailCallCell.addTarget(self, action: #selector(actionOnCallCell), for: .touchUpInside)
                
                cell.btnDetailPrimaryEmail.addTarget(self, action: #selector(actionOnPrimaryEmail), for: .touchUpInside)
                cell.btnDetailSecondaryEmail.addTarget(self, action: #selector(actionOnSecondaryEmail), for: .touchUpInside)

                //End
                
                cell.viewDetailPrimaryPhone.isHidden = false
                cell.viewDetailSecondaryPhone.isHidden = false
                cell.viewDetailCellPhone.isHidden = false
                cell.viewDetailPrimaryEmail.isHidden = false
                cell.viewDetailSecondaryEmail.isHidden = false
                cell.viewDetailBranch.isHidden = false
                cell.viewDetailSource.isHidden = false
                cell.viewDetailStatus.isHidden = false
                cell.viewDetailLeadNo.isHidden = false
                cell.viewDetailFlowType.isHidden = false
                cell.viewDetailCreatedBy.isHidden = false
                
                if (cell.lblPrimaryPhone.text?.count == 0 || cell.lblPrimaryPhone.text == "")
                {
                    cell.viewDetailPrimaryPhone.isHidden = true
                }
                if (cell.lblSecondaryPhone.text?.count == 0 || cell.lblSecondaryPhone.text == "")
                {
                    cell.viewDetailSecondaryPhone.isHidden = true
                }
                if (cell.lblCell.text?.count == 0 || cell.lblCell.text == "")
                {
                    cell.viewDetailCellPhone.isHidden = true
                }
                if (cell.lblPrimaryEmail.text?.count == 0 || cell.lblPrimaryEmail.text == "")
                {
                    cell.viewDetailPrimaryEmail.isHidden = true
                }
                if (cell.lblSecondaryEmail.text?.count == 0 || cell.lblSecondaryEmail.text == "")
                {
                    cell.viewDetailSecondaryEmail.isHidden = true
                }
                if (cell.lblBranch.text?.count == 0 || cell.lblBranch.text == "")
                {
                    cell.viewDetailBranch.isHidden = true
                }
                if (cell.lblSource.text?.count == 0 || cell.lblSource.text == "")
                {
                    cell.viewDetailSource.isHidden = true
                }
                if (cell.lblStatus.text?.count == 0 || cell.lblStatus.text == "")
                {
                    cell.viewDetailStatus.isHidden = true
                }
                if (cell.lblDetailLeadNo.text?.count == 0 || cell.lblDetailLeadNo.text == "")
                {
                    cell.viewDetailLeadNo.isHidden = true
                }
                if (cell.lblDetailFlowType.text?.count == 0 || cell.lblDetailFlowType.text == "")
                {
                    cell.viewDetailFlowType.isHidden = true
                }
                if (cell.lblDetailCreatedBy.text?.count == 0 || cell.lblDetailCreatedBy.text == "")
                {
                    cell.viewDetailCreatedBy.isHidden = true
                }
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                
                
                cell.viewDetailPrimaryPhone.isHidden = true
                cell.viewDetailSecondaryPhone.isHidden = true
                cell.viewDetailCellPhone.isHidden = true
                cell.viewDetailPrimaryEmail.isHidden = true
                cell.viewDetailSecondaryEmail.isHidden = true
                cell.viewDetailBranch.isHidden = true
                cell.viewDetailSource.isHidden = true
                cell.viewDetailStatus.isHidden = true
                cell.viewDetailLeadNo.isHidden = true
                cell.viewDetailFlowType.isHidden = true
                cell.viewDetailCreatedBy.isHidden = true
                
                // Detail NEW Code
                
                cell.lblSourceNameNew.text = " "
                
                let defsLogindDetail = UserDefaults.standard
                
                if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
                    
                    let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
                    
                    if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
                        
                        let arrOfData = dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray
                        
                        if(arrOfData.count > 0)
                        {
                            
                            if dictLeadData.value(forKey: "WebLeadSourceId") is NSArray {
                                
                                let arrWebLeadSourceId = dictLeadData.value(forKey: "WebLeadSourceId")  as! NSArray

                                var strSourceName = Global().getSourceNameMultiple(arrWebLeadSourceId as? [Any], arrOfData as? [Any])
                                
                                
                                if strSourceName == "" || arrWebLeadSourceId.count == 0
                                {
                                    strSourceName = " "
                                }
                                cell.lblSourceNameNew.text = strSourceName
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                if dictLeadData.value(forKey: "BranchesSysNames") is NSArray
                {
                    let arrBranchSysName = dictLeadData.value(forKey: "BranchesSysNames")  as! NSArray
                    
                    let strBranchName = Global().getBranchNameMultiple(arrBranchSysName as? [Any])
                    
                    cell.lblBranchNameNew.text = strBranchName
                }
                else
                {
                    cell.lblBranchNameNew.text = ""
                }
                
                
                if dictLeadData.value(forKey: "WebLeadServices") is NSArray
                {
                    let arrService = dictLeadData.value(forKey: "WebLeadServices") as! NSArray
                    let arrServiceName = NSMutableArray()
                    for item in arrService
                    {
                        let dict  = item as! NSDictionary
                        let strName = "\(dict.value(forKey: "ServiceId") ?? "")"
                        arrServiceName.add("\(dictServiceNameFromId.value(forKey: strName) ?? "")")
                    }
                                        
                    let strServiceName = arrServiceName.componentsJoined(by: ", ")
                    
                    cell.lblServiceNameNew.text = strServiceName
                }
                else
                {
                    cell.lblServiceNameNew.text = ""
                }
                
                
                
                cell.lblAccNoNew.text = "\(dictLeadData.value(forKey: "AccountNo") ?? "")"
                cell.lblLeadNoNew.text =   "\(dictLeadData.value(forKey: "LeadNumber") ?? "")"
                cell.lblTypeNew.text = "\(dictLeadData.value(forKey: "FlowType") ?? "")"
                cell.lblCreatedByNew.text = getEmployeeNameFromId(strEmpId: "\(dictLeadData.value(forKey: "CreatedBy") ?? "")")
                //cell.lblCreatedDateTimeNew.text = changeStringDateToGivenFormat(strDate: "\(dictLeadData.value(forKey: "CreatedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
                cell.lblCreatedDateTimeNew.text = serverDateToLocal(strDate: "\(dictLeadData.value(forKey: "CreatedDate") ?? "")", strServerTimeZone: "EST", strRequiredFormat: "MM/dd/yyyy hh:mm a")
                
                if "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Converted") == .orderedSame
                {
                    cell.viewSeperator.isHidden = false
                    cell.viewOpportunityDetail.isHidden = false
                }
                else
                {
                    cell.viewSeperator.isHidden = true
                    cell.viewOpportunityDetail.isHidden = true
                }
                //OppData
                //changeStringDateToGivenFormat(strDate: "\(matchesGeneralInfo.value(forKey: "scheduleStartDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
                //changeStringDateToGivenFormat(strDate: "\(matchesGeneralInfo.value(forKey: "scheduleTime") ?? "")", strRequiredFormat: "HH:mm:ss")
                let dictOpp = dictLeadData.value(forKey: "OpportunityDetail") as! NSDictionary

                cell.lblOpportunityNo.text = "\(dictOpp.value(forKey: "OpportunityNo") ?? "")"
                if dictOpp["OpportunityAccountNo"] != nil
                {
                    cell.lblOppAccNo.text = "\(dictOpp.value(forKey: "OpportunityAccountNo") ?? "")"
                }
                else
                {
                    cell.lblOppAccNo.text = "\(dictLeadData.value(forKey: "AccountNo") ?? "")"
                }
                cell.lblOppFieldSalesPerson.text = "\(dictOpp.value(forKey: "FieldSalesPersonName") ?? "")"
                cell.lblOppCreatedDateTime.text = serverDateToLocal(strDate: "\(dictOpp.value(forKey: "CreatedDate") ?? "")", strServerTimeZone: "EST", strRequiredFormat: "MM/dd/yyyy hh:mm a")//changeStringDateToGivenFormat(strDate: "\(dictOpp.value(forKey: "CreatedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
                cell.lblOppCreatedBy.text = "\(dictOpp.value(forKey: "CreatedByName") ?? "")"
                
                var strDateTime = ""
                if "\(dictOpp.value(forKey: "ScheduleDate") ?? "")".count > 0
                {
                    strDateTime = changeStringDateToGivenFormat(strDate: "\(dictOpp.value(forKey: "ScheduleDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
                    
                    if "\(dictOpp.value(forKey: "ScheduleTime") ?? "")".count > 0
                    {
                        strDateTime = strDateTime + " " + changeStringDateToGivenFormat(strDate: "\(dictOpp.value(forKey: "ScheduleTime") ?? "")", strRequiredFormat: "hh:mm a")
                    }
                }
            
                cell.lblOppScheduleDateTime.text = strDateTime
                
                var strService = "\(dictOpp.value(forKey: "ServiceNames") ?? "")" //"fig usdofju sadfj lasjf sakjf sfjasofi jsa fjsakfj asofi jsaofd jsaodifj saodfj saodifj askdfj aspdf juasdgj aldsfj asdfj asoidfj asdkf j"//"\(dictOpp.value(forKey: "ServiceNames") ?? "")"
                if strService.count == 0 || strService == ""
                {
                    strService = "  "
                }
                cell.lblOppServices.text = strService
                
                
                cell.lblOppValue.text = convertDoubleToCurrency(amount: Double("\(dictOpp.value(forKey: "OpportunityValue") ?? "")") ?? 0.0)//"\(dictOpp.value(forKey: "OpportunityValue") ?? "")"
                cell.lblOppStatus.text = "\(dictOpp.value(forKey: "StatusName") ?? "") - \(dictOpp.value(forKey: "StageName") ?? "")"
                if "\(dictOpp.value(forKey: "CancelReason") ?? "")".count == 0
                {
                    cell.lblOppReason.isHidden = true
                    cell.lblOppReasonTitle.isHidden = true
                }
                else
                {
                    cell.lblOppReason.isHidden = false
                    cell.lblOppReasonTitle.isHidden = false
                }
                cell.lblOppReason.text = "\(dictOpp.value(forKey: "CancelReason") ?? "")"
                
                
                if dictOpp["OpportunitySourceId"] != nil
                {
                    print("Opportunity Siurce ID not present")
                    
                    if dictOpp.value(forKey: "OpportunitySourceId") is NSArray
                    {
                        let arrWebLeadSourceId = dictOpp.value(forKey: "OpportunitySourceId")  as! NSArray
                        
                        let arrWebLeadSourceName = NSMutableArray()
                        
                        for item in arrWebLeadSourceId
                        {
                            let str = item as Any
                            
                            arrWebLeadSourceName.add(sourceNameFromId(sourceId: "\(str)"))
                        }
                        
                        if arrWebLeadSourceName.count == 0
                        {
                            cell.lblOppSource.text = " "
                        }
                        else
                        {
                            let strWebLeadSourceName = arrWebLeadSourceName.componentsJoined(by: ", ")
                            
                            cell.lblOppSource.text = strWebLeadSourceName
                        }
                       
                    }
                    else
                    {
                        cell.lblOppSource.text = " "
                    }

                }
                else
                {
                    cell.lblOppSource.text = "  "
                }
                return cell
                
            case .Task:
                if(indexPath.section == 0)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTask_LeadDetails") as! LeadVCCell
                    
                    let dictData = aryTasks.object(at: indexPath.row) as! NSDictionary
                    
                    cell.btnTaskCheckMark.tag = indexPath.row
                    cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
                    
                    if(aryIndexPath.contains(indexPath.row))
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                    }
                    
                    cell.lblTaskName.text = "\(dictData.value(forKey: "taskName") ?? "")"
                    cell.lblDueDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "dueDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")//
                    cell.lblDueTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "dueDate") ?? "")", strRequiredFormat: "hh:mm a")
                    
                    if("\(dictData.value(forKey: "status")!)" == "Open" || "\(dictData.value(forKey: "status")!)" == "open")
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                        
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                        
                    }
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    
                    return cell
                    
                }
                if(indexPath.section == 1)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellActivity_LeadDetails") as! LeadVCCell
                    
                    let dictData = aryActivities.object(at: indexPath.row) as! NSDictionary
                    
                    if(("\(dictData.value(forKey: "agenda") ?? "")".count > 0))
                        
                    {
                        cell.lblActivityName.text = "\(dictData.value(forKey: "agenda") ?? "")"
                        
                    }
                    else
                    {
                        cell.lblActivityName.text = "\(dictData.value(forKey: "Agenda") ?? "")"
                    }
                    
                    
                    
                    for item in aryLogType
                    {
                        if(("\(dictData.value(forKey: "logTypeId") ?? "")" == "\((item as! NSDictionary).value(forKey: "LogTypeId") ?? "")") || ("\(dictData.value(forKey: "LogTypeId") ?? "")" == "\((item as! NSDictionary).value(forKey: "LogTypeId") ?? "")") )
                        {
                            
                            cell.lblLogType.text = "\((item as! NSDictionary).value(forKey: "Name")!)"
                            
                            break
                            
                        }
                        
                    }
                    if(("\(dictData.value(forKey: "createdDate") ?? "")".count > 0))
                    {
                        cell.lblDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "createdDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
                        
                        cell.lblTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "createdDate") ?? "")", strRequiredFormat: "hh:mm a")
                    }
                    else
                    {
                        cell.lblDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "CreatedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
                        
                        
                        
                        cell.lblTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "CreatedDate") ?? "")", strRequiredFormat: "hh:mm a")
                    }
                    
                    
                    
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    return cell
                    
                }
            case .Notes:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellNotes_LeadDetails") as! LeadVCCell
                
                let dict = arrAllNotes.object(at: indexPath.row) as! NSManagedObject
                
                cell.lblNoteName.text = "\(dict.value(forKey: "Note") ?? "")"
                cell.lblNoteCreatedBy.text = "\(dict.value(forKey: "CreatedByName") ?? "N/A")"
                cell.lblNoteCreatedDate.text = changeStringDateToGivenFormat(strDate: "\(dict.value(forKey: "ClientCreatedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                
                return cell
                
            case .Timeline:
                
                if indexPath.section == 0
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTask_LeadDetails") as! LeadVCCell
                    
                    
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    
                    cell.btnTaskCheckMark.tag = indexPath.row
                    cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
                    
                    if(aryIndexPath.contains(indexPath.row))
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                    }
                    
                    cell.lblTaskName.text = "\(dictData.value(forKey: "TaskName") ?? "")"
                    cell.lblDueDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")//
                    cell.lblDueTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "hh:mm a")
                    
                    if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                    }
                    let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
                   // cell.imgViewTaskType.image = UIImage(named: strImageName)
                    cell.btnImgTask.setImage(UIImage(named: strImageName), for: .normal)
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    if("\(dictData.value(forKey: "TaskTypeId") ?? "")".count > 0 && "\(dictData.value(forKey: "TaskTypeId") ?? "")" != "<null>")
                    {
                        cell.btnImgTask.isHidden = false
                    }
                    else{
                        cell.btnImgTask.isHidden = true

                    }
                    return cell
                }
                else
                {
                    let key = aryAllKeys[indexPath.section - 2]
                      var valueForKey = dictGroupedTimeline[key as! AnyHashable]
                      let list = [Int](1...4)
                      let arrayLoc = NSMutableArray()
                      
                      for (index, element) in list.enumerated() {
                          if(index == 0){
                              
                              for item in valueForKey!{
                                  
                                  let dictLoc = item as NSDictionary
                                  
                                  if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                                      
                                      arrayLoc.add(dictLoc)
                                  }
                                  
                              }
                              
                          }
                          if(index == 1){
                              
                              for item in valueForKey!{
                                  
                                  let dictLoc = item as NSDictionary
                                  
                                  if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                                      
                                      arrayLoc.add(dictLoc)
                                  }
                                  
                              }
                          }
                          
                          if(index == 2){
                              
                              for item in valueForKey!{
                                  
                                  let dictLoc = item as NSDictionary
                                  
                                  if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                                      
                                      arrayLoc.add(dictLoc)
                                  }
                                  
                              }
                          }
                          if(index == 3){
                              
                              for item in valueForKey!{
                                  
                                  let dictLoc = item as NSDictionary
                                  
                                  if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                                      
                                      arrayLoc.add(dictLoc)
                                  }
                                  
                              }
                          }
                      }
                      
                      valueForKey = (arrayLoc as! [NSMutableDictionary])
                      
                      let dictData = valueForKey![indexPath.row]
                      
                      if("\(dictData.value(forKey: "EntityType")!)" == "Note"){
                          let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellNotesNew_WebLeadDetails") as! LeadVCCell
                          cell.lblNoteName.text = ""
                          
                          if let noteName = dictData.value(forKey: "Note"){
                              cell.lblNoteName.numberOfLines = 0
                              cell.lblNoteName.text = "Note - \(noteName)"
                          }
                          cell.selectionStyle = UITableViewCell.SelectionStyle.none
                          return cell
                      }
                      
                      if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                          
                          let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellActivity_WebLeadDetails") as! LeadVCCell
                          
                          if let agenda = dictData.value(forKey: "Agenda"){
                              cell.lblActivityName.text = "\(agenda)"
                          }else{
                              cell.lblActivityName.text = ""
                          }
                          
                          if let clientCreatedDate = dictData.value(forKey: "ClientCreatedDate"){
                              
                              cell.lblDate.text = changeDateToString(date: clientCreatedDate as! Date)
                              
                          }else{
                              cell.lblDate.text = ""
                          }
                          
                          for item in aryLogType
                          {
                              if("\(dictData.value(forKey: "LogTypeId")!)" == "\((item as! NSDictionary).value(forKey: "LogTypeId")!)" )
                              {
                                  cell.lblLogType.text = "\((item as! NSDictionary).value(forKey: "Name")!)" + " " + "|"
                                  
                                  break
                              }
                          }
                          cell.btnActivityComment.tag = indexPath.row*1000+indexPath.section
                          cell.btnActivityComment.addTarget(self, action: #selector(actionOnActvityComment), for: .touchUpInside)
                          cell.selectionStyle = UITableViewCell.SelectionStyle.none
                          return cell
                      }
                      
                      if("\(dictData.value(forKey: "EntityType")!)" == "Task"){
                          
                          let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTaskNew_WebLeadDetails") as! LeadVCCell
                          
                          if let taskname = dictData.value(forKey: "TaskName"){
                              cell.lblTaskName.text = "\(taskname)"
                          }else{
                              cell.lblTaskName.text = ""
                          }
                          
                          if let companyName = dictData.value(forKey: "CompanyName"){
                              
                              if("\(companyName)" == "<null>")
                              {
                                  cell.lblCompanyName.text = ""
                              }
                              else
                              {
                                  cell.lblCompanyName.text = "\(companyName)" + " " + "|"
                              }
                          }
                          else
                          {
                              cell.lblTaskName.text = ""
                          }
                          
                          if let assignTo = dictData.value(forKey: "AssignedToStr"){
                              
                              if("\(assignTo)" == "<null>")
                              {
                                  cell.lblAssignTo.text = ""
                              }
                              else
                              {
                                  cell.lblAssignTo.text = "\(assignTo)"
                              }
                          }
                          else
                          {
                              cell.lblAssignTo.text = ""
                          }
                          let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
                          cell.imgTaskType.image = UIImage(named: strImageName)
                          cell.selectionStyle = UITableViewCell.SelectionStyle.none
                          return cell
                      }
                      
                      if("\(dictData.value(forKey: "EntityType")!)" == "Email"){
                          
                          let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellEmail_WebLeadDetails") as! LeadVCCell
                          
                          if let emailSubject = dictData.value(forKey: "EmailSubject"){
                              
                              if("\(emailSubject)" == "<null>")
                              {
                                  cell.lblSubject.text = ""
                              }
                              else
                              {
                                  cell.lblSubject.text = "Subject: \(emailSubject)"
                              }
                          }
                          else
                          {
                              cell.lblSubject.text = ""
                          }
                          
                          if let emailFrom = dictData.value(forKey: "EmailFrom"){
                              
                              if("\(emailFrom)" == "<null>")
                              {
                                  cell.lblEmailFrom.text = ""
                              }
                              else
                              {
                                  cell.lblEmailFrom.text = "From: \(emailFrom)"
                              }
                          }
                          else
                          {
                              cell.lblEmailFrom.text = ""
                          }
                          
                          if let emailContent = dictData.value(forKey: "EmailNotes"){
                              
                              if("\(emailContent)" == "<null>")
                              {
                                  cell.constHghtTxtviewEmailContent.constant = 0.0
                                  cell.txtviewEmailContent.text = ""
                              }
                              else
                              {
                                  DispatchQueue.main.async {
                                      
                                      let noSpaceAttributedString =
                                          htmlAttributedString(strHtmlString: "\(emailContent)").trimmedAttributedString(set: CharacterSet.whitespacesAndNewlines)
                                      
                                      cell.txtviewEmailContent.attributedText = noSpaceAttributedString
                                      cell.constHghtTxtviewEmailContent.constant = 100.0
                                  }
                              }
                          }
                          else
                          {
                              cell.txtviewEmailContent.text = ""
                              cell.constHghtTxtviewEmailContent.constant = 100.0
                          }
                          
                          /*[textView setTextContainerInset:UIEdgeInsetsZero];
                           textView.textContainer.lineFragmentPadding = 0;*/
                          cell.txtviewEmailContent.textContainerInset = .zero
                          cell.txtviewEmailContent.textContainer.lineFragmentPadding = 0
                          cell.txtviewEmailContent.layer.cornerRadius = 2.0
                          cell.txtviewEmailContent.layer.borderWidth = 0.5
                          cell.txtviewEmailContent.layer.borderColor = UIColor.lightGray.cgColor
                          cell.selectionStyle = UITableViewCell.SelectionStyle.none
                          return cell
                      }
                      
                      return UITableViewCell()
                }
                
                
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellDetails_LeadDetails") as! LeadVCCell
                cell.btnConvertToOpportunity.tag = indexPath.row
                cell.btnCreateFollowUp.tag = indexPath.row
                cell.btnRelease.tag = indexPath.row
                cell.btnGrab.tag = indexPath.row
                cell.btnMarkAsDead.tag = indexPath.row
                
                cell.btnConvertToOpportunity.addTarget(self, action: #selector(actionOnConvertToOpportunity), for: .touchUpInside)
                
                cell.btnCreateFollowUp.addTarget(self, action: #selector(actionOnCreateFollowUp), for: .touchUpInside)
                
                cell.btnRelease.addTarget(self, action: #selector(actionOnRelease), for: .touchUpInside)
                
                cell.btnGrab.addTarget(self, action: #selector(actionOnGrab), for: .touchUpInside)
                
                cell.btnMarkAsDead.addTarget(self, action: #selector(actionOnMarkAsDead), for: .touchUpInside)
                
                makeCornerRadius(value: 3.0, view: cell.btnConvertToOpportunity, borderWidth: 0, borderColor: UIColor.lightGray)
                makeCornerRadius(value: 3.0, view: cell.btnCreateFollowUp, borderWidth: 0, borderColor: UIColor.lightGray)
                makeCornerRadius(value: 3.0, view: cell.btnRelease, borderWidth: 0, borderColor: UIColor.lightGray)
                makeCornerRadius(value: 3.0, view: cell.btnGrab, borderWidth: 0, borderColor: UIColor.lightGray)
                makeCornerRadius(value: 3.0, view: cell.btnMarkAsDead, borderWidth: 0, borderColor: UIColor.lightGray)
                
                cell.lblEmail.text = "\(dictLeadData.value(forKey: "primaryEmail") ?? "")"
                cell.lblPrimaryPhone.text = "\(dictLeadData.value(forKey: "primaryPhone") ?? "")"
                cell.lblSecondaryPhone.text = "\(dictLeadData.value(forKey: "secondaryPhone") ?? "")"
                cell.lblCell.text = "\(dictLeadData.value(forKey: "cellPhone1") ?? "")"
                
                var strAddress = ""
                
                if "\(dictLeadData.value(forKey: "address1") ?? "")".count > 0
                {
                    strAddress = "\(dictLeadData.value(forKey: "address1") ?? ""), "
                }
                if "\(dictLeadData.value(forKey: "address2") ?? "")".count > 0
                {
                    strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "address2") ?? "")"), "
                }
                if "\(dictLeadData.value(forKey: "cityName") ?? "")".count > 0
                {
                    strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "cityName") ?? "")"), "
                }
                if "\(dictLeadData.value(forKey: "stateId") ?? "")".count > 0
                {
                    
                    let stateName = Global().strStatName(fromID: "\((dictLeadData.value(forKey: "stateId") ?? ""))")
                    strAddress = "\(strAddress)\(stateName ?? ""), "
                    
                }
                if "\(dictLeadData.value(forKey: "zipcode") ?? "")".count > 0
                {
                    strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "zipcode") ?? "")")"
                }
                
                if (strAddress.count == 0 || strAddress == "")
                {
                    strAddress = "    "
                }
                cell.lblAddress.text = "\(strAddress)"
                
                
                if dictLeadData.value(forKey: "branchesSysNames") is NSArray
                {
                    let arrBranch = dictLeadData.value(forKey: "branchesSysNames")  as! NSArray
                    
                    let arrBranchName = NSMutableArray()
                    for item in arrBranch
                    {
                        let name = item as! String
                        arrBranchName.add(getBranchNameFromSysName(strSysName: name))
                    }
                    
                  //  let strBranch = arrBranch.componentsJoined(by: ", ")
                    let strBranch = arrBranchName.componentsJoined(by: ", ")

                    
                    cell.lblBranch.text = "\(strBranch)"
                }
                else
                {
                    cell.lblBranch.text = ""
                }
                
                if dictLeadData.value(forKey: "webLeadSourceId") is NSArray
                {
                    let arrWebLeadSourceId = dictLeadData.value(forKey: "webLeadSourceId")  as! NSArray
                    
                    let arrWebLeadSourceName = NSMutableArray()
                    
                    for item in arrWebLeadSourceId
                    {
                        let str = item as Any
                        
                        arrWebLeadSourceName.add(sourceNameFromId(sourceId: "\(str)"))
                    }
                    
                    let strWebLeadSourceName = arrWebLeadSourceName.componentsJoined(by: ", ")
                    
                    
                    cell.lblSource.text = "\(strWebLeadSourceName)"
                }
                else
                {
                    cell.lblSource.text = ""
                }
                
                //cell.lblSource.text = "" //"\(dictLeadData.value(forKey: "") ?? "")"
                
                
                cell.lblStatus.text = "\(dictLeadData.value(forKey: "statusName") ?? "")"
                
                
                
                // If Status New
                
                
                cell.btnCreateFollowUp.isHidden = false
                cell.btnRelease.isHidden = false
                cell.btnGrab.isHidden = false
                cell.btnMarkAsDead.isHidden = false
                cell.btnConvertToOpportunity.isHidden = false
                
                
                if "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("New") == .orderedSame
                {
                    cell.btnCreateFollowUp.isHidden = true
                    cell.btnRelease.isHidden = true
                }
                else if  "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("Grabbed") == .orderedSame
                {
                    cell.btnGrab.isHidden = true
                }
                else if  "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("FollowUp") == .orderedSame
                {
                    cell.btnGrab.isHidden = true
                }
                else if  "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("Dead") == .orderedSame
                {
                    cell.btnCreateFollowUp.isHidden = true
                    cell.btnRelease.isHidden = true
                    cell.btnMarkAsDead.isHidden = true
                }
                else if  "\(dictLeadData.value(forKey: "statusName") ?? "")".caseInsensitiveCompare("Converted") == .orderedSame
                {
                    //cell.btntas.isHidden = true
                    //cell.btnActivity.isHidden = true
                    //cell.btnNotes.isHidden = true
                    //cell.btnOpportunity.isHidden = true
                    cell.btnGrab.isHidden = true
                    cell.btnCreateFollowUp.isHidden = true
                    cell.btnRelease.isHidden = true
                    cell.btnConvertToOpportunity.isHidden = true
                    cell.btnMarkAsDead.isHidden = true
                    
                    // cell.btnEditLead.isHidden = true
                }
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                

                
                
                return cell
            }
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        if tableView == tblViewMatchingAccounts
        {
            let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
            viewheader.backgroundColor = UIColor.white//lightColorForCell()
            
            if(section == 0)
            {
               /* let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: tblViewMatchingAccounts.frame.width - 50, height: 40))
                lbl.text = "Matching Accounts"
                viewheader.addSubview(lbl)*/
                
                let button = UIButton()
                button.frame = CGRect( x: 5, y: 0, width: (DeviceType.IS_IPAD ? 50 : 40), height: (DeviceType.IS_IPAD ? 50 : 40))
                if (isCreateNewLeads)
                {
                    button.setImage(UIImage(named: "check_box_2.png"), for: .normal)
                }
                else
                {
                    button.setImage(UIImage(named: "check_box_1.png"), for: .normal)
                    
                }
                button.addTarget(self, action: #selector(isCreateNewLead), for: .touchUpInside)
                
                viewheader.addSubview(button)
                
                let lbl = UILabel(frame: CGRect(x: (DeviceType.IS_IPAD ? 60 : 50), y: 0, width: tblViewMatchingAccounts.frame.width - 100, height: (DeviceType.IS_IPAD ? 50 : 40)))
                lbl.text = "Create New Opportunity"
                lbl.font = UIFont.systemFont(ofSize: (DeviceType.IS_IPAD ? 21 : 18))
                viewheader.addSubview(lbl)
            }
            else if(section == 1)
            {
                let button = UIButton()
                button.frame = CGRect( x: 5, y: 0, width: (DeviceType.IS_IPAD ? 50 : 40), height: (DeviceType.IS_IPAD ? 50 : 40))
                if (isCreateNewLeads)
                {
                    button.setImage(UIImage(named: "check_box_2.png"), for: .normal)
                }
                else
                {
                    button.setImage(UIImage(named: "check_box_1.png"), for: .normal)
                    
                }
                button.addTarget(self, action: #selector(isCreateNewLead), for: .touchUpInside)
                
                viewheader.addSubview(button)
                
                let lbl = UILabel(frame: CGRect(x: (DeviceType.IS_IPAD ? 60 : 50), y: 0, width: tblViewMatchingAccounts.frame.width - 100, height: (DeviceType.IS_IPAD ? 50 : 40)))
                lbl.text = "Create New Opportunity"
                  lbl.font = UIFont.systemFont(ofSize: (DeviceType.IS_IPAD ? 21 : 18))
                viewheader.addSubview(lbl)
                
            }
            
            
            return viewheader
        }
        else if tableView == tblContactNew
        {
            return UIView()
        }
        else
        {
            
            switch detailType
            {
                 case .GeneralNew:
                   
                    let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                    viewheader.backgroundColor = UIColor.init(red: 246/255, green: 246/255, blue: 246/255, alpha: 1)
                    
                    let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                      lbl.font = UIFont.systemFont(ofSize: (DeviceType.IS_IPAD ? 21 : 18))
                    if(section == 0)
                    {
                        lbl.text = "Status"
                        lbl.textColor = UIColor.theme()
                        let button = UIButton()
                        button.frame = CGRect( x: tblviewLeadDetails.frame.size.width - 140, y: 0, width: 130, height: (DeviceType.IS_IPAD ? 50 : 40))
                        button.setTitle("\(dictLeadData.value(forKey: "StatusName") ?? "")", for: .normal)
                        //buttonRound(sender: button)
                        button.layer.cornerRadius = 20
                        //button.titleLabel?.textColor = UIColor.black
                        button.setTitleColor(UIColor.white, for: .normal)
                        
                        button.backgroundColor = hexStringToUIColor(hex: "D0B50E")
                        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 15))
                        

                        button.layer.borderColor = UIColor.darkGray.cgColor
                        button.layer.borderWidth = 1.0
                        
                        if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Converted") == .orderedSame
                        {
                            
                            
                        }else{
                            
                            button.addTarget(self, action: #selector(clickOnStatus), for: .touchUpInside)

                        }
                        //button.addTarget(self, action: #selector(clickOnStatus), for: .touchUpInside)
                        
                        let imgView = UIImageView()
                        imgView.frame = CGRect( x: button.frame.maxX - 28, y: 13.5, width: 25, height: 25)
                        imgView.image = UIImage(named: "drop_down_ipad")
                        
                        
                        viewheader.addSubview(button)
                        viewheader.addSubview(imgView)
                        
                    }
                    else if(section == 1)
                    {
                        lbl.text = "Company"
                        lbl.textColor = UIColor.theme()
                    }
                    else if(section == 2)
                    {
                        lbl.text = "Contact"
                        lbl.textColor = UIColor.theme()
                    }
                    else if(section == 3)
                    {
                        lbl.text = "Open Task"
                        lbl.textColor = UIColor.theme()
                    }
                    else if(section == 4)
                    {
                        lbl.text = "Notes"
                        lbl.textColor = UIColor.theme()
                    }
                    lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
                    viewheader.addSubview(lbl)
                    return viewheader
                
                
            case .Task:
                
                let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                viewheader.backgroundColor = UIColor.lightColorForCell()
                
                let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                
                if(section == 0)
                {
                    lbl.text = "Task List"
                }
                else if(section == 1)
                {
                    lbl.text = "Activity List"
                }
                  lbl.font = UIFont.systemFont(ofSize: (DeviceType.IS_IPAD ? 21 : 18))
                viewheader.addSubview(lbl)
                return viewheader
                
                
            case .Details:
                
                let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                viewheader.backgroundColor = UIColor.lightColorForCell()
                
                let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                
                if(section == 0)
                {
                    lbl.text = "Lead Info"
                }
                  lbl.font = UIFont.systemFont(ofSize: (DeviceType.IS_IPAD ? 21 : 18))
                viewheader.addSubview(lbl)
                return viewheader
                
            case .Timeline:
                
                if section == 0
                {
                    let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                     
                     viewHeader.backgroundColor = UIColor.headerFooter()
                     
                     let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                    
                     lbl.text = "Open Tasks"
                     lbl.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 18)
                     lbl.textColor = UIColor.theme()
                     viewHeader.addSubview(lbl)
                     
                     return viewHeader
                }
                else if section == 1
                {
                    /*let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
                    viewheader.backgroundColor = UIColor.lightColorForCell()*/
                    let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                    
                    viewHeader.backgroundColor = UIColor.headerFooter()
                    
                    let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                     lbl.font = UIFont.systemFont(ofSize: (DeviceType.IS_IPAD ? 21 : 18))
                    lbl.text = "History"
                    lbl.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 18)
                    lbl.textColor = UIColor.theme()
                    viewHeader.addSubview(lbl)
                    
                    let button = UIButton(type: .system)
                     
                    button.frame = CGRect( x: UIScreen.main.bounds.size.width - 55, y: 0, width: (DeviceType.IS_IPAD ? 50 : 40), height: (DeviceType.IS_IPAD ? 50 : 40))
                    
                    button.setImage(UIImage(named: "filter_crmTop"), for: .normal)
                    button.tintColor = UIColor.theme()
                    //button.tintColor = hexStringToUIColor(hex: "3E977A")
                    
                    button.addTarget(self, action: #selector(actionOnFilterTimeLineNew), for: .touchUpInside)
                    
                    viewHeader.addSubview(button)
                    
                    return viewHeader
                }
                else
                {
                    let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
                    
                    viewHeader.backgroundColor = UIColor.headerFooter()
                    let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 250, height: (DeviceType.IS_IPAD ? 50 : 40)))
                      lbl.font = UIFont.systemFont(ofSize: (DeviceType.IS_IPAD ? 21 : 18))
                    let key = aryAllKeys[section - 2]
                    
                    if((key as? Date) != nil)
                    {
                        let strDate = changeDateToString(date: key as! Date)
                        
                        lbl.text = strDate
                    }
                    viewHeader.addSubview(lbl)
                    
                    return viewHeader
                }
              
                
            default:
                print("nothing to do")
                break
                
            }
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == tblViewMatchingAccounts
        {
            if(section == 0)
            {
                return DeviceType.IS_IPAD ? 60 : 50
            }
            else
            {
                return 50 - 50
            }
        }
        else if tableView == tblContactNew
        {
            
            return 0
            
        }
        else
        {
            switch detailType
            {
            case .GeneralNew:
                if section == 0 // Status
                {
                    return DeviceType.IS_IPAD ? 60 : 50
                }
                else if section == 1  //Company
                {
                    if dictLeadData.value(forKey: "CrmCompany") is NSDictionary
                    {
                        return DeviceType.IS_IPAD ? 60 : 50
                    }
                    else
                    {
                        return 0
                    }
                }
                else if section == 2  //Contact
                {
                    if arrOfContacts.count > 0
                    {
                        return DeviceType.IS_IPAD ? 60 : 50
                    }
                    else
                    {
                        return 0
                    }
                }
                else if section == 3 // Task
                {
                    if aryTasksNew.count > 0
                    {
                        return DeviceType.IS_IPAD ? 60 : 50
                    }
                    else
                    {
                        return 0
                    }
                }
                else if section == 4 // Notes
                {
                    if arrAllNotes.count == 0
                    {
                        return 0
                    }
                    else
                    {
                        return 0//50
                    }
                }
                else
                {
                    return DeviceType.IS_IPAD ? 60 : 50
                }
                
            case .DocumentsNew:
                return 0
                
            case .DetailsNew:
                return 0
                
            case .Task:
                if(section == 0)
                {
                    return DeviceType.IS_IPAD ? 50 : 40
                }
                return DeviceType.IS_IPAD ? 50 : 40
                
            case .Notes:
                return 0
                
            case .Timeline:
                
                if(section == 0)
                {
                    if aryTasksNew.count == 0
                    {
                        return 0
                    }
                    else
                    {
                         return DeviceType.IS_IPAD ? 50 : 40
                    }
                }
                else
                {
                    return DeviceType.IS_IPAD ? 50 : 40
                    
                }
                
            default:
                  return DeviceType.IS_IPAD ? 50 : 40
            }
        }
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblViewMatchingAccounts
        {
            return UITableView.automaticDimension
        }
        else if tableView == tblContactNew
        {
            return DeviceType.IS_IPAD ? 40 : 30
        }
        else
        {
            switch detailType
            {
                
                case .GeneralNew:
                               
                    if indexPath.section == 1
                    {
                        return  DeviceType.IS_IPAD ? 75 :  60
                    }
                    else if indexPath.section == 2
                    {
                        return  DeviceType.IS_IPAD ? 85 :  70
                    }
                    else if indexPath.section == 3
                    {
                        let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                        if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                        {
                            return  DeviceType.IS_IPAD ? 93 : 78
                        }
                        else
                        {
                            return 0
                        }
                        //return 78
                    }
                    else
                    {
                        return DeviceType.IS_IPAD ? 93 : 78
                    }
                
                case .DocumentsNew:
                return DeviceType.IS_IPAD ? 65 : 50
                
            case .DetailsNew:
                
                if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Converted") == .orderedSame
                {
                    return UITableView.automaticDimension//350 - 100
                }
                else
                {
                    return UITableView.automaticDimension//350
                }
                
            case .Task:
                if(indexPath.section == 0)
                {
                    return DeviceType.IS_IPAD ? 93 :  78
                }
                return DeviceType.IS_IPAD ? 93 :  78
                
            case .Notes:
                return DeviceType.IS_IPAD ? 100 :  85
                
            case  .Timeline:
                
                if(indexPath.section == 0)
                {
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                    {
                        return DeviceType.IS_IPAD ? 93 :  78
                    }
                    else
                    {
                        return 0
                    }
                }
                else if(indexPath.section == 1)
                {
                    return 0
                }
                else
                {
                    let key = aryAllKeys[indexPath.section - 2]
                    var valueForKey = dictGroupedTimeline[key as! AnyHashable]
                    
                    let list = [Int](1...4)
                    let arrayLoc = NSMutableArray()
                    
                    for (index, element) in list.enumerated() {
                        if(index == 0){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                            
                        }
                        if(index == 1){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        
                        if(index == 2){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        if(index == 3){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                    }
                    
                    valueForKey = (arrayLoc as! [NSMutableDictionary])
                    let dictData = valueForKey![indexPath.row]
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Note"){
                        return UITableView.automaticDimension//350

                       // return DeviceType.IS_IPAD ? 75 :  60
                    }
                    if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                        return DeviceType.IS_IPAD ? 100 :  85
                    }
                    if("\(dictData.value(forKey: "EntityType")!)" == "Task"){
                        return DeviceType.IS_IPAD ? 85 :  70
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Email"){
                        
                        if let emailContent = dictData.value(forKey: "EmailNotes"){
                            
                            if("\(emailContent)" == "<null>")
                            {
                                return DeviceType.IS_IPAD ? 95 :  80
                            }
                            else
                            {
                                return DeviceType.IS_IPAD ? 200 :  180
                            }
                        }
                        else
                        {
                            return DeviceType.IS_IPAD ? 95 :  80
                        }
                    }
                    return 0
                }
                

                
            default:
                if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Converted") == .orderedSame
                {
                    return 350 - 100
                }
                else
                {
                    return 350
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        
        if tableView == tblViewMatchingAccounts
        {
            selectedMatchingAccounts = indexPath.row
            isCreateNewLeads = false
            
            tblViewMatchingAccounts.reloadData()
            var path = NSIndexPath()
            path = NSIndexPath(row: selectedMatchingAccounts, section: 0)
            tblViewMatchingAccounts.scrollToRow(at: path as IndexPath, at: UITableView.ScrollPosition.top, animated: false)
            
        }
        else if tableView == tblContactNew
        {
            let dictOfContacts = arrOfContacts.object(at: indexPath.row) as! NSDictionary
            goToContactDetails(dictContact: dictOfContacts)
            
        }
        else
        {
            if detailType == LeadDetailType.GeneralNew
            {
                if(indexPath.section == 0) //Status
                {
                   
                }
                else if(indexPath.section == 1) //Company
                {
                    goToCompanyEdit()
                }
                else if(indexPath.section == 2) //Contact
                {
                    let dictOfContacts = arrOfContacts.object(at: indexPath.row) as! NSDictionary
                    goToContactDetails(dictContact: dictOfContacts)
                }
                else if(indexPath.section == 3) // Open Tasks
                {
                   let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    let strTaskId = "\(dictData.value(forKey: "LeadTaskId") ?? "")"
                    vc.taskId = "\(strTaskId)"
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else
                {
                    
                }
                
            }
            if detailType == LeadDetailType.DocumentsNew
            {
                let dict = arrAllDocuments.object(at: indexPath.row) as! NSDictionary
                
                let cell = tblviewLeadDetails.cellForRow(at: indexPath) as! LeadVCCell

                if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Image") == .orderedSame
                {
                    let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
                    let vc = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as! PreviewImageVC
                    vc.img = cell.imgViewDocument.image!
                    self.present(vc, animated: false, completion: nil)
                    
                }
                else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Document") == .orderedSame
                {
                     // let strImagePath = Global().strDocName(fromPath: "\( dict.value(forKey: "Path") ?? "")")

                    goToPdfView(strPdfName: "\(dict.value(forKey: "Path") ?? "")")
                    
                }
                else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Audio") == .orderedSame
                {
                    
                    let audioURL = "\("\( dict.value(forKey: "Path") ?? "")")"
                    let player = AVPlayer(url: URL(string: audioURL)!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                    /* let strImagePath = Global().strDocName(fromPath: "\( dict.value(forKey: "Path") ?? "")")
                     
                     let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                     
                     if isImageExists!  {
                     
                     playAudioo(strAudioName: strImagePath!)
                     
                     }else {
                     
                     showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                     
                     }*/
                    
                }
                else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Video") == .orderedSame
                {
                    let audioURL = "\("\( dict.value(forKey: "Path") ?? "")")"
                    let player = AVPlayer(url: URL(string: audioURL)!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                   /* let strVideoPath = Global().strDocName(fromPath: "\( dict.value(forKey: "Path") ?? "")")
                    
                    
                    let isVideoExists: Bool? = checkIfImageExistAtPath(strFileName: strVideoPath!)
                    
                    if isVideoExists!  {
                        
                        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                        let documentsDirectory = paths[0]
                        let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(strVideoPath!)")
                        print(path)
                        let player = AVPlayer(url: path)
                        let playerViewController = AVPlayerViewController()
                        playerViewController.player = player
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                        
                    }
                    else
                    {

                        var vedioUrl = strURL + "\("\( dict.value(forKey: "Path") ?? "")")"

                        vedioUrl = vedioUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)

                        let player = AVPlayer(url: URL(string: vedioUrl)!)
                        let playerViewController = AVPlayerViewController()
                        playerViewController.player = player
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                        
                    }*/
                   
                }
                
            }
            if detailType == LeadDetailType.Task
            {
                if indexPath.section == 0
                {
                
                }
                else if indexPath.section == 1
                {
                }
            }
            if detailType == LeadDetailType.Timeline
            {
                
                if indexPath.section == 0
                {
                     let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
                    
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    let strTaskId = "\(dictData.value(forKey: "LeadTaskId") ?? "")"
                    vc.taskId = "\(strTaskId)"
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else if indexPath.section == 1
                {
                }
                else
                {
                    let key = aryAllKeys[indexPath.section - 2]
                    var valueForKey = dictGroupedTimeline[key as! AnyHashable]
                    
                    let list = [Int](1...4)
                    let arrayLoc = NSMutableArray()
                    
                    for (index, element) in list.enumerated() {
                        if(index == 0){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                            
                        }
                        if(index == 1){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        
                        if(index == 2){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        if(index == 3){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                    }
                    valueForKey = (arrayLoc as! [NSMutableDictionary])
                    
                    let dictData = valueForKey![indexPath.row]
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Note"){
                        
                        
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                        
                        self.goToActivityDetails(strId: "\(dictData.value(forKey: "ActivityId")!)")
                        
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Task"){
                        
                        self.goToTaskDetails(strId: "\(dictData.value(forKey: "LeadTaskId")!)")
                        
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Email"){
                        
                        self.goToEmailDetails(dictEmail: dictData)
                        
                    }
                }
                
                
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        if detailType == LeadDetailType.Task
        {
            let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: DeviceType.IS_IPAD ? 50 : 40))
            viewheader.backgroundColor = UIColor.lightColorForCell()
            
            if(section == 0)
            {
                
                let button = UIButton()
                button.frame = CGRect( x: tblviewLeadDetails.frame.maxX - 100, y: 0, width: 100, height: DeviceType.IS_IPAD ? 50 : 40)
                button.setTitle("View More", for: .normal)
                button.titleLabel?.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 16 : 14))
                button.addTarget(self, action: #selector(viewMoreTask), for: .touchUpInside)
                
                button.setTitleColor(UIColor.theme(), for: .normal)
                if aryTasks.count > 0
                {
                    viewheader.addSubview(button)
                }
            }
            else if(section == 1)
            {
                
                let button = UIButton()
                button.frame = CGRect( x: tblviewLeadDetails.frame.maxX - 100, y: 0, width: 100, height: DeviceType.IS_IPAD ? 50 : 40)
                button.setTitleColor(UIColor.theme(), for: .normal)
                button.titleLabel?.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 16 : 14))
                
                button.addTarget(self, action: #selector(viewMoreActivity), for: .touchUpInside)
                
                button.setTitle("View More", for: .normal)
                if aryActivities.count > 0
                {
                    viewheader.addSubview(button)
                }
                
            }
            return viewheader
        }
        else
        {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if detailType == LeadDetailType.Task
        {
            if section == 0
            {
                if aryTasks.count == 0
                {
                    return 1.0
                }
                else
                {
                    return DeviceType.IS_IPAD ? 50 : 40
                }
            }
            else
            {
                if aryActivities.count > 0
                {
                    return DeviceType.IS_IPAD ? 50 : 40
                }
                else
                {
                    return 1.0
                }
                
            }
        }
        else
        {
            return 1.0
        }
    }
    // MARK: -  ------------------ Table Button Action  ------------------
    
    @objc func actionOnMsgPrimaryPhone(sender:UIButton)
    {
        
    }
    @objc func actionOnCallPrimaryPhone(sender:UIButton)
    {
        
    }
    @objc func actionOnMsgSecondaryPhone(sender:UIButton)
    {
        
    }
    @objc func actionOnCallSecondaryPhone(sender:UIButton)
    {
    }
    
    @objc func actionOnMsgCell(sender:UIButton)
    {
        
    }
    @objc func actionOnCallCell(sender:UIButton)
    {
        
    }
    @objc func actionOnPrimaryEmail(sender:UIButton)
    {
        
    }
    @objc func actionOnSecondaryEmail(sender:UIButton)
    {
        print("Secondary Email")
    }
    
     // MARK:- ------- Contact Cell Action --------
    
    @objc func actionOnDeAssociateContact(sender: UIButton!)
    {
        
        if(isInternetAvailable() == false)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            
            let dictOfContacts = arrOfContacts.object(at: sender.tag) as! NSDictionary
           /* let ary = dictLeadData.value(forKey: "CrmCompanies") as! NSArray
            
            let dict = ary[sender.tag] as! NSDictionary*/
            
            let crmCompanyIdLocal = "\(dictOfContacts.value(forKey: "CrmCompanyId") ?? "")"
            let crmCContactIdLocal = "\(dictOfContacts.value(forKey: "CrmContactId") ?? "")"
            
            self.diAssociateContact(strCrmContactId: crmCContactIdLocal, strCrmCompanyId: crmCompanyIdLocal)
            
        }
        
    }
    
    @objc func actionOnDeAssociateCompany(sender: UIButton!)
    {
        
        if(isInternetAvailable() == false)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            
          
            let crmCompanyIdLocal = "\(dictLeadData.value(forKeyPath: "CrmCompany.CrmCompanyId") ?? "")"
            let crmCContactIdLocal = ""
            
            self.diAssociateCompany(strCrmContactId: crmCContactIdLocal, strCrmCompanyId: crmCompanyIdLocal)
            
        }
        
    }
    
     // MARK:- ------- Timeline Activity Cell Action --------
    
    @objc func actionOnActvityComment(sender: UIButton!)
    {
        
        if(isInternetAvailable() == false)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            //indexPath.row*1000+indexPath.section
            
            let row = sender.tag/1000
            let section = sender.tag%1000
            
            let key = aryAllKeys[section - 2]
            var valueForKey = dictGroupedTimeline[key as! AnyHashable]
            
            let list = [Int](1...4)
            let arrayLoc = NSMutableArray()
            
            for (index, element) in list.enumerated() {
                if(index == 0){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                    
                }
                if(index == 1){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                }
                
                if(index == 2){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                }
                if(index == 3){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                }
            }
            
            valueForKey = (arrayLoc as! [NSMutableDictionary])
            let dictData = valueForKey![row]
            if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "CommentList_iPhoneVC") as! CommentList_iPhoneVC
                            
                controller.iD_PreviousView = "\(dictData.value(forKey: "ActivityId")!)"
                self.navigationController?.pushViewController(controller, animated: false)
                
            }
            
        }
        
    }
    
    // MARK:- ------- Matchig Account Action --------
    
    @objc func action_PrimaryEmail_MatchingAccount(sender:UIButton)
    {
        let dictData = arrOfMatchingAccounts[sender.tag] as! NSDictionary
        
        let strEmail = "\(dictData.value(forKey: "PrimaryEmail") ?? "")"
        
        if strEmail.count > 0
        {
            self.sendEmail(strEmail: strEmail)
            
        }
        
    }
    @objc func action_PrimaryPhone_MatchingAccount(sender:UIButton)
    {
        let dictData = arrOfMatchingAccounts[sender.tag] as! NSDictionary
        
        let strNo = "\(dictData.value(forKey: "PrimaryPhone") ?? "")"
        
        if strNo.count>0
        {
            Global().calling(strNo)
            
        }
        
    }
    @objc func action_Address_MatchingAccount(sender:UIButton)
    {
        let dictData = arrOfMatchingAccounts[sender.tag] as! NSDictionary
        
        var strAddress =  String()
        strAddress = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
        if (strAddress.count>0)
        {
            
            Global().redirect(onAppleMap: self, strAddress)
            
        }
        
        
        
    }
    @objc func action_CheckBox_MatchingAccount(sender:UIButton)
    {
        isCreateNewLeads = false
        
        if selectedMatchingAccounts == sender.tag
        {
            selectedMatchingAccounts = sender.tag
            tblViewMatchingAccounts.reloadData()
            
            var path = NSIndexPath()
            path = NSIndexPath(row: selectedMatchingAccounts, section: 0)
            tblViewMatchingAccounts.scrollToRow(at: path as IndexPath, at: UITableView.ScrollPosition.top, animated: false)
        }
        else
        {
            selectedMatchingAccounts = sender.tag
            tblViewMatchingAccounts.reloadData()
            
            var path = NSIndexPath()
            path = NSIndexPath(row: selectedMatchingAccounts, section: 0)
            tblViewMatchingAccounts.scrollToRow(at: path as IndexPath, at: UITableView.ScrollPosition.top, animated: true)
        }
        
        
    }
    @objc func action_SecondaryPhone_MatchingAccount(sender:UIButton)
    {
        let dictData = arrOfMatchingAccounts[sender.tag] as! NSDictionary
        
        let strNo = "\(dictData.value(forKey: "SecondaryPhone") ?? "")"
        
        if strNo.count>0
        {
            Global().calling(strNo)
            
        }
        
    }
    @objc func action_Cell_MatchingAccount(sender:UIButton)
    {
        let dictData = arrOfMatchingAccounts[sender.tag] as! NSDictionary
        
        let strNo = "\(dictData.value(forKey: "CellPhone") ?? "")"
        
        if strNo.count>0
        {
            Global().calling(strNo)
            
        }
        
        
        
    }
    
    @objc func action_Message_PrimaryPhone_MatchingAccount(sender:UIButton)
       {
           let dictData = arrOfMatchingAccounts[sender.tag] as! NSDictionary
           
           let strNo = "\(dictData.value(forKey: "PrimaryPhone") ?? "")"
           
           if strNo.count>0
           {
               displayMessageInterface(strNo: strNo)
               
           }
           
       }
    @objc func action_Message_SecondaryPhone_MatchingAccount(sender:UIButton)
    {
        let dictData = arrOfMatchingAccounts[sender.tag] as! NSDictionary
        
        let strNo = "\(dictData.value(forKey: "SecondaryPhone") ?? "")"
        
        if strNo.count>0
        {
            displayMessageInterface(strNo: strNo)
            
        }
        
    }
    @objc func action_Message_CellPhone_MatchingAccount(sender:UIButton)
    {
        let dictData = arrOfMatchingAccounts[sender.tag] as! NSDictionary
        
        let strNo = "\(dictData.value(forKey: "CellPhone") ?? "")"
        
        if strNo.count>0
        {
            displayMessageInterface(strNo: strNo)
            
        }
        
    }
    
    
    
    
    @IBAction func actionOnBack_MatchingAccount(_ sender: Any)
    {
        view_MatchingAccounts.removeFromSuperview()
    }
    
    @IBAction func actionOnConverOpportunity_MatchingAccounts(_ sender: Any)
    {
        //view_MatchingAccounts.removeFromSuperview()
        
        if (isCreateNewLeads)
        {
            
            //let dictMutable = getMutableDictionaryFromNSManagedObject(obj: self.dictLeadData) as NSDictionary
             let dictMutable = dictLeadData as NSDictionary
            
            self.gotoConvertLead(strTypeToConvert: "New", dictLeadDetail: dictMutable)
            
        } else {
            
            if (selectedMatchingAccounts>=0) {
                
               // let dictMutable = getMutableDictionaryFromNSManagedObject(obj: self.dictLeadData) as NSDictionary
                 let dictMutable = dictLeadData as NSDictionary
                self.dictAccountDetail = (self.arrOfMatchingAccounts.object(at: selectedMatchingAccounts)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                self.gotoConvertLead(strTypeToConvert: "Existing", dictLeadDetail: dictMutable)
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select to convert", viewcontrol: self)
                
            }
            
        }
        
    }
    
    @IBAction func actionOnCancel_MatchingAccounts(_ sender: Any)
    {
        view_MatchingAccounts.removeFromSuperview()
        
    }
    
    
    
    // MARK: -  ------------------------------ Action  ------------------------------
    
    @IBAction func actionOnUserName(_ sender: Any)
    {
        if "\(dictLeadData.value(forKey: "CrmContactId")!)" == "" || "\(dictLeadData.value(forKey: "CrmContactId")!)".count == 0
        {

                  
                  
        }
        else
        {
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as! ContactDetailsVC_CRMContactNew_iPhone
               controller.strCRMContactIdGlobal = "\(dictLeadData.value(forKey: "CrmContactId")!)"
            controller.dictContactDetailFromList = dictLeadData
            self.navigationController?.pushViewController(controller, animated: false)
        }
         
    }
    
    @IBAction func actionOnBack(_ sender: UIButton)
    {
        nsud.set(true, forKey: "fromWebLeadDetail")
        nsud.set(true, forKey: "fromWebLeadDetailNew")
        nsud.synchronize()
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionOnAddress(_ sender: Any)
    {
        var strAddress = ""
        
        if "\(dictLeadData.value(forKey: "Address1") ?? "")".count > 0
        {
            strAddress = "\(dictLeadData.value(forKey: "Address1") ?? ""), "
        }
        if "\(dictLeadData.value(forKey: "Address2") ?? "")".count > 0
        {
            strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "Address2") ?? "")"), "
        }
        if "\(dictLeadData.value(forKey: "CityName") ?? "")".count > 0
        {
            strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "CityName") ?? "")"), "
        }
        if "\(dictLeadData.value(forKey: "StateId") ?? "")".count > 0
        {
            
            let stateName = Global().strStatName(fromID: "\((dictLeadData.value(forKey: "StateId") ?? ""))")
            strAddress = "\(strAddress)\(stateName ?? ""), "
            
        }
        if "\(dictLeadData.value(forKey: "Zipcode") ?? "")".count > 0
        {
            strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "Zipcode") ?? "")")"
        }
        
        strAddress = Global().strCombinedAddress(dictLeadData as? [AnyHashable : Any])
        
        if strAddress.count > 0
        {
            
            let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
            // Create the actions
            
            let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                //[global redirectOnAppleMap:self :_txtViewAddress_ServiceAddress.text]
                Global().redirect(onAppleMap: self, strAddress)
                
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnEditCompany(_ sender: Any)
    {
        
       /* let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "CompanyDetailViewControlleriPhone") as! CompanyDetailViewControlleriPad
        var dict = NSDictionary()
        dict = createCompanyDetailData()
        
        vc.dictContactDetail = dict as NSDictionary as? [AnyHashable : Any]
        
        vc.strFromWhere = "fromOpportunityDetailVC"
        self.present(vc, animated: false, completion: nil)*/
        
       
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
               controller.strCRMCompanyIdGlobal = "\(dictLeadData.value(forKeyPath: "CrmCompany.CrmCompanyId")!)"
               self.navigationController?.pushViewController(controller, animated: false)
    }
    
    @IBAction func actionOnEdit(_ sender: Any)
    {
        goToEditWebLeadVC()
    }
    
    @objc func actionOnTaskCheckMark(sender: UIButton!)
    {
        /*if(aryIndexPath.contains(sender.tag))
         {
         aryIndexPath.remove(sender.tag)
         }
         else
         {
         aryIndexPath.add(sender.tag)
         }
         
         tblviewLeadDetails.reloadData()*/
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            //let dict = (aryTasks.object(at: sender.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            let dict = (aryTasksNew.object(at: sender.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary

            
            //callAPIToUpdateTaskMarkAsDone(dictData: dict)
            let tagIndex = sender.tag as Int
            callAPIToUpdateTaskMarkAsDone(dictData: dict, tag: tagIndex)
        }
        
    }
    @objc func actionOnConvertToOpportunity(sender:UIButton)
    {
        if dictLeadData.value(forKey: "BranchesSysNames") is NSArray
        {
            let arrOfBranchesNames = dictLeadData.value(forKey: "BranchesSysNames") as! NSArray
            
            if arrOfBranchesNames.count > 1
            {
                let alertController = UIAlertController(title: "Alert", message: "Multiple Branches exist. Please edit and select single branch", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Yes-Edit", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                    
                    self.goToEditWebLeadVC()
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                strLeadID =  strRefIdNew//"\(dictLeadData.value(forKey: "LeadId") ?? "")"
                
                
                self.fetchMatchingAccounts(strWebLeadNumber: "\(dictLeadData.value(forKey: "LeadNumber") ?? "")")
                
                nsud.set("\(dictLeadData.value(forKey: "LeadNumber") ?? "")", forKey: "LeadNumber")
                nsud.synchronize()
                
                
            }
        }
        else
        {
            strLeadID =  strRefIdNew//"\(dictLeadData.value(forKey: "LeadId") ?? "")"
            
            self.fetchMatchingAccounts(strWebLeadNumber: "\(dictLeadData.value(forKey: "LeadNumber") ?? "")")
            
            nsud.set("\(dictLeadData.value(forKey: "leadNumber") ?? "")", forKey: "LeadNumber")
            nsud.synchronize()
        }
    }
    @objc func actionOnCreateFollowUp(sender:UIButton)
    {
        goToCreateFollowUpVC()
    }
    @objc func actionOnRelease(sender:UIButton)
    {
        goToReleaseLeadVC()
    }
    @objc func actionOnGrab(sender:UIButton)
    {
        grabLead()
    }
    @objc func actionOnMarkAsDead(sender:UIButton)
    {
        deadLead()
    }
    
    @objc func viewMoreTask(sender:UIButton)
    {
         let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone

        vc.strFromVC = "WebLeadVC"
        vc.strleadId = strRefId
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    @objc func viewMoreActivity(sender:UIButton)
    {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone

        vc.strleadId = strRefId
        vc.strFromVC = "WebLeadVC"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    // UIButton action
    
    @IBAction func actionOnTask(_ sender: UIButton)
    {
        if detailType == LeadDetailType.Task
        {
            
        }
        else
        {
            
            btnTask.backgroundColor = hexStringToUIColor(hex: "D0B50E")
            btnTask.setTitleColor(UIColor.white, for: .normal)
            btnNotes.backgroundColor = UIColor.white
            btnNotes.setTitleColor(UIColor.black, for: .normal)
            //btnDetails.backgroundColor = UIColor.white
            //btnDetails.setTitleColor(UIColor.black, for: .normal)
            
            detailType = LeadDetailType.Task
            
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()
            tblviewLeadDetails.reloadData()
        }
    }
    
    @IBAction func actionOnNotes(_ sender: UIButton)
    {
        if detailType == LeadDetailType.Notes
        {
            
        }
        else
        {
            
            // getAllNotes()
            
            
            btnNotes.backgroundColor = hexStringToUIColor(hex: "D0B50E")
            btnNotes.setTitleColor(UIColor.white, for: .normal)
            btnTask.backgroundColor = UIColor.white
            btnTask.setTitleColor(UIColor.black, for: .normal)
            //btnDetails.backgroundColor = UIColor.white
            //btnDetails.setTitleColor(UIColor.black, for: .normal)
            
            detailType = LeadDetailType.Notes
            fetchAllNotesFromCoreData()
            //tblviewLeadDetails.reloadData()
        }
    }
    
    @IBAction func actionOnDetails(_ sender: UIButton) {
        
        //btnDetails.backgroundColor = hexStringToUIColor(hex: "D0B50E")
        //btnDetails.setTitleColor(UIColor.white, for: .normal)
        btnTask.backgroundColor = UIColor.white
        btnTask.setTitleColor(UIColor.black, for: .normal)
        btnNotes.backgroundColor = UIColor.white
        btnNotes.setTitleColor(UIColor.black, for: .normal)
        
        detailType = LeadDetailType.Details
        tblviewLeadDetails.reloadData()
    }
    
    
    
    @IBAction func actionOnAdd(_ sender: UIButton)
    {
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black

             let Task = UIAlertAction(title: "Add Task", style: .default , handler:{ (UIAlertAction)in
                 self.goToAddTaskVC()
             })
             Task.setValue(#imageLiteral(resourceName: "Task"), forKey: "image")
             Task.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
             alert.addAction(Task)
        
        let Activity = UIAlertAction(title: "Add Activity", style: .default , handler:{ (UIAlertAction)in
            self.goToAddActivityVC()
        })
        Activity.setValue(#imageLiteral(resourceName: "Add Activity"), forKey: "image")
        Activity.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Activity)
        
     
        let Notes = UIAlertAction(title: "Add Notes", style: .default , handler:{ (UIAlertAction)in
             self.goToAddNote()
         })
         Notes.setValue(#imageLiteral(resourceName: "Add Notes"), forKey: "image")
         Notes.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
         alert.addAction(Notes)
        
       /* alert.addAction(UIAlertAction(title: "Add Lead", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAddLead()
            
        }))*/
        let Contact = UIAlertAction(title: "Associate Contact", style: .default , handler:{ (UIAlertAction)in
                     self.gotoAssociateContact()
                })
                Contact.setValue(#imageLiteral(resourceName: "Icon ionic-md-person"), forKey: "image")
                Contact.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                alert.addAction(Contact)
   
        
        let Company = UIAlertAction(title: "Associate Company", style: .default , handler:{ (UIAlertAction)in
            self.gotoAssociateCompany()
            
        })
        Company.setValue(#imageLiteral(resourceName: "Aso Company"), forKey: "image")
        Company.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Company)
        
        
        let Document = UIAlertAction(title: "Add Document", style: .default , handler:{ (UIAlertAction)in
            self.gotoAddDocument()
        })
        Document.setValue(#imageLiteral(resourceName: "Add Document"), forKey: "image")
        Document.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Document)
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        if let popoverController = alert.popoverPresentationController {
                        popoverController.sourceView = sender as UIView
                        popoverController.sourceRect = sender.bounds
                        popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
                    }
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    @IBAction func actionOnCall(_ sender: UIButton)
    {
        makeCall(sender: sender)
    }
    
    @IBAction func actionOnEmail(_ sender: UIButton)
    {
        sendMail(sender: sender)
    }
    
    @IBAction func actionOnTextMessage(_ sender: UIButton)
    {
        sendMessage(sender: sender)
    }
    
  
    
    @IBAction func actionOnTaskActivity(_ sender: UIButton)
    {
        clearNsud()
         let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone

         testController.strFromVC = "DashBoardView"//"WebLeadVC"
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    @IBAction func actionOnContactList(_ sender: Any)
    {
        clearNsud()
        goToContactList()
    }
    
    @IBAction func actionOnMore(_ sender: UIButton)
    {
        clearNsud()
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
       
        
       let Near = (UIAlertAction(title: "Near By", style: .default , handler:{ (UIAlertAction)in
            self.goToNearBy()
        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
        Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Near)
        let Signed = (UIAlertAction(title: "Signed View Agreements", style: .default , handler:{ (UIAlertAction)in
            
            self.goToSignedViewAgreement()
            
        }))
        
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
        Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Signed)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
      if let popoverController = alert.popoverPresentationController {
                   popoverController.sourceView = sender as UIView
                   popoverController.sourceRect = sender.bounds
                   popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
               }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    
    @IBAction func actionOnDashboard(_ sender: Any)
    {
        clearNsud()
        goToDasboard()
    }
    
    @IBAction func actionOnAppointment(_ sender: Any)
    {
        clearNsud()
        goToAppointment()
    }
    
    @IBAction func actionOnGeneralNew(_ sender: Any)
    {
       // btnFilterTimeLine.isHidden = true
        const_btnFilterTimeLine_W.constant = 0 + (DeviceType.IS_IPAD ? 50 : 40)
        //const_LblBar_L.constant = 5
        //btnGeneralNew.titleLabel?.textColor = UIColor.gray
        //btnDocumentsNew.titleLabel?.textColor = UIColor.lightGray
        //btnDetailsNew.titleLabel?.textColor = UIColor.lightGray
        
       
        
        
        
        detailType = LeadDetailType.GeneralNew
        tblviewLeadDetails.reloadData()
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations:
            {
                self.const_LblBar_L.constant = 5
                self.view.layoutIfNeeded()
        }, completion: { finished in
            print("Basket doors opened!")
          })
        

    }
    
    @IBAction func actionOnDocumentNew(_ sender: Any)
    {
      //  btnFilterTimeLine.isHidden = true
        const_btnFilterTimeLine_W.constant = 0 + (DeviceType.IS_IPAD ? 50 : 40)
        //const_LblBar_L.constant = btnDocumentsNew.frame.origin.x
        //btnGeneralNew.titleLabel?.textColor = UIColor.lightGray
        
//        btnDocumentsNew.titleLabel?.textColor = UIColor.darkGray
//        btnDetailsNew.titleLabel?.textColor = UIColor.lightGray
//        btnTimeline.titleLabel?.textColor = UIColor.lightGray
        
        btnDocumentsNew.setTitleColor(UIColor.darkGray, for: .normal)
        btnTimeline.setTitleColor(UIColor.lightGray, for: .normal)
        btnDetailsNew.setTitleColor(UIColor.lightGray, for: .normal)

        

        detailType = LeadDetailType.DocumentsNew
        tblviewLeadDetails.reloadData()

        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations:
                   {
                    self.const_LblBar_L.constant = self.btnDocumentsNew.frame.origin.x
                   self.view.layoutIfNeeded()
                 }, completion: { finished in
                   print("Basket doors opened!")
                 })
        
    }
    
    @IBAction func actionOnDetailNew(_ sender: Any)
    {
       // btnFilterTimeLine.isHidden = true
        const_btnFilterTimeLine_W.constant = 0 + (DeviceType.IS_IPAD ? 50 : 40)
        //const_LblBar_L.constant = btnDetailsNew.frame.origin.x
        //btnGeneralNew.titleLabel?.textColor = UIColor.lightGray
//        btnDocumentsNew.titleLabel?.textColor = UIColor.lightGray
//        btnTimeline.titleLabel?.textColor = UIColor.lightGray
//        btnDetailsNew.titleLabel?.textColor = UIColor.darkGray
        
        btnDocumentsNew.setTitleColor(UIColor.lightGray, for: .normal)
        btnTimeline.setTitleColor(UIColor.lightGray, for: .normal)
        btnDetailsNew.setTitleColor(UIColor.darkGray, for: .normal)
        
        detailType = LeadDetailType.DetailsNew
        tblviewLeadDetails.reloadData()
        //0.5 0.1
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations:
                          {
                           self.const_LblBar_L.constant = self.btnDetailsNew.frame.origin.x
                          self.view.layoutIfNeeded()
                        }, completion: { finished in
                          print("Basket doors opened!")
                        })

        
    }

    @IBAction func actionOnTimeLine(_ sender: Any)
    {
        // btnFilterTimeLine.isHidden = false
        const_btnFilterTimeLine_W.constant = (DeviceType.IS_IPAD ? 50 : 40)
        //const_LblBar_L.constant = btnDetailsNew.frame.origin.x
        //btnGeneralNew.titleLabel?.textColor = UIColor.lightGray
//        btnDocumentsNew.titleLabel?.textColor = UIColor.lightGray
//        btnTimeline.titleLabel?.textColor = UIColor.gray
//        btnDetailsNew.titleLabel?.textColor = UIColor.lightGray
        
        btnDocumentsNew.setTitleColor(UIColor.lightGray, for: .normal)
        btnTimeline.setTitleColor(UIColor.darkGray, for: .normal)
        btnDetailsNew.setTitleColor(UIColor.lightGray, for: .normal)
        
        detailType = LeadDetailType.Timeline
        tblviewLeadDetails.reloadData()
        //0.5 0.1
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations:
                          {
                           self.const_LblBar_L.constant = self.btnTimeline.frame.origin.x
                          self.view.layoutIfNeeded()
                        }, completion: { finished in
                          print("Basket doors opened!")
                        })
        
       /* if(aryAllKeys.count == 0){
            
            //FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                
               self.callAPToGetTimeLine()
                
            }
        }*/
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
           self.callAPToGetTimeLine()
            
        }

        
    }

    @IBAction func actionOnFilterTimeLine(_ sender: UIButton) {
          
          self.view.endEditing(true)
          
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterTimeLineVC") as! FilterTimeLineVC
          controller.delegate = self
          self.navigationController?.present(controller, animated: false, completion: nil)
          
      }
    
    @objc func actionOnFilterTimeLineNew()
    {
        self.view.endEditing(true)
                 
               let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterTimeLineVC") as! FilterTimeLineVC
                 controller.delegate = self
                 self.navigationController?.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func actionOnCreateOpportunity_MatchinAccount(_ sender: Any)
    {
    }
    
    @IBAction func actionOnLeadStatus(_ sender: UIButton)
    {
        if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Converted") == .orderedSame
        {
            
            
        }
        else
        {
            
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
   alert.view.tintColor = UIColor.black
            if "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("New") == .orderedSame
            {
                        
                alert.addAction(UIAlertAction(title: "Convert To Opportunity", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnConvertToOpportunity(sender: UIButton())
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Mark as Dead", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnMarkAsDead(sender: UIButton())
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Grab", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnGrab(sender: UIButton())
                    
                }))
                
            }
            else if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Grabbed") == .orderedSame
            {
                        
                alert.addAction(UIAlertAction(title: "Convert To Opportunity", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnConvertToOpportunity(sender: UIButton())
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Mark as Dead", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnMarkAsDead(sender: UIButton())
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Release", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnRelease(sender: UIButton())
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Create FollowUp", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnCreateFollowUp(sender: UIButton())
                    
                }))
                
            }
            else if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("FollowUp") == .orderedSame
            {
                alert.addAction(UIAlertAction(title: "Convert To Opportunity", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnConvertToOpportunity(sender: UIButton())
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Mark as Dead", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnMarkAsDead(sender: UIButton())
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Release", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnRelease(sender: UIButton())
                    
                }))
            }
            else if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Dead") == .orderedSame
            {
                alert.addAction(UIAlertAction(title: "Convert To Opportunity", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnConvertToOpportunity(sender: UIButton())
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Grab", style: .default , handler:{ (UIAlertAction)in
                    
                    self.actionOnGrab(sender: UIButton())
                    
                }))
               
            }
            else if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Converted") == .orderedSame
            {
                
                
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            if let popoverController = alert.popoverPresentationController {
                             popoverController.sourceView = sender as UIView
                             popoverController.sourceRect = sender.bounds
                             popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
                         }
            
            self.present(alert, animated: true, completion: {
                
                
            })
            
        }
    }
    //Nilind
    
    // MARK: -  ------------------------------ API Calling  ------------------------------
    func getBasicInfoWebLead()
    {
        if !isInternetAvailable()
        {
            
        }
        else
        {
            //https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/GetWebLeadById?WebLeadId=167710

            strRefId = strRefIdNew
            
            //140176&reftype=Lead
            var strUrl = "\(strServiceUrlMain)/api/LeadNowAppToSalesProcess/GetWebLeadById?WebLeadId=\(strRefId)"  as String
            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            self.dispatchGroup.enter()
            
            //FTIndicator.showProgress(withMessage: "Fetching Notes...", userInteractionEnable: false)
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strUrl, responseStringComing: "AddTaskVC_CRMNew_iPhone") { (Response, Status) in
                
                self.dispatchGroup.leave()
                
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    nsud.setValue("", forKey: "fromAddNotes")
                    nsud.synchronize()
                    
                }
                if(Status)
                {
                    
                    let statusNew = (Response as NSDictionary).allKeys.contains(where: { (k) -> Bool in
                                                                  "Message" == (k as AnyObject)as! String
                                                              })
                                           print(statusNew)
                                           

                    if statusNew
                    {
                       
                          // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    }
                    else
                    {
                        let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (Response as! [AnyHashable : Any])) as NSDictionary
                                                                 print(dictNew)
                        self.dictLeadData = dictNew
                        
                        self.setInitialValues()
                        self.tblviewLeadDetails.reloadData()
                    }
                    
                    
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                }
                else
                {
                    
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                }
                
            }
        
        }
        
    }
    
    
    func getAllNotes()
    {
        self.arrAllNotes = NSMutableArray()
        
        if !isInternetAvailable()
        {
            fetchAllNotesFromCoreData()
        }
        else
        {
            //https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/GetLeadNotesListV2?refid=140176&reftype=Lead
            strReftype = "WebLead"
            strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "LeadId") ?? "")"
            
            //140176&reftype=Lead
            var strUrl = "\(strServiceUrlMain)\(UrlGetAllNotes)\(strRefId)&reftype=\(strReftype)"  as String
            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            //FTIndicator.showProgress(withMessage: "Fetching Notes...", userInteractionEnable: false)
            self.dispatchGroup.enter()
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
                self.dispatchGroup.leave()
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    nsud.setValue("", forKey: "fromAddNotes")
                    nsud.synchronize()
                    
                }
                if(Status)
                {
                    let arrData = Response["data"] as! NSArray
                    
                    if arrData.isKind(of: NSArray.self)
                    {
                        let arrAllData = NSMutableArray()
                        
                        for item in arrData
                        {
                            let  dict = item as! NSDictionary
                            
                            let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))
                            
                            arrAllData.add(dictNew as Any)
                            
                            
                        }
                        //self.arrAllNotes = arrAllData
                        
                        /*let str = getJson(from: arrAllData)
                        print("\(str ?? "")" as Any)
                        
                        self.saveNotesToCoreData(arrAllData: arrAllData)
                        self.fetchAllNotesFromCoreData()*/
                        
                        self.arrAllNotes = arrAllData.mutableCopy() as! NSMutableArray
                        self.tblviewLeadDetails.reloadData()
                        
                        
                    }
                    else
                    {
                        
                        // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                    
                }
                else
                {
                    
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    func getAllTask()
    {
        if(isInternetAvailable() == false)
        {
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
            }
            //tblviewLeadDetails.reloadData()
           
           // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            
            strReftype = "WebLead" //"WebLead"//
            strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "leadId") ?? "")"
            
            //https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3


            let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3"
            
            var dict = [String:Any]()
            
            dict = ["RefId":strRefId,
                    "RefType":enumRefTypeWebLead,
                    "IsDefault":"false",
                    "SkipDefaultDate":"false",
                    "Status":"",
                    "FromDate":"",
                    "ToDate":"",
                    "EmployeeId":"",
                    "TakeRecords":"",
                    "TaskTypeId":"",
                    "PriorityIds":""
            ]
            self.dispatchGroup.enter()
            WebService.postRequestWithHeaders(dictJson: dict as NSDictionary, url: strURL, responseStringComing: "TimeLine") { (response, status) in
                
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
                self.dispatchGroup.leave()
                if(status == true){
                    
                    var dictTemp = NSDictionary()
                    dictTemp = response.value(forKey: "data") as! NSDictionary
                    
                   
                    
                    let aryTemp = NSMutableArray()
                    
                    if dictTemp.value(forKey: "Tasks") is NSArray
                    {
                        if((dictTemp.value(forKey: "Tasks") as! NSArray).count > 0)
                        {
                            for item in dictTemp.value(forKey: "Tasks") as! NSArray
                            {
                                let dictData = removeNullFromDict(dict: (item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                                aryTemp.add(dictData)
                            }
                        }
                    }
                    
                    print("Task >>>  \(aryTemp)")
                    self.aryTasksNew = NSMutableArray()
                    self.aryTasksNew = aryTemp.mutableCopy() as! NSMutableArray
                    self.tblviewLeadDetails.reloadData()
                }
                else
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                    }
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }
        }
        
        
      
      /*  strReftype = "WebLead" //"WebLead"//
        strRefId = "\(dictLeadData.value(forKey: "leadId") ?? "")"
        
        //https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3


        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV2_SubObjs?refid=\(strRefId)&reftype=\(strReftype)"
        
        DispatchQueue.main.async {
            
           // FTIndicator.showProgress(withMessage: "Fetching Tasks...", userInteractionEnable: false)
            
        }
        
        //FTIndicator.showProgress(withMessage: "Fetching Tasks...", userInteractionEnable: false)
        
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            //FTIndicator.dismissProgress()
            
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
            }
            
            
            if(status == true)
            {
                if((response.value(forKey: "data") as! NSArray).count > 0)
                {
                    self.aryTasks.removeAllObjects()
                    
                    self.saveTaskListToLocalDb(data: response.value(forKey: "data") as! NSArray)
                    
                }
                else
                {
                    
                    DispatchQueue.main.async{
                        
                        // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
                        FTIndicator.dismissProgress()
                    }
                    
                }
                DispatchQueue.main.async{
                    
                    
                    FTIndicator.dismissProgress()
                }
                
            }
            else
            {
                
                DispatchQueue.main.async{
                    
                    //  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    FTIndicator.dismissProgress()
                }
                
            }
        }*/
    }
    func getAllTaskOld()
    {
        strReftype = "WebLead" //"WebLead"//
        strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "leadId") ?? "")"
        
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV2_SubObjs?refid=\(strRefId)&reftype=\(strReftype)"
        
        DispatchQueue.main.async {
            
           // FTIndicator.showProgress(withMessage: "Fetching Tasks...", userInteractionEnable: false)
            
        }
        
        //FTIndicator.showProgress(withMessage: "Fetching Tasks...", userInteractionEnable: false)
        
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            //FTIndicator.dismissProgress()
            
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
            }
            
            
            if(status == true)
            {
                if((response.value(forKey: "data") as! NSArray).count > 0)
                {
                    self.aryTasks.removeAllObjects()
                    
                    self.saveTaskListToLocalDb(data: response.value(forKey: "data") as! NSArray)
                    
                }
                else
                {
                    
                    DispatchQueue.main.async{
                        
                        // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
                        FTIndicator.dismissProgress()
                    }
                    
                }
                DispatchQueue.main.async{
                    
                    
                    FTIndicator.dismissProgress()
                }
                
            }
            else
            {
                
                DispatchQueue.main.async{
                    
                    //  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    FTIndicator.dismissProgress()
                }
                
            }
        }
    }
    func callAPIToUpdateTaskMarkAsDone(dictData:NSMutableDictionary, tag:Int)
    {
        var strStatus = ""
        
        if("\(dictData.value(forKey: "Status")!)" == "Open")
        {
            strStatus = "Done"
        }
        else
        {
            strStatus = "Open"
        }
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/ChangeTaskStatus?TaskId=\(dictData.value(forKey: "LeadTaskId")!)&Status=\(strStatus)"
        
        //FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { (response, status) in
            
            self.loader.dismiss(animated: false)
            {
                FTIndicator.dismissProgress()
                
                DispatchQueue.main.async {
                    
                    FTIndicator.dismissProgress()

                }
                
                if(status == true)
                {
                    if("\(response.value(forKey: "data")!)" == "true")
                    {
                        //self.updateTaskToLocalDB(dict: dictData)
                        var dictMutable = NSMutableDictionary()
                        dictMutable = dictData.mutableCopy() as! NSMutableDictionary
                        dictMutable.setValue(strStatus, forKey: "Status")
                        
                        self.aryTasksNew.replaceObject(at: tag, with: dictMutable)
                        self.tblviewLeadDetails.reloadData()
                        //self.getAllTask()
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    if "\((dictData.value(forKey: "Status")!))" == "Open"
                    {
                        
                        nsud.set(true, forKey: "fromWebLeadDetailTableAction")
                        nsud.synchronize()
                        nsud.setValue(true, forKey: "isTaskAddedUpdated")
                        nsud.synchronize()
                        
                        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "LogAsActivityVC") as! LogAsActivityVC
                                 
                        controller.dictLeadTaskData = NSDictionary()
                        controller.leadTaskId = "\((dictData.value(forKey: "LeadTaskId")!))"
                        controller.dictOfAssociations = NSMutableDictionary()
                        self.navigationController?.pushViewController(controller, animated: false)
                        
                        
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    // something went wrong
                }
            }

        }
    }
    func callApiForFollowUp(dictData: NSMutableDictionary)
    {
        if !isInternetAvailable()
        {
            //showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
           // FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            
            var strUrl =  "\(strServiceUrlMain)\(UrlAddUpdateTaskGlobal)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            var key = NSArray()
            var value = NSArray()
            
            key = ["Id",
                   "LeadTaskId",
                   "RefId",
                   "RefType",
                   "TaskName",
                   "DueDate",
                   "ReminderDate",
                   "Description",
                   "AssignedTo",
                   "Priority",
                   "Status",
                   "CreatedBy",
                   "Tags",
                   "ChildActivities",
                   "FollowUpFromTaskId"]
            
            var arrChildActivity = NSMutableArray()
            if (dictData.value(forKey: "childActivities") ?? "") is NSArray
            {
                let arr = (dictData.value(forKey: "childActivities") ?? "") as! NSArray
                if arr.count > 0
                {
                    arrChildActivity = arr.mutableCopy() as! NSMutableArray
                }
            }
            
            value = ["1",
                     "",
                     "\(dictData.value(forKey: "refId") ?? "")",//RefId
                "\(dictData.value(forKey: "refType") ?? "")",//RefType
                "\(dictData.value(forKey: "taskName") ?? "")",
                "\(dictData.value(forKey: "dueDate") ?? "")",
                "\(dictData.value(forKey: "reminderDate") ?? "")",
                "\(dictData.value(forKey: "taskDescription") ?? "")",
                "\(dictData.value(forKey: "assignedTo") ?? "")",
                "\(dictData.value(forKey: "priority") ?? "")",
                "\(dictData.value(forKey: "status") ?? "")",
                "\(dictData.value(forKey: "createdBy") ?? "")",
                "\(dictData.value(forKey: "tags") ?? "")",
                arrChildActivity,
                (dictData.value(forKey: "leadTaskId") ?? "")]
            
            let Url = String(format: strUrl)
            
            guard
                let serviceUrl = URL(string: Url) else { return }
            
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            let parameterDictionary = dict_ToSend
            
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            if (nsud.value(forKey: "LoginDetails") != nil)
            {
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
                
            }
            
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                
                self.loader.dismiss(animated: false)
                {
                    if let response = response {
                        print(response)
                    }
                    if let data = data
                    {
                        do
                        {
                            let json = try JSONSerialization.jsonObject(with: data, options: [])
                            
                            let dict = NSMutableDictionary()
                            
                            dict.setValue(json, forKey: "response")
                            print(dict)
                            
                            print(Global().nestedDictionaryByReplacingNulls(withNil: dict as? [AnyHashable : Any]))
                            
                            let dictResponse = Global().nestedDictionaryByReplacingNulls(withNil:dict as NSDictionary as? [AnyHashable : Any])
                            
                            print(dictResponse ?? 0)
                            
                            DispatchQueue.main.async
                                {
                                    FTIndicator.dismissProgress()
                                    
                                    let alertController = UIAlertController(title: "Alert", message: "Follow up created successfully", preferredStyle: .alert)
                                    // Create the actions
                                    
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default)
                                    {
                                        UIAlertAction in
                                        
                                        self.getAllTask()
                                        
                                        FTIndicator.dismissProgress()
                                        
                                    }
                                    
                                    alertController.addAction(okAction)
                                    
                                    // Present the controller
                                    self.present(alertController, animated: true, completion: nil)
                            }
                        }
                        catch
                        {}
                    }
                }

                }.resume()
            
        }
        
    }
    func grabLead()
    {
        nsud.set(true, forKey: "fromWebLeadDetailTableAction")
        nsud.synchronize()
        if !isInternetAvailable()
        {
            
            
            
        }
        else
        {
            
            //var strUrl = "\(strServiceUrlMain)\(URLGrabWebLead)\("\(dictLeadData.value(forKey: "LeadId") ?? "")")&EmployeeId=\(strEmpID)"  as String
            var strUrl = "\(strServiceUrlMain)\(URLGrabWebLead)\(strRefIdNew)&EmployeeId=\(strEmpID)"  as String
            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
           // FTIndicator.showProgress(withMessage: "Updating Lead...", userInteractionEnable: false)
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                       self.present(loader, animated: false, completion: nil)
            
            //Description
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "GrabLead") { (Response, Status) in
                
                
                self.loader.dismiss(animated: false)
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                    if(Status)
                    {
                        let strResponse = Response["data"] as! String
                        
                        if (strResponse.count > 0) && strResponse == "true"
                        {
                           
                            var dictMutable = NSMutableDictionary()//self.dictLeadData as! NSMutableDictionary
                            dictMutable = self.dictLeadData.mutableCopy() as! NSMutableDictionary
                            dictMutable.setValue("Grabbed", forKey: "StatusName")
                            self.dictLeadData = dictMutable as NSDictionary
                            
                            self.tblviewLeadDetails.reloadData()
                            self.btnLeadStatusNew.setTitle("\(self.dictLeadData.value(forKey: "StatusName") ?? "")", for: .normal)
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        DispatchQueue.main.async{
                            
                            FTIndicator.dismissProgress()
                            
                        }
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        DispatchQueue.main.async
                            {
                                
                                FTIndicator.dismissProgress()
                        }
                        
                    }
                }
            
            }
            
        }
        
    }
    func deadLead()
    {
        
        nsud.set(true, forKey: "fromWebLeadDetailTableAction")
        nsud.synchronize()
        
        if !isInternetAvailable()
        {
            
        }
        else
        {
            
            //var strUrl = "\(strServiceUrlMain)\(URLDeadWebLead)\("\(dictLeadData.value(forKey: "LeadId") ?? "")")&EmployeeId=\(strEmpID)"  as String
            var strUrl = "\(strServiceUrlMain)\(URLDeadWebLead)\(strRefIdNew)&EmployeeId=\(strEmpID)"  as String

            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
           // FTIndicator.showProgress(withMessage: "Updating Lead...", userInteractionEnable: false)
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            
            //Description
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "GrabLead") { (Response, Status) in
                
                self.loader.dismiss(animated: false)
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                    if(Status)
                    {
                        let strResponse = Response["data"] as! String
                        
                        if (strResponse.count > 0) && strResponse == "true"
                        {
                         
                            var dictMutable = NSMutableDictionary()//self.dictLeadData as! NSMutableDictionary
                            dictMutable = self.dictLeadData.mutableCopy() as! NSMutableDictionary
                            dictMutable.setValue("Dead", forKey: "StatusName")
                            self.dictLeadData = dictMutable as NSDictionary
                            self.tblviewLeadDetails.reloadData()
                            
                            self.btnLeadStatusNew.setTitle("\(self.dictLeadData.value(forKey: "StatusName") ?? "")", for: .normal)
                            
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        DispatchQueue.main.async{
                            
                            FTIndicator.dismissProgress()
                            
                        }
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        DispatchQueue.main.async
                            {
                                
                                FTIndicator.dismissProgress()
                        }
                        
                    }
                }
                
            }
            
        }
        
    }
    
    //MARK: - Alert notes API work
    
    func clearDBBeforeAddingAPIData(){
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "AlertNotes", predicate: NSPredicate(format: "workorderId == %@", "\(dictLeadData.value(forKey: "LeadNumber") ?? "")"))
        
        if arryOfData.count > 0
        {
            for i in 0..<arryOfData.count{
                let objData = arryOfData[i] as! NSManagedObject
                deleteDataFromDB(obj: objData)
            }
        }
    }
    
    func getDataFromDB() -> NSMutableArray{
        let objOfNotes = NSMutableArray()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "AlertNotes", predicate: NSPredicate(format: "workorderId == %@", "\(dictLeadData.value(forKey: "LeadNumber") ?? "")"))
        
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objOfNotes.add(dict)
            }
        }
        return objOfNotes
    }
    
    func getNotesAlert(){
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let refType = "leadno"
        
        let strURL = String(format:  "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")" +  URL.ServiceNewPestFlow.getNotesAlert, refType, "\(dictLeadData.value(forKey: "LeadNumber") ?? "")" , Global().getCompanyKey())
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { [self] (response, status) in
            
            if(status)
            {
                self.clearDBBeforeAddingAPIData()
                let arrOfNotesTemp = response.value(forKey: "data") as! [[String:Any]]
                for i in 0..<arrOfNotesTemp.count{
                    setDataIntoCoreDate(arrOfCurrentIndex: arrOfNotesTemp[i])
                }
                    showAccountAndNotesAlert(isOffline: false)
            }
        }
    }
    
    func showAccountAndNotesAlert(isOffline: Bool){
        if isOffline == true{
           
        }
        else{
            let arrOfNotes = getDataFromDB()
            var arrOfListOfMessages = [String]()
            for i in 0..<arrOfNotes.count{
                let obj = arrOfNotes[i] as! NSManagedObject
                arrOfListOfMessages.append("\(obj.value(forKey: "noteText")!)")
            }
            
            if arrOfNotes.count > 0 {
                let alert = UIAlertController(title: "Alert", message: arrOfListOfMessages.toBulletList(), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func setDataIntoCoreDate(arrOfCurrentIndex: [String: Any]){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("noteText")
        arrOfKeys.add("noteId")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("username")
        
        
        arrOfValues.add(arrOfCurrentIndex["Note"] as? String ?? "")
        arrOfValues.add(arrOfCurrentIndex["LeadNoteId"] as? String ?? "")
        arrOfValues.add("\(dictLeadData.value(forKey: "LeadNumber") ?? "")")
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        
        saveDataInDB(strEntity: "AlertNotes", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }

    func releaseLead()
    {
        nsud.set(true, forKey: "fromWebLeadDetailTableAction")
        nsud.synchronize()
        
        if !isInternetAvailable()
        {
        }
        else
        {
            
            var strUrl = "\(strServiceUrlMain)\(URLGrabWebLead)\("\(dictLeadData.value(forKey: "opportunityId") ?? "")")&EmployeeId=\(strEmpID)"  as String
            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            //FTIndicator.showProgress(withMessage: "Updating Lead...", userInteractionEnable: false)
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
               
                self.loader.dismiss(animated: false)
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                    if(Status)
                    {
                        
                        let arrData = Response["data"] as! NSArray
                        
                        if arrData.isKind(of: NSArray.self)
                        {
                            let arrAllData = NSMutableArray()
                            
                            for item in arrData
                            {
                                let  dict = item as! NSDictionary
                                
                                let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))
                                
                                arrAllData.add(dictNew as Any)
                                
                                
                            }
                            let str = getJson(from: arrAllData)
                            print("\(str ?? "")" as Any)
                            
                        }
                        else
                        {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        DispatchQueue.main.async{
                            
                            FTIndicator.dismissProgress()
                            
                        }
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        DispatchQueue.main.async
                            {
                                
                                FTIndicator.dismissProgress()
                        }
                        
                    }
                }
                
            }
            
        }
        
    }
    func fetchMatchingAccounts(strWebLeadNumber: String)
    {
        if !isInternetAvailable()
        {
        }
        else
        {
            
            var strUrl = "\(strServiceUrlMain)\(UrlGetMatchingAccountsToConvertWebLead)\(strWebLeadNumber)\(UrlGetContactDetailByContactIdCompanykey)\(strCompanyKey)"  as String
            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
           // FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            
            
            //Description
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
                
                self.loader.dismiss(animated: false)
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                    if(Status)
                    {
                        
                        let arrData = Response["data"] as! NSArray
                        
                        if arrData.isKind(of: NSArray.self)
                        {
                            if arrData.count > 0
                            {
                                let arrAllData = NSMutableArray()
                                
                                for item in arrData
                                {
                                    let  dict = item as! NSDictionary
                                    
                                    let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))
                                    
                                    arrAllData.add(dictNew as Any)
                                    
                                }
                                
                                let str = getJson(from: arrAllData)
                                //  print("\(str ?? "")" as Any)
                                
                                self.arrOfMatchingAccounts = arrAllData.mutableCopy() as! NSArray
                                
                                self.createViewMatchingAccounts()
                                
                            }
                            else
                            {
                                self.showPopUpForCreateNewAccount()
                                
                            }
                            
                        }
                        else
                        {
                            
                            self.showPopUpForCreateNewAccount()
                            
                        }
                        DispatchQueue.main.async{
                            
                            FTIndicator.dismissProgress()
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        DispatchQueue.main.async{
                            
                            FTIndicator.dismissProgress()
                            
                        }
                        
                    }
                }
                
                
            }
            
        }
        
    }
    
    fileprivate func callAPIToGetActivityList()
    {
        strReftype = "WebLead" //"WebLead"//
        strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "leadId") ?? "")"
        
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetActivitiesByRefIdAndRefTypeAsyncV2_SubObjs?refid=\(strRefId)&reftype=\(strReftype)"
        
        DispatchQueue.main.async {
            
          //  FTIndicator.showProgress(withMessage: "Fetching Activity...", userInteractionEnable: false)
        }
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
            }
            
            if(status == true)
            {
                if((response.value(forKey: "data") as! NSArray).count > 0)
                {
                    self.aryActivities.removeAllObjects()
                    
                    self.saveActivityListToLocalDb(data: response.value(forKey: "data") as! NSArray)
                    
                }
                else
                {
                    
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                    }
                    
                }
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
            }
            else
            {
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
                
            }
        }
    }
    func callApiToGetContacts(){
        
        //lbl.removeFromSuperview()

        //FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        
        if nsud.value(forKey: "CrmContactFilter") == nil {
            
            setDefaultValuesForContactFilter()
            
        }
        
        var dictToSend = nsud.value(forKey: "CrmContactFilter") as! NSDictionary
        
        
        var dictToSendNew = NSMutableDictionary()
        dictToSendNew = dictToSend.mutableCopy() as! NSMutableDictionary
        dictToSendNew.setValue(enumRefTypeWebLead, forKey: "RefType")
        dictToSendNew.setValue(strRefIdNew, forKey: "RefId")
        
        dictToSend = dictToSendNew.mutableCopy() as! NSDictionary
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlSearchCrmContacts

        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "SearchCrmContactsNew"
        self.dispatchGroup.enter()
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            self.dispatchGroup.leave()
            DispatchQueue.main.async {
                
                FTIndicator.dismissProgress()
                //self.refresher.endRefreshing()

            }
            
            if(success)
            {
                //Contacts
                
                let dictResponse = (response as NSDictionary?)!
                
                if (dictResponse.count > 0) && (dictResponse.isKind(of: NSDictionary.self)) {
                    
                    let arrOfKey = dictResponse.allKeys as NSArray
                    
                    if arrOfKey.contains("Contacts") {
                        
                        self.arrOfContacts = dictResponse.value(forKey: "Contacts") as! NSMutableArray
                        
                        if self.arrOfContacts.count > 0
                        {
                            
                            //self.tblviewContact.isHidden = false
                            self.tblviewLeadDetails.reloadData()
                            self.tblContactNew.reloadData()
                            //self.logicForFilterContacts(arrToFilter: self.arrOfContacts)
                            
                            
                        } else {
                            
                           // self.noDataLbl()
                            
                        }
                        
                        
                    } else {
                        
                        //self.noDataLbl()
                       // showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }

                    
                } else {
                    
                    //self.noDataLbl()
                    //showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                    
                }
                
                self.heightContactTable()
            
                
            }
            else
            {
                
               // self.noDataLbl()
               // showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                
            }

        }
    }
    func heightContactTable()
    {
        const_TblContactNew_H.constant =  (DeviceType.IS_IPAD ? 40 : 30)
        const_TblContactNew_H.constant = CGFloat(arrOfContacts.count * (DeviceType.IS_IPAD ? 40 : 30))
        
        if const_TblContactNew_H.constant > (DeviceType.IS_IPAD ? 140 : 120)
        {
             const_TblContactNew_H.constant = (DeviceType.IS_IPAD ? 140 : 120)
            tblContactNew.isScrollEnabled = true
            
        }
        else
        {
            tblContactNew.isScrollEnabled = false
        }
        
        if arrOfContacts.count == 0
        {
            const_TblContactNew_H.constant = (DeviceType.IS_IPAD ? 40 : 30)
            tblContactNew.isHidden = true
        }
        else
        {
            tblContactNew.isHidden = false
        }
        tblContactNew.separatorColor = UIColor.clear
        tblContactNew.showsVerticalScrollIndicator = false
    }
    func callApiToGetCompanies(){
        
       // lbl.removeFromSuperview()
        
        arrayCompanies = NSMutableArray()
        //arrayCompaniesData = NSMutableArray()

        if nsud.value(forKey: "CrmCompanyFilter") == nil {
            
            setDefaultValuesForCompanyFilter()
            
        }
        
        let dictToSend = nsud.value(forKey: "CrmCompanyFilter")
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend!) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend!, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlSearchCrmCompanies

        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "SearchCrmCompanyNew"
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            DispatchQueue.main.async {
                
                FTIndicator.dismissProgress()
                //self.refresher.endRefreshing()

            }
            
            if(success)
            {
                //Contacts
                
                let dictResponse = (response as NSDictionary?)!
                
                if (dictResponse.count > 0) && (dictResponse.isKind(of: NSDictionary.self)) {
                    
                    let arrOfKey = dictResponse.allKeys as NSArray
                    
                    if arrOfKey.contains("CrmCompanies") {
                        
                        let arrData = dictResponse.value(forKey: "CrmCompanies") as! NSArray
                        
                        self.arrayCompanies = arrData.mutableCopy() as! NSMutableArray
                       // self.arrayCompaniesData =  self.arrayCompanies
                        
                        if self.arrayCompanies.count > 0 {
                            
                           // self.tblviewCompany.isHidden = false
                           // self.tblviewCompany.reloadData()
                            
                        } else {
                            
                           // self.noDataLbl()
                            
                        }
                        
                        
                    } else {
                        
                       // self.noDataLbl()
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }

                    
                } else {
                    
                    //self.noDataLbl()
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                    
                }
            
                
            }else {
                
                //self.noDataLbl()
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                
            }

        }
    }
    
    func diAssociateContact(strCrmContactId : String,  strCrmCompanyId: String)
    {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlDissociateContact + "\(strCrmContactId)" + "&RefType=" + enumRefTypeWebLead + "&RefId=" + strRefIdNew
        
        //FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "GrabLead") { (Response, Status) in
            
            self.loader.dismiss(animated: false)
            {
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    
                }
                if(Status)
                {
                    let strResponse = Response["data"] as! String
                    
                    if (strResponse.count > 0) && strResponse == "true"
                    {
                        
                        // Successfully deassociated
                        // call APi for Fetching assocaitions again
                        
                        FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                           // self.callAPIToGetContactAssociation()
                            self.callApiToGetContacts()
                            
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                }
                else
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async
                        {
                            
                            FTIndicator.dismissProgress()
                    }
                    
                }
            }
            
            

            
        }
        
    }
    
    func diAssociateCompany(strCrmContactId : String,  strCrmCompanyId: String)
    {

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlDissociateCompany + "\(strCrmCompanyId)" + "&RefType=" + enumRefTypeWebLead + "&RefId=" + strRefIdNew
        
        //FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "GrabLead") { (Response, Status) in
            
            self.loader.dismiss(animated: false)
            {
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    
                }
                if(Status)
                {
                    let strResponse = Response["data"] as! String
                    
                    if (strResponse.count > 0) && strResponse == "true"
                    {
                        
                        // Successfully deassociated
                        // call APi for Fetching assocaitions again
                        
                        FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                           // self.callAPIToGetContactAssociation()
                            self.getBasicInfoWebLead()
                            
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                }
                else
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async
                        {
                            
                            FTIndicator.dismissProgress()
                    }
                    
                }
            }
            
        }
        
    }
    
    fileprivate func callAPIToGetTaskDetailById(strId : String){
        
        FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)

        var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetTaskById + strId
        
        strURL = strURL.trimmingCharacters(in: .whitespaces)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "CRMConactNew_ContactDetails_About") { (response, status) in
            
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
                
            }
            
            if(status)
            {
                if((response.value(forKey: "data") as! NSDictionary).count > 0)
                {
                    var dictTaskDetailData = (response.value(forKey: "data") as! NSDictionary)
                   
                    dictTaskDetailData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictTaskDetailData as! [AnyHashable : Any]))! as NSDictionary
                    
                    // goto create follow up
                    
                    self.gotoCreateFollowUp(dictData: dictTaskDetailData)
                    
                }
                else{
                    
                    DispatchQueue.main.async {
                        
                        FTIndicator.dismissProgress()
                    }
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)

                    
                }
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)

                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
                
            }
        }
    }
    
    // MARK: -  ------------------------------ Other Functions  ------------------------------
    
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
                          lbltaskCount.isHidden = true
                          lblScheduleCount.isHidden = true
               }
                  
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }

                    if strScheduleCount.count > 0 {
                       if(DeviceType.IS_IPAD){
                            lblScheduleCount.text = strScheduleCount
                            lblScheduleCount.isHidden = false
                        }else{
                        let badgeSchedule = SPBadge()
                        badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                        badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeSchedule.badge = strScheduleCount
                        btnScheduleFooter.addSubview(badgeSchedule)
                        }
                    }
                    

                    if strTaskCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                                                  lbltaskCount.text = strTaskCount
                                                  lbltaskCount.isHidden = false
                                                  
                                              }else{
                        let badgeTasks = SPBadge()
                        badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                        badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeTasks.badge = strTaskCount
                        btnTasksFooter.addSubview(badgeTasks)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func goToLogAsActivity() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "LogAsActivityVC") as! LogAsActivityVC
                     
            vc.strType = "LogAsActivity"
            vc.strRefType = enumRefTypeWebLead
            vc.strRefTypeId = "\(self.dictLeadData.value(forKey: "LeadId") ?? "")"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    func gotoCreateFollowUp(dictData : NSDictionary) {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(2, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountId")!)", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadId")!)", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadId")!)", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactId")!)", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyId")!)", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountDisplayName")!)", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadName")!)", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadName")!)", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactName")!)", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyName")!)", forKeyPath: "Pre_CrmCompanyName")
        
        dictTaskDetailsData.addEntries(from: dictData as! [AnyHashable : Any])

        //dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadTaskId")!)", forKeyPath: "LeadTaskId")
        //dictTaskDetailsData.setValue("\(dictData.value(forKey: "status")!)", forKeyPath: "status")

       let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)

    }
    
    func catchNotification(notification:Notification) {
        
        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.getBasicInfoWebLead()
            }

        }
        
    }
    
    func catchNotification1(notification:Notification) {
        
        if (isInternetAvailable())
        {
            
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
                                 {
                                     self.getAllNotes()
                                 }

        }
        
    }
    
    func catchNotification2(notification:Notification) {
        
        if (isInternetAvailable())
        {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.callApiToGetContacts()
            }

        }
        
    }
    
    func catchNotificationTaskAdded(notification:Notification) {
        
        if (isInternetAvailable())
        {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.getAllTask()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.callAPToGetTimeLine()
            }

        }
        
    }
    func catchNotificationActivityAdded(notification:Notification) {
        
        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                
            }

        }
        
    }
    func catchNotificationDocumentsAdded(notification:Notification) {
        
        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.getAllDocuments()
                                    
            }

        }
        
    }
    func catchNotification_TimeLine(notification:Notification) {
           
           if (isInternetAvailable()){
               
               DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
               {
                   self.callAPToGetTimeLine()
               }

           }
           
       }
    
    fileprivate func getLogTypeFromMaster()
    {
        if nsud.value(forKey: "TotalLeadCountResponse") is NSDictionary
        {
            let dict = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
            
            if(dict.count > 0)
            {
                if dict.value(forKey: "ActivityLogTypeMasters") is NSArray
                {
                    let aryTemp = (dict.value(forKey: "ActivityLogTypeMasters") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    for item in aryTemp
                    {
                        if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                        {
                            aryLogType.add((item as! NSDictionary))
                        }
                    }
                    
                    print(aryLogType)
                }
            }
        }
    }
    
    
    func sourceNameFromId(sourceId: String) -> String
    {
        var sourceName = String()
        sourceName = ""
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        for item in arrOfData
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "SourceId") ?? "")" == sourceId
            {
                sourceName = "\(dict.value(forKey: "Name") ?? "")"
                break
            }
            
        }
        return sourceName
    }
    
    
    func setInitialValues()
    {
        // WebLeadSourceId  BranchesSysNames
        tblviewLeadDetails.estimatedRowHeight = 50
        tblviewLeadDetails.rowHeight = UITableView.automaticDimension;
        
        //lblHeader.text = "\(dictLeadData.value(forKey: "LeadName") ?? "")"
        
        let strAccNo = "\(dictLeadData.value(forKey: "AccountNo") ?? "")"
        if strAccNo.count > 0
        {
            lblHeader.text = "Account - # \(strAccNo) / \((dictLeadData.value(forKey: "LeadName") ?? ""))"

        }
        else
        {
            lblHeader.text = "\(dictLeadData.value(forKey: "LeadName") ?? "")"
        }
        
        lblHeader.text = "\(dictLeadData.value(forKey: "LeadName") ?? "")"
        lblHeader.textAlignment = .left
        
        //lblLeadNo.text = "\(dictLeadData.value(forKey: "LeadNumber") ?? "")"

        //lblCreatedBy.text = getEmployeeNameFromId(strEmpId: "\(dictLeadData.value(forKey: "CreatedBy") ?? "")")//Global().getEmployeeName(viaId: "\(dictLeadData.value(forKey: "CreatedBy") ?? "")")

        //btnUserName.setTitle("\(dictLeadData.value(forKey: "FirstName") ?? "")" + " " + "\(dictLeadData.value(forKey: "MiddleName") ?? "")" + " " + "\(dictLeadData.value(forKey: "LastName") ?? "")", for: .normal)
        
        if dictLeadData.value(forKey: "CrmCompany") is NSDictionary
        {
            
            btnCompanyName.setTitle("\(dictLeadData.value(forKeyPath: "CrmCompany.Name") ?? "")", for: .normal)
            
        }
        
       // lblFlowType.text = "\(dictLeadData.value(forKey: "FlowType") ?? "")"
        
        //lblCreatedDate.text = changeStringDateToGivenFormat(strDate: "\(dictLeadData.value(forKey: "CreatedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
        
        var strAddress = ""
        
        if "\(dictLeadData.value(forKey: "Address1") ?? "")".count > 0
        {
            strAddress = "\(dictLeadData.value(forKey: "Address1") ?? ""), "
        }
        if "\(dictLeadData.value(forKey: "Address2") ?? "")".count > 0
        {
            strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "Address2") ?? "")"), "
        }
        if "\(dictLeadData.value(forKey: "CityName") ?? "")".count > 0
        {
            strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "CityName") ?? "")"), "
        }
      
        if "\(dictLeadData.value(forKey: "StateId") ?? "")".count > 0
        {
            
            let stateName = Global().strStatName(fromID: "\((dictLeadData.value(forKey: "StateId") ?? ""))")
            strAddress = "\(strAddress)\(stateName ?? ""), "
            
        }
        if "\(dictLeadData.value(forKey: "Zipcode") ?? "")".count > 0
        {
            strAddress = "\(strAddress)\("\(dictLeadData.value(forKey: "Zipcode") ?? "")")"
        }
        
        btnAddress.setTitle(strAddress, for: .normal)
        
        if strAddress.count > 0 {
            
            btnAddress.isHidden = false
            
        } else {
            
            btnAddress.isHidden = true
            
        }
        
        //lblSourceName.text = ""

        let defsLogindDetail = UserDefaults.standard
        
        if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
            
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
                
                let arrOfData = dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray
                
                if(arrOfData.count > 0)
                {
                    
                    if dictLeadData.value(forKey: "WebLeadSourceId") is NSArray {
                        
                        let arrWebLeadSourceId = dictLeadData.value(forKey: "WebLeadSourceId")  as! NSArray

                        let strSourceName = Global().getSourceNameMultiple(arrWebLeadSourceId as? [Any], arrOfData as? [Any])
                        
                        //lblSourceName.text = strSourceName
                        
                    }
                    
                }
                
            }
            
        }
        
        if dictLeadData.value(forKey: "BranchesSysNames") is NSArray
        {
            let arrBranchSysName = dictLeadData.value(forKey: "BranchesSysNames")  as! NSArray
            
            let strBranchName = Global().getBranchNameMultiple(arrBranchSysName as? [Any])
            
            //lblBranchName.text = strBranchName
        }
        else
        {
            //lblBranchName.text = ""
        }
        
        
        if dictLeadData.value(forKey: "CrmCompany") is NSDictionary
        {
            btnEditCompany.isHidden = false
        }
        else
        {
            btnEditCompany.isHidden = true
        }
        // Do any additional setup after loading the view.
        
        //Temp Nilind
        
        /*if nsud.bool(forKey: "fromNearByOpportunity") == true
        {
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalWebLeads", predicate: NSPredicate(format: "leadId == %@", "\(self.dictLeadData.value(forKey: "LeadId") ?? "")"))
            
            if arryOfData.count == 0
            {
                saveWebLeadToCoreData()
                
                let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalWebLeads", predicate: NSPredicate(format: "leadId == %@", "\(self.dictLeadData.value(forKey: "LeadId") ?? "")"))
                
                
                dictLeadData = getMutableDictionaryFromNSManagedObject(obj: arryOfData.object(at: 0) as! NSManagedObject) as NSDictionary
                //dictLeadData = arryOfData.object(at: 0) as! NSManagedObject
                
            }
            else
            {
                //dictLeadData = arryOfData.object(at: 0) as! NSManagedObject
                 dictLeadData = getMutableDictionaryFromNSManagedObject(obj: arryOfData.object(at: 0) as! NSManagedObject) as NSDictionary
            }
            nsud.set(false, forKey: "fromNearByOpportunity")
            nsud.synchronize()
        }*/
        
        btnLeadStatusNew.setTitle("\(dictLeadData.value(forKey: "StatusName") ?? "")", for: .normal)
       
    }
    
    func goToNotesVC()
    {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
        
        
        //vc.dictLeadData = dictLeadData
        vc.strLeadType = "WebLead"
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    fileprivate func goToAddNote(){
        
          let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
        
        vc.strRefType = enumRefTypeWebLead
        vc.strLeadType = enumRefTypeWebLead
        vc.strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "leadId") ?? "")"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    fileprivate func goToAddLead()
    {
        
         let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    fileprivate func gotoAssociateContact(){
        
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
          
        controller.strFromWhere = "DirectAssociate"
        controller.strRefType = enumRefTypeWebLead
        controller.strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "LeadId") ?? "")"
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    fileprivate func gotoAssociateCompany(){
        
       let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
        controller.strFromWhere = "DirectAssociate"
        controller.strRefType = enumRefTypeWebLead
        controller.strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "LeadId") ?? "")"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func gotoAddDocument(){
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddDocumentCRM_VC") as! AddDocumentCRM_VC
      
        controller.strRefType = enumRefTypeWebLead
        controller.strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "LeadId") ?? "")"
        self.navigationController?.present(controller, animated: false, completion: nil)
        
    }
    
    func goToEditWebLeadVC()
    {
        
        //commented by akshay for EditLeadNewVC
      /*  let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "EditLeadsViewControlleriPad") as! EditLeadsViewControlleriPad
      
        let dictMutable =  dictLeadData as NSDictionary
        
        vc.dictLeadDetail =  dictMutable as? [AnyHashable : Any]
        vc.dictContactDetail = dictMutable as? [AnyHashable : Any]
        vc.strFromLeadDetailVC = "leadvc"
        self.present(vc, animated: false, completion: nil)*/
         let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditLeadNewVC") as! EditLeadNewVC
  
        vc.dictLeadDetails = dictLeadData
       
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func goToAddTaskVC()
    {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue(strRefIdNew, forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(dictLeadData.value(forKey: "LeadName") ?? "")", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        
       let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    func goToTaskDetail(strId : String)
    {
       let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
        vc.taskId = "\(strId)"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func goToActivityDetail(dict: NSDictionary)
    {
          let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityDetailsVC_CRMNew_iPhone") as! ActivityDetailsVC_CRMNew_iPhone
            
        vc.dictActivity = dict.mutableCopy() as! NSMutableDictionary
        vc.strFromVC = "WebLeadVC"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToAddActivityVC()
    {
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue(strRefIdNew, forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(dictLeadData.value(forKey: "LeadName") ?? "")", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        
       let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
        vc.dictActivityDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    func goToCreateFollowUpVC()
    {
        
           let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateFollowUpVC") as! CreateFollowUpVC
      
        vc.dictLeadData = dictLeadData
        
        self.present(vc, animated: false, completion: nil)
        
        nsud.set(true, forKey: "fromWebLeadDetailTableAction")
        nsud.synchronize()
    }
    func goToReleaseLeadVC()
    {
        
        
        
           let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReleaseLeadVC") as! ReleaseLeadVC
        
        vc.dictLeadData = dictLeadData
        
        self.present(vc, animated: false, completion: nil)
        
        nsud.set(true, forKey: "fromWebLeadDetailTableAction")
        nsud.synchronize()
    }
    func gotoConvertLead(strTypeToConvert: String, dictLeadDetail: NSDictionary)
    {
        
          let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "ConvertToOppoetunity_iPhoneVC") as! ConvertToOppoetunity_iPhoneVC
      
        let dictLeadDetailNew = createConverOpportunityData()
        vc.strTypeToConvert = strTypeToConvert
        vc.dictLeadDetail = self.dictLeadData;
        vc.dictAccountDetail = self.dictAccountDetail;

        self.navigationController?.pushViewController(vc, animated: false)
        view_MatchingAccounts.removeFromSuperview()
    }
    func makeCall(sender : UIButton)
    {
        let arrNoToCall = NSMutableArray()
        
        if "\(dictLeadData.value(forKey: "CellPhone1") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "CellPhone1") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")")
        }
        
        
        if arrNoToCall.count > 0
            
        {
            
            let alert = UIAlertController(title: "", message: "Make your call on selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as UIView
                popoverController.sourceRect = sender.bounds
               // popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
            }
            for item in arrNoToCall
            {
                let strNo = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strNo)", style: .default , handler:{ (UIAlertAction)in
                    
                    
                    if (strNo.count > 0)
                    {
                        Global().calling(strNo)
                    }
                    
                }))
                
                
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
         
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No contacts detail found", viewcontrol: self)
            
        }
    }
    func sendMail(sender : UIButton)
    {
        let arrEmail = NSMutableArray()
        
        if "\(dictLeadData.value(forKey: "PrimaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(dictLeadData.value(forKey: "PrimaryEmail") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "SecondaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(dictLeadData.value(forKey: "SecondaryEmail") ?? "")")
        }
        
        if arrEmail.count > 0
            
        {
            let alert = UIAlertController(title: "", message: "Make your email on selection", preferredStyle: .actionSheet)
               alert.view.tintColor = UIColor.black
            for item in arrEmail
            {
                let strEmail = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strEmail)", style: .default , handler:{ (UIAlertAction)in
                    
                    if (strEmail.count > 0)
                    {
                        //Global().emailComposer(strEmail, "", "", self)
                        self.sendEmail(strEmail: strEmail)
                        
                    }
                    
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.view.tintColor = UIColor.black
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as UIView
                popoverController.sourceRect = sender.bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
            }
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No email found", viewcontrol: self)
            
        }
    }
    
    func sendMessage(sender: UIButton)
    {
        let arrNoToCall = NSMutableArray()
        
        if "\(dictLeadData.value(forKey: "CellPhone1") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "CellPhone1") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")")
        }
        
        if arrNoToCall.count > 0
            
        {
            
            let alert = UIAlertController(title: "", message: "Make your message on selection", preferredStyle: .actionSheet)
               alert.view.tintColor = UIColor.black
            for item in arrNoToCall
            {
                let strNo = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strNo)", style: .default , handler:{ (UIAlertAction)in
                    
                    
                    if (strNo.count > 0)
                    {
                        self.displayMessageInterface(strNo: "\(strNo)")
                    }
                    
                }))
                
                
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
              alert.view.tintColor = UIColor.black
                     
                     if let popoverController = alert.popoverPresentationController {
                         popoverController.sourceView = sender as UIView
                         popoverController.sourceRect = sender.bounds
                         popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                     }
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No contacts detail found", viewcontrol: self)
            
        }
    }
    
    func createCompanyDetailData() -> NSDictionary
    {
        print("\(dictLeadData)")
        
        var strAddress1 = String()
        var strAddress2 = String()
        var strCustomerAddressId = String()
        var strCityName = String()
        var strStateId = String()
        var strZipcode = String()
        var strStateName = String()
        
        var strCompanyCell = String()
        var strCompanyPrimaryPhone = String()
        var strCompanySecondaryPhone = String()
        var strCompanyPrimaryEmail = String()
        var strCompanySecondaryEmail = String()
        
        var strCompanyId = String()
        var strCompanyName = String()
        var strCountryId = String()
        var strCountryName = String()
        var strCounty = String()
        var strCRMCompanyId = String()
        var strIsActive = String()
        var strProfileImage = String()
        
        
        /* strAddress1 = "\(dictLeadData.value(forKeyPath: "Address1") ?? "")"
         strAddress2 = "\(dictLeadData.value(forKeyPath: "Address2") ?? "")"
         strCustomerAddressId = "\(dictLeadData.value(forKeyPath: "Address1") ?? "")"
         strCityName = "\(dictLeadData.value(forKeyPath: "CityName") ?? "")"
         
         strStateId = "\(dictLeadData.value(forKeyPath: "StateId") ?? "")"
         
         strZipcode = "\(dictLeadData.value(forKeyPath: "Zipcode") ?? "")"
         
         strStateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "StateId") ?? ""))")*/
        
        
        if dictLeadData.value(forKey: "CrmCompany") is NSDictionary
        {
            strCompanyId = "\(dictLeadData.value(forKeyPath: "CrmCompany.CompanyId") ?? "")"
            strCompanyName = "\(dictLeadData.value(forKeyPath: "CrmCompany.Name") ?? "")"
            strCountryId = "\(dictLeadData.value(forKeyPath: "CrmCompany.CountryId") ?? "")"
            strCountryName = "\(dictLeadData.value(forKeyPath: "CrmCompany.CountryName") ?? "")"
            strCounty = ""//"\(dictLeadData.value(forKeyPath: "accountCompany.CountryName") ?? "")"
            strCRMCompanyId = "\(dictLeadData.value(forKeyPath: "CrmCompany.CrmCompanyId") ?? "")"
            
            strIsActive = "\(dictLeadData.value(forKeyPath: "CrmCompany.IsActive") ?? "")"
            strProfileImage = "\(dictLeadData.value(forKeyPath: "CrmCompany.ProfileImage") ?? "")"
            
            //New
            
            //New
            strCompanyCell = "\(dictLeadData.value(forKeyPath: "CrmCompany.CellPhone1") ?? "")"
            strCompanyPrimaryPhone = "\(dictLeadData.value(forKeyPath: "CrmCompany.PrimaryPhone") ?? "")"
            strCompanySecondaryPhone = "\(dictLeadData.value(forKeyPath: "CrmCompany.SecondaryPhone") ?? "")"
            strCompanyPrimaryEmail = "\(dictLeadData.value(forKeyPath: "CrmCompany.PrimaryEmail") ?? "")"
            strCompanySecondaryEmail = "\(dictLeadData.value(forKeyPath: "CrmCompany.SecondaryEmail") ?? "")"
            
            
            strAddress1 = "\(dictLeadData.value(forKeyPath: "CrmCompany.Address1") ?? "")"
            strAddress2 = "\(dictLeadData.value(forKeyPath: "CrmCompany.Address2") ?? "")"
            strCustomerAddressId = "\(dictLeadData.value(forKeyPath: "CrmCompany.Address1") ?? "")"
            strCityName = "\(dictLeadData.value(forKeyPath: "CrmCompany.CityName") ?? "")"
            strStateId = "\(dictLeadData.value(forKeyPath: "CrmCompany.StateId") ?? "")"
            strZipcode = "\(dictLeadData.value(forKeyPath: "CrmCompany.Zipcode") ?? "")"
            strStateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "CrmCompany.StateId") ?? ""))")
            
        }
        else
        {
            strCompanyId = ""
            strCompanyName = ""
            strCountryId = ""
            strCountryName = ""
            strCRMCompanyId = ""
            strIsActive = ""
            strProfileImage = ""
            
            strAddress1 = ""
            strAddress2 = ""
            strCustomerAddressId = ""
            strCityName = ""
            strStateId = ""
            strZipcode = ""
            strStateName = ""
            
            strCompanyCell = ""
            strCompanyPrimaryPhone = ""
            strCompanySecondaryPhone = ""
            strCompanyPrimaryEmail = ""
            strCompanySecondaryEmail = ""
        }
        
        
        
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("AccountId")
        arrOfKeys.add("Address1")
        arrOfKeys.add("Address2")
        arrOfKeys.add("CellPhone1")
        arrOfKeys.add("CellPhone2")
        arrOfKeys.add("CityName")
        arrOfKeys.add("CompanyId")
        arrOfKeys.add("CompanyKey")
        arrOfKeys.add("CompanyName")
        arrOfKeys.add("CountryId")
        arrOfKeys.add("CountryName")
        arrOfKeys.add("County")
        arrOfKeys.add("CreatedBy")
        arrOfKeys.add("CreatedByName")
        arrOfKeys.add("CreatedDate")
        arrOfKeys.add("CrmCompanyId")
        arrOfKeys.add("CrmContactId")
        arrOfKeys.add("FacebookLink")
        arrOfKeys.add("GoogleLink")
        arrOfKeys.add("IsActive")
        arrOfKeys.add("LeadCount")
        arrOfKeys.add("LinkedInLink")
        arrOfKeys.add("ModifiedBy")
        arrOfKeys.add("ModifiedDate")
        arrOfKeys.add("OpportunityCount")
        arrOfKeys.add("PrimaryEmail")
        arrOfKeys.add("PrimaryPhone")
        arrOfKeys.add("PrimaryPhoneExt")
        arrOfKeys.add("ProfileImage")
        arrOfKeys.add("SchoolDistrict")
        arrOfKeys.add("SecondaryEmail")
        arrOfKeys.add("SecondaryPhone")
        arrOfKeys.add("SecondaryPhoneExt")
        arrOfKeys.add("SourceId")
        arrOfKeys.add("StateId")
        arrOfKeys.add("StateName")
        arrOfKeys.add("TwitterLink")
        arrOfKeys.add("WebLeadId")
        arrOfKeys.add("Website")
        arrOfKeys.add("ZipCode")
        
        
        arrOfValues.add("") //AccountId
        
        arrOfValues.add("\(strAddress1)")
        arrOfValues.add("\(strAddress2)")
        //arrOfValues.add("\(dictLeadData.value(forKey: "cellPhone1") ?? "")")
        arrOfValues.add("\(strCompanyCell)")
        arrOfValues.add("")
        
        arrOfValues.add("\(strCityName)")
        arrOfValues.add("\(strCompanyId)")
        arrOfValues.add("\(strCompanyKey)")
        arrOfValues.add("\(strCompanyName)")
        arrOfValues.add("\(strCountryId)")
        arrOfValues.add("\(strCountryName)")
        arrOfValues.add("\(strCounty)")
        arrOfValues.add("\(strUserName)")
        arrOfValues.add("\(strUserName)")
        arrOfValues.add("\(Global().getCurrentDate() ?? "")")
        arrOfValues.add("\(strCRMCompanyId)")
        arrOfValues.add("\(dictLeadData.value(forKey: "CrmContactId") ?? "")")
        
        arrOfValues.add("FacebookLink") //FacebookLink
        arrOfValues.add("GoogleLink") //GoogleLink
        arrOfValues.add("\(strIsActive)") //IsActive
        arrOfValues.add("") //LeadCount
        arrOfValues.add("") //LinkedInLink
        arrOfValues.add("\(strUserName)") //ModifiedBy
        arrOfValues.add("\(Global().getCurrentDate() ?? "")") //ModifiedDate
        arrOfValues.add("") //OpportunityCount
        
        //arrOfValues.add("\(dictLeadData.value(forKey: "primaryEmail") ?? "")")//PrimaryEmail
        arrOfValues.add("\(strCompanyPrimaryEmail)")//PrimaryEmail
        
        
        
        //arrOfValues.add("\(dictLeadData.value(forKey: "primaryPhone") ?? "")") //PrimaryPhone
        arrOfValues.add("\(strCompanyPrimaryPhone)") //PrimaryPhone
        
        
        arrOfValues.add("") //PrimaryPhoneExt
        
        
        arrOfValues.add("\(strProfileImage)") //ProfileImage
        
        arrOfValues.add("") //SchoolDistrict
        
        //arrOfValues.add("\(dictLeadData.value(forKey: "secondaryEmail") ?? "")") //SecondaryEmail
        arrOfValues.add("\(strCompanySecondaryEmail)") //SecondaryEmail
        
        
        
        //arrOfValues.add("\(dictLeadData.value(forKey: "secondaryPhone") ?? "")") //SecondaryPhone
        arrOfValues.add("\(strCompanySecondaryPhone)") //SecondaryPhone
        
        
        
        arrOfValues.add("") //SecondaryPhoneExt
        arrOfValues.add("") //SourceId
        arrOfValues.add("\(strStateId)") //StateId
        arrOfValues.add("\(strStateName)") //StateName
        arrOfValues.add("") //TwitterLink
        arrOfValues.add("") //WebLeadId
        arrOfValues.add("") //Website
        arrOfValues.add("\(strZipcode)") //ZipCode
        
        let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
        
        return dict_ToSend as NSDictionary
        
    }
    func createConverOpportunityData() -> NSDictionary
    {
        print("\(dictLeadData)")
        
        var strAddress1 = String()
        var strAddress2 = String()
        var strCustomerAddressId = String()
        var strCityName = String()
        var strStateId = String()
        var strZipcode = String()
        var strStateName = String()
        
        var strCompanyId = String()
        var strCompanyName = String()
        var strCountryId = String()
        var strCountryName = String()
        var strCounty = String()
        var strCRMCompanyId = String()
        var strIsActive = String()
        var strProfileImage = String()
        
        
        
        strAddress1 = "\(dictLeadData.value(forKeyPath: "Address1") ?? "")"
        strAddress2 = "\(dictLeadData.value(forKeyPath: "Address2") ?? "")"
        strCustomerAddressId = "\(dictLeadData.value(forKeyPath: "Address1") ?? "")"
        strCityName = "\(dictLeadData.value(forKeyPath: "VityName") ?? "")"
        
        strStateId = "\(dictLeadData.value(forKeyPath: "StateId") ?? "")"
        
        strZipcode = "\(dictLeadData.value(forKeyPath: "Zipcode") ?? "")"
        
        strStateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "StateId") ?? ""))")
        
        
        if dictLeadData.value(forKey: "CrmCompany") is NSDictionary
        {
            strCompanyId = "\(dictLeadData.value(forKeyPath: "CrmCompany.CompanyId") ?? "")"
            strCompanyName = "\(dictLeadData.value(forKeyPath: "CrmCompany.Name") ?? "")"
            strCountryId = "\(dictLeadData.value(forKeyPath: "CrmCompany.CountryId") ?? "")"
            strCountryName = "\(dictLeadData.value(forKeyPath: "CrmCompany.CountryName") ?? "")"
            strCounty = ""//"\(dictLeadData.value(forKeyPath: "accountCompany.CountryName") ?? "")"
            strCRMCompanyId = "\(dictLeadData.value(forKeyPath: "CrmCompany.CrmCompanyId") ?? "")"
            
            strIsActive = "\(dictLeadData.value(forKeyPath: "CrmCompany.IsActive") ?? "")"
            strProfileImage = "\(dictLeadData.value(forKeyPath: "CrmCompany.ProfileImage") ?? "")"
        }
        else
        {
            strCompanyId = ""
            strCompanyName = ""
            strCountryId = ""
            strCountryName = ""
            strCRMCompanyId = ""
            strIsActive = ""
            strProfileImage = ""
        }
        
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("Address1")
        arrOfKeys.add("Address2")
        arrOfKeys.add("AssignedToId")
        arrOfKeys.add("AssignedToName")
        arrOfKeys.add("AssignedType")
        arrOfKeys.add("BranchesSysNames")
        arrOfKeys.add("CellPhone1")
        arrOfKeys.add("CityName")
        arrOfKeys.add("CompanyKey")
        arrOfKeys.add("CompanyName")
        arrOfKeys.add("CountryId")
        arrOfKeys.add("CountyName")
        arrOfKeys.add("CreatedBy")
        arrOfKeys.add("CreatedDate")
        arrOfKeys.add("CrmCompany")
        arrOfKeys.add("CrmContactId")
        arrOfKeys.add("FirstName")
        arrOfKeys.add("FlowType")
        arrOfKeys.add("IndustryId")
        arrOfKeys.add("LastName")
        arrOfKeys.add("Latitude")
        arrOfKeys.add("LeadId")
        arrOfKeys.add("LeadName")
        arrOfKeys.add("LeadNumber")
        arrOfKeys.add("Longitude")
        arrOfKeys.add("MiddleName")
        arrOfKeys.add("ModifiedBy")
        arrOfKeys.add("ModifiedDate")
        arrOfKeys.add("PrimaryEmail")
        arrOfKeys.add("PrimaryPhone")
        arrOfKeys.add("PrimaryPhoneExt")
        arrOfKeys.add("SchoolDistrict")
        arrOfKeys.add("SecondaryEmail")
        arrOfKeys.add("SecondaryPhone")
        arrOfKeys.add("SecondaryPhoneExt")
        arrOfKeys.add("StateId")
        arrOfKeys.add("StatusDate")
        arrOfKeys.add("StatusName")
        arrOfKeys.add("UserName")
        arrOfKeys.add("WebLeadSourceId")
        arrOfKeys.add("ZipCode")
        
        
        arrOfValues.add("\(strAddress1)")
        arrOfValues.add("\(strAddress2)")
        arrOfValues.add("\(dictLeadData.value(forKey: "AssignedToId") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "AssignedToName") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "AssignedType") ?? "")")
        
        if dictLeadData.value(forKey: "BranchesSysNames") is NSArray
        {
            let arrBranch = dictLeadData.value(forKey: "BranchesSysNames")  as! NSArray
            arrOfValues.add(arrBranch)
        }
        else
        {
            arrOfValues.add("")
        }
        arrOfValues.add("\(dictLeadData.value(forKey: "CellPhone1") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "CityName") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "CompanyKey") ?? "")")
        
        //arrOfValues.add("\(dictLeadData.value(forKey: "companyName") ?? "")")
        arrOfValues.add("\(strCompanyName)")
        
        
        arrOfValues.add("\(dictLeadData.value(forKey: "CountryId") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "CountyName") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "CreatedBy") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "CrmCompany") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "CrmContactId") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "FirstName") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "FlowType") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "IndustryId") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "LastName") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "Latitude") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "LeadId") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "LeadName") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "LeadNumber") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "Longitude") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "MiddleName") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "ModifiedBy") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "ModifiedDate") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "PrimaryEmail") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "PrimaryPhoneExt") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "SchoolDistrict") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "SecondaryEmail") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "SecondaryPhoneExt") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "StateId") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "StatusDate") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "StatusName") ?? "")")
        arrOfValues.add("\(dictLeadData.value(forKey: "UserName") ?? "")")
        
        if dictLeadData.value(forKey: "WebLeadSourceId") is NSArray
        {
            let arrWebLeadSourceId = dictLeadData.value(forKey: "WebLeadSourceId")  as! NSArray
            arrOfValues.add(arrWebLeadSourceId)
        }
        else
        {
            arrOfValues.add("\(dictLeadData.value(forKey: "WebLeadSourceId") ?? "")")
            
        }
        arrOfValues.add("\(dictLeadData.value(forKey: "Zipcode") ?? "")")
        
        let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
        
        return dict_ToSend as NSDictionary
        
    }
    
    func syncTask()
    {
        if(isInternetAvailable() == true)
        {
            let arrayAllObject = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@", self.strUserName, self.strCompanyKey ))
            
            if(arrayAllObject.count > 0)
            {
                syncAllTaskWebLeadDetail(strUserName: self.strUserName, strCompanyKey: self.strCompanyKey)
            }
            else
            {
                if(isInternetAvailable() == true)
                {
                    getAllTask()
                }
                else
                {
                    fetchTaskListFromLocalDB()
                    fetchActivityListFromLocalDB()
                }
            }
        }
        else
        {
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()
        }
    }
    func syncAllTaskWebLeadDetail(strUserName:String, strCompanyKey:String)
    {
        let aryNonSyncTask = NSMutableArray()
        let aryJsonToPost = NSMutableArray()
        
        let arrayAllObject = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@"  ,strUserName, strCompanyKey))
        
        for item in arrayAllObject
        {
            
            if("\((item as! NSManagedObject).value(forKey: "leadTaskId")!)".count == 0)
            {
                aryNonSyncTask.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
            }
            
        }
        
        if(aryNonSyncTask.count > 0)
        {
            for item in aryNonSyncTask
            {
                let dictData = item as! NSDictionary
                
                let aryOfKeys = NSMutableArray()
                let aryOfValues = NSMutableArray()
                
                aryOfKeys.add("AssignedTo")
                aryOfKeys.add("ChildActivities")
                aryOfKeys.add("CreatedBy")
                aryOfKeys.add("Description")
                aryOfKeys.add("DueDate")
                aryOfKeys.add("Id")
                aryOfKeys.add("LeadTaskId")
                aryOfKeys.add("Priority")
                aryOfKeys.add("RefId")
                aryOfKeys.add("RefType")
                aryOfKeys.add("ReminderDate")
                aryOfKeys.add("Status")
                aryOfKeys.add("Tags")
                aryOfKeys.add("TaskName")
                
                aryOfValues.add("\(dictData.value(forKey: "assignedTo")!)")
                
                aryOfValues.add(dictData.value(forKey: "childActivities") as! NSArray)
                aryOfValues.add("\(dictData.value(forKey: "createdBy")!)")
                aryOfValues.add("\(dictData.value(forKey: "taskDescription")!)")
                aryOfValues.add("\(dictData.value(forKey: "dueDate")!)")
                aryOfValues.add("1")
                aryOfValues.add("\(dictData.value(forKey: "leadTaskId")!)")
                aryOfValues.add("\(dictData.value(forKey: "priority")!)")
                aryOfValues.add("\(dictData.value(forKey: "refId")!)")
                aryOfValues.add("\(dictData.value(forKey: "refType")!)")
                aryOfValues.add("\(dictData.value(forKey: "reminderDate")!)")
                aryOfValues.add("\(dictData.value(forKey: "status")!)")
                aryOfValues.add("\(dictData.value(forKey: "tags")!)")
                aryOfValues.add("\(dictData.value(forKey: "taskName")!)")
                
                let dictJson = NSDictionary.init(objects: aryOfValues as! [Any], forKeys: aryOfKeys as! [NSCopying])
                
                aryJsonToPost.add(dictJson)
            }
            
            
            var jsonString = String()
            
            if(JSONSerialization.isValidJSONObject(aryJsonToPost) == true)
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: aryJsonToPost, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                
                print(jsonString)
                
            }
            
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strHrmsCompanyId = "\(dictLoginData.value(forKeyPath: "Company.HrmsCompanyId")!)"
            
            let strEmployeeNumber = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
            let strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
            let strCreatedBy = "\(dictLoginData.value(forKey: "CreatedBy")!)"
            let strEmployeeName = "\(dictLoginData.value(forKey: "EmployeeName")!)"
            let strCoreCompanyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId")!)"
            let strSalesProcessCompanyId1 = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
            let strCompanyKeyyy = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
            let strUrlSaved = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
            
            
            let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            
            let strUrl = strUrlSaved + UrlAddUpdateTaskGlobalMultiple
            let url = URL(string: strUrl)
            
            let request11 = NSMutableURLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
            
            request11.httpMethod = "POST"
            request11.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request11.addValue("application/json", forHTTPHeaderField: "Accept")
            
            request11.addValue("application/json", forHTTPHeaderField: "Accept")
            request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request11.addValue("IOS", forHTTPHeaderField: "Browser")
            
            request11.addValue(strHrmsCompanyId, forHTTPHeaderField: "HrmsCompanyId")
            
            request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "IpAddress")
            
            request11.addValue(strEmployeeNumber, forHTTPHeaderField: "EmployeeNumber")
            
            request11.addValue(strEmployeeId, forHTTPHeaderField: "EmployeeId")
            
            request11.addValue(strCreatedBy, forHTTPHeaderField: "CreatedBy")
            
            request11.addValue(strEmployeeName, forHTTPHeaderField: "EmployeeName")
            
            request11.addValue(strCoreCompanyId, forHTTPHeaderField: "CoreCompanyId")
            
            request11.addValue(strSalesProcessCompanyId1, forHTTPHeaderField: "SalesProcessCompanyId")
            
            request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
            
            request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
            
            request11.setValue("\(requestData!.count)", forHTTPHeaderField: "Content-Length")
            
            request11.httpBody = requestData
            
            URLSession.shared.dataTask(with: request11 as URLRequest) { (data, response, error) in
                
                if(error == nil)
                {
                    print("Background Syncing Success Task")
                    if(isInternetAvailable() == true)
                    {
                        self.getAllTask()
                    }
                    else
                    {
                        self.fetchTaskListFromLocalDB()
                        self.fetchActivityListFromLocalDB()
                    }
                }
                }.resume()
        }
        else
        {
            if(isInternetAvailable() == true)
            {
                self.getAllTask()
            }
            else
            {
                self.fetchTaskListFromLocalDB()
                self.fetchActivityListFromLocalDB()
            }
        }
    }
    func syncActivity()
    {
        if(isInternetAvailable() == true)
        {
            
            let arrayAllObject = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@", self.strUserName, self.strCompanyKey ))
            
            if(arrayAllObject.count > 0)
            {
                syncAllActivityWebLeadDetail(strUserName: self.strUserName, strCompanyKey: self.strCompanyKey)
            }
            else
            {
                if(isInternetAvailable() == true)
                {
                    callAPIToGetActivityList()
                }
                else
                {
                    fetchTaskListFromLocalDB()
                    fetchActivityListFromLocalDB()
                }
            }
        }
        else
        {
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()
        }
    }
    func syncAllActivityWebLeadDetail(strUserName:String, strCompanyKey:String)
    {
        let aryNonSyncActivity = NSMutableArray()
        let aryJsonToPost = NSMutableArray()
        
        let arrayAllObject = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@"  ,strUserName, strCompanyKey))
        
        for item in arrayAllObject
        {
            
            if("\((item as! NSManagedObject).value(forKey: "activityId")!)".count == 0)
            {
                aryNonSyncActivity.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
            }
            
        }
        
        if(aryNonSyncActivity.count > 0)
        {
            for item in aryNonSyncActivity
            {
                let dictData = item as! NSDictionary
                
                let aryOfKeys = NSMutableArray()
                let aryOfValues = NSMutableArray()
                
                
                aryOfKeys.add("ToDate")
                aryOfKeys.add("LogTypeId")
                aryOfKeys.add("Address2")
                aryOfKeys.add("EmployeeExtDcs")
                aryOfKeys.add("ActivityTime")
                aryOfKeys.add("ChildTasks")
                aryOfKeys.add("CountryId")
                aryOfKeys.add("ActivityCommentExtDcs")
                aryOfKeys.add("CityName")
                aryOfKeys.add("Agenda")
                aryOfKeys.add("StateId")
                aryOfKeys.add("CreatedDate")
                aryOfKeys.add("RefId")
                aryOfKeys.add("CreatedBy")
                aryOfKeys.add("ZipCode")
                aryOfKeys.add("ModifiedDate")
                aryOfKeys.add("Id")
                aryOfKeys.add("ActivityLogTypeMasterExtDc")
                aryOfKeys.add("Participants")
                aryOfKeys.add("ActivityId")
                aryOfKeys.add("Address1")
                aryOfKeys.add("LeadExtDcs")
                aryOfKeys.add("ModifiedBy")
                aryOfKeys.add("FromDate")
                aryOfKeys.add("RefType")
                
                // values
                aryOfValues.add("\(dictData.value(forKey: "toDate")!)")
                aryOfValues.add("\(dictData.value(forKey: "logTypeId")!)")
                aryOfValues.add("\(dictData.value(forKey: "addressLine2")!)")
                aryOfValues.add(NSArray())//EmployeeExtDcs
                aryOfValues.add("\(dictData.value(forKey: "activityTime")!)")
                aryOfValues.add(dictData.value(forKey: "childTasks") as! NSArray)
                aryOfValues.add("\(dictData.value(forKey: "countryId")!)")
                
                aryOfValues.add(NSArray())//ActivityCommentExtDcs
                aryOfValues.add("\(dictData.value(forKey: "cityName")!)")
                aryOfValues.add("\(dictData.value(forKey: "agenda")!)")
                
                aryOfValues.add("\(dictData.value(forKey: "stateId")!)")
                
                aryOfValues.add("\(dictData.value(forKey: "createdDate")!)")
                aryOfValues.add("\(dictData.value(forKey: "refId")!)")
                aryOfValues.add("\(dictData.value(forKey: "createdBy")!)")
                aryOfValues.add("\(dictData.value(forKey: "zipcode")!)")
                aryOfValues.add("\(dictData.value(forKey: "modifiedDate")!)")
                aryOfValues.add("") // id
                aryOfValues.add(NSArray()) //ActivityLogTypeMasterExtDc
                aryOfValues.add("\(dictData.value(forKey: "participants")!)")
                aryOfValues.add("\(dictData.value(forKey: "activityId")!)")
                aryOfValues.add("\(dictData.value(forKey: "addressLine1")!)")
                aryOfValues.add(NSArray())//LeadExtDcs
                aryOfValues.add("\(strEmpID)")//modifiedBy
                aryOfValues.add("\(dictData.value(forKey: "fromDate")!)")
                aryOfValues.add("\(dictData.value(forKey: "refType")!)")
                
                let dictJson = NSDictionary.init(objects: aryOfValues as! [Any], forKeys: aryOfKeys as! [NSCopying])
                
                aryJsonToPost.add(dictJson)
            }
            
            
            var jsonString = String()
            
            if(JSONSerialization.isValidJSONObject(aryJsonToPost) == true)
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: aryJsonToPost, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                
                print(jsonString)
                
            }
            
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strHrmsCompanyId = "\(dictLoginData.value(forKeyPath: "Company.HrmsCompanyId")!)"
            
            let strEmployeeNumber = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
            let strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
            let strCreatedBy = "\(dictLoginData.value(forKey: "CreatedBy")!)"
            let strEmployeeName = "\(dictLoginData.value(forKey: "EmployeeName")!)"
            let strCoreCompanyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId")!)"
            let strSalesProcessCompanyId1 = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
            let strCompanyKeyyy = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
            let strUrlSaved = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
            
            
            let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            
            let strUrl = strUrlSaved + UrlAddUpdateActivityMultiple
            let url = URL(string: strUrl)
            
            let request11 = NSMutableURLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
            
            request11.httpMethod = "POST"
            request11.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request11.addValue("application/json", forHTTPHeaderField: "Accept")
            
            request11.addValue("application/json", forHTTPHeaderField: "Accept")
            request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request11.addValue("IOS", forHTTPHeaderField: "Browser")
            
            request11.addValue(strHrmsCompanyId, forHTTPHeaderField: "HrmsCompanyId")
            
            request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "IpAddress")
            
            request11.addValue(strEmployeeNumber, forHTTPHeaderField: "EmployeeNumber")
            
            request11.addValue(strEmployeeId, forHTTPHeaderField: "EmployeeId")
            
            request11.addValue(strCreatedBy, forHTTPHeaderField: "CreatedBy")
            
            request11.addValue(strEmployeeName, forHTTPHeaderField: "EmployeeName")
            
            request11.addValue(strCoreCompanyId, forHTTPHeaderField: "CoreCompanyId")
            
            request11.addValue(strSalesProcessCompanyId1, forHTTPHeaderField: "SalesProcessCompanyId")
            
            request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
            
            request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
            
            request11.setValue("\(requestData!.count)", forHTTPHeaderField: "Content-Length")
            
            request11.httpBody = requestData
            
            URLSession.shared.dataTask(with: request11 as URLRequest) { (data, response, error) in
                
                if(error == nil)
                {
                    print("Background Syncing Success Activity")
                    
                    if(isInternetAvailable() == true)
                    {
                        self.callAPIToGetActivityList()
                    }
                    else
                    {
                        self.fetchTaskListFromLocalDB()
                        self.fetchActivityListFromLocalDB()
                    }
                }
                }.resume()
        }
        else
        {
            if(isInternetAvailable() == true)
            {
                self.callAPIToGetActivityList()
            }
            else
            {
                self.fetchTaskListFromLocalDB()
                self.fetchActivityListFromLocalDB()
            }
        }
    }
    
    func callAllAPISynchronously()
    {
        chkCalledApiSynchro = true

        
        if(isInternetAvailable() == false)
        {
           /* fetchAllNotesFromCoreData()
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()*/
        }
        else
        {
            
            //FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {

                self.getBasicInfoWebLead()
                //self.setInitialValues()

            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
            {
                //self.syncTask()
                self.getAllTask()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
            {
               // self.syncActivity()
                self.callApiToGetContacts()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8)
            {
               // self.syncActivity()
                //self.callApiToGetCompanies()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.callAPToGetTimeLine()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)
            {
                self.getAllNotes()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
            {
                self.getAllDocuments()
                
                self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                    
                    //all asynchronous tasks added to this DispatchGroup are completed. Proceed as required.
                    FTIndicator.dismissProgress()
                    
                })
                
                //Loader Dismiss
                self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                    
                  self.loader.dismiss(animated: false)
                             {
                  }
                    
                })
            }
        }
        
    }
    func callAllAPISynchronouslyOld()
    {
        if(isInternetAvailable() == false)
        {
           /* fetchAllNotesFromCoreData()
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()*/
        }
        else
        {
            
            FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {

                self.getBasicInfoWebLead()
                //self.setInitialValues()

            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
            {
                //self.syncTask()
                self.getAllTask()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
            {
               // self.syncActivity()
                self.callApiToGetContacts()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8)
            {
               // self.syncActivity()
                //self.callApiToGetCompanies()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.callAPToGetTimeLine()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)
            {
                self.getAllNotes()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
            {
                self.getAllDocuments()
                
                self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                    
                    //all asynchronous tasks added to this DispatchGroup are completed. Proceed as required.
                    FTIndicator.dismissProgress()
                    
                })
            }
        }
        
    }
    func showPopUpForCreateNewAccount()
    {
        let alertController = UIAlertController(title: "Alert", message: "No matching Accounts found. Do you want to Create New Opportunity", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
            
            NSLog("OK Pressed")
        }
        let okAction = UIAlertAction(title: "Yes-Create", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            //let dictMutable = getMutableDictionaryFromNSManagedObject(obj: self.dictLeadData) as NSDictionary
            
            let dictMutable = self.dictLeadData as NSDictionary
            self.gotoConvertLead(strTypeToConvert: "New", dictLeadDetail: dictMutable)
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func createViewMatchingAccounts()
    {
        //View Maching Accounts
        self.tblViewMatchingAccounts.reloadData()
        self.view_MatchingAccounts.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view.addSubview(self.view_MatchingAccounts)
        self.tblViewMatchingAccounts.reloadData()
        
        selectedMatchingAccounts = -1
        isCreateNewLeads = true
        tblViewMatchingAccounts.reloadData()
        
        //var path = NSIndexPath()
        //path = NSIndexPath(row: arrOfMatchingAccounts.count-1 , section: 0)
        //tblViewMatchingAccounts.scrollToRow(at: path as IndexPath, at: UITableView.ScrollPosition.top, animated: false)
    }
    @objc func isCreateNewLead()
    {
        if isCreateNewLeads == true
        {
            isCreateNewLeads = false
        }
        else
        {
            isCreateNewLeads = true
        }
        selectedMatchingAccounts = -1
        tblViewMatchingAccounts.reloadData()
        
        var path = NSIndexPath()
        //path = NSIndexPath(row: arrOfMatchingAccounts.count - 1, section: 0)
        path = NSIndexPath(row: 0, section: 0)

        
        tblViewMatchingAccounts.scrollToRow(at: path as IndexPath, at: UITableView.ScrollPosition.top, animated: false)
    }
    @objc func clickOnStatus(sender : UIButton)
    {
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
   alert.view.tintColor = UIColor.black
        if "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("New") == .orderedSame
        {
                    
            alert.addAction(UIAlertAction(title: "Convert To Opportunity", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnConvertToOpportunity(sender: UIButton())
                
            }))
            
            alert.addAction(UIAlertAction(title: "Mark as Dead", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnMarkAsDead(sender: UIButton())
                
            }))
            
            alert.addAction(UIAlertAction(title: "Grab", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnGrab(sender: UIButton())
                
            }))
            
        }
        else if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Grabbed") == .orderedSame
        {
                    
            alert.addAction(UIAlertAction(title: "Convert To Opportunity", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnConvertToOpportunity(sender: UIButton())
                
            }))
            
            alert.addAction(UIAlertAction(title: "Mark as Dead", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnMarkAsDead(sender: UIButton())
                
            }))
            
            alert.addAction(UIAlertAction(title: "Release", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnRelease(sender: UIButton())
                
            }))
            
            alert.addAction(UIAlertAction(title: "Create FollowUp", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnCreateFollowUp(sender: UIButton())
                
            }))
            
        }
        else if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("FollowUp") == .orderedSame
        {
            alert.addAction(UIAlertAction(title: "Convert To Opportunity", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnConvertToOpportunity(sender: UIButton())
                
            }))
            
            alert.addAction(UIAlertAction(title: "Mark as Dead", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnMarkAsDead(sender: UIButton())
                
            }))
            
            alert.addAction(UIAlertAction(title: "Release", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnRelease(sender: UIButton())
                
            }))
        }
        else if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Dead") == .orderedSame
        {
            alert.addAction(UIAlertAction(title: "Convert To Opportunity", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnConvertToOpportunity(sender: UIButton())
                
            }))
            
            alert.addAction(UIAlertAction(title: "Grab", style: .default , handler:{ (UIAlertAction)in
                
                self.actionOnGrab(sender: UIButton())
                
            }))
           
        }
        else if  "\(dictLeadData.value(forKey: "StatusName") ?? "")".caseInsensitiveCompare("Converted") == .orderedSame
        {
            
            
        }
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
         alert.view.tintColor = UIColor.black
                 
                 if let popoverController = alert.popoverPresentationController {
                     popoverController.sourceView = sender as UIView
                     popoverController.sourceRect = sender.bounds
                   //  popoverController.permittedArrowDirections = UIPopoverArrowDirection.to
                 }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    func getBranchNameFromSysName(strSysName: String) -> String
    {
        var strName = ""
        var dictSalesLeadMaster = NSDictionary()
        
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary
        {
             dictSalesLeadMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            var arrBranchMaster = NSArray()
            arrBranchMaster = dictSalesLeadMaster.value(forKey: "BranchMasters") as! NSArray
            
            for item in arrBranchMaster
            {
                let dict = item as! NSDictionary
                
                if "\(dict.value(forKey: "SysName") ?? "")" == strSysName
                {
                    strName = "\(dict.value(forKey: "Name") ?? "")"
                    
                    break
                }
            }
            
        }
        return strName
        
    }
    
    func goToContactList()
    {
        
       let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func goToDasboard()
    {
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func goToAppointment()
    {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
//        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboardIpad.instantiateViewController(withIdentifier: "AppointmentView") as! AppointmentView
//        self.navigationController?.pushViewController(vc, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
        
    }
    func goToSignedViewAgreement()
    {
        if(DeviceType.IS_IPAD){
                           let mainStoryboard = UIStoryboard(
                               name: "CRMiPad",
                               bundle: nil)
                           let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
                           self.navigationController?.present(objByProductVC!, animated: true)
                       }else{
                        let storyboardIpad = UIStoryboard.init(name: "CRM", bundle: nil)
                                let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as! SignedAgreementiPhoneVC
                                
                                self.navigationController?.present(vc, animated: false, completion: nil)
                       }
        //nsud.set(true, forKey: "fromCompanyVC")
       // nsud.synchronize()
     
        
    }
    func goToNearBy()
    {
        /*
          if(DeviceType.IS_IPAD){
                              let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                              let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
                              self.navigationController?.pushViewController(vc!, animated: false)
                          }else{
                              let storyboard = UIStoryboard(name: "Main", bundle: nil)
                              let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
                              self.navigationController?.pushViewController(vc!, animated: false)
                          }*/
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)

    }
    func getEmployeeNameFromId(strEmpId: String) -> String
    {
        var strName = ""
        
        if nsud.value(forKey: "EmployeeList") is NSArray
        {
            let arrEmployeeList = nsud.value(forKey: "EmployeeList") as! NSArray
            for item in arrEmployeeList
            {
                let dict = item as! NSDictionary
                if "\(dict.value(forKey: "EmployeeId") ?? "")" == strEmpId
                {
                    strName = "\(dict.value(forKey: "FullName") ?? "")"
                    break
                }
            }
        }
        if strName.count == 0
        {
            strName = ""
        }
        
        return strName
        
    }
    
    func getAllDocuments()
    {
        self.arrAllNotes = NSMutableArray()
        
        if !isInternetAvailable()
        {
            //fetchAllNotesFromCoreData()
        }
        else
        {
            strReftype = "WebLead"
            strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "leadId") ?? "")"

            let UrlGetAllDocuments = "/api/LeadNowAppToSalesProcess/GetDocumentsByRefIdAndRefType?refId="
            
            //140176&reftype=Lead
            var strUrl = "\(strServiceUrlMain)\(UrlGetAllDocuments)\(strRefId)&reftype=\(strReftype)"  as String
            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            //FTIndicator.showProgress(withMessage: "Fetching Documents...", userInteractionEnable: false)
            self.dispatchGroup.enter()
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
                self.dispatchGroup.leave()
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    nsud.setValue("", forKey: "fromAddNotes")
                    nsud.synchronize()
                    
                }
                if(Status)
                {
                    let arrData = Response["data"] as! NSArray
                    
                    if arrData.isKind(of: NSArray.self)
                    {
                        let arrAllData = NSMutableArray()
                        
                        for item in arrData
                        {
                            let  dict = item as! NSDictionary
                            
                            let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))
                            
                            arrAllData.add(dictNew as Any)
                            
                            
                        }
                        //self.arrAllDocuments = arrAllData
                        let str = getJson(from: arrAllData)
                        
                        print("\(str ?? "")" as Any)
                        
                        self.arrAllDocuments = arrAllData.mutableCopy() as! NSMutableArray
                        self.tblviewLeadDetails.reloadData()
                      
                        
                    }
                    else
                    {
                        
                      
                        
                    }
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                    
                }
                else
                {
                    
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    func goToPdfView(strPdfName : String)
    {
        
       /* let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
         let  strPdfNameNew = strPdfName.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
        guard let url = URL(string: "\(strPdfNameNew)") else { return }
        UIApplication.shared.open(url)
        
    }
    func playAudioo(strAudioName : String) {
       
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    func goToContactDetails(dictContact : NSDictionary){
       
        if "\(dictContact.value(forKey: "CrmContactId")!)" == "" || "\(dictContact.value(forKey: "CrmContactId")!)".count == 0 {

            
            
        } else {
            
             let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as! ContactDetailsVC_CRMContactNew_iPhone
            controller.strCRMContactIdGlobal = "\(dictContact.value(forKey: "CrmContactId")!)"
            controller.dictContactDetailFromList = dictContact
            self.navigationController?.pushViewController(controller, animated: false)
            
        }
        
    }
    func goToCompanyEdit()
    {
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
        controller.strCRMCompanyIdGlobal = "\(dictLeadData.value(forKeyPath: "CrmCompany.CrmCompanyId")!)"
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func goToEmailDetails(dictEmail : NSDictionary){
       
        let controller = storyboardNewCRMContact.instantiateViewController(withIdentifier: "EmailDetail_iPhoneVC") as? EmailDetail_iPhoneVC
        controller?.dictEmailContain = dictEmail
        self.navigationController?.present(controller!, animated: false, completion: nil)
        
    }
    
    func goToTaskDetails(strId : String){
       
       let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
        vc.taskId = "\(strId)"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToActivityDetails(strId : String){
       
          let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityDetailsVC_CRMNew_iPhone") as! ActivityDetailsVC_CRMNew_iPhone
            
        vc.activityID = "\(strId)"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func clearNsud()
    {
        nsud.setValue("", forKey: "cureentTab")
        nsud.setValue("", forKey: "cureentSwipeCount")
        nsud.synchronize()
    }
    func serviceNameFromId()
    {
        
        let arrServiceName = NSMutableArray()
        let arrServiceId = NSMutableArray()
        
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if dictMaster.value(forKey: "ServiceMasters") is NSArray
            {
                let arrService = dictMaster.value(forKey: "ServiceMasters") as! NSArray
                
                if arrService.count > 0
                {
                    for itemService in arrService
                    {
                        let dictService = itemService as! NSDictionary
                        
                        arrServiceName.add("\(dictService.value(forKey: "Name") ?? "")")
                        arrServiceId.add("\(dictService.value(forKey: "ServiceId") ?? "")")
                    }
                }
            }
        }
        dictServiceNameFromId = NSDictionary.init(objects: arrServiceName as! [Any], forKeys: arrServiceId as! [NSCopying])

    }
    // MARK: -  ------------------------------ Core Data Functions  ------------------------------
    
    func saveNotesToCoreData(arrAllData: NSMutableArray)
    {
        deleteAllRecordsFromDB(strEntity: "TotalNotesCRM", predicate: NSPredicate(format: "userName=%@ && leadNumber=%@", strUserName,"\(dictLeadData.value(forKey: "leadNumber") ?? "")"))
        
        for item in arrAllData
        {
            let dict = item as! NSDictionary
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("createdByName")
            arrOfKeys.add("leadNoteId")
            arrOfKeys.add("leadNumber")
            arrOfKeys.add("note")
            arrOfKeys.add("opportunityNumber")
            arrOfKeys.add("userName")
            
            arrOfValues.add("\(dict.value(forKey: "ClientCreatedDate") ?? "")")//clientCreatedDate
            arrOfValues.add("\(strCompanyKey)")//companyKey
            arrOfValues.add("\(strUserName)")//createdByName
            
            arrOfValues.add("\(dict.value(forKey: "LeadNoteId") ?? "")")//leadNoteId
            arrOfValues.add("\(dict.value(forKey: "LeadNumber") ?? "")")//leadNumber
            arrOfValues.add("\(dict.value(forKey: "Note") ?? "")")//note
            arrOfValues.add("\(dict.value(forKey: "OpportunityNumber") ?? "")")//opportunityNumber
            arrOfValues.add("\(strUserName)")//userName
            
            saveDataInDB(strEntity: "TotalNotesCRM", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        
    }
    func fetchAllNotesFromCoreData()
    {
        //ServiceAddressPOCDetailDcs
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalNotesCRM", predicate: NSPredicate(format: "userName == %@ && leadNumber == %@", strUserName,"\(dictLeadData.value(forKey: "leadNumber") ?? "")"))
        
        arrAllNotes = arryOfData.mutableCopy() as! NSMutableArray
        
        tblviewLeadDetails.reloadData()
        
    
    }
    
    func saveTaskListToLocalDb(data:NSArray)
    {
        //deleteAllRecordsFromDB(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "refType =="))
        //  deleteAllRecordsFromDB(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "refType == %@",strTypeOfTaskToFetch))
        
        deleteAllRecordsFromDB(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "refId == %@ && userName == %@ && refType == %@",strRefId,strUserName,strReftype))
        
        //temp
        
        /* let context = getContext()
         let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\("TaskListCRMNew")")
         let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
         
         do {
         try context.execute(deleteRequest)
         try context.save()
         
         } catch {
         print ("There was an error")
         }*/
        //End
        
        for item in data
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            let dictData = removeNullFromDict(dict: (item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            arrOfKeys.add("userName")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("accountNo")
            arrOfKeys.add("addressLine1")
            arrOfKeys.add("addressLine2")
            arrOfKeys.add("assignedByStr")
            arrOfKeys.add("assignedTo")
            arrOfKeys.add("assignedToStr")
            arrOfKeys.add("cellPhone1")
            arrOfKeys.add("cellPhone2")
            arrOfKeys.add("childActivities")
            arrOfKeys.add("cityName")
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("clientModifiedDate")
            arrOfKeys.add("companyName")
            arrOfKeys.add("countryId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("dueDate")
            arrOfKeys.add("dueTime")
            arrOfKeys.add("firstName")
            arrOfKeys.add("followUpFromTaskId")
            arrOfKeys.add("lastName")
            arrOfKeys.add("leadNo")
            arrOfKeys.add("leadTaskId")
            arrOfKeys.add("middleName")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("primaryEmail")
            arrOfKeys.add("primaryPhone")
            arrOfKeys.add("primaryPhoneExt")
            arrOfKeys.add("priority")
            arrOfKeys.add("priorityStr")
            arrOfKeys.add("refId")
            arrOfKeys.add("refType")
            arrOfKeys.add("reminderDate")
            arrOfKeys.add("secondaryEmail")
            arrOfKeys.add("secondaryPhone")
            arrOfKeys.add("secondaryPhoneExt")
            arrOfKeys.add("stateId")
            arrOfKeys.add("status")
            arrOfKeys.add("tags")
            arrOfKeys.add("taskDescription")
            arrOfKeys.add("taskName")
            arrOfKeys.add("webLeadNo")
            arrOfKeys.add("zipcode")
            
            arrOfKeys.add("compareDate")
            
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add("\(dictData.value(forKey: "AccountNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "AddressLine1")!)")
            arrOfValues.add("\(dictData.value(forKey: "AddressLine2")!)")
            arrOfValues.add("\(dictData.value(forKey: "AssignedByStr")!)")
            arrOfValues.add("\(dictData.value(forKey: "AssignedTo")!)")
            arrOfValues.add("\(dictData.value(forKey: "AssignedToStr")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone1")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone2")!)")
            
            if (dictData.value(forKey: "ChildActivities")) is NSArray
            {
                if((dictData.value(forKey: "ChildActivities") as! NSArray).count > 0)
                {
                    let aryChild = NSMutableArray()
                    let aryTemp = (dictData.value(forKey: "ChildActivities") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    for itemTemp in aryTemp
                    {
                        aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
                    }
                    arrOfValues.add(aryChild)
                }
                else
                {
                    arrOfValues.add(NSMutableArray())
                }
            }
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            arrOfValues.add("\(dictData.value(forKey: "CityName")!)")
            
            if("\(dictData.value(forKey: "ClientCreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientCreatedDate")!)" != "")
            {
                //                arrOfValues.add(Global().convertDate("\(dictData.value(forKey: "ClientCreatedDate")!)"))
                
                arrOfValues.add("\(dictData.value(forKey: "ClientCreatedDate")!)")
                
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "ClientModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientModifiedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ClientModifiedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "CompanyName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CountryId")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CreatedBy")!)")
            
            if("\(dictData.value(forKey: "CreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "CreatedDate")!)" != "")
            {
                
                arrOfValues.add("\(dictData.value(forKey: "CreatedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "DueDate")!)" != "<null>" && "\(dictData.value(forKey: "DueDate")!)" != "")
            {
                arrOfValues.add(Global().convertDate("\(dictData.value(forKey: "DueDate")!)"))
                // arrOfValues.add("\(dictData.value(forKey: "DueDate")!)")
                arrOfValues.add(Global().convertTime("\(dictData.value(forKey: "DueDate")!)"))
            }
            else
            {
                arrOfValues.add("")
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "FirstName")!)")
            arrOfValues.add("\(dictData.value(forKey: "FollowUpFromTaskId")!)")
            arrOfValues.add("\(dictData.value(forKey: "LastName")!)")
            arrOfValues.add("\(dictData.value(forKey: "LeadNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "LeadTaskId")!)")
            arrOfValues.add("\(dictData.value(forKey: "MiddleName")!)")
            arrOfValues.add("\(dictData.value(forKey: "ModifiedBy")!)")
            
            if("\(dictData.value(forKey: "ModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ModifiedDate")!)" != "")
            {
                // arrOfValues.add(Global().convertDate("\(dictData.value(forKey: "ModifiedDate")!)"))
                
                arrOfValues.add("\(dictData.value(forKey: "ModifiedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "PrimaryEmail")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhone")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhoneExt")!)")
            arrOfValues.add("\(dictData.value(forKey: "Priority")!)")
            arrOfValues.add("\(dictData.value(forKey: "PriorityStr")!)")
            arrOfValues.add("\(dictData.value(forKey: "RefId")!)")
            arrOfValues.add("\(dictData.value(forKey: "RefType")!)")
            
            if("\(dictData.value(forKey: "ReminderDate")!)" != "<null>" && "\(dictData.value(forKey: "ReminderDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ReminderDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "SecondaryEmail")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhone")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhoneExt")!)")
            arrOfValues.add("\(dictData.value(forKey: "StateId")!)")
            arrOfValues.add("\(dictData.value(forKey: "Status")!)")
            arrOfValues.add("\(dictData.value(forKey: "Tags")!)")
            arrOfValues.add("\(dictData.value(forKey: "TaskDescription")!)")
            arrOfValues.add("\(dictData.value(forKey: "TaskName")!)")
            arrOfValues.add("\(dictData.value(forKey: "WebLeadNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "Zipcode")!)")
            
            let dateString = Global().convertDate("\(dictData.value(forKey: "DueDate")!)")
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "MM/dd/yyyy"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            let compareDate = Global().getOnlyDate(dateFormatter.date(from: dateString!))
            
            arrOfValues.add(compareDate!)
            
            saveDataInDB(strEntity: "TaskListCRMNew", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        fetchTaskListFromLocalDB()
    }
    func fetchTaskListFromLocalDB()
    {
        
        let arrayTemp = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND  refId == %@ AND refType == %@",strUserName, "\(strRefId)", "\(strReftype)"))
        
        if(arrayTemp.count > 0)
        {
            aryTasks.removeAllObjects()
            
            for item in arrayTemp
            {
                aryTasks.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
                
            }
            print(arrayTemp)
            
            
            //tblviewTask.reloadData()
            tblviewLeadDetails.reloadData()
            
        }
        else
        {
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
        }
    }
    func updateTaskToLocalDB(dict:NSMutableDictionary)
    {
        
        let arrayAllObject = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "leadTaskId == %@", "\(dict.value(forKey: "leadTaskId")!)"))
        
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("status")
        
        
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strEmpID)
        arrOfValues.add(Global().modifyDate())
        
        if("\(dict.value(forKey: "status")!)" == "Open")
        {
            arrOfValues.add("Done")
        }
        else
        {
            arrOfValues.add("Open")
        }
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "leadTaskId == %@", "\(dict.value(forKey: "leadTaskId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            fetchTaskListFromLocalDB()
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
        }
        
    }
    
    fileprivate func saveActivityListToLocalDb(data:NSArray)
    {
        //  deleteAllRecordsFromDB(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "refType =="))
        //   deleteAllRecordsFromDB(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "refType == %@",strTypeOfActivityToFetch))
        
        deleteAllRecordsFromDB(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "refId == %@ && userName == %@ && refType == %@",strRefId,strUserName,strReftype))
        
        
        //temp
        
        /* let context = getContext()
         let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\("ActivityListCRMNew")")
         let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
         
         do {
         try context.execute(deleteRequest)
         try context.save()
         
         } catch {
         print ("There was an error")
         }*/
        //End
        
        for item in data
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            let dictData = removeNullFromDict(dict: (item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            
            arrOfKeys.add("userName")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("accountNo")
            arrOfKeys.add("activityId")
            arrOfKeys.add("agenda")
            arrOfKeys.add("logTypeId")
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("toDate")
            arrOfKeys.add("fromDate")
            arrOfKeys.add("participants")
            arrOfKeys.add("activityTime")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("leadNo")
            arrOfKeys.add("webLeadNo")
            arrOfKeys.add("firstName")
            arrOfKeys.add("middleName")
            arrOfKeys.add("lastName")
            arrOfKeys.add("cellPhone1")
            arrOfKeys.add("cellPhone2")
            arrOfKeys.add("primaryPhone")
            arrOfKeys.add("primaryPhoneExt")
            arrOfKeys.add("secondaryPhone")
            arrOfKeys.add("secondaryPhoneExt")
            arrOfKeys.add("primaryEmail")
            arrOfKeys.add("secondaryEmail")
            arrOfKeys.add("addressLine1")
            arrOfKeys.add("addressLine2")
            arrOfKeys.add("cityName")
            arrOfKeys.add("stateId")
            arrOfKeys.add("zipcode")
            arrOfKeys.add("countryId")
            arrOfKeys.add("companyName")
            arrOfKeys.add("refId")
            arrOfKeys.add("refType")
            arrOfKeys.add("activityComments")
            arrOfKeys.add("childTasks")
            arrOfKeys.add("compareDate")
            arrOfKeys.add("isSystem")
            
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add("\(dictData.value(forKey: "AccountNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "ActivityId")!)")
            arrOfValues.add("\(dictData.value(forKey: "Agenda")!)")
            arrOfValues.add("\(dictData.value(forKey: "LogTypeId")!)")
            
            if("\(dictData.value(forKey: "ClientCreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientCreatedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ClientCreatedDate")!)")
                
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "ToDate")!)" != "<null>" && "\(dictData.value(forKey: "ToDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ToDate")!)")
                
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "FromDate")!)" != "<null>" && "\(dictData.value(forKey: "FromDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "FromDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "Participants")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "ActivityTime")!)")
            
            if("\(dictData.value(forKey: "CreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "CreatedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "CreatedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "ModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ModifiedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ModifiedDate")!)")
                
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add(strEmpID) // created by
            arrOfValues.add("\(dictData.value(forKey: "LeadNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "WebLeadNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "FirstName")!)")
            arrOfValues.add("\(dictData.value(forKey: "MiddleName")!)")
            arrOfValues.add("\(dictData.value(forKey: "LastName")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone1")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone2")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhone")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhoneExt")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhone")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhoneExt")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryEmail")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryEmail")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "AddressLine1")!)")
            arrOfValues.add("AddressLine2") // address line2
            arrOfValues.add("\(dictData.value(forKey: "CityName")!)")
            arrOfValues.add("\(dictData.value(forKey: "StateId")!)")
            arrOfValues.add("\(dictData.value(forKey: "Zipcode")!)")
            arrOfValues.add("\(dictData.value(forKey: "CountryId")!)")
            arrOfValues.add("\(dictData.value(forKey: "CompanyName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "RefId")!)")// refId
            arrOfValues.add("\(dictData.value(forKey: "RefType")!)") // refType
            
            if var aryTemp = dictData.value(forKey: "ActivityComments") as? NSArray
            {
                
                let aryChild = NSMutableArray()
                aryTemp = (dictData.value(forKey: "ActivityComments") as! NSArray).mutableCopy() as! NSMutableArray
                
                for itemTemp in aryTemp
                {
                    aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
                }
                arrOfValues.add(aryChild)
                
            }
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            if var aryTemp = dictData.value(forKey: "ChildTasks") as? NSArray
            {
                let aryChild = NSMutableArray()
                aryTemp = (dictData.value(forKey: "ChildTasks") as! NSArray).mutableCopy() as! NSMutableArray
                
                for itemTemp in aryTemp
                {
                    aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
                }
                arrOfValues.add(aryChild)
                
            }
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            if("\(dictData.value(forKey: "ModifiedDate")!)" == "" || "\(dictData.value(forKey: "ModifiedDate")!)" == "<null>")
            {
                // do nothing
            }
                
            else
            {
                let dateString = Global().convertDate("\(dictData.value(forKey: "ModifiedDate")!)")
                
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                dateFormatter.dateFormat = "MM/dd/yyyy"
                
                let compareDate = Global().getOnlyDate(dateFormatter.date(from: dateString!))
                
                arrOfValues.add(compareDate!)
                
                if(dictData.value(forKey: "IsSystem") as! Bool == true)
                {
                    arrOfValues.add(true)
                }
                else
                {
                    arrOfValues.add(false)
                }
                
                saveDataInDB(strEntity: "ActivityListCRMNew", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            
        }
        
        fetchActivityListFromLocalDB()
    }
    fileprivate func fetchActivityListFromLocalDB()
    {
        
        aryActivities.removeAllObjects()
        
        // let arrayTemp = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND refType == %@",strUserName,"WebLead"))
        
        let arrayTemp = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND refId == %@ AND refType == %@",strUserName, "\(strRefId)", "\(strReftype)"))
        
        if(arrayTemp.count > 0)
        {
            for item in arrayTemp
            {
                aryActivities.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
                
            }
            print(arrayTemp)
            //tblviewLeadDetails.reloadData()
            
            for item in aryTasks
            {
                let dict  = item as! NSDictionary
                
                if "\(dict.value(forKey: "leadTaskId") ?? "")".count == 0
                {
                    if((dict.value(forKey: "childActivities") as! NSArray).count > 0)
                    {
                        //aryActivities.removeAllObjects()
                        for innerItem in dict.value(forKey: "childActivities") as! NSArray
                        {
                            aryActivities.add(innerItem as! NSDictionary)
                            
                        }
                    }
                }
                
            }
            tblviewLeadDetails.reloadData()
            
        }
        else
        {
            for item in aryTasks
            {
                let dict  = item as! NSDictionary
                
                if((dict.value(forKey: "childActivities") as! NSArray).count > 0)
                {
                    aryActivities.removeAllObjects()
                    for innerItem in dict.value(forKey: "childActivities") as! NSArray
                    {
                        aryActivities.add(innerItem as! NSDictionary)
                        
                    }
                }
            }
            tblviewLeadDetails.reloadData()
        }
    }
    
    
    // MARK: -  ------------------------------ Message Composer Delegate ------------------------------
    
    func displayMessageInterface(strNo: String)
    {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = ["\(strNo)"]
        composeVC.body = ""
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
       
        // dismiss(animated: false, completion: nil)
        dismiss(animated: false) {
            
            if result == .sent || result == .failed{
                
                self.goToLogAsActivity()
                
            }
            
        }
       
    }
    
    // MARK: -  ------------------------------ Send Mail Delegate ------------------------------
    
    func sendEmail(strEmail: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(strEmail)"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    //        controller.dismiss(animated: true)

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
       
        // dismiss(animated: false, completion: nil)
        dismiss(animated: false) {
            
            if result == .sent || result == .failed{
                
                self.goToLogAsActivity()
                
            }
            
        }
       
    }
    
}
extension WebLeadDetailsVC_CRMNew_iPhone : FilterTimeLineData
{
    
    func getFilterTimeLineDataOnSelection(dictData: NSMutableDictionary, strType: String) {
        
                    
        if(isInternetAvailable() == false)
        {

            tblviewLeadDetails.reloadData()
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
           
            FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
            
//            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
//            self.present(loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                
                self.callAPToGetTimeLine()
                
                //Loader Dismiss
//                self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
//
//                    self.loader.dismiss(animated: false)
//                    {
//                    }
//
//                })
                
            }
           
            
        }
        
    }
    
    
}

extension WebLeadDetailsVC_CRMNew_iPhone : CXCallObserverDelegate{
   func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
       if call.hasConnected {
              print("Call Connect -> \(call.uuid)")
          }

          if call.isOutgoing {
              print("Call outGoing \(call.uuid)")
          }

          if call.hasEnded {
               self.goToLogAsActivity()
              print("Call hasEnded \(call.uuid)")
          }

          if call.isOnHold {
              print("Call onHold \(call.uuid)")
            }
        }
      
}
