//
//  LeadVC.swift
//  DPS
//
//  Created by Akshay Hastekar on 20/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class OpportunityVC: UIViewController
{
    
    
    weak var delegateTaskAssociate: AddTaskAssociateProtocol?
    weak var delegateActivityAssociate: AddActivityAssociateProtocol?
    var selectionTag = Int()
    var strSearchText = ""

    // MARK: -  ----------------------------------- Outlet  -----------------------------------
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    @IBOutlet weak var scrollViewLeads: UIScrollView!
    
    
    @IBOutlet weak var tblViewLeads: UITableView!
    
    //New Change
    @IBOutlet weak var btnWebLead: UIButton!
    @IBOutlet weak var btnOpportunity: UIButton!
    @IBOutlet weak var viewTopButton: UIView!
    @IBOutlet weak var const_BtnBack_W: NSLayoutConstraint!
    @IBOutlet weak var btnOpportunityNew: UIButton!
    @IBOutlet weak var const_Footer_H: NSLayoutConstraint!
    @IBOutlet weak var const_HomeButton_L: NSLayoutConstraint!
    
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    @IBOutlet weak var headerView: TopView!

    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    var strTypeOfAssociations = String()
    
    let global = Global()
    var loader = UIAlertController()
    // MARK: -  ----------------------------------- Variable Declaration -----------------------------------
    
    // MARK: -  --- String ---
    
    var strFromDate = String()
    var strToDate = String()
    var strCurrentTab = String()
    // MARK: -  --- Array ---
    
    var arrOfLeads = NSArray()
    var arrOfLeadsFilter = NSArray()
    var arrOfData = NSArray()
    var arrOfFilterData = NSArray()
    
    
    var arrAllOpportunityLeads = NSMutableArray()
    var arrAllOpportunityLeadsData = NSMutableArray()
    var arrAllOpportunityCount = NSMutableArray()
    var arrOfStatus = NSMutableArray()
    var arrOfStatusName = NSMutableArray()
    var arrOfUrgency = NSMutableArray()

    
    // MARK: -  --- Dictionary ---
    
    var dictLoginData = NSDictionary()
    
    
    // MARK: -  --- Bool ---
    
    var yesFiltered = Bool()
    
    // MARK: -  --- Int ---
    
    var swipeCount = Int()
    var currentSwipeCount = Int()
    @IBOutlet weak var lbltaskCount: UILabel!
        @IBOutlet weak var lblScheduleCount: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                lblScheduleCount.text = "0"
                lbltaskCount.layer.cornerRadius = 18.0
                lbltaskCount.backgroundColor = UIColor.red
                lblScheduleCount.layer.cornerRadius = 18.0
                lblScheduleCount.backgroundColor = UIColor.red
                lbltaskCount.layer.masksToBounds = true
                lblScheduleCount.layer.masksToBounds = true
                
            }
            tblViewLeads.tableFooterView = UIView()
            tblViewLeads.estimatedRowHeight = 55.0
            
            if #available(iOS 13.0, *) {
                searchBar.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
            } else {
                // Fallback on earlier versions
            }
            
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
            swipeLeft.direction = .left
            self.tblViewLeads!.addGestureRecognizer(swipeLeft)
            
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
            swipeRight.direction = .right
            self.tblViewLeads!.addGestureRecognizer(swipeRight)
            
            
            swipeCount = 0
            currentSwipeCount = 0
            strCurrentTab = ""
                    
            if nsud.value(forKey: "LoginDetails") != nil && nsud.value(forKey: "LoginDetails") is NSDictionary
            {
                dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            }
            else
            {
                let alert = UIAlertController(title: Alert, message: alertReLaunchApp, preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in

                    nsud.set(false, forKey: "fromOpportunityAssociation")
                    nsud.synchronize()
                    self.navigationController?.popViewController(animated: false)

                }))

                alert.popoverPresentationController?.sourceView = self.view
                self.present(alert, animated: true, completion: {
                })
            }
            
            strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
            
            strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
            
            strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
            
            
            strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            
            strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            
            strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            
            
            yesFiltered = false
            arrOfLeads = ["New", "Complete", "Incomplete", "Scheduled", "Defered"]
            
            getStartDateEndDate()
            getStatus()
            addButton()
            
            
            let isForAssociation = nsud.bool(forKey: "fromOpportunityAssociation")
            //isForAssociation = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setTopMenuOption(tagForAssociation: isForAssociation)
            })
            if isForAssociation == true
            {
                const_BtnBack_W.constant = DeviceType.IS_IPAD ? 84 : 40
                btnOpportunityNew.isHidden = false
                btnWebLead.isHidden = true
                btnOpportunity.isHidden = true
                const_Footer_H.constant = 0
                const_HomeButton_L.constant = DeviceType.IS_IPAD ? 0 : 5
            }
            else
            {
                const_BtnBack_W.constant = 0
                btnOpportunityNew.isHidden = true
                btnWebLead.isHidden = false
                btnOpportunity.isHidden = false
                const_Footer_H.constant = DeviceType.IS_IPAD ? 115 : 85
                const_HomeButton_L.constant = DeviceType.IS_IPAD ? 0 : 15
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                    self.setFooterMenuOption()
                })
            }
            setUpView()
            
            // Setup constraints for layout
            /* childVC.view.translatesAutoresizingMaskIntoConstraints = false
             childVC.view.topAnchor.constraint(equalTo: self.scrollViewLeads.bottomAnchor).isActive = true
             childVC.view.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
             childVC.view.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
             childVC.view.heightAnchor.constraint(equalToConstant: 500).isActive = true*/
            
            // Do any additional setup after loading the view.
            
          //  createBadgeView()
        }
       
        
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        let isForAssociation = nsud.bool(forKey: "fromOpportunityAssociation")
        if isForAssociation == false
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setFooterMenuOption()
            })
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setTopMenuOption(tagForAssociation: isForAssociation)
        })
      }
    override func viewWillAppear(_ animated: Bool)
    {
        if !Check_Login_Session_expired() {
            if nsud.bool(forKey: "fromOpportunityDetail") == true
            {
                nsud.set(false, forKey: "fromOpportunityDetail")
                nsud.synchronize()
                
                /*if self.arrOfStatus.count > 0
                 {
                 //swipeCount = 0
                 //getStatus()
                 //addBorder()
                 
                 let strStatus = "\(self.arrOfStatus.object(at: 0))"
                 self.fetchOpportunityLeadsFromCoreData(strStatus: "\(strStatus)")
                 }
                 else
                 {
                 self.fetchOpportunityLeadsFromCoreData(strStatus: "New")
                 }*/
                
                if nsud.bool(forKey: "fromScheduledOpportunity") == true
                {
                    swipeCount = 0
                    getStatus()
                    //addBorder()
                    getAllOpportunityLeads()
                    //addBorder()
                    
                    //                if self.arrOfStatus.count > 0
                    //                {
                    //                    let strStatus = "\(self.arrOfStatus.object(at: 0))"
                    //                    self.fetchOpportunityLeadsFromCoreData(strStatus: "\(strStatus)")
                    //                }
                    //                else
                    //                {
                    //                    self.fetchOpportunityLeadsFromCoreData(strStatus: "New")
                    //                }
                    
                }
                else
                {
                    nsud.setValue("", forKey: "cureentTabOpportunity")
                    nsud.setValue("", forKey: "cureentSwipeCountOpportunity")
                    nsud.synchronize()
                }
                
                tblViewLeads.reloadData()
                
            }
            else
            {
                if nsud.bool(forKey: "fromOpportunityFilter") == true
                {
                    swipeCount = 0
                    //getStatus()
                    addButton()
                    getAllOpportunityLeads()
                    addButton()
                }
                else
                {
                    
                    swipeCount = 0
                    getStatus()
                    addButton()
                    getStartDateEndDate()
                    getAllOpportunityLeads()
                    addButton()
                }
                
            }
            nsud.set(false, forKey: "forEditOpportunity")
            nsud.set(false, forKey: "fromOpportunityFilter")
            nsud.set(false, forKey: "fromAddUpdateTask")
            nsud.set(false, forKey: "fromAddUpdateActivity")
            nsud.set(false, forKey: "fromScheduledOpportunity")
            
            nsud.synchronize()        }
        
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchBar.text = ""
        searchBar.text = nil
        searchBar.endEditing(true)
        self.view.endEditing(true)
    }
    // MARK: ---------------------------Footer Functions ---------------------------
    func setFooterMenuOption() {
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Sales", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
            self.view.endEditing(true)
            self.goToDasboard()
           

        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
            self.view.endEditing(true)
            self.goToAppointment()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.goToContactList()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
           
        }
       
    }

    // MARK: ---------------------------Header Functions ---------------------------
    func setTopMenuOption(tagForAssociation : Bool) {
        
        for view in self.view.subviews {
            if(view is HeaderView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue(tagForAssociation == true ? "OpportunityAssosi" : "Opportunity", forKey: "DPS_TopMenuOptionTitle")
        nsud.synchronize()
        var headerViewtop = HeaderView()
         headerViewtop = HeaderView.init(frame: CGRect(x: 0, y: 0, width: self.headerView.frame.size.width, height:self.headerView.frame.size.height))
        headerViewtop.searchBarTask.delegate = self
        if #available(iOS 13.0, *) {
            headerViewtop.searchBarTask.searchTextField.text = self.strSearchText
        } else {
            headerViewtop.searchBarTask.text = self.strSearchText
        }
        self.headerView.addSubview(headerViewtop)
        headerViewtop.onClickBackButton = {() -> Void in
            nsud.set(false, forKey: "fromOpportunityAssociation")
            nsud.synchronize()
            self.navigationController?.popViewController(animated: false)
        }
        headerViewtop.onClickFilterButton = {() -> Void in
            self.goToFilter()
        }
        headerViewtop.onClickAddButton = {() -> Void in
            self.goToAddOpportunity()
        }
        headerViewtop.onClickVoiceRecognizationButton = { () -> Void in
            
            self.view.endEditing(true)
            if self.arrAllOpportunityLeads.count != 0 {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.delegate = self
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: {})
            }
        }
        headerViewtop.onClickTopMenuButton = {(str) -> Void in
            print(str)
            switch str {
            case "Map":
                self.goToNearBy()
                break
            case "Lead":
                self.goToWebLead()
                break
            case "Opportunity":
                break
            case "Tasks":
                self.GotoTaskViewController()
                break
            case "Activity":
                self.GotoActivityViewController()
                break
            case "View Schedule":
                self.GotoViewScheduleViewController()
                break
            case "Signed Agreements":
                self.goToSignedViewAgreement()
                break
            default:
                break
            }
           
        }

    }
    
    func GotoViewScheduleViewController() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ScheduleD2D" : "ScheduleD2D", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoTaskViewController() {
        
         let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone

        testController.strFromVC = "DashBoardView"//"WebLeadVC"
        self.navigationController?.pushViewController(testController, animated: false)
    }
    
    func GotoActivityViewController()  {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
        //vc.strFromVC = strFromVC
        //nsud.setValue(nil, forKey: "dictFilterActivity")
        //nsud.synchronize()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    //MARK:
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.right {
            
            swipeCount = swipeCount - 1
            
            if (swipeCount < 0)
            {
                swipeCount = 0
            }
            print("Swipe count \(swipeCount)")
            addButton()
            let strStatus = "\(arrOfStatus.object(at: swipeCount))"
            fetchOpportunityLeadsFromCoreData(strStatus: strStatus)
            
            if swipeCount == 0
            {
                scrollViewLeads.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                
            }
            else
            {
                scrollViewLeads.setContentOffset(CGPoint(x: (swipeCount + 1)*100 - 200, y: 0), animated: true)
            }
            /* swipeCount = swipeCount + 1
             
             if (swipeCount >= arrOfStatus.count)
             {
             swipeCount = arrOfStatus.count - 1
             }
             print("Swipe count \(swipeCount)")
             addBorder()
             let strStatus = "\(arrOfStatus.object(at: swipeCount))"
             fetchOpportunityLeadsFromCoreData(strStatus: strStatus)*/
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.left {
            swipeCount = swipeCount + 1
            
            if (swipeCount >= arrOfStatus.count)
            {
                swipeCount = arrOfStatus.count - 1
            }
            print("Swipe count \(swipeCount)")
            addButton()
            let strStatus = "\(arrOfStatus.object(at: swipeCount))"
            fetchOpportunityLeadsFromCoreData(strStatus: strStatus)
            
            if swipeCount == 0
            {
                scrollViewLeads.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                
            }
            else
            {
                scrollViewLeads.setContentOffset(CGPoint(x: (swipeCount + 1)*100 - 200, y: 0), animated: true)
            }
            
            /*swipeCount = swipeCount - 1
             
             if (swipeCount < 0)
             {
             swipeCount = 0
             }
             print("Swipe count \(swipeCount)")
             addBorder()
             let strStatus = "\(arrOfStatus.object(at: swipeCount))"
             fetchOpportunityLeadsFromCoreData(strStatus: strStatus)*/
        }
        
    }
    // MARK: -  ------------------------------ Action ------------------------------
    
    func getStatus()
    {
        arrOfStatus = NSMutableArray()
        arrOfStatusName = NSMutableArray()
        
        if nsud.value(forKey: "LeadStatusMasters") is NSArray
        {
            var arrStatusTemp = NSArray()
            arrStatusTemp = nsud.value(forKey: "LeadStatusMasters") as! NSArray
            for item in arrStatusTemp
            {
                let dict = item as! NSDictionary
                
                if ("\(dict.value(forKey: "SysName") ?? "")").caseInsensitiveCompare("void") == .orderedSame
                {
                    
                }
                else
                {
                    arrOfStatus.add("\(dict.value(forKey: "SysName")!)")
                    arrOfStatusName.add("\(dict.value(forKey: "StatusName")!)")
                    
                }
                
            }
        }
        
    }
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        /*for controller in self.navigationController!.viewControllers as NSArray
         {
         if controller is DashBoardView
         {
         self.navigationController!.popToViewController(controller as! UIViewController, animated: false)
         break
         }
         }*/
        nsud.set(false, forKey: "fromOpportunityAssociation")
        nsud.synchronize()
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func actionOnSegmentControl(_ sender: Any)
    {
        if segmentController.selectedSegmentIndex == 0
        {
            goToWebLead()
        }
        else if segmentController.selectedSegmentIndex == 1
        {
            
        }
    }
    
    @IBAction func actionOnFilter(_ sender: Any)
    {
        goToFilter()
    }
    
    @IBAction func actionOnLead(_ sender: Any)
    {
        goToWebLead()
    }
    
    @IBAction func actionOnOpportunity(_ sender: Any)
    {
        
    }
    @IBAction func actionOnSearch(_ sender: Any)
    {
        
    }
    
    @IBAction func actionOnAdd(_ sender: Any)
    {
        goToAddOpportunity()
    }
    
    @IBAction func actionOnTaskActivity(_ sender: UIButton) {
        
        let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
        
        testController.strFromVC = "DashBoardView"//"OpportunityVC"
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    @IBAction func actionOnNearBy(_ sender: Any)
    {
        goToNearBy()
    }
    
    @IBAction func actionOnContactList(_ sender: Any)
    {
        goToContactList()
    }
    
    @IBAction func actionOnMore(_ sender: UIButton)
    {
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        
        
        let Near = (UIAlertAction(title: "Near By", style: .default , handler:{ (UIAlertAction)in
            self.goToNearBy()
        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
        Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Near)
        
        
        let Signed = (UIAlertAction(title: "Signed View Agreements", style: .default , handler:{ (UIAlertAction)in
            
            self.goToSignedViewAgreement()
            
        }))
        
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
        Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Signed)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    @IBAction func actionOnDashboard(_ sender: Any)
    {
        nsud.set(nil, forKey: "opportunityFilterData")
        nsud.synchronize()
        goToDasboard()
    }
    
    @IBAction func actionOnAppointment(_ sender: Any)
    {
        goToAppointment()
    }
    
    // MARK: -  ------------------------------ API Calling------------------------------
    
    func getAllOpportunityLeads()
    {
        
        let startGlobal = CFAbsoluteTimeGetCurrent()
        
        self.arrAllOpportunityLeads = NSMutableArray()
        self.arrOfUrgency = NSMutableArray()

        if !isInternetAvailable()
        {
            
            if self.arrOfStatus.count > 0
            {
                let strStatus = "\(self.arrOfStatus.object(at: 0))"
                self.fetchOpportunityLeadsFromCoreData(strStatus: "\(strStatus)")
            }
            else
            {
                self.fetchOpportunityLeadsFromCoreData(strStatus: "New")
            }
            
            // Global().displayAlertController(alertMessage, "\(ErrorInternetMsg)", self)
        }
        else
        {
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            /* DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
             
             
             self.callApiToGetOpportunity()
             
             })*/
            
            self.callApiToGetOpportunity()
            
            
        }
        
    }
    func callApiToGetOpportunity()
    {
        //deleteAllRecordsFromDB(strEntity: "TotalOpportunityLeads", predicate: NSPredicate(format: "userName=%@", strUserName))
        
        deleteAllRecordsWithoutPredicateFromDB(strEntity: "TotalOpportunityLeads", predicate: NSPredicate(format: "userName=%@", strUserName))
        
        let startGlobal = CFAbsoluteTimeGetCurrent()
        
        self.arrAllOpportunityLeads = NSMutableArray()
        
        getFilterDataOpportunity()
        
        var strUrl = "\(strServiceUrlMain)\(UrlGetAllOpportunityLead)\("")&startDate=\(strFromDate)&endDate=\(strToDate)"  as String
        
        strUrl = strUrl.trimmingCharacters(in: .whitespaces)
        
        // FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
        
        
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
            
            
            self.loader.dismiss(animated: false)
            {
                DispatchQueue.main.async{
                    
                    //FTIndicator.dismissProgress()
                    //self.refresher.endRefreshing()
                    
                }
                if(Status)
                {
                    
                    //let dictAddress = Response["data"] as! NSDictionary
                    
                    
                    let arrData = Response["data"] as! NSArray
                    
                    //let arrData = Response["data"] as! NSArray
                    
                    if arrData.isKind(of: NSArray.self)
                    {
                        let arrAllData = NSMutableArray()
                        
                        for item in arrData
                        {
                            let  dict = item as! NSDictionary
                            
                            let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))
                            
                            arrAllData.add(dictNew as Any)
                            
                            
                        }
                        // print(arrAllData)
                        
                        //self.arrAllOpportunityLeads = arrAllData
                        
                        //let str = getJson(from: arrAllData)
                        //print("\(str ?? "")" as Any)
                        
                        self.saveOpportunityLeadsToCoreData(arrAllData: arrAllData)
                        
                        
                        self.addButton()
                        
                        if self.arrOfStatus.count > 0
                        {
                            let strStatus = "\(self.arrOfStatus.object(at: 0))"
                            self.fetchOpportunityLeadsFromCoreData(strStatus: "\(strStatus)")
                        }
                        else
                        {
                            self.fetchOpportunityLeadsFromCoreData(strStatus: "New")
                        }
                        
                        let diffGlobal = CFAbsoluteTimeGetCurrent() - startGlobal
                        print("Took \(diffGlobal) seconds to get Data OverAll To Render From SQL Server. Count of Opportunity :-- \(arrAllData.count)")
                        
                        // self.addBorder()
                        
                        /*  if self.arrOfStatus.count > 0
                         {
                         let strStatus = "\(self.arrOfStatus.object(at: 0))"
                         self.fetchOpportunityLeadsFromCoreData(strStatus: "\(strStatus)")
                         }
                         else
                         {
                         self.fetchOpportunityLeadsFromCoreData(strStatus: "New")
                         }*/
                        
                        
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }
                else
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                }
                
                //                nsud.setValue("", forKey:  "cureentTabOpportunity")
                //                nsud.setValue("", forKey: "cureentSwipeCountOpportunity")
                //                nsud.synchronize()
                
            }
            
            
        }
        
    }
    
    // MARK: -  ------------------------------ Function  ------------------------------
    
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
                         lbltaskCount.isHidden = true
                         lblScheduleCount.isHidden = true
              }
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }
                    
                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                                                 lblScheduleCount.text = strScheduleCount
                                                 lblScheduleCount.isHidden = false
                        }else{
                            let badgeSchedule = SPBadge()
                                                   badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                                                   badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                                                   badgeSchedule.badge = strScheduleCount
                                                   btnScheduleFooter.addSubview(badgeSchedule)
                        }
                       
                        
                    }
                    
                    
                    if strTaskCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                            lbltaskCount.text = strTaskCount
                            lbltaskCount.isHidden = false
                            
                        }else{
                            let badgeTasks = SPBadge()
                            badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                            badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeTasks.badge = strTaskCount
                            btnTasksFooter.addSubview(badgeTasks)
                        }
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func goToFilter()
    {
        
        let mainStoryboard = UIStoryboard(
            name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
            bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "FilterOpportunityVC_CRMNew_iPhone") as! FilterOpportunityVC_CRMNew_iPhone
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func goToWebLead()
    {
        /*let storyboardIpad = UIStoryboard.init(name: "CRM_NewiPhone", bundle: nil)
         let testController = storyboardIpad.instantiateViewController(withIdentifier: "WebLeadVC") as? WebLeadVC
         self.navigationController?.pushViewController(testController!, animated: false)*/
        
        /*   let storyboardIpad = UIStoryboard.init(name: "CRM_NewiPhone", bundle: nil)
         let testController = storyboardIpad.instantiateViewController(withIdentifier: "WebLeadVC") as? WebLeadVC*/
        let mainStoryboard = UIStoryboard(
            name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone",
            bundle: nil)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebLeadVC") as? WebLeadVC
        
        /*   UIView.animate(withDuration: 0.75, animations: {() -> Void in
         UIView.setAnimationCurve(.easeInOut)
         self.navigationController?.pushViewController(testController!, animated: false)
         UIView.setAnimationTransition(.flipFromRight, for: (self.navigationController?.view)!, cache: false)
         })
         */
        self.navigationController?.pushViewController(testController!, animated: false)
        
    }
    
    
    func addButton()
    {
        if "\(nsud.value(forKey:  "cureentTabOpportunity") ?? "")".count > 0
        {
            let strCount = "\(nsud.value(forKey: "cureentSwipeCountOpportunity") ?? "")"
            swipeCount = Int(strCount)!
        }
        
        var width = CGFloat()
        for btn  in scrollViewLeads.subviews
        {
            btn.removeFromSuperview()
        }
        var xAxis = 0
        
        for i in 0 ..< arrOfStatus.count
        {
            let btn = UIButton()
            let lbl1 = UILabel()
            let lbl2 = UILabel()
            let lbl3 = UILabel()
            
            lbl1.textAlignment = NSTextAlignment.center
            lbl2.textAlignment = NSTextAlignment.center
            lbl3.textAlignment = NSTextAlignment.center
            
            lbl1.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
            lbl2.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 16 : 12)
            lbl3.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 16 : 12)
            
            lbl1.textColor = UIColor.white
            lbl2.textColor = UIColor.white
            lbl3.textColor = UIColor.white
            
            
            btn.frame = CGRect(x: xAxis, y: 1, width: DeviceType.IS_IPAD ? 170 : 120, height: Int(scrollViewLeads.frame.height) - 2)
            lbl1.frame = CGRect(x: xAxis, y: 8, width: Int(btn.frame.width), height: Int(scrollViewLeads.frame.height/3) - 8)
            lbl2.frame =  CGRect(x: xAxis, y: Int(lbl1.frame.maxY) + 2, width: Int(btn.frame.width), height: Int(scrollViewLeads.frame.height/3) - 4)
            lbl3.frame =  CGRect(x: xAxis, y: Int(lbl2.frame.maxY) + 2, width: Int(btn.frame.width), height: Int(scrollViewLeads.frame.height/3) - 4)
            
            xAxis = xAxis + Int(btn.frame.width + 5)
            
            lbl1.text = ""
            lbl2.text = ""
            lbl3.text = ""
            
            
            if arrOfStatus.count > 0
            {
                lbl1.text = "\(arrOfStatusName.object(at: i))".uppercased()
            }
            
            
            let strStatus = "\(self.arrOfStatus.object(at: i))"
            self.fetchOpportunityLeadsFromCoreDataForButton(strStatus: "\(strStatus)")
            lbl2.text = String(format: "\(arrAllOpportunityCount.count)")
            
            var totalLeadAmount = Double()
            
            for item in arrAllOpportunityCount
            {
                let dict = item as! NSManagedObject
                let amount = ("\(dict.value(forKey: "opportunityValue") ?? "")" as NSString).doubleValue
                totalLeadAmount = totalLeadAmount + amount
            }
            
            lbl3.text = String(format: "%@", convertDoubleToCurrency(amount: totalLeadAmount))
            
            scrollViewLeads.addSubview(lbl1)
            scrollViewLeads.addSubview(lbl2)
            scrollViewLeads.addSubview(lbl3)
            
            
            if i == swipeCount
            {
                btn.backgroundColor = UIColor(red: 208.0/255, green: 191.0/255, blue: 35.0/255, alpha: 1)
                
            }
            else
            {
                btn.backgroundColor = UIColor.clear
                
                
            }
            btn.addTarget(self, action: #selector(action_ButtonClicked), for: .touchUpInside)
            
            btn.setTitle("\(i)", for: .normal)
            btn.setTitleColor(UIColor.clear, for: .normal)
            
            width = width + btn.frame.width
            
            scrollViewLeads.addSubview(btn)
            
            scrollViewLeads.bringSubviewToFront(lbl1)
            scrollViewLeads.bringSubviewToFront(lbl2)
            scrollViewLeads.bringSubviewToFront(lbl3)
            
            
        }
        
        
    }
    
    @objc func action_ButtonClicked(sender:UIButton)
    {
        
        let value = Int("\(sender.titleLabel?.text ?? "")")
        
        print("Button Clicked \(value ?? 0)")
        swipeCount = value ?? 0
        
        print("Swipe count \(swipeCount)")
        
        if swipeCount == 0
        {
            scrollViewLeads.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
        }
        else
        {
            scrollViewLeads.setContentOffset(CGPoint(x: sender.frame.maxX - sender.frame.size.width*2, y: 0), animated: true)
        }
        
        
        addButton()
        
        let strStatus = "\(arrOfStatus.object(at: swipeCount))"
        fetchOpportunityLeadsFromCoreData(strStatus: strStatus)
        
    }
    
    
    @objc func action_ButtonClickedOld(sender:UIButton)
    {
        
        let value = Int("\(sender.titleLabel?.text ?? "")")
        
        print("Button Clicked \(value ?? 0)")
        swipeCount = value ?? 0
        
        
        print("Swipe count \(swipeCount)")
        
        /*nsud.setValue(arrOfStatus.object(at: swipeCount), forKey:  "cureentTabOpportunity")
         nsud.setValue(arrOfStatus.index(of: "\(arrOfStatus.object(at: swipeCount))"), forKey: "cureentSwipeCountOpportunity")
         nsud.synchronize()*/
        
        
        
        addButton()
        
        let strStatus = "\(arrOfStatus.object(at: swipeCount))"
        fetchOpportunityLeadsFromCoreData(strStatus: strStatus)
        
    }
    
    
    func goToAddOpportunity()
    {
        /* let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
         
         let vc = storyboardIpad.instantiateViewController(withIdentifier: "") as! AddLeadViewAddLeadView
         vc.strFromWhere = "OpportunityVC"
         
         self.navigationController?.pushViewController(vc, animated: false)*/
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToAddTask()
    {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
        
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToNearBy()
    {
        /*
        if(DeviceType.IS_IPAD){
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
            self.navigationController?.pushViewController(vc!, animated: false)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
            self.navigationController?.pushViewController(vc!, animated: false)
        }*/
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)

    }
    func goToContactList()
    {
      
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    func getStartDateEndDate()
    {
        strFromDate = ""
        strToDate = ""
        let strNoOfDays = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.DisplayLeadDays") ?? "")"
        let strDisplayWebLeadFor = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.DisplayLeadFor") ?? "")"
        
        
        if strDisplayWebLeadFor == "currentday"
        {
            strFromDate = Global().getCurrentDate()
            strToDate = Global().getCurrentDate()
        }
        else if strDisplayWebLeadFor == "currentmonth"
        {
            
            let objDelegate = StasticsClass()
            objDelegate.delegate = self as StasticsClassDelegate
            objDelegate.getStart_EndDate("This Month")
            
        }
        else if strNoOfDays == "" || strNoOfDays == "0"
        {
            strFromDate = Global().getCurrentDate()
            strToDate = Global().getCurrentDate()
        }
        else
        {
            strFromDate = Global().getStartDate(Global().getCurrentDate(), Int(strNoOfDays)!)
            strToDate = Global().getEndDate(Global().getCurrentDate(), Int(strNoOfDays)!)
            
        }
        
    }
    func moveToTop()
    {
        if arrAllOpportunityLeads.count > 0
        {
            /*  var path = NSIndexPath()
             path = NSIndexPath(row: arrAllOpportunityLeads.count-1 , section: 0)
             tblViewLeads.scrollToRow(at: path as IndexPath, at: UITableView.ScrollPosition.top, animated: false)*/
            tblViewLeads.setContentOffset(CGPoint.zero, animated:true)
            
        }
    }
    func setUpView()
    {
        
        //lblNoDataFound.isHidden = true
        viewTopButton.backgroundColor = UIColor.white
        
        btnWebLead.layer.cornerRadius = btnWebLead.frame.size.height/2
        btnWebLead.layer.borderWidth = 0
        
        viewTopButton.layer.cornerRadius = viewTopButton.frame.size.height/2
        viewTopButton.layer.borderWidth = 0
        
        btnOpportunity.layer.cornerRadius = btnOpportunity.frame.size.height/2
        btnOpportunity.layer.borderWidth = 0
        
        btnOpportunityNew.layer.cornerRadius = btnOpportunity.frame.size.height/2
        btnOpportunityNew.layer.borderWidth = 0
        
        viewTopButton.bringSubviewToFront(btnOpportunity)
        
        if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .none//.roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15//txfSearchField.frame.size.height/2
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
            
        }
        searchBar.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        //
        searchBar.layer.borderWidth = 1
        searchBar.layer.opacity = 1.0
        
        //        searchBarCompany.layer.backgroundColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        
        // searchBar.layer.cornerRadius = searchBar.frame.size.height/2
        //searchBar.layer.borderWidth = 0
        
        
        
    }
    func goToDasboard()
    {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func goToAppointment()
    {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
//        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboardIpad.instantiateViewController(withIdentifier: "AppointmentView") as! AppointmentView
//        self.navigationController?.pushViewController(vc, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
                
            }
                 }
        
    }
    
    func goToSignedViewAgreement()
    {
        //nsud.set(true, forKey: "fromCompanyVC")
        if(DeviceType.IS_IPAD){
            let mainStoryboard = UIStoryboard(
                name: "CRMiPad",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }else{
            let mainStoryboard = UIStoryboard(
                name: "CRM",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }
        
    }
    func getFilterDataOpportunity()
    {
        //nsud.set(dictFilterData, forKey: "webLeadFilterData")
        
        var dictData = NSDictionary()
        arrOfUrgency = NSMutableArray()

        if nsud.value(forKey: "opportunityFilterData") is NSDictionary
        {
            dictData = nsud.value(forKey: "opportunityFilterData") as! NSDictionary
            
            var strUserType = String()
            var strDayType = String()
            
            print("\(dictData)")
            if (dictData.count > 0)
            {
                
                strUserType = "\(dictData.value(forKeyPath: "userTypeFromFilter") ?? "")"
                if (dictData.value(forKeyPath: "selectedUserFromFilter")) is NSArray
                {
                    let arr1 = dictData.value(forKeyPath: "selectedUserFromFilter") as! NSArray
                    let arrUser =  arr1.mutableCopy() as! NSMutableArray//dictData.value(forKeyPath: "selectedUserFromFilter") as! NSMutableArray
                    if arrUser.count > 0
                    {
                        strUserType = "\(arrUser.object(at: 0))"
                    }
                    else
                    {
                        strUserType = ""
                    }
                }
                else
                {
                    strUserType = ""
                }
                //Urgency type new added by ruchika
                if (dictData.value(forKeyPath: "selectedUrgencyTypeFromOppFilter")) is NSArray
                {
                    let arr1 = dictData.value(forKeyPath: "selectedUrgencyTypeFromOppFilter") as! NSArray
                    let arrUrgencyType =  arr1.mutableCopy() as! NSMutableArray//dictData.value(forKeyPath: "selectedUserFromFilter") as! NSMutableArray
                    if arrUrgencyType.count > 0
                    {
                        self.arrOfUrgency = arrUrgencyType
                    }
                    else
                    {
                        self.arrOfUrgency = NSMutableArray()
                    }
                    print(arrOfUrgency)
                }
                else
                {
                    self.arrOfUrgency = NSMutableArray()
                }
                
                if dictData.value(forKeyPath: "selectedDayFromFilter") is NSArray
                {
                    let arr1 = dictData.value(forKeyPath: "selectedDayFromFilter") as! NSArray
                    
                    let arrDayType = arr1.mutableCopy() as! NSMutableArray//dictData.value(forKeyPath: "selectedDayFromFilter") as! NSMutableArray
                    if arrDayType.count > 0
                    {
                        strDayType = "\(arrDayType.object(at: 0))"
                    }
                    else
                    {
                        strDayType = ""
                    }
                }
                else
                {
                    strDayType = ""
                }
                
                if strDayType.count > 0
                {
                    let objDelegate = StasticsClass()
                    objDelegate.delegate = self as StasticsClassDelegate
                    objDelegate.getStart_EndDate("\(strDayType)")
                }
                else
                {
                    strFromDate = "\(dictData.value(forKeyPath: "fromDateFromFilter") ?? "")"
                    strToDate = "\(dictData.value(forKeyPath: "toDateFromFilter") ?? "")"
                }
                if strFromDate.count == 0  || strToDate.count == 0
                {
                    strFromDate = Global().getCurrentDate()
                    strToDate = Global().getCurrentDate()
                }
                
                if dictData.value(forKey: "statusFromFilter") is NSArray
                {
                    arrOfStatus = NSMutableArray()
                    arrOfStatusName = NSMutableArray()
                    
                    let arr1 = dictData.value(forKey: "statusFromFilter") as! NSArray
                    let arr2 = dictData.value(forKey: "statusFromFilter") as! NSArray
                    
                    arrOfStatus =  arr1.mutableCopy() as! NSMutableArray//dictData.value(forKey: "statusFromFilter") as! NSMutableArray
                    arrOfStatusName = arr2.mutableCopy() as! NSMutableArray//dictData.value(forKey: "statusFromFilter") as! NSMutableArray
                    // addBorder()
                }
                else
                {
                    getStartDateEndDate()
                    getStatus()
                    addButton()
                }
                
                
            }
            
        }
    }
    
    func getServiceAddress(dictLeadData : NSDictionary) -> String
    {
        var strAddress = ""
        
        if dictLeadData.count > 0
        {
            
            if "\(dictLeadData.value(forKeyPath: "Address1") ?? "")".count > 0
            {
                strAddress = "\(dictLeadData.value(forKeyPath: "Address1") ?? ""), "
            }
            if "\(dictLeadData.value(forKeyPath: "Address2") ?? "")".count > 0
            {
                strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "Address2") ?? "")"), "
            }
            if "\(dictLeadData.value(forKeyPath: "CityName") ?? "")".count > 0
            {
                strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "CityName") ?? "")"), "
            }
            
            if "\(dictLeadData.value(forKeyPath: "StateId") ?? "")".count > 0
            {
                let stateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "StateId") ?? ""))")
                
                strAddress = "\(strAddress)\(stateName ?? ""), "
            }
            if "\(dictLeadData.value(forKeyPath: "Zipcode") ?? "")".count > 0
            {
                strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "Zipcode") ?? "")")"
            }
        }
        
        return strAddress
    }
    // MARK: -  ------------------------------ Core Data Function  ------------------------------

    func saveOpportunityLeadsToCoreData(arrAllData: NSMutableArray)
    {
        //deleteAllRecordsFromDB(strEntity: "TotalOpportunityLeads", predicate: NSPredicate(format: "userName=%@", strUserName)) // remove user name to delete
        deleteAllRecordsWithoutPredicateFromDB(strEntity: "TotalOpportunityLeads", predicate: NSPredicate(format: "userName=%@", strUserName)) // remove user name to delete
        
        for item in arrAllData
        {
            let dict = item as! NSDictionary
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            
            arrOfKeys.add("accountCompany")
            arrOfKeys.add("accountCompanyName")
            arrOfKeys.add("accountContactName")
            arrOfKeys.add("accountId")
            arrOfKeys.add("accountNo")
            arrOfKeys.add("billingAddress")
            arrOfKeys.add("billingAddressSameAsService")
            arrOfKeys.add("billingLocationId")
            arrOfKeys.add("branchSysName")
            arrOfKeys.add("cell")
            arrOfKeys.add("companySize")
            arrOfKeys.add("confidenceLevel")
            arrOfKeys.add("crmContactId")
            arrOfKeys.add("customerRating")
            arrOfKeys.add("departmentSysName")
            arrOfKeys.add("direction")
            arrOfKeys.add("driveTime")
            arrOfKeys.add("expectedClosingDate")
            arrOfKeys.add("feedbackRating")
            arrOfKeys.add("fieldSalesPerson")
            arrOfKeys.add("fieldSalesPersonId")
            arrOfKeys.add("firstName")
            arrOfKeys.add("followUpDate")
            arrOfKeys.add("grabbedDate")
            arrOfKeys.add("industryId")
            arrOfKeys.add("insideSalesPerson")
            arrOfKeys.add("insideSalesPersonId")
            arrOfKeys.add("isTaxExempt")
            arrOfKeys.add("lastName")
            arrOfKeys.add("middleName")
            arrOfKeys.add("notes")
            arrOfKeys.add("opportunityContactAddress")
            arrOfKeys.add("opportunityContactFullName")
            arrOfKeys.add("opportunityContactProfileImage")
            arrOfKeys.add("opportunityDescription")
            arrOfKeys.add("opportunityId")
            arrOfKeys.add("opportunityName")
            arrOfKeys.add("opportunityNumber")
            arrOfKeys.add("opportunityStage")
            arrOfKeys.add("opportunityStatus")
            arrOfKeys.add("opportunityType")
            arrOfKeys.add("opportunityValue")
            arrOfKeys.add("primaryEmail")
            arrOfKeys.add("primaryPhone")
            arrOfKeys.add("primaryPhoneExt")
            arrOfKeys.add("proposedAmount")
            arrOfKeys.add("salesAppDate")
            arrOfKeys.add("scheduleDate")
            arrOfKeys.add("scheduleTime")
            arrOfKeys.add("secondaryEmail")
            arrOfKeys.add("secondaryPhone")
            arrOfKeys.add("secondaryPhoneExt")
            arrOfKeys.add("service")
            arrOfKeys.add("serviceAddress")
            arrOfKeys.add("completeServiceAddress")
            arrOfKeys.add("serviceCategoryId")
            arrOfKeys.add("serviceId")
            arrOfKeys.add("serviceLocationId")
            arrOfKeys.add("sizeId")
            arrOfKeys.add("sourceIdsList")
            arrOfKeys.add("stageId")
            arrOfKeys.add("statusId")
            arrOfKeys.add("submittedBy")
            arrOfKeys.add("submittedById")
            arrOfKeys.add("submittedDate")
            arrOfKeys.add("taxSysName")
            arrOfKeys.add("techNote")
            arrOfKeys.add("totalEstimationTime")
            arrOfKeys.add("urgency")
            arrOfKeys.add("urgencyId")
            arrOfKeys.add("userName")
            arrOfKeys.add("confidenceValue")
            arrOfKeys.add("taxExemptionNo")
            
            
            
            if (dict.value(forKey: "AccountCompany") ?? "") is NSDictionary
            {
                let dictAccountCompany = (dict.value(forKey: "AccountCompany") ?? "") as! NSDictionary
                arrOfValues.add(dictAccountCompany) //accountCompany
                arrOfValues.add("\(dictAccountCompany.value(forKey: "Name") ?? "")") //accountCompany name
            }
            else
            {
                arrOfValues.add("") //accountCompany
                arrOfValues.add("") //accountCompany name
            }
            
            
            
            arrOfValues.add("\(dict.value(forKey: "AccountContactName") ?? "")")//accountContactName
            
            arrOfValues.add("\(dict.value(forKey: "AccountId") ?? "")")//accountId
            arrOfValues.add("\(dict.value(forKey: "AccountNo") ?? "")")//accountNo
            
            
            if (dict.value(forKey: "BillingAddress") ?? "") is NSDictionary
            {
                let dictBillingAddress = (dict.value(forKey: "BillingAddress") ?? "") as! NSDictionary
                arrOfValues.add(dictBillingAddress) //billingAddress
            }
            else
            {
                arrOfValues.add("")//billingAddress
            }
            
            //arrOfValues.add("\(dict.value(forKey: "BillingAddress") ?? "")")//billingAddress
            
            
            arrOfValues.add("\(dict.value(forKey: "BillingAddressSameAsService") ?? "")")//billingAddressSameAsService
            arrOfValues.add("\(dict.value(forKey: "BillingLocationId") ?? "")")//billingLocationId
            arrOfValues.add("\(dict.value(forKey: "BranchSysName") ?? "")")//branchSysName
            arrOfValues.add("\(dict.value(forKey: "Cell") ?? "")")//cell
            arrOfValues.add("\(dict.value(forKey: "CompanySize") ?? "")")//companySize
            arrOfValues.add("\(dict.value(forKey: "ConfidenceLevel") ?? "")")//confidenceLevel
            arrOfValues.add("\(dict.value(forKey: "CrmContactId") ?? "")")//crmContactId
            arrOfValues.add("\(dict.value(forKey: "CustomerRating") ?? "")")//customerRating
            arrOfValues.add("\(dict.value(forKey: "DepartmentSysName") ?? "")")//departmentSysName
            arrOfValues.add("\(dict.value(forKey: "Direction") ?? "")")//direction
            arrOfValues.add("\(dict.value(forKey: "DriveTime") ?? "")")//driveTime
            arrOfValues.add("\(dict.value(forKey: "ExpectedClosingDate") ?? "")")//expectedClosingDate
            arrOfValues.add("\(dict.value(forKey: "FeedbackRating") ?? "")")//feedbackRating
            arrOfValues.add("\(dict.value(forKey: "FieldSalesPerson") ?? "")")//fieldSalesPerson
            arrOfValues.add("\(dict.value(forKey: "FieldSalesPersonId") ?? "")")//fieldSalesPersonId
            arrOfValues.add("\(dict.value(forKey: "FirstName") ?? "")")//firstName
            arrOfValues.add("\(dict.value(forKey: "FollowUpDate") ?? "")")//followUpDate
            arrOfValues.add("\(dict.value(forKey: "GrabbedDate") ?? "")")//grabbedDate
            arrOfValues.add("\(dict.value(forKey: "IndustryId") ?? "")")//industryId
            arrOfValues.add("\(dict.value(forKey: "InsideSalesPerson") ?? "")")//insideSalesPerson
            arrOfValues.add("\(dict.value(forKey: "InsideSalesPersonId") ?? "")")//insideSalesPersonId
            arrOfValues.add("\(dict.value(forKey: "IsTaxExempt") ?? "")")//isTaxExempt
            arrOfValues.add("\(dict.value(forKey: "LastName") ?? "")")//lastName
            arrOfValues.add("\(dict.value(forKey: "MiddleName") ?? "")")//middleName
            arrOfValues.add("\(dict.value(forKey: "Notes") ?? "")")//notes
            
            
            if (dict.value(forKey: "OpportunityContactAddress") ?? "") is NSDictionary
            {
                let dictOpportunityContactAddress = (dict.value(forKey: "OpportunityContactAddress") ?? "") as! NSDictionary
                arrOfValues.add(dictOpportunityContactAddress) //OpportunityContactAddress
            }
            else
            {
                arrOfValues.add("")//OpportunityContactAddress
            }
            //arrOfValues.add("\(dict.value(forKey: "OpportunityContactAddress") ?? "")")//opportunityContactAddress
            
            
            arrOfValues.add("\(dict.value(forKey: "OpportunityContactFullName") ?? "")")//opportunityContactFullName
            arrOfValues.add("\(dict.value(forKey: "OpportunityContactProfileImage") ?? "")")//opportunityContactProfileImage
            arrOfValues.add("\(dict.value(forKey: "OpportunityDescription") ?? "")")//opportunityDescription
            arrOfValues.add("\(dict.value(forKey: "OpportunityId") ?? "")")//opportunityId
            arrOfValues.add("\(dict.value(forKey: "OpportunityName") ?? "")")//opportunityName
            arrOfValues.add("\(dict.value(forKey: "OpportunityNumber") ?? "")")//opportunityNumber
            arrOfValues.add("\(dict.value(forKey: "OpportunityStage") ?? "")")//opportunityStage
            arrOfValues.add("\(dict.value(forKey: "OpportunityStatus") ?? "")")//opportunityStatus
            arrOfValues.add("\(dict.value(forKey: "OpportunityType") ?? "")")//opportunityType
            arrOfValues.add("\(dict.value(forKey: "OpportunityValue") ?? "")")//opportunityValue
            arrOfValues.add("\(dict.value(forKey: "PrimaryEmail") ?? "")")//primaryEmail
            arrOfValues.add("\(dict.value(forKey: "PrimaryPhone") ?? "")")//primaryPhone
            arrOfValues.add("\(dict.value(forKey: "PrimaryPhoneExt") ?? "")")//primaryPhoneExt
            arrOfValues.add("\(dict.value(forKey: "ProposedAmount") ?? "")")//proposedAmount
            arrOfValues.add("\(dict.value(forKey: "SalesAppDate") ?? "")")//salesAppDate
            arrOfValues.add("\(dict.value(forKey: "ScheduleDate") ?? "")")//scheduleDate
            arrOfValues.add("\(dict.value(forKey: "ScheduleTime") ?? "")")//scheduleTime
            arrOfValues.add("\(dict.value(forKey: "SecondaryEmail") ?? "")")//secondaryEmail
            arrOfValues.add("\(dict.value(forKey: "SecondaryPhone") ?? "")")//secondaryPhone
            arrOfValues.add("\(dict.value(forKey: "SecondaryPhoneExt") ?? "")")//secondaryPhoneExt
            arrOfValues.add("\(dict.value(forKey: "Service") ?? "")")//service
            
            
            if (dict.value(forKey: "ServiceAddress") ?? "") is NSDictionary
            {
                let dictServiceAddress = (dict.value(forKey: "ServiceAddress") ?? "") as! NSDictionary
                arrOfValues.add(dictServiceAddress) //ServiceAddress
                arrOfValues.add(getServiceAddress(dictLeadData: dictServiceAddress))
                
            }
            else
            {
                arrOfValues.add("")//ServiceAddress
                arrOfValues.add("")//Complete ServiceAddress
            }
            
            // arrOfValues.add("\(dict.value(forKey: "ServiceAddress") ?? "")")//serviceAddress
            
            
            arrOfValues.add("\(dict.value(forKey: "ServiceCategoryId") ?? "")")//serviceCategoryId
            arrOfValues.add("\(dict.value(forKey: "ServiceId") ?? "")")//serviceId
            arrOfValues.add("\(dict.value(forKey: "ServiceLocationId") ?? "")")//serviceLocationId
            arrOfValues.add("\(dict.value(forKey: "SizeId") ?? "")")//sizeId
            
            if (dict.value(forKey: "SourceIdsList") ?? "") is NSArray
            {
                let arrSourceIdsList = (dict.value(forKey: "SourceIdsList") ?? "") as! NSArray
                arrOfValues.add(arrSourceIdsList) //sourceIdsList
            }
            else
            {
                arrOfValues.add("")//sourceIdsList
            }
            
            // arrOfValues.add("\(dict.value(forKey: "SourceIdsList") ?? "")")//sourceIdsList
            
            
            arrOfValues.add("\(dict.value(forKey: "StageId") ?? "")")//stageId
            arrOfValues.add("\(dict.value(forKey: "StatusId") ?? "")")//statusId
            arrOfValues.add("\(dict.value(forKey: "SubmittedBy") ?? "")")//submittedBy
            arrOfValues.add("\(dict.value(forKey: "SubmittedById") ?? "")")//submittedById
            arrOfValues.add("\(dict.value(forKey: "SubmittedDate") ?? "")")//submittedDate
            arrOfValues.add("\(dict.value(forKey: "TaxSysName") ?? "")")//taxSysName
            arrOfValues.add("\(dict.value(forKey: "TechNote") ?? "")")//techNote
            arrOfValues.add("\(dict.value(forKey: "TotalEstimationTime") ?? "")")//totalEstimationTime
            arrOfValues.add("\(dict.value(forKey: "Urgency") ?? "")")//urgency
            arrOfValues.add("\(dict.value(forKey: "UrgencyId") ?? "")")//urgencyId
            arrOfValues.add("\(strUserName)")//userName
            
            arrOfValues.add("\(dict.value(forKey: "ConfidenceValue") ?? "")")//confidenceValue
            
            arrOfValues.add("\(dict.value(forKey: "TaxExemptionNo") ?? "")")//confidenceValue
            
            
            saveDataInDB(strEntity: "TotalOpportunityLeads", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            
            //Update Modify Date In Work Order DB
            //updateServicePestModifyDate(strWoId: self.strWoId as String)
        }
        
        
    }
    func fetchOpportunityLeadsFromCoreData(strStatus: String)
    {
        
        var strStatusNew = String()
        strStatusNew = strStatus
        
        
        if "\(nsud.value(forKey:  "cureentTabOpportunity") ?? "")".count > 0
        {
            strStatusNew = "\(nsud.value(forKey:  "cureentTabOpportunity")!)"
            
            nsud.setValue("", forKey:  "cureentTabOpportunity")
            nsud.setValue("", forKey: "cureentSwipeCountOpportunity")
            nsud.synchronize()
            
        }
        
        
        //ServiceAddressPOCDetailDcs
        var sortDescriptor = NSSortDescriptor()
        let isSortBySubmittedDateAscending = nsud.bool(forKey: "SortByOpportunitySubmittedDate")
        
        if isSortBySubmittedDateAscending{
            sortDescriptor = NSSortDescriptor(key: "submittedDate", ascending: true)
        }
        else{
            sortDescriptor = NSSortDescriptor(key: "submittedDate", ascending: false)
        }
        
        let arryOfData = getDataFromCoreDataBaseArraySorted(strEntity: "TotalOpportunityLeads", predicate: NSPredicate(format: "userName == %@ && opportunityStatus == %@", strUserName, strStatusNew), sort: sortDescriptor)
        
        arrAllOpportunityLeads = arryOfData.mutableCopy() as! NSMutableArray
        arrAllOpportunityLeadsData = arryOfData.mutableCopy() as! NSMutableArray
        
        if arrAllOpportunityLeads.count > 0 {
            if arrOfUrgency.count > 0 {
                let finalDict = NSMutableArray()
                let finalDictData = NSMutableArray()
                
                let arrOfOppTemp = arrAllOpportunityLeads
                let arrofOppTempData = arrAllOpportunityLeadsData
                
                for i in 0..<arrOfUrgency.count{
                    if arrOfUrgency[i] is NSDictionary{
                        let dict = arrOfUrgency[i] as! NSDictionary
                        let strUrgencyId = "\(dict.value(forKey: "UrgencyId") ?? "")"
                        let predicate = NSPredicate(format: "urgencyId contains[cd] %@", strUrgencyId)
                        
                        let filtered = arrOfOppTemp.filtered(using: predicate)
                        for item in filtered{
                            finalDict.add(item)
                        }
                        // arrAllOpportunityLeads.add((filtered as NSArray).mutableCopy() as! NSMutableArray)
                        
                        let filtered2 = arrofOppTempData.filtered(using: predicate)
                        for item in filtered2{
                            finalDictData.add(item)
                        }
                        //arrAllOpportunityLeadsData.add((filtered2 as NSArray).mutableCopy() as! NSMutableArray)
                    }
                }
                arrAllOpportunityLeads = finalDict
                arrAllOpportunityLeadsData = finalDictData
                
                let temp = arrAllOpportunityLeads.sortedArray(using: [sortDescriptor])
                print(temp)
                arrAllOpportunityLeads = (temp as NSArray).mutableCopy() as! NSMutableArray
                
                let temp2 = arrAllOpportunityLeadsData.sortedArray(using: [sortDescriptor])
                print(temp2)
                arrAllOpportunityLeadsData = (temp2 as NSArray).mutableCopy() as! NSMutableArray
            }
        }
        
      
        
        var totalLeadAmount = Float()
        totalLeadAmount = 0.0
        
        for item in arrAllOpportunityLeads
        {
            let dict = item as! NSManagedObject
            var value = Float()
            value = 0.0
            let myString = "\(dict.value(forKey: "opportunityValue") ?? "")"
            let myFloat = (myString as NSString).floatValue
            value = value + myFloat
            totalLeadAmount = totalLeadAmount + value
        }
        
        
        tblViewLeads.reloadData()
        moveToTop()
        
        //---Navin-
        //Search bar clear when click on other tab ---
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        let isForAssociation = nsud.bool(forKey: "fromOpportunityAssociation")
        self.strSearchText = ""
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setTopMenuOption(tagForAssociation: isForAssociation)
        })
        //----End
        
        
        
    }
    func fetchOpportunityLeadsFromCoreDataForButton(strStatus: String)
    {
        
        //ServiceAddressPOCDetailDcs
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalOpportunityLeads", predicate: NSPredicate(format: "userName == %@ && opportunityStatus == %@", strUserName, strStatus))
        
        arrAllOpportunityCount = arryOfData.mutableCopy() as! NSMutableArray
        
        if arrAllOpportunityCount.count > 0 {
            if arrOfUrgency.count > 0 {
                let finalDict = NSMutableArray()
                let arrOfOppTemp = arrAllOpportunityCount
                
                for i in 0..<arrOfUrgency.count{
                    if arrOfUrgency[i] is NSDictionary{
                        let dict = arrOfUrgency[i] as! NSDictionary
                        let strUrgencyId = "\(dict.value(forKey: "UrgencyId") ?? "")"
                        let predicate = NSPredicate(format: "urgencyId contains[cd] %@", strUrgencyId)
                        
                        let filtered = arrOfOppTemp.filtered(using: predicate)
                        for item in filtered{
                            finalDict.add(item)
                        }
                    }
                }
                arrAllOpportunityCount = finalDict
            }
        }
        
        var totalLeadAmount = Double()
        totalLeadAmount = 0.0
        
        for item in arrAllOpportunityCount
        {
            let dict = item as! NSManagedObject
            var value = Double()
            value = 0.0
            let myString = "\(dict.value(forKey: "opportunityValue") ?? "")"
            let myFloat = (myString as NSString).doubleValue
            value = value + myFloat
            totalLeadAmount = totalLeadAmount + value
        }
    }
}
// MARK: --------------------- Tableview Delegate --------------

extension OpportunityVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrAllOpportunityLeads.count
        /*if yesFiltered
         {
         //return 5
         return arrOfLeadsFilter.count
         }
         else
         {
         //return 5
         return arrOfLeads.count
         }*/
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let cell = tblViewLeads.dequeueReusableCell(withIdentifier: "LeadVCCell", for: indexPath as IndexPath) as! LeadVCCell
        
        let matches = arrAllOpportunityLeads.object(at: indexPath.row) as! NSManagedObject
        
        /*cell.lblNewAccNo.text = "Account: #\(matches.value(forKey: "AccountNo") ?? "N/A")  /  "
         
         cell.lblNewOppNo.text = "Opp: #\(matches.value(forKey: "OpportunityNumber") ?? "N/A")  /  "*/
        
        cell.lblNewAccNo.text = "\(matches.value(forKey: "opportunityName") ?? "")  \u{2022}  "
        
        cell.lblNewOppNo.text = ""
        
        cell.lblNewStatus.text = "\(matches.value(forKey: "opportunityStage") ?? "")"
        
        if matches.value(forKey: "accountCompany") is NSDictionary
        {
            let dict = matches.value(forKey: "accountCompany") as! NSDictionary
            cell.lblNewCompanyName.text = "\(dict.value(forKey: "Name") ?? "")"
        }
        else
        {
            cell.lblNewCompanyName.text = ""
        }
        
        cell.lblNewCustomerName.text = "\(matches.value(forKey: "accountContactName") ?? "")"
        
        var strName = ""
        if "\(matches.value(forKey: "firstName") ?? "")".count > 0
        {
            strName = "\(matches.value(forKey: "firstName") ?? "")"
        }
        if "\(matches.value(forKey: "middleName") ?? "")".count > 0
        {
            strName = strName + " " + "\(matches.value(forKey: "middleName") ?? "")"
        }
        if "\(matches.value(forKey: "lastName") ?? "")".count > 0
        {
            strName = strName + " " + "\(matches.value(forKey: "lastName") ?? "")"
        }
        
        //cell.lblNewCustomerName.text = "\(matches.value(forKey: "firstName") ?? "")" + " " + "\(matches.value(forKey: "middleName") ?? "")" + " " + "\(matches.value(forKey: "lastName") ?? "")"
        cell.lblNewCustomerName.text = strName
        
        
        
        //AccountContactName FieldSalesPerson
        var strConfidence = "\(matches.value(forKey: "confidenceLevel") ?? "")"
        //var strConfidence = "\(matches.value(forKey: "ConfidenceValue") ?? "")"
        
        
        
        if strConfidence.count == 0 || strConfidence == ""
        {
            strConfidence = "0.0"
        }
        
        var strAmount = "\(matches.value(forKey: "opportunityValue") ?? "")"
        
        if strAmount.count == 0 || strAmount == ""
        {
            strAmount = "0.00"
        }
        let amount = (strAmount as NSString).doubleValue
        
        cell.lblNewAmount.text = convertDoubleToCurrency(amount: amount)
        
        
        cell.lblNewPercentage.text = "\(strConfidence) %"
        let arrayTemp = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND status == %@ AND refId == %@ AND refType == %@",strUserName, "Open", "\(matches.value(forKey: "opportunityId") ?? "")", "Lead"))
        
        
        
        
        cell.lblNewAmount.text = convertDoubleToCurrency(amount: Double(strAmount)!) + "  \u{2022}  \(strConfidence) %  \u{2022}  " + (cell.lblNewCompanyName.text != "" ? "\(cell.lblNewCompanyName.text!)  \u{2022}  " : "") +  cell.lblNewCustomerName.text!
        
        
        if arrayTemp.count > 0
        {
            cell.lblNewStatusBar.backgroundColor = UIColor.red
            
            cell.lblNewStatusBar.backgroundColor = UIColor(red: 207.0/255.0, green: 68.0/255.0, blue: 47.0/255.0, alpha: 1)
            
            
            /*let arrayTaskCount = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND refId == %@ AND refType == %@",strUserName,"\(matches.value(forKey: "opportunityId") ?? "")", "Lead"))
             
             if arrayTaskCount.count == 0
             {
             cell.lblNewStatusBar.backgroundColor =  UIColor.green
             }else {
             cell.lblNewStatusBar.backgroundColor = UIColor.red
             }*/
            
        }
        else
        {
            cell.lblNewStatusBar.backgroundColor =  UIColor.green//UIColor(red: 13.0/255, green: 101.0/255, blue: 15.0/255, alpha: 1)
            cell.lblNewStatusBar.backgroundColor = UIColor(red: 62.0/255.0, green: 147.0/255.0, blue: 84.0/255.0, alpha: 1)
            
        }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        //let dict = aryTBL.object(at: indexPath.row)as? NSDictionary
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let isForAssociation = nsud.bool(forKey: "fromOpportunityAssociation")
        
        if isForAssociation {
            
            nsud.set(false, forKey: "fromOpportunityAssociation")
            nsud.synchronize()
            
            let dictData = getMutableDictionaryFromNSManagedObject(obj: arrAllOpportunityLeads.object(at: indexPath.row) as! NSManagedObject) as NSDictionary
            
            if strTypeOfAssociations == "Activity" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedActivity_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
                
            } else if strTypeOfAssociations == "Task"{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedTask_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
                
            } else if strTypeOfAssociations == "Company"{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedCompany_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
                
            } else if strTypeOfAssociations == "Contact"{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedContact_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
                
            } else if strTypeOfAssociations == "Account"{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAccount_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
                
            } //-------Association Task Activity------
            else if(strTypeOfAssociations == "AddActivityCRM_Associations"){
                self.delegateActivityAssociate?.getDataAddActivityAssociateProtocol(notification: dictData, tag: selectionTag)
            }
            else if(strTypeOfAssociations == "AddTaskCRM_Associations"){
                self.delegateTaskAssociate?.getDataAddTaskAssociateProtocol(notification: dictData, tag: selectionTag)
            }
            
            //---------------------------//
            self.navigationController?.popViewController(animated: false)
            
        } else {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityDetailsVC_CRMNew_iPhone") as! OpportunityDetailsVC_CRMNew_iPhone
            
            //vc.dictLeadData = arrAllOpportunityLeads.object(at: indexPath.row) as! NSManagedObject
            
            
            vc.dictLeadData = getMutableDictionaryFromNSManagedObject(obj: arrAllOpportunityLeads.object(at: indexPath.row) as! NSManagedObject) as NSDictionary
            let matches = arrAllOpportunityLeads.object(at: indexPath.row) as! NSManagedObject
            vc.strRefIdNew = "\(matches.value(forKey: "opportunityId") ?? "")"
            vc.strReftype = "Opportunity"
            self.navigationController?.pushViewController(vc, animated: false)
            
            nsud.setValue(arrOfStatus.object(at: swipeCount), forKey:  "cureentTabOpportunity")
            nsud.setValue(arrOfStatus.index(of: "\(arrOfStatus.object(at: swipeCount))"), forKey: "cureentSwipeCountOpportunity")
            nsud.setValue(arrOfStatus.object(at: swipeCount), forKey:  "cureentTabOpportunityNew")
            
            nsud.synchronize()
            
        }
        
    }
    
    /*func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
     return UIView()
     }*/
}
// MARK: --------------------- Searchbar Delegate --------------


extension  OpportunityVC : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: searchBar.text! as NSString) // ""
    }
    func searchAutocomplete(Searching: NSString) -> Void
    {
        //accountCompany fieldSalesPerson
        let resultPredicate = NSPredicate(format: "accountNo contains[c] %@ OR opportunityNumber contains[c] %@ OR opportunityStage contains[c] %@ OR opportunityValue  contains[c] %@ OR accountCompany contains[c] %@  OR fieldSalesPerson contains[c] %@ OR accountContactName contains[c] %@ OR firstName contains[c] %@ OR middleName contains[c] %@ OR lastName contains[c] %@ OR accountCompanyName contains[c] %@ OR completeServiceAddress contains[c] %@ OR opportunityName contains[c] %@", argumentArray: [Searching, Searching , Searching , Searching, Searching, Searching, Searching, Searching, Searching, Searching, Searching, Searching, Searching])
        if !(Searching.length == 0)
        {
            let arrayfilter = (self.arrAllOpportunityLeadsData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arrAllOpportunityLeads = NSMutableArray()
            self.arrAllOpportunityLeads = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tblViewLeads.reloadData()
        }
        else
        {
            self.arrAllOpportunityLeads = NSMutableArray()
            self.arrAllOpportunityLeads = self.arrAllOpportunityLeadsData.mutableCopy() as! NSMutableArray
            self.tblViewLeads.reloadData()
          //  self.view.endEditing(true)
            searchBar.text = ""
        }
        if(arrAllOpportunityLeads.count == 0){
        }
        
        
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty
        {
            self.view.endEditing(true)
            self.searchAutocomplete(Searching: "")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                searchBar.resignFirstResponder()
            }
        }
    }
}
/*extension  OpportunityVC : UISearchBarDelegate
 {
 
 func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
 {
 var txtAfterUpdate:NSString = searchBar.text! as NSString
 txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
 self.searchAutocomplete(Searching: txtAfterUpdate)
 return true
 }
 func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
 {
 self.view.endEditing(true)
 self.searchAutocomplete(Searching: "")
 searchBar.text = ""
 const_ViewTop_H.constant = 60
 const_SearchBar_H.constant = 0
 }
 func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
 {
 self.view.endEditing(true)
 self.searchAutocomplete(Searching: "")
 }
 func searchAutocomplete(Searching: NSString) -> Void
 {
 
 }
 }*/

/*extension LeadVC: UISearchBarDelegate
 {
 func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
 {
 const_ViewTop_H.constant = 60
 const_SearchBar_H.constant = 0
 }
 func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
 
 searchBar.resignFirstResponder()
 }
 func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
 {
 if searchText.count == 0
 {
 yesFiltered = false
 tblViewLeads.isHidden = false
 tblViewLeads.reloadData()
 
 }
 else
 {
 yesFiltered = true
 var arrFilter = NSMutableArray()
 
 }
 }
 }*/



// MARK: --------------------- Filter Delegate --------------


extension OpportunityVC : StasticsClassDelegate
{
    
    func getData(_ dict: [AnyHashable : Any]!)
    {
        let dictNew = dict! as NSDictionary
        
        strFromDate = "\(dictNew.value(forKey: "StartDate") ?? "")"
        strToDate = "\(dictNew.value(forKey: "EndDate") ?? "")"
    }
    
}
// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension OpportunityVC : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        strSearchText = str
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
           
            let isForAssociation = nsud.bool(forKey: "fromWebLeadAssociation")
                self.setTopMenuOption(tagForAssociation: isForAssociation)
            self.searchAutocomplete(Searching: str as NSString)
        }
    }
    
    
}
