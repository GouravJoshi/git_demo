//
//  FilterTaskVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 25/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
protocol FilterTask_iPhone_Protocol : class
{
    func getDataTask_iPhone_Protocol(dictData : NSDictionary ,tag : Int)
}


class FilterTaskCell:UITableViewCell
{
    @IBOutlet weak var lblFilterName: UILabel!
    
}

class FilterTaskDateCell:UITableViewCell
{
    
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    
    @IBOutlet weak var btnClearDate: UIButton!
    
    @IBOutlet weak var txtFromDate: ACFloatingTextField!
    @IBOutlet weak var txtToDate: ACFloatingTextField!
    
    
}

class FilterTaskVC_CRMNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource {
    weak var delegate: FilterTask_iPhone_Protocol?

    @IBOutlet weak var tblviewFilterTask: UITableView!
    
    fileprivate let aryUserName = NSMutableArray()//["Akshay Hastekar", "EveryOne"]
    
    fileprivate  let aryOverdue : NSMutableArray! = ["Today", "Tomorrow","This week", "Next week"]
    fileprivate  let aryPriority = NSMutableArray()
    fileprivate  let arySortBy : NSMutableArray!  = ["A to Z", "Z to A"]
    
    fileprivate  let arySortBySegment : NSMutableArray! = [["title" : "Date" , "id" : "1"],["title" : "Priority" , "id" : "2"],["title" : "Task Type" , "id" : "3"],["title" : "Status" , "id" : "4"], ["title" : "A-Z" , "id" : "5"], ["title" : "Z-A" , "id" : "6"]]

    
    fileprivate var strFromDate = ""
    fileprivate var strToDate = ""
    fileprivate var isFromDate = false
    fileprivate var strEmpID = ""
    fileprivate var aryIndexPath = NSMutableArray()
    fileprivate var dictDetailsFortblView = NSDictionary()
    fileprivate var dictLoginData = NSDictionary()
    fileprivate var strUserName = ""
    fileprivate var arySelectedUser = NSMutableArray()
    fileprivate var arySelectedOverDue = NSMutableArray()
    fileprivate var arySelectedPriority = NSMutableArray()
    fileprivate var arySelectedSortBy = NSMutableArray()
    fileprivate let aryTaskTypeMaster = NSMutableArray()
    fileprivate var arySelectedTaskType = NSMutableArray()
    fileprivate  let aryTaskStatus : NSMutableArray! = ["Open", "Done"]
    fileprivate var arySelectedTaskStatus = NSMutableArray()
    
    // MARK: View's life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username")!)"
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
       
        aryUserName.add(strUserName)
        aryUserName.add("Everyone")
        
        dictDetailsFortblView = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
       
        // priority master
        let aryTemp = dictDetailsFortblView.value(forKey: "PriorityMasters") as! NSArray
               
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryPriority.add(dict)
                }
            }
        }
        
        // task type master
        let aryTaskTypeTemp = dictDetailsFortblView.value(forKey: "TaskTypeMasters") as! NSArray
        
        if(aryTaskTypeTemp.count > 0)
        {
            for item in aryTaskTypeTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryTaskTypeMaster.add(dict)
                }
            }
        }
        
          // task status master(LeadStatusMasters)
        
    /*    let aryTaskStatusTemp = dictDetailsFortblView.value(forKey: "LeadStatusMasters") as! NSArray
        
        if(aryTaskStatusTemp.count > 0)
        {
            for item in aryTaskStatusTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryTaskStatusMaster.add(dict)
                }
            }
        }
        
        if(aryTaskStatusMaster.count > 0){
            
            let dict = NSMutableDictionary()
            dict.setValue("All", forKey: "StatusName")
            dict.setValue("0", forKey: "IsSelected")
            aryTaskStatusMaster.insert(dict, at: 0)
        }*/
        
        // commented on 28 April 2020
      /*  if let dict = nsud.value(forKey: "dictFilterTask")
        {
            if("\((dict as! NSDictionary).value(forKey: "fromDate")!)".count > 0 && "\((dict as! NSDictionary).value(forKey: "toDate")!)".count > 0)
            {
                strFromDate = "\((dict as! NSDictionary).value(forKey: "fromDate")!)"
                
                strToDate = "\((dict as! NSDictionary).value(forKey: "toDate")!)"
                
            }
        }*/
        
        showPrefilledData()
    }
    
    // MARK: UITableView delegate and datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: DeviceType.IS_IPAD ? 55 : 40))
        viewheader.backgroundColor = UIColor.groupTableViewBackground
        let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: DeviceType.IS_IPAD ? 55 : 40))
     
        //--------------
        let btnCheckAll = UIButton(frame: CGRect(x: self.tblviewFilterTask.frame.width - (DeviceType.IS_IPAD ? 65 : 50), y: 0, width: DeviceType.IS_IPAD ? 55 : 40, height: DeviceType.IS_IPAD ? 55 : 40))
        let lblAll = UILabel(frame: CGRect(x: self.tblviewFilterTask.frame.width - 90, y: 0, width: DeviceType.IS_IPAD ? 55 : 40, height: DeviceType.IS_IPAD ? 55 : 40))
        lblAll.text = "All"
        btnCheckAll.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
        btnCheckAll.tag = section
        btnCheckAll.addTarget(self, action: #selector(actionSelectAllData), for: .touchUpInside)
        //---------
        if(DeviceType.IS_IPAD){
            lblAll.font = UIFont.systemFont(ofSize: 21)
            lbl.font = UIFont.systemFont(ofSize: 21)
            
        }else{
            lblAll.font = UIFont.systemFont(ofSize: 18)
            lbl.font = UIFont.systemFont(ofSize: 18)
        }
             
        if(section == 0)
        {
            lbl.text = "User"
        }
        else if(section == 1)
        {
            lbl.text = "Time"//"Overdue"
        }
        else if(section == 2)
        {
            lbl.text = "Date"
        }
        else if(section == 3)
        {
            
            lbl.text = "Priority"
            viewheader.addSubview(btnCheckAll)
            viewheader.addSubview(lblAll)
            if(self.aryPriority.count == self.arySelectedPriority.count){
                                   btnCheckAll.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
                               }
        }
       /* else if(section == 4)
        {
            lbl.text = "Sort by"
        }*/
        else if(section == 4)
        {
            lbl.text = "Task Type"
            viewheader.addSubview(btnCheckAll)
            viewheader.addSubview(lblAll)
            if(self.aryTaskTypeMaster.count == self.arySelectedTaskType.count){
                        btnCheckAll.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            }
        }
        else if(section == 5)
        {
            lbl.text = "Status"
            viewheader.addSubview(btnCheckAll)
            viewheader.addSubview(lblAll)
            if(self.aryTaskStatus.count == self.arySelectedTaskStatus.count){
                btnCheckAll.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            }
        }
        else if(section == 6)
        {
            lbl.text = "Sort by"
        }
        viewheader.addSubview(lbl)
        return viewheader
    }
    @objc func actionSelectAllData (sender : UIButton){
        if(sender.tag == 3) //Priority
         {
            //Uncheck
            if(sender.currentImage == UIImage(named: "check_box_1New.png")){
                self.arySelectedPriority  = NSMutableArray()
                for item in self.aryPriority {
                    let dict = (item as AnyObject)as! NSDictionary
                    arySelectedPriority.add("\(dict.value(forKey: "PriorityId")!)")
                }
            }
            //Check
            else{
                   self.arySelectedPriority  = NSMutableArray()
            }
         }
      
         else if(sender.tag == 4) //"Task Type"
         {
            //Uncheck
            if(sender.currentImage == UIImage(named: "check_box_1New.png")){
                self.arySelectedTaskType  = NSMutableArray()
                for item in self.aryTaskTypeMaster {
                    let dict = (item as AnyObject)as! NSDictionary
                    arySelectedTaskType.add("\(dict.value(forKey: "TaskTypeId")!)")
                }
            }
            //Check
            else{
                   self.arySelectedTaskType  = NSMutableArray()
            }
         }
         else if(sender.tag == 5) //Status
         {
            //Uncheck
            if(sender.currentImage == UIImage(named: "check_box_1New.png")){
                  self.arySelectedTaskStatus  = NSMutableArray()
                for item in self.aryTaskStatus {
                    self.arySelectedTaskStatus.add("\(item)")
                }
            }
            //Check
            else{
                   self.arySelectedTaskStatus  = NSMutableArray()
            }
         }
        self.tblviewFilterTask.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 55 : 40
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0)
        {
            return aryUserName.count
        }
        if(section == 1)
        {
            return aryOverdue.count
        }
        if(section == 2)
        {
            return 1
        }
        if(section == 3)
        {
            return aryPriority.count
        }
       /* if(section == 4)
        {
          return arySortBy.count
        }*/
        if(section == 4)
        {
            return aryTaskTypeMaster.count
        }
        if(section == 5)
        {
            return aryTaskStatus.count
        }
        if(section == 6)
               {
                   return arySortBySegment.count
               }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTaskCell") as! FilterTaskCell
        
        if(indexPath.section == 0)
        {
            cell.lblFilterName.text =  "\(aryUserName[indexPath.row])"
            
        if(arySelectedUser.contains("\(aryUserName[indexPath.row])"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        else if(indexPath.section == 1)
        {
            cell.lblFilterName.text =  "\(aryOverdue[indexPath.row])"
            if(arySelectedOverDue.contains("\(aryOverdue[indexPath.row])"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        else if(indexPath.section == 2)
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "FilterTaskDateCell") as! FilterTaskDateCell
            
            cell1.btnFromDate.addTarget(self, action: #selector(actionOnFromDate), for: .touchUpInside)
            
            cell1.btnToDate.addTarget(self, action: #selector(actionOnToDate), for: .touchUpInside)
            
            cell1.btnFromDate.setTitle(strFromDate, for: .normal)
            cell1.btnToDate.setTitle(strToDate, for: .normal)
            cell1.btnClearDate.addTarget(self, action: #selector(actionOnClearDate), for: .touchUpInside)
            
            cell1.btnFromDate.layer.borderWidth = 1.0
            cell1.btnToDate.layer.borderWidth = 1.0
            cell1.btnFromDate.layer.borderColor = UIColor.lightGray.cgColor
            cell1.btnToDate.layer.borderColor = UIColor.lightGray.cgColor
            cell1.btnFromDate.layer.cornerRadius = 2.0
            cell1.btnToDate.layer.cornerRadius = 2.0
            
            if let dictForSort = nsud.value(forKey: "dictFilterTask"){
                
                if let overdue = (dictForSort as! NSDictionary).value(forKey: "overdue"){
                    
                    if("\(overdue)".count > 0){
                        cell1.btnFromDate.setTitle("", for: .normal)
                        cell1.btnToDate.setTitle("", for: .normal)
                    }
                }
            }
            
            return cell1
        }
        else if(indexPath.section == 3)
        {
            let dict = aryPriority.object(at: indexPath.row) as! NSDictionary
            cell.lblFilterName.text = "\(dict.value(forKey: "Name")!)"
            if(arySelectedPriority.contains("\(dict.value(forKey: "PriorityId")!)"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
            
       /* else if(indexPath.section == 4){
            cell.lblFilterName.text =  "\(arySortBy[indexPath.row])"
         if(arySelectedSortBy.contains("\(arySortBy[indexPath.row])"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }*/
        else if(indexPath.section == 4)
        {
            let dict = aryTaskTypeMaster.object(at: indexPath.row) as! NSDictionary
            cell.lblFilterName.text = "\(dict.value(forKey: "Name")!)"
            if(arySelectedTaskType.contains("\(dict.value(forKey: "TaskTypeId")!)"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        
        else if(indexPath.section == 5)
        {
            cell.lblFilterName.text = "\(aryTaskStatus[indexPath.row])"
        if(arySelectedTaskStatus.contains("\(aryTaskStatus[indexPath.row])"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        else if(indexPath.section == 6)
               {
                
                let dict = (arySortBySegment[indexPath.row]as! NSDictionary)
                cell.lblFilterName.text = "\(dict.value(forKey: "title")!)"
                if(nsud.value(forKey: "TaskFilterSort") != nil){
                    if("\(dict.value(forKey: "id")!)" == "\((nsud.value(forKey: "TaskFilterSort")as! NSDictionary).value(forKey: "id")!)"){
                        cell.accessoryType = .checkmark

                    }
                  else{
                        cell.accessoryType = .none
                    }
                }else{
                    cell.accessoryType = .none
                }
               }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.section == 2)
        {
            return DeviceType.IS_IPAD ? 150 : 110
        }
        return DeviceType.IS_IPAD ? 80 : 50
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        cell.alpha = 0.4
    //        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    //        UIView.animate(withDuration: 1.0) {
    //            cell.alpha = 1
    //            cell.transform = .identity
    //        }
    //    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if(indexPath.section == 0)
        {
            let str = aryUserName.object(at: indexPath.row)
            if(arySelectedUser.contains(str))
            {
                arySelectedUser.remove(str)
            }
            else
            {
                arySelectedUser.removeAllObjects()
                arySelectedUser.add(str)
            }
        }
        else if(indexPath.section == 1)
        {
            let str = aryOverdue.object(at: indexPath.row)
            strFromDate = ""
            strToDate = ""
            if(arySelectedOverDue.contains(str))
            {
                arySelectedOverDue.remove(str)
            }
            else
            {
                /*arySelectedOverDue.removeAllObjects()
                arySelectedSortBy.removeAllObjects()
                arySelectedPriority.removeAllObjects()*/
                arySelectedOverDue.removeAllObjects()
                arySelectedOverDue.add(str)
            }
        }
        if(indexPath.section == 2)
        {// date wala section
            
        }
        else if(indexPath.section == 3)
        {
            let dict = aryPriority.object(at: indexPath.row) as! NSDictionary
           // strFromDate = ""
           // strToDate = ""
            
            if(arySelectedPriority.contains("\(dict.value(forKey: "PriorityId")!)"))
            {
                arySelectedPriority.remove("\(dict.value(forKey: "PriorityId")!)")
            }
            else
            {
              /*  arySelectedOverDue.removeAllObjects()
                arySelectedSortBy.removeAllObjects()*/
                arySelectedPriority.add("\(dict.value(forKey: "PriorityId")!)")
            }
        }
      /*  else if(indexPath.section == 4)
        {
            let str = arySortBy.object(at: indexPath.row)
            strFromDate = ""
            strToDate = ""
            if(arySelectedSortBy.contains(str))
            {
                arySelectedSortBy.remove(str)
            }
            else
            {
                arySelectedOverDue.removeAllObjects()
                arySelectedPriority.removeAllObjects()
                arySelectedSortBy.removeAllObjects()
                arySelectedSortBy.add(str)
            }
        }*/
        else if(indexPath.section == 4)
        {
            let dict = aryTaskTypeMaster.object(at: indexPath.row) as! NSDictionary
          //  strFromDate = ""
          //  strToDate = ""
            
            if(arySelectedTaskType.contains("\(dict.value(forKey: "TaskTypeId")!)"))
            {
                arySelectedTaskType.remove("\(dict.value(forKey: "TaskTypeId")!)")
            }
            else
            {
               /* arySelectedOverDue.removeAllObjects()
                arySelectedSortBy.removeAllObjects()
                arySelectedPriority.removeAllObjects()*/
                arySelectedTaskType.add("\(dict.value(forKey: "TaskTypeId")!)")
            }
        }
            
        else if(indexPath.section == 5)
        { if(arySelectedTaskStatus.contains("\(aryTaskStatus[indexPath.row])"))
        {
            arySelectedTaskStatus.remove("\(aryTaskStatus[indexPath.row])")
        }
        else
        {                arySelectedTaskStatus.add("\(aryTaskStatus[indexPath.row])")
            
            }
        }
        else if(indexPath.section == 6){
            
            let dict = (arySortBySegment[indexPath.row]as! NSDictionary)
            if(nsud.value(forKey: "TaskFilterSort") != nil){
                if("\(dict.value(forKey: "id")!)" == "\((nsud.value(forKey: "TaskFilterSort")as! NSDictionary).value(forKey: "id")!)"){
                    nsud.setValue(nil, forKey: "TaskFilterSort")
                }
                else{
                    nsud.setValue((arySortBySegment[indexPath.row]as! NSDictionary), forKey: "TaskFilterSort")
                }
            }else{
                nsud.setValue((arySortBySegment[indexPath.row]as! NSDictionary), forKey: "TaskFilterSort")
            }
            
        }
        tblviewFilterTask.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    // MARK: UIButton action
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionOnClearAll(_ sender: UIButton) {
        let alert = UIAlertController(title: alertMessage, message: alertFilterClear, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            self.delegate?.getDataTask_iPhone_Protocol(dictData: NSDictionary(), tag: 0)
            self.navigationController?.popViewController(animated: false)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
       }
    @IBAction func actionOnApply(_ sender: UIButton) {
        
        if(arySelectedUser.count == 0 && arySelectedOverDue.count == 0 && strFromDate.count == 0 && strToDate.count == 0 && arySelectedPriority.count == 0 && arySelectedTaskType.count == 0 && arySelectedTaskStatus.count == 0){
            if(nsud.value(forKey: "TaskFilterSort") != nil){
                self.navigationController?.popViewController(animated: false)

            }else{
                  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select any condition to apply filter.", viewcontrol: self)
            }
         
        }
        else{
            let dictFilter = NSMutableDictionary()
            dictFilter.setValue("", forKey: "overdue")
            if arySelectedUser.count > 0
                   {
                       if("\(arySelectedUser.firstObject!)" == "Everyone"){
                           dictFilter.setValue("", forKey: "user")
                       }
                       else{
                           dictFilter.setValue("\(strEmpID)", forKey: "user")
                       }
                   }
                   else
                   {
                       dictFilter.setValue("", forKey: "user")
                   }
                   if arySelectedOverDue.count > 0
                   {
                       if("\(arySelectedOverDue.firstObject!)" == "Today"){
                           let objDelegate = StasticsClass()
                           objDelegate.delegate = self as StasticsClassDelegate
                           objDelegate.getStart_EndDate("Today")
                           dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                           dictFilter.setValue("\(strToDate)", forKey: "toDate")
                           dictFilter.setValue("Today", forKey: "overdue")
                       }
                       else if("\(arySelectedOverDue.firstObject!)" == "Tomorrow"){
                           
                          let objDelegate = StasticsClass()
                           objDelegate.delegate = self as StasticsClassDelegate
                           objDelegate.getStart_EndDate("Tomorrow")
                           
                           dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                           dictFilter.setValue("\(strToDate)", forKey: "toDate")
                          dictFilter.setValue("Tomorrow", forKey: "overdue")
                       }
                       else if("\(arySelectedOverDue.firstObject!)" == "This week"){
                           
                           let objDelegate = StasticsClass()
                           objDelegate.delegate = self as StasticsClassDelegate
                           objDelegate.getStart_EndDate("This Week")
                           
                           dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                           dictFilter.setValue("\(strToDate)", forKey: "toDate")
                           dictFilter.setValue("This week", forKey: "overdue")
                       }
                       else if("\(arySelectedOverDue.firstObject!)" == "Next week"){
                           
                           let objDelegate = StasticsClass()
                           objDelegate.delegate = self as StasticsClassDelegate
                           objDelegate.getStart_EndDate("Next Week")
                           
                           dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                           dictFilter.setValue("\(strToDate)", forKey: "toDate")
                           dictFilter.setValue("Next week", forKey: "overdue")
                       }
                   }
                   else
                   {
                       dictFilter.setValue("", forKey: "fromDate")
                       dictFilter.setValue("", forKey: "toDate")
                       
                       //dictFilter.setValue("", forKey: "overdue")
                   }
                   
                   if(strFromDate.count > 0 && strToDate.count == 0)
                   {
                       showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select To Date", viewcontrol: self)
                    return
                       
                   }
                   if(strFromDate.count == 0 && strToDate.count > 0)
                   {
                       showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select From Date", viewcontrol: self)
                    return
                   }
                   
                   if(strFromDate.count > 0 && strToDate.count > 0)
                   {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yyyy"
                    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                    let fromDate = dateFormatter.date(from: strFromDate)
                    let toDate = dateFormatter.date(from: strToDate)
                    
                    if(fromDate! > toDate!){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To Date should be greater than From Date.", viewcontrol: self)
                        return
                    }
                    
                    dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                    dictFilter.setValue("\(strToDate)", forKey: "toDate")
                   }
                   else
                   {
                       dictFilter.setValue("", forKey: "fromDate")
                       dictFilter.setValue("", forKey: "toDate")
                   }
                   
                   if arySelectedPriority.count > 0
                   {
                       dictFilter.setValue(arySelectedPriority, forKey: "priority")
                   }
                   else
                   {
                        dictFilter.setValue(NSMutableArray(), forKey: "priority")
                   }
                   
                  /* if arySelectedSortBy.count > 0
                   {
                       dictFilter.setValue("\(arySelectedSortBy.firstObject!)", forKey: "sortby")
                   }
                   else
                   {
                       dictFilter.setValue("", forKey: "sortby")
                   }*/
                   
                   if arySelectedTaskType.count > 0
                   {
                       dictFilter.setValue(arySelectedTaskType, forKey: "tasktypeid")
                   }
                   else
                   {
                       dictFilter.setValue(NSMutableArray(), forKey: "tasktypeid")
                   }
                   
                   if arySelectedTaskStatus.count > 0
                   {
                       dictFilter.setValue(arySelectedTaskStatus, forKey: "taskstatus")
                   }
                   else
                   {
                       dictFilter.setValue(NSMutableArray(), forKey: "taskstatus")
                   }
                           

//            nsud.setValue(dictFilter, forKey: "dictFilterTask")
//            nsud.synchronize()
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TaskAdded_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
            delegate?.getDataTask_iPhone_Protocol(dictData: dictFilter, tag: 0)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @objc func actionOnFromDate(sender: UIButton!) {
        
        //arySelectedPriority.removeAllObjects()
        arySelectedSortBy.removeAllObjects()
        isFromDate = true
        gotoDatePickerView(sender: sender, strType: "Date")
        
    }
    @objc func actionOnToDate(sender: UIButton!) {
        
       // arySelectedPriority.removeAllObjects()
        arySelectedSortBy.removeAllObjects()
        isFromDate = false
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    
    @objc func actionOnClearDate(sender: UIButton!)
    {
        strFromDate = ""
        strToDate = ""
        tblviewFilterTask.reloadData()
    }
    
    // MARK: Functions
    
    fileprivate func showPrefilledData(){
        
        if  let dict = nsud.value(forKey: "dictFilterTask")
        {
            if(dict is NSDictionary){
                if((dict as! NSDictionary).count > 0){
                    
                    let dictForSort = (dict as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    
                    if let user = dictForSort.value(forKey: "user"){
                        if("\(user)".count > 0){
                            
                            arySelectedUser.add(strUserName)
                        }else{
                            arySelectedUser.add("Everyone")
                        }
                    }
                    
                    if let taskStatus = dictForSort.value(forKey: "taskstatus")
                    {
                        if(taskStatus as! NSArray).count > 0{
                            
                            arySelectedTaskStatus = (taskStatus as! NSArray).mutableCopy() as! NSMutableArray
                        }
                    }
                    
                    if let taskType = dictForSort.value(forKey: "tasktypeid")
                    {
                        if(taskType as! NSArray).count > 0{
                            
                            arySelectedTaskType = (taskType as! NSArray).mutableCopy() as! NSMutableArray
                        }
                    }
                    
                    if let priority = dictForSort.value(forKey: "priority")
                    {
                        if(priority is NSArray){
                            
                            if(priority as! NSArray).count > 0{
                                
                                arySelectedPriority = (priority as! NSArray).mutableCopy() as! NSMutableArray
                            }
                        }
                    }
                    
                    if let fromdate = dictForSort.value(forKey: "fromDate"){
                        
                        if("\(fromdate)".count > 0){
                            
                            if let overdue = dictForSort.value(forKey: "overdue"){
                                
                                if("\(overdue)" == "Today"){
                                    
                                    arySelectedOverDue.add("Today")
                                }
                                else if("\(overdue)" == "Tomorrow"){
                                    
                                    arySelectedOverDue.add("Tomorrow")
                                }
                                    
                                else if("\(overdue)" == "This week"){
                                    
                                    arySelectedOverDue.add("This week")
                                }
                                else if("\(overdue)" == "Next week"){
                                    
                                    arySelectedOverDue.add("Next week")
                                }
                                else{
                                    
                                    strFromDate =  "\(dictForSort.value(forKey: "fromDate")!)"
                                    strToDate =  "\(dictForSort.value(forKey: "toDate")!)"
                                }
                            }
                            else{
                                
                                strFromDate =  "\(dictForSort.value(forKey: "fromDate")!)"
                                strToDate =  "\(dictForSort.value(forKey: "toDate")!)"
                            }
                        }
                    }
                }
            }
        }
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
}

extension FilterTaskVC_CRMNew_iPhone: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if  let dictForSort = nsud.value(forKey: "dictFilterTask") {
            
            if(dictForSort is NSDictionary){
                
                if((dictForSort as! NSDictionary).count > 0){
                    
                    let dict = (dictForSort as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    
                    if let days = dict.value(forKey: "overdue"){
                        if("\(days)".count > 0){
                           dict.setValue("", forKey: "overdue")
                           strFromDate = ""
                           strToDate = ""
                           nsud.setValue(dict, forKey: "dictFilterTask")
                           nsud.synchronize()
                        }
                    }
                }
            }
        }
       
        if isFromDate == true {
            
            strFromDate = strDate
        }
        else
        {
            strToDate = strDate
        }
        
        arySelectedOverDue.removeAllObjects()
       // arySelectedPriority.removeAllObjects()
       // arySelectedSortBy.removeAllObjects()
        tblviewFilterTask.reloadData()
    }
}
extension FilterTaskVC_CRMNew_iPhone : StasticsClassDelegate
{
    
    func getData(_ dict: [AnyHashable : Any]!)
    {
        let dictNew = dict! as NSDictionary
        
        strFromDate = "\(dictNew.value(forKey: "StartDate") ?? "")"
        strToDate = "\(dictNew.value(forKey: "EndDate") ?? "")"
    }
    
}
