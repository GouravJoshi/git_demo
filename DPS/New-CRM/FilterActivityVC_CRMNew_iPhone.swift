//
//  FilterActivityVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 25/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit


protocol FilterActivity_iPhone_Protocol : class
{
    func getDataFilterActivity_iPhone_Protocol(dictData : NSDictionary ,tag : Int)
}



class FilterActivityVC_CRMNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    weak var delegate: FilterActivity_iPhone_Protocol?

    // outlets
    @IBOutlet weak var tblviewFilterActivity: UITableView!
    
    // variables
    fileprivate let aryUserName = NSMutableArray()
    fileprivate  let aryDays : NSMutableArray! = ["Today", "Tomorrow","This week", "Next week"]
    
    fileprivate  let aryActivityLogType = NSMutableArray()
    
    fileprivate  let arySortBy : NSMutableArray!  = ["A to Z", "Z to A"]
    fileprivate  var strFromDate = ""
    fileprivate  var strToDate = ""
    fileprivate  var isFromDate = false
    fileprivate  var aryIndexPath = NSMutableArray()
    fileprivate  var dictLoginData = NSDictionary()
    fileprivate  var strUserName = ""
    
    fileprivate var arySelectedUser = NSMutableArray()
    fileprivate var arySelectedDays = NSMutableArray()
    fileprivate var arySelectedActivityLogType = NSMutableArray()
    fileprivate var arySelectedSortBy = NSMutableArray()
    fileprivate var strEmpID = ""
    
    // MARK: view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
                
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username")!)"
        aryUserName.add(strUserName)
        aryUserName.add("Everyone")
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        getActivityLogTypeFromMaster()
        
       /* if let dict = nsud.value(forKey: "dictFilterActivity")
        {
            if("\((dict as! NSDictionary).value(forKey: "fromDate")!)".count > 0 && "\((dict as! NSDictionary).value(forKey: "toDate")!)".count > 0)
            {
                strFromDate = "\((dict as! NSDictionary).value(forKey: "fromDate")!)"
                
                strToDate = "\((dict as! NSDictionary).value(forKey: "toDate")!)"
            }
        }
        */
        showPrefilledData()
    }
    
    // MARK: UITableView delegate and datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: DeviceType.IS_IPAD ? 55 : 40))
        viewheader.backgroundColor = UIColor.groupTableViewBackground
        
        let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: DeviceType.IS_IPAD ? 55 : 40))
       
        //--------------
        let btnCheckAll = UIButton(frame: CGRect(x: self.tblviewFilterActivity.frame.width - (DeviceType.IS_IPAD ? 65 : 50), y: 0, width: DeviceType.IS_IPAD ? 55 : 40, height: DeviceType.IS_IPAD ? 55 : 40))
        let lblAll = UILabel(frame: CGRect(x: self.tblviewFilterActivity.frame.width - 90, y: 0, width: DeviceType.IS_IPAD ? 55 : 40, height: DeviceType.IS_IPAD ? 55 : 40))
        lblAll.text = "All"
        btnCheckAll.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
        btnCheckAll.tag = section
        btnCheckAll.addTarget(self, action: #selector(actionSelectAllData), for: .touchUpInside)
        //---------
        if(DeviceType.IS_IPAD){
            lblAll.font = UIFont.systemFont(ofSize: 21)
            lbl.font = UIFont.systemFont(ofSize: 21)

        }else{
            lblAll.font = UIFont.systemFont(ofSize: 18)
            lbl.font = UIFont.systemFont(ofSize: 18)
        }
        
        if(section == 0)
        {
            lbl.text = "User"
        }
        else if(section == 1)
        {
            lbl.text = "Days"
        }
        else if(section == 2)
        {
            lbl.text = "Date"
        }
        else if(section == 3)
        {
            lbl.text = "Activity Type"
            viewheader.addSubview(btnCheckAll)
            viewheader.addSubview(lblAll)
            if(self.aryActivityLogType.count == self.arySelectedActivityLogType.count){
                btnCheckAll.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            }
        }
      /*  else
        {
            lbl.text = "Sort by"
        }
        */
        viewheader.addSubview(lbl)
        return viewheader
    }
    @objc func actionSelectAllData (sender : UIButton){
           if(sender.tag == 3) //Activity Type
            {
               //Uncheck
               if(sender.currentImage == UIImage(named: "check_box_1New.png")){
                   self.arySelectedActivityLogType  = NSMutableArray()
                   for item in self.aryActivityLogType {
                       let dict = (item as AnyObject)as! NSDictionary
                       arySelectedActivityLogType.add("\(dict.value(forKey: "LogTypeId")!)")
                   }
               }
               //Check
               else{
                      self.arySelectedActivityLogType  = NSMutableArray()
               }
            }
        
           self.tblviewFilterActivity.reloadData()
       }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 55 : 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0)
        {
            return aryUserName.count
        }
        if(section == 1)
        {
            return aryDays.count
        }
        if(section == 2)
        {
            return 1
        }
        if(section == 3)
        {
            return aryActivityLogType.count
        }
        
        return 0 //arySortBy.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTaskCell") as! FilterTaskCell
        
        if(indexPath.section == 0)
        {
            cell.lblFilterName.text =  "\(aryUserName[indexPath.row])"
            
            if(arySelectedUser.contains("\(aryUserName[indexPath.row])"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        else  if(indexPath.section == 1)
        {
            cell.lblFilterName.text =  "\(aryDays[indexPath.row])"
            if(arySelectedDays.contains("\(aryDays[indexPath.row])"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        else  if(indexPath.section == 2)
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "FilterTaskDateCell") as! FilterTaskDateCell
            
            cell1.btnFromDate.addTarget(self, action: #selector(actionOnFromDate), for: .touchUpInside)
            
            cell1.btnToDate.addTarget(self, action: #selector(actionOnToDate), for: .touchUpInside)
            cell1.btnClearDate.addTarget(self, action: #selector(actionOnClearDate), for: .touchUpInside)
            cell1.btnFromDate.setTitle(strFromDate, for: .normal)
            cell1.btnToDate.setTitle(strToDate, for: .normal)
            
            cell1.btnFromDate.layer.borderWidth = 1.0
            cell1.btnToDate.layer.borderWidth = 1.0
            cell1.btnFromDate.layer.borderColor = UIColor.lightGray.cgColor
            cell1.btnToDate.layer.borderColor = UIColor.lightGray.cgColor
            cell1.btnFromDate.layer.cornerRadius = 2.0
            cell1.btnToDate.layer.cornerRadius = 2.0
         
            if let dictForSort = nsud.value(forKey: "dictFilterActivity"){
                
                if let days = (dictForSort as! NSDictionary).value(forKey: "days"){
                    
                    if("\(days)".count > 0){
                        cell1.btnFromDate.setTitle("", for: .normal)
                        cell1.btnToDate.setTitle("", for: .normal)
                    }
                }
            }
            
            return cell1
        }
        else if(indexPath.section == 3)
        {
            let dict = aryActivityLogType.object(at: indexPath.row) as! NSDictionary
            cell.lblFilterName.text = "\(dict.value(forKey: "Name")!)"
            if(arySelectedActivityLogType.contains("\(dict.value(forKey: "LogTypeId")!)"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        /*else{
            cell.lblFilterName.text =  "\(arySortBy[indexPath.row])"
            
            if(arySelectedSortBy.contains("\(arySortBy[indexPath.row])"))
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        */
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.section == 2)
        {
            return DeviceType.IS_IPAD ? 150 : 110
        }
        return DeviceType.IS_IPAD ? 80 : 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if(indexPath.section == 0)
        {
            let str = aryUserName.object(at: indexPath.row)
            if(arySelectedUser.contains(str))
            {
                arySelectedUser.remove(str)
            }
            else
            {
                arySelectedUser.removeAllObjects()
                arySelectedUser.add(str)
            }
        }
            
        else if(indexPath.section == 1)
        {
            strFromDate = ""
            strToDate = ""
            let str = aryDays.object(at: indexPath.row)
            if(arySelectedDays.contains(str))
            {
                arySelectedDays.remove(str)
            }
            else
            {
                
               /* arySelectedActivityLogType.removeAllObjects()
                arySelectedSortBy.removeAllObjects()*/
                arySelectedDays.removeAllObjects()
                arySelectedDays.add(str)
            }
        }
            
        else if(indexPath.section == 2)
        {// fromDate and toDate
            
        }
        else if(indexPath.section == 3)
        {
          //  strFromDate = ""
          //  strToDate = ""
            let dict = aryActivityLogType.object(at: indexPath.row) as! NSDictionary
            
            if(arySelectedActivityLogType.contains("\(dict.value(forKey: "LogTypeId")!)"))
            {
                arySelectedActivityLogType.remove("\(dict.value(forKey: "LogTypeId")!)")
            }
            else
            {
              //  arySelectedDays.removeAllObjects()
               // arySelectedSortBy.removeAllObjects()
                arySelectedActivityLogType.add("\(dict.value(forKey: "LogTypeId")!)")
            }
        }
      /*  else if(indexPath.section == 4)
        {
            strFromDate = ""
            strToDate = ""
            let str = arySortBy.object(at: indexPath.row)
            if(arySelectedSortBy.contains(str))
            {
                arySelectedSortBy.remove(str)
            }
            else
            {
                arySelectedDays.removeAllObjects()
                arySelectedActivityLogType.removeAllObjects()
                arySelectedSortBy.removeAllObjects()
                arySelectedSortBy.add(str)
            }
        }*/
        tblviewFilterActivity.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    // MARK: UIButton action
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionOnClearAll(_ sender: UIButton) {
        let alert = UIAlertController(title: alertMessage, message: alertFilterClear, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            self.delegate?.getDataFilterActivity_iPhone_Protocol(dictData: NSDictionary(), tag: 0)
            self.navigationController?.popViewController(animated: false)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func actionOnApply(_ sender: UIButton) {
        
        if(arySelectedUser.count == 0 && arySelectedDays.count == 0 && strFromDate.count == 0 && strToDate.count == 0 && arySelectedActivityLogType.count == 0){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select any condition to apply filter.", viewcontrol: self)
            
        }
            
        else{
            
            let dictFilter = NSMutableDictionary()
            dictFilter.setValue("", forKey: "days")
            
            if arySelectedUser.count > 0
            {
                if("\(arySelectedUser.firstObject!)" == "Everyone"){
                    
                    dictFilter.setValue("", forKey: "user")
                }
                else{
                    
                    dictFilter.setValue("\(strEmpID)", forKey: "user")
                }
            }
            else
            {
                dictFilter.setValue("", forKey: "user")
            }
            
            if arySelectedDays.count > 0
            {
                if("\(arySelectedDays.firstObject!)" == "Today"){
                    
                    let objDelegate = StasticsClass()
                    objDelegate.delegate = self as StasticsClassDelegate
                    objDelegate.getStart_EndDate("Today")
                    
                    dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                    dictFilter.setValue("\(strToDate)", forKey: "toDate")
                    dictFilter.setValue("Today", forKey: "days")
                }
                else if("\(arySelectedDays.firstObject!)" == "Tomorrow"){
                    
                    let objDelegate = StasticsClass()
                    objDelegate.delegate = self as StasticsClassDelegate
                    objDelegate.getStart_EndDate("Tomorrow")
                    
                    dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                    dictFilter.setValue("\(strToDate)", forKey: "toDate")
                    dictFilter.setValue("Tomorrow", forKey: "days")
                }
                else if("\(arySelectedDays.firstObject!)" == "This week"){
                    
                    let objDelegate = StasticsClass()
                    objDelegate.delegate = self as StasticsClassDelegate
                    objDelegate.getStart_EndDate("This Week")
                    
                    dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                    dictFilter.setValue("\(strToDate)", forKey: "toDate")
                 
                    dictFilter.setValue("This week", forKey: "days")
                }
                else if("\(arySelectedDays.firstObject!)" == "Next week"){
                    
                    let objDelegate = StasticsClass()
                    objDelegate.delegate = self as StasticsClassDelegate
                    objDelegate.getStart_EndDate("Next Week")
                    
                    dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                    dictFilter.setValue("\(strToDate)", forKey: "toDate")
                    dictFilter.setValue("Next week", forKey: "days")
                }
            }
            else
            {
                dictFilter.setValue("", forKey: "fromDate")
                dictFilter.setValue("", forKey: "toDate")
            }
            
            
            // pehle days chalta h
            /*   if arySelectedDays.count > 0
             {
             dictFilter.setValue("\(arySelectedDays.firstObject!)", forKey: "days")
             }
             else
             {
             dictFilter.setValue("", forKey: "days")
             }*/
            
            if(strFromDate.count > 0 && strToDate.count == 0)
            {
                 
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select To Date", viewcontrol: self)
                return
                
            }
            if(strFromDate.count == 0 && strToDate.count > 0)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select From Date", viewcontrol: self)
                return
            }
            
            if(strFromDate.count > 0 && strToDate.count > 0)
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                let fromDate = dateFormatter.date(from: strFromDate)
                let toDate = dateFormatter.date(from: strToDate)
                
                if(fromDate! > toDate!){
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To Date should be greater than From Date.", viewcontrol: self)
                    return
                }
                
                dictFilter.setValue("\(strFromDate)", forKey: "fromDate")
                dictFilter.setValue("\(strToDate)", forKey: "toDate")
            }
            else
            {
                dictFilter.setValue("", forKey: "fromDate")
                dictFilter.setValue("", forKey: "toDate")
            }
            
            if arySelectedActivityLogType.count > 0
            {
                dictFilter.setValue(arySelectedActivityLogType, forKey: "logTypeId")
            }
            else
            {
                dictFilter.setValue(NSMutableArray(), forKey: "logTypeId")
            }
            
            /* if arySelectedSortBy.count > 0
             {
             dictFilter.setValue("\(arySelectedSortBy.firstObject!)", forKey: "sortby")
             }
             else
             {
             dictFilter.setValue("", forKey: "sortby")
             }
             */
//            nsud.setValue(dictFilter, forKey: "dictFilterActivity")
//            nsud.synchronize()
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ActivityAdded_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
//
            delegate?.getDataFilterActivity_iPhone_Protocol(dictData: dictFilter, tag: 0)
            self.navigationController?.popViewController(animated: false)
            
        }
      
    }
    
    @objc func actionOnFromDate(sender: UIButton!) {
        
     //   arySelectedSortBy.removeAllObjects()
        arySelectedDays.removeAllObjects()
      //  arySelectedActivityLogType.removeAllObjects()
        
        isFromDate = true
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    @objc func actionOnToDate(sender: UIButton!) {
        
      //  arySelectedSortBy.removeAllObjects()
        arySelectedDays.removeAllObjects()
       // arySelectedActivityLogType.removeAllObjects()
        
        isFromDate = false
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    
    @objc func actionOnClearDate(sender: UIButton!)
    {
        strFromDate = ""
        strToDate = ""
        tblviewFilterActivity.reloadData()
    }
    
    // MARK: Functions
    
    fileprivate func showPrefilledData(){
        
        if let dict = nsud.value(forKey: "dictFilterActivity") {
            
            if(dict is NSDictionary){
                
                if((dict as! NSDictionary).count > 0){
                    
                    let dictForSort = (dict as! NSDictionary)
                    if let user = dictForSort.value(forKey: "user"){
                        if("\(user)".count > 0){
                            
                            arySelectedUser.add(strUserName)
                        }else{
                            arySelectedUser.add("Everyone")
                        }
                    }
                    
                    if let logtype = dictForSort.value(forKey: "logTypeId")
                    {
                        if(logtype is NSArray){
                            
                            if(logtype as! NSArray).count > 0{
                                
                                arySelectedActivityLogType = (logtype as! NSArray).mutableCopy() as! NSMutableArray
                            }
                        }
                    }
                    
                    if let fromdate = dictForSort.value(forKey: "fromDate"){
                        
                        if("\(fromdate)".count > 0){
                            
                            if let overdue = dictForSort.value(forKey: "days"){
                                
                                if("\(overdue)" == "Today"){
                                    
                                    arySelectedDays.add("Today")
                                }
                                else if("\(overdue)" == "Tomorrow"){
                                    
                                    arySelectedDays.add("Tomorrow")
                                }
                                    
                                else if("\(overdue)" == "This week"){
                                    
                                    arySelectedDays.add("This week")
                                }
                                else if("\(overdue)" == "Next week"){
                                    
                                    arySelectedDays.add("Next week")
                                }
                                else{
                                    
                                    strFromDate =  "\(dictForSort.value(forKey: "fromDate")!)"
                                    strToDate =  "\(dictForSort.value(forKey: "toDate")!)"
                                }
                            }
                            else{
                                
                                strFromDate =  "\(dictForSort.value(forKey: "fromDate")!)"
                                strToDate =  "\(dictForSort.value(forKey: "toDate")!)"
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func getActivityLogTypeFromMaster()
    {
        
        let dictDetailsFortblView = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        
        let aryTemp = dictDetailsFortblView.value(forKey: "ActivityLogTypeMasters") as! NSArray
        
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryActivityLogType.add(dict)
                }
            }
        }
    }
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
}

extension FilterActivityVC_CRMNew_iPhone: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
            
        if let dictForSort = nsud.value(forKey: "dictFilterActivity") {
            
            if(dictForSort is NSDictionary){
                if((dictForSort as! NSDictionary).count > 0){
                    let dictTemp = (dictForSort as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    
                    if let days = dictTemp.value(forKey: "days"){
                        if("\(days)".count > 0){
                           dictTemp.setValue("", forKey: "days")
                           strFromDate = ""
                           strToDate = ""
                           nsud.setValue(dictTemp, forKey: "dictFilterActivity")
                           nsud.synchronize()
                        }
                    }
                }
            }
        }
        
        if isFromDate == true {
            
            strFromDate = strDate
        }
        else
        {
            strToDate = strDate
        }
        
        arySelectedDays.removeAllObjects()
     //   arySelectedActivityLogType.removeAllObjects()
       // arySelectedSortBy.removeAllObjects()
        
        tblviewFilterActivity.reloadData()
    }

}

extension FilterActivityVC_CRMNew_iPhone : StasticsClassDelegate
{
    func getData(_ dict: [AnyHashable : Any]!)
    {
        let dictNew = dict! as NSDictionary
        
        strFromDate = "\(dictNew.value(forKey: "StartDate") ?? "")"
        strToDate = "\(dictNew.value(forKey: "EndDate") ?? "")"
    }
    
}
