//
//  OpportunityDetailsVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 02/12/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import AVKit
enum OpportunityDetailType
{
    case Task, Notes,Document,Details, GeneralNew, DocumentsNew, DetailsNew, Timeline
    
}

class CellOpportunityDetailsCustomerInfo:UITableViewCell
{
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPrimaryPhone: UILabel!
    @IBOutlet weak var lblSecondaryPhone: UILabel!
    @IBOutlet weak var lblCell: UILabel!
    @IBOutlet weak var lblPrimaryEmail: UILabel!
    @IBOutlet weak var lblSecondaryEmail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    //New
    @IBOutlet weak var lblUrgency: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblTechNote: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblDueTime: UILabel!
    @IBOutlet weak var lblFlowType: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    //New
    
    @IBOutlet weak var viewOppPrimaryPhone: UIView!
    @IBOutlet weak var viewOppSecondaryPhone: UIView!
    @IBOutlet weak var viewOppCellPhone: UIView!
    @IBOutlet weak var viewOppPrimaryEmail: UIView!
    @IBOutlet weak var viewOppSecondaryEmail: UIView!
    @IBOutlet weak var viewOppUrgency: UIView!
    @IBOutlet weak var viewOppService: UIView!
    @IBOutlet weak var viewOppTechNote: UIView!
    @IBOutlet weak var viewOppFlowType: UIView!
    @IBOutlet weak var viewOppDueDate: UIView!
    @IBOutlet weak var viewOppDueTime: UIView!
    @IBOutlet weak var viewOppCreatedBy: UIView!
    
    @IBOutlet weak var btnDetailMsgPrimaryPhone: UIButton!
    @IBOutlet weak var btnDetailCallPrimaryPhone: UIButton!
    
    @IBOutlet weak var btnDetailMsgSecondaryPhone: UIButton!
    @IBOutlet weak var btnDetailCallSecondaryPhone: UIButton!
    
    @IBOutlet weak var btnDetailMsgCell: UIButton!
    @IBOutlet weak var btnDetailCallCell: UIButton!
    
    @IBOutlet weak var btnDetailPrimaryEmail: UIButton!
    @IBOutlet weak var btnDetailSecondaryEmail: UIButton!
    //progressConfidenceLvel
    
    //OpportunityDetail New
    @IBOutlet weak var lblOppNoNew: UILabel!
    @IBOutlet weak var lblAccNoNew: UILabel!
    @IBOutlet weak var lblServiceNameNew: UILabel!
    @IBOutlet weak var lblUrgencyNameNew: UILabel!
    @IBOutlet weak var lblTechNoteNew: UILabel!
    @IBOutlet weak var lblTypeNew: UILabel!
    @IBOutlet weak var lblCreatedByNew: UILabel!
    
    @IBOutlet weak var lblCreatedDateTimeNew: UILabel!
    
}

class CellOpportunityDetailsOpportunityInfo:UITableViewCell
{
    
   /* @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblStage: UILabel!
    @IBOutlet weak var lblUrgency: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblTechNote: UILabel!
    @IBOutlet weak var lblFollowUpDate: UILabel!*/
    
    
}

class OpportunityDetailsVC_CRMNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate
{
    
    
    // MARK: - ----------------------------------- Outlets Variables -----------------------------------
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblOpportunityName: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblSubmittedBy: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblClosingDate: UILabel!
    @IBOutlet weak var lblProgressValue: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblOpportunityTypeType: UILabel!
    @IBOutlet weak var btnEditCompany: UIButton!
    
    @IBOutlet weak var viewBtnContainer: UIView!
    @IBOutlet weak var btnTask: UIButton!
    @IBOutlet weak var btnNotes: UIButton!
    @IBOutlet weak var btnDocument: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var tblviewOpportunityDetails: UITableView!
    
    @IBOutlet weak var btnGeneralNew: UIButton!
    @IBOutlet weak var btnDocumentsNew: UIButton!
    @IBOutlet weak var btnDetailsNew: UIButton!
    @IBOutlet weak var const_LblBar_L: NSLayoutConstraint!
    
    
    //New
    @IBOutlet weak var progressConfidenceLvel: UISlider!
    @IBOutlet weak var lblOpportunityStatus: UILabel!
    @IBOutlet weak var lblOpportunityStage: UILabel!
    @IBOutlet weak var btnScheduleNow: UIButton!
    
    @IBOutlet weak var const_ViewDetail_H: NSLayoutConstraint!
    
    @IBOutlet weak var const_LblLine_AbouveTable_H: NSLayoutConstraint!
    
    
    @IBOutlet weak var btnTimeline: UIButton!
    @IBOutlet weak var btnCompanyName: UIButton!
    @IBOutlet weak var btnUserName: UIButton!
    @IBOutlet weak var lblUrgency: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblTechNote: UILabel!
    @IBOutlet weak var lblFlowType: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnFilterTimeLine: UIButton!
    @IBOutlet weak var const_btnFilterTimeLine_W: NSLayoutConstraint!
    
    @IBOutlet weak var tblContactNew: UITableView!
    @IBOutlet weak var const_TblContactNew_H: NSLayoutConstraint!
    @IBOutlet weak var btnGoToAppointments: UIButton!
    @IBOutlet weak var txtFldOpportunityValue: UITextField!
    
    @IBOutlet weak var btnOpportunityValue: UIButton!
    
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    
    // MARK: - ----------------------------------- Global Variables -----------------------------------
    
    var strCompanyKey = String()
    var strUserName = String()
    var strServiceUrlMain = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    var dictLoginData = NSDictionary()
    var loader = UIAlertController()
    // MARK: -  --- String ---
    
    var strRefId = String()
    @objc var strReftype = String()
    var strTypeOfTaskToFetch = String()
    var strURL = String()
    @objc var strRefIdNew = String()
    
    var strStatusNameGlobal = String()
    var strStageNameGlobal = String()
    var strOppAmount = String()
    
    // MARK: -  --- Array ---
    var aryIndexPath = NSMutableArray()
    var arrAllNotes = NSMutableArray()
    var aryTasks = NSMutableArray()
    var arrAllDocuments = NSMutableArray()
    var aryActivities = NSMutableArray()
    var aryActivitiesCopy = NSMutableArray()
    var aryLogType = NSMutableArray()
    var aryTasksNew = NSMutableArray()
    var arrOfContacts = NSMutableArray()

    // MARK: -  --- Disctionary ---
    @objc var dictLeadData = NSDictionary()//NSManagedObject()
    @objc var dictLeadDataTemp = NSDictionary()
    var dictServiceNameFromId = NSDictionary()
    var dictOpportunityStageNameFromId = NSDictionary()
    var dictOpportunityStageNameFromSysName = NSDictionary()
    
    var dictOpportunityStatusNameFromId = NSDictionary()
    var dictOpportunityStatusNameFromSysName = NSDictionary()
    
    // variables
    var detailType = OpportunityDetailType.Timeline // GeneralNew
    
    var dictTimeLine = NSDictionary()
    var arrayTimeline = NSMutableArray()
    var filterTimelineAry = [NSMutableDictionary]()
    var dictGroupedTimeline = Dictionary<AnyHashable, [NSMutableDictionary]>()
    var aryAllKeys = Array<Any>()
    
    var chkConfidenceUpdate = Bool()
    let dispatchGroup = DispatchGroup()
    
    
    var ChkCalledApiSynchro = Bool()

    
    @IBOutlet weak var lbltaskCount: UILabel!
         @IBOutlet weak var lblScheduleCount: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                         lblScheduleCount.text = "0"
                           lbltaskCount.layer.cornerRadius = 18.0
                                 lbltaskCount.backgroundColor = UIColor.red
                                 lblScheduleCount.layer.cornerRadius = 18.0
                                 lblScheduleCount.backgroundColor = UIColor.red
                                 lbltaskCount.layer.masksToBounds = true
                                 lblScheduleCount.layer.masksToBounds = true

                       }
            lblHeader.text = ""
            strOppAmount = ""
            tblviewOpportunityDetails.tableFooterView = UIView()
            tblviewOpportunityDetails.estimatedRowHeight = 50.0
            
           
            
            btnDocumentsNew.setTitleColor(UIColor.lightGray, for: .normal)
            btnTimeline.setTitleColor(UIColor.darkGray, for: .normal)
            btnDetailsNew.setTitleColor(UIColor.lightGray, for: .normal)
            
            chkConfidenceUpdate = false
            const_btnFilterTimeLine_W.constant = 0 + (DeviceType.IS_IPAD ? 50 : 40)
            //btnFilterTimeLine.isHidden = true
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
            
            strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
            
            strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
            
            
            strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            
            strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            
            strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                
                strURL = "\(value)"
                
            }
            strTypeOfTaskToFetch = "Lead"
            strReftype = "Lead"
            strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "opportunityId") ?? "")"
            //const_ViewDetail_H.constant = 115
            
            //[progressConfidenceLvel, addTarget,:self action:@selector(onSliderValChanged:forEvent:) forControlEvents:UIControlEventValueChanged]
           // progressConfidenceLvel.addTarget(self, action: #selector(onSliderValChanged), for: .valueChanged)

            //setDefaultValuesForTimeLineFilter()
            //setDefaultValuesForContactFilter()
            //setInitialValues()
            setupUI()
            getLogTypeFromMaster()
            callAllAPISynchronously()
            

            // perform(#selector(getAllNotes()), with: nil, afterDelay: 1)
            //perform(#selector(getAllTask()), with: nil, afterDelay: 1)
            
            // Do any additional setup after loading the view.
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AssociatedCompany_Notification"),
            object: nil,
            queue: nil,
            using:catchNotification)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedNotes_Notification"),
            object: nil,
            queue: nil,
            using:catchNotification1)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AssociatedContact_Notification"),
            object: nil,
            queue: nil,
            using:catchNotification2)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedActivity_Notification"),
            object: nil,
            queue: nil,
            using:catchNotificationActivityAdded)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedTask_Notification"),
            object: nil,
            queue: nil,
            using:catchNotificationTaskAdded)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedDocuments_Notification"),
                object: nil,
                queue: nil,
                using:catchNotificationDocumentsAdded)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "TimeLine_Notification"),
            object: nil,
            queue: nil,
            using:catchNotification_TimeLine)
            
            opportunityStageStatusFromId()
            
            UserDefaults.standard.set(false, forKey: "isTaskAddedUpdated")
            UserDefaults.standard.set(false, forKey: "isActivityAddedUpdated")
            UserDefaults.standard.set(false, forKey: "isNotesAdded")

            //createBadgeView()
       
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                    self.setFooterMenuOption()
                })
        }
        
       
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setFooterMenuOption()
        })
      }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !Check_Login_Session_expired() {
            viewBtnContainer.layer.borderColor = hexStringToUIColor(hex: "D0B50E").cgColor
            viewBtnContainer.layer.borderWidth = 1.0
            viewBtnContainer.layer.cornerRadius = 5.0
            
            if "\(nsud.value(forKey: "fromAddNotes") ?? "")" == "fromAddNotes"
            {
                 getAllNotes()
                
                //fetchAllNotesFromCoreData()
                
            }
            if nsud.bool(forKey:"fromAddTask") == true
            {
                fetchTaskListFromLocalDB()
                fetchActivityListFromLocalDB()
                nsud.set(false, forKey: "fromAddTask")
                nsud.synchronize()
            }
            if nsud.bool(forKey:"fromAddActivity") == true
            {
                fetchTaskListFromLocalDB()
                fetchActivityListFromLocalDB()
                nsud.set(false, forKey: "fromAddActivity")
                nsud.synchronize()
            }
            
            if nsud.bool(forKey: "forEditOpportunity") == true
            {
                nsud.set(false, forKey: "forEditOpportunity")
                nsud.synchronize()
                //self.navigationController?.popViewController(animated: false)
                
                //Nilind 13 June
                nsud.set(true, forKey: "fromScheduledOpportunity")
                nsud.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0)
                {
                    //self.syncTask()
                    self.getBasicInfoOpportunity()
                    
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
                {
                    //self.syncTask()
                    self.callApiToGetContacts()
                    
                }
            }
            if nsud.bool(forKey: "fromAddUpdateTask") == true
            {
                nsud.set(false, forKey: "fromAddUpdateTask")
                nsud.synchronize()
                
                setInitialValues()
                getLogTypeFromMaster()
                
                if ChkCalledApiSynchro == true
                {
                    
                }
                else
                {
                    self.callAllAPISynchronously()
                }
            }
            if nsud.bool(forKey: "fromAddUpdateActivity") == true
            {
                nsud.set(false, forKey: "fromAddUpdateActivity")
                nsud.synchronize()
                
                setInitialValues()
                getLogTypeFromMaster()
                
                if ChkCalledApiSynchro == true
                {
                    
                }
                else
                {
                    self.callAllAPISynchronously()
                }
            }
            
            if nsud.bool(forKey: "RefreshAssociations_ContactDetails") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "RefreshAssociations_ContactDetails")
                    
                    if ChkCalledApiSynchro == true
                    {
                        
                    }
                    else
                    {
                        self.callAllAPISynchronously()
                    }
                    
                }
                
            }else if nsud.bool(forKey: "RefreshAssociations_CompanyDetails") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "RefreshAssociations_CompanyDetails")

                    if ChkCalledApiSynchro == true
                    {
                        
                    }
                    else
                    {
                        self.callAllAPISynchronously()
                    }

                }
                
            }else if nsud.bool(forKey: "isTaskAddedUpdated") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "isTaskAddedUpdated")

                    self.callAPToGetTimeLine()//isActivityAddedUpdated

                }
                
            }else if nsud.bool(forKey: "isActivityAddedUpdated") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "isActivityAddedUpdated")

                    self.callAPToGetTimeLine()//isActivityAddedUpdated

                }
                
            }else if nsud.bool(forKey: "isNotesAdded") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "isNotesAdded")

                    self.callAPToGetTimeLine()//isActivityAddedUpdated

                }
                
            }
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            
        }
       
    }
    // MARK: ---------------------------Footer Functions ---------------------------
    func setFooterMenuOption() {
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Sales", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
            self.view.endEditing(true)
            self.goToDasboard()
           

        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
            self.view.endEditing(true)
            self.goToAppointment()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.goToContactList()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
           
        }
       
    }
    // MARK:
    func saveOpportunityLeads()
    {
        var dict = NSDictionary()
        dict = dictLeadDataTemp as NSDictionary
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("accountCompany")
        arrOfKeys.add("accountContactName")
        arrOfKeys.add("accountId")
        arrOfKeys.add("accountNo")
        arrOfKeys.add("billingAddress")
        arrOfKeys.add("billingAddressSameAsService")
        arrOfKeys.add("billingLocationId")
        arrOfKeys.add("branchSysName")
        arrOfKeys.add("cell")
        arrOfKeys.add("companySize")
        arrOfKeys.add("confidenceLevel")
        arrOfKeys.add("crmContactId")
        arrOfKeys.add("customerRating")
        arrOfKeys.add("departmentSysName")
        arrOfKeys.add("direction")
        arrOfKeys.add("driveTime")
        arrOfKeys.add("expectedClosingDate")
        arrOfKeys.add("feedbackRating")
        arrOfKeys.add("fieldSalesPerson")
        arrOfKeys.add("fieldSalesPersonId")
        arrOfKeys.add("firstName")
        arrOfKeys.add("followUpDate")
        arrOfKeys.add("grabbedDate")
        arrOfKeys.add("industryId")
        arrOfKeys.add("insideSalesPerson")
        arrOfKeys.add("insideSalesPersonId")
        arrOfKeys.add("isTaxExempt")
        arrOfKeys.add("lastName")
        arrOfKeys.add("middleName")
        arrOfKeys.add("notes")
        arrOfKeys.add("opportunityContactAddress")
        arrOfKeys.add("opportunityContactFullName")
        arrOfKeys.add("opportunityContactProfileImage")
        arrOfKeys.add("opportunityDescription")
        arrOfKeys.add("opportunityId")
        arrOfKeys.add("opportunityName")
        arrOfKeys.add("opportunityNumber")
        arrOfKeys.add("opportunityStage")
        arrOfKeys.add("opportunityStatus")
        arrOfKeys.add("opportunityType")
        arrOfKeys.add("opportunityValue")
        arrOfKeys.add("primaryEmail")
        arrOfKeys.add("primaryPhone")
        arrOfKeys.add("primaryPhoneExt")
        arrOfKeys.add("proposedAmount")
        arrOfKeys.add("salesAppDate")
        arrOfKeys.add("scheduleDate")
        arrOfKeys.add("scheduleTime")
        arrOfKeys.add("secondaryEmail")
        arrOfKeys.add("secondaryPhone")
        arrOfKeys.add("secondaryPhoneExt")
        arrOfKeys.add("service")
        arrOfKeys.add("serviceAddress")
        arrOfKeys.add("serviceCategoryId")
        arrOfKeys.add("serviceId")
        arrOfKeys.add("serviceLocationId")
        arrOfKeys.add("sizeId")
        arrOfKeys.add("sourceIdsList")
        arrOfKeys.add("stageId")
        arrOfKeys.add("statusId")
        arrOfKeys.add("submittedBy")
        arrOfKeys.add("submittedById")
        arrOfKeys.add("submittedDate")
        arrOfKeys.add("taxSysName")
        arrOfKeys.add("techNote")
        arrOfKeys.add("totalEstimationTime")
        arrOfKeys.add("urgency")
        arrOfKeys.add("urgencyId")
        arrOfKeys.add("userName")
        arrOfKeys.add("confidenceValue")
        arrOfKeys.add("taxExemptionNo")
        
        
        if (dict.value(forKey: "accountCompany") ?? "") is NSDictionary
        {
            let dictAccountCompany = (dict.value(forKey: "accountCompany") ?? "") as! NSDictionary
            arrOfValues.add(dictAccountCompany) //accountCompany
        }
        else
        {
            arrOfValues.add("") //accountCompany
            
        }
        
        
        
        arrOfValues.add("\(dict.value(forKey: "accountContactName") ?? "")")//accountContactName
        
        arrOfValues.add("\(dict.value(forKey: "accountId") ?? "")")//accountId
        arrOfValues.add("\(dict.value(forKey: "accountNo") ?? "")")//accountNo
        
        
        if (dict.value(forKey: "billingAddress") ?? "") is NSDictionary
        {
            let dictBillingAddress = (dict.value(forKey: "billingAddress") ?? "") as! NSDictionary
            arrOfValues.add(dictBillingAddress) //billingAddress
        }
        else
        {
            arrOfValues.add("")//billingAddress
        }
        
        //arrOfValues.add("\(dict.value(forKey: "BillingAddress") ?? "")")//billingAddress
        
        
        arrOfValues.add("\(dict.value(forKey: "billingAddressSameAsService") ?? "")")//billingAddressSameAsService
        arrOfValues.add("\(dict.value(forKey: "billingLocationId") ?? "")")//billingLocationId
        arrOfValues.add("\(dict.value(forKey: "branchSysName") ?? "")")//branchSysName
        arrOfValues.add("\(dict.value(forKey: "cell") ?? "")")//cell
        arrOfValues.add("\(dict.value(forKey: "companySize") ?? "")")//companySize
        arrOfValues.add("\(dict.value(forKey: "confidenceLevel") ?? "")")//confidenceLevel
        arrOfValues.add("\(dict.value(forKey: "crmContactId") ?? "")")//crmContactId
        arrOfValues.add("\(dict.value(forKey: "customerRating") ?? "")")//customerRating
        arrOfValues.add("\(dict.value(forKey: "departmentSysName") ?? "")")//departmentSysName
        arrOfValues.add("\(dict.value(forKey: "direction") ?? "")")//direction
        arrOfValues.add("\(dict.value(forKey: "driveTime") ?? "")")//driveTime
        arrOfValues.add("\(dict.value(forKey: "ExpectedClosingDate") ?? "")")//expectedClosingDate
        arrOfValues.add("\(dict.value(forKey: "feedbackRating") ?? "")")//feedbackRating
        arrOfValues.add("\(dict.value(forKey: "fieldSalesPerson") ?? "")")//fieldSalesPerson
        arrOfValues.add("\(dict.value(forKey: "fieldSalesPersonId") ?? "")")//fieldSalesPersonId
        arrOfValues.add("\(dict.value(forKey: "firstName") ?? "")")//firstName
        arrOfValues.add("\(dict.value(forKey: "followUpDate") ?? "")")//followUpDate
        arrOfValues.add("\(dict.value(forKey: "grabbedDate") ?? "")")//grabbedDate
        arrOfValues.add("\(dict.value(forKey: "industryId") ?? "")")//industryId
        arrOfValues.add("\(dict.value(forKey: "insideSalesPerson") ?? "")")//insideSalesPerson
        arrOfValues.add("\(dict.value(forKey: "insideSalesPersonId") ?? "")")//insideSalesPersonId
        arrOfValues.add("\(dict.value(forKey: "isTaxExempt") ?? "")")//isTaxExempt
        arrOfValues.add("\(dict.value(forKey: "lastName") ?? "")")//lastName
        arrOfValues.add("\(dict.value(forKey: "middleName") ?? "")")//middleName
        arrOfValues.add("\(dict.value(forKey: "notes") ?? "")")//notes
        
        
        if (dict.value(forKey: "opportunityContactAddress") ?? "") is NSDictionary
        {
            let dictOpportunityContactAddress = (dict.value(forKey: "opportunityContactAddress") ?? "") as! NSDictionary
            arrOfValues.add(dictOpportunityContactAddress) //OpportunityContactAddress
        }
        else
        {
            arrOfValues.add("")//OpportunityContactAddress
        }
        //arrOfValues.add("\(dict.value(forKey: "OpportunityContactAddress") ?? "")")//opportunityContactAddress
        
        
        arrOfValues.add("\(dict.value(forKey: "opportunityContactFullName") ?? "")")//opportunityContactFullName
        arrOfValues.add("\(dict.value(forKey: "opportunityContactProfileImage") ?? "")")//opportunityContactProfileImage
        arrOfValues.add("\(dict.value(forKey: "opportunityDescription") ?? "")")//opportunityDescription
        arrOfValues.add("\(dict.value(forKey: "opportunityId") ?? "")")//opportunityId
        arrOfValues.add("\(dict.value(forKey: "opportunityName") ?? "")")//opportunityName
        arrOfValues.add("\(dict.value(forKey: "opportunityNumber") ?? "")")//opportunityNumber
        arrOfValues.add("\(dict.value(forKey: "opportunityStage") ?? "")")//opportunityStage
        arrOfValues.add("\(dict.value(forKey: "opportunityStatus") ?? "")")//opportunityStatus
        arrOfValues.add("\(dict.value(forKey: "opportunityType") ?? "")")//opportunityType
        arrOfValues.add("\(dict.value(forKey: "opportunityValue") ?? "")")//opportunityValue
        arrOfValues.add("\(dict.value(forKey: "primaryEmail") ?? "")")//primaryEmail
        arrOfValues.add("\(dict.value(forKey: "primaryPhone") ?? "")")//primaryPhone
        arrOfValues.add("\(dict.value(forKey: "primaryPhoneExt") ?? "")")//primaryPhoneExt
        arrOfValues.add("\(dict.value(forKey: "proposedAmount") ?? "")")//proposedAmount
        arrOfValues.add("\(dict.value(forKey: "salesAppDate") ?? "")")//salesAppDate
        arrOfValues.add("\(dict.value(forKey: "scheduleDate") ?? "")")//scheduleDate
        arrOfValues.add("\(dict.value(forKey: "scheduleTime") ?? "")")//scheduleTime
        arrOfValues.add("\(dict.value(forKey: "secondaryEmail") ?? "")")//secondaryEmail
        arrOfValues.add("\(dict.value(forKey: "secondaryPhone") ?? "")")//secondaryPhone
        arrOfValues.add("\(dict.value(forKey: "secondaryPhoneExt") ?? "")")//secondaryPhoneExt
        arrOfValues.add("\(dict.value(forKey: "service") ?? "")")//service
        
        
        if (dict.value(forKey: "serviceAddress") ?? "") is NSDictionary
        {
            let dictServiceAddress = (dict.value(forKey: "serviceAddress") ?? "") as! NSDictionary
            arrOfValues.add(dictServiceAddress) //ServiceAddress
        }
        else
        {
            arrOfValues.add("")//ServiceAddress
        }
        
        // arrOfValues.add("\(dict.value(forKey: "ServiceAddress") ?? "")")//serviceAddress
        
        
        arrOfValues.add("\(dict.value(forKey: "serviceCategoryId") ?? "")")//serviceCategoryId
        arrOfValues.add("\(dict.value(forKey: "serviceId") ?? "")")//serviceId
        arrOfValues.add("\(dict.value(forKey: "serviceLocationId") ?? "")")//serviceLocationId
        arrOfValues.add("\(dict.value(forKey: "sizeId") ?? "")")//sizeId
        
        if (dict.value(forKey: "sourceIdsList") ?? "") is NSArray
        {
            let arrSourceIdsList = (dict.value(forKey: "sourceIdsList") ?? "") as! NSArray
            arrOfValues.add(arrSourceIdsList) //sourceIdsList
        }
        else
        {
            arrOfValues.add("")//sourceIdsList
        }
        
        // arrOfValues.add("\(dict.value(forKey: "SourceIdsList") ?? "")")//sourceIdsList
        
        
        arrOfValues.add("\(dict.value(forKey: "stageId") ?? "")")//stageId
        arrOfValues.add("\(dict.value(forKey: "statusId") ?? "")")//statusId
        arrOfValues.add("\(dict.value(forKey: "submittedBy") ?? "")")//submittedBy
        arrOfValues.add("\(dict.value(forKey: "submittedById") ?? "")")//submittedById
        arrOfValues.add("\(dict.value(forKey: "submittedDate") ?? "")")//submittedDate
        arrOfValues.add("\(dict.value(forKey: "taxSysName") ?? "")")//taxSysName
        arrOfValues.add("\(dict.value(forKey: "techNote") ?? "")")//techNote
        arrOfValues.add("\(dict.value(forKey: "totalEstimationTime") ?? "")")//totalEstimationTime
        arrOfValues.add("\(dict.value(forKey: "urgency") ?? "")")//urgency
        arrOfValues.add("\(dict.value(forKey: "urgencyId") ?? "")")//urgencyId
        arrOfValues.add("\(strUserName)")//userName
        
        arrOfValues.add("\(dict.value(forKey: "confidenceValue") ?? "")")//confidenceValue
        
        arrOfValues.add("\(dict.value(forKey: "taxExemptionNo") ?? "")")//confidenceValue
        
        
        saveDataInDB(strEntity: "TotalOpportunityLeads", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        //Update Modify Date In Work Order DB
        //updateServicePestModifyDate(strWoId: self.strWoId as String)
    }
    
 
    
    // MARK: -  ------------------------------ UITableview delegate and data source  ------------------------------

    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == tblContactNew
        {
            return 1
        }
        else
        {
            switch detailType
            {
            case.GeneralNew:
                
                return 4
                
            case.DocumentsNew:
                
                return 1
                
            case.DetailsNew:
                
                return 1
                
            case .Task:
                return 2
                
            case .Notes:
                return 1
                
          
                
            case .Timeline:
                return (aryAllKeys.count) + 2
                
            default:
                return 2
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if tableView == tblContactNew
        {
            
            return arrOfContacts.count
            
        }
        else
        {
            switch detailType
            {
            case .GeneralNew:
                if(section == 0) // Contact
                {
                    return arrOfContacts.count
                }
                else if(section == 1) // Company
                {
                    if dictLeadData.value(forKey: "AccountCompany") is NSDictionary
                    {
                        return 1
                    }
                    else
                    {
                        return 0
                    }
                }
                else if(section == 2) // Task
                {
                    return aryTasksNew.count
                }
                else if(section == 3) // Notes
                {
                    return 0//arrAllNotes.count
                }
                else
                {
                    return 2
                }
            case.DocumentsNew:
                
                return arrAllDocuments.count
                
            case.DetailsNew:
                
                
                return 1
                
            case .Task:
                if(section == 0)
                {
                    if aryTasks.count >= 3
                    {
                        return 3
                    }
                    else
                    {
                        return aryTasks.count
                    }
                }
                else
                {
                    if aryActivities.count >= 3
                    {
                        return 3
                    }
                    else
                    {
                        return aryActivities.count
                    }
                }
                
                /* if(section == 0)
                 {
                 return aryTasks.count
                 }
                 return aryActivities.count*/
                
            case .Notes:
                return arrAllNotes.count
                
            
            case .Timeline:
                if section == 0 // Open Task
                {
                    return aryTasksNew.count
                }
                else if section == 1 //History
                {
                    return 0
                }
                else
                {
                    let key = aryAllKeys[section - 2]
                    let valueForKey = dictGroupedTimeline[key as! AnyHashable]
                    return (valueForKey?.count)!
                }
                
            default:
                if(section == 0)
                {
                    return 1
                }
                return 1
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
         if tableView == tblContactNew
         {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellContact_LeadDetails") as! LeadVCCell
            
            let dictOfContacts = arrOfContacts.object(at: indexPath.row) as! NSDictionary
            //cell.lblNewCustomerName.text = "\(dictOfContacts.value(forKey: "") ?? "")"
            //cell.lblNewCompanyName.text = ""
            
            if((Global().strFullName(dictOfContacts as AnyHashable as? [AnyHashable : Any])!).count > 0)
            {
                
                cell.lblContactNameNew.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any]))
                
                if "\(dictOfContacts.value(forKey: "JobTitle") ?? "")".count > 0 {
                    
                    cell.lblContactNameNew.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any])) + ", \(dictOfContacts.value(forKey: "JobTitle") ?? "")"
                    
                }
                
            }else{
                
                cell.lblContactNameNew.text = ""
                
            }
            // cell.btnDeAssociateContact.tag = indexPath.row
            // cell.btnDeAssociateContact.addTarget(self, action: #selector(actionOnDeAssociateContact), for: .touchUpInside)
            
            //            if "\(dictOfContacts.value(forKey: "IsPrimary") ?? "")" == "1" || "\(dictOfContacts.value(forKey: "IsPrimary") ?? "")" == "True"
            //            {
            //                cell.btnDeAssociateContact.isHidden = true
            //            }
            //            else
            //            {
            //                cell.btnDeAssociateContact.isHidden = false
            //            }
            
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
         }
         else
         {
            switch detailType
            {
            case .GeneralNew:
                
                if(indexPath.section == 0) // Contact
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellContact_LeadDetails") as! LeadVCCell
                    
                    let dictOfContacts = arrOfContacts.object(at: indexPath.row) as! NSDictionary
                    //cell.lblNewCustomerName.text = "\(dictOfContacts.value(forKey: "") ?? "")"
                    cell.lblNewCompanyName.text = ""
                    
                    if((Global().strFullName(dictOfContacts as AnyHashable as? [AnyHashable : Any])!).count > 0)
                    {
                        
                        cell.lblNewCustomerName.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any]))
                        
                        if "\(dictOfContacts.value(forKey: "JobTitle") ?? "")".count > 0 {
                            
                            cell.lblNewCustomerName.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any])) + ", \(dictOfContacts.value(forKey: "JobTitle") ?? "")"
                            
                        }
                        
                    }else{
                        
                        cell.lblNewCustomerName.text = ""
                        
                    }
                    cell.btnDeAssociateContact.tag = indexPath.row
                    cell.btnDeAssociateContact.addTarget(self, action: #selector(actionOnDeAssociateContact), for: .touchUpInside)
                    cell.btnDeAssociateContact.isHidden = true
                    
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    return cell
                }
                else if(indexPath.section == 1) //Company
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellCompany_LeadDetails") as! LeadVCCell
                    
                    if dictLeadData.value(forKey: "AccountCompany") is NSDictionary
                    {
                        
                        cell.lblCompanyName.text = "\(dictLeadData.value(forKeyPath: "AccountCompany.Name") ?? "")"
                    }
                    cell.btnDeAssociateCompany.tag = indexPath.row
                    cell.btnDeAssociateCompany.addTarget(self, action: #selector(actionOnDeAssociateCompany), for: .touchUpInside)
                    cell.btnDeAssociateCompany.isHidden = true
                    
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    return cell
                }
                else if(indexPath.section == 2) //Task
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTask_OpportunityDetails") as! LeadVCCell
                    
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    
                    cell.btnTaskCheckMark.tag = indexPath.row
                    cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
                    
                    if(aryIndexPath.contains(indexPath.row))
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                    }
                    cell.lblTaskName.text = "\(dictData.value(forKey: "TaskName") ?? "")"
                    cell.lblDueDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")//
                    cell.lblDueTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "hh:mm a")
                    
                    if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                        
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                        
                    }
                    let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
                    // cell.imgViewTaskType.image = UIImage(named: strImageName)
                    cell.btnImgTask.setImage(UIImage(named: strImageName), for: .normal)
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    if("\(dictData.value(forKey: "TaskTypeId") ?? "")".count > 0 && "\(dictData.value(forKey: "TaskTypeId") ?? "")" != "<null>")
                    {
                        cell.btnImgTask.isHidden = false
                    }
                    else{
                        cell.btnImgTask.isHidden = true
                        
                    }
                    
                    return cell
                    
                }
                else if(indexPath.section == 3) //Notes
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellNotes_OpportunityDetails") as! LeadVCCell
                    cell.btnDownload.isHidden = true
                    
                    let dict = arrAllNotes.object(at: indexPath.row) as! NSDictionary
                    
                    cell.lblNoteName.text = "\(dict.value(forKey: "Note") ?? "")"
                    cell.lblNoteCreatedBy.text = "\(dict.value(forKey: "CreatedByName") ?? "N/A")"
                    cell.lblNoteCreatedDate.text = changeStringDateToGivenFormat(strDate: "\(dict.value(forKey: "ClientCreatedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    
                    return cell
                    
                }
                
            case .DocumentsNew:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellDocumentDetail") as! LeadVCCell
                let dict = arrAllDocuments.object(at: indexPath.row) as! NSDictionary
                
                cell.lblDocumentTitle.text = "\(dict.value(forKey: "Title") ?? "")"
                let strDocPathLocal = "\(dict.value(forKey: "Path") ?? "")"
                
                
                
                if "\(dict.value(forKey: "MediaTypeSysName") ?? "")" == "Image"
                {
                    if let imgURL = dict.value(forKey: "Path")
                    {
                        if("\(imgURL)" == "<null>" || "\(imgURL)".count == 0 || "\(imgURL)" == " ")
                        {
                            cell.imgViewDocument.image = UIImage(named: "no_image.jpg")
                            cell.imgViewDocument.isHidden = true
                            //cell.lblImageName.isHidden = false
                            // cell.lblImageName.text = firstCharactersFromString(type: "CompanyName", first: name, second: "")
                            
                        }
                        else
                        {
                            
                            cell.imgViewDocument.isHidden = false
                            
                            // Remove Logic for image URL
                            
                            var imgUrlNew = imgURL as! String
                            
                            imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\", with: "/")
                            imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\", with: "/")
                            imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\", with: "/")
                            imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\", with: "/")
                            imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\", with: "/")
                            imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
                            imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
                            
                            imgUrlNew = imgUrlNew.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
                            cell.imgViewDocument.setImageWith(URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                        }
                    }else{
                        
                        cell.imgViewDocument.isHidden = true
                        // cell.lblImageName.isHidden = false
                        // let name = dictData.value(forKey: "Name") as! String
                        // cell.lblImageName.text = firstCharactersFromString(type: "CompanyName", first: name, second: "")
                        
                    }
                }
                else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Document") == .orderedSame
                {
                    let image: UIImage = UIImage(named: "exam")!
                    
                    cell.imgViewDocument.image = image
                    
                    /*let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                     
                     let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                     
                     if isImageExists!  {
                     
                     // kch nhi krna
                     
                     }else {
                     
                     strURL = strURL + "\(strDocPathLocal)"
                     
                     strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                     strURL = replaceBackSlasheFromUrl(strUrl: strURL)
                     
                     strURL = strURL.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
                     
                     DispatchQueue.global(qos: .background).async {
                     
                     let url = URL(string:self.strURL)
                     let data = try? Data(contentsOf: url!)
                     
                     if data != nil && (data?.count)! > 0 {
                     
                     DispatchQueue.main.async {
                     
                     savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                     
                     }}
                     
                     }
                     
                     }*/
                    
                }
                else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Audio") == .orderedSame
                {
                    
                    let image: UIImage = UIImage(named: "audio")!
                    
                    cell.imgViewDocument.image = image
                    
                    /* let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                     
                     let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                     
                     if isImageExists!  {
                     
                     // kch nhi krna
                     
                     }else {
                     
                     strURL = strURL + "\(strDocPathLocal)"
                     
                     strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                     
                     DispatchQueue.global(qos: .background).async {
                     
                     let url = URL(string:self.strURL)
                     let data = try? Data(contentsOf: url!)
                     
                     if data != nil && (data?.count)! > 0 {
                     
                     DispatchQueue.main.async {
                     
                     savepdfDocumentDirectory(strFileName: strImagePath!, dataa: data!)
                     
                     }}
                     
                     }
                     
                     }*/
                    
                }
                else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Video") == .orderedSame
                {
                    
                    let image: UIImage = UIImage(named: "video")!
                    
                    cell.imgViewDocument.image = image
                    
                }
                
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                return cell
                
            case .DetailsNew:
                
                if(indexPath.section == 0)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CellOpportunityDetailsCustomerInfo") as! CellOpportunityDetailsCustomerInfo
                    /* cell.viewOppPrimaryPhone.isHidden = true
                     cell.viewOppSecondaryPhone.isHidden = true
                     cell.viewOppCellPhone.isHidden = true
                     cell.viewOppPrimaryEmail.isHidden = true
                     cell.viewOppSecondaryEmail.isHidden = true*/
                    
                    //cell.lblEmail.text = "\(dictLeadData.value(forKey: "PrimaryEmail") ?? "")"
                    
                    
                    cell.lblPrimaryPhone.text = "\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")"
                    
                    cell.lblSecondaryPhone.text = "\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")"
                    
                    cell.lblCell.text = "\(dictLeadData.value(forKey: "Cell") ?? "")"
                    
                    
                    cell.lblPrimaryEmail.text = "\(dictLeadData.value(forKey: "PrimaryEmail") ?? "")"
                    
                    cell.lblSecondaryEmail.text = "\(dictLeadData.value(forKey: "SecondaryEmail") ?? "")"
                    
                    
                    var strAddress = ""
                    if dictLeadData.value(forKey: "ServiceAddress") is NSDictionary
                    {
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? "")".count > 0
                        {
                            strAddress = "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? ""), "
                        }
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address2") ?? "")".count > 0
                        {
                            strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.Address2") ?? "")"), "
                        }
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.CityName") ?? "")".count > 0
                        {
                            strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.CityName") ?? "")"), "
                        }
                        
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? "")".count > 0
                        {
                            let stateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? ""))")
                            
                            strAddress = "\(strAddress)\(stateName ?? ""), "
                        }
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Zipcode") ?? "")".count > 0
                        {
                            strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.Zipcode") ?? "")")"
                        }
                        
                    }
                    
                    
                    //New
                    
                    cell.btnDetailMsgPrimaryPhone.tag = indexPath.row
                    cell.btnDetailCallPrimaryPhone.tag = indexPath.row
                    cell.btnDetailMsgSecondaryPhone.tag = indexPath.row
                    cell.btnDetailCallSecondaryPhone.tag = indexPath.row
                    cell.btnDetailPrimaryEmail.tag = indexPath.row
                    cell.btnDetailSecondaryEmail.tag = indexPath.row
                    
                    cell.btnDetailMsgPrimaryPhone.addTarget(self, action: #selector(actionOnMsgPrimaryPhone), for: .touchUpInside)
                    cell.btnDetailCallPrimaryPhone.addTarget(self, action: #selector(actionOnCallPrimaryPhone), for: .touchUpInside)
                    
                    cell.btnDetailMsgSecondaryPhone.addTarget(self, action: #selector(actionOnMsgSecondaryPhone), for: .touchUpInside)
                    cell.btnDetailCallSecondaryPhone.addTarget(self, action: #selector(actionOnCallSecondaryPhone), for: .touchUpInside)
                    
                    cell.btnDetailMsgCell.addTarget(self, action: #selector(actionOnMsgCell), for: .touchUpInside)
                    cell.btnDetailCallCell.addTarget(self, action: #selector(actionOnCallCell), for: .touchUpInside)
                    
                    cell.btnDetailPrimaryEmail.addTarget(self, action: #selector(actionOnPrimaryEmail), for: .touchUpInside)
                    cell.btnDetailSecondaryEmail.addTarget(self, action: #selector(actionOnSecondaryEmail), for: .touchUpInside)
                    
                    //End
                    
                    cell.viewOppPrimaryPhone.isHidden = false
                    cell.viewOppSecondaryPhone.isHidden = false
                    cell.viewOppCellPhone.isHidden = false
                    cell.viewOppPrimaryEmail.isHidden = false
                    cell.viewOppSecondaryEmail.isHidden = false
                    cell.viewOppUrgency.isHidden = false
                    cell.viewOppService.isHidden = false
                    cell.viewOppTechNote.isHidden = false
                    cell.viewOppFlowType.isHidden = false
                    cell.viewOppDueDate.isHidden = false
                    cell.viewOppCreatedBy.isHidden = false
                    
                    if (cell.lblPrimaryPhone.text?.count == 0 || cell.lblPrimaryPhone.text == "")
                    {
                        cell.viewOppPrimaryPhone.isHidden = true
                    }
                    if (cell.lblSecondaryPhone.text?.count == 0 || cell.lblSecondaryPhone.text == "")
                    {
                        cell.viewOppSecondaryPhone.isHidden = true
                    }
                    if (cell.lblCell.text?.count == 0 || cell.lblCell.text == "")
                    {
                        cell.viewOppCellPhone.isHidden = true
                    }
                    if (cell.lblPrimaryEmail.text?.count == 0 || cell.lblPrimaryEmail.text == "")
                    {
                        cell.viewOppPrimaryEmail.isHidden = true
                    }
                    if (cell.lblSecondaryEmail.text?.count == 0 || cell.lblSecondaryEmail.text == "")
                    {
                        cell.viewOppSecondaryEmail.isHidden = true
                    }
                    if (cell.lblUrgency.text?.count == 0 || cell.lblUrgency.text == "")
                    {
                        cell.viewOppUrgency.isHidden = true
                    }
                    if (cell.lblService.text?.count == 0 || cell.lblService.text == "")
                    {
                        cell.viewOppService.isHidden = true
                    }
                    if (cell.lblTechNote.text?.count == 0 || cell.lblTechNote.text == "")
                    {
                        cell.viewOppTechNote.isHidden = true
                    }
                    if (cell.lblFlowType.text?.count == 0 || cell.lblFlowType.text == "")
                    {
                        cell.viewOppFlowType.isHidden = true
                    }
                    if (cell.lblDueDate.text?.count == 0 || cell.lblDueDate.text == "")
                    {
                        cell.viewOppDueDate.isHidden = true
                    }
                    /*if (cell.lblCreatedBy.text?.count == 0 || cell.lblCreatedBy.text == "")
                     {
                     cell.viewOppCreatedBy.isHidden = true
                     }*/
                    
                    
                    
                    //cell.lblAddress.text = "\(strAddress)"
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    
                    cell.lblUrgency.text = "\(dictLeadData.value(forKey: "Urgency") ?? "")"
                    cell.lblService.text = "\(dictLeadData.value(forKey: "Service") ?? "")"
                    cell.lblTechNote.text = "\(dictLeadData.value(forKey: "TechNote") ?? "")"
                    cell.lblFlowType.text = "\(dictLeadData.value(forKey: "opportunityType") ?? "")"
                    
                    cell.lblDueDate.text = ""
                    cell.lblDueTime.text = ""
                    //cell.lblCreatedBy.text = getEmployeeNameFromId(strEmpId: "\(dictLeadData.value(forKey: "submittedById") ?? "")")
                    
                    
                    cell.viewOppPrimaryPhone.isHidden = true
                    cell.viewOppSecondaryPhone.isHidden = true
                    cell.viewOppCellPhone.isHidden = true
                    cell.viewOppPrimaryEmail.isHidden = true
                    cell.viewOppSecondaryEmail.isHidden = true
                    cell.viewOppUrgency.isHidden = true
                    cell.viewOppService.isHidden = true
                    cell.viewOppTechNote.isHidden = true
                    cell.viewOppFlowType.isHidden = true
                    cell.viewOppDueDate.isHidden = true
                    
                    cell.viewOppCreatedBy.isHidden = false
                    
                    
                    cell.lblOppNoNew.text = "\(dictLeadData.value(forKey: "OpportunityNumber") ?? "")"
                    cell.lblAccNoNew.text = "\(dictLeadData.value(forKey: "AccountNo") ?? "")"
                    cell.lblServiceNameNew.text = "\(dictServiceNameFromId.value(forKey: "\("\(dictLeadData.value(forKey: "ServiceId") ?? "")")") ?? "")"
                    cell.lblUrgencyNameNew.text = "\(dictLeadData.value(forKey: "Urgency") ?? "")"
                    cell.lblTechNoteNew.text = "\(dictLeadData.value(forKey: "TechNote") ?? "")"
                    cell.lblTypeNew.text =  "\(dictLeadData.value(forKey: "OpportunityType") ?? "")"
                    
                    cell.lblCreatedByNew.text = getEmployeeNameFromId(strEmpId: "\(dictLeadData.value(forKey: "SubmittedById") ?? "")")
                    
                    
                    if "\(dictLeadData.value(forKey: "SubmittedDate") ?? "")".count > 0
                    {
                        //cell.lblCreatedDateTimeNew.text = changeStringDateToGivenFormat(strDate: "\(dictLeadData.value(forKey: "SubmittedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
                        
                        cell.lblCreatedDateTimeNew.text = serverDateToLocal(strDate: "\(dictLeadData.value(forKey: "SubmittedDate") ?? "")", strServerTimeZone: "EST", strRequiredFormat: "MM/dd/yyyy hh:mm a")

                    }
                    else
                    {
                        cell.lblCreatedDateTimeNew.text = ""
                    }
                    
                    return cell
                }
                /* if(indexPath.section == 1)
                 {
                 let cell = tableView.dequeueReusableCell(withIdentifier: "CellOpportunityDetailsOpportunityInfo") as! CellOpportunityDetailsOpportunityInfo
                 
                 cell.lblStatus.text = "\(dictLeadData.value(forKey: "OpportunityStatus") ?? "")"
                 
                 cell.lblStage.text = "\(dictLeadData.value(forKey: "OpportunityStage") ?? "")"
                 
                 cell.lblUrgency.text = "\(dictLeadData.value(forKey: "Urgency") ?? "")"
                 
                 cell.lblService.text = "\(dictLeadData.value(forKey: "Service") ?? "")"
                 
                 cell.lblTechNote.text = "\(dictLeadData.value(forKey: "TechNote") ?? "")"
                 
                 cell.lblFollowUpDate.text = "\(dictLeadData.value(forKey: "FollowUpDate") ?? "")"
                 cell.selectionStyle = UITableViewCell.SelectionStyle.none
                 
                 
                 return cell
                 }*/
            case .Task:
                if(indexPath.section == 0)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTask_OpportunityDetails") as! LeadVCCell
                    
                    let dictData = aryTasks.object(at: indexPath.row) as! NSDictionary
                    
                    cell.btnTaskCheckMark.tag = indexPath.row
                    cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
                    
                    if(aryIndexPath.contains(indexPath.row))
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                    }
                    cell.lblTaskName.text = "\(dictData.value(forKey: "taskName") ?? "")"
                    cell.lblDueDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "dueDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")//
                    cell.lblDueTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "dueDate") ?? "")", strRequiredFormat: "hh:mm a")
                    
                    if("\(dictData.value(forKey: "status")!)" == "Open" || "\(dictData.value(forKey: "status")!)" == "open")
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                        
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                        
                    }
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    
                    return cell
                    
                }
                if(indexPath.section == 1)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellActivity_OpportunityDetails") as! LeadVCCell
                    
                    let dictData = aryActivities.object(at: indexPath.row) as! NSDictionary
                    
                    // cell.lblActivityName.text = "\(dictData.value(forKey: "agenda") ?? "")"
                    
                    if(("\(dictData.value(forKey: "agenda") ?? "")".count > 0))
                        
                    {
                        cell.lblActivityName.text = "\(dictData.value(forKey: "agenda") ?? "")"
                        
                    }
                    else
                    {
                        cell.lblActivityName.text = "\(dictData.value(forKey: "Agenda") ?? "")"
                    }
                    
                    for item in aryLogType
                    {
                        if(("\(dictData.value(forKey: "logTypeId") ?? "")" == "\((item as! NSDictionary).value(forKey: "LogTypeId") ?? "")") || ("\(dictData.value(forKey: "LogTypeId") ?? "")" == "\((item as! NSDictionary).value(forKey: "LogTypeId") ?? "")") )
                        {
                            
                            cell.lblLogType.text = "\((item as! NSDictionary).value(forKey: "Name")!)"
                            
                            break
                            
                        }
                        
                    }
                    
                    if(("\(dictData.value(forKey: "createdDate") ?? "")".count > 0))
                    {
                        cell.lblDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "createdDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
                        
                        cell.lblTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "createdDate") ?? "")", strRequiredFormat: "hh:mm a")
                    }
                    else
                    {
                        cell.lblDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "CreatedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
                        
                        
                        
                        cell.lblTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "CreatedDate") ?? "")", strRequiredFormat: "hh:mm a")
                    }
                    
                    
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    return cell
                    
                }
            case .Notes:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellNotes_OpportunityDetails") as! LeadVCCell
                
                
                let dict = arrAllNotes.object(at: indexPath.row) as! NSManagedObject
                
                cell.lblNoteName.text = "\(dict.value(forKey: "Note") ?? "")"
                cell.lblNoteCreatedBy.text = "\(dict.value(forKey: "CreatedByName") ?? "N/A")"
                cell.lblNoteCreatedDate.text = changeStringDateToGivenFormat(strDate: "\(dict.value(forKey: "ClientCreatedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
                
                
                
                cell.btnDownload.isHidden = true
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                return cell
                
  
                
            case .Timeline:
                
                if indexPath.section == 0
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTask_OpportunityDetails") as! LeadVCCell
                    
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    
                    cell.btnTaskCheckMark.tag = indexPath.row
                    cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
                    
                    if(aryIndexPath.contains(indexPath.row))
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                    }
                    cell.lblTaskName.text = "\(dictData.value(forKey: "TaskName") ?? "")"
                    cell.lblDueDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")//
                    cell.lblDueTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "hh:mm a")
                    
                    if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                        
                    }
                    else
                    {
                        cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                        
                    }
                    let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
                    // cell.imgViewTaskType.image = UIImage(named: strImageName)
                    cell.btnImgTask.setImage(UIImage(named: strImageName), for: .normal)
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    if("\(dictData.value(forKey: "TaskTypeId") ?? "")".count > 0 && "\(dictData.value(forKey: "TaskTypeId") ?? "")" != "<null>")
                    {
                        cell.btnImgTask.isHidden = false
                    }
                    else{
                        cell.btnImgTask.isHidden = true
                        
                    }
                    
                    return cell
                    
                }
                else
                {
                    let key = aryAllKeys[indexPath.section - 2]
                    var valueForKey = dictGroupedTimeline[key as! AnyHashable]
                    
                    let list = [Int](1...4)
                    let arrayLoc = NSMutableArray()
                    
                    for (index, element) in list.enumerated() {
                        if(index == 0){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                            
                        }
                        if(index == 1){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        
                        if(index == 2){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        if(index == 3){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                    }
                    
                    valueForKey = (arrayLoc as! [NSMutableDictionary])
                    
                    let dictData = valueForKey![indexPath.row]
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Note"){
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellNotesNew_OpportunityDetails") as! LeadVCCell
                        cell.lblNoteName.text = ""
                        
                        if let noteName = dictData.value(forKey: "Note"){
                            
                            cell.lblNoteName.text = "Note - \(noteName)"
                            cell.lblNoteName.numberOfLines = 0
                        }
                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        return cell
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellActivityTimeLine_OpportunityDetails") as! LeadVCCell
                        
                        if let agenda = dictData.value(forKey: "Agenda"){
                            cell.lblActivityName.text = "\(agenda)"
                        }else{
                            cell.lblActivityName.text = ""
                        }
                        
                        if let clientCreatedDate = dictData.value(forKey: "ClientCreatedDate"){
                            
                            cell.lblDate.text = changeDateToString(date: clientCreatedDate as! Date)
                            
                        }else{
                            cell.lblDate.text = ""
                        }
                        
                        for item in aryLogType
                        {
                            if("\(dictData.value(forKey: "LogTypeId")!)" == "\((item as! NSDictionary).value(forKey: "LogTypeId")!)" )
                            {
                                cell.lblLogType.text = "\((item as! NSDictionary).value(forKey: "Name")!)" + " " + "|"
                                
                                break
                            }
                        }
                        cell.btnActivityComment.tag = indexPath.row*1000+indexPath.section
                        cell.btnActivityComment.addTarget(self, action: #selector(actionOnActvityComment), for: .touchUpInside)
                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        return cell
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Task"){
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTaskNew_OpportunityDetails") as! LeadVCCell
                        
                        if let taskname = dictData.value(forKey: "TaskName"){
                            cell.lblTaskName.text = "\(taskname)"
                        }else{
                            cell.lblTaskName.text = ""
                        }
                        
                        if let companyName = dictData.value(forKey: "CompanyName"){
                            
                            if("\(companyName)" == "<null>")
                            {
                                cell.lblCompanyName.text = ""
                            }
                            else
                            {
                                cell.lblCompanyName.text = "\(companyName)" + " " + "|"
                            }
                        }
                        else
                        {
                            cell.lblTaskName.text = ""
                        }
                        
                        if let assignTo = dictData.value(forKey: "AssignedToStr"){
                            
                            if("\(assignTo)" == "<null>")
                            {
                                cell.lblAssignTo.text = ""
                            }
                            else
                            {
                                cell.lblAssignTo.text = "\(assignTo)"
                            }
                        }
                        else
                        {
                            cell.lblAssignTo.text = ""
                        }
                        let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
                        cell.imgTaskType.image = UIImage(named: strImageName)
                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        return cell
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Email"){
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellEmail_OpportunityDetails") as! LeadVCCell
                        
                        if let emailSubject = dictData.value(forKey: "EmailSubject"){
                            
                            if("\(emailSubject)" == "<null>")
                            {
                                cell.lblSubject.text = ""
                            }
                            else
                            {
                                cell.lblSubject.text = "Subject: \(emailSubject)"
                            }
                        }
                        else
                        {
                            cell.lblSubject.text = ""
                        }
                        
                        if let emailFrom = dictData.value(forKey: "EmailFrom"){
                            
                            if("\(emailFrom)" == "<null>")
                            {
                                cell.lblEmailFrom.text = ""
                            }
                            else
                            {
                                cell.lblEmailFrom.text = "From: \(emailFrom)"
                            }
                        }
                        else
                        {
                            cell.lblEmailFrom.text = ""
                        }
                        
                        if let emailContent = dictData.value(forKey: "EmailNotes"){
                            
                            if("\(emailContent)" == "<null>")
                            {
                                cell.constHghtTxtviewEmailContent.constant = 0.0
                                cell.txtviewEmailContent.text = ""
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                    
                                    let noSpaceAttributedString =
                                        htmlAttributedString(strHtmlString: "\(emailContent)").trimmedAttributedString(set: CharacterSet.whitespacesAndNewlines)
                                    
                                    cell.txtviewEmailContent.attributedText = noSpaceAttributedString
                                    cell.constHghtTxtviewEmailContent.constant = 100.0
                                }
                            }
                        }
                        else
                        {
                            cell.txtviewEmailContent.text = ""
                            cell.constHghtTxtviewEmailContent.constant = 100.0
                        }
                        
                        /*[textView setTextContainerInset:UIEdgeInsetsZero];
                         textView.textContainer.lineFragmentPadding = 0;*/
                        cell.txtviewEmailContent.textContainerInset = .zero
                        cell.txtviewEmailContent.textContainer.lineFragmentPadding = 0
                        cell.txtviewEmailContent.layer.cornerRadius = 2.0
                        cell.txtviewEmailContent.layer.borderWidth = 0.5
                        cell.txtviewEmailContent.layer.borderColor = UIColor.lightGray.cgColor
                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        return cell
                    }
                    
                    return UITableViewCell()
                }
                
                
            default:
                
                if(indexPath.section == 0)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CellOpportunityDetailsCustomerInfo") as! CellOpportunityDetailsCustomerInfo
                    
                    cell.lblEmail.text = "\(dictLeadData.value(forKey: "PrimaryEmail") ?? "")"
                    
                    cell.lblPrimaryPhone.text = "\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")"
                    
                    cell.lblSecondaryPhone.text = "\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")"
                    
                    cell.lblCell.text = "\(dictLeadData.value(forKey: "Cell") ?? "")"
                    
                    
                    cell.lblPrimaryEmail.text = "\(dictLeadData.value(forKey: "PrimaryEmail") ?? "")"
                    
                    cell.lblSecondaryEmail.text = "\(dictLeadData.value(forKey: "SecondaryEmail") ?? "")"
                    
                    
                    var strAddress = ""
                    if dictLeadData.value(forKey: "ServiceAddress") is NSDictionary
                    {
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? "")".count > 0
                        {
                            strAddress = "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? ""), "
                        }
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address2") ?? "")".count > 0
                        {
                            strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.Address2") ?? "")"), "
                        }
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.CityName") ?? "")".count > 0
                        {
                            strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.CityName") ?? "")"), "
                        }
                        
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? "")".count > 0
                        {
                            let stateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? ""))")
                            
                            strAddress = "\(strAddress)\(stateName ?? ""), "
                        }
                        if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Zipcode") ?? "")".count > 0
                        {
                            strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.Zipcode") ?? "")")"
                        }
                        
                    }
                    cell.lblAddress.text = "\(strAddress)"
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    return cell
                }
                /* if(indexPath.section == 1)
                 {
                 let cell = tableView.dequeueReusableCell(withIdentifier: "CellOpportunityDetailsOpportunityInfo") as! CellOpportunityDetailsOpportunityInfo
                 
                 cell.lblStatus.text = "\(dictLeadData.value(forKey: "OpportunityStatus") ?? "")"
                 
                 cell.lblStage.text = "\(dictLeadData.value(forKey: "OpportunityStage") ?? "")"
                 
                 cell.lblUrgency.text = "\(dictLeadData.value(forKey: "Urgency") ?? "")"
                 
                 cell.lblService.text = "\(dictLeadData.value(forKey: "Service") ?? "")"
                 
                 cell.lblTechNote.text = "\(dictLeadData.value(forKey: "TechNote") ?? "")"
                 
                 cell.lblFollowUpDate.text = "\(dictLeadData.value(forKey: "FollowUpDate") ?? "")"
                 cell.selectionStyle = UITableViewCell.SelectionStyle.none
                 
                 
                 return cell
                 }*/
                
                
                /*  cell.btnConvertToOpportunity.addTarget(self, action: #selector(actionOnConvertToOpportunity), for: .touchUpInside)
                 
                 cell.btnCreateFollowUp.addTarget(self, action: #selector(actionOnCreateFollowUp), for: .touchUpInside)
                 
                 cell.btnRelease.addTarget(self, action: #selector(actionOnRelease), for: .touchUpInside)
                 
                 cell.btnGrab.addTarget(self, action: #selector(actionOnGrab), for: .touchUpInside)
                 
                 cell.btnMarkAsDead.addTarget(self, action: #selector(actionOnMarkAsDead), for: .touchUpInside)*/
                
                
            }
            
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        if tableView == tblContactNew
        {
            return UIView()
        }
        else
        {
            switch detailType
            {
            case .GeneralNew:
                
                let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                viewheader.backgroundColor = UIColor.init(red: 246/255, green: 246/255, blue: 246/255, alpha: 1)
                
                let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                
                if(section == 0)
                {
                    lbl.text = "Contact"
                    lbl.textColor = UIColor.theme()
                }
                if(section == 1)
                {
                    lbl.text = "Company"
                    lbl.textColor = UIColor.theme()
                }
                else if(section == 2)
                {
                    lbl.text = "Open Task"
                    lbl.textColor = UIColor.theme()
                }
                else if(section == 3)
                {
                    lbl.text = "Notes"
                    lbl.textColor = UIColor.theme()
                }
                lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
                viewheader.addSubview(lbl)
                return viewheader
            case .Task:
                
                let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                viewheader.backgroundColor = UIColor.lightColorForCell()
                
                let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                
                lbl.textColor = hexStringToUIColor(hex: "3E977A")
                
                  lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
                if(section == 0)
                {
                    lbl.text = "Task List"
                }
                else if(section == 1)
                {
                    lbl.text = "Activity List"
                }
                
                viewheader.addSubview(lbl)
                return viewheader
                
            case .Timeline:
                
                if section == 0
                {
                    let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                    
                    viewHeader.backgroundColor = UIColor.headerFooter()
                    
                    let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                      lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
                    lbl.text = "Open Tasks"
                    lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
                    lbl.textColor = UIColor.theme()
                    viewHeader.addSubview(lbl)
                    
                    return viewHeader
                }
                else if section == 1
                {
                    /*let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
                     viewheader.backgroundColor = UIColor.lightColorForCell()*/
                    let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                    
                    viewHeader.backgroundColor = UIColor.headerFooter()
                    
                    let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                    
                    lbl.text = "History"
                    lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
                    lbl.textColor = UIColor.theme()
                      lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
                    viewHeader.addSubview(lbl)
                    
                    let button = UIButton(type: .system)
                    button.frame = CGRect( x: UIScreen.main.bounds.size.width - 55, y: 0, width: (DeviceType.IS_IPAD ? 50 : 40), height: (DeviceType.IS_IPAD ? 50 : 40))
                    
                    button.setImage(UIImage(named: "filter_crmTop"), for: .normal)
                    button.tintColor = UIColor.theme()
                    //button.tintColor = hexStringToUIColor(hex: "3E977A")
                    button.addTarget(self, action: #selector(actionOnFilterTimeLineNew), for: .touchUpInside)
                    
                    viewHeader.addSubview(button)
                    
                    return viewHeader
                }
                else
                {
                    let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                    
                    viewHeader.backgroundColor = UIColor.headerFooter()
                    
                    let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 250, height: (DeviceType.IS_IPAD ? 50 : 40)))
                    let key = aryAllKeys[section - 2]
                    
                    if((key as? Date) != nil)
                    {
                        let strDate = changeDateToString(date: key as! Date)
                        
                        lbl.text = strDate
                    }
                      lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
                    viewHeader.addSubview(lbl)
                    return viewHeader
                }
                
                
            default:
                print("nothing to do")
                let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
                viewheader.backgroundColor = UIColor.lightColorForCell()
                
                let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: 150, height: (DeviceType.IS_IPAD ? 50 : 40)))
                lbl.textColor = hexStringToUIColor(hex: "3E977A")
                  lbl.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 18 : 16))
                if(section == 0)
                {
                    lbl.text = "Customer Info"
                }
                else if(section == 1)
                {
                    lbl.text = "Opportunity Info"
                }
                
                viewheader.addSubview(lbl)
                return viewheader
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == tblContactNew
        {
            
            return 0
            
        }
        else
        {
            switch detailType
            {
            case .GeneralNew:
                
                if section == 0  //Cotact
                {
                    if arrOfContacts.count > 0
                    {
                        return (DeviceType.IS_IPAD ? 60 : 50)
                    }
                    else
                    {
                        return 0
                    }
                }
                else if section == 1  //Company
                {
                    if dictLeadData.value(forKey: "AccountCompany") is NSDictionary
                    {
                        return (DeviceType.IS_IPAD ? 60 : 50)
                    }
                    else
                    {
                        return 0
                    }
                }
                else if section == 2  //Task
                {
                    if aryTasksNew.count > 0
                    {
                        return (DeviceType.IS_IPAD ? 60 : 50)
                    }
                    else
                    {
                        return 0
                    }
                }
                else if section == 3  //Notes
                {
                    if arrAllNotes.count > 0
                    {
                        return 0//50
                    }
                    else
                    {
                        return 0
                    }
                }
                else
                {
                    return (DeviceType.IS_IPAD ? 60 : 50)
                }
                
                
            case .DocumentsNew:
                return 0
                
            case .DetailsNew:
                return 0
            case .Task:
                if(section == 0)
                {
                    return (DeviceType.IS_IPAD ? 50 : 40)
                }
                return (DeviceType.IS_IPAD ? 50 : 40)
                
            case .Notes:
                return 0
                
          
            case .Timeline:
                if(section == 0)
                {
                    if aryTasksNew.count == 0
                    {
                        return 0
                    }
                    else
                    {
                        return (DeviceType.IS_IPAD ? 50 : 40)
                    }
                }
                else
                {
                    return (DeviceType.IS_IPAD ? 50 : 40)
                    
                }
            default:
                return (DeviceType.IS_IPAD ? 50 : 40)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblContactNew
        {
            return (DeviceType.IS_IPAD ? 44 : 30)
        }
        else
        {
            switch detailType
            {
            case .GeneralNew:
                if indexPath.section == 0
                {
                    return (DeviceType.IS_IPAD ? 85 : 70)
                }
                else if indexPath.section == 1
                {
                    return (DeviceType.IS_IPAD ? 75 : 60)
                }
                else if indexPath.section == 2
                {
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                    {
                        return (DeviceType.IS_IPAD ? 93 : 78)
                    }
                    else
                    {
                        return 0
                    }
                    //return 78
                }
                else
                {
                        return (DeviceType.IS_IPAD ? 93 : 78)
                }
                
            case .DocumentsNew:
                return (DeviceType.IS_IPAD ? 65 : 50)
                
            case .DetailsNew:
                
                
                return UITableView.automaticDimension
                
            case .Task:
                if(indexPath.section == 0)
                {
                    return (DeviceType.IS_IPAD ? 93 : 78)
                }
                return (DeviceType.IS_IPAD ? 93 : 78)
                
            case .Notes:
                return (DeviceType.IS_IPAD ? 100 : 85)
                
          
            case .Timeline:
                
                if(indexPath.section == 0)
                {
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                    {
                        return (DeviceType.IS_IPAD ? 93 : 78)
                    }
                    else
                    {
                        return 0
                    }
                }
                else if(indexPath.section == 1)
                {
                    return 0
                }
                else
                {
                    let key = aryAllKeys[indexPath.section - 2]
                    var valueForKey = dictGroupedTimeline[key as! AnyHashable]
                    
                    let list = [Int](1...4)
                    let arrayLoc = NSMutableArray()
                    
                    for (index, element) in list.enumerated() {
                        if(index == 0){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                            
                        }
                        if(index == 1){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        
                        if(index == 2){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        if(index == 3){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                    }
                    
                    valueForKey = (arrayLoc as! [NSMutableDictionary])
                    
                    let dictData = valueForKey![indexPath.row]
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Note"){
                        return UITableView.automaticDimension//350
                       // return (DeviceType.IS_IPAD ? 75 : 60)
                    }
                    if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                        return (DeviceType.IS_IPAD ? 100 : 85)
                    }
                    if("\(dictData.value(forKey: "EntityType")!)" == "Task"){
                        return (DeviceType.IS_IPAD ? 85 : 70)
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Email"){
                        
                        if let emailContent = dictData.value(forKey: "EmailNotes"){
                            
                            if("\(emailContent)" == "<null>")
                            {
                                return (DeviceType.IS_IPAD ? 95 : 80)
                            }
                            else
                            {
                                return (DeviceType.IS_IPAD ? 200 : 180)
                            }
                        }
                        else
                        {
                            return (DeviceType.IS_IPAD ? 95 : 80)
                        }
                    }
                    return 0
                }
                
                
            default:
                if(indexPath.section == 0)
                {
                    return (DeviceType.IS_IPAD ? 220 : 210)
                }
                return (DeviceType.IS_IPAD ? 200 : 180)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        
        
        if tableView == tblContactNew
        {
            let dictOfContacts = arrOfContacts.object(at: indexPath.row) as! NSDictionary
            goToContactDetails(dictContact: dictOfContacts)
            
        }
        else
        {
            if detailType == OpportunityDetailType.GeneralNew
            {
                if(indexPath.section == 0) //Contact
                {
                    let dictOfContacts = arrOfContacts.object(at: indexPath.row) as! NSDictionary
                    goToContactDetails(dictContact: dictOfContacts)
                }
                else if(indexPath.section == 1) //Company
                {
                    goToCompanyEdit()
                }
                else if(indexPath.section == 2) // Open Tasks
                {
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    let strTaskId = "\(dictData.value(forKey: "LeadTaskId") ?? "")"
                    vc.taskId = "\(strTaskId)"
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else if(indexPath.section == 3) // Notes
                {
                    
                }
                else
                {
                    
                }
                
            }
            else if detailType == OpportunityDetailType.Task
            {
                /* if indexPath.section == 0
                 {
                 let dict = aryTasks.object(at: indexPath.row) as! NSDictionary
                 goToTaskDetail(dict: dict)
                 
                 }
                 else if indexPath.section == 1
                 {
                 let dict = aryActivities.object(at: indexPath.row) as! NSDictionary
                 if ("\((dict.value(forKey: "activityId") ?? ""))".count > 0 || "\((dict.value(forKey: "ActivityId") ?? ""))".count > 0)
                 {
                 goToActivityDetail(dict: dict)
                 
                 }
                 else
                 {
                 showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please sysc activity online, before proceed", viewcontrol: self)
                 }
                 }*/
                
            }
            else if detailType == OpportunityDetailType.DocumentsNew
            {
                let dict = arrAllDocuments.object(at: indexPath.row) as! NSDictionary
                
                let cell = tblviewOpportunityDetails.cellForRow(at: indexPath) as! LeadVCCell
                
                if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Image") == .orderedSame
                {
                    let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
                    let vc = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as! PreviewImageVC
                    vc.img = cell.imgViewDocument.image!
                    self.present(vc, animated: false, completion: nil)
                    
                }
                else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Document") == .orderedSame
                {
                    let strImagePath = Global().strDocName(fromPath: "\( dict.value(forKey: "Path") ?? "")")
                    
                    //goToPdfView(strPdfName: strImagePath!)
                    goToPdfView(strPdfName: "\(dict.value(forKey: "Path") ?? "")")
                    
                    
                }
                else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Audio") == .orderedSame
                {
                    
                    let audioURL = "\("\( dict.value(forKey: "Path") ?? "")")"
                    let player = AVPlayer(url: URL(string: audioURL)!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                    /* let strImagePath = Global().strDocName(fromPath: "\( dict.value(forKey: "Path") ?? "")")
                     
                     let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)
                     
                     if isImageExists!  {
                     
                     playAudioo(strAudioName: strImagePath!)
                     
                     }else {
                     
                     showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                     
                     }*/
                    
                }
                else if "\(dict.value(forKey: "MediaTypeSysName") ?? "")".caseInsensitiveCompare("Video") == .orderedSame
                {
                    let audioURL = "\("\( dict.value(forKey: "Path") ?? "")")"
                    let player = AVPlayer(url: URL(string: audioURL)!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                    /* let strVideoPath = Global().strDocName(fromPath: "\( dict.value(forKey: "Path") ?? "")")
                     
                     
                     let isVideoExists: Bool? = checkIfImageExistAtPath(strFileName: strVideoPath!)
                     
                     if isVideoExists!  {
                     
                     let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                     let documentsDirectory = paths[0]
                     let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(strVideoPath!)")
                     print(path)
                     let player = AVPlayer(url: path)
                     let playerViewController = AVPlayerViewController()
                     playerViewController.player = player
                     self.present(playerViewController, animated: true) {
                     playerViewController.player!.play()
                     }
                     
                     }
                     else
                     {
                     
                     var vedioUrl = strURL + "\("\( dict.value(forKey: "Path") ?? "")")"
                     
                     vedioUrl = vedioUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                     
                     let player = AVPlayer(url: URL(string: vedioUrl)!)
                     let playerViewController = AVPlayerViewController()
                     playerViewController.player = player
                     self.present(playerViewController, animated: true) {
                     playerViewController.player!.play()
                     }
                     
                     }*/
                    
                }
                
                
                
            }
            else if detailType == OpportunityDetailType.Timeline
            {
                if(indexPath.section == 0) //Task
                {
                    
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
                    let dictData = aryTasksNew.object(at: indexPath.row) as! NSDictionary
                    let strTaskId = "\(dictData.value(forKey: "LeadTaskId") ?? "")"
                    vc.taskId = "\(strTaskId)"
                    self.navigationController?.pushViewController(vc, animated: false)
                    
                }
                else if(indexPath.section == 1) // History
                {
                }
                else
                {
                    let key = aryAllKeys[indexPath.section - 2]
                    var valueForKey = dictGroupedTimeline[key as! AnyHashable]
                    
                    let list = [Int](1...4)
                    let arrayLoc = NSMutableArray()
                    
                    for (index, element) in list.enumerated() {
                        if(index == 0){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                            
                        }
                        if(index == 1){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        
                        if(index == 2){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                        if(index == 3){
                            
                            for item in valueForKey!{
                                
                                let dictLoc = item as NSDictionary
                                
                                if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                                    
                                    arrayLoc.add(dictLoc)
                                }
                                
                            }
                        }
                    }
                    valueForKey = (arrayLoc as! [NSMutableDictionary])
                    
                    let dictData = valueForKey![indexPath.row]
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Note"){
                        
                        
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                        
                        self.goToActivityDetails(strId: "\(dictData.value(forKey: "ActivityId")!)")
                        
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Task"){
                        
                        self.goToTaskDetails(strId: "\(dictData.value(forKey: "LeadTaskId")!)")
                        
                    }
                    
                    if("\(dictData.value(forKey: "EntityType")!)" == "Email"){
                        
                        self.goToEmailDetails(dictEmail: dictData)
                        
                    }
                }
                
                
            }
            else
            {
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        if detailType == OpportunityDetailType.Task
        {
            let viewheader = CardView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
            viewheader.backgroundColor = UIColor.lightColorForCell()
            
            if(section == 0)
            {
                
                let button = UIButton()
                button.frame = CGRect( x: tblviewOpportunityDetails.frame.maxX - 100, y: 0, width: 100, height: (DeviceType.IS_IPAD ? 50 : 40))
                button.setTitle("View More", for: .normal)
                button.titleLabel?.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 16 : 14))
                button.addTarget(self, action: #selector(viewMoreTask), for: .touchUpInside)
                
                button.setTitleColor(UIColor.theme(), for: .normal)
                
                if aryTasks.count > 0
                {
                    viewheader.addSubview(button)
                }
            }
            else if(section == 1)
            {
                
                let button = UIButton()
                button.frame = CGRect( x: tblviewOpportunityDetails.frame.maxX - 100, y: 0, width: 100, height: (DeviceType.IS_IPAD ? 50 : 40))
                button.setTitleColor(UIColor.theme(), for: .normal)
                button.titleLabel?.font = UIFont.boldSystemFont(ofSize: (DeviceType.IS_IPAD ? 16 : 14))
                
                button.addTarget(self, action: #selector(viewMoreActivity), for: .touchUpInside)
                
                button.setTitle("View More", for: .normal)
                if aryActivities.count > 0
                {
                    viewheader.addSubview(button)
                }
                
            }
            return viewheader
        }
        else
        {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if detailType == OpportunityDetailType.Task
        {
            if section == 0
            {
                if aryTasks.count == 0
                {
                    return 1.0
                }
                else
                {
                    return (DeviceType.IS_IPAD ? 50 : 40)
                }
            }
            else
            {
                if aryActivities.count > 0
                {
                    return (DeviceType.IS_IPAD ? 50 : 40)
                }
                else
                {
                    return 1.0
                }
                
            }
        }
        else
        {
            return 1.0
        }
    }
    

    // MARK: -  ------------------ Table Button Action  ------------------
  
    
    @objc func actionOnMsgPrimaryPhone(sender:UIButton)
    {
        
    }
    @objc func actionOnCallPrimaryPhone(sender:UIButton)
    {
        
    }
    @objc func actionOnMsgSecondaryPhone(sender:UIButton)
    {
        
    }
    @objc func actionOnCallSecondaryPhone(sender:UIButton)
    {
        
    }
    
    @objc func actionOnMsgCell(sender:UIButton)
    {
        
    }
    @objc func actionOnCallCell(sender:UIButton)
    {
        
    }
    @objc func actionOnPrimaryEmail(sender:UIButton)
    {
        
    }
    @objc func actionOnSecondaryEmail(sender:UIButton)
    {
        
    }
     // MARK:- ------- Contact Cell Action --------
    
    @objc func actionOnDeAssociateContact(sender: UIButton!)
    {
        
        if(isInternetAvailable() == false)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            
            let dictOfContacts = arrOfContacts.object(at: sender.tag) as! NSDictionary
           /* let ary = dictLeadData.value(forKey: "CrmCompanies") as! NSArray
            
            let dict = ary[sender.tag] as! NSDictionary*/
            
            let crmCompanyIdLocal = "\(dictOfContacts.value(forKey: "CrmCompanyId") ?? "")"
            let crmCContactIdLocal = "\(dictOfContacts.value(forKey: "CrmContactId") ?? "")"
            
            self.diAssociateContact(strCrmContactId: crmCContactIdLocal, strCrmCompanyId: crmCompanyIdLocal)
            
        }
        
    }
    @objc func actionOnDeAssociateCompany(sender: UIButton!)
    {
        
        if(isInternetAvailable() == false)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            
          
            let crmCompanyIdLocal = "\(dictLeadData.value(forKeyPath: "AccountCompany.CrmCompanyId") ?? "")"
            let crmCContactIdLocal = ""
            
            self.diAssociateCompany(strCrmContactId: crmCContactIdLocal, strCrmCompanyId: crmCompanyIdLocal)
            
        }
        
    }
     // MARK:- ------- Timeline Activity Cell Action --------
    
    @objc func actionOnActvityComment(sender: UIButton!)
    {
        
        if(isInternetAvailable() == false)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            //indexPath.row*1000+indexPath.section
            
            let row = sender.tag/1000
            let section = sender.tag%1000
            
            let key = aryAllKeys[section - 2]
            var valueForKey = dictGroupedTimeline[key as! AnyHashable]
            
            let list = [Int](1...4)
            let arrayLoc = NSMutableArray()
            
            for (index, element) in list.enumerated() {
                if(index == 0){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                    
                }
                if(index == 1){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                }
                
                if(index == 2){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                }
                if(index == 3){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                }
            }
            
            valueForKey = (arrayLoc as! [NSMutableDictionary])
            let dictData = valueForKey![row]
            if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                
                 let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "CommentList_iPhoneVC") as! CommentList_iPhoneVC
                            
                controller.iD_PreviousView = "\(dictData.value(forKey: "ActivityId")!)"
                self.navigationController?.pushViewController(controller, animated: false)
                
            }
            
        }
        
    }
    // MARK: -  ------------------------------ Actions ------------------------------
    
    @objc func actionOnTaskCheckMark(sender: UIButton!)
    {
        self.view.endEditing(true)
        /*if(aryIndexPath.contains(sender.tag))
         {
         aryIndexPath.remove(sender.tag)
         }
         else
         {
         aryIndexPath.add(sender.tag)
         }
         
         tblviewOpportunityDetails.reloadData()*/
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            let dict = (aryTasksNew.object(at: sender.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            
            let tagIndex = sender.tag as Int
            
            //callAPIToUpdateTaskMarkAsDone(dictData: dict , tag: tagIndex : Int)
            callAPIToUpdateTaskMarkAsDone(dictData: dict, tag: tagIndex)
        }
        
    }
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.view.endEditing(true)
        nsud.set(true, forKey: "fromOpportunityDetail")
        nsud.synchronize()
        
        UserDefaults.standard.set(true, forKey: "RefreshAssociations_ContactDetails")
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnTask(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if detailType == OpportunityDetailType.Task
        {
            
        }
        else
        {
            btnTask.backgroundColor = hexStringToUIColor(hex: "D0B50E")
            btnTask.setTitleColor(UIColor.white, for: .normal)
            
            btnNotes.backgroundColor = UIColor.white
            btnNotes.setTitleColor(UIColor.black, for: .normal)
            btnDocument.backgroundColor = UIColor.white
            btnDocument.setTitleColor(UIColor.black, for: .normal)
            
            btnDetails.backgroundColor = UIColor.white
            btnDetails.setTitleColor(UIColor.black, for: .normal)
            
            
            detailType = OpportunityDetailType.Task
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()
            tblviewOpportunityDetails.reloadData()
        }
    }
    
    @IBAction func actionOnNotes(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if detailType == OpportunityDetailType.Notes
        {
            
        }
        else
        {
            
            //getAllNotes()
            
            
            btnNotes.backgroundColor = hexStringToUIColor(hex: "D0B50E")
            btnNotes.setTitleColor(UIColor.white, for: .normal)
            
            btnTask.backgroundColor = UIColor.white
            btnTask.setTitleColor(UIColor.black, for: .normal)
            btnDocument.backgroundColor = UIColor.white
            btnDocument.setTitleColor(UIColor.black, for: .normal)
            
            btnDetails.backgroundColor = UIColor.white
            btnDetails.setTitleColor(UIColor.black, for: .normal)
            
            detailType = OpportunityDetailType.Notes
            fetchAllNotesFromCoreData()
            //tblviewOpportunityDetails.reloadData()
        }
    }
    
    @IBAction func actionOnDocument(_ sender: UIButton)
    {
        self.view.endEditing(true)
      /*  if detailType == OpportunityDetailType.Document
        {
            
        }
        else
        {
            //getAllDocuments()
            
            btnDocument.backgroundColor = hexStringToUIColor(hex: "D0B50E")
            btnDocument.setTitleColor(UIColor.white, for: .normal)
            
            btnTask.backgroundColor = UIColor.white
            btnTask.setTitleColor(UIColor.black, for: .normal)
            btnNotes.backgroundColor = UIColor.white
            btnNotes.setTitleColor(UIColor.black, for: .normal)
            
            btnDetails.backgroundColor = UIColor.white
            btnDetails.setTitleColor(UIColor.black, for: .normal)
            
            detailType = OpportunityDetailType.Document
            fetchAllDocumentsFromCoreData()
            //tblviewOpportunityDetails.reloadData()
        }*/
    }
    
    @IBAction func actionOnDetails(_ sender: UIButton) {
        
        self.view.endEditing(true)
        btnDetails.backgroundColor = hexStringToUIColor(hex: "D0B50E")
        btnDetails.setTitleColor(UIColor.white, for: .normal)
        
        btnTask.backgroundColor = UIColor.white
        btnTask.setTitleColor(UIColor.black, for: .normal)
        btnNotes.backgroundColor = UIColor.white
        btnNotes.setTitleColor(UIColor.black, for: .normal)
        
        btnDocument.backgroundColor = UIColor.white
        btnDocument.setTitleColor(UIColor.black, for: .normal)
        
        
        detailType = OpportunityDetailType.Details
        tblviewOpportunityDetails.reloadData()
        
    }
    
    
    @IBAction func actionOnAdd(_ sender: UIButton)
    {
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
           alert.view.tintColor = UIColor.black

                let Task = UIAlertAction(title: "Add Task", style: .default , handler:{ (UIAlertAction)in
                    self.goToAddTaskVC()
                })
                Task.setValue(#imageLiteral(resourceName: "Task"), forKey: "image")
                Task.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                alert.addAction(Task)
        
        let Activity = UIAlertAction(title: "Add Activity", style: .default , handler:{ (UIAlertAction)in
            self.goToAddActivityVC()
        })
        Activity.setValue(#imageLiteral(resourceName: "Add Activity"), forKey: "image")
        Activity.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Activity)
       
        let Notes = UIAlertAction(title: "Add Notes", style: .default , handler:{ (UIAlertAction)in
               self.goToAddNote()
         })
         Notes.setValue(#imageLiteral(resourceName: "Add Notes"), forKey: "image")
         Notes.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
         alert.addAction(Notes)
        
    
        
        /*alert.addAction(UIAlertAction(title: "Add Opportunity", style: .default , handler:{ (UIAlertAction)in
            
            self.goToAddLead()
            
        }))*/
        let Contact = UIAlertAction(title: "Associate Contact", style: .default , handler:{ (UIAlertAction)in
                       self.gotoAssociateContact()
               })
               Contact.setValue(#imageLiteral(resourceName: "Icon ionic-md-person"), forKey: "image")
               Contact.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
               alert.addAction(Contact)
        
        let Company = UIAlertAction(title: "Associate Company", style: .default , handler:{ (UIAlertAction)in
            self.gotoAssociateCompany()
        })
        Company.setValue(#imageLiteral(resourceName: "Aso Company"), forKey: "image")
        Company.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Company)
        
        let Document = UIAlertAction(title: "Add Document", style: .default , handler:{ (UIAlertAction)in
            self.gotoAddDocument()
        })
        Document.setValue(#imageLiteral(resourceName: "Add Document"), forKey: "image")
        Document.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Document)
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
       if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
        }
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    
    @IBAction func actionOnEdit(_ sender: UIButton)
    {
        self.view.endEditing(true)
        goToEditOpportunityVC()
    }
    
    @IBAction func actionOnCall(_ sender: UIButton)
    {
        self.view.endEditing(true)
        makeCall()
    }
    
    @IBAction func actionOnEmail(_ sender: UIButton)
    {
        self.view.endEditing(true)
        sendMail()
    }
    
    @IBAction func actionOnTextMessage(_ sender: UIButton)
    {
        self.view.endEditing(true)
        sendMessage()
    }
    
    @IBAction func actionOnAddress(_ sender: Any)
    {
        self.view.endEditing(true)
        var strAddress = ""
        if dictLeadData.value(forKey: "ServiceAddress") is NSDictionary
        {
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? "")".count > 0
            {
                strAddress = "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? ""), "
            }
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address2") ?? "")".count > 0
            {
                strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.Address2") ?? "")"), "
            }
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.CityName") ?? "")".count > 0
            {
                strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.CityName") ?? "")"), "
            }
            
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? "")".count > 0
            {
                let stateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? ""))")
                
                strAddress = "\(strAddress)\(stateName ?? ""), "
            }
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Zipcode") ?? "")".count > 0
            {
                strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.Zipcode") ?? "")")"
            }
            
        }
        if dictLeadData.value(forKey: "ServiceAddress") is NSDictionary
        {
            
            strAddress = Global().strCombinedAddress(dictLeadData.value(forKey: "ServiceAddress") as? [AnyHashable : Any])
        }

        if strAddress.count > 0
        {
            
            let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
            // Create the actions
            
            let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                //[global redirectOnAppleMap:self :_txtViewAddress_ServiceAddress.text]
                Global().redirect(onAppleMap: self, strAddress)
                
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnCompanyEdit(_ sender: Any)
    {
        self.view.endEditing(true)
      /*  let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "CompanyDetailViewControlleriPhone") as! CompanyDetailViewControlleriPad
        
        // let dictMutable = getMutableDictionaryFromNSManagedObject(obj: dictLeadData) as NSDictionary
        var dict = NSDictionary()
        
        dict = createCompanyDetailData()
        
        vc.dictContactDetail = dict as NSDictionary as? [AnyHashable : Any]
        
        vc.strFromWhere = "fromOpportunityDetailVC"
        self.present(vc, animated: false, completion: nil)*/
        
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
        controller.strCRMCompanyIdGlobal = "\(dictLeadData.value(forKeyPath: "AccountCompany.CrmCompanyId")!)"
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    @IBAction func actionOnTaskActivity(_ sender: UIButton)
    {
        self.view.endEditing(true)
        clearNsud()
           let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone

         testController.strFromVC = "DashBoardView"//"WebLeadVC"
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    @IBAction func actionOnContactList(_ sender: Any)
    {
        self.view.endEditing(true)
        clearNsud()
        goToContactList()
    }
    
    @IBAction func actionOnMore(_ sender: UIButton)
    {
        self.view.endEditing(true)
        clearNsud()
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
         alert.view.tintColor = UIColor.black
     
        
        let Near = (UIAlertAction(title: "Near By", style: .default , handler:{ (UIAlertAction)in
            
            self.goToNearBy()
            
        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
        Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Near)
        
        let Signed = (UIAlertAction(title: "Signed View Agreements", style: .default , handler:{ (UIAlertAction)in
            
            self.goToSignedViewAgreement()
            
        }))
        
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
         Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
         alert.addAction(Signed)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
         if let popoverController = alert.popoverPresentationController {
                  popoverController.sourceView = sender as UIView
                  popoverController.sourceRect = sender.bounds
                  popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
              }
              
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    
    @IBAction func actionOnDashboard(_ sender: Any)
    {
        self.view.endEditing(true)
        clearNsud()
        goToDasboard()
    }
    
    @IBAction func actionOnAppointment(_ sender: Any)
    {
        self.view.endEditing(true)
        clearNsud()
        goToAppointment()
    }
    
    @IBAction func actionOnGeneralNew(_ sender: Any)
    {
        self.view.endEditing(true)
       // btnFilterTimeLine.isHidden = true
        const_btnFilterTimeLine_W.constant = 0 + (DeviceType.IS_IPAD ? 50 : 40)
       // const_LblBar_L.constant = 5
        const_LblLine_AbouveTable_H.constant = 1
        //const_ViewDetail_H.constant = 115
        btnGeneralNew.titleLabel?.textColor = UIColor.gray
        btnTimeline.titleLabel?.textColor = UIColor.lightGray
        //btnDetailsNew.titleLabel?.textColor = UIColor.lightGray
        btnDocumentsNew.titleLabel?.textColor = UIColor.lightGray
        
        detailType = OpportunityDetailType.GeneralNew
        tblviewOpportunityDetails.reloadData()
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations:
                                 {
                                  self.const_LblBar_L.constant = 5
                                 self.view.layoutIfNeeded()
                               }, completion: { finished in
                                 print("Basket doors opened!")
                               })

    }
    
    @IBAction func actionOnDocumentNew(_ sender: Any)
    {
        self.view.endEditing(true)
       // btnFilterTimeLine.isHidden = true
        const_btnFilterTimeLine_W.constant = 0 + (DeviceType.IS_IPAD ? 50 : 40)
        //const_ViewDetail_H.constant = 0
        const_LblLine_AbouveTable_H.constant = 0

       // const_LblBar_L.constant = btnDocumentsNew.frame.origin.x
       // btnGeneralNew.titleLabel?.textColor = UIColor.lightGray
       // btnDocumentsNew.titleLabel?.textColor = UIColor.gray
        //btnDetailsNew.titleLabel?.textColor = UIColor.lightGray
       // btnTimeline.titleLabel?.textColor = UIColor.lightGray
        
        
        btnDocumentsNew.setTitleColor(UIColor.darkGray, for: .normal)
        btnTimeline.setTitleColor(UIColor.lightGray, for: .normal)
        btnDetailsNew.setTitleColor(UIColor.lightGray, for: .normal)

        detailType = OpportunityDetailType.DocumentsNew
        tblviewOpportunityDetails.reloadData()
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations:
                          {
                           self.const_LblBar_L.constant = self.btnDocumentsNew.frame.origin.x
                          self.view.layoutIfNeeded()
                        }, completion: { finished in
                          print("Basket doors opened!")
                        })

    }
    
    @IBAction func actionOnDetailNew(_ sender: Any)
    {
        self.view.endEditing(true)
        //btnFilterTimeLine.isHidden = true
        const_btnFilterTimeLine_W.constant = 0 + (DeviceType.IS_IPAD ? 50 : 40)
        //const_LblBar_L.constant = btnDetailsNew.frame.origin.x
        //const_ViewDetail_H.constant = 0
        const_LblLine_AbouveTable_H.constant = 0

//        btnGeneralNew.titleLabel?.textColor = UIColor.lightGray
//        btnTimeline.titleLabel?.textColor = UIColor.lightGray
//        btnDetailsNew.titleLabel?.textColor = UIColor.gray
        
        btnDocumentsNew.setTitleColor(UIColor.lightGray, for: .normal)
        btnTimeline.setTitleColor(UIColor.lightGray, for: .normal)
        btnDetailsNew.setTitleColor(UIColor.darkGray, for: .normal)

        
        detailType = OpportunityDetailType.DetailsNew
        tblviewOpportunityDetails.reloadData()
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations:
                              {
                               self.const_LblBar_L.constant = self.btnDetailsNew.frame.origin.x
                              self.view.layoutIfNeeded()
                            }, completion: { finished in
                              print("Basket doors opened!")
                            })

       
        
    }
    
    @IBAction func actionOnTimeLine(_ sender: Any)
    {
        self.view.endEditing(true)
        //btnFilterTimeLine.isHidden = false
        const_btnFilterTimeLine_W.constant = (DeviceType.IS_IPAD ? 50 : 40)
        //const_ViewDetail_H.constant = 0
        const_LblLine_AbouveTable_H.constant = 0

       // const_LblBar_L.constant = btnTimeline.frame.origin.x
       // btnGeneralNew.titleLabel?.textColor = UIColor.lightGray
        //btnTimeline.titleLabel?.textColor = UIColor.gray
        //btnDetailsNew.titleLabel?.textColor = UIColor.lightGray
        //btnDocumentsNew.titleLabel?.textColor = UIColor.lightGray
        
        btnDocumentsNew.setTitleColor(UIColor.lightGray, for: .normal)
        btnTimeline.setTitleColor(UIColor.darkGray, for: .normal)
        btnDetailsNew.setTitleColor(UIColor.lightGray, for: .normal)

        detailType = OpportunityDetailType.Timeline
        tblviewOpportunityDetails.reloadData()
        
        /*if(aryAllKeys.count == 0){
            
           // FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                
                self.callAPToGetTimeLine()
                
            }
        }*/
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
           self.callAPToGetTimeLine()
            
        }
        
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations:
                                 {
                                    self.const_LblBar_L.constant = self.btnTimeline.frame.origin.x
                                 self.view.layoutIfNeeded()
                               }, completion: { finished in
                                 print("Basket doors opened!")
                               })
        
    }
    
    @IBAction func actionOnScheduleNow(_ sender: Any)
    {
        self.view.endEditing(true)
        let strCrmContactId =  "\(dictLeadData.value(forKey: "CrmContactId") ?? "")"
        if strCrmContactId.count == 0 || strCrmContactId == ""
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please add contact to schedule", viewcontrol: self)
        }
        else
        {
            goToSchdeuleNow()
        }
        
    }
    
    @IBAction func actionOnSliderProgress(_ sender: UISlider)
    {
        self.view.endEditing(true)
        //let currentValue = (Float(sender.value)) * 100
        //let val = String(format: "%.00f",currentValue)
        var val = round(sender.value * 100)
        val =  val.rounded(to: 5)
        let finalVal = String(describing: Int(val))
        lblProgressValue.text = "\(finalVal)%"
    
        if progressConfidenceLvel.isTracking
        {
            print("Track on")
        }
        else
        {
            print("Track Release")
            if !isInternetAvailable()
            {
                //fetchAllNotesFromCoreData()
            }
            else
            {
                updateConfidenceLevel()
            }
            
        }
    }
    
    @IBAction func actionOnContactUserName(_ sender: Any)
    {
        self.view.endEditing(true)
        if "\(dictLeadData.value(forKey: "CrmContactId")!)" == "" || "\(dictLeadData.value(forKey: "CrmContactId")!)".count == 0
        {

            
        } else {
            
             let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as! ContactDetailsVC_CRMContactNew_iPhone
            controller.strCRMContactIdGlobal = "\(dictLeadData.value(forKey: "CrmContactId")!)"
            controller.dictContactDetailFromList = dictLeadData
            self.navigationController?.pushViewController(controller, animated: false)
            
        }

    }
    @IBAction func actionOnFilterTimeLine(_ sender: UIButton)
    {
             self.view.endEditing(true)
             self.view.endEditing(true)
             
             let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterTimeLineVC") as! FilterTimeLineVC
             controller.delegate = self
             self.navigationController?.present(controller, animated: false, completion: nil)
             
         }
    
    @objc func actionOnFilterTimeLineNew()
    {
        self.view.endEditing(true)
        self.view.endEditing(true)
        
       let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterTimeLineVC") as! FilterTimeLineVC
        controller.delegate = self
        self.navigationController?.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func actionOnGoToAppointments(_ sender: Any)
    {
        self.view.endEditing(true)
        clearNsud()
        goToAppointment()
    }
    
    @IBAction func actionOnOppValueText(_ sender: Any)
    {
        print("Call function")
    }
    
    @IBAction func actionOnOpportunityValue(_ sender: Any)
    {
        let ac = UIAlertController(title: "Enter Opportunity Value", message: nil, preferredStyle: .alert)
        
        
        ac.addTextField { (textField) in
            
            textField.placeholder = "Enter Amount"
            textField.text = self.txtFldOpportunityValue.text
            textField.delegate = self
            textField.keyboardType = .decimalPad
            textField.tag = 77
            
        }
        
//        let ok = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
//
//        }

        let submitAction = UIAlertAction(title: "Submit", style: .default)
        { [unowned ac] _ in
            
            
            let answer = ac.textFields![0]
            
            let amount = ("\(self.txtFldOpportunityValue.text ?? "0.00")" as NSString).doubleValue
            let amountText = ("\(answer.text ?? "")" as NSString).doubleValue
            
            if amount == amountText
            {
                
            }
            else
            {
               
                self.strOppAmount =  String(format: "%.02f", Double(amountText))
                self.updateOpportunityValue()
            }
            self.view.endEditing(true)
            print("\(answer)")
            // do something interesting with "answer" here
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        {  _ in
           
             self.view.endEditing(true)
            // do something interesting with "answer" here
        }

        ac.addAction(submitAction)
        ac.addAction(cancelAction)
        //ac.addAction(ok)
        present(ac, animated: true)
        
    }
    // MARK: -  ------------------------------ API Calling  ------------------------------
    func getBasicInfoOpportunity()
    {
        if !isInternetAvailable()
        {
            
        }
        else
        {
            //https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/GetLeadById?LeadId=216506


            strRefId = strRefIdNew
            
            //140176&reftype=Lead
            var strUrl = "\(strServiceUrlMain)/api/LeadNowAppToSalesProcess/GetLeadById?LeadId=\(strRefId)"  as String
            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            

            
            //FTIndicator.showProgress(withMessage: "Fetching Notes...", userInteractionEnable: false)
            self.dispatchGroup.enter()
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strUrl, responseStringComing: "AddTaskVC_CRMNew_iPhone") { (Response, Status) in
                
                self.dispatchGroup.leave()
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    nsud.setValue("", forKey: "fromAddNotes")
                    nsud.synchronize()
                    
                }
                if(Status)
                {
                    
                    let statusNew = (Response as NSDictionary).allKeys.contains(where: { (k) -> Bool in
                                                                  "Message" == (k as AnyObject)as! String
                                                              })
                                           print(statusNew)
                                           

                    if statusNew
                    {
                       
                          // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    }
                    else
                    {
                        let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (Response as! [AnyHashable : Any])) as NSDictionary
                                                                 print(dictNew)
                        self.dictLeadData = dictNew
                        
                        self.setInitialValues()
                    }
                    
                    
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                }
                else
                {
                    
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                }
                
            }
        
        }
        
    }
    
    
    
    func getAllNotes()
    {
        self.arrAllNotes = NSMutableArray()
        
        if !isInternetAvailable()
        {
            //fetchAllNotesFromCoreData()
        }
        else
        {
            //https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/GetLeadNotesListV2?refid=140176&reftype=Lead
            strReftype = "Lead"
            strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "OpportunityId") ?? "")"
            
            //140176&reftype=Lead
            var strUrl = "\(strServiceUrlMain)\(UrlGetAllNotes)\(strRefId)&reftype=\(strReftype)"  as String
            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
             self.dispatchGroup.enter()
            //FTIndicator.showProgress(withMessage: "Fetching Notes...", userInteractionEnable: false)
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
                 self.dispatchGroup.leave()
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    nsud.setValue("", forKey: "fromAddNotes")
                    nsud.synchronize()
                    
                }
                if(Status)
                {
                    
                    
                    let arrData = Response["data"] as! NSArray
                    
                    if arrData.isKind(of: NSArray.self)
                    {
                        let arrAllData = NSMutableArray()
                        
                        for item in arrData
                        {
                            let  dict = item as! NSDictionary
                            
                            let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))
                            
                            arrAllData.add(dictNew as Any)
                            
                            
                        }
                        //self.arrAllNotes = arrAllData
                        
                        /*let str = getJson(from: arrAllData)
                        print("\(str ?? "")" as Any)
                        
                        self.saveNotesToCoreData(arrAllData: arrAllData)
                        self.fetchAllNotesFromCoreData()*/
                        
                        self.arrAllNotes = arrAllData.mutableCopy() as! NSMutableArray
                        self.tblviewOpportunityDetails.reloadData()
                        
                    }
                    else
                    {
                        
                        //  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                    
                }
                else
                {
                    
                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    func getAllTask()
    {
        if(isInternetAvailable() == false)
        {
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
            }
            //tblviewLeadDetails.reloadData()
           
           // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            
            strReftype = "Lead" //"WebLead"//
            strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "opportunityId") ?? "")"

            //https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3


            let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3"
            
            var dict = [String:Any]()
            
            dict = ["RefId":strRefId,
                    "RefType":enumRefTypeOpportunity,
                    "IsDefault":"false",
                    "SkipDefaultDate":"false",
                    "Status":"",
                    "FromDate":"",
                    "ToDate":"",
                    "EmployeeId":"",
                    "TakeRecords":"",
                    "TaskTypeId":"",
                    "PriorityIds":""
            ]
            self.dispatchGroup.enter()
            WebService.postRequestWithHeaders(dictJson: dict as NSDictionary, url: strURL, responseStringComing: "TimeLine") { (response, status) in
                
                 self.dispatchGroup.leave()
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
                
                if(status == true){
                    
                    var dictTemp = NSDictionary()
                    dictTemp = response.value(forKey: "data") as! NSDictionary
                    
                   
                    
                    let aryTemp = NSMutableArray()
                    
                    if((dictTemp.value(forKey: "Tasks") as! NSArray).count > 0)
                    {
                        for item in dictTemp.value(forKey: "Tasks") as! NSArray
                        {
                            let dictData = removeNullFromDict(dict: (item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                            aryTemp.add(dictData)
                        }
                    }
                    
                    print("Task >>>  \(aryTemp)")
                    self.aryTasksNew = NSMutableArray()
                    self.aryTasksNew = aryTemp.mutableCopy() as! NSMutableArray
                    self.tblviewOpportunityDetails.reloadData()
                }
                else
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                    }
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }
        }
        
        
      
      /*  strReftype = "WebLead" //"WebLead"//
        strRefId = "\(dictLeadData.value(forKey: "leadId") ?? "")"
        
        //https://pcrmsstaging.pestream.com/api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3


        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV2_SubObjs?refid=\(strRefId)&reftype=\(strReftype)"
        
        DispatchQueue.main.async {
            
           // FTIndicator.showProgress(withMessage: "Fetching Tasks...", userInteractionEnable: false)
            
        }
        
        //FTIndicator.showProgress(withMessage: "Fetching Tasks...", userInteractionEnable: false)
        
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            //FTIndicator.dismissProgress()
            
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
            }
            
            
            if(status == true)
            {
                if((response.value(forKey: "data") as! NSArray).count > 0)
                {
                    self.aryTasks.removeAllObjects()
                    
                    self.saveTaskListToLocalDb(data: response.value(forKey: "data") as! NSArray)
                    
                }
                else
                {
                    
                    DispatchQueue.main.async{
                        
                        // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
                        FTIndicator.dismissProgress()
                    }
                    
                }
                DispatchQueue.main.async{
                    
                    
                    FTIndicator.dismissProgress()
                }
                
            }
            else
            {
                
                DispatchQueue.main.async{
                    
                    //  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    FTIndicator.dismissProgress()
                }
                
            }
        }*/
    }
    func getAllTaskOld()
    {
        
        strReftype = "Lead"
        strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "opportunityId") ?? "")"
        
        
        
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV2_SubObjs?refid=\(strRefId)&reftype=\(strReftype)"
        
        DispatchQueue.main.async {
            
         //   FTIndicator.showProgress(withMessage: "Fetching Tasks...", userInteractionEnable: false)
            
        }
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
                
            }
            
            if(status == true)
            {
                if((response.value(forKey: "data") as! NSArray).count > 0)
                {
                    self.aryTasks.removeAllObjects()
                    
                    self.saveTaskListToLocalDb(data: response.value(forKey: "data") as! NSArray)
                    
                }
                else
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        //  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
                        
                    }
                    
                    
                }
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
            }
            else
            {
                
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    //   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
                
                
                
            }
        }
    }
    func getAllDocuments()
    {
        self.arrAllNotes = NSMutableArray()
        
        if !isInternetAvailable()
        {
            //fetchAllNotesFromCoreData()
        }
        else
        {
            strReftype = "Lead"
            strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "OpportunityId") ?? "")"
            
            let UrlGetAllDocuments = "/api/LeadNowAppToSalesProcess/GetDocumentsByRefIdAndRefType?refId="
            
            //140176&reftype=Lead
            var strUrl = "\(strServiceUrlMain)\(UrlGetAllDocuments)\(strRefId)&reftype=\(strReftype)"  as String
            
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            //FTIndicator.showProgress(withMessage: "Fetching Documents...", userInteractionEnable: false)
             self.dispatchGroup.enter()
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
                
                self.dispatchGroup.leave()
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    nsud.setValue("", forKey: "fromAddNotes")
                    nsud.synchronize()
                    
                }
                if(Status)
                {
                    let arrData = Response["data"] as! NSArray
                    
                    if arrData.isKind(of: NSArray.self)
                    {
                        let arrAllData = NSMutableArray()
                        
                        for item in arrData
                        {
                            let  dict = item as! NSDictionary
                            
                            let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))
                            
                            arrAllData.add(dictNew as Any)
                            
                            
                        }
                        //self.arrAllDocuments = arrAllData
                       /* let str = getJson(from: arrAllData)
                        
                        print("\(str ?? "")" as Any)
                        self.saveDocumentsToLocalDb(arrAllData: arrAllData)
                        self.fetchAllDocumentsFromCoreData()*/
                        
                        self.arrAllDocuments = arrAllData.mutableCopy() as! NSMutableArray
                        self.tblviewOpportunityDetails.reloadData()
                        
                    }
                    else
                    {
                        
                        //  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                    
                }
                else
                {
                    
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        nsud.setValue("", forKey: "fromAddNotes")
                        nsud.synchronize()
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func callAPIToUpdateTaskMarkAsDone(dictData:NSMutableDictionary, tag:Int)
    {
        var strStatus = ""
        
        if("\(dictData.value(forKey: "Status")!)" == "Open")
        {
            strStatus = "Done"
        }
        else
        {
            strStatus = "Open"
        }
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/ChangeTaskStatus?TaskId=\(dictData.value(forKey: "LeadTaskId")!)&Status=\(strStatus)"
        
      //  FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        print(" callAPIToUpdateTaskMarkAsDone start")
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { (response, status) in
            
            print(" callAPIToUpdateTaskMarkAsDone end")

            self.loader.dismiss(animated: false)
            {
                FTIndicator.dismissProgress()
                
                DispatchQueue.main.async {
                    
                    FTIndicator.dismissProgress()

                }
                
                if(status == true)
                {
                    if("\(response.value(forKey: "data")!)" == "true")
                    {
                        //self.updateTaskToLocalDB(dict: dictData)
                        var dictMutable = NSMutableDictionary()
                        dictMutable = dictData.mutableCopy() as! NSMutableDictionary
                        dictMutable.setValue(strStatus, forKey: "Status")
                        
                        self.aryTasksNew.replaceObject(at: tag, with: dictMutable)
                        self.tblviewOpportunityDetails.reloadData()
                        //self.getAllTask()
                    }
                    else
                    {
                        // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    if "\((dictData.value(forKey: "Status")!))" == "Open"
                    {
                        /*let alertController = UIAlertController(title: "Alert", message: "Do you want to create follow up", preferredStyle: .alert)
                        // Create the actions
                        
                        let okAction = UIAlertAction(title: "Yes - Create", style: UIAlertAction.Style.default)
                        {
                            UIAlertAction in
                            
                           //self.gotoCreateFollowUp(dictData: dictData)
                            
                           self.callAPIToGetTaskDetailById(strId: "\((dictData.value(forKey: "LeadTaskId")!))")

                        }
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                        {
                            UIAlertAction in
                            NSLog("Cancel Pressed")
                        }
                        // Add the actions
                        alertController.addAction(okAction)
                        alertController.addAction(cancelAction)
                        
                        // Present the controller
                        self.present(alertController, animated: true, completion: nil)*/
                        
                      let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "LogAsActivityVC") as! LogAsActivityVC
                                 
                        controller.dictLeadTaskData = NSDictionary()
                        controller.leadTaskId = "\((dictData.value(forKey: "LeadTaskId")!))"
                        controller.dictOfAssociations = NSMutableDictionary()
                        self.navigationController?.pushViewController(controller, animated: false)
                        
                        nsud.set(true, forKey: "fromWebLeadDetailTableAction")
                        nsud.synchronize()
                        nsud.setValue(true, forKey: "isTaskAddedUpdated")
                        nsud.synchronize()
                    }
                }
                else
                {
                    // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    // something went wrong
                }
            }
            
        }
    }
    func callApiForFollowUp(dictData: NSMutableDictionary)
    {         if !isInternetAvailable()
        {
            //showAlertWithoutAnyAction(strtitle: alertInternet, strMessage: ErrorInternetMsg, viewcontrol: self)
        }
        else
        {
            //FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
            print(" callApiForFollowUp start")

            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            var strUrl =  "\(strServiceUrlMain)\(UrlAddUpdateTaskGlobal)"
            strUrl = strUrl.trimmingCharacters(in: .whitespaces)
            
            var key = NSArray()
            var value = NSArray()
            
            key = ["Id",
                   "LeadTaskId",
                   "RefId",
                   "RefType",
                   "TaskName",
                   "DueDate",
                   "ReminderDate",
                   "Description",
                   "AssignedTo",
                   "Priority",
                   "Status",
                   "CreatedBy",
                   "Tags",
                   "ChildActivities",
                   "FollowUpFromTaskId"]
            
            var arrChildActivity = NSMutableArray()
            if (dictData.value(forKey: "childActivities") ?? "") is NSArray
            {
                let arr = (dictData.value(forKey: "childActivities") ?? "") as! NSArray
                if arr.count > 0
                {
                    arrChildActivity = arr.mutableCopy() as! NSMutableArray
                }
            }
            
            value = ["1",
                     "",
                     "\(dictData.value(forKey: "refId") ?? "")",//RefId
                "\(dictData.value(forKey: "refType") ?? "")",//RefType
                "\(dictData.value(forKey: "taskName") ?? "")",
                "\(dictData.value(forKey: "dueDate") ?? "")",
                "\(dictData.value(forKey: "reminderDate") ?? "")",
                "\(dictData.value(forKey: "taskDescription") ?? "")",
                "\(dictData.value(forKey: "assignedTo") ?? "")",
                "\(dictData.value(forKey: "priority") ?? "")",
                "\(dictData.value(forKey: "status") ?? "")",
                "\(dictData.value(forKey: "createdBy") ?? "")",
                "\(dictData.value(forKey: "tags") ?? "")",
                arrChildActivity,
                (dictData.value(forKey: "leadTaskId") ?? "")]
            
            let Url = String(format: strUrl)
            
            guard
                let serviceUrl = URL(string: Url) else { return }
            
            
            let dict_ToSend = NSDictionary(objects:value as! [Any], forKeys:key as! [NSCopying]) as Dictionary
            let parameterDictionary = dict_ToSend
            
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
            request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
            request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
            request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
            
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request.addValue("IOS", forHTTPHeaderField: "Browser")
            
            if (nsud.value(forKey: "LoginDetails") != nil)
            {
                let dict = nsud.value(forKey: "LoginDetails") as! NSDictionary
                request.addValue("\(dict.value(forKeyPath: "Company.HrmsCompanyId") ?? "")", forHTTPHeaderField: "HrmsCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeNumber") ?? "")", forHTTPHeaderField: "EmployeeNumber")
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeId") ?? "")", forHTTPHeaderField: "EmployeeId")
                
                request.addValue("\(dict.value(forKeyPath: "CreatedBy") ?? "")", forHTTPHeaderField: "CreatedBy")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CoreCompanyId") ?? "")", forHTTPHeaderField: "CoreCompanyId")
                
                
                request.addValue("\(dict.value(forKeyPath: "EmployeeName") ?? "")", forHTTPHeaderField: "EmployeeName")
                
                
                request.addValue("\(dict.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "")", forHTTPHeaderField: "SalesProcessCompanyId")
                
                request.addValue("\(dict.value(forKeyPath: "Company.CompanyKey") ?? "")", forHTTPHeaderField: "CompanyKey")
                
            }
            
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                
                print(" callApiForFollowUp end")

                self.loader.dismiss(animated: false)
                {
                    if let response = response {
                        print(response)
                    }
                    if let data = data
                    {
                        do
                        {
                            let json = try JSONSerialization.jsonObject(with: data, options: [])
                            
                            
                            let dict = NSMutableDictionary()
                            
                            dict.setValue(json, forKey: "response")
                            print(dict)
                            
                            print(Global().nestedDictionaryByReplacingNulls(withNil: dict as? [AnyHashable : Any]))
                            
                            let dictResponse = Global().nestedDictionaryByReplacingNulls(withNil:dict as NSDictionary as? [AnyHashable : Any])
                            
                            print(dictResponse ?? 0)
                            
                            DispatchQueue.main.async
                                {
                                    FTIndicator.dismissProgress()
                                    
                                    let alertController = UIAlertController(title: "Alert", message: "Follow up created successfully", preferredStyle: .alert)
                                    // Create the actions
                                    
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default)
                                    {
                                        UIAlertAction in
                                        
                                        self.getAllTask()
                                        
                                        FTIndicator.dismissProgress()
                                        
                                    }
                                    
                                    alertController.addAction(okAction)
                                    
                                    // Present the controller
                                    self.present(alertController, animated: true, completion: nil)
                            }
                        }
                        catch
                        {}
                    }
                }
                
                }.resume()
            
        }
        
    }
    
    fileprivate func callAPIToGetActivityList()
    {
        strReftype = "Lead"
        strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "opportunityId") ?? "")"
        
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetActivitiesByRefIdAndRefTypeAsyncV2_SubObjs?refid=\(strRefId)&reftype=\(strReftype)"
        
        DispatchQueue.main.async {
            
          //  FTIndicator.showProgress(withMessage: "Fetching Activity...", userInteractionEnable: false)
        }
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "") { (response, status) in
            
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
            }
            
            if(status == true)
            {
                if((response.value(forKey: "data") as! NSArray).count > 0)
                {
                    self.aryActivities.removeAllObjects()
                    
                    self.saveActivityListToLocalDb(data: response.value(forKey: "data") as! NSArray)
                    
                }
                else
                {
                    
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                    }
                    
                }
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
            }
            else
            {
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
            }
        }
    }
    
    fileprivate func callAPToGetTimeLine(){
        
        
        if nsud.value(forKey: "DPS_FilterTimeLine") == nil {
            
            setDefaultValuesForTimeLineFilter()
            
        }
        
        if(isInternetAvailable() == false)
        {
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
            }
            tblviewOpportunityDetails.reloadData()
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetTimeline
            
            var dict = [String:Any]()
            
            let dictFilterData = (nsud.value(forKey: "DPS_FilterTimeLine")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
            var arySelectedInteraction = NSMutableArray()
            
            if dictFilterData.value(forKey: "SelectedInteraction") is NSArray {
                
                arySelectedInteraction = (dictFilterData.value(forKey: "SelectedInteraction")as! NSArray).mutableCopy()as! NSMutableArray
                
            }else {
                
                
            }
            strRefId = strRefIdNew
            dict = ["fromDate":dictFilterData.value(forKey: "FromDate") ?? "",
                    "toDate":dictFilterData.value(forKey: "Todate") ?? "",
                    "refId":strRefId,
                    "refType":enumRefTypeOpportunity,
                    "employeeid":dictFilterData.value(forKey: "SelectedUserName") ?? "",
                    "Entities":arySelectedInteraction
            ]
             self.dispatchGroup.enter()
            WebService.postRequestWithHeaders(dictJson: dict as NSDictionary, url: strURL, responseStringComing: "TimeLine") { (response, status) in
                
                
                 self.dispatchGroup.leave()
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                }
                
                if(status == true){
                    
                    //self.dictTimeLine = response.value(forKey: "data") as! NSDictionary
                    
                    self.dictTimeLine = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (response.value(forKey: "data") as! NSDictionary as! [AnyHashable : Any]))! as NSDictionary

                    
                    let aryTemp = NSMutableArray()
                    
                    if self.dictTimeLine.value(forKey: "Activities") is NSArray{
                        if((self.dictTimeLine.value(forKey: "Activities") as! NSArray).count > 0)
                        {
                            for item in self.dictTimeLine.value(forKey: "Activities") as! NSArray{
                                
                                aryTemp.add(item)
                            }
                        }
                    }
                    
                    if self.dictTimeLine.value(forKey: "Emails") is NSArray{
                        if((self.dictTimeLine.value(forKey: "Emails") as! NSArray).count > 0)
                        {
                            for item in self.dictTimeLine.value(forKey: "Emails") as! NSArray{
                                aryTemp.add(item)
                            }
                        }
                    }
                        if self.dictTimeLine.value(forKey: "Notes") is NSArray{
                            if((self.dictTimeLine.value(forKey: "Notes") as! NSArray).count > 0)
                            {
                                for item in self.dictTimeLine.value(forKey: "Notes") as! NSArray{
                                    aryTemp.add(item)
                                }
                            }
                        }
                        if self.dictTimeLine.value(forKey: "Tasks") is NSArray{
                            if((self.dictTimeLine.value(forKey: "Tasks") as! NSArray).count > 0)
                            {
                                for item in self.dictTimeLine.value(forKey: "Tasks") as! NSArray{
                                    aryTemp.add(item)
                                }
                            }
                        }
                    
                    self.arrayTimeline.removeAllObjects()
                    for item in aryTemp{
                        
                        let innerItem = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        let dateClientCreated = Global().convertDate("\(innerItem.value(forKey: "ClientCreatedDate")!)")
                        
                        let dateFormatter = DateFormatter()
                        
                        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                        dateFormatter.dateFormat = "MM/dd/yyyy"
                        
                        let date1 = dateFormatter.date(from: dateClientCreated!)
                        print(date1!)
                        
                        let date = Global().getOnlyDate(date1)
                        print(date!)
                        
                        innerItem.setValue(date, forKey: "ClientCreatedDate")
                        
                        self.arrayTimeline.add(innerItem)
                    }
                    
                    if(self.arrayTimeline.count > 0){
                        
                        let sortedArray = self.arrayTimeline.sorted(by: {
                            (($0 as! NSMutableDictionary)["ClientCreatedDate"] as? Date)! >= (($1 as! NSMutableDictionary)["ClientCreatedDate"] as? Date)!
                        })
                        
                        self.filterTimelineAry = sortedArray as! [NSMutableDictionary]
                        
                        var arr = [NSMutableDictionary]()
                        
                        arr = sortedArray as! [NSMutableDictionary]
                        
                        self.dictGroupedTimeline = Dictionary(grouping: arr) { $0["ClientCreatedDate"] as! Date }
                        
                        self.aryAllKeys = Array(self.dictGroupedTimeline.keys)
                        
                        let aryAllKeysCopy = self.aryAllKeys
                        
                        self.aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
                        
                    }
                    
                    self.tblviewOpportunityDetails.reloadData()
                }
                else
                {
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                    }
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }
        }
    }
    
    
    func updateConfidenceLevel()
    {
        chkConfidenceUpdate = false
        
        var strLeadId = String()
        var strConfidence = lblProgressValue.text ?? "00"
        strConfidence = strConfidence.replacingOccurrences(of: "%", with: "")
        
        strLeadId = strRefIdNew//"\((dictLeadData.value(forKey: "OpportunityId") ?? ""))"
        
        let strURL = strServiceUrlMain + "/api/LeadNowAppToSalesProcess/UpdateOpportunityConfidenceLevel?LeadId=\(strLeadId)&ConfidenceLevel=\(strConfidence)&EmployeeId=\(strEmpID)"
        
        //FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "GrabLead") { (Response, Status) in
            
            DispatchQueue.main.async{
                
                FTIndicator.dismissProgress()
                
            }
            if(Status)
            {
                let strResponse = Response["data"] as! String
                
                if (strResponse.count > 0) && strResponse == "true"
                {
                    
                    self.chkConfidenceUpdate = true
                    print("Confidence updated successfully")
                    nsud.set(true, forKey: "fromScheduledOpportunity")
                    nsud.synchronize()

                    
                }
                else
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    
                }
            }
            else
            {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                DispatchQueue.main.async
                    {
                        
                        FTIndicator.dismissProgress()
                }
                
            }
            
        }
    }
    func updateOpportunityValue()
    {
        
        var strLeadId = String()
        var strOpportunityValue = strOppAmount//txtFldOpportunityValue.text ?? "00"
        strOpportunityValue = strOpportunityValue.replacingOccurrences(of: "%", with: "")
        
        strLeadId = strRefIdNew//"\((dictLeadData.value(forKey: "OpportunityId") ?? ""))"
        

        let strURL = strServiceUrlMain + "/api/LeadNowAppToSalesProcess/UpdateOpportunitValuebyId?LeadId=\(strLeadId)&LeadValue=\(strOpportunityValue)"
        
        //FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        
        loader = loader_Show(controller: self, strMessage: "Updating value...", title: "", style: .alert)
                 self.present(loader, animated: false, completion: nil)

        
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "GrabLead") { (Response, Status) in
            
            self.loader.dismiss(animated: false)
            {
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    
                }
                if(Status)
                {
                    let strResponse = Response["data"] as! String
                    
                    if (strResponse.count > 0) && strResponse == "true"
                    {
                        self.txtFldOpportunityValue.text = self.strOppAmount
                        
                        self.getBasicInfoOpportunity()
                        
                        print("Opportunity Value updated successfully")
                        nsud.set(true, forKey: "fromScheduledOpportunity")
                        nsud.synchronize()

                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                }
                else
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async
                        {
                            
                            FTIndicator.dismissProgress()
                    }
                    
                }
            }

            
        }
    }
    func callApiToGetContacts(){
        
        //lbl.removeFromSuperview()

        //FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        
        if nsud.value(forKey: "CrmContactFilter") == nil {
            
            setDefaultValuesForContactFilter()
            
        }
        
        var dictToSend = nsud.value(forKey: "CrmContactFilter") as! NSDictionary
        var dictToSendNew = NSMutableDictionary()
        dictToSendNew = dictToSend.mutableCopy() as! NSMutableDictionary
        dictToSendNew.setValue(enumRefTypeOpportunity, forKey: "RefType")
        dictToSendNew.setValue(strRefIdNew, forKey: "RefId")
        
        dictToSend = dictToSendNew.mutableCopy() as! NSDictionary
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlSearchCrmContacts

        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "SearchCrmContactsNew"
        self.dispatchGroup.enter()
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
             self.dispatchGroup.leave()
            DispatchQueue.main.async {
                
                FTIndicator.dismissProgress()
                //self.refresher.endRefreshing()

            }
            
            if(success)
            {
                //Contacts
                
                let dictResponse = (response as NSDictionary?)!
                
                if (dictResponse.count > 0) && (dictResponse.isKind(of: NSDictionary.self)) {
                    
                    let arrOfKey = dictResponse.allKeys as NSArray
                    
                    if arrOfKey.contains("Contacts") {
                        
                        self.arrOfContacts = dictResponse.value(forKey: "Contacts") as! NSMutableArray
                        
                        if self.arrOfContacts.count > 0 {
                            
                            //self.tblviewContact.isHidden = false
                            self.tblviewOpportunityDetails.reloadData()
                            self.tblContactNew.reloadData()
                            //self.logicForFilterContacts(arrToFilter: self.arrOfContacts)
                            
                        } else {
                            
                           // self.noDataLbl()
                            
                        }
                        
                        
                    } else {
                        
                        //self.noDataLbl()
                       // showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }

                     self.heightContactTable()
                    
                } else {
                    
                    //self.noDataLbl()
                    //showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                    
                }
            
                
            }else {
                
               // self.noDataLbl()
               // showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                
            }

        }
    }
    func heightContactTable()
    {
        const_TblContactNew_H.constant = (DeviceType.IS_IPAD ? 44 : 30)
        const_TblContactNew_H.constant = CGFloat(arrOfContacts.count * (DeviceType.IS_IPAD ? 44 : 30))
        
        if const_TblContactNew_H.constant > (DeviceType.IS_IPAD ? 150 : 120)
        {
             const_TblContactNew_H.constant = (DeviceType.IS_IPAD ? 150 : 120)
            tblContactNew.isScrollEnabled = true
            
        }
        else
        {
            tblContactNew.isScrollEnabled = false
        }
        
        if arrOfContacts.count == 0
        {
            const_TblContactNew_H.constant = (DeviceType.IS_IPAD ? 44 : 30)
            tblContactNew.isHidden = true
        }
        else
        {
            tblContactNew.isHidden = false
        }
        tblContactNew.separatorColor = UIColor.clear
        tblContactNew.showsVerticalScrollIndicator = false
    }
    func diAssociateContact(strCrmContactId : String,  strCrmCompanyId: String)
    {
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlDissociateContact + "\(strCrmContactId)" + "&RefType=" + enumRefTypeWebLead + "&RefId=" + strRefIdNew
        
        //FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        
        print(" diAssociateContact start")

        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "GrabLead") { (Response, Status) in
            
            print(" diAssociateContact end")

            self.loader.dismiss(animated: false)
            {
                
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    
                }
                if(Status)
                {
                    let strResponse = Response["data"] as! String
                    
                    if (strResponse.count > 0) && strResponse == "true"
                    {
                        
                        // Successfully deassociated
                        // call APi for Fetching assocaitions again
                        
                        FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                            // self.callAPIToGetContactAssociation()
                            self.callApiToGetContacts()
                            
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                }
                else
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async
                        {
                            
                            FTIndicator.dismissProgress()
                    }
                    
                }

            }
            
            
        }
        
    }
    func diAssociateCompany(strCrmContactId : String,  strCrmCompanyId: String)
    {

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlDissociateCompany + "\(strCrmCompanyId)" + "&RefType=" + enumRefTypeWebLead + "&RefId=" + strRefIdNew
        
       // FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
        print(" diAssociateCompany start")

        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "GrabLead") { (Response, Status) in
            
            print(" diAssociateCompany end")

            self.loader.dismiss(animated: false)
            {
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    
                }
                if(Status)
                {
                    let strResponse = Response["data"] as! String
                    
                    if (strResponse.count > 0) && strResponse == "true"
                    {
                        
                        // Successfully deassociated
                        // call APi for Fetching assocaitions again
                        
                        FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                           // self.callAPIToGetContactAssociation()
                            self.getBasicInfoOpportunity()
                            
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                        
                    }
                }
                else
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    DispatchQueue.main.async
                        {
                            
                            FTIndicator.dismissProgress()
                    }
                    
                }
            }
            
        }
        
    }
    
    fileprivate func callAPIToGetTaskDetailById(strId : String){
        
        
        print(" callAPIToGetTaskDetailById start")
        //FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        

        var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetTaskById + strId
        
        strURL = strURL.trimmingCharacters(in: .whitespaces)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "CRMConactNew_ContactDetails_About") { (response, status) in
            
            print(" callAPIToGetTaskDetailById end ")

            
            self.loader.dismiss(animated: false)
            {
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    
                }
                
                if(status)
                {
                    
                    
                    if((response.value(forKey: "data") as! NSDictionary).count > 0)
                    {
                        var dictTaskDetailData = (response.value(forKey: "data") as! NSDictionary)
                       
                        dictTaskDetailData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictTaskDetailData as! [AnyHashable : Any]))! as NSDictionary
                        
                        // goto create follow up
                        
                        self.gotoCreateFollowUp(dictData: dictTaskDetailData)
                        
                    }
                    else{
                        
                        DispatchQueue.main.async {
                            
                            FTIndicator.dismissProgress()
                        }
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)

                        
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)

                    DispatchQueue.main.async{
                        
                        FTIndicator.dismissProgress()
                    }
                    
                }
            }
            

        }
    }
    
    // MARK: -  ------------------------------ Core Data Functions  ------------------------------
    
    func saveNotesToCoreData(arrAllData: NSMutableArray)
    {
        deleteAllRecordsFromDB(strEntity: "TotalNotesCRM", predicate: NSPredicate(format: "userName=%@ && opportunityNumber=%@", strUserName,"\(dictLeadData.value(forKey: "opportunityNumber") ?? "")"))
        
        for item in arrAllData
        {
            let dict = item as! NSDictionary
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("createdByName")
            arrOfKeys.add("leadNoteId")
            arrOfKeys.add("leadNumber")
            arrOfKeys.add("note")
            arrOfKeys.add("opportunityNumber")
            arrOfKeys.add("userName")
            
            arrOfValues.add("\(dict.value(forKey: "ClientCreatedDate") ?? "")")//clientCreatedDate
            arrOfValues.add("\(strCompanyKey)")//companyKey
            arrOfValues.add("\(strUserName)")//createdByName
            
            arrOfValues.add("\(dict.value(forKey: "LeadNoteId") ?? "")")//leadNoteId
            arrOfValues.add("\(dict.value(forKey: "LeadNumber") ?? "")")//leadNumber
            arrOfValues.add("\(dict.value(forKey: "Note") ?? "")")//note
            arrOfValues.add("\(dict.value(forKey: "OpportunityNumber") ?? "")")//opportunityNumber
            arrOfValues.add("\(strUserName)")//userName
            
            saveDataInDB(strEntity: "TotalNotesCRM", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        
    }
    func fetchAllNotesFromCoreData()
    {
        //ServiceAddressPOCDetailDcs
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalNotesCRM", predicate: NSPredicate(format: "userName == %@ && opportunityNumber == %@", strUserName,"\(dictLeadData.value(forKey: "opportunityNumber") ?? "")"))
        
        arrAllNotes = arryOfData.mutableCopy() as! NSMutableArray
        
        tblviewOpportunityDetails.reloadData()
        
    }
    func saveTaskListToLocalDb(data:NSArray)
    {
        //deleteAllRecordsFromDB(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "refType =="))
        deleteAllRecordsFromDB(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "refId == %@ && userName == %@ && refType == %@",strRefId,strUserName,strReftype))
        
        //temp
        
        /*let context = getContext()
         let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\("TaskListCRMNew")")
         let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
         
         do {
         try context.execute(deleteRequest)
         try context.save()
         
         } catch {
         print ("There was an error")
         }
         //End*/
        
        for item in data
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            let dictData = removeNullFromDict(dict: (item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            arrOfKeys.add("userName")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("accountNo")
            arrOfKeys.add("addressLine1")
            arrOfKeys.add("addressLine2")
            arrOfKeys.add("assignedByStr")
            arrOfKeys.add("assignedTo")
            arrOfKeys.add("assignedToStr")
            arrOfKeys.add("cellPhone1")
            arrOfKeys.add("cellPhone2")
            arrOfKeys.add("childActivities")
            arrOfKeys.add("cityName")
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("clientModifiedDate")
            arrOfKeys.add("companyName")
            arrOfKeys.add("countryId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("dueDate")
            arrOfKeys.add("dueTime")
            arrOfKeys.add("firstName")
            arrOfKeys.add("followUpFromTaskId")
            arrOfKeys.add("lastName")
            arrOfKeys.add("leadNo")
            arrOfKeys.add("leadTaskId")
            arrOfKeys.add("middleName")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("primaryEmail")
            arrOfKeys.add("primaryPhone")
            arrOfKeys.add("primaryPhoneExt")
            arrOfKeys.add("priority")
            arrOfKeys.add("priorityStr")
            arrOfKeys.add("refId")
            arrOfKeys.add("refType")
            arrOfKeys.add("reminderDate")
            arrOfKeys.add("secondaryEmail")
            arrOfKeys.add("secondaryPhone")
            arrOfKeys.add("secondaryPhoneExt")
            arrOfKeys.add("stateId")
            arrOfKeys.add("status")
            arrOfKeys.add("tags")
            arrOfKeys.add("taskDescription")
            arrOfKeys.add("taskName")
            arrOfKeys.add("webLeadNo")
            arrOfKeys.add("zipcode")
            
            arrOfKeys.add("compareDate")
            
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add("\(dictData.value(forKey: "AccountNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "AddressLine1")!)")
            arrOfValues.add("\(dictData.value(forKey: "AddressLine2")!)")
            arrOfValues.add("\(dictData.value(forKey: "AssignedByStr")!)")
            arrOfValues.add("\(dictData.value(forKey: "AssignedTo")!)")
            arrOfValues.add("\(dictData.value(forKey: "AssignedToStr")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone1")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone2")!)")
            
            if (dictData.value(forKey: "ChildActivities")) is NSArray
            {
                if((dictData.value(forKey: "ChildActivities") as! NSArray).count > 0)
                {
                    let aryChild = NSMutableArray()
                    let aryTemp = (dictData.value(forKey: "ChildActivities") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    for itemTemp in aryTemp
                    {
                        aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
                    }
                    arrOfValues.add(aryChild)
                }
                else
                {
                    arrOfValues.add(NSMutableArray())
                }
            }
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            arrOfValues.add("\(dictData.value(forKey: "CityName")!)")
            
            if("\(dictData.value(forKey: "ClientCreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientCreatedDate")!)" != "")
            {
                //                arrOfValues.add(Global().convertDate("\(dictData.value(forKey: "ClientCreatedDate")!)"))
                
                arrOfValues.add("\(dictData.value(forKey: "ClientCreatedDate")!)")
                
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "ClientModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientModifiedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ClientModifiedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "CompanyName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CountryId")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CreatedBy")!)")
            
            if("\(dictData.value(forKey: "CreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "CreatedDate")!)" != "")
            {
                
                arrOfValues.add("\(dictData.value(forKey: "CreatedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "DueDate")!)" != "<null>" && "\(dictData.value(forKey: "DueDate")!)" != "")
            {
                arrOfValues.add(Global().convertDate("\(dictData.value(forKey: "DueDate")!)"))
                // arrOfValues.add("\(dictData.value(forKey: "DueDate")!)")
                arrOfValues.add(Global().convertTime("\(dictData.value(forKey: "DueDate")!)"))
            }
            else
            {
                arrOfValues.add("")
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "FirstName")!)")
            arrOfValues.add("\(dictData.value(forKey: "FollowUpFromTaskId")!)")
            arrOfValues.add("\(dictData.value(forKey: "LastName")!)")
            arrOfValues.add("\(dictData.value(forKey: "LeadNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "LeadTaskId")!)")
            arrOfValues.add("\(dictData.value(forKey: "MiddleName")!)")
            arrOfValues.add("\(dictData.value(forKey: "ModifiedBy")!)")
            
            if("\(dictData.value(forKey: "ModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ModifiedDate")!)" != "")
            {
                // arrOfValues.add(Global().convertDate("\(dictData.value(forKey: "ModifiedDate")!)"))
                
                arrOfValues.add("\(dictData.value(forKey: "ModifiedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "PrimaryEmail")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhone")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhoneExt")!)")
            arrOfValues.add("\(dictData.value(forKey: "Priority")!)")
            arrOfValues.add("\(dictData.value(forKey: "PriorityStr")!)")
            arrOfValues.add("\(dictData.value(forKey: "RefId")!)")
            arrOfValues.add("\(dictData.value(forKey: "RefType")!)")
            
            if("\(dictData.value(forKey: "ReminderDate")!)" != "<null>" && "\(dictData.value(forKey: "ReminderDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ReminderDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "SecondaryEmail")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhone")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhoneExt")!)")
            arrOfValues.add("\(dictData.value(forKey: "StateId")!)")
            arrOfValues.add("\(dictData.value(forKey: "Status")!)")
            arrOfValues.add("\(dictData.value(forKey: "Tags")!)")
            arrOfValues.add("\(dictData.value(forKey: "TaskDescription")!)")
            arrOfValues.add("\(dictData.value(forKey: "TaskName")!)")
            arrOfValues.add("\(dictData.value(forKey: "WebLeadNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "Zipcode")!)")
            
            let dateString = Global().convertDate("\(dictData.value(forKey: "DueDate")!)")
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "MM/dd/yyyy"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            let compareDate = Global().getOnlyDate(dateFormatter.date(from: dateString!))
            
            arrOfValues.add(compareDate!)
            
            saveDataInDB(strEntity: "TaskListCRMNew", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        fetchTaskListFromLocalDB()
    }
    func fetchTaskListFromLocalDB()
    {
        
        //let arrayTemp = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ ",strUserName))
        let arrayTemp = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND  refId == %@ AND refType == %@",strUserName, "\(strRefId)", "\(strReftype)"))
        
        if(arrayTemp.count > 0)
        {
            aryTasks.removeAllObjects()
            
            for item in arrayTemp
            {
                aryTasks.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
                
            }
            print(arrayTemp)
            //tblviewTask.reloadData()
            tblviewOpportunityDetails.reloadData()
            
        }
        else
        {
            // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
        }
    }
    func updateTaskToLocalDB(dict:NSMutableDictionary)
    {
        
        let arrayAllObject = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "leadTaskId == %@", "\(dict.value(forKey: "leadTaskId")!)"))
        
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("status")
        
        
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strEmpID)
        arrOfValues.add(Global().modifyDate())
        
        if("\(dict.value(forKey: "status")!)" == "Open")
        {
            arrOfValues.add("Done")
        }
        else
        {
            arrOfValues.add("Open")
        }
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "leadTaskId == %@", "\(dict.value(forKey: "leadTaskId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            fetchTaskListFromLocalDB()
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
        }
        
    }
    
    func saveDocumentsToLocalDb(arrAllData: NSMutableArray)
    {
        deleteAllRecordsFromDB(strEntity: "TotalDocumentsCRM", predicate: NSPredicate(format: "userName=%@ && opportunityId=%@", strUserName,"\(dictLeadData.value(forKey: "opportunityId") ?? "")"))
        
        for item in arrAllData
        {
            let dict = item as! NSDictionary
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("docTypeSysName")
            arrOfKeys.add("opportunityId")
            arrOfKeys.add("mediaSourceSysName")
            arrOfKeys.add("mediaTypeSysName")
            arrOfKeys.add("path")
            arrOfKeys.add("title")
            arrOfKeys.add("userName")
            
            arrOfValues.add("\(strCompanyKey)") //companyKey
            arrOfValues.add("\(strUserName)") //createdBy
            
            arrOfValues.add("\(Global().getCurrentDate() ?? "")") //createdDate
            
            arrOfValues.add("\(dict.value(forKey: "DocTypeSysName") ?? "")")//docTypeSysName
            arrOfValues.add("\(dictLeadData.value(forKey: "opportunityId") ?? "")")//LeadId
            arrOfValues.add("\(dict.value(forKey: "MediaSourceSysName") ?? "")")//MediaSourceSysName
            arrOfValues.add("\(dict.value(forKey: "MediaTypeSysName") ?? "")")//MediaTypeSysName
            arrOfValues.add("\(dict.value(forKey: "Path") ?? "")")//Path
            arrOfValues.add("\(dict.value(forKey: "Title") ?? "")")//Title
            arrOfValues.add("\(strUserName)")//UserName
            
            saveDataInDB(strEntity: "TotalDocumentsCRM", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        
    }
    func fetchAllDocumentsFromCoreData()
    {
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalDocumentsCRM", predicate: NSPredicate(format: "userName == %@ && opportunityId == %@", strUserName,"\(dictLeadData.value(forKey: "opportunityId") ?? "")"))
        
        arrAllDocuments = NSMutableArray()
        
        if(arryOfData.count > 0)
        {
            arrAllDocuments.removeAllObjects()
            
            for item in arryOfData
            {
                arrAllDocuments.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
                
            }
            print(arryOfData)
            //tblviewOpportunityDetails.reloadData()
            
        }
        
        
        //arrAllDocuments = arryOfData.mutableCopy() as! NSMutableArray
        
        tblviewOpportunityDetails.reloadData()
        
    }
    fileprivate func saveActivityListToLocalDb(data:NSArray)
    {
        //  deleteAllRecordsFromDB(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "refType =="))
        //     deleteAllRecordsFromDB(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "refType == %@",strTypeOfActivityToFetch))
        
        deleteAllRecordsFromDB(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "refId == %@ && userName == %@ && refType == %@",strRefId,strUserName,strReftype))
        
        //temp
        
        /*let context = getContext()
         let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\("ActivityListCRMNew")")
         let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
         
         do {
         try context.execute(deleteRequest)
         try context.save()
         
         } catch {
         print ("There was an error")
         }*/
        //End
        
        for item in data
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            let dictData = removeNullFromDict(dict: (item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            
            arrOfKeys.add("userName")
            arrOfKeys.add("companyKey")
            arrOfKeys.add("accountNo")
            arrOfKeys.add("activityId")
            arrOfKeys.add("agenda")
            arrOfKeys.add("logTypeId")
            arrOfKeys.add("clientCreatedDate")
            arrOfKeys.add("toDate")
            arrOfKeys.add("fromDate")
            arrOfKeys.add("participants")
            arrOfKeys.add("activityTime")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("leadNo")
            arrOfKeys.add("webLeadNo")
            arrOfKeys.add("firstName")
            arrOfKeys.add("middleName")
            arrOfKeys.add("lastName")
            arrOfKeys.add("cellPhone1")
            arrOfKeys.add("cellPhone2")
            arrOfKeys.add("primaryPhone")
            arrOfKeys.add("primaryPhoneExt")
            arrOfKeys.add("secondaryPhone")
            arrOfKeys.add("secondaryPhoneExt")
            arrOfKeys.add("primaryEmail")
            arrOfKeys.add("secondaryEmail")
            arrOfKeys.add("addressLine1")
            arrOfKeys.add("addressLine2")
            arrOfKeys.add("cityName")
            arrOfKeys.add("stateId")
            arrOfKeys.add("zipcode")
            arrOfKeys.add("countryId")
            arrOfKeys.add("companyName")
            arrOfKeys.add("refId")
            arrOfKeys.add("refType")
            arrOfKeys.add("activityComments")
            arrOfKeys.add("childTasks")
            arrOfKeys.add("compareDate")
            arrOfKeys.add("isSystem")
            
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add("\(dictData.value(forKey: "AccountNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "ActivityId")!)")
            arrOfValues.add("\(dictData.value(forKey: "Agenda")!)")
            arrOfValues.add("\(dictData.value(forKey: "LogTypeId")!)")
            
            if("\(dictData.value(forKey: "ClientCreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientCreatedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ClientCreatedDate")!)")
                
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "ToDate")!)" != "<null>" && "\(dictData.value(forKey: "ToDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ToDate")!)")
                
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "FromDate")!)" != "<null>" && "\(dictData.value(forKey: "FromDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "FromDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "Participants")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "ActivityTime")!)")
            
            if("\(dictData.value(forKey: "CreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "CreatedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "CreatedDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "ModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ModifiedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ModifiedDate")!)")
                
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add(strEmpID) // created by
            arrOfValues.add("\(dictData.value(forKey: "LeadNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "WebLeadNo")!)")
            arrOfValues.add("\(dictData.value(forKey: "FirstName")!)")
            arrOfValues.add("\(dictData.value(forKey: "MiddleName")!)")
            arrOfValues.add("\(dictData.value(forKey: "LastName")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone1")!)")
            arrOfValues.add("\(dictData.value(forKey: "CellPhone2")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhone")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhoneExt")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhone")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhoneExt")!)")
            arrOfValues.add("\(dictData.value(forKey: "PrimaryEmail")!)")
            arrOfValues.add("\(dictData.value(forKey: "SecondaryEmail")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "AddressLine1")!)")
            arrOfValues.add("AddressLine2") // address line2
            arrOfValues.add("\(dictData.value(forKey: "CityName")!)")
            arrOfValues.add("\(dictData.value(forKey: "StateId")!)")
            arrOfValues.add("\(dictData.value(forKey: "Zipcode")!)")
            arrOfValues.add("\(dictData.value(forKey: "CountryId")!)")
            arrOfValues.add("\(dictData.value(forKey: "CompanyName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "RefId")!)")// refId
            arrOfValues.add("\(dictData.value(forKey: "RefType")!)") // refType
            
            if var aryTemp = dictData.value(forKey: "ActivityComments") as? NSArray
            {
                
                let aryChild = NSMutableArray()
                aryTemp = (dictData.value(forKey: "ActivityComments") as! NSArray).mutableCopy() as! NSMutableArray
                
                for itemTemp in aryTemp
                {
                    aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
                }
                arrOfValues.add(aryChild)
                
            }
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            if var aryTemp = dictData.value(forKey: "ChildTasks") as? NSArray
            {
                let aryChild = NSMutableArray()
                aryTemp = (dictData.value(forKey: "ChildTasks") as! NSArray).mutableCopy() as! NSMutableArray
                
                for itemTemp in aryTemp
                {
                    aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
                }
                arrOfValues.add(aryChild)
                
            }
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            if("\(dictData.value(forKey: "ModifiedDate")!)" == "" || "\(dictData.value(forKey: "ModifiedDate")!)" == "<null>")
            {
                // do nothing
            }
                
            else
            {
                let dateString = Global().convertDate("\(dictData.value(forKey: "ModifiedDate")!)")
                
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                dateFormatter.dateFormat = "MM/dd/yyyy"
                
                let compareDate = Global().getOnlyDate(dateFormatter.date(from: dateString!))
                
                arrOfValues.add(compareDate!)
                
                if(dictData.value(forKey: "IsSystem") as! Bool == true)
                {
                    arrOfValues.add(true)
                }
                else
                {
                    arrOfValues.add(false)
                }
                
                saveDataInDB(strEntity: "ActivityListCRMNew", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            
        }
        
        fetchActivityListFromLocalDB()
    }
    fileprivate func fetchActivityListFromLocalDB()
    {
        
        aryActivities.removeAllObjects()
        aryActivitiesCopy.removeAllObjects()
        
        // let arrayTemp = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND refType == %@",strUserName,"Lead"))
        let arrayTemp = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND refId == %@ AND refType == %@",strUserName, "\(strRefId)", "\(strReftype)"))
        
        if(arrayTemp.count > 0)
        {
            for item in arrayTemp
            {
                aryActivities.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
                
            }
            print(arrayTemp)
            
            for item in aryTasks
            {
                let dict  = item as! NSDictionary
                
                if "\(dict.value(forKey: "leadTaskId") ?? "")".count == 0
                {
                    if((dict.value(forKey: "childActivities") as! NSArray).count > 0)
                    {
                        //aryActivities.removeAllObjects()
                        for innerItem in dict.value(forKey: "childActivities") as! NSArray
                        {
                            aryActivities.add(innerItem as! NSDictionary)
                            
                        }
                    }
                }
                
            }
            
            aryActivitiesCopy = aryActivities.mutableCopy() as! NSMutableArray
            
            tblviewOpportunityDetails.reloadData()
            
        }
        else
        {
            for item in aryTasks
            {
                let dict  = item as! NSDictionary
                
                if((dict.value(forKey: "childActivities") as! NSArray).count > 0)
                {
                    aryActivities.removeAllObjects()
                    for innerItem in dict.value(forKey: "childActivities") as! NSArray
                    {
                        aryActivities.add(innerItem as! NSDictionary)
                        
                    }
                }
            }
            tblviewOpportunityDetails.reloadData()
        }
    }
    
    // MARK: -  ------------------------------ Other Functions  ------------------------------
   
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
                          lbltaskCount.isHidden = true
                          lblScheduleCount.isHidden = true
               }
                  
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }

                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                            lblScheduleCount.text = strScheduleCount
                            lblScheduleCount.isHidden = false
                        }else{
                        let badgeSchedule = SPBadge()
                        badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                        badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeSchedule.badge = strScheduleCount
                        btnScheduleFooter.addSubview(badgeSchedule)
                        }
                    }
                    

                    if strTaskCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                                                  lbltaskCount.text = strTaskCount
                                                  lbltaskCount.isHidden = false
                                                  
                                              }else{
                        let badgeTasks = SPBadge()
                        badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                        badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeTasks.badge = strTaskCount
                        btnTasksFooter.addSubview(badgeTasks)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func gotoCreateFollowUp(dictData : NSDictionary) {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(2, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountId")!)", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadId")!)", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadId")!)", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactId")!)", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyId")!)", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountDisplayName")!)", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadName")!)", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadName")!)", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactName")!)", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyName")!)", forKeyPath: "Pre_CrmCompanyName")
        
        dictTaskDetailsData.addEntries(from: dictData as! [AnyHashable : Any])

        //dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadTaskId")!)", forKeyPath: "LeadTaskId")
        //dictTaskDetailsData.setValue("\(dictData.value(forKey: "status")!)", forKeyPath: "status")

      let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)

    }
    
    func catchNotification(notification:Notification) {
        
        if (isInternetAvailable()){
            
           DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.getBasicInfoOpportunity()
            }

        }
        
    }
    
    func catchNotification1(notification:Notification) {
        
        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
                                            {
                                                self.getAllNotes()
                                            }

        }
        
    }
    
    func catchNotification2(notification:Notification) {
        
        if (isInternetAvailable()){
            
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
                                                       {
                                                           self.callApiToGetContacts()
                                                       }

        }
        
    }
    
    func catchNotificationTaskAdded(notification:Notification) {
        
        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.getAllTask()
            }

        }
        
    }
    func catchNotificationActivityAdded(notification:Notification) {
        
        if (isInternetAvailable()){
            
            //

        }
        
    }
    func catchNotificationDocumentsAdded(notification:Notification) {
        
        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.getAllDocuments()
                                    
            }

        }
        
    }
    
    func catchNotification_TimeLine(notification:Notification) {
           
           if (isInternetAvailable()){
               
               DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
               {
                   self.callAPToGetTimeLine()
               }

           }
           
       }
    
    fileprivate func getLogTypeFromMaster()
    {
        
        let dict = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        
        if(dict.count > 0)
        {
            let aryTemp = (dict.value(forKey: "ActivityLogTypeMasters") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in aryTemp
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    aryLogType.add((item as! NSDictionary))
                }
            }
            
            print(aryLogType)
        }
    }
    func setupUI()
    {
        
        btnGoToAppointments.isHidden = true
        
        tblviewOpportunityDetails.estimatedRowHeight = 50
        tblviewOpportunityDetails.rowHeight = UITableView.automaticDimension;

        tblContactNew.estimatedRowHeight = 30
        tblContactNew.rowHeight = UITableView.automaticDimension;

        buttonRound(sender: btnScheduleNow)
        btnScheduleNow.layer.cornerRadius = btnScheduleNow.frame.size.height/2
        
        buttonRound(sender: btnGoToAppointments)
        btnGoToAppointments.layer.cornerRadius = btnGoToAppointments.frame.size.height/2
        
        
        txtFldOpportunityValue.layer.borderColor = UIColor.theme().cgColor
        
        /*let lblLineFotOppValue = UILabel()
        lblLineFotOppValue.text = ""
        lblLineFotOppValue.backgroundColor = UIColor.theme()
        lblLineFotOppValue.textAlignment = .center
        lblLineFotOppValue.font = UIFont.systemFont(ofSize: 14)
        lblLineFotOppValue.frame = CGRect(x:txtFldOpportunityValue.frame.origin.x,y:txtFldOpportunityValue.frame.origin.y,width:txtFldOpportunityValue.frame.size.width,height:1)
        self.view.addSubview(lblLineFotOppValue)*/
        
        //txtFldOpportunityValue.layer.borderWidth = 1.0
        
        //txtFldOpportunityValue.layer.cornerRadius = 5.0
        //txtFldOpportunityValue.setLeftPaddingPoints(5)
        
    }
    func setInitialValues()
    {
        serviceNameFromId()
        
        tblviewOpportunityDetails.estimatedRowHeight = 50
        tblviewOpportunityDetails.rowHeight = UITableView.automaticDimension;

        buttonRound(sender: btnScheduleNow)
        btnScheduleNow.layer.cornerRadius = btnScheduleNow.frame.size.height/2
        
        buttonRound(sender: btnGoToAppointments)
        btnGoToAppointments.layer.cornerRadius = btnGoToAppointments.frame.size.height/2
        
       // lblHeader.text = "Account - # \(dictLeadData.value(forKey: "AccountNo") ?? "") / Opportunity - \((dictLeadData.value(forKey: "OpportunityNumber") ?? ""))"
        
         lblHeader.text = "\(dictLeadData.value(forKey: "OpportunityName") ?? "")"
         lblHeader.textAlignment = .left
        
     /*   lblCreatedBy.text = getEmployeeNameFromId(strEmpId: "\(dictLeadData.value(forKey: "SubmittedById") ?? "")")
        
        if "\(dictLeadData.value(forKey: "SubmittedDate") ?? "")".count > 0
        {
            lblCreatedDate.text = changeStringDateToGivenFormat(strDate: "\(dictLeadData.value(forKey: "SubmittedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
        }
        else
        {
             lblCreatedDate.text = ""
        }

        lblFlowType.text = "\(dictLeadData.value(forKey: "OpportunityType") ?? "")"
        lblUrgency.text = "\(dictLeadData.value(forKey: "Urgency") ?? "")"
        lblTechNote.text = "\(dictLeadData.value(forKey: "TechNote") ?? "")"
        
        lblService.text = "\(dictServiceNameFromId.value(forKey: "\("\(dictLeadData.value(forKey: "ServiceId") ?? "")")") ?? "")"

        btnUserName.setTitle("\(dictLeadData.value(forKey: "FirstName") ?? "")" + " " + "\(dictLeadData.value(forKey: "MiddleName") ?? "")" + " " + "\(dictLeadData.value(forKey: "LastName") ?? "")", for: .normal)
        */
        
        //btnAddress.setTitle("\(dictLeadData.value(forKey: "serviceAddress") ?? "")", for: .normal)

        if (dictLeadData.value(forKey: "AccountCompany") ?? "") is NSDictionary
        {
            
            btnCompanyName.setTitle("\(dictLeadData.value(forKeyPath: "AccountCompany.Name") ?? "")", for: .normal)
            
        }
        
        let amount = ("\(dictLeadData.value(forKey: "OpportunityValue") ?? "")" as NSString).doubleValue
        
        //lblPrice.text = String(format: " $%.02f", amount)
        
        lblPrice.text = convertDoubleToCurrency(amount: Double(amount))
        txtFldOpportunityValue.text = String(format: "%.02f", Double(amount))
        
        
        
        let valueConfidence = "\(dictLeadData.value(forKeyPath: "ConfidenceLevel") ?? "")"
        
        //valueConfidence = "88"
        let valueConfidenceFloat = (valueConfidence as NSString).floatValue
        progressView.progress = valueConfidenceFloat / 100
        
        if valueConfidence == "" || valueConfidence == "0"
        {
            lblProgressValue.text = "00 %"
            
        }
        else
        {
            lblProgressValue.text = "\(valueConfidence)%"
        }
        lblOpportunityStatus.text = "\(dictLeadData.value(forKeyPath: "OpportunityStatus") ?? "")   -"
        lblOpportunityStage.text = "\(dictLeadData.value(forKeyPath: "OpportunityStage") ?? "")"

        
        progressView.setProgress(progressView.progress, animated: true)

        let currentValue = Float("\((dictLeadData.value(forKey: "ConfidenceLevel") ?? ""))") ?? 0 * 100
        let val = String(format: "%.00f",currentValue)
        
        lblProgressValue.text = "\(val)%"
        progressConfidenceLvel.value = Float("\(currentValue/100)") ?? 0
        
        var strAddress = ""
        if dictLeadData.value(forKey: "ServiceAddress") is NSDictionary
        {
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? "")".count > 0
            {
                strAddress = "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? ""), "
            }
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address2") ?? "")".count > 0
            {
                strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.Address2") ?? "")"), "
            }
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.CityName") ?? "")".count > 0
            {
                strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.CityName") ?? "")"), "
            }
            
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? "")".count > 0
            {
                let stateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? ""))")
                
                strAddress = "\(strAddress)\(stateName ?? ""), "
            }
            if "\(dictLeadData.value(forKeyPath: "ServiceAddress.Zipcode") ?? "")".count > 0
            {
                strAddress = "\(strAddress)\("\(dictLeadData.value(forKeyPath: "ServiceAddress.Zipcode") ?? "")")"
            }
            
        }
        
        btnAddress.setTitle(strAddress, for: .normal)
        
        if strAddress.count > 0 {
            
            btnAddress.isHidden = false
            
        } else {
            
            btnAddress.isHidden = true
            
        }
        
        
        if dictLeadData.value(forKey: "AccountCompany") is NSDictionary
        {
            btnEditCompany.isHidden = false
        }
        else
        {
            btnEditCompany.isHidden = true
        }
        
        
    
        let strOpportunityStage =  "\(dictLeadData.value(forKeyPath: "OpportunityStage") ?? "")"
        
        if strOpportunityStage == "Scheduled"
        {
            btnScheduleNow.setTitle("Re-Schedule", for: .normal)
        }
        
       /* if nsud.bool(forKey: "fromNearByOpportunity") == true
        {
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalOpportunityLeads", predicate: NSPredicate(format: "opportunityId == %@", "\(self.dictLeadData.value(forKey: "opportunityId") ?? "")"))
            
            if arryOfData.count == 0
            {
                saveOpportunityLeads()
                
                let arryOfData = getDataFromCoreDataBaseArray(strEntity: "TotalOpportunityLeads", predicate: NSPredicate(format: "opportunityId == %@", "\(self.dictLeadData.value(forKey: "OpportunityId") ?? "")"))
                //dictLeadData = arryOfData.object(at: 0) as! NSManagedObject
                
                dictLeadData = getMutableDictionaryFromNSManagedObject(obj: arryOfData.object(at: 0) as! NSManagedObject) as NSDictionary

            }
            else
            {
                //dictLeadData = arryOfData.object(at: 0) as! NSManagedObject
                
                dictLeadData = getMutableDictionaryFromNSManagedObject(obj: arryOfData.object(at: 0) as! NSManagedObject) as NSDictionary

            }
            nsud.set(false, forKey: "fromNearByOpportunity")
            nsud.synchronize()
        }*/
        
        //print("\(dictOpportunityStatusNameFromId.value(forKey: "\(dictLeadData.value(forKey: "StatusId")!)")!)")
        //print("\(dictOpportunityStageNameFromId.value(forKey: "\(dictLeadData.value(forKey: "StageId")!)")!)")
        
        
        
        //print("\(dictOpportunityStageNameFromId.value(forKey: "StageId"))")
        
        
        strStatusNameGlobal = "\(dictOpportunityStatusNameFromId.value(forKey: "\(dictLeadData.value(forKey: "StatusId") ?? "")") ?? "")"
        strStageNameGlobal = "\(dictOpportunityStageNameFromId.value(forKey: "\(dictLeadData.value(forKey: "StageId") ?? "")") ?? "")"
        
        
        if strStageNameGlobal.caseInsensitiveCompare("Scheduled") == .orderedSame//== "Scheduled"
        {
            btnScheduleNow.setTitle("Re-Schedule", for: .normal)
        }
        
        
        if strStatusNameGlobal.caseInsensitiveCompare("Open") == .orderedSame && strStageNameGlobal.caseInsensitiveCompare("Scheduled") == .orderedSame
        {
            btnGoToAppointments.isHidden = false
        }
        else
        {
             btnGoToAppointments.isHidden = true
        }
        
        if strStatusNameGlobal.caseInsensitiveCompare("Complete") == .orderedSame && strStageNameGlobal.caseInsensitiveCompare("Won") == .orderedSame
        {
            btnScheduleNow.isEnabled = false
            //progressConfidenceLvel.isEnabled = false
            //btnEditCompany.isEnabled = false
            
        
        }
        else
        {
            btnScheduleNow.isEnabled = true
            //progressConfidenceLvel.isEnabled = true
            //btnEditCompany.isEnabled = true
        }
        
    }
    func funForDisable()
    {
        
    }
    func opportunityStageStatusFromId()
    {
        let dictTemp = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        let arrStageMaster = dictTemp.value(forKey: "LeadStageMasters") as! NSArray
        let arrStatusMaster = dictTemp.value(forKey: "LeadStatusMasters") as! NSArray
        print(arrStageMaster,arrStatusMaster)
        
        let arrStageName = NSMutableArray()
        let arrStageId = NSMutableArray()
        let arrStageSysName = NSMutableArray()
        let arrStatusName = NSMutableArray()
        let arrStatusId = NSMutableArray()
        let arrStatusSysName = NSMutableArray()
        
        if arrStageMaster.count > 0
        {
            for item in arrStageMaster
            {
                let dict = item as! NSDictionary
                arrStageName.add("\(dict.value(forKey: "Name")!)")
                arrStageId.add("\(dict.value(forKey: "LeadStageId")!)")
                arrStageSysName.add("\(dict.value(forKey: "SysName")!)")
            }
        }
        
        if arrStatusMaster.count > 0
        {
            for item in arrStatusMaster
            {
                let dict = item as! NSDictionary
                arrStatusName.add("\(dict.value(forKey: "StatusName")!)")
                arrStatusId.add("\(dict.value(forKey: "StatusId")!)")
                arrStatusSysName.add("\(dict.value(forKey: "SysName")!)")
            }
        }
        

        dictOpportunityStageNameFromId = NSDictionary(objects:arrStageName as! [Any], forKeys:arrStageId as! [NSCopying]) as Dictionary as NSDictionary
        dictOpportunityStageNameFromSysName = NSDictionary(objects:arrStageName as! [Any], forKeys:arrStageSysName as! [NSCopying]) as Dictionary as NSDictionary
        
        dictOpportunityStatusNameFromId = NSDictionary(objects:arrStatusName as! [Any], forKeys:arrStatusId as! [NSCopying]) as Dictionary as NSDictionary
        dictOpportunityStatusNameFromSysName = NSDictionary(objects:arrStatusName as! [Any], forKeys:arrStatusSysName as! [NSCopying]) as Dictionary as NSDictionary
        
        print(dictOpportunityStageNameFromId)
        print(dictOpportunityStageNameFromSysName)
        
    }
    
    func goToNotesVC()
    {
         let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
    
        //vc.dictLeadData = dictLeadData
        vc.strLeadType = "Opportunity"
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToAddTaskVC()
    {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue(strRefIdNew, forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(dictLeadData.value(forKey: "OpportunityName") ?? "")", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        
      let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func goToAddLead()
    {
        
    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func goToTaskDetail(strId : String)
    {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
        vc.taskId = "\(strId)"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func goToActivityDetail(dict: NSDictionary)
    {
           let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityDetailsVC_CRMNew_iPhone") as! ActivityDetailsVC_CRMNew_iPhone
            
        vc.dictActivity = dict.mutableCopy() as! NSMutableDictionary
        vc.strFromVC = "OpportunityVC"
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToAddActivityVC()
    {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue(strRefIdNew, forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(dictLeadData.value(forKey: "OpportunityName") ?? "")", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
        vc.dictActivityDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func goToEditOpportunityVC()
    {
        
            let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditOpportunityCRM_VC") as! EditOpportunityCRM_VC
     
        let dictMutable = dictLeadData.mutableCopy() as! NSMutableDictionary
        testController.dictOpportunity = dictMutable
        //self.present(testController, animated: false, completion: nil)
        self.navigationController?.pushViewController(testController, animated: false)

        
        /*let storyboardIpad = UIStoryboard.init(name: "CRM", bundle: nil)
        
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "EditOpportunityVC") as! EditOpportunityVC
        
        //let dictMutable = getMutableDictionaryFromNSManagedObject(obj: dictLeadData) as NSMutableDictionary
        let dictMutable = dictLeadData.mutableCopy() as! NSMutableDictionary
        
        if chkConfidenceUpdate == true
        {
            var strConfidence = lblProgressValue.text ?? "00"
            strConfidence = strConfidence.replacingOccurrences(of: "%", with: "")
            dictMutable.setValue("\(strConfidence)", forKey: "confidenceLevel")
        }
        vc.dictOpportunityDetail = dictMutable as NSDictionary
        
        vc.strFromWhere = "opportunityDetailVC"
        // nsud.set(true, forKey: "forEditOpportunityFromDetail")
        //nsud.synchronize()
        
        self.present(vc, animated: false, completion: nil)*/
        
    }
    
    func makeCall()
    {
        let arrNoToCall = NSMutableArray()
        
        if "\(dictLeadData.value(forKey: "Cell") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "Cell") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")")
        }
        
        
        if arrNoToCall.count > 0
            
        {
            
            let alert = UIAlertController(title: "", message: "Make your call on selection", preferredStyle: .actionSheet)
               alert.view.tintColor = UIColor.black
            for item in arrNoToCall
            {
                let strNo = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strNo)", style: .default , handler:{ (UIAlertAction)in
                    
                    
                    if (strNo.count > 0)
                    {
                        Global().calling(strNo)
                    }
                    
                }))
                
                
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No contacts detail found", viewcontrol: self)
            
        }
    }
    func sendMail()
    {
        let arrEmail = NSMutableArray()
        
        if "\(dictLeadData.value(forKey: "PrimaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(dictLeadData.value(forKey: "PrimaryEmail") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "SecondaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(dictLeadData.value(forKey: "SecondaryEmail") ?? "")")
        }
        
        if arrEmail.count > 0
            
        {
            let alert = UIAlertController(title: "", message: "Make your email on selection", preferredStyle: .actionSheet)
               alert.view.tintColor = UIColor.black
            for item in arrEmail
            {
                let strEmail = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strEmail)", style: .default , handler:{ (UIAlertAction)in
                    
                    if (strEmail.count > 0)
                    {
                        //Global().emailComposer(strEmail, "", "", self)
                        self.sendEmail(strEmail: strEmail)
                    }
                    
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No email found", viewcontrol: self)
            
        }
    }
    
    func sendMessage()
    {
        let arrNoToCall = NSMutableArray()
        
        if "\(dictLeadData.value(forKey: "Cell") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "Cell") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "PrimaryPhone") ?? "")")
        }
        if "\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictLeadData.value(forKey: "SecondaryPhone") ?? "")")
        }
        
        if arrNoToCall.count > 0
            
        {
            
            let alert = UIAlertController(title: "", message: "Make your message on selection", preferredStyle: .actionSheet)
               alert.view.tintColor = UIColor.black
            for item in arrNoToCall
            {
                let strNo = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strNo)", style: .default , handler:{ (UIAlertAction)in
                    
                    
                    if (strNo.count > 0)
                    {
                        self.displayMessageInterface(strNo: "\(strNo)")
                    }
                    
                }))
                
                
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No contacts detail found", viewcontrol: self)
            
        }
    }
    func createCompanyDetailData() -> NSDictionary
    {
        print("\(dictLeadData)")
        
        var strAddress1 = String()
        var strAddress2 = String()
        var strCustomerAddressId = String()
        var strCityName = String()
        var strStateId = String()
        var strZipcode = String()
        var strStateName = String()
        
        var strCompanyId = String()
        var strCompanyName = String()
        var strCountryId = String()
        var strCountryName = String()
        var strCounty = String()
        var strCRMCompanyId = String()
        var strIsActive = String()
        var strProfileImage = String()
        
        
        var strCompanyCell = String()
        var strCompanyPrimaryPhone = String()
        var strCompanySecondaryPhone = String()
        var strCompanyPrimaryEmail = String()
        var strCompanySecondaryEmail = String()
        
        
        
        
        if dictLeadData.value(forKey: "ServiceAddress") is NSDictionary
        {
            if dictLeadData.value(forKey: "ServiceAddress") is NSDictionary
            {
                strAddress1 = "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? "")"
                strAddress2 = "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address2") ?? "")"
                strCustomerAddressId = "\(dictLeadData.value(forKeyPath: "ServiceAddress.Address1") ?? "")"
                strCityName = "\(dictLeadData.value(forKeyPath: "ServiceAddress.CityName") ?? "")"
                
                strStateId = "\(dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? "")"
                
                strZipcode = "\(dictLeadData.value(forKeyPath: "ServiceAddress.Zipcode") ?? "")"
                
                strStateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "ServiceAddress.StateId") ?? ""))")
            }
            
        }
        if dictLeadData.value(forKey: "AccountCompany") is NSDictionary
        {
            strCompanyId = "\(dictLeadData.value(forKeyPath: "AccountCompany.CompanyId") ?? "")"
            strCompanyName = "\(dictLeadData.value(forKeyPath: "AccountCompany.Name") ?? "")"
            strCountryId = "\(dictLeadData.value(forKeyPath: "AccountCompany.CountryId") ?? "")"
            strCountryName = "\(dictLeadData.value(forKeyPath: "AccountCompany.CountryName") ?? "")"
            strCounty = ""//"\(dictLeadData.value(forKeyPath: "accountCompany.CountryName") ?? "")"
            strCRMCompanyId = "\(dictLeadData.value(forKeyPath: "AccountCompany.CrmCompanyId") ?? "")"
            
            strIsActive = "\(dictLeadData.value(forKeyPath: "AccountCompany.IsActive") ?? "")"
            strProfileImage = "\(dictLeadData.value(forKeyPath: "AccountCompany.ProfileImage") ?? "")"
            //New
            
            strAddress1 = "\(dictLeadData.value(forKeyPath: "AccountCompany.Address1") ?? "")"
            strAddress2 = "\(dictLeadData.value(forKeyPath: "AccountCompany.Address2") ?? "")"
            strCustomerAddressId = "\(dictLeadData.value(forKeyPath: "AccountCompany.Address1") ?? "")"
            strCityName = "\(dictLeadData.value(forKeyPath: "AccountCompany.CityName") ?? "")"
            
            strStateId = "\(dictLeadData.value(forKeyPath: "AccountCompany.StateId") ?? "")"
            
            strZipcode = "\(dictLeadData.value(forKeyPath: "AccountCompany.Zipcode") ?? "")"
            
            strStateName = Global().strStatName(fromID: "\((dictLeadData.value(forKeyPath: "AccountCompany.StateId") ?? ""))")
            
            //New
            strCompanyCell = "\(dictLeadData.value(forKeyPath: "AccountCompany.CellPhone1") ?? "")"
            strCompanyPrimaryPhone = "\(dictLeadData.value(forKeyPath: "AccountCompany.PrimaryPhone") ?? "")"
            strCompanySecondaryPhone = "\(dictLeadData.value(forKeyPath: "AccountCompany.SecondaryPhone") ?? "")"
            strCompanyPrimaryEmail = "\(dictLeadData.value(forKeyPath: "AccountCompany.PrimaryEmail") ?? "")"
            strCompanySecondaryEmail = "\(dictLeadData.value(forKeyPath: "AccountCompany.SecondaryEmail") ?? "")"
            
        }
        else
        {
            strCompanyId = ""
            strCompanyName = ""
            strCountryId = ""
            strCountryName = ""
            strCRMCompanyId = ""
            strIsActive = ""
            strProfileImage = ""
            
            //New
            strCompanyCell = ""
            strCompanyPrimaryPhone = ""
            strCompanySecondaryPhone = ""
            strCompanyPrimaryEmail = ""
            strCompanySecondaryEmail = ""
        }
        
        
        
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("AccountId")
        arrOfKeys.add("Address1")
        arrOfKeys.add("Address2")
        arrOfKeys.add("CellPhone1")
        arrOfKeys.add("CellPhone2")
        arrOfKeys.add("CityName")
        arrOfKeys.add("CompanyId")
        arrOfKeys.add("CompanyKey")
        arrOfKeys.add("CompanyName")
        arrOfKeys.add("CountryId")
        arrOfKeys.add("CountryName")
        arrOfKeys.add("County")
        arrOfKeys.add("CreatedBy")
        arrOfKeys.add("CreatedByName")
        arrOfKeys.add("CreatedDate")
        arrOfKeys.add("CrmCompanyId")
        arrOfKeys.add("CrmContactId")
        arrOfKeys.add("FacebookLink")
        arrOfKeys.add("GoogleLink")
        arrOfKeys.add("IsActive")
        arrOfKeys.add("LeadCount")
        arrOfKeys.add("LinkedInLink")
        arrOfKeys.add("ModifiedBy")
        arrOfKeys.add("ModifiedDate")
        arrOfKeys.add("OpportunityCount")
        arrOfKeys.add("PrimaryEmail")
        arrOfKeys.add("PrimaryPhone")
        arrOfKeys.add("PrimaryPhoneExt")
        arrOfKeys.add("ProfileImage")
        arrOfKeys.add("SchoolDistrict")
        arrOfKeys.add("SecondaryEmail")
        arrOfKeys.add("SecondaryPhone")
        arrOfKeys.add("SecondaryPhoneExt")
        arrOfKeys.add("SourceId")
        arrOfKeys.add("StateId")
        arrOfKeys.add("StateName")
        arrOfKeys.add("TwitterLink")
        arrOfKeys.add("WebLeadId")
        arrOfKeys.add("Website")
        arrOfKeys.add("ZipCode")
        
        
        arrOfValues.add("\(dictLeadData.value(forKey: "AccountId") ?? "")")
        
        arrOfValues.add("\(strAddress1)")
        arrOfValues.add("\(strAddress2)")
        arrOfValues.add("\(strCompanyCell)")//arrOfValues.add("\(dictLeadData.value(forKey: "cell") ?? "")")
        arrOfValues.add("")
        arrOfValues.add("\(strCityName)")
        arrOfValues.add("\(strCompanyId)")
        arrOfValues.add("\(strCompanyKey)")
        arrOfValues.add("\(strCompanyName)")
        arrOfValues.add("\(strCountryId)")
        arrOfValues.add("\(strCountryName)")
        arrOfValues.add("\(strCounty)")
        arrOfValues.add("\(strUserName)")
        arrOfValues.add("\(strUserName)")
        arrOfValues.add("\(Global().getCurrentDate() ?? "")")
        arrOfValues.add("\(strCRMCompanyId)")
        arrOfValues.add("\(dictLeadData.value(forKey: "CrmContactId") ?? "")")
        arrOfValues.add("FacebookLink") //FacebookLink
        arrOfValues.add("GoogleLink") //GoogleLink
        arrOfValues.add("\(strIsActive)") //IsActive
        arrOfValues.add("") //LeadCount
        arrOfValues.add("") //LinkedInLink
        arrOfValues.add("\(strUserName)") //ModifiedBy
        arrOfValues.add("\(Global().getCurrentDate() ?? "")") //ModifiedDate
        arrOfValues.add("") //OpportunityCount
        // arrOfValues.add("\(dictLeadData.value(forKey: "primaryEmail") ?? "")")//PrimaryEmail
        arrOfValues.add("\(strCompanyPrimaryEmail)")//PrimaryEmail
        
        // arrOfValues.add("\(dictLeadData.value(forKey: "primaryPhone") ?? "")") //PrimaryPhone
        arrOfValues.add("\(strCompanyPrimaryPhone)") //PrimaryPhone
        
        
        
        arrOfValues.add("") //PrimaryPhoneExt
        arrOfValues.add("\(strProfileImage)") //ProfileImage
        arrOfValues.add("") //SchoolDistrict
        // arrOfValues.add("\(dictLeadData.value(forKey: "secondaryEmail") ?? "")") //SecondaryEmail
        arrOfValues.add("\(strCompanySecondaryEmail)") //SecondaryEmail
        
        //arrOfValues.add("\(dictLeadData.value(forKey: "secondaryPhone") ?? "")") //SecondaryPhone
        arrOfValues.add("\(strCompanySecondaryPhone)") //SecondaryPhone
        
        
        
        arrOfValues.add("") //SecondaryPhoneExt
        arrOfValues.add("") //SourceId
        arrOfValues.add("\(strStateId)") //StateId
        
        arrOfValues.add("\(strStateName)") //StateName
        arrOfValues.add("") //TwitterLink
        arrOfValues.add("") //WebLeadId
        arrOfValues.add("") //Website
        arrOfValues.add("\(strZipcode)") //ZipCode
        
        let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
        
        return dict_ToSend as NSDictionary
        
    }
    func syncTask()
    {
        if(isInternetAvailable() == true)
        {
            let arrayAllObject = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@", self.strUserName, self.strCompanyKey ))
            
            if(arrayAllObject.count > 0)
            {
                syncAllTaskWebLeadDetail(strUserName: self.strUserName, strCompanyKey: self.strCompanyKey)
            }
            else
            {
                if(isInternetAvailable() == true)
                {
                    getAllTask()
                }
                else
                {
                    fetchTaskListFromLocalDB()
                    fetchActivityListFromLocalDB()
                }
            }
        }
        else
        {
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()
        }
    }
    func syncAllTaskWebLeadDetail(strUserName:String, strCompanyKey:String)
    {
        let aryNonSyncTask = NSMutableArray()
        let aryJsonToPost = NSMutableArray()
        
        let arrayAllObject = getDataFromLocal(strEntity: "TaskListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@"  ,strUserName, strCompanyKey))
        
        for item in arrayAllObject
        {
            
            if("\((item as! NSManagedObject).value(forKey: "leadTaskId")!)".count == 0)
            {
                aryNonSyncTask.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
            }
            
        }
        
        if(aryNonSyncTask.count > 0)
        {
            for item in aryNonSyncTask
            {
                let dictData = item as! NSDictionary
                
                let aryOfKeys = NSMutableArray()
                let aryOfValues = NSMutableArray()
                
                aryOfKeys.add("AssignedTo")
                aryOfKeys.add("ChildActivities")
                aryOfKeys.add("CreatedBy")
                aryOfKeys.add("Description")
                aryOfKeys.add("DueDate")
                aryOfKeys.add("Id")
                aryOfKeys.add("LeadTaskId")
                aryOfKeys.add("Priority")
                aryOfKeys.add("RefId")
                aryOfKeys.add("RefType")
                aryOfKeys.add("ReminderDate")
                aryOfKeys.add("Status")
                aryOfKeys.add("Tags")
                aryOfKeys.add("TaskName")
                
                aryOfValues.add("\(dictData.value(forKey: "assignedTo")!)")
                
                aryOfValues.add(dictData.value(forKey: "childActivities") as! NSArray)
                aryOfValues.add("\(dictData.value(forKey: "createdBy")!)")
                aryOfValues.add("\(dictData.value(forKey: "taskDescription")!)")
                aryOfValues.add("\(dictData.value(forKey: "dueDate")!)")
                aryOfValues.add("1")
                aryOfValues.add("\(dictData.value(forKey: "leadTaskId")!)")
                aryOfValues.add("\(dictData.value(forKey: "priority")!)")
                aryOfValues.add("\(dictData.value(forKey: "refId")!)")
                aryOfValues.add("\(dictData.value(forKey: "refType")!)")
                aryOfValues.add("\(dictData.value(forKey: "reminderDate")!)")
                aryOfValues.add("\(dictData.value(forKey: "status")!)")
                aryOfValues.add("\(dictData.value(forKey: "tags")!)")
                aryOfValues.add("\(dictData.value(forKey: "taskName")!)")
                
                let dictJson = NSDictionary.init(objects: aryOfValues as! [Any], forKeys: aryOfKeys as! [NSCopying])
                
                aryJsonToPost.add(dictJson)
            }
            
            
            var jsonString = String()
            
            if(JSONSerialization.isValidJSONObject(aryJsonToPost) == true)
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: aryJsonToPost, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                
                print(jsonString)
                
            }
            
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strHrmsCompanyId = "\(dictLoginData.value(forKeyPath: "Company.HrmsCompanyId")!)"
            
            let strEmployeeNumber = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
            let strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
            let strCreatedBy = "\(dictLoginData.value(forKey: "CreatedBy")!)"
            let strEmployeeName = "\(dictLoginData.value(forKey: "EmployeeName")!)"
            let strCoreCompanyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId")!)"
            let strSalesProcessCompanyId1 = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
            let strCompanyKeyyy = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
            let strUrlSaved = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
            
            
            let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            
            let strUrl = strUrlSaved + UrlAddUpdateTaskGlobalMultiple
            let url = URL(string: strUrl)
            
            let request11 = NSMutableURLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
            
            request11.httpMethod = "POST"
            request11.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request11.addValue("application/json", forHTTPHeaderField: "Accept")
            
            request11.addValue("application/json", forHTTPHeaderField: "Accept")
            request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request11.addValue("IOS", forHTTPHeaderField: "Browser")
            
            request11.addValue(strHrmsCompanyId, forHTTPHeaderField: "HrmsCompanyId")
            
            request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "IpAddress")
            
            request11.addValue(strEmployeeNumber, forHTTPHeaderField: "EmployeeNumber")
            
            request11.addValue(strEmployeeId, forHTTPHeaderField: "EmployeeId")
            
            request11.addValue(strCreatedBy, forHTTPHeaderField: "CreatedBy")
            
            request11.addValue(strEmployeeName, forHTTPHeaderField: "EmployeeName")
            
            request11.addValue(strCoreCompanyId, forHTTPHeaderField: "CoreCompanyId")
            
            request11.addValue(strSalesProcessCompanyId1, forHTTPHeaderField: "SalesProcessCompanyId")
            
            request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
            
            request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
            
            request11.setValue("\(requestData!.count)", forHTTPHeaderField: "Content-Length")
            
            request11.httpBody = requestData
            
            URLSession.shared.dataTask(with: request11 as URLRequest) { (data, response, error) in
                
                if(error == nil)
                {
                    print("Background Syncing Success Task")
                    if(isInternetAvailable() == true)
                    {
                        self.getAllTask()
                    }
                    else
                    {
                        self.fetchTaskListFromLocalDB()
                        self.fetchActivityListFromLocalDB()
                    }
                }
                }.resume()
        }
        else
        {
            if(isInternetAvailable() == true)
            {
                self.getAllTask()
            }
            else
            {
                self.fetchTaskListFromLocalDB()
                self.fetchActivityListFromLocalDB()
            }
        }
    }
    func syncActivity()
    {
        if(isInternetAvailable() == true)
        {
            
            let arrayAllObject = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@", self.strUserName, self.strCompanyKey ))
            
            if(arrayAllObject.count > 0)
            {
                syncAllActivityWebLeadDetail(strUserName: self.strUserName, strCompanyKey: self.strCompanyKey)
            }
            else
            {
                if(isInternetAvailable() == true)
                {
                    callAPIToGetActivityList()
                }
                else
                {
                    fetchTaskListFromLocalDB()
                    fetchActivityListFromLocalDB()
                }
            }
        }
        else
        {
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()
        }
    }
    func syncAllActivityWebLeadDetail(strUserName:String, strCompanyKey:String)
    {
        let aryNonSyncActivity = NSMutableArray()
        let aryJsonToPost = NSMutableArray()
        
        let arrayAllObject = getDataFromLocal(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "userName == %@ AND companyKey == %@"  ,strUserName, strCompanyKey))
        
        for item in arrayAllObject
        {
            
            if("\((item as! NSManagedObject).value(forKey: "activityId")!)".count == 0)
            {
                aryNonSyncActivity.add(getMutableDictionaryFromNSManagedObject(obj: item as! NSManagedObject))
            }
            
        }
        
        if(aryNonSyncActivity.count > 0)
        {
            for item in aryNonSyncActivity
            {
                let dictData = item as! NSDictionary
                
                let aryOfKeys = NSMutableArray()
                let aryOfValues = NSMutableArray()
                
                
                aryOfKeys.add("ToDate")
                aryOfKeys.add("LogTypeId")
                aryOfKeys.add("Address2")
                aryOfKeys.add("EmployeeExtDcs")
                aryOfKeys.add("ActivityTime")
                aryOfKeys.add("ChildTasks")
                aryOfKeys.add("CountryId")
                aryOfKeys.add("ActivityCommentExtDcs")
                aryOfKeys.add("CityName")
                aryOfKeys.add("Agenda")
                aryOfKeys.add("StateId")
                aryOfKeys.add("CreatedDate")
                aryOfKeys.add("RefId")
                aryOfKeys.add("CreatedBy")
                aryOfKeys.add("ZipCode")
                aryOfKeys.add("ModifiedDate")
                aryOfKeys.add("Id")
                aryOfKeys.add("ActivityLogTypeMasterExtDc")
                aryOfKeys.add("Participants")
                aryOfKeys.add("ActivityId")
                aryOfKeys.add("Address1")
                aryOfKeys.add("LeadExtDcs")
                aryOfKeys.add("ModifiedBy")
                aryOfKeys.add("FromDate")
                aryOfKeys.add("RefType")
                
                // values
                aryOfValues.add("\(dictData.value(forKey: "toDate")!)")
                aryOfValues.add("\(dictData.value(forKey: "logTypeId")!)")
                aryOfValues.add("\(dictData.value(forKey: "addressLine2")!)")
                aryOfValues.add(NSArray())//EmployeeExtDcs
                aryOfValues.add("\(dictData.value(forKey: "activityTime")!)")
                aryOfValues.add(dictData.value(forKey: "childTasks") as! NSArray)
                aryOfValues.add("\(dictData.value(forKey: "countryId")!)")
                
                aryOfValues.add(NSArray())//ActivityCommentExtDcs
                aryOfValues.add("\(dictData.value(forKey: "cityName")!)")
                aryOfValues.add("\(dictData.value(forKey: "agenda")!)")
                
                aryOfValues.add("\(dictData.value(forKey: "stateId")!)")
                
                aryOfValues.add("\(dictData.value(forKey: "createdDate")!)")
                aryOfValues.add("\(dictData.value(forKey: "refId")!)")
                aryOfValues.add("\(dictData.value(forKey: "createdBy")!)")
                aryOfValues.add("\(dictData.value(forKey: "zipcode")!)")
                aryOfValues.add("\(dictData.value(forKey: "modifiedDate")!)")
                aryOfValues.add("") // id
                aryOfValues.add(NSArray()) //ActivityLogTypeMasterExtDc
                aryOfValues.add("\(dictData.value(forKey: "participants")!)")
                aryOfValues.add("\(dictData.value(forKey: "activityId")!)")
                aryOfValues.add("\(dictData.value(forKey: "addressLine1")!)")
                aryOfValues.add(NSArray())//LeadExtDcs
                aryOfValues.add("\(strEmpID)")//modifiedBy
                aryOfValues.add("\(dictData.value(forKey: "fromDate")!)")
                aryOfValues.add("\(dictData.value(forKey: "refType")!)")
                
                let dictJson = NSDictionary.init(objects: aryOfValues as! [Any], forKeys: aryOfKeys as! [NSCopying])
                
                aryJsonToPost.add(dictJson)
            }
            
            
            var jsonString = String()
            
            if(JSONSerialization.isValidJSONObject(aryJsonToPost) == true)
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: aryJsonToPost, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                
                print(jsonString)
                
            }
            
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strHrmsCompanyId = "\(dictLoginData.value(forKeyPath: "Company.HrmsCompanyId")!)"
            
            let strEmployeeNumber = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
            let strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
            let strCreatedBy = "\(dictLoginData.value(forKey: "CreatedBy")!)"
            let strEmployeeName = "\(dictLoginData.value(forKey: "EmployeeName")!)"
            let strCoreCompanyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId")!)"
            let strSalesProcessCompanyId1 = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
            let strCompanyKeyyy = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
            let strUrlSaved = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
            
            
            let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            
            let strUrl = strUrlSaved + UrlAddUpdateActivityMultiple
            let url = URL(string: strUrl)
            
            let request11 = NSMutableURLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
            
            request11.httpMethod = "POST"
            request11.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request11.addValue("application/json", forHTTPHeaderField: "Accept")
            
            request11.addValue("application/json", forHTTPHeaderField: "Accept")
            request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
            request11.addValue("IOS", forHTTPHeaderField: "Browser")
            
            request11.addValue(strHrmsCompanyId, forHTTPHeaderField: "HrmsCompanyId")
            
            request11.addValue(Global().getIPAddress(), forHTTPHeaderField: "IpAddress")
            
            request11.addValue(strEmployeeNumber, forHTTPHeaderField: "EmployeeNumber")
            
            request11.addValue(strEmployeeId, forHTTPHeaderField: "EmployeeId")
            
            request11.addValue(strCreatedBy, forHTTPHeaderField: "CreatedBy")
            
            request11.addValue(strEmployeeName, forHTTPHeaderField: "EmployeeName")
            
            request11.addValue(strCoreCompanyId, forHTTPHeaderField: "CoreCompanyId")
            
            request11.addValue(strSalesProcessCompanyId1, forHTTPHeaderField: "SalesProcessCompanyId")
            
            request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
            
            request11.addValue(strCompanyKeyyy, forHTTPHeaderField: "CompanyKey")
            
            request11.setValue("\(requestData!.count)", forHTTPHeaderField: "Content-Length")
            
            request11.httpBody = requestData
            
            URLSession.shared.dataTask(with: request11 as URLRequest) { (data, response, error) in
                
                if(error == nil)
                {
                    print("Background Syncing Success Activity")
                    
                    if(isInternetAvailable() == true)
                    {
                        self.callAPIToGetActivityList()
                    }
                    else
                    {
                        self.fetchTaskListFromLocalDB()
                        self.fetchActivityListFromLocalDB()
                    }
                }
                }.resume()
        }
        else
        {
            if(isInternetAvailable() == true)
            {
                self.callAPIToGetActivityList()
            }
            else
            {
                self.fetchTaskListFromLocalDB()
                self.fetchActivityListFromLocalDB()
            }
        }
    }
    func callAllAPISynchronously()
    {
        ChkCalledApiSynchro = true
        
        if(isInternetAvailable() == false)
        {
            /*fetchAllNotesFromCoreData()
            fetchAllDocumentsFromCoreData()
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()*/
        }
        else
        {
            
           // FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
            print(" callAllAPISynchronously start")
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
                     
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.getBasicInfoOpportunity()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
            {
                self.getAllTask()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
            {
                self.callAPToGetTimeLine()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                 self.callApiToGetContacts()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)
            {
                self.getAllNotes()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
            {
                self.getAllDocuments()
                self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                    
                    //all asynchronous tasks added to this DispatchGroup are completed. Proceed as required.
                    FTIndicator.dismissProgress()
                    print(" callAllAPISynchronously end")

                })
                
                self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                    
                  self.loader.dismiss(animated: false)
                             {
                  }
                    
                })
                
                
            }
        }
        
    }
    func callAllAPISynchronouslyOld()
    {
        if(isInternetAvailable() == false)
        {
            /*fetchAllNotesFromCoreData()
            fetchAllDocumentsFromCoreData()
            fetchTaskListFromLocalDB()
            fetchActivityListFromLocalDB()*/
        }
        else
        {
            
            FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                //self.syncTask()
                self.getBasicInfoOpportunity()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
            {
                //self.syncTask()
                self.getAllTask()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
            {
                self.callAPToGetTimeLine()
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                //self.syncActivity()
                 self.callApiToGetContacts()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)
            {
                self.getAllNotes()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
            {
                self.getAllDocuments()
                self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                    
                    //all asynchronous tasks added to this DispatchGroup are completed. Proceed as required.
                    FTIndicator.dismissProgress()
                    
                })
            }
        }
        
    }
    
    @objc func viewMoreTask(sender:UIButton)
    {
          let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone

        vc.strFromVC = "OpportunityVC"
        vc.strleadId = strRefId
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    @objc func viewMoreActivity(sender:UIButton)
    {
           let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone

        vc.strleadId = strRefId
        vc.strFromVC = "OpportunityVC"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func getEmployeeNameFromId(strEmpId: String) -> String
    {
        var strName = ""
        
        if nsud.value(forKey: "EmployeeList") is NSArray
        {
            let arrEmployeeList = nsud.value(forKey: "EmployeeList") as! NSArray
            for item in arrEmployeeList
            {
                let dict = item as! NSDictionary
                if "\(dict.value(forKey: "EmployeeId") ?? "")" == strEmpId
                {
                    strName = "\(dict.value(forKey: "FullName") ?? "")"
                    break
                }
            }
        }
        if strName.count == 0
        {
            strName = ""
        }
        
        return strName
        
    }
    
    fileprivate func goToAddNote(){
        
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
        
        vc.strRefType = enumRefTypeOpportunity
        vc.strLeadType = enumRefTypeOpportunity
        vc.strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "opportunityId") ?? "")"
        vc.isFromOpp = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    fileprivate func gotoAssociateContact(){
        
           let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
          
        controller.strFromWhere = "DirectAssociate"
        controller.strRefType = enumRefTypeOpportunity
        controller.strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "opportunityId") ?? "")"
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    fileprivate func gotoAssociateCompany(){
           let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
      
        controller.strFromWhere = "DirectAssociate"
        controller.strRefType = enumRefTypeOpportunity
        controller.strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "opportunityId") ?? "")"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    fileprivate func gotoAddDocument(){
        
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddDocumentCRM_VC") as! AddDocumentCRM_VC
        controller.strRefType = enumRefTypeOpportunity
        controller.strRefId = strRefIdNew//"\(dictLeadData.value(forKey: "opportunityId") ?? "")"
        self.navigationController?.present(controller, animated: false, completion: nil)
        
    }
    
    func goToContactList()
    {
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    func goToDasboard()
    {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func goToAppointment()
    {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
//        let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboardIpad.instantiateViewController(withIdentifier: "AppointmentView") as! AppointmentView
//        self.navigationController?.pushViewController(vc, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
        
    }
    func goToSignedViewAgreement()
    {
        //nsud.set(true, forKey: "fromCompanyVC")
       // nsud.synchronize()
    if(DeviceType.IS_IPAD){
                           let mainStoryboard = UIStoryboard(
                               name: "CRMiPad",
                               bundle: nil)
                           let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
                           self.navigationController?.present(objByProductVC!, animated: true)
                       }else{
                           let mainStoryboard = UIStoryboard(
                               name: "CRM",
                               bundle: nil)
                           let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
                           self.navigationController?.present(objByProductVC!, animated: true)
                       }
        
    }
    func goToNearBy()
    {
        /*
    if(DeviceType.IS_IPAD){
                              let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                              let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
                              self.navigationController?.pushViewController(vc!, animated: false)
                          }else{
                              let storyboard = UIStoryboard(name: "Main", bundle: nil)
                              let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
                              self.navigationController?.pushViewController(vc!, animated: false)
                          }*/
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)

    }
    @objc func clickOnStatus()
    {
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
           alert.view.tintColor = UIColor.black
        //alert.setValue(UIColor.black, forKey: "titleTextColor")
        alert.addAction(UIAlertAction(title: "Convert To Opportunity", style: .default , handler:{ (UIAlertAction)in
            
            self.goToNearBy()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Release", style: .default , handler:{ (UIAlertAction)in
            
            self.goToSignedViewAgreement()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Grab", style: .default , handler:{ (UIAlertAction)in
                   
                   self.goToSignedViewAgreement()
                   
               }))
        
        alert.addAction(UIAlertAction(title: "Mark as Dead", style: .default , handler:{ (UIAlertAction)in
                          
                          self.goToSignedViewAgreement()
                          
                      }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        alert.view.tintColor = UIColor.gray
        
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    func setViewBorderColor(view: UIView)
    {
        //67
        view.layer.borderColor = UIColor.init(red: 67/255, green: 67/255, blue: 67/255, alpha: 0.5).cgColor
        view.layer.borderWidth = 1
        view.layer.opacity = 0.5
    }
    func goToPdfView(strPdfName : String)
    {
        
       /* let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "DetailDownloadedFilesView") as? DetailDownloadedFilesView
        testController!.strFileName = strPdfName
        testController!.strFrom = "Pest"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
         let  strPdfNameNew = strPdfName.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
        guard let url = URL(string: "\(strPdfNameNew)") else { return }
        UIApplication.shared.open(url)
        
    }
    func playAudioo(strAudioName : String) {
       
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
        testController!.strAudioName = strAudioName
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    func serviceNameFromId()
    {
        
        let arrServiceName = NSMutableArray()
        let arrServiceId = NSMutableArray()
        
        if nsud.value(forKey: "LeadDetailMaster") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            if dictMaster.value(forKey: "ServiceMasters") is NSArray
            {
                let arrService = dictMaster.value(forKey: "ServiceMasters") as! NSArray
                
                if arrService.count > 0
                {
                    for itemService in arrService
                    {
                        let dictService = itemService as! NSDictionary
                        
                        arrServiceName.add("\(dictService.value(forKey: "Name") ?? "")")
                        arrServiceId.add("\(dictService.value(forKey: "ServiceId") ?? "")")
                    }
                }
            }
        }
        dictServiceNameFromId = NSDictionary.init(objects: arrServiceName as! [Any], forKeys: arrServiceId as! [NSCopying])

    }
    func goToContactDetails(dictContact : NSDictionary)
    {
        if "\(dictContact.value(forKey: "CrmContactId")!)" == "" || "\(dictContact.value(forKey: "CrmContactId")!)".count == 0
        {

            
            
        }
        else
        {
       
     let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as! ContactDetailsVC_CRMContactNew_iPhone
        controller.strCRMContactIdGlobal = "\(dictContact.value(forKey: "CrmContactId")!)"
        controller.dictContactDetailFromList = dictContact
        self.navigationController?.pushViewController(controller, animated: false)
        }
        
    }
    func goToCompanyEdit()
    {
           let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
        controller.strCRMCompanyIdGlobal = "\(dictLeadData.value(forKeyPath: "AccountCompany.CrmCompanyId")!)"
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func goToEmailDetails(dictEmail : NSDictionary){
       
        let controller = storyboardNewCRMContact.instantiateViewController(withIdentifier: "EmailDetail_iPhoneVC") as? EmailDetail_iPhoneVC
        controller?.dictEmailContain = dictEmail
        self.navigationController?.present(controller!, animated: false, completion: nil)
        
    }
    
    func goToTaskDetails(strId : String){
       
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
        vc.taskId = "\(strId)"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToActivityDetails(strId : String){
       
           let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityDetailsVC_CRMNew_iPhone") as! ActivityDetailsVC_CRMNew_iPhone
            
        vc.activityID = "\(strId)"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func goToSchdeuleNow()
    {
        
        
           let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleNowVC_CRMNew_iPhone") as! ScheduleNowVC_CRMNew_iPhone
       
        controller.dictLeadData = dictLeadData
        //controller.dictServiceNameFromId = dictServiceNameFromId
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func clearNsud()
    {
        nsud.setValue("", forKey: "cureentTabOpportunity")
        nsud.setValue("", forKey: "cureentSwipeCountOpportunity")
        nsud.synchronize()
    }
    
    func decimalValidationNew(textField : UITextField , range: NSRange , string : String) -> Bool
    {
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }
        
        let newText = oldText.replacingCharacters(in: r, with: string)
        let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1
        
        let numberOfDecimalDigits: Int
        if let dotIndex = newText.index(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
        return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2 && textField.text!.count <= 13
    }
    
    // MARK: -  ------------------------------ Message Composer Delegate ------------------------------
    
    func displayMessageInterface(strNo: String)
    {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = ["\(strNo)"]
        composeVC.body = ""
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        dismiss(animated: false, completion: nil)
    }
    // MARK: -  ------------------------------ Send Mail Delegate ------------------------------
    
    func sendEmail(strEmail: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(strEmail)"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}
extension OpportunityDetailsVC_CRMNew_iPhone : FilterTimeLineData
{
    
    func getFilterTimeLineDataOnSelection(dictData: NSMutableDictionary, strType: String) {
        
                    
        if(isInternetAvailable() == false)
        {

            tblviewOpportunityDetails.reloadData()
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
           
            FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                
                self.callAPToGetTimeLine()
                
            }
            
        }
        
    }
    
    
}
// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
extension OpportunityDetailsVC_CRMNew_iPhone:UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 77  {
            
            let count = (textField.text!.count) as Int
            
            var intMaxLenght = 0
            
            if string.contains(".") {
                
                intMaxLenght = 13
                
            }else if textField.text!.contains(".") {
                
                intMaxLenght = 13
                
            } else {
                
                intMaxLenght = 10
                
            }
            
            return Global().isNumberOnly(withDecimal: string, range, Int32(intMaxLenght), Int32(count),".0123456789", textField.text!)

        }else if textField == txtFldOpportunityValue  {
            
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk


        }
        else
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
        }
        
        //return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == txtFldOpportunityValue
        {
            if !isInternetAvailable()
            {
                
            }
            else
            {
                let amount = ("\(dictLeadData.value(forKey: "OpportunityValue") ?? "")" as NSString).doubleValue
                let amountText = ("\(txtFldOpportunityValue.text ?? "")" as NSString).doubleValue

                
                if amount == amountText
                {
                    
                }
                else
                {
                    //self.updateOpportunityValue()
                }
            }
        }
    }
}
extension FloatingPoint {
    func rounded(to n: Int) -> Self {
        let n = Self(n)
        return (self / n).rounded() * n

    }
}
