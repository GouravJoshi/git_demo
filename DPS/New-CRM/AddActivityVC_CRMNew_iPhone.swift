//
//  AddActivityVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 03/12/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  saavan Changes 2020 

import UIKit

class AddActivityVC_CRMNew_iPhone: UIViewController {
    
    // Outlets
    
    @IBOutlet weak var constHghtSearchByAcc: NSLayoutConstraint!
 
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnAccNo: UIButton!
    
    @IBOutlet weak var btnOppNo: UIButton!
    @IBOutlet weak var txtfldSearchByAccountNo: ACFloatingTextField!
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var btnTime: UIButton!
    @IBOutlet weak var btnLogType: UIButton!
    @IBOutlet weak var txtfldActivity: ACFloatingTextField!
    @IBOutlet weak var btnParticipants: UIButton!
    @IBOutlet weak var btnDueDate: UIButton!
    @IBOutlet weak var txtfldTaskName: ACFloatingTextField!
    @IBOutlet weak var btnSave: ButtonWithShadow!
    
    
    
    // variable
    
    var dictDetailsFortblView = NSDictionary()
    var dictActivity = NSDictionary()
    var dictLoginData = NSDictionary()
    var aryPopUp = NSMutableArray()
    var aryMultipleParticipants = NSMutableArray()
    var arrOfSubTask = NSMutableArray()
    var aryEmployeeList = NSArray()
    var strServiceUrlMain = ""
    var strEmpID = ""
    var strCreatedBy = ""
    var strUserName = ""
    var strCompanyKey = ""
    var strCompanyId = ""
    var strLogTypeIdd = ""
    var strSalesPersonIds = ""
    var strAccountIdByAccountNo = ""
    var strLeadIdByAccountNo = ""
    var isSelectedLeadByAccountNo = false
    var strRefTypee = ""
    var strRefIdd = ""
    var strleadIdToUpdate = ""
    var strActivityId = ""
    var strModifiedDate = ""
    var strModifyBy = ""
    var strGlobalActivityTime = ""
    var isForUpdate = false
    
    @objc var strFromVC = ""
    @objc var strLeadNo = ""
    @objc var strAccNo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        dictDetailsFortblView = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        
        strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
        
        strEmpID  = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        
        strCreatedBy  = "\(dictLoginData.value(forKey: "CreatedBy")!)"
        
        strUserName  = "\(dictLoginData.value(forKeyPath: "Company.Username")!)"
        
        strCompanyKey     = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        
        strCompanyId      = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
        
        aryEmployeeList = nsud.value(forKey: "EmployeeList") as! NSArray
        
        strRefTypee = "Employee"
        strRefIdd = strEmpID
        
        if(strFromVC == "DashBoardView")
        {
            strRefTypee = "Employee"
            strRefIdd = strEmpID
        }
        else if(strFromVC == "WebLeadVC")
        {
             constHghtSearchByAcc.constant = 0.0
            strRefTypee = "WebLead"
            strRefIdd = strleadIdToUpdate
            
            btnAccNo.setTitle("Account #: \(strAccNo)", for: .normal)
            
            btnOppNo.setTitle("Lead #: \(strLeadNo)", for: .normal)
        }
        else if(strFromVC == "OpportunityVC")
        {
             constHghtSearchByAcc.constant = 0.0
            strRefTypee = "Lead"
            strRefIdd = strleadIdToUpdate
            
            btnAccNo.setTitle("Account #: \(strAccNo)", for: .normal)
            
            btnOppNo.setTitle("Opportunity #: \(strLeadNo)", for: .normal)
        }
        //strRefTypee = "Lead"
        //strRefIdd = "227149"
        
        lblTitle.text = "Add Activity"
        
        if(isForUpdate)
        {
            constHghtSearchByAcc.constant = 0.0
            
            lblTitle.text = "Update Activity"
            btnSave.setTitle("Update", for: .normal)
            
            if(strFromVC == "OpportunityVC")
            {
                btnAccNo.setTitle("Account #: \(dictActivity.value(forKey: "accountNo")!)", for: .normal)
                
                btnOppNo.setTitle("Opportunity #: \(dictActivity.value(forKey: "leadNo")!)", for: .normal)
                
            }
            
           else if(strFromVC == "WebLeadVC")
            {
                btnAccNo.setTitle("Account #: \(dictActivity.value(forKey: "accountNo")!)", for: .normal)
                
                btnOppNo.setTitle("Lead #: \(dictActivity.value(forKey: "webLeadNo")!)", for: .normal)
                
            }
            else{
                
                if "\(dictActivity.value(forKey: "leadNo")!)".count > 0 {
                    
                    btnAccNo.setTitle("Account #: \(dictActivity.value(forKey: "accountNo")!)", for: .normal)
                    
                    btnOppNo.setTitle("Opportunity #: \(dictActivity.value(forKey: "leadNo")!)", for: .normal)
                    
                }
                if "\(dictActivity.value(forKey: "webLeadNo")!)".count > 0 {
                    
                    btnAccNo.setTitle("Account #: \(dictActivity.value(forKey: "accountNo")!)", for: .normal)
                    
                    btnOppNo.setTitle("Lead #: \(dictActivity.value(forKey: "webLeadNo")!)", for: .normal)

                }
                
            }
            
            strActivityId = "\(dictActivity.value(forKey: "activityId")!)"
            
            let strActivityDate = Global().convertDate("\(dictActivity.value(forKey: "fromDate")!)")
            
            btnDate.setTitle(strActivityDate, for: .normal)
            
            let strActivityTime = Global().convertTime("\(dictActivity.value(forKey: "activityTime")!)")
            
            btnTime.setTitle(strActivityTime, for: .normal)
            
            let aryTemp = dictDetailsFortblView.value(forKey: "ActivityLogTypeMasters") as! NSArray
            
            if(aryTemp.count > 0)
            {
                for item in aryTemp
                {
                    let dict = item as! NSDictionary
                    if("\(dict.value(forKey: "LogTypeId")!)" == "\(dictActivity.value(forKey: "logTypeId")!)")
                    {
                        btnLogType.setTitle("\(dict.value(forKey: "Name")!)", for: .normal)
                        
                        strLogTypeIdd = "\(dict.value(forKey: "LogTypeId")!)"
                        break
                    }
                }
            }
            
            txtfldActivity.text = "\(dictActivity.value(forKey: "agenda")!)"
            
            if("\(dictActivity.value(forKey: "participants")!)".count > 0)
            {
                let aryParticipantsIds = "\(dictActivity.value(forKey: "participants")!)".components(separatedBy: ",")
                
                let aryEmp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
                
                for item1 in aryParticipantsIds
                {
                    for emp in aryEmp
                    {
                        if("\(item1)" == "\((emp as! NSDictionary).value(forKey: "EmployeeNo")!)")//EmployeeId
                        {
                            aryMultipleParticipants.add(emp as! NSDictionary)
                        }
                    }
                }
                
                var termsTitle = ""
                var ids = ""
                for item in aryMultipleParticipants
                {
                    termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "FullName")!)" + ","
                    ids = ids + "\((item as! NSDictionary).value(forKey: "EmployeeId")!)" + ","
                }
                if(aryMultipleParticipants.count > 0)
                {
                    termsTitle.removeLast()
                    ids.removeLast()
                    btnParticipants.setTitle(termsTitle, for: .normal)
                    strSalesPersonIds = ids
                }
                else
                {
                    btnParticipants.setTitle("", for: .normal)
                }
            }
            
            // childTasks
            
            let arrTemp = dictActivity.value(forKey: "childTasks") as! NSArray
            
            arrOfSubTask = arrTemp.mutableCopy() as! NSMutableArray
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        makeCornerRadius(value: 20, view: btnSave, borderWidth: 0.0, borderColor: UIColor.lightGray)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    
    // MARK: UIButton action
    
    @IBAction func actionOnAccNo(_ sender: UIButton) {
    }
    
    @IBAction func actionOnOPPNo(_ sender: UIButton) {
    }
    
    @IBAction func actionOnSearchAccount(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if(isInternetAvailable() == true)
        {
            if(txtfldSearchByAccountNo.text?.count == 0)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Account Number to search", viewcontrol: self)
            }
            else
            {
                if(isInternetAvailable() == false)
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                }
                else{
                    callAPIToFetchLeadsByAccountNumber()
                }
                
            }
        }
        
        
        
        /* BOOL isNetReachable=[global isNetReachable];
         
         if (isNetReachable) {
         
         if (_txtFld_AccountNoToSearchLead.text.length==0) {
         
         [global displayAlertController:Alert :@"Please enter account no to search" :self];
         
         } else {
         
         [_txtFld_AccountNoToSearchLead resignFirstResponder];
         
         [DejalBezelActivityView activityViewForView:self.view withLabel:@"Searching..."];
         
         [self performSelector:@selector(callAPIFetchLeadsByAccountNo) withObject:nil afterDelay:0.2];
         
         }
         
         }else {
         
         [global AlertMethod:Alert :ErrorInternetMsg];
         
         }*/
    }
    
    @IBAction func actionOnDate(_ sender: UIButton) {
        
        
        sender.tag = 401
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    
    @IBAction func actinOnTime(_ sender: UIButton) {
        
        sender.tag = 402
        gotoDatePickerView(sender: sender, strType: "Time")
    }
    
    
    @IBAction func actionOnLogType(_ sender: UIButton) {
        
        
        self.view.endEditing(true)
        aryPopUp.removeAllObjects()
        
        let aryTemp = dictDetailsFortblView.value(forKey: "ActivityLogTypeMasters") as! NSArray
        
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryPopUp.add(dict)
                }
            }
            
            if(aryPopUp.count > 0)
            {
                sender.tag = 403
                
                openTableViewPopUp(tag: 403, ary: aryPopUp , aryselectedItem: NSMutableArray())
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnParticipants(_ sender: UIButton) {
        
        self.view.endEditing(true)
        aryPopUp.removeAllObjects()
        
        let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
        
        if(aryTemp.count > 0)
        {
            for item in aryTemp
            {
                let dict = item as! NSDictionary
                if(dict.value(forKey: "IsActive") as! Bool == true)
                {
                    aryPopUp.add(dict)
                }
            }
            
            if(aryPopUp.count > 0)
            {
                sender.tag = 404
                
                openTableViewPopUp(tag: 404, ary: aryPopUp , aryselectedItem: aryMultipleParticipants)
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
            }
            
        }
            
        else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnDueDate(_ sender: UIButton) {
        
        sender.tag = 405
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    
    @IBAction func actionOnSave(_ sender: ButtonWithShadow)
    {
        self.view.endEditing(true)
        nsud.set(true, forKey: "fromAddActivity")
        nsud.synchronize()
        
        let errMsg = validation()
        
        if(errMsg.count > 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: errMsg, viewcontrol: self)
        }
        else
        {
            if(isInternetAvailable() == true)
            {
                if (isForUpdate) {
                    
                    FTIndicator.showProgress(withMessage: "Updating Activity...", userInteractionEnable: false)
                    
                }else{
                    
                    FTIndicator.showProgress(withMessage: "Adding Activity...", userInteractionEnable: false)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    
                    self.callAPIToAddActivity()
                }
            }
            else
            {
                if(isForUpdate)
                {
                    //  updateActivityToLocalDBInOldTable()
                    updateActivityToLocalDBInNewTable(item: NSDictionary())
                    
                }
                else{
                    
                    // saveActivityToLocalDBInOldTable()
                    saveActivityToLocalDBInNewTable(item: NSDictionary())
                }
            }
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: Functions
    
    fileprivate func configureUI()
    {
        makeCornerRadius(value: 2.0, view: btnDate, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnTime, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnLogType, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnParticipants, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnDueDate, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    fileprivate func validation()-> String
    {
        var errorMessage = ""
        
        if(txtfldActivity.text?.count == 0)
        {
            errorMessage = "Please enter Activity name";
        }
        else if(btnDate.titleLabel?.text == nil)
        {
            errorMessage = "Please enter Date.";
        }
        else if(btnTime.titleLabel?.text == nil)
        {
            errorMessage = "Please enter Time.";
        }
        else if(btnLogType.titleLabel?.text == nil)
        {
            errorMessage = "Please select Activity Type.";
        }
        if(txtfldTaskName.text?.count != 0)
        {
            if(btnDueDate.titleLabel?.text == nil)
            {
                errorMessage = "Please enter Task Due Date";
            }
        }
        else if(btnDueDate.titleLabel?.text != nil)
        {
            if(txtfldTaskName.text?.count == 0)
            {
                errorMessage = "Please enter Task Name";
            }
        }
        
        return errorMessage
    }
    
    fileprivate func getTimeWithouAMPM(strTime:String)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let dateString1 = dateFormatter.string(from: date)
        return dateString1
    }
    
    // MARK: Web Service
    
    fileprivate func callAPIToAddActivity()
    {
        //arrOfSubTask.removeAllObjects()
        
        strRefTypee = "Employee"
        strRefIdd = strEmpID
        
        if(strFromVC == "WebLeadVC")
        {
            //  strLeadTaskId = ""
            strRefTypee = "WebLead"
            strRefIdd = strleadIdToUpdate
        }
        if(strFromVC == "DashBoardView")
        {
            // strLeadTaskId = ""
            strRefTypee = "Employee"
            strRefIdd = strEmpID
        }
        
        if(strFromVC == "OpportunityVC")
        {
            strRefTypee = "Lead"
            strRefIdd = strleadIdToUpdate
            
        }
        
        if(isSelectedLeadByAccountNo == true)
        {
            if(strLeadIdByAccountNo.count > 0)
            {
                //  strLeadTaskId = ""
                strRefTypee = "Lead"
                strRefIdd = strLeadIdByAccountNo;
            }
            else if (strAccountIdByAccountNo.count > 0){
                
                //  strLeadTaskId = ""
                strRefTypee = "Account"
                strRefIdd = strAccountIdByAccountNo
                
            } else {
                
                // strLeadTaskId = ""
                strRefTypee = "Employee"
                strRefIdd = strEmpID;
                
            }
        }
        
        if (txtfldTaskName.text?.count != 0) {
            
            var object1 = NSArray()
            var keys1 = NSArray()
            
            keys1 = ["TaskName","DueDate","CompanyId"]
            
            object1 = ["\(txtfldTaskName.text ?? "")", (btnDueDate.titleLabel?.text!)!, strCompanyId]
            
            
            
            let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
            
            if arrOfSubTask.contains(dict_ToSend1){
                
                
                
            }else{
                
                arrOfSubTask.add(dict_ToSend1)
                
            }
            
        }
        else{
            
            //arrOfSubTask.add(NSMutableArray())
            
        }
        
        let aryTemp = NSArray()
        
        if(strModifiedDate.count == 0)
        {
            strModifiedDate = Global().getModifiedDate()
        }
        
        
        
        var keys = NSArray()
        var values = NSArray()
        keys = ["Id",
                "FromDate",
                "ToDate",
                "Participants",
                "LogTypeId",
                "Agenda",
                "Address1",
                "Address2",
                "CityName",
                "StateId",
                "CountryId",
                "ZipCode",
                "CreatedDate",
                "ActivityLogTypeMasterExtDc",
                "RefType",
                "RefId",
                "ActivityId",
                "ActivityCommentExtDcs",
                "LeadExtDcs",
                "EmployeeExtDcs",
                "CreatedBy",
                "ModifiedDate",
                "ModifiedBy",
                "ChildTasks",
                "ActivityTime"]
        
        values = ["0",(btnDate.titleLabel?.text!)!,                     (btnDate.titleLabel?.text!)!,strSalesPersonIds,
                  strLogTypeIdd,
                  txtfldActivity.text!,
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  strModifiedDate,
                  aryTemp,
                  strRefTypee,
                  strRefIdd,
                  strActivityId,
                  aryTemp,
                  aryTemp,
                  aryTemp,
                  strCreatedBy,
                  strModifiedDate,
                  strModifyBy,
                  arrOfSubTask,
                  getTimeWithouAMPM(strTime: (btnTime.titleLabel?.text!)!)]
        
        let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        
        let strURL = strServiceUrlMain + UrlAddUpdateActivityGlobal
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "addActivity"
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            DispatchQueue.main.async {
                
                FTIndicator.dismissProgress()
            }
            
            if(success)
            {
                print((response as NSDictionary?)!)
                nsud.set(true, forKey: "fromAddUpdateActivity")
                nsud.synchronize()
                if(self.isForUpdate)
                {
                    self.updateActivityToLocalDBInNewTable(item: (response as NSDictionary?)!)
                }
                else
                {
                    self.saveActivityToLocalDBInNewTable(item: (response as NSDictionary?)!)
                    
                    let alertCOntroller = UIAlertController(title: "Message", message: "Activity Added Successfully", preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                        
                        self.navigationController?.popViewController(animated: false)
                    })
                    
                    alertCOntroller.addAction(alertAction)
                    self.present(alertCOntroller, animated: true, completion: nil)
                }
            }
        }
    }
    fileprivate func callAPIToFetchLeadsByAccountNumber()
    {
        let url = "\(strServiceUrlMain)" + "\(UrlGetLeadListByAccountNo)" + "\(strCompanyKey)" + "\(UrlGetLeadListByAccountNoName)" + "\(txtfldSearchByAccountNo                                                                                                                                                                                                                                                                                            .text!)"
        
        FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: url, responseStringComing: "AddTaskVC_CRMNew_iPhone") { (response, status) in
            
            FTIndicator.dismissProgress()
            
            print(response)
            
            if(status == true)
            {
                if((response.value(forKey: "Leads") as! NSArray).count == 0){
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                }
                else
                {
                    self.strAccountIdByAccountNo = "\(response.value(forKey: "AccountId")!)"
                    
                    let arytemp = (response.value(forKey: "Leads") as!  NSArray).mutableCopy() as! NSMutableArray
                    
                    self.openTableViewPopUp(tag: 400, ary: arytemp, aryselectedItem: NSMutableArray())
                }
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    
    // MARK: Core Date
    /* fileprivate func saveActivityToLocalDBInOldTable()
     {
     arrOfSubTask.removeAllObjects()
     
     strRefTypee = "Employee"
     strRefIdd = strEmpID
     
     if(strFromVC == "WebLeadVC")
     {
     //  strLeadTaskId = ""
     strRefTypee = "Lead"
     strRefIdd = strleadIdToUpdate
     }
     if(strFromVC == "DashBoardView")
     {
     // strLeadTaskId = ""
     strRefTypee = "Employee"
     strRefIdd = strEmpID
     }
     
     if(strFromVC == "OpportunityVC")
     {
     strRefTypee = "WebLead"
     }
     
     if(isSelectedLeadByAccountNo == true)
     {
     if(strLeadIdByAccountNo.count > 0)
     {
     //  strLeadTaskId = ""
     strRefTypee = "Lead"
     strRefIdd = strLeadIdByAccountNo;
     }
     else if (strAccountIdByAccountNo.count > 0){
     
     //  strLeadTaskId = ""
     strRefTypee = "Account"
     strRefIdd = strAccountIdByAccountNo
     
     } else {
     
     // strLeadTaskId = ""
     strRefTypee = "Employee"
     strRefIdd = strEmpID;
     
     }
     }
     
     if (txtfldTaskName.text?.count != 0) {
     
     var object1 = NSArray()
     var keys1 = NSArray()
     
     keys1 = ["TaskName","DueDate","CompanyId"]
     
     object1 = ["\(txtfldTaskName.text ?? "")", (btnDueDate.titleLabel?.text!)!, strCompanyId]
     
     
     
     let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
     
     arrOfSubTask.add(dict_ToSend1)
     }
     else{
     
     arrOfSubTask.add(NSMutableArray())
     }
     
     let aryTemp = NSArray()
     
     if(strModifiedDate.count == 0)
     {
     strModifiedDate = Global().getModifiedDate()
     }
     
     
     
     var keys = NSArray()
     var values = NSArray()
     
     keys = ["Id",
     "FromDate",
     "ToDate",
     "Participants",
     "LogTypeId",
     "Agenda",
     "Address1",
     "Address2",
     "CityName",
     "StateId",
     "CountryId",
     "ZipCode",
     "CreatedDate",
     "ActivityLogTypeMasterExtDc",
     "RefType",
     "RefId",
     "ActivityId",
     "ActivityCommentExtDcs",
     "LeadExtDcs",
     "EmployeeExtDcs",
     "CreatedBy",
     "ModifiedDate",
     "ModifiedBy",
     "ChildTasks",
     "ActivityTime"]
     
     values = ["0",(btnDate.titleLabel?.text!)!,                     (btnDate.titleLabel?.text!)!,strSalesPersonIds,
     strLogTypeIdd,
     txtfldActivity.text!,
     "",
     "",
     "",
     "",
     "",
     "",
     strModifiedDate,
     aryTemp,
     strRefTypee,
     strRefIdd,
     strActivityId,
     aryTemp,
     aryTemp,
     aryTemp,
     strCreatedBy,
     strModifiedDate,
     strModifyBy,
     arrOfSubTask,
     getTimeWithouAMPM(strTime: (btnTime.titleLabel?.text!)!)]
     
     saveDataInDB(strEntity: "ActivityList", arrayOfKey: keys.mutableCopy() as! NSMutableArray, arrayOfValue: values.mutableCopy() as! NSMutableArray)
     
     }*/
    
    fileprivate func saveActivityToLocalDBInNewTable(item:NSDictionary)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        let dictData = removeNullFromDict(dict: item.mutableCopy() as! NSMutableDictionary)
        
        
        arrOfKeys.add("userName")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("accountNo")
        arrOfKeys.add("activityId")
        arrOfKeys.add("agenda")
        arrOfKeys.add("logTypeId")
        arrOfKeys.add("clientCreatedDate")
        arrOfKeys.add("toDate")
        arrOfKeys.add("fromDate")
        arrOfKeys.add("participants")
        arrOfKeys.add("activityTime")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("leadNo")
        arrOfKeys.add("webLeadNo")
        arrOfKeys.add("firstName")
        arrOfKeys.add("middleName")
        arrOfKeys.add("lastName")
        arrOfKeys.add("cellPhone1")
        arrOfKeys.add("cellPhone2")
        arrOfKeys.add("primaryPhone")
        arrOfKeys.add("primaryPhoneExt")
        arrOfKeys.add("secondaryPhone")
        arrOfKeys.add("secondaryPhoneExt")
        arrOfKeys.add("primaryEmail")
        arrOfKeys.add("secondaryEmail")
        arrOfKeys.add("addressLine1")
        arrOfKeys.add("addressLine2")
        arrOfKeys.add("cityName")
        arrOfKeys.add("stateId")
        arrOfKeys.add("zipcode")
        arrOfKeys.add("countryId")
        arrOfKeys.add("companyName")
        arrOfKeys.add("refId")
        arrOfKeys.add("refType")
        arrOfKeys.add("activityComments")
        arrOfKeys.add("childTasks")
        arrOfKeys.add("uniqueId")
        arrOfKeys.add("compareDate")
        arrOfKeys.add("isSystem")
       
        if(dictData.count > 0)
        {
            
            arrOfValues.add(strUserName)
            
            arrOfValues.add(strCompanyKey)
            
            arrOfValues.add("\(dictData.value(forKey: "AccountNo")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "ActivityId")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "Agenda")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "LogTypeId")!)")
            
            if("\(dictData.value(forKey: "ClientCreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "ClientCreatedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ClientCreatedDate")!)")
                
            }
                
            else
            {
                arrOfValues.add("")
            }
            
            
            if("\(dictData.value(forKey: "ToDate")!)" != "<null>" && "\(dictData.value(forKey: "ToDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ToDate")!)")
                
            }
                
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "FromDate")!)" != "<null>" && "\(dictData.value(forKey: "FromDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "FromDate")!)")
            }
                
            else
            {
                arrOfValues.add("")
            }
            
            
            arrOfValues.add("\(dictData.value(forKey: "Participants")!)")
            arrOfValues.add("\(dictData.value(forKey: "ActivityTime")!)")
            
            if("\(dictData.value(forKey: "CreatedDate")!)" != "<null>" && "\(dictData.value(forKey: "CreatedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "CreatedDate")!)")
            }
                
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "ModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ModifiedDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ModifiedDate")!)")
            }
                
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add(strEmpID) // created by
            
            arrOfValues.add("\(dictData.value(forKey: "LeadNo")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "WebLeadNumber")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "FirstName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "MiddleName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "LastName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CellPhone1")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CellPhone2")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "PrimaryPhone")!)")
            
            arrOfValues.add("")//PrimaryPhoneExt
            
            arrOfValues.add("\(dictData.value(forKey: "SecondaryPhone")!)")
            
            arrOfValues.add("")
            
            arrOfValues.add("\(dictData.value(forKey: "PrimaryEmail")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "SecondaryEmail")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "AddressLine1")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "AddressLine2")!)") // address line2
            
            arrOfValues.add("\(dictData.value(forKey: "CityName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "StateId")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "ZipCode")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CountryId")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "CompanyName")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "RefId")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "RefType")!)")
            
            if var aryTemp = dictData.value(forKey: "ActivityCommentExtDcs") as? NSArray
            {
                
                let aryChild = NSMutableArray()
                
                aryTemp = (dictData.value(forKey: "ActivityCommentExtDcs") as! NSArray).mutableCopy() as! NSMutableArray
                
                for itemTemp in aryTemp
                {
                    aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
                    
                }
                arrOfValues.add(aryChild)
                
            }
                
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            if var aryTemp = dictData.value(forKey: "ChildTasks") as? NSArray
            {
                
                let aryChild = NSMutableArray()
                
                aryTemp = (dictData.value(forKey: "ChildTasks") as! NSArray).mutableCopy() as! NSMutableArray
                
                
                
                for itemTemp in aryTemp
                {
                    aryChild.add(removeNullFromDict(dict: (itemTemp as! NSDictionary).mutableCopy() as! NSMutableDictionary))
                }
                
                arrOfValues.add(aryChild)
                
            }
                
            else
            {
                arrOfValues.add(NSMutableArray())
            }
            
            arrOfValues.add(getUniqueValueForId())
            
            arrOfValues.add(Global().getCompareDate())
            arrOfValues.add(false)

            saveDataInDB(strEntity: "ActivityListCRMNew", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
            
        else
        {
            
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add("") // account no
            arrOfValues.add("") // activity id
            arrOfValues.add(txtfldActivity.text!)
            arrOfValues.add(strLogTypeIdd)
            arrOfValues.add("") //clientCreatedDate
            arrOfValues.add((btnDate.titleLabel?.text!)!)
            arrOfValues.add((btnDate.titleLabel?.text!)!)
            arrOfValues.add(strSalesPersonIds)
            arrOfValues.add(getTimeWithouAMPM(strTime: (btnTime.titleLabel?.text!)!))
            arrOfValues.add(Global().getModifiedDate()) // created date
            arrOfValues.add(Global().getModifiedDate()) // modified date
            
            arrOfValues.add(strEmpID) // created by
            arrOfValues.add("") // lead no
            arrOfValues.add("") //WebLeadNo
            arrOfValues.add("")//FirstName
            arrOfValues.add("")//MiddleName
            arrOfValues.add("")//LastName
            arrOfValues.add("")//CellPhone1
            arrOfValues.add("") //CellPhone2
            arrOfValues.add("")//PrimaryPhone
            arrOfValues.add("") //PrimaryPhoneExt
            arrOfValues.add("")//SecondaryPhone
            arrOfValues.add("")//SecondaryPhoneExt
            arrOfValues.add("")//PrimaryEmail
            arrOfValues.add("")//SecondaryEmail
            arrOfValues.add("")//AddressLine1
            arrOfValues.add("") // AddressLine2
            arrOfValues.add("")//CityName
            arrOfValues.add("")//StateId
            arrOfValues.add("")//Zipcode
            arrOfValues.add("")//CountryId
            arrOfValues.add("")//CompanyName
            arrOfValues.add(strRefIdd)//
            arrOfValues.add(strRefTypee)
            
            arrOfValues.add(NSMutableArray()) //ActivityComments
            
            if (txtfldTaskName.text?.count != 0) {
                
                var object1 = NSArray()
                var keys1 = NSArray()
                
                keys1 = ["TaskName","DueDate","CompanyId"]
                
                object1 = ["\(txtfldTaskName.text ?? "")", (btnDueDate.titleLabel?.text!)!, strCompanyId]
                
                let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
                
                arrOfSubTask.add(dict_ToSend1)
            }
            else{
                
                arrOfSubTask.add(NSMutableArray())
            }
            
            arrOfValues.add(arrOfSubTask)
            arrOfValues.add(getUniqueValueForId())
            
            arrOfValues.add(Global().getCompareDate())
            
            arrOfValues.add(false)
            saveDataInDB(strEntity: "ActivityListCRMNew", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        let alertCOntroller = UIAlertController(title: "Message", message: "Activity Added Successfully", preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            var isControllerFound = false
            for item in (self.navigationController?.viewControllers)!
            {
                if(item is ActivityVC_CRMNew_iPhone)
                {
                    isControllerFound = true
                    self.navigationController?.popToViewController(item, animated: false)
                    break
                }
                
            }
            if(isControllerFound == false)
            {
                self.navigationController?.popViewController(animated: false)
            }
            
        })
        alertCOntroller.addAction(alertAction)
        self.present(alertCOntroller, animated: true, completion: nil)
    }
    
    fileprivate func updateActivityToLocalDBInNewTable(item:NSDictionary)
    {
        
        var isSuccess = false
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        let dictData = removeNullFromDict(dict: item.mutableCopy() as! NSMutableDictionary)
        
        arrOfKeys.add("userName")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("agenda")
        arrOfKeys.add("logTypeId")
        arrOfKeys.add("toDate")
        arrOfKeys.add("fromDate")
        arrOfKeys.add("participants")
        arrOfKeys.add("activityTime")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("refId")
        arrOfKeys.add("refType")
        arrOfKeys.add("childTasks")
        arrOfKeys.add("compareDate")
        
        if(dictData.count > 0)
        {
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add((txtfldActivity.text!))
            arrOfValues.add("\(strLogTypeIdd)")
            
            if("\(dictData.value(forKey: "ToDate")!)" != "<null>" && "\(dictData.value(forKey: "ToDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "ToDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            if("\(dictData.value(forKey: "FromDate")!)" != "<null>" && "\(dictData.value(forKey: "FromDate")!)" != "")
            {
                arrOfValues.add("\(dictData.value(forKey: "FromDate")!)")
            }
            else
            {
                arrOfValues.add("")
            }
            
            arrOfValues.add("\(dictData.value(forKey: "Participants")!)")
            
            arrOfValues.add("\(dictData.value(forKey: "ActivityTime")!)")
            
            arrOfValues.add(Global().getModifiedDate())
            
            
            /*  if("\(dictData.value(forKey: "ModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ModifiedDate")!)" != "")
             {
             arrOfValues.add("\(dictData.value(forKey: "ModifiedDate")!)")
             
             }
             else
             {
             arrOfValues.add("")
             }*/
            
            arrOfValues.add("\(dictData.value(forKey: "RefId")!)")
            arrOfValues.add("\(dictData.value(forKey: "RefType")!)")
            
            if (txtfldTaskName.text?.count != 0) {
                
                var object1 = NSArray()
                var keys1 = NSArray()
                
                keys1 = ["TaskName","DueDate","CompanyId"]
                
                object1 = ["\(txtfldTaskName.text ?? "")", (btnDueDate.titleLabel?.text!)!, strCompanyId]
                
                let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
                
                if arrOfSubTask.contains(dict_ToSend1){
                    
                    
                    
                }else{
                    
                    arrOfSubTask.add(dict_ToSend1)
                    
                }
                
                //arrOfSubTask.add(dict_ToSend1)
                
            }
            else{
                
                //arrOfSubTask.add(NSMutableArray())
                
            }
            
            arrOfValues.add(arrOfSubTask)
            
            arrOfValues.add(Global().getCompareDate())
            
            
            if("\(dictActivity.value(forKey: "activityId")!)".count > 0)
            {
                isSuccess =  getDataFromDbToUpdate(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "activityId == %@", "\(dictActivity.value(forKey: "activityId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            else
            {
                isSuccess =  getDataFromDbToUpdate(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "uniqueId == %@", "\(dictActivity.value(forKey: "uniqueId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
        }
        else
        {
            arrOfValues.add(strUserName)
            arrOfValues.add(strCompanyKey)
            arrOfValues.add((txtfldActivity.text!))
            arrOfValues.add("\(strLogTypeIdd)")
            
            arrOfValues.add((btnDate.titleLabel?.text!)!)
            arrOfValues.add((btnDate.titleLabel?.text!)!)
            
            arrOfValues.add(strSalesPersonIds)
            
            arrOfValues.add(getTimeWithouAMPM(strTime: (btnTime.titleLabel?.text!)!))
            
            arrOfValues.add(Global().getModifiedDate())
            
            
            /*  if("\(dictData.value(forKey: "ModifiedDate")!)" != "<null>" && "\(dictData.value(forKey: "ModifiedDate")!)" != "")
             {
             arrOfValues.add("\(dictData.value(forKey: "ModifiedDate")!)")
             
             }
             else
             {
             arrOfValues.add("")
             }*/
            
            arrOfValues.add("\(dictActivity.value(forKey: "refId")!)")
            arrOfValues.add("\(dictActivity.value(forKey: "refType")!)")
            
            if (txtfldTaskName.text?.count != 0) {
                
                var object1 = NSArray()
                var keys1 = NSArray()
                
                keys1 = ["TaskName","DueDate","CompanyId"]
                
                object1 = ["\(txtfldTaskName.text ?? "")", (btnDueDate.titleLabel?.text!)!, strCompanyId]
                
                let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
                
                if arrOfSubTask.contains(dict_ToSend1){
                    
                    
                    
                }else{
                    
                    arrOfSubTask.add(dict_ToSend1)
                    
                }
            }
            else{
                
                //arrOfSubTask.add(NSMutableArray())
            }
            
            arrOfValues.add(arrOfSubTask)
            
            arrOfValues.add(Global().getCompareDate())
            
            
            if("\(dictActivity.value(forKey: "activityId")!)".count > 0)
            {
                isSuccess =  getDataFromDbToUpdate(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "activityId == %@", "\(dictActivity.value(forKey: "activityId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            else
            {
                isSuccess =  getDataFromDbToUpdate(strEntity: "ActivityListCRMNew", predicate: NSPredicate(format: "uniqueId == %@", "\(dictActivity.value(forKey: "uniqueId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
        }
        
        
        
        
        if(isSuccess)
        {
            let alertCOntroller = UIAlertController(title: "Message", message: "Activity Updated Successfully", preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                
                var isControllerFound = false
                for item in (self.navigationController?.viewControllers)!
                {
                    if(item is ActivityVC_CRMNew_iPhone)
                    {
                        isControllerFound = true
                        self.navigationController?.popToViewController(item, animated: false)
                        
                        break
                    }
                }
                if(isControllerFound == false)
                {
                    self.navigationController?.popViewController(animated: false)
                }
            })
            
            alertCOntroller.addAction(alertAction)
            self.present(alertCOntroller, animated: true, completion: nil)
            
        }
    }
    
    /*fileprivate func updateActivityToLocalDBInOldTable()
     {
     arrOfSubTask.removeAllObjects()
     
     strRefTypee = "Employee"
     strRefIdd = strEmpID
     
     if(strFromVC == "WebLeadVC")
     {
     //  strLeadTaskId = ""
     strRefTypee = "Lead"
     strRefIdd = strleadIdToUpdate
     }
     if(strFromVC == "DashBoardView")
     {
     // strLeadTaskId = ""
     strRefTypee = "Employee"
     strRefIdd = strEmpID
     }
     
     if(strFromVC == "OpportunityVC")
     {
     strRefTypee = "WebLead"
     }
     
     if(isSelectedLeadByAccountNo == true)
     {
     if(strLeadIdByAccountNo.count > 0)
     {
     //  strLeadTaskId = ""
     strRefTypee = "Lead"
     strRefIdd = strLeadIdByAccountNo;
     }
     else if (strAccountIdByAccountNo.count > 0){
     
     //  strLeadTaskId = ""
     strRefTypee = "Account"
     strRefIdd = strAccountIdByAccountNo
     
     } else {
     
     // strLeadTaskId = ""
     strRefTypee = "Employee"
     strRefIdd = strEmpID;
     
     }
     }
     
     if (txtfldTaskName.text?.count != 0) {
     
     var object1 = NSArray()
     var keys1 = NSArray()
     
     keys1 = ["TaskName","DueDate","CompanyId"]
     
     object1 = ["\(txtfldTaskName.text ?? "")", (btnDueDate.titleLabel?.text!)!, strCompanyId]
     
     
     
     let dict_ToSend1 = NSDictionary.init(objects: object1 as! [Any], forKeys: keys1 as! [NSCopying])
     
     arrOfSubTask.add(dict_ToSend1)
     }
     else{
     
     arrOfSubTask.add(NSMutableArray())
     }
     
     if(strModifiedDate.count == 0)
     {
     strModifiedDate = Global().getModifiedDate()
     }
     
     var keys = NSArray()
     var values = NSArray()
     
     keys = ["Id",
     "FromDate",
     "ToDate",
     "Participants",
     "LogTypeId",
     "Agenda",
     "CreatedDate",
     "RefType",
     "RefId",
     "ActivityId",
     "CreatedBy",
     "ModifiedDate",
     "ModifiedBy",
     "ChildTasks",
     "ActivityTime"]
     
     values = ["0",(btnDate.titleLabel?.text!)!,                     (btnDate.titleLabel?.text!)!,strSalesPersonIds,
     strLogTypeIdd,
     txtfldActivity.text!,
     strModifiedDate,
     strRefTypee,
     strRefIdd,
     strActivityId,
     strCreatedBy,
     strModifiedDate,
     strModifyBy,
     arrOfSubTask,
     (btnTime.titleLabel?.text!)!]
     
     saveDataInDB(strEntity: "ActivityList", arrayOfKey: keys.mutableCopy() as! NSMutableArray, arrayOfValue: values.mutableCopy() as! NSMutableArray)
     }*/
    
}

extension AddActivityVC_CRMNew_iPhone: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 400)
        {
            strLeadIdByAccountNo = "\(dictData.value(forKey: "LeadId")!)"
            isSelectedLeadByAccountNo = true
            
            if(strFromVC == "OpportunityVC")
            {
                btnAccNo.setTitle("Account #:\(txtfldSearchByAccountNo.text!)", for: .normal)
                
                btnOppNo.setTitle("Opportunity #: \(dictData.value(forKey: "LeadNumber")!)", for: .normal)
                
            }
            
            if(strFromVC == "WebLeadVC")
            {
                btnAccNo.setTitle("Account #:\(txtfldSearchByAccountNo.text!)", for: .normal)
                
                btnOppNo.setTitle("Lead #: \(dictData.value(forKey: "LeadNumber")!)", for: .normal)
                
            }
            else
            {
                
                btnAccNo.setTitle("Account #:\(txtfldSearchByAccountNo.text!)", for: .normal)
                
                btnOppNo.setTitle("Opportunity #: \(dictData.value(forKey: "LeadNumber")!)", for: .normal)
                
            }
            
            
            
        }
        if(tag == 403)
        {
            btnLogType.setTitle("\(dictData.value(forKey: "Name")!)", for: .normal)
            strLogTypeIdd = "\(dictData.value(forKey: "LogTypeId")!)"
        }
        if(tag == 404)
        {
            
            aryMultipleParticipants = (dictData.value(forKey: "multi") as! NSArray).mutableCopy() as! NSMutableArray
            
            var termsTitle = ""
            var ids = ""
            for item in aryMultipleParticipants
            {
                termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "FullName")!)" + ","
                ids = ids + "\((item as! NSDictionary).value(forKey: "EmployeeNo")!)" + ","
            }
            if(aryMultipleParticipants.count > 0)
            {
                termsTitle.removeLast()
                ids.removeLast()
                btnParticipants.setTitle(termsTitle, for: .normal)
                strSalesPersonIds = ids
            }
            else
            {
                btnParticipants.setTitle("", for: .normal)
            }
        }
    }
}
extension AddActivityVC_CRMNew_iPhone: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if(tag == 401)
        {
            btnDate.setTitle(strDate, for: .normal)
        }
        if(tag == 402)
        {
            btnTime.setTitle(strDate, for: .normal)
        }
        if(tag == 405)
        {
            btnDueDate.setTitle(strDate, for: .normal)
        }
        
    }
}
extension AddActivityVC_CRMNew_iPhone: UITextFieldDelegate, UITextViewDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtfldSearchByAccountNo)
        {
            if(string == " ")
            {
                return false
            }
        }
        if(textField == txtfldTaskName )
        {
            if(string == " " && txtfldTaskName.text?.count == 0)
            {
                return false
            }
            return true
        }
        
        if(textField == txtfldActivity)
        {
            if(string == " " && txtfldActivity.text?.count == 0)
            {
                return false
            }
            return true
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
