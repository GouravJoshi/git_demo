//
//  TaskDetailsVC_CRMNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 26/11/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class TaskDetailsVC_CRMNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate {
    
    // Outlets
    
    @IBOutlet weak var lblTaskName: UILabel!
    @IBOutlet weak var lblProjectTaskGroupName_ProjectName: UILabel!

    @IBOutlet weak var lblTaskDetails: UILabel!
    @IBOutlet weak var lblAssignTo: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblDueTime: UILabel!
    @IBOutlet weak var viewContainerBtnTaskType: UIView!
    @IBOutlet weak var btnTaskType: UIButton!
    @IBOutlet weak var imgviewTaskType: UIImageView!
    @IBOutlet weak var tblviewTaskDetails: UITableView!
    @IBOutlet weak var btnMarkAsDone: UIButton!
    @IBOutlet weak var viewContainerBtnMarkAsDone: UIView!
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    
    fileprivate var dictLoginData = NSDictionary()
    fileprivate var dictTaskDetails = NSMutableDictionary()
    fileprivate  var aryAssociations = NSMutableArray()
    fileprivate  var aryTaskTypeMaster = NSMutableArray()
    fileprivate  var strServiceUrlMain = ""
    var aryPriority = NSMutableArray()
    var taskId = ""
    
    var loader = UIAlertController()
    @IBOutlet weak var lbltaskCount: UILabel!
      @IBOutlet weak var lblScheduleCount: UILabel!
    // MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                         lblScheduleCount.text = "0"
                          lbltaskCount.layer.cornerRadius = 18.0
                                lbltaskCount.backgroundColor = UIColor.red
                                lblScheduleCount.layer.cornerRadius = 18.0
                                lblScheduleCount.backgroundColor = UIColor.red
                                lbltaskCount.layer.masksToBounds = true
                                lblScheduleCount.layer.masksToBounds = true

                      }
            imgviewTaskType.image = UIImage(named: "")
            setUpViews()
            getPriorityFromMaster()
            getTaskTypeFromMaster()
            print(aryTaskTypeMaster)
            tblviewTaskDetails.estimatedRowHeight = 90
            tblviewTaskDetails.rowHeight = UITableView.automaticDimension
            
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
            
            //createBadgeView()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setFooterMenuOption()
            })
        }
        
       
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setFooterMenuOption()
        })
        
      }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !Check_Login_Session_expired() {
            if isInternetAvailable() {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.callAPITogetTaskDetailsByTaskId()
                }
            }
            
            /* if  nsud.bool(forKey: "fromAddUpdateTask")  == true
             {
             self.navigationController?.popViewController(animated: false)
             }*/
        }
        
       
    }
    
    
    
    // MARK: Functions
    func setFooterMenuOption() {
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Sales", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
           self.GotoDashboardViewController()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
          self.GotoScheduleViewController()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.GotoAccountViewController()
        }
        
       
    }
    func GotoDashboardViewController() {
        var status = Bool()
        if (self.navigationController != nil) {

            for vc in  self.navigationController!.viewControllers {

                if vc is DashBoardNew_iPhoneVC {
                    status = true
                    self.navigationController?.popToViewController(vc, animated: false)
                }
            }
        }

        if !(status){

           let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC

            self.navigationController?.pushViewController(controller, animated: false)

        }

    }
    func GotoScheduleViewController() {
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        
//        let mainStoryboard = UIStoryboard(
//                                           name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Main",
//                                           bundle: nil)
//
//        let controller = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView")
//
//        self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
        
    }
    func GotoAccountViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
         self.navigationController?.pushViewController(controller, animated: false)
    }
    
    
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
                        lbltaskCount.isHidden = true
                        lblScheduleCount.isHidden = true
             }
                
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }

                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                                                   lblScheduleCount.text = strScheduleCount
                                                   lblScheduleCount.isHidden = false
                                               }else{
                        let badgeSchedule = SPBadge()
                        badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                        badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeSchedule.badge = strScheduleCount
                        btnScheduleFooter.addSubview(badgeSchedule)
                        }
                    }
                    

                    if strTaskCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                                                 lbltaskCount.text = strTaskCount
                                                 lbltaskCount.isHidden = false
                                                 
                                             }else{
                        let badgeTasks = SPBadge()
                        badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                        badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeTasks.badge = strTaskCount
                        btnTasksFooter.addSubview(badgeTasks)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    fileprivate func getPriorityFromMaster()
    {
        if nsud.value(forKey: "TotalLeadCountResponse")  is NSDictionary {
            let dictDetailsFortblView = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
            if(dictDetailsFortblView.value(forKey: "PriorityMasters") is NSArray){
                let aryTemp = dictDetailsFortblView.value(forKey: "PriorityMasters") as! NSArray
                
                if(aryTemp.count > 0)
                {
                    for item in aryTemp
                    {
                        let dict = item as! NSDictionary
                        if(dict.value(forKey: "IsActive") as! Bool == true)
                        {
                            aryPriority.add(dict)
                        }
                    }
                }
            }
            
        }
        
    }
    
    fileprivate func getTaskTypeFromMaster(){
   
        if let dict = nsud.value(forKey: "TotalLeadCountResponse")
        {
            if(dict is NSDictionary){
                
                if let aryTaskTypeTemp = (dict as! NSDictionary).value(forKey: "TaskTypeMasters") {
                    
                    if(aryTaskTypeTemp is NSArray)
                    {
                        if((aryTaskTypeTemp as! NSArray).count > 0)
                        {
                            for item in aryTaskTypeTemp as! NSArray
                            {
                                let dict = item as! NSDictionary
                                if(dict.value(forKey: "IsActive") as! Bool == true)
                                {
                                    aryTaskTypeMaster.add(dict)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func changeDateToString(date: Date)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    fileprivate func setUpViews(){
        
        viewContainerBtnTaskType.layer.cornerRadius = viewContainerBtnTaskType.frame.size.height/2.0
        viewContainerBtnTaskType.layer.borderWidth = 0.5
        viewContainerBtnTaskType.layer.borderColor = hexStringToUIColor(hex: "D0B50E").cgColor
        viewContainerBtnTaskType.layer.masksToBounds = true
        
        viewContainerBtnMarkAsDone.layer.cornerRadius = viewContainerBtnMarkAsDone.frame.size.height/2.0
        viewContainerBtnMarkAsDone.layer.borderWidth = 0.5
        viewContainerBtnMarkAsDone.layer.borderColor = hexStringToUIColor(hex: "D0B50E").cgColor
        viewContainerBtnMarkAsDone.layer.masksToBounds = true
        
    }
    
    fileprivate func showTaskDetails(){
        
        var dictAssociation = NSMutableDictionary()
        
        // adding CRM Contact
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "CrmContactName")!)", forKey: "Name")
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "CrmContactId")!)", forKey: "Id")
        dictAssociation.setValue("Contact", forKey: "Title")
        
        aryAssociations.add(dictAssociation)
        
        dictAssociation = NSMutableDictionary()
        
        // adding CRM Company
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "CrmCompanyName")!)", forKey: "Name")
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "CrmCompanyId")!)", forKey: "Id")
        dictAssociation.setValue("Company", forKey: "Title")
        aryAssociations.add(dictAssociation)
        
        dictAssociation = NSMutableDictionary()
        
        // adding Lead
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "WebLeadName")!)", forKey: "Name")
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "WebLeadId")!)", forKey: "Id")
        dictAssociation.setValue("Lead", forKey: "Title")
        aryAssociations.add(dictAssociation)
        
        dictAssociation = NSMutableDictionary()
        
        // adding Opportunity
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "LeadName")!)", forKey: "Name")
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "LeadId")!)", forKey: "Id")
        dictAssociation.setValue("Opportunity", forKey: "Title")
        aryAssociations.add(dictAssociation)
        
        dictAssociation = NSMutableDictionary()
        
        // adding Account
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "AccountDisplayName")!)", forKey: "Name")
        dictAssociation.setValue("\(dictTaskDetails.value(forKey: "AccountId")!)", forKey: "Id")
        dictAssociation.setValue("Account", forKey: "Title")
        aryAssociations.add(dictAssociation)
        
        // showing task details
        lblTaskName.text = "\(dictTaskDetails.value(forKey: "TaskName")!)"
        
        lblProjectTaskGroupName_ProjectName.text = ""
    
        let strProjectName = "\(dictTaskDetails.value(forKey: "ProjectName")!)"
        let strProjectTaskGroupName = "\(dictTaskDetails.value(forKey: "ProjectTaskGroupName")!)"
//        let strProjectName = "Satick "
//        let strProjectTaskGroupName = "Name for the TaskGroupName"
        lblProjectTaskGroupName_ProjectName.numberOfLines = 2
        if(strProjectName.count > 0 && strProjectName != "<null>")
        {
            lblProjectTaskGroupName_ProjectName.text = "Project: \(strProjectName)"
        }
        if(strProjectTaskGroupName.count > 0 && strProjectTaskGroupName != "<null>")
        {
            if(lblProjectTaskGroupName_ProjectName.text?.count != 0){
                lblProjectTaskGroupName_ProjectName.text = "\(lblProjectTaskGroupName_ProjectName.text!), Task Group: \(strProjectTaskGroupName)"

            }else{
                lblProjectTaskGroupName_ProjectName.text = "Task Group: \(strProjectTaskGroupName)"

            }
        }

        

        
        if("\(dictTaskDetails.value(forKey: "TaskDescription")!)".count > 0 && "\(dictTaskDetails.value(forKey: "TaskDescription")!)" != "<null>")
        {
            lblTaskDetails.text = "\(dictTaskDetails.value(forKey: "TaskDescription")!)"
        }
        
        if("\(dictTaskDetails.value(forKey: "AssignedToStr")!)".count > 0 && "\(dictTaskDetails.value(forKey: "AssignedToStr")!)" != "<null>")
        {
            lblAssignTo.text = "\(dictTaskDetails.value(forKey: "AssignedToStr")!)"
        }
        
        if("\(dictTaskDetails.value(forKey: "PriorityStr")!)".count > 0 && "\(dictTaskDetails.value(forKey: "PriorityStr")!)" != "<null>")
        {
            lblPriority.text = "\(dictTaskDetails.value(forKey: "PriorityStr")!)"
        }
        
        btnTaskType.setTitle(strSelectString, for: .normal)

        if("\(dictTaskDetails.value(forKey: "TaskTypeId")!)".count > 0 && "\(dictTaskDetails.value(forKey: "TaskTypeId")!)" != "<null>"){
            
            if(aryTaskTypeMaster.count > 0){
                
                for item in aryTaskTypeMaster{
                    
                    if("\(dictTaskDetails.value(forKey: "TaskTypeId")!)" == "\((item as! NSDictionary).value(forKey: "TaskTypeId")!)"){
                        
                        btnTaskType.setTitle("\((item as! NSDictionary).value(forKey: "Name")!)", for: .normal)
                        
                        
                        let strImageName = getIconNameViaTaskTypeId(strId: "\((item as! NSDictionary).value(forKey: "TaskTypeId") ?? "")")
                                       print("strImageName Print")
                                       print(strImageName)
                        self.imgviewTaskType.image = UIImage(named: strImageName)
                        
                    }
                }
            }
        }
        
        var date = ""
        var time = ""
        
        if("\(dictTaskDetails.value(forKey: "DueDate")!)".count > 0 && "\(dictTaskDetails.value(forKey: "DueDate")!)" != "<null>"){
            
            date = Global().convertDate("\(dictTaskDetails.value(forKey: "DueDate")!)")
            time = Global().convertTime("\(dictTaskDetails.value(forKey: "DueDate")!)")
        }
        
        if(date.count > 0) {
            
            lblDueDate.text = date
        }
        
        if(time.count > 0){
            
            lblDueTime.text = time
        }
        
        if("\(dictTaskDetails.value(forKey: "Status")!)" == "Open")
        {
            self.btnMarkAsDone.setTitle("Open", for: .normal)
        }
        else
        {
            self.btnMarkAsDone.setTitle("Done", for: .normal)
        }
        
        tblviewTaskDetails.reloadData()
    }
    
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray, idToShowSelected: String)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            vc.strIdToShowSelected = idToShowSelected
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    fileprivate func goToCompanyDetails(strCompanyId:String){
        
           let mainStoryboard = UIStoryboard(
               name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone",
               bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
        
        controller.strCRMCompanyIdGlobal = strCompanyId
        
        self.navigationController?.pushViewController(controller, animated: false)
    }
    fileprivate func goToContactDetails(strContactId:String , dictContact:NSDictionary){
       
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as! ContactDetailsVC_CRMContactNew_iPhone
        controller.strCRMContactIdGlobal = strContactId
        controller.dictContactDetailFromList = dictContact
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    fileprivate func goToWebLeadDetails(strWebLeadId:String)
    {
        
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadDetailsVC_CRMNew_iPhone") as! WebLeadDetailsVC_CRMNew_iPhone
        
        vc.strReftype = enumRefTypeWebLead
        
        vc.strRefIdNew = strWebLeadId
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    fileprivate func goToOpportunityDetails(strOpportunityId:String){
        
        
          let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityDetailsVC_CRMNew_iPhone") as! OpportunityDetailsVC_CRMNew_iPhone
        
        vc.strRefIdNew = strOpportunityId
        vc.strReftype = enumRefTypeOpportunity
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func gotoCreateFollowUp(dictData : NSDictionary) {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(2, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountId")!)", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadId")!)", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadId")!)", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactId")!)", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyId")!)", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountDisplayName")!)", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadName")!)", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadName")!)", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactName")!)", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyName")!)", forKeyPath: "Pre_CrmCompanyName")
        
        dictTaskDetailsData.addEntries(from: dictData as! [AnyHashable : Any])

     let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    // MARK: Web Service
    
    func callAPIToUpdateTaskMarkAsDone(dictData:NSMutableDictionary)
    {
        
        var strStatus = ""
        
        if("\(dictData.value(forKey: "Status") ?? "")" == "Open")
        {
            strStatus = "Done"
        }
        else
        {
            strStatus = "Open"
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "api/LeadNowAppToSalesProcess/ChangeTaskStatus?TaskId=\(dictData.value(forKey: "LeadTaskId")!)&Status=\(strStatus)"
    
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status == true)
                         {
                             
                                 let arrKeys = response.allKeys as NSArray
                                 
                                 if arrKeys.contains("data") {
                                     
                                     print("Yes Data is there ")
                                     if("\(response.value(forKey: "data")!)" == "true")
                                     {
                                         
                                        UserDefaults.standard.set(true, forKey: "refreshTimelineWhenBackFromTaskDetail")

                                        UserDefaults.standard.set(true, forKey: "RefreshTasks_ContactDetails")

                                        UserDefaults.standard.set(true, forKey: "RefreshTasks_CompanyDetails")
                                        
                                         nsud.setValue(true, forKey: "isTaskAddedUpdated")
                                         nsud.synchronize()
                                         
                                         self.dictTaskDetails.setValue(strStatus, forKey: "Status")
                                                         
                                         self.btnMarkAsDone.setTitle(strStatus, for: .normal)

                                         if strStatus == "Done"
                                         {
                                            
                                             let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "LogAsActivityVC") as! LogAsActivityVC
                                             controller.dictLeadTaskData = NSDictionary()
                                             controller.leadTaskId = "\((dictData.value(forKey: "LeadTaskId")!))"
                                            controller.dictOfAssociations = NSMutableDictionary()
                                             self.navigationController?.pushViewController(controller, animated: false)

                                         }
                                         
                                     }
                                     else
                                     {
                                         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                     }
                                     
                                 } else {
                                     
                                     print("Data is not there ")
                                     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                             
                                 }

                         }
                         else
                         {
                             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                 }
            }
        }
        
    }
        
    fileprivate func callAPITogetTaskDetailsByTaskId(){
        
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/GetTaskById?LeadTaskId=\(taskId)"
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskDetailsVC_CRMNew_iPhone") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status == true)
                           {
                               if (response.value(forKey: "data") is NSDictionary)
                               {
                                  var dict = response.value(forKey: "data") as! NSDictionary
                                
                                   dict = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))! as NSDictionary
                                 
                                   self.dictTaskDetails = dict.mutableCopy() as! NSMutableDictionary

                                   print(self.dictTaskDetails)
                               
                                   self.aryAssociations.removeAllObjects()
                                   self.showTaskDetails()
                               }
                               else
                               {
                                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                               }
                           }
                           else
                           {
                               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                               // something went wrong
                           }
            }
            
        
            
           
        }
    }
    
    fileprivate func callAPIToDissociateEntity(strEntityName:String){
        
        var entityType = ""
        if(strEntityName == "Contact"){
             entityType = "CrmContact"
         }
        else if(strEntityName == "Company"){
            entityType = "CrmCompany"
         }
        else if(strEntityName == "Lead"){
            entityType = "WebLead"
         }
        else if(strEntityName == "Opportunity"){
            entityType = "Lead"
         }
         else{
             entityType = "Account"
         }
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/DissociateTaskWithEntityType?TaskId=\(taskId)&EntityType=\(entityType)"
       
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskDetailsVC_CRMNew_iPhone") { (response, status) in
            
            self.dismiss(animated: false) {

                if(status == true)
                {
                    if (response.value(forKey: "data") is Bool)
                    {
                        if(response.value(forKey: "data") as! Bool == true){
                            
                            UserDefaults.standard.set(true, forKey: "RefreshTasks_ContactDetails")

                            UserDefaults.standard.set(true, forKey: "RefreshTasks_CompanyDetails")
                            
                            self.callAPITogetTaskDetailsByTaskId()
                         /*   for (index, item) in self.aryAssociations.enumerated(){
                                
                                let dict = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                if("\(dict.value(forKey: "Title")!)" == strEntityName){
                                   
                                    dict.setValue("", forKey: "Name")
                                    self.aryAssociations.replaceObject(at: index, with: dict)
                                    self.tblviewTaskDetails.reloadData()
                                    break
                                }
                            }*/
                        }
                        else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    // something went wrong
                }
            }
            
        }
    }
    
    fileprivate func callAPIToUpdateTaskType(dict:NSDictionary){
        
        var strTaskTypeId = ""
        
        if dict.count > 0 {
            
             strTaskTypeId = "\(dict.value(forKey: "TaskTypeId")!)"

        } else {
            
             strTaskTypeId = ""
            
        }
        
        let strURL = strServiceUrlMain + "api/LeadNowAppToSalesProcess/UpdateTaskType?TaskId=\(taskId)&TaskTypeId=\(strTaskTypeId)"
     
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskDetailsVC_CRMNew_iPhone") { (response, status) in
            
            self.loader.dismiss(animated: false) {
                if(status == true)
                           {
                               if (response.value(forKey: "data") is Bool)
                               {
                                   if(response.value(forKey: "data") as! Bool == true){
                                   
                                       nsud.setValue(true, forKey: "isTaskAddedUpdated")
                                       nsud.synchronize()
                                       
                                    UserDefaults.standard.set(true, forKey: "RefreshTasks_ContactDetails")

                                    UserDefaults.standard.set(true, forKey: "RefreshTasks_CompanyDetails")
                                    
                                       self.dictTaskDetails.setValue(strTaskTypeId, forKey: "TaskTypeId")
                                    
                                       print(self.dictTaskDetails)
                                       let strImageName = getIconNameViaTaskTypeId(strId: "\(strTaskTypeId)")
                                       print("strImageName Print")
                                       print(strImageName)
                                       self.imgviewTaskType.image = UIImage(named: strImageName)
                                       if dict.count > 0 {
                                           self.btnTaskType.setTitle("\(dict.value(forKey: "Name")!)", for: .normal)
                                           
                                       }else{
                                           
                                           self.btnTaskType.setTitle(strSelectString, for: .normal)

                                       }
                                       
                                       showAlertWithoutAnyAction(strtitle:"Message" , strMessage: "Task Type updated successfully.", viewcontrol: self)
                                       
                                       }
                                   
                                   else{
                                       showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                   }
                               }
                               else
                               {
                                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                               }
                           }
                           else
                           {
                               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                               // something went wrong
                           }
            }
            
           
        }
        
    }
    
    // MARK: -  ------------------------------ Message Composer Delegate ------------------------------
    
    func displayMessageInterface(strNo: String)
    {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = ["\(strNo)"]
        composeVC.body = ""
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        dismiss(animated: false, completion: nil)
    }
    // MARK: -  ------------------------------ Send Mail Delegate ------------------------------
    
    func sendEmail(strEmail: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(strEmail)"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    
    // MARK: UITableView delegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryAssociations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTaskAssociates") as! LeadVCCell
        
        let dict = aryAssociations[indexPath.row] as! NSDictionary
        
        cell.lblHeader.text = "\(dict.value(forKey: "Title")!)"
        
        if let name = dict.value(forKey: "Name"){
            
            if("\(name)".count > 0 && "\(name)" != "<null>"){
                
                cell.lblTitle.text = "\(name)"
            }
            else{
                cell.lblTitle.text = " "
            }
        }
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(actionOnDissociate), for: .touchUpInside)
        
        /* let dictData = aryDetails.object(at: indexPath.row) as! NSDictionary
         
         var strDateNew = "", strCreatedDateNew = ""
         
         if("\(dictData.value(forKey: "fromDate")!)".count > 0)
         {
         strDateNew = Global().convertDate("\(dictData.value(forKey: "fromDate")!)")
         }
         
         if("\(dictData.value(forKey: "clientCreatedDate")!)".count > 0)
         {
         strCreatedDateNew = Global().convertDate("\(dictData.value(forKey: "clientCreatedDate")!)")
         }
         else if("\(dictData.value(forKey: "createdDate")!)".count > 0)
         {
         
         strCreatedDateNew = Global().convertDate("\(dictData.value(forKey: "createdDate")!)")
         }
         
         cell.lblActivtyDate.text = strDateNew
         cell.lblCreatedDate.text = strCreatedDateNew
         if("\(dictData.value(forKey: "activityTime")!)".count > 0)//activityTime
         {
         let strTime = Global().convertTime("\(dictData.value(forKey: "activityTime")!)")
         
         cell.lblTime.text = strTime
         }
         
         
         cell.lblParticipants.text = " "
         if("\(dictData.value(forKey: "participants")!)".count > 0)
         {
         let  ary = "\(dictData.value(forKey: "participants")!)".components(separatedBy: ",")
         var name = ""
         for item in ary
         {
         name = name + "," + getParticipantNameFromId(id: item)
         }
         if(name.count > 0)
         {
         name.removeFirst()
         }
         
         cell.lblParticipants.text = name
         cell.lblParticipants.textAlignment = .left
         }
         */
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let dict = aryAssociations[indexPath.row] as! NSDictionary
        
        if("\(dict.value(forKey: "Name")!)".count > 0 && "\(dict.value(forKey: "Name")!)" != "<null>"){
            return UITableView.automaticDimension
        }
         return 0
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
      
        let dict = self.aryAssociations[indexPath.row] as! NSDictionary
      
        if("\(dict.value(forKey: "Title")!)" == "Contact"){
            goToContactDetails(strContactId: "\(dict.value(forKey: "Id")!)" , dictContact: dict)
        }
        else if("\(dict.value(forKey: "Title")!)" == "Company"){
           
            goToCompanyDetails(strCompanyId: "\(dict.value(forKey: "Id")!)")
        }
        else if("\(dict.value(forKey: "Title")!)" == "Lead"){//weblead
       
            goToWebLeadDetails(strWebLeadId: "\(dict.value(forKey: "Id")!)")
        }
        else if("\(dict.value(forKey: "Title")!)" == "Opportunity"){//weblead
            
            goToOpportunityDetails(strOpportunityId: "\(dict.value(forKey: "Id")!)")
        }
        
    }
    
    // MARK: UIButton action
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        //UserDefaults.standard.set(true, forKey: "refreshTimelineWhenBackFromTaskDetail")
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnAddTask(_ sender: UIButton)
    {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
     
     let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    @IBAction func actionOnEditTasks(_ sender: UIButton) {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(1, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "AccountId") ?? "")", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "LeadId") ?? "")", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "WebLeadId") ?? "")", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "CrmContactId") ?? "")", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "CrmCompanyId") ?? "")", forKeyPath: "Pre_CrmCompanyId")
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "AccountDisplayName") ?? "")", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "LeadName") ?? "")", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "WebLeadName") ?? "")", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "CrmContactName") ?? "")", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("\(self.dictTaskDetails.value(forKey: "CrmCompanyName") ?? "")", forKeyPath: "Pre_CrmCompanyName")
        
        dictTaskDetailsData.addEntries(from: dictTaskDetails as! [AnyHashable : Any])

        
       let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func actionOnTaskType(_ sender: UIButton) {
        
        if(aryTaskTypeMaster.count > 0){
            //"\(dictTaskDetails.value(forKey: "TaskTypeId")!)"
            openTableViewPopUp(tag: 1001, ary: aryTaskTypeMaster, aryselectedItem: NSMutableArray(), idToShowSelected: "\(dictTaskDetails.value(forKey: "TaskTypeId") ?? "")")
        }
    }
    
    
    @IBAction func actionOnDashboard(_ sender: UIButton) {
        GotoDashboardViewController()

    }
    
    @IBAction func actionOnLeads(_ sender: UIButton) {
  
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func actionOnAppointment(_ sender: UIButton) {
       
        self.GotoScheduleViewController()
        
    }
    
    @IBAction func actionOnContact(_ sender: UIButton) {
     
        self.GotoAccountViewController()
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        
        self.view.endEditing(true)

        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
           alert.view.tintColor = UIColor.black
        let Near = (UIAlertAction(title: enumNearBy, style: .default , handler:{ (UIAlertAction)in
               /*if(DeviceType.IS_IPAD){
                                let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
                                self.navigationController?.pushViewController(vc!, animated: false)
                            }else{
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
                                self.navigationController?.pushViewController(vc!, animated: false)
                            }*/
            let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
            self.navigationController?.pushViewController(vc!, animated: false)

            
        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
                         Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                         alert.addAction(Near)
        
         let Signed = (UIAlertAction(title: enumSignedAgreement, style: .default , handler:{ (UIAlertAction)in
            
      if(DeviceType.IS_IPAD){
                               let mainStoryboard = UIStoryboard(
                                   name: "CRMiPad",
                                   bundle: nil)
                               let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
                               self.navigationController?.present(objByProductVC!, animated: true)
                           }else{
                               let mainStoryboard = UIStoryboard(
                                   name: "CRM",
                                   bundle: nil)
                               let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
                               self.navigationController?.present(objByProductVC!, animated: true)
                           }
            
        }))
        
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
                                     Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                                     alert.addAction(Signed)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        if let popoverController = alert.popoverPresentationController {
                            popoverController.sourceView = sender as UIView
                            popoverController.sourceRect = sender.bounds
                            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                        }
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    @IBAction func actionOnMarkAsDone(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var strMsg = ""
        
        if("\(dictTaskDetails.value(forKey: "Status") ?? "")" == "Open")
        {
            strMsg = "Mark As Done"
        }
        else
        {
            strMsg = "Open"
        }
        

        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
                 
        alert.addAction(UIAlertAction(title: strMsg, style: .default , handler:{ (UIAlertAction)in
                    
            self.callAPIToUpdateTaskMarkAsDone(dictData: self.dictTaskDetails)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
          if let popoverController = alert.popoverPresentationController {
                    popoverController.sourceView = sender as UIView
                    popoverController.sourceRect = sender.bounds
                    popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
                }
                
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    @objc func actionOnDissociate(sender: UIButton!)
    {
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            let alert = UIAlertController(title: "Message", message: "Are you sure you want to dissociate?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default , handler:{ (UIAlertAction)in
                
                let dict = self.aryAssociations[sender.tag] as! NSDictionary
               
               
               DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                   
                   self.callAPIToDissociateEntity(strEntityName: "\(dict.value(forKey: "Title")!)")
               }
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .default , handler:{ (UIAlertAction)in
                
                self.dismiss(animated: false, completion: nil)
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}
extension TaskDetailsVC_CRMNew_iPhone: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        self.dismiss(animated: false, completion: nil)
      
        if(isInternetAvailable() == true)
        {
            let alertController = UIAlertController(title: "Message", message: "Do you want to update task type?", preferredStyle: .alert)
              
              let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default)
              {
                  UIAlertAction in
                  
                           
                           DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                               
                              self.callAPIToUpdateTaskType(dict: dictData)
                            
                           }
                    
              }
              
              let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
              
              // Add the actions
              alertController.addAction(okAction)
              alertController.addAction(cancelAction)
            
              // Present the controller
               self.present(alertController, animated: true, completion: nil)
        }
        else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
}
