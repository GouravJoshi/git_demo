//
//  ServiceDocumentsViewController.h
//  DPS
//
//  Created by Saavan Patidar on 20/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  Saavan 2021

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"


@interface ServiceDocumentsViewController : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityServiceDocs;
    NSManagedObjectContext *context;
    NSFetchRequest *requestServiceDocs;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matchesServiceDocs;
    NSArray *arrAllObjServiceDocs;
}
- (IBAction)action_Back:(id)sender;
- (IBAction)action_Refresh:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewDocs;
@property (strong, nonatomic) NSString *strAccountNo;
- (IBAction)action_BackPdf:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *webViewww;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceDocs;
@property (strong, nonatomic) IBOutlet UIView *pdfView;
@end
