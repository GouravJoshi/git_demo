//
//  TexasTermiteServiceDetail+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 14/11/17.
//
//

#import "TexasTermiteServiceDetail+CoreDataProperties.h"

@implementation TexasTermiteServiceDetail (CoreDataProperties)

+ (NSFetchRequest<TexasTermiteServiceDetail *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TexasTermiteServiceDetail"];
}

@dynamic companyKey;
@dynamic userName;
@dynamic workorderId;
@dynamic texasTermiteServiceDetail;

@end
