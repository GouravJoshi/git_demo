//
//  ServiceFinalJson+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 30/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ServiceFinalJson+CoreDataProperties.h"

@implementation ServiceFinalJson (CoreDataProperties)

@dynamic companyKey;
@dynamic empId;
@dynamic userName;
@dynamic finalJson;
@dynamic workOrderId;

@end
