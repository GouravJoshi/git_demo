//
//  ServiceGraphCollectionViewCell.h
//  DPS
//gfhjgfgh
//  Created by Saavan Patidar on 09/10/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceGraphCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageBefore;

@property (strong, nonatomic) IBOutlet UILabel *lblStats;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnShow;

@end
