//
//  HistoryView.m
//  DPS
//
//  Created by Rakesh Jain on 29/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  2021
//  Saavan Patidar

#import "HistoryView.h"

#import "HistoryTableViewCell.h"
#import "OutBoxView.h"
#import "SettingsView.h"

#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "Global.h"
#import "Header.h"
#import "UIColor+globalColor.h"
#import "DPS-Swift.h"

@interface HistoryView ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *arrOfLeads;
    Global *global;
    NSMutableArray *arrOfHistory;
    NSString *strCustomerTabType;
    UIRefreshControl *refreshControl;

}

@end

@implementation HistoryView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];

    arrOfHistory =[[NSMutableArray alloc]init];
    
    //=============
    
    [self downloadHistory];
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsTab=[NSUserDefaults standardUserDefaults];
    strCustomerTabType = [defsTab valueForKey:@"CustomerTabType"];
    
    //============================================================================
    //============================================================================

    if ([strCustomerTabType isEqualToString:@"NewCustomer"]) {
        
        arrOfLeads=@[@"ADD ACCOUNT",@"HISTORY"];
        _scroll_SlideView.delegate=self;
        CGFloat scrollWidth=arrOfLeads.count*150;
        [_scroll_SlideView setFrame:CGRectMake(100,0,320,65)];
        [_scroll_SlideView setContentSize:CGSizeMake(scrollWidth,65)];
        [_scroll_SlideView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    else if([strCustomerTabType isEqualToString:@"ExistingCustomer"]){
        
        arrOfLeads=@[@"EXISTING",@"HISTORY"];
        _scroll_SlideView.delegate=self;
        CGFloat scrollWidth=arrOfLeads.count*150;
        [_scroll_SlideView setFrame:CGRectMake(100,0,320,65)];
        [_scroll_SlideView setContentSize:CGSizeMake(scrollWidth,65)];
        [_scroll_SlideView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    else{
        
        arrOfLeads=@[@"EXISTING",@"ADD ACCOUNT",@"HISTORY"];
        _scroll_SlideView.delegate=self;
        CGFloat scrollWidth=arrOfLeads.count*150;
        [_scroll_SlideView setFrame:CGRectMake(100,0,320,65)];
        [_scroll_SlideView setContentSize:CGSizeMake(scrollWidth,65)];
        [_scroll_SlideView setContentOffset:CGPointMake(140, 0) animated:YES];
    }
   // [self addButtons];
    //============================================================================
    //============================================================================

    //============================================================================
    //============================================================================
    
    _tblView_History.rowHeight=UITableViewAutomaticDimension;
    _tblView_History.estimatedRowHeight=200;
    _tblView_History.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    // Do any additional setup after loading the view.
    
    // Initialize the refresh control.
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor lightTextColor];
    refreshControl.tintColor = [UIColor themeColor];
    [refreshControl addTarget:self
                       action:@selector(reloadTableDataHistory)
             forControlEvents:UIControlEventValueChanged];
    NSString *title = [NSString stringWithFormat:@"Pull to refresh"];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    refreshControl.attributedTitle = attributedTitle;
    [_tblView_History addSubview:refreshControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
//============================================================================
#pragma mark- Methods
//============================================================================
//============================================================================

- (void)reloadTableDataHistory
{
    
    [refreshControl endRefreshing];
    [self downloadHistory];
    
}

//============================================================================
//============================================================================
#pragma mark- TableView Delegate and Data Source Methods
//============================================================================
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrOfHistory.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"HistoryTableViewCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
- (void)configureCell:(HistoryTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    // ([[matches valueForKey:@"companyId"] isEqualToString:@""]) ? @"" : [matches valueForKey:@"companyId"]
    NSDictionary *dict =[arrOfHistory objectAtIndex:indexPath.row];
    
    NSString *strFullName;
    
    NSString *strAccCompany=([[dict valueForKey:@"AccountCompany"] isKindOfClass:[NSNull class]]) ? @"" : [dict valueForKey:@"AccountCompany"];
    if (!(strAccCompany.length==0)) {
        
        NSString *strAccContact=([[dict valueForKey:@"AccountContact"] isKindOfClass:[NSNull class]]) ? @"" : [dict valueForKey:@"AccountContact"];
        
        if (!(strAccContact.length==0)) {
            
            strFullName=[NSString stringWithFormat:@"%@ / %@",strAccCompany,strAccContact];
            
        }
        else{
            
            strFullName=[NSString stringWithFormat:@"%@",strAccCompany];
            
        }
        
    }else{
        
        NSString *strAccContact=([[dict valueForKey:@"AccountContact"] isKindOfClass:[NSNull class]]) ? @"" : [dict valueForKey:@"AccountContact"];
        
        if (!(strAccContact.length==0)) {
            
            strFullName=[NSString stringWithFormat:@"%@",strAccContact];
            
        }else{
            
            strFullName=[NSString stringWithFormat:@"N/A"];
            
        }
        
    }
    
    cell.lbl_LeadName.text = strFullName;//([[dict valueForKey:@"LeadName"] isKindOfClass:[NSNull class]]) ? @"" : [dict valueForKey:@"LeadName"];
    cell.lbl_TechNote.text = [NSString stringWithFormat:@"Tech Note : %@",([[dict valueForKey:@"TechNote"] isKindOfClass:[NSNull class]]) ? @"" : [dict valueForKey:@"TechNote"]];
    if (cell.lbl_TechNote.text.length==0) {
        cell.lbl_TechNote.text=@"NA";
    }
    
    cell.lbl_AccountNo.text = [NSString stringWithFormat:@"Acc# : %@",([[dict valueForKey:@"AccountNumber"] isKindOfClass:[NSNull class]]) ? @"" : [dict valueForKey:@"AccountNumber"]];
    
    cell.lbl_LeadStatus.text = [NSString stringWithFormat:@"Status : %@",([[dict valueForKey:@"Status"] isKindOfClass:[NSNull class]]) ? @"" : [dict valueForKey:@"Status"]];
    cell.lbl_Date.text=[NSString stringWithFormat:@"Date :%@",[global ChangeDateToLocalDate:([[dict valueForKey:@"DateCreated"] isKindOfClass:[NSNull class]]) ? @"" : [dict valueForKey:@"DateCreated"]]];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.alpha = 0.4;
    cell.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void)
     {
         cell.alpha = 1;
         cell.transform = CGAffineTransformMakeScale(1, 1);
     } completion:^(BOOL finished){ }];
    
}
//============================================================================
//============================================================================
#pragma mark- Action Button Methods
//============================================================================
//============================================================================

- (IBAction)action_OutBox:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"fromHistoryTOutBox"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    OutBoxView
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"OutBoxView"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

- (IBAction)action_Settings:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"fromHistory"];
    [defs synchronize];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SettingsView
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SettingsView"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

- (IBAction)action_refresh:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HistoryView
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"HistoryView"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

- (IBAction)action_Back:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"DashBoard"
                                                             bundle: nil];
    DashBoardNew_iPhoneVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardNew_iPhoneVC"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

//============================================================================
//============================================================================
#pragma mark- ----------------Methods----------------
//============================================================================
//============================================================================

-(void)downloadHistory{
    
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *days = [prefs stringForKey:@"HistoryDays"];
    if (days.length<1) {//if number of days not available set 100 as default
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        days=@"100";
        [prefs setObject:@"100" forKey:@"HistoryDays"];
        [prefs synchronize];
    }
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];

        NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMain,UrlGetHistory,days,UrlGetHistoryAddId,strEmployeeId];
    
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching History..."];
    
        //============================================================================
        //============================================================================
    
        NSString *strType=@"History";
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         arrOfHistory=[response valueForKey:@"LeadHistoryExtSerDcs"];
                         if (arrOfHistory.count==0) {
                             
                             UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Information..!!" message:@"No History Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             [alert show];
                             
                             [_tblView_History setHidden:YES];
                             
                         } else {
                             [_tblView_History reloadData];
                         }
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
        //============================================================================
        //============================================================================
}

@end
