//
//  EquipmentDynamicForm+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 21/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "EquipmentDynamicForm+CoreDataProperties.h"

@implementation EquipmentDynamicForm (CoreDataProperties)

+ (NSFetchRequest<EquipmentDynamicForm *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"EquipmentDynamicForm"];
}

@dynamic arrFinalInspection;
@dynamic companyKey;
@dynamic departmentId;
@dynamic empId;
@dynamic isSentToServer;
@dynamic userName;
@dynamic workorderId;
@dynamic mobileEquipId;
@dynamic woEquipId;

@end
