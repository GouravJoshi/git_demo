//
//  TotalLeads+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 27/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TotalLeads+CoreDataProperties.h"

@implementation TotalLeads (CoreDataProperties)

@dynamic arrOfLeads;
@dynamic companyKey;
@dynamic empId;
@dynamic userName;

@end
