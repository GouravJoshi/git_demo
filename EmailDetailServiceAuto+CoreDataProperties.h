//
//  EmailDetailServiceAuto+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EmailDetailServiceAuto.h"

NS_ASSUME_NONNULL_BEGIN

@interface EmailDetailServiceAuto (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *workorderId;
@property (nullable, nonatomic, retain) NSString *woInvoiceMailId;
@property (nullable, nonatomic, retain) NSString *emailId;
@property (nullable, nonatomic, retain) NSString *subject;
@property (nullable, nonatomic, retain) NSString *isMailSent;
@property (nullable, nonatomic, retain) NSString *isCustomerEmail;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString   *modifiedDate;
@property (nullable, nonatomic, retain) NSString *modifiedBy;//isDefaultEmail
@property (nullable, nonatomic, retain) NSString *isDefaultEmail;//isInvoiceMailSent
@property (nullable, nonatomic, retain) NSString *isInvoiceMailSent;
@end

NS_ASSUME_NONNULL_END
