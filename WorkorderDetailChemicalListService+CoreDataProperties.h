//
//  WorkorderDetailChemicalListService+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WorkorderDetailChemicalListService.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorkorderDetailChemicalListService (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *workorderId;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) id chemicalList;
@property (nullable, nonatomic, retain) id otherChemicalList;
@end

NS_ASSUME_NONNULL_END
