//
//  OutBox+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 06/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OutBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface OutBox (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companKey;
@property (nullable, nonatomic, retain) NSString *datenTime;
@property (nullable, nonatomic, retain) id finalJson;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) id dictData;

@end

NS_ASSUME_NONNULL_END
