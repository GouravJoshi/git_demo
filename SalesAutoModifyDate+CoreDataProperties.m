//
//  SalesAutoModifyDate+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 03/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SalesAutoModifyDate+CoreDataProperties.h"

@implementation SalesAutoModifyDate (CoreDataProperties)

@dynamic companyKey;
@dynamic modifyDate;
@dynamic userName;
@dynamic leadIdd;

@end
