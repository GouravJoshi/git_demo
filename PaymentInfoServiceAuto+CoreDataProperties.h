//
//  PaymentInfoServiceAuto+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PaymentInfoServiceAuto.h"

NS_ASSUME_NONNULL_BEGIN

@interface PaymentInfoServiceAuto (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *woPaymentId;
@property (nullable, nonatomic, retain) NSString *workorderId;
@property (nullable, nonatomic, retain) NSString *paymentMode;
@property (nullable, nonatomic, retain) NSString *paidAmount;
@property (nullable, nonatomic, retain) NSString *checkNo;
@property (nullable, nonatomic, retain) NSString *drivingLicenseNo;
@property (nullable, nonatomic, retain) NSString *expirationDate;
@property (nullable, nonatomic, retain) NSString *checkFrontImagePath;
@property (nullable, nonatomic, retain) NSString *checkBackImagePath;
@property (nullable, nonatomic, retain) NSString *createdDate;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *modifiedDate;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSString *userName;

@end

NS_ASSUME_NONNULL_END
