//
//  ServiceDocuments+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 20/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ServiceDocuments+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ServiceDocuments (CoreDataProperties)

+ (NSFetchRequest<ServiceDocuments *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSDate   *scheduleStartDate;
@property (nullable, nonatomic, copy) NSString *leadDocumentId;
@property (nullable, nonatomic, copy) NSString *leadId;
@property (nullable, nonatomic, copy) NSString *accountNo;
@property (nullable, nonatomic, copy) NSString *docType;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *fileName;
@property (nullable, nonatomic, copy) NSString *descriptions;
@property (nullable, nonatomic, copy) NSString *isAgreementSigned;
@property (nullable, nonatomic, copy) NSString *signatureLinkforAgree;
@property (nullable, nonatomic, copy) NSString *leadNumber;

@end

NS_ASSUME_NONNULL_END
