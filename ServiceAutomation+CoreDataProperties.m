//
//  ServiceAutomation+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 27/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ServiceAutomation+CoreDataProperties.h"

@implementation ServiceAutomation (CoreDataProperties)

@dynamic dictOfWorkOrders;
@dynamic userName;
@dynamic companyKey;
@dynamic empId;

@end
