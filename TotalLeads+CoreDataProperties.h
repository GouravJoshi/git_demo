//
//  TotalLeads+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 27/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TotalLeads.h"

NS_ASSUME_NONNULL_BEGIN

@interface TotalLeads (CoreDataProperties)

@property (nullable, nonatomic, retain) id arrOfLeads;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *empId;
@property (nullable, nonatomic, retain) NSString *userName;

@end

NS_ASSUME_NONNULL_END
