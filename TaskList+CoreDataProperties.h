//
//  TaskList+CoreDataProperties.h
//  DPS
//  Saavan Patidar 2021 Ji Patidar Ji
//  Created by Saavan Patidar on 06/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TaskList.h"

NS_ASSUME_NONNULL_BEGIN

@interface TaskList (CoreDataProperties)

@property (nullable, nonatomic, retain) id arrOfTask;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *date;
@property (nullable, nonatomic, retain) NSString *typeOfTask;
@property (nullable, nonatomic, retain) NSNumber *uniqueId;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) id dictData;

@end

NS_ASSUME_NONNULL_END
