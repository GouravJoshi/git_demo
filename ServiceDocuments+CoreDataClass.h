//
//  ServiceDocuments+CoreDataClass.h
//  DPS
//
//  Created by Saavan Patidar on 20/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceDocuments : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ServiceDocuments+CoreDataProperties.h"
