//
//  WorkOrderEquipmentDetails+CoreDataClass.h
//  DPS
//
//  Created by Saavan Patidar on 18/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface WorkOrderEquipmentDetails : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "WorkOrderEquipmentDetails+CoreDataProperties.h"
