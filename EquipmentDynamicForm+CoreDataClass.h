//
//  EquipmentDynamicForm+CoreDataClass.h
//  DPS
//
//  Created by Saavan Patidar on 21/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface EquipmentDynamicForm : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "EquipmentDynamicForm+CoreDataProperties.h"
