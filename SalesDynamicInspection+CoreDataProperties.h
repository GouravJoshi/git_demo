//
//  SalesDynamicInspection+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 21/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SalesDynamicInspection.h"

NS_ASSUME_NONNULL_BEGIN

@interface SalesDynamicInspection (CoreDataProperties)

@property (nullable, nonatomic, retain) id arrFinalInspection;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *empId;
@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *isSentToServer;


@end

NS_ASSUME_NONNULL_END
