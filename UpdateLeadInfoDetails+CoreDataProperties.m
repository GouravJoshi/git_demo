//
//  UpdateLeadInfoDetails+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 14/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UpdateLeadInfoDetails+CoreDataProperties.h"

@implementation UpdateLeadInfoDetails (CoreDataProperties)

@dynamic companyKey;
@dynamic userName;
@dynamic leadInfoDict;

@end
